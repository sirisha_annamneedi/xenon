package com.xenon.test.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;

public class Utility {

	private JSONObject temp = new JSONObject();

	public JSONObject convertSession(HttpSession session) throws JSONException {
		temp = new JSONObject();
		Enumeration<String> sessionKeys = session.getAttributeNames();
		while (sessionKeys.hasMoreElements()) {
			String val = sessionKeys.nextElement().toString();
			//System.out.println(val);
			temp.put(val, session.getAttribute(val).toString());
		}

		temp.remove("userPhoto");
		temp.remove("userProfilePhoto");
		temp.remove("customerLogo");
		return temp;
	}

	public void updateSession() {

	}

	@SuppressWarnings("unchecked")
	public HashMap<String, Object> convertJsonStringToJson(String jsonObjectStr) throws JSONException {

		HashMap<String, Object> map = new HashMap<String, Object>();
		JSONObject jObj = new JSONObject(jsonObjectStr);
		Iterator<String> keys = jObj.keys();

		while (keys.hasNext()) {
			String key = keys.next().toString();
			Object value = jObj.getString(key);
			map.put(key, value);
		}

		return map;
	}
	
	public String getAPIMsg(int statusCode, String action, String thing){
		String result = null;
		
		switch (statusCode) {
		  case 200: // Created
	        result = thing +" is successfully "+ action;
	        break;
		  case 201: // Created
		        result = thing +" is successfully "+ action;
		        break;
		  case 208: // Duplicate
			    result = "Duplicate record found while "+ action +" "+thing;
			    break;
		  case 403: // Forbidden
			    result = action +" " + thing +" is forbidden";
			    break;
		  case 500: // Internal Server Error       
			    result = "Internal Server Error while "+thing +" "+action;
			    break;
		  case 412: // Internal Server Error       
			    result = "Precondition failed while "+action +" "+thing;
			    break;
		  default: //Internal Server Error
			  	result ="Internal Server Error";
			  	break;
		}
		return result;
	}
	
	public String getCurrentDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public org.json.simple.JSONObject convertStringToJson(String jsonObjectStr) {

		org.json.simple.parser.JSONParser par = new org.json.simple.parser.JSONParser();
		org.json.simple.JSONObject json = null;
		try {
			json = (org.json.simple.JSONObject) par.parse(jsonObjectStr);
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public void verifyPropertyValue(HashMap<String, String> config) {
		Set<String> keys = config.keySet();
		for (String key : keys) {
			Assert.assertTrue(!config.get(key).isEmpty(), "Value of " + key + " should not empty.");
		}
	}
}

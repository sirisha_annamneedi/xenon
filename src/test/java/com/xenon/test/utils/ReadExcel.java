package com.xenon.test.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Assert;

public class ReadExcel {

	private static final Logger logger = LogManager.getLogger(ReadExcel.class);
	private XSSFWorkbook workbook = null;
	private XSSFSheet sheet = null;
	private XSSFCell cell = null;
	private HashMap<String, Object[][]> exceldata = new HashMap<String, Object[][]>();

	@SuppressWarnings("deprecation")
	public String[][] getExcelData(String xlsFilePath, String sheetName) {
		String[][] tabArray = null;
		try {
			logger.info("Reading xlsx file: " + xlsFilePath + " for factory method.");

			int ci = 0, cj = 0, noOfRow = 0, noOfCols = 0;
			cell = null;
			FileInputStream file;

			file = new FileInputStream(new File(xlsFilePath));

			workbook = new XSSFWorkbook(file);
			sheet = workbook.getSheet(sheetName);
			noOfRow = sheet.getLastRowNum();
			noOfCols = workbook.getSheet(sheetName).getRow(0).getLastCellNum();
			tabArray = new String[noOfRow][noOfCols];
			int celcounter = 0;
			cj = 0;
			int totCol = sheet.getRow(0).getLastCellNum();
			for (ci = 0; ci < noOfRow; ci++) {
				cj = celcounter;
				for (int c = 0; c < totCol; c++) {
					cell = sheet.getRow(ci + 1).getCell(c, Row.RETURN_BLANK_AS_NULL);
					tabArray[ci][cj] = getCellValueAsString(cell);
					cj++;
				}
			}

			file.close();
			return (tabArray);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail("Not able to read excel file: " + xlsFilePath);
		}
		return (tabArray);
	}

	@SuppressWarnings("deprecation")
	public HashMap<String, Object[][]> getExcelSheetAllData(String xlsFilePath) {
		String[][] tabArray = null;
		try {
			logger.info("Reading xlsx file: " + xlsFilePath + " for factory method.");
			FileInputStream file;

			file = new FileInputStream(new File(xlsFilePath));
			List<String> sheets = getExcelSheet(xlsFilePath);
			workbook = new XSSFWorkbook(file);

			for (String sheetName : sheets) {
				int ci = 0, cj = 0, noOfRow = 0, noOfCols = 0;
				cell = null;
				sheet = workbook.getSheet(sheetName);
				noOfRow = sheet.getLastRowNum();
				noOfCols = workbook.getSheet(sheetName).getRow(0).getLastCellNum();
				tabArray = new String[noOfRow][noOfCols];
				int celcounter = 0;
				cj = 0;
				int totCol = sheet.getRow(0).getLastCellNum();
				for (ci = 0; ci < noOfRow; ci++) {
					cj = celcounter;
					for (int c = 0; c < totCol; c++) {
						cell = sheet.getRow(ci + 1).getCell(c, Row.RETURN_BLANK_AS_NULL);
						tabArray[ci][cj] = getCellValueAsString(cell);
						cj++;
					}
				}
				exceldata.put(sheetName, checkOption(tabArray));
			}

			file.close();
			return (exceldata);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail("Not able to read excel file: " + xlsFilePath);
		}
		return (exceldata);
	}

	@SuppressWarnings("resource")
	private List<String> getExcelSheet(String xlsFilePath) {
		List<String> sheetList = new ArrayList<String>();
		try {
			logger.info("Reading xlsx file: " + xlsFilePath + " for factory method.");
			FileInputStream file = new FileInputStream(new File(xlsFilePath));
			XSSFWorkbook my_xlsx_workbook = new XSSFWorkbook(file);
			int noOfSheet = my_xlsx_workbook.getNumberOfSheets();
			for (int sheetno = 0; sheetno < noOfSheet; sheetno++) {
				XSSFSheet sheet = my_xlsx_workbook.getSheetAt(sheetno);
				sheetList.add(sheet.getSheetName().toString());
			}
			file.close();
			return (sheetList);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail("Not able to read excel file: " + xlsFilePath);
		}
		return (sheetList);
	}

	@SuppressWarnings("deprecation")
	public String getCellValueAsString(Cell cell) {
		String strCellValue = null;
		if (cell != null) {
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				strCellValue = cell.toString();
				break;
			case Cell.CELL_TYPE_NUMERIC:
				if (DateUtil.isCellDateFormatted(cell)) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
					strCellValue = dateFormat.format(cell.getDateCellValue());
				} else {
					Double value = cell.getNumericCellValue();
					Long longValue = value.longValue();
					strCellValue = new String(longValue.toString());
				}
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				strCellValue = new String(new Boolean(cell.getBooleanCellValue()).toString());
				break;
			case Cell.CELL_TYPE_BLANK:
				strCellValue = "";
				break;
			}
		} else {
			strCellValue = "";
		}
		return strCellValue;
	}

	private String[][] checkOption(String xlsData[][]) {

		String[][] tabArray = new String[0][0];
		String data = "";
		int noOfRow = xlsData.length;
		if (noOfRow > 0) {
			int noOfCols = xlsData[0].length;
			int newRows = 0;
			// Calculate no of rows with y option only for second Column.
			for (int ci = 0; ci < noOfRow; ci++) {
				data = xlsData[ci][1];
				if (data.equalsIgnoreCase("y")) {
					newRows++;
				}
			}
			int rowNo = 0;
			if (newRows > 0) {
				tabArray = new String[newRows][noOfCols];
				for (int ci = 0; ci < noOfRow; ci++) {
					data = xlsData[ci][1];
					if (data.equalsIgnoreCase("y")) {
						for (int cj = 0; cj < noOfCols; cj++) {
							tabArray[rowNo][cj] = xlsData[ci][cj];
						}
						rowNo++;
					}
				}
			}
			//System.out.println("No. of  Rows with Y option :" + newRows);
			return removeCol(tabArray, 1);
		} else {
			//System.out.println("Input excel sheet is empty.");
			return tabArray;
		}
	}

	private String[][] removeCol(String[][] tabArray, int colRemove) {
		int row = tabArray.length;
		int col = tabArray[0].length;
		String[][] newArray = new String[row][col - 1];
		for (int i = 0; i < row; i++) {
			for (int j = 0, currColumn = 0; j < col; j++) {
				if (j != colRemove) {
					newArray[i][currColumn++] = tabArray[i][j];
				}
			}
		}
		return newArray;
	}
}

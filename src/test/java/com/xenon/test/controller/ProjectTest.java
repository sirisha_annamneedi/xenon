package com.xenon.test.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xenon.api.administration.ProjectService;
import com.xenon.api.common.UtilsService;
import com.xenon.test.database.ProjectDB;
import com.xenon.test.database.UserDB;
import com.xenon.test.utils.Utility;

@RestController
public class ProjectTest {
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	UtilsService utilsService;

	private static final Logger logger = LoggerFactory.getLogger(ProjectTest.class);
	private Utility util = new Utility();
	private ProjectDB projectDb=new ProjectDB();
	private HashMap<String, Object>data = new HashMap<String, Object>();
	private JSONObject sessionData;
	private UserDB userDb = new UserDB();
	
	@RequestMapping(value = "/testInsertProject", method = RequestMethod.POST)
	public Map<String, String> createProject(@RequestParam("projectName") String projectName,@RequestParam("jiraprojectName") String jiraProjectName,
			@RequestParam("projectDescription") String projectDescription, @RequestParam("bugPrefix") String bugPrefix,
			@RequestParam("tcPrefix") String tcPrefix, @RequestParam("proStatus") String proStatus,
			String seesionDetails) throws Exception {
		Map<String, String> reponseData=new HashMap<String,String>();
		data = util.convertJsonStringToJson(seesionDetails);
		//System.out.println("===");
		String schema = data.get("cust_schema").toString();
		 
		reponseData = projectService.insertProject(projectName, jiraProjectName, projectDescription, bugPrefix, tcPrefix, proStatus, schema,utilsService.getCurrentDateTime());
		
		return reponseData;
	}
	@RequestMapping(value = "/testUserId", method = RequestMethod.POST)
	public void createProject(@RequestParam("userName") String userName){

	}
	
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request set current project of user
	 * 
	 * @param projectName
	 * @param seesionDetails
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/testSetCurrentProject", method = RequestMethod.POST)
	public String testSetCurrentProject(@RequestParam("projectName") String projectName,
			@RequestParam("seesionDetails") String seesionDetails) throws Exception {
		
		String apiMessage;
		int flag;
		sessionData  = new JSONObject();
		
		data = util.convertJsonStringToJson(seesionDetails);
		String schema =data.get("cust_schema").toString();
		int userId =Integer.parseInt(data.get("userId").toString());
		List<Map<String, Object>> prjdetails = projectDb.getProjectDetailsByName(projectName, schema);
		if(prjdetails.size()>0)
		{
			int projectId=Integer.parseInt(prjdetails.get(0).get("project_id").toString());
			
			flag = projectService.setCurrentProject(userId, projectId, schema);
			
			apiMessage = util.getAPIMsg(flag, "Set", "Application");
			
			sessionData = util.convertStringToJson(seesionDetails);
			
			sessionData.put("apiMessage", apiMessage);
			
			return sessionData.toString();
		}else{
			logger.error("Project not found.");
			flag = 412;
			apiMessage = util.getAPIMsg(flag, "Set", "Application");
			
			sessionData = util.convertStringToJson(seesionDetails);
			
			sessionData.put("apiMessage", apiMessage);
			
			return sessionData.toString();
		}
	}
	
	@RequestMapping(value = "/testGetAllProjects", method = RequestMethod.GET)
	public String testGetAllProjects(String customerName) throws Exception {
		
		//data = util.convertJsonStringToJson(seesionDetails);
		String schema = "jadetest11282016151116";//data.get("cust_schema").toString();

		List<Map<String, Object>> prjdetails = projectDb.getAllProjectDetails(schema);
		
		return prjdetails.toString();
		
		
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/testAssignAppToUsers", method = RequestMethod.POST)
	public String testAssignAppToUsers(@RequestParam("appName") String appName,
			@RequestParam("userEmailIds") String userEmailIds,
			@RequestParam("seesionDetails") String seesionDetails) throws Exception {
		
		String apiMessage;
		int flag;
		sessionData  = new JSONObject();
		
		data = util.convertJsonStringToJson(seesionDetails);
		String schema =data.get("cust_schema").toString();
		int  loginUser = Integer.parseInt(data.get("userId").toString());
		
		List<Map<String, Object>> prjdetails = projectDb.getProjectDetailsByName(appName, schema);
		if(prjdetails.size()>0)
		{
			int appId = Integer.parseInt(prjdetails.get(0).get("project_id").toString());
			
			String users = userDb.getUsersByEmailID(userEmailIds, schema);
			
			if(users.length() > 0){
				projectService.assignProjectToUsers(appId, appName, users, loginUser, schema); 
				
				flag = 200;
				apiMessage = util.getAPIMsg(flag, "Set", "Application");
				
				sessionData = util.convertStringToJson(seesionDetails);
				
				sessionData.put("apiMessage", apiMessage);
				
				return sessionData.toString();
			}else{
				logger.error("Users not found.");
				flag = 412;
				apiMessage = util.getAPIMsg(flag, "Assign", "Application");
				
				sessionData = util.convertStringToJson(seesionDetails);
				
				sessionData.put("apiMessage", apiMessage);
				
				return sessionData.toString();
			}	
		}else{
			logger.error("Project not found.");
			flag = 412;
			apiMessage = util.getAPIMsg(flag, "Assign", "Application");
			
			sessionData = util.convertStringToJson(seesionDetails);
			
			sessionData.put("apiMessage", apiMessage);
			
			return sessionData.toString();
		}
	}
}

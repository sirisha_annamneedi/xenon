package com.xenon.test.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xenon.api.buildmanager.ModuleService;
import com.xenon.api.buildmanager.UserService;
import com.xenon.test.database.ModuleDB;
import com.xenon.test.database.ProjectDB;
import com.xenon.test.database.UserDB;
import com.xenon.test.utils.Utility;

@RestController
public class ModuleTest {
	private static final Logger logger = LoggerFactory.getLogger(ModuleTest.class);
		
		@Autowired
		ModuleService moduleService;

		@Autowired
		UserService userService;
		
		private Utility util = new Utility();
		private HashMap<String, Object>data = new HashMap<String, Object>();
		private ProjectDB projectDb=new ProjectDB();
		private JSONObject sessionData;
		private ModuleDB moduleDb=new ModuleDB();
		private UserDB userDb = new UserDB();
		
		@RequestMapping(value = "/testInsertModule", method = RequestMethod.POST)
		public void createProject(@RequestParam("moduleName") String moduleName,
				@RequestParam("moduleDescription") String moduleDescription,
				@RequestParam("projectName") String projectName,
				@RequestParam("moduleStatus") String moduleStatus,
				String seesionDetails) throws Exception {
			
			data = util.convertJsonStringToJson(seesionDetails);
			String schema =data.get("cust_schema").toString();
			String roleId=data.get("roleId").toString();
			List<String> bmAccess = Arrays.asList(data.get("bm_access").toString().split(","));
			int projectId = 0 ;
			List<Map<String, Object>> prjdetails = projectDb.getProjectDetailsByName(projectName, schema);
			if(prjdetails.size()>0)
			{
				projectId=Integer.parseInt(prjdetails.get(0).get("project_id").toString());
				@SuppressWarnings("unused")
				int result = moduleService.insertModule(bmAccess, moduleName, moduleDescription, projectId, moduleStatus, schema,roleId);
			}
			
			//System.out.println("===");
			
		}
		
		
		@SuppressWarnings("unchecked")
		@RequestMapping(value = "/testAssignModuleToUsers", method = RequestMethod.POST)
		public String testAssignModuleToUsers(@RequestParam("moduleName") String moduleName,
					@RequestParam("projectName") String projectName,
					@RequestParam("userEmailIds") String userEmailIds,
					@RequestParam("seesionDetails") String seesionDetails) throws Exception {
				
				String apiMessage;
				int flag;
				sessionData  = new JSONObject();
				data = util.convertJsonStringToJson(seesionDetails);
				String schema =data.get("cust_schema").toString();
				int  loginUser = Integer.parseInt(data.get("userId").toString());
				
				List<Map<String, Object>> moddetails = moduleDb.getModuleDetailsByName(moduleName, schema);
				if(moddetails.size()>0)
				{
					int moduleID = Integer.parseInt(moddetails.get(0).get("module_id").toString());
					String users = userDb.getUsersByEmailID(userEmailIds, schema);
					
						if(users.length() > 0){
						moduleService.assignModuleToUsers(projectName,moduleID, moduleName, users, loginUser, schema);
						
						flag = 200;
						apiMessage = util.getAPIMsg(flag, "Set", "Module");
						
						sessionData = util.convertStringToJson(seesionDetails);
						
						sessionData.put("apiMessage", apiMessage);
						
						return sessionData.toString();
						}else{
						logger.error("Users not found.");
						flag = 412;
						apiMessage = util.getAPIMsg(flag, "Assign", "Module");
						
						sessionData = util.convertStringToJson(seesionDetails);
						
						sessionData.put("apiMessage", apiMessage);
						
						return sessionData.toString();	
				
						}
				}
				else
				{
					logger.error("Project not found.");
					flag = 412;
					apiMessage = util.getAPIMsg(flag, "Assign", "Module");
					
					sessionData = util.convertStringToJson(seesionDetails);
					
					sessionData.put("apiMessage", apiMessage);
					
					return sessionData.toString();
				
				}
				
}
		
		
		
	
}

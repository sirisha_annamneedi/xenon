package com.xenon.test.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xenon.api.buildmanager.ModuleService;
import com.xenon.api.buildmanager.UserService;
import com.xenon.api.buildmanager.impl.UserServiceImpl;
import com.xenon.test.database.ModuleDB;
import com.xenon.test.database.ProjectDB;
import com.xenon.test.database.UserDB;
import com.xenon.test.utils.Utility;

@RestController
public class UserTest extends AbstractTestNGSpringContextTests {
	@Resource(name = "configurationProperties")
	private Properties configurationProperties;
	
	private static final Logger logger = LoggerFactory.getLogger(UserTest.class);

	@Autowired
	UserService userService = new UserServiceImpl();
	
	@Autowired
	ModuleService moduleService;
	
	private UserDB userDb = new UserDB();
	private ModuleDB moduleDb = new ModuleDB();
	private ProjectDB projectDb=new ProjectDB();
	private Utility util = new Utility();
	private HashMap<String, Object> data = new HashMap<String, Object>();
	private int result = 0 ;
	private JSONObject sessionData;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/testInsertUser", method = RequestMethod.POST)
	public String createProject(@RequestParam("emailId") String emailId, @RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName,
			@RequestParam(value = "password", defaultValue = "admin@123") String passWord,
			@RequestParam(value = "userRole", defaultValue = "0") String userRole,
			@RequestParam(value = "tmStatus") String tmStatus, @RequestParam(value = "btStatus") String btStatus,
			@RequestParam(value = "userStatus") String userStatus,
			@RequestParam(value = "projects", defaultValue = "No Projects") String projects, String seesionDetails)
			throws Exception {
		data = util.convertJsonStringToJson(seesionDetails);
		String schema = data.get("cust_schema").toString();
		sessionData  = new JSONObject();
		String apiMessage;
		String emailCheck = userService.checkUser(emailId);
		
		if(emailCheck=="true"){
			List<String> bmAccess = Arrays.asList(data.get("bm_access").toString().split(","));
			String customerName = data.get("customerName").toString();
			String createDateTime = util.getCurrentDateTime();
			String roleId=data.get("roleId").toString();
			
			result = userService.insertUser(emailId, firstName, lastName, passWord, userRole, tmStatus, btStatus, userStatus, schema, customerName, projects, createDateTime,configurationProperties.getProperty("XENON_REPOSITORY_PATH"),roleId,1,1,0,0);
			
			sessionData = util.convertStringToJson(seesionDetails);
			
			apiMessage = util.getAPIMsg(result, "Created", "User");

			sessionData.put("apiMessage", apiMessage);
			
			return sessionData.toString();
		}else{
			result = 208;
			sessionData = util.convertStringToJson(seesionDetails);
			
			apiMessage = util.getAPIMsg(result, "Creating", "User");
			
			sessionData.put("apiMessage", apiMessage);
			
			return sessionData.toString();
		}
	}

	@RequestMapping(value = "/testUpdateUser", method = RequestMethod.GET)
	public List<Map<String, Object>> updateUser(@RequestParam("emailId") String emailId) throws Exception {

		//System.out.println("===");
		List<Map<String, Object>> userData = userDb.getUserDetails(emailId);

		return userData;

	}


	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request assigns projects to user
	 * 
	 * @param user
	 * @param projectNames
	 * @param seesionDetails
	 * @throws Exception
	 */
	@RequestMapping(value = "/testAssignProjectsToUser", method = RequestMethod.POST)
	public void testAssignProjectsToUser(@RequestParam("user") String user, @RequestParam("projectNames") String projectNames, @RequestParam("seesionDetails") String seesionDetails) throws Exception {

		//System.out.println("===");
		List<Map<String, Object>> userDetails = userDb.getUserDetails(user);
		if (userDetails.size() > 0) {
			int userId = Integer.parseInt(userDetails.get(0).get("user_id").toString());

			data = util.convertJsonStringToJson(seesionDetails);
			String schema = data.get("cust_schema").toString();
			String appIds = projectDb.getProjectIdsFromNames(projectNames, schema);

			if(appIds.length()>0)
			userService.assignProjectsToUser(userId, user, appIds, schema);
			else
				logger.error("Projects not found......");
		}
	}
	
//	
//	@RequestMapping(value = "/testSetUserCurrentProject", method = RequestMethod.POST)
//	public void setCurrentProject() throws Exception {
//		//int userId, int projectId, String SchemeName
//		//projectAPI.setCurrentProject(userId, projectId, SchemeName);
//		//System.out.println("===");
//		
//	}


	@RequestMapping(value = "/testAssignModulesToUser", method = RequestMethod.POST)
	public void testAssignModulesToUser(@RequestParam("user") String userName,
			@RequestParam("projectName") String projectName, @RequestParam("moduleNames") String moduleNames,
			 @RequestParam("seesionDetails") String seesionDetails) throws Exception {

		data = util.convertJsonStringToJson(seesionDetails);
		//System.out.println("===");
		String schema = data.get("cust_schema").toString();
		List<Map<String, Object>> userDetails = userDb.getUserDetails(userName);
		List<Map<String, Object>> projectDetails = projectDb.getProjectDetailsByName(projectName, schema);
		if(userDetails.size()>0 &&  projectDetails.size()>0){
			int userId =Integer.parseInt(userDetails.get(0).get("user_id").toString());
			int projectId = Integer.parseInt(projectDetails.get(0).get("project_id").toString());
			String modules="";
			modules=moduleDb.getModuleIdsFromNames(moduleNames, projectId, userId, schema);
			userService.assignModuleToUser(userId, projectId, modules, schema);
			//System.out.println("===");
		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/testUnassignUserProject", method = RequestMethod.POST)
	public String testUnassignUserProject(@RequestParam("user") String user, @RequestParam("projectName") String projectName, @RequestParam("seesionDetails") String seesionDetails) throws Exception {
		String apiMessage;
		int flag;
		sessionData  = new JSONObject();
		
		List<Map<String, Object>> userDetails = userDb.getUserDetails(user);
		if (userDetails.size() > 0) {
			int userId = Integer.parseInt(userDetails.get(0).get("user_id").toString());
			data = util.convertJsonStringToJson(seesionDetails);
			String schema = data.get("cust_schema").toString();
			int appId = projectDb.getProjectIdByName(projectName, schema);
			if(appId !=0){
				flag = userService.unAssignProjectsToUser(appId, projectName, userId, schema,Integer.parseInt(data.get("userId").toString()));
				apiMessage = util.getAPIMsg(flag, "Unassigned", "Application");
				
				sessionData = util.convertStringToJson(seesionDetails);
				
				sessionData.put("apiMessage", apiMessage);
				
				return sessionData.toString();
			}else{
				logger.error("Application not found.");
				flag = 412;
				apiMessage = util.getAPIMsg(flag, "Unassigning", "Application");
				
				sessionData = util.convertStringToJson(seesionDetails);
				
				sessionData.put("apiMessage", apiMessage);
				
				return sessionData.toString();
			}
		}else{
			logger.error("User not found.");
			flag = 412;
			apiMessage = util.getAPIMsg(flag, "Unassigning", "Application");
			
			sessionData = util.convertStringToJson(seesionDetails);
			
			sessionData.put("apiMessage", apiMessage);
			
			return sessionData.toString();
		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/testUnassignUserModule", method = RequestMethod.POST)
	public String testUnassignUserModule(@RequestParam("user") String userName,
			@RequestParam("projectName") String projectName, @RequestParam("moduleName") String moduleName,
			@RequestParam("seesionDetails") String seesionDetails) throws Exception {

		data = util.convertJsonStringToJson(seesionDetails);
		String apiMessage;
		int flag;
		sessionData  = new JSONObject();
		//System.out.println("===");
		String schema = data.get("cust_schema").toString();
		List<Map<String, Object>> userDetails = userDb.getUserDetails(userName);
		List<Map<String, Object>> projectDetails = projectDb.getProjectDetailsByName(projectName, schema);
		if (userDetails.size() > 0 && projectDetails.size() > 0) {
			int userId = Integer.parseInt(userDetails.get(0).get("user_id").toString());
			int moduleId = moduleDb.getModuleIdByName(moduleName, schema);
			if (moduleId != 0){
				flag = moduleService.unassignModule(userId, moduleId, schema);
				apiMessage = util.getAPIMsg(flag, "Unassigned", "Module");
				
				sessionData = util.convertStringToJson(seesionDetails);
				
				sessionData.put("apiMessage", apiMessage);
				
				return sessionData.toString();
			}else{
				flag = 412;
				apiMessage = util.getAPIMsg(flag, "Unassigning", "Module");
				
				sessionData = util.convertStringToJson(seesionDetails);
				
				sessionData.put("apiMessage", apiMessage);
				
				return sessionData.toString();
			}
		}else{
			flag = 412;
			apiMessage = util.getAPIMsg(flag, "Unassigning", "Module");
			
			sessionData = util.convertStringToJson(seesionDetails);
			
			sessionData.put("apiMessage", apiMessage);
			
			return sessionData.toString();
		}

	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/testRemoveUser", method = RequestMethod.POST)
	public String testRemoveUser(@RequestParam("emailID") String emailID,
			@RequestParam("seesionDetails") String seesionDetails) throws Exception {
		
		data = util.convertJsonStringToJson(seesionDetails);
		String apiMessage;
		String flag;
		sessionData  = new JSONObject();
		String schema = data.get("cust_schema").toString();
		
		//get the user id from mail id
		int userID = userDb.getUserIDbyEmail(emailID, schema);
		if(userID != 0){ 
			// user is present with this email id
			flag = userService.removeUser(userID, schema);
			
			
			apiMessage = util.getAPIMsg(Integer.parseInt(flag), "Removed", "User");
			
			sessionData = util.convertStringToJson(seesionDetails);
			
			sessionData.put("apiMessage", apiMessage);
			
			return sessionData.toString();	
			
		}else{
			// user is not present with this email id
			flag = "500";
			apiMessage = util.getAPIMsg(Integer.parseInt(flag), "Removing", "User");
			
			sessionData = util.convertStringToJson(seesionDetails);
			
			sessionData.put("apiMessage", apiMessage);
			
			return sessionData.toString();	
		}
	}	
}

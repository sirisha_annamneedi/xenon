package com.xenon.api.common;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.xenon.common.dao.UserDetailsDAO;
import com.xenon.test.utils.ReadExcel;

@ContextConfiguration(locations = { "classpath:/WEB-INF/spring/appServlet/servlet-context.xml" })
@WebAppConfiguration
@Test(groups = "login")
public class LoginServiceTest extends AbstractTestNGSpringContextTests {

	@Autowired
	LoginService loginService;

	@Autowired
	UserDetailsDAO userDetailsDAO;

	private ReadExcel excel = new ReadExcel();

	String filePath = System.getProperty("user.dir") +File.separator +"src"+File.separator+"test"+File.separator+"resources"+File.separator+"Login"+File.separator+"loginTestData.xlsx";

	@BeforeClass
	public void executeBefore() {

	}

	
	@DataProvider(name = "AuthenticateUserTest")
	public Object[][] checkUserTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "AuthenticateUserTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "validation" }, dataProvider = "AuthenticateUserTest")
	public void authenticateUserTest(String userName, String password) {
		HttpSession session = new MockHttpSession();
		String result = "";
		try {
			result = loginService.authenticateUser(userName, password, session);
			if(result=="xedashboard"||result=="password"){
				result="1";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(result, "1","login not  successful");
	}

	@DataProvider(name = "AuthenticateSuperAdminUserTest")
	public Object[][] authenticateSuperAdminUserTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "AuthenticateSuperAdminUserTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "validation" }, dataProvider = "AuthenticateSuperAdminUserTest")
	public void authenticateSuperAdminUserTest(String userName, String password) {
		HttpSession session = new MockHttpSession();
		String result = "";
		//System.out.println("" + session);
		try {
			result = loginService.authenticateUser(userName, password, session);
			
			if(result=="spadmindashboard"||result=="password"){
				result="1";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		assertEquals(result, "1","login not successful");
	}

	@DataProvider(name = "AuthenticateInvalidUserTestData")
	public Object[][] authenticateInvalidUserTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "AuthenticateInvalidUserTestData");
		return arrayObject;
	}

	@Test(groups = { "smoke", "validation" }, dataProvider = "AuthenticateInvalidUserTestData")
	public void authenticateInvalidUserTest(String userName, String password) {
		HttpSession session = new MockHttpSession();
		String result = "";
		//System.out.println("" + session);
		try {
			result = loginService.authenticateUser(userName, password, session);
		} catch (Exception e) {
			e.printStackTrace();
		}

		assertEquals(result, "","User is Invalid");
	}

	@DataProvider(name = "ValidateCustomerByName")
	public Object[][] getUserCustomerValidDateByUsernameTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "ValidateCustomerByName");
		return arrayObject;
	}

	@Test(groups = { "smoke", "validation" }, dataProvider = "ValidateCustomerByName")
	public void getUserCustomerValidDateByUsernameTest(String username) {

		List<Map<String, Object>> serviceResult = loginService.getUserCustomerValidDateByUsername(username);
		for (Map<String, Object> map : serviceResult) {
			assertEquals(map.get("customer_name"), "DefaultCustomer");
			assertEquals(map.get("customer_type"), "Platnium");
			//assertEquals(map.get("customer_id"), "2");
			//assertEquals(map.get("customer_schema"), TestUtils.SCHEMA_NAME);
			assertEquals(map.get("cust_type_id"), 4);
		}
	
	}
	
	
	
	/*@DataProvider(name = "AuthenticateNormalUserTest")
	public Object[][] normalUserTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "AuthenticateNormalUserTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "validation" }, dataProvider = "AuthenticateNormalUserTest")
	public void authenticateNormalUserTest(String userName, String password) {
		HttpSession session = new MockHttpSession();
		String result = "";
		try {
			result = loginService.authenticateUser(userName, password, session);
			if(result=="xedashboard"||result=="password"){
				result="1";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(result, "1","login successful");
	}*/

}

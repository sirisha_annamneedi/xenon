package com.xenon.api.bugtracker;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import java.io.File;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.xenon.bugtracker.dao.BugDAO;
import com.xenon.test.utils.ReadExcel;
import com.xenon.test.utils.TestUtils;
import com.xenon.util.service.FileUploadForm;

@ContextConfiguration(locations = { "classpath:/WEB-INF/spring/appServlet/servlet-context.xml" })
@WebAppConfiguration
@Test(groups = "bug")
public class BugServiceTest extends AbstractTestNGSpringContextTests {

	@Resource(name = "configurationProperties")
	private Properties configurationProperties;

	@Resource(name = "dbProperties")
	private Properties dbProperties;
	
	@Autowired
	BugService bugService;

	@Autowired
	BugDAO bugDao;

	private ReadExcel excel = new ReadExcel();

	String filePath = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator
			+ "resources" + File.separator + "Bug" + File.separator + "BugServiceTest.xlsx";

	@BeforeClass
	public void executeBefore() {

	}

	@DataProvider(name = "InsertBugTest")
	public Object[][] insertBugTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "InsertBugTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, dataProvider = "InsertBugTest")
	public void insertBugTest(String projectId, String moduleId, String summary, String statusId, String priorityId,
			String severityId, String categoryId, String buildTypeId, String selectedBuildTypeId, String userId,
			String bugDescription, String assignedUserID) {
		FileUploadForm uploadForm = new FileUploadForm();
		int result = bugService.insertBug(projectId, moduleId, summary, statusId, priorityId, severityId, categoryId,
				buildTypeId, selectedBuildTypeId, userId, bugDescription, TestUtils.SCHEMA_NAME, assignedUserID, uploadForm, configurationProperties.getProperty("XENON_REPOSITORY_PATH"));
		assertNotEquals(result, 0);
	}

	@DataProvider(name = "UpdateBugTestData")
	public Object[][] updateBugTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "UpdateBugTestData");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, dataProvider = "UpdateBugTestData")
	public void updateBugTest(String bugId, String statusId, String priorityId, String assignee, String prevStatusId,
			String prevPriorityId, String severityId, String prevSeverityId, String categoryId, String prevCategoryId,
			String prevAssignee, String buildId, String prevBuildId, String prevBuildType, String comments,
			String userId) {
		FileUploadForm uploadForm = new FileUploadForm();
		int result = bugService.updateBug(bugId, statusId, priorityId, assignee, prevStatusId, prevPriorityId,
				severityId, prevSeverityId, categoryId, prevCategoryId, prevAssignee, buildId, prevBuildId,
				prevBuildType, comments, userId, TestUtils.SCHEMA_NAME, uploadForm, configurationProperties.getProperty("XENON_REPOSITORY_PATH"));
		assertNotEquals(result, 0);
	}

	@DataProvider(name = "GetAllBugDetailsTestData")
	public Object[][] getAllBugDetailsTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "GetAllBugDetailsTestData");
		return arrayObject;
	}

	@Test(groups = { "smoke", "validation" }, dataProvider = "GetAllBugDetailsTestData")
	public void getAllBugDetailsTest(String projectId, String userId, String startValue, String offsetValue) {
		Map<String, Object> daoResult = bugDao.getAllBugDetails(Integer.parseInt(projectId), Integer.parseInt(userId),
				Integer.parseInt(startValue), Integer.parseInt(offsetValue), TestUtils.SCHEMA_NAME);
		Map<String, Object> serviceResult = bugService.getAllBugDetails(Integer.parseInt(projectId),
				Integer.parseInt(userId), Integer.parseInt(startValue), Integer.parseInt(offsetValue), TestUtils.SCHEMA_NAME);
		assertEquals(daoResult, serviceResult);
	}

	@DataProvider(name = "GetAssignedToMeBugsTestData")
	public Object[][] getAssignedToMeBugsTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "GetAssignedToMeBugsTestData");
		return arrayObject;
	}

	@Test(groups = { "smoke", "validation" }, dataProvider = "GetAssignedToMeBugsTestData")
	public void getAssignedToMeBugsTest(String projectId, String userID, String startValue, String offsetValue) {
		Map<String, Object> daoResult = bugDao.getAssignedToMeBugs(Integer.parseInt(projectId),
				Integer.parseInt(userID), Integer.parseInt(startValue), Integer.parseInt(offsetValue), TestUtils.SCHEMA_NAME);
		Map<String, Object> serviceResult = bugService.getAssignedToMeBugs(Integer.parseInt(projectId),
				Integer.parseInt(userID), Integer.parseInt(startValue), Integer.parseInt(offsetValue), TestUtils.SCHEMA_NAME);
		assertEquals(daoResult, serviceResult);
	}

	@DataProvider(name = "GetReportedByMeBugsTestData")
	public Object[][] getReportedByMeBugsTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "GetReportedByMeBugsTestData");
		return arrayObject;
	}

	@Test(groups = { "smoke", "validation" }, dataProvider = "GetReportedByMeBugsTestData")
	public void getReportedByMeBugsTest(String projectId, String userID, String startValue, String offsetValue) {
		Map<String, Object> daoResult = bugDao.getReportedByMeBugs(Integer.parseInt(projectId),
				Integer.parseInt(userID), Integer.parseInt(startValue), Integer.parseInt(offsetValue), TestUtils.SCHEMA_NAME);
		Map<String, Object> serviceResult = bugService.getReportedByMeBugs(Integer.parseInt(projectId),
				Integer.parseInt(userID), Integer.parseInt(startValue), Integer.parseInt(offsetValue), TestUtils.SCHEMA_NAME);
		assertEquals(daoResult, serviceResult);
	}
}

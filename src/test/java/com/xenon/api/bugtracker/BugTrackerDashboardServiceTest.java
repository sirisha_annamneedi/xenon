package com.xenon.api.bugtracker;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.Resource;

import org.apache.commons.lang3.ArrayUtils;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.bugtracker.dao.BugDAO;
import com.xenon.bugtracker.dao.BugGroupDAO;
import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.test.utils.ReadExcel;

@ContextConfiguration(locations = { "classpath:/WEB-INF/spring/appServlet/servlet-context.xml" })
@WebAppConfiguration
@Test(groups = "bug")
public class BugTrackerDashboardServiceTest extends AbstractTestNGSpringContextTests {

	@Resource(name = "colorProperties")
	private Properties colorProperties;

	@Autowired
	BugDAO bugDAO;

	@Autowired
	BugGroupDAO bugGroupDAO;

	@Autowired
	ActivitiesDAO activitiesDAO;

	@Autowired
	LoginService loginService;

	@Autowired
	UtilsService utilsService;

	@Autowired
	BugTrackerDashboardService bugTrackerDashboardService;

	private ReadExcel excel = new ReadExcel();

	String filePath = System.getProperty("user.dir")
			+ File.separator +"src"+File.separator+"test"+File.separator+"resources"+File.separator+"Bug"+File.separator+"BugTrackerDashboardServiceTest.xlsx";

	@BeforeClass
	public void executeBefore() {

	}

	@DataProvider(name = "GetDashboardDataTest")
	public Object[][] getDashboardDataTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "GetDashboardDataTest");
		return arrayObject;
	}

	@SuppressWarnings("unchecked")
	@Test(groups = { "smoke", "validation" }, dataProvider = "GetDashboardDataTest")
	public void getDashboardDataTest(String schema, String userID, String projectID, String userTimeZone,
			String fullName, String role, String pageNo, String userprofilePhoto) {

		int userId = Integer.parseInt(userID);
		int projectId = Integer.parseInt(projectID);
		int page = Integer.parseInt(pageNo);
		List<Map<String, Object>> OpenBugList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> getBugtable = new ArrayList<Map<String, Object>>();
		Map<String, Object> bugStat = new HashMap<String, Object>();
		Map<String, Object> modelData = new HashMap<String, Object>();

		int pageSize = 10;
		// model.addAttribute("pageSize",pageSize);
		modelData.put("pageSize", pageSize);
		int startValue = (page - 1) * pageSize;
		Map<String, Object> bugGraph = bugDAO.getBugDashboard(projectId, userId, startValue, pageSize, schema);

		List<Map<String, Object>> userWiseBugs = (List<Map<String, Object>>) bugGraph.get("#result-set-1");

		List<Map<String, Object>> userList = (List<Map<String, Object>>) bugGraph.get("#result-set-2");

		List<Map<String, Object>> bugStatusList = (List<Map<String, Object>>) bugGraph.get("#result-set-3");
		String userColors[] = { colorProperties.getProperty("btDashboard.user0"),
				colorProperties.getProperty("btDashboard.user1"), colorProperties.getProperty("btDashboard.user2"),
				colorProperties.getProperty("btDashboard.user3"), colorProperties.getProperty("btDashboard.user4"),
				colorProperties.getProperty("btDashboard.user5"), colorProperties.getProperty("btDashboard.user6"),
				colorProperties.getProperty("btDashboard.user7"), colorProperties.getProperty("btDashboard.user8"),
				colorProperties.getProperty("btDashboard.user9") };
		for (int i = 0; i < userList.size(); i++) {
			userList.get(i).put("color", userColors[i % 10]);
			userList.get(i).put("fullName",
					userList.get(i).get("first_name").toString() + " " + userList.get(i).get("last_name").toString());
		}
		try {
			modelData.put("userWiseBugs", utilsService.convertListOfMapToJson(userWiseBugs));
			modelData.put("userList", utilsService.convertListOfMapToJson(userList));
			modelData.put("bugStatusList", utilsService.convertListOfMapToJson(bugStatusList));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		List<Map<String, Object>> getBugGraphData = (List<Map<String, Object>>) bugGraph.get("#result-set-5");
		List<Map<String, Object>> dashboardData = (List<Map<String, Object>>) bugGraph.get("#result-set-6");
		;
		List<Map<String, Object>> userAssignedModuleList = (List<Map<String, Object>>) bugGraph.get("#result-set-4");
		int[] moduleArray = new int[userAssignedModuleList.size()];
		for (int count = 0; count < userAssignedModuleList.size(); count++)
			moduleArray[count] = Integer.parseInt(userAssignedModuleList.get(count).get("module_id").toString());
		int priorityCount = 0, statusNewCount = 0, statusClosedCount = 0, assignedToMeCount = 0, assignStatus = 0,
				newBugStatus = 0;
		for (int i = 0; i < dashboardData.size(); i++) {
			int assignedStatus = Integer.parseInt(dashboardData.get(i).get("user_count").toString());
			int project = Integer.parseInt(dashboardData.get(i).get("project").toString());
			int status = Integer.parseInt(dashboardData.get(i).get("bug_status").toString());
			int priority = Integer.parseInt(dashboardData.get(i).get("bug_priority").toString());
			int assignTo = Integer.parseInt(dashboardData.get(i).get("assignee").toString());
			int bugAssignStatus = Integer.parseInt(dashboardData.get(i).get("assign_status").toString());
			int groupAssignStatus = Integer.parseInt(dashboardData.get(i).get("group_assign").toString());

			if (assignedStatus > 0 && projectId == project) {

				if (status == 1) {
					newBugStatus++;
				}
				if (status == 2) {
					assignStatus++;
				}

				if (status == 5) {
					statusClosedCount++;
				} else {
					if (priority == 1) {
						priorityCount++;
					}
					statusNewCount++;
					OpenBugList.add(dashboardData.get(i));
				}

				if (assignTo == userId && !(status == 5 || status == 6) && (bugAssignStatus == 1)
						&& (groupAssignStatus == 1)) {
					assignedToMeCount++;
				}
			}
			if (priority == 1 && status != 5) {
				if (ArrayUtils.contains(moduleArray, Integer.parseInt(dashboardData.get(i).get("module").toString())))
					getBugtable.add(dashboardData.get(i));
			}
		}
		bugStat.put("status_closed", statusClosedCount);
		bugStat.put("status_new", statusNewCount);
		bugStat.put("priority", priorityCount);
		bugStat.put("assigned_to_me", assignedToMeCount);

		modelData.put("status_closed", statusClosedCount);
		modelData.put("statusCount_new", newBugStatus);
		modelData.put("statusCount_assign", assignStatus);

		Set<String> graphWeeks = new TreeSet<String>();
		for (int i = 0; i < getBugGraphData.size(); i++) {
			if (graphWeeks.size() == 6) {
				break;
			}
			graphWeeks.add(getBugGraphData.get(i).get("weekday").toString());
		}

		int newGraphBug = 0, readyGraphBug = 0, closedGraphBug = 0, devGraphBug = 0, assigned = 0, x = 0;
		List<Map<String, Object>> getGraphBugData = new ArrayList<Map<String, Object>>();
		Iterator<String> weekdaySet = graphWeeks.iterator();
		while (weekdaySet.hasNext()) {
			String day = weekdaySet.next();
			for (int i = 0; i < getBugGraphData.size(); i++) {
				if (day.equals(getBugGraphData.get(i).get("weekday").toString())) {
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("New")) {
						newGraphBug++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Ready for QA")) {
						readyGraphBug++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Closed")) {
						closedGraphBug++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Assigned")) {
						assigned++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Dev in Progress")) {
						devGraphBug++;
					}
				}
			}

			Map<String, Object> internalMap = new HashMap<String, Object>();
			internalMap.put("New", newGraphBug);
			internalMap.put("Assign", assigned);
			internalMap.put("ReadyforQA", readyGraphBug);
			internalMap.put("Closed", closedGraphBug);
			internalMap.put("DevinProgress", devGraphBug);
			internalMap.put("weekday", day);
			getGraphBugData.add(x++, internalMap);
			newGraphBug = 0;
			readyGraphBug = 0;
			closedGraphBug = 0;
			devGraphBug = 0;
			assigned = 0;
		}

		JSONArray jArray = new JSONArray(getGraphBugData);
		modelData.put("graphData", jArray);

		for (int j = 0; j < OpenBugList.size(); j++) {
			OpenBugList.get(j).put("create_date", utilsService.getDateTimeOfTimezone(userTimeZone,
					utilsService.getDate(OpenBugList.get(j).get("create_date").toString())));

			String logo = loginService.getImage((byte[]) OpenBugList.get(j).get("user_photo"));
			OpenBugList.get(j).put("user_photo", logo);

			if (OpenBugList.get(j).get("assign_status").toString().equals("2")) {
				OpenBugList.get(j).put("assigneeFN", "None");
				OpenBugList.get(j).put("assigneeLN", "");
			} else if (OpenBugList.get(j).get("group_status").toString().equals("1")) {
				int bugGroupId = Integer.parseInt(OpenBugList.get(j).get("group_id").toString());
				List<Map<String, Object>> bugGroup = bugGroupDAO.getGroupbyId(bugGroupId, schema);

				OpenBugList.get(j).put("assigneeFN", bugGroup.get(0).get("bug_group_name"));
				OpenBugList.get(j).put("assigneeLN", "");
			}
		}

		modelData.put("allBugDetails", OpenBugList);
		modelData.put("bugCount", bugStat);
		modelData.put("bugTable", getBugtable);
		modelData.put("UserProfilePhoto", userprofilePhoto);
		modelData.put("userName", fullName);
		modelData.put("role", role);
		List<Map<String, Object>> topActivities = null;
		/*try {
			topActivities = activitiesDAO.getTopActivities(schema);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}*/
		for (int i = 0; i < topActivities.size(); i++) {
			topActivities.get(i).put("activity_date", utilsService.getDateTimeOfTimezone(userTimeZone,
					topActivities.get(i).get("activity_date").toString()));
		}

		modelData.put("nowDate", utilsService.getNowDateOfTimezone(userTimeZone));
		modelData.put("prevDate", utilsService.getPrevDateOfTimezone(userTimeZone));
		modelData.put("topActivities", topActivities);
		modelData.put("New", colorProperties.getProperty("bug.New"));
		modelData.put("Fixed", colorProperties.getProperty("bug.Fixed"));
		modelData.put("Closed", colorProperties.getProperty("bug.Closed"));
		modelData.put("Assigned", colorProperties.getProperty("bug.Assigned"));
		modelData.put("Verified", colorProperties.getProperty("bug.Verified"));
		modelData.put("Reject", colorProperties.getProperty("bug.Reject"));

		Map<String, Object> serviceModelData = bugTrackerDashboardService.getDashboardData(schema, userId, projectId,
				userTimeZone, userprofilePhoto, fullName, role, page);

		modelData.remove("graphData");
		serviceModelData.remove("graphData");

		assertEquals(serviceModelData, modelData);
	}

	@DataProvider(name = "ViewAllBTDAshboardTest")
	public Object[][] viewAllBTDAshboardTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "ViewAllBTDAshboardTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "validation" }, dataProvider = "ViewAllBTDAshboardTest")
	public void viewAllBTDAshboardTest(String userID, String schema, String userTimeZone, String userFullName,
			String role) {

		byte[] userProfilePhoto = new byte[] { 1, 2, 3 };
		int userId = Integer.parseInt(userID);

		List<Map<String, Object>> allOpenBugList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> graphData = new ArrayList<Map<String, Object>>();
		HashMap<String, Integer> bugStat = new HashMap<String, Integer>();
		Map<String, Object> modelData = new HashMap<String, Object>();
		int compareProject = 0;
		List<Map<String, Object>> dashboardData = bugDAO.getDashboardData(userId, schema);
		int priorityCount = 0, statusNewCount = 0, statusClosedCount = 0, assignedToMeCount = 0;
		int graphNew = 0, graphClosed = 0, graphDevInProgress = 0;

		for (int i = 0; i < dashboardData.size(); i++) {
			dashboardData.get(i).put("create_date", utilsService.getDateTimeOfTimezone(userTimeZone,
					utilsService.getDate(dashboardData.get(i).get("create_date").toString())));
			dashboardData.get(i).put("update_date", utilsService.getDateTimeOfTimezone(userTimeZone,
					utilsService.getDate(dashboardData.get(i).get("update_date").toString())));
			int assignedStatus = Integer.parseInt(dashboardData.get(i).get("user_count").toString());
			int status = Integer.parseInt(dashboardData.get(i).get("bug_status").toString());
			int priority = Integer.parseInt(dashboardData.get(i).get("bug_priority").toString());
			int assignTo = Integer.parseInt(dashboardData.get(i).get("assignee").toString());
			int project = Integer.parseInt(dashboardData.get(i).get("project").toString());
			int bugAssignStatus = Integer.parseInt(dashboardData.get(i).get("assign_status").toString());
			int groupAssignStatus = Integer.parseInt(dashboardData.get(i).get("group_assign").toString());
			if (compareProject != project) {
				if (i != 0) {
					String projectName = dashboardData.get(i - 1).get("project_name").toString();
					Map<String, Object> data = new HashMap<String, Object>();
					data.put("new_bug", graphNew);
					data.put("closed_bug", graphClosed);
					data.put("dev_in_progress", graphDevInProgress);
					data.put("project_name", projectName);
					data.put("project_id", project);
					graphData.add(data);
				}
				graphClosed = 0;
				graphDevInProgress = 0;
				graphNew = 0;
				if (status == 5) {
					graphClosed = 1;
				}
				if (status == 3) {
					graphDevInProgress = 1;
				}
				if (status == 1) {
					graphNew = 1;
				}
				compareProject = project;
			} else {
				if (status == 5) {
					graphClosed++;
				}
				if (status == 3) {
					graphDevInProgress++;
				}
				if (status == 1) {
					graphNew++;
				}
				if (i == dashboardData.size() - 1) {
					String projectName = dashboardData.get(i).get("project_name").toString();
					Map<String, Object> data = new HashMap<String, Object>();
					data.put("new_bug", graphNew);
					data.put("closed_bug", graphClosed);
					data.put("dev_in_progress", graphDevInProgress);
					data.put("project_name", projectName);
					data.put("project_id", project);
					graphData.add(data);

				}
			}
			if (assignedStatus > 0) {
				if (status == 5) {
					statusClosedCount++;
				} else {
					if (priority == 1) {
						priorityCount++;
					}
					statusNewCount++;
					allOpenBugList.add(dashboardData.get(i));
				}

				if (assignTo == userId && !(status == 5 || status == 6) && (bugAssignStatus == 1)
						&& (groupAssignStatus == 1)) {
					assignedToMeCount++;
				}
			}

		}

		List<Map<String, Object>> getBugGraphData = bugDAO.getAllBugGraphDashboardData(userId, schema);
		Set<String> graphWeeks = new TreeSet<String>();
		for (int i = 0; i < getBugGraphData.size(); i++) {
			if (graphWeeks.size() == 6) {
				break;
			}
			graphWeeks.add(getBugGraphData.get(i).get("weekday").toString());
		}

		int newGraphBug = 0, readyGraphBug = 0, closedGraphBug = 0, devGraphBug = 0, assigned = 0, x = 0;
		List<Map<String, Object>> getGraphBugData = new ArrayList<Map<String, Object>>();
		Iterator<String> weekdaySet = graphWeeks.iterator();
		while (weekdaySet.hasNext()) {
			String day = weekdaySet.next();
			for (int i = 0; i < getBugGraphData.size(); i++) {
				if (day.equals(getBugGraphData.get(i).get("weekday").toString())) {
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("New")) {
						newGraphBug++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Ready for QA")) {
						readyGraphBug++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Closed")) {
						closedGraphBug++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Assigned")) {
						assigned++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Dev in Progress")) {
						devGraphBug++;
					}
				}
			}

			Map<String, Object> internalMap = new HashMap<String, Object>();
			internalMap.put("New", newGraphBug);
			internalMap.put("Assign", assigned);
			internalMap.put("ReadyforQA", readyGraphBug);
			internalMap.put("Closed", closedGraphBug);
			internalMap.put("DevinProgress", devGraphBug);
			internalMap.put("weekday", day);
			getGraphBugData.add(x++, internalMap);
			newGraphBug = 0;
			readyGraphBug = 0;
			closedGraphBug = 0;
			devGraphBug = 0;
			assigned = 0;
		}

		JSONArray jArray = new JSONArray(getGraphBugData);
		modelData.put("graphDataProject", jArray);

		List<Map<String, Object>> topActivities = null;
		/*try {
			topActivities = activitiesDAO.getTopActivities(schema);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}*/
		for (int i = 0; i < topActivities.size(); i++) {
			String dateInString = topActivities.get(i).get("activity_date").toString();
			topActivities.get(i).put("activity_date", utilsService.getDateTimeOfTimezone(userTimeZone, dateInString));
		}

		for (int j = 0; j < allOpenBugList.size(); j++) {
			if (allOpenBugList.get(j).get("assign_status").toString().equals("2")) {
				allOpenBugList.get(j).put("assigneeFN", "None");
				allOpenBugList.get(j).put("assigneeLN", "");
			} else if (allOpenBugList.get(j).get("group_status").toString().equals("1")) {
				int bugGroupId = Integer.parseInt(allOpenBugList.get(j).get("group_id").toString());
				List<Map<String, Object>> bugGroup = bugGroupDAO.getGroupbyId(bugGroupId, schema);

				allOpenBugList.get(j).put("assigneeFN", bugGroup.get(0).get("bug_group_name"));
				allOpenBugList.get(j).put("assigneeLN", "");
			}
		}

		bugStat.put("status_closed", statusClosedCount);
		bugStat.put("status_new", statusNewCount);
		bugStat.put("priority", priorityCount);
		bugStat.put("assigned_to_me", assignedToMeCount);

		try {
			modelData.put("graphData", utilsService.convertListOfMapToJson(graphData));
		} catch (Exception e) {
			e.printStackTrace();
		}
		modelData.put("allBugDetails", allOpenBugList);
		modelData.put("bugCount", bugStat);
		modelData.put("UserProfilePhoto", loginService.getImage((byte[]) userProfilePhoto));
		modelData.put("userName", userFullName);
		modelData.put("role", role);
		modelData.put("nowDate", utilsService.getNowDateOfTimezone(userTimeZone));
		modelData.put("prevDate", utilsService.getPrevDateOfTimezone(userTimeZone));
		modelData.put("topActivities", topActivities);
		modelData.put("New", colorProperties.getProperty("bug.New"));
		modelData.put("Fixed", colorProperties.getProperty("bug.Fixed"));
		modelData.put("Closed", colorProperties.getProperty("bug.Closed"));
		modelData.put("Assigned", colorProperties.getProperty("bug.Assigned"));
		modelData.put("Verified", colorProperties.getProperty("bug.Verified"));
		modelData.put("Reject", colorProperties.getProperty("bug.Reject"));

		Map<String, Object> serviceModelData = bugTrackerDashboardService.viewAllBTDAshboard(userId, schema,
				userTimeZone, userProfilePhoto, userFullName, role);

		modelData.remove("graphDataProject");
		serviceModelData.remove("graphDataProject");

		assertEquals(serviceModelData, modelData);
	}
}

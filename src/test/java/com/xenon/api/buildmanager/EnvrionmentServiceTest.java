package com.xenon.api.buildmanager;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.xenon.test.utils.ReadExcel;
import com.xenon.test.utils.TestUtils;

@ContextConfiguration(locations = {"classpath:/WEB-INF/spring/appServlet/servlet-context.xml"})
@WebAppConfiguration
@Test
public class EnvrionmentServiceTest extends AbstractTestNGSpringContextTests {
	
	Properties properties;
	
	@Autowired
	EnvironmentService environmentService;
	private ReadExcel excel = new ReadExcel();

	String filePath = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator
			+ "resources" + File.separator + "Buildmanager" + File.separator + "Environment.xlsx";

	@BeforeClass
	public void create() throws IOException {
		
	}
	
	@DataProvider(name = "InsertEnvironment")
	public Object[][] InsertEnvironmentTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "InsertEnvironment");
		return arrayObject;
	}
	@Test(groups={"smoke","verification"}, dataProvider = "InsertEnvironment")
	public void insertEnvironment(String envName,String envDescription,String configParameterName,String envStatus)
	{
		int result=environmentService.insertEnvDetails(envName, envDescription, configParameterName,Integer.parseInt(envStatus), TestUtils.SCHEMA_NAME);
			Assert.assertEquals(result, 201, "Environment not created");
		
		
	}
	@DataProvider(name = "UpdateEnvironment")
	public Object[][] updateEnvironmentTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "UpdateEnvironment");
		return arrayObject;
	}
	@Test(groups={"smoke","verification"}, dataProvider = "UpdateEnvironment",dependsOnMethods={"insertEnvironment"})
	public void updateEnvironment(String envId, String envName, String configParameterName, String envDescription,String envStatus)
	{
		int result=environmentService.updateEnvDetails(envId, envName, configParameterName, envDescription,Integer.parseInt(envStatus), TestUtils.SCHEMA_NAME);
			Assert.assertEquals(result, 201, "Environment not Updated");
		
		
	}
	@DataProvider(name = "DeleteEnvironment")
	public Object[][] deleteEnvironmentTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "DeleteEnvironment");
		return arrayObject;
	}
	@Test(groups={"smoke","verification"}, dataProvider = "DeleteEnvironment",dependsOnMethods={"insertEnvironment","updateEnvironment"})
	public void deleteEnvironment(String envId)
	{
		int result=environmentService.deleteEnvironment(envId,TestUtils.SCHEMA_NAME);
			Assert.assertEquals(result, 201, "Environment not Deleted");
		
		
	}
}

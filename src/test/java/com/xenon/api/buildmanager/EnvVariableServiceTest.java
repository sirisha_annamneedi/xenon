package com.xenon.api.buildmanager;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.xenon.test.utils.ReadExcel;
import com.xenon.test.utils.TestUtils;
@ContextConfiguration(locations = { "classpath:/WEB-INF/spring/appServlet/servlet-context.xml" })
@WebAppConfiguration
@Test
public class EnvVariableServiceTest extends AbstractTestNGSpringContextTests{
Properties properties;
	
@Autowired
EnvVariableService envVariableService;

	private ReadExcel excel = new ReadExcel();

	String filePath = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator
			+ "resources" + File.separator + "Buildmanager" + File.separator + "EnvVariable.xlsx";

	@BeforeClass
	public void create() throws IOException {
		
	}
	
	@DataProvider(name = "InsertEnvVariable")
	public Object[][] InsertEnvVariableTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "InsertEnvVariable");
		return arrayObject;
	}
	@Test(groups={"smoke","verification"}, dataProvider = "InsertEnvVariable")
	public void insertEnvVariable(String variableName,String configValue,String variableValue,String envId)
	{
		int result=envVariableService.insertEnvVariableDetails(variableName, configValue, variableValue,envId,TestUtils.SCHEMA_NAME);
			Assert.assertEquals(result, 201, "Environment variable not created");
		
		
	}
	@DataProvider(name = "UpdateEnvVariable")
	public Object[][] updateEnvVariableTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "UpdateEnvVariable");
		return arrayObject;
	}
	@Test(groups={"smoke","verification"}, dataProvider = "UpdateEnvVariable",dependsOnMethods={"insertEnvVariable"})
	public void updateEnvVariable(String updateVariableName, String updateConfigValue, String updateVariableValue,String updateVariableId)
	{
		int result=envVariableService.updateVariableDetails(updateVariableName, updateConfigValue, updateVariableValue, updateVariableId, TestUtils.SCHEMA_NAME);
			Assert.assertEquals(result, 201, "Environment variable not Updated");
		
		
	}
	@DataProvider(name = "DeleteEnvVariable")
	public Object[][] deleteEnvVaribleTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "DeleteEnvVariable");
		return arrayObject;
	}
	@Test(groups={"smoke","verification"}, dataProvider = "DeleteEnvVariable",dependsOnMethods={"insertEnvVariable","updateEnvVariable"})
	public void deleteEnvVariable(String variableId)
	{
		int result=envVariableService.deleteVariable(TestUtils.SCHEMA_NAME,variableId);
			Assert.assertEquals(result, 201, "Environment variable not Deleted");
	}
}

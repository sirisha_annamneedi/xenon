package com.xenon.api.buildmanager;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.xenon.api.buildmanager.ModuleService;
import com.xenon.test.utils.ReadExcel;
import com.xenon.test.utils.TestUtils;


@ContextConfiguration(locations = {"classpath:/WEB-INF/spring/appServlet/servlet-context.xml"})
@WebAppConfiguration
@Test
public class ModuleServiceTest extends AbstractTestNGSpringContextTests {
		
		Properties properties;
		
		@Autowired
		ModuleService moduleService;
		private ReadExcel excel = new ReadExcel();

		String filePath = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator
				+ "resources" + File.separator + "Buildmanager" + File.separator + "Module.xlsx";

		@BeforeClass
		public void create() throws IOException {
			
		}
		
		@DataProvider(name = "InsertModule")
		public Object[][] InsertModuleTestData() throws Exception {
			Object[][] arrayObject = excel.getExcelData(filePath, "InsertModule");
			return arrayObject;
		}
		@Test(groups={"smoke","verification"}, dataProvider = "InsertModule")
		public void createModule(String eMail,String schema,String projectName,String moduleName,String moduleStatus,String moduleDescrption)
		{
			int result=moduleService.createModule(eMail,schema,projectName,moduleName,moduleStatus,moduleDescrption);
			if(result!=200)
			{
				int returnResult=moduleService.createModule(eMail,schema,projectName,moduleName+new SimpleDateFormat("MMddyyyyHHmmss").format(new Date()),moduleStatus,moduleDescrption);
				Assert.assertEquals(returnResult, 200, "Module not created");
			}
			
		}
		
		@DataProvider(name = "UpdateModule")
		public Object[][] updateModuleTestData() throws Exception {
			Object[][] arrayObject = excel.getExcelData(filePath, "UpdateModule");
			return arrayObject;
		}
		@Test(groups={"smoke","verification"}, dataProvider = "UpdateModule",dependsOnMethods={"createModule"})
		public void updateModule(String newModuleName,String currentModuleName,String schema,String emailId,String moduleDescription,String moduleStatus)
		{
			int result=moduleService.updateModuleByName(newModuleName,currentModuleName,schema,emailId,moduleDescription,moduleStatus);
			Assert.assertEquals(result, 200, "Error: Error occured while updating module ");
		}
		
		@DataProvider(name = "AssignModule")
		public Object[][] assignModuleTestData() throws Exception {
			Object[][] arrayObject = excel.getExcelData(filePath, "AssignModule");
			return arrayObject;
		}
		@Test(groups={"smoke","verification"},dataProvider = "AssignModule",dependsOnMethods={"updateModule"})
		public void assignModuleToUsers(String moduleName,String projectName,String assignEmailId,String schema)
		{
			int result=moduleService.assignModuleToUsers(moduleName,projectName,assignEmailId,schema);
			Assert.assertEquals(result, 200, "Error: Error occured while assigning module ");
		}
		
		@DataProvider(name = "UnAssignModule")
		public Object[][] unassignModuleTestData() throws Exception {
			Object[][] arrayObject = excel.getExcelData(filePath, "UnAssignModule");
			return arrayObject;
		}
		@Test(groups={"smoke","verification"},dataProvider = "UnAssignModule",dependsOnMethods={"assignModuleToUsers"})
		public void unAssignModuleToUsers(String unAssignEmail,String moduleName,String schema){
			int result=moduleService.unAssignModuleToUsers(unAssignEmail,moduleName,schema);
			Assert.assertEquals(result, 200, "Error: Error occured while unassigning module ");
			
		}
		
		/*@DataProvider(name = "CheckDuplicateModule")
		public Object[][] checkDuplicateModuleTestData() throws Exception {
			Object[][] arrayObject = excel.getExcelData(filePath, "CheckDuplicateModule");
			return arrayObject;
		}
		@Test(groups={"smoke","verification"},dataProvider = "CheckDuplicateModule",dependsOnMethods={"updateModule"})
		public void checkDuplicateModule(String emailId,String schema,String projectName,String moduleName,String moduleStatus,String moduleDesription){
			int result=moduleService.createModule(emailId,schema,projectName,moduleName,moduleStatus,moduleDesription);
			Assert.assertEquals(result, 208, "Name of Module is not duplicated");
		}*/
		
}

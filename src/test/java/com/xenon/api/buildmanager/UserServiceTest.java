package com.xenon.api.buildmanager;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;



import javax.annotation.Resource;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.api.core.CustomerService;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.common.domain.OauthClient;
import com.xenon.test.utils.ReadExcel;
import com.xenon.test.utils.TestUtils;
import com.xenon.util.service.OauthTokenGenerator;
import com.xenon.common.dao.OauthUserDAO;
import com.xenon.common.domain.OauthUser;
import static org.testng.Assert.assertNotEquals;

@ContextConfiguration(locations = { "classpath:/WEB-INF/spring/appServlet/servlet-context.xml" })
@WebAppConfiguration
@Test(groups = { "UserServiceTest.user" })
public class UserServiceTest extends AbstractTestNGSpringContextTests {

	@Resource(name = "configurationProperties")
	private Properties configurationProperties;

	@Resource(name = "dbProperties")
	private Properties dbProperties;

	@Autowired
	UserService userService;

	@Autowired
	private UserDAO userDao;

	@Autowired
	LoginService loginService;
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	OauthUserDAO oauthUserDAO;
	
	@Autowired
	UtilsService utilsService;


	private ReadExcel excel = new ReadExcel();

	String filePath = System.getProperty("user.dir") + File.separator + "src"
			 + File.separator + "test" + File.separator+ "resources" + File.separator + "User" + File.separator + "UserServiceTest.xlsx";
	@BeforeClass
	public void executeBefore() {
	}

	/*@DataProvider(name = "AssignModuleToUserTest")
	public Object[][] assignModuleToUserData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "AssignModuleToUserTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, dataProvider = "AssignModuleToUserTest")
	public void assignModuleToUserTest(String userId, String projectID, String selectedModules, String schema) {
		boolean result = false;
		try {
			result = userService.assignModuleToUser(Integer.parseInt(userId), Integer.parseInt(projectID),
					selectedModules, schema);
			assertEquals(result, true, "Module ");
		} catch (NumberFormatException e) {
			assertEquals(result, true, "" + e);
		}
	}*/

	@DataProvider(name = "InsertUserTest")
	public Object[][] insertUserTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "InsertUserTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, dataProvider = "InsertUserTest" ,enabled=true)
	public void insertUserTest(String emailId, String firstName, String lastName, String passWord, String userRole,
			String tmStatus, String btStatus, String userStatus, String schema, String customerName, String projects,
			String createDateTime, String repoPath,String userName,String jenkinStatus,String gitStatus) {
		int index=emailId.indexOf('@');
		
		String emailFlag=userService.checkUser(emailId);
		boolean usernameFlag=userService.checkUserName(userName);
		
		if(emailFlag.equals("true")&&usernameFlag==true){
			
			int result = userService.insertUser(emailId, firstName, lastName, passWord, userRole, tmStatus,
				btStatus, userStatus,schema, customerName, projects, utilsService.getCurrentDateTime(), repoPath,userName,Integer.parseInt(jenkinStatus),Integer.parseInt(gitStatus),0,0);
			assertEquals(result, 201,"user not created");
		}
		else{
			
			String userEmail=emailId.substring(0, index)+new SimpleDateFormat("MMddyyyyHHmmss").format(new Date())+emailId.substring(index,emailId.length());
			

			int result = userService.insertUser(userEmail, firstName, lastName, passWord, userRole, tmStatus,
				btStatus, userStatus, schema, customerName, projects, utilsService.getCurrentDateTime(), repoPath,userName+new SimpleDateFormat("MMddyyyyHHmmss").format(new Date()),Integer.parseInt(jenkinStatus),Integer.parseInt(gitStatus),0,0);
			assertEquals(result, 201,"user not created");
		}
	}

	@DataProvider(name = "UpdateUserTest")
	public Object[][] updateUserTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "UpdateUserTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, dataProvider = "UpdateUserTest",dependsOnMethods={"insertUserTest"})
	public void updateUserTest(String userId, String firstName, String lastName, String userRole, String tmStatus,
			String btStatus, String userStatus, String schema,String jenkinStatus,String gitStatus) {
		boolean result = userService.updateUser(Integer.parseInt(userId), firstName, lastName, userRole, tmStatus,
				btStatus, userStatus,schema,utilsService.getCurrentDateTime(),Integer.parseInt(jenkinStatus),Integer.parseInt(gitStatus),0,0);
		assertEquals(result, true);

	}

	@DataProvider(name = "RemoveUserTest")
	public Object[][] removeUserTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "RemoveUserTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, dataProvider = "RemoveUserTest",dependsOnMethods={"insertUserTest"})
	public void removeUserTest(String userid, String schema) {
		String result = userService.removeUser(Integer.parseInt(userid), schema);
		assertEquals(result, "200", "problem in User deletion");
	}

	@DataProvider(name = "CheckUserTest")
	public Object[][] checkUserTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "CheckUserTest");
		return arrayObject;
	}

	// This is validation Testcase
	@Test(groups = { "smoke", "validation" }, dataProvider = "CheckUserTest",dependsOnMethods={"insertUserTest"})
	public void checkUserTest(String emailId) {
		String result = userService.checkUser(emailId);
		assertEquals(result, "false", "User already exist !!!");
	}

	@DataProvider(name = "CreateUserTest")
	public Object[][] createUserTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "CreateUserTest");
		return arrayObject;
	}

	/*@SuppressWarnings("unchecked")
	@Test(groups = { "smoke", "validation" }, dataProvider = "CreateUserTest")
	public void createUserTest(String schema, String custTypeId, String userId, String btStatus, String tmStatus) {

		Map<String, Object> modelData = new HashMap<String, Object>();
		int flag = 0;
		try {
			flag = userDao.createNewUserStatus(schema, Integer.parseInt(custTypeId));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		modelData.put("flag", flag);
		Map<String, Object> createUserData = userDao.getCreateUserData(Integer.parseInt(userId), schema);
		modelData.put("userRoles", (List<Map<String, Object>>) createUserData.get("#result-set-2"));
		modelData.put("bugTStatus", Integer.parseInt(btStatus));
		modelData.put("testMStatus", Integer.parseInt(tmStatus));
		modelData.put("projects", (List<Map<String, Object>>) createUserData.get("#result-set-1"));

		List<Map<String, Object>> emailSetting = (List<Map<String, Object>>) createUserData.get("#result-set-3");
		String mailFlag;

		if (emailSetting.size() == 0) {
			mailFlag = "disable";
		} else {
			if (emailSetting.get(0).get("is_enabled").toString().equals("1")) {
				mailFlag = "enable";
			} else {
				mailFlag = "disable";
			}
		}
		modelData.put("emailSetting", mailFlag);

		Map<String, Object> serviceModelData = userService.createUser(schema, Integer.parseInt(custTypeId),
				Integer.parseInt(userId), btStatus, tmStatus);
		//System.out.println(serviceModelData);
		//System.out.println(modelData);
		assertEquals(serviceModelData, modelData);

	}*/

	@DataProvider(name = "ViewUserTest")
	public Object[][] viewUserTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "ViewUserTest");
		return arrayObject;
	}

	@SuppressWarnings("unchecked")
	@Test(groups = { "smoke", "validation" }, dataProvider = "ViewUserTest")
	public void viewUserTest(String page, String createStatus, String schema) {

		Map<String, Object> modelData = new HashMap<String, Object>();

		if (Boolean.parseBoolean(createStatus)) {
			modelData.put("createUserAccessStatus", 1);
		}

		int pageSize = 10;
		modelData.put("pageSize", pageSize);

		int startValue = (Integer.parseInt(page) - 1) * pageSize;

		Map<String, Object> data = userDao.getViewUserData(startValue, pageSize, schema);

		int allUserCount = (Integer) data.get("count");
		////System.out.println("All user count=" + allUserCount);

		List<Map<String, Object>> listUser = (List<Map<String, Object>>) data.get("#result-set-1");
		for (int i = 0; i < listUser.size(); i++) {
			String logo = null;
			logo = loginService.getImage((byte[]) listUser.get(i).get("user_photo"));
			listUser.get(i).put("user_photo", logo);
		}
		
		boolean isMailEnabled = userDao.isMailSettingEnabled(schema);
		
		modelData.put("emailSettingEnabled", isMailEnabled);
		modelData.put("allUserCount", allUserCount);
		modelData.put("allUserDetails", listUser);
		Map<String, Object> serviceModelData = userService.viewUser(Integer.parseInt(page),
				Boolean.parseBoolean(createStatus), schema);
		
		assertEquals(serviceModelData, modelData);
	}

	/*@DataProvider(name = "UserSettingTest")
	public Object[][] userSettingTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "UserSettingTest");
		return arrayObject;
	}

	@SuppressWarnings("unchecked")
	@Test(groups = { "smoke", "validation" }, dataProvider = "UserSettingTest")
	public void userSettingTest(String schema, String userId, String role, String btStatus, String tmStatus,
			String actionForTab, String userFullName,int jenkinStatus,int gitStatus) {
		Map<String, Object> editUserData = userDao.getEditUserData(Integer.parseInt(userId), schema);
		Map<String, Object> modelData = new HashMap<String, Object>();

		List<Map<String, Object>> userDetails = (List<Map<String, Object>>) editUserData.get("#result-set-1");
		List<Map<String, Object>> userRoles = (List<Map<String, Object>>) editUserData.get("#result-set-2");
		modelData.put("userDetails", userDetails);
		modelData.put("userRoles", userRoles);
		modelData.put("recentFlag", 0);
		modelData.put("role", role);
		modelData.put("bugTStatus", Integer.parseInt(btStatus));
		modelData.put("testMStatus", Integer.parseInt(tmStatus));

		Map<String, Object> projects = userDao.getUserAssignedProjects(Integer.parseInt(userId), schema);

		List<Map<String, Object>> assignProject = (List<Map<String, Object>>) projects.get("#result-set-1");
		List<Map<String, Object>> unassignProject = (List<Map<String, Object>>) projects.get("#result-set-2");
		List<Map<String, Object>> assignModule = (List<Map<String, Object>>) projects.get("#result-set-3");
		List<Map<String, Object>> unassignModule = (List<Map<String, Object>>) projects.get("#result-set-4");

		modelData.put("assignProject", assignProject);
		modelData.put("unassignProject", unassignProject);
		modelData.put("assignModule", assignModule);
		modelData.put("unassignModule", unassignModule);

		JSONArray jarray = new JSONArray(unassignModule);

		modelData.put("unassignModuleJson", jarray);
		modelData.put("actionForTab", actionForTab);
		modelData.put("userFullName", userFullName);
		modelData.put("userID", Integer.parseInt(userId));

		Map<String, Object> serviceModelData = userService.userSetting(schema, Integer.parseInt(userId), role,
				Integer.parseInt(btStatus), Integer.parseInt(tmStatus), actionForTab, userFullName,jenkinStatus,gitStatus);

		assertEquals(serviceModelData.get("userRoles"), modelData.get("userRoles"));
		assertEquals(serviceModelData.get("recentFlag"), modelData.get("recentFlag"));
		assertEquals(serviceModelData.get("role"), modelData.get("role"));
		assertEquals(serviceModelData.get("bugTStatus"), modelData.get("bugTStatus"));
		assertEquals(serviceModelData.get("testMStatus"), modelData.get("testMStatus"));
		assertEquals(serviceModelData.get("assignProject"), modelData.get("assignProject"));
		assertEquals(serviceModelData.get("unassignProject"), modelData.get("unassignProject"));
		assertEquals(serviceModelData.get("assignModule"), modelData.get("assignModule"));
		assertEquals(serviceModelData.get("unassignModule"), modelData.get("unassignModule"));
		assertEquals(serviceModelData.get("actionForTab"), modelData.get("actionForTab"));
		assertEquals(serviceModelData.get("userFullName"), modelData.get("userFullName"));
		assertEquals(serviceModelData.get("userID"), modelData.get("userID"));

	}*/

	@Test(groups = { "user", "smoke", "verification", "automation" })
	public void insertAdminUserTest() {

		TestUtils.SCHEMA_NAME = customerService.createCustomerSchema("DefaultCustomer",
	
				configurationProperties.getProperty("XENON_REPOSITORY_PATH"), dbProperties.getProperty("jdbc.driver"),
				dbProperties.getProperty("jdbc.url"), dbProperties.getProperty("jdbc.username"),
				dbProperties.getProperty("jdbc.password"));
		int flag = customerService.insertCustomer("DefaultCustomer", "Jadeglobal", 4, 1, 1, 1, 1, 1, 1,
				TestUtils.SCHEMA_NAME, configurationProperties.getProperty("XENON_REPOSITORY_PATH"),1,1,0,0,"#");
		assertNotEquals(flag, 0);
		OauthClient oauthClient = new OauthClient();
		oauthClient.setClientId("DefaultCustomer");
		oauthUserDAO.insertOauthClient(oauthClient);
		int result = userService.insertAdminUser("default.customer@jadeglobal.com", "Default", "Customer", "2", "1",
				"1", "1", TestUtils.SCHEMA_NAME, configurationProperties.getProperty("XENON_REPOSITORY_PATH"),"default.customer@jadeglobal.com",1,1);
		assertNotEquals(result, 0);
		String passToken = TestUtils.SCHEMA_NAME + "Default" + "_" + "Customer";
		OauthUser oauthUser = new OauthUser();
		oauthUser.setUserName("default.customer@jadeglobal.com");
		oauthUser.setPassword(OauthTokenGenerator.getPasstoken(passToken));
		oauthUserDAO.insertOauthUser(oauthUser);

	}
}

package com.xenon.api.administration;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.test.utils.ReadExcel;


@ContextConfiguration(locations = { "classpath:/WEB-INF/spring/appServlet/servlet-context.xml" })
@WebAppConfiguration
@Test(groups = "project")
public class ProjectServiceTest extends AbstractTestNGSpringContextTests {

	@Autowired
	UtilsService utils;

	@Autowired
	ProjectService projectService;

	@Autowired
	ProjectDAO projectDao;

	private ReadExcel excel = new ReadExcel();

	String filePath = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator
			+ "resources" + File.separator + "Project" + File.separator + "ProjectServiceTest.xlsx";

	@BeforeClass
	public void create() throws IOException {
	}

	@DataProvider(name = "CreateProjectTest")
	public Object[][] createProjectTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "CreateProjectTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, dataProvider = "CreateProjectTest")
	public void createProjectTest(String projectName, String jiraProjectName, String projectDescription, String bugPrefix, String tcPrefix,
			String proStatus,String schema) {
		
		Map<String, String> returnDetails =projectService.insertProject(projectName, jiraProjectName, projectDescription, bugPrefix,tcPrefix, proStatus, schema, utils.getCurrentDateTime());
		int projectStatus = Integer.parseInt(returnDetails.get("responseCode"));
		
		
		
		if(projectStatus==208){
		Map<String, String> reponseData = projectService.insertProject(projectName+ utils.getCurrentDateTime(), jiraProjectName, projectDescription, bugPrefix+ utils.getCurrentDateTime(),
				tcPrefix+ utils.getCurrentDateTime(), proStatus, schema, utils.getCurrentDateTime());
				int result = Integer.parseInt(reponseData.get("responseCode"));
				Assert.assertEquals(result, 201, "Project not created");
		}
		
		
	}

	@DataProvider(name = "UpdateProjectTest")
	public Object[][] updateProjectTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "UpdateProjectTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, dataProvider = "UpdateProjectTest",dependsOnMethods={"createProjectTest"})
	public void updateProjectTest(String projectName, String Projectdescription, String projectStatus,String schema) {
		int projectId = projectDao.getProjectID(projectName, schema);
		int result = projectService.updateProject(projectId, Projectdescription, Integer.parseInt(projectStatus),schema);
		Assert.assertEquals(result, 1, "Error: Error occured while updating application");
	}

	@DataProvider(name = "AssignProjectToUsersTest")
	public Object[][] assignProjectToUsersTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "AssignProjectToUsersTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, dataProvider = "AssignProjectToUsersTest",dependsOnMethods={"createProjectTest"})
	public void assignProjectToUsersTest(String projectID,String projectName, String assignUserEmail, String loginUserEmailId,String schema)
			throws Exception {
		
		projectService.assignProjectToUsers(Integer.parseInt(projectID),projectName,assignUserEmail,Integer.parseInt(loginUserEmailId),schema);
	}

	/*@DataProvider(name = "UnAssignProjectToUsersTest")
	public Object[][] unAssignProjectToUsersTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "UnAssignProjectToUsersTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, dataProvider = "UnAssignProjectToUsersTest",dependsOnMethods={"createProjectTest","assignProjectToUsersTest"})
	public void unassignProjectToUsers(String projectName, String emailId, String loginUserEmailId,String schema) {
	int flag = projectService.unAssignProjectToUsers(projectName, emailId, schema,
			loginUserEmailId);
		Assert.assertEquals(flag, 200, "Error: Error occured while unassigning project");
	}*/

	@DataProvider(name = "CheckDuplicateProjectTest")
	public Object[][] checkDuplicateProjectTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "CheckDuplicateProjectTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, dataProvider = "CheckDuplicateProjectTest",dependsOnMethods={"createProjectTest"})
	public void checkDuplicateProjectTest(String projectName, String jiraProjectName, String projectDescription, String bugPrefix,
			String tcPrefix, String proStatus,String schema) throws Exception {
		Map<String, String> reponseData = projectService.insertProject(projectName, jiraProjectName, projectDescription, bugPrefix,
				tcPrefix, proStatus,schema, utils.getCurrentDateTime());
		int result = Integer.parseInt(reponseData.get("responseCode"));
		Assert.assertEquals(result, 208);
	}
	
	
}

package com.xenon.api.administration;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.test.utils.ReadExcel;
import com.xenon.test.utils.TestUtils;
import com.xenon.api.administration.LDAPService;
import com.xenon.core.dao.CustomerDAO;

@ContextConfiguration(locations = { "classpath:/WEB-INF/spring/appServlet/servlet-context.xml" })
@WebAppConfiguration
@Test(groups = "LDAP")
public class LDAPServiceTest extends AbstractTestNGSpringContextTests{

	@Autowired
	LDAPService lDAPService;
	
	@Autowired
	UtilsService utils;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	CustomerDAO customerDAO;

	
	
	private ReadExcel excel = new ReadExcel();

	String filePath = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator
			+ "resources" + File.separator + "LDAP" + File.separator + "LDAP.xlsx";

	@BeforeClass
	public void create() throws IOException {
	}

	
	@DataProvider(name = "InsertLDAPDetails")
	public Object[][] LDAPTestData() throws Exception {
	
		Object[][] arrayObject = excel.getExcelData(filePath, "InsertLDAPDetails");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, dataProvider = "InsertLDAPDetails")
	public void insertLDAPDetails(String ldapURL,String rootDN,String searchBase,String searchFilter,String userDistinguishedName,String userPassword,String displayNameAttr,String emailAddrAttr,String emailDomainName,String customerId,String ldapUserMail) {
		Map<String,String> testData=lDAPService.testLdapDetails(ldapURL,rootDN,searchBase,searchFilter,userDistinguishedName,userPassword,ldapUserMail);
		int status = Integer.parseInt(testData.get("responseCode").toString());
		if(status==201){	
		String customerSchema=customerDAO.getCustomerSchema(customerId);
		////System.out.println(customerSchema);
		Map<String,String> result=lDAPService.insertLDAPDetails(ldapURL, rootDN, searchBase, searchFilter, userDistinguishedName, userPassword, displayNameAttr, emailAddrAttr, emailDomainName, Integer.parseInt(customerId), ldapUserMail,customerSchema);
		String returnStatus = result.get("responseCode").toString();
		Assert.assertEquals(returnStatus, "201","LDAP Details inserted");
		}
		
		
		
	}
	@DataProvider(name = "TestLDAPDetails")
	public Object[][] testLDAPTestData() throws Exception {
	
		Object[][] arrayObject = excel.getExcelData(filePath, "TestLDAPDetails");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, dataProvider = "TestLDAPDetails",dependsOnMethods={"insertLDAPDetails"})
	public void testLDAPDetails(String ldapURL, String rootDN, String userSearchBase,String ldapSearchFilter, String userDn, String userPassword,String ldapUserMail) {
		
		Map<String,String> result=lDAPService.testLdapDetails(ldapURL,rootDN,userSearchBase,ldapSearchFilter,userDn,userPassword,ldapUserMail);
		
		String status = result.get("responseCode").toString();
		Assert.assertEquals(status, "201","testing unsuccessful");
	}
	
	@DataProvider(name = "AuthenticateLDAPUserTest")
	public Object[][] LDAPUserTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "AuthenticateLDAPUserTest");
		return arrayObject;
	}

	@Test(groups = { "smoke", "validation" }, dataProvider = "AuthenticateLDAPUserTest",dependsOnMethods={"insertLDAPDetails"})
	public void authenticateLDAPUserTest(String userName, String password) {
		HttpSession session = new MockHttpSession();
		String result = "";
		try {
			result = loginService.authenticateUser(userName, password, session);
			if(result=="xedashboard"){
				result="1";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(result, "1","login successful");
	}
	
	
}

package com.xenon.unit.testcase.testmanager;

import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


import com.xenon.api.common.UtilsService;
import com.xenon.test.utils.ReadExcel;
import com.xenon.test.utils.TestUtils;

import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import com.xenon.api.testmanager.ScenarioService;
import com.xenon.api.testmanager.TestCaseService;

@ContextConfiguration(locations = { "classpath:/WEB-INF/spring/appServlet/servlet-context.xml" })
@WebAppConfiguration

@Test
public class TestcaseUnitTest extends AbstractTestNGSpringContextTests {

	Properties properties;

	@Autowired
	ScenarioService scenarioService;
	
	@Autowired
	TestCaseService testCaseService;
	
	@Autowired
	UtilsService utilsService;
	
	private ReadExcel excel = new ReadExcel();

	String filePath = System.getProperty("user.dir") + "\\src\\test\\resources\\Testmanager\\Testcase.xlsx";
	
	@BeforeClass
	public void create() throws IOException {
	}
	
	@DataProvider(name = "InsertTestcase")
	public Object[][] insertTestcaseTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "InsertTestcase");
		return arrayObject;
	}
	
	@Test(groups={"smoke","verification"},enabled=true,dataProvider = "InsertTestcase")
	public void createTestcase(String moduleName,String userEmailId,String projectName,String testcaseName,String testcaseSummary,String preCondition,
			    String executionType,String executionTime,String testCaseStatus,String scenarioName) throws Exception{
		int result=testCaseService.createTestcase(moduleName,TestUtils.SCHEMA_NAME,userEmailId,projectName,testcaseName,testcaseSummary,preCondition,executionType,executionTime,testCaseStatus,scenarioName,2);
		//Assert.assertEquals(result, result>0, "Testcase is created");
	}
	
	@DataProvider(name = "UpdateTestcase")
	public Object[][] updateTestcaseTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "UpdateTestcase");
		return arrayObject;
	}
	
	@Test(groups={"smoke","verification"},enabled=true,dataProvider="UpdateTestcase")
	public void updateTestcase(String testcaseId, String testcaseName, String testcaseSummary,
				String testcasePrecondition, String executionType, String executionTime, String statusId, String updater){
		
		int result=testCaseService.updateTestcase(testcaseId,testcaseName,testcaseSummary,testcasePrecondition,executionType,executionTime,statusId,Integer.parseInt(updater),TestUtils.SCHEMA_NAME,"2",null);
		Assert.assertEquals(result,1, "Testcase is updated");
	}	
	

	@DataProvider(name = "InsertComment")
	public Object[][] InsertCommentTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "InsertComment");
		return arrayObject;
	}
	
	@Test(groups={"smoke","verification"},enabled=true,dataProvider="InsertComment")
	public void insertComment(String commentDescription, String commnetor, String testcaseId){
		int result=testCaseService.insertTestcaseComment(commentDescription,Integer.parseInt(commnetor),Integer.parseInt(testcaseId),TestUtils.SCHEMA_NAME,utilsService.getCurrentDateTime());
		Assert.assertEquals(result,0, "Comment is posted");
	}
}


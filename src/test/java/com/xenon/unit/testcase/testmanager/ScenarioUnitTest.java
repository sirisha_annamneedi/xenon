package com.xenon.unit.testcase.testmanager;

import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.Properties;

import com.xenon.api.common.UtilsService;
import com.xenon.test.utils.ReadExcel;
import com.xenon.test.utils.TestUtils;

import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import com.xenon.api.testmanager.ScenarioService;

@ContextConfiguration(locations = { "classpath:/WEB-INF/spring/appServlet/servlet-context.xml" })
@WebAppConfiguration

@Test
public class ScenarioUnitTest extends AbstractTestNGSpringContextTests {

	UtilsService utils;
	Properties properties;

	@Autowired
	ScenarioService scenarioService;
	
	private ReadExcel excel = new ReadExcel();

	String filePath = System.getProperty("user.dir") + "\\src\\test\\resources\\Testmanager\\Scenario.xlsx";

	@BeforeClass
	public void create() throws IOException {
		
	}
	
	@DataProvider(name = "InsertScenario")
	public Object[][] createScenarioTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "InsertScenario");
		return arrayObject;
	}
	
	@Test(groups={"smoke","verification"},enabled=true, dataProvider = "InsertScenario")
	public void createScenario(String moduleName,String scenarioName,String scenarioDesription,String Status,String projectName,String userEmailId) throws Exception{
		int result=scenarioService.createScenarioTest(moduleName,TestUtils.SCHEMA_NAME,scenarioName,scenarioDesription,Status,projectName,userEmailId);
		Assert.assertEquals(result, 1, "Scenario is created");
	}
	
	@DataProvider(name = "UpdateScenario")
	public Object[][] updateScenarioTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "UpdateScenario");
		return arrayObject;
	}
	@Test(groups={"smoke","verification"},enabled=true, dataProvider = "UpdateScenario")
	public void updateScenario(String scenarioName, String scenarioDescription, String scenarioId, String projectName,
			String moduleName, String userId) throws Exception{
		int result=scenarioService.updateScenario(scenarioName,scenarioDescription,scenarioId,projectName,moduleName,Integer.parseInt(userId),TestUtils.SCHEMA_NAME);
		Assert.assertEquals(result, 1, "Scenario is updated");
	}
}


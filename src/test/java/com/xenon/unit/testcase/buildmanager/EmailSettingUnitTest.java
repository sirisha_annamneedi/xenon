package com.xenon.unit.testcase.buildmanager;

import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import java.io.IOException;

import com.xenon.test.utils.ReadExcel;
import com.xenon.test.utils.TestUtils;
import com.xenon.api.buildmanager.SettingService;

import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:/WEB-INF/spring/appServlet/servlet-context.xml" })
@WebAppConfiguration

@Test
public class EmailSettingUnitTest extends AbstractTestNGSpringContextTests {

	@Autowired
	SettingService SettingService;

	private ReadExcel excel = new ReadExcel();

	String filePath = System.getProperty("user.dir") + "\\src\\test\\resources\\Buildmanager\\EMail.xlsx";

	@BeforeClass
	public void create() throws IOException {

	}

	@DataProvider(name = "UpdateNotificationBM")
	public Object[][] UpdateNotificationBMTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "UpdateNotificationBM");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, enabled = false, dataProvider = "UpdateNotificationBM")
	public void UpdateNotificationBM(String mailNotificationId, String mailStatusCreateUser,
			String mailStatusAssignProject, String mailStatusAssignModule, String mailStatusAssignBuild,
			String mailStatusBuildExecComplete) throws Exception {

		int result = SettingService.updateMailNotificationBm(TestUtils.SCHEMA_NAME,mailNotificationId,mailStatusCreateUser,mailStatusAssignProject,mailStatusAssignModule,mailStatusAssignBuild,
				 mailStatusBuildExecComplete);
		Assert.assertEquals(result, 201, "Notification for BM is updated");
	}

	@DataProvider(name = "UpdateNotificationBT")
	public Object[][] UpdateNotificationBTTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "UpdateNotificationBT");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, enabled = false, dataProvider = "UpdateNotificationBT")
	public void UpdateNotificationBT(String mailNotificationId, String mailStatusCreateBug,
			String mailStatusUpdateBug) throws Exception {

		int result = SettingService.updateMailNotificationBt(TestUtils.SCHEMA_NAME,mailNotificationId,mailStatusCreateBug,mailStatusUpdateBug);
		Assert.assertEquals(result, 201, "Notification for BT is updated");
	}
}

package com.xenon.unit.testcase.buildmanager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.xenon.api.buildmanager.BuildService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.test.utils.ReadExcel;
import com.xenon.test.utils.TestUtils;

@ContextConfiguration(locations = { "classpath:/WEB-INF/spring/appServlet/servlet-context.xml" })
@WebAppConfiguration

@Test
public class ManualBuildUnitTest extends AbstractTestNGSpringContextTests {
	
	@Autowired
	BuildService buildService;
	
	@Autowired
	UtilsService utils;
	
	@Autowired
	BuildDAO buildDAO;
	
	private ReadExcel excel = new ReadExcel();

	String filePath = System.getProperty("user.dir") + "\\src\\test\\resources\\Buildmanager\\ManualBuild.xlsx";
	
	@DataProvider(name="InsertManulaBuild")
	public Object[][] ManualBuildTestData() throws Exception{
		Object[][] arrayObject=excel.getExcelData(filePath, "InsertManulaBuild");
		return arrayObject;
	}
	
	@Test(groups = { "smoke", "verification" }, enabled = true, dataProvider = "InsertManulaBuild")
	public void InsertManualBuild(String buildName, String buildDescription, String buildStatus,
			String buildExecutionType, String buildEnv, String userName, String buildReleaseId,
			String buildCreatorID,String roleId) throws Exception {
		int result=buildService.insertBuild(buildName,buildDescription,Integer.parseInt(buildStatus),buildExecutionType, Integer.parseInt(buildEnv),1,userName,TestUtils.SCHEMA_NAME,Integer.parseInt(buildReleaseId),
				Integer.parseInt(buildCreatorID),utils.getCurrentDateTime(), roleId, null);
		Assert.assertEquals(result, 200, "Manual Build is created");
	}
	
	@DataProvider(name="UpdateManulaBuild")
	public Object[][] UpdateBuildTestData() throws Exception{
		Object[][] arrayObject=excel.getExcelData(filePath, "UpdateManulaBuild");
		return arrayObject;
	}
	
	@Test(groups = { "smoke", "verification" }, enabled = true, dataProvider = "UpdateManulaBuild")
	public void UpdateBuild(String buildName, String buildDescription, String buildStatus,
			String buildExecutionType, String buildEnv,String buildReleaseId, String buildUpdater, String buildId,String roleId) throws Exception {
		
		int result=buildService.updateBuild(buildName+utils.getCurrentDateTime(),buildDescription,Integer.parseInt(buildStatus),Integer.parseInt(buildExecutionType),Integer.parseInt(buildEnv),TestUtils.SCHEMA_NAME,Integer.parseInt(buildReleaseId),Integer.parseInt(buildUpdater) ,Integer.parseInt(buildId) ,utils.getCurrentDateTime(),roleId);
		
		Assert.assertEquals(result, 200, "Manual Build is updated");
	}
	
	
}

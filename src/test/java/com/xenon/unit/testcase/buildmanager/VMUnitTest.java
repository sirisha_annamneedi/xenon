package com.xenon.unit.testcase.buildmanager;

import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import java.io.IOException;

import com.xenon.test.utils.ReadExcel;
import com.xenon.test.utils.TestUtils;
import com.xenon.api.buildmanager.SettingService;

import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:/WEB-INF/spring/appServlet/servlet-context.xml" })
@WebAppConfiguration

@Test
public class VMUnitTest extends AbstractTestNGSpringContextTests {

	@Autowired
	SettingService SettingService;

	private ReadExcel excel = new ReadExcel();

	String filePath = System.getProperty("user.dir") + "\\src\\test\\resources\\Buildmanager\\VMDetails.xlsx";

	@BeforeClass
	public void create() throws IOException {

	}

	@DataProvider(name = "InsertVMDetails")
	public Object[][] InsertVMDetailsTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "InsertVMDetails");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, enabled = false, dataProvider = "InsertVMDetails")
	public void InsertVMDetails(String hostname, String ipAddress, String portNumber, String remoteUsername,
			String remotePassword, String projectLocation, String concExecStatus, String concurrentExecNumber,
			String ieStatus, String chromeStatus, String firefoxStatus, String safariStatus, String vmStatus,
			String customerId, String roleId,String gitStatus) throws Exception {

		int result = SettingService.insertvmdetails(hostname, ipAddress, Integer.parseInt(portNumber), remoteUsername,
				remotePassword, projectLocation, Integer.parseInt(concExecStatus),
				Integer.parseInt(concurrentExecNumber), Integer.parseInt(ieStatus), Integer.parseInt(chromeStatus),
				Integer.parseInt(firefoxStatus), Integer.parseInt(safariStatus), Integer.parseInt(vmStatus), customerId,
				TestUtils.SCHEMA_NAME, roleId,Integer.parseInt(gitStatus));
		Assert.assertEquals(result, 201, "Virtual machine is created");
	}

	@DataProvider(name = "UpdateVMDetails")
	public Object[][] UpdateVMDetailsTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "UpdateVMDetails");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, enabled = false, dataProvider = "UpdateVMDetails")
	public void UpdateVMDetails(String vmStatusFree, String hostname, String ipAddress, String portNumber,
			String remoteUsername, String remotePassword, String projectLocation, String concExecStatus,
			String concurrentExecNumber, String ieStatus, String chromeStatus, String firefoxStatus,
			String safariStatus, String vmStatus, String customerId, String selectedVmId, String roleId,String gitStatus)
			throws Exception {

		int result = SettingService.updatevmdetails(Integer.parseInt(vmStatusFree), hostname, ipAddress,
				Integer.parseInt(portNumber), remoteUsername, remotePassword, projectLocation,
				Integer.parseInt(concExecStatus), Integer.parseInt(concurrentExecNumber), Integer.parseInt(ieStatus),
				Integer.parseInt(chromeStatus), Integer.parseInt(firefoxStatus), Integer.parseInt(safariStatus),
				Integer.parseInt(vmStatus), customerId, Integer.parseInt(selectedVmId), TestUtils.SCHEMA_NAME,
				roleId,Integer.parseInt(gitStatus));
		Assert.assertEquals(result, 201, "Virtual machine details are updated");
	}

	@DataProvider(name = "InsertCloudVMDetails")
	public Object[][] InsertCloudVMTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "InsertCloudVMDetails");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, enabled = false, dataProvider = "InsertCloudVMDetails")
	public void InsertCloudVMDetails(String hostname, String ipAddress, String portNumber, String remoteUsername,
			String remotePassword, String projectLocation, String concExecStatus, String concurrentExecNumber,
			String ieStatus, String chromeStatus, String firefoxStatus, String safariStatus, String vmStatus,
			String customerId, String roleId,String gitStatus) throws Exception {

		int result = SettingService.insertCloudVmDetails(hostname, ipAddress, Integer.parseInt(portNumber),
				remoteUsername, remotePassword, projectLocation, Integer.parseInt(concExecStatus),
				Integer.parseInt(concurrentExecNumber), Integer.parseInt(ieStatus), Integer.parseInt(chromeStatus),
				Integer.parseInt(firefoxStatus), Integer.parseInt(safariStatus), Integer.parseInt(vmStatus), customerId,
				TestUtils.SCHEMA_NAME, roleId,Integer.parseInt(gitStatus));
		Assert.assertEquals(result, 201, "Could Virtual machine is created");
	}

	@DataProvider(name = "UpdateCloudVMDetails")
	public Object[][] UpdateCloudVMTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "UpdateCloudVMDetails");
		return arrayObject;
	}

	@Test(groups = { "smoke", "verification" }, enabled = true, dataProvider = "UpdateCloudVMDetails")
	public void UpdateCloudVMDetails(String vmStatusFree, String hostname, String ipAddress, String portNumber,
			String remoteUsername, String remotePassword, String projectLocation, String concExecStatus,
			String concurrentExecNumber, String ieStatus, String chromeStatus, String firefoxStatus,
			String safariStatus, String vmStatus, String selectedVmId, String roleId,String gitStatus) throws Exception {

		int result = SettingService.updateCloudvmdetails(Integer.parseInt(vmStatusFree), hostname, ipAddress,
				Integer.parseInt(portNumber), remoteUsername, remotePassword, projectLocation,
				Integer.parseInt(concExecStatus), Integer.parseInt(concurrentExecNumber), Integer.parseInt(ieStatus),
				Integer.parseInt(chromeStatus), Integer.parseInt(firefoxStatus), Integer.parseInt(safariStatus),
				Integer.parseInt(vmStatus), Integer.parseInt(selectedVmId), TestUtils.SCHEMA_NAME, roleId,Integer.parseInt(gitStatus));
		Assert.assertEquals(result, 201, "Could Virtual machine is updated");
	}

}

package com.xenon.unit.testcase.buildmanager;

import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import com.xenon.api.buildmanager.ReleaseService;
import com.xenon.api.common.UtilsService;
import com.xenon.test.utils.ReadExcel;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:/WEB-INF/spring/appServlet/servlet-context.xml" })
@WebAppConfiguration

@Test
public class ReleaseUnitTest extends AbstractTestNGSpringContextTests {

	UtilsService utils;
	Properties properties;

	@Autowired
	ReleaseService releaseService;
	
	private ReadExcel excel = new ReadExcel();

	String filePath = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator
			+ "resources" + File.separator + "Buildmanager" + File.separator + "Release.xlsx";

	@BeforeClass
	public void create() throws IOException {
		
	}
	
	@DataProvider(name = "InsertRelease")
	public Object[][] InsertReleaseTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "InsertRelease");
		return arrayObject;
		
	}
	
	@Test(groups={"smoke","verification"},enabled=true, dataProvider = "InsertRelease")
	public void InsertRelease(String releaseName, String releaseDescription, String releaseStatus,String sDate, String eDate,String roleId,String schema) throws Exception{
		int result =releaseService.createReleaseByUnitTest(releaseName+new SimpleDateFormat("MMddyyyyHHmmss").format(new Date()),releaseDescription,releaseStatus,sDate,eDate,schema,roleId);
		Assert.assertEquals(result, 201, "Release is created");
	}
	
	@DataProvider(name = "UpdateRelease")
	public Object[][] UpdateReleaseTestData() throws Exception {
		Object[][] arrayObject = excel.getExcelData(filePath, "UpdateRelease");
		return arrayObject;
	}
	
	@Test(groups={"smoke","verification"},enabled=true, dataProvider = "UpdateRelease",dependsOnMethods={"InsertRelease"})
	public void UpdateRelease(String releaseID, String releaseName, String releaseDescription,String releaseStatus, String eDate,String roleId,String schema) throws Exception{
		int result =releaseService.updateReleaseByUnitTest(releaseID,releaseName,releaseDescription,releaseStatus,eDate,schema,roleId);
		Assert.assertEquals(result, 201, "Release is updated");
	}
	
	
}


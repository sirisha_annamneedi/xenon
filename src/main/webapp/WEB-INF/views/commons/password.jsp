<div class="bar" id="barInMenu">
	<p>loading</p>
</div>
<div id="wrapper">
	<div class="row border-bottom white-bg page-heading">
		<div class="col-lg-12">
			<h2>Reset Password</h2>
		</div>
		<!-- end col -->
	</div>

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content">

						<!-- new pass and retype pass unmatch error -->
						<div class="alert alert-danger hidden col-lg-6" id="errorMessage">
							<button type="button" class="close" data-dismiss="alert"
								aria-hidden="true">�</button>
							<strong>Please enter same New Password & Retype
								Password.</strong>
						</div>

						<div class="alert alert-danger hidden " id="errorAlert">
							<button type="button" class="close" data-dismiss="alert"
								aria-hidden="true">�</button>
							<h4>
								Reset password failed <br />Please enter correct current
								password.
							</h4>
						</div>

						<div class="alert alert-success hidden" id="successAlert">
							<button type="button" class="close" data-dismiss="alert"
								aria-hidden="true">�</button>
							<h4>Password is successfully updated. Please logout and
								login with new password.</h4>
						</div>

						<form action="" id="form"
							class="wizard-big wizard clearfix form-horizontal" method="">
							<div class="content clearfix">
								<fieldset class="body current">
									<label class="col-lg-2 pull-right">* fields are
										mandatory</label><br>
									<div class="row">
										<div class="col-lg-8">
											<div class="form-group">
												<label class="control-label col-lg-2">Current
													Password:*</label>
												<div class="col-lg-7">
													<input type="password" placeholder="Current password"
														id="CurrentPassword" class="form-control"
														name="currentPassword">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-lg-2">New Password:*</label>
												<div class="col-lg-7">
													<input type="password" placeholder="New password"
														id="newPassword" class="form-control" name="newPassword"
														onchange="checkPassword('newPassword','newPswdError')">
													<label class="error hidden" id="newPswdError"></label>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-lg-2">Retype
													Password:*</label>
												<div class="col-lg-7">
													<input type="password" placeholder="Retype password"
														id="retypePassword" class="form-control"
														name="retypePassword"> <input type="hidden"
														id="userId" name="userId" value="${Model.userId}">
												</div>
											</div>
										</div>
										<div class="col-lg-4">
											<h3>The password should :</h3>
											<ul class="nav nav-third-level">
												<li>Be 8 - 16 characters long</li>
												<li>Include at least 1 uppercase character (A,B,C)</li>
												<li>Include at least 1 lowercase character (a,b,c)</li>
												<li>Include at least 1 number (1,2,3)</li>
												<li>Include at least 1 special character ($,#,@)</li>
											</ul>
										</div>
									</div>
								</fieldset>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-4">
										<button class="btn btn-white pull-left"
											style="margin-right: 15px;" type="button" id="cancelBtn"
											tabindex="6">Cancel</button>
										<button class="btn btn-success pull-left"
											style="margin-right: 15px;" type="button" id="submitBtn">Submit</button>
									</div>
								</div>
							</div>
						</form>
						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<!-- end row -->
	</div>
	<!-- end .wrapper -->
</div>
<!-- end #wrapper -->



<input type="hidden" id="hiddenPassStatus"
	value='<%=session.getAttribute("passUpdateStatus")%>'>

<script>
	$(window).load(function() {
		$("#barInMenu").fadeOut();
	});

	$(document).ready(function() {
		//password alerts.
		var status = $('#hiddenPassStatus').val();
		if (status == 1) {
			$('#successAlert').removeClass('hidden');
		} else if (status == 2) {
			$('#errorAlert').removeClass('hidden');
		}
	});

	$('#form').validate({
		rules : {
			currentPassword : {
				required : true,
				minlength : 8,
				maxlength : 16
			},
			newPassword : {
				required : true,
			},
			retypePassword : {
				required : true,
				minlength : 8,
				maxlength : 16
			}
		}
	});

	$("#submitBtn").click(function() {

		$("#errorMessage").addClass('hidden');
		$("#errorAlert").addClass('hidden');
		$('#successAlert').addClass('hidden');

		var formValid = $("#form").valid();
		if (formValid) {
			if ($("#retypePassword").val() == $("#newPassword").val()) {
				$("#barInMenu").fadeIn();
				$("#wrapper").addClass("hidden");
				var posting = $.post('updatepassword', {
					password : $("#CurrentPassword").val(),
					newPassword : $("#newPassword").val(),
					userId : $("#userId").val()
				}, function(response) {
					if (response == 1) {
						swal({
					        title: "",
					        text: "Password is successfully updated.",
					        type: "success",
					        showCancelButton: false,
					        confirmButtonColor: "rgb(49, 148, 127)",
					        confirmButtonText: "Ok",
					        closeOnConfirm: true
					    }, function (isConfirm) {
					    	if (isConfirm) {
					    		document.location.href="logout";
							}
					    });
					} else if (response == 2) {
						window.location.href = "password";
					} else if (response == 3) {
						window.location.href = "500";
					}
				});
			} else {
				$("#errorMessage").removeClass('hidden');
			}
		}
	});

	$("#cancelBtn").click(function() {
		window.location.href = "logout";
	});
	function checkPassword(checkId1, msgboxId1) {
		var checkId = "#" + checkId1;
		var msgboxId = "#" + msgboxId1;
		var errorMsg = "";
		var flag = 0;
		$("#submitBtn").prop("disabled", true);
		//alert($("#txtPassword").val());
		var pattern1 = /[0-9]/;
		var pattern2 = /[a-z]/;
		var pattern3 = /[A-Z]/;
		var pattern4 = /[!@#\$]/;
		var pattern = /[a-zA-Z0-9]/;
		var password = $(checkId).val();
		if (password.length > 7 && password.length < 17) {

		} else {
			errorMsg += "Password must be 8 - 16 characters long<br>";
			flag = 1;
		}
		if ($(checkId).val().match(pattern1)) {

		} else {
			errorMsg += "Password must contain at least one numeric<br>";
			flag = 1;
		}

		if ($(checkId).val().match(pattern2)) {
		} else {
			errorMsg += "Password must contain at least one a-z<br>";
			flag = 1;
		}
		if ($(checkId).val().match(pattern3)) {
		} else {
			errorMsg += "Password must contain at least one A-Z<br>";
			flag = 1;
		}
		if ($(checkId).val().match(pattern4)) {
		} else {
			errorMsg += "Password must contain at least one special symbol from !@#$<br>";
			flag = 1;
		}

		if (flag == 1) {
			//$(msgboxId).html("Password must contain at least one A-Z and  a-z  and any one special character from !@#$");
			$(msgboxId).html(errorMsg);
			$(msgboxId).removeClass("hidden");
			$(msgboxId).css("display","");
			$("#newPassword-error").addClass("hidden");

		} else {
			$(msgboxId).html("This field is required");
			$(msgboxId).addClass("hidden");
			$("#submitBtn").prop("disabled", false);
		}
	}
</script>

<%
	//set session variable to another value
	session.setAttribute("passUpdateStatus", 11);
%>
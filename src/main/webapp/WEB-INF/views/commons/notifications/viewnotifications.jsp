<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Notifications</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li class="active"><strong>Notifications</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	
	<div class="row" id="VMListDiv">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-content">
				
					<h2>Notification Details</h2>

					<div class="clients-list">
						<div class="full-height-scroll">
							<div class="table-responsive">
								<table class="table table-striped table-hover"
									id="tblNotificationList">
									<thead>
										<tr>
											<th style="display: none">Id</th>
											<!-- <th>Activity</th> -->
											<th>Description</th>
											<!-- <th>Module</th> -->
											<th>Date</th>
											<th>User</th>
										
										</tr>
									</thead>
									<tbody>
										<c:forEach var="notifications" items="${Model.notifications}">
											<tr>
												<td style="display: none">${notifications.notification_id}</td>
												<%-- <td>${notifications.activity}</td> --%>
												<td>${notifications.notification_description}</td>
												<%-- <td>${notifications.module}</td> --%>
												<td>${notifications.notification_date}</td>
												<td>${notifications.first_name}&nbsp;${notifications.last_name}</td>
												
														
											
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<!-- end row -->
</div>
<!-- end wrapper -->
<!-- Input Mask-->
    <script src="<%=request.getContextPath()%>/resources/js/mask/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript">
$(window).load(function() {
	$("#barInMenu").fadeOut();
	$("#wrapper").removeClass("hidden");
});
  $(function () {
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));

	});
$(function() {
 		$('#tblNotificationList').DataTable({
 			"paging": true,
 	 	    "lengthChange": true,
 	 	    "searching": true,
 	 	    "ordering": true,
 	 	    "info": true,
 	        "autoWidth": false,
 	       "aoColumnDefs" : [ {
				'bSortable' : false,
				'aTargets' : [ 4 ]
			}],
			"order": [[ 0, "desc" ]]

 		});
 		
 		  
 	});

	 
</script>

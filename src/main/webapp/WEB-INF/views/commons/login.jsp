<%@ taglib tagdir='/WEB-INF/tags' prefix='sc'%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Login</title>

<link rel="icon" type="image/png"
	href="<%=request.getContextPath()%>/resources/img/xenon.png">

<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/font-awesome/css/font-awesome.css"
	rel="stylesheet">

<link href="<%=request.getContextPath()%>/resources/css/animate.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/resources/css/style.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/xenon_custom.css"
	rel="stylesheet">
	
</head>


<body class="white-bg">
<div class="bar" id="bar">
	<p>Loading For Login</p>
</div>
<div id="backgroundImgDiv" class="img-responsive hidden" style="background:linear-gradient(0deg, rgba(3, 169, 244, 0.4), rgba(77, 171, 245, 0.6)), url(<%=request.getContextPath()%>/resources/img/login/loginpage-bg1.jpg); width: 100%;height: 100%; background-size:cover; background-position:center">
<div class=" " style="height: 100%; padding-top: 0px; padding-bottom: 0px;" >
	<div class="row text-left" style="height:100%;">
	
	<div class="col-md-4 col-md-offset-8">
	<div class="main-login">
	<div id="imageDiv" class="text-center"> 
				<img alt="screenshot" src="<%=request.getContextPath()%>/resources/img/Jade-Logo.png" style="width:100px;">
			</div><!-- end #imageDiv -->
	<div id="formDiv" class="login-form"> 
				<form class="" role="form" id="loginForm" action="authenticateUserRequest" method="POST" autocomplete="off">
					
					<div class="form-group">
						<div class="input-group m-b">
						<label>Enter Username</label>
							<input id="userName" name="userName" type="text" autocomplete="off"
								class="form-control" placeholder="Username" required="required">
							<!-- <span class="input-group-addon"><i class="fa fa-user"></i></span> -->
						</div>
					</div>
					
					<div class="form-group">
						<div class="input-group m-b">
						<label>Enter Password</label>
							 <input id="password" name="password" autocomplete="off"
								type="password" class="form-control" placeholder="Password"
								required="required">
							<!-- <span class="input-group-addon"><i class="fa fa-lock"></i></span> -->
						</div>
					</div>
					<font color="red" style="font-size: 20px;">${Model.message}</font>
				<%-- <sc:captcha/> --%>

				
					<p class="text-danger hidden" id="failValidationMessage">
						The Username or password you entered don't match.
					</p>
										
					<button type="submit" class="btn btn-primary" id="signInbtn" 
						style="border : none; font-size: 20px; padding-right: 25px; padding-left: 25px;margin-top: 3%;">Sign In</button>
					
				</form>
			</div><!-- end #formDiv -->
			<div id="linkDiv">
				<a href="forgotpassword" style="color: #fff;"> 
					Forgot password? Click Here.
				</a>
			</div><!-- end #linkDiv -->
	</div>
	</div>
	
	<%-- 	<div class="col-md-4 col-md-offset-1" style="background : #e3e7eb; height: 100%;">
			<div id="screenshotDiv" style="" class="vertical-center">
				<h1 style="color: #233545; font-size: 26px; margin-bottom: 10%;">				
				<img alt="screenshot" src="<%=request.getContextPath()%>/resources/img/xenon_logo.png" width="150" height="65">
</h1>
				<img alt="screenshot" src="<%=request.getContextPath()%>/resources/img/login/screenshot.png">
			</div><!-- end #screenshotDiv -->
		</div><!-- end .col --> --%>
		
	<%-- 	<div class="col-md-6" style="background : #FFFFFF; height: 100%;" >
			<div class="vertical-center" style="padding: 0 10%;"> 
			<div id="imageDiv" style="margin-bottom: 3%;"> 
				<img alt="screenshot" src="<%=request.getContextPath()%>/resources/img/Jade-Logo.png" width="150" height="60">
			</div><!-- end #imageDiv -->
			
			<div id="formDiv" style="margin-bottom: 3%;"> 
				<form class="" role="form" id="loginForm" action="authenticateUserRequest" method="POST" autocomplete="off">
					
					<div class="form-group">
						<div class="input-group m-b">
						
							<input id="userName" name="userName" type="text" autocomplete="off"
								class="form-control" placeholder="Username" required="required">
							<span class="input-group-addon"><i class="fa fa-user"></i></span>
						</div>
					</div>
					
					<div class="form-group">
						<div class="input-group m-b">
							 <input id="password" name="password" autocomplete="off"
								type="password" class="form-control" placeholder="Password"
								required="required">
							<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						</div>
					</div>
					<font color="red" style="font-size: 20px;">${Model.message}</font>
				<sc:captcha/>

				
					<p class="text-danger hidden" id="failValidationMessage">
						The Username or password you entered don't match.
					</p>
										
					<button type="submit" class="btn btn-success" id="signInbtn" 
						style="background: #ff7e00; border : none; font-size: 20px; padding-right: 25px; padding-left: 25px;margin-top: 3%;">Sign In</button>
					
				</form>
			</div><!-- end #formDiv -->
			
			<div id="linkDiv">
				<a href="forgotpassword" style="color: #6d757c;"> 
					Forgot password?
				</a>
			</div><!-- end #linkDiv -->
			</div>
		</div><!-- end .col --> --%>
		
	</div><!-- end .row -->
</div><!-- end .container -->
</div>
<!-- hidden session values -->
<input type="hidden" value="<%=session.getAttribute("LogoutFlag")%>"
	id="hiddenValidationStatus">
<input type="hidden" name="userId" id="userId"
	value="<%=session.getAttribute("userId")%>">

<script src="<%=request.getContextPath()%>/resources/js/jquery-2.1.1.js"></script>
<!-- Jquery Validate -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/validate/jquery.validate.min.js"></script>
<script type="text/javascript">
	$(window).load(function() {
		 $("#bar").fadeOut();
		$("#backgroundImgDiv").removeClass("hidden"); 
	});

	$(document).ready(function() {
		
		if (window.history && window.history.pushState) {
			window.history.pushState('forward', null, null);
			$(window).on('popstate', function() {
				window.location.href = window.location.href;
			});

		}

		var currentState = history.state;
		console.log(currentState);

		userId = $("#userId").val();
		if (userId > 0)
			window.location.replace("xedashboard");

		var loginField = $('#hiddenValidationStatus').val();

		if (loginField == -1) {
			$('#failValidationMessage').removeClass('hidden');
			$('#hiddenValidationStatus').val(10);
		}
		
		$("#loginForm").validate({
			rules : {
				userName : {
					required : true
				},
				password: {
					required : true
				}
			}
		});
	});

	/* $('#loginForm').submit(function() {
		$("#bar > p").text('Authenticating');
		$("#bar").fadeIn();
		$("#backgroundImgDiv").addClass("hidden");
		return true;
	});  */
	
	$('#signInbtn').click(function(){
		var formValid = $("#loginForm").valid();
		if (formValid){
			$("#bar > p").text('Authenticating');
			$("#bar").fadeIn();
			$("#backgroundImgDiv").addClass("hidden");
			
			$('#loginForm').submit();
		}
	});
</script>
 <style>
 .main-login{
 background:rgba(0,0,0,0.7);
 height:100vh;
 padding: 80px 50px;
 color:#fff;
 }
 .login-form{
 padding: 50px 0;
 }
 .login-form input{
 border-radius:3px !important;
 border-top-left-radius: 3px !important;
    border-bottom-left-radius: 3px !important;
 }
 .input-group .form-control{
 color:#000;
 }
 
 </style>
</body>
<%
	//set session variable to another value
	session.setAttribute("LogoutFlag", 99);
%>
</html>

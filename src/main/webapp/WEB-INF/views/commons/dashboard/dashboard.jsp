<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />



<title>dashboard</title>
<link
	href="<%=request.getContextPath()%>/resources/css/xenon_custom.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/resources/css/newstyle.css"
	rel="stylesheet">
<script
	src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
<style>
.dropdown:hover .dropdown-menu {
	display: block;
}
.dropdown-menu {
	margin-top: -10px !important;
}
.class-apps-dropdown-menu-ul{
	overflow-y: scroll;
	max-height: 500px; 
}
</style>
</head>
<body class="top-navigation white-bg pace-done">
	<!-- 	<div id="spinner"></div> -->
	<div class="bar" id="bar">
		<p>loading</p>
	</div>

	<div class="modal inmodal" id="assignModel" tabindex="-1" role="dialog"
		aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-sm">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<!-- <span aria-hidden="false">�</span><span class="sr-only">Close</span> -->
					</button>
					<h6 class="modal-title">Set Default Application</h6>
					<small>Please select default for Test Manager and Bug
						Tracker. </small>
				</div>
				<div class="modal-body" style="padding: 5px 5px 5px 5px;">
					<div class="row">
						<div class="col-lg-8" align="center"
							style="margin: auto; float: none;">
							<div class="dropdown" style="width: 100%">
								<button class="btn btn-success dropdown-toggle" type="button"
									data-toggle="dropdown">
									<span id="assignCurrent"
										name="${Model.projectList[0].project_id}">${Model.projectList[0].project_name}</span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu"
									style="position: absolute; width: 100%">

									<c:forEach var="projectList" items="${Model.projectList}">
										<li id="${projectList.project_id}"><a href="#"
											onclick="setCurrentProject('${projectList.project_id}','${projectList.project_name}')">${projectList.project_name}</a></li>
									</c:forEach>
								</ul>
							</div>
						</div>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success" data-dismiss="modal"
						id="inserrtProject">Save changes</button>
				</div>
			</div>
		</div>
	</div>


	<div class="sweet-overlay" tabindex="-1"
		style="opacity: -0.06; display: none;"></div>




	<div class="sweet-alert hideSweetAlert" data-custom-class=""
		data-has-cancel-button="false" data-has-confirm-button="true"
		data-allow-outside-click="false" data-has-done-function="false"
		data-animation="pop" data-timer="null"
		style="display: none; margin-top: -170px; opacity: -0.06;">
		<div class="sa-icon sa-error" style="display: none;">
			<span class="sa-x-mark"> <span class="sa-line sa-left"></span>
				<span class="sa-line sa-right"></span>
			</span>
		</div>
		<div class="sa-icon sa-warning" style="display: none;">
			<span class="sa-body"></span> <span class="sa-dot"></span>
		</div>
		<div class="sa-icon sa-info" style="display: none;"></div>
		<div class="sa-icon sa-success" style="display: block;">
			<span class="sa-line sa-tip"></span> <span class="sa-line sa-long"></span>

			<div class="sa-placeholder"></div>
			<div class="sa-fix"></div>
		</div>
		<div class="sa-icon sa-custom" style="display: none;"></div>
		<h2>Good job!</h2>
		<p style="display: block;">You clicked the button!</p>
		<fieldset>
			<input type="text" tabindex="3" placeholder="">
			<div class="sa-input-error"></div>
		</fieldset>
		<div class="sa-error-container">
			<div class="icon">!</div>
			<p>Not valid!</p>
		</div>
		<div class="sa-button-container">
			<button class="cancel" tabindex="2"
				style="display: none; box-shadow: none;">Cancel</button>
			<button class="confirm" tabindex="1"
				style="display: inline-block; box-shadow: rgba(174, 222, 244, 0.8) 0px 0px 2px, rgba(0, 0, 0, 0.0470588) 0px 0px 0px 1px inset; background-color: rgb(174, 222, 244);">OK</button>
		</div>
	</div>


	<div id="divContent" class="xedashboard-head">
		<div class="row border-bottom blackRowIssue">
			<nav class="navbar-fixed-top left0" role="navigation"
				style="background-color: #FFFFFF !important">
				<div class="navbar-header navHeaderWidth">
					<a href="xedashboard" class="navbar-minimalize minimalize-styl-2 headerLogo smallMargin"><img
						src="<%=request.getContextPath()%>/resources/img/Jade-Logo.png" height="40px" alt=""></a>

				</div>
				<div class="navbar-collapse collapse" id="navbar2">

					<ul class="nav navbar-nav pull-right">
					<li class="dropdown">
	       				<c:if test="${sessionScope.redwoodStatus==1}">
<!-- 									<li><a href="#" onclick="gotoLink('bpft')">BPFT</a></li> -->
									<li><a href="bpft">BPFT</a></li>
								</c:if>
	       			</li>
					
        			<li class="dropdown mobile-hide" id="${Model.projectList[0].projectId}" >
					
						<!-- This is hidden on the medium and large screens -->
						<a aria-expanded="false" role="button" href="#"
							class="dropdown-toggle hidden-md hidden-lg firstAppName" data-toggle="dropdown" id="">
	<%-- 						${fn:substring(Model.projectList[0].projectName,0,5)} --%>
							Applications
							<span class="caret"></span>
						</a>
						
						<!-- This is hidden on the extra small and small screens -->
						<a aria-expanded="false" role="button" href="#"
							class="dropdown-toggle hidden-xs hidden-sm firstAppName" data-toggle="dropdown" id="">
							Applications
	<%-- 						${Model.projectList[0].projectName} --%>
							<span class="caret"></span>
						</a>
	
						<ul class="dropdown-menu animated fadeInRight m-t-xs" > 
							<c:forEach var="projectList" items="${sessionScope.projectListSession}">
								<li id="${projectList.project_id}"><a href="#"
									onclick="setCurrentProjectSession('${projectList.project_id}','${projectList.project_name}')">${projectList.project_name}</a></li>
							</c:forEach>
	<!-- 						<li><a href="#" onclick="setAllProjectTM()">All Applications</a></li> -->
						</ul> 
						
						<%-- <ul class="dropdown-menu">
						<select class="form-control selcls" id="projectName_${projectList.project_name}" name="" onchange="changeProjectName(${projectList.project_name})">
							<c:forEach var="projectList" items="${sessionScope.projectListSession}">
								<li id="${projectList.project_id}" data-toggle="dropdown"><a href="#"  class="dropdown-item"
									onclick="setCurrentProjectSession('${projectList.project_id}','${projectList.project_name}')">${projectList.project_name}</a></li>
							</c:forEach><span class="caret"></span>
	<!-- 						<li><a href="#" onclick="setAllProjectTM()">All Applications</a></li> -->
						</ul> --%>
					</li>
						<li class="mobile-hide dropdown"><a aria-expanded="false"
							role="button" href="#" onclick=""
							class="dropdown-toggle firstAppName" data-toggle="dropdown">Test
								Manager <span class="caret"></span>
						</a>
							<ul class="dropdown-menu animated fadeInRight">
								
<!-- 								<li><a href="#" onclick="gotoLink('testcase')">Application -->
<!-- 										Test Manager</a></li> -->
								<li><a href="testcase">Application
										Test Manager</a></li>
								<c:if test="${sessionScope.apiStatus==1}">
<!--                                 	<li><a href="#" onclick="gotoLink('apitest')">API Test Manager</a></li> -->
                                	<li><a href="apitest">API Test Manager</a></li>
                               </c:if>
<!--                                 <li><a href="#" -->
<!-- 									onclick="gotoLink('documentlibrary')"> Document Library </a></li> -->
								<li><a href="documentlibrary"> Document Library </a></li>
										
					</ul>
					</li>
	       			<li class="dropdown tst-exc">
	       				<a aria-expanded="false" role="button" href="#"
							class="dropdown-toggle firstAppName" data-toggle="dropdown" id="">Test Execution <span class="caret"></span></a>
	       				<ul class="dropdown-menu animated fadeInRight">
<!-- 						    <li><a href="#" onclick="gotoLink('executebuild')">Manual</a></li> -->
						    <li><a href="executebuild">Manual</a></li>
<!-- 						    <li><a href="#" onclick="gotoLink('autoexecute')">Automation</a></li> -->
						    <li><a href="autoexecute">Automation</a></li>
						</ul>
	       			</li>
        			<li><a href="viewallbugs">Bug Tracker</a></li>
        			<c:if test="${Model.userRole == 2}">
<!--                         <li><a onclick="checkAccess('admindashboard')">Administration</a></li> -->
                        <li><a href="admindashboard">Administration</a></li>
                    </c:if>
<!--         			<li><a onclick="checkAccess('dashandrepo')">Dashboard & Reports</a></li> -->
        			<li><a href="bmdashboard">Dashboard & Reports</a></li>
<!-- 						<li><a href="help" class="fa fa-support" -->
<!-- 							data-original-title='Support' data-container='body' -->
<!-- 							data-toggle='tooltip' data-placement='bottom'> </a></li> -->
					<li id="rightToggleLi">
	                   <a class="right-sidebar-toggle count-info"> <i class="fa fa-tasks"></i>
	                       <span class="label label-primary" id="notificationReadCount" style="background-color: #3f81bc;">${Model.unreadCount}</span>
	                   </a>
	               	</li>    
	               	<li style="margin-top: 15px; margin-right: 20px;">
	               	 <div class="dropdown profile-element">
                            
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
                                    <img alt="image" class="rounded-circle img-circle" src="./resources/img/delete.png">
                                <b class="caret"></b>
                                </a>
                                <ul class="abc dropdown-menu animated fadeInRight m-t-xs" x-placement="bottom-start" style="position: absolute; top: 28px; left: -90px; will-change: top, left;">
                                    <li><a class="dropdown-item" href="viewtmtrash">Test Manager</a></li>
                                
    <!--                                 <li class="divider"></li> -->
                                    <li><a class="dropdown-item" href="viewbuildtrash">Test Execution</a></li>
    <!--                                 <li class="divider"></li> -->
                                </ul>
                        </div>
	                  
	                </li>  
					<li class="main-page-logout">
					
                   		<div class="dropdown profile-element">
                          
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
                                <img alt="image" class="rounded-circle img-circle" src="./resources/img/user.png" style="width:20px; height:19px;">
                            <b class="caret"></b>
                            </a>
                            <ul class="abc dropdown-menu animated fadeInRight m-t-xs" x-placement="bottom-start" style="position: absolute; top: 34px; left: -90px; will-change: top, left;">
                                <li><a class="dropdown-item" href="#" onclick="gotoLinkInMenu('profile')">${sessionScope.userFullName }<br><span class="user-role">${sessionScope.role }</span></a></li>
                              
                                <li class="divider"></li>
                                <li><a class="dropdown-item" href="#" onclick="gotoLink('logout')">Logout</a></li>
                            </ul>
                        </div>
                   </li>

					</ul>
				</div>
				<!-- end #navbar2 -->
			</nav>
		</div>

</div>
<!-- <button id="b1"> click</button> -->

		<div id="wrapper" class="xedashboard-bg xedashboard-bg-custom banner-cutom-text">
		<div class="container">
		<%-- <div class="row">
		<div class="col-md-3">
		<div class="user-profile">
		<div class="user-content">
		<div class="user-img">
		<img src="<%=request.getContextPath()%>/resources/img/icon/avtar.jpg">
		</div>
		<div class="user-details">
		<h5>Mohammad Afzal Khan</h5>
		<p>UI Developer (TS)</p>
		<p>Last Login: 19-09-2018 12:19pm</p>
		</div>
		</div>
		<div class="user-personal-details">
		<div class="user-widget-info">
		<span>Email:</span>
		<a href="mailto:mohammadafzal.khan@jadeglobal.com">mohammadafzal.khan@jadeglobal.com</a>
		</div>
		<div class="user-widget-info">
		<span>Phone:</span>
		<a href="tel:+91-9911097247">+91-9911097247</a>
		</div>
		<div class="user-widget-info">
		<span>Location:</span>
		<a href="javascript:void(0)">Pune, India</a>
		</div>
		</div>
		<div class="user-about ">
		<h3>About:</h3>
		<p>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
		</p>
		<a href="#" class="btn btn-success btn-profile btn-sm"><i class="fa fa-user"></i> Change Avtar</a>
		<a href="#" class="btn btn-info btn-profile btn-sm"><i class="fa fa-key"></i> Change Password</a>
		<a href="#" class="btn btn-danger btn-profile btn-sm"><i class="fa fa-sign-out"></i> Logout</a>
		</div>
		</div>
		</div>
		<div class="col-md-6">
		<div class="row">
		<div class="col-md-6">
		<div class="navwidget-container text-center" id="tmDiv" onclick="checkAccess('testspecification')">
		<a href="javascript:void(0)">
			<div class="navwidget-image">
			<img src="<%=request.getContextPath()%>/resources/img/icon/test-manager.png">
			</div>
			<div class="navwidget-title">
			<h3>Test Manager</h3>
			<p>Manage Your Business Scenarios and Test Cases Efficiently </p>
			</div>
			</a>
		</div>
		</div>
		<div class="col-md-6">
		<div class="navwidget-container text-center" id="bmDiv">
		<a href="javascript:void(0)">
			<div class="navwidget-image">
			<img src="<%=request.getContextPath()%>/resources/img/icon/test-execution.png">
			</div>
			<div class="navwidget-title">
			<h3>Test Execution</h3>
			<p>Manage and Make Testing Agile </p>
			</div>
			</a>
		</div>
		</div>
		<div class="col-md-6">
		<div class="navwidget-container text-center" id="btDiv" onclick="checkAccess('btdashboard')">
		<a href="javascript:void(0)">
			<div class="navwidget-image">
			<img src="<%=request.getContextPath()%>/resources/img/icon/bug-tracker.png">
			</div>
			<div class="navwidget-title">
			<h3>Bug Tracker</h3>
			<p>Report and Track Your Bugs Effectively</p>			
			</div>
			</a>
		</div>
		</div>
		<div class="col-md-6">
		<c:if test="${Model.userRole == 2}">
		<div class="navwidget-container text-center" id="adminDiv">
		<a href="javascript:void(0)">
			<div class="navwidget-image">
			<img src="<%=request.getContextPath()%>/resources/img/icon/administration.png">
			</div>
			<div class="navwidget-title">
			<h3>Administration</h3>
			<p>Admin Settings</p>
			</div>
			</a>
		</div>
		</c:if>
		</div>
		<div class="col-md-6">
		<div class="navwidget-container text-center" id="drDiv" onclick="">
		<a href="javascript:void(0)">
			<div class="navwidget-image">
			<img src="<%=request.getContextPath()%>/resources/img/icon/dashboard.png">
			</div>
			<div class="navwidget-title">
			<h3>Dashboard & Reports</h3>
			<p>Dashboard & Reports Functionality </p>
			</div>
			</a>
		</div>
		</div>
		
		</div>
		</div>
		<div class="col-md-3">
		<div class="bug-list-dashboard">
		<h3 class="text-center">Pending Bugs</h3>
		<table class="table table-bordered">
		    <thead>
		      <tr>
		        <th>Bug Id</th>
		        <th>Bug Title</th>
		        
		      </tr>
		    </thead>
		    <tbody>
		    <tr>
		        <td><a href="#">KT001</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		      <tr>
		        <td><a href="#">KT002</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		      <tr>
		        <td><a href="#">KT003</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		      <tr>
		        <td><a href="#">KT004</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		      <tr>
		        <td><a href="#">KT005</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		      <tr>
		        <td><a href="#">KT006</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		      <tr>
		        <td><a href="#">KT007</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		      <tr>
		        <td><a href="#">KT008</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		      <tr>
		        <td><a href="#">KT009</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		      <tr>
		        <td><a href="#">KT010</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		      <tr>
		        <td><a href="#">KT011</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		      <tr>
		        <td><a href="#">KT012</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		      <tr>
		        <td><a href="#">KT013</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		      <tr>
		        <td><a href="#">KT014</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		      <tr>
		        <td><a href="#">KT015</a></td>
		        <td><a href="#">Header Bug</a></td>
		      </tr>
		     
		  
		      
		    </tbody>
		    </table>
		</div>
		</div>
		</div> --%>
		</div>	
				
			
		</div>
			<!-- end #wrapper -->

<!-- 				<section class="promo"> -->
<!-- 					<h1> -->
<!-- 						Xenon<sup -->
<!-- 							style="vertical-align: super; font-size: 0.3em !important;">TM</sup> -->
<!-- 						is a Silver Level Selenium Sponsor -->
<!-- 					</h1> -->
<!-- 				</section> -->
				<div class="footer fixed">
<!-- 					<div class="pull-right"> -->
<!-- 						<a href="http://www.seleniumhq.org/sponsors/"><strong>Xenon -->
<!-- 						</strong>Silver Level Selenium Sponsor.</strong></a> -->
<!-- 					</div> -->
					<div>
						<strong>Copyright</strong>&copy;2021 Jade Global Inc. All rights reserved.
					</div>
				</div>
			
		
		
	
	<!-- end #page-Wrapper -->

	<!-- Mainly scripts -->
	<script
		src="<%=request.getContextPath()%>/resources/js/jquery-2.1.1.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/plugins/toastr/toastr.min.js"></script>
		

	<script type="text/javascript">
		$(window).load(function() {
			$("#bar").fadeOut();
			$("#divContent").removeClass("hidden");
		})
	</script>
	<script type="text/javascript">
		var image = '${Model.customerLogo}';
		localStorage.setItem("customerLogo", image);
		$('[data-toggle="tooltip"]').tooltip();
	</script>
	<script type="text/javascript">
		var tmStatus = '${Model.tmStatus}';
		var btStatus = '${Model.btStatus}';
		var coreStatus = '${Model.coreStatus}';
		var adminStatus = '${Model.adminStatus}';

		localStorage.setItem("tmStatus", tmStatus);
		localStorage.setItem("btStatus", btStatus);
		localStorage.setItem("coreStatus", coreStatus);
		localStorage.setItem("adminStatus", adminStatus);
		var userRole = "${Model.userRole}";

		if (btStatus == 2 && tmStatus == 1) {
			$("#tmDiv").removeClass("hidden");
			$("#btDiv").addClass("hidden");
			$("#tmDiv").addClass("col-lg-6");
			$("#bmDiv").addClass("col-lg-6");
			if (userRole == 2) {
				$("#adminDiv").addClass("col-lg-4");
				$("#tmDiv").addClass("col-lg-4");
				$("#btDiv").addClass("col-lg-4");
				$("#bmDiv").addClass("col-lg-4");
			}

		} else if (tmStatus == 2 && btStatus == 1) {
			$("#btDiv").removeClass("hidden");
			$("#tmDiv").addClass("hidden");
			$("#btDiv").addClass("col-lg-6");
			$("#bmDiv").addClass("col-lg-6");
			if (userRole == 2) {
				$("#adminDiv").addClass("col-lg-4");
				$("#bmDiv").addClass("col-lg-4");
				$("#btDiv").addClass("col-lg-4");
			}
		} else if (btStatus == 2 && tmStatus == 2) {
			$("#tmDiv").addClass("hidden");
			$("#btDiv").addClass("hidden");
			$("#bmDiv").addClass("col-lg-12");
			if (userRole == 2) {
				$("#bmDiv").addClass("col-lg-6");
				$("#adminDiv").addClass("col-lg-6");
			}
		} else if (btStatus == 1 && tmStatus == 1) {
			$("#tmDiv").removeClass("hidden");
			$("#btDiv").removeClass("hidden");
			$("#tmDiv").addClass("col-lg-4");
			$("#bmDiv").addClass("col-lg-4");
			$("#btDiv").addClass("col-lg-4");
			if (userRole == 2) {
				$("#adminDiv").addClass("col-lg-3");
				$("#tmDiv").addClass("col-lg-3");
				$("#bmDiv").addClass("col-lg-3");
				$("#btDiv").addClass("col-lg-3");
			}
		}

		var moduleLink;

		function checkAccess(link) {
			moduleLink = link;
			var assignStatus = '${Model.projectList[0].AssignProject}';
			if (assignStatus == "False") {
				swal({
					title : "You need a minimum 1 application to access. ",
					text : "Please contact administrator."
				});
			} else if (assignStatus == "True") {
				$('#assignModel').modal('show');

			} else {
				$("#bar").fadeIn();
				$("#divContent").addClass("hidden");

				window.location.href = link;
			}
		}
		$("#bmDiv").click(function() {

			$("#bar").fadeIn();
			$("#divContent").addClass("hidden");

			//window.location.href = "bmdashboard"; //changes new dashboard
			window.location.href = "viewbuild";
		});
		$("#adminDiv").click(function() {

			$("#bar").fadeIn();
			$("#divContent").addClass("hidden");

			window.location.href = "admindashboard";
		});
		
		$("#drDiv").click(function(){
			$("#bar").fadeIn();
			$("#divContent").addClass("hidden");
			window.location.href = "dashandrepo";
		});
		
		
		$('#inserrtProject').click(function() {

			var id = $('#assignCurrent').attr("name");
			var name = $('#assignCurrent').val();

			var posting = $.post('insertCurrentProject', {
				projectId : id,
				projectName : name
			});

			posting.done(function(data) {
				$("#bar").fadeIn();
				$("#divContent").addClass("hidden");
				window.location.href = moduleLink;
			});

		});

		 
		  $('.dropdown').click(function() {
			$(this).toggleClass("open");
		});  
		 
		/*  $('.drop').click(function() {
			$(this).dropdown("toggle");
			//e.stopPropagation(); 
		});  */ 
		
		function setCurrentProject(id, name) {

			$('#assignCurrent').text(name);
			$('#assignCurrent').attr("name", id);
		}
		/* function setCurrentProjectSession(id, name) {
			var posting = $.post('setCurrentProjectTM', {
				projectId : id,
				projectName : name
			}); 
		}*/
		
		function setCurrentProjectSession(id, name) {
			var posting = $.post('setCurrentProjectTM', {
				projectId : id,
				projectName : name,
				data:{projectId:id ,projectName:name},
				success:function( data ) {
					
					 if("projectId==project_id"|| "projectName==project_name"){
						  toastr.options = {
								 "closeButton": true,
								  "debug": true,
								  "progressBar": true,
								  "preventDuplicates": true,
								  "positionClass": "toast-top-right",
								  "showDuration": "400",
								  "hideDuration": "100",
								  "timeOut": "1500",
								  "extendedTimeOut": "1000",
								  "showEasing": "swing",
								  "hideEasing": "linear",
								  "showMethod": "slideDown",
								  "hideMethod": "slideUp"
					     };
					     toastr.success('',name+" is changed successfully ");
				           window.setTimeout(function(){location.reload()},1600)
					 }else{
						 toastr.options = {
								 "closeButton": true,
								  "debug": true,
								  "progressBar": true,
								  "preventDuplicates": true,
								  "positionClass": "toast-top-right",
								  "showDuration": "400",
								  "hideDuration": "1000",
								  "timeOut": "1500",
								  "extendedTimeOut": "1000",
								  "showEasing": "swing",
								  "hideEasing": "linear",
								  "showMethod": "slideDown",
								  "hideMethod": "slideUp"
					     };
					     toastr.error('',"Failed to select application");
			           	 }    
				}
			}); 
		}
		$("#helpDiv").click(function() {

			$("#bar").fadeIn();
			$("#divContent").addClass("hidden");

			window.location.href = "help";
		});

		$("#sfdcDiv").click(function() {

			$("#bar").fadeIn();
			$("#divContent").addClass("hidden");

			window.location.href = "sfdcdashboard";
		});
		
		function gotoLink(link)//link
		{
			if( link == 'executebuild')
				sessionStorage.setItem("executionType",'executebuild');
			else if( link == 'autoexecute')
				sessionStorage.setItem("executionType",'autoexecute');
			
// 			$('body').addClass("white-bg");
// 			$("#barInMenu").removeClass("hidden");
// 			$("#wrapper").addClass("hidden");	
			window.location.href = link;
			
			
			
		}
	</script>
<style>
.main-page-logout{
display: inline-block;
    margin-top: 15px;
    margin-right: 33px;
}
</style>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="modal inmodal" id="assignModel" tabindex="-1" role="dialog"
	aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-sm">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<!-- <span aria-hidden="false">�</span><span class="sr-only">Close</span> -->
				</button>
				<h6 class="modal-title">Set Default Application</h6>
				<small>Please select default for Test Manager and Bug
					Tracker. </small>
			</div>
			<div class="modal-body" style="padding: 5px 5px 5px 5px;">
				<div class="row">
					<div class="col-lg-8" align="center"
						style="margin: auto; float: none;">
						<div class="dropdown" style="width: 100%">
							<button class="btn btn-primary dropdown-toggle" type="button"
								data-toggle="dropdown">
								<span id="assignCurrent"
									name="${Model.projectList[0].project_id}">${Model.projectList[0].project_name}</span>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" style="position: absolute; width: 100%">

								<c:forEach var="projectList" items="${Model.projectList}">
									<li id="${projectList.project_id}"><a href="#"
										onclick="setCurrentProject('${projectList.project_id}','${projectList.project_name}')">${projectList.project_name}</a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>


			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal"
					id="inserrtProject">Save changes</button>
			</div>
		</div>
	</div>
</div>


<div class="sweet-overlay" tabindex="-1"
	style="opacity: -0.06; display: none;"></div>




<div class="sweet-alert hideSweetAlert" data-custom-class=""
	data-has-cancel-button="false" data-has-confirm-button="true"
	data-allow-outside-click="false" data-has-done-function="false"
	data-animation="pop" data-timer="null"
	style="display: none; margin-top: -170px; opacity: -0.06;">
	<div class="sa-icon sa-error" style="display: none;">
		<span class="sa-x-mark"> <span class="sa-line sa-left"></span>
			<span class="sa-line sa-right"></span>
		</span>
	</div>
	<div class="sa-icon sa-warning" style="display: none;">
		<span class="sa-body"></span> <span class="sa-dot"></span>
	</div>
	<div class="sa-icon sa-info" style="display: none;"></div>
	<div class="sa-icon sa-success" style="display: block;">
		<span class="sa-line sa-tip"></span> <span class="sa-line sa-long"></span>

		<div class="sa-placeholder"></div>
		<div class="sa-fix"></div>
	</div>
	<div class="sa-icon sa-custom" style="display: none;"></div>
	<h2>Good job!</h2>
	<p style="display: block;">You clicked the button!</p>
	<fieldset>
		<input type="text" tabindex="3" placeholder="">
		<div class="sa-input-error"></div>
	</fieldset>
	<div class="sa-error-container">
		<div class="icon">!</div>
		<p>Not valid!</p>
	</div>
	<div class="sa-button-container">
		<button class="cancel" tabindex="2"
			style="display: none; box-shadow: none;">Cancel</button>
		<button class="confirm" tabindex="1"
			style="display: inline-block; box-shadow: rgba(174, 222, 244, 0.8) 0px 0px 2px, rgba(0, 0, 0, 0.0470588) 0px 0px 0px 1px inset; background-color: rgb(174, 222, 244);">OK</button>
	</div>
</div>
<div class="bar" id="bar">
	<p id="loading_text">loading</p>
</div>
<div class="wrapper hidden" id="divContent">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="col-lg-3">
						<div class="widget-head-color-box yellow-bg p-lg text-center">
							<div class="m-b-md">
								<a href="#" style="color: #ffffff">
									<h2 class="font-bold no-margins"
										onclick="checkAccess('tmdashboard','Xenon Test Manager')">Xenon
										Test Manager</h2>
								</a>
							</div>
							<img style="height: 120px; width: 120px;"
								src="data:image/jpg;base64,${Model.userPhoto } "
								class="img-circle circle-border m-b-md" alt="profile">

						</div>
						<div class="widget-text-box text-center">
							<h4 class="media-heading">Xenon TM</h4>

						</div>
					</div>

					<div class="col-lg-3">
						<div class="widget-head-color-box red-bg p-lg text-center">
							<div class="m-b-md">
								<a href="#" style="color: #ffffff"><h2
										class="font-bold no-margins"
										onclick="checkAccess('btdashboard','Xenon Bug Tracker')">Xenon
										Bug Tracker</h2></a>
							</div>
							<img style="height: 120px; width: 120px;"
								src="data:image/jpg;base64,${Model.userPhoto } "
								class="img-circle circle-border m-b-md" alt="profile">

						</div>
						<div class="widget-text-box text-center">
							<h4 class="media-heading">Xenon BT</h4>

						</div>
					</div>

					<div class="col-lg-3">
						<div class="widget-head-color-box navy-bg p-lg text-center">
							<div class="m-b-md">
								<a href="#" onclick="gotoLink('bmdashboard')"
									style="color: #ffffff"><h2 class="font-bold no-margins">Xenon
										Test Plan Manager</h2></a>
							</div>
							<img style="height: 120px; width: 120px;"
								src="data:image/jpg;base64,${Model.userPhoto } "
								class="img-circle circle-border m-b-md" alt="profile">

						</div>
						<div class="widget-text-box text-center">
							<h4 class="media-heading">Xenon BM</h4>

						</div>
					</div>


					<div class="col-lg-3">
						<div class="widget-head-color-box lazur-bg p-lg text-center">
							<div class="m-b-md">
								<a href="#" onclick="gotoLink('coredashboard')"
									style="color: #ffffff"><h2 class="font-bold no-margins">Xenon
										Core Manager</h2></a>
							</div>
							<img style="height: 120px; width: 120px;"
								src="data:image/jpg;base64,${Model.userPhoto } "
								class="img-circle circle-border m-b-md" alt="profile">

						</div>
						<div class="widget-text-box text-center">
							<h4 class="media-heading">Xenon Core</h4>

						</div>
					</div>
					<div class="col-lg-3">
						<div class="widget-head-color-box blue-bg p-lg text-center">
							<div class="m-b-md">
								<a href="#" onclick="gotoLink('admindashboard')"
									style="color: #ffffff"><h2 class="font-bold no-margins">Administration</h2></a>
							</div>
							<img style="height: 120px; width: 120px;"
								src="data:image/jpg;base64,${Model.userPhoto } "
								class="img-circle circle-border m-b-md" alt="profile">

						</div>
						<div class="widget-text-box text-center">
							<h4 class="media-heading">Xenon Core</h4>

						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	var image = '${Model.customerLogo}';
	localStorage.setItem("customerLogo", image);

	var tmStatus = '${Model.tmStatus}';
	var btStatus = '${Model.btStatus}';
	var coreStatus = '${Model.coreStatus}';
	var adminStatus = '${Model.adminStatus}';
	
	localStorage.setItem("tmStatus", tmStatus);
	localStorage.setItem("btStatus", btStatus);
	localStorage.setItem("coreStatus", coreStatus);
	localStorage.setItem("adminStatus", adminStatus);
	
</script>
<script type="text/javascript">
	$(window).load(function() {
		$("#bar > p").text('Loding Dashboard');
		$("#bar").fadeOut();
		$("#divContent").removeClass("hidden");
	})
</script>
<script type="text/javascript">
	var moduleLink;
	function checkAccess(link, dashText) {
		var size = ${Model.projectList.size()};
		moduleLink = link;

		var assignStatus = '${Model.projectList[0].AssignProject}';

		if (assignStatus == "False") {
			swal({
				title : "You need a minimum 1 application to access "
						+ dashText,
				text : "Please contact administrator."
			});
		} else if (assignStatus == "True") {
			$('#assignModel').modal('show');

		} else {
			$("#bar").fadeIn();
			$("#divContent").addClass("hidden");
			window.location.href = link;
		}
	}

	$('#inserrtProject').click(function() {

		var id = $('#assignCurrent').attr("name");
		var name = $('#assignCurrent').val();

		var posting = $.post('insertCurrentProject', {
			projectId : id,
			projectName : name
		});

		posting.done(function(data) {
			$("#bar").fadeIn();
			$("#divContent").addClass("hidden");
			window.location.href = moduleLink;
		});

	});

	$('.dropdown').click(function() {
		$(this).toggleClass("open");
	});
	function setCurrentProject(id, name) {

		$('#assignCurrent').text(name);
		$('#assignCurrent').attr("name", id);
	}

	function gotoLink(link) {
		$("#bar").fadeIn();
		$("#divContent").addClass("hidden");
		window.location.href = link;
	}
</script>





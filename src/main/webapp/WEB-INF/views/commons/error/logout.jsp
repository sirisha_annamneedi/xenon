<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Logout</title>
<link
	href="<%=request.getContextPath()%>/resources/css/xenon_custom.css"
	rel="stylesheet">
</head>
<body>

	<div id="spinner" class="bar"></div>

<script src="<%=request.getContextPath()%>/resources/js/jquery-2.1.1.js"></script>

<script type="text/javascript">
	$(window).load(function() {
		var currentState = history.state;
		var url = window.location.href;
		window.history.go(-(window.history.length)); 
		window.location.href = url;
	});
</script>

</body>
</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div class="middle-box text-center animated fadeInDown">
	<h1>404</h1>
	<h3 class="font-bold">Page Not Found</h3>
	<div class="row border-bottom">
		<div class="error-desc">
			Sorry, but the page you are looking for has not been found. Try
			checking the URL for error, then hit the refresh button on your
			browser or try found something else in our app.
			<br /> <a href="xedashboard" class="btn btn-success m-t">Dashboard</a>
		</div>
	</div>
</div>
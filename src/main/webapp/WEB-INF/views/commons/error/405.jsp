<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div class="middle-box text-center animated fadeInDown">
	<h1>405</h1>
	<h3 class="font-bold">Method Not Allowed</h3>
	<div class="row border-bottom">
		<div class="error-desc">
			Sorry, but the page you are looking for has been restricted for POST request. Try
			checking the URL for error, then hit the refresh button on your
			browser or try found something else in our application.
			<br /> <a href="xedashboard" class="btn btn-success m-t">Dashboard</a>
		</div>
	</div>
</div>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div class="middle-box text-center animated fadeInDown">
	<h1>401</h1>
	<h3 class="font-bold">Access denied</h3>
	<div class="row border-bottom">
		<div class="error-desc">
			You do not have enough privileges to perform requested operation. 
			<br /> <a href="xedashboard" class="btn btn-success m-t">Dashboard</a>
		</div>
	</div>
</div>
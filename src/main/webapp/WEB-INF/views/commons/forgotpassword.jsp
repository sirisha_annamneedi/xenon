<div class="bar" id="barInMenu" class="">
	<p>loading</p>
</div>
<div class="wrapper passwordBox animated fadeInDown hidden" id="wrapper">
	<div class="row">
		<div class="col-md-12">
			<div id="success" class="alert alert-success alert-dismissable hidden">
				<button aria-hidden="true" data-dismiss="alert" class="close"
					type="button">�</button>
				"Mail Sent Successfully"
			</div>
			<div id="failure" class="alert alert-danger alert-dismissable  hidden">
				<button aria-hidden="true" data-dismiss="alert" class="close"
					type="button">�</button>
				"Send Mail Unsuccessful"
			</div>
		</div>
		
	</div>
	<div class="row">
            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold">Forgot password</h2>

                    <p>
                        Enter the email address associated with your account to reset password.
                    </p>

                    <div class="row">

                        <div class="col-lg-12">
                            <form class="m-t" role="form" action="setforgotpassword" method="post" id="forgetPassForm">
                                <div class="form-group"> 
                                    <input type="email" class="form-control" placeholder="Email address" name="emailId" required="" aria-required="true">
                                </div>

                                <button class="btn btn-success ladda-button ladda-button-demo pull-left block full-width m-b" data-style="slide-up" id="sendMailBtn">Send new password</button>

                            </form>
                             <a href="./" class="pull-right">Click here to Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-8">
                <strong>Copyright</strong> &copy;Jade Global Inc.
            </div>
            <div class="col-md-4 text-right">
               <small>&copy;2019</small>
            </div>
        </div>
    </div>
<input type="hidden" id="hiddenInput"
	value=<%=session.getAttribute("SendMail")%>>
 <!-- Mainly scripts -->
    <script src="<%=request.getContextPath()%>/resources/js/jquery-2.1.1.js"></script>
    <script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
 <!-- Jquery Validate -->
    <script src="<%=request.getContextPath()%>/resources/js/plugins/validate/jquery.validate.min.js"></script>
<script>
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});
var status=-1;
	$(function(){
		var state = $('#hiddenInput').val();
		if(state == 1){
			swal({
		        title: "Email sent successfully",
		        type: "success",
		        showCancelButton: false,
		        confirmButtonColor: "#DD6B55",
		        confirmButtonText: "Ok",
		        closeOnConfirm: false
		    }, function () {
		        window.location.href="./";
		    });
		}
		if(state == 0){
			swal({
		        title: "Send email unsuccessful",
		        type: "error",
		        showCancelButton: false,
		        confirmButtonColor: "#DD6B55",
		        confirmButtonText: "Ok",
		        closeOnConfirm: false
		    }, function () {
		        window.location.href="forgotpassword";
		    });
		}
		if(state == 2){
			swal({
		        title: "User email is invalid",
		        type: "error",
		        showCancelButton: false,
		        confirmButtonColor: "#DD6B55",
		        confirmButtonText: "Ok",
		        closeOnConfirm: false
		    }, function () {
		        window.location.href="forgotpassword";
		    });
			
		}
		
	});
	
	$("#sendMailBtn").click(function(){
		var formStat = $("#forgetPassForm").valid();
		if(formStat)
		   {
			   $('body').addClass("white-bg");
			   $("#barInMenu").removeClass("hidden");
			   $("#wrapper").addClass("hidden");
			   $(this).attr("disabled","true");
				$('#forgetPassForm').submit();
				$(this).removeAttr("disabled");
		   }
		
	});
	
	
</script>
<%
session.setAttribute("SendMail","-1");
%>
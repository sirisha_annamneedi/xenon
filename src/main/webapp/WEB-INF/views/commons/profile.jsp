<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="bar" id="barInMenu">
  <p>loading</p>
</div>
<div id="wrapper">
<div class="row border-bottom white-bg page-heading">
	<div class="col-lg-12">
		<h2>Profile</h2>
		<ol class="breadcrumb">
			<li><a href="xedashboard">Home</a></li>
			<li><strong class="active">Profile</strong></li>
		</ol>
		<!-- end breadcrumb -->

	</div>
	<!-- end col -->
</div>
<!-- end row -->
<div class="wrapper wrapper-content">
	<div class="row animated fadeInRight">
		<div class="col-lg-offset-4 col-lg-8">
			<div class="alert alert-success hidden" id="successAlert">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">�</button>
				<h4>Password is successfully updated.</h4>
			</div>
			<div class="alert alert-danger hidden " id="errorAlert">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">�</button>
				<h4>Reset password failed <br/>Please enter correct current password.</h4>
			</div>
		</div>
	</div>
	<div class="row animated fadeInRight">
		<div class="col-lg-4" id="profileDetailsDiv">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Profile Details</h5>
				</div>
				<div class="ibox-content no-padding border-left-right">
					<img alt="Profile photo"
						src="data:image/jpg;base64,${userProfileModel.userDetails[0].photo}"
						class="img-responsive" style="display: block;
    margin: 0 auto;">
				</div>
				<!-- end ibox-content -->
				<div class="ibox-content profile-content">
					<h4>
						<strong>${userProfileModel.userDetails[0].first_name} ${userProfileModel.userDetails[0].last_name}</strong>
					</h4>
					<p>
						<i class="fa fa-map-marker"></i>
						${userProfileModel.userDetails[0].location}
					</p>
					<h5>About me</h5>
					<p>${userProfileModel.userDetails[0].about_me}</p>
				</div>
				<!-- end ibox-content -->
			</div>
			<!-- end ibox float-e-margins -->
		</div>
		<!-- end #profileDetailsDiv -->
		<div class="col-lg-8" id="profileManagementDiv">
			<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab"
						href="#profileManagementTab">Profile Management </a></li>
					<li class=""><a data-toggle="tab"
						href="#passwordManagementTab"> Password Management </a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="profileManagementTab">
						<div class="panel-body">
							<div class="row">
								<label class="col-lg-4 text-right">* fields are mandatory</label>
							</div>
							<form class="form-horizontal" id="updateUserDetailsForm"
								enctype="multipart/form-data" action="updateprofile"
								method="POST">
								<%-- <div class="form-group hidden">
											<div class="col-lg-6">
												<input type="text" class="form-control" value="${userProfileModel[0].user_password}" id="userCheck">
											</div>
										</div> --%>


								<div class="form-group">
									<label class="control-label col-lg-2">First Name*:</label>
									<div class="col-lg-6">
										<input type="text" class="form-control"
											value="${userProfileModel.userDetails[0].first_name}"
											placeholder="First Name" name="firstName" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Last Name*:</label>
									<div class="col-lg-6">
										<input type="text" class="form-control"
											value="${userProfileModel.userDetails[0].last_name}"
											placeholder="Last Name" name="lastName" disabled>
									</div>
								</div>
								<div class="form-group" id="location">
									<label class="control-label col-lg-2">Location:</label>
									<div class="col-lg-6">
										<input type="text" class="form-control" name="location"
											value="${userProfileModel.userDetails[0].location}" disabled>
									</div>
								</div>
								<div class="form-group" id="aboutMe">
									<label class="control-label col-lg-2">About Me:</label>
									<div class="col-lg-8">
										<textarea class="form-control" rows="5" name="aboutMe"
											style="max-width: 100%;" disabled>${userProfileModel.userDetails[0].about_me}</textarea>
									</div>
								</div>
								<div class="form-group" id="timezoneDiv">
								<label class="control-label col-lg-2">Timezone:</label>
								<div class="col-lg-6">
								<select data-placeholder="Select"
													class="chosen-select" style="width: 350px;" tabindex="2"
													name="timezone" id="timezone" disabled>
												 <c:forEach var="timezoneItem" items="${userProfileModel.timezoneDetails}">
														<option name="timezoneOption" value="${timezoneItem.xe_timezone_id}" ${timezoneItem.xe_timezone_id == userProfileModel.userDetails[0].user_timezone ? 'selected="selected"' : ''}>${timezoneItem.timezone_name}</option>
													</c:forEach> 
													
												</select>
								</div>
								</div>
								<div class="form-group hidden" id="uploadImageDiv">
									<label class="control-label col-lg-2">Profile Photo:</label>
									<div class="fileupload fileupload-new col-lg-6"
										data-provides="fileupload">
										<span class="btn btn-success btn-file"><span
											class="fileupload-new">Upload Photo</span> <span
											class="fileupload-exists">Change</span> <input type="file"
											accept=".png,.jpg"  id="uploadImage" name="uploadImage" /></span> <span
											class="fileupload-preview"></span> <a href="#"
											class="close fileupload-exists" data-dismiss="fileupload"
											style="float: none">�</a>
											 <label
											class="error hidden" id="uploadSizeError">Please upload file with size less than 500 KB</label>
											 <label
											class="error hidden" id="uploadTypeError">Please upload only PNG/JPG file</label>
									</div>
								</div>
								<div class="row">
								<div class="col-lg-6">
									<button class="btn btn-success" type="button"
										id="editUserDetailsBtn">Edit</button>
									<button class="btn btn-white hidden" type="button"
										style="margin-right: 10px;" id="cancelUserDetailsBtn">Cancel</button>
									<button
										class="btn btn-success hidden ladda-button ladda-button-demo"
										id="submitUserDetailsBtn" data-style="slide-up">Submit</button>
								</div>
								</div>
							</form>
						</div>
					</div>
					<div class="tab-pane" id="passwordManagementTab">
						<div class="panel-body">
							<div class="row">
								<label class="col-lg-4 text-right">* fields are mandatory</label>
							</div>
							<div class="row">
							<div class="col-lg-8">
							<form class="form-horizontal" id="updatePasswordForm"
								action="updatepassword" method="POST">
								<div class="alert alert-danger hidden col-lg-10" id="errorMessage">
									<button type="button" class="close" data-dismiss="alert"
										aria-hidden="true">�</button>
									<strong>Please enter same New Password & Retype
										Password.</strong>
								</div>
								<div class="form-group has-feedback">
									<label class="control-label col-lg-3">Current
										Password:*</label>
									<div class="col-lg-7 passClass">
										<input type="password" placeholder="Current password" 
											id="txtPassword" class="form-control" name="password"
											required=required> <span
											class="glyphicon  form-control-feedback"></span> <label
											class="error hidden" id="passwordErrorMsg">This field is required.</label>
									</div>
								</div>
								<div class="form-group has-feedback">
									<label class="control-label col-lg-3">New Password:*</label>
									<div class="col-lg-7 passClass">
										<input type="password" placeholder="New password" onchange="checkPassword('txtNewPassword','newPswdError')"
											class="form-control" id="txtNewPassword" name="newPassword"
											required=required> <span
											class="glyphicon  form-control-feedback"></span> <label
											class="error hidden" id="newPswdError">This field is required.</label>
									</div>
								</div>
								<div class="form-group has-feedback">
									<label class="control-label col-lg-3">Retype Password:*</label>
									<div class="col-lg-7 passClass">
										<input type="password" placeholder="Retype password"
											class="form-control" id="txtConfirmPassword"
											name="retypePassword" required=required> <span
											class="glyphicon  form-control-feedback"></span> <label
											class="error hidden">This field is required.</label>
									</div>
								</div>
								<div class="col-lg-12">
									<button class="btn btn-white" type="button"
										style="margin-right: 10px;" id="passCancelBtn">Cancel</button>
									<button class="btn btn-success ladda-button" type="button"
										id="passSubmitBtn" data-style="slide-up" disabled>Submit</button>
								</div>
							</form>
							</div>
							<div class="col-lg-4">
								<h3>The password should : </h3>
								<ul class="nav nav-third-level">
									<li>Be 8 - 16 characters long</li>
									<li>Include at least 1 uppercase character (A,B,C)</li>
									<li>Include at least 1 lowercase character (a,b,c)</li>
									<li>Include at least 1 number (1,2,3)</li>
									<li>Include at least 1 special character ($,#,@)</li>
								</ul>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end tabs-container -->
		</div>
		<!-- end #profileManagementDiv -->
	</div>
</div>
<!-- end wrapper -->
</div>
<style>
.error {
	color: #8a1f11;
	margin-left: 1.5em;
	font-weight: bold;
}
</style>
<input type="hidden" id="hiddenPassStatus"
	value='<%=session.getAttribute("passUpdateStatus")%>'>
<script>
	$(window).load(function() {
		$("#barInMenu").fadeOut();
		var config = {
				'.chosen-select' : {},
				'.chosen-select-deselect' : {
					allow_single_deselect : true
				},
				'.chosen-select-no-single' : {
					disable_search_threshold : 10
				},
				'.chosen-select-no-results' : {
					no_results_text : 'Oops, nothing found!'
				},
				'.chosen-select-width' : {
					width : "95%"
				}
			}
			for ( var selector in config) {
				$(selector).chosen(config[selector]);
			}
		
	});
	//var l = $('.ladda-button-demo').ladda();
	$("#submitUserDetailsBtn").click(function() {
		$("#uploadSizeError").addClass('hidden');
		$("#uploadTypeError").addClass('hidden');
		var formValid = $("#updateUserDetailsForm").valid();
		if(formValid){
			 $("#barInMenu").fadeIn();
			 $("#wrapper").addClass("hidden");
			 $('#updateUserDetailsForm').submit();
		}
	});
	$('#uploadImage').bind('change', function(e) {
		  $("#uploadSizeError").css("display","none");
		  $("#uploadTypeError").css("display","none");
		  var fsize = 0;
			var filetype = 1;
			if($('#uploadImage')[0].files[0]){
				file = $('#uploadImage')[0].files[0];
		       fsize = file.size;
		       var fileName = file.name;
	          var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
	          if(!(fileNameExt.toLowerCase() == "png" || fileNameExt.toLowerCase() == "jpg"))
	        	  {
	        	  filetype = 0;
	        	  }
			}
			if(filetype == 0){
				$("#uploadTypeError").removeClass('hidden');
				$("#uploadTypeError").css("display","");
				 $("#uploadTypeError").text("Image type must be png or jpg");
				 $("#barInMenu").fadeOut();
				 $("#wrapper").removeClass("hidden");
				 $("#submitUserDetailsBtn").prop("disabled",true);
				return false;
			}
			else if(fsize>512000) //do something if file size more than 1 mb (1048576)
	        {
	        	$("#uploadSizeError").removeClass('hidden');
	        	$("#barInMenu").fadeOut();
				 $("#wrapper").removeClass("hidden");
				 $("#uploadSizeError").css("display","");
				 $("#uploadSizeError").text("Size can not be greater than 500 KB");
				 $("#submitUserDetailsBtn").prop("disabled",true);
	        	return false;
	        }else{
	        	name = $("#uploadImage").attr("name");
	        	if(name != "uploadImage")
	        		{
	        		$("#uploadImage").attr("name","uploadImage");
	        		}
	        	 $("#submitUserDetailsBtn").prop("disabled",false);
	        }
		
		});
	$(document).ready(function() {
		//password alerts.
		var status = $('#hiddenPassStatus').val();
		if(status == 1){
			$('#successAlert').removeClass('hidden');
		}
		else if(status == 2){
			$('#errorAlert').removeClass('hidden');
		}
		$('#updateUserDetailsForm').validate({
			rules : {
				firstName : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				lastName : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				userEmail : {
					required : true
				},
				location :{
					minlength : 1,
					maxlength : 100
				},
				aboutMe :{
					minlength : 1,
					maxlength : 1000
				}
			}
		});

		$('#editUserDetailsBtn').click(function() {
			$('input[name="firstName"]').removeAttr('disabled');
			$('input[name="lastName"]').removeAttr('disabled');
			$('input[name="userEmail"]').removeAttr('disabled');
			$('#cancelUserDetailsBtn').removeClass('hidden');
			$('#submitUserDetailsBtn').removeClass('hidden');
			$('#uploadImageDiv').removeClass('hidden');
			$('#editUserDetailsBtn').addClass('hidden');
			$('#timezone').removeAttr('disabled').trigger('chosen:updated');
			$('input[name="location"]').removeAttr('disabled');
			$('textarea[name="aboutMe"]').removeAttr('disabled');
		});
		$('#cancelUserDetailsBtn').click(function() {
			window.location.href = "profile";
		});
		$('#passCancelBtn').click(function() {
			window.location.href = "profile";
		});
	});

	$('#passSubmitBtn')
			.click(
					function() {
						$("#errorMessage").addClass('hidden');
						$("#errorAlert").addClass('hidden');
						$('#successAlert').addClass('hidden');
						//check validation's
						var formValid = true;
						$("#updatePasswordForm [required=required]")
								.each(
										function() {
											var formGroup = $(this).parents(
													'.form-group');
											var glyphicon = formGroup
													.find('.glyphicon');
											var errorLabel = formGroup
													.find('.error');
											var colDiv = formGroup
													.find('.passClass');
											// use the html5 checkvalidity function
											if (this.checkValidity()) {
												colDiv.addClass('has-success')
														.removeClass(
																'has-error');
												glyphicon
														.addClass(
																'glyphicon-ok')
														.removeClass(
																'glyphicon-remove');
												errorLabel.addClass('hidden');
											} else {
												colDiv.addClass('has-error')
														.removeClass(
																'has-success');
												glyphicon.addClass(
														'glyphicon-remove')
														.removeClass(
																'glyphicon-ok');
												errorLabel
														.removeClass('hidden');
												formValid = false;
											}
										});
						if (formValid) {
							if ($("#txtNewPassword").val() != $(
									"#txtConfirmPassword").val()) {
								$("#errorMessage").removeClass('hidden');
								$( "#passSubmitBtn" ).prop( "disabled", false );
							}

							else {
								$("#barInMenu").fadeIn();
								 $("#wrapper").addClass("hidden");
								var posting = $
										.post(
												'updatepassword',
												{
													password : $("#txtPassword")
															.val(),
													newPassword : $(
															"#txtConfirmPassword")
															.val()
												},
												function(response) {
													if (response == 1) {
														window.location.href = "profile";
													} else if (response == 2) {
														window.location.href = "profile";
													} else if (response == 3) {
														window.location.href = "500";
																
													}
												});
							}
						}
					});
	
	
	
	function checkPassword(checkId1,msgboxId1) {
		var checkId="#"+checkId1;
		var msgboxId="#"+msgboxId1;
		var errorMsg = "";
		var flag = 0;
		$( "#passSubmitBtn" ).prop( "disabled", true );
		//alert($("#txtPassword").val());
		var pattern1 = /[0-9]/;
		var pattern2 = /[a-z]/;
		var pattern3 = /[A-Z]/;
		var pattern4 = /[!@#\$]/;
		var pattern = /[a-zA-Z0-9]/;
		var password = $(checkId).val();
		if (password.length>7 && password.length<17 ) {

		} else {
			errorMsg += "Password must be 8 - 16 characters long<br>";
			flag = 1;
		}
		if ($(checkId).val().match(pattern1)) {

		} else {
			errorMsg += "Password must contain at least one numeric<br>";
			flag = 1;
		}

		if ($(checkId).val().match(pattern2)) {
		} else {
			errorMsg += "Password must contain at least one a-z<br>";
			flag =1;
		}
		if ($(checkId).val().match(pattern3)) {
		} else {
			errorMsg += "Password must contain at least one A-Z<br>";
			flag = 1;
		}
		if ($(checkId).val().match(pattern4)) {
		} else {
			errorMsg += "Password must contain at least one special symbol from !@#$<br>";
			flag = 1;
		}
		
		if (flag == 1) {
			//$(msgboxId).html("Password must contain at least one A-Z and  a-z  and any one special character from !@#$");
			$(msgboxId).html(errorMsg);
			$(msgboxId).removeClass("hidden");
			
		}
		else
			{
			$(msgboxId).html("This field is required");
			$(msgboxId).addClass("hidden");
			$( "#passSubmitBtn" ).prop( "disabled", false );
			}
	}
</script>

<%
	//set session variable to another value
	session.setAttribute("passUpdateStatus",11);
%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Cache-control" content="private">

<link rel="icon" type="image/png"
	href="<%=request.getContextPath()%>/resources/img/xenon.png">

	
	<link
	href="<%=request.getContextPath()%>/resources/css/xenon_custom.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/font-awesome/css/font-awesome.css"
	rel="stylesheet">

<link
	href="<%=request.getContextPath()%>/resources/css/plugins/sweetalert/sweetalert.css"
	rel="stylesheet">

<link
	href="<%=request.getContextPath()%>/resources/css/plugins/select2/select2.min.css"
	rel="stylesheet">
<!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/css/bootstrap.min.css" rel="stylesheet"> -->
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/profile/profile.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/steps/jquery.steps.css"
	rel="stylesheet">

<!-- Ladda style -->
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/ladda/ladda-themeless.min.css"
	rel="stylesheet">
	 <link href="<%=request.getContextPath()%>/resources/css/plugins/chosen/chosen.css" rel="stylesheet">
	
<!-- Toastr style -->
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/toastr/toastr.min.css"
	rel="stylesheet">

<link href="<%=request.getContextPath()%>/resources/css/animate.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/resources/css/style.css"
	rel="stylesheet">
<title><tiles:insertAttribute name="title" ignore="true" /></title>

</head>

<body class="top-navigation  pace-done dashboard-bg">

	<!-- Mainly scripts -->
	<script
		src="<%=request.getContextPath()%>/resources/js/jquery-2.1.1.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

	<!-- Custom and plugin javascript -->
	<script src="<%=request.getContextPath()%>/resources/js/inspinia.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/plugins/pace/pace.min.js"></script>

	<!-- Jquery Validate -->
	<script
		src="<%=request.getContextPath()%>/resources/js/plugins/validate/jquery.validate.min.js"></script>

	<script
		src="<%=request.getContextPath()%>/resources/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/plugins/select2/select2.full.min.js"></script>


	<script
		src="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/js/bootstrap.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/plugins/profile/profile.js"></script>
	<!-- Jquery Validate -->
	<script
		src="<%=request.getContextPath()%>/resources/js/plugins/validate/jquery.validate.min.js"></script>

	<!-- Ladda -->
	<script
		src="<%=request.getContextPath()%>/resources/js/plugins/ladda/spin.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/plugins/ladda/ladda.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/plugins/ladda/ladda.jquery.min.js"></script>

	<!-- Toastr script -->
	<script
		src="<%=request.getContextPath()%>/resources/js/plugins/toastr/toastr.min.js"></script>
<!-- Chosen -->
    <script src="<%=request.getContextPath()%>/resources/js/plugins/chosen/chosen.jquery.js"></script>

	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="body" />
	<tiles:insertAttribute name="footer" />
</body>

</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="bar" id="barInMenu" class="">
	<p>loading</p>
</div>

<div id="wrapper">
	<nav class="navbar-default navbar-static-side" role="navigation"> <!-- style="min-height: 100%;" -->
		<div class="sidebar-collapse">
			<ul class="nav metismenu" id="side-menu">
				<li class="nav-header">
					<div class="dropdown profile-element">
						<span> <img alt="image" class="img-circle"
							style="width: 48px; height: 48px"
							src="data:image/jpg;base64,${Model.UserProfilePhoto }" />
						</span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
							class="clear"> <span class="block m-t-xs"> <strong
									class="font-bold">${Model.userName }</strong>
							</span>
						</span>
						</a> <span class="text-muted text-xs block">${Model.role}<b
							class=""></b></span>
					</div>
					<div class="logo-element" style="padding: 14px !important">
						<a href="#" onclick="gotoLinkInMenu('xedashboard')">
						<img alt="image" class=""
							src="<%=request.getContextPath()%>/resources/img/xenon.png" />
						</a>
					</div>
				</li>

				<li><a href="#" onclick="gotoLinkInMenu('help');"><i class="fa fa-diamond"></i> <span
						class="nav-label">DashBoard</span></a></li>
							
				<c:forEach var="subcomponentList" items="${Model.subcomponentList}">
					<li onclick="viewComponent(${subcomponentList.id})"><a
						href="#"><i class="fa fa-diamond"></i> <span
							class="nav-label">${subcomponentList.subcmp_name}</span></a></li>
				</c:forEach>

			</ul>

		</div>
	</nav>

<script type="text/javascript">
	function viewComponent(id) {

		$("#barInMenu").removeClass('hidden');
		$('body').addClass('white-bg');
		$("#wrapper").addClass("hidden");
		
		var posting = $.post('setSubcomponentId', {
			subcomponentId : id,
		});

		posting.done(function(data) {
			window.location.href = "subcomponent";
		});
	}
	
	function gotoLinkInMenu(link)
	{
		$("#barInMenu").removeClass('hidden');
		$('body').addClass('white-bg');
		$("#wrapper").addClass("hidden");
		window.location.href = link; 
	}
</script>
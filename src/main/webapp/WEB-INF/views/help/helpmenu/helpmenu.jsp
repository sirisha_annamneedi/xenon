<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="bar" id="barInMenu" class="">
	<p>loading</p>
</div>

<div id="wrapper" class="hidden ">
	<nav class="navbar-default navbar-static-side " role="navigation" > <!-- style="min-height: 100%; background-color: transparent;" -->
		<div class="sidebar-collapse">
			<ul class="nav metismenu" id="side-menu">
				<li class="nav-header">
					<div class="dropdown profile-element">
						<span> <img alt="image" class="img-circle"
							style="width: 48px; height: 48px"
							src="data:image/jpg;base64,${Model.UserProfilePhoto }" />
						</span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
							class="clear"> <span class="block m-t-xs"> <strong
									class="font-bold">${Model.userName }</strong>
							</span>
						</span>
						</a> <span class="text-muted text-xs block">${Model.role}<b
							class=""></b></span>
					</div>
					<div class="logo-element" style="padding: 14px !important">
						<a href="#" onclick="gotoLinkInMenu('xedashboard')">
						<img alt="image" class=""
							src="<%=request.getContextPath()%>/resources/img/xenon.png" />
						</a>
					</div>
				</li>

				<li><a s onclick="gotoLinkInMenu('help')"><i
						class="fa fa-diamond"></i> <span class="nav-label">DashBoard</span></a></li>

				<c:forEach var="helpMenu" items="${Model.helpMenu}">
					<li class="dashboardNameMenu" componentId="${helpMenu.id}"><a
						href="#"><i class="fa fa-diamond"></i> <span class="nav-label">${helpMenu.cmp_name}</span></a></li>
				</c:forEach>
				<li><a href="#" onclick="gotoLinkInMenu('viewfaq')"><i
						class="fa fa-diamond"></i> <span class="nav-label">FAQ</span> </a></li>

			</ul>

		</div>
	</nav>
	
<script type="text/javascript">

	function gotoLinkInMenu(link)//link
	{
		$('body').addClass('white-bg');
		$("#barInMenu").removeClass('hidden');
		$("#wrapper").addClass("hidden");
		window.location.href = link; 
	}
	
	$('.dashboardNameMenu').click(function(){
		
		$('body').addClass('white-bg');
		$("#barInMenu").removeClass('hidden');
		$("#wrapper").addClass("hidden");

		var value = $(this).attr('componentId');
		var posting = $.post('setcomponentid', {
 			componentId : value
 		});
		
 		 posting.done(function(data) {
 			window.location.href = "helpdesk"; 
 		}); 
	});
</script>
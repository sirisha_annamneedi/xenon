<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Create FAQ</h2>
		<ol class="breadcrumb">
			<li><a href="help">Dashboard</a></li>
			<li class="active"><strong>New FAQ</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form action="insertfaq" method="POST" id="faqForm"
						class="wizard-big wizard clearfix form-horizontal" enctype="multipart/form-data">
						<div class="content clearfix">
							<fieldset class="body current">
								<label class="col-lg-2 pull-right">* fields are
									mandatory</label><br>
								<div class="row">
									<div class="col-lg-10">
										<div class="form-group">
											<label class="control-label col-sm-2">Title:*</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Title" class="form-control characters"
													name="questionTitle" tabindex="1">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label"> Description:</label>
											<div class="col-sm-10">
											<!-- <textarea class="form-control" name="questionDescription">
											</textarea> -->
												<div class="ibox-content no-padding">
													<textarea class="summernote" name="questionDescription">
						
													</textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-white pull-left" id="createFAQCancel"
										style="margin-right: 15px;" type="button" tabindex="5">Cancel</button>
									<button
										class="btn btn-success pull-left ladda-button ladda-button-demo"
										style="margin-right: 15px;" type="button"
										data-style="slide-up" tabindex="6" id="submitBtn">Submit</button>
								</div>
							</div>
						</div>
					</form>
					<!-- end form -->
				</div>
				<!-- end ibox-content -->

			</div>
			<!-- end ibox -->
		</div>
		<!-- end col -->
	</div>
	<!-- end row -->
</div>
<!-- end wrapper -->
</div> <!-- end .md-skin -->
<script type="text/javascript">

$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});

$(document).ready(function(){
	
    $('.summernote').summernote();
    
    $("#faqForm").validate({
		rules : {
			questionTitle : {
				required : true,
				minlength : 1,
				maxlength : 400
			}
		}
	});
    
    $('#submitBtn').click(function(){
		var formValid = $("#faqForm").valid();
		if(formValid){	
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");
		 	
			$('.summernote').each( function() {
				$(this).val($(this).code());
			});
			
			$('#faqForm').submit();
		}
    });
    
});

</script>


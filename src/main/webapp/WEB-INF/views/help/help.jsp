
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading ">
	<div class="col-lg-8">
		<h2>Help</h2>
		<ol class="breadcrumb">
		<li><a href="help">Dashboard</a></li>
			<li class="active"><strong>Help</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content ">
	<div class="row animated fadeInRight">
		<c:forEach var="data" items="${Model.componentData}">
		<div class="col-lg-3">
			<div class="ibox">
				<div class="ibox-content">
					<a href="#" class="btn-link">
						<h2 class="dashboardName" componentId ="${data.id}">${data.cmp_name}</h2>
					</a>
					<p>${data.cmp_desc}</p>
				</div>
			</div>
		</div>
		</c:forEach>
	</div>

	<div class="row animated fadeInRight">
		<div class="col-md-12">
			<h2>FAQ</h2>
			 <c:if test="${Model.createFaq==1}">
			<div class="ibox">
				<div class="ibox-title border-bottom">Recent FAQ</div>
				<div class="ibox-content">
					<div class="text-center p-lg">
						<h2>If you don't find the answer to your question</h2>
						<span>add your question by selecting </span>
						
						<a href="addfaq">
						<button title="Create new cluster" class="btn btn-primary btn-sm">
							<i class="fa fa-plus"></i> <span class="bold">Add question</span>
						</button>
						</a> 
						button
					</div>
				</div>
			</div>
			</c:if>
			<c:forEach var="data" items="${Model.faqData}">
						<div class="faq-item">
							<div class="row">
								<div class="col-md-7">
									<a data-toggle="collapse" href="#${data.faq_id}"
										class="faq-question"> ${data.title} </a> 
										<small><%-- Added by <strong>${data.faq_creator}</strong> --%> 
										<i class="fa fa-clock-o"></i> ${data.create_date}
										 <%-- <fmt:formatDate type="both" dateStyle="medium" timeStyle="medium" value="${data.create_date}" /> --%>
									</small>
								</div>
								<div class="col-md-3">
									<span class="small font-bold">Robert Nowak</span>
									<div class="tag-list">
										<span class="tag-item">General</span> <span class="tag-item">License</span>
									</div>
								</div>
								<div class="col-md-2 text-right">
									<span class="small font-bold">Likes</span><br /> 42
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div id="${data.faq_id}" class="panel-collapse collapse ">
										<div class="faq-answer">
											<%-- <p>${fn:substring(data.description, 0, 500)}</p> --%>
											<div class="row">
												<div class="col-lg-12">
													<a href="#" class="btn btn-success btn-sm pull-right" 
													onclick="saveFaqID(${data.faq_id})">Know more</a>
												</div>
											</div>
											<p>${data.description}</p>
											<div class="row">
												<div class="col-lg-12">
													<a href="#" class="btn btn-success btn-sm pull-right" 
													onclick="saveFaqID(${data.faq_id})">Know more</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
		</div>
	</div>
</div>
</div> <!-- end .md-skin -->
<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
	
	$('.dashboardName').click(function(){
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");
		
		var value = $(this).attr('componentId');
		var posting = $.post('setcomponentid', {
 			componentId : value
 		});
 		 posting.done(function(data) {
 			window.location.href = "helpdesk"; 
 		}); 
	});
	
	function saveFaqID(faqID){
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");
		
		var posting = $.post('setfaqid', {
			faqId : faqID
 		});
 		 posting.done(function(data) {
 			window.location.href = "faqdetails"; 
 		}); 
	}
</script>
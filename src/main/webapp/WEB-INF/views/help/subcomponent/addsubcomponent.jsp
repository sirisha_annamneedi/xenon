
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Add Subcomponent</h2>
		<ol class="breadcrumb">
			<li><a href="help">Dashboard</a></li>
			<li><a href="helpdesk">${Model.componentName}</a></li>
			<li class="active"><strong>Add Sub-component</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form id="form" class="wizard-big wizard clearfix form-horizontal">
						<div class="content clearfix">
							<fieldset class="body current">
								<label class="col-lg-2 pull-right">* fields are
									mandatory</label><br>

								<div class="alert alert-danger hidden" id="nameError">Sub-component
									name already exists, Please try another.</div>
								<div class="row">
									<div class="col-lg-10">
										<div class="form-group">
											<label class="control-label col-sm-2">Title* :</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Title" class="form-control characters"
													name="subcomponentName" id="subcomponentName" tabindex="1">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label"> Description* :</label>
											<div class="col-sm-10">
												<!-- <textarea class="form-control" name="questionDescription">
											</textarea> -->
												<div class="ibox-content no-padding">
													<textarea class="summernote" id="subcomponentDescription"
														name="subcomponentDescription">
						
													</textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-white pull-left"
										style="margin-right: 15px;" type="button"
										id="createSubcompoCancelBtn" tabindex="6">Cancel</button>
									<button
										class="btn btn-success pull-left ladda-button ladda-button-demo"
										style="margin-right: 15px;" type="button"
										data-style="slide-up" tabindex="7" id="submitBtn">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


</div>


<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
    $(document).ready(function(){
        $('.summernote').summernote();
        
        $("#form").validate({
	        rules: {
	        	subcomponentName: {
	                required: true,
	                minlength: 1,
	                maxlength: 99
	            },
	            subcomponentDescription: {
	            	 required: true,
	                minlength: 1,
	            }
	        }
		
	    });
   });
    
    $('#createSubcompoCancelBtn').click(function(){
		window.location.href="helpdesk";
	});
  
   var l = $('.ladda-button-demo').ladda();
	$("#submitBtn").click(function() {
		  var aHTML = $('.summernote').code();
		var formValid = $("#form").valid();
		if(formValid){
			
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");
			 
			 var posting = $.post('insertsubcomponent',{
					subcomponentName : $("#subcomponentName").val(),
					subcomponentDescription :aHTML ,
					componentId : '1'
				});
			 
			 posting.done(function(data) {
				 if(data==1)
				{
				   window.location.href="helpdesk";
				}
				 else if(data==0)
					{
					 $('body').removeClass("white-bg");
					 $("#barInMenu").addClass("hidden");
					 $("#wrapper").removeClass("hidden");
					 $("#nameError").removeClass("hidden");
					}
				 else
					{
					 window.location.href="";
					}
				  })
		}
	});

</script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>${Model.subcomponentList[0].subcmp_name}</h2>
		<ol class="breadcrumb">
			<li><a href="help">Dashboard</a></li>
			<li><a href="helpdesk">${Model.componentName}</a></li>
			<li class="active"><strong>${Model.subcomponentList[0].subcmp_name}</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Modules</h5>

				</div>
				<div class="ibox-content">

					<button type="button" class="btn btn-success btn-xs"
						style="float: right;" onclick="location.href = 'addarticle';">Add
						Article</button>
					<c:forEach var="subcomponentList" items="${Model.subcomponentList}">

						<div class="table-responsive">
							<table class="table table-striped articleList">
								<tbody>
									<c:forEach var="articleList" items="${Model.articleList}">
										<c:if test="${subcomponentList.id == articleList.subcomponent_id}">
											<tr>
												<td><a href="#"
													onclick="viewArticle(${articleList.id})">${articleList.art_name }</a></td>
												<td style="float: right; margin-right: 5%">Last Updated
													: <i class="fa fa-clock-o"></i> ${articleList.updatedDate}
												</td>
											</tr>
										</c:if>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
	function viewArticle(id)
	{
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");
		
		var posting = $.post('setArticleId', {
			 articleId : id
			});
			 posting.done(function(data) {
				  window.location.href="article";
			});
			
	}

</script>
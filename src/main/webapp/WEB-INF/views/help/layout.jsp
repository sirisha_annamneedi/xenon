<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Cache-control" content="private">

<link rel="icon" type="image/png"
	href="<%=request.getContextPath()%>/resources/img/xenon.png">

<title><tiles:insertAttribute name="title" ignore="true" /></title>

<link rel="icon" type="image/png"
	href="<%=request.getContextPath()%>/resources/img/xenon.png">
<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/font-awesome/css/font-awesome.css"
	rel="stylesheet">
	
<link href="<%=request.getContextPath()%>/resources/css/plugins/steps/jquery.steps.css"
	rel="stylesheet">

<!-- Ladda style -->
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/ladda/ladda-themeless.min.css"
	rel="stylesheet">

 <!-- Toastr style -->
<link href="<%=request.getContextPath()%>/resources/css/plugins/toastr/toastr.min.css" rel="stylesheet">
		    
<link href="<%=request.getContextPath()%>/resources/css/animate.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/resources/css/plugins/summernote/summernote.css" rel="stylesheet">
<%-- <link href="<%=request.getContextPath()%>/resources/css/plugins/summernote/summernote-bs3.css" rel="stylesheet"> --%>

<link href="<%=request.getContextPath()%>/resources/css/style.css"
	rel="stylesheet">
	
<!-- xenon custom style sheet -->
<link
	href="<%=request.getContextPath()%>/resources/css/xenon_custom.css"
	rel="stylesheet">
	
	
<!-- Mainly scripts -->
 <script src="<%=request.getContextPath()%>/resources/js/jquery-2.1.1.js"></script>
 <script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
 <script src="<%=request.getContextPath()%>/resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>
 <script src="<%=request.getContextPath()%>/resources/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
 
 <!-- Custom and plugin javascript -->
 <script src="<%=request.getContextPath()%>/resources/js/inspinia.js"></script>
 <script src="<%=request.getContextPath()%>/resources/js/plugins/pace/pace.min.js"></script>
 

 <!-- Jquery Validate -->
<script	src="<%=request.getContextPath()%>/resources/js/plugins/validate/jquery.validate.min.js"></script>


<!-- Steps -->
<script	src="<%=request.getContextPath()%>/resources/js/plugins/staps/jquery.steps.min.js"></script>
<%--   <!-- jQuery UI -->
 <script src="<%=request.getContextPath()%>/resources/js/plugins/jquery-ui/jquery-ui.min.js"></script> --%>	  
 
 <!-- SUMMERNOTE -->
<script src="<%=request.getContextPath()%>/resources/js/plugins/summernote/summernote.min.js"></script>

<!-- Ladda -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/ladda/spin.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/ladda/ladda.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Toastr script -->
    <script src="<%=request.getContextPath()%>/resources/js/plugins/toastr/toastr.min.js"></script>
    
</head>

<body class=""> <!-- md-skin --> <!-- style="background-color: #ffffff !important;" -->
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="body" />
	<tiles:insertAttribute name="footer" />
</body>
</html>
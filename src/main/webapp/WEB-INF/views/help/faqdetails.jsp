<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>FAQ</h2>
		<ol class="breadcrumb">
			<li><a href="help">Dashboard</a></li>
			<li><a href="viewfaq">FAQ</a></li>
			<li class="active"><strong>${Model.details[0].title}</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight article">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<c:if test="${Model.create_faq == 1}">
						<button id="edit" class="btn btn-primary btn-xs"
							onclick="edit()" type="button">Edit</button>
						<button id="save" class="btn btn-primary btn-xs"
							onclick="save()" type="button">Save</button>
						<button id="submit" class="btn btn-primary btn-xs" 
						onclick="submit()" type="button">Submit</button>
					</c:if>
					<div class="">
						<span class="text-muted text-right" style="float: right;"><i class="fa fa-clock-o"></i>
						<%-- <fmt:formatDate type="both" dateStyle="medium" timeStyle="medium" value="${Model.details[0].create_date}" /> --%>
						${Model.details[0].create_date}
						</span>
						<h1 style="font-size: 35px; font-weight: 400;" class="text-center">${Model.details[0].title}</h1>
					</div>
					
				</div>
	            <div class="ibox-content">
	            	<input type="hidden" id="faqId" value="${Model.details[0].faq_id}">
					<div class="click2edit wrapper p-md">${Model.details[0].description}</div>
				</div>
            </div>
		</div>
	</div>
</div>
</div> <!-- end .md-skin -->

<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});

var edit = function() {
	$('.click2edit').summernote({
		focus : true
	});
};

 var save = function() {
     $('.click2edit').destroy();
 };

 var submit = function() {
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");
		
		var aHTML = $('.click2edit').code();
	
		var posting = $.post('updatefaq', {
			faqDescription : aHTML,
			faqId:$("#faqId").val()
		});
		
		posting.done(function(data) {
			window.location.href = "faqdetails";
		})

	};
	
</script>
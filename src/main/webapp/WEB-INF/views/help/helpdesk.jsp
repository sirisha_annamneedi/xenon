<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>${Model.componentName}</h2>
		<ol class="breadcrumb">
			<li><a href="help">Dashboard</a></li>
			<li class="active"><strong>${Model.componentName}</strong></li>
		</ol>
	</div>
</div>


<div class="row">
	<div class="col-lg-12">
		<div class="wrapper wrapper-content animated fadeInRight">
			<c:if test="${Model.createSubcomponent==1}">
				<div class="faq-item">
					<div class="row">
						<div class="col-md-7"></div>
						<div class="col-md-3"></div>
						<div class="col-md-2 text-right">

							<button type="button" class="btn btn-success btn-xs"
								onclick="location.href = 'addsubcomponent';">Add
								Sub-component</button>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12"></div>
					</div>
				</div>
			</c:if>
			<c:forEach var="subcomponentList" items="${Model.subcomponentList}">
				<div class="faq-item">
					<div class="row">
						<div class="col-md-7">
							<a data-toggle="collapse" href="#faq_${subcomponentList.id}"
								class="faq-question" style="color: #1c84c6;"
								onclick="viewComponent(${subcomponentList.id})">${subcomponentList.subcmp_name }</a>
							<div class="subCompoDesc">${subcomponentList.subcmp_desc}</div>
						</div>
						<div class="col-md-3">
							<span class="small font-bold">Last Updated Date</span>
							<div class="tag-list">
								<span class="tag-item"> <i class="fa fa-clock-o"></i> <c:choose>

										<c:when test="${subcomponentList.TimeDiff < 60 }">
											${subcomponentList.TimeDiff}
												min ago
										</c:when>

										<c:when
											test="${subcomponentList.TimeDiff >= 60 && subcomponentList.TimeDiff < 1440 }">
											<fmt:parseNumber var="hour" integerOnly="true" type="number"
												value="${subcomponentList.TimeDiff/60}" />
											${hour} hr ago 
										</c:when>


										<c:when
											test="${subcomponentList.TimeDiff >= 1440 && subcomponentList.TimeDiff < 2880 }">
											Yesterday 
										</c:when>
										<c:otherwise>
											${subcomponentList.lastUpdateDate}
											
										</c:otherwise>
									</c:choose>




								</span>
							</div>
						</div>
						<div class="col-md-2 text-right">
							<span class="small font-bold">Articles</span><br />
							${subcomponentList.articleCount}
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div id="faq_${subcomponentList.id}"
								class="panel-collapse collapse ">
								<div class="faq-answer" style="background: none;">
									<c:if test="${Model.createArticle==1}">
										<button type="button" class="btn btn-success btn-xs"
											style="float: right; margin-left: 1%;"
											onclick="location.href = 'addarticle';">Add Article</button>
									</c:if>
									<div class="table-responsive">
										<table class="table table-striped articleList">
											<tbody>
												<c:forEach var="articleList" items="${Model.articleList}">
													<c:if
														test="${subcomponentList.id == articleList.subcomponent_id}">
														<tr>
															<td><a href="#"
																onclick="viewArticle(${articleList.id})">${articleList.art_name }</a></td>
															<td style="float: right; margin-right: 5%">Last
																Updated : <i class="fa fa-clock-o"></i>
																${articleList.updatedDate}
															</td>
														</tr>
													</c:if>
												</c:forEach>

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>

		</div>
	</div>

</div>

<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
	$(document).ready(
			function() {

	/* $('.articleList').DataTable({
		"paging" : false,
		"lengthChange" : false,
		"searching" : false,
		"ordering" : false,
		"info" : true,
		"autoWidth" : true
	}); */
	 $( ".subCompoDesc" ).each(function(index ) {
	var string = $(this).html();
	//alert(string.length);
	var str50 = string.substr(0,50);
	
		$(this).html('<a class="readmore" href="#">Read More</a>');
		$(this).attr('data-text',string);
		$('.readmore').click(function(e)
		{
		    e.preventDefault();
		    $(this).parent().html($(this).parent().attr('data-text'))
		});		
			
		}); 
	});
	function viewArticle(id)
	{
		$("#barInMenu").removeClass('hidden');
		$("#wrapper").addClass("hidden");
		var posting = $.post('setArticleId', {
			 articleId : id
			});
			 posting.done(function(data) {
					$("#barInMenu").removeClass('hidden');
					$("#wrapper").addClass("hidden");
				  	window.location.href="article";
			});
	}
	function viewComponent(id)
	{
		var posting = $.post('setSubcomponentId', {
			subcomponentId : id
			});
	}
</script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Article</h2>
		<ol class="breadcrumb">
			<li><a href="help">Dashboard</a></li>
			<li><a href="helpdesk">${Model.componentName}</a></li>
			<li><a href="subcomponent">${Model.subcomponentName}</a></li>
			<li class="active"><strong>Article</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight article">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5></h5>
					  <c:if test="${Model.createArticle==1}">
					<button id="edit" class="btn btn-primary btn-xs m-l-sm"
						onclick="edit()" type="button">Edit</button>
					<button id="save" class="btn btn-primary btn-xs"
						onclick="save()" type="button">Save</button>
					<button id="submit" class="btn btn-primary  btn-xs" 
					onclick="submit()"
						type="button">Submit</button>
						</c:if>
				</div>
				<div class="ibox-content no-padding">
					<input type="hidden" id="articleId" value="${Model.Article[0].id}">
					<div class="text-center wrapper p-md">
						<span class="text-muted" style="float: right;"><i
							class="fa fa-clock-o"></i> ${Model.Article[0].updatedDate} </span>
						<h2>${Model.Article[0].art_name}</h2>
					</div>
					<div class="click2edit wrapper p-md">

						${Model.Article[0].art_desc}</div>

				</div>
			</div>
		</div>
	</div>

</div>


<script type="text/javascript">
	
	$(document).ready(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
		//$('.click2edit').summernote();

	});

	var edit = function() {
		$('.click2edit').summernote({
			focus : true
		});
	};
	
	 var save = function() {
         var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
         $('.click2edit').destroy();
     };
     
	var submit = function() {
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");
		
		var aHTML = $('.click2edit').code();
	
		var posting = $.post('updatearticle', {
			articleDescription : aHTML,
			articleId:$("#articleId").val()
		});
		
		posting.done(function(data) {
			window.location.href = "article";
		})

	};
</script>
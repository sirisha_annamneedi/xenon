<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>
				<ol class="breadcrumb">
					<li><strong> Execution Trash </strong></li>
					<li>${UserCurrentProjectName}</li>
				</ol>
			</h2>
		</div>

	</div>
	<div class="row sub-topmenu">
		<div class="col-md-12">
			<div class="">
				<div class="col-md-6 pd-l-0">
					<ul class="tabb">
						<li class="active"><a href="#manual" role="tab"
							data-toggle="tab">Manual</a></li>
						<li><a href="#automation" role="tab" data-toggle="tab"
							class=" ">Automation</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="ibox float-e-margins">
			<div class="ibox-content">
				<!-- <ul id="tabs" class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#manual"
						role="tab" data-toggle="tab">Manual</a></li>
					<li role="presentation"><a href="#automation" role="tab"
						data-toggle="tab">Automation</a></li>
				</ul> -->

				<div class="tab-content mt-20">
					<div role="tabpanel" class="tab-pane fade in active col-md-10"
						id="manual">
						<div class="table-responsive"
							style="overflow-x: visible; table-layout: fixed; width: 100%">
							<table
								class="table table-striped table-bordered table-hover dataTables-example display select"
								id="trashDTable">
								<thead>
									<tr>

										<th>Release Name</th>
										<th>Build Name</th>
										<th>Restore</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="manual" items="${Model.manualTrash}">
										<tr style="cursor: pointer;">
											<td>${manual.release_name}</td>
											<%-- <td>${manual.build_id}</td> --%>
											<td>${manual.build_name}</td>
											<!--  <td>${manual.build_status}</td> -->
											<%-- <td>${manual.release_id}</td> --%>
											<%-- <td>${manual.release_active}</td> --%>
											<td style="text-align: center">
												<button
													class="btn-white btn btn-xs edit-btn restore-manual-button"
													id="editBugBtn" data-build-id="${manual.build_id}"
													data-release-id="${manual.release_id}"
													data-release-status="${manual.release_active}">
													<i class="fa fa-undo"></i>
												</button>
											</td>

										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade col-md-10"
						id="automation">

						<div class="table-responsive"
							style="overflow-x: visible; table-layout: fixed; width: 100%">
							<table
								class="table table-striped table-bordered table-hover dataTables-example display select"
								id="trashDTableAuto">
								<thead>
									<tr>

										<th>Release Name</th>
										<th>Build Name</th>
										<th>Restore</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="automation" items="${Model.automationTrash}">
										<tr style="cursor: pointer;">
											<td>${automation.release_name}</td>
											<%-- <td>${automation.build_id}</td> --%>
											<td>${automation.build_name}</td>
											<!--  <td>${automation.build_status}</td> -->
											<%-- <td>${automation.release_id}</td> --%>
											<%-- <td>${automation.release_active}</td> --%>
											<td style="text-align: center">
												<button
													class="btn-white btn btn-xs edit-btn restore-automation-button"
													id="editBugBtn" data-build-id="${automation.build_id}"
													data-release-id="${automation.release_id}"
													data-release-status="${automation.release_active}">
													<i class="fa fa-undo"></i>
												</button>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
	$(function() {
	});
</script>
<script>
	$(function() {
		$('.restore-automation-button').click(function(e) {
			var $this = $(this);
			$.ajax({
				type : "POST",
				url : "restoreAutomation",
				data : {
					buildId : $this.data('build-id'),
					releaseId : $this.data('release-id'),
					releaseStatus : $this.data('release-status'),
				}
			}).done(function(response) {
				window.location.href = "viewbuildtrash";
			}).fail(function(jqXHR, testStatus, errorThrown) {
				return false;
			});
		});

		$('.restore-manual-button').click(function(e) {
			var $this = $(this);
			$.ajax({
				type : "POST",
				url : "restoreManual",
				data : {
					buildId : $this.data('build-id'),
					releaseId : $this.data('release-id'),
					releaseStatus : $this.data('release-status'),
				}
			}).done(function(response) {
				window.location.href = "viewbuildtrash";
			}).fail(function(jqXHR, testStatus, errorThrown) {
				return false;
			});
		});

	});

	$(document).ready(function() {
		$('#trashDTable').DataTable();
		$('#trashDTableAuto').DataTable();

	});
</script>
<style>
.wrapper-content {
	padding: 10px 10px 30px;
}
</style>

<script>
	$(function() {
		$('.restore-automation-button').click(function(e) {
			var $this = $(this);
			$.ajax({
				type : "POST",
				url : "restoreAutomation",
				data : {
					buildId : $this.data('build-id'),
					releaseId : $this.data('release-id'),
					releaseStatus : $this.data('release-status'),
				}
			}).done(function(response) {
				window.location.href = "viewbuildtrash";
			}).fail(function(jqXHR, testStatus, errorThrown) {
				return false;
			});
		});

		$('.restore-manual-button').click(function(e) {
			var $this = $(this);
			$.ajax({
				type : "POST",
				url : "restoreManual",
				data : {
					buildId : $this.data('build-id'),
					releaseId : $this.data('release-id'),
					releaseStatus : $this.data('release-status'),
				}
			}).done(function(response) {
				window.location.href = "viewbuildtrash";
			}).fail(function(jqXHR, testStatus, errorThrown) {
				return false;
			});
		});

	});
</script>
<style>
.tabb li.active, .sub-topmenu ul li:hover {
	color: #1c84c6;
	background: aliceblue;
	border-radius: 3px;
	padding: 8px;
}

.tabb li.active a {
	color: #1c84c6 !important;
	padding: 8px
}

.sub-topmenu ul li {
	padding: 8px;
}

.tab-content, .ibox-content {
	overflow: hidden;
}

.sub-topmenu ul {
	list-style-type: none;
	padding-top: 5px;
	padding-left: 0;
	margin-bottom: 5px;
}
</style>
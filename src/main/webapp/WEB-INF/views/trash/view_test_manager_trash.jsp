<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>
				<ol class="breadcrumb">
					<li><strong> Test Manager Trash </strong></li>
					<li>${UserCurrentProjectName}</li>
				</ol>
			</h2>
		</div>

	</div>
	<div class="row sub-topmenu">
		<div class="col-md-12">
			<div class="">
				<div class="col-md-6 pd-l-0">
					<ul class="tabb">
						<li class="active"><a href="#manual" role="tab"
							data-toggle="tab">Module</a></li>
						<li><a href="#automation" role="tab" data-toggle="tab"
							class=" ">Scenario </a></li>
						<li><a href="#testCaseTrash" role="tab" data-toggle="tab">Test
								Case</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="ibox float-e-margins">
			<div class="ibox-content">
				<!-- <ul id="tabs" class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#module"
						role="tab" data-toggle="tab">Manual</a></li>
					<li role="presentation"><a href="#scenario" role="tab"
						data-toggle="tab">Automation</a></li>
				</ul> -->

				<div class="tab-content mt-20">
					<div role="tabpanel" class="tab-pane fade in active col-md-10"
						id="manual">
						<div class="table-responsive"
							style="overflow-x: visible; table-layout: fixed; width: 100%">
							<table
								class="table table-striped table-bordered table-hover dataTables-example display select"
								id="trashDTable">
								<thead>
									<tr>

										<th>Module ID</th>
										<th>Module Name</th>
										<th>Restore</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="module" items="${Model.dataModuleTrash}">
										<tr style="cursor: pointer;">
											<td>${module.module_id}</td>
											<td>${module.module_name}</td>

											<td style="text-align: center">
												<button
													class="btn-white btn btn-xs edit-btn restore-module-button"
													id="editBugBtn" data-module-id="${module.module_id}">


													<i class="fa fa-undo"></i>
												</button>
											</td>

										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade col-md-10"
						id="automation">

						<div class="table-responsive"
							style="overflow-x: visible; table-layout: fixed; width: 100%">
							<table
								class="table table-striped table-bordered table-hover dataTables-example display select"
								id="trashDTableAuto">
								<thead>
									<tr>
										<th>Module Name</th>
										<th>Scenario ID</th>
										<th>Scenario Name</th>
										<th>Restore</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="scenario" items="${Model.dataScenarioTrash}">
										<tr style="cursor: pointer;">
											<td>${scenario.module_name}</td>
											<td>${scenario.scenario_id}</td>
											<td>${scenario.scenario_name}</td>

											<td style="text-align: center">
												<button
													class="btn-white btn btn-xs edit-btn restore-scenario-button"
													id="editBugBtn" data-module-id="${scenario.module_id}"
													data-scenario-id="${scenario.scenario_id}">
													<i class="fa fa-undo"></i>
												</button>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

					</div>
					<div role="tabpanel" class="tab-pane fade col-md-12"
						id="testCaseTrash">

						<div class="table-responsive"
							style="overflow-x: auto; table-layout: fixed; width: 100%">
							<table
								class="table table-striped table-bordered table-hover dataTables-example display select"
								id="testCaseTrashTable">
								<thead>
									<tr>
										<th width="100px">Module ID</th>
										<th width="100px">Scenario ID</th>
										<th width="100px">Test Case ID</th>
										<th style="width: auto">Test Case Name</th>
										<th width="100px">Restore</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="testCaseTrash"
										items="${Model.dataTestcaseTrash}">
										<tr style="cursor: pointer;">
											<td width="100px">${testCaseTrash.module_id}</td>
											<td width="100px">${testCaseTrash.scenario_id}</td>
											<td width="100px">${testCaseTrash.testcase_id}</td>
											<td style="width: auto; word-break: break-all">${testCaseTrash.testcase_name}</td>
											<td style="text-align: center" width="100px">
												<button
													class="btn-white btn btn-xs edit-btn restore-testcase-button"
													id="editBugBtn"
													data-testcase-id="${testCaseTrash.testcase_id}">
													<i class="fa fa-undo"></i>
												</button>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
	$(function() {
	});
</script>
<script>
	$(function() {
		$('.restore-scenario-button').click(function(e) {
			var $this = $(this);

			$.ajax({
				type : "POST",
				url : "restoreScenario",
				data : {
					moduleId : $this.data('module-id'),
					scenarioId : $this.data('scenario-id'),
				}
			}).done(function(response) {
				window.location.href = "viewtmtrash";
			}).fail(function(jqXHR, testStatus, errorThrown) {
				return false;
			});
		});

		$('.restore-module-button').click(function(e) {
			var $this = $(this);
			//alert($this.data('module-id'));
			$.ajax({
				type : "POST",
				url : "restoreModule",
				data : {
					moduleId : $this.data('module-id'),

				}
			}).done(function(response) {
				window.location.href = "viewtmtrash";
			}).fail(function(jqXHR, testStatus, errorThrown) {
				return false;
			});
		});

		$('.restore-testcase-button').click(function(e) {
			var $this = $(this);

			$.ajax({
				type : "POST",
				url : "restoreTestcase",
				data : {
					testcaseId : $this.data('testcase-id'),

				}
			}).done(function(response) {
				window.location.href = "viewtmtrash";
			}).fail(function(jqXHR, testStatus, errorThrown) {
				return false;
			});
		});

	});

	$(document).ready(function() {
		$('#trashDTable').DataTable();
		$('#trashDTableAuto').DataTable();
		$('#testCaseTrashTable').DataTable();
	});
</script>
<style>
.wrapper-content {
	padding: 10px 10px 30px;
}
</style>

<script>
	$(function() {
		$('.restore-scenario-button').click(function(e) {
			var $this = $(this);
			$.ajax({
				type : "POST",
				url : "restoreAutomation",
				data : {
					buildId : $this.data('build-id'),
					releaseId : $this.data('release-id'),
					releaseStatus : $this.data('release-status'),
				}
			}).done(function(response) {
				window.location.href = "viewtmtrash";
			}).fail(function(jqXHR, testStatus, errorThrown) {
				return false;
			});
		});

		$('.restore-manual-button').click(function(e) {
			var $this = $(this);
			$.ajax({
				type : "POST",
				url : "restoreManual",
				data : {
					buildId : $this.data('build-id'),
					releaseId : $this.data('release-id'),
					releaseStatus : $this.data('release-status'),
				}
			}).done(function(response) {
				window.location.href = "viewtmtrash";
			}).fail(function(jqXHR, testStatus, errorThrown) {
				return false;
			});
		});

	});
</script>
<style>
.tabb li.active, .sub-topmenu ul li:hover {
	color: #1c84c6;
	background: aliceblue;
	border-radius: 3px;
	padding: 8px;
}

.tabb li.active a {
	color: #1c84c6 !important;
	padding: 8px
}

.sub-topmenu ul li {
	padding: 8px;
}

.tab-content, .ibox-content {
	overflow: hidden;
}

.sub-topmenu ul {
	list-style-type: none;
	padding-top: 5px;
	padding-left: 0;
	margin-bottom: 5px;
}
</style>
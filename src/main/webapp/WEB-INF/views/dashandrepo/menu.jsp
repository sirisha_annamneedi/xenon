<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="bar" id="barInMenu" class="">
	<p>loading</p>
</div>

<div id="wrapper" class="hidden">
	<nav class="navbar-default navbar-static-side " role="navigation">
		<!-- style="min-height: 100%; background-color: transparent;" -->
		<div class="sidebar-collapse">
			<ul class="nav metismenu" id="side-menu">
				<%-- <li class="nav-header">
					<div class="dropdown profile-element">
						<span> <img alt="image" class="img-circle"
							style="width: 48px; height: 48px"
							src="data:image/jpg;base64,${Model.UserProfilePhoto }" />
						</span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
							class="clear"> <span class="block m-t-xs"> <strong
									class="font-bold">${Model.userName }</strong>
							</span>
						</span>
						</a> <span class="text-muted text-xs block">${Model.role}<b
							class=""></b></span>
					</div>
					<div class="logo-element" style="padding: 14px !important">
						<a href="#" onclick="gotoLinkInMenu('xedashboard')">
						<img alt="image" class=""
							src="<%=request.getContextPath()%>/resources/img/xenon.png" />
						</a>
					</div>
				</li> --%>
				<li class="nav-header">
                    <div class="dropdown profile-element text-center">
                        <a href="#" onclick="gotoLink('xedashboard')" >
                        <img src="<%=request.getContextPath()%>/resources/img/Jade-Logo.png" class="xenon-logo">
                        </a>
                    <!--    <img class="img-circle" style="width:48px; height:48px" src="resources/img/user.jpg">
                         <img alt="image" class="img-circle"
                            style="width: 48px; height: 48px"
                            src="data:image/jpg;base64,${Model.UserProfilePhoto }" />
                        </span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
                            class="clear"> <span class="block m-t-xs"> <strong
                                    class="font-bold">${Model.userName }</strong>
                            </span>
                        </span>
                        </a> <span class="text-muted text-xs block">${Model.role}<b
                            class=""></b></span>  -->
                    </div>
                    <div class="logo-element" style="padding: 14px !important">
                        <a href="#" onclick="gotoLinkInMenu('xedashboard')">
                        <img alt="image" class=""
                            src="<%=request.getContextPath()%>/resources/img/xenon.png" />
                        </a>
                    </div>
                </li>
				<li class="mainLi"><a href="#" onclick="gotoLinkInMenu('dashandrepo')">  
					<i class="fa fa-diamond"></i> <span class="nav-label">DashBoard And Reports</span></a>
				</li>
				
				<!-- <li class="mainLi" id="id-report-li"><a href="#"><i class="fa fa-bar-chart-o"></i>
					<span class="nav-label">Reports</span><span class="fa arrow"></span></a>
					<ul class="nav nav-second-level collapse">
						<li><a href="#" onclick="gotoLinkInMenu('automationsummary')">Test Set Report</a></li>
						<li><a href="#" onclick="gotoLinkInMenu('tcbuganalysis')">Test Case Report</a></li>
						<li><a href="#" onclick="gotoLinkInMenu('buganalysis')">Bug Report</a></li>
					</ul>
				</li>  -->
				
				<li class="mainLi" id="id-report-li">
					<a href="#"><i class="fa fa-bar-chart-o"></i>
					<span class="nav-label">Reports</span><span class="fa arrow"></span></a>
					<ul class="nav nav-second-level collapse">
						<li class="class-sub-li"  id="id-test-manager-li"><a href="#" onclick="gotoLinkInMenu('tmdashboardlink')">Test Manager</a></li>
						<li class="class-sub-li" id="id-test-execution-li">
							<a href="#">Test Execution
							<!-- <span class="accordion-heading" data-toggle="collapse" data-target="#submenu">Test Execution
							</span> -->
							<span class="fa arrow"></span></a>
							<ul class="nav nav-list collapse" id="submenu">
								<li class="class-sub-3-li" id="id-manual-sub-li"> <a href="buildreportmanual">Manual</a></li>
								<li class="class-sub-3-li" id="id-automation-sub-li"> <a href="buildreportautomation">Automation</a></li>
							</ul>
						</li>
						<li class="class-sub-li" id="id-bug-tracker-li"><a href="#" onclick="gotoLinkInMenu('buganalysis')">Bug Tracker</a></li>
					</ul> 
				</li>
					
				<li class="mainLi" id="id-dashboard-li"><a href="#"><i class="fa fa-pie-chart"></i>
					<span class="nav-label">Dashboards</span><span class="fa arrow"></span></a>
					<ul class="nav nav-second-level collapse">
					 	<li class="class-sub-li" id="id-test-manager-dashboard-li"><a href="#" onclick="gotoLinkInMenu('tmdashboard')">Test Manager</a></li>
					 	<li class="class-sub-li" id="id-test-execution-dashboard-li"><a href="#" onclick="gotoLinkInMenu('bmdashboard')">Test Execution</a></li>
					 	<li class="class-sub-li" id="id-bug-tracker-dashboard-li"><a href="#" onclick="gotoLinkInMenu('btdashboard')">Bug Tracker</a></li>
					</ul>
				</li>
				
			</ul>

		</div> 
	</nav>

	<input type="hidden" value="<%=session.getAttribute("menuLiText")%>"
		id="hiddenLiText">
</div>

	<script type="text/javascript">
		$(function() {

			var liText = $('#hiddenLiText').val();
			$('.nav-label').each(function() {
				var label = $(this).text().trim();
				console.log(label +" : "+ liText)
				if (label == liText) {
					var parentLi = $(this).parents('.mainLi');
					parentLi.addClass('active');
					parentLi.find('.collapse').addClass('in');
				}
			});
			
			$('.mainLi').on('click', function(){
				sessionStorage.setItem("clicked",$(this).prop('id'));
			});
			
			$('.class-sub-3-li').on('click', function(){
				sessionStorage.setItem("sub-3-clicked",$(this).prop('id'));
			});
			$('.class-sub-li').on('click', function(){
				/* sessionStorage.removeItem("sub-clicked"); */
				sessionStorage.setItem("sub-clicked",$(this).prop('id'));
			});
			
			
			$('#'+sessionStorage.getItem("clicked")+' > a').trigger('click');
			$('#'+sessionStorage.getItem("sub-clicked")).addClass('active');
			if( "id-test-execution-li" == sessionStorage.getItem("sub-clicked") ) {
 				$('#submenu').addClass('in');
// 				$('#id-sub-li').addClass('active');
				$('#'+sessionStorage.getItem("sub-3-clicked")).addClass('active');
			}
		});

		function gotoLinkInMenu(link)//link
		{
			$('body').addClass('white-bg');
			$("#barInMenu").removeClass('hidden');
			$("#wrapper").addClass("hidden");
			
			window.location.href = link;		
		}
		
		/* function setSessionValueRep(){
			var a=$("#reports").text();
			sessionStorage.setItem("clicked", a);
			alert(sessionStorage.getItem("clicked"));
			
		}
		
		function setSessionValueDash(){
			var a=$("#dashboard").text();
			sessionStorage.setItem("clicked", a);
			alert(sessionStorage.getItem("clicked"));
		} */
		
	</script>
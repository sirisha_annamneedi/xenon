<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
   var $x = jQuery.noConflict();
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>	

<script type="text/javascript">
	$(window).load(function() {
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
</script>

	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>
				<ol class="breadcrumb">
					<li><strong>Dashboard & Report</strong></li>
<%-- 					<li>${UserCurrentProjectName}</li> --%>
<%-- 					<li>${Model.totalReleaseTestData[0].release_name}</li> --%>
				</ol>
			</h2>
		</div>

	</div>
	<%-- <div>${Model.cancelCount}</div> --%>
<%-- <div>${Model.buildTc1}</div> --%>

<div class="wrapper wrapper-content ">
<div class="md-skin mb-40">
	<div class="wrapper wrapper-content">
<div class="graph-dashboard">
	<div class="col-md-6">
	<div class="gd-title">
	All Open Bugs in Application
	</div>
	 <div class="testc-status table-responsive mt-20">
       		<table id="openBugs" class="table table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Total Count</th>
                <th>Assignee</th>

            </tr>
        </thead>
        <tbody>
        
        	<c:forEach var="cancelBugs" items="${Model.cancelCount}">
	            <tr>
	            	
	            	<td>${cancelBugs.total}</td>
	            	<td>${cancelBugs.first_name} ${cancelBugs.last_name}</td>
	            	
	            </tr>
	           
            </c:forEach>
            
        </tbody>
       
    </table>
	 </div>
	
	</div>
	<div class="col-md-6">
	<div class="gd-title">
	Bugs Assigned To
	</div>
	<div class="mt-20">
	<canvas id="bugsAssignedChart" width="400" height="300" style="border: 1px solid #ddd;margin-bottom:20px"></canvas>
	</div>
	
	</div>
	</div>
	
	<div class="graph-dashboard">
	<div class="col-md-12">
	<div class="gd-title">
	Root Cause Analysis
	</div>
	<div class="row">
	<div class="col-md-6">
	 <div class="testc-status mt-20">
		<canvas id="rootCauseChart" width="400" height="300"></canvas>
	 </div>
	</div>
	<div class="col-md-6">
	<div class="mt-50">
	<div class="table-responsive">
	<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Category</th>
	      	<th scope="col">Count</th> 

    </tr>
  </thead>
  <tbody>
  
        <c:forEach var="totalc" items="${Model.rootGraph}">
	            <tr>
	                <td><b>${totalc.category_name}</b></td>
	                <td>${totalc.total}</td>

	            </tr>
            </c:forEach>
  </tbody>
</table>
</div>
	</div>
	
	</div>
	</div>

	
	
	</div>
	</div>
	
	<div class="graph-dashboard">
	<div class="col-md-12">
	<div class="gd-title">
	Test Set Wise Test Case
	</div>
	<div class="row">
	<div class="col-md-6">
	 <div class="testc-status mt-20">
		<canvas id="testWiseSetChart" width="400" height="400"></canvas>
	 </div>
	</div>
	<div class="col-md-6">
	<div class="mt-50">
	<div class="table-responsive">
	<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Build Name</th>
      <th scope="col">Total Test Case Count</th>
      <th scope="col">Pass Count</th>
      <th scope="col">Fail Count</th>
      <th scope="col">Skip Count</th>
      <th scope="col">Block Count</th>
      <th scope="col">Not Run Count</th>
      
    </tr>
  </thead>
  <tbody>
     <c:forEach var="buildwise" items="${Model.buildTc}">
	            <tr>
	                <td><b>${buildwise.build_name}</b></td>
	                <td>${buildwise.totalTc}</td>
	            	<td>${buildwise.PassCount}</td>
	            	<td>${buildwise.FailCount}</td>
	            	<td>${buildwise.SkipCount}</td>
	            	<td>${buildwise.BlockCount}</td>
	            	<td>${buildwise.NotRunCount}</td>

	            </tr>
            </c:forEach>

  </tbody>
</table>
</div>
	</div>
	
	</div>
	</div>

	
	
	</div>
	</div>

	<!--  <div class="graph-dashboard">
	<div class="col-md-12">
	<div class="gd-title">
	Opens Bugs Assigned to me
	</div>
	<div class="row">
	<div class="col-md-12">
	 <div class="testc-status mt-20">
	 <div class="table-responsive">
		<table id="bugsToMe" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Bug ID</th>
                <th>Status</th>
                <th>Title</th>
                <th>Priority</th>
                <th>Module</th>
                <th>Severity</th>
                <th>Category</th>
                <th>Reported By</th>
                <th>Updated Date</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>As10</td>
                <td>Closed</td>
                <td>Margin Issue</td>
                <td>High</td>
                <td>TestApp1</td>
                <td>asd</td>
                <td>App</td>
                <td>Afzal</td>
                <td>26-11-2019</td>
            </tr>
            <tr>
                <td>As10</td>
                <td>Closed</td>
                <td>Margin Issue</td>
                <td>High</td>
                <td>TestApp1</td>
                <td>asd</td>
                <td>App</td>
                <td>Afzal</td>
                <td>26-11-2019</td>
            </tr>
            <tr>
                <td>As10</td>
                <td>Closed</td>
                <td>Margin Issue</td>
                <td>High</td>
                <td>TestApp1</td>
                <td>asd</td>
                <td>App</td>
                <td>Afzal</td>
                <td>26-11-2019</td>
            </tr>
        </tbody>
       
    </table>
    </div>
	 </div>
	</div>
	
	</div>

	
	
	</div>
	</div> -->
	
	
	</div> 
	</div>  
</div>
<script type="text/javascript">
	var ctx = document.getElementById("bugsAssignedChart");
	//var userBugs=[];
	cancelCountData=${Model.cancelCountData};
	var nameArray=[];
	var dataArray=[];
	for(var i=0;i<cancelCountData.length;i++){
		dataArray[i]=cancelCountData[i].total;
		nameArray[i]=cancelCountData[i].first_name +' ' + cancelCountData[i].last_name;
	}
	
	var bugsAssignedChart = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        //labels: ["Deepa", "Afzal", "Shailesh", "Shantaram", "Anmol"],
	        labels:nameArray,
	        datasets: [{
	        	label: "Bug Count",
	            data: dataArray,
	            backgroundColor: [
			        'rgba(54, 162, 235, 0.9)',
			        'rgba(54, 162, 235, 0.9)',
			        'rgba(54, 162, 235, 0.9)',
			        'rgba(54, 162, 235, 0.9)',
			        'rgba(54, 162, 235, 0.9)'
			      ],
			      borderColor: [
			        'rgba(54, 162, 235, 1)',
			        'rgba(54, 162, 235, 1)',
			        'rgba(54, 162, 235, 1)',
			        'rgba(54, 162, 235, 1)',
			        'rgba(54, 162, 235, 1)'
			      ],
			      borderWidth: 1,
	        }]
	    },
	    options: {
	    		"hover": {
	        	"animationDuration": 1
	        },
	        title: {
	            display: true,
	            text: '',
	           
	          },
	        "animation": {
	        	"duration": 1,
							"onComplete": function () {
								var chartInstance = this.chart,
									ctx = chartInstance.ctx;
								
								ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
								ctx.textAlign = 'center';
								ctx.textBaseline = 'bottom';
								this.data.datasets.forEach(function (dataset, i) {
									var meta = chartInstance.controller.getDatasetMeta(i);
									meta.data.forEach(function (bar, index) {
										var data = dataset.data[index];                            
										ctx.fillText(data, bar._model.x, bar._model.y - 5);
									});
								});
							}
	        },
	    		legend: {
	        	"display": false
	        },
	        tooltips: {
	        	"enabled": true
	         },
	        scales: {
	            yAxes: [{
	            		display: true,            		
	            		gridLines: {
	                	display : true 
	                },
	                
	                ticks: {
	                		display: true,
	                    beginAtZero:true,
	                    fontColor: "rgba(0,0,0,0.9)"
	                }
	            }],
	            xAxes: [{
	            		gridLines: {
	                	display : true
	                },
	                ticks: {
	                    beginAtZero:true,
	                     maxRotation: 40,
	          			minRotation: 30,
	          			fontColor: "rgba(0,0,0,0.9)"
	                }
	            }]
	        }
	    }

	});
		
	// root cause
	var ctx = document.getElementById("rootCauseChart");
	var rootGraphData=[];
	rootGraphData=${Model.rootGraphData};
	var rootNameArray=[];
	var rootDataArray=[];
	for(var i=0;i<rootGraphData.length;i++){
		rootDataArray[i]=rootGraphData[i].total;
		rootNameArray[i]=rootGraphData[i].category_name;
	}
	var rootCauseChart = new Chart(ctx, {
	    type: 'horizontalBar',
	    data: {
	        //labels: ["Code Issue", "Environment Issue", "DB Issue", "Data Issue", "UI Issue","Invalid Issue", "Duplicate Issue", "Design Issue", "Requirement Cap", "Performance Issue","Unknown", "Other"],
	        labels: rootNameArray,
	        datasets: [{
	        	label: "Test Count",
	            data: rootDataArray,
	            backgroundColor: [
			        'rgba(255, 99, 132, 0.9)',
			        'rgba(54, 162, 235, 0.9)',
			        'rgba(255, 206, 86, 0.9)',
			        'rgba(65,105,225,0.9)',
			        'rgba(75, 192, 192, 0.9)',
			        'rgba(153, 102, 255, 0.9)',
			        'rgba(255, 159, 64, 0.9)',
			        'rgba(139,0,0,0.9)',
			        'rgba(34,139,34,0.9)',
			        'rgba(255,165,0,0.9)',
			        'rgba(128,128,128,0.9)',
			        'rgba(153,0,0,0.9)'
			      ],
			       borderColor: [
			        'rgba(255, 99, 132, 0.9)',
			        'rgba(54, 162, 235, 0.9)',
			        'rgba(255, 206, 86, 0.9)',
			        'rgba(65,105,225,0.9)',
			        'rgba(75, 192, 192, 0.9)',
			        'rgba(153, 102, 255, 0.9)',
			        'rgba(255, 159, 64, 0.9)',
			        'rgba(139,0,0,0.9)',
			        'rgba(34,139,34,0.9)',
			        'rgba(255,165,0,0.9)',
			        'rgba(128,128,128,0.9)',
			        'rgba(153,0,0,0.9)'
			      ], 
			      borderWidth: 1,
	        }]
	    },
	    options: {
	    		"hover": {
	        	"animationDuration": 1
	        },
	        title: {
	            display: true,
	            text: '',
	           
	          },
	        "animation": {
	        	"duration": 1,
							"onComplete": function () {
								var chartInstance = this.chart,
									ctx = chartInstance.ctx;
								
								ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
								ctx.textAlign = 'center';
								ctx.textBaseline = 'bottom';
								this.data.datasets.forEach(function (dataset, i) {
									var meta = chartInstance.controller.getDatasetMeta(i);
									meta.data.forEach(function (bar, index) {
										var data = dataset.data[index];                            
										ctx.fillText(data, bar._model.x - (-5), bar._model.y - (-8));
									});
								});
							}
	        },
	    		legend: {
	        	"display": false
	        },
	        tooltips: {
	        	"enabled": true,
	        	position: "nearest",
	            mode: "index",
	            intersect: false,
	         },
	        scales: {
	            yAxes: [{
	            		display: true,            		
	            		gridLines: {
	                	display : true 
	                },
	                barThickness : 30,
	                ticks: {
	                		display: true,
	                    beginAtZero:true,
	                    fontColor: "rgba(0,0,0,0.9)"
	                }
	            }],
	            xAxes: [{
	            		gridLines: {
	                	display : true
	                },
	                
	                
	                ticks: {
	                    beginAtZero:true,
	                     maxRotation: 0,
	          			minRotation: 0,
	          			fontColor: "rgba(0,0,0,0.9)"
	                }
	            }]
	        }
	    }

	});
// Test Case Wise Set
var ctx = document.getElementById("testWiseSetChart");

var buildTc=[];
buildTc=${Model.buildTc1};


var buildNameArray=[];
var tcCountArray=[];
for(var i=0;i<buildTc.length;i++){
	tcCountArray[i]=buildTc[i].totalTc;
	buildNameArray[i]=buildTc[i].build_name;
} 


	var testWiseSetChart = new Chart(ctx, {
	    type: 'bar',
	    data: {

	        labels: buildNameArray,
	        datasets: [{
	        	label: "Test Count",
	           data: tcCountArray,
	           backgroundColor: "rgba(54, 162, 235, 0.9)",
	           borderColor: "rgba(54, 162, 235, 1)",

			      borderWidth: 1,
	        }]
	    },
	    options: {
	    		"hover": {
	        	"animationDuration": 1
	        },
	        title: {
	            display: true,
	            text: '',
	           
	          },
	        "animation": {
	        	"duration": 1,
							"onComplete": function () {
								var chartInstance = this.chart,
									ctx = chartInstance.ctx;
								
								ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
								ctx.textAlign = 'center';
								ctx.textBaseline = 'bottom';
								this.data.datasets.forEach(function (dataset, i) {
									var meta = chartInstance.controller.getDatasetMeta(i);
									meta.data.forEach(function (bar, index) {
										var data = dataset.data[index];                            
										ctx.fillText(data, bar._model.x, bar._model.y -5);
									});
								});
							}
	        },
	    		legend: {
	        	"display": false
	        },
	        tooltips: {
	        	"enabled": true
	         },
	        scales: {
	            yAxes: [{
	            		display: true,            		
	            		gridLines: {
	                	display : true 
	                },
	                
	                ticks: {
	                		display: true,
	                    beginAtZero:true,
	                    fontColor: "rgba(0,0,0,0.9)"
	                }
	            }],
	            xAxes: [{
	            		gridLines: {
	                	display : true
	                },
	                barThickness : 30,
	                ticks: {
	                    beginAtZero:true,
	                     maxRotation: 90,
	          			minRotation: 20,
	          			fontColor: "rgba(0,0,0,0.9)"
	                }
	            }]
	        }
	    }

	});
// bugs assigned to me data table
$(document).ready(function() {
    $('#bugsToMe').DataTable({
    	language: { search: '', searchPlaceholder: "Search..." },
    });
    $('#openBugs').DataTable({
    	language: { search: '', searchPlaceholder: "Search..." },
    });
} );
	</script>
	
<style>
.table-responsive{
max-height:400px;
}
.wrapper-content {
    padding: 0;
}
</style>
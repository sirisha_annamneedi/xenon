<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="bar" id="barInMenu" class="">
	<p>loading</p>
</div>

<div id="wrapper" class="hidden ">
	<nav class="navbar-default navbar-static-side " role="navigation" > <!-- style="min-height: 100%; background-color: transparent;" -->
		<div class="sidebar-collapse">
			<ul class="nav metismenu" id="side-menu">
				<li class="nav-header">
                    <div class="dropdown profile-element text-center">
                        <a href="#" onclick="gotoLink('xedashboard')" >
                        <img src="<%=request.getContextPath()%>/resources/img/Jade-Logo.png" class="xenon-logo">
                        </a>
                    </div>
                    <div class="logo-element" style="padding: 14px !important">
                        <a href="#" onclick="gotoLinkInMenu('xedashboard')">
                        <img alt="image" class=""
                            src="<%=request.getContextPath()%>/resources/img/xenon.png" />
                        </a>
                    </div>
                </li>
		
				<li><a href="#" onclick="gotoLinkInMenu('dashandrepo')"><i class="fa fa-diamond"></i> <span
						class="nav-label">DashBoard</span></a></li>

				<li><a  href="#" onclick="gotoLinkInMenu('testcase')"><i class="fa fa-th-large"></i>
						<span class="nav-label">Test Case Management</span> </a></li>
						
				<!-- <li class="mainLi"><a href="#" onclick="gotoLinkInMenu('documentlibrary')"><i
						class="fa fa-files-o"></i> <span class="nav-label">Document
							Library</span></a></li> -->
				
				
			</ul>

		</div>
	</nav>
</div>	
<script type="text/javascript">
/* $(function () {
	$("#custLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
}); */

	function gotoLinkInMenu(link)//link
	{
		$('body').addClass('white-bg');
		$("#barInMenu").removeClass('hidden');
		$("#wrapper").addClass("hidden");
		window.location.href = link; 
	}

</script>
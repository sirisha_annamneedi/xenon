<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div id="wrapper">
	<nav class="navbar-default navbar-static-side" role="navigation">
		<div class="sidebar-collapse">
			<ul class="nav metismenu" id="side-menu">
				<li class="nav-header">
					<div class="dropdown profile-element">
						<span> <img alt="image" class="img-circle"
							src="<%=request.getContextPath()%>/resources/img/profile_small.jpg" />
						</span> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <span
							class="clear"> <span class="block m-t-xs"> <strong
									class="font-bold">David Williams</strong>
							</span> <span class="text-muted text-xs block">Art Director <b
									class="caret"></b></span>
						</span>
						</a>
						<ul class="dropdown-menu animated fadeInRight m-t-xs">
							<li><a href="profile.html">Profile</a></li>
							<li><a href="contacts.html">Contacts</a></li>
							<li><a href="mailbox.html">Mailbox</a></li>
							<li class="divider"></li>
							<li><a href="login">Logout</a></li>
						</ul>
					</div>
					<div class="logo-element">IN+</div>
				</li>
				<c:if test="${fn:length(Model.projectTree.moduleList) == 0}">
				   <li><a href="" > <span>No Applications Found</span> </a></li>
                 </c:if>
				<c:forEach var="projects" items="${Model.projectTree.moduleList}">
				<li id="projectsLi_${projects.moduleId}"><a href="" ><span>${projects.moduletName}</span> </a></li>
				</c:forEach>
			</ul>

		</div>
	</nav>







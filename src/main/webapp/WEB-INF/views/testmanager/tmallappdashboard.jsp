<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="https://cdn.rawgit.com/emn178/Chart.PieceLabel.js/master/build/Chart.PieceLabel.min.js"></script>

<div class="md-skin">
	<div class="wrapper wrapper-content">
		<div class="row">
			<div class="col-lg-3">
				<div class="ibox float-e-margins dashlink"
					onclick="dashLink('totalApp')">
					<div class="ibox-title">
						<h5>Total Applications</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins text-success" id="projectCount">${Model.allCounts[0].projectCount}</h1>
					</div>
				</div>
				<!--  end .ibox -->
			</div>
			<!-- end col-md-3 -->
			<div class="col-lg-3">
				<div class="ibox float-e-margins dashlink"
					onclick="dashLink('totalMod')">
					<div class="ibox-title">
						<h5>Total Modules</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins text-success">${Model.allCounts[0].moduleCount}</h1>
					</div>
				</div>
				<!--  end .ibox -->
			</div>
			<!-- end col-md-3 -->
			<div class="col-lg-3">
				<div class="ibox float-e-margins dashlink"
					onclick="dashLink('totalSce0')">
					<div class="ibox-title">
						<h5>Total Scenarios</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins text-success">${Model.allCounts[0].scenarioCount}</h1>
					</div>
				</div>
				<!--  end .ibox -->
			</div>
			<!-- end col-md-3 -->
			<div class="col-lg-3">
				<div class="ibox float-e-margins dashlink"
					onclick="dashLink('totalTest')">
					<div class="ibox-title">
						<h5>Total Test Cases</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins text-success">${Model.allCounts[0].testCaseCount}</h1>
					</div>
				</div>
				<!--  end .ibox -->
			</div>
			<!-- end col-md-3 -->
		</div>
		<!-- end .row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Month-wise New Test Case Analysis</h5>
						<div class="pull-right"></div>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-12">
								<div>
									<canvas id="barChart" height  = "100"></canvas>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">			
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Test Case Status</h5>
						<div class="pull-right"></div>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-10">
								<div>
									<canvas id="myChart"></canvas>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-6">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Application-wise Test Case Count</h5>
						<div class="ibox-tools"></div>
					</div>
					<div class="ibox-content">
						<table class="table table-hover no-margins" id="buildListTable">
							<thead>
								<tr>
									<th class="hidden">Application Id</th>
									<th>Application Name</th>
									<th>Modules</th>
									<th>Scenarios</th>
									<th>Test Case</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="project" items="${Model.moduleTestCaseCounts}">
									<tr>
										<td class="hidden">${project.project_id}</td>
										<td class="textAlignment"
											data-original-title="${project.project_name}"
											data-container="body" data-toggle="tooltip"
											data-placement="right" class="issue-info">${project.project_name}</td>
										<td>${project.module_count}</td>
										<td>${project.sce_count}</td>
										<td>${project.test_count}</td>
									</tr>
								</c:forEach>


							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="ibox float-e-margins">
				<div class="client-detail" style="max-height: 405px">
						<div class="full-height-scroll">
					<!-- <div class="ibox-title">
						<h5>Recent Activities</h5>
						<div class="ibox-tools">
							<span class="label label-warning-light pull-right"> Recent
								&nbsp;${fn:length(Model.topActivities)} &nbsp;Activities</span>
						</div>
					</div>-->
					<div class="ibox-content">
						<div class="feed-activity-list">
							<c:forEach var="topActivities" items="${Model.topActivities}"
								varStatus="loop">
								<div class="feed-element">
									<a class="pull-left"> <img alt="image" class="img-circle"
										src="data:image/jpg;base64,${topActivities.user_image}">
									</a>
									<div class="media-body ">


										<c:set var="date"
											value="${fn:split(topActivities.activity_date,' ')}" />


										<c:choose>

											<c:when test="${topActivities.TimeDiff < 60 }">
												<strong class="pull-right">${topActivities.TimeDiff}
													min ago</strong>
											</c:when>

											<c:when
												test="${topActivities.TimeDiff >= 60 && topActivities.TimeDiff < 1440 }">
												<fmt:parseNumber var="hour" integerOnly="true" type="number"
													value="${topActivities.TimeDiff/60}" />
												<strong class="pull-right">${hour} hr ago </strong>
											</c:when>


											<c:when
												test="${topActivities.TimeDiff >= 1440 && topActivities.TimeDiff < 2880 }">
												<strong class="pull-right"> Yesterday : ${date[1]}
													${date[2]} </strong>
											</c:when>
											<c:otherwise>
												<strong class="pull-right">${topActivities.activity_date}
												</strong>
											</c:otherwise>
										</c:choose>

										<strong>${topActivities.first_name}&nbsp;${topActivities.last_name}
										</strong>${topActivities.activityState}.<br> <small
											class="text-muted">${topActivities.activity_date}</small>

										<div class="well">
											<div class="client-detail" style="max-height: 80px">
												<div class="full-height-scroll">
													${topActivities.activity_desc}
												</div>
											</div>
										</div>


									</div>
								</div>
							</c:forEach>

						</div>
					</div>
					</div>
				</div>
				</div>
			</div>
		</div>


	</div>
	<!-- moduleTestCaseCounts -->
	<!--  end .wrapper -->
</div>
<!-- end .md-skin -->

<script type="text/javascript">
	$(window)
			.load(
					function() {

						$('body').removeClass("white-bg");
						$("#barInMenu").addClass("hidden");
						$("#wrapper").removeClass("hidden");

						var projectwiseTestCount = '${Model.projectwiseCount}';
						projectwiseTestCount = JSON.parse(projectwiseTestCount);

						var data = [];
						var projects = '${Model.projects}';
						var graphLabels = [];

						projects = JSON.parse(projects);

						for (var j = 0; j < projectwiseTestCount.length; j++) {
							graphLabels.push(projectwiseTestCount[j].monthyear);
						}

						var UniqueGraphLabels = graphLabels.filter(function(
								itm, i, graphLabels) {
							return i == graphLabels.indexOf(itm);
						});

						for (var i = 0; i < projects.length; i++) {
							var monthlycount = [];
							for (var k = 0; k < UniqueGraphLabels.length; k++) {
								monthlycount.push(0);
							}
							
							
							for (var j = 0; j < projectwiseTestCount.length; j++) {

								if (projects[i].project_id == projectwiseTestCount[j].project) {
									
									for (var k = 0; k < UniqueGraphLabels.length; k++) {
										
										if (UniqueGraphLabels[k] == projectwiseTestCount[j].monthyear) {
											monthlycount[k] = projectwiseTestCount[j].count;
											break;
										}
										
									}
									
								}
								
							}

							var dataset;
							dataset = {
								label : projects[i].project_name,
								hoverBackgroundColor : projects[i].color,
								backgroundColor : projects[i].color,
								data : monthlycount

							}
							data.push(dataset);
						}

						var barData = {
							labels : UniqueGraphLabels,
							datasets : data
						};
						
						console.log(UniqueGraphLabels);

						var barOptions = {
							scaleBeginAtZero : true,
							scaleShowGridLines : false,
							scaleGridLineColor : "rgba(0,0,0,.05)",
							scaleGridLineWidth : 0,
							barShowStroke : false,
							barStrokeWidth : 2,
							barValueSpacing : 5,
							barDatasetSpacing : 1,
							responsive : true,
							pieceLabel: {
						         render: 'value' //show values
						      },
						
							scales: {
                                xAxes: [{
                            		
                                    ticks: {
                                        callback: function(value) {
                                            return value.substr(0, 3)+ " " + value.substr(6,7);//truncate
                                        },
                                    }
                                }],
                                yAxes: [{
                                        ticks: {
                                            min: 0,
                                            callback: function(value) {if (value % 1 === 0) {return value;}}
                                        }
                                }]
                            },
                            tooltips: {
                                enabled: true,
                                mode: 'label',
                                callbacks: {
                                    title: function(tooltipItems, data) {
                                        var idx = tooltipItems[0].index;
                                        return data.labels[idx];//title
                                    }
                                }
                            }
                    }

						

						var ctx = document.getElementById("barChart");
						var myNewChart = new Chart(ctx, {
							type : 'bar',
							data : barData,
							options : barOptions
						});
						
						ctx.onclick = function(evt) {
						      var activePoints = myNewChart.getElementsAtEvent(evt);
						      if (activePoints[0]) {
						        var chartData = activePoints[0]['_chart'].config.data;
						        var idx = activePoints[0]['_index'];

						        var label = chartData.labels[idx];
						        var value = chartData.datasets[0].data[idx];
						       
						        var str1 = "test";
						      	var str2 = label.toString();
						        var res = str1.concat(str2);
						       
						      	dashLink(res);
						      }
						    };
						sessionStorage.removeItem('currentModuleId');
					});

	$(function() {

		//change selected project name in header
		if ($(window).width() >= 768) {
			// is desktop
			$('.firstAppName').html(
					'All Applications <span class="caret"></span>');
		} else {
			// is phone or tablet
			$('.firstAppName').html('All<span class="caret"></span>');
		}

		
		$('#buildListTable').DataTable({
			"order": [[0, "desc"]],
			"paging" : true,
			"lengthChange" : true,
			"searching" : false,
			"info" : true,
			"autoWidth" : true,
			"aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
			"iDisplayLength": 5,
			"pageLength": 5
		});
		
	});
	function dashLink(link) {

		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");

		var posting = $.post('setTmLinkSession', {
			filterText : link,
			dashboard : "all"
		});

		posting.done(function(data) {
			window.location.href = "tmdashboardlink";
		});
	}

	function getRandomColor() {
		var letters = '0123456789ABCDEF'.split('');
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}
	
	var data = {
			  datasets: [{
			    data: [${Model.allCounts[0].testcaseDraft},${Model.allCounts[0].testcaseReadyForReview}, ${Model.allCounts[0].testcaseReviewInProgress}, ${Model.allCounts[0].testcaseReviewCompleted},${Model.allCounts[0].testcaseFinal}],
			    backgroundColor: [
			    	"rgb(46, 204, 113)",
				      "rgb(241, 148, 138)",
				      "rgb(133, 193, 233)",
				      "rgb(195, 155, 211)",
				      "rgb(170, 133, 233)"
			    ]
			  }],
			  labels: [
				  "Draft",
				    "In Review",
				    "In Progress Review",
				    "Review Completed",
				    "Final"
			  ]
			};


			$(document).ready(
			  function() {
			    var canvas = document.getElementById("myChart");
			    var ctx = canvas.getContext("2d");
			    var myNewChart = new Chart(ctx, {
			      type: 'doughnut',
			      data: data,
			      options: {
				      pieceLabel: {
				         render: 'value' //show values
				      },
				  	legend: {
						display : true,
						position : 'right'
			        },
				   }
			      
			    });

			    canvas.onclick = function(evt) {
			      var activePoints = myNewChart.getElementsAtEvent(evt);
			      if (activePoints[0]) {
			        var chartData = activePoints[0]['_chart'].config.data;
			        var idx = activePoints[0]['_index'];

			        var label = chartData.labels[idx];
			        var value = chartData.datasets[0].data[idx];

			        if (label == "Draft")
			        {
			        	dashLink('totalDraft');
			       	}
			        else if (label == "In Review")
			        {
			        	dashLink('review');
			        }
			        else if (label == "In Progress Review")
			        {
			        	dashLink('progressReview');
			        }
			        else if (label == "Review Completed")
			        {
			        	dashLink('reviewcompleted');
			        }
			        else
			        {
			        	dashLink('final');
			        }
			      }
			    };
			  }
			);
			
</script>
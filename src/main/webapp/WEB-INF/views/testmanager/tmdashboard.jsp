<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Fixed chart issue -->
<%-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>	
<script	src="<%=request.getContextPath()%>/resources/js/plugins/chartJs/chartjs-plugin-labels.min.js"></script>
 --%>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
   var $x = jQuery.noConflict();
</script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/plugins/c3/c3.min.css">


<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">

		<div class="col-lg-8">
			<h2>
				<ol class="breadcrumb">
					<li><strong>Test Manager Dashboard</strong></li>

					<li>${UserCurrentProjectName}</li>

				</ol>
			</h2>
		</div>

	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<%-- <div class="row">
				<div class="col-lg-3">
			<div class="ibox float-e-margins dashlink" onclick="dashLink('totalApp')">
				<div class="ibox-title">
					<h5>Total Applications</h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins" id="projectCount">${Model.allCounts[0].projectCount}</h1>
				</div>
			</div>
			<!--  end .ibox -->
		</div>
			<!-- end col-md-3 -->
			<div class="col-lg-4">
				<div class="ibox float-e-margins dashlink"
					onclick="dashLink('totalMod')">
					<div class="ibox-title">
						<h5>Total Modules</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins text-success">${Model.allCounts[0].moduleCount}</h1>
					</div>
				</div>
				<!--  end .ibox -->
			</div>
			<!-- end col-md-3 -->
			<div class="col-lg-4">
				<div class="ibox float-e-margins dashlink"
					onclick="dashLink('totalSce0')">
					<div class="ibox-title">
						<h5>Total Scenarios</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins text-success">${Model.allCounts[0].scenarioCount}</h1>
					</div>
				</div>
				<!--  end .ibox -->
			</div>
			<!-- end col-md-3 -->
			<div class="col-lg-4">
				<div class="ibox float-e-margins dashlink"
					onclick="dashLink('totalTest0')">
					<div class="ibox-title">
						<h5>Total Test Cases</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins text-success">${Model.allCounts[0].testCaseCount}</h1>
					</div>
				</div>
				<!--  end .ibox -->
			</div>
			<!-- end col-md-3 -->
		</div> --%>
		<!-- end .row -->


		<div class="graph-dashboard">
			<div class="col-md-12">
				<div class="gd-title">Application Wise Test Case Status</div>
				<div class="row">
					<div class="col-md-6">
						<div class="testc-status mt-20">
							<canvas id="myChartt" width="400" height="300"></canvas>
							<table class="table table-bordered">
								<thead>
									<tr>
										<th scope="col">Test Count</th>
										<th scope="col">Draft</th>
										<th scope="col">In Review</th>
										<th scope="col">In Progress Review</th>
										<th scope="col">Review Completed</th>
										<th scope="col">Final</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>${Model.allCounts[0].testcaseDraft+Model.allCounts[0].testcaseReadyForReview + Model.allCounts[0].testcaseReviewInProgress+Model.allCounts[0].testcaseReviewCompleted+Model.allCounts[0].testcaseFinal}</td>
										<td>${Model.allCounts[0].testcaseDraft}</td>
										<td>${Model.allCounts[0].testcaseReadyForReview}</td>
										<td>${Model.allCounts[0].testcaseReviewInProgress}</td>
										<td>${Model.allCounts[0].testcaseReviewCompleted}</td>
										<td>${Model.allCounts[0].testcaseFinal}</td>

									</tr>


								</tbody>
							</table>

						</div>
					</div>




					<div class="col-md-6">
						<div class="mt-20">
							<div id="piee"></div>
							<!-- <div id="morris-donut-chart"></div> -->

						</div>
						<div class="counter mt-20">

							<p class="counter-count">${Model.allCounts[0].testCaseCount}</p>
							<h3 class="testcases-p">Total Test Cases</h3>

						</div>
					</div>
				</div>



			</div>
		</div>




		<div class="graph-dashboard">
			<div class="col-md-12">
				<div class="gd-title">Type of Test Cases</div>
				<div class="row">
					<div class="col-md-6">
						<div class="testc-status mt-20">
							<div class="mt-20">
								<div id="testCaseType"></div>
							</div>
							<%-- <canvas id="AutoManualTestCaseChart"></canvas> --%>
						</div>
					</div>
					<div class="col-md-6">

						<div class="mt-80">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">Count</th>
										<th scope="col">Percentage</th>
										<!--  <th scope="col">In Review</th>
      <th scope="col">In Progress Review</th>
      <th scope="col">Review Completed</th>
      <th scope="col">Final</th> -->
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row">Manual</th>
										<td>${Model.allCounts[0].AMTestCase[0].test_case_count}</td>
										<td>${Model.allCounts[0].AMTestCase[0].test_case_count/(Model.allCounts[0].AMTestCase[0].test_case_count+Model.allCounts[0].AMTestCase[1].test_case_count+Model.allCounts[0].AMTestCase[2].test_case_count)*100}
											%</td>

									</tr>
									<tr>
										<th scope="row">Automation</th>
										<td>${Model.allCounts[0].AMTestCase[1].test_case_count}</td>
										<td>${Model.allCounts[0].AMTestCase[1].test_case_count/(Model.allCounts[0].AMTestCase[0].test_case_count+Model.allCounts[0].AMTestCase[1].test_case_count+Model.allCounts[0].AMTestCase[2].test_case_count)*100}
											%</td>

									</tr>
									<tr>
										<th scope="row">Both</th>
										<td>${Model.allCounts[0].AMTestCase[2].test_case_count}</td>
										<td>${Model.allCounts[0].AMTestCase[2].test_case_count/(Model.allCounts[0].AMTestCase[0].test_case_count+Model.allCounts[0].AMTestCase[1].test_case_count+Model.allCounts[0].AMTestCase[2].test_case_count)*100}
											%</td>
									</tr>


								</tbody>
							</table>

						</div>
					</div>
				</div>



			</div>
		</div>

		<div class="graph-dashboard">
			<div class="col-md-12">
				<div class="gd-title">Module Wise Test Case Status</div>
				<div class="row">
					<div class="col-md-6">
						<div class="testc-status mt-20">
							<canvas id="moduleWiseChart" width="400" height="300"></canvas>

							<table class="table table-bordered " id="tableData">
								<thead>
									<tr>
										<td><b>Module Name</b></td>
										<td><b>Total Sc</b></td>
										<td><b>Total TC</b></td>
										<td><b>Draft</b></td>
										<td><b>Ready Review</b></td>
										<td><b>In-Progress Review</b></td>
										<td><b>Rework</b></td>
										<td><b>Obsolete</b></td>
										<td><b>Future</b></td>
										<td><b>Final</b></td>
									</tr>
								</thead>
								<tbody>
									<c:set var="sCountTotal" value="${0}" />
									<c:set var="TcSCount" value="${0}" />
									<c:set var="Draft" value="${0}" />
									<c:set var="ReadyForReview" value="${0}" />
									<c:set var="ReadyForProgress" value="${0}" />
									<c:set var="Rework" value="${0}" />
									<c:set var="Obsolete" value="${0}" />
									<c:set var="Future" value="${0}" />
									<c:set var="Final" value="${0}" />
									<c:forEach var="tcs"
										items="${Model.allCounts[0].totalTestCountByExecutionType}">
										<tr>
											<td>${tcs.module_name}</td>
											<td>${tcs.sCount}</td>
											<td>${tcs.Draft + tcs.ReadyForReview + tcs.ReadyForProgress + tcs.Rework + tcs.Obsolete + tcs.Future + tcs.Final}
											</td>
											<td>${tcs.Draft}</td>
											<td>${tcs.ReadyForReview}</td>
											<td>${tcs.ReadyForProgress}</td>
											<td>${tcs.Rework}</td>
											<td>${tcs.Obsolete}</td>
											<td>${tcs.Future}</td>
											<td>${tcs.Final}</td>
											<c:set var="sCountTotal" value="${sCountTotal+tcs.sCount}" />
											<c:set var="TcSCount"
												value="${TcSCount+tcs.Draft + tcs.ReadyForReview + tcs.ReadyForProgress + tcs.Rework + tcs.Obsolete + tcs.Future + tcs.Final}" />
											<c:set var="Draft" value="${Draft+tcs.Draft}" />
											<c:set var="ReadyForReview"
												value="${ReadyForReview+tcs.ReadyForReview}" />
											<c:set var="ReadyForProgress"
												value="${ReadyForProgress+tcs.ReadyForProgress}" />
											<c:set var="Rework" value="${Rework+tcs.Rework}" />
											<c:set var="Obsolete" value="${Obsolete+tcs.Obsolete}" />
											<c:set var="Future" value="${Future+tcs.Future}" />
											<c:set var="Final" value="${Final+tcs.Final}" />
										</tr>
									</c:forEach>
									<tr>
										<td><b>Grand Total</b></td>
										<td>${sCountTotal}</td>
										<td>${TcSCount}</td>
										<td>${Draft}</td>
										<td>${ReadyForReview}</td>
										<td>${ReadyForProgress}</td>
										<td>${Rework}</td>
										<td>${Obsolete}</td>
										<td>${Future}</td>
										<td>${Final}</td>
									</tr>
								</tbody>
							</table>

						</div>
					</div>
					<div class="col-md-6">
						<div class="mt-20">
							<div id="moduleWisePie"></div>
							<!-- <div id="morris-donut-chart"></div> -->

						</div>
						<div class="counter mt-20">

							<p class="counter-count">${Model.allCounts[0].moduleCount}</p>
							<h3 class="testcases-p">Total Modules</h3>

						</div>
					</div>
				</div>



			</div>
		</div>

		<div class="row" style="display: none;">
			<%-- 		<div class="ibox float-e-margins">

			<div class="ibox-content">
				  <div class="row">
					<div class="col-lg-12">

						<div class="ibox-title">
							<h5>Month-wise New Test Case Analysis</h5>
							<div class="pull-right"></div>
						</div>

						<div class="row">
							<div class="col-lg-12">
								<canvas id="moduleBarChart"></canvas>
							</div>
						</div>
					</div> 

					<div class="col-lg-3">
						<div class="ibox-title">
							<h5>Module-wise Open Bugs Count</h5>
							<div class="pull-left"></div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<canvas id="doughnutChart" height="400"></canvas>
							</div>
						</div>
					</div>
				</div> 
			</div>
		</div>
		--%>
			<div class="col-md-6">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Test Case Status</h5>
						<div class="pull-right"></div>
					</div>
					<div class="ibox-content">
						<canvas id="TestCaseChart"></canvas>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Manual/Automation Test case count</h5>
						<div class="pull-right"></div>
					</div>
					<div class="ibox-content">
						<canvas id="AutoManualTestCaseChart"></canvas>
					</div>
				</div>
			</div>
		</div>



		<div class="row" style="display: none;">

			<%-- 						<div class="ibox float-e-margins">
							<div class="ibox-content">
								  <div class="row">
									<div class="col-lg-12">

										<div class="ibox-title">
											<h5>Month-wise New Test Case Analysis</h5>
											<div class="pull-right"></div>
										</div>
							 
									<div class="row">
											<div class="col-lg-12">
												<canvas id="moduleBarChart"></canvas>
											</div>
										</div>
									</div> 

									<div class="col-lg-3">
										<div class="ibox-title">
											<h5>Module-wise Open Bugs Count</h5>
											<div class="pull-left"></div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<canvas id="doughnutChart" height="400"></canvas>
											</div>
										</div>
									</div>
									</div>
							</div>
						</div> --%>



			<div class="col-md-6">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Module wise Scenario Status</h5>
						<div class="pull-right"></div>
					</div>
					<div class="ibox-content">
						<canvas id="myModuleChart"></canvas>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Components wise Scenarios</h5>
						<div class="pull-right"></div>
					</div>
					<div class="ibox-content">
						<canvas id="ScenarioChart"></canvas>
					</div>
				</div>
			</div>
		</div>





		<c:set var="string1" value='${Model.filterText}' />
		<script>
		var SceLabel = [];
		var TCSCount = [];
		var ComID = [];
		var SCName = [];
		var AutoManualCount = [];
		var ModLabel = [];
		var modSCCount = [];
		var MID = [];
		var Mname = [];
		var EXEType = [];
		var comp_component_id = [];
		var comp_scenario_id = [];
		</script>
		<c:forEach var="SName" items="${Model.allCounts[0].ComponentName}">
			<script>
				 var temp = '${SName.component_name}';
				 var idS = '${SName.component_id}';
				 SceLabel.push(temp);
				 var tcCount = 0;
				</script>
			<c:forEach var="data2"
				items="${Model.allCounts[0].ComponentSceCount}">
				<script>
				 		var idT = '${data2.component_id}';
// 				 		var n = '${data2.scenario_name}';
						 if (idS == idT)
				 		 {
							tcCount = '${data2.scenario_count}';
							
				 		 }
						 //SCName.push(n);
					</script>
			</c:forEach>
			<script>
				ComID.push(idS);
				TCSCount.push(tcCount);
				</script>

		</c:forEach>
		<c:forEach var="CSID" items="${Model.allCounts[0].CompSceID}">
			<script>
				var temp2 = '${CSID.scenario_id}';
				var temp3 = '${CSID.component_id}'
				 comp_scenario_id.push(temp2);
				 comp_component_id.push(temp3);
				</script>
		</c:forEach>
		<c:forEach var="d_val" items="${Model.allCounts[0].AMTestCase}">

			<script>
				 		
				 		var e = '${d_val.execution_type}';
				 		var t = 0;
				 		t = '${d_val.test_case_count}';
			 			AutoManualCount.push(t);
			 			EXEType.push(e);
				</script>


		</c:forEach>
		<c:forEach var="M_Name" items="${Model.allCounts[0].moduleName}">
			<script>
				 var temp = '${M_Name.module_name}';
				 var idS = '${M_Name.module_id}';
				 ModLabel.push(temp);
				 var tcCount = 0;
				</script>
			<c:forEach var="M_Count"
				items="${Model.allCounts[0].moduleScenarioCount}">
				<script>
				 		var idT = '${M_Count.module_id}';
				 		var n = '${M_Count.module_name}';
						 if (idS == idT)
				 		 {
							tcCount = '${M_Count.countOfScenario}';
							
				 		 }
						 MID.push(idT);
						 Mname.push(n);
					</script>
			</c:forEach>
			<script>
				modSCCount.push(tcCount);
				</script>
		</c:forEach>

		<!--  <div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins" style="background-color: white;">
					<div class="ibox-title">
						<h5>Application-wise Test Case Count</h5>
					</div>
					<div class="ibox-content">
						<div class="">
							<table
								class="table table-striped table-bordered table-hover dataTables-example"
								id="appListTable">
								<thead>
									<tr>
										<th class="hidden">Application Id</th>
										<th>Application Name</th>
										<th>Modules</th>
										<th>Scenarios</th>
										<th>Test Case</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="project" items="${Model.moduleTestCaseCounts}">
										<tr>
											<td class="hidden">${project.project_id}</td>
											<td class="textAlignment"
												data-original-title="${project.project_name}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${project.project_name}</td>
											<td>${project.module_count}</td>
											<td>${project.sce_count}</td>
											<td>${project.test_count}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- <div class="col-lg-6">
				<div class="ibox float-e-margins">
					<div class="client-detail" style="max-height: 405px">
						<div class="full-height-scroll">
							<div class="ibox-title">
								<h5>Recent Activities</h5>
								<div class="ibox-tools">
									<span class="label label-warning-light pull-right">
										Recent &nbsp;${fn:length(Model.topActivities)}
										&nbsp;Activities</span>
								</div>
							</div>
							<div class="ibox-content">
								<div class="feed-activity-list">
									<c:forEach var="topActivities" items="${Model.topActivities}"
										varStatus="loop">
										<div class="feed-element">
											<a class="pull-left"> <img alt="image" class="img-circle"
												src="data:image/jpg;base64,${topActivities.user_image}">
											</a>
											<div class="media-body ">


												<c:set var="date"
													value="${fn:split(topActivities.activity_date,' ')}" />


												<c:choose>

													<c:when test="${topActivities.TimeDiff < 60 }">
														<strong class="pull-right">${topActivities.TimeDiff}
															min ago</strong>
													</c:when>

													<c:when
														test="${topActivities.TimeDiff >= 60 && topActivities.TimeDiff < 1440 }">
														<fmt:parseNumber var="hour" integerOnly="true"
															type="number" value="${topActivities.TimeDiff/60}" />
														<strong class="pull-right">${hour} hr ago </strong>
													</c:when>


													<c:when
														test="${topActivities.TimeDiff >= 1440 && topActivities.TimeDiff < 2880 }">
														<strong class="pull-right"> Yesterday </strong>
													</c:when>
													<c:otherwise>
														<strong class="pull-right">${topActivities.activity_date}
														</strong>
													</c:otherwise>
												</c:choose>

												<strong>${topActivities.first_name}&nbsp;${topActivities.last_name}
												</strong>${topActivities.activityState}.<br> <small
													class="text-muted">${topActivities.activity_date}</small>

												<div class="well">
													<div class="client-detail" style="max-height: 80px">
														<div class="full-height-scroll">
															${topActivities.activity_desc}</div>
													</div>
												</div>


											</div>
										</div>
									</c:forEach>

								</div>

							</div>
						</div>
					</div>
				</div>

			</div>
		</div> -->


	</div>
	<!-- moduleTestCaseCounts -->
	<!--  end .wrapper -->
</div>
<script type="text/javascript">
var modules = '${Model.moduleDetails}';
var moduleName =[];
	$(window)
			.load(
					function() {
						
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
		
		var moduleWiseCount = '${Model.moduleWiseCount}';
		moduleWiseCount = JSON.parse(moduleWiseCount);
		var dataBar = [];
		var modules = '${Model.moduleDetails}';
		var monthList=${Model.monthList};
		var currentMonth=${Model.currentMonth};
		currentMonth=currentMonth-1;
		modules = JSON.parse(modules);
		 for (var i = 0; i < modules.length; i++) {
			 
			 moduleName [i] = modules[i].module_name;
			var monthlycount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0 ];
			for (var j = 0; j < moduleWiseCount.length; j++) {
				
				if (moduleWiseCount[j].module_id == modules[i].module_id ) {
					monthlycount[(moduleWiseCount[j].month) - 1] = moduleWiseCount[j].tcCount;
				}
			}
			for(k=0;k<monthlycount.length;k++)
			{
				var flag=false;
				if(k>=currentMonth)
					{
						var counter=-1;
						var monthlycountNew = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
							 					0, 0 ];
						for(j=currentMonth;j<monthlycount.length;j++)
						{
							counter++;
							monthlycountNew[counter]=monthlycount[j];
						}
						for(l=0;l<currentMonth;l++)
						{
							counter++;
							monthlycountNew[counter]=monthlycount[l];
						}
						flag=true;
					}
				if (flag) {
					break;
				}
			}
			
			var dataset;
			dataset = {
				label : modules[i].module_name,
				backgroundColor: modules[i].color,
		        hoverBackgroundColor: modules[i].color,
				data : monthlycountNew
			}
			dataBar.push(dataset);
		} 
		 
		var barData = {
			labels : monthList,
			datasets : dataBar
		};
		
	
		var barOptions = {
				scaleBeginAtZero: true,
		        scaleShowGridLines: true,
		        scaleGridLineColor: "rgba(0,0,0,.05)",
		        scaleGridLineWidth: 1,
		        barShowStroke: true,
		        barStrokeWidth: 2,
		        barValueSpacing: 5,
		        barDatasetSpacing: 1,
		        responsive: true,
		        scales: {
		        	xAxes: [{
		                ticks: {
		                    callback: function(value) {
		                        return value.substr(0, 3)+ " " + value.substr(6,7);//truncate
		                    },
		                }
		            }],
		            yAxes: [{
		                    ticks: {
		                        min: 0,
		                        callback: function(value) {if (value % 1 === 0) {return value;}}
		                    }
		            }]
		        },
		        tooltips: {
		            enabled: true,
		            mode: 'label',
		            callbacks: {
		                title: function(tooltipItems, data) {
		                    var idx = tooltipItems[0].index;
		                    return data.labels[idx];//title
		                }
		            }
		        },
		}

		var ctx = document.getElementById("moduleBarChart");
		var myNewChart = new Chart(ctx, {
		    type: 'bar',
		    data: barData,
		    options: barOptions
		});
			
						sessionStorage.removeItem('currentModuleId');
						
						//var dcData = '${Model.donutChartData}';
						var donutChartData ='${Model.donutChartData}';
						donutChartData = JSON.parse(donutChartData);
						var donutLabels = [];
						var donutData = [];
						var donutColors = [];
						for (var i = 0; i < donutChartData.length; i++) {
							donutLabels.push(donutChartData[i].module_name);
							donutData.push(donutChartData[i].module_wise_bug_count);
							donutColors.push(donutChartData[i].color);
						}

						var doughnutData = {
					    labels: donutLabels,
					    datasets: [
					        {
					            data: donutData,
					            backgroundColor: donutColors,
					            hoverBackgroundColor: donutColors
					        }]
						};
							
						var doughnutOptions = {
							segmentShowStroke : true,
							segmentStrokeColor : "#fff",
							segmentStrokeWidth : 2,
							percentageInnerCutout : 45, // This is 0 for Pie charts
							animationSteps : 100,
							animationEasing : "easeOutBounce",
							animateRotate : true,
							animateScale : true,
							responsive : true,
							legend: {
								position : 'top'
					        }
						};
						
						var ctx = document.getElementById("doughnutChart");
						var myDoughnutChart = new Chart(ctx, {
						    type: 'doughnut',
						    data: doughnutData,
						    options: doughnutOptions
						});
					});
		
	
	function dashLink(link) {
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		var posting = $.post('setTmLinkSession', {
			filterText : link,
			dashboard : "projectWise"
		});

		posting.done(function(data) {
			window.location.href = "tmdashboardlink";
		});
	}
	
	$(document).ready(function() {
		
		$('#appListTable').DataTable({
			"order": [[0, "desc"]],
			"paging" : true,
			"lengthChange" : true,
			"searching" : false,
			"info" : true,
			"autoWidth" : true,
			"aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
			"iDisplayLength": 5,
			"pageLength": 5
		});
		
	});
	
	
	$(function() {
		var appFlag='${Model.appFlag}';
		if(appFlag=='true'){
			if ($(window).width() >= 768) {
				// is desktop
				$('.firstAppName').html(
						'All Applications <span class="caret"></span>');
			} else {
				// is phone or tablet
				$('.firstAppName').html('All<span class="caret"></span>');
			}
		}
		
	});
	
	
	var ChartColor = ["#1EC0FB","#2ECC71","#F1948A", "#85C1E9","#C39BD3","#AA85E9", "#2EC7CC","#9C2ECC","#BBFF33","#CC492E","#EA185E","#827017","#F76E85","#FFDD33","#FF7733"];

			$(document).ready(
			  function() {
				  
					var data = {
							  datasets: [{
							    data: [${Model.allCounts[0].testcaseDraft},${Model.allCounts[0].testcaseReadyForReview}, ${Model.allCounts[0].testcaseReviewInProgress}, ${Model.allCounts[0].testcaseReviewCompleted},${Model.allCounts[0].testcaseFinal}],
							    backgroundColor: ChartColor,
							  }],
							  labels: [
								  "Draft",
								    "In Review",
								    "In Progress Review",
								    "Review Completed",
								    "Final"
							  ]
							};  
				  
			    var canvas = document.getElementById("TestCaseChart");
			    var ctx = canvas.getContext("2d");
			    var myNewChart = new Chart(ctx, {
			      type: 'doughnut',
			      data: data,
			      options: {
				      pieceLabel: {
				         render: 'value' //show values
				      },
				      legend: {
				            display: true,
				            position:'right',
				   }
			      }
			      
			    });

			    canvas.onclick = function(evt) {
			      var activePoints = myNewChart.getElementsAtEvent(evt);
			      if (activePoints[0]) {
			        var chartData = activePoints[0]['_chart'].config.data;
			        var idx = activePoints[0]['_index'];

			        var label = chartData.labels[idx];
			        var value = chartData.datasets[0].data[idx];

			        if (label == "Draft")
			        {
			        	dashLink('totalDraft');
			       	}
			        else if (label == "In Review")
			        {
			        	dashLink('review');
			        }
			        else if (label == "In Progress Review")
			        {
			        	dashLink('progressReview');
			        }
			        else if (label == "Review Completed")
			        {
			        	dashLink('reviewcompleted');
			        }
			        else
			        {
			        	dashLink('final');
			        }
			      }
			    };
			  }
			);
			
			
			// for module sprint chart
			$(document).ready(
			  function() {
				  
			
					var data = {
							  datasets: [{
							    data: modSCCount,
							    
							    backgroundColor:ChartColor,
							  }],
							  labels: subStringList(ModLabel),
							};  
					     
				 
			    var canvas = document.getElementById("myModuleChart");
			    var ctx = canvas.getContext("2d");
			    var myNewChart = new Chart(ctx, {
			      type: 'doughnut',
			      data: data,
			      options: {
				      pieceLabel: {
				         render: 'value' //show values
				      },
				      legend: {
				            display: true,
				            position:'right',
				   }
				   }
			      
			    });

			    canvas.onclick = function(evt) {
			      var activePoints = myNewChart.getElementsAtEvent(evt);
			      if (activePoints[0]) {
			        var chartData = activePoints[0]['_chart'].config.data;
			        var idx = activePoints[0]['_index'];

			        var label = chartData.labels[idx];
			        var value = chartData.datasets[0].data[idx];

			        var temp;
			        for (var i = 0 ; i < ModLabel.length; i++)
			        {
			        	if (ModLabel[i] == label)
			        	{
			        		temp = MID[i];
			        	}
			        }
			        var str2 = 'totalSce';
			        var res = str2.concat(temp);
			       dashLink(res);
			    }
			    };
			  }
			);
	
			$(document).ready(
					  function() {
						  
					 	var data = {
									  datasets: [{
									    data: TCSCount,
									    backgroundColor: ChartColor,
									  }],
									  labels: subStringList(SceLabel),
									};  
						  
					    var canvas = document.getElementById("ScenarioChart");
					    var ctx = canvas.getContext("2d");
					    var myNewChart = new Chart(ctx, {
					      type: 'doughnut',
					      data: data,
					      options: {
						      pieceLabel: {
						         render: 'value' //show values
						      },
						      legend: {
						            display: true,
						            position:'right',
						   }
						   }
					      
					    });

					    canvas.onclick = function(evt) {
					    	var activePoints = myNewChart.getElementsAtEvent(evt);
						      if (activePoints[0]) {
						        var chartData = activePoints[0]['_chart'].config.data;
						        var idx = activePoints[0]['_index'];

						        var label = chartData.labels[idx];
						        var value = chartData.datasets[0].data[idx];
								
						        var CS = [];
						        for (var i = 0 ; i < SceLabel.length; i++)
						        {
						        	if (SceLabel[i] == label)
						        	{
						        		for(var a = 0; a < comp_component_id.length; a++)
						        		{
						        			if(ComID[i] == comp_component_id[a])
						        			{
						        				CS.push(comp_scenario_id[a]);
						        			}
						        		}
						        	}
						        }
						        var str2 = 'totalSce';
						        var Str3 = CS.toString();
						        var res = str2.concat(Str3);
						        dashLink(res);
						      }
					    };
					  }
					);	
			
			// for Execution wise test case count chart
			
			$(document).ready(
					  function() {
						  
						  var chartData = [];
						  var bool = false;
						  
						  for(var i = 1; i < 4; i++)
						  {
								for(var x = 0; x < EXEType.length; x++)
								{
									bool = false;
									if(EXEType[x] == i ) // EXEType = 1
									{
										chartData.push(AutoManualCount[x]);
										bool = true;
										break;
									}
								}
								
								if(bool == false)
								{
									chartData.push(0);
								}
						  }
						  
							var data = {
									  datasets: [{
									    data: chartData,
									    backgroundColor: ChartColor,
									  }],
									  labels: [
										  "Manual",
										  "Automation",
										  "Both"
									  ],
									};  
						  
					    var canvas = document.getElementById("AutoManualTestCaseChart");
					    var ctx = canvas.getContext("2d");
					    var myNewChart = new Chart(ctx, {
					      type: 'doughnut',
					      data: data,
					      options: {
						      pieceLabel: {
						         render: 'value' //show values
						      },
						      legend: {
						            display: true,
						            position:'right',
						   		}
						   }
					      
					    });
					    
					    
					    
					    $('#AutoManualTestCaseChart1').append(chartData);

					    canvas.onclick = function(evt) {
					      var activePoints = myNewChart.getElementsAtEvent(evt);
					      if (activePoints[0]) {
					        var chartData = activePoints[0]['_chart'].config.data;
					        var idx = activePoints[0]['_index'];

					        var label = chartData.labels[idx];
					        var value = chartData.datasets[0].data[idx];
					       
					        	if (label == "Manual")
					        	{
					        		dashLink('1');
					        	}
					        	else if (label == "Automation")
					        	{
					        		dashLink('two');
					        	}
					        	else
					        	{
					        		dashLink('3');
					        	}
					        
					      }
					    };
					  }
					);	
			
function subStringList(inputList){
		for(var t = 0; t < inputList.length; t++)
			{
				if (inputList[t].length >= 22)
					inputList[t] = inputList[t].substring(0,18).concat("...");
			}
		return inputList;
}	
</script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/d3/d3.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/c3/c3.min.js"></script>
<script>
var ctx = document.getElementById("myChartt");
var myChartt = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Draft", "In Review", "In Progress Review", "Review Completed", "Final"],
        datasets: [{
        	label: "Test Count",
        	 data: [${Model.allCounts[0].testcaseDraft},${Model.allCounts[0].testcaseReadyForReview}, ${Model.allCounts[0].testcaseReviewInProgress}, ${Model.allCounts[0].testcaseReviewCompleted},${Model.allCounts[0].testcaseFinal}],
            backgroundColor: [
		        'rgba(54, 162, 235, 0.9)',
		        'rgba(54, 162, 235, 0.9)',
		        'rgba(54, 162, 235, 0.9)',
		        'rgba(54, 162, 235, 0.9)',
		        'rgba(54, 162, 235, 0.9)'
		      ],
		      borderColor: [
		        'rgba(54, 162, 235, 1)',
		        'rgba(54, 162, 235, 1)',
		        'rgba(54, 162, 235, 1)',
		        'rgba(54, 162, 235, 1)',
		        'rgba(54, 162, 235, 1)'
		      ],
		      borderWidth: 1,
        }]
    },
    options: {
    		"hover": {
        	"animationDuration": 1
        },
        title: {
            display: true,
            text: '',
           
          },
        "animation": {
        	"duration": 1,
						"onComplete": function () {
							var chartInstance = this.chart,
								ctx = chartInstance.ctx;
							
							ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
							ctx.textAlign = 'center';
							ctx.textBaseline = 'bottom';
							this.data.datasets.forEach(function (dataset, i) {
								var meta = chartInstance.controller.getDatasetMeta(i);
								meta.data.forEach(function (bar, index) {
									var data = dataset.data[index];                            
									ctx.fillText(data, bar._model.x, bar._model.y - 5);
								});
							});
						}
        },
    		legend: {
        	"display": false
        },
        tooltips: {
        	"enabled": true
         },
        scales: {
            yAxes: [{
            		display: true,            		
            		gridLines: {
                	display : true 
                },
                
                ticks: {
                		display: true,
                    beginAtZero:true,
                    fontColor: "rgba(0,0,0,0.9)"
                }
            }],
            xAxes: [{
            		gridLines: {
                	display : true
                },
                ticks: {
                    beginAtZero:true,
                     maxRotation: 40,
          			minRotation: 30,
          			fontColor: "rgba(0,0,0,0.9)"
                }
            }]
        }
    }

});




$(document).ready(function () {	
	var chartData = [{label:'Draft',value:${Model.allCounts[0].testcaseDraft}},
		{label:'In Review', value:${Model.allCounts[0].testcaseReadyForReview}},
		{label:'In Progress Review', value:${Model.allCounts[0].testcaseReviewInProgress}},
		{label:'Review Completed', value:${Model.allCounts[0].testcaseReviewCompleted}},
		{label:'Final', value:${Model.allCounts[0].testcaseFinal}},
		]
	var chart = null;
    (function () {
        setTimeout(function () {
            if (chart == null) {
                chart = Morris.Donut({
                	element: 'morris-donut-chart',
                    data: chartData,
                    resize: true,
                    colors: ['rgba(255, 99, 132, 0.9)', 'rgba(54, 162, 235, 0.9)','rgba(255, 206, 86, 0.9)','rgba(75, 192, 192, 0.9)','rgba(153, 102, 255, 0.9)'],
                });
            } else {
                chart.setData(sourceData);
            }
        }, 100)
    })();
    
    
    
});
 
 $(document).ready(function () {
	 var radarchart = null;
	 (function () {
	        setTimeout(function () {
	            if (radarchart == null) {
	            	 c3.generate({
	            	     bindto: '#piee',
	            	     data:{
	                         columns: [
	                        	 ['Draft', ${Model.allCounts[0].testcaseDraft}],
	            	             ['In Review', ${Model.allCounts[0].testcaseReadyForReview}],
	            	             ['In Progress Review', ${Model.allCounts[0].testcaseReviewInProgress}],
	            	             ['Review Completed', ${Model.allCounts[0].testcaseReviewCompleted}],
	            	             ['Final', ${Model.allCounts[0].testcaseFinal}]
	                         ],
	                         colors:{
	                        	 'Draft': 'rgba(255, 99, 132, 0.9)',
	                             'In Review': 'rgba(54, 162, 235, 0.9)',
	                             'In Progress Review': 'rgba(255, 206, 86, 0.9)',
	                             'Review Completed': 'rgba(46, 204, 113, 0.9)',
	                             'Final': 'rgba(153, 102, 255, 0.9)'
	                         },
	            	         
	            	         type : 'pie'
	            	     }
	            	
	            	 });
	            } else {
	            	radarchart.setData(sourceData);
	            }
	        }, 100)
	    })();
	 
	});
 $(document).ready(function () {
	 var manual = 0;
	 var automation = 0;
	 var both = 0;


	 var radarchart = null;
	 (function () {
	        setTimeout(function () {
	            if (radarchart == null) {
	            	 c3.generate({
	            	     bindto: '#testCaseType',
	            	     data:{
	                         columns: [
	                        	 ['Manual', '${Model.allCounts[0].AMTestCase[0].test_case_count}'],
	            	             ['Automation', '${Model.allCounts[0].AMTestCase[1].test_case_count}'],
	            	             ['Both', '${Model.allCounts[0].AMTestCase[2].test_case_count}']
	                         ],
	                         colors:{
	                        	 'Manual': 'rgba(255, 99, 132, 0.9)',
	                             'Automation': 'rgba(54, 162, 235, 0.9)',
	                             'Both': 'rgba(255, 206, 86, 0.9)'
	                         },
	            	         
	            	         type : 'pie'
	            	     }
	            	
	            	 });
	            } else {
	            	radarchart.setData(sourceData);
	            }
	        }, 100)
	    })();
	 
	});
 $(document).ready(function () {
	 var radarchart = null;
	 (function () {
	        setTimeout(function () {
	            if (radarchart == null) {
	            	 c3.generate({
	            	     bindto: '#moduleWisePie',
	            	     data:{
	                         columns: [
	                        	 ['Draft', '${Draft}'],
	            	             ['Ready Review', '${ReadyForReview}'],
	            	             ['In Progress Review', '${ReadyForProgress}'],
	            	             ['Rework', '${Rework}'],
	            	             ['Obsolete', '${Obsolete}'],
	            	             ['Future', '${Future}'],
	            	             ['Final', '${Final}']
	                        	 
	                         ],
	                         colors:{
	                        	 'Draft': 'rgba(255, 99, 132, 0.9)',
	                             'Ready Review': 'rgba(54, 162, 235, 0.9)',
	                             'In Progress Review': 'rgba(255, 206, 86, 0.9)',
	                             'Rework': 'rgba(46, 204, 113, 0.9)',
	                             'Obsolete': 'rgba(153, 102, 255, 0.9)',
	                              'Future': 'rgba(255, 159, 64, 0.9)',
	                              'Final':   'rgba(255, 99, 132, 0.9)'
	                         },
	            	         
	            	         type : 'pie'
	            	     }
	            	
	            	 });
	            } else {
	            	radarchart.setData(sourceData);
	            }
	        }, 100)
	    })();
	 
	});
 $('.counter-count').each(function () {
     $(this).prop('Counter',0).animate({
         Counter: $(this).text()
     }, {
         duration: 5000,
         easing: 'swing',
         step: function (now) {
             $(this).text(Math.ceil(now));
         }
     });
 });
 
 
 
 
 var ctx = document.getElementById("moduleWiseChart");
 var moduleWiseChart = new Chart(ctx, {
     type: 'bar',
     data: {
         labels: ["Draft", "Ready Review", "In Progress Review","Rework" ,"Obsolete", "Future" , "Final"],
         datasets: [{
         	label: "Test Count",
             data: ['${Draft}', '${ReadyForReview}', '${ReadyForProgress}', '${Rework}', '${Obsolete}','${Future}','${Final}'],
             backgroundColor: [
 		        'rgba(54, 162, 235, 0.9)',
 		        'rgba(54, 162, 235, 0.9)',
 		        'rgba(54, 162, 235, 0.9)',
 		        'rgba(54, 162, 235, 0.9)',
 		        'rgba(54, 162, 235, 0.9)',
 		        'rgba(54, 162, 235, 0.9)',
 		        'rgba(54, 162, 235, 0.9)'
 		      ],
 		      borderColor: [
 		        'rgba(54, 162, 235, 1)',
 		        'rgba(54, 162, 235, 1)',
 		        'rgba(54, 162, 235, 1)',
 		        'rgba(54, 162, 235, 1)',
 		        'rgba(54, 162, 235, 1)',
 		        'rgba(54, 162, 235, 1)',
 		        'rgba(54, 162, 235, 1)'
 		      ],
 		      borderWidth: 1,
         }]
     },
     options: {
     		"hover": {
         	"animationDuration": 0
         },
         title: {
             display: true,
             text: '',
            
           },
         "animation": {
         	"duration": 1,
 						"onComplete": function () {
 							var chartInstance = this.chart,
 								ctx = chartInstance.ctx;
 							
 							ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
 							ctx.textAlign = 'center';
 							ctx.textBaseline = 'bottom';
 							this.data.datasets.forEach(function (dataset, i) {
 								var meta = chartInstance.controller.getDatasetMeta(i);
 								meta.data.forEach(function (bar, index) {
 									var data = dataset.data[index];                            
 									ctx.fillText(data, bar._model.x, bar._model.y - 5);
 								});
 							});
 						}
         },
     		legend: {
         	"display": false
         },
         tooltips: {
         	"enabled": true
          },
         scales: {
             yAxes: [{
             		display: true,            		
             		gridLines: {
                 	display : true 
                 },
                 
                 ticks: {
                 		display: true,
                     beginAtZero:true,
                     fontColor: "rgba(0,0,0,0.9)"
                 }
             }],
             xAxes: [{
             		gridLines: {
                 	display : true
                 },
                 ticks: {
                     beginAtZero:true,
                      maxRotation: 40,
           			minRotation: 30,
           			fontColor: "rgba(0,0,0,0.9)"
                 }
             }]
         }
     }

 });



</script>
<style>
.counter {
	text-align: center;
}

.counter-count {
	font-size: 48px;
	background: rgba(54, 162, 235, 1); /* fallback for old browsers */
	border-radius: 10%;
	position: relative;
	color: #ffffff;
	text-align: center;
	line-height: 92px;
	width: 120px;
	height: 90px;
	-webkit-border-radius: 10%;
	-moz-border-radius: 10%;
	-ms-border-radius: 10%;
	-o-border-radius: 10%;
	display: inline-block;
	font-weight: 600;
}
.md-skin .wrapper-content {
    padding-bottom: 40px;
    }
</style>
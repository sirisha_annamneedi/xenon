
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-11">
			<h2>
				<ol class="breadcrumb">
					<li>Test Manager Report</li>
					<li><strong>${UserCurrentProjectName}</strong></li>
				</ol>
			</h2>
		</div>
	</div>

	<div class="wrapper wrapper-content animated fadeInRight row">
		<div class="col-md-3 secondlevel-sidebar">
			<div class="pt-20">
				<form>
					<div class="form-group">
						<label for="columns">Select Columns</label> <br> <select
							class="custom-select" onchange="allFilter(this)">
							<option>Select Column</option>
							<option>Module</option>
							<option>Scenario</option>
							<option>Status</option>
						</select>

					</div>

					<div class="form-group">
						<div class="pt-20">
							<div style="display: none;" id="Status">
								<h4>Value</h4>
								<div style="display: none;" id="Status1">
									<select class="select2-multiple2" id="selectedStatus"
										name="selectedStatus" multiple onchange="statusFilter(this)">

									</select>
								</div>
								<div style="display: none;" id="Status2">
									<select class="select2-multiple2" multiple
										onchange="statusFilter(this)">
										<option value="Draft">Draft</option>
										<option value="Ready">Ready</option>
										<option value="Review">Review</option>
										<!--  <option value="Future">Future</option> -->
										<option value="Final">Final</option>
									</select>
								</div>
								<BR>
								<input type="text" id="showingStatus"
								class="form-control" placeholder="Selected Status display here"text-align:center">
							</div>

							<div style="display: none;" id="Module">

								<h4>Value</h4>

								<div style="display: none;" id="Module1">
									<select class="select2-multiple21" id="selectedModule"
										name="selectedModule" multiple onchange="moduleFilter(this)">
									</select>
								</div>
								<div style="display: none;" id="Module2">
									<select class="select2-multiple21" id="selectedModule1" style="display: none;"
									   name="selectedModule1"
										multiple onchange="moduleFilter(this)">
										<c:forEach var="modulefilter"
											items="${Model.totalMod}">
                                            
											<option value="${modulefilter.module_name}">${modulefilter.module_name}</option>
										</c:forEach>
									</select>
								</div>
								<BR> <input type="text" id="showingModule"
								class="form-control" placeholder="Selected Module display here"text-align:center">
							</div>

							<div style="display: none;" id="Scenario">
								<h4>Value</h4>


								<div style="display: none;" id="Scenario1">
									<select class="select2-multiple22" multiple
										id="selectedScenario" name="selectedScenario"
										onchange="scenarioFilter(this)">
										<c:forEach var="modulefilter"
											items="${Model.totalMod}">
											<c:forEach var="scenariofilter" items="${Model.totalSce}">
												<c:if
													test="${modulefilter.module_id == scenariofilter.module_id}">
													<option value="${scenariofilter.scenario_name}">${scenariofilter.scenario_name}</option>
												</c:if>
											</c:forEach>
										</c:forEach>
									</select>
								</div>
								<div style="display: none;" id="Scenario2">
									<select class="select2-multiple22" multiple
										onchange="scenarioFilter(this)">
										
											<c:forEach var="scenariofilter" items="${Model.totalSce}">
											
													
													<option value="${scenariofilter.scenario_name}">${scenariofilter.scenario_name}</option>
												
											</c:forEach>
										
									</select>
								</div>
								<BR> <input type="text" id="showingScenario"
								class="form-control"
								placeholder=" Selected Scenario display here"text-align:center">
							</div>
							
							
							
						</div>
					</div>
					<input type="button" id="runBtn" disabled value="Run"
						class="btn btn-primary" /> <input type="button" id="saveBtn" disabled
						value="Save" class="btn btn-primary" /> <br> <br> <br>
					<label for="columns">Your Saved Queries</label> <br>


					<div>


						<select class="select2-multiple33"
							onchange="getQuery(this.value);">
							<c:forEach var="filterVal" items="${Model.getfilteredValues}">
								<option value="${filterVal.query}">${filterVal.queryname} </option>
							</c:forEach>
						</select>

					</div>
				</form>
			</div>


		</div>
		<div class="col-md-9 pr-0">
			<div class="ibox-content">
				<div class="row">
					<div class="graph-dashboard">
<!-- 						<div class="col-md-12"> -->
<!-- 							<div class="gd-title">Bar Chart</div> -->
<!-- 						</div> -->
						<div class="row">
							<div class="col-md-12">
<!-- 								<div class="mt-20"> -->
<%-- 									<canvas id="TestCaseChart" height="100%"></canvas> --%>
<%-- 									<canvas id="TestCaseChart1" height="100%"></canvas> --%>
<!-- 								</div> -->
							</div>

							<div class="col-md-12">
								<div class="gd-title">Test Manager Status</div>
								<div class="table-responsive" style="overflow-x: visible;">
									<BR>
									<table
										class="table table-striped table-bordered table-hover dataTables-example"
										id="tableData">
										<thead>
											<tr>
												<td><b>Module Name</b></td>
												
												<td><b>Scenario Name</b></td>
												
												<td><b>Total TC</b></td>
												<td><b>Draft</b></td>
												<td><b>Ready Review</b></td>
												<td><b>In-Progress Review</b></td>
												<td style="display: none;"><b>Rework</b></td>
												<td style="display: none;"><b>Obsolete</b></td>
												<td style="display: none;"><b>Future</b></td>
												<td><b>Final</b></td>
												<td style="display: none;"><b>Total Scenario</b></td>
											</tr>
										</thead>
										<tbody>
											<c:set var="sCountTotal" value="${0}" />
											
											<c:set var="Draft" value="${0}" />
											<c:set var="ReadyForReview" value="${0}" />
											<c:set var="ReadyForProgress" value="${0}" />
											<c:set var="Rework" value="${0}" />
											<c:set var="Obsolete" value="${0}" />
											<c:set var="Future" value="${0}" />
											<c:set var="Final" value="${0}" />
											<c:set var="TcSCount" value="${0}" />
											<c:forEach var="tcs" items="${Model.totalTestCountByStatus}">
												<tr>

													<td>${tcs.module_name}</td>
													
													<td>${tcs.scenario_name}</td>
													<td>${tcs.Draft + tcs.ReadyForReview + tcs.ReadyForProgress + tcs.Rework + tcs.Obsolete + tcs.Future + tcs.Final}
													</td>
													<td class="getDraftVal">${tcs.Draft}</td>
													<td class="getReadyForReviewVal">${tcs.ReadyForReview}</td>
													<td class="getReadyForProgressVal">${tcs.ReadyForProgress}</td>
													<td class="getReworkVal" style="display: none;">${tcs.Rework}</td>
													<td class="getObsoleteVal" style="display: none;">${tcs.Obsolete}</td>
													<td class="getFutureVal" style="display: none;">${tcs.Future}</td>
													<td class="getFinalVal">${tcs.Final}</td>
													<c:set var="sCountTotal" value="${sCountTotal+tcs.sCount}" />
													<c:set var="TcSCount"
														value="${TcSCount+tcs.Draft + tcs.ReadyForReview + tcs.ReadyForProgress + tcs.Rework + tcs.Obsolete + tcs.Future + tcs.Final}" />
													<c:set var="Draft" value="${Draft+tcs.Draft}" />
													<c:set var="ReadyForReview"
														value="${ReadyForReview+tcs.ReadyForReview}" />
													<c:set var="ReadyForProgress"
														value="${ReadyForProgress+tcs.ReadyForProgress}" />
													<c:set var="Rework" value="${Rework+tcs.Rework}" />
													<c:set var="Obsolete" value="${Obsolete+tcs.Obsolete}" />
													<c:set var="Future" value="${Future+tcs.Future}" />
													<c:set var="Final" value="${Final+tcs.Final}" />
													<td style="display: none;">${tcs.sCount}</td>

												</tr>
											</c:forEach>

										</tbody>
										<tbody>
											<tr style="display: none;">
												<td><b>Grand Total</b></td>
												<td>${sCountTotal}</td>
												<td>${TcSCount}</td>
												<td>${Draft}</td>
												<td>${ReadyForReview}</td>
												<td>${ReadyForProgress}</td>
												<td>${Rework}</td>
												<td>${Obsolete}</td>
												<td>${Future}</td>
												<td>${Final}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>

						</div>
					</div>


				</div>


			</div>

			<div class="">
				<c:if test="${Model.filterText=='totalApp'}">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Applications</h5>
								</div>
								<div class="ibox-content">
									<div class="table-responsive">
										<table
											class="table table-striped table-bordered table-hover dataTables-example"
											id="table" name="Total Applications">
											<thead>
												<tr>
													<th class="hidden">App Id</th>
													<th>Name</th>
													<th>Description</th>
													<th>Created Date</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="data" items="${Model.totalApp}">
													<tr class="gradeX">
														<td class="hidden">${data.project_id}</td>
														<td class="textAlignment"
															data-original-title="${data.project_name}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${data.project_name}</td>
														<td>${data.project_description}</td>
														<td class="textAlignment"
															data-original-title="${data.project_createdate}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${data.project_createdate}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</c:if>

				<c:if test="${Model.filterText=='totalMod'}">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Modules</h5>
								</div>
								<div class="ibox-content">
									<div class="table-responsive">
										<table
											class="table table-striped table-bordered table-hover dataTables-example"
											id="table" name="Total Modules">
											<thead>
												<tr>
													<th class="hidden">Module Id</th>
													<th>Name</th>
													<th>Status</th>
													<th>Description</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="data" items="${Model.totalMod}">
													<tr class="gradeX">
														<td class="hidden">${data.module_id}</td>
														<td class="textAlignment"
															data-original-title="${data.module_name}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${data.module_name}</td>
														<td><c:if test="${data.module_active == 1}">
																<span class="label label-primary">Active</span>
															</c:if> <c:if test="${data.module_active == 2}">
																<span class="label label-danger">Inactive</span>
															</c:if></td>
														<td>${data.module_description}</td>
														<%-- <td  class="textAlignment" data-original-title="${data.module_description}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.module_description}</td> --%>
													</tr>
												</c:forEach>
											</tbody>

										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</c:if>

				<script>
					function dashLink(link) {

						$('body').addClass("white-bg");
						$("#barInMenu").removeClass("hidden");
						$("#wrapper").addClass("hidden");

						var posting = $.post('setTmLinkSession', {
							filterText : link,
							dashboard : "projectWise"
						});

						posting.done(function(data) {
							window.location.href = "tmdashboardlink";
						});
					}
				</script>

				<c:set var="string1" value='${Model.filterText}' />
				<c:set var="string2" value="${fn:substring(string1, 0, 8)}" />
				<c:set var="moduleId" value="${fn:substring(string1, 8, 100)}" />
				<c:set var="modBoolVal" value="${fn:contains(moduleId,',')}" />
				<c:if test="${string2 == 'totalSce' }">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Scenarios</h5>
								</div>

								<div class="ibox-content">
									<div class="table-responsive">
										<table
											class="table table-striped table-bordered table-hover dataTables-example"
											id="table" name="Total Scenarios">
											<thead>
												<tr>
													<th class="hidden">Sce Id</th>
													<th>Name</th>
													<!-- <th>Status</th> -->
													<th>Description</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="data" items="${Model.totalSce}">
													<c:if test="${modBoolVal == true}">
														<c:set var="arrayoflist" value="${fn:split(moduleId,',')}" />
														<c:forEach var="i" begin="0"
															end="${fn:length(arrayoflist)-1}">
															<fmt:parseNumber var="sceId" integerOnly="true"
																type="number" value="${arrayoflist[i]}" />
															<c:if test="${data.scenario_id == sceId}">
																<tr class="gradeX" style="cursor: pointer"
																	onclick="dashLink('totalTest${data.scenario_id}')">
																	<td class="hidden">${data.scenario_id}</td>
																	<td class="textAlignment"
																		data-original-title="${data.scenario_name}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.scenario_name}</td>
																	<td>${data.scenario_description}</td>

																</tr>
															</c:if>
														</c:forEach>
													</c:if>
													<c:if test="${modBoolVal == false}">

														<c:if test="${moduleId == '0'}">
															<tr class="gradeX" style="cursor: pointer"
																onclick="dashLink('totalTest${data.scenario_id}')">
																<td class="hidden">${data.scenario_id}</td>
																<td class="textAlignment"
																	data-original-title="${data.scenario_name}"
																	data-container="body" data-toggle="tooltip"
																	data-placement="right" class="issue-info">${data.scenario_name}</td>
																<td>${data.scenario_description}</td>

															</tr>
														</c:if>
														<c:if test="${data.module_id == moduleId}">
															<tr class="gradeX" style="cursor: pointer"
																onclick="dashLink('totalTest${data.scenario_id}')">
																<td class="hidden">${data.scenario_id}</td>
																<td class="textAlignment"
																	data-original-title="${data.scenario_name}"
																	data-container="body" data-toggle="tooltip"
																	data-placement="right" class="issue-info">${data.scenario_name}</td>
																<td>${data.scenario_description}</td>

															</tr>
														</c:if>
													</c:if>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</c:if>

				<c:set var="string1" value='${Model.filterText}' />
				<c:set var="string2" value="${fn:substring(string1, 0, 9)}" />
				<c:set var="SID" value="${fn:substring(string1, 9, 100)}" />
				<c:set var="boolVal" value="${fn:contains(SID,',')}" />
				<%-- <c:if test="${string2=='totalTest'}"> --%>
				<div class="row mt-10">
					<div class="col-lg-12">
						<div class="graph-dashboard ibox-content">
							<div class="gd-title">Test Cases</div>
							<div class="mt-10">
								<div class="table-responsive">
									<table
										class="table table-striped table-bordered table-hover dataTables-example"
										id="table" name="Total Test Cases">
										<thead>
											<tr>
												<th>Module<input type="checkbox" class="column_filter"
													id="col0_regex" checked="checked" style="display: none">
													<input type="checkbox" class="column_filter"
													id="col0_smart" checked="checked" style="display: none"></th>
												<th>Scenario<input type="checkbox"
													class="column_filter" id="col1_regex" checked="checked"
													style="display: none"> <input type="checkbox"
													class="column_filter" id="col1_smart" checked="checked"
													style="display: none"></th>
												<th>Id</th>
												<th>Name</th>
												<th>Status<input type="checkbox" class="column_filter"
													id="col4_regex" checked="checked" style="display: none">
													<input type="checkbox" class="column_filter"
													id="col4_smart" checked="checked" style="display: none"></th>
												<!-- 									<th>Precondition</th> -->
												<th>Created Date</th>
												<!-- 									<th>Month Year</th> -->
											</tr>
										</thead>
										<tbody>
											<c:forEach var="data" items="${Model.totalTest}">
												<c:if test="${boolVal == true}">
													<c:set var="arrayoflist" value="${fn:split(SID,',')}" />
													<c:forEach var="i" begin="0"
														end="${fn:length(arrayoflist)-1}">
														<fmt:parseNumber var="sceId" integerOnly="true"
															type="number" value="${arrayoflist[i]}" />
														<c:if test="${data.scenario_id == sceId}">
															<tr class="gradeX" style="cursor: pointer"
																onclick="location.href='testcasebyprefix?tcPrefix=${data.test_prefix}'">
																<td class="hidden">${data.testcase_id}</td>
																<td>${data.test_prefix}</td>
																<td class="textAlignment"
																	data-original-title="${data.testcase_name}"
																	data-container="body" data-toggle="tooltip"
																	data-placement="right" class="issue-info">${data.testcase_name}</td>
																<td>${data.status}</td>
																<%-- 													<td>${data.precondition}</td> --%>
																<td class="textAlignment"
																	data-original-title="${data.created_date}"
																	data-container="body" data-toggle="tooltip"
																	data-placement="right" class="issue-info">${data.created_date}</td>
																<td>${data.monthyear}</td>
															</tr>
														</c:if>
													</c:forEach>
												</c:if>
												<c:if test="${boolVal == false}">
													<c:if test="${SID == data.scenario_id}">
														<tr class="gradeX" style="cursor: pointer"
															onclick="location.href='testcasebyprefix?tcPrefix=${data.test_prefix}'">
															<td class="hidden">${data.testcase_id}</td>

															<td>${data.test_prefix}</td>
															<td class="textAlignment"
																data-original-title="${data.testcase_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.testcase_name}</td>
															<td>${data.status}</td>
															<%-- 										<td>${data.precondition}</td> --%>
															<td class="textAlignment"
																data-original-title="${data.created_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.created_date}</td>
															<td>${data.monthyear}</td>
														</tr>
													</c:if>
													<c:if test="${SID == 0}">
														<tr class="gradeX">
															<c:forEach var="data1" items="${Model.totalSce}">
																<c:if test="${data.scenario_id==data1.scenario_id}">
																	<c:forEach var="data2" items="${Model.totalMod}">
																		<c:if test="${data1.module_id==data2.module_id}">
																			<td>${data2.module_name}</td>
																			<td>${data1.scenario_name}</td>
																		</c:if>
																	</c:forEach>
																</c:if>
															</c:forEach>
															<td>${data.test_prefix}</td>
															<td class="textAlignment"
																data-original-title="${data.testcase_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.testcase_name}</td>
															<td>${data.status}</td>
															<%-- 										<td>${data.precondition}</td> --%>
															<td class="textAlignment"
																data-original-title="${data.created_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.created_date}</td>
															<%-- 										<td>${data.monthyear}</td> --%>

														</tr>
													</c:if>
												</c:if>
											</c:forEach>
										</tbody>

									</table>
								</div>

							</div>
						</div>
					</div>
				</div>
				<%-- </c:if> --%>

				<c:set var="search" value='${Model.filterText}' />
				<c:set var="filter" value="${fn:substring(search, 0, 4)}" />
				<c:set var="month" value="${fn:substring(search, 4, 12)}" />

				<c:if test="${filter == 'test'}">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Test Cases - ${month}</h5>
								</div>

								<div class="ibox-content">
									<div class="table-responsive">
										<table
											class="table table-striped table-bordered table-hover dataTables-example"
											id="table" name="Total Test Cases">
											<thead>
												<tr>
													<th class="hidden">test Id</th>
													<th>Id</th>
													<th>Name</th>
													<!-- <th>Status</th> -->
													<th>Precondition</th>
													<th>Created Date</th>

												</tr>
											</thead>
											<tbody>
												<c:forEach var="data" items="${Model.totalTest}">
													<c:if test="${data.monthyear == month}">
														<tr class="gradeX" style="cursor: pointer"
															onclick="location.href='testcasebyprefix?tcPrefix=${data.test_prefix}'">
															<td class="hidden">${data.testcase_id}</td>
															<td>${data.test_prefix}</td>
															<td class="textAlignment"
																data-original-title="${data.testcase_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.testcase_name}</td>
															<%-- <td>${data.status}</td> --%>
															<td>${data.precondition}</td>
															<td class="textAlignment"
																data-original-title="${data.created_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.created_date}</td>

														</tr>
													</c:if>
												</c:forEach>
											</tbody>

										</table>
									</div>

								</div>
							</div>
						</div>
					</div>
				</c:if>




				<!-- for draft view -->
				<c:if
					test="${Model.filterText=='totalDraft' or Model.filterText=='review' or Model.filterText=='progressReview' or Model.filterText=='reviewcompleted' or Model.filterText=='final' or Model.filterText=='1' or Model.filterText=='two' or Model.filterText=='3'}">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<c:if test="${Model.filterText=='totalDraft'}">
										<h5>Draft Test Cases</h5>
									</c:if>
									<c:if test="${Model.filterText=='review'}">
										<h5>In Review Test Cases</h5>
									</c:if>
									<c:if test="${Model.filterText=='progressReview'}">
										<h5>Review In Progress Test Cases</h5>
									</c:if>
									<c:if test="${Model.filterText=='reviewcompleted'}">
										<h5>Review Completed Test Cases</h5>
									</c:if>
									<c:if test="${Model.filterText=='final'}">
										<h5>Final State Test Cases</h5>
									</c:if>
								</div>

								<div class="ibox-content">

									<div class="table-responsive">
										<table
											class="table table-striped table-bordered table-hover dataTables-example"
											id="table" name="Total Test Cases">
											<thead>
												<tr>
													<th class="hidden">test Id</th>
													<th>Id</th>
													<th>Name</th>
													<!-- <th>Status</th> -->
													<th>Precondition</th>
													<th>Created Date</th>
													<th>Status</th>

												</tr>
											</thead>
											<tbody>
												<c:forEach var="data" items="${Model.totalTest}">
													<c:if
														test="${data.status=='Draft' and Model.filterText=='totalDraft'}">
														<tr class="gradeX" style="cursor: pointer"
															onclick="location.href='testcasebyprefix?tcPrefix=${data.test_prefix}'">
															<td class="hidden">${data.testcase_id}</td>
															<td>${data.test_prefix}</td>
															<td class="textAlignment"
																data-original-title="${data.testcase_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.testcase_name}</td>
															<%-- <td>${data.status}</td> --%>
															<td>${data.precondition}</td>
															<td class="textAlignment"
																data-original-title="${data.created_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.created_date}</td>
															<td>${data.status}</td>


														</tr>
													</c:if>
													<c:if
														test="${data.status=='Ready for review' and Model.filterText=='review'}">
														<tr class="gradeX" style="cursor: pointer"
															onclick="location.href='testcasebyprefix?tcPrefix=${data.test_prefix}'">
															<td class="hidden">${data.testcase_id}</td>
															<td>${data.test_prefix}</td>
															<td class="textAlignment"
																data-original-title="${data.testcase_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.testcase_name}</td>
															<%-- <td>${data.status}</td> --%>
															<td>${data.precondition}</td>
															<td class="textAlignment"
																data-original-title="${data.created_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.created_date}</td>
															<td>${data.status}</td>

														</tr>
													</c:if>
													<c:if
														test="${data.status=='Review in progress' and Model.filterText=='progressReview'}">
														<tr class="gradeX" style="cursor: pointer"
															onclick="location.href='testcasebyprefix?tcPrefix=${data.test_prefix}'">
															<td class="hidden">${data.testcase_id}</td>
															<td>${data.test_prefix}</td>
															<td class="textAlignment"
																data-original-title="${data.testcase_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.testcase_name}</td>
															<%-- <td>${data.status}</td> --%>
															<td>${data.precondition}</td>
															<td class="textAlignment"
																data-original-title="${data.created_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.created_date}</td>
															<td>${data.status}</td>

														</tr>
													</c:if>
													<c:if
														test="${data.status=='Review Completed' and Model.filterText=='reviewcompleted'}">
														<tr class="gradeX" style="cursor: pointer"
															onclick="location.href='testcasebyprefix?tcPrefix=${data.test_prefix}'">
															<td class="hidden">${data.testcase_id}</td>
															<td>${data.test_prefix}</td>
															<td class="textAlignment"
																data-original-title="${data.testcase_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.testcase_name}</td>
															<%-- <td>${data.status}</td> --%>
															<td>${data.precondition}</td>
															<td class="textAlignment"
																data-original-title="${data.created_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.created_date}</td>
															<td>${data.status}</td>

														</tr>
													</c:if>
													<c:if
														test="${data.status=='Final' and Model.filterText=='final'}">
														<tr class="gradeX" style="cursor: pointer"
															onclick="location.href='testcasebyprefix?tcPrefix=${data.test_prefix}'">
															<td class="hidden">${data.testcase_id}</td>
															<td>${data.test_prefix}</td>
															<td class="textAlignment"
																data-original-title="${data.testcase_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.testcase_name}</td>
															<%-- <td>${data.status}</td> --%>
															<td>${data.precondition}</td>
															<td class="textAlignment"
																data-original-title="${data.created_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.created_date}</td>
															<td>${data.status}</td>

														</tr>
													</c:if>
													<c:if
														test="${data.execution_type =='1' and Model.filterText=='1'}">
														<tr class="gradeX" style="cursor: pointer"
															onclick="location.href='testcasebyprefix?tcPrefix=${data.test_prefix}'">
															<td class="hidden">${data.testcase_id}</td>
															<td>${data.test_prefix}</td>
															<td class="textAlignment"
																data-original-title="${data.testcase_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.testcase_name}</td>
															<%-- <td>${data.status}</td> --%>
															<td>${data.precondition}</td>
															<td class="textAlignment"
																data-original-title="${data.created_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.created_date}</td>
															<td>${data.status}</td>


														</tr>
													</c:if>
													<c:if
														test="${data.execution_type =='2' and Model.filterText=='two'}">
														<tr class="gradeX" style="cursor: pointer"
															onclick="location.href='testcasebyprefix?tcPrefix=${data.test_prefix}'">
															<td class="hidden">${data.testcase_id}</td>
															<td>${data.test_prefix}</td>
															<td class="textAlignment"
																data-original-title="${data.testcase_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.testcase_name}</td>
															<%-- <td>${data.status}</td> --%>
															<td>${data.precondition}</td>
															<td class="textAlignment"
																data-original-title="${data.created_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.created_date}</td>
															<td>${data.status}</td>


														</tr>
													</c:if>
													<c:if
														test="${data.execution_type =='3' and Model.filterText=='3'}">
														<tr class="gradeX" style="cursor: pointer"
															onclick="location.href='testcasebyprefix?tcPrefix=${data.test_prefix}'">
															<td class="hidden">${data.testcase_id}</td>
															<td>${data.test_prefix}</td>
															<td class="textAlignment"
																data-original-title="${data.testcase_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.testcase_name}</td>
															<%-- <td>${data.status}</td> --%>
															<td>${data.precondition}</td>
															<td class="textAlignment"
																data-original-title="${data.created_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.created_date}</td>
															<td>${data.status}</td>


														</tr>
													</c:if>

												</c:forEach>
											</tbody>

										</table>
									</div>

								</div>
							</div>
						</div>
					</div>
				</c:if>


				<c:if test="${Model.filterText=='totalTestBugs'}">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Bugs</h5>
								</div>

								<div class="ibox-content">

									<div class="table-responsive">
										<table
											class="table table-striped table-bordered table-hover dataTables-example"
											id="table" name="Total Test Cases">
											<thead>
												<tr>
													<th class="hidden">Bug Id</th>
													<th>Bug Id</th>
													<th>Status</th>
													<th>Test Case ID</th>
													<th>Test Case Name</th>
													<th>Build Name</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="data" items="${Model.allBugDetails}">
													<tr class="gradeX" style="cursor: pointer"
														onclick="window.open('bugsummarybyprefix?btPrefix=${data.bug_prefix}')">
														<td class="hidden">${data.bug_id}</td>
														<td>${data.bug_prefix}</td>
														<td><span class="label td_bugStatus"
															style="color: #FFFFFF">${data.bugStatus}</span></td>
														<td class="textAlignment"
															data-original-title="${data.test_prefix}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${data.test_prefix}</td>
														<td class="textAlignment"
															data-original-title="${data.testcase_name}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${data.testcase_name}</td>
														<td class="textAlignment"
															data-original-title="${data.build_name}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${data.build_name}</td>
													</tr>
												</c:forEach>
											</tbody>

										</table>
									</div>

								</div>
							</div>
						</div>
					</div>
				</c:if>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(window).load(
				function() {
					var ChartColor = [ "rgba(255, 99, 132, 0.9)",
							"rgba(54, 162, 235, 0.9)",
							"rgba(255, 206, 86, 0.9)",
							"rgba(75, 192, 192, 0.9)",
							"rgba(153, 102, 255, 0.9)",
							"rgba(255, 159, 64, 0.9)",
							"rgba(75, 192, 192, 0.9)" ];
					$('body').removeClass("white-bg");
					$("#barInMenu").addClass("hidden");
					$("#wrapper").removeClass("hidden");

					var oTable = document.getElementById('tableData');
					var dataAry = [];
					//gets rows of table
					var rowLength = oTable.rows.length;
					//gets cells of current row  

					var oCells = oTable.rows.item(rowLength - 1).cells;

					//gets amount of cells of current row
					var cellLength = oCells.length;
					//loops through each cell in current row
					for (var j = 3; j < cellLength; j++) {
						var cellVal = oCells.item(j).innerHTML;
						dataAry.push(cellVal);

					}
					//console.log(dataAry);
					var barData = {

						labels : [ '${UserCurrentProjectName}' ],
						datasets : [ {
							label : "Draft",
							backgroundColor : ChartColor[0],
							hoverBackgroundColor : ChartColor[0],
							data : dataAry
						}, {
							label : "Ready For Review",
							backgroundColor : ChartColor[1],
							hoverBackgroundColor : ChartColor[1],
							data : dataAry[1]
						}, {
							label : "In Progress Review",
							backgroundColor : ChartColor[2],
							hoverBackgroundColor : ChartColor[2],
							data : dataAry[2]
						}, 
// 						{
// 							label : "Rework",
// 							backgroundColor : ChartColor[3],
// 							hoverBackgroundColor : ChartColor[3],
// 							data : dataAry[3]
// 						}, 
// 						{
// 							label : "Obsolete",
// 							backgroundColor : ChartColor[4],
// 							hoverBackgroundColor : ChartColor[4],
// 							data : dataAry[4]
// 						}, 
						{
							label : "Future",
							backgroundColor : ChartColor[5],
							hoverBackgroundColor : ChartColor[5],
							data : dataAry[5]
						}, {
							label : "Final",
							backgroundColor : ChartColor[6],
							hoverBackgroundColor : ChartColor[6],
							data : dataAry[6]
						} ]

					};
					var barOptions = {
						scaleBeginAtZero : true,
						scaleShowGridLines : true,
						scaleGridLineColor : "rgba(0,0,0,.05)",
						scaleGridLineWidth : 1,
						barShowStroke : true,
						barStrokeWidth : 2,
						barValueSpacing : 5,
						barDatasetSpacing : 1,
						responsive : true,
						scales : {
							yAxes : [ {
								ticks : {
									min : 0,
									callback : function(value) {
										if (value % 1 === 0) {
											return value;
										}
									}
								}
							} ]
						}
					}
					//var ctx = document.getElementById("TestCaseChart");

					// $("#runBtn").on("click", function() {

// 					var myNewChart = new Chart(ctx, {

// 						type : 'bar',
// 						data : barData,
// 						options : barOptions
// 					});
					// });

				});

		$(function() {
			var appFlag = '${Model.appFlag}';
			if (appFlag == 'true') {
				if ($(window).width() >= 768) {
					
					$('.firstAppName').html(
							'All Applications <span class="caret"></span>');
				} else {
					
					$('.firstAppName').html('All<span class="caret"></span>');
				}
			}

		});

		$(document).ready(function() {
			setSpanColor();
			$('#table_paginate').on('click', function() {
				setSpanColor();
			});
			$('#table_length select').on('change', function() {
				setSpanColor();
			});
			$('#table_filter input').on('keypress', function() {
				setSpanColor();
			});
			$('#table thead tr').on('click', function() {
				setSpanColor();
			});

		});
		var setSpanColor = function() {
			$('.td_bugStatus').each(function() {
				var stat = $(this).text();
				if (stat == 'New') {
					$(this).css("background-color", "${Model.New}");
				} else if (stat == 'Assigned') {
					$(this).css("background-color", "${Model.Assigned}");
				} else if (stat == 'Dev in Progress') {
					$(this).css("background-color", "${Model.Fixed}");
				} else if (stat == 'Ready for QA') {
					$(this).css("background-color", "${Model.Verified}");
				} else if (stat == 'Closed') {
					$(this).css("background-color", "${Model.Closed}");
				} else if (stat == 'Reject') {
					$(this).css("background-color", "${Model.Reject}");
				}
			});
		}
	</script>

	<!-- -My Script -->

	<script>
		function getQuery(query) {
			document.getElementById("runBtn").disabled = false;
			var res = query.split("|");
			document.getElementById("showingStatus").value = res[0];
			var str1 = document.getElementById("showingStatus").value;
			var str2 = str1.split(",").join("|");
			document.getElementById("Status").value = str2;

			document.getElementById("showingModule").value = res[1];
			var str1 = document.getElementById("showingModule").value;
			var str2 = str1.split(",").join("|");
			document.getElementById("Module").value = str2;

			document.getElementById("showingScenario").value = res[2];
			var str1 = document.getElementById("showingScenario").value;
			var str2 = str1.split(",").join("|");
			document.getElementById("Scenario").value = str2;
		}
		$("#runBtn").on('click', function() {
			filterColumn1(0);
			filterColumn2(1);
			filterColumn(4);
			if (document.getElementById("showingModule").value != "") {
				filterChart();
			}
		});

		function filterColumn(i) {
			$('#table').DataTable().column(i).search($('#Status').val(),
					$('#col' + i + '_regex').prop('checked'),
					$('#col' + i + '_smart').prop('checked')).draw();
			//     $('#tableData').DataTable().column( i ).search(
			// 	        $('#Status').val(),
			// 	        $('#col'+i+'_regex').prop('checked'),
			// 	        $('#col'+i+'_smart').prop('checked')
			// 	    ).draw();

		}
		function filterColumn1(i) {
 			$('#table').DataTable().column(i).search($('#Module').val(),
					$('#col' + i + '_regex').prop('checked'),
 					$('#col' + i + '_smart').prop('checked')).draw(), $(
					'#tableData').DataTable().column(i).search(
 					$('#Module').val(),
 					$('#col' + i + '_regex').prop('checked'),
 					$('#col' + i + '_smart').prop('checked')).draw();
			
			$('#table').DataTable().column(i).search($('#Module').val(),
					$('#col' + i + '_regex').prop('checked'),
					$('#col' + i + '_smart').prop('checked')).draw();
			
			

		}
		function filterColumn2(i) {
			$('#table').DataTable().column(i).search($('#Scenario').val(),
					$('#col' + i + '_regex').prop('checked'),
					$('#col' + i + '_smart').prop('checked')).draw(), $(
					'#tableData').DataTable().column(i).search(
					$('#Scenario').val(),
					$('#col' + i + '_regex').prop('checked'),
					$('#col' + i + '_smart').prop('checked')).draw();
			
// 			$('#table').DataTable().column(i).search($('#Scenario').val(),
// 					$('#col' + i + '_regex').prop('checked'),
// 					$('#col' + i + '_smart').prop('checked')).draw(); 
		}

		function filterChart() {
			//document.getElementById("TestCaseChart").style.display = 'none';
			var ChartColor = [ "rgba(255, 99, 132, 0.9)",
					"rgba(54, 162, 235, 0.9)", "rgba(255, 206, 86, 0.9)",
					"rgba(75, 192, 192, 0.9)", "rgba(153, 102, 255, 0.9)",
					"rgba(255, 159, 64, 0.9)", "rgba(255, 99, 132, 0.9)" ];
			$('body').removeClass("white-bg");
			$("#barInMenu").addClass("hidden");
			$("#wrapper").removeClass("hidden");

			var oTable = document.getElementById('tableData');
			var dataAry = [];
			var draftVal = 0;
			var reviewVal = 0;
			var progressVal = 0;
			var reworkVal = 0;
			var obsoleteVal = 0;
			var futureVal = 0;
			var finalVal = 0;

			$('.getDraftVal').each(function() {
				if (!isNaN(this.innerHTML) && this.innerHTML.length != 0) {
					draftVal += parseFloat(this.innerHTML);
				}
			});
			dataAry.push(draftVal);
			$('.getReadyForReviewVal').each(function() {
				if (!isNaN(this.innerHTML) && this.innerHTML.length != 0) {
					reviewVal += parseFloat(this.innerHTML);
				}
			});
			dataAry.push(reviewVal);

			$('.getReadyForProgressVal').each(function() {
				if (!isNaN(this.innerHTML) && this.innerHTML.length != 0) {
					progressVal += parseFloat(this.innerHTML);
				}
			});
			dataAry.push(progressVal);

			$('.getReworkVal').each(function() {
				if (!isNaN(this.innerHTML) && this.innerHTML.length != 0) {
					reworkVal += parseFloat(this.innerHTML);
				}
			});
			dataAry.push(reworkVal);

			$('.getObsoleteVal').each(function() {
				if (!isNaN(this.innerHTML) && this.innerHTML.length != 0) {
					obsoleteVal += parseFloat(this.innerHTML);
				}
			});
			dataAry.push(obsoleteVal);

			$('.getFutureVal').each(function() {
				if (!isNaN(this.innerHTML) && this.innerHTML.length != 0) {
					futureVal += parseFloat(this.innerHTML);
				}
			});
			dataAry.push(futureVal);

			$('.getFinalVal').each(function() {
				if (!isNaN(this.innerHTML) && this.innerHTML.length != 0) {
					finalVal += parseFloat(this.innerHTML);
				}
			});
			dataAry.push(finalVal);

			var barData = {

				labels : [ '${UserCurrentProjectName}' ],
				datasets : [ {
					label : "Draft",
					backgroundColor : ChartColor[0],
					hoverBackgroundColor : ChartColor[0],
					data : dataAry
				}, {
					label : "Ready For Review",
					backgroundColor : ChartColor[1],
					hoverBackgroundColor : ChartColor[1],
					data : dataAry[1]
				}, {
					label : "In Progress Review",
					backgroundColor : ChartColor[2],
					hoverBackgroundColor : ChartColor[2],
					data : dataAry[2]
				}, {
					label : "Rework",
					backgroundColor : ChartColor[3],
					hoverBackgroundColor : ChartColor[3],
					data : dataAry[3]
				}, {
					label : "Obsolete",
					backgroundColor : ChartColor[4],
					hoverBackgroundColor : ChartColor[4],
					data : dataAry[4]
				}, {
					label : "Future",
					backgroundColor : ChartColor[5],
					hoverBackgroundColor : ChartColor[5],
					data : dataAry[5]
				}, {
					label : "Final",
					backgroundColor : ChartColor[6],
					hoverBackgroundColor : ChartColor[6],
					data : dataAry[6]
				} ]

			};
			var barOptions = {
				scaleBeginAtZero : true,
				scaleShowGridLines : true,
				scaleGridLineColor : "rgba(0,0,0,.05)",
				scaleGridLineWidth : 1,
				barShowStroke : true,
				barStrokeWidth : 2,
				barValueSpacing : 5,
				barDatasetSpacing : 1,
				responsive : true,
				scales : {
					yAxes : [ {
						ticks : {
							min : 0,
							callback : function(value) {
								if (value % 1 === 0) {
									return value;
								}
							}
						}
					} ]
				}
			}
			
			//var ctx = document.getElementById("TestCaseChart1");

// 			var myNewChart = new Chart(ctx, {

// 				type : 'bar',
// 				data : barData,
// 				options : barOptions
// 			});
		};
	
</script>



	<script>
		$(document)
				.ready(
						function() {
							var templateName = $(".dataTables-example").attr(
									"name");

							var table = $('.dataTables-example')
									.DataTable(
											{
												dom : '<"html5buttons"B>lTfgitp',
												buttons : [
														{
															extend : 'csv',
															title : templateName,
															exportOptions : {
																columns : ':visible'
															}
														},
														{
															extend : 'excel',
															title : templateName,
															exportOptions : {
																columns : ':visible'
															}
														},
														{
															extend : 'pdf',
															title : templateName,
															exportOptions : {
																columns : ':visible'
															}
														},

														{
															extend : 'print',
															exportOptions : {
																columns : ':visible'
															},
															customize : function(
																	win) {
																$(
																		win.document.body)
																		.addClass(
																				'white-bg');
																$(
																		win.document.body)
																		.css(
																				'font-size',
																				'10px');

																$(
																		win.document.body)
																		.find(
																				'table')
																		.addClass(
																				'compact')
																		.css(
																				'font-size',
																				'inherit');
															}
														} ],
												"aaSorting" : [ [ 0, "desc" ] ]

											});

						});
	</script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
	<script>
		$(document).click(function() {
			$(".selectpicker").selectpicker();

		});

		$('.filterColumns')
				.change(
						function() {
							if ($("#" + $(this).val()).length == 0) {
								/* alert($(this).val()); */
								$('#selectColumns')
										.append(
												'<p class="columnsSelectP" id="'
														+ $(this).val()
																.replace(/ /g,
																		"")
														+ '">'
														+ $(this).val()
														+ '<a id="'
														+ $(this).val()
																.replace(/ /g,
																		"")
														+ '" class="columnsSelect" href="#" style="float:right">X</a></p>');
								$('.columnsSelect').click(function() {
									$('#' + $(this).attr("id")).remove();
								});
							}
						});

		$('.filterContains')
				.change(
						function() {
							if ($("#" + $(this).val()).length == 0) {
								
								$('#selectContains')
										.append(
												'<p class="columnsSelectP" id="'
														+ $(this).val()
																.replace(/ /g,
																		"")
														+ '">'
														+ $(this).val()
														+ '<a id="'
														+ $(this).val()
																.replace(/ /g,
																		"")
														+ '" class="columnsSelect" href="#" style="float:right">X</a></p>');
								$('.columnsSelect').click(function() {
									$('#' + $(this).attr("id")).remove();
								});
							}
						});
		$('.filterValue')
				.change(
						function() {
							if ($("#" + $(this).val()).length == 0) {
								// alert($(this).val()); */    
								$('#selectValue')
										.append(
												'<p class="columnsSelectP" id="'
														+ $(this).val()
																.replace(/ /g,
																		"")
														+ '">'
														+ $(this).val()
														+ '<a id="'
														+ $(this).val()
																.replace(/ /g,
																		"")
														+ '" class="columnsSelect" href="#" style="float:right">X</a></p>');
								$('.columnsSelect').click(function() {
									$('#' + $(this).attr("id")).remove();
								});
							}
						});
		$('.filterGroup')
				.change(
						function() {
							if ($("#" + $(this).val()).length == 0) {
								/* alert($(this).val()); */
								$('#selectGroup')
										.append(
												'<p class="columnsSelectP" id="'
														+ $(this).val()
																.replace(/ /g,
																		"")
														+ '">'
														+ $(this).val()
														+ '<a id="'
														+ $(this).val()
																.replace(/ /g,
																		"")
														+ '" class="columnsSelect" href="#" style="float:right">X</a></p>');
								$('.columnsSelect').click(function() {
									$('#' + $(this).attr("id")).remove();
								});
							}
						});
	</script>

	<script>
		var allData1 = '';
		var allData2 = '';
		var allData3 = '';
		var commonData = '';
		var mod = '';
		var sce = '';

		function allFilter(sel) {
			
		    
			var opts = '', opt;
			var totalTestJson=${Model.totalTestJson};
			var moduleName = '';
			var scenarioName = '';
			var statusName = '';
			var len = sel.options.length;
			for (var i = 0; i < len; i++) {
				opt = sel.options[i];
				if (opt.selected) {

					opts = opt.value;
					switch (opts) {
					case "Module":
						document.getElementById("Module").style.display = 'block';
						document.getElementById("Scenario").style.display = 'none';
						document.getElementById("Status").style.display = 'none';
						var showingStatus = document
								.getElementById("showingStatus").value;
						if (showingStatus.trim() != "") {
							//document.getElementById("Module2").style.display = 'none';
							//document.getElementById("Module1").style.display = 'block';
							document.getElementById("Module2").style.display = 'block';
							for (var j = 0; j < totalTestJson.length; j++) {
								if (showingStatus.includes(",")) {
									var statusAry = showingStatus.split(",");
									for (var a = 0; a < statusAry.length; a++) {
										if (statusAry[a] == totalTestJson[a].status) {
										  mod =totalTestJson[j].module_name;
										  //alert(statusAry[a]);
										  //alert(totalTestJson[a].status);
										  //alert(mod);
											if (moduleName != mod) {
												jQuery('#selectedModule')
														.append(
																jQuery(
																		"<option></option>")
																		.val(
																				mod)
																		.text(
																				mod));
												moduleName = mod;
											}
										}
									}

								} else {
									if (showingStatus == totalTestJson[j].status) {
										if (moduleName != mod) {
											//alert(statusAry[a]);
											  //alert(totalTestJson[a].status);
											 // alert(mod);
											jQuery('#selectedModule')
													.append(
															jQuery(
																	"<option></option>")
																	.val(
																			mod)
																	.text(
																			mod));
											moduleName = mod;
										}
									}
								}
								
								var optionValues1 =[];
								$('#selectedModule option').each(function(){
								   if($.inArray(this.value, optionValues1) >-1){
								      $(this).remove()
								   }else{
								      optionValues1.push(this.value);
								   }
								});
								
								$("#selectedModule1 option").val(function(idx, val) {
									  $(this).siblings('[value="'+ val +'"]').remove();
									});
								
								$("#selectedModule option").val(function(idx, val) {
									  $(this).siblings('[value="'+ val +'"]').remove();
									});
							}

						} else {
							//document.getElementById("Module1").style.display = 'none';
							document.getElementById("Module2").style.display = 'block';
						}

						break;
					case "Scenario":
						document.getElementById("Scenario").style.display = 'block';
						document.getElementById("Module").style.display = 'none';
						document.getElementById("Status").style.display = 'none';
						var showingStatus = document
								.getElementById("showingStatus").value;
						if (showingStatus.trim() != "") {
							//document.getElementById("Scenario2").style.display = 'none';
							//document.getElementById("Scenario1").style.display = 'block';
							document.getElementById("Scenario2").style.display = 'block';
							for (var j = 0; j < totalTestJson.length; j++) {
								if (showingStatus.includes(",")) {
									var statusAry = showingStatus.split(",");
									for (var a = 0; a < statusAry.length; a++) {
										if (statusAry[a] == totalTestJson[a].status) {
											sce =totalTestJson[j].scenario_name;
											if (scenarioName != sce) {
												jQuery('#selectedScenario')
														.append(
																jQuery(
																		"<option></option>")
																		.val(
																				sce)
																		.text(
																				sce));
												scenarioName = sce;
											}
										}

									}

								} else {
									if (showingStatus == totalTestJson[j].status) {
										if (scenarioName != sce) {
											jQuery('#selectedScenario')
													.append(
															jQuery(
																	"<option></option>")
																	.val(
																			sce)
																	.text(
																			sce));
											scenarioName = sce;
										}
									}
								}
							}
							var code = {};
							$("#selectedScenario option").each(
									function() {
										if (code[this.text]) {
											$(this).remove();
										} else {
											code[this.text] = this.value;
										}
									});
							
							var code = {};
							$("#selectedScenario1 option").each(
									function() {
										if (code[this.text]) {
											$(this).remove();
										} else {
											code[this.text] = this.value;
										}
									});
							
							
						} else {
							document.getElementById("Scenario1").style.display = 'none';
							document.getElementById("Scenario2").style.display = 'block';
						}

						break;
					case "Status":
						document.getElementById("Status").style.display = 'block';
						document.getElementById("Scenario").style.display = 'none';
						document.getElementById("Module").style.display = 'none';
						var showingModule = document
								.getElementById("showingModule").value;
						if (showingModule.trim() != "") {
							//document.getElementById("Status2").style.display = 'none';
							//document.getElementById("Status1").style.display = 'block';
							document.getElementById("Status2").style.display = 'block';
							for (var j = 0; j < totalTestJson.length; j++) {
								if (showingModule.includes(",")) {
									var moduleAry = showingModule.split(",");
									for (var a = 0; a < moduleAry.length; a++) {
										if (moduleAry[a] == totalTestJson[a].module_name) {
											if (statusName != totalTestJson[j].status) {
												jQuery('#selectedStatus')
														.append(
																jQuery(
																		"<option></option>")
																		.val(
																				totalTestJson[j].status)
																		.text(
																				totalTestJson[j].status));
												statusName = totalTestJson[j].status;
											}
										}
									}

								} else {
									if (showingModule == totalTestJson[j].module_name) {
										var exists = false;
										if (statusName != totalTestJson[j].status) {
											jQuery('#selectedStatus')
													.append(
															jQuery(
																	"<option></option>")
																	.val(
																			totalTestJson[j].status)
																	.text(
																			totalTestJson[j].status));
											statusName = totalTestJson[j].status;
										}
									}
								}
							}
							var code = {};
							$("select[name='selectedStatus'] > option").each(
									function() {
										if (code[this.text]) {
											$(this).remove();
										} else {
											code[this.text] = this.value;
										}
									});
						} else {
							//document.getElementById("Status1").style.display = 'none';
							document.getElementById("Status2").style.display = 'block';
						}

						break;

					default:
					}

				}
			}

		}

		function statusFilter(sel) {

			var opts = '', opt='', opt1='',opts2='',valOpt ='';
			var len = sel.options.length;

		 var optVal=[];var withoutSpace;

		for (var i = 0; i < len; i++) {
						opt = sel.options[i];

						if (opt.selected) {
							var valOpt=opt.value;
							
							if(valOpt.indexOf(' ') >= 0){
								optVal=valOpt.split(" ");
								
								withoutSpace=optVal[0];
							}else{
								withoutSpace=opt.value;
							}
							if (opts != "") {
								
								opts = opts + "|" + withoutSpace;
								
								opts2=opts2 + "," + valOpt;
								
								

							} else {

								opts = withoutSpace;
								opts2=valOpt;

							}
						}
						
					}
			document.getElementById("Status").value = opts;
			document.getElementById("saveBtn").disabled = false;
			document.getElementById("runBtn").disabled = false;
			var str1 = document.getElementById("Status").value;
			var str2 = opts2;
			
			document.getElementById("showingStatus").value = str2;
			allData1 = str2 + "|";
			return false;
		}

		function moduleFilter(sel) {
			var opts = '', opt='', opt1='',opts2='',valOpt ='';
			var len = sel.options.length;

		 var optVal=[];var withoutSpace;

		for (var i = 0; i < len; i++) {
						opt = sel.options[i];

						if (opt.selected) {
							var valOpt=opt.value;
						
							if(valOpt.indexOf(' ') >= 0){
								optVal=valOpt.split(" ");
								
								withoutSpace=optVal[0];
							}else{
								withoutSpace=opt.value;
							}
							if (opts != "") {
								
								opts = opts + "|" + withoutSpace;
								
								opts2=opts2 + "," + valOpt;
								
								

							} else {

								opts = withoutSpace;
								opts2=valOpt;

							}
						}
						
					}
			document.getElementById("Module").value = opts;
			document.getElementById("saveBtn").disabled = false;
			document.getElementById("runBtn").disabled = false;
			var str1 = document.getElementById("Module").value;
			var str2 = opts2;
			
			document.getElementById("showingModule").value = str2;
			allData2 = str2 + "|";
			return false;
		}

		function scenarioFilter(sel) {
			var opts = '', opt='', opt1='',opts2='',valOpt ='';
			var len = sel.options.length;

		 var optVal=[];var withoutSpace;

		for (var i = 0; i < len; i++) {
						opt = sel.options[i];

						if (opt.selected) {
							var valOpt=opt.value;
							//alert(valOpt);
							if(valOpt.indexOf(' ') >= 0){
								optVal=valOpt.split(" ");
								
								withoutSpace=optVal[0];
							}else{
								withoutSpace=opt.value;
							}
							if (opts != "") {
								
								opts = opts + "|" + withoutSpace;
								
								opts2=opts2 + "," + valOpt;
								
								

							} else {

								opts = withoutSpace;
								opts2=valOpt;

							}
						}
						
					}
			document.getElementById("Scenario").value = opts;
			document.getElementById("saveBtn").disabled = false;
			document.getElementById("runBtn").disabled = false;
			var str1 = document.getElementById("Scenario").value;
			var str2 = opts2;
			
			document.getElementById("showingScenario").value = str2;
			allData3 = str2 + "|";
			return false;
		}
	</script>



	<script>
		$(function() {
			$('.button-checkbox')
					.each(
							function() {

								
								var $widget = $(this), $button = $widget
										.find('button'), $checkbox = $widget
										.find('input:checkbox'), color = $button
										.data('color'), settings = {
									on : {
										icon : 'glyphicon glyphicon-check'
									},
									off : {
										icon : 'glyphicon glyphicon-unchecked'
									}
								};

								
								$button.on('click', function() {
									$checkbox.prop('checked', !$checkbox
											.is(':checked'));
									$checkbox.triggerHandler('change');
									updateDisplay();
								});
								$checkbox.on('change', function() {
									updateDisplay();
								});

								
								function updateDisplay() {
									var isChecked = $checkbox.is(':checked');

									
									$button.data('state', (isChecked) ? "on"
											: "off");

									
									$button
											.find('.state-icon')
											.removeClass()
											.addClass(
													'state-icon '
															+ settings[$button
																	.data('state')].icon);

									
									if (isChecked) {
										$button.removeClass('btn-default')
												.addClass(
														'btn-' + color
																+ ' active');
									} else {
										$button.removeClass(
												'btn-' + color + ' active')
												.addClass('btn-default');
									}
								}

								
								function init() {

									updateDisplay();

									
									if ($button.find('.state-icon').length == 0) {
										$button.prepend('<i class="state-icon '
												+ settings[$button
														.data('state')].icon
												+ '"></i>�');
									}
								}
								init();
							});
		});

		jQuery(function($) {
			$.fn.select2.amd
					.require(
							[ 'select2/selection/single',
									'select2/selection/placeholder',
									'select2/selection/allowClear',
									'select2/dropdown',
									'select2/dropdown/search',
									'select2/dropdown/attachBody',
									'select2/utils' ],
							function(SingleSelection, Placeholder, AllowClear,
									Dropdown, DropdownSearch, AttachBody, Utils) {

								var SelectionAdapter = Utils.Decorate(
										SingleSelection, Placeholder);

								SelectionAdapter = Utils.Decorate(
										SelectionAdapter, AllowClear);

								var DropdownAdapter = Utils.Decorate(Utils
										.Decorate(Dropdown, DropdownSearch),
										AttachBody);

								var base_element = $('.select2-multiple2')
								$(base_element)
										.select2(
												{
													placeholder : 'Select multiple items',
													selectionAdapter : SelectionAdapter,
													dropdownAdapter : DropdownAdapter,
													allowClear : true,
													templateResult : function(
															data) {

														if (!data.id) {
															return data.text;
														}

														var $res = $('<div></div>');

														$res.text(data.text);
														$res.addClass('wrap');

														return $res;
													},
													templateSelection : function(
															data) {
														if (!data.id) {
															return data.text;
														}
														var selected = ($(
																base_element)
																.val() || []).length;
														var total = $('option',
																$(base_element)).length;
														
														return "You Selected ";
													}
												})

							});

		});
	</script>

	<script>
		$(function() {
			$('.button-checkbox')
					.each(
							function() {

								
								var $widget = $(this), $button = $widget
										.find('button'), $checkbox = $widget
										.find('input:checkbox'), color = $button
										.data('color'), settings = {
									on : {
										icon : 'glyphicon glyphicon-check'
									},
									off : {
										icon : 'glyphicon glyphicon-unchecked'
									}
								};

								
								$button.on('click', function() {
									$checkbox.prop('checked', !$checkbox
											.is(':checked'));
									$checkbox.triggerHandler('change');
									updateDisplay();
								});
								$checkbox.on('change', function() {
									updateDisplay();
								});

								
								function updateDisplay() {
									var isChecked = $checkbox.is(':checked');

									
									$button.data('state', (isChecked) ? "on"
											: "off");

									
									$button
											.find('.state-icon')
											.removeClass()
											.addClass(
													'state-icon '
															+ settings[$button
																	.data('state')].icon);

									
									if (isChecked) {
										$button.removeClass('btn-default')
												.addClass(
														'btn-' + color
																+ ' active');
									} else {
										$button.removeClass(
												'btn-' + color + ' active')
												.addClass('btn-default');
									}
								}

								
								function init() {

									updateDisplay();

									
									if ($button.find('.state-icon').length == 0) {
										$button.prepend('<i class="state-icon '
												+ settings[$button
														.data('state')].icon
												+ '"></i>�');
									}
								}
								init();
							});
		});

		jQuery(function($) {
			$.fn.select2.amd
					.require(
							[ 'select2/selection/single',
									'select2/selection/placeholder',
									'select2/selection/allowClear',
									'select2/dropdown',
									'select2/dropdown/search',
									'select2/dropdown/attachBody',
									'select2/utils' ],
							function(SingleSelection, Placeholder, AllowClear,
									Dropdown, DropdownSearch, AttachBody, Utils) {

								var SelectionAdapter = Utils.Decorate(
										SingleSelection, Placeholder);

								SelectionAdapter = Utils.Decorate(
										SelectionAdapter, AllowClear);

								var DropdownAdapter = Utils.Decorate(Utils
										.Decorate(Dropdown, DropdownSearch),
										AttachBody);

								var base_element = $('.select2-multiple21')
								$(base_element)
										.select2(
												{
													placeholder : 'Select multiple items',
													selectionAdapter : SelectionAdapter,
													dropdownAdapter : DropdownAdapter,
													allowClear : true,
													templateResult : function(
															data) {

														if (!data.id) {
															return data.text;
														}

														var $res = $('<div></div>');

														$res.text(data.text);
														$res.addClass('wrap');

														return $res;
													},
													templateSelection : function(
															data) {
														if (!data.id) {
															return data.text;
														}
														var selected = ($(
																base_element)
																.val() || []).length;
														var total = $('option',
																$(base_element)).length;
														
														 return "You Selected ";
													}
												})

							});

		});
	</script>

	<script>
		$(function() {
			$('.button-checkbox')
					.each(
							function() {

								
								var $widget = $(this), $button = $widget
										.find('button'), $checkbox = $widget
										.find('input:checkbox'), color = $button
										.data('color'), settings = {
									on : {
										icon : 'glyphicon glyphicon-check'
									},
									off : {
										icon : 'glyphicon glyphicon-unchecked'
									}
								};

								
								$button.on('click', function() {
									$checkbox.prop('checked', !$checkbox
											.is(':checked'));
									$checkbox.triggerHandler('change');
									updateDisplay();
								});
								$checkbox.on('change', function() {
									updateDisplay();
								});

								
								function updateDisplay() {
									var isChecked = $checkbox.is(':checked');

									
									$button.data('state', (isChecked) ? "on"
											: "off");

									
									$button
											.find('.state-icon')
											.removeClass()
											.addClass(
													'state-icon '
															+ settings[$button
																	.data('state')].icon);

									
									if (isChecked) {
										$button.removeClass('btn-default')
												.addClass(
														'btn-' + color
																+ ' active');
									} else {
										$button.removeClass(
												'btn-' + color + ' active')
												.addClass('btn-default');
									}
								}

								
								function init() {

									updateDisplay();

									
									if ($button.find('.state-icon').length == 0) {
										$button.prepend('<i class="state-icon '
												+ settings[$button
														.data('state')].icon
												+ '"></i>�');
									}
								}
								init();
							});
		});

		jQuery(function($) {
			$.fn.select2.amd
					.require(
							[ 'select2/selection/single',
									'select2/selection/placeholder',
									'select2/selection/allowClear',
									'select2/dropdown',
									'select2/dropdown/search',
									'select2/dropdown/attachBody',
									'select2/utils' ],
							function(SingleSelection, Placeholder, AllowClear,
									Dropdown, DropdownSearch, AttachBody, Utils) {

								var SelectionAdapter = Utils.Decorate(
										SingleSelection, Placeholder);

								SelectionAdapter = Utils.Decorate(
										SelectionAdapter, AllowClear);

								var DropdownAdapter = Utils.Decorate(Utils
										.Decorate(Dropdown, DropdownSearch),
										AttachBody);

								var base_element = $('.select2-multiple22')
								$(base_element)
										.select2(
												{
													placeholder : 'Select multiple items',
													selectionAdapter : SelectionAdapter,
													dropdownAdapter : DropdownAdapter,
													allowClear : true,
													templateResult : function(
															data) {

														if (!data.id) {
															return data.text;
														}

														var $res = $('<div></div>');

														$res.text(data.text);
														$res.addClass('wrap');

														return $res;
													},
													templateSelection : function(
															data) {
														if (!data.id) {
															return data.text;
														}
														var selected = ($(
																base_element)
																.val() || []).length;
														var total = $('option',
																$(base_element)).length;
														
														return "You Selected ";
													}
												})

							});

		});

		function openpopUp() {
			swal({
				title : "Save your query",
				text : "Enter the name of query",
				type : "input",
				showCancelButton : true,
				closeOnConfirm : false,
				animation : "slide-from-left",
				inputPlaceholder : "The name of query"
			},

			function(inputValue) {
				if (inputValue === false)
					return false;
				if (inputValue === "") {
					swal.showInputError("Please enter name of the query!");
					return false
				}
				$('#queryName').val(inputValue);
				var saveFlag = true;
				//commonData = allData1+allData2+allData3;
				commonData = $("#showingStatus").val() + "|"
						+ $("#showingModule").val() + "|"
						+ $("#showingScenario").val();
				saveOrExecuteQuery(inputValue, commonData);
			});

			function saveOrExecuteQuery(inputValue, commonData) {
				//alert(inputValue);
				//commonData = allData1+allData2+allData3;
				//alert(commonData);
				$.ajax({
					type : "POST",
					url : "insertFilterQuery",
					data : {
						inputValue : inputValue,
						commonData : commonData

					}
				}).done(function(response) {
					window.location.href = "tmdashboardlink";
				}).fail(function(jqXHR, testStatus, errorThrown) {
					return false;
				});
			}

		}

		$("#saveBtn").on('click', function() {

			openpopUp();

			//commonData = allData1+allData2+allData3;
			//alert(commonData);
		});
	</script>


	<script>
		$(function() {
			$('.button-checkbox')
					.each(
							function() {

								
								var $widget = $(this), $button = $widget
										.find('button'), $checkbox = $widget
										.find('input:checkbox'), color = $button
										.data('color'), settings = {
									on : {
										icon : 'glyphicon glyphicon-check'
									},
									off : {
										icon : 'glyphicon glyphicon-unchecked'
									}
								};

								
								$button.on('click', function() {
									$checkbox.prop('checked', !$checkbox
											.is(':checked'));
									$checkbox.triggerHandler('change');
									updateDisplay();
								});
								$checkbox.on('change', function() {
									updateDisplay();
								});

								
								function updateDisplay() {
									var isChecked = $checkbox.is(':checked');

									
									$button.data('state', (isChecked) ? "on"
											: "off");

									
									$button
											.find('.state-icon')
											.removeClass()
											.addClass(
													'state-icon '
															+ settings[$button
																	.data('state')].icon);

									
									if (isChecked) {
										$button.removeClass('btn-default')
												.addClass(
														'btn-' + color
																+ ' active');
									} else {
										$button.removeClass(
												'btn-' + color + ' active')
												.addClass('btn-default');
									}
								}

								
								function init() {

									updateDisplay();

									
									if ($button.find('.state-icon').length == 0) {
										$button.prepend('<i class="state-icon '
												+ settings[$button
														.data('state')].icon
												+ '"></i>�');
									}
								}
								init();
							});
		});

		jQuery(function($) {
			$.fn.select2.amd
					.require(
							[ 'select2/selection/single',
									'select2/selection/placeholder',
									'select2/selection/allowClear',
									'select2/dropdown',
									'select2/dropdown/search',
									'select2/dropdown/attachBody',
									'select2/utils' ],
							function(SingleSelection, Placeholder, AllowClear,
									Dropdown, DropdownSearch, AttachBody, Utils) {

								var SelectionAdapter = Utils.Decorate(
										SingleSelection, Placeholder);

								SelectionAdapter = Utils.Decorate(
										SelectionAdapter, AllowClear);

								var DropdownAdapter = Utils.Decorate(Utils
										.Decorate(Dropdown, DropdownSearch),
										AttachBody);

								var base_element = $('.select2-multiple33')
								$(base_element)
										.select2(
												{
													placeholder : 'Select multiple items',
													selectionAdapter : SelectionAdapter,
													dropdownAdapter : DropdownAdapter,
													allowClear : true,
													templateResult : function(
															data) {

														if (!data.id) {
															return data.text;
														}

														var $res = $('<div></div>');

														$res.text(data.text);
														$res.addClass('wrap');

														return $res;
													},
													templateSelection : function(
															data) {
														if (!data.id) {
															return data.text;
														}
														var selected = ($(
																base_element)
																.val() || []).length;
														var total = $('option',
																$(base_element)).length;
														
														return data.text;
													}
												})

							});

		});

		function openpopUp() {
			swal({
				title : "Save your query",
				text : "Enter the name of query",
				type : "input",
				showCancelButton : true,
				closeOnConfirm : false,
				animation : "slide-from-left",
				inputPlaceholder : "The name of query"
			},

			function(inputValue) {
				if (inputValue === false)
					return false;
				if (inputValue === "") {
					swal.showInputError("Please enter name of the query!");
					return false
				}
				$('#queryName').val(inputValue);
				var saveFlag = true;
				//commonData = allData1+allData2+allData3;
				commonData = $("#showingStatus").val() + "|"
						+ $("#showingModule").val() + "|"
						+ $("#showingScenario").val();
				saveOrExecuteQuery(inputValue, commonData);
			});

			function saveOrExecuteQuery(inputValue, commonData) {
				//alert(inputValue);
				//commonData = allData1+allData2+allData3;
				//alert(commonData);
				$.ajax({
					type : "POST",
					url : "insertFilterQuery",
					data : {
						inputValue : inputValue,
						commonData : commonData

					}
				}).done(function(response) {
					window.location.href = "tmdashboardlink";
				}).fail(function(jqXHR, testStatus, errorThrown) {
					return false;
				});
			}

		}

		$("#saveBtn").on('click', function() {

			openpopUp();

			//commonData = allData1+allData2+allData3;
			//alert(commonData);
		});
	</script>


	<style>
#overflowTest {
	color: white;
	padding: 15px;
	width: 100%;
	height: 130px;
	overflow: scroll;
	border: 1px solid #ccc;
}
</style>
	<style>
.select2-results__option .wrap:before {
	font-family: fontAwesome;
	color: #999;
	content: "\f096";
	width: 25px;
	height: 25px;
	padding-right: 10px;
}

.select2-results__option[aria-selected=true] .wrap:before {
	content: "\f14a";
}

/* not required css */
.select2-multiple, .select2-multiple2 {
	width: 220%
}

.select2-multiple, .select2-multiple21 {
	width: 220%
}

.select2-multiple, .select2-multiple22 {
	width: 220%
}

.select2-multiple, .select2-multiple33 {
	width: 100%
}
</style>
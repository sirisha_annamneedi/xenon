<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
<!-- Afzal Html -->
<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-11">
			<h2>
			<span class="lgMenuTextAlignment" style="width: 90% !important;" data-original-title="${Model.scenarioDetails[0].scenario_name}" data-toggle="tooltip">
				<ol class="breadcrumb">
					<li><strong>${UserCurrentProjectName}</strong></li>
					<c:forEach var="moduleL" items="${Model.moduleList}">
						<c:if test="${moduleL.module_id == Model.moduleId}">
							<li>${moduleL.module_name}</li>
						</c:if>
					</c:forEach>
					<li>${Model.scenarioDetails[0].scenario_name}</li>
					
				</ol>
				</span>
			</h2>
<!-- 			<ol class="breadcrumb"> -->
				<!-- 			<li><a href="tmdashboard">Dashboard</a></li>
 -->
<!-- 				<li class="active"><a href="testcase">Test Cases</a></li> -->
<!-- 			</ol> -->
		</div>
	</div>

	<div class="row sub-topmenu">
		<div class="col-md-12">
			<div class="">

				<div class="col-md-6 pd-l-0">
					<ul>
						<li><a href="javascript:void(0)" id="homeMenu"
							class="active "><i class="fa fa-home"></i> Home</a></li>
						<c:if test="${Model.scenarioDetails[0].scenario_id >0}">
							<!-- <li><a href="javascript:void(0)" id="addTestCase"><i
									class="fa fa-plus"></i> Add Test Case</a></li> -->
							<li><a href="javascript:void(0)" id="uploadTestCaseMenu"><i
									class="fa fa-upload"></i> Upload Test Case</a></li>
							<li><a href="javascript:void(0)" id="addParameter"><i
									class="fa fa-share-alt"></i> Add Parameter</a></li>
						</c:if>
					</ul>
				</div>
			</div>
		</div>
	</div>
<div class="wrapper wrapper-content animated fadeInRight">

	<div class="row" id="testcaseDetails">
		<div class="col-md-12">
			<div class="white-bg usr-dtl">
				<div class="title">
					<h2>Test Case Detail</h2>
				</div>
				
				<div class="row">
					<form class="form-inline">
						<div class="form-group col-md-4">
							<label for="user-id">Name:</label> <input type="text"
								readonly="readonly" class="form-control" id="nameDeatils"
								value="${Model.testCaseList[0].testcase_name}">
						</div>
						<div class="form-group col-md-4">
							<label for="user-id">User:</label> <input type="text"
								readonly="readonly" class="form-control" id="user-id"
								value="${Model.testCaseList[0].authorFullName}">
						</div>
						<div class="form-group col-md-4">
							<label for="status">Execution Type:</label> <input type="text"
								readonly="readonly" class="form-control" id="typeDeatils"
								value="${Model.testCaseList[0].exe_type}">
						</div>
						<div class="form-group col-md-4">
							<label for="status">Status:</label> <input type="text"
								readonly="readonly" class="form-control" id="statusDeatils"
								value="${Model.testCaseList[0].status}">
						</div>
						<div class="form-group col-md-4">
							<label for="created-date">Created Time:</label> <input
								type="text" class="form-control" readonly="readonly"
								id="createdDeatils"
								value="${Model.testCaseList[0].created_date}"
								pattern="dd-MM-yyyy">
						</div>
						<div class="form-group col-md-4">
							<label for="created-date">Updated Time:</label> <input
								type="text" class="form-control" readonly="readonly"
								id="updated-date"
								value="${Model.testCaseSummary[0].activity_date}">
						</div>
						<div class="form-group col-md-4" style="display: none">
							<label for="priority">Priority:</label> <select
								class="form-control" id="sel1">
								<option>Urgent</option>
								<option>High</option>
								<option>Medium</option>
								<option>Low</option>
							</select>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>

	<div class="row" id="testcaseListDiv">
		<div class="col-sm-12">
			<div class="tstmng ibox ">
				<div class="ibox-title">
					<!-- <div class="pull-right">
							<button class="btn btn-success btn-xs " type="button"
								id="addParameter" style="margin: 5px">Add Test
								Parameters</button>
							<button class="btn btn-success btn-xs " type="button"
								id="addTestcase" style="margin: 5px">Add Test Case</button>
							<button class="btn btn-success btn-xs " type="button"
								id="uploadTestcase" style="margin: 5px">Upload Test
								Case</button>
						</div> -->
					<h5>Test Cases</h5>
					<button type="button" class="btn btn-primary btn-sm" id="addTestCase" style="float:right;margin-top: -5px">Add Test Case</button>
				</div>
				<div class="ibox-content">

					<div class="table-responsive">
						<table
							class="table table-striped table-bordered table-hover dataTables-example"
							id="tblTestcaseList">
							<thead>
								<tr>
									<th style="display: none">Id</th>
									<th style="width: 70px">Id</th>
									<th>Title</th>
									<th>Summary</th>
									<th style="display: none">condition</th>
									<th>Type</th>
									<th style="display: none">Time</th>
									<th>Status</th>
									<th style="display: none">Author Full Name</th>
									<th  data-sort-ignore="true" style="width: 114px;">Action</th>
									<th style="display: none">User Photo</th>
									<th style="display: none">About Me</th>
									<th style="display: none">Activity</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="testcase" items="${Model.testCaseList}">
									<tr class="gradeX">
										<td style="display: none">${testcase.testcase_id}</td>
										<td
											onClick="goTestCaseDetails('${testcase.testcase_id}','${testcase.testcase_name}','${testcase.exe_type}','${testcase.exe_type}','${testcase.tc_status_id}','${testcase.execution_type}','${testcase.created_date}');"
											style="cursor: pointer"><a>${testcase.test_prefix}</a></td>
										<td name="${testcase.testcase_id}" class="xlTextAlignment"
											data-original-title="${testcase.testcase_name}"
											data-container="body" data-toggle="tooltip"
											data-placement="right" class="issue-info">${testcase.testcase_name}</td>
										<td>${testcase.summary}</td>
										<td style="display: none">${testcase.precondition}</td>
										<td>${testcase.exe_type}</td>
										<td style="display: none">${testcase.execution_time}</td>
										<td name="${testcase.testcase_id}"
											onclick="location.href='testcasebyprefix?tcPrefix=${testcase.test_prefix}'">${testcase.status}</td>
										<td style="display: none">${testcase.authorFullName}</td>
										<td>
											<%-- <button class="btn-white btn btn-xs edit-btn"
														onclick="viewTestcase(${testcase.testcase_id},'Edit')"
														name="${testcase.testcase_id}"><i class="fa fa-edit"></i></button> --%>
											<button class="btn-white btn btn-xs edit-btn"
												onClick="goEdit('${testcase.testcase_id}','${testcase.testcase_name}','${testcase.summary}','${testcase.precondition}','${testcase.tc_status_id}','${testcase.execution_type}');"
												data-toggle="modal">
												<i class="fa fa-edit"></i>
											</button>
											<button class="btn-danger btn btn-xs del-btn"
												data-toggle="modal"
												onClick="goDelete('${testcase.testcase_id}');">
												<i class="fa fa-trash"></i>
											</button>
											<button class="btn btn-success btn-xs "
												style="cursor: pointer" type="button" id="goSteps"
												style="margin: 5px"
												onclick="location.href='testcasebyprefix?tcPrefix=${testcase.test_prefix}'">Test
												Steps</button> <%-- <button class="btn-danger btn btn-xs del-btn"
														onclick="removeTestCase(${testcase.testcase_id})"
														name="${testcase.testcase_id}"><i class="fa fa-trash"></i></button> --%>
											<%-- <div class="btn-group">
													 <button class="btn-white btn btn-xs"
														onclick="viewTestcase(${testcase.testcase_id},'View')"
														name="${testcase.testcase_id}">View</button> 
													<button class="btn-white btn btn-xs"
														onclick="viewTestcase(${testcase.testcase_id},'Edit')"
														name="${testcase.testcase_id}"><i class="fa fa-edit"></i></button>
													<button class="btn-white btn btn-xs"
														onclick="removeTestCase(${testcase.testcase_id})"
														name="${testcase.testcase_id}"><i class="fa fa-trash"></i></button>
												</div> --%>
										</td>
										<td style="display: none">${testcase.user_photo}</td>
										<td style="display: none">${testcase.authorAboutMe}</td>
										<td style="display: none"><c:forEach
												var="testCaseSummary" items="${Model.testCaseSummary}"
												varStatus="summary">
												<c:if
													test="${testCaseSummary.test_case_id == testcase.testcase_id}">
													<div class="vertical-timeline-block">
														<div class="vertical-timeline-icon gray-bg">
															<i class="fa fa-briefcase"></i>
														</div>
														<div class="vertical-timeline-content">
															<p>${testCaseSummary.activity_desc}</p>
															<strong class=""><small class="">by </small>${testCaseSummary.user_name}</strong><br />
															<c:if test="${testCaseSummary.TimeDiff < 60 }">
																<strong class="">${testCaseSummary.TimeDiff}
																	min ago</strong>
																<br />
															</c:if>
															<c:if
																test="${testCaseSummary.TimeDiff > 60 && testCaseSummary.TimeDiff < 1440 }">
																<fmt:parseNumber var="hour" integerOnly="true"
																	type="number" value="${testCaseSummary.TimeDiff/60}" />
																<strong class="">${hour} hr ago</strong>
																<br />
															</c:if>
															<c:set var="date"
																value="${fn:split(testCaseSummary.activity_date,' ')}" />
															<%-- <c:if test="${testCaseSummary.TimeDiff > 1440}">
																	<strong class="pull-right">${date[0]} </strong>
																</c:if> --%>
															<c:if
																test="${testCaseSummary.activityDate == Model.nowDate}">
																<small class="text-muted"> ${date[1]} ${date[2]}
																</small>
															</c:if>
															<c:if
																test="${testCaseSummary.activityDate != Model.nowDate}">
																<c:choose>
																	<c:when
																		test="${testCaseSummary.activity_date == Model.prevDate}">
																		<small class="text-muted"> Yesterday -
																			${date[1]} ${date[2]} </small>
																	</c:when>
																	<c:otherwise>
																		<small class="text-muted">
																			${testCaseSummary.activity_date} </small>
																	</c:otherwise>
																</c:choose>
															</c:if>
														</div>
													</div>
												</c:if>
											</c:forEach></td>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>


	<!-- row afzal -->
	<div class="row" id="parameterDiv">
		<div class="col-md-12">
			<form name="frm">
				<div class="ibox">
					<div class="ibox-content">
						<div class="table-responsive">
							<table class="table table-bordered table-fixed" id="tableData">
								<thead>
									<tr>
										<th width="100" scope="col">Id</th>
										<th width="200" scope="col">Title</th>
										<th scope="col">Parameters</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="testcase" items="${Model.testCaseList}">
										<tr>
											<td width="100">${testcase.test_prefix}</td>
											<td width="200">${testcase.testcase_name}</td>
											<td>
												<div class="parameter-input"
													id="${testcase.test_prefix}_div">
													<c:choose>
														<c:when test="${Model.testdatas.size() > 0}">
															<c:forEach var="testdatas" items="${Model.testdatas}">
																<c:set var="dataFlag" value="0" />
																<c:choose>
																	<c:when
																		test="${testcase.testcase_id==testdatas.testcase_id}">
																		<c:set var="dataFlag" value="1" />

																		<c:choose>
																			<c:when test="${fn:contains(testdatas.data, ',')}">
																				<c:set var="counter" value="0" />
																				<c:forTokens items="${testdatas.data}" delims=","
																					var="mySplit">
																					<c:set var="counter" value="${counter+1}" />
																					<c:choose>
																						<c:when test="${counter==1}">
																							<span>
																								<div class="input-group input-width">
																									<input type="text" class="form-control"
																										id="${testcase.test_prefix}_${counter}"
																										name="p_scnt" value="${mySplit}"
																										placeholder="Parameter Name"> <a
																										href="#" class="input-group-addon"
																										onclick="addParam('${testcase.test_prefix}'); return false"><i
																										class="fa fa-plus-circle"></i></a>
																								</div>
																							</span>
																						</c:when>
																						<c:otherwise>
																							<span>
																								<div class="input-group input-width">
																									<input type="text" class="form-control"
																										id="${testcase.test_prefix}_${counter}"
																										name="p_scnt" value="${mySplit}"
																										placeholder="Parameter Name"> <a
																										href="#" class="input-group-addon"
																										onclick="removeParam('${testcase.test_prefix}',${counter}); return false"><i
																										class="fa fa-minus-circle"></i></a>
																								</div>
																							</span>
																						</c:otherwise>
																					</c:choose>
																				</c:forTokens>
																			</c:when>
																			<c:otherwise>
																				<span>
																					<div class="input-group input-width">
																						<input type="text" class="form-control"
																							id="${testcase.test_prefix}_1" name="p_scnt"
																							value="${testdatas.data}"
																							placeholder="Parameter Name"> <a href="#"
																							class="input-group-addon"
																							onclick="addParam('${testcase.test_prefix}'); return false"><i
																							class="fa fa-plus-circle"></i></a>
																					</div>
																				</span>
																			</c:otherwise>
																		</c:choose>
																	</c:when>
																	<c:otherwise>
																		<c:if test="${dataFlag > 0}">
																			<span>
																				<div class="input-group input-width">
																					<input type="text" class="form-control"
																						id="${testcase.test_prefix}_1" name="p_scnt"
																						value="${testdatas.data}"
																						placeholder="Parameter Name"> <a href="#"
																						class="input-group-addon"
																						onclick="addParam('${testcase.test_prefix}'); return false"><i
																						class="fa fa-plus-circle"></i></a>
																				</div>
																			</span>
																		</c:if>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</c:when>
														<c:otherwise>

															<span>
																<div class="input-group input-width">
																	<input type="text" class="form-control"
																		id="${testcase.test_prefix}_1" name="p_scnt"
																		placeholder="Parameter Name"> <a href="#"
																		class="input-group-addon"
																		onclick="addParam('${testcase.test_prefix}'); return false"><i
																		class="fa fa-plus-circle"></i></a>
																</div>
															</span>

														</c:otherwise>
													</c:choose>
												</div>
											</td>
											<td style="display: none">${testcase.testcase_id}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>



						</div>
						<button class="btn btn-white" style="margin-right: 15px;"
							type="button" id="btnCancel3" type="button" tabindex="6">Cancel</button>
						<button class="btn btn-success" type="button" id="saveData"
							style="margin: 5px" onclick="getTableData()">Submit</button>
					</div>
			</form>
		</div>

	</div>


</div>
</div>

<div class="modal fade editTestCaseModal" id="addTestcaseDiv"
	tabindex="-1" role="dialog" aria-labelledby="editTestCaseModalLabel"
	aria-hidden="true" style="display: none;">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h5 class="modal-title" id="editTestCaseModalLabel">Add Test
					Case</h5>
			</div>
			<div class="modal-body">
				<div class="ibox">
					<div class="ibox-content">
						<form action="inserttestcase" id="createTestcaseForm" method="POST"
							class="wizard-big wizard clearfix form-horizontal"
							enctype="multipart/form-data">
							<div class="row">
								<label class="col-lg-6 pull-right ">* fields are
									mandatory</label>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group hidden">
										<div class="col-sm-6">
											<input type="text" class="form-control"
											value="${Model.scenarioDetails[0].scenario_id}"
											name="scenarioId" id="activeScenario">
										</div>
									</div>
									<div class="form-group hidden">
										<div class="col-sm-6">
											<input type="text" class="form-control characters"
													value="${Model.scenarioDetails[0].scenario_name}"
													name="scenarioName" id="activeScenario">
										</div>
									</div>
									<div class="form-group hidden">
										<div class="col-sm-8">
											<input type="text" class="form-control"
											value="${Model.moduleId}" name="moduleId" id="activeModule">
										</div>
									</div>
									<div class="form-group hidden">
										<div class="col-sm-8">
											<input type="text" class="form-control characters"
											value="${Model.moduleName}" name="activeModuleName"
											id="activeModuleName">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Title *:</label>
										<div class="col-sm-8">
											<input type="text" class="form-control characters"
												placeholder="Testcase Name" name="testcaseName" tabindex="1">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Summary:</label>
										<div class="col-sm-8">
											<input type="text" class="form-control characters"
												placeholder="Testcase Summary" name="testcaseSummary"
												tabindex="1">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Precondition:</label>
										<div class="col-sm-8">
											<input type="text" class="form-control characters"
												placeholder="Testcase Precondition"
												name="testcasePrecondition" tabindex="1">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Status :</label>
										<div class="col-sm-8">
											<select name="statusId" class="form-control"
												data-placeholder="Choose a Status" tabindex="6">
												<c:forEach var="status" items="${Model.statusList}">
													<option value="${status.tc_status_id}">${status.status}</option>
												</c:forEach>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Execution Type:</label>
										<div class="col-sm-10">
											<div class="radio radio-primary radio-inline col-sm-3">
												<input type="radio" id="inlineRadio1"
													value="${executionTypes[0].execution_type_id}"
													name="executionType" checked="" tabindex="3"> <label
													for="inlineRadio1">
													${executionTypes[0].description} </label>
											</div>
											<c:if test="${executionTypes[1].execution_type_id > 0}">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" id="inlineRadio2"
														value="${executionTypes[1].execution_type_id}"
														name="executionType" tabindex="4"> <label
														for="inlineRadio2">
														${executionTypes[1].description} </label>
											</div>
											</c:if>
											<c:if test="${executionTypes[2].execution_type_id > 0}">
											<div class="radio radio-default radio-inline col-sm-3">
												<input type="radio" id="inlineRadio3"
														value="${executionTypes[2].execution_type_id}"
														name="executionType" tabindex="5"> <label
														for="inlineRadio3">
														${executionTypes[2].description} </label>
											</div>
											</c:if>
										</div>
									</div>

									<div id="datasheetStatusDiv" class="form-group hidden">
										<div class="form-group">
											<label class="col-sm-2 control-label">Datasheet
												Present:</label>
											<div class="col-sm-10">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="datasheetStatus1" value="1"
													name="datasheetStatus" tabindex="5"> <label
													for="datasheetStatus1">YES</label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="datasheetStatus2" value="2"
													name="datasheetStatus" checked="" tabindex="6"> <label
													for="datasheetStatus2">NO</label>
												</div>

												<div class="col-sm-6" id="uploadDatasheetDiv">
													<input type="file" class="filestyle" name="uploadDatasheet"
													id="uploadDatasheetBtn1" data-buttonName="btn-primary"
													required="true">
												</div>

											</div>
										</div>
									</div>


									<div class="form-group hidden">
										<label class="col-sm-2 control-label">Execution Time *
											:</label>
										<div class="col-sm-8">
											<input type="text" min="0" value="0" class="form-control"
												placeholder="Estimated Execution Time in Minutes"
												name="executionTime">
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
										<button class="btn btn-success pull-right ladda-button"
											id="uploadTestcaseBtn" data-style="slide-up" tabindex="7">Submit</button>&nbsp;
										<button class="btn btn-white pull-right" type="button"
											style="margin-right: 15px;" id="btnCancel">Cancel</button>
										
									</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 

<!-------- upload test case -------------->

<div class="row" id="uploadTestcaseDiv" style="display: none;">
	<div class="col-md-12">
		<div class="ibox">
			<form action="bulkinserttestcase"
				class="wizard-big wizard clearfix form-horizontal" method="POST"
				id="uploadTestcaseForm" enctype="multipart/form-data">
				<div class="content clearfix">
					<fieldset class="body current">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group hidden">
									<div class="col-sm-6">
										<input type="text" class="form-control"
											value="${Model.scenarioDetails[0].scenario_id}"
											name="scenarioId" id="activeScenario">
									</div>
								</div>
								<div class="form-group hidden">
									<div class="col-sm-6">
										<input type="text" class="form-control characters"
											value="${Model.scenarioDetails[0].scenario_name}"
											name="scenarioName" id="activeScenario">
									</div>
								</div>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" class="form-control"
											value="${Model.moduleId}" name="moduleId" id="activeModule">
									</div>
								</div>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" class="form-control characters"
											value="${Model.moduleName}" id="activeModuleName">
									</div>
								</div>

								<div class="col-lg-12">

									<div class="form-group" id="datasheetStatusDiv">
										<label class="col-sm-2 control-label" style="text-align:left">Datasheet : </label>
										<div class="col-sm-10">

											<div class="col-sm-6" id="uploadDatasheetDiv">
												<input type="file" class="filestyle" name="uploadDatasheet"
													id="uploadDatasheetBtn" data-buttonName="btn-primary"
													required="true">
											</div>

										</div>

									</div>

									<div class="form-group hidden">
										<label class="col-sm-2 control-label">Execution Time
											*:</label>
										<div class="col-sm-10">
											<input type="text" min="0" value="0" class="form-control"
												placeholder="Estimated Execution Time in Minutes"
												name="executionTime">
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<button class="btn btn-white pull-left"
												style="margin-right: 15px;" type="button" id="btnCancel2"
												type="button" tabindex="6">Cancel</button>
											<button class="btn btn-success pull-left ladda-button"
												id="uploadTestcaseBtn" data-style="slide-up" tabindex="7">Submit</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="hr-line-solid"></div>
					</fieldset>
				</div>

			</form>
		</div>

	</div>
</div>

<!-- Edit Modal Start -->
<div class="modal fade editTestCaseModal" id="editTestCaseModal"
	tabindex="-1" role="dialog" aria-labelledby="editTestCaseModalLabel"
	aria-hidden="true" style="display: none;">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h5 class="modal-title" id="editTestCaseModalLabel">Edit Test
					Case</h5>
			</div>
			<div class="modal-body">
				<div class="ibox">
					<div class="ibox-content">
						<form action="updatetestcase" id="editTestcaseform" method="POST"
							class="wizard-big wizard clearfix form-horizontal"
							enctype="multipart/form-data">
							<div class="row">
								<label class="col-lg-6 pull-right ">* fields are
									mandatory</label>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group hidden">
										<div class="col-sm-6">
											<input type="text" class="form-control"
												value="${Model.scenarioDetails[0].scenario_id}"
												name="scenarioId" id="activeScenario">
										</div>
									</div>
									<div class="form-group hidden">
										<div class="col-sm-8">
											<input type="text" class="form-control"
												value="${Model.moduleId}" name="moduleId" id="activeModule">
										</div>
									</div>
									<div class="form-group hidden">
										<div class="col-sm-8">
											<input type="text" class="form-control characters"
												value="${Model.moduleName}" id="activeModuleName">
										</div>
									</div>
									<div class="form-group hidden">
										<div class="col-sm-8">
											<input type="text" class="form-control" value=""
												name="testcaseId" id="updateTestcaseId">
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-2 control-label">Title *:</label>
										<div class="col-sm-8">
											<input type="text" class="form-control characters"
												placeholder="Testcase Name" value="" name="testcaseName"
												id="updateTestCaseName">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Summary:</label>
										<div class="col-sm-8" id="txtAreaSummary">
											<input type="text" placeholder="Testcase Summary"
												class="form-control characters tcSummernote"
												style="resize: none; border: 1px solid #ccc;"
												name="testcaseSummary" id="updateTestcaseSummary" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Precondition:</label>
										<div class="col-sm-8" id="txtAreaCondition">
											<input type="text" placeholder="Testcase Precondition"
												class="form-control characters tcSummernote"
												style="resize: none; border: 1px solid #ccc;"
												name="testcasePrecondition" id="updateTestcasePrecondition">${Model.testcaseDetails[0].precondition}
											</textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Status :</label>
										<div class="col-sm-8">
											<select name="statusId" id="updateStatusId"
												class="form-control" data-placeholder="Choose a Status">
												<c:forEach var="status" items="${Model.statusList}">
													<option value="${status.tc_status_id}">${status.status}</option>
												</c:forEach>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Execution Type:</label>
										<div class="col-sm-10">
											<div class="radio radio-primary radio-inline col-sm-3">
												<input type="radio" id="updateInlineRadio1"
													value="${executionTypes[0].execution_type_id}"
													name="executionType"
													${executionTypes[0].execution_type_id == Model.testcaseDetails[0].execution_type ?'checked=""':''}>
												<label for="inlineRadio1">
													${Model.executionTypes[0].description} </label>
											</div>
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" id="updateInlineRadio2"
													value="${executionTypes[1].execution_type_id}"
													name="executionType"
													${executionTypes[1].execution_type_id == Model.testcaseDetails[0].execution_type ?'checked=""':''}>
												<label for="inlineRadio2">
													${executionTypes[1].description} </label>
											</div>
											<div class="radio radio-default radio-inline col-sm-3">
												<input type="radio" id="updateInlineRadio3"
													value="${executionTypes[2].execution_type_id}"
													name="executionType"
													${executionTypes[2].execution_type_id == Model.testcaseDetails[0].execution_type ?'checked=""':''}>
												<label for="inlineRadio3">
													${executionTypes[2].description} </label>
											</div>
										</div>
									</div>

									<div id="datasheetStatusDiv" class="form-group hidden">
										<div class="form-group">
											<label class="col-sm-2 control-label">Datasheet
												Present:</label>
											<div class="col-sm-10">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="datasheetStatus1" value="1"
														name="datasheetStatus" tabindex="5"
														${Model.testcaseDetails[0].excel_file_present_status == '1' ?'checked=""':''}>
													<label for="datasheetStatus1">YES</label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="datasheetStatus2" value="2"
														name="datasheetStatus" tabindex="6" checked="checked">
													<label for="datasheetStatus2">NO</label>
												</div>

												<div class="col-sm-6" id="uploadDatasheetDiv">
													<input type="file" class="filestyle" name="uploadDatasheet"
														id="uploadDatasheetBtn2" data-buttonName="btn-primary">
												</div>

											</div>
										</div>
										<c:if
											test="${Model.testcaseDetails[0].excel_file_present_status == '1'}">
											<div class="form-group" id="datasheetFileDiv">
												<label class="col-sm-2 control-label">Uploaded File
													:</label>
												<div class="col-sm-8">
													<input class="hidden" id="datasheetLabel"
														value="${Model.datasheetFileName}" /> <label
														class="control-label">${Model.datasheetFileName}</label>
												</div>
											</div>
										</c:if>
									</div>


									<div class="form-group hidden">
										<label class="col-sm-2 control-label">Execution Time *
											:</label>
										<div class="col-sm-8">
											<input type="text" class="form-control"
												placeholder="Estimated Execution Time in Minuit"
												name="executionTime" value="0" id="executionTime">
										</div>
									</div>
								</div>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-5">
										<button class="btn btn-white pull-left hidden" type="button"
											style="margin-right: 15px;" id="btnCancel">Cancel</button>
										<c:if test="${Model.recentFlag == 1}">
											<button class="btn btn-success pull-left" type="button"
												id="btnEdit">Edit</button>
											<button class="btn btn-success pull-left" type="button"
												style="display: none" id="btnUpdate">Update</button>
										</c:if>
										<c:if test="${Model.recentFlag == 0}">
											<button class="btn btn-success pull-left" type="button"
												id="btnUpdate">Update</button>
										</c:if>
									</div>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="editTestcaseBtn">Submit</button>
			</div>
		</div>
	</div>
</div>
<!-- Edit Modal End -->

<!-- Delete Modal Start -->

<div id="deleteTestCaseModal" class="modal fade deleteTestCaseModal">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="material-icons">&#xE5CD;</i>
				</div>
				<h4 class="modal-title">Are you sure?</h4>
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p>Do you really want to delete this record? This process cannot
					be undone.</p>
			</div>
			<form id="deleteTestcaseform" method="post" action="deletetestcase">
				<input type="hidden" name="deleteTestcaseId" id="deleteTestcaseId" />
				<div class="modal-footer">
					<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-danger" id="deleteTestcaseBtn">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- Delete Modal End -->

<script type="text/javascript">
        var i=0;
        function addParam (id) {
        	 var scntDiv = $('#'+id+'_div');
             i = $('#'+id+'_div span').length + 1;
             var nameId=id+'_'+i;
			//	alert(nameId);
        	 $('<span><div class="input-group input-width"><input type="text" class="form-control"  id="'+nameId+'" name="p_scnt_' + i + '" value="" placeholder="Parameter Name"><a href="#" class="input-group-addon" onclick="removeParam('+"'"+id+"',"+i+'); return false"><i class="fa fa-minus-circle"></i></a></div></span>').appendTo(scntDiv);
          		i++;
            return false;        
           }
        function removeParam (id,i) {
        //	alert(id);
        	 var myDiv = document.getElementById(id+"_div");
         	var spans = myDiv.getElementsByTagName("span");   
       	 	 myDiv.removeChild(spans[i-1]);
              return false;   
          }
        
        function getTableData() {
        	//alert('hi');
        	var n1 = document.getElementById("tableData").rows.length;

        	var i=0,j=0;
        	var str="";
        	 
        	for(i=1; i<n1;i++){
        	 
        	var x=document.getElementById("tableData").rows[i].cells.item(0).innerHTML;

        	// alert(x) ;
        	 var y=document.getElementById("tableData").rows[i].cells.item(1).innerHTML;

        	 //alert(y) ;
        	 var z=getAllValues(x);

        	 //alert(z) ;
        	 var id=document.getElementById("tableData").rows[i].cells.item(3).innerHTML;
//         	 alert(id),
        	saveData(id,z);
        	}
        	location.reload();
			showToster('Test Data saved successfully!.');
        }
        function getAllValues(x) {
        	var str='';
            var inputValues = $('#'+x+'_div :input').map(function() {
            	str+=$(this).val()+',';
            })
            return str;
        }
        function saveData(id,z) {
        	var flag=0
//         	alert(id+":"+z);
        	var dataPram='testcaseId='+id+'&testData='+z;
        	$.ajax({        
        	    url: 'insertdata' ,
        	    data: dataPram,
        	    type: 'POST',
        	    success: function (resp) {
        	        flag=1;
        	    }
//         	    success: function (resp) {
//         	        alert(resp);
//         	    },
//         	    error: function(e){
//         	        alert('Error: '+e);
//         	    }  
        	});
        	return flag;
        }
    </script>

<input type="hidden" id="hiddenInput"
	value=<%=session.getAttribute("TcCreateStatus")%>>

<input type="hidden" id="newTcName"
	value='<%=session.getAttribute("newTcName")%>'>
<script type="text/javascript">
$(window).load(function() {

	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
});

  $(function () {
	    $('.summernote').summernote();
	    $('.readOnlyDiv .note-toolbar').remove();
	    $(".readOnlyDiv .note-editor").attr("style", "background: #eeeeee !important; border : 1px solid #ccc !important ");
	    $('.readOnlyDiv .note-editor').css('pointer-events','none');		  
		
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	  $("#vertical-timeline").html($("#tblTestcaseList tbody tr:first td:eq(12)").html());
	  
	if(!$("#inlineRadio2").val())
		{
		$("#inlineRadio2").parent('div').remove();
		}
	if(!$("#inlineRadio3").val())
	{
		$("#inlineRadio3").parent('div').remove();
	}
	  var state = $('#hiddenInput').val();
		var name = $('#newTcName').val();
		if(state == 1){
			var message =" Test case is successfully created";
			var toasterMessage = name +  message;
			showToster(toasterMessage);
		}else if(state == 2){
			showToster(" Test case is successfully updated.");
		}else if(state == 4){
			showToster(" Test case is successfully deleted.");
		}
		
		 $('#hiddenInput').val(3);
	});
	  function showToster(toasterMessage){
			 toastr.options = {
					 "closeButton": true,
					  "debug": true,
					  "progressBar": true,
					  "preventDuplicates": true,
					  "positionClass": "toast-top-right",
					  "showDuration": "400",
					  "hideDuration": "1000",
					  "timeOut": "9000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "slideDown",
					  "hideMethod": "slideUp"
	          };
	          toastr.success('',toasterMessage);
		}  
	
</script>
<script type="text/javascript">
$(function() {
 		var table = $('#tblTestcaseList').DataTable({
 			dom : 'Bfrtip',
 			buttons : [
//				'copyHtml5',
//	            'excelHtml5',
//	            'csvHtml5',	
//	            'pdfHtml5'
	            {
	                extend: 'excelHtml5',
	                exportOptions: {
	                    columns: [ 1,2,3,4,5,7]
	                }
	            },
	            {
	                extend: 'csvHtml5',
	                exportOptions: {
	                    columns: [ 1,2,3,4,5,7 ]
	                }
	            },
	            {
	                extend: 'pdfHtml5',
	                exportOptions: {
	                    columns: [ 1,2,3,4,5,7 ]
	                }
	            } 
	            ],
	            "paging" : true,
				"lengthChange" : true,
				"searching" : true,
				"ordering" : true,
				"info" : true,
				"autoWidth" : true
		    
 		});
 		
 		
//  		var scenarioList=[];
//  		scenarioList=${Model.scenarioList};
//  		var moduleId = $("#activeModule").val();
//  		//alert("moduleId"+moduleId);
//  		var moduleName = $("#activeModuleName").val();
//  			for(var i=0;i<scenarioList.length;i++)
//  				{
//  				if(moduleId == scenarioList[i].module_id){
//  				scenarioMenu = "<ul class='nav nav-second-level collapse-in'><li id='"+scenarioList[i].scenario_id+"' class='scenarioLinks'>"+
//  				                "<a class='lgMenuTextAlignment menuAnchor menuTooltip' href='#' data-original-title='"+scenarioList[i].scenario_name+"' data-container='body' data-toggle='tooltip' data-placement='right' onclick='getTestCaseList("+scenarioList[i].scenario_id+",\""+scenarioList[i].scenario_name+"\","+moduleId+",\""+moduleName+"\")'>"+scenarioList[i].scenario_name+"</a></li></ul>";
//  				  $("#module_"+moduleId).append(scenarioMenu);
//  				  $("#module_"+moduleId).addClass("active");
//  				}
//  				}
 		  
		//tooltip
		$('[data-toggle="tooltip"]').tooltip(); 
 	});

$("#addTestcase").click(function(){
   /* $("#createTestcaseDiv").removeClass('hidden');
   $(this).addClass('hidden');
	$("input[name=testcaseName]").focus();
	
	$('.tcSummernote').summernote();
	$('.note-editor').css('background','white');
	$('button[ data-event="codeview"]').remove();
	$('.note-editor').css('border','1px solid #CBD5DD');
	$('div.note-insert').remove();
	$('div.note-table').remove();
	$('div.note-help').remove();
	$('div.note-style').remove();
	$('div.note-color').remove();
	$('button[ data-event="removeFormat"]').remove();
	$('button[ data-event="insertUnorderedList"]').remove();
	$('button[ data-event="fullscreen"]').remove();
	$('button[ data-original-title="Line Height"]').remove();
	$('button[ data-original-title="Font Family"]').remove();
	$('button[ data-original-title="Paragraph"]').remove();
	
	$("#testcaseListDiv").fadeOut(); */
   
	 $("#barInMenu").removeClass("hidden");
	 $("#wrapper").addClass("hidden");
	 window.location.href="createtestcase"; 
	 
});

$(".viewtestcase").click(function(){
	
	 $('body').addClass("white-bg");
	 $("#barInMenu").removeClass("hidden");
	 $("#wrapper").addClass("hidden");	

	 var id=$(this).attr("name");
	 var posting = $.post('settestcaseid', {
			testcaseId : id,
			viewType : 'View'
		});
	 
		 posting.done(function(data) {
			window.location.href="teststeps"; 
		}); 
});

 $("#cancelUpdate").click(function(){
	 $("#editTestcaseDiv").addClass('hidden'); 
	 $("#addTestcase").removeClass('hidden');
 });
 
 
 function viewTestcase(id,viewType)
	{
	 
	 $('body').addClass("white-bg");
	 $("#barInMenu").removeClass("hidden");
	 $("#wrapper").addClass("hidden");	
	 
	 var posting = $.post('settestcaseid', {
			testcaseId : id,
			viewType : viewType
		});
		 posting.done(function(data) {
			window.location.href="teststeps"; 
		}); 
	}
 $("#btnEdit").click(function(){
	 $("#btnEdit").addClass("hidden");
	 $("#btnSubmit").removeClass("hidden");
	 $("#cancelUpdate").removeClass("hidden");
	 $("#fieldEditTestCase").prop("disabled", false);
 });
 $("#createTestcaseForm").validate({
		rules : {
			testcaseName : {
				required : true,
				minlength : 1,
				maxlength : 250
			},
			executionTime :{
				required : true,
				number : true
			},
			testcaseSummary : {
				minlength : 1,
				maxlength : 4000
			},
		}
		/* messages: {
			executionTime:{
				required: "Estimated execution time in Minutes."
			} 
		}*/
	});

 
$('#createTCbtn').click(function() {
	if($('#createTestcaseForm').valid()){
		  $(this).attr("disabled","true");
		  
		  $('body').addClass("white-bg");
		  $("#barInMenu").removeClass("hidden");
		  $("#wrapper").addClass("hidden");	
		 
		  $('.tcSummernote').each( function() {
			  $(this).val($(this).code());
		   });
		  
		  $('#createTestcaseForm').submit();
	  }
	});
	
	 function removeTestCase(testcase_id){
			
			swal({
				title : "Are you sure?",
				text : "Do you want to remove test case!",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "Yes, Remove it!",
				cancelButtonText : "No, cancel!",
				closeOnConfirm : true,
				closeOnCancel : true
			}, function(isConfirm) {

				if (isConfirm) {
					
					$('body').addClass("white-bg");
					$("#barInMenu").removeClass("hidden");
					$("#wrapper").addClass("hidden");
					
					var posting = $.post('removetestcase',{
						testcaseId : testcase_id
					});
					
					posting.done(function(data){
						window.location.href = "testcase";
					}); 	
				}
			});
			
		}
</script>

<%
	//set session variable to another value
	session.setAttribute("TcCreateStatus", 3);
%>

<style>
#parameterDiv {
	display: none;
}

.input-width {
	width: 200px;
	margin: 0 5px;
}

.parameter-input {
	display: flex;
}

.table-fixed {
	table-layout: fixed;
}
</style>
<script>
$('#addParameter').click(function() {
	document.getElementById("addTestCase").classList.remove('active');
	document.getElementById("uploadTestCaseMenu").classList.remove('active');
	document.getElementById("homeMenu").classList.remove('active');
	document.getElementById("addParameter").classList.add('active');
	   $('#testcaseListDiv').css('display','none');
	   $('#testcaseDetails').css('display','none');
	   $('#addTestcaseDiv').css('display','none');
	   $('#uploadTestcaseDiv').css('display','none');
	   $('#parameterDiv').css('display','block');
	   $('#parameterDiv').css('padding-bottom','25px');
	});
$('#homeMenu').click(function() {
	document.getElementById("addParameter").classList.remove('active');
	document.getElementById("addTestCase").classList.remove('active');
	document.getElementById("uploadTestCaseMenu").classList.remove('active');
	document.getElementById("homeMenu").classList.add('active');
	   $('#testcaseListDiv').css('display','block');
	   $('#testcaseDetails').css('display','block');
	   $('#parameterDiv').css('display','none');
	   $('#addTestcaseDiv').css('display','none');
	   $('#uploadTestcaseDiv').css('display','none');
	});
$('#addTestCase').click(function() {
	document.getElementById("addParameter").classList.remove('active');
	document.getElementById("uploadTestCaseMenu").classList.remove('active');
	document.getElementById("homeMenu").classList.remove('active');
	document.getElementById("addTestCase").classList.add('active');
	   $('#testcaseListDiv').css('display','block');
	   $('#testcaseDetails').css('display','block');
	   $('#parameterDiv').css('display','none');
	   $('#uploadTestcaseDiv').css('display','none');
	   //$('#addTestcaseDiv').css('display','block');
	   $('#addTestcaseDiv').modal('show'); 
	   
	});
$('#uploadTestCaseMenu').click(function() {
	document.getElementById("addParameter").classList.remove('active');
	document.getElementById("addTestCase").classList.remove('active');
	document.getElementById("homeMenu").classList.remove('active');
	document.getElementById("uploadTestCaseMenu").classList.add('active');
	   $('#testcaseListDiv').css('display','none');
	   $('#testcaseDetails').css('display','none');
	   $('#parameterDiv').css('display','none');
	   $('#addTestcaseDiv').css('display','none');
	   $('#uploadTestcaseDiv').css('display','block');
	   
	});
// $(".edit-btn").click(function() {
// 	alert('hi')
// 	var id =  $(this).closest( "tr").children('td:eq(0)').html();
//  	var title =  $(this).closest( "tr").children('td:eq(2)').html();
//  	var summery =  $(this).closest( "tr").children('td:eq(3)').html();
//  	var precondition =  $(this).closest( "tr").children('td:eq(4)').html();

//  	var executionTypes =  $(this).closest( "tr").children('td:eq(5)').html();
// 	alert(executionTypes);
// 	document.getElementById('#_1234').checked = true;
//  	var status =  $(this).closest( "tr").children('td:eq(7)').html();
// 	alert(status);
// 	document.getElementById('updateTestcaseId').value=id;
//  	document.getElementById('updateTestCaseName').value=title;
//  	document.getElementById('updateTestcaseSummary').value=summery;
// 		$('#editTestCaseModal').modal('show'); 
// });

 $("#editTestcaseBtn").click(function(){        
        $("#editTestcaseform").submit(); 
    });
 $("#deleteTestcaseBtn").click(function(){        
     $("#deleteTestcaseform").submit(); 
 });
 function goTestCaseDetails(id,name,status,type,status_id,execution_type,created_date){
	 	document.getElementById('nameDeatils').value=name;
	 	document.getElementById('typeDeatils').value=type;
	 	document.getElementById('statusDeatils').value=status;
	 	document.getElementById('createdDeatils').value=created_date;
	}
 function goDelete(id){
		document.getElementById('deleteTestcaseId').value=id;
		$('#deleteTestCaseModal').modal('show'); 
	}
function goEdit(id,name,summery,precondition,status_id,execution_type){
	document.getElementById('updateTestcaseId').value=id;
 	document.getElementById('updateTestCaseName').value=name;
 	document.getElementById('updateTestcaseSummary').value=summery;
 	document.getElementById('updateTestcasePrecondition').value=precondition;
 	//alert(status_id);
//  	$('#updateStatusId option').val(status_id).prop('selected', true);
 	$('#updateStatusId option[value='+status_id+']').prop('selected', true);
 	if(execution_type==1){
 	 	document.getElementById('updateInlineRadio2').checked = false;
 	 	document.getElementById('updateInlineRadio3').checked = false;
 	 	document.getElementById('updateInlineRadio1').checked = true;
 	}else if(execution_type==2){
 	 	document.getElementById('updateInlineRadio1').checked = false;
 	 	document.getElementById('updateInlineRadio3').checked = false;
 	 	document.getElementById('updateInlineRadio2').checked = true;
 	}else{
 		document.getElementById('updateInlineRadio1').checked = false;
 	 	document.getElementById('updateInlineRadio2').checked = false;
 	 	document.getElementById('updateInlineRadio3').checked = true;
 	}
	$('#editTestCaseModal').modal('show'); 
}
</script>


<!-- create test case page script -->
<script>
$(window).load(function() {

	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	if ($('#datasheetStatus1').is(':checked')) {
		$("#uploadDatasheetDiv").removeClass("hidden")
	}

// 	if ($('#datasheetStatus2').is(':checked')) {
// 		$("#uploadDatasheetDiv").addClass("hidden")
// 	}
	
// 	if ($('#inlineRadio1').is(':checked')) {
// 		$("#datasheetStatusDiv").addClass("hidden")
// 	}
	
	
// 	if ($('#inlineRadio2').is(':checked') || $('#inlineRadio3').is(':checked')) {
// 		$("#datasheetStatusDiv").removeClass("hidden")
// 	}

	var page= <%=request.getParameter("page")%>;
	 if(page == null){
		 page = 1;
	 }
	 
	 var allTcCount='${Model.allTcCount}';
	 
	 var pageSize='${Model.pageSize}';
	 
	 var lastRec=((page-1)*pageSize+parseInt(pageSize));
	 
	 if(lastRec>allTcCount)
	 	lastRec=allTcCount;
	 var showCount = ((page-1)*pageSize+1);
	 if(showCount <= lastRec){
		 $("#tblTestcaseList_info").html("Showing "+showCount+" to "+lastRec+" of "+allTcCount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="prevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="nextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
	 }else{
		 $("#tblTestcaseList_info").html("");
	 }
	
	 if(page == 1){
		 $("#prevBtn").attr('disabled','disabled');
	 }
	 
	 if(lastRec == allTcCount){
		 $("#nextBtn").attr('disabled','disabled');
	 }
	 
	    $("#prevBtn").click(function() {
	    	$("#barInMenu").removeClass("hidden");
	    	$("#wrapper").addClass("hidden");
	    	
	    	if(page == 1)
	    		window.location.href = "apitestcase";
	    	else
	    		window.location.href = "apitestcase?page="+(page - 1);
		});
		
		$("#nextBtn").click(function(){
			$("#barInMenu").removeClass("hidden");
	    	$("#wrapper").addClass("hidden");
	    	
			window.location.href = "apitestcase?page="+(page + 1);
		});
});

// $('#datasheetStatus1').click(function() {
// 	if ($('#datasheetStatus1').is(':checked')) {
// 		$("#uploadDatasheetDiv").removeClass("hidden")
// 	}
// 	;
// 	if ($('#datasheetStatus2').is(':checked')) {
// 		$("#uploadDatasheetDiv").addClass("hidden")
// 	}
// 	;
// });

// $('#datasheetStatus2').click(function() {
// 	if ($('#datasheetStatus1').is(':checked')) {
// 		$("#uploadDatasheetDiv").removeClass("hidden")
// 	}
// 	;
// 	if ($('#datasheetStatus2').is(':checked')) {
// 		$("#uploadDatasheetDiv").addClass("hidden")
// 	}
// 	;
// });

// $('#inlineRadio1').click(function() {
// 	if ($('#inlineRadio1').is(':checked')) {
// 		$("#datasheetStatusDiv").addClass("hidden")
// 	};
// });

// $('#inlineRadio2').click(function() {
// 	if ($('#inlineRadio2').is(':checked')) {
// 		$("#datasheetStatusDiv").removeClass("hidden")
// 	};
	
// 	if ($('#inlineRadio1').is(':checked')) {
// 		$("#datasheetStatusDiv").addClass("hidden")
// 	};
// });

// $('#inlineRadio3').click(function() {
// 	if ($('#inlineRadio3').is(':checked')) {
// 		$("#datasheetStatusDiv").removeClass("hidden")
// 	};
	
// 	if ($('#inlineRadio1').is(':checked')) {
// 		$("#datasheetStatusDiv").addClass("hidden")
// 	};
// });

	$('#createTCbtn').click(function() {
		if ($('#createTestcaseForm').valid()) {

			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");

			$('.summernote').each(function() {
				$(this).val($(this).code());
			});

			$('#createTestcaseForm').submit();
		}
	});
	$('#createTCbtn2').click(function() {
		if ($('#uploadTestcaseForm').valid()) {

			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");

			$('.summernote').each(function() { 
				$(this).val($(this).code());
			});

			$('#uploadTestcaseForm').submit();
		}
	});
	 function getScenarios(moduleId,moduleName){
         sessionStorage.setItem('selectedTestModuleId',moduleId);
         sessionStorage.setItem('selectedTestModuleName',moduleName);
          $(".moduleLinks").find('ul').remove();
            $(".moduleLinks").append("<ul class='nav nav-second-level collapse-in'></ul>");
            scenarioList=${Model.scenarioList};
            for(var i=0;i<scenarioList.length;i++)
                {
                if(moduleId==scenarioList[i].module_id){
                var tbody = "<tr style='cursor: pointer'><td style='display: none'>"+scenarioList[i].scenario_id+"</td><td class='textAlignment' data-original-title='"+scenarioList[i].scenario_name+"' data-container='body' data-toggle='tooltip' data-placement='right' class='issue-info'>"+scenarioList[i].scenario_name+"</td><td class='hidden'>"+scenarioList[i].scenario_description+"</td><td class='hidden'>"+"https://jadeglobaldemo.atlassian.net/browse/"+scenarioList[i].jira_story_id+"</td><td class='hidden'>"+scenarioList[i].jira_story_id+" "+scenarioList[i].jira_story_name+"</td><td class='text-right'>"+
                 "<div class='btn-group'><button class='btn-white btn btn-xs' onclick='viewScenario("+scenarioList[i].scenario_id+",\"View\")' name='scenario_"+scenarioList[i].scenario_id+"'>View</button>"+
                 "<button class='btn-white btn btn-xs' onclick='removeScenario("+scenarioList[i].scenario_id+")' name='scenario_"+scenarioList[i].scenario_id+"'>Delete</button>"+
                "<button class='btn-white btn btn-xs' onclick='viewScenario("+scenarioList[i].scenario_id+",\"Edit\")' name='scenario_"+scenarioList[i].scenario_id+"'>Edit</button> </div></td></tr>";
                $("#scenarioBody").append(tbody);
                scenarioMenu = "<li id='"+scenarioList[i].scenario_id+"' class='scenarioLinks'>"+
                                "<a class='lgMenuTextAlignment menuAnchor menuTooltip' href='#' data-original-title='"+scenarioList[i].scenario_name+"' data-container='body' data-toggle='tooltip' data-placement='right' onclick='getTestCaseList("+scenarioList[i].scenario_id+",\""+scenarioList[i].scenario_name+"\","+moduleId+",\""+moduleName+"\")'>"+scenarioList[i].scenario_name+"</a></li>";
                  $("#module_"+moduleId).find('ul').append(scenarioMenu);
                  $("#module_"+moduleId).addClass("active");
                 
                  $('[data-toggle="tooltip"]').tooltip();
                    //tooltip
                    if(!$('.menuSpan').hasClass('menuTextAlignment')) {
                        //turn on the tooltips
                        $('.menuTooltip').tooltip('disable');
                    }
                }
                }
     }
</script>
<!-- Upload Test Case  Page Script -->
<script>
	$(window).load(function() {
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
		
		$(":file").filestyle({
			buttonName : "btn-primary"
		});

	});	

	$(function() {
		//form validation
		$("#uploadTestcaseForm").validate({
			rules : {
				testcaseName : {
					required : true,
					minlength : 1,
					maxlength : 250
				},
				executionTime : {
					required : true,
					number : true
				},
				testcaseSummary : {
					minlength : 1,
					maxlength : 4000
				},
			}
		});
		
	});

	$('#createTCbtn').click(function() {
		if ($('#uploadTestcaseForm').valid()) {

			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");

			$('.summernote').each(function() {
				$(this).val($(this).code());
			});

			$('#uploadTestcaseForm').submit();
		}
	});
	$("#btnCancel").click(function(){
		document.getElementById("addParameter").classList.remove('active');
		document.getElementById("addTestCase").classList.remove('active');
		document.getElementById("uploadTestCaseMenu").classList.remove('active');
		   $('#testcaseListDiv').css('display','block');
		   $('#testcaseDetails').css('display','block');
		   $('#parameterDiv').css('display','none');
		   //$('#addTestcaseDiv').css('display','none');
		   $('#uploadTestcaseDiv').css('display','none');
		   $('#addTestcaseDiv').modal('hide');
		   
	 });
	$("#btnCancel2").click(function(){
		document.getElementById("addParameter").classList.remove('active');
		document.getElementById("addTestCase").classList.remove('active');
		document.getElementById("uploadTestCaseMenu").classList.remove('active');
		   $('#testcaseListDiv').css('display','block');
		   $('#testcaseDetails').css('display','block');
		   $('#parameterDiv').css('display','none');
		   $('#addTestcaseDiv').css('display','none');
		   $('#uploadTestcaseDiv').css('display','none');
	 });
	$("#btnCancel3").click(function(){
		document.getElementById("addParameter").classList.remove('active');
		document.getElementById("addTestCase").classList.remove('active');
		document.getElementById("uploadTestCaseMenu").classList.remove('active');
		   $('#testcaseListDiv').css('display','block');
		   $('#testcaseDetails').css('display','block');
		   $('#parameterDiv').css('display','none');
		   $('#addTestcaseDiv').css('display','none');
		   $('#uploadTestcaseDiv').css('display','none');
	 });
</script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/codemirror/mode/filestyle/bootstrap-filestyle.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/codemirror/mode/filestyle/bootstrap-filestyle.js"></script>

<style>
.ibox-content{
padding:10px;
}
.md-skin .wrapper-content{
padding-bottom:0;
margin: 0;
}
.md-skin .ibox{
background:#fff;
}
 

</style>	
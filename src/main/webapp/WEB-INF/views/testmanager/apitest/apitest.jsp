<%@ page contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="md-skin">
		<script src="<%=request.getContextPath()%>/resources/js/JSONedtr.js"></script>
		<link href="<%=request.getContextPath()%>/resources/css/JSONedtr.css" rel="stylesheet">
		<%-- <link href="<%=request.getContextPath()%>/resources/css/JSONedtr-dark.css" rel="stylesheet"> --%>
		
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-11">
			<h2>
				<span class="lgMenuTextAlignment" style="width: 90% !important;"
					data-original-title="${Model.scenarioDetails[0].scenario_name}"
					data-toggle="tooltip">
					<ol class="breadcrumb"> 
						<li><strong>${UserCurrentProjectName}</strong></li>
						<c:forEach var="moduleL" items="${Model.moduleList}">
							 <c:if test="${moduleL.module_id == Model.moduleId}">
								<li>${moduleL.module_name}</li>
							 </c:if> 
						</c:forEach>
						<li>${Model.scenarioDetails[0].scenario_name}</li>

					</ol>
				</span>
			</h2>
			<!-- 			<ol class="breadcrumb"> -->
			<!-- 			<li><a href="tmdashboard">Dashboard</a></li>
 -->
			<!-- 				<li class="active"><a href="testcase">Test Cases</a></li> -->
			<!-- 			</ol> -->
		</div>
	</div>
	
	

	<div class="row sub-topmenu">
		<div class="col-md-12">
			<div class="">

				<div class="col-md-6 pd-l-0">
					<ul>
						<li><a href="testcaseApi" id="homeMenu" class=" "><i
								class="fa fa-home"></i> Home</a></li>    
						<c:if test="${Model.scenarioDetails[0].scenario_id >0}">
							<!-- <li><a href="javascript:void(0)" id="addTestCase"><i
									class="fa fa-plus"></i> Add Test Case</a></li>
							<li><a href="javascript:void(0)" id="uploadTestCaseMenu"><i
									class="fa fa-upload"></i> Upload Test Case</a></li>
							<li><a href="javascript:void(0)" id="addParameter"><i
									class="fa fa-share-alt"></i> Add Parameter</a></li> -->
							<li><a href="apitest" id="" class="active"><i
									class="fa fa-plus"></i> Create API Test Case</a></li>
							<!-- <li><a href="javascript:void(0)" id="assert" class="active"><i
									class="fa fa-plus"></i> Assertion</a></li> --> 
						</c:if>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-md-6 pd-3">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Request</h5>
				</div>
				<div class="ibox-content pt-10 pb-10">
					<form id="myform" class="form-inline mt-5">
						<div class="form-group rd-form">
							<label for="urltext">URL:</label> <input type="text" id="urltext" class="form-control"
								placeholder="Enter URL" >
						</div>
						<div class="form-group mt-10">
							<select class="form-control" id="method">
								<option value="GET">GET</option>
								<option value="POST">POST</option>
							</select>
							<button type="button" id="sendbtn" onclick="send()" class="btn btn-primary btn-md ml-5">Submit</button>
						</div>

					</form>
					<div class="mt-10">
						<!-- Nav tabs -->
						<ul id="tabs" class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#all"
								role="tab" data-toggle="tab">Content to Send</a></li>
							<li role="presentation"><a href="#online" role="tab"
								data-toggle="tab" style="display:none">Headers</a></li>   
							<li role="presentation"><a href="#offline" role="tab"
								data-toggle="tab">Parameters</a></li>
						</ul>

						<div class="tab-content mt-20">
							<div role="tabpanel" class="tab-pane fade in active" id="all">
								<form class="form-inline mt-10">
									<div class="form-group rd-form tab-form mt-20">
										<label> Content Type: </label> <select class="form-control"
											id="contentTypeSelect">
											<option value="1">application/json</option>
											<option value="2">application/xml</option>  
										</select>
									</div>
									<div class="form-group rd-form tab-form mt-20">
										<textarea class="form-control" rows="10" id="bodytext"></textarea>
									</div>
								</form>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="online">Headers</div>
							<div role="tabpanel" class="tab-pane fade" id="offline">
								<table class="table rowfy">
								    <thead>
								      <tr>
								        <th>S.No</th>
								        <th>Key</th>
								        <th>Value</th>
								        <th>Add/delete</th>  
								      </tr>
								    </thead>
								    <tbody id="tbody_id">
								      <tr>
								        <td><span class="sn">1</span></td> 
								        <td><input id="key_1" type="text" class="form-control"></td>
								        <td><input id="val_1" type="text" class="form-control"></td>
								      </tr> 
								    </tbody> 
								  </table>
							</div>
							
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 pd-3">
			<div class="ibox float-e-margins mb-0">
				<div class="ibox-title">
					<h5>Response</h5>
					<div>
						<button onclick="openModal()" style="float:right; margin-bottom:0%" class="btn btn-primary btn-md ml-5"  id="assertionId" disabled>Assertion</button>
					</div>
				</div>
				<div class="ibox-content pt-10 pb-10">
					<form class="form-inline">
						<div class="form-group response-grp">
							<label>URL:</label>
							<label class="no-font-wt" id="url"></label> 
						</div>
						<div class="form-group response-grp mt-5">
							<span class="w-100">
							<label>Status: </label> 
								<label class="no-font-wt" id="status"></label> 
							
							</span>
							<label
								class="ml-20">Execution: </label> 
								<label class="no-font-wt" id="exeresult"></label>   
								<!-- <label class="radio-inline ml-5"> 
									<input type="radio"
										name="optradio" checked>Browser
								</label> 
								<label class="radio-inline"> 
									<input type="radio" name="optradio">Text
								</label> -->
						</div>
						<div class="form-group rd-form tab-form mt-5">
							<textarea class="form-control" rows="10" id="responsetext"></textarea>
						</div>
					</form>
				</div>
			</div>
			<div class="ibox float-e-margins">
				<div class="ibox-title custom-ibox-title">
					<h5>Headers</h5>
				</div>
				<div class="ibox-content pt-10 pb-10">
					<form class="form-inline">
						<div class="form-group rd-form tab-form mt-5">
							<label>Content Type</label> <input type="text" id="respContentType"
								class="form-control" readonly>
						</div>
						<div class="form-group rd-form tab-form mt-5">
							<label>Connection</label> <input type="text" id="respConnection" class="form-control"
								readonly>
						</div>
						<div class="form-group rd-form tab-form mt-5">
							<label>Date</label> <input type="text" id="respDate" class="form-control"
								readonly>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>History</h5>
			</div>
			<div class="ibox-content">
				<div class="col-md-10">
					<div class="table-responsive"
						style="overflow-x: visible; table-layout: fixed; width: 100%">
						<table class="table table-striped table-bordered table-hover dataTables-example display select" id="tblTC">
							<thead>
					            <tr>
					             <th class=""><input type="checkbox" name="select_all" value="1" id="selectAll"></th>
					                <th style="display:none">Request ID</th>
					                <th>Test Case Name</th>
					                <th>URL</th>
					                <th>Method</th>
					                <th>Body</th>
					                <th>Parameters</th>
					                <th>Date and Time</th>
					                <th style="display:none">Content Type</th>
					            </tr>
					        </thead>
					        <tbody> 
					        <tr style="display:none"> 
					           		<td><input type="checkbox"></td>
					                <td style="display:none"></td>
					                <td></td>
					                <td></td>
					                <td></td>
					                <td></td>
					                <td></td>
					                <td></td>
					                <td style="display:none"></td>
					            </tr>
					        </tbody>
						</table>
					</div>
				</div>
				<div class="col-md-2">
					<div class="rd-btn-list">
					 <button type="button" class="btn btn-primary" id="saveTcBtn" onclick="saveApiTestCase()" disabled>Save</button>              					</div>
				</div>
			</div>
		</div>

	</div>


</div>


		<!-- <div>
			<input type="textarea" id="txtarea"/>
			<button id="compare" onclick="compare()">Compare</button>
		</div> -->
	<!-- <div class="modal fade" id="assertModal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content animated fadeIn">
				<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
					<div id="output" >
					</div>
				</div>
			</div>
		</div>
	</div> -->
	
	<div class="modal fade" id="assertModal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content animated fadeIn">
				<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
					<div id="">
						<input type="file" id="assertLoadFile">
					</div>
					<!-- <button id="compare" onclick="compare()" disabled>Compare</button> -->
					<button id="validate" onclick="validate()" disabled>Validate</button>
					<div id="output" >
					</div>
					<div id="output1" style="display:none">
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="modal fade" id="addValidationModal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content animated fadeIn">
				<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        	<div>
		        		Key:<label id="key"></label><br>
		        		Value:<label id="val"></label><br>
		        		Level:<label id="level"></label><br>
		        		<input type="checkbox" id="isNullId" value=""> isNull<br>
						<input type="checkbox" id="isEmptyId" value=""> isEmpty<br>
						<input type="checkbox" id="isNotNullId" value=""> isNotNull<br>
						<input type="checkbox" id="isNotEmptyId" value=""> IsNotEmpty<br>
						<input type="checkbox" id="containsStringId" value=""> containsString &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="compareStringId" value=""><br>
		        		<button id="saveValidation">Save</button>
		        	</div>
					
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="validationResultModal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content animated fadeIn">
				<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        	<div id="validationResult">
		        	</div>
		        	<input type="text" placeholder="Enter Test case name" id="testcasename" val="">
		        	<button id="saveAssertion" onclick="saveAssertion()">Save API Test Case</button>
					
				</div>
			</div>
		</div>
	</div>
		
<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
		//$('.dataTables_length').addClass('bs-select');

		var foo = $('.footable').footable();
		foo.trigger('footable_initialize'); //Reinitialize
		foo.trigger('footable_redraw'); //Redraw the table
		foo.trigger('footable_resize'); //Resize the table
		

	});
	var table;
	$(document).ready(function() {
		
		table = $('#tblTC').DataTable({
			select : {
				style : 'multi',
				selector : 'td:first-child',
			},
			order : [],
			columnDefs : [ {
				orderable : false,
				targets : [ 0 ] 
			} ] ,
			bFilter:false 
		}); 
	});
	$(document).ready(function() {
		$('body').on('click', '#selectAll', function() {
			if ($(this).hasClass('allChecked')) {
				$('input[type="checkbox"]', '#tblTC').prop('checked', false);
			} else {
				$('input[type="checkbox"]', '#tblTC').prop('checked', true);
			}
			$(this).toggleClass('allChecked');
		})
		$(document).on("blur","input.jse--value",function(){
			//alert($(this).data('key')+" : "+$(this).val());
		});
		
		$("#compareStringId").on("keyup",function(){
			if($("#compareStringId").val() != null && $("#compareStringId").val() != ''){
			 $('#containsStringId').prop('checked', true);
			}else{
				$('#containsStringId').prop('checked', false);
			}
		});
		
	});	
</script>


<script>
/**
 * Load file for content check assertion
 */
 


</script>

<script>

function validationUrl(urltext){
	//var regexp = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.​\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[​6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1​,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00​a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u​00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;

	var regexp=/^(ftp|http|https):\/\/[^ "]+$/; 
	//regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    if (regexp.test(urltext))
    {  
      return true;
    }
    else
    {
      return false;
    }
}
var requestCounter=1;
function send(){
	var urltext=$("#urltext").val().trim();
	if(validationUrl(urltext)){ 
	$("#sendbtn").attr("disabled",true);
	var method=$("#method option:selected").val();
	var bodytext=$("#bodytext").val();
	var contentTypeId=$("#contentTypeSelect option:selected").val();
	
	/** Creating Json for Parameter**/
	jsonObj=[];
	var counter=1;
	//$("input[class=param]").each(function(){
	$("#tbody_id tr").each(function(){
		var keys=$("#key_"+counter).val();
		var vals=$("#val_"+counter).val();
		parameter={};
		if(keys!="" || vals!=""){
			parameter["key"]=keys;
			parameter["value"]=vals;
			jsonObj.push(parameter);
			counter++; 
		} 
	});
	//console.log(jsonObj); 
	var paramstr=JSON.stringify(jsonObj);
	var dt=new Date;
	
	
	
	/** Posting all data to controller**/
	$.ajax(
			{
				type:"POST",
				url: "testapi",
				//contentType: "application/json",
				data:{urltext:urltext.trim(), method1:method, bodytext:bodytext, paramstr:paramstr.trim(),contentTypeId:contentTypeId},
				success: function(result,status,xhr){
					if(typeof result["urlText"] === "undefined"){
						toastr.options = {
								 "closeButton": true,
								  "debug": true,
								  "progressBar": true,
								  "preventDuplicates": true,
								  "positionClass": "toast-top-right",
								  "showDuration": "400",
								  "hideDuration": "1000",
								  "timeOut": "1500",
								  "extendedTimeOut": "1000",
								  "showEasing": "swing",
								  "hideEasing": "linear",
								  "showMethod": "slideDown",
								  "hideMethod": "slideUp"
					     };
					     toastr.error('',"Invalid URL");
					     $("#sendbtn").attr("disabled",false);
					}else{
						
						$("#assertionId").attr("disabled",false);
						$("#sendbtn").attr("disabled",false);
		    			$("#responsetext").val(result["responseData"]);
		    			$("#url").text(result["method"]+" on "+result["urlText"]);   
		    			$("#status").text(result["statusCode"]+" "+result["statusMsg"]);
		    			if(result["statusCode"]=="200"){
		    				$("#exeresult").text("Pass");
		    			}else{
		    				$("#exeresult").text("Fail");
		    			} 
		    			$("#respContentType").val(result["Content-Type"]);
		    			$("#respConnection").val(result["Connection"]); 
		    			$("#respDate").val(result["Date"]);
		    			$('#tblTC tbody').prepend('<tr><td><input type="checkbox" id="tccheck_'+requestCounter+'" name="tcase[]"></td>'+
		    			'<td style="display:none"><label class="no-font-wt" id="request_id_'+requestCounter+'">'+requestCounter+'</label></td>'+
				        '<td><input type="text" placeholder="Enter Test Case Title" onkeyup="tCaseName('+requestCounter+')" class="form-control" id="tcname_'+requestCounter+'"></td>'+
		    			'<td><label class="no-font-wt" id="url_'+requestCounter+'">'+urltext.trim()+'</label></td>'+
		    			'<td><label class="no-font-wt" id="method_'+requestCounter+'">'+method+'</label></td>'+
		    			'<td><label class="no-font-wt" id="body_'+requestCounter+'">'+bodytext+'</label></td>'+
		    			'<td><label class="no-font-wt" id="parameter_'+requestCounter+'">'+paramstr.trim()+'</label></td>'+ 
		    			'<td><label class="no-font-wt" id="datetime_'+requestCounter+'">'+dt.toLocaleString()+'</label></td>'+
		    			'<td style="display:none"><label class="no-font-wt" id="contentId_'+requestCounter+'">'+contentTypeId+'</label></td></tr>');      
		    			
		    			requestCounter++;
					}
		  		}  
				
			}    
	);
	
	}else{
		toastr.options = {
				 "closeButton": true,
				  "debug": true,
				  "progressBar": true,
				  "preventDuplicates": true,
				  "positionClass": "toast-top-right",
				  "showDuration": "400",
				  "hideDuration": "1000",
				  "timeOut": "1500",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "slideDown",
				  "hideMethod": "slideUp"
	     };
	     toastr.error('',"Invalid URL");
	     $("#sendbtn").attr("disabled",false);
	}
	
}
	
function saveApiTestCase(){

		 var values = new Array();
		       $.each($("input[name='tcase[]']:checked"), function() {
		           var data = $(this).parents('tr:eq(0)');
		           values.push({ 'tcname':$(data).find("td:eq(2) input[type='text']").val().trim().replace(/\s/g,"_"),
		        	   			 'urltext':$(data).find('td:eq(3)').text(),
		        	   			 'method':$(data).find('td:eq(4)').text(),
		        	   			 'bodytext':$(data).find('td:eq(5)').text(),
		        	   			 'parameter':$(data).find('td:eq(6)').text(),
		        	   			'contentTypeId':$(data).find('td:eq(8)').text()
		        	   			 });             
		       }); 
	
	       var tcdata=JSON.stringify(values);
		       $.ajax(
		   			{
		   				type:"POST",
		   				url: "saveapitestcases",
		   				data:{tcdata : tcdata},
		   				success:function(){
		   					if("tcdata"=="tcdata")
				           	 {
				           		 swal({
				           		        title: "Success",
				           		        text: "Test Cases Added Successfully",
				           		        type: "success",
				           		        showCancelButton: false,
				           		        confirmButtonColor: "#DD6B55",
				           		        confirmButtonText: "Ok",
				           		        closeOnConfirm: false
				           		    }, function () {
				           		    	
				           		        window.location.href="testcase";
				           		    });
				           	 }else{
				           		swal({
			           		        title: "Failed",
			           		        text: "Execution Error!",
			           		        type: "error",
			           		        showCancelButton: false,
			           		        confirmButtonColor: "#DD6B55",
			           		        confirmButtonText: "Ok",
			           		        closeOnConfirm: false
			           		    });
				           	 }
		   						
		   					}
		   				
		   			});
}
</script>


<style>
.ibox-content {
	overflow: hidden;
}

.ibox {
	margin-bottom: 10px;
}

.lgMenuTextAlignment {
	overflow: visible !important;
}

.breadcrumb {
	background: transparent !important;
	height: 42px;
}

.md-skin .page-heading {
	height: 50px;
}

.breadcrumb>li {
	padding-top: 5px;
}

.wrapper-content {
	padding: 10px 10px 40px;
}

.rd-btn-list button{
	width:100px;
	margin-left:60px;
}
</style>

<script>
$(document).on('click', '.rowfy-addrow', function(){
  var size=jQuery('#tbody_id>tr').length;
  var counter=size+1;
  $('#tbody_id').append('<tr><td><span class="sn">'+counter+'</span></td>'+
  '<td><input type="text" class="form-control txt1" id="key_'+counter+'"</td>'+
  '<td><input type="text" class="form-control txt2" id="val_'+counter+'"</td>'+
  '<td><button type="button" class="btn btn-sm rowfy-addrow btn-success">+</button></td></tr>');
  $(this).removeClass('rowfy-addrow btn-success').addClass('rowfy-deleterow btn-danger').text('-');
});

 

/*Delete row event*/
$(document).on('click', '.rowfy-deleterow', function(){
	$(this).closest('tr').remove();
	 $('#tbody_id tr').each(function(index) {
		 var idx = index+1;
         $(this).find('span.sn').html(idx);
         $(this).find('input.txt1').prop('id','key_'+idx);
         $(this).find('input.txt2').prop('id','val_'+idx);
         
     });
	
});

 

/*Initialize all rowfy tables*/
$('.rowfy').each(function(){
  $('tbody', this).find('tr').each(function(){
    $(this).append('<td><button type="button" class="btn btn-sm '
      + ($(this).is(":last-child") ?
        'rowfy-addrow btn-success">+' :
        'rowfy-deleterow btn-danger">-') 
      +'</button></td>');
  });
});


</script>

<script>
function getScenarios(moduleId,moduleName){
    sessionStorage.setItem('selectedTestModuleId',moduleId);
    sessionStorage.setItem('selectedTestModuleName',moduleName);
     $(".moduleLinks").find('ul').remove();
       $(".moduleLinks").append("<ul class='nav nav-second-level collapse-in'></ul>");
       scenarioList=${Model.scenarioList};
       for(var i=0;i<scenarioList.length;i++)
           {
           if(moduleId==scenarioList[i].module_id){
           var tbody = "<tr style='cursor: pointer'><td style='display: none'>"+scenarioList[i].scenario_id+"</td><td class='textAlignment' data-original-title='"+scenarioList[i].scenario_name+"' data-container='body' data-toggle='tooltip' data-placement='right' class='issue-info'>"+scenarioList[i].scenario_name+"</td><td class='hidden'>"+scenarioList[i].scenario_description+"</td><td class='hidden'>"+"https://jadeglobaldemo.atlassian.net/browse/"+scenarioList[i].jira_story_id+"</td><td class='hidden'>"+scenarioList[i].jira_story_id+" "+scenarioList[i].jira_story_name+"</td><td class='text-right'>"+
            "<div class='btn-group'><button class='btn-white btn btn-xs' onclick='viewScenario("+scenarioList[i].scenario_id+",\"View\")' name='scenario_"+scenarioList[i].scenario_id+"'>View</button>"+
            "<button class='btn-white btn btn-xs' onclick='removeScenario("+scenarioList[i].scenario_id+")' name='scenario_"+scenarioList[i].scenario_id+"'>Delete</button>"+
           "<button class='btn-white btn btn-xs' onclick='viewScenario("+scenarioList[i].scenario_id+",\"Edit\")' name='scenario_"+scenarioList[i].scenario_id+"'>Edit</button> </div></td></tr>";
           $("#scenarioBody").append(tbody);
           scenarioMenu = "<li id='"+scenarioList[i].scenario_id+"' class='scenarioLinks'>"+
           "<a class='lgMenuTextAlignment menuAnchor menuTooltip' href='#' data-original-title='"+scenarioList[i].scenario_name+"' data-container='body' data-toggle='tooltip' data-placement='right' onclick='setTestCaseList("+scenarioList[i].scenario_id+",\""+scenarioList[i].scenario_name+"\","+moduleId+",\""+moduleName+"\")'>"+scenarioList[i].scenario_name+"</a></li>";
             $("#module_"+moduleId).find('ul').append(scenarioMenu);
             $("#module_"+moduleId).addClass("active"); 
            
             $('[data-toggle="tooltip"]').tooltip();
               //tooltip
               if(!$('.menuSpan').hasClass('menuTextAlignment')) {
                   //turn on the tooltips
                   $('.menuTooltip').tooltip('disable');
               }
           }
           }
}
</script>

<script>
	function tCaseName(counter){
		if($('#tcname_'+counter).val() != ""){
		$('#tccheck_'+counter).prop("checked",true);
		$('#saveTcBtn').prop("disabled",false);
		 }else{
		$('#tccheck_'+counter).prop("checked",false);
		$('#saveTcBtn').prop("disabled",true); 
		}
	} 
</script>
<script>
var modifiedData;
var validationArray = new Array();
function getDataOnChange(data){
		//console.log('DATAWASCHANGED',data.getData());
		modifiedData=data.getData();
}
	
	
/**
 * Load File into plugin
 */
$("#assertLoadFile").change(JSONedtr,function(){
	 if(this.files[0] !=null || this.files[0] != undefined){
		 validationArray=[];
		 $("#key").text("");
		 $("#val").text("");
		 $("#level").text("");
		 $("#isNullId").attr("checked",false);
		 $("#isEmptyId").attr("checked",false);
		 $("#isNotNullId").attr("checked",false);
		 $("#isNotEmptyId").attr("checked",false);
		 $("#containsStringId").attr("checked",false);
		 $("#compareStringId").val("");
		
		 var file=this.files[0];
		 //console.log(file);
		 var reader = new FileReader();
		    reader.readAsText(file, "UTF-8");
		    reader.onload = function (evt) {
		        var data=evt.target.result;
		    	new JSONedtr(data,'#output',{
		    	    runFunctionOnUpdate:'getDataOnChange',
		    	    instantChange:true 
		    	});
		    	$("#validate").attr("disabled",false);
		    }
		 
	 }
});

/**
 * Content Comparision
 */
function compare(){
	
	var md=JSON.stringify(modifiedData);
	//var md=modifiedData;
	//console.log(md); 
			
	var od=JSON.parse($("#responsetext").val());
	var ood=JSON.stringify(JSON.parse(JSON.stringify(od, (k, v) => v && typeof v === 'object' ? v : '' + v)));
	//console.log(ood); 
			
	if(md == ood){
		alert("OK");
	}else{
		alert("NOT MATCHED");
	}
}


function openModal(){
	$("#assertModal").modal('show');
}


/**
 * Get Validation Modal with details
 */
var currentKey,currentVal,currentLevel,currentRow,currentCompareString;
var indexArray= new Array();
$(document).on("click",".addValidation",function(){
	$("#key").text($(this).data('key'));
	currentKey=$(this).data('key');
	
	$("#val").text($(this).data('value'));
	currentVal=$(this).data('value');
	
	$("#level").text($(this).data('level'));
	currentLevel=$(this).data('level');
	
	currentRow=$(this).data('index');
	indexArray.push(currentRow);
	
	 $("#isNullId").attr("checked",false);
	 $("#isEmptyId").attr("checked",false);
	 $("#isNotNullId").attr("checked",false);
	 $("#isNotEmptyId").attr("checked",false);
	 $("#containsStringId").attr("checked",false);
	 $("#compareStringId").val("");
	 
	$("#addValidationModal").modal('show');
});


/**
 * Saving Validation Details
 */
 
$("#saveValidation").on("click",function(){
	
	var isNullFlag="false",isEmptyFlag="false",isNotNullFlag="false",isNotEmptyFlag="false",currentCompareString;
	if($("#isNullId").is(":checked")){
		isNullFlag="true";
	}
	if($("#isEmptyId").is(":checked")){
		isEmptyFlag="true";
	}
	if($("#isNotNullId").is(":checked")){
		isNotNullFlag="true";
	}
	if($("#isNotEmptyId").is(":checked")){ 
		isNotEmptyFlag="true";
	}
	if($("#containsStringId").is(":checked")){
		currentCompareString=$("#compareStringId").val();
	}
	validationArray.push({key:currentKey, value:currentVal, level:currentLevel, row:currentRow, isNull:isNullFlag, isEmpty:isEmptyFlag, isNotNull:isNotNullFlag, isNotEmpty:isNotEmptyFlag, containsString:currentCompareString});
	//console.log(validationArray);
	
	$("#added_"+currentRow).text("Added");
	$("#addValidation_"+currentRow).attr("disabled",true);
	$("#addValidationModal").modal('hide');
	
});


/**
 * Remove Validation Details
 */
$(document).on("click","#removeValidation",function(){

	currentRow=$(this).data('index');
	var deleteValidationIndex=indexArray.indexOf(currentRow);
	validationArray.splice(deleteValidationIndex,1);
	$("#added_"+currentRow).text("");
	$("#addValidation_"+currentRow).attr("disabled",false);
	//console.log(validationArray);
});

function validate(){
	//console.log(JSON.stringify(validationArray));
	if(JSON.stringify(validationArray)=="[]"){
		alert("Please Add Assertion");
	}else{
	var dtt=$("#responsetext").val();
	/* var originalResponse=JSON.parse($("#responsetext").val());
	var responseJson=JSON.stringify(JSON.parse(JSON.stringify(originalResponse, (k, v) => v && typeof v === 'object' ? v : '' + v))); */
	var original=new JSONedtr(dtt,'#output1' );
	var responseJson=original.getDataString();
	//console.log(responseJson);
	 var res= $.ajax(
   			{
   				type:"POST",
   				url: "performValidation",
   				data:{responseJson : responseJson, validationArray:JSON.stringify(validationArray)},
   				success:function(data,status,xhr){
   					var resp=JSON.parse(data);
   					//console.log(resp[0].isNullResult);
   					$("#validationResult").empty(); 
   					for(var i=0;i<resp.length;i++){
   						
   						Object.keys(resp[i]).forEach(function(keyy) {
   							$("#validationResult").append(keyy+' : <label>'+resp[i][keyy]+'</label><br>');
   						});
   						$("#validationResult").append('----------------------------------------------------------------------<br>');
   						
   						
   					}
   					$("#validationResultModal").modal('show');
   				}
   			});	 
	
		
	}
}

function saveAssertion(){
		
		var values = new Array();
		var testcasename=$("#testcasename").val().trim().replace(/\s/g,"_");
    	
         var data = $('#tblTC tr:first-child').find('td:eq(1)');
        values.push({ 'tcname':testcasename,
     	   			 'urltext':$('#tblTC tr:first-child').find('td:eq(3)').text(),
     	   			 'method':$('#tblTC tr:first-child').find('td:eq(4)').text(),
     	   			 'bodytext':$('#tblTC tr:first-child').find('td:eq(5)').text(),
     	   			 'parameter':$('#tblTC tr:first-child').find('td:eq(6)').text(),
     	   			'contentTypeId':$('#tblTC tr:first-child').find('td:eq(8)').text()
     	   			 });        
   // }); 

var tcdata=JSON.stringify(values)
//console.log(tcdata);
var res= $.ajax(
	   			{
	   				type:"POST",
	   				url: "saveapitestcasesWithAssertion",
	   				data:{tcdata : tcdata, validationArray:JSON.stringify(validationArray)},
	   				success:function(data,status,xhr){
	   					if(data=="Success")
			           	 {
			           		 swal({
			           		        title: "Success",
			           		        text: "Test Cases Added Successfully",
			           		        type: "success",
			           		        showCancelButton: false,
			           		        confirmButtonColor: "#DD6B55",
			           		        confirmButtonText: "Ok",
			           		        closeOnConfirm: false
			           		    }, function () {
			           		    	
			           		        window.location.href="testcase";
			           		    });
			           	 }else{
			           		swal({
		           		        title: "Failed",
		           		        text: "Execution Error!",
		           		        type: "error",
		           		        showCancelButton: false,
		           		        confirmButtonColor: "#DD6B55",
		           		        confirmButtonText: "Ok",
		           		        closeOnConfirm: false
		           		    });
			           	 }
	   				}
	   			});
}

</script>

<style>
.jse--output .jse--delete{
	display:none !important;
}
.jse--output .jse--row.jse--add{
	display:none !important;
}
#assertModal .modal-dialog{
width:98%;
margin:10px;
overflow-x:auto;
}
#assertModal #output{
max-width:100% !important;
width:100% !important;
margin:0;
}
#assertModal .modal-body button{
margin-top: -19px;
margin-right: -19px;
}

#assertModal .modal-body button {
    margin-top: -19px;
    margin-right: 4px;
}
</style>

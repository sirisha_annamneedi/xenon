<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Application - ${Model.projectDetails[0].project_name}</h2>
		<ol class="breadcrumb">
<!-- 			<li><a href="tmdashboard">Dashboard</a></li>
 -->			<li><strong>Test Case Management</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row" id="projectDetailsDiv">
		<div class="ibox m-b-sm">
			<div class="ibox collapsed">
				<div class="ibox-title">
					<div class="ibox-tools">
						<a class="collapse-link"> <i
							class="fa fa-chevron-right pull-left"></i>
							<h5>${Model.projectDetails[0].project_name}</h5>
						</a>
					</div>
				</div>
				<div class="ibox-content">
					<c:if test="${Model.createproject == 1}">
						<button class="btn btn-success btn-xs pull-right" type="button"
							id="addProject">Add Application</button>
					</c:if>
					<form action="#" id="form"
						class="wizard-big wizard clearfix form-horizontal">
						<div class="content clearfix">
							<fieldset class="body current" disabled>
								<div class="row">
									<div class="col-lg-10">
										<div class="form-group">
											<label class="control-label col-sm-2">Name:</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Build Name"
													class="form-control characters"
													value="${Model.projectDetails[0].project_name}"
													name="buildName">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label"> Description:</label>
											<div class="col-sm-10 readOnlyDiv">
												<%-- <textarea name="buildDescription" class="form-control characters"
													rows="10" style="resize: none; border: 1px solid #ccc;"> ${Model.projectDetails[0].project_description}</textarea> --%>
												<div class="readOnlySummernote">${Model.projectDetails[0].project_description}</div>
											</div>
										</div>
									</div>
								</div>


							</fieldset>
						</div>

					</form>
					<!-- end form -->
				</div>
			</div>


		</div>
	</div>
	<div class="row hidden" id="moduleDetailsDiv">
		<div class="ibox m-b-sm">
			<div class="ibox collapsed">
				<div class="ibox-title">
					<div class="ibox-tools">
						<a class="collapse-link"> <i
							class="fa fa-chevron-right pull-left"></i>
							<h5 id="moduleTitle"></h5>
						</a>
					</div>
				</div>
				<div class="ibox-content">
<%-- 					<c:if test="${Model.createmodule == 1}"> --%>
						<button class="btn btn-success btn-xs pull-right addModule" type="button" onClick="setmoduleAppId(${Model.projectDetails[0].project_id})"
							id="addModule">Add Module</button>
<%-- 					</c:if> --%>
					<form action="#" id="form"
						class="wizard-big wizard clearfix form-horizontal">
						<div class="content clearfix">
							<fieldset class="body current" disabled>
								<div class="row">
									<div class="col-lg-10">
										<div class="form-group">
											<label class="control-label col-sm-2">Name:</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Module Name"
													class="form-control characters"
													value=""
													id="moduleDetailsName">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label"> Description:</label>
											<div class="col-sm-10 readOnlyDiv">
												<!-- <textarea class="form-control" style="max-width: 100%;"></textarea> -->
												<!-- <textarea name="moduleDescription" rows="10"
													class="form-control characters"
													style="resize: none; border: 1px solid #ccc;" id="moduleDetailsDescription"></textarea> -->
												<div class="readOnlySummernote" id="moduleDetailsDescription"></div>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</div>
					</form>
					<!-- end form -->
				</div>
			</div>

		</div>
	</div>
	
	<div class="row hidden" id="createScenarioDiv">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form action="insertscenario"
						class="wizard-big wizard clearfix form-horizontal" method="POST"
						id="createScenarioForm">
						<div class="content clearfix">
							<fieldset class="body current">
								<div class="row">
									<label class="col-lg-4 pull-right text-right">* fields are mandatory</label>
								</div>
								<div class="row">
									<div class="col-lg-10">
										<div class="form-group hidden">
											<div class="col-sm-10">
												<input type="text" class="form-control"
													value="" name="moduleId"
													id="activeModuleId">
											</div>
										</div>
										<div class="form-group hidden">
											<label class="control-label col-sm-2">Name:</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Module Name"
													class="form-control characters"
													value=""
													name="moduleName" id="activeModuleName">
											</div>
										</div>
										<c:if test="${Model.scenarioError == 1}">
											<div class="alert alert-danger">Scenario name already
												exists, Please try another.</div>
										</c:if>
										<div class="form-group">
											<label class="col-sm-2 control-label">Name *:</label>
											<div class="col-sm-10">
												<input type="text" class="form-control characters"
													placeholder="Scenario Name" name="scenarioName" id="addScenarioName"
													tabindex="1">
											</div>
										</div>
										<c:if test="${Model.jiraStatus == 'y'}">
											<div class="form-group" id="jiraStoryToggle">
												<label class="col-sm-2 control-label">Jira Story:</label>
												<div class="col-sm-10">
												<select class="chosen-select" style="width: 450px;height:30px;" name="jiraStory">
													<option value="default"></option>
													<c:forEach var="jiraStory" items="${Model.jiraId}" varStatus="status">
														<option value="${jiraStory}">${jiraStory}</option>
													</c:forEach>
												</select>
												</div>
											</div>
										<div class="form-group">
										<label class="col-sm-2 control-label"></label>
											<div class="col-sm-10">
												<button class="btn btn-success" type="button"
						                            id="jiraStoryAltText" style="margin : 5px">Add Jira Story</button>
						                             <font size="2"> *Click here to add Jira Story Manually</font>
											</div>
										</div>
										<div class="form-group" id="jiraStoryAlt">
											<label class="col-sm-2 control-label">Jira Story:</label>
											<div class="col-sm-10">
												<input type="text" class="form-control characters"
													placeholder="Jira Story" name="jiraStoryAlt">
											</div>
										</div>
										</c:if>
										<div class="form-group">
											<label class="col-sm-2 control-label">Description:</label>
											<div class="col-sm-10">
												<textarea placeholder="Scenario Description" rows="6"
													class="form-control maxWidth100 characters summernote"
													style="border: 1px solid #ccc;" name="scenarioDescription"
													tabindex="2"></textarea>
											</div>
										</div>
										<input type="text" class="hidden" value="1" name="status">
									</div>
								</div>
								<div class="hr-line-solid"></div>
							</fieldset>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-white pull-left"
										style="margin-right: 15px;" type="button" id="btnCancel"
										type="button" tabindex="3">Cancel</button>
									<button class="btn btn-success pull-left ladda-button"
										tabindex="4" data-style="slide-up" id="insertScenarioBtn">Submit</button>
								</div>
							</div>
						</div>
					</form>
					<!-- end form -->
				</div>
				<!-- end ibox-content -->

			</div>
			<!-- end ibox -->
		</div>
		<!-- end col -->
	</div>

	<div class="row hidden" id="uploadScenarioDiv">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form action="bulkinsertscenarios"
						class="wizard-big wizard clearfix form-horizontal" method="POST"
						id="uploadScenarioForm" enctype="multipart/form-data">
						<div class="content clearfix">
							<fieldset class="body current">
								<div class="row">
									<div class="col-lg-12">
													<div class="form-group hidden">
										<div class="col-sm-8">
											<input type="text" class="form-control"
													value="" name="moduleId"
													id="activeModuleIds">
										</div>
									</div>
									<div class="form-group hidden">
										<div class="col-sm-8">
											<input type="text" placeholder="Module Name"
													class="form-control characters"
													value=""
													name="moduleName" id="activeModuleNames">
										</div>
									</div>

									<div class="col-lg-12">
										
										<div class="form-group" id="datasheetStatusDiv">
											<label class="col-sm-2 control-label">Datasheet : </label>
											<div class="col-sm-10">

												<div class="col-sm-6" id="uploadDatasheetDiv">
													<input type="file" class="filestyle" name="uploadDatasheet"
														id="uploadDatasheetBtn" data-buttonName="btn-primary" required="true">
												</div>

											</div>
										</div>
									</div>
									<input type="text" class="hidden" value="1" name="status">
									</div>
								</div>
								<div class="hr-line-solid"></div>
							</fieldset>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-white pull-left"
										style="margin-right: 15px;" type="button" id="btnnCancel"
										type="button" tabindex="3">Cancel</button>
									<button class="btn btn-success pull-left ladda-button"
										tabindex="4" data-style="slide-up" id="uploadScenarioBtn">Submit</button>
								</div>
							</div>
						</div>
					</form>
					<!-- end form -->
				</div>
				<!-- end ibox-content -->

			</div>
			<!-- end ibox -->
		</div>
		<!-- end col -->
	</div>
	<div class="row hidden" id="editScenarioDiv">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form action="updatescenario"
						class="wizard-big wizard clearfix form-horizontal" method="POST"
						id="editScenarioForm">
						<div class="content clearfix">
								<div class="row">
									<div class="col-lg-10">
									<fieldset class="body current" id="fieldEditScenario">
										<div class="form-group hidden">
											<div class="col-sm-10">
												<input type="text" class="form-control" name="scenarioId" 
													id="scenarioId">
											</div>
										</div>
										<div class="form-group hidden">
											<label class="control-label col-sm-2">module Name:</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Module Name"
													class="form-control characters"
													 id="EditScenModuleName"
													name="moduleName">
											</div>
										</div>
										<div class="form-group hidden">
											<div class="col-sm-10">
												<input type="text"
													class="form-control characters"
													 id="EditScenModuleId"
													name="moduleId">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Name:*</label>
											<div class="col-sm-10">
												<input type="text" class="form-control characters"
													placeholder="Scenario Name" name="scenarioName"
													id="scenarioName">
											</div>
										</div>
										</fieldset>
										<c:if test="${Model.jiraStatus == 'y'}">
										<div class="form-group" id="jiraStoryLink">
											<label class="col-sm-2 control-label">Jira Story Link</label>
											<div class="col-sm-10">
													<a href="" id="jiraStoryId" target="_blank">
													<input type="text" class="form-control characters"
													readonly="true" style="cursor:pointer"
													id="jiraStoryLinkId">
													</a>
											</div>
										</div>
										<div class="form-group" id="jiraStory">
											<label class="col-sm-2 control-label">Jira Story:</label>
											<div class="col-sm-10">
												<input type="text" class="form-control characters" name="jiraStory"
													id="jiraIdStory">
											</div>
										</div>
										</c:if>
										<fieldset class="body current" id="fieldEditScenarioDescription">
										<div class="form-group">
											<label class="col-sm-2 control-label">Description:</label>
											<div class="col-sm-10">
												<textarea 
													placeholder="Scenario Description" rows="6"
													class="form-control maxWidth100" style=" border: 1px solid #ccc;"
													name="scenarioDescription" id="scenarioDescription"> </textarea>
											</div>
										</div>
										</fieldset>
									</div>
								</div>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-success pull-left" type="button"
										id="btnEdit">Edit</button>
									<button class="btn btn-white pull-left"
										style="margin-right: 15px;" type="button" id="cancelUpdate"
										type="button">Cancel</button>
									<button
										class="btn btn-success pull-left ladda-button-demo ladda-button"
										id="btnSubmit" data-style="slide-up">Update</button>
								</div>
							</div>
						</div>
					</form>
					<!-- end form -->
				</div>
				<!-- end ibox-content -->

			</div>
			<!-- end ibox -->
		</div>
		<!-- end col -->
	</div>
	<div class="row" id="moduleListDiv">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Modules</h5>
<%-- 					<c:if test="${Model.createmodule == 1}"> --%>
						<button class="btn btn-success btn-xs pull-right addModule" onClick="setmoduleAppId(${Model.projectDetails[0].project_id})" type="button"
							id="addModule">Add Module</button>
<%-- 					</c:if> --%>
				</div>

				<div class="ibox-content">

					<div class="table-responsive">
						<table
							class="table table-striped table-bordered table-hover dataTables-example"
							id="tblModuletList">
							<thead>
								<tr>
									<th style="display: none">Module Id</th>
									<th>Name</th>
									<th class="hidden">Description</th>
<%-- 									<c:if test="${Model.editmodule == 1}"> --%>
										<th class="text-right" data-sort-ignore="true">Action</th>
<%-- 									</c:if> --%>

								</tr>
							</thead>
							<tbody>
								<c:forEach var="module" items="${Model.moduleList}">
									<tr style="cursor: pointer" onclick="location.href='#'">
										<td style="display: none">${module.module_id}</td>
										<td class="textAlignment" data-original-title="${module.module_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${module.module_name}</td>
										<td class="hidden">${module.module_description}</td>
<%-- 										<c:if test="${Model.editmodule == 1}"> --%>
											<td class="text-right">
												<div class="btn-group">
													<button class="btn-white btn btn-xs" onclick="settingModule(${module.module_id},${Model.projectDetails[0].project_id},'${module.module_name}')"
														>Setting</button>
													
												</div>
											</td>
<%-- 										</c:if> --%>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>
	</div>
	
	<div class="row hidden" id="scenarioListDiv">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Scenarios</h5>
					<button class="btn btn-success btn-xs pull-right " type="button"
						id="uploadScenario" style="margin : 5px">Upload Scenarios</button>
						<button class="btn btn-success btn-xs pull-right " type="button"
						id="addScenario" style="margin : 5px">Add Scenario</button>
				</div>

				<div class="ibox-content">

					<div class="table-responsive" style="overflow-x: visible">
						<table
							class="table table-striped table-bordered table-hover dataTables-example"
							id="tblScenarioList">
							<thead>
								<tr>
									<th style="display: none">Scenario Id</th>
									<th>Name</th>
									<th class="hidden">Description</th>
									<th class="text-right" data-sort-ignore="true">Action</th>

								</tr>
							</thead>
							<tbody id="scenarioBody">
							
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>
	</div>

	<!-- end row -->
</div>
</div>
<input type="hidden" id="toasterScenarioStatus"
	value=<%=session.getAttribute("insertedScenarioStatus")%>>

<input type="hidden" id="toasterScenarioName"
	value='<%=session.getAttribute("insertedScenarioName")%>'>

<input type="hidden" id="toasterJiraId"
	value='<%=session.getAttribute("insertedJiraId")%>'>
<!-- end wrapper -->

<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});
</script>

<script>
	$(function() {
		
		   
		   
			$(".chosen-select").chosen();
			$("ul.chosen-results").css('max-height','250px'); 
		    /* $('.note-editor').css('pointer-events','none');
		    $('button[ data-event="codeview"]').remove();
		    $('.note-editor').css('background-color','#eeeeee');
		    $('div.note-insert').remove();
		    $('div.note-table').remove();
		    $('div.note-help').remove();
		    $('div.note-style').remove();
		    $('div.note-color').remove();
		    $('button[ data-event="removeFormat"]').remove();
		    $('button[ data-event="insertUnorderedList"]').remove();
		    $('button[ data-event="fullscreen"]').remove();
		    $('button[ data-original-title="Line Height"]').remove();
		    $('button[ data-original-title="Font Family"]').remove();
		    $('button[ data-original-title="Paragraph"]').remove(); */
		    
		moduleId = sessionStorage.getItem('currentModuleId');
	 	
	 	 $('#tblModuletList tbody tr').each(function() {
			 if($(this).children('td').first().html()== moduleId)
				 {
				 $(this).children('td').first().trigger('click');
				 }
	 	 });
		
		var table = $('#tblModuletList').DataTable({
				"aoColumnDefs" : [ {
					'bSortable' : false,
					'aTargets' : [ 1 ]
				} ]});
		
			  $(".md-editor").addClass("active");
			  
			  var scenarioError=${Model.scenarioError};
			  if(scenarioError==1)
				  {
						   $("#createScenarioDiv").removeClass('hidden');
					   
					  		 $(this).addClass('hidden');
							$("#addScenarioName").focus();
					  
				  }
			  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
			  var insertedStatus = $('#toasterScenarioStatus').val();
				var insertedScenario = $('#toasterScenarioName').val();
				//var insertedJiraStatus = $('#toasterJiraIdStatus').val();
				var insertedJiraId = $('#toasterJiraId').val();
				if(insertedStatus == 1){
					var message = " scenario is created successfully";
					var toasterMessage = insertedScenario +  message;
					showScenarioToster(toasterMessage);
				}
				
				else if(insertedStatus == 2){
					var message =" scenario is successfully Updated";
					var toasterMessage = insertedScenario +  message;
					showScenarioToster(toasterMessage);
				}
				 else if(insertedStatus == 3){
					var message =" story not present in JIRA";
					var toasterMessage = insertedJiraId +  message;
					showScenarioToster(toasterMessage);
				} 
		 $('#toasterScenarioStatus').val('11');
		
			$('[data-toggle="tooltip"]').tooltip(); 	
	
	
		$('.summernote').summernote();
		$('.note-editor').css('background','white');
		$('button[ data-event="codeview"]').remove();
		$('.note-editor').css('border','1px solid #CBD5DD');
		$('div.note-insert').remove();
		$('div.note-table').remove();
		$('div.note-help').remove();
		$('div.note-style').remove();
		$('div.note-color').remove();
		$('button[ data-event="removeFormat"]').remove();
		$('button[ data-event="insertUnorderedList"]').remove();
		$('button[ data-event="fullscreen"]').remove();
		$('button[ data-original-title="Line Height"]').remove();
		$('button[ data-original-title="Font Family"]').remove();
		$('button[ data-original-title="Paragraph"]').remove();  
		   
		    $('.readOnlySummernote').summernote();
		    $('.readOnlyDiv .note-toolbar').remove();
		    $(".readOnlyDiv .note-editor").attr("style", "background: #eeeeee !important; border : 1px solid #ccc !important ");
		    $('.readOnlyDiv .note-editor').css('pointer-events','none');		    
	});
	  function showScenarioToster(toasterMessage){
			 toastr.options = {
					 "closeButton": true,
					  "debug": true,
					  "progressBar": true,
					  "preventDuplicates": true,
					  "positionClass": "toast-top-right",
					  "showDuration": "400",
					  "hideDuration": "1000",
					  "timeOut": "9000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "slideDown",
					  "hideMethod": "slideUp"
	       };
	       toastr.success('',toasterMessage);
		}
	  
	  function settingModule(id,projectID,moduleName){
		swal({
			title : "Alert!",
			text : "Do you want to navigate to Test Plan Manager?",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "Yes",
			cancelButtonText : "No",
			closeOnConfirm : false,
			closeOnCancel : true
		}, function(isConfirm) {

			if (isConfirm) {
				$('body').addClass("white-bg");
				$("#barInMenu").removeClass("hidden");
				$("#wrapper").addClass("hidden");	
				
				var posting = $.post('setmoduleidforassign',{
					moduleID : id,
					projectID : projectID,
					moduleName: moduleName
				});
				
				posting.done(function(data){
					location.href  = "modulesetting";
				}); 
			
			} 
		});
 	}
	  
 function setmoduleAppId(appId){
	   /* swal({
			title : "Alert!",
			text : "Do you want to navigate to Test Plan Manager?",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "Yes",
			cancelButtonText : "No",
			closeOnConfirm : false,
			closeOnCancel : true
		}, function(isConfirm) {

			if (isConfirm) {
				
				$('body').addClass("white-bg");
				$("#barInMenu").removeClass("hidden");
				$("#wrapper").addClass("hidden");	
				
				var posting = $.post('setmoduleapplicationid',{
					applicationID : appId
				});
				posting.done(function(data){
					location.href  = "createmodule";
				});
			} 
		}); */
		
	 $('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		var posting = $.post('setmoduleapplicationid',{
			applicationID : appId
		});
		posting.done(function(data){
			location.href  = "createmodule";
		});
	   
	};
   
   $("#addProject").click(function(){
		  // 
		   swal({
				title : "Alert!",
				text : "Do you want to navigate to Administration?",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "Yes",
				cancelButtonText : "No",
				closeOnConfirm : false,
				closeOnCancel : true
			}, function(isConfirm) {

				if (isConfirm) {
					$("#spinner").fadeIn("slow");
					window.location.href="createproject";
				
				} 
				/* else
					{
					location.reload(true);
					} */
			});
	});
 $("#tblModuletList tbody tr td:not(.text-right)").click(function() {
	var moduleId =  $(this).parent().children('td').first().html();
	var moduleName =  $(this).parent().children('td:eq(1)').html();
	var moduleDescription =  $(this).parent().children('td:eq(2)').html();
	var scenarioList=[];
	$("#scenarioBody").text("");
	$(".moduleLinks").find('ul').remove();
	$(".moduleLinks").append("<ul class='nav nav-second-level collapse-in'></ul>");
	scenarioList=${Model.scenarioList};
	for(var i=0;i<scenarioList.length;i++)
		{
		if(moduleId==scenarioList[i].module_id){
		var tbody = "<tr style='cursor: pointer'><td style='display: none'>"+scenarioList[i].scenario_id+"</td><td class='textAlignment' data-original-title='"+scenarioList[i].scenario_name+"' data-container='body' data-toggle='tooltip' data-placement='right' class='issue-info'>"+scenarioList[i].scenario_name+"</td><td class='hidden'>"+scenarioList[i].scenario_description+"</td><td class='hidden'>"+"https://jadeglobaldemo.atlassian.net/browse/"+scenarioList[i].jira_story_id+"</td><td class='hidden'>"+scenarioList[i].jira_story_id+" "+scenarioList[i].jira_story_name+"</td><td class='text-right'>"+
 		"<div class='btn-group'><button class='btn-white btn btn-xs' onclick='viewScenario("+scenarioList[i].scenario_id+",\"View\")' name='scenario_"+scenarioList[i].scenario_id+"'>View</button>"+
 		"<button class='btn-white btn btn-xs' onclick='removeScenario("+scenarioList[i].scenario_id+")' name='scenario_"+scenarioList[i].scenario_id+"'>Delete</button>"+
        "<button class='btn-white btn btn-xs' onclick='viewScenario("+scenarioList[i].scenario_id+",\"Edit\")' name='scenario_"+scenarioList[i].scenario_id+"'>Edit</button> </div></td></tr>";
		$("#scenarioBody").append(tbody);
		scenarioMenu = "<li id='"+scenarioList[i].scenario_id+"' class='scenarioLinks'>"+
		                "<a class='lgMenuTextAlignment menuAnchor menuTooltip' href='#' data-original-title='"+scenarioList[i].scenario_name+"' data-container='body' data-toggle='tooltip' data-placement='right' onclick='getTestCaseList("+scenarioList[i].scenario_id+",\""+scenarioList[i].scenario_name+"\","+moduleId+",\""+moduleName+"\")'>"+scenarioList[i].scenario_name+"</a></li>";
		  $("#module_"+moduleId).find('ul').append(scenarioMenu);
		  $("#module_"+moduleId).addClass("active");
		  
		  $('[data-toggle="tooltip"]').tooltip(); 
			//tooltip
			if(!$('.menuSpan').hasClass('menuTextAlignment')) {
				//turn on the tooltips
				$('.menuTooltip').tooltip('disable'); 
			}
		}
		}
	$("#moduleListDiv").addClass("hidden");
	$("#projectDetailsDiv").addClass("hidden");
	$("#scenarioListDiv").removeClass("hidden");
	$("#moduleDetailsName").val(moduleName);
	$("#moduleDetailsDescription").html(moduleDescription);
	$("#moduleTitle").text(moduleName);
	$("#activeModuleId").val(moduleId);
	$("#activeModuleName").val(moduleName);
	$("#activeModuleIds").val(moduleId);
	$("#activeModuleNames").val(moduleName);
	$("#EditScenModuleName").val(moduleName);
	$("#EditScenModuleId").val(moduleId);
	
	$("#moduleDetailsDiv").removeClass("hidden");
	
	 $.fn.dataTable.ext.errMode = 'none';
	var table = $('#tblScenarioList').DataTable({
		"aoColumnDefs" : [ {
			'bSortable' : false,
			'aTargets' : [ 3 ]
		} ]});
	sessionStorage.setItem("currentModuleId", moduleId) 
	});
 
 
 $(document).on("click", "#scenarioBody tr td:not(.text-right,.dataTables_empty)", function(e) {
		var scenarioId =  $(this).parent().children('td').first().html();
	 	var scenarioName =  $(this).parent().children('td:eq(1)').html();
	 	
	 	$('body').addClass("white-bg");
	 	$("#barInMenu").removeClass("hidden");
	 	$("#wrapper").addClass("hidden");	
	 	
	    getTestCaseList(scenarioId,scenarioName,$("#activeModuleId").val(),$("#activeModuleName").val());
	});
 
 $("#addScenario").click(function(){
	 $("#jiraStoryAlt").addClass("hidden");
	 var createNewScenarioStatus=${Model.createNewScenarioStatus};
	   if(createNewScenarioStatus==1)
	   {
		   $("#createScenarioDiv").removeClass('hidden');
	   
	  		 $(this).addClass('hidden');
			$("#addScenarioName").focus();
	   }
	   else
		   window.location.href="trialerror";
});
 
$("#jiraStoryAltText").click(function(){
	$("#jiraStoryToggle").addClass("hidden");	
	 $("#jiraStoryAlt").removeClass("hidden");
});
 
 $("#uploadScenario").click(function(){
	   
	   var createNewScenarioStatus=${Model.createNewScenarioStatus};
	   if(createNewScenarioStatus==1)
	   {
		   $("#uploadScenarioDiv").removeClass('hidden');
	   
	  		 $(this).addClass('hidden');
			$("#addScenarioName").focus();
	   }
	   else
		   window.location.href="trialerror";
	   
	});
 
$('#insertScenarioBtn').click(function() {
	if($('#createScenarioForm').valid()){
	  
	  $('body').addClass("white-bg");
	  $("#barInMenu").removeClass("hidden");
	  $("#wrapper").addClass("hidden");	
	  
	  $('.summernote').each( function() {
		  $(this).val($(this).code());
	  });
	   
	  $('#createScenarioForm').submit();
	}
	});
	
$('#uploadScenarioBtn').click(function() {
	if($('#uploadScenarioForm').valid()){
	  
	  $('body').addClass("white-bg");
	  $("#barInMenu").removeClass("hidden");
	  $("#wrapper").addClass("hidden");	
	  
	  $('.summernote').each( function() {
		  $(this).val($(this).code());
	  });
	   
	  $('#uploadScenarioForm').submit();
	}
	});
	
$('#btnSubmit').click(function() {
	if($('#editScenarioForm').valid()){

	   $('body').addClass("white-bg");
	   $("#barInMenu").removeClass("hidden");
	   $("#wrapper").addClass("hidden");	
	   
	   $('#scenarioDescription').each( function() {
		   $(this).val($(this).code());
	   });
	   
	   $('#editScenarioForm').submit();
	}
	});
	function viewScenario(id,viewType)
 	{
	
		var $row =$("button[name='scenario_"+id+"']").closest('tr');
	     $("#scenarioId").val($row.find("td:nth-child(1)").text());  
	     $("#scenarioName").val($row.find("td:nth-child(2)").text()); 
	     $("#scenarioDescription").html($row.find("td:nth-child(3)").html());
	 	$("#editScenarioDiv").removeClass('hidden');
	 	$("#scenarioName").focus();
	 	document.getElementById("jiraStoryId").href=$row.find("td:nth-child(4)").text();
	 	var jiraIdStory = $row.find("td:nth-child(5)").text();
	 	jiraIdStory = jiraIdStory.replace(new RegExp("null", 'g'),"");
	 	jiraIdStory = jiraIdStory.trim();
	 	$("#jiraIdStory").val(jiraIdStory);
	 	var jiraStoryLink = $row.find("td:nth-child(4)").text();
	 	if(jiraStoryLink.endsWith("null")){
	 		jiraStoryLink = null;
	 		$("#jiraStoryLinkId").val(jiraStoryLink);
	 	}
	 	else{
	 		$("#jiraStoryLinkId").val(jiraStoryLink);
	 	}
	 	
	 	
	 	 if(viewType == "Edit")
	 		 {
	 		 $("#btnEdit").addClass("hidden");
	 		 $("#btnSubmit").removeClass("hidden");
	 		 $("#cancelUpdate").removeClass("hidden");
	 		 $("#fieldEditScenario").prop("disabled", false);
	 		$("#fieldEditScenarioDescription").prop("disabled", false);
	 		$("#jiraStoryLink").addClass("hidden");
		 	 $("#jiraStory").removeClass("hidden");
	 		 
	 		 $('#scenarioDescription').summernote();
	 		$('.note-editor').css('background','white');
	 		$('button[ data-event="codeview"]').remove();
	 		$('.note-editor').css('border','1px solid #CBD5DD');
	 		$('div.note-insert').remove();
	 		$('div.note-table').remove();
	 		$('div.note-help').remove();
	 		$('div.note-style').remove();
	 		$('div.note-color').remove();
	 		$('button[ data-event="removeFormat"]').remove();
	 		$('button[ data-event="insertUnorderedList"]').remove();
	 		$('button[ data-event="fullscreen"]').remove();
	 		$('button[ data-original-title="Line Height"]').remove();
	 		$('button[ data-original-title="Font Family"]').remove();
	 		$('button[ data-original-title="Paragraph"]').remove();  
	 		 }
	 	 
	 	if(viewType == "View")
		 {
	 	  $("#fieldEditScenario").prop("disabled", true);
	 	 $("#fieldEditScenarioDescription").prop("disabled", true);
	 	 $("#jiraStoryLink").removeClass("hidden");
 		 $("#jiraStory").addClass("hidden");
		 $("#btnSubmit").addClass("hidden");
		 $("#cancelUpdate").addClass("hidden");
		 $("#btnEdit").removeClass("hidden");
		 
		 $('#scenarioDescription').summernote();
		 $('.note-editor').css('background','white');
		 $('button[ data-event="codeview"]').remove();
		 $('.note-editor').css('border','1px solid #CBD5DD');
		 $('div.note-insert').remove();
		 $('div.note-table').remove();
		 $('div.note-help').remove();
		 $('div.note-style').remove();
		 $('div.note-color').remove();
		 $('button[ data-event="removeFormat"]').remove();
		 $('button[ data-event="insertUnorderedList"]').remove();
		 $('button[ data-event="fullscreen"]').remove();
		 $('button[ data-original-title="Line Height"]').remove();
		 $('button[ data-original-title="Font Family"]').remove();
		 $('button[ data-original-title="Paragraph"]').remove(); 
		 }
	 	
 	}
	 $("#btnCancel").click(function(){
		 /* $("#createScenarioDiv").addClass('hidden'); 
		 $("#addScenario").removeClass('hidden'); */
		 $("#barInMenu").removeClass("hidden");
		 $("#wrapper").addClass("hidden");
		 location.reload();
	 });
	 
	 $("#btnnCancel").click(function(){
		 /* $("#createScenarioDiv").addClass('hidden'); 
		 $("#addScenario").removeClass('hidden'); */
		 $("#barInMenu").removeClass("hidden");
		 $("#wrapper").addClass("hidden");
		 location.reload();
	 });
	 
	 $("#cancelUpdate").click(function(){
		 /* $("#editScenarioDiv").addClass('hidden'); 
		 $("#addScenario").removeClass('hidden'); */

		 $("#barInMenu").removeClass("hidden");
		 $("#wrapper").addClass("hidden");
		 location.reload();
	 });
	 $("#btnEdit").click(function(){
		 $("#jiraStoryLink").addClass("hidden");
	 	 $("#jiraStory").removeClass("hidden");
		 $("#btnEdit").addClass("hidden");
		 $("#btnSubmit").removeClass("hidden");
		 $("#cancelUpdate").removeClass("hidden");
		 $("#fieldEditScenario").prop("disabled", false);
		 $("#fieldEditScenarioDescription").prop("disabled", false);
		 $('.note-editable').attr('contenteditable','true'); 
	 });
	 
	 $("#createScenarioForm").validate({
			rules : {
				scenarioName : {
					required : true,
					minlength : 1,
					maxlength : 50
				}
			}
		});
	 $("#editScenarioForm").validate({
			rules : {
				scenarioName : {
					required : true,
					minlength : 1,
					maxlength : 50
				}
			}
		});
	 
	 function getScenarios(moduleId,moduleName)
	 {
		 $("#activeModuleId").val(moduleId);
		 $("#activeModuleName").val(moduleName);
		 module = sessionStorage.getItem('currentModuleId');
		 
				 if(module != moduleId){
					 $('#tblModuletList tbody tr').each(function() {
						 if($(this).children('td').first().html()== moduleId)
							 {
							 $(this).children('td').first().trigger('click');
							 }
				 	 });
				 }

	 }
	 
	 function removeScenario(scenario_id){
			
			swal({
				title : "Are you sure?",
				text : "Do you want to remove scenario!",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "Yes, Remove it!",
				cancelButtonText : "No, cancel!",
				closeOnConfirm : true,
				closeOnCancel : true
			}, function(isConfirm) {

				if (isConfirm) {
					
					$('body').addClass("white-bg");
					$("#barInMenu").removeClass("hidden");
					$("#wrapper").addClass("hidden");
					
					var posting = $.post('removescenario',{
						scenarioId : scenario_id,
					});
					
					posting.done(function(data){
						window.location.href = "testspecification";
					}); 	
				}
			});
			
		}
	 
</script>
<%
	//set session variable to another value
	session.setAttribute("insertedScenarioStatus", 4);
%>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/codemirror/mode/filestyle/bootstrap-filestyle.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/codemirror/mode/filestyle/bootstrap-filestyle.js"></script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Dataset</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li class="Active"><strong>Dataset</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce" id="datasetDiv">
<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Dataset</h5>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-12">
								<div>
									<canvas id="doughnutChart" height="80"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Dataset</h5>
				</div>

				<div class="ibox-content">
					<div class="">
						<div class="tab-content">
							<div class="tab-pane active">
								<div class="full-height-scroll">
									<div class="table-responsive" style="overflow-x: visible;">
										<table class="table table-striped table-hover"
											id="viewdataset">
											<thead>
												<tr>
													<th>Dataset Name</th>
													<th>Status</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="row" items="${Model.datasetList}">
													<tr data-toggle="tab" class="dataSetTr" onclick="showDatasetTestStep(${row.dataset_id})"
											style="cursor: pointer;">
														<td class="xlTextAlignment"
															data-original-title="${row.dataset_name}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${row.dataset_name}</td>

														<td class="xlTextAlignment"
															data-original-title="${row.description}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${row.description}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<c:forEach var="testCaseList" items="${Model.datasetList}">
	<div id="testStepDiv_${testCaseList.dataset_id}" class="hidden commonClass">
	<div class="wrapper wrapper-content animated fadeInRight row">
	
	
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Test Steps</h5>
						
						<button type="button" onclick="window.location.href='viewdataset'" class="btn btn-success btn-sm pull-right">Back to Datasets</button>
					</div>
					<div class="ibox-content">
						<table class="table table-stripped table-hover testStepTable"
							id="testStepsDetailsTable_${testCaseList.dataset_id}">
							<thead>
								<tr>
									<th>Step ID</th>
									<th>Step Description</th>
									<th>Test/Execution Data</th>
									<th>Actual Result</th>
									<th>Status</th>
									<th class="text-right">Screenshot</th>
									<th>Bug ID</th>
								</tr>

							</thead>
							<tbody>
								<c:forEach var="testStepsList" items="${Model.testStepsList}">
									<c:if
										test="${testCaseList.dataset_id == testStepsList.dataset_id}">
										<tr>
											<td>${testStepsList.Step_id}</td>
											<td>${testStepsList.step_details}</td>
											<td>${testStepsList.step_value}</td>
											<td>${testStepsList.actual_result}</td>
											<td>${testStepsList.description}</td>
											<td class="text-right">
											<c:if
													test="${not empty testStepsList.screenshot_title}">
													<div class="btn-group">
														<button id="view_${testStepsList.trial_step_id}"
															class="btn-white btn btn-xs"
															onclick="viewScreenshot(${testStepsList.trial_step_id},'${testStepsList.screenshot_title}');"
															style="margin-right: 10px;">View</button>
														<%-- <button class="btn btn-white btn-xs" 
													onclick="getScreenshot(${testStepsList.trial_step_id},'${testStepsList.screenshot_title}');">Download</button> --%>
													</div>


												</c:if> <c:if test="${empty testStepsList.screenshot_title}">
													<div class="btn-group">NA</div>
												</c:if>
												</td>
											<td><c:if test="${testStepsList.is_bug_raised=='1'}">
											   -
											  </c:if> <c:if test="${testStepsList.is_bug_raised=='2'}">
													<c:forEach var="testStepsBugList"
														items="${Model.testStepsBugList}">
														<c:if
															test="${testStepsList.build_id == testStepsBugList.build_id}">
															<c:if
																test="${testCaseList.testcase_id == testStepsBugList.test_case_id}">
																<c:if
																	test="${testStepsList.step_id == testStepsBugList.test_step_id}">
																	<a
																		href="bugsummarybyprefix?btPrefix=${testStepsBugList.bug_prefix}"
																		target="_blank">
																		Bug #${testStepsBugList.bug_prefix}</a>
																</c:if>
															</c:if>
														</c:if>
													</c:forEach>
												</c:if></td>
										</tr>
									</c:if>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div> 
		</div>
		</c:forEach>


<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
		getDatasetPiechart();
	});
	
	function getDatasetPiechart(){
		var testCaseDetails = $('#viewdataset').DataTable();

		var allTableRow = testCaseDetails.$(".dataSetTr", {
			"page" : "all"
		});
		var fail=0;
		var pass=0;
		var skipp=0;
		var block=0;
		var notRun=0;
		
		allTableRow.each(function() {
			var status = $.trim($(this).find("td:eq(1)").html().toLowerCase());
			if(status == "not run"){
				notRun++;
			}
			else if(status == "pass"){
				pass++;
			}
			else if(status == "skip"){
				skipp++;
			}
			else if(status == "fail"){
				fail++;
			}
			else if(status == "block"){
				block++;
			}
		});
		
		
		var doughnutData = {
			    labels: ["Pass","Fail","Skip","Block","Not Run"],
			    datasets: [
			        {
			            data: [pass,fail,skipp,block,notRun],
			            backgroundColor: [
			                "#009900",
			                "#cc2900",
			                "#e6e600",
			                "#804000",
			                "#787878"
			            ],
			            hoverBackgroundColor: [
			                                "#009900",
			       			                "#cc2900",
			       			                "#e6e600",
			       			                "#804000",
			       			                "#787878"
			            ]
			        }]
			};
		
		var doughnutOptions = {
			segmentShowStroke : true,
			segmentStrokeColor : "#fff",
			segmentStrokeWidth : 2,
			percentageInnerCutout : 45, // This is 0 for Pie charts
			animationSteps : 100,
			animationEasing : "easeOutBounce",
			animateRotate : true,
			animateScale : true,
			responsive : true,
		};
		 	    var ctx = document.getElementById("doughnutChart")	;
		 	   var myDoughnutChart = new Chart(ctx, {
				    type: 'doughnut',
				    data: doughnutData,
				    options: doughnutOptions
				});
		
	}
	
	$(function(){
		$('.testStepTable').DataTable();
	});
	
	 function showDatasetTestStep(dataSetId){
		 $('.commonClass').each(function(){
			$(this).addClass('hidden');
		});
		 $('#datasetDiv').addClass('hidden');
		
		 $('#testStepDiv_'+dataSetId).removeClass('hidden');  
		 /* $('body').addClass("white-bg");
		 $("#barInMenu").removeClass("hidden");
		 $("#wrapper").addClass("hidden");
		var posting = $.post('settcid',{
			testcaseId : testCaseID
		});
		posting.done(function(data){
			window.location.href="viewdataset";
		}); */
	} 
	 
	  function showModuleWiseReport(projectId){
	 }
	  
	  function showScenarioWiseReport(showScenarioWiseReport){
	  }
	  
	  function showTCWiseReport(scenarioID,moduleID){
		  window.location.href="automationsummary"; 
	  }
</script>
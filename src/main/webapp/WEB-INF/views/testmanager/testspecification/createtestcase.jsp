<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Scenario - ${Model.scenarioName}</h2>
		<ol class="breadcrumb">
<!-- 			<li><a href="tmdashboard">Dashboard</a></li>
 -->			<li><a href="testspecification">Test Case Management</a></li>
			<li><a href="testcase">Test Cases</a></li>
			<li class="active"><strong>Create Test Cases</strong></li>
		</ol>
	</div> 
</div>

<div class="wrapper row wrapper-content animated fadeInRight">
	<div class="col-lg-12">
		<div class="ibox">
			<div class="ibox-content">
				<form action="inserttestcase"
					class="wizard-big wizard clearfix form-horizontal" method="POST"
					id="createTestcaseForm" enctype="multipart/form-data">
					<div class="content clearfix">
						<fieldset class="body current">
							<div class="row">
								<label class="col-lg-4 pull-right text-right">* fields
									are mandatory</label>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group hidden">
										<div class="col-sm-6">
											<input type="text" class="form-control"
												value="${Model.scenarioId}" name="scenarioId"
												id="activeScenario">
										</div>
									</div>
									<div class="form-group hidden">
										<div class="col-sm-6">
											<input type="text" class="form-control characters"
												value="${Model.scenarioName}" name="scenarioName"
												id="activeScenario">
										</div>
									</div>
									<div class="form-group hidden">
										<div class="col-sm-8">
											<input type="text" class="form-control"
												value="${Model.moduleId}" name="moduleId" id="activeModule">
										</div>
									</div>
									<div class="form-group hidden">
										<div class="col-sm-8">
											<input type="text" class="form-control characters"
												value="${Model.moduleName}" id="activeModuleName">
										</div>
									</div>

									<div class="col-lg-12">

										<div class="form-group">
											<label class="col-sm-2 control-label">Title *:</label>
											<div class="col-sm-10">
												<input type="text" class="form-control characters"
													placeholder="Testcase Name" name="testcaseName"
													tabindex="1">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Summary :</label>
											<div class="col-sm-10">
												<textarea placeholder="Testcase Summary" rows="4"
													class="form-control characters summernote"
													style="resize: none;" name="testcaseSummary" tabindex="2"> </textarea>
											</div>
										</div>

										
									</div>
									<div class="col-lg-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">Status :</label>
											<div class="col-sm-10">
												<select name="statusId" class="form-control"
													data-placeholder="Choose a Status" tabindex="6">
													<c:forEach var="status" items="${Model.statusList}">
														<option value="${status.tc_status_id}">${status.status}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Precondition :</label>
											<div class="col-sm-10">
												<textarea placeholder="Testcase Precondition" rows="4"
													class="form-control characters summernote"
													style="resize: none;" name="testcasePrecondition"
													tabindex="7"> </textarea>
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-2 control-label">Execution Type
												:</label>
											<div class="col-sm-10">
												<div class="radio radio-primary radio-inline col-sm-2 ">
													<input type="radio" id="inlineRadio1"
														value="${executionTypes[0].execution_type_id}"
														name="executionType" checked="" tabindex="3"> <label
														for="inlineRadio1">
														${executionTypes[0].description} </label>
												</div>
												<c:if test="${executionTypes[1].execution_type_id > 0}">
													<div class="radio radio-success radio-inline col-sm-2">
														<input type="radio" id="inlineRadio2"
															value="${executionTypes[1].execution_type_id}"
															name="executionType" tabindex="4"> <label
															for="inlineRadio2">
															${executionTypes[1].description} </label>
													</div>
												</c:if>
												<c:if test="${executionTypes[2].execution_type_id > 0}">
													<div class="radio radio-default radio-inline col-sm-2">
														<input type="radio" id="inlineRadio3"
															value="${executionTypes[2].execution_type_id}"
															name="executionType" tabindex="5"> <label
															for="inlineRadio3">
															${executionTypes[2].description} </label>
													</div>
												</c:if>
											</div>
										</div>
										
										<div class="form-group" id="datasheetStatusDiv">
											<label class="col-sm-2 control-label">Datasheet
												Present:</label>
											<div class="col-sm-10">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="datasheetStatus1" value="1"
														name="datasheetStatus" tabindex="5"> <label
														for="datasheetStatus1">YES</label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="datasheetStatus2" value="2"
														name="datasheetStatus" checked="" tabindex="6"> <label
														for="datasheetStatus2">NO</label>
												</div>

												<div class="col-sm-6" id="uploadDatasheetDiv">
													<input type="file" class="filestyle" name="uploadDatasheet"
														id="uploadDatasheetBtn" data-buttonName="btn-primary" required="true">
												</div>

											</div>
										</div>

										<div class="form-group hidden">
											<label class="col-sm-2 control-label">Execution Time
												*:</label>
											<div class="col-sm-10">
												<input type="text" min="0" value="0" class="form-control"
													placeholder="Estimated Execution Time in Minutes"
													name="executionTime">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="hr-line-solid"></div>
						</fieldset>
					</div>
					<div class="actions clearfix">
						<div class="row">
							<div class="col-sm-4">
								<button class="btn btn-white pull-left"
									style="margin-right: 15px;" type="button" id="btnCancel"
									type="button" tabindex="6">Cancel</button>
								<button class="btn btn-success pull-left ladda-button"
									id="uploadTestcaseBtn" data-style="slide-up" tabindex="7">Submit</button>
							</div>
						</div>
					</div>
				</form>
				<!-- end form -->
			</div>
		</div>
	</div>
</div>

<script>
	$(window).load(function() {
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");

		if ($('#datasheetStatus1').is(':checked')) {
			$("#uploadDatasheetDiv").removeClass("hidden")
		}

		if ($('#datasheetStatus2').is(':checked')) {
			$("#uploadDatasheetDiv").addClass("hidden")
		}
		
		if ($('#inlineRadio1').is(':checked')) {
			$("#datasheetStatusDiv").addClass("hidden")
		}
		
		
		if ($('#inlineRadio2').is(':checked') || $('#inlineRadio3').is(':checked')) {
			$("#datasheetStatusDiv").removeClass("hidden")
		}
		
		$(":file").filestyle({
			buttonName : "btn-primary"
		});

	});

	$('#datasheetStatus1').click(function() {
		if ($('#datasheetStatus1').is(':checked')) {
			$("#uploadDatasheetDiv").removeClass("hidden")
		}
		;
		if ($('#datasheetStatus2').is(':checked')) {
			$("#uploadDatasheetDiv").addClass("hidden")
		}
		;
	});

	$('#datasheetStatus2').click(function() {
		if ($('#datasheetStatus1').is(':checked')) {
			$("#uploadDatasheetDiv").removeClass("hidden")
		}
		;
		if ($('#datasheetStatus2').is(':checked')) {
			$("#uploadDatasheetDiv").addClass("hidden")
		}
		;
	});
	
	$('#inlineRadio1').click(function() {
		if ($('#inlineRadio1').is(':checked')) {
			$("#datasheetStatusDiv").addClass("hidden")
		};
	});

	$('#inlineRadio2').click(function() {
		if ($('#inlineRadio2').is(':checked')) {
			$("#datasheetStatusDiv").removeClass("hidden")
		};
		
		if ($('#inlineRadio1').is(':checked')) {
			$("#datasheetStatusDiv").addClass("hidden")
		};
	});

	$('#inlineRadio3').click(function() {
		if ($('#inlineRadio3').is(':checked')) {
			$("#datasheetStatusDiv").removeClass("hidden")
		};
		
		if ($('#inlineRadio1').is(':checked')) {
			$("#datasheetStatusDiv").addClass("hidden")
		};
	});

	$(function() {
		//form validation
		$("#createTestcaseForm").validate({
			rules : {
				testcaseName : {
					required : true,
					minlength : 1,
					maxlength : 250
				},
				executionTime : {
					required : true,
					number : true
				},
				testcaseSummary : {
					minlength : 1,
					maxlength : 4000
				},
			}
		});

		//code for generating the left menu 
		var scenarioList = [];
		scenarioList = ${Model.scenarioList};
		var moduleId = $("#activeModule").val();
		var moduleName = $("#activeModuleName").val();
		for (var i = 0; i < scenarioList.length; i++) {
			if (moduleId == scenarioList[i].module_id) {
				scenarioMenu = "<ul class='nav nav-second-level collapse-in'><li id='"+scenarioList[i].scenario_id+"' class='scenarioLinks'>"
						+ "<a class='lgMenuTextAlignment menuAnchor menuTooltip' href='#' data-original-title='"
						+ scenarioList[i].scenario_name
						+ "' data-container='body' data-toggle='tooltip' data-placement='right' onclick='getTestCaseList("
						+ scenarioList[i].scenario_id
						+ ",\""
						+ scenarioList[i].scenario_name
						+ "\","
						+ moduleId
						+ ",\""
						+ moduleName
						+ "\")'>"
						+ scenarioList[i].scenario_name + "</a></li></ul>";
				$("#module_" + moduleId).append(scenarioMenu);
				$("#module_" + moduleId).addClass("active");
			}
		}
		//tooltip
		$('[data-toggle="tooltip"]').tooltip();

		//summernote rich text editor
		$('.summernote').summernote();
		$('.note-editor').css('background', 'white');
		$('button[ data-event="codeview"]').remove();
		$('div.note-insert').remove();
		$('div.note-table').remove();
		$('div.note-help').remove();
		$('div.note-style').remove();
		$('div.note-color').remove();
		$('button[ data-event="removeFormat"]').remove();
		$('button[ data-event="insertUnorderedList"]').remove();
		$('button[ data-event="fullscreen"]').remove();
		$('button[ data-original-title="Line Height"]').remove();
		$('button[ data-original-title="Font Family"]').remove();
		$('button[ data-original-title="Paragraph"]').remove();
	});

	$('#createTCbtn').click(function() {
		if ($('#createTestcaseForm').valid()) {

			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");

			$('.summernote').each(function() {
				$(this).val($(this).code());
			});

			$('#createTestcaseForm').submit();
		}
	});

	function getScenarios(moduleId, moduleName) {
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");

		sessionStorage.setItem('currentModuleId', moduleId);
		window.location.href = "testspecification";
	}
</script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/codemirror/mode/filestyle/bootstrap-filestyle.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/codemirror/mode/filestyle/bootstrap-filestyle.js"></script>
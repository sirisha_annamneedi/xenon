<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-11">
		<h2>
			<span class="lgMenuTextAlignment" style="width: 90% !important;" data-original-title="${Model.scenarioDetails[0].scenario_name}" data-toggle="tooltip">
				<ol class="breadcrumb">
					<li><strong>${UserCurrentProjectName}</strong></li>
					<c:forEach var="moduleL" items="${Model.moduleList}">
						<c:if test="${moduleL.module_id == Model.moduleId}">
							<li>${moduleL.module_name}</li>
						</c:if>
					</c:forEach>
				<li>${selectedScenarioName}</li>
					<li>${Model.testcaseDetails[0].test_prefix}</li>
					
				</ol>
				</span>
			</h2>
<!-- 			<ol class="breadcrumb"> -->
				<!-- 				<li><a href="tmdashboard">Dashboard</a></li>
 -->
				<!--  <li><a href="testspecification">Test Case Management</a></li> -->
<!-- 				<li><a href="testcase">Test Cases</a></li> -->
<!-- 				<li class="active"><strong>Steps</strong></li> -->
<!-- 			</ol> -->
		</div>
	</div>
	<div class="row sub-topmenu">
		<div class="col-md-12">
			<div class="">
				<div class="col-md-6 pd-l-0">
					<ul>
						<li><a href="testcase"><i class="fa fa-home"></i> Home</a></li>
						<li><a href="javascript:void(0)" id="homeMenu"
							class="active "><i class="fa fa-list"></i> Test Steps </a></li>
						<!-- <li><a href="javascript:void(0)" id="btnAddStep"><i
								class="fa fa-plus"></i> Add Test Step</a></li> -->
						<li><a href="javascript:void(0)" id="commentMenu"><i
								class="fa fa-pencil"></i> Add Comment</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">

		<div class="row" style="display: none;">
			<div class="col-lg-12">
				<div class="ibox collapsed">
					<div class="ibox-title">

						<div class="ibox-tools">
							<a class="collapse-link"> <i
								class="fa fa-chevron-right pull-left"></i>
								<h5>${Model.testcaseDetails[0].testcase_name}</h5>
							</a>
						</div>
					</div>
					<div class="ibox-content" id="editTestCaseForm">
						<form action="updatetestcase" id="editTestcaseform" method="POST"
							class="wizard-big wizard clearfix form-horizontal"
							enctype="multipart/form-data">
							<div class="content clearfix">
								<c:if test="${Model.recentFlag == 1}">
									<fieldset class="body current" id="fieldset" disabled>
								</c:if>
								<c:if test="${Model.recentFlag == 0}">
									<fieldset class="body current" id="fieldset">
								</c:if>
								<div class="row">
									<label class="col-lg-4 pull-right text-right">* fields
										are mandatory</label>
								</div>
								<div class="row">
									<div class="col-lg-10">
										<div class="form-group hidden">
											<div class="col-sm-6">
												<input type="text" class="form-control"
													value="${Model.scenarioId}" name="scenarioId"
													id="activeScenario">
											</div>
										</div>
										<div class="form-group hidden">
											<div class="col-sm-8">
												<input type="text" class="form-control"
													value="${Model.moduleId}" name="moduleId" id="activeModule">
											</div>
										</div>
										<div class="form-group hidden">
											<div class="col-sm-8">
												<input type="text" class="form-control characters"
													value="${Model.moduleName}" id="activeModuleName">
											</div>
										</div>
										<div class="form-group hidden">
											<div class="col-sm-8">
												<input type="text" class="form-control"
													value="${Model.testcaseDetails[0].testcase_id}"
													name="testcaseId" id="testcaseId">
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">Title *:</label>
											<div class="col-sm-8">
												<input type="text" class="form-control characters"
													placeholder="Testcase Name"
													value="${Model.testcaseDetails[0].testcase_name}"
													name="testcaseName" id="testcaseName">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Summary:</label>
											<div class="col-sm-8" id="txtAreaSummary">
												<textarea placeholder="Testcase Summary" rows="1"
													class="form-control characters tcSummernote"
													style="resize: none; border: 1px solid #ccc;"
													name="testcaseSummary" id="testcaseSummary">${Model.testcaseDetails[0].summary}</textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Precondition:</label>
											<div class="col-sm-8" id="txtAreaCondition">
												<textarea placeholder="Testcase Precondition" rows="1"
													class="form-control characters tcSummernote"
													style="resize: none; border: 1px solid #ccc;"
													name="testcasePrecondition" id="testcasePrecondition">${Model.testcaseDetails[0].precondition} </textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Status :</label>
											<div class="col-sm-8">
												<select name="statusId" class="form-control"
													data-placeholder="Choose a Status">
													<c:forEach var="status" items="${Model.statusList}">
														<option value="${status.tc_status_id}"
															${status.tc_status_id == Model.testcaseDetails[0].tc_status_id ? 'selected="selected"' : ''}>${status.status}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Execution Type:</label>
											<div class="col-sm-10">
												<div class="radio radio-primary radio-inline col-sm-2 ">
													<input type="radio" id="inlineRadio1"
														value="${executionTypes[0].execution_type_id}"
														name="executionType"
														${executionTypes[0].execution_type_id == Model.testcaseDetails[0].execution_type ?'checked=""':''}>
													<label for="inlineRadio1">
														${Model.executionTypes[0].description} </label>
												</div>
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="inlineRadio2"
														value="${executionTypes[1].execution_type_id}"
														name="executionType"
														${executionTypes[1].execution_type_id == Model.testcaseDetails[0].execution_type ?'checked=""':''}>
													<label for="inlineRadio2">
														${executionTypes[1].description} </label>
												</div>
												<div class="radio radio-default radio-inline col-sm-2">
													<input type="radio" id="inlineRadio3"
														value="${executionTypes[2].execution_type_id}"
														name="executionType"
														${executionTypes[2].execution_type_id == Model.testcaseDetails[0].execution_type ?'checked=""':''}>
													<label for="inlineRadio3">
														${executionTypes[2].description} </label>
												</div>
											</div>
										</div>

										<div id="datasheetStatusDiv">
											<div class="form-group">
												<label class="col-sm-2 control-label">Datasheet
													Present:</label>
												<div class="col-sm-10">
													<div class="radio radio-success radio-inline col-sm-2">
														<input type="radio" id="datasheetStatus1" value="1"
															name="datasheetStatus" tabindex="5"
															${Model.testcaseDetails[0].excel_file_present_status == '1' ?'checked=""':''}>
														<label for="datasheetStatus1">YES</label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-2">
														<input type="radio" id="datasheetStatus2" value="2"
															name="datasheetStatus" tabindex="6"
															${Model.testcaseDetails[0].excel_file_present_status == '2' ?'checked=""':''}>
														<label for="datasheetStatus2">NO</label>
													</div>

													<div class="col-sm-6" id="uploadDatasheetDiv">
														<input type="file" class="filestyle"
															name="uploadDatasheet" id="uploadDatasheetBtn"
															data-buttonName="btn-primary">
													</div>

												</div>
											</div>
											<c:if
												test="${Model.testcaseDetails[0].excel_file_present_status == '1'}">
												<div class="form-group" id="datasheetFileDiv">
													<label class="col-sm-2 control-label">Uploaded File
														:</label>
													<div class="col-sm-8">
														<input class="hidden" id="datasheetLabel"
															value="${Model.datasheetFileName}" /> <label
															class="control-label">${Model.datasheetFileName}</label>
													</div>
												</div>
											</c:if>
										</div>


										<div class="form-group hidden">
											<label class="col-sm-2 control-label">Execution Time
												* :</label>
											<div class="col-sm-8">
												<input type="text" class="form-control"
													placeholder="Estimated Execution Time in Minuit"
													name="executionTime" value="0" id="executionTime">
											</div>
										</div>
									</div>
								</div>
								</fieldset>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-5">
										<button class="btn btn-white pull-left hidden" type="button"
											style="margin-right: 15px;" id="btnCancel">Cancel</button>
										<c:if test="${Model.recentFlag == 1}">
											<button class="btn btn-success pull-left" type="button"
												id="btnEdit">Edit</button>
											<button class="btn btn-success pull-left" type="button"
												style="display: none" id="btnUpdate">Update</button>
										</c:if>
										<c:if test="${Model.recentFlag == 0}">
											<button class="btn btn-success pull-left" type="button"
												id="btnUpdate">Update</button>
										</c:if>
									</div>
								</div>
							</div>

						</form>
						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<div class="row" id="testCaseSteps">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Test Case Steps</h5>
						<!-- <button type="button" class="btn btn-primary btn-sm" style="float:right" id="btnAddStep">Add Test Steps</button>-->
					</div>
					<div class="ibox-content">

						<div class="table-responsive">
							<table
								class="table table-striped table-bordered table-hover dataTables-example"
								id="c">
								<thead>
									<tr>
										<th style="display: none">Id</th>
										<th>Step No</th>
										<th>Steps</th>
										<th>Expected Result</th>
										<th class="text-right">Action</th>
									</tr>
								</thead>
								<tbody>
								
									<c:forEach var="step" items="${Model.stepList}" varStatus="stepLoop">
										<tr class="tblStepRow">
											<td style="display: none">${step.step_id}</td>
<%-- 											<td>${step.test_step_id}</td> --%>
											<td>${stepLoop.index+1}</td>
											<td>${step.action}</td>
											<td>${step.result}</td>
											<td class="text-right">
												<button class="btn-white btn btn-xs edit-btn editSteps">
													<i class="fa fa-edit"></i>
												</button>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>


			</div>
		</div>

		<div class="row" id="createStepDiv">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Add Test Case Steps</h5>
						<a onclick="fnClickAddRow();" href="javascript:void(0);"
								class="btn btn-success btn-sm pull-right">Add Row</a>
								<!-- <button onclick="fnCancel();"
								class="btn btn-default btn-sm pull-right"
								style="margin-right: 10px">Cancel</button>-->
					</div>
					<div class="ibox-content">
						<div class="">
							
							
						</div>
						<table class="table table-striped table-bordered table-hover"
							id="editable">
							<thead>
								<tr>
									<th width="10%">Step No</th>
									<th>Steps</th>
									<th>Expected Result</th>
									<th width="5%"></th>

								</tr>
							</thead>
							<tbody>
								<tr class="gradeX stepRow">
									<td>0</td>
									<td class="stepVal"><textarea class="summernote"></textarea>
										<label class="error hidden">This field is required</label></td>
									<td class="expResult"><textarea class="summernote"></textarea>
										<label class='error hidden'>This field is required</label></td>
									<td><button
											class="btn btn-xs btn-default btn-circle pull-right"
											type="button" id="removeRow">
											<i class="fa fa-times"></i>
										</button></td>

								</tr>
							</tbody>
						</table>
						<label id="actualResultError" class="error hidden"
							for="actualResult"> * The field Step is required</label> <label
							id="expectedResultError" class="error hidden"
							for="ExpectedResult">* The field Expected Result is
							required</label>
						<div class="">
							<!-- <a onclick="fnCancel();" href="javascript:void(0);"
								class="btn btn-default btn-sm " style="margin-right: 10px">Cancel</a>-->
							<a href="javascript:void(0);"
								class="btn btn-success btn-sm ladda-button ladda-demo"
								id="btnSubmitStep" data-style="slide-up">Submit</a>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="addCommentDiv" style="display: none;">
			<div class="col-lg-12 graph-dashboard">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Comments</h5>
						<div class="ibox-tools"></div>
					</div>
					<div class="ibox-content">
						<div class="feed-activity-list">
							<div class="feed-element">
								<div class="col-lg-12" id="commentDiv">
									<form id="commentForm" role="form" action="inserttccomment"
										method="POST" novalidate="novalidate">
										<div class="form-group ">
											<textarea class="form-control summernote"
												id="commentDescription" name="commentDescription"
												placeholder="Comment"></textarea>
											<label id="commentError" class="error hidden"
												for="commentError"> The field is required</label>
										</div>
										<div class="form-group ">
											<button class="btn btn-success pull-right" type="button"
												id="btnSubmitComment">
												<i class="fa fa-pencil"></i>Add Comment
											</button>

											<button class="btn btn-white pull-right" type="button"
												style="margin-right: 15px;" onclick="fnCancel();">Cancel</button>

										</div>
									</form>
								</div>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content">
						<div class="row m-t-sm">
							<div class="col-lg-12">
								<div class="panel blank-panel">
									<div class="panel-heading">
										<div class="panel-options">
											<ul class="nav nav-tabs">
												<li class="active"><a href="#tab-1" data-toggle="tab"
													aria-expanded="true"> Comments </a></li>
												<li class=""><a href="#tab-2" data-toggle="tab"
													aria-expanded="false"> Activity </a></li>


											</ul>
										</div>
										<!-- end panel-options -->
									</div>

									<!-- end panel heading -->
									<div class="panel-body">
										<div class="tab-content">
											<div class="tab-pane active" id="tab-1">
												<div class="chat-activity-list">
													<c:forEach var="testComments" items="${Model.testComments}"
														varStatus="loop">
														<div class="feed-element">
															<a href="#" class="pull-left"> <img alt="image"
																class="img-circle"
																src="data:image/jpg;base64,${testComments.commenterImage}">
															</a>
															<div class="media-body ">


																<c:set var="date"
																	value="${fn:split(testComments.comment_time,' ')}" />


																<c:choose>

																	<c:when test="${testComments.TimeDiff < 60 }">
																		<strong class="pull-right">${testComments.TimeDiff}
																			min ago</strong>
																	</c:when>

																	<c:when
																		test="${testComments.TimeDiff >= 60 && testComments.TimeDiff < 1440 }">
																		<fmt:parseNumber var="hour" integerOnly="true"
																			type="number" value="${testComments.TimeDiff/60}" />
																		<strong class="pull-right">${hour} hr ago </strong>
																	</c:when>


																	<c:when
																		test="${testComments.TimeDiff >= 1440 && testComments.TimeDiff < 2880 }">
																		<strong class="pull-right"> Yesterday :
																			${date[1]} ${date[2]} </strong>
																	</c:when>
																	<c:otherwise>
																		<strong class="pull-right">${testComments.comment_time}
																		</strong>
																	</c:otherwise>
																</c:choose>

																<strong>${testComments.user_name} </strong> posted
																comment.<br> <small class="text-muted">${testComments.comment_time}</small>
																<div class="pull-right">

																	<c:if test="${testComments.likeStatus == 2 }">
																		<a class="btn btn-xs btn-white"
																			onClick="changeLikeStatus(${testComments.xetm_testcase_comments_id})"
																			id="like_${testComments.xetm_testcase_comments_id}"><i
																			class="fa fa-thumbs-up"></i> <span
																			id="pLikeCount_${testComments.xetm_testcase_comments_id}">${testComments.likeCount}</span>
																		</a>
																	</c:if>
																	<c:if test="${testComments.likeStatus == 1 }">
																		<a class="btn btn-xs btn-info"
																			onClick="changeLikeStatus(${testComments.xetm_testcase_comments_id})"
																			id="like_${testComments.xetm_testcase_comments_id}">
																			<i class="fa fa-thumbs-up"></i> <span
																			id="pLikeCount_${testComments.xetm_testcase_comments_id}">${testComments.likeCount}</span>
																		</a>

																	</c:if>

																</div>
																<div class="well">${testComments.comment_description}</div>
															</div>
														</div>
													</c:forEach>
												</div>
											</div>
											<!-- end #tab-1 -->
											<div class="tab-pane" id="tab-2">
												<table class="table table-striped">
													<thead>
														<tr>
															<th class="hidden">Test Case Id</th>
															<th></th>
															<th>Activity</th>
															<th>User</th>
															<th>Activity Date</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="summary" items="${Model.testCaseSummary}"
															varStatus="loopCounter">
															<tr>
																<td class="hidden">${summary.test_case_id}</td>
																<td><a href="#"> <img alt="image"
																		class="img-circle" style="height: 38px"
																		src="data:image/jpg;base64,${summary.user_photo}">
																</a></td>
																<td><span>${summary.activity_desc} </span></td>
																<td>${summary.user_name}</td>
																<td>${summary.activity_date}</td>
															</tr>

														</c:forEach>
													</tbody>
												</table>
											</div>
											<!-- end #tab-2 -->
										</div>
										<!-- end tab-content -->
									</div>
									<!-- end panel body -->
								</div>
								<!-- end panel -->
							</div>
							<!-- end col -->
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>


<div class="modal inmodal" id="updateStep" tabindex="-1" role="dialog"
	aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<h6 class="modal-title pull-left">Update Step</h6>
				<button type="button" class="close pull-right" data-dismiss="modal">
					<span aria-hidden="false">�</span><span class="sr-only">Close</span>
				</button>

			</div>

			<div class="modal-body">
				<form action="updatestep"
					class="wizard-big wizard clearfix form-horizontal"
					enctype="multipart/form-data" method="POST" id="updateStepForm">
					<div class="form-group" id="updateStepActionDivGrp">
						<input type="text" class="form-control hidden" name="updateStepId">
						<input type="text" class="form-control hidden"
							name="updateTestStepId"> <label
							class="control-label col-lg-2">Step *</label>
						<div class="col-lg-10" id="updateStepActionDiv">
							<textarea placeholder="Step" class="form-control summernote"
								aria-required="true" name="updateStepAction"
								id="updateStepAction"></textarea>

						</div>
						<label class='error hidden' style="margin-left: 20%;">This
							field is required</label>
					</div>
					<div class="form-group" id="updateStepResultDivGrp">
						<label class="control-label col-lg-2">Expected Result*</label>
						<div class="col-lg-10" id="updateStepResultDiv">
							<textarea placeholder="Step Result"
								class="form-control summernote" aria-required="true"
								name="updateStepResult" id="updateStepResult"></textarea>

						</div>
						<label class='error hidden' style="margin-left: 20%;">This
							field is required</label>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white pull-left"
					data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-success pull-left"
					id="btnUpdateStep">Submit</button>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="hiddenInput"
	value=<%=session.getAttribute("TcCreateStatus")%>>

<input type="hidden" id="newTcName"
	value='<%=session.getAttribute("newTcName")%>'>

<input type="hidden" id="TcStepCreateStatus"
	value=<%=session.getAttribute("TcStepCreateStatus")%>>

<style>
@media only screen and (min-width : 1224px) {
	/* Styles */
	.table-responsive {
		overflow-x: visible ! important;
	}
}

/* Large screens ----------- */
@media only screen and (min-width : 1824px) {
	.table-responsive {
		overflow-x: visible ! important;
	}
}
</style>
<!-- end wrapper -->
<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	if ($('#datasheetStatus1').is(':checked')) {
		$("#uploadDatasheetDiv").removeClass("hidden")
		$("#datasheetFileDiv").removeClass("hidden")
	}
	
	if ($('#inlineRadio1').is(':checked')) {
		$("#datasheetStatusDiv").addClass("hidden")
	}
	
	
	if ($('#inlineRadio2').is(':checked') || $('#inlineRadio3').is(':checked')) {
		$("#datasheetStatusDiv").removeClass("hidden")
	}

	if ($('#datasheetStatus2').is(':checked')) {
		$("#uploadDatasheetDiv").addClass("hidden")
		$("#datasheetFileDiv").addClass("hidden")
	}
	$(":file").filestyle({
		buttonName : "btn-primary"
	});
});

$('#datasheetStatus1').click(function() {
	if ($('#datasheetStatus1').is(':checked')) {
		$("#uploadDatasheetDiv").removeClass("hidden")
		$("#datasheetFileDiv").removeClass("hidden")
	}
	;
	if ($('#datasheetStatus2').is(':checked')) {
		$("#uploadDatasheetDiv").addClass("hidden")
		$("#datasheetFileDiv").addClass("hidden")
	}
	;
});

$('#datasheetStatus2').click(function() {
	if ($('#datasheetStatus1').is(':checked')) {
		$("#uploadDatasheetDiv").removeClass("hidden")
		$("#datasheetFileDiv").removeClass("hidden")
	}
	;
	if ($('#datasheetStatus2').is(':checked')) {
		$("#uploadDatasheetDiv").addClass("hidden")
		$("#datasheetFileDiv").addClass("hidden")
	}
	;
});

$('#inlineRadio1').click(function() {
	if ($('#inlineRadio1').is(':checked')) {
		$("#datasheetStatusDiv").addClass("hidden")
	};
});

$('#inlineRadio2').click(function() {
	if ($('#inlineRadio2').is(':checked')) {
		$("#datasheetStatusDiv").removeClass("hidden")
	};
	
	if ($('#inlineRadio1').is(':checked')) {
		$("#datasheetStatusDiv").addClass("hidden")
	};
});

$('#inlineRadio3').click(function() {
	if ($('#inlineRadio3').is(':checked')) {
		$("#datasheetStatusDiv").removeClass("hidden")
	};
	
	if ($('#inlineRadio1').is(':checked')) {
		$("#datasheetStatusDiv").addClass("hidden")
	};
});
  
	$(function () {
	  $('.summernote').summernote();
	  $('.note-editor').css('background','white');
      $('button[ data-event="codeview"]').remove();
      $('.note-editor').css('border','1px solid #CBD5DD');
      $('.note-editor').css('margin-left','10px');
      
      $('div.note-insert').remove();
      $('div.note-table').remove();
      $('div.note-help').remove();
      $('div.note-style').remove();
      $('div.note-color').remove();
      $('button[ data-event="removeFormat"]').remove();
      $('button[ data-event="insertUnorderedList"]').remove();
      $('button[ data-event="fullscreen"]').remove();
      $('button[ data-original-title="Line Height"]').remove();
      $('button[ data-original-title="Font Family"]').remove();
      $('button[ data-original-title="Paragraph"]').remove();

      $('.tcSummernote').summernote();
	  $('.note-editor').css('background','white');
      $('button[ data-event="codeview"]').remove();
      $('.note-editor').css('border','1px solid #CBD5DD');
      $('div.note-insert').remove();
      $('div.note-table').remove();
      $('div.note-help').remove();
      $('div.note-style').remove();
      $('div.note-color').remove();
      $('button[ data-event="removeFormat"]').remove();
      $('button[ data-event="insertUnorderedList"]').remove();
      $('button[ data-event="fullscreen"]').remove();
      $('button[ data-original-title="Line Height"]').remove();
      $('button[ data-original-title="Font Family"]').remove();
      $('button[ data-original-title="Paragraph"]').remove();
		
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	  if(!$("#inlineRadio2").val())
		{
		$("#inlineRadio2").parent('div').remove();
		}
		if(!$("#inlineRadio3").val())
		{
			$("#inlineRadio3").parent('div').remove();
		}
	  
	  $("#btnSubmitStep").click(function (event) { 
  		$("#actualResultError").addClass('hidden');
  		$("#expectedResultError").addClass('hidden');
  		var testStep = [], action = [], result = [];
  		var status=1;
  		$('.summernote').each( function() {
				$(this).val($(this).code());
				});
  		$('#editable tbody tr.stepRow').each(function(row, tr) {
  			$(tr).find('td.stepVal').find("label.error").addClass("hidden");
  			$(tr).find('td.expResult').find("label.error").addClass("hidden");
  			$(tr).find('td.stepVal').find('.note-editor').css("border","");
  			$(tr).find('td.expResult').find('.note-editor').css("border","");
  			$(tr).find('td.stepVal').find(".note-editor").removeClass("errorBorder");
  			$(tr).find('td.expResult').find(".note-editor").removeClass("errorBorder");
  			if(!($(tr).find('td.stepVal').find('.summernote').code()).localeCompare('<p><br></p>')==0  &&  
  					!($(tr).find('td.expResult').find('.summernote').code()).localeCompare('<p><br></p>')==0)
  			{
	  			testStep.push($(tr).find('td:eq(0)').text());
	  			action.push($(tr).find('td.stepVal').find('.summernote').val());
	  			result.push($(tr).find('td.expResult').find('.summernote').val()); 
  			}
  			else
  			{
  				status=0;
  				if(($(tr).find('td.stepVal').find('.summernote').code()).localeCompare('<p><br></p>')==0)
  				{
  					$(tr).find('td.stepVal').find("label.error").removeClass("hidden");
  					$(tr).find('td.stepVal').find('.note-editor').addClass('errorBorder');
  				}
  				if(($(tr).find('td.expResult').find('.summernote').code()).localeCompare('<p><br></p>')==0)
  				{
  					$(tr).find('td.expResult').find("label.error").removeClass("hidden");
  					$(tr).find('td.expResult').find('.note-editor').addClass('errorBorder');
  				}
  			}
  		});
  		
  		if(status == 1){
  			$(this).off('click');
  			$('body').addClass("white-bg");
  			$("#barInMenu").removeClass("hidden");
  			$("#wrapper").addClass("hidden");	
  		    var posting = $.post('insertstep', {
  			testcaseId : $("#testcaseId").val(),
  			testcaseName : $("#testcaseName").val(),
  			testStep : testStep,
  			action : action,
  			result : result
  		});
  		posting.done(function(data) {
  				<%session.setAttribute("TcStepCreateStatus", 1);%>
  			window.location.href = "teststeps";
  		});
  		}
    });
	  var state = $('#hiddenInput').val();
		var name = $('#newTcName').val();
		if(state == 1){
			var message =" Test case is successfully created";
			var toasterMessage = name +  message;
			showToster(toasterMessage);
		}
		else if(state == 2){
			var message =" Test case is successfully Updated";
			var toasterMessage = name +  message;
			showToster(toasterMessage);
		}
		$('#hiddenInput').val(3);
		var stepState = $('#TcStepCreateStatus').val();
				if(stepState == 1){
					var message ="Steps are successfully created";
					var toasterMessage =   message;
					showToster(toasterMessage);
				}
				else if(stepState == 2){
					var message ="Step is successfully Updated";
					var toasterMessage =   message;
					showToster(toasterMessage);
				}
		 
				$('#TcStepCreateStatus').val(3);
		
	});
	  function showToster(toasterMessage){
			 toastr.options = {
					 "closeButton": true,
					  "debug": true,
					  "progressBar": true,
					  "preventDuplicates": true,
					  "positionClass": "toast-top-right",
					  "showDuration": "400",
					  "hideDuration": "1000",
					  "timeOut": "9000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "slideDown",
					  "hideMethod": "slideUp"
	          };
	          toastr.success('',toasterMessage);
		}  
</script>
<script>
var firstRowNo;

$('#commentForm').validate({
	rules : {
		commentDescription : {
	        noSpace: true,
			required : true,
			minlength : 1,
			maxlength : 4000
		}
	}
});
	$(function() {
		if($("#tblTestStepList tbody tr.tblStepRow").length == 0)
		{
		firstRowNo = parseInt(1);
		}
	else
		{
		
		firstRowNo = parseInt($("#tblTestStepList tbody tr.tblStepRow:last td:eq(1)")
				.html())
				+ parseInt(1);
		}	
		if (!firstRowNo) {
			$("#editable tbody tr:first td:first").html(parseInt(1));
		} else {
			$("#editable tbody tr:first td:first").html(firstRowNo)
		}
		var table = $('.dataTables-example').DataTable({
			dom : 'Bfrtip',
 			buttons : [
//  				'copyHtml5',
//  	            'excelHtml5',
//  	            'csvHtml5',	
//  	            'pdfHtml5'
 	            ],
			"paging" : true,
			"lengthChange" : false,
			"searching" : true,
			"ordering" : true,
		    "pageLength": 10,
		});
		
		var scenarioList=[];
 		scenarioList=${Model.scenarioList};
 		var moduleId = $("#activeModule").val();
 		var moduleName = $("#activeModuleName").val();
 			for(var i=0;i<scenarioList.length;i++)
 				{
 				if(moduleId == scenarioList[i].module_id){
 				scenarioMenu = "<ul class='nav nav-second-level collapse-in'><li id='"+scenarioList[i].scenario_id+"' class='scenarioLinks'>"+
 				                "<a href='#' class='lgMenuTextAlignment menuAnchor menuTooltip' data-original-title='"+scenarioList[i].scenario_name+"' data-container='body' data-toggle='tooltip' data-placement='right' onclick='getTestCaseList("+scenarioList[i].scenario_id+",\""+scenarioList[i].scenario_name+"\","+moduleId+",\""+moduleName+"\")'><span>"+scenarioList[i].scenario_name+"</span></a></li></ul>";
 				  $("#module_"+moduleId).append(scenarioMenu);
 				  $("#module_"+moduleId).addClass("active");
 				}
 				}
 		
 			//tooltip
 			$('[data-toggle="tooltip"]').tooltip();
	});
	$("#btnEdit").click(function() {
		$('#fieldset').removeAttr("disabled");
		$("#txtAreaSummary").find('button').removeAttr("disabled");
		$("#txtAreaCondition").find('button').removeAttr("disabled");
		$('.note-editable').attr('contenteditable','true');
		$('.note-editor').css('background','white'); 
		$('#btnUpdate').show();
		$('#btnCancel').removeClass('hidden');
		$(this).hide();
	});
	
	function fnClickAddRow() {
		
		var SrNo;
		var rows = parseInt($("#editable tbody tr.stepRow").length);
		
		 if($("#editable tbody tr:first td:first").hasClass('dataTables_empty')) {
			 srNo = parseInt(1);
		 }
		 else if (rows>0) {
			srNo = parseInt(rows)+firstRowNo;
			
		} else {
			srNo = firstRowNo;
		}
		  $.fn.dataTable.ext.errMode = 'none';
		var table = $('#editable').dataTable({
			 "bPaginate": false,
			 "searching": false,
			 "ordering": false
		});
		var newRow = $('#editable')
				.dataTable()
				.fnAddData(
						[
								srNo,
								"<textarea class='summernote'></textarea>  <label class='error hidden'>This field is required</label>",
								"<textarea class='summernote'></textarea><label class='error hidden'>This field is required</label>",
								"<button class='btn btn-xs btn-default btn-circle pull-right' name='removeRow' type='button' ><i class='fa fa-times'></i></button>" ]);
		$('.summernote').summernote();
		$('.note-editor').css('background','white');
        $('button[ data-event="codeview"]').remove();
        $('.note-editor').css('border','1px solid #CBD5DD');
        $('.note-editor').css('margin-left','10px');
        
        $('div.note-insert').remove();
        $('div.note-table').remove();
        $('div.note-help').remove();
        $('div.note-style').remove();
        $('div.note-color').remove();
        $('button[ data-event="removeFormat"]').remove();
        $('button[ data-event="insertUnorderedList"]').remove();
        $('button[ data-event="fullscreen"]').remove();
        $('button[ data-original-title="Line Height"]').remove();
        $('button[ data-original-title="Font Family"]').remove();
        $('button[ data-original-title="Paragraph"]').remove();

		var oSettings = table.fnSettings();
		var nTr = oSettings.aoData[newRow[0]].nTr;
	 	$('td', nTr)[1].setAttribute('class', 'stepVal');
		$('td', nTr)[2].setAttribute('class', 'expResult'); 
		$(nTr)[0].setAttribute('class', 'stepRow');
		var counter = firstRowNo;
		$('#editable tbody tr.stepRow').each(function(row, tr) {
			if(!$(tr).children('td:eq(0)').hasClass('dataTables_empty')){
						$(tr).children('td:eq(0)').text(counter);
						$(tr).addClass('stepRow');
						$(tr).children('td:eq(1)').addClass('stepVal');
						$(tr).children('td:eq(2)').addClass('expResult');
						counter++;
			}
		});

		$("button[name='removeRow']").click(function() {
			var $row = $(this).closest("tr");
			var table = $('#editable').dataTable();
			table.dataTable().fnDeleteRow($row.get(0));
			var counter = firstRowNo;
			$('#editable tbody tr.stepRow').each(function(row, tr) {
				if(!$(tr).find('td:eq(0)').hasClass('dataTables_empty')){
					$(tr).find('td:eq(0)').text(counter);
					counter++;
				}
				
			});
		});
		
	}

	$("#btnCancel").click(function() {
	/* 	$("#editTestCaseForm").css({
			display : "none"
		}); */
	
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");
		location.reload();
	});
// 	$("#btnAddStep").click(function() {
// 		$("#createStepDiv").removeClass("hidden");
// 		$("#txtComment").attr("autofocus",true);
// 		 $('html, body').animate({
// 		        scrollTop: $("#createStepDiv").offset().top
// 		    }, 1000);
// 		$(this).hide();
// 	});
 $('#btnAddStep').click(function() {
	document.getElementById("homeMenu").classList.remove('active');
	document.getElementById("commentMenu").classList.remove('active');
	document.getElementById("btnAddStep").classList.add('active');
	  $('#testCaseSteps').css('display','none');
	   $('#addCommentDiv').css('display','none');
	   $('#createStepDiv').css('display','block');
	   
	});
 $('#commentMenu').click(function() {
		document.getElementById("homeMenu").classList.remove('active');
		//document.getElementById("btnAddStep").classList.remove('active');
		document.getElementById("commentMenu").classList.add('active');
		  $('#testCaseSteps').css('display','none');
		   $('#createStepDiv').css('display','none');
		   $('#addCommentDiv').css('display','block');
		});
 $('#homeMenu').click(function() {
		//document.getElementById("btnAddStep").classList.remove('active');
		document.getElementById("commentMenu").classList.remove('active');
		 document.getElementById("homeMenu").classList.add('active');
		  $('#createStepDiv').css('display','block');
		   $('#addCommentDiv').css('display','none');
		   $('#testCaseSteps').css('display','block');
		});
	$("#btnAddComment").click(function() {
		$("#addCommentDiv").removeClass("hidden");
		$("#commentError").addClass("hidden");
		$('#commentDiv').find('.note-editor').css("border","");
		 $('html, body').animate({
		        scrollTop: $("#addCommentDiv").offset().top
		    }, 1000);
		$(this).hide();
	});
	
	$("#btnCancelComment").click(function() {
		/* $("#addCommentDiv").addClass("hidden");
		$('#commentDiv').find('.note-editor').removeClass('errorBorder');
		$("#btnAddComment").show(); */
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");
		location.reload();
	});
	$("#btnSubmitComment").click(function() {
		$("#commentError").addClass("hidden");
		$('#commentDiv').find('.note-editor').removeClass('errorBorder');
		var commentFormFlag=false;
		if(!($('#commentDiv').find('.summernote').code()).localeCompare('<p><br></p>')==0  &&  
				!($('#commentDiv').find('.summernote').code()).localeCompare('')==0 )
		{
			if($("#commentForm").valid())
				commentFormFlag=true;
		}
		else
		{
			commentFormFlag=false;
			$("#commentError").removeClass("hidden");
			$('#commentDiv').find('.note-editor').addClass('errorBorder');
		}
		if(commentFormFlag)
		{
			$(this).attr("disabled","true");
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
			$("#commentForm").submit();
			$("#addCommentDiv").removeClass("hidden");
		}
		
	});
	
// 	function fnCancel() {
// 		var table = $('#editable').dataTable();
// 		$("#createStepDiv").addClass("hidden")
// 		  table.fnClearTable()
// 		$("#btnAddStep").show();
// 		$('#editable tbody tr').each(function(row, tr) {
// 			table.dataTable().fnDeleteRow(row);
// 		});
// 		fnClickAddRow();
// 		$("#actualResultError").addClass('hidden');
// 		$("#expectedResultError").addClass('hidden');
// 	}
	function fnCancel() {
		document.getElementById("btnAddStep").classList.remove('active');
		document.getElementById("commentMenu").classList.remove('active');
		 document.getElementById("homeMenu").classList.add('active');
		  $('#createStepDiv').css('display','none');
		   $('#addCommentDiv').css('display','none');
		   $('#testCaseSteps').css('display','block');
	}
	$("#removeRow").click(function() {
		var $row = $(this).closest("tr");
		var table = $('#editable').dataTable();
		table.dataTable().fnDeleteRow($row.get(0));
		var counter = firstRowNo;
		$('#editable tbody tr.stepRow').each(function(row, tr) {
			if(!$(tr).find('td:eq(0)').hasClass('dataTables_empty')){
			$(tr).find('td:eq(0)').text(counter);
			counter++;
			}

		});
	});
	
	$("#btnUpdateStep").click(function() {
		var formValid=false;
		$('#updateStepActionDivGrp').find("label.error").addClass("hidden");
		  $('#updateStepResultDivGrp').find("label.error").addClass("hidden");
		  $('#updateStepActionDiv').find('.note-editor').removeClass("errorBorder");
		  $('#updateStepResultDiv').find('.note-editor').removeClass("errorBorder");
		  
			if(!($('#updateStepActionDiv').find('.summernote').code()).localeCompare('<p><br></p>')==0  &&  
					!($('#updateStepActionDiv').find('.summernote').code()).localeCompare('')==0  &&  
					!($('#updateStepResultDiv').find('.summernote').code()).localeCompare('<p><br></p>')==0  &&
					!($('#updateStepResultDiv').find('.summernote').code()).localeCompare('')==0)
			{
				formValid=true;
			}
			else
			{
				formValid=false;
				if(($('#updateStepActionDiv').find('.summernote').code()).localeCompare('<p><br></p>')==0  ||  ($('#updateStepActionDiv').find('.summernote').code()).localeCompare('')==0)
				{
					$('#updateStepActionDivGrp').find("label.error").removeClass("hidden");
					$('#updateStepActionDiv').find('.note-editor').addClass('errorBorder');;
				}
				if(($('#updateStepResultDiv').find('.summernote').code()).localeCompare('<p><br></p>')==0   ||  ($('#updateStepResultDiv').find('.summernote').code()).localeCompare('')==0)
				{
					$('#updateStepResultDivGrp').find("label.error").removeClass("hidden");
					$('#updateStepResultDiv').find('.note-editor').addClass('errorBorder');;
				}
			}
	if(formValid){
		  $(this).attr("disabled","true");
		  $('body').addClass("white-bg");
		  $("#barInMenu").removeClass("hidden");
		  $("#wrapper").addClass("hidden");	
		  $('textarea[name=updateStepAction]').val($('#updateStepActionDiv').find('.summernote').code());
		  $('textarea[name=updateStepResult]').val($('#updateStepResultDiv').find('.summernote').code());
		  
		  $('#updateStepForm').submit();
			$('#updateStep').modal('hide'); 
		
		}
	else
		return false;
	}); 
	$("#btnUpdate").click(function() {
		if($('#editTestcaseform').valid()){
				if($.trim($('#datasheetLabel').val()) !='' && ($('#datasheetStatus2').is(':checked') || $('#inlineRadio1').is(':checked'))){
					swal({
						title : "Are you sure?",
						text : "This Operation will remove your Uploaded datasheet file "+$('#datasheetLabel').val()+"\n and update information",
						type : "warning",
						showCancelButton : true,
						confirmButtonColor : "#DD6B55",
						confirmButtonText : "Yes, Remove it!",
						cancelButtonText : "No, cancel!",
						closeOnConfirm : true,
						closeOnCancel : true
					}, function() {
						
						$('body').addClass("white-bg");
						$("#barInMenu").removeClass("hidden");
						$("#wrapper").addClass("hidden");	
						
						$(this).attr("disabled","true");
						  
						  $('body').addClass("white-bg");
						  $("#barInMenu").removeClass("hidden");
						  $("#wrapper").addClass("hidden");	
						   
						  $('.tcSummernote').each( function() {
							  $(this).val($(this).code());
						  });
						  
						$('#editTestcaseform').submit();
					});
				}else{
					$(this).attr("disabled","true");
					  
					  $('body').addClass("white-bg");
					  $("#barInMenu").removeClass("hidden");
					  $("#wrapper").addClass("hidden");	
					   
					  $('.tcSummernote').each( function() {
						  $(this).val($(this).code());
					  });
					  
					$('#editTestcaseform').submit();
				}
			}
		}); 
	$("#editTestcaseform").validate({
		rules : {
			testcaseName : {
				required : true,
				minlength : 1,
				maxlength : 100
			},
			executionTime :{
				required : true,
				number : true
			}
		}
	});
	$(".editSteps").click(function() {
		
		var stepId =  $(this).closest( "tr").children('td').first().html();
	 	var testStepId =  $(this).closest( "tr").children('td:eq(1)').html();
	 	var stepAction =  $(this).closest('tr').children('td:eq(2)').html();
	 	var testResult =  $(this).closest('tr').children('td:eq(3)').html();
	   	 $("textarea[name='updateStepAction']").code(stepAction);
			$("textarea[name='updateStepResult']").code(testResult);
			$("input[name='updateStepId']").val(stepId);
			$("input[name='updateTestStepId']").val(testStepId);
			$("#updateStepAction-error").addClass('hidden');
			$("#updateStepResult-error").addClass('hidden');
			$("textarea[name='updateStepAction']").removeClass('error');
			$("textarea[name='updateStepResult']").removeClass('error');
			$('#updateStep').modal('show'); 
	});
	
	function changeLikeStatus(commentId)
	{
		if($("#pLikeCount_"+commentId).text().length==0)
			$("#pLikeCount_"+commentId).text("0");
		if($("#like_"+commentId).hasClass("btn-info"))
		{
			$("#like_"+commentId).addClass("btn-white");
			$("#like_"+commentId).removeClass("btn-info");
		}
		else if($("#like_"+commentId).hasClass("btn-white"))
		{
			$("#like_"+commentId).addClass("btn-info");
			$("#like_"+commentId).removeClass("btn-white");
		}
		
		if($("#like_"+commentId).hasClass("btn-info"))
		{
			var posting = $.post('inserttccommentlike',{
				commentId : commentId
			});
			
			$("#pLikeCount_"+commentId).text(parseInt($("#pLikeCount_"+commentId).text())+1);
		}
		else if($("#like_"+commentId).hasClass("btn-white"))
		{
			var posting = $.post('removetccommentlike',{
				commentId : commentId
			});
			$("#pLikeCount_"+commentId).text(parseInt($("#pLikeCount_"+commentId).text())-1);
		}
	}
	 function getScenarios(moduleId,moduleName){
         sessionStorage.setItem('selectedTestModuleId',moduleId);
         sessionStorage.setItem('selectedTestModuleName',moduleName);
          $(".moduleLinks").find('ul').remove();
            $(".moduleLinks").append("<ul class='nav nav-second-level collapse-in'></ul>");
            scenarioList=${Model.scenarioList};
            for(var i=0;i<scenarioList.length;i++)
                {
                if(moduleId==scenarioList[i].module_id){
                var tbody = "<tr style='cursor: pointer'><td style='display: none'>"+scenarioList[i].scenario_id+"</td><td class='textAlignment' data-original-title='"+scenarioList[i].scenario_name+"' data-container='body' data-toggle='tooltip' data-placement='right' class='issue-info'>"+scenarioList[i].scenario_name+"</td><td class='hidden'>"+scenarioList[i].scenario_description+"</td><td class='hidden'>"+"https://jadeglobaldemo.atlassian.net/browse/"+scenarioList[i].jira_story_id+"</td><td class='hidden'>"+scenarioList[i].jira_story_id+" "+scenarioList[i].jira_story_name+"</td><td class='text-right'>"+
                 "<div class='btn-group'><button class='btn-white btn btn-xs' onclick='viewScenario("+scenarioList[i].scenario_id+",\"View\")' name='scenario_"+scenarioList[i].scenario_id+"'>View</button>"+
                 "<button class='btn-white btn btn-xs' onclick='removeScenario("+scenarioList[i].scenario_id+")' name='scenario_"+scenarioList[i].scenario_id+"'>Delete</button>"+
                "<button class='btn-white btn btn-xs' onclick='viewScenario("+scenarioList[i].scenario_id+",\"Edit\")' name='scenario_"+scenarioList[i].scenario_id+"'>Edit</button> </div></td></tr>";
                $("#scenarioBody").append(tbody);
                scenarioMenu = "<li id='"+scenarioList[i].scenario_id+"' class='scenarioLinks'>"+
                                "<a class='lgMenuTextAlignment menuAnchor menuTooltip' href='#' data-original-title='"+scenarioList[i].scenario_name+"' data-container='body' data-toggle='tooltip' data-placement='right' onclick='getTestCaseList("+scenarioList[i].scenario_id+",\""+scenarioList[i].scenario_name+"\","+moduleId+",\""+moduleName+"\")'>"+scenarioList[i].scenario_name+"</a></li>";
                  $("#module_"+moduleId).find('ul').append(scenarioMenu);
                  $("#module_"+moduleId).addClass("active");
                 
                  $('[data-toggle="tooltip"]').tooltip();
                    //tooltip
                    if(!$('.menuSpan').hasClass('menuTextAlignment')) {
                        //turn on the tooltips
                        $('.menuTooltip').tooltip('disable');
                    }
                }
                }
     }
	 
	 $('#uploadDatasheet').bind('change', function(e) {
		  $("#sizeError").addClass('hidden');
		  $("#fileError").addClass('hidden');
		if(this.files[0]){
		  var fileSize=this.files[0].size;
		  if(fileSize>1048576)
			{
			  $("#sizeError").removeClass('hidden');
			  $(this).val(null);
			}
		}
		});
	 
</script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/codemirror/mode/filestyle/bootstrap-filestyle.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/codemirror/mode/filestyle/bootstrap-filestyle.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/sweetalert/sweetalert.min.js"></script>
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/sweetalert/sweetalert.css"
	rel="stylesheet">
<%
	//set session variable to another value
	session.setAttribute("TcCreateStatus", 3);
	session.setAttribute("TcStepCreateStatus", 3);
%>


<!-- Modal -->
<!-- <div id="testStepModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    Modal content
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Test Step</h4>
      </div>
      <div class="modal-body">
      
       <div id="createStepDiv">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					
					<div class="ibox-content">
					
						<table class="table table-striped table-bordered table-hover"
							id="editable">
							<thead>
								<tr>
									<th width="10%">Step No</th>
									<th>Steps</th>
									<th>Expected Result</th>
									<th width="5%"></th>

								</tr>
							</thead>
							<tbody>
								<tr class="gradeX stepRow">
									<td>0</td>
									<td class="stepVal"><textarea class="summernote"></textarea>
										<label class="error hidden">This field is required</label></td>
									<td class="expResult"><textarea class="summernote"></textarea>
										<label class='error hidden'>This field is required</label></td>
									<td><button
											class="btn btn-xs btn-default btn-circle pull-right"
											type="button" id="removeRow">
											<i class="fa fa-times"></i>
										</button></td>

								</tr>
							</tbody>
						</table>
						<label id="actualResultError" class="error hidden"
							for="actualResult"> * The field Step is required</label> <label
							id="expectedResultError" class="error hidden"
							for="ExpectedResult">* The field Expected Result is
							required</label>
						
					</div>
				</div>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" ladda-button ladda-demo"
								id="btnSubmitStep" data-style="slide-up">Submit </button>
      </div>
    </div>

  </div>
</div> -->
<style>
.md-skin .ibox{
margin-bottom:10px;
}
#tblTestStepList_wrapper .dt-buttons{
display:none;
} 
#testCaseSteps .ibox-content{
padding:10px ;
}
</style>
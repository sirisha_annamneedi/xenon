<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="bar" id="barInMenu">
  <p>loading</p>
</div>

<div id="wrapper" class="hidden">
	<nav class="navbar-default navbar-static-side " role="navigation" > <!-- style="min-height: 100%; background-color: transparent;" -->
		<div class="sidebar-collapse">
			<ul class="nav metismenu" id="side-menu">
				<%-- <li class="nav-header">	
					<div class="dropdown profile-element">
						<span> <img alt="image" class="img-circle"
							style="width: 48px; height: 48px"
							src="data:image/jpg;base64,${Model.UserProfilePhoto }" />
						</span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
							class="clear"> <span class="block m-t-xs"> <strong
									class="font-bold">${Model.userName }</strong>
							</span>
						</span>
						</a> <span class="text-muted text-xs block">${Model.role}<b
							class=""></b></span>
					</div>
					<div class="logo-element" style="padding: 14px !important">
						<a href="#" onclick="gotoLinkInMenu('xedashboard')">
						<img alt="image" class=""
							src="<%=request.getContextPath()%>/resources/img/xenon.png" />
						</a>
					</div>
				</li> --%>
				<li class="nav-header">
                    <div class="dropdown profile-element text-center">
                        <a href="#" onclick="gotoLink('xedashboard')" >
                        <img src="<%=request.getContextPath()%>/resources/img/Jade-Logo.png" class="xenon-logo">
                        </a>
                    <!--    <img class="img-circle" style="width:48px; height:48px" src="resources/img/user.jpg">
                         <img alt="image" class="img-circle"
                            style="width: 48px; height: 48px"
                            src="data:image/jpg;base64,${Model.UserProfilePhoto }" />
                        </span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
                            class="clear"> <span class="block m-t-xs"> <strong
                                    class="font-bold">${Model.userName }</strong>
                            </span>
                        </span>
                        </a> <span class="text-muted text-xs block">${Model.role}<b
                            class=""></b></span>  -->
                    </div>
                    <div class="logo-element" style="padding: 14px !important">
                        <a href="#" onclick="gotoLinkInMenu('xedashboard')">
                        <img alt="image" class=""
                            src="<%=request.getContextPath()%>/resources/img/xenon.png" />
                        </a>
                    </div>
                </li>
				
				<li id="id-crud-btns-li">
					<div class="class-crud-btns-div">
						<ul>
			                <li id="id-add-module-li">
<!-- 			                	<a href="#" data-toggle="tooltip" title="" data-original-title="Add Module"><i class="fa fa-plus"></i></a> -->
			                	<button id="id-add-module-btn" type="button" class="btn btn-success btn-xs" 
			                			data-toggle="modal" data-target="#addModuleModal">
			                		<i class="fa fa-plus" data-toggle="tooltip" title="" data-original-title="Add Module" data-placement="bottom"></i>
		                			<span>Add Module</span>
			                	</button>
			                </li>
			                <li id="id-add-scenario-li">
<!-- 			                	<a href="#" data-toggle="tooltip" title="" data-original-title="Add Scenario"><i class="fa fa-plus"></i></a> -->
			                	<button id="id-add-scenario-btn" type="button" disabled class="btn btn-success btn-xs" 
			                			data-toggle="modal" data-target="#addScenarioModal">
		                			<i class="fa fa-plus" data-toggle="tooltip" title="" data-original-title="Add Scenario" data-placement="bottom"></i>
	                				<span>Add Scenario</span>
			                	</button>
			                </li>
			                <!-- <li id="id-update-module-li" data-toggle="modal" data-target="#updateModuleModal" class="disabled">
			                	<a href="#" data-toggle="tooltip" title="" data-original-title="Edit Module"><i class="fa fa-edit"></i></a>
			                </li> -->
			                <!-- <li id="id-update-scenario-li" data-toggle="modal" data-target="#updateScenarioModal" style="display: none;">
			                	<a href="#" data-toggle="tooltip" title="" data-original-title="Edit Scenario"><i class="fa fa-edit"></i></a>
			                </li> -->
		                </ul>
                	</div>
				</li>
				
				<c:if test="${fn:length(Model.moduleList) == 0}">
					<li><a href=""> <span>No Modules Found</span>
					</a></li>
				</c:if>
				
				<%-- <li class="mainLi nav">
					<span class="navbar-brand">${UserCurrentProjectName }</span>
				</li> --%>
						
				<c:forEach var="module" items="${Model.moduleList}">
					<li class="mainLi class-modules-li" id="id-module-li-${module.module_id }" 
						data-module-name='${module.module_name}'  data-module-id='${module.module_id}' data-module-status="${module.module_status}">
							<a href="#" onclick="getScenarios(${module.module_id},'${module.module_name}')"><i class="fa fa-th-large"></i>
								<span class="nav-label">${module.module_name}</span>
								<span class="fa arrow"></span>
							</a>
						<ul class="nav nav-second-level collapse">	
							<li id="module_${module.module_id}" class="moduleLinks">
								<a  data-original-title="${module.module_name}"  class="customeTooltip menuTooltip" href="#" onclick="getScenarios(${module.module_id},'${module.module_name}')">
						 		</a>
						 	</li>
						</ul>
					</li>
				</c:forEach>
				
			</ul>
			

		</div>
	</nav>
<div class="modal fade" id="addScenarioModal" tabindex="-1" role="dialog" aria-labelledby="addScenarioModalLabel" aria-hidden="false" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="addScenarioModalLabel">Add Scenario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="fasle">x</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="insertscenario" method="post" id="id-scenario-add-form">
       <!-- {"Form data":{"moduleId":"6","moduleName":"TestModule2","scenarioName":"Scenario4","scenarioDescription":"","status":"1"}} -->
       <input type="text" id="id-add-scenario-name-text" class="form-control" name="scenarioName" placeholder="Add Scenario" required="required" pattern="^[^ ].+[^ ]$"
       			oninvalid="setCustomValidity('Please do not enter leading and/or trailing spaces')" oninput="setCustomValidity('')">
       <input type="hidden" id="id-add-scenario-module-id-hidden" name='moduleId'>
       <input type="hidden" id="id-add-scenario-module-name-hidden" name='moduleName'>
       <input type="hidden" id="id-add-scenario-scenario-description-hidden" name='scenarioDescription'>
       <input type="hidden" id="id-add-scenario-scenario-status-hidden" name='status' value="1">
       <input type="hidden" id="id-add-scenario-module-source-hidden" name='source' value="LeftNavMenu">
       <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
<!--         <input type="submit" class="btn btn-primary" >Add Module<> -->
        <input type="submit" value="Add" class="btn btn-success">
      </div>
       </form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success">Add Scenario</button>
      </div> -->
    </div>
  </div>
</div>

<div class="modal fade" id="updateScenarioModal" tabindex="-1" role="dialog" aria-labelledby="updateScenarioModalLabel" aria-hidden="false" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="updateScenarioModalLabel">Update Scenario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="fasle">x</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="updatescenario" method="post" id="id-scenario-update-form">
       <input type="text" id="id-update-scenario-name-text" class="form-control" name="scenarioName" required="required" pattern="^[^ ].+[^ ]$"
       			oninvalid="setCustomValidity('Please do not enter leading and/or trailing spaces')" oninput="setCustomValidity('')">
       <input type="hidden" id="id-update-scenario-scenario-id-hidden" name='scenarioId'>
<%--        <input type="hidden" id="id-update-scenario-project-id-hidden" name='projectId' value="${UserCurrentProjectId }"> --%>
<!--        <input type="hidden" id="id-update-scenario-scenario-status-hidden" name='scenarioStatus' value="1"> -->
       <input type="hidden" id="id-update-scenario-scenario-description-hidden" name='scenarioDescription' value=" ">
       <input type="hidden" id="id-update-scenario-scenario-module-name-hidden" name='moduleName' value=" ">
       <input type="hidden" id="id-update-scenario-scenario-source-hidden" name='source' value="LeftNavMenu">
       <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
<!--         <input type="submit" class="btn btn-success" >Add Scenario<> -->
        <input type="submit" value="Update" class="btn btn-success">
      </div>
       </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="deleteScenarioModal" tabindex="-1" role="dialog" aria-labelledby="deleteScenarioModalLabel" aria-hidden="false" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="deleteScenarioModalLabel">Delete Scenario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="fasle">x</span>
        </button>
      </div>
      <div class="modal-body">
       <div id="id-scenario-delete-confirm-div" class="alert alert-danger"></div>
       <form action="deletescenario" method="post" id="id-scenario-delete-form">
       <input type="hidden" id="id-delete-scenario-scenario-id-hidden" name='scenarioId'>
<%--        <input type="hidden" id="id-delete-scenario-project-id-hidden" name='projectId' value="${UserCurrentProjectId }"> --%>
       <input type="hidden" id="id-delete-scenario-scenario-status-hidden" name='scenarioStatus' value="2">
       <input type="hidden" id="id-delete-scenario-scenario-source-hidden" name='source' value="LeftNavMenu">
       <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
<!--         <input type="submit" class="btn btn-success" >Add Scenario<> -->
        <input type="submit" value="Delete" class="btn btn-danger">
      </div>
       </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="addModuleModal" tabindex="-1" role="dialog" aria-labelledby="addModuleModalLabel" aria-hidden="false" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="addModuleModalLabel">Add Module</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="fasle">x</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="insertmodule" method="post" id="id-module-add-form">
       <input type="text" id="id-add-module-name-text" class="form-control" name="moduleName" placeholder="Add Module" required="required" pattern="^[^ ].+[^ ]$"
       			oninvalid="setCustomValidity('Please do not enter leading and/or trailing spaces')" oninput="setCustomValidity('')">
       <input type="hidden" id="id-add-module-project-id-hidden" name='projectId' value="${UserCurrentProjectId }">
       <input type="hidden" id="id-add-module-module-status-hidden" name='moduleStatus' value="1">
       <input type="hidden" id="id-add-module-module-description-hidden" name='moduleDescription' value=" "><!--  intentionally left blank. Dont remove it -->
       <input type="hidden" id="id-add-module-module-source-hidden" name='source' value="LeftNavMenu">
       <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
<!--         <input type="submit" class="btn btn-success" >Add Module<> -->
        <input type="submit" value="Add" class="btn btn-success">
      </div>
       </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="updateModuleModal" tabindex="-1" role="dialog" aria-labelledby="updateModuleModalLabel" aria-hidden="false" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="updateModuleModalLabel">Update Module</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="fasle">x</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="updatemodule" method="post" id="id-module-update-form">
       <input type="text" id="id-update-module-name-text" class="form-control" name="moduleName" required="required" pattern="^[^ ].+[^ ]$"
       			oninvalid="setCustomValidity('Please do not enter leading and/or trailing spaces')" oninput="setCustomValidity('')">
       <input type="hidden" id="id-update-module-module-id-hidden" name='moduleId'>
<%--        <input type="hidden" id="id-update-module-project-id-hidden" name='projectId' value="${UserCurrentProjectId }"> --%>
       <input type="hidden" id="id-update-module-module-status-hidden" name='moduleStatus' value="1">
       <input type="hidden" id="id-update-module-module-description-hidden" name='moduleDescription' value=" ">
       <input type="hidden" id="id-update-module-module-source-hidden" name='source' value="LeftNavMenu">
       <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
<!--         <input type="submit" class="btn btn-success" >Add Module<> -->
        <input type="submit" value="Update" class="btn btn-success">
      </div>
       </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="deleteModuleModal" tabindex="-1" role="dialog" aria-labelledby="deleteModuleModalLabel" aria-hidden="false" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="deleteModuleModalLabel">Delete Module</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="fasle">x</span>
        </button>
      </div>
      <div class="modal-body">
       <div id="id-module-delete-confirm-div" class="alert alert-danger"></div>
       <form action="updateModuleStatus" method="post" id="id-module-delete-form">
       <input type="hidden" id="id-delete-module-module-id-hidden" name='moduleId'>
<%--        <input type="hidden" id="id-delete-module-project-id-hidden" name='projectId' value="${UserCurrentProjectId }"> --%>
       <input type="hidden" id="id-delete-module-module-status-hidden" name='moduleStatus' value="2">
       <input type="hidden" id="id-delete-module-module-source-hidden" name='source' value="LeftNavMenu">
       <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
<!--         <input type="submit" class="btn btn-success" >Add Module<> -->
        <input type="submit" value="Delete" class="btn btn-danger">
      </div>
       </form>
      </div>
    </div>
  </div>
</div>
</div>	

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.ui.position.js"></script>	
<script type="text/javascript">
$(function () {
	/* $("#custLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo")); */
	
	//tooltip
	$('[data-toggle="tooltip"]').tooltip(); 
		 
});

	function gotoLinkInMenu(link)//link
	{
		$('body').addClass('white-bg');
		$("#barInMenu").removeClass('hidden');
		$("#wrapper").addClass("hidden");
		window.location.href = link; 
	}

</script>

<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	$('.class-modules-li').click( function(){
		if( $(this).hasClass('active') ) {
			$('#id-add-scenario-btn').prop('disabled', false);
		}
		else {
			$('#id-add-scenario-btn').prop('disabled', true);
		}
	});
	
	$('#id-add-scenario-btn').click( function( e ){
		if( $(this).hasClass('disabled') )
			return false;
		var $selectedModule = $('.class-modules-li.active');
		
// 		$('#addScenarioModalLabel').html("Add Scenario Under " + $selectedModule.data('module-name') );
		$('#id-add-scenario-module-id-hidden').val( $selectedModule.data('module-id') );
		$('#id-add-scenario-module-name-hidden').val( $selectedModule.data('module-name') );
	});
	
	/* $('#id-update-module-li').click( function( e ){
		if( $(this).hasClass('disabled') )
			return false;
		var isPropogate = false;
		$('.class-modules-li').each( function(){
			if( $(this).hasClass('active') ) {
				$('#id-update-module-name-text').val( $(this).data('module-name') );
				$('#id-update-module-module-id-hidden').val( $(this).data('module-id') );
				isPropogate = true; 
		        return false;
			}
		});
		if( !isPropogate ) {
			e.stopPropogation();
			alert("Select Module First");
		}
	}); */
	$('#id-module-li-'+'${moduleId} a').trigger('click');
// 	$('#module_${scenarioDetails[0].scenario_id}').trigger('click', function(e){e.preventDefault();});

	$.contextMenu({
        selector: '.class-modules-li', 
        callback: function(key, options) {
            var m = "clicked: " + key;
//             window.console && console.log(m) || alert(m); 
            switch(key) {
		        // A case for each action. Your actions here
		        case "edit": 
		    	    	$('#id-update-module-name-text').val( this.data('module-name') );
		    	    	$('#id-update-module-module-id-hidden').val( this.data('module-id') );
	        			$('#updateModuleModal').modal('show');
		        	break;
		        case "delete": 
		        	$('#id-delete-module-module-id-hidden').val( this.data('module-id') );
		        	$('#id-module-delete-confirm-div').html("Are you sure you want to delete <b>"+ this.data('module-name')+"</b> ?" );
		        	$('#deleteModuleModal').modal('show');
		        	break;
		    }
        },
        items: {
            "edit": {name: "Edit Module", icon: "edit"},
            /* "cut": {name: "Cut", icon: "cut"},
           copy: {name: "Copy", icon: "copy"},
            "paste": {name: "Paste", icon: "paste"}, */
            "delete": {name: "Delete Module", icon: "delete"},
            "sep1": "---------",
            /* "quit": {name: "Quit", icon: function(){
                return 'context-menu-icon context-menu-icon-quit';
            }} */
        }
    });
	$.contextMenu({
        selector: '.scenarioLinks', 
        callback: function(key, options) {
            var m = "clicked: " + key;
//             window.console && console.log(m) || alert(m); 
            switch(key) {
		        // A case for each action. Your actions here
		        case "edit": 
		    	    	$('#id-update-scenario-name-text').val( this.first('a').text() );
		    	    	$('#id-update-scenario-scenario-id-hidden').val( this.prop('id') );
// 		    	    	$('#id-update-scenario-scenario-module-name-hidden').val( this.parent('.class-modules-li').data('module-name') );
	        			$('#updateScenarioModal').modal('show');
		        	break;
		        case "delete":
		        	$('#id-delete-scenario-scenario-id-hidden').val( this.prop('id') );
		        	$('#id-scenario-delete-confirm-div').html("Are you sure you want to delete <b>"+ this.find('a:first').text()+"</b> ?" );
		        	$('#deleteScenarioModal').modal('show');
		        	break;
		    }
        },
        items: {
            "edit": {name: "Edit Scenario", icon: "edit"},
            /* "cut": {name: "Cut", icon: "cut"},
           copy: {name: "Copy", icon: "copy"},
            "paste": {name: "Paste", icon: "paste"}, */
            "delete": {name: "Delete Scenario", icon: "delete"},
            "sep1": "---------",
            /* "quit": {name: "Quit", icon: function(){
                return 'context-menu-icon context-menu-icon-quit';
            }} */
        }
    });
    /* $('.class-modules-li').on('click', function(e){
        console.log('clicked', this);
    })  */
});
</script>

<style>
.btn-primary{
	background-color: #1a7bb9;
	border-color: #1a7bb9;
	color: #FFFFFF;
}
.class-crud-btns-div{
    text-align:center;
    justify-content: center;
    display:flex;
}
.class-crud-btns-div ul{
    display: flex;
    list-style-type: none;
   
    padding-left: 0;
}
.class-crud-btns-div li{
    background:#1c84c6;
/*     padding:8px 12px; */
    margin:15px 5px;
/*     border-radius:50%; */
    
}
.class-crud-btns-div li.disabled{
    background:#ddd;
}
.class-crud-btns-div li a{
    color:#fff;
}
@media screen and (max-width:768px){
    .class-crud-btns-div{
    display:block;}
}
.mini-navbar .class-crud-btns-div{
    display:block;}
.mini-navbar .class-crud-btns-div li{
    width: 38px;
/*     margin:15px; */
}
.class-crud-btns-div button i{
    display: none;
}
.mini-navbar .class-crud-btns-div button i{
    display: block;
}
.mini-navbar .class-crud-btns-div button span{
    display: none;
}
.tooltip.bottom .tooltip-inner {
    background-color: #1c84c6;
}
.tooltip.bottom .tooltip-arrow {
      border-bottom-color: #1c84c6;
}


</style>
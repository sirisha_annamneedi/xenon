<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	
<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Cloud Virtual Machine Settings</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li class="active"><strong>VM Settings</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row hidden" id="createVMDiv">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<h2>Add Virtual Machine</h2>
					<form action="insertcloudvmdetails"
						class="wizard-big wizard clearfix form-horizontal" method="POST"
						id="insertVmDetailsForm">
						<div class="content clearfix">
							<fieldset class="body current">
								<div class="row">
									<label class="col-lg-4 text-right">* fields are mandatory</label>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="col-lg-12">
										
											<div class="alert alert-danger hidden" id="errorDiv">Virtual machine name already
												exists, Please try another.</div>
										
											<div class="form-group">
												<label class="col-sm-2 control-label">Virtual Machine Name * :</label>
												<div class="col-sm-10">
													<input type="text" class="form-control characters"
														placeholder="Virtual Machine Name" name="hostname"
														tabindex="1">
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-sm-2 control-label">IP Address * :</label>
												<div class="col-sm-10">
													<input type="text" class="form-control .ipv4"
														placeholder="IP Address" name="ipAddress" onchange="validateIp()"
														tabindex="2">
												</div>
												
											</div>
											<label class="error hidden" id="ipError" style="margin-left: 18%;">Please enter valid IP address.</label>
											<div class="form-group">
												<label class="col-sm-2 control-label">Port * :</label>
												<div class="col-sm-10">
													<input type="text" class="form-control"
														placeholder="Port Number" name="portNumber"
														tabindex="2">
												</div>
											</div>
											<div class="form-group">
											<label class="col-sm-2 control-label">Project Repository:</label>
											<div class="col-sm-4">
												<div class="radio radio-success radio-inline col-sm-4">
													<input type="radio" id="gitLoc" value="1"
														name="repositoryStatus" checked="" tabindex="1"> 
													<label for="git">Git</label>
												</div>
												<div class="radio radio-success radio-inline col-sm-4">
													<input type="radio" id="machineLoc" value="2"
														name="repositoryStatus" tabindex="2"> 
													<label for="machine">Machine Location</label>
												</div>
											</div>
											</div>
											<div class="form-group hidden" id="projectLoc">
												<label class="col-sm-2 control-label">Project Location * :</label>
												<div class="col-sm-10">
													<input type="text" class="form-control characters"
														placeholder="Project Location" name="projectLocation"
														tabindex="1">
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-sm-2 control-label">Concurrent Execution Status :</label>
												<div class="col-sm-4">
													<div class="radio radio-success radio-inline col-sm-4">
														<input type="radio" id="inlineRadio11" value="1"
															name="concExecStatus"   onchange="numberExecs()"> <label
															for="inlineRadio11" tabindex="3"> Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-4">
														<input type="radio" id="inlineRadio12" value="2"
															name="concExecStatus" checked="" onchange="numberExecs()"> <label for="inlineRadio12">
															Inactive </label>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group" style="display:none;" id="concurrentExecNumber">
														<label class="col-sm-5 control-label">Number of Concurrent Executions * :</label>
														<div class="col-sm-5">
															<input type="text" class="form-control"
																placeholder="" name="concurrentExecNumber"
																tabindex="2" >
														</div>
													</div>
												</div>
											</div>
											
											
											<div class="form-group">
												<label class="col-sm-2 control-label">IE :</label>
												<div class="col-sm-4">
													<div class="radio radio-success radio-inline col-sm-4">
														<input type="radio" id="inlineRadio1" value="1"
															name="ieStatus" checked=""> <label
															for="inlineRadio1" tabindex="3"> Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-4">
														<input type="radio" id="inlineRadio2" value="2"
															name="ieStatus"> <label for="inlineRadio2">
															Inactive </label>
													</div>
												</div>
											</div>
											
											
											<div class="form-group">
												<label class="col-sm-2 control-label">Chrome :</label>
												<div class="col-sm-4">
													<div class="radio radio-success radio-inline col-sm-4">
														<input type="radio" id="inlineRadio3" value="1"
															name="chromeStatus" checked=""> <label
															for="inlineRadio3" tabindex="3"> Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-4">
														<input type="radio" id="inlineRadio4" value="2"
															name="chromeStatus"> <label for="inlineRadio4">
															Inactive </label>
													</div>
												</div>
											</div>
											
											
											<div class="form-group">
												<label class="col-sm-2 control-label">Firefox :</label>
												<div class="col-sm-4">
													<div class="radio radio-success radio-inline col-sm-4">
														<input type="radio" id="inlineRadio5" value="1"
															name="firefoxStatus" checked=""> <label
															for="inlineRadio5" tabindex="3"> Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-4">
														<input type="radio" id="inlineRadio6" value="2"
															name="firefoxStatus"> <label for="inlineRadio6">
															Inactive </label>
													</div>
												</div>
											</div>
											
											
											<div class="form-group">
												<label class="col-sm-2 control-label">Safari :</label>
												<div class="col-sm-4">
													<div class="radio radio-success radio-inline col-sm-4">
														<input type="radio" id="inlineRadio7" value="1"
															name="safariStatus" checked=""> <label
															for="inlineRadio7" tabindex="3"> Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-4">
														<input type="radio" id="inlineRadio8" value="2"
															name="safariStatus"> <label for="inlineRadio8">
															Inactive </label>
													</div>
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-sm-2 control-label">Virtual Machine Status:</label>
												<div class="col-sm-4">
													<div class="radio radio-success radio-inline col-sm-4">
														<input type="radio" id="inlineRadio9" value="1"
															name="vmStatus" checked=""> <label
															for="inlineRadio9" tabindex="3"> Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-4">
														<input type="radio" id="inlineRadio10" value="2"
															name="vmStatus"> <label for="inlineRadio10">
															Inactive </label>
													</div>
												</div>
											</div>
									</div>
								</div>
							</div>
							</fieldset>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-white pull-left"
										style="margin-right: 15px;" type="button" id="btnCancel"
										type="button" tabindex="6">Cancel</button>
									<button class="btn btn-success pull-left ladda-button" type="button"
										id="createVmbtn" data-style="slide-up" tabindex="7">Submit</button>
								</div>
							</div>
						</div>
					</form>
					<!-- end form -->
				</div>
				<!-- end ibox-content -->

			</div>
			<!-- end ibox -->
		</div>
		<!-- end col -->
	</div>
	<div class="row" id="VMListDiv">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-content">
				<c:if test="${Model.createvm==1}">
					<button class="btn btn-success btn-xs pull-right" type="button"
						id="addVM">Add Virtual Machine</button>
				</c:if>
					<h2>Cloud Virtual Machine Details</h2>

					<div class="clients-list">
						<div class="full-height-scroll">
							<div class="table-responsive">
								<table class="table table-striped table-hover"
									id="tblVMList">
									<thead>
										<tr>
											<th style="display: none">Id</th>
											<th>Host Name</th>
											<th>IP Address</th>
											<th>Port Number</th>
											<th>Project Location</th>
											<th>Browsers</th>
											<th>Available</th>
											<th>Status</th>
											<th>Action</th>
											
										</tr>
									</thead>
									<tbody>
										<c:forEach var="vmList" items="${Model.vmList}">
											<tr>
												<td style="display: none">${vmList.cloude_vm_id}</td>
												<td class="textAlignment" data-original-title="${vmList.hostname}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${vmList.hostname}</td>
												<td class="textAlignment" data-original-title="${vmList.ip_address}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${vmList.ip_address}</td>
												<td>${vmList.port}</td>
												<td class="textAlignment" data-original-title="${vmList.project_location}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${vmList.project_location}</td>
												<td>
												<c:if test="${vmList.ie_status==1}">
												<img src="<%=request.getContextPath()%>/resources/img/browsers/ie.jpg">
												</c:if>
												<c:if test="${vmList.chrome_status==1}">
												<img src="<%=request.getContextPath()%>/resources/img/browsers/Chrome.jpg">
												</c:if>
												<c:if test="${vmList.firefox_status==1}">
												<img src="<%=request.getContextPath()%>/resources/img/browsers/firefox.jpg">
												</c:if>
												<c:if test="${vmList.safari_status==1}">
												<img src="<%=request.getContextPath()%>/resources/img/browsers/safari.jpg">
												</c:if>
												
												<c:if test="${vmList.is_free == 1}">
															<td class="client-status"><span
																class="label label-primary">Yes</span></td>
														</c:if>
														<c:if test="${vmList.is_free == 2}">
															<td class="client-status"><span
																class="label label-danger">No</span></td>
														</c:if>
														
												<c:if test="${vmList.status == 1}">
															<td class="client-status"><span
																class="label label-primary">Active</span></td>
														</c:if>
														<c:if test="${vmList.status == 2}">
															<td class="client-status"><span
																class="label label-danger">Inactive</span></td>
														</c:if>
												<td class="text-right">
													<div class="btn-group">
														<button class="btn-white btn btn-xs"
															onclick="viewVmDetails(${vmList.cloude_vm_id},1)"
															name="${vmList.cloude_vm_id}">View</button>
														<c:if test="${Model.createvm==1}">
														<button class="btn-white btn btn-xs"
															onclick="viewVmDetails(${vmList.cloude_vm_id},2)"
															name="${vmList.cloude_vm_id}">Edit</button></c:if>
													</div>
												</td>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<!-- end row -->
</div>
<!-- end wrapper -->
</div>
<!-- Input Mask-->
    <script src="<%=request.getContextPath()%>/resources/js/mask/jquery.input-ip-address-control-1.0.min.js"></script>
<input type="hidden" id="hiddenInput"
	value=<%=session.getAttribute("vmCreateStatus")%>>

<input type="hidden" id="newVmName"
	value='<%=session.getAttribute("newVmName")%>'>
<input type="hidden" id="duplicateVmNameError"
	value='<%=session.getAttribute("duplicateVmNameError")%>'>
<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});
  $(function () {
	 
	  $("#vertical-timeline").html($("#tblVMList tbody tr:first td:eq(12)").html());
	  if($('#duplicateVmNameError').val()==1)
		{
		  $("#errorDiv").removeClass("hidden");
		  $("#createVMDiv").removeClass('hidden');
		   $(this).addClass('hidden');
			$("input[name=hostname]").focus();
			$("#VMListDiv").fadeOut();
		}
	  else 
		{
		  var state = $('#hiddenInput').val();
			var name = $('#newVmName').val();
			if(state == 1){
				var message =" VM is successfully created";
				var toasterMessage = "'"+name+"'" +  message;
				showToster(toasterMessage);
			}
			
			 $('#hiddenInput').val(3);
		}
	});
	  function showToster(toasterMessage){
			 toastr.options = {
					 "closeButton": true,
					  "debug": true,
					  "progressBar": true,
					  "preventDuplicates": true,
					  "positionClass": "toast-top-right",
					  "showDuration": "400",
					  "hideDuration": "1000",
					  "timeOut": "9000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "slideDown",
					  "hideMethod": "slideUp"
	          };
	          toastr.success('',toasterMessage);
		}  
	
</script>
<script>
$(function() {
 		$('#tblVMList').DataTable({
 			"paging": true,
 	 	    "lengthChange": true,
 	 	    "searching": true,
 	 	    "ordering": true,
 	 	    "info": true,
 	        "autoWidth": false,
 	       "aoColumnDefs" : [ {
				'bSortable' : false,
				'aTargets' : [ 4 ]
			}]
 		});
 		
 		  
 	});

$("#addVM").click(function(){
   $("#createVMDiv").removeClass('hidden');
   $(this).addClass('hidden');
	$("input[name=hostname]").focus();
	$("#VMListDiv").fadeOut();
   
});


 $("#btnCancel").click(function(){
	 /* $("#createVMDiv").addClass('hidden'); 
	 $("#addVM").removeClass('hidden');
	 $("#VMListDiv").fadeIn(); */
	 <%
	 session.setAttribute("duplicateVmNameError",0);
	 %>
	 
	 $("#barInMenu").removeClass("hidden");
	 $("#wrapper").addClass("hidden");
	 location.reload();
 });

 function viewVmDetails(id,viewType)
	{
	 $('body').addClass("white-bg");
	 $("#barInMenu").removeClass("hidden");
	 $("#wrapper").addClass("hidden");
	 
	 var posting = $.post('setselectedcloudvmid', {
			vmId : id,
			viewType : viewType
		});
		 posting.done(function(data) {
			window.location.href="editcloudvmdetails"; 
		}); 
	}

 $("#insertVmDetailsForm").validate({
		rules : {
			hostname : {
				required : true,
				minlength : 1,
				maxlength : 100
			},
			ipAddress : {
				required : true,
				minlength : 1,
				maxlength : 17
			},
			remoteUsername : {
				required : true
			},
			remotePassword : {
				required : true
			},
			projectLocation : {
				required : true,
				minlength : 1,
				maxlength : 1000
			},
			portNumber:{
				required : true,
				minlength : 1,
				maxlength : 5,
				number: true,
				min : 0,
				max : 65535
			},
			concurrentExecNumber :{
				required : function(){
				if($("input[name=concExecStatus]:checked").attr("value")==1)
				{
					return true;
				}
			else
				{
					return false;
				}},
				number : true,
				min : 1,
				max : 20
			}
			
		}
		/* messages: {
			executionTime:{
				required: "Estimated execution time in Minutes."
			} 
		}*/
	});

 
$('#createVmbtn').click(function() {
	validateIp();
		 if($('#insertVmDetailsForm').valid() && validateIp()){
			  $(this).attr("disabled","true");
			  
			  $('body').addClass("white-bg");
			  $("#barInMenu").removeClass("hidden");
			  $("#wrapper").addClass("hidden");
			  
			$('#insertVmDetailsForm').submit();   
		  } 
	});
	
	function validateIp()
	{
		var ipAddr = $("input[name=ipAddress]").val();

		if(ipAddr.isIpv4() && (ipAddr.indexOf("-") == -1)) {
			$("#ipError").addClass("hidden");
		    return true;
		} else {
			$("#ipError").removeClass("hidden");
			$("#ipError").text("Please enter valid IP address.").show();
		    return false;
		}
	}
	
	function numberExecs()
	{
		var value= $("input[name=concExecStatus]:checked").attr("value");
		if(value==1)
			{
				$("#concurrentExecNumber").fadeIn();
			}
		else
			{
				$("#concurrentExecNumber").fadeOut();
			}
	}
	$("#machineLoc").click(function(){
		if($("#machineLoc").is(':checked')){
			$("#projectLoc").removeClass("hidden");
		}
		
	});
	$("#gitLoc").click(function(){
		if($("#gitLoc").is(':checked')){
			$("#projectLoc").addClass("hidden");
		}
		
	})
	 
</script>

<%
	//set session variable to another value
	session.setAttribute("vmCreateStatus", 3);
%>


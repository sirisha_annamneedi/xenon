<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Cloud VM Settings</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li class="active"><strong>VM Settings</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row" id="createVMDiv">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<h2>Edit VM Details</h2>
					<form action="updatecloudvmdetails"
						class="wizard-big wizard clearfix form-horizontal" method="POST"
						id="updateVmDetailsForm">
						<div class="content clearfix">
							<fieldset class="body current">
							<div class="row">
								<label class="col-lg-4 text-right">* fields are mandatory</label>
							</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="col-lg-12">
											<div class="form-group">
												<label class="col-sm-3 control-label">Virtual Machine Name * :</label>
												<div class="col-sm-9">
													<input type="text" class="form-control characters"
														placeholder="Virtual Machine Name" name="hostname" id="editHostName"
														tabindex="1" value="${Model.vmDetails[0].hostname}">
												</div>
											</div>
																						
											<div class="form-group">
												<label class="col-sm-3 control-label">IP Address * :</label>
												<div class="col-sm-9">
													<input type="text" class="form-control .ipv4"
														placeholder="IP Address" name="ipAddress" onchange="validateIp()"
														tabindex="2"  value="${Model.vmDetails[0].ip_address }">
												</div>
												
											</div>
											<label class="error hidden" id="ipError" style="margin-left: 26%;">Please enter valid IP address.</label>
											<div class="form-group">
												<label class="col-sm-3 control-label">Port * :</label>
												<div class="col-sm-9">
													<input type="text" class="form-control"
														placeholder="Port Number" name="portNumber"
														tabindex="2"  value="${Model.vmDetails[0].port }">
												</div>
											</div>
											<div class="form-group">
											<label class="col-sm-3 control-label">Project Repository:</label>
											<div class="col-sm-5">
												<div class="radio radio-success radio-inline col-sm-3">
													<input type="radio" id="gitLoc" value="1"
														name="repositoryStatus" tabindex="1" ${Model.vmDetails[0].git_status== 1 ? 'checked="true"' : ""}> 
													<label for="git">Git</label>
												</div>
												<div class="radio radio-success radio-inline col-sm-4">
													<input type="radio" id="machineLoc" value="2"
														name="repositoryStatus" tabindex="2" ${Model.vmDetails[0].git_status== 2 ? 'checked="true"' : ""}> 
													<label for="machine">Machine Location</label>
												</div>
											</div>
										</div>
											<div class="form-group hidden" id="projectLoc">
												<label class="col-sm-3 control-label">Project Location * :</label>
												<div class="col-sm-9">
													<input type="text" class="form-control characters"
														placeholder="Project Location" name="projectLocation"
														tabindex="1" value="${Model.vmDetails[0].project_location }">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Concurrent Execution Status :</label>
												<div class="col-sm-3">
													<div class="radio radio-success radio-inline col-sm-5">
														<input type="radio" id="inlineRadio11" value="1"
															name="concExecStatus"  ${Model.vmDetails[0].concurrent_exec_status == 1 ? 'checked="true"' : ""} onchange="numberExecs()"> <label
															for="inlineRadio11" tabindex="3"> Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-5">
														<input type="radio" id="inlineRadio12" value="2"
															name="concExecStatus" ${Model.vmDetails[0].concurrent_exec_status == 2 ? 'checked="true"' : ""} onchange="numberExecs()"> <label for="inlineRadio12">
															Inactive </label>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group" id="concurrentExecNumber">
														<label class="col-sm-5 control-label">Number of Concurrent Executions * :</label>
														<div class="col-sm-5">
															<input type="text" class="form-control"
																placeholder="" name="concurrentExecNumber"
																tabindex="2" value="${Model.vmDetails[0].concurrent_execs}"	>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">IE Status:</label>
												<div class="col-sm-3">
													<div class="radio radio-success radio-inline col-sm-5">
														<input type="radio" id="inlineRadio1" value="1"
															name="ieStatus" ${Model.vmDetails[0].ie_status == 1 ? 'checked="true"' : ""}> <label
															for="inlineRadio1" tabindex="3"> Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-5">
														<input type="radio" id="inlineRadio2" value="2"
															name="ieStatus" ${Model.vmDetails[0].ie_status == 2 ? 'checked="true"' : ""}> <label for="inlineRadio2">
															Inactive </label>
													</div>
												</div>
											</div>
											
											
											<div class="form-group">
												<label class="col-sm-3 control-label">Chrome Status:</label>
												<div class="col-sm-3">
													<div class="radio radio-success radio-inline col-sm-5">
														<input type="radio" id="inlineRadio3" value="1"
															name="chromeStatus" ${Model.vmDetails[0].chrome_status == 1 ? 'checked="true"' : ""}> <label
															for="inlineRadio3" tabindex="3"> Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-5">
														<input type="radio" id="inlineRadio4" value="2"
															name="chromeStatus" ${Model.vmDetails[0].chrome_status == 2 ? 'checked="true"' : ""}> <label for="inlineRadio4">
															Inactive </label>
													</div>
												</div>
											</div>
											
											
											<div class="form-group">
												<label class="col-sm-3 control-label">Firefox Status:</label>
												<div class="col-sm-3">
													<div class="radio radio-success radio-inline col-sm-5">
														<input type="radio" id="inlineRadio5" value="1"
															name="firefoxStatus" ${Model.vmDetails[0].firefox_status == 1 ? 'checked="true"' : ""}> <label
															for="inlineRadio5" tabindex="3"> Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-5">
														<input type="radio" id="inlineRadio6" value="2"
															name="firefoxStatus" ${Model.vmDetails[0].firefox_status == 2 ? 'checked="true"' : ""}> <label for="inlineRadio6">
															Inactive </label>
													</div>
												</div>
											</div>
											
											
											<div class="form-group">
												<label class="col-sm-3 control-label">Safari Status:</label>
												<div class="col-sm-3">
													<div class="radio radio-success radio-inline col-sm-5">
														<input type="radio" id="inlineRadio7" value="1"
															name="safariStatus" ${Model.vmDetails[0].safari_status == 1 ? 'checked="true"' : ""}> <label
															for="inlineRadio7" tabindex="3"> Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-5">
														<input type="radio" id="inlineRadio8" value="2"
															name="safariStatus" ${Model.vmDetails[0].safari_status == 2 ? 'checked="true"' : ""}> <label for="inlineRadio8">
															Inactive </label>
													</div>
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-sm-3 control-label">Status:</label>
												<div class="col-sm-3">
													<div class="radio radio-success radio-inline col-sm-5">
														<input type="radio" id="inlineRadio9" value="1"
															name="vmStatus" ${Model.vmDetails[0].status == 1 ? 'checked="true"' : ""}> <label
															for="inlineRadio9" tabindex="3"> Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-5">
														<input type="radio" id="inlineRadio10" value="2"
															name="vmStatus" ${Model.vmDetails[0].status == 2 ? 'checked="true"' : ""}> <label for="inlineRadio10">
															Inactive </label>
													</div>
												</div>
											</div>
											
											
											<div class="form-group">
												<label class="col-sm-3 control-label">Is Free:</label>
												<div class="col-sm-3">
													<div class="radio radio-success radio-inline col-sm-5">
														<input type="radio" id="inlineRadio11" value="1"
															name="vmStatusFree" ${Model.vmDetails[0].is_free == 1 ? 'checked="true"' : ""}> <label
															for="inlineRadio11" tabindex="3"> Yes </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-5">
														<input type="radio" id="inlineRadio12" value="2"
															name="vmStatusFree" ${Model.vmDetails[0].is_free == 2 ? 'checked="true"' : ""}> <label for="inlineRadio12">
															No </label>
													</div>
												</div>
											</div>
											
											
										<div class="col-lg-12">
											
										</div>
									</div>
								</div>
							</div>
							</fieldset>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-white pull-left"
										style="margin-right: 15px;" type="button" id="btnCancel"
										type="button" tabindex="6">Cancel</button>
									<c:if test="${Model.createvm==1}">
									<button class="btn btn-success pull-left hidden" style="margin-right: 15px;" type="button" tabindex="7"
										id="editVmBtn">Edit</button></c:if>
									<button class="btn btn-success pull-left ladda-button" type="button"
										id="updateVmBtn" data-style="slide-up" tabindex="8">Submit</button>
								</div>
							</div>
						</div>
					</form>
					<!-- end form -->
				</div>
				<!-- end ibox-content -->

			</div>
			<!-- end ibox -->
		</div>
		<!-- end col -->
	</div>

	<!-- end row -->
</div>
<!-- end wrapper -->
</div>
<!-- Input Mask-->
    <script src="<%=request.getContextPath()%>/resources/js/mask/jquery.input-ip-address-control-1.0.min.js"></script>
<input type="hidden" id="hiddenInput"
	value=<%=session.getAttribute("vmupdateStatus")%>>

<input type="hidden" id="updateVmName"
	value='<%=session.getAttribute("updateVmName")%>'>
<script type="text/javascript">

$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	var gitStatus="${Model.vmDetails[0].git_status}";
	if(gitStatus==2){
		$("#projectLoc").removeClass("hidden");
	}
});

  $(function () {

	  var viewType=${Model.viewType};
	  if(viewType==1)
		{
		  $("input").attr("disabled", true);
		  $("#editHostName").attr("disabled", true);
		  $("#updateVmBtn").addClass("hidden");
		  $("#editVmBtn").removeClass("hidden");
		  
		}
	  else if(viewType==2)
		 {
		  $("input").attr("disabled", false);	
		  $("#editHostName").attr("disabled", true);
		 }
	    var state = $('#hiddenInput').val();
		var name = $('#updateVmName').val();
		if(state == 1){
			window.location.href="cloudvmdetails"; 
		}
		 $('#hiddenInput').val(3);  
	});
  
	  function showToster(toasterMessage){
			 toastr.options = {
					 "closeButton": true,
					  "debug": true,
					  "progressBar": true,
					  "preventDuplicates": true,
					  "positionClass": "toast-top-right",
					  "showDuration": "400",
					  "hideDuration": "1000",
					  "timeOut": "9000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "slideDown",
					  "hideMethod": "slideUp"
	          };
	          toastr.success('',toasterMessage);
	         window.location.href="cloudvmdetails"; 
		}  
	
</script>
<script>

 $("#btnCancel").click(function(){
	 $('body').addClass("white-bg");
	 $("#barInMenu").removeClass("hidden");
	 $("#wrapper").addClass("hidden");	
	 window.location.href="cloudvmdetails"; 
 });
 
 $("#editVmBtn").click(function(){
 
	 $("input").attr("disabled", false);
	 $("#editHostName").attr("disabled", true);
	 $("#updateVmBtn").removeClass("hidden");
	 $("#editVmBtn").addClass("hidden");
 });
 $("#updateVmDetailsForm").validate({
		rules : {
			hostname : {
				required : true,
				minlength : 1,
				maxlength : 100
			},
			ipAddress : {
				required : true,
				minlength : 1,
				maxlength : 17
			},
			remoteUsername : {
				required : true
			},
			remotePassword : {
				required : true
			},
			projectLocation : {
				required : true,
				minlength : 1,
				maxlength : 1000
			},
			portNumber:{
				required : true,
				minlength : 1,
				maxlength : 5,
				number: true,
				min : 0,
				max : 65535
			},
			concurrentExecNumber :{
				required : function(){
				if($("input[name=concExecStatus]:checked").attr("value")==1)
				{
					return true;
				}
			else
				{
					return false;
				}},
				number : true,
				min : 1,
				max : 20
			}
		}
		/* messages: {
			executionTime:{
				required: "Estimated execution time in Minutes."
			} 
		}*/
	});

 
$('#updateVmBtn').click(function() {
	validateIp();
	if($('#updateVmDetailsForm').valid() && validateIp()){
		  $(this).attr("disabled","true");
		  
		  $('body').addClass("white-bg");
		  $("#barInMenu").removeClass("hidden");
		  $("#wrapper").addClass("hidden");
		  
		   $("input").attr("disabled", false);

			$('#updateVmDetailsForm').submit();
	  }
	});
function validateIp()
{
	var ipAddr = $("input[name=ipAddress]").val();
	if(ipAddr.isIpv4() && (ipAddr.indexOf("-") == -1)) {
		$("#ipError").addClass("hidden");
	    return true;
	} else {
		$("#ipError").removeClass("hidden");
		$("#ipError").text("Please enter valid IP address.").show();
	    return false;
	}
} 

function numberExecs()
{
	var value= $("input[name=concExecStatus]:checked").attr("value");
	
	if(value==1)
		{
			$("#concurrentExecNumber").fadeIn();
		}
	else
		{
			$("input[name=concurrentExecNumber]").val("1");
			$("#concurrentExecNumber").fadeOut();
		}
}
$("#machineLoc").click(function(){
	if($("#machineLoc").is(':checked')){
		$("#projectLoc").removeClass("hidden");
	}
	
});
$("#gitLoc").click(function(){
	if($("#gitLoc").is(':checked')){
		$("#projectLoc").addClass("hidden");
	}
	
});
</script>

<%
	//set session variable to another value
	session.setAttribute("vmupdateStatus", 3);
%>


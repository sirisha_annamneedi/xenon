<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Create New Customer</h2>
		<ol class="breadcrumb">
			<li><a href="coredashboard">Dashboard</a></li>
			<li><a href="viewcustomer">View Customer</a></li>
			<li class="active"><strong>View Customer Details</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form id="updateCustInfoForm" action="editcustomer" method="post">
						<div class="row">
							<div class="col-lg-6">
								<h3>Customer Information</h3>
								<div class="form-group hidden">
									<input id="custId" type="hidden" class="form-control"
										value="${Model.CustDetails[0].customer_id}" name="custId"
										disabled>
								</div>
								<div class="form-group hidden">
									<label class="hidden">Customer Schema </label> <input
										id="custSchema" type="text" class="form-control hidden"
										name="custSchema"
										value="${Model.CustDetails[0].customer_schema}">
								</div>
								<div class="form-group">
									<label>Customer Name *</label> <input id="custNameVerify"
										type="text" class="form-control" name="custName"
										value="${Model.CustDetails[0].customer_name}" disabled>
								</div>
								<div class="form-group">
									<label>Address </label>
									<textarea rows="5" cols="50" style="resize: none;"
										id="custAddressVerify" name="custAddress" class="form-control"
										placeholder="Address" disabled>${Model.CustDetails[0].customer_address}</textarea>
								</div>
							</div>
							<div class="col-lg-6">
								<h3>Licence Information</h3>
								<div class="form-group">
									<div class="row">
										<label class="col-sm-4 margine-lable">Customer Type *</label>
										<div class="input-group col-sm-8">

											<select disabled class="select-picker form-control"
												id="custTypeVerify" name="custTypeVerify">
												<c:forEach var="type" items="${Model.custTypes}">
													<option value="${type.id}">${type.customer_type}</option>
												</c:forEach>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Customer Status</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" value="1" name="custStatusVerify"
													checked="" disabled> <label for="inlineRadio1">
													Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="2" name="custStatusVerify"
													disabled> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Bug Tracker
											Status</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" value="1" name="bugTrackerStatusVerify"
													checked="" disabled> <label for="inlineRadio1">
													Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="2" name="bugTrackerStatusVerify"
													disabled> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Test Manager
											Status</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" value="1" name="testManagerStatusVerify"
													checked="" disabled> <label for="inlineRadio1">
													Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="2" name="testManagerStatusVerify"
													disabled> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Scriptless
											Status</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" value="1" name="scriptlessStatusVerify"
													checked="" disabled> <label for="inlineRadio1">
													Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="2" name="scriptlessStatusVerify"
													disabled> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Automation
											Status</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" value="1" name="automationStatusVerify"
													checked="" disabled> <label for="inlineRadio1">
													Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="2" name="automationStatusVerify"
													disabled> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">On Premise Execution
											Status</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" value="1" name="onPremiseExecution"
													checked="" disabled> <label for="inlineRadio1">
													Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="2" name="onPremiseExecution"
													disabled> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Jenkin 
											Status</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" value="1" name="pluginJenkinStatus"
													checked="" disabled> <label for="inlineRadio11">
													Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="2" name="pluginJenkinStatus"
													disabled> <label for="inlineRadio12">
													Inactive </label>
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Git 
											Status</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" value="1" name="pluginGitStatus"
													checked="" disabled> <label for="inlineRadio1">
													Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="2" name="pluginGitStatus"
													disabled> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Redwood Status</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" value="1" name="redwoodStatus"
													checked="" disabled> <label for="inlineRadio1">
													Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="0" name="redwoodStatus"
													disabled> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
											<div class="form-group">
												<label class="col-sm-3 margine-lable">Redwood URL </label>
												<div class="col-sm-8">
													<div class="form-group">
													<input id="redwoodUrl"name="redwoodUrl" type="text"
															class="form-control characters required" value="${Model.CustDetails[0].redwood_url}"
															aria-required="true">
													</div>
												</div>
											</div>
										</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Api Status</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" value="1" name="apiStatus"
													checked="" disabled> <label for="inlineRadio1">
													Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="0" name="apiStatus"
													disabled> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="row">

							<div class="col-lg-6">
								<h3>Account Information</h3>

								<div class="form-group">
									<label>Created Date</label> <input type="text"
										class="form-control"
										value="${Model.CustDetails[0].created_date}" disabled>
								</div>

								<div class="form-group">
									<label>Start Date</label> <input type="text"
										class="form-control"
										value="${Model.CustDetails[0].start_date}" disabled>
								</div>
								<div class="form-group">
									<label>End Date</label> <input type="text" class="form-control"
										value="${Model.CustDetails[0].end_date}" disabled>
								</div>

							</div>

							<div class="col-lg-6">
								<h3>Admin Information</h3>
								<div class="form-group">
									<label>First name *</label> <input id="firstNameVerify"
										type="text" class="form-control"
										value="${Model.custAdminDetails[0].first_name}" disabled>
								</div>
								<div class="form-group">
									<label>Last Name *</label> <input id="lastNameVerify"
										type="text" class="form-control"
										value="${Model.custAdminDetails[0].last_name}" disabled>
								</div>
								<div class="form-group">
									<label>Email *</label> <input id="emailIdVerify"
										value="${Model.custAdminDetails[0].email_id}" type="email"
										class="form-control" disabled>
								</div>

							</div>
						</div>

						<button type="button" class="btn btn-w-m btn-primary"
							id="editCustInfo">Edit</button>
						<button style="visibility: hidden;" type="button"
							class="btn btn-w-m btn-default" id="cancelCustInfo">
							Cancel</button>
						<button style="visibility: hidden;"
							class="btn btn-w-m btn-primary" id="saveCustInfo">
							Update</button>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>
<style>
.margine-lable {
	margin-bottom: 2.0em !important;
}
</style>
<script type="text/javascript">
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});
</script>
<script>
	var customerType;
	var customerBTStatus;
	var customerTMStatus;
	var customerScriptStatus;
	var customerAutomationstatus;
	var customerStatus;
	var redwoodStatus;
	var apiStatus;
	$(document)
			.ready(
					function() {

						customerType = '${Model.CustDetails[0].customer_type}';
						customerBTStatus = '${Model.CustDetails[0].bt_status}';
						customerTMStatus = '${Model.CustDetails[0].tm_status}';
						onPremiseStatus = '${Model.CustDetails[0].on_premise}';
						customerScriptStatus='${Model.CustDetails[0].scriptless_status}';
						customerAutomationstatus = '${Model.CustDetails[0].automation_status}';
						customerStatus = '${Model.CustDetails[0].customer_status}';
						jenkinStatus = '${Model.CustDetails[0].jenkin_status}';
						gitStatus = '${Model.CustDetails[0].git_status}';
						redwoodStatus = '${Model.CustDetails[0].redwood_status}';
						apiStatus = '${Model.CustDetails[0].api_status}';

						$(
								"input[name=custStatusVerify][value="
										+ customerStatus + "]").prop('checked',
								true);
						$(
								"input[name=bugTrackerStatusVerify][value="
										+ customerBTStatus + "]").prop(
								'checked', true);
						$(
								"input[name=testManagerStatusVerify][value="
										+ customerTMStatus + "]").prop(
								'checked', true);
						$(
								"input[name=scriptlessStatusVerify][value="
										+ customerScriptStatus + "]").prop(
								'checked', true);
						$(
								"input[name=onPremiseExecution][value="
										+ onPremiseStatus + "]").prop(
								'checked', true);
						$(
								"input[name=automationStatusVerify][value="
										+ customerAutomationstatus + "]").prop(
								'checked', true);
						$(
								"input[name=pluginJenkinStatus][value="
										+ jenkinStatus + "]").prop(
								'checked', true);
						$(
								"input[name=pluginGitStatus][value="
										+ gitStatus + "]").prop(
								'checked', true);
						$(
								"input[name=redwoodStatus][value="
										+ redwoodStatus + "]").prop(
								'checked', true);
						$(
								"input[name=apiStatus][value="
										+ apiStatus + "]").prop(
								'checked', true);
						$("#custTypeVerify").val(customerType);

						if (localStorage.getItem("CustomerView") == "1")
							editCustInfo();

					});

	$("#editCustInfo").click(function(event) {
		editCustInfo();
	});

	function editCustInfo() {
		localStorage.setItem("CustomerView", "0");
		$('#custNameVerify').prop("disabled", false);
		$('#custAddressVerify').prop("disabled", false);
		//$('#firstNameVerify').prop("disabled", false);
		// $('#lastNameVerify').prop("disabled", false);
		// $('#emailIdVerify').prop("disabled", false);
		$('input[name=custStatusVerify]').prop("disabled", false);
		$('input[name=scriptlessStatusVerify]').prop("disabled", false);
		$('input[name=bugTrackerStatusVerify]').prop("disabled", false);
		$('input[name=testManagerStatusVerify]').prop("disabled", false);
		$('input[name=automationStatusVerify]').prop("disabled", false);
		$('#custTypeVerify').prop("disabled", false);
		$("#editCustInfo").hide();
		$("#saveCustInfo").css("visibility", "");
		$("#cancelCustInfo").css("visibility", "");
		$('input[name=pluginJenkinStatus]').prop("disabled", false);
		$('input[name=pluginGitStatus]').prop("disabled", false);
		$('input[name=redwoodStatus]').prop("disabled", false);
		$('input[name=apiStatus]').prop("disabled", false);
		
	}

	$("#saveCustInfo").click(function(event) {
		$("#updateCustInfoForm").submit();
	});

	$("#cancelCustInfo").click(function(event) {
		location.reload(1);
	});
</script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Customer</h2>
		<ol class="breadcrumb">
			<li><a href="coredashboard">Dashboard</a></li>
			<li class="active"><strong>View Customer</strong></li>
		</ol>
	</div>
	<!-- end col-lg-10 -->
</div>
<!-- end row -->
<!-- end wrapper -->

<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-content">
					<div class="clients-list">
						<ul class="nav nav-tabs">

							<li class="active"><a data-toggle="tab" href="#tab-1"><i
									class="fa fa-user"></i> Contacts</a></li>
							<li class=""><a data-toggle="tab" href="#tab-2"><i
									class="fa fa-briefcase"></i>Customer</a></li>
						</ul>
						<div class="tab-content">
							<div id="tab-1" class="tab-pane active" style="margin-top: 3%">
								<div class="full-height-scroll">
									<div class="table-responsive" style="overflow-x: visible;">
										<table
											class="table table-striped table-hover dataTables-example"
											id="viewCustAdminDetailsTable">
											<thead>
												<tr>
													<td></td>
													<td><b>Company Name</b></td>
													<td><b>User name</b></td>
													<td><b>Status</b></td>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="CustomerAdminList"
													items="${Model.CustomerAdminList}">
													<tr style="cursor: pointer" data-toggle="tab"
														href="#custAdminInfo_${CustomerAdminList.customer_id }_${CustomerAdminList.user_id}"
														class="client-link">
														<td class="client-avatar"><img alt="image"
															src="data:image/jpg;base64,${CustomerAdminList.logo }"></td>
														<td>${CustomerAdminList.customer_name }</td>
														<td>${CustomerAdminList.email_id }</td>


														<td class="client-status"><span
															class="label label-primary"
															style="text-transform: capitalize;">${CustomerAdminList.user_status }</span></td>

													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div id="tab-2" class="tab-pane" style="margin-top: 3%">
								<div class="full-height-scroll">
									<div class="table-responsive" style="overflow-x: visible;">
										<table class="table table-striped table-hover"
											id="viewCustCompanyTable">
											<thead>
												<tr>
													<td></td>
													<td>Company Name</td>
													<td>Address</td>
													<td>Status</td>
													<td></td>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="CustomerList" items="${Model.CustomerList}">
													<tr style="cursor: pointer" data-toggle="tab"
														href="#custInfo_${CustomerList.customer_id }"
														class="client-link">
														<td class="client-avatar"><img alt="image"
															src="data:image/jpg;base64,${CustomerList.logo }"></td>
														<td>${CustomerList.customer_name}</td>
														<td>${CustomerList.customer_address}</td>

														<c:if test="${CustomerList.customer_status == 1}">
															<td class="client-status"><span
																class="label label-primary">Active</span></td>
														</c:if>
														<c:if test="${CustomerList.customer_status == 2}">
															<td class="client-status"><span
																class="label label-danger">Inactive</span></td>
														</c:if>

														<td class="project-actions"><a
															class="btn btn-white btn-sm"
															onclick="viewCustomer(${CustomerList.customer_id })"
															name="${CustomerList.customer_id }"><i
																class="fa fa-folder"></i> View </a> <a
															class="btn btn-white btn-sm"
															onclick="editCustomer(${CustomerList.customer_id })"
															name="${CustomerList.customer_id }"><i
																class="fa fa-pencil" name="${CustomerList.customer_id }"></i>
																Edit </a></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="ibox ">

				<div class="ibox-content">
					<div class="tab-content" id="custInfo">


						<c:forEach var="CustomerAdminList"
							items="${Model.CustomerAdminList}">

							<div
								id="custAdminInfo_${CustomerAdminList.customer_id }_${CustomerAdminList.user_id}"
								class="tab-pane">
								<div class="row m-b-lg">
									<div class="col-lg-4 text-center">
										<h4>${CustomerAdminList.first_name}</h4>
										<div class="m-b-sm">
											<img alt="image" class="img-circle"
												src="data:image/jpg;base64,${CustomerAdminList.logo }"
												style="width: 62px">
										</div>
									</div>
									<div class="col-lg-8">
										<strong> About me </strong>

										<p>${CustomerAdminList.about_me}</p>
									</div>
								</div>
								<div class="">
									<div class="full-height-scroll">
										<strong>User Information</strong>

										<ul class="list-group clear-list">
											<li class="list-group-item fist-item"><span
												class="pull-right"> ${CustomerAdminList.role_type} </span>User
												Role</li>
											<li class="list-group-item"><c:if
													test="${CustomerAdminList.tmStatus==1}">
													<span class="pull-right label label-primary"> Active
													</span>
												</c:if> <c:if test="${CustomerAdminList.tmStatus==2}">
													<span class="pull-right label label-danger">
														Inactive </span>
												</c:if> Test Manager Status</li>
											<li class="list-group-item"><c:if
													test="${CustomerAdminList.btStatus==1}">
													<span class="pull-right label label-primary"> Active
													</span>
												</c:if> <c:if test="${CustomerAdminList.btStatus==2}">
													<span class="pull-right label label-danger">
														Inactive </span>
												</c:if> Bug Tracker Status</li>
												
											<li class="list-group-item"><span class="pull-right">
													0 </span>Application Assigned</li>
										</ul>
										<strong>Notes</strong>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing
											elit, sed do eiusmod tempor incididunt ut labore et dolore
											magna aliqua.</p>
									</div>
								</div>
							</div>
						</c:forEach>

						<c:forEach var="CustomerList" items="${Model.CustomerList}">
							<div id="custInfo_${CustomerList.customer_id }" class="tab-pane">
								<div class="row m-b-lg">
									<div class="col-lg-4 text-center">
										<h4>${CustomerList.customer_name }</h4>

										<div class="m-b-sm">
											<img alt="image" class="img-circle"
												src="data:image/jpg;base64,${CustomerList.logo }"
												style="width: 62px">
										</div>
									</div>
									<div class="col-lg-8">
										<strong> Address </strong>

										<p>${CustomerList.customer_address }</p>

									</div>
								</div>
								<div class="client-detail">
									<div class="full-height-scroll">

										<strong>Details</strong>

										<ul class="list-group clear-list">
											<li class="list-group-item fist-item"><span
												class="pull-right"> ${CustomerList.userCount} </span> No. of
												Users</li>
											<li class="list-group-item"><span class="pull-right">
													${CustomerList.projectCount } </span> No. of Applications</li>
											<li class="list-group-item"><span class="pull-right">
													${CustomerList.cloudeSpace} MB</span> Cloud space</li>

										</ul>
										<hr>
										<strong>Status</strong>

										<ul class="list-group clear-list">

											<li class="list-group-item fist-item"><c:if
													test="${CustomerList.customer_type == 1}">
													<span class="pull-right label label-warning"> Trial
													</span>
												</c:if> <c:if test="${CustomerList.customer_type == 2}">
													<span class="pull-right label label-info"> Silver </span>
												</c:if> <c:if test="${CustomerList.customer_type == 3}">
													<span class="pull-right label label-success"> Gold </span>
												</c:if> <c:if test="${CustomerList.customer_type == 4}">
													<span class="pull-right label label-danger">
														Platinum </span>
												</c:if> Customer Type</li>

											<li class="list-group-item"><c:if
													test="${CustomerList.bt_status == 1}">
													<span class="pull-right label label-primary"> Active
													</span>
												</c:if> <c:if test="${CustomerList.bt_status == 2}">
													<span class="pull-right label label-danger">
														Inactive </span>
												</c:if> Bug Tracker Status</li>
											<li class="list-group-item"><c:if
													test="${CustomerList.tm_status == 1}">
													<span class="pull-right label label-primary"> Active
													</span>
												</c:if> <c:if test="${CustomerList.tm_status == 2}">
													<span class="pull-right label label-danger">
														Inactive </span>
												</c:if> Test M Status</li>
											<li class="list-group-item"><c:if
													test="${CustomerList.customer_status == 1}">
													<span class="pull-right label label-primary"> Active
													</span>
												</c:if> <c:if test="${CustomerList.customer_status == 2}">
													<span class="pull-right label label-danger">
														Inactive </span>
												</c:if> Customer Status</li>
											<li class="list-group-item"><c:if
													test="${CustomerList.automation_status==1}">
													<span class="pull-right label label-primary"> Active
													</span>
												</c:if> <c:if test="${CustomerList.automation_status==2}">
													<span class="pull-right label label-danger">
														Inactive </span>
												</c:if> Automation Status</li>
										</ul>

										<strong>Notes</strong>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing
											elit, sed do eiusmod tempor incididunt ut labore et dolore
											magna aliqua.</p>
										<hr />
									</div>
								</div>
							</div>
						</c:forEach>
					</div>



				</div>
			</div>
		</div>
	</div>
</div>

<script
	src="<%=request.getContextPath()%>/resources/js/plugins/dataTables/datatables.min.js"></script>
<script type="text/javascript">
  $(function () {
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	});  
</script>
<script>
 	$(function(){
 		localStorage.setItem("CustomerView", "0");
 		$('#viewCustAdminDetailsTable').DataTable({
 			"paging": true,
 	 	    "lengthChange": true,
 	 	    "searching": true,
 	 	    "ordering": true,
 	 	    "info": true,
 	        "autoWidth": true
 		});
 		$('#viewCustCompanyTable').DataTable({
 			"paging": true,
 	 	    "lengthChange": true,
 	 	    "searching": true,
 	 	    "ordering": true,
 	 	    "info": true,
 	        "autoWidth": false
 		});
 		
 		$('#custInfo  div:first-child').addClass('active');
 	});
 	
 	function viewCustomer(id)
 	{
 		$("#spinner").fadeIn("slow");
 		localStorage.setItem("CustomerView", "0");
 		var posting = $.post('setCustomerId', {
 			custId : id,
 		});
 		 posting.done(function(data) {
 			window.location.href="viewcustomerdetails"; 

 		}); 
 		
 		//alert("View ID  = "+id);
 	}
 	
 	function editCustomer(id)
 	{
 		$("#spinner").fadeIn("slow");
 		localStorage.setItem("CustomerView", "1");
 		var posting = $.post('setCustomerId', {
 			custId : id,
 		});
 		 posting.done(function(data) {
 			window.location.href="viewcustomerdetails"; 

 		}); 
 	}
 	
 </script>
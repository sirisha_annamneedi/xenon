<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="spinner"></div>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Create New Customer</h2>
		<ol class="breadcrumb">
			<li><a href="coredashboard">Dashboard</a></li>
			<li class="active"><strong>Create New Customer</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form id="form" action="insertCustomer" method="post"
						class="wizard-big">
						<h1>Customer Details</h1>
						<fieldset>
							<label class="col-lg-2 ">* fields are mandatory</label><br>
							<h2>Customer Information</h2>
							<div class="row">
								<div class="col-lg-8">
									<div class="form-group">
										<label>Customer Name *</label> <input id="custName"
											name="custName" type="text"
											class="form-control characters required" aria-required="true">
									</div>
									<div class="form-group">
										<label>Address </label>
										<textarea rows="5" cols="50" style="resize: none;"
											id="custAddress" name="custAddress"
											class="form-control characters"></textarea>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="text-center">
										<div style="margin-top: 20px">
											<i class="fa fa-sign-in"
												style="font-size: 180px; color: #e5e5e5"></i>
										</div>
									</div>
								</div>
							</div>

						</fieldset>

						<h1>Licence Details</h1>
						<fieldset>
							<label class="col-lg-2 ">* fields are mandatory</label><br>
							<h2>Licence Information</h2>
							<div class="row">
								<div class="col-lg-8">
									<div class="form-group">
										<div class="row">
											<label class="col-sm-3 margine-lable">Customer Type *</label>
											<div class="input-group col-sm-6">

												<select class="select-picker form-control" name="custType"
													id="custType">
													<c:forEach var="type" items="${Model.custTypes}">
														<option value="${type.id}">${type.customer_type}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Customer Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="activeUser" value="1"
														name="custStatus" checked=""> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inactiveUser" value="2"
														name="custStatus"> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Bug Tracker
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="activeBugTracker" value="1"
														name="bugTrackerStatus" checked=""> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inactiveBugTracker" value="2"
														name="bugTrackerStatus"> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Test Manager
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="activeTestManager" value="1"
														name="testManagerStatus" checked=""> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inactiveTestManager" value="2"
														name="testManagerStatus"> <label
														for="inlineRadio2"> Inactive </label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Automation
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="activeAutomation" value="1"
														name="automationStatus" checked=""> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inactiveAutomation" value="2"
														name="automationStatus"> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Scriptless
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="activeScriptless" value="1"
														name="scriptlessStatus" checked=""> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inactiveScriptless" value="2"
														name="scriptlessStatus"> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">On Premise
												Execution</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="activeOnPremise" value="1"
														name="onPremiseStatus" checked=""> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inactiveOnPremise" value="2"
														name="onPremiseStatus"> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>


									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Plugin Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="activePlugin" value="1"
														name="pluginStatus"> <label for="inlineRadio9">
														Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inactivePlugin" value="2"
														name="pluginStatus" checked=""> <label
														for="pluginRadio10"> Inactive </label>
												</div>
											</div>
										</div>
									</div>
									<div id="pluginDiv" class="hidden">
										<div class="row">
											<div class="form-group">
												<label class="col-sm-3 margine-lable">Jenkin Status</label>
												<div class="col-sm-8">
													<div class="radio radio-success radio-inline col-sm-2">
														<input type="radio" id="activeJenkinPlugin" value="1"
															name="pluginJenkinStatus"> <label
															for="inlineRadio11"> Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-2">
														<input type="radio" id="inactiveJenkinPlugin" value="2"
															checked="" name="pluginJenkinStatus"> <label
															for="pluginRadio12"> Inactive </label>
													</div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="form-group">
												<label class="col-sm-3 margine-lable">Git Status</label>
												<div class="col-sm-8">
													<div class="radio radio-success radio-inline col-sm-2">
														<input type="radio" id="activeGitPlugin" value="1"
															name="pluginGitStatus"> <label for="inlineRadio9">
															Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-2">
														<input type="radio" id="inactiveGitPlugin" value="2"
															checked="" name="pluginGitStatus"> <label
															for="pluginRadio10"> Inactive </label>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group">
												<label class="col-sm-3 margine-lable">Redwood Status</label>
												<div class="col-sm-8">
													<div class="radio radio-success radio-inline col-sm-2">
														<input type="radio" id="redwoodStatusActive" value="1"
															name="redwoodStatus"> <label for="inlineRadio9">
															Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-2">
														<input type="radio" id="redwoodStatusInactive" value="0"
															checked="" name="redwoodStatus"> <label
															for="pluginRadio10"> Inactive </label>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group">
												<label class="col-sm-3 margine-lable">Redwood URL </label>
												<div class="col-sm-8">
													<div class="form-group">
													<input id="redwoodUrl"name="redwoodUrl" type="text"
															class="form-control characters required"
															aria-required="true">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group">
												<label class="col-sm-3 margine-lable">Api Status</label>
												<div class="col-sm-8">
													<div class="radio radio-success radio-inline col-sm-2">
														<input type="radio" id="activeApi" value="1"
															name="apiStatus"> <label for="inlineRadio9">
															Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-2">
														<input type="radio" id="inactiveApi" value="0" checked=""
															name="apiStatus"> <label for="pluginRadio10">
															Inactive </label>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>

						</fieldset>

						<h1>Administrator Details</h1>
						<fieldset>
							<label class="col-lg-2 ">* fields are mandatory</label><br>
							<h2>Account Information</h2>
							<div class="alert alert-danger hidden errorMessage1"
								id="errorMessage1">User already exists with this email id.</div>
							<div class="alert alert-danger hidden errorMessage2"
								id="errorMessage2">User already exists with this user
								name.</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>First name *</label> <input id="firstName"
											name="firstName" type="text"
											class="form-control characters required">
									</div>
									<div class="form-group">
										<label>Last Name *</label> <input id="lastName"
											name="lastName" type="text"
											class="form-control characters required">
									</div>
									<div class="form-group">
										<label>Username *</label> <input id="usernameLogin"
											name="usernameLogin" type="text"
											class="form-control required characters" tabindex="2">
									</div>
									<div class="form-group">
										<label>Email *</label> <input id="emailId" name="emailId"
											type="email" class="form-control">
									</div>

								</div>
							</div>
						</fieldset>


						<h1>Finish</h1>
						<fieldset>
							<label class="col-lg-2 ">* fields are mandatory</label><br>
							<h1>Confirm Customer Information</h1>
							<h3 class="text-info">Customer Information</h3>
							<div class="row">
								<div class="col-lg-8">
									<div class="form-group">
										<label>Customer Name *</label> <input id="custNameVerify"
											type="text" class="form-control" disabled>
									</div>
									<div class="form-group">
										<label>Address </label>
										<textarea rows="5" cols="50" style="resize: none;"
											id="custAddressVerify" class="form-control" disabled></textarea>
									</div>
								</div>

							</div>


							<h3 class="text-info">Licence Information</h3>
							<div class="row">
								<div class="col-lg-8">
									<div class="form-group">
										<div class="row">
											<label class="col-sm-3 margine-lable">Customer Type *</label>
											<div class="input-group col-sm-6">

												<select disabled class="select-picker form-control"
													id="custTypeVerify">
													<c:forEach var="type" items="${Model.custTypes}">
														<option value="${type.id}">${type.customer_type}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Customer Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" value="1" name="custStatusVerify"
														checked="" disabled> <label for="inlineRadio1">
														Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" value="2" name="custStatusVerify"
														disabled> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Bug Tracker
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" value="1" name="bugTrackerStatusVerify"
														checked="" disabled> <label for="inlineRadio1">
														Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" value="2" name="bugTrackerStatusVerify"
														disabled> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Test Manager
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" value="1"
														name="testManagerStatusVerify" checked="" disabled>
													<label for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" value="2"
														name="testManagerStatusVerify" disabled> <label
														for="inlineRadio2"> Inactive </label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Automation
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" value="1" name="automationStatusVerify"
														checked="" disabled> <label for="inlineRadio1">
														Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" value="2" name="automationStatusVerify"
														disabled> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Jenkin Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" value="1" name="automationStatusVerify"
														checked="" disabled> <label for="inlineRadio1">
														Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" value="2" name="automationStatusVerify"
														disabled> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Git Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" value="1" name="automationStatusVerify"
														checked="" disabled> <label for="inlineRadio1">
														Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" value="2" name="automationStatusVerify"
														disabled> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>

							<h3 class="text-info">Account Information</h3>
							<div class="row">
								<div class="alert alert-danger hidden" id="errorMessage">
									User already exists with this email id.</div>
								<div class="alert alert-danger hidden" id="errorMessage3">
									User already exists with this user name.</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>First name *</label> <input id="firstNameVerify"
											type="text" class="form-control characters" disabled>
									</div>
									<div class="form-group">
										<label>Last Name *</label> <input id="lastNameVerify"
											type="text" class="form-control characters" disabled>
									</div>
									<div class="form-group">
										<label>User name *</label> <input id="usernameLoginVerify"
											type="text" class="form-control required" disabled>
									</div>
									<div class="form-group">
										<label>Email *</label> <input id="emailIdVerify" type="email"
											class="form-control" disabled>
									</div>

								</div>
							</div>

						</fieldset>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>
<style>
.margine-lable {
	margin-bottom: 2.0em !important;
}
</style>
<script type="text/javascript">
	$(window).load(function() {
		$("#spinner").fadeOut("slow");
	})
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});
</script>
<script>
	var customerName;
	var customerAddress;
	var customerType;
	var customerBTStatus;
	var customerTMStatus;
	var customerAutomationStatus;
	var customerStatus;
	var custAdminFirstName;
	var custAdminLastName;
	var custAdminEmailId;
	var custUserName;
	$(document)
			.ready(
					function() {
						$("#wizard").steps();
						$("#form")
								.steps(
										{
											bodyTag : "fieldset",
											onStepChanging : function(event,
													currentIndex, newIndex) {
												// Always allow going backward even if the current step contains invalid fields!
												if (currentIndex > newIndex) {
													return true;
												}
												// Forbid suppressing "Warning" step if the user is to young
												if (newIndex === 3
														&& Number($("#age")
																.val()) < 18) {
													return false;
												}

												var form = $(this);

												// Clean up if user went backward before
												if (currentIndex < newIndex) {
													// To remove error styles
													$(
															".body:eq("
																	+ newIndex
																	+ ") label.error",
															form).remove();
													$(
															".body:eq("
																	+ newIndex
																	+ ") .error",
															form).removeClass(
															"error");
												}

												$("#errorMessage").addClass(
														'hidden');
												$(".errorMessage1").addClass(
														'hidden');
												$("#errorMessage2").addClass(
														'hidden');
												$("#errorMessage3").addClass(
														'hidden');

												// Disable validation on fields that are disabled or hidden.
												form.validate().settings.ignore = ":disabled,:hidden";

												// Start validation; Prevent going forward if false
												return form.valid();
											},
											onCanceled : function(event,
													currentIndex) {
												window.location.href = "viewcustomer";
											},
											onStepChanged : function(event,
													currentIndex, priorIndex) {
												// Suppress (skip) "Warning" step if the user is old enough.
												if (currentIndex === 2
														&& Number($("#age")
																.val()) >= 18) {
													$(this).steps("next");
												}
												if (currentIndex == 3) {
													customerName = $(
															"#custName").val();
													customerAddress = $(
															"#custAddress")
															.val();
													customerType = $(
															"#custType option:selected")
															.val();
													customerBTStatus = $(
															"input:radio[name=bugTrackerStatus]:checked")
															.val();
													customerTMStatus = $(
															"input:radio[name=testManagerStatus]:checked")
															.val();
													customerAutomationStatus = $(
															"input:radio[name=automationStatus]:checked")
															.val();
													customerStatus = $(
															"input:radio[name=custStatus]:checked")
															.val();
													custAdminFirstName = $(
															"#firstName").val();
													custAdminLastName = $(
															"#lastName").val();
													custUserName = $(
															"#usernameLogin")
															.val();
													custAdminEmailId = $(
															"#emailId").val();
													$('#custNameVerify').val(
															customerName);
													$('#custAddressVerify')
															.val(
																	customerAddress);
													$('#firstNameVerify').val(
															custAdminFirstName);
													$('#lastNameVerify').val(
															custAdminLastName);
													$('#emailIdVerify').val(
															custAdminEmailId);
													$('#usernameLoginVerify')
															.val(custUserName);

													$(
															"input[name=custStatusVerify][value="
																	+ customerStatus
																	+ "]")
															.prop('checked',
																	true);
													$(
															"input[name=bugTrackerStatusVerify][value="
																	+ customerBTStatus
																	+ "]")
															.prop('checked',
																	true);
													$(
															"input[name=testManagerStatusVerify][value="
																	+ customerTMStatus
																	+ "]")
															.prop('checked',
																	true);
													$(
															"input[name=automationStatusVerify][value="
																	+ customerAutomationStatus
																	+ "]")
															.prop('checked',
																	true);

													$("#custTypeVerify").val(
															customerType);
												}

												// Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.

											},
											onFinishing : function(event,
													currentIndex) {
												var form = $(this);

												// Disable validation on fields that are disabled.
												// At this point it's recommended to do an overall check (mean ignoring only disabled fields)
												form.validate().settings.ignore = ":disabled";

												// Start validation; Prevent form submission if false
												return form.valid();
											},
											onFinished : function(event,
													currentIndex) {
												var form = $(this);
												$("#spinner").fadeIn("slow");
												$
														.post(
																"checkuser",
																{
																	emailId : $(
																			"#emailId")
																			.val(),
																	userName : $(
																			"#usernameLogin")
																			.val()
																},
																function(
																		response) {

																	var responseJson = JSON
																			.parse(response);
																	if (responseJson.emailFlag == "true"
																			&& responseJson.usernameFlag) {
																		form
																				.submit();
																	} else if (responseJson.emailFlag == "false") {

																		$(
																				"#barInMenu")
																				.addClass(
																						"hidden");
																		$(
																				"#wrapper")
																				.removeClass(
																						"hidden");

																		$(
																				"#submittedEmail")
																				.addClass(
																						'error');
																		$(
																				"#emailIdVerify")
																				.addClass(
																						'error');
																		$(
																				"#errorMessage")
																				.removeClass(
																						'hidden');
																		$(
																				".errorMessage1")
																				.removeClass(
																						'hidden');
																	} else if (!responseJson.usernameFlag) {

																		$(
																				"#barInMenu")
																				.addClass(
																						"hidden");
																		$(
																				"#wrapper")
																				.removeClass(
																						"hidden");

																		$(
																				"#usernameLoginVerify")
																				.addClass(
																						'error');
																		$(
																				"#usernameLogin")
																				.addClass(
																						'error');
																		$(
																				"#errorMessage2")
																				.removeClass(
																						'hidden');
																		$(
																				"#errorMessage3")
																				.removeClass(
																						'hidden');
																	}
																});

												// alert( "Handler for .click() called." );
											}
										}).validate({
									errorPlacement : function(error, element) {
										element.before(error);
									},
									rules : {
										firstName : {
											required : true,
											minlength : 3
										},
										emailId : {
											required : true,
										},
										lastName : {
											required : true,
											minlength : 3
										},
										emailId : {
											required : true
										}

									}
								});
					});

	$(document).ready(function() {
		$('#activePlugin,#inactivePlugin').change(function() {
			if (this.value == 1) {
				$("#pluginDiv").removeClass("hidden");
			}
			if (this.value == 2) {
				$("#pluginDiv").addClass("hidden");
			}

		});

	});
</script>
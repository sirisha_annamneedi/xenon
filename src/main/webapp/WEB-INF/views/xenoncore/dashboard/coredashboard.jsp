
<div class="wrapper wrapper-content animated fadeInUp">
	<div class="row">
		<div class="col-lg-3">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<span class="label label-success pull-right">Monthly</span>
					<h5>Builds</h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins">40</h1>
					<div class="stat-percent font-bold text-success">
						98% <i class="fa fa-bolt"></i>
					</div>
					<small>Total Builds</small>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<span class="label label-info pull-right">Annual</span>
					<h5>Releases</h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins">12</h1>
					<div class="stat-percent font-bold text-info">
						20% <i class="fa fa-level-up"></i>
					</div>
					<small>Add Releases</small>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<span class="label label-primary pull-right">Today</span>
					<h5>Test cases</h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins">106/120</h1>
					<div class="stat-percent font-bold text-navy">
						44% <i class="fa fa-level-up"></i>
					</div>
					<small>Pass / Fail</small>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<span class="label label-danger pull-right">Low value</span>
					<h5>Bugs</h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins">17</h1>
					<div class="stat-percent font-bold text-danger">
						38% <i class="fa fa-level-down"></i>
					</div>
					<small>In first month</small>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Orders</h5>
					<div class="pull-right">
						<div class="btn-group">
							<button type="button" class="btn btn-xs btn-white active">Today</button>
							<button type="button" class="btn btn-xs btn-white">Monthly</button>
							<button type="button" class="btn btn-xs btn-white">Annual</button>
						</div>
					</div>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-lg-9">
							<div class="flot-chart">
								<div class="flot-chart-content" id="flot-dashboard-chart"
									style="padding: 0px; position: relative;">
									<canvas class="flot-base" width="1187" height="200"
										style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1187px; height: 200px;"></canvas>
									<div class="flot-text"
										style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
										<div class="flot-x-axis flot-x1-axis xAxis x1Axis"
											style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
											<div class="flot-tick-label tickLabel"
												style="position: absolute; max-width: 98px; top: 185px; left: 108px; text-align: center;">Jan
												03</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; max-width: 98px; top: 185px; left: 220px; text-align: center;">Jan
												06</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; max-width: 98px; top: 185px; left: 331px; text-align: center;">Jan
												09</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; max-width: 98px; top: 185px; left: 442px; text-align: center;">Jan
												12</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; max-width: 98px; top: 185px; left: 553px; text-align: center;">Jan
												15</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; max-width: 98px; top: 185px; left: 665px; text-align: center;">Jan
												18</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; max-width: 98px; top: 185px; left: 776px; text-align: center;">Jan
												21</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; max-width: 98px; top: 185px; left: 887px; text-align: center;">Jan
												24</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; max-width: 98px; top: 185px; left: 999px; text-align: center;">Jan
												27</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; max-width: 98px; top: 185px; left: 1110px; text-align: center;">Jan
												30</div>
										</div>
										<div class="flot-y-axis flot-y1-axis yAxis y1Axis"
											style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
											<div class="flot-tick-label tickLabel"
												style="position: absolute; top: 173px; left: 19px; text-align: right;">0</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; top: 132px; left: 6px; text-align: right;">250</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; top: 92px; left: 6px; text-align: right;">500</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; top: 52px; left: 6px; text-align: right;">750</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; top: 12px; left: 0px; text-align: right;">1000</div>
										</div>
										<div class="flot-y-axis flot-y2-axis yAxis y2Axis"
											style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
											<div class="flot-tick-label tickLabel"
												style="position: absolute; top: 173px; left: 1175px;">0</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; top: 144px; left: 1175px;">5</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; top: 115px; left: 1175px;">10</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; top: 87px; left: 1175px;">15</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; top: 58px; left: 1175px;">20</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; top: 29px; left: 1175px;">25</div>
											<div class="flot-tick-label tickLabel"
												style="position: absolute; top: 1px; left: 1175px;">30</div>
										</div>
									</div>
									<canvas class="flot-overlay" width="1187" height="200"
										style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1187px; height: 200px;"></canvas>
									<div class="legend">
										<div
											style="position: absolute; width: 111px; height: 30px; top: 13px; left: 35px; opacity: 0.85; background-color: rgb(255, 255, 255);">
										</div>
										<table
											style="position: absolute; top: 13px; left: 35px;; font-size: smaller; color: #545454">
											<tbody>
												<tr>
													<td class="legendColorBox"><div
															style="border: 1px solid #000000; padding: 1px">
															<div
																style="width: 4px; height: 0; border: 5px solid #1ab394; overflow: hidden"></div>
														</div></td>
													<td class="legendLabel">Number of orders</td>
												</tr>
												<tr>
													<td class="legendColorBox"><div
															style="border: 1px solid #000000; padding: 1px">
															<div
																style="width: 4px; height: 0; border: 5px solid #1C84C6; overflow: hidden"></div>
														</div></td>
													<td class="legendLabel">Payments</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3">
							<ul class="stat-list">
								<li>
									<h2 class="no-margins">2,346</h2> <small>Total orders
										in period</small>
									<div class="stat-percent">
										48% <i class="fa fa-level-up text-navy"></i>
									</div>
									<div class="progress progress-mini">
										<div style="width: 48%;" class="progress-bar"></div>
									</div>
								</li>
								<li>
									<h2 class="no-margins ">4,422</h2> <small>Orders in
										last month</small>
									<div class="stat-percent">
										60% <i class="fa fa-level-down text-navy"></i>
									</div>
									<div class="progress progress-mini">
										<div style="width: 60%;" class="progress-bar"></div>
									</div>
								</li>
								<li>
									<h2 class="no-margins ">9,180</h2> <small>Monthly
										income from orders</small>
									<div class="stat-percent">
										22% <i class="fa fa-bolt text-navy"></i>
									</div>
									<div class="progress progress-mini">
										<div style="width: 22%;" class="progress-bar"></div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="animated fadeInUp">

				<div class="ibox">
					<div class="ibox-title">
						<h5>All Applications assigned to this account</h5>
						<div class="ibox-tools">
							<a href="" class="btn btn-primary btn-xs">Add Application</a>
						</div>
					</div>
					<div class="ibox-content">
						<div class="row m-b-sm m-t-sm">
							<div class="col-md-1">
								<button type="button" id="loading-example-btn"
									class="btn btn-white btn-sm">
									<i class="fa fa-refresh"></i> Refresh
								</button>
							</div>
							<div class="col-md-11">
								<div class="input-group">
									<input type="text" placeholder="Search"
										class="input-sm form-control"> <span
										class="input-group-btn">
										<button type="button" class="btn btn-sm btn-primary">
											Go!</button>
									</span>
								</div>
							</div>
						</div>

						<div class="project-list">

							<table class="table table-hover">
								<tbody>
									<tr>
										<td class="project-status"><span
											class="label label-primary">Active</span></td>
										<td class="project-title"><a href="project_detail.html">Contract
												with Zender Company</a> <br> <small>Created
												14.08.2014</small></td>
										<td class="project-completion"><small>Completion
												with: 48%</small>
											<div class="progress progress-mini">
												<div style="width: 48%;" class="progress-bar"></div>
											</div></td>

										<td class="project-actions"><a href="#"
											class="btn btn-white btn-sm"><i class="fa fa-folder"></i>
												View </a> <a href="#" class="btn btn-white btn-sm"><i
												class="fa fa-pencil"></i> Edit </a></td>
									</tr>
									<tr>
										<td class="project-status"><span
											class="label label-primary">Active</span></td>
										<td class="project-title"><a href="project_detail.html">There
												are many variations of passages</a> <br> <small>Created
												11.08.2014</small></td>
										<td class="project-completion"><small>Completion
												with: 28%</small>
											<div class="progress progress-mini">
												<div style="width: 28%;" class="progress-bar"></div>
											</div></td>

										<td class="project-actions"><a href="#"
											class="btn btn-white btn-sm"><i class="fa fa-folder"></i>
												View </a> <a href="#" class="btn btn-white btn-sm"><i
												class="fa fa-pencil"></i> Edit </a></td>
									</tr>
									<tr>
										<td class="project-status"><span
											class="label label-default">Inactive</span></td>
										<td class="project-title"><a href="project_detail.html">Many
												desktop publishing packages and web</a> <br> <small>Created
												10.08.2014</small></td>
										<td class="project-completion"><small>Completion
												with: 8%</small>
											<div class="progress progress-mini">
												<div style="width: 8%;" class="progress-bar"></div>
											</div></td>

										<td class="project-actions"><a href="#"
											class="btn btn-white btn-sm"><i class="fa fa-folder"></i>
												View </a> <a href="#" class="btn btn-white btn-sm"><i
												class="fa fa-pencil"></i> Edit </a></td>
									</tr>


								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="row">

		<div class="col-lg-6">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>User builds list</h5>
					<div class="ibox-tools">
						<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
						</a> <a class="close-link"> <i class="fa fa-times"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content">
					<table class="table table-hover no-margins">
						<thead>
							<tr>
								<th>Build Name</th>
								<th>Status</th>
								<th>Start Date</th>
								<th>User</th>
								<th>% Completed</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Build 1</td>
								<td><small>In progress</small></td>
								<td><i class="fa fa-clock-o"></i> 11:20pm</td>
								<td>Samantha</td>
								<td class="text-navy"><i class="fa fa-level-up"></i> 24%</td>
							</tr>
							<tr>
								<td>Build 2</td>
								<td><span class="label label-warning">Waiting</span></td>
								<td><i class="fa fa-clock-o"></i> 10:40am</td>
								<td>Monica</td>
								<td class="text-navy"><i class="fa fa-level-up"></i> 66%</td>
							</tr>
							<tr>
								<td>Build 3</td>
								<td><small>In progress</small></td>
								<td><i class="fa fa-clock-o"></i> 01:30pm</td>
								<td>John</td>
								<td class="text-navy"><i class="fa fa-level-up"></i> 54%</td>
							</tr>
							<tr>
								<td>Build 4</td>
								<td><small>In progress</small></td>
								<td><i class="fa fa-clock-o"></i> 02:20pm</td>
								<td>Agnes</td>
								<td class="text-navy"><i class="fa fa-level-up"></i> 12%</td>
							</tr>
							<tr>
								<td>Build 5</td>
								<td><small>In progress</small></td>
								<td><i class="fa fa-clock-o"></i> 09:40pm</td>
								<td>Janet</td>
								<td class="text-navy"><i class="fa fa-level-up"></i> 22%</td>
							</tr>
							<tr>
								<td>Build 6</td>
								<td><span class="label label-primary">Completed</span></td>
								<td><i class="fa fa-clock-o"></i> 04:10am</td>
								<td>Amelia</td>
								<td class="text-navy"><i class="fa fa-level-up"></i> 66%</td>
							</tr>
							<tr>
								<td>Build 7</td>
								<td><small>In progress</small></td>
								<td><i class="fa fa-clock-o"></i> 12:08am</td>
								<td>Damian</td>
								<td class="text-navy"><i class="fa fa-level-up"></i> 23%</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Recent Activities</h5>
					<div class="ibox-tools">
						<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
						</a> <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i
							class="fa fa-wrench"></i>
						</a>
						<ul class="dropdown-menu dropdown-user">
							<li><a href="#">Config option 1</a></li>
							<li><a href="#">Config option 2</a></li>
						</ul>
						<a class="close-link"> <i class="fa fa-times"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content no-padding">
					<ul class="list-group">
						<li class="list-group-item">
							<p>
								<a class="text-info" href="#">@Alan Marry</a> I belive that.
								Lorem Ipsum is simply dummy text of the printing and typesetting
								industry.
							</p> <small class="block text-muted"><i class="fa fa-clock-o"></i>
								1 minuts ago</small>
						</li>
						<li class="list-group-item">
							<p>
								<a class="text-info" href="#">@Stock Man</a> Check this stock
								chart. This price is crazy!
							</p> <small class="block text-muted"><i class="fa fa-clock-o"></i>
								2 hours ago</small>
						</li>
						<li class="list-group-item">
							<p>
								<a class="text-info" href="#">@Kevin Smith</a> Lorem ipsum
								unknown printer took a galley
							</p> <small class="block text-muted"><i class="fa fa-clock-o"></i>
								2 minuts ago</small>
						</li>
						<li class="list-group-item ">
							<p>
								<a class="text-info" href="#">@Jonathan Febrick</a> The standard
								chunk of Lorem Ipsum
							</p> <small class="block text-muted"><i class="fa fa-clock-o"></i>
								1 hour ago</small>
						</li>
						<li class="list-group-item ">
							<p>
								<a class="text-info" href="#">@Jonathan Febrick</a> The standard
								chunk of Lorem Ipsum
							</p> <small class="block text-muted"><i class="fa fa-clock-o"></i>
								1 hour ago</small>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
      
        var data2 = [
                     [gd(2012, 1, 1), 7], [gd(2012, 1, 2), 6], [gd(2012, 1, 3), 4], [gd(2012, 1, 4), 8],
                     [gd(2012, 1, 5), 9], [gd(2012, 1, 6), 7], [gd(2012, 1, 7), 5], [gd(2012, 1, 8), 4],
                     [gd(2012, 1, 9), 7], [gd(2012, 1, 10), 8], [gd(2012, 1, 11), 9], [gd(2012, 1, 12), 6],
                     [gd(2012, 1, 13), 4], [gd(2012, 1, 14), 5], [gd(2012, 1, 15), 11], [gd(2012, 1, 16), 8],
                     [gd(2012, 1, 17), 8], [gd(2012, 1, 18), 11], [gd(2012, 1, 19), 11], [gd(2012, 1, 20), 6],
                     [gd(2012, 1, 21), 6], [gd(2012, 1, 22), 8], [gd(2012, 1, 23), 11], [gd(2012, 1, 24), 13],
                     [gd(2012, 1, 25), 7], [gd(2012, 1, 26), 9], [gd(2012, 1, 27), 9], [gd(2012, 1, 28), 8],
                     [gd(2012, 1, 29), 5], [gd(2012, 1, 30), 8], [gd(2012, 1, 31), 25]
                 ];

                 var data3 = [
                     [gd(2012, 1, 1), 800], [gd(2012, 1, 2), 500], [gd(2012, 1, 3), 600], [gd(2012, 1, 4), 700],
                     [gd(2012, 1, 5), 500], [gd(2012, 1, 6), 456], [gd(2012, 1, 7), 800], [gd(2012, 1, 8), 589],
                     [gd(2012, 1, 9), 467], [gd(2012, 1, 10), 876], [gd(2012, 1, 11), 689], [gd(2012, 1, 12), 700],
                     [gd(2012, 1, 13), 500], [gd(2012, 1, 14), 600], [gd(2012, 1, 15), 700], [gd(2012, 1, 16), 786],
                     [gd(2012, 1, 17), 345], [gd(2012, 1, 18), 888], [gd(2012, 1, 19), 888], [gd(2012, 1, 20), 888],
                     [gd(2012, 1, 21), 987], [gd(2012, 1, 22), 444], [gd(2012, 1, 23), 999], [gd(2012, 1, 24), 567],
                     [gd(2012, 1, 25), 786], [gd(2012, 1, 26), 666], [gd(2012, 1, 27), 888], [gd(2012, 1, 28), 900],
                     [gd(2012, 1, 29), 178], [gd(2012, 1, 30), 555], [gd(2012, 1, 31), 993]
                 ];


                 var dataset = [
                     {
                         label: "Number of orders",
                         data: data3,
                         color: "#1ab394",
                         bars: {
                             show: true,
                             align: "center",
                             barWidth: 24 * 60 * 60 * 600,
                             lineWidth:0
                         }

                     }, {
                         label: "Payments",
                         data: data2,
                         yaxis: 2,
                         color: "#1C84C6",
                         lines: {
                             lineWidth:1,
                                 show: true,
                                 fill: true,
                             fillColor: {
                                 colors: [{
                                     opacity: 0.2
                                 }, {
                                     opacity: 0.4
                                 }]
                             }
                         },
                         splines: {
                             show: false,
                             tension: 0.6,
                             lineWidth: 1,
                             fill: 0.1
                         },
                     }
                 ];


                 var options = {
                     xaxis: {
                         mode: "time",
                         tickSize: [3, "day"],
                         tickLength: 0,
                         axisLabel: "Date",
                         axisLabelUseCanvas: true,
                         axisLabelFontSizePixels: 12,
                         axisLabelFontFamily: 'Arial',
                         axisLabelPadding: 10,
                         color: "#d5d5d5"
                     },
                     yaxes: [{
                         position: "left",
                         max: 1070,
                         color: "#d5d5d5",
                         axisLabelUseCanvas: true,
                         axisLabelFontSizePixels: 12,
                         axisLabelFontFamily: 'Arial',
                         axisLabelPadding: 3
                     }, {
                         position: "right",
                         clolor: "#d5d5d5",
                         axisLabelUseCanvas: true,
                         axisLabelFontSizePixels: 12,
                         axisLabelFontFamily: ' Arial',
                         axisLabelPadding: 67
                     }
                     ],
                     legend: {
                         noColumns: 1,
                         labelBoxBorderColor: "#000000",
                         position: "nw"
                     },
                     grid: {
                         hoverable: false,
                         borderWidth: 0
                     }
                 };

                 function gd(year, month, day) {
                     return new Date(year, month - 1, day).getTime();
                 }

                 var previousPoint = null, previousLabel = null;

                 $.plot($("#flot-dashboard-chart"), dataset, options);
                 
                 $(document).ready(function(){
                     $('.dataTables-example').DataTable({
                         dom: '<"html5buttons"B>lTfgitp',
                         buttons: [
                             { extend: 'copy'},
                             {extend: 'csv'},
                             {extend: 'excel', title: 'ExampleFile'},
                             {extend: 'pdf', title: 'ExampleFile'},

                             {extend: 'print',
                              customize: function (win){
                                     $(win.document.body).addClass('white-bg');
                                     $(win.document.body).css('font-size', '10px');

                                     $(win.document.body).find('table')
                                             .addClass('compact')
                                             .css('font-size', 'inherit');
                             }
                             }
                         ]

                     });

                 });
		localStorage.setItem("CustomerView", "0");

    </script>
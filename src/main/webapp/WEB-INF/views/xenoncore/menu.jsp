<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="wrapper">

	<nav class="navbar-default navbar-static-side topMargin60"
		role="navigation">
		<div class="sidebar-collapse">
			<ul class="nav metismenu" id="side-menu">
				<li class="nav-header">
					<div class="dropdown profile-element">
						<span><img alt="image" class="img-circle"
							style="width: 48px; height: 48px"
							src="data:image/jpg;base64,${Model.UserProfilePhoto } " /> </span> <a
							data-toggle="dropdown" class="dropdown-toggle" href="#"> </a> <span
							class="clear"> </span> <a href="profile"> <span class="clear">
								<span class="block m-t-xs"> <strong class="font-bold">${Model.userName }</strong>
							</span> <span class="text-muted text-xs block"><b>${Model.role}
								</b></span>
						</span>
						</a>

						<ul class="dropdown-menu animated fadeInRight m-t-xs">
							<li><a href="profile.html">Profile</a></li>
							<li><a href="contacts.html">Contacts</a></li>
							<li><a href="mailbox.html">Mailbox</a></li>
							<li class="divider"></li>
							<li><a href="logout">Logout</a></li>
						</ul>
					</div>
					<div class="logo-element">IN+</div>
				</li>
				<li class="active"><a href="index.html"><i
						class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span>
						<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level collapse">
						<li><a href="createcustomer">Add Customer</a></li>
						<li><a href="viewcustomer">View Customer</a></li>
					</ul></li>
				<li class="active"><a href="cloudvmdetails"><i
						class="fa fa-th-large"></i> <span class="nav-label">Virtual
							Machines</span> </a></li>
			</ul>

		</div>
	</nav>
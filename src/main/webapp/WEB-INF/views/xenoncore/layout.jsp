<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Cache-control" content="private">
<link rel="icon" type="image/png"
	href="<%=request.getContextPath()%>/resources/img/xenon.png">
<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/font-awesome/css/font-awesome.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/iCheck/custom.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/steps/jquery.steps.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/resources/css/animate.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/resources/css/style.css"
	rel="stylesheet">
	

	
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/chosen/chosen.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/resources/css/animate.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/resources/css/style.css"
	rel="stylesheet">

<link
	href="<%=request.getContextPath()%>/resources/css/plugins/dataTables/datatables.min.css"
	rel="stylesheet">

<link
	href="<%=request.getContextPath()%>/resources/css/plugins/colorpicker/bootstrap-colorpicker.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/clockpicker/clockpicker.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/datapicker/datepicker3.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/select2/select2.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/switchery/switchery.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/bootstrap-markdown/bootstrap-markdown.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/daterangepicker/daterangepicker-bs3.css"
	rel="stylesheet">
	  <link href="<%=request.getContextPath()%>/resources/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
	
<!-- xenon custom style sheet -->
<link href="<%=request.getContextPath()%>/resources/css/xenon_custom.css"
	rel="stylesheet">
<title><tiles:insertAttribute name="title" ignore="true" /></title>
<style>
#spinner {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background:
		url("<%=request.getContextPath()%>/resources/img/page-loader.gif") 50%
		50% no-repeat rgb(249, 249, 249);
}
</style>
</head>

<!-- Mainly scripts -->
<script src="<%=request.getContextPath()%>/resources/js/jquery-2.1.1.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/dataTables/datatables.min.js"></script>


<!-- Custom and plugin javascript -->
<script src="<%=request.getContextPath()%>/resources/js/inspinia.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/pace/pace.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<script
	src="<%=request.getContextPath()%>/resources/js/plugins/chosen/chosen.jquery.js"></script>
<!-- Steps -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/staps/jquery.steps.min.js"></script>

<!-- Jquery Validate -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/validate/jquery.validate.min.js"></script>

<!-- Data picker -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- Select2 -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/select2/select2.full.min.js"></script>

<!-- Switchery -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/switchery/switchery.js"></script>

<!-- Bootstrap markdown -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/bootstrap-markdown/bootstrap-markdown.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/bootstrap-markdown/markdown.js"></script>


<!-- Date range use moment.js same as full calendar plugin -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/fullcalendar/moment.min.js"></script>

<!-- Date range picker -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/daterangepicker/daterangepicker.js"></script>


<!-- Flot -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/flot/jquery.flot.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/flot/jquery.flot.spline.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/flot/jquery.flot.resize.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/flot/jquery.flot.pie.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/flot/jquery.flot.symbol.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/flot/jquery.flot.time.js"></script>

<!-- SUMMERNOTE -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/summernote/summernote.min.js"></script>
<!-- Jquery Validate -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/validate/jquery.validate.min.js"></script>
<!-- ChartJS-->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/chartJs/Chart.min.js"></script>
	
	  <!-- iCheck -->
 <script src="<%=request.getContextPath()%>/resources/js/plugins/iCheck/icheck.min.js"></script>
 <script src="<%=request.getContextPath()%>/resources/js/buildmanager/build/addtestcase.js"></script>

<body class="fixed-sidebar pace-done fixed-nav fixed-nav-basic md-skin">
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="body" />
	<tiles:insertAttribute name="footer" />

</body>

</html>
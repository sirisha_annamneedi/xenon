<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
	<div class="col-lg-8">
		<h2>Git</h2>
		<ol class="breadcrumb">
			<li><a href="admindashboard">Dashboard</a></li>
				<li><a href="viewgitprojects">Git</a></li>
				<li class="Active"><strong>View Repositories</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Git Instances</h5>
					<div class="ibox-tools">
						
					</div>
				</div>


				<div class="ibox-content">
					<div class="">
						<div class="tab-content">
							<div class="tab-pane active">
								<div class="full-height-scroll">
									<div class="table-responsive" style="overflow-x: visible;">
										<table class="table table-striped table-hover"
											id="viewModulesTable">
											<thead>
												<tr>
													<th>Repository Name</th>
													<th>Project Name</th>
													<th>Project Url</th>
													<th>Branch Name</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="row" items="${model.repoDetails}">
													<tr data-toggle="tab">
														<td class="xlTextAlignment"
															data-original-title="${row.repo_name}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${row.repo_name}</td>

														<td class="xlTextAlignment"
															data-original-title="${row.git_instance_name}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${row.git_instance_name}</td>

														<td class="xlTextAlignment"
															data-original-title="${row.git_server_url}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${row.git_server_url}</td>

														<td class="xlTextAlignment"
															data-original-title="${row.branch_name}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${row.branch_name}</td>
														<td>
														 <c:if test="${model.updatePermission == '1'}">
																<button type="button" class="btn btn-primary btn-xs"
																	onclick="editRepo(${row.repo_id},${model.projectId})">Settings</button>
															</c:if>
																
												</td>
													</tr>
											</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
	
	function editRepo(id,projId){
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		var posting = $.post('setrepositoryid',{
			repositoryId : id,
			projectId : projId
		});
		posting.done(function(data){
			window.location.href="editgitrepository";
		});
	}
	
	
</script>
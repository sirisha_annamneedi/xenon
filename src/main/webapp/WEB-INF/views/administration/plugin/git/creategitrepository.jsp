<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>New Git Repository</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li><a href="viewgitprojects">Git</a></li>
				<li class="Active"><strong>New Repository</strong></li>
			</ol>
		</div>
	</div>

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-10">
				<div class="ibox">
					<div class="ibox-content">
						<form action="insertgitrepository" id="createProjectForm"
							class="wizard-big wizard clearfix form-horizontal" method="POST">
							<div class="content clearfix">
								<fieldset class="body current">
									<div class="row">
										<label class="col-lg-4 pull-right text-right">* fields
											are mandatory</label>
									</div>
									<div class="row">
										<div class="col-lg-10">

											<div class="form-group">
												<label class="control-label col-sm-3">Repository
													Name :* </label>
												<div class="col-sm-8">
													<input type="text" placeholder="Enter repository name"
														class="form-control " name="repositoryName"
														id="repositoryName" tabindex="2">
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-sm-3">Git Project
													Name :* </label>
												<div class="col-sm-5">
													<select class="chosen-select" name="instanceList"
														id="instanceList" onchange="validateLabelValue(this);">
														<c:forEach var="List" items="${model.instanceDetails}">
															<c:if test="${List.git_instance_id == model.projectId}">
																<option value="${List.git_instance_id}">${List.git_instance_name}</option>
															</c:if>
														</c:forEach>
													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-sm-3">Project Url :*
												</label>
												<div class="col-sm-8">
													<c:forEach var="List" items="${model.instanceDetails}">
														<c:if test="${List.git_instance_id == model.projectId}">
															<label class="control-label hidden" name="gitUrlLabel"
																id="gitUrlLabel_${List.git_instance_id}">${List.git_server_url}</label>
														</c:if>
													</c:forEach>
												</div>
											</div>


											<div class="form-group">
												<label class="control-label col-sm-3">Branch Name :*
												</label>
												<div class="col-sm-8">
													<input type="text" placeholder="Enter branch name"
														class="form-control " name="branchName" id="branchName"
														tabindex="2">
												</div>
											</div>
										</div>
									</div>
									<div class="hr-line-solid"></div>
								</fieldset>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-6">
										<button class="btn btn-white pull-left"
											style="margin-right: 15px;" type="button"
											id="createEnvCancelBtn"
											onclick="window.location.href='viewgitprojects';"
											tabindex="7">Cancel</button>
										<button class=" btn btn-success pull-left" style="margin-right: 15px;"
											data-style="slide-up" type="button" tabindex="8"
											id="saveRepositoryBtn">Save Repository</button>
									</div>
								</div>

							</div>
						</form>
						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<!-- end row -->
	</div>
	<!-- end wrapper -->
</div>
<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
		$('.chosen-select').chosen().trigger('chosen:updated');

		$('label[name=gitUrlLabel]').addClass("hidden");
		var selectedItem = 'gitUrlLabel_' + $("#instanceList").val();
		$("#" + selectedItem).removeClass("hidden");
	});

	function validateLabelValue(selectedIndex) {
		$('label[name=gitUrlLabel]').addClass("hidden");
		var selectedItem = 'gitUrlLabel_' + selectedIndex.value;
		$("#" + selectedItem).removeClass("hidden");

	}
</script>


<script>
	$(document).ready(function() {

		$("#createGitForm").validate({
			rules : {
				gitName : {
					required : true,
					minlength : 1,
					maxlength : 100
				},

				instanceList : {
					required : true,
					minlength : 1,
					maxlength : 100
				},
				branchName : {
					required : true,
					minlength : 1,
					maxlength : 40
				}
			}
		});

	});

	$("#saveRepositoryBtn").click(function() {
		$("#createProjectForm").submit();

	});
</script>

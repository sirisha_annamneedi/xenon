<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>New Git Project</h2>
			<ol class="breadcrumb">
				<li><a href="bmdashboard">Dashboard</a></li>
				<li><a href="viewgitprojects">Git</a></li>
				<li class="Active"><strong>Add new project</strong></li>
			</ol>
		</div>
	</div>

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content">
						<form action="insertgitproject" id="createGitProjectForm"
							class="wizard-big wizard clearfix form-horizontal" method="POST">
							<div class="content clearfix">
								<fieldset class="body current">
									<div class="row">
										<label class="col-lg-4 pull-right text-right">* fields
											are mandatory</label>
									</div>
									<div class="row">
										<div class="col-lg-10">


											<div class="form-group">
												<label class="control-label col-sm-2">Git Project
													Name:* </label>
												<div class="col-sm-10">
													<input type="text" placeholder="Enter git project name"
														class="form-control" name="gitProjectName" tabindex="1">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-2">Project Url :*
												</label>
												<div class="col-sm-10">
													<input type="text" placeholder="Enter project url"
														class="form-control " name="gitUrl" id="gitUrl"
														tabindex="2">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-2">Username :* </label>
												<div class="col-sm-10">
													<input type="text" placeholder="Enter username"
														class="form-control " name="Username" id="Username"
														tabindex="2">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-2">API Token :* </label>
												<div class="col-sm-10">
													<input type="text" placeholder="Enter API token"
														class="form-control " name="apiToken" id="apiToken"
														tabindex="2">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Status:</label>
												<div class="col-sm-10">
													<div class="radio radio-success radio-inline col-sm-2">
														<input type="radio" id="active" value="1" name="gitStatus"
															checked=""> <label for="active">Active</label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-2">
														<input type="radio" id="inactive" value="2"
															name="gitStatus"> <label for="active">Inactive</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<button class="pull-right btn btn-success"
													data-style="slide-up" type="button" tabindex="8"
													id="testConnection">Test</button>
												<input class="hidden" id="setTestStatus">
											</div>
										</div>
									</div>
									<div class="hr-line-solid"></div>
								</fieldset>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-4">
										<button class="btn btn-white pull-left"
											style="margin-right: 15px;" type="button"
											id="createEnvCancelBtn"
											onclick="window.location.href='viewgitinstances';"
											tabindex="7">Cancel</button>
										<button class="pull-left btn btn-success"
											data-style="slide-up" type="button" tabindex="8"
											id="submitBtn">Submit</button>
									</div>
								</div>

							</div>
						</form>
						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<!-- end row -->
	</div>
	<!-- end wrapper -->
</div>
<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
</script>


<script>
	$(document).ready(function() {

		$("#createGitProjectForm").validate({
			rules : {
				gitProjectName : {
					required : true,
					minlength : 1,
					maxlength : 100
				},

				gitUrl : {
					required : true,
					minlength : 1,
					maxlength : 100
				},
				Username : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				apiToken : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
			}
		});

	});

	$("#submitBtn").click(function() {
		if ($("#createGitProjectForm").valid()) {
			submitDetails();
		}
	});

	$("#testConnection").click(function() {
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");
		$.ajax({
			url : "testGitConnection",
			data : {
				gitUrl : $("#gitUrl").val(),
				username : $("#Username").val(),
				apiToken : $("#apiToken").val()
			},
			type : "POST",
		}).done(function(data) {
			$('body').removeClass("white-bg");
			$("#barInMenu").addClass("hidden");
			$("#wrapper").removeClass("hidden");
			if (data == 201) {
				$("#setTestStatus").val(1);
				swal({
					title : "Success",
					text : "Connection tested successfully",
					type : "success",
					closeOnConfirm : true
				}, function(isConfirm) {
				});
			} else if (data == 500) {

				$("#setTestStatus").val(0);
				swal({
					title : "Unauthorized",
					text : "Please enter correct credentials",
					type : "error",
					closeOnConfirm : true
				}, function(isConfirm) {
				});
			}
		});
	});

	function submitDetails() {
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");
		$.ajax({
			url : "testGitConnection",
			data : {
				gitUrl : $("#gitUrl").val(),
				username : $("#Username").val(),
				apiToken : $("#apiToken").val()
			},
			type : "POST",
		}).done(function(data) {
			$('body').removeClass("white-bg");
			$("#barInMenu").addClass("hidden");
			$("#wrapper").removeClass("hidden");
			if (data == 201) {
				$('body').addClass("white-bg");
				$("#barInMenu").removeClass("hidden");
				$("#wrapper").addClass("hidden");
				$("#createGitProjectForm").submit();
			} else if (data == 500) {

				$("#setTestStatus").val(2);
				swal({
					title : "Unauthorized",
					text : "Please enter correct credentials",
					type : "error",
					closeOnConfirm : true
				}, function(isConfirm) {
				});
			}
		});
	}
</script>

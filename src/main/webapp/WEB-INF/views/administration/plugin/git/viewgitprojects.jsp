<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<h2>Git Projects</h2>
		<ol class="breadcrumb">
			<li><a href="admindashboard">Dashboard</a></li>
			<li class="Active"><strong>Git</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Git Project</h5>
					<div class="ibox-tools">
						<c:if test="${model.isAdminUser == 'true'}">
							<button type="button" class="btn btn-success btn-xs"
								onclick="location.href = 'addgitproject';">Add New
								Project</button>
						</c:if>
					</div>
				</div>


				<div class="ibox-content">
					<div class="">
						<div class="tab-content">
							<div class="tab-pane active">
								<div class="full-height-scroll">
									<div class="table-responsive" style="overflow-x: visible;">
										<table class="table table-striped table-hover"
											id="viewModulesTable">
											<thead>
												<tr>
													<th>Project Name</th>
													<th>Project URL</th>
													<th>API Token</th>
													<th>Username</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="row" items="${model.allRows}">
													<tr data-toggle="tab">
														<td class="xlTextAlignment"
															data-original-title="${row.git_instance_name}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${row.git_instance_name}</td>

														<td class="xlTextAlignment"
															data-original-title="${row.git_server_url}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${row.git_server_url}</td>

														<td class="xlTextAlignment"
															data-original-title="${row.api_token}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${row.api_token}</td>

														<td class="xlTextAlignment"
															data-original-title="${row.username}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${row.username}</td>
														<td><c:if test="${model.isAdminUser == 'true'}">
																<button type="button" class="btn btn-primary btn-xs"
																	onclick="editServer(${row.git_instance_id})">Settings</button>

															</c:if> <c:if test="${row.update_status == '1'}">
																<button type="button" class="btn btn-primary btn-xs"
																	onclick="createRepo(${row.git_instance_id})">New
																	Repository</button>
															</c:if>
															<button type="button" class="btn btn-primary btn-xs"
																id="viewrepo" onclick="viewrepo(${row.git_instance_id},${row.update_status})">View
																Repositories</button></td>

													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
	
	function editServer(id){
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		var posting = $.post('setgitinstanceid',{
			instanceId : id,
			updatePermission : 2
		});
		posting.done(function(data){
			window.location.href="gitprojectsetting";
		});
	}
	function viewrepo(id,updateStatus){
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		var posting = $.post('setgitinstanceid',{
			instanceId : id,
			updatePermission : updateStatus
		});
		posting.done(function(data){
			window.location.href="viewgitrepository";
		});
	};
	
	function createRepo(projId){
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		var posting = $.post('setgitprojectid',{
			projectId : projId
		});
		posting.done(function(data){
			window.location.href="creategitrepository";
		});
	}
</script>
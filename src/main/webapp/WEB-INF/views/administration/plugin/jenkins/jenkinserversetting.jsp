<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Jenkins Server Settings</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li><a href="viewjenkinsurls">Jenkins</a></li>
				<li class="Active"><strong>Settings</strong></li>
			</ol>
		</div>
	</div>

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="tabs-container">
					<ul class="nav nav-tabs">
						<li class=""><a data-toggle="tab" href="#detailsTab">
								Edit </a></li>
						<li class="active"><a data-toggle="tab" href="#membersTab">
								Members </a></li>
						<li class=""><a data-toggle="tab" href="#permissionsTab">
								Permissions </a></li>
					</ul>
					<div class="tab-content">
						<div id="detailsTab" class="tab-pane">
							<div class="panel-body">
								<div class="ibox">
									<div class="ibox-content">
										<form action="updatejenkinserver" method="POST"
											id="updateForm"
											class="wizard-big wizard clearfix form-horizontal">
											<div class="content clearfix">
												<fieldset class="body current">
													<div class="row">
														<label class="col-lg-4 pull-right text-right">*
															fields are mandatory</label>
													</div>
													<div class="row">
														<div class="col-lg-12">
															<div class="form-group">
																<label class="control-label col-sm-2">Server
																	Name *</label>
																<div class="col-sm-10">
																	<input type="text" placeholder="Server Name"
																		class="form-control characters" name="serverName"
																		tabindex="1" required="true"
																		value="${ServerDetails[0].jenkin_name}">
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-sm-2">Username *</label>
																<div class="col-sm-10">
																	<input type="text" placeholder="Username"
																		class="form-control characters" name="username"
																		tabindex="1" required="true"
																		value="${ServerDetails[0].jenkin_username}">
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-sm-2">Jenkins
																	URL *</label>
																<div class="col-sm-10">
																	<input type="text" placeholder="URL"
																		class="form-control " name="jenkinsUrl" tabindex="1"
																		required="true" value="${ServerDetails[0].jenkin_url}">
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-sm-2">API Token
																	*</label>
																<div class="col-sm-10">
																	<input type="text" placeholder="API Token"
																		class="form-control characters" name="apiToken"
																		tabindex="1" required="true"
																		value="${ServerDetails[0].jenkin_token}">
																</div>
															</div>
														</div>

														<div class="form-group">
															<label class="col-sm-2 control-label">Status:</label>
															<div class="col-sm-10">
																<div class="radio radio-success radio-inline col-sm-2 ">
																	<input type="radio" id="inlineRadio1" value="1"
																		name="instanceStatus" checked="" tabindex="3"
																		${ServerDetails[0].jenkin_instance_status == '1' ? 'checked="true"' : ""}>
																	<label for="inlineRadio1"> Active </label>
																</div>
																<div class="radio radio-danger radio-inline col-sm-2">
																	<input type="radio" id="inlineRadio2" value="2"
																		name="instanceStatus" tabindex="4"
																		${ServerDetails[0].jenkin_instance_status == '2' ? 'checked="true"' : ""}>
																	<label for="inlineRadio2"> Inactive </label>
																</div>
															</div>
														</div>
														<div class="form-group hidden">
															<label class="control-label col-sm-2">Server Id</label>
															<div class="col-sm-10">
																<input type="text" placeholder="Server Id"
																	class="form-control" name="serverId" tabindex="1"
																	value="${ServerDetails[0].jenkin_id}">
															</div>
														</div>
													</div>
													<div class="hr-line-solid"></div>
													<br>
												</fieldset>
											</div>
											<div class="actions clearfix">
												<div class="row">
													<div class="col-sm-8">
														<button class="btn btn-white pull-left"
															id="registrationCancel" style="margin-right: 15px;"
															type="button" tabindex="5"
															onclick="window.location.href='viewjenkinsurls';">Cancel</button>
														<button
															class="btn btn-success pull-left ladda-button ladda-button-demo"
															style="margin-right: 15px;" type="button"
															data-style="slide-up" tabindex="6" id="updateBtn">Update</button>
													</div>
												</div>
											</div>

										</form>
										<!-- end form -->
									</div>
									<!-- end ibox-content -->

								</div>
								<!-- end ibox -->
							</div>
						</div>

						<div id="membersTab" class="tab-pane active">
							<div class="panel-body">
								<div class="ibox ">
									<div class="ibox-title">
										<strong>Members</strong>
										<button class="btn btn-success btn-sm pull-right"
											id="showUsersToAddBtn">Add users to Instance</button>
									</div>
									<div class="ibox-content">
										<div id="addUsersPanel" class="panel panel-default hidden">
											<div class="panel-heading">Add users</div>
											<div class="panel-body">
												<form action="addInstancePermissionToMember" class="form-horizontal"
													method="POST" id="addPermissionToMember">

													<div class="form-group hidden">
														<label class="control-label col-sm-2">Instance id</label>
														<div class="col-sm-10">
															<input type="text" placeholder="Instance Id"
																class="form-control" name="instanceId" value="${ServerDetails[0].jenkin_id}">
														</div>
													</div>

													<div class="form-group">
														<label class="col-lg-2 control-label">Users:*</label>
														<div class="col-lg-6">
															<select data-placeholder="Choose Users" id="usersSelect"
																class="chosen-select" name="users" multiple>
																<c:forEach var="users"
																	items="${model.permissionNonAssignedMembers}">
																	<option value="${users.user_id}">${users.first_name} ${users.last_name}</option>
																</c:forEach>
															</select> <label class="error hidden" id="addUserValidationLabel">This
																field is required.</label>
														</div>
													</div>

												</form>
											</div>
											<div class="panel-footer">
												<button class="btn btn-white" id="cancelAddUsersBtn">Cancel</button>
												<button class="btn btn-success" id="addUsersBtn">Submit</button>
											</div>
										</div>
										<div class="table-responsive">
											<table class="table table-hover" id="membersTable">
												<thead>
													<tr>
														<th>Image</th>
														<th>Name</th>
														<th>E-Mail</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data"
														items="${model.permissionAssignedMembers}">
														<tr>
															<td class="client-avatar"><img alt="image"
																class="img-circle "
																src="data:image/jpg;base64,${data.photo}"></td>

															<td>${data.first_name}&nbsp;${data.last_name}</td>

															<td>${data.email_id}</td>

															<td>
																<button class="btn btn-white btn-sm"
																	onclick="removeUsersPermission(${data.user_id},${ServerDetails[0].jenkin_id})">Remove</button>
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div id="permissionsTab" class="tab-pane">
							<div class="panel-body">
								<div class="ibox ">
									<div class="ibox-title">
										<strong>Permissions</strong>
										<button class="btn btn-success btn-sm pull-right"
											id="showUsersForPermissionBtn">Add Permission</button>
									</div>
									<div class="ibox-content">
										<div id="addPermissionsToUserPanel"
											class="panel panel-default hidden">
											<div class="panel-heading">Add Permission</div>
											<div class="panel-body">
												<form action="updateUsersInstancePermission"
													class="form-horizontal" method="POST"
													id="updateUsersInstancePermission">

													<div class="form-group hidden">
														<label class="control-label col-sm-2">Job ID</label>
														<div class="col-sm-10">
															<input type="text" placeholder="Job Id"
																class="form-control" name="instanceId" value="${ServerDetails[0].jenkin_id}">
														</div>
													</div>

													<div class="form-group">
														<label class="col-lg-2 control-label">Edit/Update:*</label>
														<div class="col-lg-6">
															<select data-placeholder="Choose Users"
																id="editPermissionUsers" class="chosen-select"
																name="editPermissionUsers" multiple>
																<c:forEach var="users"
																	items="${model.executePermissionAssignedMembers}">
																	<option value="${users.user_id}">${users.first_name} ${users.last_name}</option>
																</c:forEach>
																<c:forEach var="users"
																	items="${model.noPermissionAssignedMembers}">
																	<option value="${users.user_id}">${users.first_name} ${users.last_name}</option>
																</c:forEach>
															</select> <label class="error hidden"
																id="userPermissionValidationLabel">This field is
																required.</label>
														</div>
													</div>
													
												</form>
											</div>
											<div class="panel-footer">
												<button class="btn btn-white"
													id="cancelAddUserPermissionBtn">Cancel</button>
												<button class="btn btn-success" id="updatePermissionBtn">Submit</button>
											</div>
										</div>
										<div class="table-responsive">
											<table class="table table-hover" id="membersTable">
												<thead>
													<tr>
														<th>Image</th>
														<th>Name</th>
														<th>E-Mail</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data"
														items="${model.permissionAssignedMembers}">
														<c:if
															test="${data.update_status == 1 || data.execute_status == 1}">
															<tr>
																<td class="client-avatar"><img alt="image"
																	class="img-circle "
																	src="data:image/jpg;base64,${data.photo}"></td>

																<td>${data.first_name}&nbsp;${data.last_name}</td>

																<td>${data.email_id}</td>

																<td><c:if test="${data.update_status == 1}">
																		<span class="label label-primary">Update Instance</span>
																	</c:if> <c:if test="${data.view_status == 1}">
																		<span class="label label-warning">View Instance</span>
																	</c:if>

																	<button class="btn btn-white btn-sm"
																		onclick="revokeUsersPermission(${data.user_id},${ServerDetails[0].jenkin_id})">Revoke
																		Permissions</button></td>
															</tr>
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});

	$('#showUsersToAddBtn').click(function() {
		//to apply choosen
		$("#addUsersPanel").removeClass('hidden');
		$(".chosen-select").chosen();
	});
	
	$('#cancelAddUsersBtn').click(function() {
		$("#addUsersPanel").addClass('hidden');
		$("#addUserValidationLabel").addClass("hidden");
	});
	
	$('#addUsersBtn').click(function() {
	       
		var values = $('#usersSelect').val();
        if(values!=null) {
        	
        	$('body').addClass("white-bg");
    		$("#barInMenu").removeClass("hidden");
    		$("#wrapper").addClass("hidden");
    		
        	$("#addPermissionToMember").submit();
        }else {
       		$("#addUserValidationLabel").removeClass("hidden");
        } 

	});
	
	$('#updateBtn').click(function() {

		if ($("#updateForm").valid()) {

			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");

			$('#updateForm').submit();
		}
	});
	
	$('#showUsersForPermissionBtn').click(function() {
		//to apply choosen
		$("#addPermissionsToUserPanel").removeClass('hidden');
		$(".chosen-select").chosen();
	});
	
	$('#updatePermissionBtn').click(function() {
	       
		var editPermissionUsers = $('#editPermissionUsers').val();
        if(editPermissionUsers!=null) {
        	
        	$('body').addClass("white-bg");
    		$("#barInMenu").removeClass("hidden");
    		$("#wrapper").addClass("hidden");
    		
        	$("#updateUsersInstancePermission").submit();
        }else {
       		$("#addUserValidationLabel").removeClass("hidden");
        } 

	});
	
function revokeUsersPermission(userId,instanceId){
		
		swal({
			title : "Are you sure?",
			text : "Do you want to revoke all Instance permissions for this User",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "Yes, Revoke Permissions!",
			cancelButtonText : "No, cancel!",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function() {
		
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
			
			$.ajax({
				url: "revokeUsersInstancePermission",
				data: {
					userId: userId,
					instanceId : instanceId
				},
				type: "POST",
			})
			.done(function(data){
					location.reload();
			});
		});
	}
	
function removeUsersPermission(userId,instanceId){
		
		swal({
			title : "Are you sure?",
			text : "Do you want to remove this user from Instance members",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "Yes, Remove it!",
			cancelButtonText : "No, cancel!",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function() {
		
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
			
			$.ajax({
				url: "removeUsersInstancePermission",
				data: {
					userId: userId,
					instanceId : instanceId
				},
				type: "POST",
			})
			.done(function(data){
					location.reload();
			});
		});
	}
</script>



<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Jenkins Job Settings</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li><a href="admindashboard">Plugins</a></li>
				<li><a href="viewjenkinsurls">Jenkins</a></li>
				<li><a href="jenkinsreports">View Jobs</a></li>
				<li class="Active"><strong>Settings</strong></li>
			</ol>
		</div>
	</div>

	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-lg-12">
				<div class="tabs-container">
					<ul class="nav nav-tabs">
						<li class=""><a data-toggle="tab" href="#editTab">
								Edit </a></li>
						<li class="active"><a data-toggle="tab" href="#membersTab">
								Members </a></li>
						<li class=""><a data-toggle="tab" href="#permissionsTab">
								Permissions </a></li>
					</ul>
					<div class="tab-content">
						<div id="editTab" class="tab-pane ">

							<div class="panel-body">
								<div class="ibox">
									<div class="ibox-title"></div>
									<div class="ibox-content">
										<form action="updatejenkinsjob" method="POST"
											id="updatejenkinsjobForm"
											class="wizard-big wizard clearfix form-horizontal">
											<div class="content clearfix">
												<fieldset class="body current">
													<div class="row">
														<label class="col-lg-4 pull-right text-right">*
															fields are mandatory</label>
													</div>
													<div class="row">
														<div class="col-sm-10">
															<div class="form-group">
																<input type="text" class="hidden" name="jobId" value="${model.jenkinsDetails.jobId}">
																<label class="control-label col-sm-2">Instance:</label>
																<div class="col-sm-8">
																	<label class="control-label">${model.jenkinsInstanceName}</label> 
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-sm-2">Jenkins
																	Job Name:</label>
																<div class="col-sm-8">
																	<label class="control-label">${model.jenkinsDetails.jenkinsJobName}</label> 
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-sm-2">Job Name*:</label>
																<div class="col-sm-8">
																	<input type="text" class="form-control" name="jobname" value="${model.jenkinsDetails.jobName}">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-2 control-label">
																	Description:</label>
																<div class="col-sm-8">

																	<textarea rows="5"
																		class="form-control characters summernote"
																		name="jobDesciption"> ${model.jenkinsDetails.jobDescription}</textarea>
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-sm-2">Build
																	Trigger Token*:</label>
																<div class="col-sm-8">
																	<input type="text" class="form-control"
																		name="buildtoken" value="${model.jenkinsDetails.buildToken}">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-2 control-label">Status:</label>
																<div class="col-sm-10">
																	<div class="radio radio-success radio-inline col-sm-2">
																		<input type="radio" id="active" value="1"
																			name="jobStatus" 
																			${model.jenkinsDetails.jobStatus == '1' ? 'checked="true"' : ""}> <label
																			for="active">Active</label>
																	</div>
																	<div class="radio radio-danger radio-inline col-sm-2">
																		<input type="radio" id="inactive" value="2"
																			name="jobStatus"
																			${model.jenkinsDetails.jobStatus != '1' ? 'checked="true"' : ""}> <label for="active">Inactive</label>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="hr-line-solid" style="margin-top: 10em"></div>
												</fieldset>
											</div>
											<div class="actions clearfix">
												<div class="row">

													<div class="col-sm-4">
														<button class="btn btn-white pull-left"
															style="margin-right: 15px;" type="button" id="cancelEditBtn">Cancel</button>
														<button class="pull-left btn btn-success" type="button"
															id="updatejenkinsjobBtn">Update</button>
													</div>

												</div>
											</div>
										</form>
									</div>
								</div>
							</div>

						</div>

						<div id="membersTab" class="tab-pane active">
							<div class="panel-body">
								<div class="ibox ">
									<div class="ibox-title">
										<strong>Members</strong>
										<button class="btn btn-success btn-sm pull-right"
											id="showUsersToAddBtn">Add users to Job</button>
									</div>
									<div class="ibox-content">
										<div id="addUsersPanel" class="panel panel-default hidden">
											<div class="panel-heading">Add users</div>
											<div class="panel-body">
												<form action="addPermissionToMember" class="form-horizontal"
													method="POST" id="addPermissionToMember">

													<div class="form-group hidden">
														<label class="control-label col-sm-2">Job ID</label>
														<div class="col-sm-10">
															<input type="text" placeholder="Job Id"
																class="form-control" name="jobId" value="${model.jobId}">
														</div>
													</div>

													<div class="form-group hidden">
														<label class="control-label col-sm-2">Application
															Name</label>
														<div class="col-sm-10">
															<input type="text" placeholder="Application Name"
																class="form-control characters" name="appName"
																id="appName" value="${applicationData[0].project_name}">
														</div>
													</div>

													<div class="form-group">
														<label class="col-lg-2 control-label">Users:*</label>
														<div class="col-lg-6">
															<select data-placeholder="Choose Users" id="usersSelect"
																class="chosen-select" name="users" multiple>
																<c:forEach var="users"
																	items="${model.permissionNonAssignedMembers}">
																	<option value="${users.user_id}">${users.first_name} ${users.last_name}</option>
																</c:forEach>
															</select> <label class="error hidden" id="addUserValidationLabel">This
																field is required.</label>
														</div>
													</div>

												</form>
											</div>
											<div class="panel-footer">
												<button class="btn btn-white" id="cancelAddUsersBtn">Cancel</button>
												<button class="btn btn-success" id="addUsersBtn">Submit</button>
											</div>
										</div>
										<div class="table-responsive">
											<table class="table table-hover" id="membersTable">
												<thead>
													<tr>
														<th>Image</th>
														<th>Name</th>
														<th>E-Mail</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data"
														items="${model.permissionAssignedMembers}">
														<tr>
															<td class="client-avatar"><img alt="image"
																class="img-circle "
																src="data:image/jpg;base64,${data.photo}"></td>

															<td>${data.first_name}&nbsp;${data.last_name}</td>

															<td>${data.email_id}</td>

															<td>
																<button class="btn btn-white btn-sm"
																	onclick="removeUsersPermission(${data.user_id},${model.jobId})">Remove</button>
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div id="permissionsTab" class="tab-pane">
							<div class="panel-body">
								<div class="ibox ">
									<div class="ibox-title">
										<strong>Permissions</strong>
										<button class="btn btn-success btn-sm pull-right"
											id="showUsersForPermissionBtn">Add Permission</button>
									</div>
									<div class="ibox-content">
										<div id="addPermissionsToUserPanel"
											class="panel panel-default hidden">
											<div class="panel-heading">Add Permission</div>
											<div class="panel-body">
												<form action="updateUsersJobPermission"
													class="form-horizontal" method="POST"
													id="updateUsersJobPermission">

													<div class="form-group hidden">
														<label class="control-label col-sm-2">Job ID</label>
														<div class="col-sm-10">
															<input type="text" placeholder="Job Id"
																class="form-control" name="jobId" value="${model.jobId}">
														</div>
													</div>

													<div class="form-group">
														<label class="col-lg-2 control-label">Edit/Update:*</label>
														<div class="col-lg-6">
															<select data-placeholder="Choose Users"
																id="editPermissionUsers" class="chosen-select"
																name="editPermissionUsers" multiple>
																<c:forEach var="users"
																	items="${model.executePermissionAssignedMembers}">
																	<option value="${users.user_id}">${users.first_name} ${users.last_name}</option>
																</c:forEach>
																<c:forEach var="users"
																	items="${model.noPermissionAssignedMembers}">
																	<option value="${users.user_id}">${users.first_name} ${users.last_name}</option>
																</c:forEach>
															</select> <label class="error hidden"
																id="userPermissionValidationLabel">This field is
																required.</label>
														</div>
													</div>

													<div class="form-group">
														<label class="col-lg-2 control-label">Execute
															Build:*</label>
														<div class="col-lg-6">
															<select data-placeholder="Choose Users"
																id="executePermissionUsers" class="chosen-select"
																name="executePermissionUsers" multiple>
																<c:forEach var="users"
																	items="${model.updatePermissionAssignedMembers}">
																	<option value="${users.user_id}">${users.first_name} ${users.last_name}</option>
																</c:forEach>
																<c:forEach var="users"
																	items="${model.noPermissionAssignedMembers}">
																	<option value="${users.user_id}">${users.first_name} ${users.last_name}</option>
																</c:forEach>
															</select>
														</div>
													</div>

												</form>
											</div>
											<div class="panel-footer">
												<button class="btn btn-white"
													id="cancelAddUserPermissionBtn">Cancel</button>
												<button class="btn btn-success" id="updatePermissionBtn">Submit</button>
											</div>
										</div>
										<div class="table-responsive">
											<table class="table table-hover" id="membersTable">
												<thead>
													<tr>
														<th>Image</th>
														<th>Name</th>
														<th>E-Mail</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data"
														items="${model.permissionAssignedMembers}">
														<c:if
															test="${data.update_status == 1 || data.execute_status == 1}">
															<tr>
																<td class="client-avatar"><img alt="image"
																	class="img-circle "
																	src="data:image/jpg;base64,${data.photo}"></td>

																<td>${data.first_name}&nbsp;${data.last_name}</td>

																<td>${data.email_id}</td>

																<td><c:if test="${data.update_status == 1}">
																		<span class="label label-primary">Update Job</span>
																	</c:if> <c:if test="${data.execute_status == 1}">
																		<span class="label label-warning">Execute Job</span>
																	</c:if>

																	<button class="btn btn-white btn-sm"
																		onclick="revokeUsersPermission(${data.user_id},${model.jobId})">Revoke
																		Permissions</button></td>
															</tr>
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="actionForTab"
	value='<%=session.getAttribute("actionForTab")%>'>

<input type="hidden" id="hiddenInput"
	value='<%=session.getAttribute("proCreateStatus")%>'>

<input type="hidden" id="newProName"
	value='<%=session.getAttribute("newProjectName")%>'>

<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});

	$(function () {
		  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
		//tostr on redirect from create project
		var state = $('#hiddenInput').val();
		var newProjectName = $('#newProName').val();
		if(state == 1){
			var message =" application is successfully created";
			var toasterMessage = newProjectName +  message;
			showToster(toasterMessage);
		}
		else if(state == 2){
			var message =" application is successfully Updated";
			var toasterMessage = newProjectName +  message;
			showToster(toasterMessage);
		}
		
		$('#hiddenInput').val('11');
	});
	
	 function showToster(toasterMessage){
		 toastr.options = {
				 "closeButton": true,
				  "debug": true,
				  "progressBar": true,
				  "preventDuplicates": true,
				  "positionClass": "toast-top-right",
				  "showDuration": "400",
				  "hideDuration": "1000",
				  "timeOut": "9000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "slideDown",
				  "hideMethod": "slideUp"
          };
          toastr.success('',toasterMessage);
	}
	
	
	$(function() {
		$('#membersTable').DataTable();
		
		//summernote
		$('.summernote').summernote();
		$('.note-editor').css('background','white');
		$('button[ data-event="codeview"]').remove();
		$('div.note-insert').remove();
		$('div.note-table').remove();
		$('div.note-help').remove();
		$('div.note-style').remove();
		$('div.note-color').remove();
		$('button[ data-event="removeFormat"]').remove();
		$('button[ data-event="fullscreen"]').remove();
		$('button[ data-original-title="Line Height"]').remove();
		$('button[ data-original-title="Font Family"]').remove();
		$('button[ data-original-title="Paragraph"]').remove();
		$('button[ data-event="insertUnorderedList"]').remove();

		var actionForTab = $('#actionForTab').val();
		if(actionForTab == "members"){
			//set the automation tab active
			$('a[href="#membersTab"]').click();
		}
	});

	$('#btnCancel').click(function() {
		location.reload();
	});
	
	
	$('#btnUpdate').click(function() {
		
		if ($("#updateAppform").valid()) {
			
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
			
			$("#projectName").removeAttr("disabled");
		
			$('.summernote').each(function() {
				$(this).val($(this).code());
			});
	
			$('#updateAppform').submit();
		}
	});

function removeUsersPermission(userId,jobId){
		
		swal({
			title : "Are you sure?",
			text : "Do you want to remove this user from Job members",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "Yes, Remove it!",
			cancelButtonText : "No, cancel!",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function() {
		
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
			
			$.ajax({
				url: "removeUsersJobPermission",
				data: {
					userId: userId,
					jobId : jobId
				},
				type: "POST",
			})
			.done(function(data){
					location.reload();
			});
		});
	}
	
function revokeUsersPermission(userId,jobId){
	
	swal({
		title : "Are you sure?",
		text : "Do you want to revoke all job permissions for this User",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "Yes, Revoke Permissions!",
		cancelButtonText : "No, cancel!",
		closeOnConfirm : true,
		closeOnCancel : true
	}, function() {
	
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		$.ajax({
			url: "revokeUsersJobPermission",
			data: {
				userId: userId,
				jobId : jobId
			},
			type: "POST",
		})
		.done(function(data){
				location.reload();
		});
	});
}
	
	function unAssignUser(ID,appID,name){
		
		swal({
			title : "Are you sure?",
			text : "Do you want to remove user from application?",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "Yes, Remove it!",
			cancelButtonText : "No, cancel!",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function() {
		
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
			
			$.ajax({
				url: "unassignapptouser",
				data: {
					appID: appID,
					userID: ID,
					appName: name
				},
				type: "POST",
			})
			.done(function(data){
				if(data == "500"){
					location.href  = "500";
				}else if(data == "200"){
					location.reload();
				}else{
					location.href  = "404";
				}
			});
		});
	}
	
	
	$('#cancelEditBtn').click(function() {
		location.reload();
	});
	
	$('#showUsersToAddBtn').click(function() {
		//to apply choosen
		$("#addUsersPanel").removeClass('hidden');
		$(".chosen-select").chosen();
	});
	
	$('#showUsersForPermissionBtn').click(function() {
		//to apply choosen
		$("#addPermissionsToUserPanel").removeClass('hidden');
		$(".chosen-select").chosen();
	});

	$('#addUsersBtn').click(function() {
       
		var values = $('#usersSelect').val();
        if(values!=null) {
        	
        	$('body').addClass("white-bg");
    		$("#barInMenu").removeClass("hidden");
    		$("#wrapper").addClass("hidden");
    		
        	$("#addPermissionToMember").submit();
        }else {
       		$("#addUserValidationLabel").removeClass("hidden");
        } 

	});
	
	$('#updatejenkinsjobBtn').click(function() {
		$("#updatejenkinsjobForm").submit();
	});
	
	
	$('#updatePermissionBtn').click(function() {
	       
		var executePermissionUsers = $('#executePermissionUsers').val();
		var editPermissionUsers = $('#editPermissionUsers').val();
        if(executePermissionUsers!=null || editPermissionUsers!=null) {
        	
        	$('body').addClass("white-bg");
    		$("#barInMenu").removeClass("hidden");
    		$("#wrapper").addClass("hidden");
    		
        	$("#updateUsersJobPermission").submit();
        }else {
       		$("#addUserValidationLabel").removeClass("hidden");
        } 

	});
	
	

	$('#cancelAddUsersBtn').click(function() {
		$("#addUsersPanel").addClass('hidden');
		$("#addUserValidationLabel").addClass("hidden");
	});
	
	$('#cancelAddUserPermissionBtn').click(function() {
		$("#addPermissionsToUserPanel").addClass('hidden');
		$("#userPermissionValidationLabel").addClass("hidden");
	});
</script>

<%
	//set session variable to another value
	session.setAttribute("proCreateStatus", 11);
%>

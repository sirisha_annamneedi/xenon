<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style>
.has-error {
	color: #8a1f11;
}
</style>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Jenkins Configuration Information</h2>
		<ol class="breadcrumb">
			<li><a href="admindashboard">Dashboard</a></li>
			<li><a href="admindashboard">Plugins</a></li>
			<li><a href="viewjenkinsurls">Jenkins</a></li>
			<li class="active"><strong>API Token</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				
				<div class="ibox-content">
					<form action="registerjenkinsurl" method="POST" id="form"
						class="wizard-big wizard clearfix form-horizontal">
						<div class="row">
						<h5 class="col-sm-9" style="font-size: 18">Configuration Information for ${model.serverName}</h5>
						<c:if test="${!model.tokenDetails.token}">
								<button type="button" class="btn btn-success btn-xs pull-right col-sm-3" style="margin-right: 1%;"
									onclick="location.href = 'generateapitoken?serverName=${model.serverName}';">Generate New Token</button>
								</c:if>
						</div>
						<div class="content clearfix">
							<fieldset class="body current">
								
								
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label class="control-label col-sm-2">Xenon API :</label> <label
												class="control-label col-sm-10">${model.tokenDetails.token}</label>
										</div>
										<%-- <div class="form-group">
											<label class="control-label col-sm-2">Pre Step URL :</label> <label
												class="control-label col-sm-10" style="text-align: left "><p id="preUrl">${model.tokenDetails.windows_script}</p></label>
											
											<button class="btn btn-primary btn-sm" type="button" data-clipboard-target="#preUrl"><i class="fa fa-copy"></i> Copy Url</button>
										</div> --%>

										<%-- <div class="form-group">
											<label class="control-label col-sm-2">Powershell :</label> <label
												class="control-label">${model.tokenDetails.powershell_script}</label>
										</div> --%>
									</div>
								</div>
								<div class="hr-line-solid"></div>
								
								<div class="row">
									<div class="col-lg-12">
										<%-- <div class="form-group">
											<label class="control-label col-sm-2">Xenon API :</label> <label
												class="control-label col-sm-10">${model.tokenDetails.token}</label>
										</div> --%>

										<%-- <div class="form-group">
											<label class="control-label col-sm-2">Post Step URL :</label> <label
												class="control-label col-sm-10" style="text-align: left"> <p id="postUrl">${model.tokenDetails.powershell_script}</p></label>
												
												<button class="btn btn-primary  btn-sm" type="button" data-clipboard-target="#postUrl"><i class="fa fa-copy"></i> Copy Url</button>
										</div>
 --%>
										<%-- <div class="form-group">
											<label class="control-label col-sm-2">Powershell :</label> <label
												class="control-label">${model.tokenDetails.powershell_script}</label>
										</div> --%>
									</div>
								</div>
							</fieldset>
						</div>
					</form>
					<!-- end form -->
				</div>
			</div>
		</div>

	</div>
</div>

<style>
.margine-lable {
	margin-bottom: 2.0em !important;
}
</style>
<!-- Clipboard -->
<script src="<%=request.getContextPath()%>/resources/js/plugins/clipboard/clipboard.min.js"></script>
<script type="text/javascript">
	$(window).load(function() {
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");

		$(".content").addClass("no-overflow");
	})
</script>
<script type="text/javascript">
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});
	
	$(document).ready(function (){
        new Clipboard('.btn');
    });

</script>
<script>
	
</script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style>
.has-error {
	color: #8a1f11;
}
</style>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Jenkins URL Registration</h2>
		<ol class="breadcrumb">
			<li><a href="admindashboard">Dashboard</a></li>
			<li><a href="viewjenkinsurls">Jenkins</a></li>
			<li class="active"><strong>Register URL</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>Jenkins Server Information</h5>
				</div>
				<div class="ibox-content">
						<form action="insertjenkinsinstance" method="POST" id="form"
							class="wizard-big wizard clearfix form-horizontal">
							<div class="content clearfix">
								<fieldset class="body current">
									<div class="row">
										<label class="col-lg-4 pull-right text-right">* fields
											are mandatory</label>
									</div>
									<div class="row">
										<div class="col-lg-12">
												<div class="form-group">
													<label class="control-label col-sm-2">Server Name *</label>
													<div class="col-sm-10">
														<input type="text" placeholder="Server Name"
															class="form-control characters" name="serverName"
															tabindex="1" required="true">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-2">Username *</label>
													<div class="col-sm-10">
														<input type="text" placeholder="Username"
															class="form-control characters" name="username"
															tabindex="1" required="true">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-2">Jenkins URL *</label>
													<div class="col-sm-10">
														<input type="text" placeholder="URL"
															class="form-control " name="jenkinsUrl"
															tabindex="1"  required="true">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-2">API Token *</label>
													<div class="col-sm-10">
														<input type="text" placeholder="API Token"
															class="form-control characters" name="apiToken"
															tabindex="1" required="true">
													</div>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-2 control-label">Status:</label>
												<div class="col-sm-10">
													<div class="radio radio-success radio-inline col-sm-2 ">
														<input type="radio" id="inlineRadio1" value="1"
															name="instanceStatus" checked="" tabindex="3"> <label
															for="inlineRadio1"> Active </label>
													</div>
													<div class="radio radio-danger radio-inline col-sm-2">
														<input type="radio" id="inlineRadio2" value="2"
															name="instanceStatus" tabindex="4"> <label
															for="inlineRadio2"> Inactive </label>
													</div>
												</div>
											</div>
										</div>
									<div class="hr-line-solid"></div>
									<br>
								</fieldset>
								</div>
								<div class="actions clearfix">
									<div class="row">
										<div class="col-sm-8">
											<button class="btn btn-white pull-left"
												id="registrationCancel" style="margin-right: 15px;"
												type="button" tabindex="5" onclick="window.location.href='viewjenkinsurls';">Cancel</button>
											<button
												class="btn btn-success pull-left ladda-button ladda-button-demo"
												style="margin-right: 15px;" type="button"
												data-style="slide-up" tabindex="6" id="saveBtn">Save</button>
										</div>
									</div>
								</div>
							
						</form>
						<!-- end form -->
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-content"></div>
				<div class="ibox-title">
					<h5>Help</h5>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
.margine-lable {
	margin-bottom: 2.0em !important;
}
</style>
<script type="text/javascript">
	$(window).load(function() {
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");

		$(".content").addClass("no-overflow");
	})
</script>
<script type="text/javascript">
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});
	
	
	$("#saveBtn").click(function() {
		var formValid = $("#form").valid();
		if(formValid){			
			$('#form').submit();
		}
	});
</script>
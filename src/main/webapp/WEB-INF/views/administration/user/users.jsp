<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- Sweet Alert -->
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/sweetalert/sweetalert.css"
	rel="stylesheet">

	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Users</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li class="active"><strong>Users</strong></li>
			</ol>
		</div>
		<!-- end col-lg-10 -->
	</div>
	<!-- end row -->
<!-- end wrapper -->

<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>Users</h5>
				</div>
				<div class="ibox-content">
					<div class="">
						<div class="tab-content">
							<div class="tab-pane active">
								<div class="full-height-scroll">
									<div class="table-responsive" style="overflow-x: visible;">
										<table class="table table-striped table-hover" id="usersTable">
											<thead>
												<tr>
													<th class="hidden"></th>
													<th class="hidden"></th>
													<th><b>Name</b></th>
													<th class="hidden"></th>
													<th><b>Email</b></th>
													<th><b>Status</b></th>
													<th style="text-align: right;">Action</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="data" items="${model.allUserDetails}">
													<tr data-toggle="tab" href="#${fn:trim(data.user_id)}"
														style="cursor: pointer">
														<td class="hidden userId">${data.user_id}</td>
														<td class="client-avatar hidden""><img alt="image"
															src="data:image/jpg;base64,${data.user_photo}"></td>
														<td class="userFullName">${data.first_name}&nbsp;${data.last_name}</td>
														<td class="hidden"></td>
														<td>${data.email_id}</td>
														<td class="client-status"><span
															class="label statusLabels">${data.userStatus}</span></td>
														<td class="project-actions">
															<button class="btn btn-white btn-xs editUserBtn">
																Edit
															</button>
															<c:if test="${model.assignProjectStatus == 1}">
															<button class="btn btn-white btn-xs userAssignedBtn">
																Assign Applications
															</button>
															</c:if>
															
														</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(window).load(function() {
	$("#barInMenu").fadeOut();
	$("#wrapper").removeClass("hidden");
})

	$(window).load(function() {
		$("#barInMenu").fadeOut();
	})
</script>
<script type="text/javascript">
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});
</script>
<script>
	$(function() {
		$('body').addClass('md-skin');
		var userTable = $('#usersTable').DataTable({
			/* "order": [[0, "desc"]], */
			"paging" : true,
			"lengthChange" : true,
			"searching" : true,
			"ordering" : true,
			"info" : true,
			"autoWidth" : true
		});
		var allRows =  userTable.$(".statusLabels", {"page": "all"});
		allRows.each(function() {
			var status = $.trim($(this).text());
			if (status.toLowerCase() == 'active') {
				$(this).addClass('label-primary');
			} else {
				$(this).addClass('label-danger');
			}
		});
		$('#extraUserDetails > :first-child').addClass('active');
	});
	$('.editUserBtn').click(function() {
		$("#barInMenu").fadeIn();
		$("#wrapper").addClass("hidden");
		
		$("#spinner").fadeIn("slow");
		 var userId = $(this).parents('tr').first().find('.userId').text();
		var posting = $.post('setuserID', {
			userID : userId,
		});
		posting.done(function(data) {
			window.location.href = "edituser";

		});
		$("#spinner").fadeOut("slow");
	});

	
	$('.userAssignedBtn').click(function(){
		
		$("#barInMenu").fadeIn();
		$("#wrapper").addClass("hidden");
		
		$("#spinner").fadeIn("slow");
		var userId = $(this).parents('tr').first().find('.userId').text();
		var userFullName = $(this).parents('tr').first().find('.userFullName').text();
		var posting = $.post('setuserID', {
			userID : userId,
			userFullName : userFullName
		});
		posting.done(function(data) {
			window.location.href = "assignapplications";

		});
	});
</script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="md-skin">
<div class="wrapper">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Assign Applications to - ${userModel.userFullName}</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li><a href="viewuser">Users</a></li>
				<li class="active"><strong>Assign Applications</strong></li>
			</ol>
		</div>
		<!-- end col-lg-10 -->
	</div>
	<!-- end row -->
</div>
<!-- end wrapper -->

<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>Assign Applications</h5>
				</div>
				<div class="ibox-content">
					<input type="text" class="form-control input-sm m-b-xs" id="filter"
						placeholder="Search">
					<table class="footable table table-stripped toggle-arrow-tiny"
						data-page-size="10" data-filter="#filter">
						<thead>
							<tr>
								<th class="checkAllProjects" data-sort-ignore="true" style="width: 20px;"><input
									type="checkbox" id="select-all-projects" value="1"></th>
								<th>Name</th>
								<th>State</th>
								<th data-hide="all"></th>


							</tr>
						</thead>
						<tbody>
							<c:set var="proId" value="0" scope="page" />
							<c:forEach var="data" items="${userModel.details}">
								<c:if test="${proId != data.project_id }">
									<tr>
										<td><input type="checkbox" class="checkProjects" id="checkproject_${data.project_id}"
											value="${data.module_id}"></td>
										<td>${data.project_name}</td>
										<td></td>
										<td>
											<table class="table datatable moduleTable" id="moduleTable_${data.project_id}">
												<thead>
													<tr>
														<th class="checkAllModules" style="width: 20px;"><input
															type="checkbox" id="select-all-modules-${data.project_id}" value="1"></th>
														<th>Module Name</th>
														<th>Module Status</th>

													</tr>
												</thead>
												<tbody>
													<c:forEach var="model" items="${userModel.details}">


														<c:if test="${data.project_id == model.project_id && data.module_id != null}">
															<tr>
																<td><input type="checkbox"
																	${data.assign_count >0 ? 'checked' : ''}
																	class="select-all-modules-${data.project_id}" id="sdsadas"></td>
																<td>${model.module_name}</td>
																<td>${model.assign_count >0 ? 'Assigned' : 'Unassigned'}</td>
															</tr>
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</td>

									</tr>
								</c:if>
								<c:set var="proId" value="${data.project_id}" scope="page" />
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="6">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
					<div>
							<button class="btn btn-white" type="button"
								id="assignProjectCancelBtn">Cancel</button>
							<button class="btn btn-success ladda-button" type="button"
								id="assignProjectBtn" data-style="slide-up">Update</button>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
		
		 var foo = $('.footable').footable();
		 foo.trigger('footable_initialize'); //Reinitialize
		 foo.trigger('footable_redraw'); //Redraw the table
		 foo.trigger('footable_resize'); //Resize the table
	});

	$(document).ready(function() {
		var projectTable = $('.footable').footable();
		
		var moduleTable = $('.moduleTable').DataTable({
			"paging" : false,
			"lengthChange" : false,
			"searching" : false,
			"ordering" : false,
			"info" : false,
			"autoWidth" : true
		});
    $('.moduleTable').each(function(){
		   lastIndex = $(this).attr('id').length;
		   var table = $(this);
		   id=$(this).attr('id').substring(12,lastIndex);
     $('#select-all-modules-'+id).click(function(event) { 
    	 className = $(table).find('tbody').find('tr').find('input').attr('class');
    	 if(this.checked) {
    		 $("."+className).each(function(){
					this.checked = true; 
				});
		    }
		    else{
		    	$("."+className).each(function(){
					this.checked = false; 
				});
		    } 	
		});	 
		 })
		
		$('#select-all-projects').click(function(event) {  
			var allRows =  $(".checkProjects");	
		    if(this.checked) {
		    	allRows.each(function(){
		    		lastIndex = $(this).attr('id').length;
		  		   id=$(this).attr('id').substring(13,lastIndex);
		  		   className = 'select-all-modules-'+id;
					this.checked = true; 
					$("#"+className).prop('checked',true);
		    		   $("."+className).each(function(){
							this.checked = true; 
						});
				});
		    }
		    else{
		    	allRows.each(function(){
					this.checked = false; 
					lastIndex = $(this).attr('id').length;
		  		    id=$(this).attr('id').substring(13,lastIndex);
		  		    className = 'select-all-modules-'+id;
					$("#"+className).prop('checked',false);
		    		   $("."+className).each(function(){
							this.checked = false; 
						});
				});
		    } 	
		});
    $(".checkProjects").click(function(){
    	  lastIndex = $(this).attr('id').length;
		  id=$(this).attr('id').substring(13,lastIndex);
		  className = 'select-all-modules-'+id;
    	 if(this.checked) {
    		 $("#"+className).prop('checked',true);
    		   $("."+className).each(function(){
					this.checked = true; 
				});
    	 }
    	 else
    		 {
    		 $("#"+className).prop('checked',false);
    		 $("."+className).each(function(){
					this.checked = false; 
				});
    		 }
	})
		});	

	
	
</script>
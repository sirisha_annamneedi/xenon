<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- Sweet Alert -->
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/sweetalert/sweetalert.css"
	rel="stylesheet">

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Users</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li class="active"><strong>Users</strong></li>
			</ol>
		</div>
		<!-- end col-lg-10 -->
	</div>
	<!-- end row -->
	<!-- end wrapper -->

	<div class="wrapper wrapper-content  animated fadeInRight">
		<div class="row">
			<div class="col-sm-8">
				<div class="ibox">
					<div class="ibox-title">
						<h5>Users</h5>
						<c:if test="${model.createUserAccessStatus == 1}">
							<div class="ibox-tools">
								<button type="button" class="btn btn-success btn-xs"
									onclick="location.href = 'createuser';">Add User</button>
							</div>
						</c:if>
					</div>
					<div class="ibox-content">
						<div class="">
							<div class="tab-content">
								<div class="tab-pane active">
									<div class="full-height-scroll">
										<div class="table-responsive" style="overflow-x: visible;">
											<table class="table table-striped table-hover"
												id="usersTable">
												<thead>
													<!-- 												<tr>
													<td class="hidden"></td>
													<td class="hidden"></td>
													<td><input type="text" class="form-control" style="width :100%" /></td>
													<td class="hidden"></td>
													<td><input type="text" class="form-control" style="width :100%" /></td>
													<td style="width : 60px"><input type="text" class="form-control" style="width :100%" /></td>
													<td style="width :120px"></td>
												</tr> -->
													<tr>
														<th class="hidden"></th>
														<th class="hidden"></th>
														<th><b>Name</b></th>
														<th class="hidden"></th>
														<th><b>Email</b></th>
														<th style="width: 60px"><b>Status</b></th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data" items="${model.allUserDetails}">
														<tr data-toggle="tab" href="#${fn:trim(data.user_id)}"
															style="cursor: pointer">
															<td class="hidden userId">${data.user_id}</td>
															<td class="client-avatar hidden""><img alt="image"
																src="data:image/jpg;base64,${data.user_photo}"></td>
															<td class="userFullName mdTextAlignment"
																data-original-title="${data.first_name} ${data.last_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.first_name}&nbsp;${data.last_name}</td>
															<td class="hidden"></td>
															<td class="lgTextAlignment"
																data-original-title="${data.email_id}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.email_id}</td>
															<td class=""><span class="label statusLabels">${data.userStatus}</span></td>
															<td class=""><c:if test="${data.roleID != 2}">
																	<button class="btn btn-white btn-xs removeUser">
																		Remove</button>
																</c:if> <!-- 	<button class="btn btn-white btn-xs editUserBtn">
																Edit
															</button>
															<button class="btn btn-white btn-xs userAssignedBtn">
																Applications
															</button> -->

																<button class="btn-white btn btn-xs"
																	onclick="userSetting(${data.user_id},'${data.email_id}','${data.first_name} ${data.last_name}')"
																	name="${data.user_id}">Setting</button> <c:if
																	test="${model.emailSettingEnabled =='disable'}">
																	<button class="btn btn-white btn-xs resetPassword" onclick="openPasswordResetModal(${data.user_id})">Reset Password</button>
																</c:if></td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="ibox ">
					<div class="ibox-content">
						<div class="tab-content" id="extraUserDetails">
							<c:forEach var="data" items="${model.allUserDetails}">
								<div id="${fn:trim(data.user_id)}" class="tab-pane">
									<div class="row m-b-lg">
										<div class="col-lg-4 text-center">

											<div class="m-b-sm">
												<img alt="image" class="img-circle"
													src="data:image/jpg;base64,${data.user_photo}"
													style="width: 62px">
											</div>
										</div>
										<div class="col-lg-8">
											<h4
												style="vertical-align: middle; text-align: center; margin-top: 20px;">${data.first_name}&nbsp;${data.last_name}</h4>


										</div>
									</div>
									<div class="">
										<div class="full-height-scroll">

											<strong> About me </strong>
											<p>${data.about_me}</p>

											<strong>User Information</strong>

											<ul class="list-group clear-list">
												<li class="list-group-item fist-item"><span
													class="pull-right"> ${data.role_type} </span>User Role</li>
												<li class="list-group-item"><span class="pull-right">
														${data.tmStatus} </span>Test Manager Status</li>
												<li class="list-group-item"><span class="pull-right">
														${data.btStatus} </span>Bug Tracker Status</li>

											</ul>

										</div>
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal inmodal" id="resetPasswordModal" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated flipInY">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title">Password Reset</h4>
			</div>
			<div class="modal-body">
				<div class="content clearfix">
					<fieldset class="body current">
						<div class="row">
						<input type="hidden" id="resetPasswordUserId" value=""/>
						
							<div class="col-lg-10">
								<div class="form-group">
									<label class="control-label col-lg-4">New Password:*</label>
									<div class="col-lg-8">
										<input type="password" placeholder="New password"
											id="newPassword" class="form-control" name="newPassword" 
											onchange="checkPassword('newPassword','rePswdError','retypePassword')" onKeyUp="checkPassword('newPassword','rePswdError','retypePassword')"  />
										<label class="error hidden" id="newPswdError"></label>
									</div>
								</div>
							</div>
							
							<div class="col-lg-10">
								<div class="form-group">
									<label class="control-label col-lg-4">Retype Password:*</label>
									<div class="col-lg-8">
										<input type="password" placeholder="Retype password"
											id="retypePassword" class="form-control" name="retypePassword"
											onchange="checkPassword('retypePassword','rePswdError','newPassword')" onKeyUp="checkPassword('retypePassword','rePswdError','newPassword')" />
										<label class="error hidden" id="rePswdError"></label>
									</div>
								</div>
							</div>
							
							<div class="col-lg-10">
								<h3>The password should :</h3>
								<ul class="nav nav-third-level">
									<li>Be 8 - 16 characters long</li>
									<li>Include at least 1 uppercase character (A,B,C)</li>
									<li>Include at least 1 lowercase character (a,b,c)</li>
									<li>Include at least 1 number (1,2,3)</li>
									<li>Include at least 1 special character ($,#,@)</li>
								</ul>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="resetPasswordButton" onclick="resetPassword()" disabled>Reset Password</button>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="tosterStatus"
	value='<%=session.getAttribute("tosterStatusForUser")%>'>
<input type="hidden" id="tosterUserFullName"
	value='<%=session.getAttribute("tosterUserFullName")%>'>
<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	
	 var page= <%=request.getParameter("page")%>;
	 if(page == null){
		 page = 1;
	 }
	 
	 var allUserCount='${model.allUserCount}';
	 
	 var pageSize='${model.pageSize}';
	 
	 var lastRec=((page-1)*pageSize+parseInt(pageSize));
	 
	 if(lastRec>allUserCount)
	 	lastRec=allUserCount;
	 var showCount = ((page-1)*pageSize+1);
	 
	 if(showCount <= lastRec){
		 $("#usersTable_info").html("Showing "+showCount+" to "+lastRec+" of "+allUserCount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="prevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="nextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
	 }else{
		 $("#usersTable_info").html("");
	 }
	 
	 
	 if(page == 1){
		 $("#prevBtn").attr('disabled','disabled');
	 }
	 
	 if(lastRec == allUserCount){
		 $("#nextBtn").attr('disabled','disabled');
	 }
	 
	    $("#prevBtn").click(function() {
	    	$("#barInMenu").removeClass("hidden");
	    	$("#wrapper").addClass("hidden");
	    	
	    	if(page == 1)
	    		window.location.href = "viewuser";
	    	else
	    		window.location.href = "viewuser?page="+(page - 1);
		});
		
		$("#nextBtn").click(function(){
			$("#barInMenu").removeClass("hidden");
	    	$("#wrapper").addClass("hidden");
	    	
			window.location.href = "viewuser?page="+(page + 1);
		});
});

</script>
<script type="text/javascript">
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
		var status = $('#tosterStatus').val();
		var tosterUserFullName = $('#tosterUserFullName').val();

		if(status == 1){
			var toasterMessage =" User : "+ tosterUserFullName +" is successfully created";
			showToster(toasterMessage);
		}
		 else if(status == 2){
			 var toasterMessage =" User : "+ tosterUserFullName +" is successfully Updated";	 
			 showToster(toasterMessage);
		}
		
		$('#tosterStatus').val('11');
	});
	
	  function showToster(toasterMessage){
			 toastr.options = {
					 "closeButton": true,
					  "debug": true,
					  "progressBar": true,
					  "preventDuplicates": true,
					  "positionClass": "toast-top-right",
					  "showDuration": "400",
					  "hideDuration": "1000",
					  "timeOut": "9000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "slideDown",
					  "hideMethod": "slideUp"
	          };
	          toastr.success('',toasterMessage);
		} 
</script>
<!-- Sweet alert -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/sweetalert/sweetalert.min.js"></script>
<script>
	$(function() {
		//$('body').addClass('md-skin');
		var userTable = $('#usersTable').DataTable({
			/* "order": [[0, "desc"]], */
			dom : '<"html5buttons"B>lTfgitp', 
			buttons : [],
			"paging" : false,
			"lengthChange" : false,
			"searching" : false,
			"ordering" : false
		});
		var allRows =  userTable.$(".statusLabels", {"page": "all"});
		allRows.each(function() {
			var status = $.trim($(this).text());
			if (status.toLowerCase() == 'active') {
				$(this).addClass('label-primary');
			} else {
				$(this).addClass('label-danger');
			}
		});
		$('#extraUserDetails > :first-child').addClass('active');
		
		 // Apply the filter
	    $("#usersTable thead input").on( 'keyup change', function () {
	    	userTable
	            .columns( $(this).parent().index()+':visible' )
	            .search( this.value )
	            .draw();
	    	
	    	setSpanColor();
	    } );
	});
	
	$('.editUserBtn').click(function() {
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		 var userId = $(this).parents('tr').first().find('.userId').text();
		var posting = $.post('setuserID', {
			userID : userId,
		});
		posting.done(function(data) {
			window.location.href = "edituser";

		});

	});
	
	function openPasswordResetModal(userId){
		$('#resetPasswordUserId').val(userId);
		$('#newPassword').val('');
		$('#retypePassword').val('');
		$('#resetPasswordModal').modal('show');
	}
	
	function resetPassword(){
		
		var posting = $.post('resetpassword', {
			userId : $('#resetPasswordUserId').val(),
			password : $('#retypePassword').val()			
		});
		$('#resetPasswordModal').modal('toggle');
		var toasterMessage =" Password successfully reset";
		showToster(toasterMessage);
	}

		$('.removeUser').click(function() {
			
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
			
			var thisVal=this;
						swal({
		                        title: "Do you want to remove User?",
		                        text: "Your will not be able to recover this user!",
		                        type: "warning",
		                        showCancelButton: true,
		                        confirmButtonColor: "#DD6B55",
		                        confirmButtonText: "Yes, remove it!",
		                        cancelButtonText: "No, cancel!",
		                        closeOnConfirm: false,
		                        closeOnCancel: true },
		                    function (isConfirm) {
		                        if (isConfirm) {
		                        	
		             		    	var userId = $(thisVal).parents('tr').first().find('.userId').text();
		             				var posting = $.post('removeuser', {
		             					userID : parseInt(userId),
		             				});
		             				posting.done(function(data) {
		                            	window.location.href = "viewuser";
		             				});
		             				
		                        }  else {
		                        	$('body').removeClass("white-bg");
		                        	$("#barInMenu").addClass("hidden");
		                        	$("#wrapper").removeClass("hidden");
		                        } 
		                    });
		});
	
	$('.userAssignedBtn').click(function(){
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		var userId = $(this).parents('tr').first().find('.userId').text();
		var userFullName = $(this).parents('tr').first().find('.userFullName').text();
		var posting = $.post('setuserID', {
			userID : userId,
			userFullName : userFullName
		});
		posting.done(function(data) {
			window.location.href = "userassignedproject";

		});
	});
	
function userSetting(user_id,userFullName,userMailID)
{
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");
	
	var posting = $.post('setuserID', {
		userID : user_id,
		userMailID : userMailID,
		userFullName : userFullName
	});
	posting.done(function(data) {
		window.location.href = "usersettings";
	});
	
}
	
$('.appAssignedBtn').click(function(){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var userId = $(this).parents('tr').first().find('.userId').text();
	var userFullName = $(this).parents('tr').first().find('.userFullName').text();
	var posting = $.post('setuserID', {
		userID : userId,
		userFullName : userFullName
	});
	posting.done(function(data) {
		window.location.href = "assignapplications";

	});
});
	
function checkPassword(checkId1, msgboxId1, checkId2) {
	var checkId = "#" + checkId1;
	var checkToId = "#" + checkId2;
	var msgboxId = "#" + msgboxId1;
	var errorMsg = "";
	var flag = 0;
	
	//alert($("#txtPassword").val());
	var pattern1 = /[0-9]/;
	var pattern2 = /[a-z]/;
	var pattern3 = /[A-Z]/;
	var pattern4 = /[!@#\$]/;
	var pattern = /[a-zA-Z0-9]/;
	var password = $(checkId).val();
	
	if($(checkId).val() == $(checkToId).val()){
		flag = 0;
	}
	else{
		errorMsg += "Both password should be same<br>";
		flag = 1;
	}
	
	if (password.length > 7 && password.length < 17) {

	} else {
		errorMsg += "Password must be 8 - 16 characters long<br>";
		flag = 1;
	}
	if ($(checkId).val().match(pattern1)) {

	} else {
		errorMsg += "Password must contain at least one numeric<br>";
		flag = 1;
	}

	if ($(checkId).val().match(pattern2)) {
	} else {
		errorMsg += "Password must contain at least one a-z<br>";
		flag = 1;
	}
	if ($(checkId).val().match(pattern3)) {
	} else {
		errorMsg += "Password must contain at least one A-Z<br>";
		flag = 1;
	}
	if ($(checkId).val().match(pattern4)) {
	} else {
		errorMsg += "Password must contain at least one special symbol from !@#$<br>";
		flag = 1;
	}

	if (flag == 1) {
		//$(msgboxId).html("Password must contain at least one A-Z and  a-z  and any one special character from !@#$");
		$(msgboxId).html(errorMsg);
		$(msgboxId).removeClass("hidden");
		$(msgboxId).css("display","");
		$("#newPassword-error").addClass("hidden");
		$("#resetPasswordButton").prop("disabled", true);

	} else {
		$(msgboxId).addClass("hidden");
		$("#resetPasswordButton").prop("disabled", false);
	}
}
	
</script>
<%
	//set session variable to another value
	session.setAttribute("tosterStatusForUser", 11);
%>
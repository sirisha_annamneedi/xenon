<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>User Settings</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li><a href="viewuser">Users</a></li>
				<li class="active"><strong>User Settings</strong></li>
			</ol>
		</div>
	</div>
	
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-lg-12">
				<div class="tabs-container">
					<ul class="nav nav-tabs">
						<li
							class="<c:if test="${userModel.actionForTab == 'edit'}">active </c:if>"><a
							data-toggle="tab" href="#detailsTab"> Edit </a></li>
						<li
							class="<c:if test="${userModel.actionForTab == 'members'}">active </c:if>"><a
							data-toggle="tab" href="#membersTab"> Applications </a></li>

						<li class="<c:if test="${userModel.actionForTab == 'module'}">active </c:if>"><a
							data-toggle="tab" href="#moduleTab"> Modules </a></li>
					</ul>
					<div class="tab-content">
						<div id="detailsTab"
							class="tab-pane <c:if test="${userModel.actionForTab == 'edit'}">active </c:if>">
							<div class="panel-body">
								<div class="ibox">
									<div class="ibox-content">
										<form action="updateuser" method="POST" id="form"
											class="wizard-big wizard clearfix form-horizontal">
											<div class="content clearfix">
												<fieldset class="body current" id="fieldset">
													<div class="row">
														<div class="col-lg-10">
															<div class="form-group hidden">
																<label class="control-label col-sm-2">UserId</label>
																<div class="col-sm-10">
																	<input type="text" placeholder="User Id"
																		class="form-control"
																		value="${userModel.userDetails[0].user_id}"
																		name="userId">
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-sm-2">First name
																	*:</label>
																<div class="col-sm-10">
																	<input type="text" placeholder="First Name"
																		id="firstName" class="form-control characters"
																		name="firstName"
																		value="${userModel.userDetails[0].first_name}"
																		tabindex="1" required>
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-sm-2">Last name
																	*:</label>
																<div class="col-sm-10">
																	<input type="text" placeholder="Last Name"
																		id="lastName" class="form-control characters"
																		name="lastName"
																		value="${userModel.userDetails[0].last_name}" required
																		tabindex="2">
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-sm-2">Email *:</label>
																<div class="col-sm-10">
																	<input type="text" placeholder="Email Id" id="emailId"
																		class="form-control"
																		value="${userModel.userDetails[0].email_id}" disabled>
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-sm-2">Username *:</label>
																<div class="col-sm-10">
																	<input type="text" placeholder="Email Id" id="emailId"
																		class="form-control"
																		value="${userModel.userDetails[0].user_name}" disabled>
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-2 control-label">Select
																	Role *:</label>
																<div class="col-sm-4">
																	<c:if test="${userModel.userDetails[0].role_id == 2}">
																		<select class="form-control m-b"
																			data-placeholder="Choose a Country..."
																			name="userRole" readonly>
																			<option value="${userModel.userDetails[0].role_id}">Admin</option>
																		</select>
																	</c:if>
																	<c:if test="${userModel.userDetails[0].role_id != 2}">
																		<select id="userRole" class="form-control m-b"
																			data-placeholder="Choose a Country..."
																			name="userRole" tabindex="3">
																			<c:forEach var="roles" items="${userModel.userRoles}">
																				<option value="${roles.role_id}"
																					${roles.role_id == userModel.userDetails[0].role_id ? 'selected="selected"' : ''}>
																					${roles.role_type}</option>
																			</c:forEach>
																		</select>
																	</c:if>
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-2 control-label">User
																	Status:</label>
																<div class="col-sm-10">
																	<div class="radio radio-success radio-inline col-sm-2">
																		<input type="radio" value="1" name="userStatus"
																			${userModel.userDetails[0].user_status == 1 ? 'checked="true" tabindex="4"' : ""}
																			${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
																		<label for="inlineRadio1"> Active </label>
																	</div>
																	<div class="radio radio-danger radio-inline col-sm-2">
																		<input type="radio" value="2" name="userStatus"
																			${userModel.userDetails[0].user_status == 2 ? 'checked="true" tabindex="4"' : ""}
																			${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
																		<label for="inlineRadio2"> Inactive </label>
																	</div>
																</div>
															</div>

															<div class="form-group" id="bugTStatus">
																<label class="col-sm-2 control-label">Bug
																	Tracker Status:</label>
																<div class="col-sm-10">
																	<div class="radio radio-success radio-inline col-sm-2">
																		<input type="radio" value="1" name="bugTrackerStatus"
																			${userModel.userDetails[0].bt_status == 1 ? 'checked="true" tabindex="5"' : ""}
																			${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
																		<label for="inlineRadio1"> Active </label>
																	</div>
																	<div class="radio radio-danger radio-inline col-sm-2">
																		<input type="radio" value="2" name="bugTrackerStatus"
																			${userModel.userDetails[0].bt_status == 2 ? 'checked="true" tabindex="5"' : ""}
																			${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
																		<label for="inlineRadio2"> Inactive </label>
																	</div>
																</div>
															</div>

															<div class="form-group" id="testMStatus">
																<label class="col-sm-2 control-label">Test
																	Manager Status:</label>
																<div class="col-sm-10">
																	<div class="radio radio-success radio-inline col-sm-2">
																		<input type="radio" value="1" name="testManagerStatus"
																			${userModel.userDetails[0].tm_status == 1 ? 'checked="true" tabindex="6"' : ""}
																			${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
																		<label for="inlineRadio1"> Active </label>
																	</div>
																	<div class="radio radio-danger radio-inline col-sm-2">
																		<input type="radio" value="2" name="testManagerStatus"
																			${userModel.userDetails[0].tm_status == 2 ? 'checked="true" tabindex="6"' : ""}
																			${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
																		<label for="inlineRadio2"> Inactive </label>
																	</div>
																</div>
															</div>
															
															
															<div class="form-group ${userModel.jenkinStatus == 2 ? 'hidden' : ''}" id="jenkinStatus">
																<label class="col-sm-2 control-label">Jenkin Status:</label>
																<div class="col-sm-10">
																	<div class="radio radio-success radio-inline col-sm-2">
																		<input type="radio" value="1" name="jenkinStatus"
																			${userModel.userDetails[0].jenkin_status == 1 ? 'checked="true" tabindex="6"' : ""}
																			${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
																		<label for="inlineRadio1"> Active </label>
																	</div>
																	<div class="radio radio-danger radio-inline col-sm-2">
																		<input type="radio" value="2" name="jenkinStatus"
																			${userModel.userDetails[0].jenkin_status == 2 ? 'checked="true" tabindex="6"' : ""}
																			${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
																		<label for="inlineRadio2"> Inactive </label>
																	</div>
																</div>
															</div>
															
															<div class="form-group ${userModel.gitStatus == 2 ? 'hidden' : ''}" id="gitStatus">
																<label class="col-sm-2 control-label">Git Status:</label>
																<div class="col-sm-10">
																	<div class="radio radio-success radio-inline col-sm-2">
																		<input type="radio" value="1" name="gitStatus"
																			${userModel.userDetails[0].git_status == 1 ? 'checked="true" tabindex="6"' : ""}
																			${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
																		<label for="inlineRadio1"> Active </label>
																	</div>
																	<div class="radio radio-danger radio-inline col-sm-2">
																		<input type="radio" value="2" name="gitStatus"
																			${userModel.userDetails[0].git_status == 2 ? 'checked="true" tabindex="6"' : ""}
																			${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
																		<label for="inlineRadio2"> Inactive </label>
																	</div>
																</div>
														</div>
														<div class="form-group ${userModel.redwoodStatus == 0 ? 'hidden' : ''}" id="redwoodStatus">
																<label class="col-sm-2 control-label">BPFT Status:</label>
																<div class="col-sm-10">
																	<div class="radio radio-success radio-inline col-sm-2">
																		<input type="radio" value="1" name="redwoodStatus"
																			${userModel.userDetails[0].redwood_status == 1 ? 'checked="true" tabindex="7"' : ""}
																			${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
																		<label for="inlineRadio1"> Active </label>
																	</div>
																	<div class="radio radio-danger radio-inline col-sm-2">
																		<input type="radio" value="0" name="redwoodStatus"
																			${userModel.userDetails[0].redwood_status == 0 ? 'checked="true" tabindex="7"' : ""}
																			${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
																		<label for="inlineRadio2"> Inactive </label>
																	</div>
																</div>
														</div>
														<div class="form-group ${userModel.apiStatus == 0 ? 'hidden' : ''}" id="apiStatus">
																<label class="col-sm-2 control-label">Api Status:</label>
																<div class="col-sm-10">
																	<div class="radio radio-success radio-inline col-sm-2">
																		<input type="radio" value="1" name="apiStatus"
																			${userModel.userDetails[0].api_status == 1 ? 'checked="true" tabindex="8"' : ""}
																			${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
																		<label for="inlineRadio1"> Active </label>
																	</div>
																	<div class="radio radio-danger radio-inline col-sm-2">
																		<input type="radio" value="0" name="apiStatus"
																			${userModel.userDetails[0].api_status == 0 ? 'checked="true" tabindex="8"' : ""}
																			${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
																		<label for="inlineRadio2"> Inactive </label>
																	</div>
																</div>
														</div>
														</div>
														
														
													</div>
													<div class="hr-line-solid"></div>
												</fieldset>
											</div>
											<div class="actions clearfix">
												<div class="row">
													<div class="col-sm-5">
														<c:if test="${userModel.recentFlag == 1}">
															<button class="btn btn-success pull-left" type="button"
																id="btnEdit" tabindex="7">Edit</button>
															<button class="btn btn-white pull-left" type="button"
																style="display: none; margin-right: 15px;"
																id="btnCancel" tabindex="8">Cancel</button>
															<button class="btn btn-success pull-left" type="submit"
																style="display: none" id="btnUpdate" tabindex="9">Update</button>
														</c:if>
														<c:if test="${userModel.recentFlag == 0}">
															<button class="btn btn-white pull-left"
																style="margin-right: 15px;" type="button" id="btnCancel"
																tabindex="7">Cancel</button>
															<button
																class="btn btn-success pull-left ladda-button ladda-button-demo"
																id="btnUpdate" data-style="slide-up" tabindex="8">Update</button>
														</c:if>
													</div>
												</div>
											</div>
										</form>
										<!-- end form -->
									</div>
									<!-- end ibox-content -->

								</div>
								<!-- end ibox -->
							</div>
						</div>
						<div id="membersTab"
							class="tab-pane <c:if test="${userModel.actionForTab == 'members'}">active </c:if>">
							<div class="panel-body">
								<div class="ibox ">
									<div class="ibox-title">
										<button class="btn btn-success btn-sm pull-right"
											id="showUsersBtn">Add application to user</button>
									</div>
									<div class="ibox-content">
										<div class="panel panel-default hidden" id="addAppPanel">
											<div class="panel-heading">Add Application</div>
											<form action="assignusertoproject"
												class="wizard-big wizard clearfix form-horizontal"
												id="assignusertoproject" method="POST"
												style="overflow: visible !important;">
												<div class="panel-body">
													<div class="content clearfix"
														style="overflow: visible !important;">
														<fieldset class="body current">
															<div class="form-group">
																<label class="col-lg-3 control-label">Select
																	Applications:*</label>
																<div class="col-lg-7">
																	<select data-placeholder="Please Select Applications"
																		class="chosen-select-add" aria-required="true"
																		multiple name="appId" id="selectMultiUser"
																		style="min-width: 100%; z-index: 9999;">
																		<c:forEach var="application"
																			items="${userModel.unassignProject}">
																			<option value="${application.project_id}::${application.project_name}">${application.project_name}</option>
																		</c:forEach>
																	</select>
																	<div class="hidden" id="multiSelect">
																		<span class="error"
																			style="font-weight: bold; color: #8a1f11;">This
																			field is required.</span>
																	</div>
																</div>
															</div>

														</fieldset>
													</div>

												</div>

												<div class="panel-footer">
													<button class="btn btn-white" type="button"
														id="cancelAddUsersBtn">Cancel</button>
													<button class="btn btn-success" type="button"
														id="addApptoUser">Submit</button>

												</div>
											</form>
										</div>
										<div class="table-responsive">
											<table class="table table-striped table-hover"
												id="applicationTable">
												<thead>
													<tr>
														<th>Application</th>
														<th>Status</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>

													<c:forEach var="data" items="${userModel.assignProject}">
														<tr class="assignModuleRow">
															<td>${data.project_name}</td>
															<c:if test="${data.project_active == 1}">
																<td><span class="label label-success">Active</span></td>
															</c:if>
															<c:if test="${data.project_active != 1}">
																<td><span class="label label-danger">Inactive</span></td>
															</c:if>
															<td>
																<button class="btn-white btn btn-xs"
																	onclick="removeApplication(${data.project_id},${userModel.userDetails[0].user_id},'${data.project_name}')"
																	name="${userModel.userDetails[0].user_id}">Remove</button>
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>

										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="moduleTab"
							class="tab-pane <c:if test="${userModel.actionForTab == 'module'}">active </c:if>">
							<div class="panel-body">
								<div class="ibox ">
									<div class="ibox-title">
										<button class="btn btn-success btn-sm pull-right"
											id="showModuleBtn">Add module to user</button>
									</div>
									<div class="ibox-content">
										<div class="panel panel-default hidden" id="showModulePanel">
											<div class="panel-heading">Add Module</div>
											<form action="assignmoduletouser"
												class="wizard-big wizard clearfix form-horizontal"
												id="assignmoduletouser" method="POST"
												style="overflow: visible !important;">
												<div class="panel-body">
													<div class="content clearfix"
														style="overflow: visible !important;">
														<fieldset class="body current">
															<div class="form-group">
																<label class="col-sm-3 control-label">Select
																	Application:*</label>
																<div class="col-sm-4">
																	<select data-placeholder="Please Select Application"
																		class="chosen-select" id="projectIdchosen"
																		style="width: 350px;" name="projectID">
																		<option></option>
																		<c:forEach var="project"
																			items="${userModel.assignProject}">
																			<option value="${project.project_id}">${project.project_name}</option>
																		</c:forEach>
																	</select>
																	<div class="hidden" id="projectSelect">
																		<span class="error"
																			style="font-weight: bold; color: #8a1f11;">This
																			field is required.</span>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<label class="col-lg-3 control-label">Select
																	Modules:*</label>
																<div class="col-lg-7">
																	<select data-placeholder="Please Select Modules"
																		class="chosen-select-add" aria-required="true"
																		multiple name="selectedModules"
																		id="selectMultiUserModule"
																		style="min-width: 100%; z-index: 9999;">
																		<%-- <c:forEach var="data"
																			items="${userModel.unassignModule}">
																			<option value="${data.module_id}">${data.module_name}</option>
																		</c:forEach> --%>
																	</select>
																	<div class="hidden" id="multiModuleSelect">
																		<span class="error"
																			style="font-weight: bold; color: #8a1f11;">This
																			field is required.</span>
																	</div>
																</div>
															</div>

														</fieldset>
													</div>

												</div>
												<div class="panel-footer">
													<button class="btn btn-white" type="button"
														id="cancelAddModulesBtn">Cancel</button>
													<button class="btn btn-success" type="button"
														id="addModuletoUser">Submit</button>

												</div>
											</form>
										</div>
										<div class="table-responsive">
											<table class="table table-striped table-hover"
												id="membersModuleTable">
												<thead>
													<tr>
														<th>Module</th>
														<th>Application</th>
														<th>Status</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data" items="${userModel.assignModule}">
														<tr class="assignModuleRow">
															<td>${data.module_name}</td>
															<td>${data.project_name}</td>
															<c:if test="${data.module_active == 1}">
																<td><span class="label label-success">Active</span></td>
															</c:if>
															<c:if test="${data.module_active != 1}">
																<td><span class="label label-danger">Inactive</span></td>
															</c:if>

															<td>
																<button class="btn-white btn btn-xs"
																	onclick="removeModulefromUser(${userModel.userDetails[0].user_id},${data.module_id},'member')"
																	name="${data.module_id}">Remove</button>
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>

										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});

	$(function() {
		$('#applicationTable').DataTable();
		$('#membersModuleTable').DataTable();
	});

	$('#showUsersBtn').click(function() {
		$('#addAppPanel').removeClass('hidden');
		$(".chosen-select-add").chosen();
	});
	
	$('#showModuleBtn').click(function() {
		$('#showModulePanel').removeClass('hidden');
		$("#projectIdchosen").chosen();
		$(".chosen-select-add").chosen();
	});

	$('#cancelAddUsersBtn').click(function() {
		$('#addAppPanel').addClass('hidden');
	});
	
	
	$('#cancelAddModulesBtn').click(function() {
		$('#showModulePanel').addClass('hidden');
	});
</script>


<script type="text/javascript">

	$(document).ready(function() {

		$('.summernote').summernote();
		$('.note-editor').css('background', 'white');
		$('button[ data-event="codeview"]').remove();

		$('div.note-insert').remove();
		$('div.note-table').remove();
		$('div.note-help').remove();
		$('div.note-style').remove();
		$('button[ data-event="removeFormat"]').remove();
		$('button[ data-event="italic"]').remove();
		$('button[ data-event="fullscreen"]').remove();
		$('button[ data-original-title="Line Height"]').remove();
		$('button[ data-original-title="Font Family"]').remove();
		$('button[ data-original-title="Paragraph"]').remove();

		$("#form").validate({
			rules : {
				moduleName : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				moduleDescription : {
					minlength : 1,
					maxlength : 200
				}
			}
		});
	});
	
	function removeModulefromUser(user_id,module_id){
		swal({
			title : "Are you sure?",
			text : "Do you want to remove module from user!",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "Yes, Remove it!",
			cancelButtonText : "No, cancel!",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function(isConfirm) {

			if (isConfirm) {
				
				$('body').addClass("white-bg");
				$("#barInMenu").removeClass("hidden");
				$("#wrapper").addClass("hidden");
				
				var posting = $.post('removemoduleuser',{
					userId : user_id,
					moduleId : module_id
				});
				
				posting.done(function(data){
					window.location.href = "usersettings";
				}); 	
			}
		});
		
	}
	
	
	
	
	function removeApplication(app_id,user_id,app_name){
		
		swal({
			title : "Are you sure?",
			text : "Do you want to remove application from user!",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "Yes, Remove it!",
			cancelButtonText : "No, cancel!",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function(isConfirm) {

			if (isConfirm) {
				
				$('body').addClass("white-bg");
				$("#barInMenu").removeClass("hidden");
				$("#wrapper").addClass("hidden");
				
				var posting = $.post('unassignapptouser',{
					appID : app_id,
					userID : user_id,
					appName : app_name
				});
				
				posting.done(function(data){
					if(data == 500){
						location.href  = "500";
					}else if(data == 200){
						location.reload();
					}else{
						location.href  = "404";
					}
				}); 	
			}
		});
		
	}
	
	$("#addApptoUser").click(function()
			{
		
		var values = $('#selectMultiUser').val();
	    
		
			 if($("#assignusertoproject").valid())
				{
				 if(values!=null)
					 {
					    $('body').addClass("white-bg");
						$("#barInMenu").removeClass("hidden");
						$("#wrapper").addClass("hidden");
						
					 $("#assignusertoproject").submit();
					 }
				 else
					 {
					 $("#multiSelect").removeClass("hidden");
					 } 
				}
			});
	
	
	$("#addModuletoUser").click(function()
			{
		
		var values = $('#selectMultiUserModule').val();
		var projectvalues = $('#projectIdchosen').val();
		
			 if($("#assignmoduletouser").valid())
				{
				 if(projectvalues!='')
					 {
					 if(values!=null)
						 {
						    $('body').addClass("white-bg");
							$("#barInMenu").removeClass("hidden");
							$("#wrapper").addClass("hidden");
							
						    $("#assignmoduletouser").submit();
						 }
					 else
						 {
						 $("#multiModuleSelect").removeClass("hidden");
						 }
					 }
				 else
					 {
					     $("#projectSelect").removeClass("hidden");
					 }
				}
			});
	
	
	
	$("#selectMultiUser").change(function() {
		$("#multiSelect").addClass("hidden");
	});
	
	
	$("#selectMultiUserModule").change(function() {
		$("#multiModuleSelect").addClass("hidden");
	});
	
	$("#projectIdchosen").change(function() {
		$("#projectSelect").addClass("hidden");
		$("#selectMultiUserModule").val('').trigger('chosen:updated');
		$("#selectMultiUserModule").find('option').remove();

		var projectvalues = $('#projectIdchosen').val();
		var moduleValue = '${userModel.unassignModuleJson}';
		moduleValue= JSON.parse(moduleValue);
		if(projectvalues!='')
			{
			  for(i=0;i<moduleValue.length;i++)
				  {
				   if(projectvalues==moduleValue[i].project_id)
					  {
					     $("#selectMultiUserModule").append('<option value="'+moduleValue[i].module_id+'">'+moduleValue[i].module_name+'</option>');
					     $("#selectMultiUserModule").trigger("chosen:updated");
					  }
				  }
			}
		
		
	});
	

	$("#btnUpdate").click(function() {
		var formValid = $("#form").valid();
		if (formValid) {
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");

			$('.summernote').each(function() {
				$(this).val($(this).code());
			})

			$('#form').submit();
		}
	});
</script>

<script type="text/javascript">
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
		$("#form").validate();
	 	$('#form').bind('submit', function() {
	 		if($("#form").valid()){
			$(this).find(':input').prop('disabled', false);
	 		}
		}); 
		
		var btStatus = '${userModel.bugTStatus}';
		if(btStatus == 2){
			$('#bugTStatus').addClass('hidden');
		}
		
		var tmStatus = '${userModel.testMStatus}';
		if(tmStatus == 2){
			$('#testMStatus').addClass('hidden');
		}
	});
</script>
<script type="text/javascript">

	$("#btnCancel").click(function() {
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		window.location.href="viewuser";
	});
	
	$("#btnEdit").click(function() {
		$('#fieldset').removeAttr("disabled");
		$('#btnUpdate').show();
		$('#btnCancel').show();
		$(this).hide();
	});
	
	$("#btnUpdate").click(function() {
		var form = $( "#form" );
		form.validate();
		if(form.valid()){
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
		}
	});
	
	var l = $('.ladda-button-demo').ladda();
	l.click(function() {
		// Start loading
		l.ladda('start');
		// Do something in backend and then stop ladda
		// setTimeout() is only for demo purpose
		setTimeout(function() {
			//l.ladda('stop');
			$('#form').submit();
			l.ladda('stop');
		}, 1000)
	});
</script>



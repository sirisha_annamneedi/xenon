<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style>
.has-error {
	color: #8a1f11;
}
</style>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Add User</h2>
		<ol class="breadcrumb">
			<li><a href="admindashboard">Dashboard</a></li>
			<li class="active"><strong>Add User</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form id="form" action="insertuser" method="POST"
						class="wizard-big no-overflow" autocomplete="off">
						<h1>Account</h1>
						<fieldset>
							<div class="row">
										<label class="col-lg-4 pull-right text-right">* fields
											are mandatory</label>
									</div>
							<h2>Account Information</h2>
							<div class="alert alert-danger hidden errorMessage1" id="errorMessage1">
								User already exists with this email id.</div>
							<div class="alert alert-danger hidden errorMessage2" id="errorMessage2">
								User already exists with this user name.</div>
							<div class="row">
								<div class="col-lg-8">
									<div class="form-group">
										<label>First name *</label> <input id="firstName"
											name="firstName" type="text" class="form-control required characters"
											tabindex="1">
									</div>
									<div class="form-group">
										<label>Last name *</label> <input id="lastName"
											name="lastName" type="text" class="form-control required characters"
											tabindex="2">
									</div>
									<div class="form-group">
										<label>Username *</label> <input id="usernameLogin"
											name="usernameLogin" type="text" class="form-control required characters"
											tabindex="2">
									</div>
									<div class="form-group">
										<label>Email *</label> 
										<input  id="email" name="emailId" onkeypress="checkEmailID()" onkeyup="checkEmailID()" id="txtPassword"
											type="text" class="form-control required email" tabindex="3">
										<label class="error hidden" id="errorEmailID"></label>
									</div>

									<c:if test="${userModel.emailSetting=='disable'}">
										<div class="form-group">
											<label>Set Password *</label> <input onkeypress="checkPassword()" onkeyup="checkPassword()" id="txtPassword"
												name="password" type="text"
												class="form-control  required password" tabindex="3">
											<label class="error hidden" id="errorPassword">This
												field is required.</label>
										</div>
									</c:if>
								</div>
							</div>
						</fieldset>
						<h1>Profile</h1>
						<fieldset>
							<label class="col-lg-2 ">* fields are mandatory</label><br>
							<h2>Profile Information</h2>
							<div class="row">
								<div class="col-lg-8">
									<div class="form-group">
										<div class="row">
											<label class="col-sm-3 margine-lable" id="select_role">Select
												Role*</label>
											<div class="input-group col-sm-6">
												<select id="userRole" class="chosen-select"
													data-placeholder="Choose a role" name="userRole"
													tabindex="4" style="min-width: 250px;"
													onchange="roleChange()">
													<option value="0"></option>
													<c:forEach var="roles" items="${userModel.userRoles}">
														<option value="${roles.role_id}">${roles.role_type}</option>
													</c:forEach>
												</select>
											</div>
											<div class="col-sm-offset-3 hidden" id="roleErrorMessage">
											<span class="has-error" style="font-weight: bold;">This field is required.</span></div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-3">Assign Applications</label>
											<div class="input-group col-sm-6">
												<c:if test="${userModel.projects.size()==0}">
													<select data-placeholder="No application for assign"
														class="chosen-select-add" multiple name="projects"
														tabindex="4" style="min-width: 100%; z-index: 9999;">

													</select>

												</c:if>

												<c:if test="${userModel.projects.size()>=1}">
													<select data-placeholder="Please assign project"
														class="chosen-select" multiple name="projects"
														tabindex="4" style="min-width: 350px;">
														<c:forEach var="projects" items="${userModel.projects}">
															<option value="${projects.project_id}">${projects.project_name}</option>
														</c:forEach>
													</select>

												</c:if>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">User Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="activeUser" value="1"
														name="userStatus" checked=""> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inactiveUser" value="2"
														name="userStatus"> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>
									<div
										class="row 
									${userModel.bugTStatus == 2 ? 'hidden' : ''}">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Bug Tracker
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="activeBugTracker" value="1"
														name="bugTrackerStatus"> <label for="inlineRadio1">
														Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inactiveBugTracker" value="2"
														name="bugTrackerStatus" checked=""> <label
														for="inlineRadio2"> Inactive </label>
												</div>
											</div>
										</div>
									</div>
									<div
										class="row 
									${userModel.testMStatus == 2 ? 'hidden' : ''}">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Test Manager
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="activeTestManager" value="1"
														name="testManagerStatus"> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inactiveTestManager" value="2"
														name="testManagerStatus" checked=""> <label
														for="inlineRadio2"> Inactive </label>
												</div>
											</div>
										</div>
									</div>
									
									<div
										class="row 
									${userModel.jenkinStatus == 2 ? 'hidden' : ''}">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Jenkin
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio"  value="1"
														name="jenkinStatus"> <label
														for="inlineRadio11"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" value="2"
														name="jenkinStatus" checked=""> <label
														for="inlineRadio12"> Inactive </label>
												</div>
											</div>
										</div>
									</div>
									
									<div
										class="row 
									${userModel.gitStatus == 2 ? 'hidden' : ''}">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Git
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio"value="1"
														name="gitStatus"> <label
														for="inlineRadio12"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio"  value="2"
														name="gitStatus" checked=""> <label
														for="inlineRadio14"> Inactive </label>
												</div>
											</div>
										</div>
									</div>
									<div
										class="row 
									${userModel.redwoodStatus == 0 ? 'hidden' : ''}">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Redwood
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio"value="1"
														name="redwoodStatus"> <label
														for="inlineRadio12"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio"  value="0"
														name="redwoodStatus" checked=""> <label
														for="inlineRadio14"> Inactive </label>
												</div>
											</div>
										</div>
									</div>
									<div
										class="row 
									${userModel.apiStatus == 0 ? 'hidden' : ''}">
										<div class="form-group">
											<label class="col-sm-3 margine-lable">Api
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio"value="1"
														name="apiStatus"> <label
														for="inlineRadio12"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio"  value="0"
														name="apiStatus" checked=""> <label
														for="inlineRadio14"> Inactive </label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
						<h1>Finish</h1>
						<fieldset disabled>
							<label class="col-lg-2 ">* fields are mandatory</label><br>
							<h2>User Information</h2>
							<div class="alert alert-danger hidden" id="errorMessage">
								User already exists with this email id.</div>
							<div class="alert alert-danger hidden" id="errorMessage3">
								User already exists with this user name.</div>
							<div class="row">
								<div class="form-group col-sm-9">
									<label>First name *</label> <input id="submittedFirstName"
										type="text" class="form-control required">
								</div>
								<div class="form-group col-sm-9">
									<label>Last name *</label> <input id="submittedLastName"
										type="text" class="form-control required">
								</div>
								<div class="form-group col-sm-9">
									<label>User name *</label> <input id="submittedUsernameLogin"
										type="text" class="form-control required">
								</div>
								<div class="form-group col-sm-9">
									<label>Email *</label> <input id="submittedEmail" type="text"
										class="form-control required email">
								</div>
							</div>
							<div class="row">
								<div class="col-lg-8">
									<div class="form-group">
										<div class="row">
											<label class="font-noraml col-sm-3">User Role</label>
											<div class="input-group">
												<select id="submittedUserRole" class="form-control m-b"
													data-placeholder="Choose a Country...">
													<c:forEach var="roles" items="${userModel.userRoles}">
														<option value="${roles.role_id}">${roles.role_type}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>


									<div class="row">
										<div class="form-group">
											<label class="font-noraml col-sm-3">User Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="activeUser" value="1"
														name="submittedUserStatus" checked=""> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inactiveUser" value="2"
														name="submittedUserStatus"> <label
														for="inlineRadio2"> Inactive </label>
												</div>
											</div>
										</div>
									</div>
									<div
										class="row
									${userModel.bugTStatus == 2 ? 'hidden' : ''}">
										<div class="form-group">
											<label class="font-noraml col-sm-3">Bug Tracker
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="activeBugTracker" value="1"
														name="submittedBTStatus" checked=""> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inactiveBugTracker" value="2"
														name="submittedBTStatus"> <label
														for="inlineRadio2"> Inactive </label>
												</div>
											</div>
										</div>
									</div>
									<div
										class="row
									${userModel.testMStatus == 2 ? 'hidden' : ''}">
										<div class="form-group">
											<label class="font-noraml col-sm-3">Test Manager
												Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="activeTestManager" value="1"
														name="submittedTMStatus" checked=""> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inactiveTestManager" value="2"
														name="submittedTMStatus"> <label
														for="inlineRadio2"> Inactive </label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>

</div>

<style>
.margine-lable {
	margin-bottom: 2.0em !important;
}
</style>
<script type="text/javascript">
	$(window).load(function() {
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
		
		$(".content").addClass("no-overflow");
	})
</script>
<script type="text/javascript">
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});
</script>
<script>
	$(document)
			.ready(
					function() {
						
					
						
						var emaiSetting = "${userModel.emailSetting}";
						$("#form")
								.steps(
										
										{
											
											
											bodyTag : "fieldset",
											onStepChanging : function(event,
													currentIndex, newIndex) {
												// Always allow going backward even if the current step contains invalid fields!

												
												if (currentIndex > newIndex) {
													return true;
												}
												
												var form = $(this);
												
												if (currentIndex == 1) {

													var value = $('#userRole')
															.val();

													if (value == 0) {
														$('#select_role').addClass('has-error');
														$('#roleErrorMessage').removeClass('hidden');
														$('#userRole_chosen').css( "border", " 1px solid #fbc2c4" );
														
														//$("#select_role").trigger('chosen:updated');
														return false;
													} else {
														$('#select_role').removeClass('has-error');
														$('#roleErrorMessage').addClass('hidden');
														$('#userRole_chosen').css( "border", " 1px solid #e7eaec" );
													}

												}

												//check for the validity of select

												// Clean up if user went backward before
												if (currentIndex < newIndex) {
													// To remove error styles
													$(
															".body:eq("
																	+ newIndex
																	+ ") label.error",
															form).remove();
													$(
															".body:eq("
																	+ newIndex
																	+ ") .error",
															form).removeClass(
															"error");
													
													$("#errorMessage").addClass('hidden');
													$(".errorMessage1").addClass('hidden');
													$("#errorMessage2").addClass('hidden');
													$("#errorMessage3").addClass('hidden');
												}

												// Disable validation on fields that are disabled or hidden.
												form.validate().settings.ignore = ":disabled,:hidden";

												// Start validation; Prevent going forward if false
												if(form.valid())
													{
													if (currentIndex == 0) {
														var status =  checkEmailID();
														if(status =="false"){
															var errorMessage = "The Email can only consist of alphabetical, number, dot, underscore and @";
															$('#errorEmailID').text(errorMessage);
															$('#errorEmailID').removeClass('hidden');
															$('#errorEmailID').show();
														}
														return status;
													}
													
													if (currentIndex == 0) {
														if(emaiSetting=='disable')
															return checkPassword();
														else
															return form.valid();
														
													}
													else
														{
														return form.valid();
														}
													}
												else
													{
													return form.valid();
													}

											},
											onStepChanged : function(event,
													currentIndex, priorIndex) {
												// Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
												if (currentIndex === 2
														&& priorIndex === 3) {
													$(this).steps("previous");
												}
												if (currentIndex === 1) {
													var config = {
														'.chosen-select' : {},
														'.chosen-select-deselect' : {
															allow_single_deselect : true
														},
														'.chosen-select-no-single' : {
															disable_search_threshold : 10
														},
														'.chosen-select-no-results' : {
															no_results_text : 'Oops, nothing found!'
														},
														'.chosen-select-width' : {
															width : "95%"
														}
													}
													for ( var selector in config) {
														$(selector)
																.chosen(
																		config[selector]);
													}

												}
												if (currentIndex === 2) {

													$("#submittedUserName")
															.val(
																	$(
																			"#userName")
																			.val());
													$("#submittedFirstName")
															.val(
																	$(
																			"#firstName")
																			.val());
													$("#submittedLastName")
															.val(
																	$(
																			"#lastName")
																			.val());
													$("#submittedUsernameLogin")
													.val(
															$(
																	"#usernameLogin")
																	.val());
													
													$("#submittedEmail").val(
															$("#email").val());
													$("#submittedUserRole")
															.val(
																	$(
																			"#userRole")
																			.val());
													$(
															"input:radio[name=submittedUserStatus][value="
																	+ $(
																			"input:radio[name=userStatus]:checked")
																			.val()
																	+ "]")
															.prop("checked",
																	true);
													$(
															"input:radio[name=submittedBTStatus][value="
																	+ $(
																			"input:radio[name=bugTrackerStatus]:checked")
																			.val()
																	+ "]")
															.prop("checked",
																	true);
													$(
															"input:radio[name=submittedTMStatus][value="
																	+ $(
																			"input:radio[name=testManagerStatus]:checked")
																			.val()
																	+ "]")
															.prop("checked",
																	true);
												}
											},
											onCanceled : function(event,
													currentIndex) {
												window.location.href = "viewuser";
											},
											onFinishing : function(event,
													currentIndex) {
												var form = $(this);

												// Disable validation on fields that are disabled.
												// At this point it's recommended to do an overall check (mean ignoring only disabled fields)
												form.validate().settings.ignore = ":disabled";

												// Start validation; Prevent form submission if false
												return form.valid();
											},
											onFinished : function(event,
													currentIndex) {
												var form = $(this);//
												$("#barInMenu").removeClass("hidden");
												$("#wrapper").addClass("hidden");
												
												$.post("checkuser",
												{
													emailId : $("#email").val(),
													userName : $("#usernameLogin").val()
												},
												function(response) {
													var responseJson=JSON.parse(response);
													if (responseJson.emailFlag=="true" &&  responseJson.usernameFlag) {
														form.submit();
													} else if(responseJson.emailFlag=="false") {
														
														$("#barInMenu").addClass("hidden");
														$("#wrapper").removeClass("hidden");
														
														$("#submittedEmail").addClass('error');
														$("#email").addClass('error');
														$("#errorMessage").removeClass('hidden');
														$(".errorMessage1").removeClass('hidden');
													}else if(!responseJson.usernameFlag) {

														$("#barInMenu").addClass("hidden");
														$("#wrapper").removeClass("hidden");
														
														$("#submittedUsernameLogin").addClass('error');
														$("#usernameLogin").addClass('error');
														$("#errorMessage2").removeClass('hidden');
														$("#errorMessage3").removeClass('hidden');
													}
												});
											}
										}).validate({
									errorPlacement : function(error, element) {
										element.before(error);
									},
									rules : {
										confirm : {
											equalTo : "#password"
										}
									}
								});
						
							
					});
	function checkPassword(){
		var checkId="#txtPassword";
		var msgboxId = "#errorPassword";
		var errorMsg = "";
		var flag = 0;
		var pattern1 = /[0-9]/;
		var pattern2 = /[a-z]/;
		var pattern3 = /[A-Z]/;
		var pattern4 = /[!@#\$]/;
		var pattern = /[a-zA-Z0-9]/;
		var password = $(checkId).val();
		if (password.length>7 && password.length<17 ) {

		} else {
			errorMsg += "Password must be 8 - 16 characters long<br>";
			flag = 1;
		}
		if ($(checkId).val().match(pattern1)) {

		} else {
			errorMsg += "Password must contain at least one numeric<br>";
			flag = 1;
		}

		if ($(checkId).val().match(pattern2)) {
		} else {
			errorMsg += "Password must contain at least one a-z<br>";
			flag = 1;
		}
		if ($(checkId).val().match(pattern3)) {
		} else {
			errorMsg += "Password must contain at least one A-Z<br>";
			flag = 1;
		}
		if ($(checkId).val().match(pattern4)) {
		} else {
			errorMsg += "Password must contain at least one special symbol from !@#$<br>";
			flag = 1;
		}
	 if (flag == 1) {
			//$(msgboxId).html("Password must contain at least one A-Z and  a-z  and any one special character from !@#$");
			$(msgboxId).html(errorMsg);
			$(msgboxId).removeClass("hidden");
			$(msgboxId).css("display","");
			return false;

		} else {
			$(msgboxId).html("This field is required");
			$(msgboxId).addClass("hidden");
			return true;
		}
		
	};
	
	function roleChange(){
		$('#select_role').removeClass('has-error');
		$('#roleErrorMessage').addClass('hidden');
		$('#userRole_chosen').css( "border", " 1px solid #e7eaec" );
	}
	
	function checkEmailID(){
		var pattern = /[^A-Za-z0-9@_.]/g;
		var emailID = $('#email').val();
		if(emailID.match(pattern)){
			var errorMessage = "The Email can only consist of alphabetical, number, dot, underscore and @";
			$('#errorEmailID').text(errorMessage);
			$('#errorEmailID').removeClass('hidden');
			return false;
		}else{	
			$('#errorEmailID').addClass('hidden');
			return true;
		}
	}
	
</script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="md-skin">
<div class="wrapper">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Application Assigned to - ${userModel.userFullName}</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li><a href="viewuser">Users</a></li>
				<li class="active"><strong>Applications Assigned to user</strong></li>
			</ol>
		</div>
		<!-- end col-lg-10 -->
	</div>
	<!-- end row -->
</div>
<!-- end wrapper -->

<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>Assigned Applications</h5>
				</div>
				<div class="ibox-content">
					<input type="text" class="form-control input-sm m-b-xs" id="filter"
						placeholder="Search">
					<table class="footable table table-stripped toggle-arrow-tiny"
						data-page-size="10" data-filter="#filter">
						<thead>
							<tr>
								<th>Name</th>
								<th>Status</th>
								<th data-hide="all"></th>
								
							</tr>
						</thead>
						<tbody>
							<c:set var="proId" value="0" scope="page" />
							<c:forEach var="data" items="${userModel.details}">
								<c:if test="${proId != data.project_id }">
									<tr>
										<td>${data.project_name}</td>
										<c:if test="${data.project_active == 1}">
											<td><span class="label label-success">Active</span></td>
										</c:if>
										<c:if test="${data.project_active != 1}">
											<td><span class="label label-danger">Inactive</span></td>
										</c:if>
										<td>
											<table class="table datatable moduleTable">
												<thead>
													<tr>
														<th>Module Name</th>
														<th>Module Status</th>
														
													</tr>
												</thead>
												<tbody>
													<c:forEach var="model" items="${userModel.details}">
														
														
														<c:if test="${data.project_id == model.project_id}">
														<c:if test="${model.user_id  == userModel.userID}">
															<tr>
																<td>${model.module_name}</td>
																<c:if test="${model.module_active == 1}">
																	<td><span class="label label-success">Active</span></td>
																</c:if>
																<c:if test="${model.module_active != 1}">
																	<td><span class="label label-danger">Inactive</span></td>
																</c:if>
															</tr>

														</c:if>
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</td>
										
									</tr>
								</c:if>
								<c:set var="proId" value="${data.project_id}" scope="page" />
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="6">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	 var foo = $('.footable').footable();
	 foo.trigger('footable_initialize'); //Reinitialize
	 foo.trigger('footable_redraw'); //Redraw the table
	 foo.trigger('footable_resize'); //Resize the table
})

	$(document).ready(function() {
		$('.moduleTable').DataTable({
			"paging" : false,
			"lengthChange" : false,
			"searching" : false,
			"ordering" : false,
			"info" : true,
			"autoWidth" : true
		});

	});
</script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Edit User -
			${userModel.userDetails[0].first_name}&nbsp;${userModel.userDetails[0].last_name}</h2>
		<ol class="breadcrumb">
			<li><a href="admindashboard">Dashboard</a></li>
			<li><a href="viewuser">Users List</a></li>
			<li class="active"><strong>Edit User</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form action="updateuser" method="POST" id="form"
						class="wizard-big wizard clearfix form-horizontal">
						<div class="content clearfix">
							<fieldset class="body current" id="fieldset">
								<div class="row">
									<div class="col-lg-10">
										<div class="form-group hidden">
											<label class="control-label col-sm-2">UserId</label>
											<div class="col-sm-10">
												<input type="text" placeholder="User Id"
													class="form-control"
													value="${userModel.userDetails[0].user_id}" name="userId">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-2">First name *</label>
											<div class="col-sm-10">
												<input type="text" placeholder="First Name" id="firstName"
													class="form-control characters" name="firstName"
													value="${userModel.userDetails[0].first_name}" tabindex="1" required>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-2">Last name *</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Last Name" id="lastName"
													class="form-control characters" name="lastName"
													value="${userModel.userDetails[0].last_name}" required tabindex="2">
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-sm-2">Email *</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Email Id" id="emailId"
													class="form-control"
													value="${userModel.userDetails[0].email_id}" disabled>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Select Role *</label>
											<div class="col-sm-4">
												<c:if test="${userModel.userDetails[0].role_id == 2}">
													<select class="form-control m-b"
														data-placeholder="Choose a Country..." name="userRole"
														readonly>
														<option value="${userModel.userDetails[0].role_id}"  >Admin</option>
													</select>
												</c:if>
												<c:if test="${userModel.userDetails[0].role_id != 2}">
													<select id="userRole" class="form-control m-b"
														data-placeholder="Choose a Country..." name="userRole" tabindex="3">
														<c:forEach var="roles" items="${userModel.userRoles}">
															<option value="${roles.role_id}"
																${roles.role_id == userModel.userDetails[0].role_id ? 'selected="selected"' : ''}>
																${roles.role_type}</option>
														</c:forEach>
													</select>
												</c:if>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">User Status</label>
											<div class="col-sm-10">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" value="1" name="userStatus"
														${userModel.userDetails[0].user_status == 1 ? 'checked="true" tabindex="4"' : ""}
														${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
													<label for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" value="2" name="userStatus"
														${userModel.userDetails[0].user_status == 2 ? 'checked="true" tabindex="4"' : ""}
														${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
													<label for="inlineRadio2"> Inactive </label>
												</div>
											</div>
										</div>

										<div class="form-group" id="bugTStatus">
											<label class="col-sm-2 control-label">Bug Tracker
												Status</label>
											<div class="col-sm-10">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" value="1" name="bugTrackerStatus"
														${userModel.userDetails[0].bt_status == 1 ? 'checked="true" tabindex="5"' : ""}
														${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
													<label for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" value="2" name="bugTrackerStatus"
														${userModel.userDetails[0].bt_status == 2 ? 'checked="true" tabindex="5"' : ""}
														${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
													<label for="inlineRadio2"> Inactive </label>
												</div>
											</div>
										</div>

										<div class="form-group" id="testMStatus">
											<label class="col-sm-2 control-label">Test Manager
												Status</label>
											<div class="col-sm-10">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" value="1" name="testManagerStatus"
														${userModel.userDetails[0].tm_status == 1 ? 'checked="true" tabindex="6"' : ""}
														${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
													<label for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" value="2" name="testManagerStatus"
														${userModel.userDetails[0].tm_status == 2 ? 'checked="true" tabindex="6"' : ""}
														${userModel.userDetails[0].role_id == 2 ? 'disabled="disabled"' : ""}>
													<label for="inlineRadio2"> Inactive </label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-5">
									<c:if test="${userModel.recentFlag == 1}">
										<button class="btn btn-success pull-left" type="button"
											id="btnEdit" tabindex="7">Edit</button>
										<button class="btn btn-white pull-left" type="button"
											style="display: none; margin-right: 15px;" id="btnCancel" tabindex="8">Cancel</button>
										<button class="btn btn-success pull-left" type="submit"
											style="display: none" id="btnUpdate" tabindex="9">Update</button>
									</c:if>
									<c:if test="${userModel.recentFlag == 0}">
										<button class="btn btn-white pull-left"
											style="margin-right: 15px;" type="button" id="btnCancel" tabindex="7">Cancel</button>
										<button
											class="btn btn-success pull-left ladda-button ladda-button-demo"
											id="btnUpdate" data-style="slide-up" tabindex="8">Update</button>
									</c:if>
								</div>
							</div>
						</div>
					</form>
					<!-- end form -->
				</div>
				<!-- end ibox-content -->

			</div>
			<!-- end ibox -->
		</div>
		<!-- end col -->
	</div>
	<!-- end row -->
</div>
</div>

<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
})

</script>
<script type="text/javascript">
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
		$("#form").validate();
	 	$('#form').bind('submit', function() {
	 		if($("#form").valid()){
			$(this).find(':input').prop('disabled', false);
	 		}
		}); 
		
		var btStatus = '${userModel.bugTStatus}';
		if(btStatus == 2){
			$('#bugTStatus').addClass('hidden');
		}
		
		var tmStatus = '${userModel.testMStatus}';
		if(tmStatus == 2){
			$('#testMStatus').addClass('hidden');
		}
	});
</script>
<script type="text/javascript">

	$("#btnCancel").click(function() {
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		window.location.href="viewuser";
	});
	
	$("#btnEdit").click(function() {
		$('#fieldset').removeAttr("disabled");
		$('#btnUpdate').show();
		$('#btnCancel').show();
		$(this).hide();
	});
	
	$("#btnUpdate").click(function() {
		var form = $( "#form" );
		form.validate();
		if(form.valid()){
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
		}
	});
	
	var l = $('.ladda-button-demo').ladda();
	l.click(function() {
		// Start loading
		l.ladda('start');
		// Do something in backend and then stop ladda
		// setTimeout() is only for demo purpose
		setTimeout(function() {
			//l.ladda('stop');
			$('#form').submit();
			l.ladda('stop');
		}, 1000)
	});
</script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Access Control Settings</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li class="active"><strong>Access Control Settings </strong></li>
			</ol>
			<!-- end breadcrumb -->
		</div>

		<!-- end col-lg-10 -->
	</div>
	<!-- end wrapper -->
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content">
						<form:form action="#"
							class="wizard-big wizard clearfix form-horizontal" method="POST"
							id="accessControlForm">
							<div class="clearfix">
								<fieldset class="body current">
									<label class="col-lg-4 pull-right">* fields are
										mandatory</label><br>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<label class="control-label col-lg-2">Access Control</label>
												<div class="col-lg-10">
													<div class="radio radio-success">
														<input type="radio" id="ldapInactive" value="2"
															name="accessControlChecked" checked=""> <label>Xenon</label>
													</div>
													<div class="radio radio-success">
														<input id="ldapActive" type="radio" value="1"
															name="accessControlChecked"> <label>LDAP</label>
													</div>

													<div id="ldapActiveDiv" style="text-align: left;"
														class="hidden">
														<div class="form-group">
															<label class="control-label col-lg-2 pull-left">URL
																*</label>
															<div class="col-lg-10">
																<input type="text"
																	class="form-control required characters" name="ldapUrl"
																	value="${Model.ldapDetails[0].ldap_url}" id="ldapUrl" />
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-lg-2">Root DN *</label>
															<div class="col-lg-10">
																<input type="text"
																	class="form-control required characters"
																	value="${Model.ldapDetails[0].root_dn}"
																	name="ldapRootDN" id="ldapRootDN" />
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-lg-2">User Search
																Base</label>
															<div class="col-lg-10">
																<input type="text" class="form-control"
																	value="${Model.ldapDetails[0].user_search_base}"
																	name="ldapSearchBase" id="ldapSearchBase" />
															</div>
														</div>
														<!-- <div class="form-group">
															<label class="control-label col-lg-2">Email Address LDAP attribute</label>
															<div class="col-lg-10">
																<input type="text" value="mail" class="form-control"
																	name="emailAttrName" />
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-lg-2">Email Address Domain Name *</label>
															<div class="col-lg-10">
																<input type="text" class="form-control"
																	name="emailDomainName" />
															</div>
														</div> -->
														
														<div class="form-group">
															<label class="control-label col-lg-2">User Search
																Filter *</label>
															<div class="col-lg-10">
																<select data-placeholder="Select" class="chosen-select"
																	style="width: 350px;" tabindex="2"
																	name="userSearchFilter" id="userSearchFilter">
																	<option value="mail={0}">mail={0}</option>
																	 <option value="cn={0}">cn={0}</option>
																	<option value="uid={0}">uid={0}</option>
																	<option value="sAMAccountName={0}">sAMAccountName={0}</option> 
																</select>
															</div>
														</div>
														
														<div class="form-group">
															<label class="control-label col-lg-2">User
																Distinguished Name *</label>
															<div class="col-lg-10">
																<input type="text"
																	class="form-control required characters"
																	value="${Model.ldapDetails[0].user_dn}"
																	name="ldapUserDN" id="ldapUserDN" />
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-lg-2">User
																Password *</label>
															<div class="col-lg-10">
																<input type="password"
																	class="form-control required characters"
																	value="${Model.ldapDetails[0].user_password}"
																	name="ldapUserPass" id="ldapUserPass" />
															</div>
														</div>
														
															<div class="form-group">
														<div class="col-lg-2"></div>
															<label class="control-label col-lg-10 text-success" style="text-align: left;">Note: Below user mail will be mapped as admin</label>
														</div>
														<div class="form-group">
															<label class="control-label col-lg-2">Admin User
																mail *</label>
															<div class="col-lg-10">
																<input type="text"
																	class="form-control required characters"
																	value="${Model.ldapDetails[0].user_mail}"
																	name="ldapUserMail" id="ldapUserMail" />
															</div>
														</div>
														<button type="button" id="btnTestLdap"
															class="btn-sm btn-success pull-right ladda-button"
															data-style="slide-up">Test LDAP Settings</button>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="hr-line-solid"></div>
									<br>
								</fieldset>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-md-12">
										<button class="btn btn-white pull-left"
											style="margin-right: 15px" type="button" id="btnCancel">Cancel</button>
										<button type="button" id="btnSubmit"
											class="btn btn-success pull-left ladda-button"
											data-style="slide-up">Submit</button>
									</div>
								</div>
							</div>
						</form:form>


						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<!-- end row -->
	</div>
</div>
<!-- end .md-skin -->
<input type="hidden" value="" id="userData">

<script type="text/javascript">
	$(window).load(
			function() {
				$('body').removeClass("white-bg");
				$("#barInMenu").addClass("hidden");
				$("#wrapper").removeClass("hidden");
				var config = {
					'.chosen-select' : {},
					'.chosen-select-deselect' : {
						allow_single_deselect : true
					},
					'.chosen-select-no-single' : {
						disable_search_threshold : 10
					},
					'.chosen-select-no-results' : {
						no_results_text : 'Oops, nothing found!'
					},
					'.chosen-select-width' : {
						width : "95%"
					}
				}
				
				for ( var selector in config) {
					$(selector).chosen(config[selector]);
					$("ul.chosen-results").css('max-height', '150px');
				}

				var accesscontrolchecked = ${Model.customerLDAPStatus};
				$(
						'input:radio[name="accessControlChecked"][value='
								+ accesscontrolchecked + ']').prop('checked',
						true);
				if (accesscontrolchecked == 2) {
					$("#ldapActiveDiv").addClass("hidden");
				} else {
					$("#ldapActiveDiv").removeClass("hidden");
				}
			});

	$(document).ready(function() {

		$('.summernote').summernote();
		$('.note-editor').css('background', 'white');
		$('button[ data-event="codeview"]').remove();
		$('.note-editor').css('border', '1px solid #CBD5DD');

		$('div.note-insert').remove();
		$('div.note-table').remove();
		$('div.note-help').remove();
		$('div.note-style').remove();
		$('div.note-color').remove();
		$('button[ data-event="removeFormat"]').remove();
		$('button[ data-event="insertUnorderedList"]').remove();
		$('button[ data-event="fullscreen"]').remove();
		$('button[ data-original-title="Line Height"]').remove();
		$('button[ data-original-title="Font Family"]').remove();
		$('button[ data-original-title="Paragraph"]').remove();

	});
	$(function() {

		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});

	$("#ldapActive").click(function() {
		$("#ldapActiveDiv").removeClass("hidden");
	});
	$("#ldapInactive").click(function() {
		$("#ldapActiveDiv").addClass("hidden");
	});
	$("#btnTestLdap")
			.click(
					function() {
						$('body').addClass("white-bg");
						$("#barInMenu").removeClass("hidden");
						$("#wrapper").addClass("hidden");
						$
								.post(
										"testldapdetails",
										{
											ldapUrl : $("#ldapUrl").val(),
											ldapRootDN : $("#ldapRootDN").val(),
											ldapSearchBase : $(
													"#ldapSearchBase").val(),
											userSearchFilter : $(
													"#userSearchFilter").val(),
											ldapUserDN : $("#ldapUserDN").val(),
											ldapUserPass : $("#ldapUserPass")
													.val(),
											ldapUserMail : $("#ldapUserMail").val()
										},
										function(data, status) {
											if (data==201) {
												$('body').removeClass("white-bg");
												$("#barInMenu").addClass("hidden");
												$("#wrapper").removeClass("hidden");
												swal(
														"Success",
														"LDAP settings tested successfully",
														"success");
											} else if(data==204){
												$('body').removeClass("white-bg");
												$("#barInMenu").addClass("hidden");
												$("#wrapper").removeClass("hidden");
												swal(
														"Failure",
														"LDAP user mail not found, Please verify user mail",
														"warning");
											}else {
												$('body').removeClass("white-bg");
												$("#barInMenu").addClass("hidden");
												$("#wrapper").removeClass("hidden");
												swal(
														"Failure",
														"LDAP settings are not valid, Please verify the details",
														"warning");
											}

										});

					});
	$("#btnSubmit")
			.click(
					function() {
						$('body').addClass("white-bg");
						$("#barInMenu").removeClass("hidden");
						$("#wrapper").addClass("hidden");
						var accesscontrolchecked = $(
								"input[name='accessControlChecked']:checked")
								.val();

						if (accesscontrolchecked == 2) {
							$('#accessControlForm').attr('action',
									"insertldapdetails").submit();

						} else if (accesscontrolchecked == 1) {
							
							var ldapDetailsSize=${Model.ldapDetails.size()};
							var customerLDAPStatus=${Model.customerLDAPStatus};
							
							if(ldapDetailsSize==1 && customerLDAPStatus==1){
								swal({
								    title: "Are you sure?",
								    text: "Submitting ldap details will require to update LDAP users mapping, Are you sure to submit details?",
								    type: "warning",
								    showCancelButton: true,
								    confirmButtonColor: "#DD6B55",
								    confirmButtonText: "Yes, Submit",
								    cancelButtonText: "No, Cancel Please!",
								    closeOnConfirm: false,
								    closeOnCancel: false },
								function (isConfirm) {
								    if (isConfirm) {
								    	submitLdapDetails();
								    } else{
								    	 window.location.href="accesscontrol";
								 	}
								    });
							}else{
								submitLdapDetails();
							}

						}

					});
	$("#btnCancel").click(function() {
		window.location.href = "admindashboard";
	});

	function testLdapSettings() {
		$.post("testldapdetails", {
			ldapUrl : $("#ldapUrl").val(),
			ldapRootDN : $("#ldapRootDN").val(),
			ldapSearchBase : $("#ldapSearchBase").val(),
			userSearchFilter : $("#userSearchFilter").val(),
			ldapUserDN : $("#ldapUserDN").val(),
			ldapUserPass : $("#ldapUserPass").val(),
			ldapUserMail : $("#ldapUserMail").val()
		}, function(data, status) {
			//alert("Data: " + data + "\nStatus: " + status);
			return data;
		});
	}
	function submitLdapDetails(){
		$
		.post(
				"testldapdetails",
				{
					ldapUrl : $("#ldapUrl").val(),
					ldapRootDN : $("#ldapRootDN")
							.val(),
					ldapSearchBase : $(
							"#ldapSearchBase")
							.val(),
					userSearchFilter : $(
							"#userSearchFilter option:selected")
							.text(),
					ldapUserDN : $("#ldapUserDN")
							.val(),
					ldapUserPass : $(
							"#ldapUserPass").val(),
					ldapUserMail : $("#ldapUserMail").val()
				},
				function(data, status) {
					if (data==201) {

						$('#accessControlForm')
								.attr('action',
										"insertldapdetails")
								.submit();

					}else if(data==204){
						$('body').removeClass("white-bg");
						$("#barInMenu").addClass("hidden");
						$("#wrapper").removeClass("hidden");
						swal(
								"Failure",
								"LDAP user mail not found, Please verify user mail",
								"warning");
					}else {
						$('body').removeClass("white-bg");
						$("#barInMenu").addClass("hidden");
						$("#wrapper").removeClass("hidden");
						swal(
								"Failure",
								"LDAP settings are not valid, Please verify the details",
								"warning");
					}
				});
	}
</script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Xenon Database User Mapping </h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li class="active"><strong>Xenon Database User Mapping  </strong></li>
			</ol>
			<!-- end breadcrumb -->
		</div>

		<!-- end col-lg-10 -->
	</div>
	<!-- end wrapper -->
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content" >
						<form:form action="#"
							class="wizard-big wizard clearfix form-horizontal"
							modelAttribute="uploadForm" enctype="multipart/form-data"
							method="POST" id="createBugForm" style="overflow: visible !important;">
							<div class="clearfix" style="overflow: visible !important;">
								<fieldset class="body current">
									<label class="col-lg-4 pull-right">* fields are
										mandatory</label><br>
									<div class="row">
										<div class="col-lg-12">
											<div class="col-lg-6">
												<div class="form-group">
													<label class="control-label col-lg-4">Own Database User </label>
													<div class="col-lg-8">
														<select data-placeholder="Select" class="chosen-select"
															style="width: 100%;" tabindex="2" disabled
															name="userSearchFilter" id="selectedModuleID">
															<option value="">marie@jadeglobal.com</option>
														</select>
													</div>
												</div>

											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label class="control-label col-lg-4">LDAP User</label>
													<div class="col-lg-8">
														<select data-placeholder="Select" class="chosen-select"
															style="width: 100%;" tabindex="2"
															name="userSearchFilter" id="selectedModuleID">
															<option value="">john@example.com</option>
															<option value="">carol@example.com</option>
															<option value="">marie@example.com</option>
														</select>
													</div>
												</div>

											</div>
										</div>
									</div>
									<div class="hr-line-solid" style="margin-bottom: 1%;"></div>
									<div class="row">
										<div class="col-lg-12">
											<div class="col-lg-6">
												<div class="form-group">
													<label class="control-label col-lg-4">Own Database User</label>
													<div class="col-lg-8">
														<select data-placeholder="Select" class="chosen-select"
															style="width: 100%;" tabindex="2" disabled
															name="userSearchFilter" id="selectedModuleID">
															<option value="">john@jadeglobal.com</option>
														</select>
													</div>
												</div>

											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label class="control-label col-lg-4">LDAP User</label>
													<div class="col-lg-8">
														<select data-placeholder="Select" class="chosen-select"
															style="width: 100%;" tabindex="2"
															name="userSearchFilter" id="selectedModuleID">
															<option value="">john@example.com</option>
															<option value="">carol@example.com</option>
															<option value="">marie@example.com</option>
														</select>
													</div>
												</div>

											</div>
										</div>
									</div>
									<div class="hr-line-solid"  style="margin-bottom: 1%;"></div>
									<div class="row">
										<div class="col-lg-12">
											<div class="col-lg-6">
												<div class="form-group">
													<label class="control-label col-lg-4">Own Database User</label>
													<div class="col-lg-8">
														<select data-placeholder="Select" class="chosen-select"
															style="width: 100%;" tabindex="2" disabled
															name="userSearchFilter" id="selectedModuleID">
															<option value="">carol@jadeglobal.com</option>
														</select>
													</div>
												</div>

											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label class="control-label col-lg-4">LDAP User</label>
													<div class="col-lg-8">
														<select data-placeholder="Select" class="chosen-select"
															style="width: 100%;" tabindex="2"
															name="userSearchFilter" id="selectedModuleID">
															<option value="">john@example.com</option>
															<option value="">carol@example.com</option>
															<option value="">marie@example.com</option>
														</select>
													</div>
												</div>

											</div>
										</div>
									</div>
									<div class="hr-line-solid" style="margin-bottom: 1%;"></div>
								</fieldset>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-md-12">
										<button class="btn btn-white pull-left"
											style="margin-right: 15px" type="button" id="btnCancel">Cancel</button>
										<button type="button" id="btnSubmit"
											style="margin-right: 15px"
											class="btn btn-success pull-left ladda-button"
											data-style="slide-up">Submit</button>
									</div>
								</div>
							</div>
						</form:form>


						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<!-- end row -->
	</div>
</div>
<!-- end .md-skin -->
<input type="hidden" value="" id="userData">

<script type="text/javascript">
	$(window).load(
			function() {
				$('body').removeClass("white-bg");
				$("#barInMenu").addClass("hidden");
				$("#wrapper").removeClass("hidden");
				var config = {
					'.chosen-select' : {},
					'.chosen-select-deselect' : {
						allow_single_deselect : true
					},
					'.chosen-select-no-single' : {
						disable_search_threshold : 10
					},
					'.chosen-select-no-results' : {
						no_results_text : 'Oops, nothing found!'
					},
					'.chosen-select-width' : {
						width : "95%"
					}
				}
				for ( var selector in config) {
					$(selector).chosen(config[selector]);
					$("ul.chosen-results").css('max-height', '150px');
				}

				var accesscontrolchecked = $(
						"input[name='accessControlChecked']:checked").val();

				if (accesscontrolchecked == 1) {
					$("#ldapActiveDiv").addClass("hidden");
					$("#btnTestLdap").addClass("hidden");
				} else {
					$("#ldapActiveDiv").removeClass("hidden");
					$("#btnTestLdap").removeClass("hidden");
				}
			});

	$(document).ready(function() {

		$('.summernote').summernote();
		$('.note-editor').css('background', 'white');
		$('button[ data-event="codeview"]').remove();
		$('.note-editor').css('border', '1px solid #CBD5DD');

		$('div.note-insert').remove();
		$('div.note-table').remove();
		$('div.note-help').remove();
		$('div.note-style').remove();
		$('div.note-color').remove();
		$('button[ data-event="removeFormat"]').remove();
		$('button[ data-event="insertUnorderedList"]').remove();
		$('button[ data-event="fullscreen"]').remove();
		$('button[ data-original-title="Line Height"]').remove();
		$('button[ data-original-title="Font Family"]').remove();
		$('button[ data-original-title="Paragraph"]').remove();

	});
	$(function() {

		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});

	$("#ldapActive").click(function() {
		$("#ldapActiveDiv").removeClass("hidden");
		$("#btnTestLdap").removeClass("hidden");
	});
	$("#ldapInactive").click(function() {
		$("#ldapActiveDiv").addClass("hidden");
		$("#btnTestLdap").addClass("hidden");
	});
	$("#btnTestLdap").click(function() {
		swal({
			title : "Test LDAP",
			text : "This will test given LDAP Settings."
		});
	});
	$("#btnSubmit").click(function() {
		swal({
			title : "Submit",
			text : "This will submit given LDAP Settings."
		});
	});
	$("#btnCancel").click(function() {
		window.location.href = "admindashboard";
	});
</script>
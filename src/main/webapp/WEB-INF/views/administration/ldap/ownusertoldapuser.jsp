<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Xenon User Mapping</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li class="active"><strong>Xenon User Mapping </strong></li>
			</ol>
			<!-- end breadcrumb -->
		</div>

		<!-- end col-lg-10 -->
	</div>
	<!-- end wrapper -->
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content">
						<form:form action="#"
							class="wizard-big wizard clearfix form-horizontal"
							modelAttribute="uploadForm" enctype="multipart/form-data"
							method="POST" id="createBugForm"
							style="overflow: visible !important;">
							<div class="clearfix" style="overflow: visible !important;">
								<fieldset class="body current">
									<label class="col-lg-4 pull-right">* fields are
										mandatory</label><br>
									<c:forEach items="${Model.userDetailsList}"
										var="userDetailsList">
										<div class="row">
											<div class="col-lg-12">
												<div class="col-lg-6">

													<div class="form-group">
														<label class="control-label col-lg-4">Xenon User </label>
														<div class="col-lg-8">
															<select data-placeholder="Select"
																class="chosen-select xenonUser" style="width: 100%;"
																tabindex="2" disabled name="userSearchFilter"
																id="selectedModuleID">
																<option value="${userDetailsList.user_name}">${userDetailsList.user_name}</option>
															</select>
														</div>
													</div>

												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-lg-4">LDAP User</label>
														<div class="col-lg-8">
															<select data-placeholder="Select"
																class="chosen-select ldapUser" style="width: 100%;"
																tabindex="2" name="userSearchFilter"
																id="selectedModuleID">
																<option value="None">None</option>
																<c:forEach items="${Model.ldapUserDetails}"
																	var="ldapUserDetails">
																	<c:choose>
																	<c:when test="${userDetailsList.user_name== ldapUserDetails.user_name}">
																		<option value="${ldapUserDetails.user_name}" selected>${ldapUserDetails.user_name}</option>
																	</c:when>
																	<c:otherwise>
																	<option value="${ldapUserDetails.user_name}">${ldapUserDetails.user_name}</option>
																	</c:otherwise>
																	</c:choose>
																</c:forEach>
															</select> <label class="errorMessage hidden">User already
																mapped, Please select another user.</label>
														</div>
													</div>

												</div>
											</div>
										</div>
									</c:forEach>
									<div class="hr-line-solid" style="margin-bottom: 1%;"></div>

								</fieldset>
							</div>
							<c:if test="${Model.requireToMap>1 }">
								<div class="actions clearfix">
									<div class="row">
										<div class="col-md-12">
											<button class="btn btn-white pull-left"
												style="margin-right: 15px" type="button" id="btnCancel">Cancel</button>
											<button type="button" id="btnSubmit"
												style="margin-right: 15px"
												class="btn btn-success pull-left ladda-button"
												data-style="slide-up">Submit</button>
										</div>
									</div>
								</div>
							</c:if>
							<c:if test="${Model.requireToMap==0 }">
								<div class="form-group">
									<label class="control-label col-lg-4">All users are already mapped</label>
								</div>
							</c:if>
						</form:form>


						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<!-- end row -->
	</div>
</div>
<!-- end .md-skin -->
<input type="hidden" value="" id="userData">

<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
		var config = {
			'.chosen-select' : {},
			'.chosen-select-deselect' : {
				allow_single_deselect : true
			},
			'.chosen-select-no-single' : {
				disable_search_threshold : 10
			},
			'.chosen-select-no-results' : {
				no_results_text : 'Oops, nothing found!'
			},
			'.chosen-select-width' : {
				width : "95%"
			}
		}
		for ( var selector in config) {
			$(selector).chosen(config[selector]);
			$("ul.chosen-results").css('max-height', '150px');
		}

	});
	var xenonUserList = [];
	$(document).ready(function() {

		$('.summernote').summernote();
		$('.note-editor').css('background', 'white');
		$('button[ data-event="codeview"]').remove();
		$('.note-editor').css('border', '1px solid #CBD5DD');

		$('div.note-insert').remove();
		$('div.note-table').remove();
		$('div.note-help').remove();
		$('div.note-style').remove();
		$('div.note-color').remove();
		$('button[ data-event="removeFormat"]').remove();
		$('button[ data-event="insertUnorderedList"]').remove();
		$('button[ data-event="fullscreen"]').remove();
		$('button[ data-original-title="Line Height"]').remove();
		$('button[ data-original-title="Font Family"]').remove();
		$('button[ data-original-title="Paragraph"]').remove();

		$(".xenonUser").each(function() {
			xenonUserList.push($(this).val());
		});

	});
	$(function() {

		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});

	$("#btnSubmit").click(
			function() {

				var ldapUserList = [];
				var status = false;
				var noneUserFlag = false;
				$(".ldapUser").each(
						function() {
								if($(this).val().localeCompare("None")==0){
									noneUserFlag=true;
								}
								if ($.inArray($(this).val(), ldapUserList) >= 0 &&  $(this).val().localeCompare("None")!=0) {
									status = false;
									$(this).parent('div').find('.errorMessage')
											.removeClass('hidden');
									$(this).parent('div').find('.chosen-container')
											.addClass('errorDisplay');
									$(this).parent('div').find(
											'.chosen-container > a').css(
											"background", "rgb(251, 227, 228)");
								} else {
									status = true;
									$(this).parent('div').find('.errorMessage')
											.addClass('hidden');
									$(this).parent('div').find('.chosen-container')
											.removeClass('errorDisplay');
									$(this).parent('div').find(
											'.chosen-container > a').css(
											"background", "#ffffff");
									ldapUserList.push($(this).val());
								}
							

						});
			
				 if (status) {
					 
					 if(noneUserFlag){
						 swal({
						        title: "Are you sure?",
						        text: "Unmapped users will be deactivated, Do you want to submit it?",
						        type: "warning",
						        showCancelButton: true,
						        confirmButtonColor: "#DD6B55",
						        confirmButtonText: "Yes",
						        closeOnConfirm: false
						    }, function () {
						    	$.post("mapuserslist", {
									ldapUserList : ldapUserList,
									xenonUserList : xenonUserList
								}, function(data, status) {
									if (data) {
										swal({
											title: "",
									        text: "Users mapped successfully",
									        type: "success",
									        showCancelButton: false,
									        confirmButtonColor: "#1c84c6",
									        confirmButtonText: "Ok",
									        closeOnConfirm: false
									    }, function () {
									    	window.location.reload();
									    });
									} else {
										swal("Failure", "Users mapping unsuccessful ",
												"warning");
									}

								});
						    });
					 }else{
						 $.post("mapuserslist", {
								ldapUserList : ldapUserList,
								xenonUserList : xenonUserList
							}, function(data, status) {
								if (data) {
									swal("Success", "Users mapped successfully",
											"success");
								} else {
									swal("Failure", "Users mapping unsuccessful ",
											"warning");
								}

							});
					 }
					
				}

			});
	$("#btnCancel").click(function() {
		window.location.href = "admindashboard";
	});
</script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="md-skin">
	<div class="row wrapper border-bottom page-heading">
		<div class="col-lg-12">
			<h2>Jenkins Job</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
			<li><a href="viewjenkinsurls">Jenkins</a></li>
			<li class="active"><strong>Add Job</strong></li>
			</ol>
		</div>

	</div>

	<div class="wrapper wrapper-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-title"></div>
					<div class="ibox-content">
						<form action="insertjenkinsjob" method="POST" id="createJobForm"
							class="wizard-big wizard clearfix form-horizontal">
							<div class="content clearfix" >
								<fieldset class="body current" >
									<div class="row">
										<label class="col-lg-4 pull-right text-right">* fields
											are mandatory</label>
									</div>
									<div class="row">
										<div class="col-sm-10">
											<div class="form-group">
												<label class="control-label col-sm-2">Instance:</label>
												<div class="col-sm-4">
													<select data-placeholder="Choose a Instance"
														class="chosen-select" onchange="getInstanceValue(this);" id="instanceName" name="instanceid">
														
															<c:forEach var="instance" items="${Model.instanceData}">
																	<option value="${instance.jenkin_id}" URL="${instance.jenkin_url}" username="${instance.jenkin_username}" token="${instance.jenkin_token}">${instance.jenkin_name}</option>
														</c:forEach>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-2">Jenkins Job
													Name:</label>
												<div class="col-sm-4">
													<select data-placeholder="Choose a Instance" id="jobName" name="jenkinsJobName"
														class="chosen-select form-control">
														<option value="0">None</option>
														<c:forEach var="job" items="${Model.jobList}">
																	<option value="${job.jobName}" class="instance_${job.instanceId} jenkinsjob hidden">${job.jobName}</option>
														</c:forEach>
													</select>
													<label class="hidden text-danger" id="labelId">This fiels is required</label>
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-sm-2">Job Name*:</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="jobname">
												</div>
											</div>
											<div class="form-group">
											<label class="col-sm-2 control-label"> Description:</label>
											<div class="col-sm-8">
												
												<textarea  rows="5" class="form-control characters summernote"
													 name="jobDesciption" > </textarea>
											</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-2">Build Trigger Token*:</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="buildtoken">
												</div>
											</div>
											<div class="form-group">
											<label class="col-sm-2 control-label">Status:</label>
											<div class="col-sm-10">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="active" value="1"
														name="jobStatus" checked="" > 
													<label for="active">Active</label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inactive" value="2"
														name="jobStatus" > 
													<label for="active">Inactive</label>
												</div>
											</div>
										</div>
										</div>
									</div>
									<div class="hr-line-solid" ></div>
								</fieldset>
							</div>
							<div class="actions clearfix">
								<div class="row">

									<div class="col-sm-4">
									<button class="btn btn-white pull-left"
										style="margin-right: 15px;" type="button">Cancel</button>
									<button
										class="pull-left btn btn-success" type="button" id="saveJobBtn">Save</button>
									</div>

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(window).load(function() {
		$("#wrapper").removeClass("hidden");
		$("#barInMenu").addClass("hidden");
		$(".chosen-select").chosen().trigger('chosen:updated');
		var instanceNameVal=$("#instanceName option:first").val();
		$('.instance_'+instanceNameVal).removeClass("hidden");
		$(".instance_"+instanceNameVal).each(function() {
			$('.instance_'+instanceNameVal).removeClass("hidden");
			$('#jobName').trigger("chosen:updated");

		});
	});
	
	function getInstanceValue(){
		var instanceValue=$("#instanceName").val();
		$("#instanceName").chosen().trigger('chosen:updated');
		
	
		$('.jenkinsjob').each(function(){
			$('.jenkinsjob').addClass("hidden");
			$('#jobName').trigger("chosen:updated");
		})
		$("#jobName .instance_"+instanceValue).each(function(){
			$('.instance_'+instanceValue).removeClass("hidden");
			$('#jobName').trigger("chosen:updated");
		})
	}
	
	
	$("#saveJobBtn").click(function(){
		$("#createJobForm").validate({
			rules:{
				jobname : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				buildtoken:{
					required : true,
					minlength : 1,
					maxlength : 60
				}
			}	
		});
		var status=false;
		
		var jobName=$("#jobName").val();
		var instanceName=$("#instanceName").val();
		if((jobName==0&&instanceName))
		{
			$("#labelId").removeClass("hidden");
		}
		else
			{
			
			if($("#createJobForm").valid()){
				$("#createJobForm").submit();
			}
				
				
		}
			
			
			 
			
	})
</script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>	
<style>
.sidebar-container ul.nav-tabs.navs-3 li {
    width: 50%;
}
.text-date{
	color: cornflowerblue;
}
.dropdown:hover .dropdown-menu {
	display: block;
}
</style>
<div id="page-wrapper" class="gray-bg">
	<div class="row border-bottom">
		<nav class="navbar navbar-static-top white-bg" role="navigation"
			style="margin-bottom: 0"> <!-- navbar-fixed-top -->
			<div class="navbar-header">
				<span>
					<%-- <a href="#" onclick="gotoLink('xedashboard')" 
					class="navbar-minimalize minimalize-styl-2 headerLogo smallMargin">
						<img alt="image" class="header-logo-image"
						src="<%=request.getContextPath()%>/resources/img/Jade-Logo.png" height="40px" id="riverbed_logo"/>
					</a> --%>
							
					<a class="navbar-minimalize minimalize-styl-2 btn btn-primary pull-right toggleButtonMargin" href="#"
					style= "background-color : #3f81bc; border-color: #3f81bc;">
						<i class="fa fa-bars"></i>
					</a>
				</span>
			</div>

			<ul class="nav navbar-top-links"> 
			<li class="dropdown">
	       				<c:if test="${sessionScope.redwoodStatus==1}">
									<li><a href="#" onclick="gotoLink('bpft')">BPFT</a></li>
								</c:if>
	       		</li>  
						<!-- navbar-right -->
				<!-- <li class="hidden-xs hidden-sm"><a href="#" onclick="gotoLink('admindashboard')">Dashboard</a></li> -->
				<li class="dropdown mobile-hide" id="${Model.projectList[0].projectId}">
					
					<!-- This is hidden on the medium and large screens -->
					<a aria-expanded="false" role="button" href="#"
						class="dropdown-toggle hidden-md hidden-lg firstAppName" data-toggle="dropdown" id="">
<%-- 						${fn:substring(Model.projectList[0].projectName,0,5)} --%>
						Applications
						<span class="caret"></span>
					</a>
					
					<!-- This is hidden on the extra small and small screens -->
					<a aria-expanded="false" role="button" href="#"
						class="dropdown-toggle hidden-xs hidden-sm firstAppName" data-toggle="dropdown" id="">
						Applications
<%-- 						${Model.projectList[0].projectName} --%>
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu animated fadeInRight m-t-xs">
						<c:forEach var="projectList" items="${sessionScope.projectListSession}">
							<li id="${projectList.project_id}"><a href="#"
								onclick="setCurrentProject('${projectList.project_id}','${projectList.project_name}')">${projectList.project_name}</a></li>
						</c:forEach>
<!-- 						<li><a href="#" onclick="setAllProjectTM()">All Applications</a></li> -->
					</ul>
				</li>
				<li class="mobile-hide dropdown">
					<a aria-expanded="false" role="button" href="#" onclick="" class="dropdown-toggle firstAppName" data-toggle="dropdown">Test Manager <span class="caret"></span></a>
					<ul class="dropdown-menu animated fadeInRight">
						
							<li><a href="#" onclick="gotoLink('testcase')">Application Test
								Manager</a></li>
                        <c:if test="${sessionScope.apiStatus==1}">
                                	<li><a href="#" onclick="gotoLink('apitest')">API Test Manager</a></li>
                               </c:if>
                        <li><a href="#" onclick="gotoLinkInMenu('documentlibrary')">
								<span class="">Document Library</span>
							</a>
						</li> 
					</ul>
				</li>
       			<li class="dropdown tst-exc">
       				<a aria-expanded="false" role="button" href="#"
						class="dropdown-toggle firstAppName" data-toggle="dropdown" id="">Test Execution <span class="caret"></span></a>
       				<ul class="dropdown-menu animated fadeInRight">
					    <li><a href="#" onclick="gotoLink('executebuild')">Manual</a></li>
					    <li><a href="#" onclick="gotoLink('autoexecute')">Automation</a></li>
					</ul>
       			</li>
       			<li class="mobile-hide"><a href="viewallbugs">Bug Tracker</a></li>
       			<c:if test="${sessionScope.roleId == 2}">
       				<li class="active mobile-hide"><a href="#" onclick="gotoLink('admindashboard')">Administration</a></li>
       			</c:if>
       			<li class="mobile-hide"><a href="#" onclick="gotoLink('bmdashboard')">Dashboard & Reports</a></li>
					
				<!-- <li style="float: right;" class="navbar-last-child">
					<a href="#" onclick="gotoLink('logout')"
					class="fa fa-sign-out" data-original-title='Log out' data-container='body' data-toggle='tooltip' data-placement='bottom'> 
					</a>
				</li> -->
				
<!-- 				<li style="float: right;" class="hidden-xs"> -->
<!-- 					<a href="#" onclick="gotoLink('help')" -->
<!-- 					class="fa fa-support" data-original-title='Support' data-container='body' data-toggle='tooltip' data-placement='bottom'> -->
<!-- 					</a> -->
<!-- 				</li> -->
				
				<li id="rightToggleLi">
                   <a class="right-sidebar-toggle count-info"> <i class="fa fa-tasks"></i>
                       <span class="label label-primary" id="notificationReadCount" style="background-color: #3f81bc;">${Model.unreadCount}</span>
                   </a>
               	</li>  
               	
               	<!-- Trash functionality -->
                           <li style="margin-top: 15px;">
                       <div class="dropdown profile-element">
                            
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
                                    <img alt="image" class="rounded-circle img-circle" src="./resources/img/delete.png" >
                                <b class="caret"></b>
                                </a>
                                <ul class="abc dropdown-menu animated fadeInRight m-t-xs" x-placement="bottom-start" style="position: absolute; top: 34px; left: -90px; will-change: top, left;">
                                    <li><a class="dropdown-item" href="viewtmtrash">Test Manager</a></li>
                                
    <!--                                 <li class="divider"></li> -->
                                    <li><a class="dropdown-item" href="viewbuildtrash">Test Execution</a></li>
    <!--                                 <li class="divider"></li> -->
                                    
                                </ul>
                        </div>
                        </li>
               	<li>
                   <div class="dropdown profile-element">
                           
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
                                <img alt="image" class="rounded-circle img-circle" src="./resources/img/user.png" style="width: 20px;height: 19px;">
                            <b class="caret"></b>
                            </a>
                            <ul class="abc dropdown-menu animated fadeInRight m-t-xs" x-placement="bottom-start" style="position: absolute; top: 42px; left: -90px; will-change: top, left;">
                                <li><a class="dropdown-item" href="#" onclick="gotoLinkInMenu('profile')">${sessionScope.userFullName }<br><span class="user-role">${sessionScope.role }</span></a></li>
                               
                                <li class="divider"></li>
                                <li><a class="dropdown-item" href="#" onclick="gotoLink('logout')">Logout</a></li>
                            </ul>
                        </div>
                   </li>   
               
               	         	
			</ul>

		</nav>
	</div>

<div id="right-sidebar">
            <div class="sidebar-container">

                <ul class="nav nav-tabs navs-3">

                    <li class="active"><a data-toggle="tab" href="#tab-1">
                        Notifications
                    </a></li>
                    <li><a data-toggle="tab" href="#tab-2">
                        Build Progress
                    </a></li>
                  <!--   <li class=""><a data-toggle="tab" href="#tab-3">
                        <i class="fa fa-gear"></i>
                    </a></li> -->
                </ul>

                <div class="tab-content">


                    <div id="tab-1" class="tab-pane active">

                        <div class="sidebar-title">
                            <h3> <i class="fa fa-comments-o"></i> Latest Notifications</h3>
                           <small id="nfMsgText"><i class="fa fa-tim"></i>You have ${Model.notifications.size()} new message.</small>
                        </div>

                        <div>
						<c:forEach var="notifications" items="${Model.notifications}"
							varStatus="loop" begin="0" end="9">
							<c:if test="${((notifications.user_id == notifications.requiredId  ||  notifications.nfUserId == notifications.requiredId )  && notifications.created_by != notifications.requiredId)}">
							<c:choose>
							<c:when test="${(notifications.nf_read_status==1  || notifications.read_status==1 ) }">
								<c:choose>
								<c:when test="${(notifications.nf_read_count_flag==1  || notifications.read_count_flag==1 ) }">
								<div class="sidebar-message "  style="background: white;border-bottom: 1px solid gainsboro;">
								</c:when>
								<c:otherwise >
								<div class="sidebar-message notificationDiv"  style="background: white;border-bottom: 1px solid gainsboro;">
								</c:otherwise>
								</c:choose>
 						    </c:when>
 						    <c:otherwise> 						    
 						    	<c:choose>
								<c:when test="${(notifications.nf_read_count_flag==1  || notifications.read_count_flag==1 ) }">
								<div class="sidebar-message"  style="background: whitesmoke;border-bottom: 1px solid gainsboro;">
								</c:when>
								<c:otherwise>
								<div class="sidebar-message notificationDiv"  style="background: whitesmoke;border-bottom: 1px solid gainsboro;">
								</c:otherwise>
								</c:choose>
 						    </c:otherwise>
 						    </c:choose>
                                <a onclick="markAsRead('${notifications.notification_url}',${notifications.notification_id})"> 
                                    <div class="pull-left text-center">
                                        <img alt="image" class="img-circle message-avatar" height="40px" width="40px" src="data:image/jpg;base64,${notifications.user_image}">

                                        <div class="m-t-xs">
                                             ${notifications.notification_icon}
                                            
                                        </div>
                                    </div>
                                    <div class="media-body">
										<strong style="color: #676a6c;text-transform: capitalize;">${notifications.first_name}&nbsp;${notifications.last_name}</strong>
										<%--  ${notifications.activity} --%>
										<br>
                                        ${notifications.notification_description}.
                                        <br>
                                        <c:set var="date"
										value="${fn:split(notifications.notification_date,' ')}" />


									<c:choose>

										<c:when test="${notifications.TimeDiff < 60 }">
										 <small class="text-muted text-date">${notifications.TimeDiff}
												min ago</small>
										
											
										</c:when>

										<c:when
											test="${notifications.TimeDiff >= 60 && notifications.TimeDiff < 1440 }">
											<fmt:parseNumber var="hour" integerOnly="true" type="number"
												value="${notifications.TimeDiff/60}" />
											<small class="text-muted text-date">${hour} hr ago </small>
										</c:when>


										<c:when
											test="${notifications.TimeDiff >= 1440 && notifications.TimeDiff < 2880 }">
											<small class="text-muted text-date"> Yesterday </small>
										</c:when>
										<c:otherwise>
											<small class="text-muted text-date">${notifications.notification_date}
											</small>
										</c:otherwise>
									</c:choose>
                                        
                                    </div>
                                </a>
                            </div>
 						 </c:if>
 
 						</c:forEach>
                        </div>

                    </div>

                    <div id="tab-2" class="tab-pane">

                        <div class="sidebar-title">
                            <h3> <i class="fa fa-cube"></i> Latest Builds</h3>
                        </div>

                        <div>
						 <c:forEach var="buildList" items="${Model.topBuilds}" begin="0" end="9">
								<div class="sidebar-message"  >
                                <a> 
                                
                                    <div class="pull-left text-center">
                                        <img alt="image" class="img-circle message-avatar" height="40px" width="40px" src="data:image/jpg;base64,${buildList.user_image}">

                                        <div class="m-t-xs">
                                            
                                        </div>
                                    </div>
                                    <div class="media-body">
										<strong style="color: #676a6c;text-transform: capitalize;">${buildList.first_name}&nbsp;${buildList.last_name}</strong>
										<%--  ${notifications.activity} --%>
										<br>
										${buildList.build_name}
										<br>
                                    <c:choose>
                                				<c:when test="${buildList.build_state ==1 }">
																<span
																		class="label label-success">
																			${buildList.desc}</span>
															</c:when>
															<c:when test="${buildList.build_state ==2 }">
																<span
																		class="label label-info "> ${buildList.desc}</span>
															</c:when>
															<c:when test="${buildList.build_state ==3 }">
																<span
																		class="label label-warning ">
																			${buildList.desc}</span>
															</c:when>
															<c:when test="${buildList.build_state ==4 }">
																<span
																		class="label label-danger ">
																			${buildList.desc}</span>
															</c:when>
															<c:when test="${buildList.build_state ==5 }">
																<span
																		class="label label-primary ">
																			${buildList.desc}</span>
															</c:when>
															<c:when test="${buildList.build_state ==6 }">
																<span
																		class="label label-default ">
																			${buildList.desc}</span>
															</c:when>
														</c:choose> 
														
									
									<br>
									Completion Percent : ${buildList.percentCompleted}%
                                         <div class="progress progress-mini" style="background-color: #DCDCDC;">
                                        <div style="width: ${buildList.percentCompleted}%;" class="progress-bar"></div>
                                    </div>
                                    
									<c:choose>

										<c:when test="${buildList.TimeDiff < 60 }">
										 <small class="text-muted text-date ">${buildList.TimeDiff}
												min ago</small>
										
											
										</c:when>

										<c:when
											test="${buildList.TimeDiff >= 60 && buildList.TimeDiff < 1440 }">
											<fmt:parseNumber var="hour" integerOnly="true" type="number"
												value="${buildList.TimeDiff/60}" />
											<small class="text-muted text-date ">${hour} hr ago </small>
										</c:when>


										<c:when
											test="${buildList.TimeDiff >= 1440 && buildList.TimeDiff < 2880 }">
											<small class="text-muted text-date "> Yesterday </small>
										</c:when>
										<c:otherwise>
											<small class="text-muted text-date ">${buildList.build_createdate}
											</small>
										</c:otherwise>
									</c:choose>
                                    </div>
                                </a>
                            </div>
 						 
 
 						</c:forEach>

                    </div>

                    </div>

                </div>

            </div>
</div>
	<div class="theme-config">
		<div class="theme-config-box">
			<div class="spin-icon" style="background: #1c84c6;">
				<i class="fa fa-cogs fa-spin"></i>
			</div>
			<div class="skin-setttings">
				<div class="title">Switch Dashboard</div>

				<div id="btdashboardShortcut2" class="setings-item blue-skin"
					style="padding: 15px 20px; border-bottom: 1px solid #e7eaec; background: #ed5565; display: none;">
					<span class="skin-name "> <a href="#"
						onclick="gotoLink('btdashboard')" class="s-skin-1"
						style="color: aliceblue;"> Bug Tracker </a>
					</span>
				</div>
				<div id="tmdashboardShortcut2" class="setings-item yellow-skin"
					style="padding: 15px 20px; border-bottom: 1px solid #e7eaec; display: none;">
					<span class="skin-name "> <a href="#"
						onclick="gotoLink('testcase')" class="s-skin-3"
						style="color: aliceblue;"> Test Manager </a>
					</span>
				</div>
				<div id="bmdashboardShortcut2" class="setings-item ultra-skin"
					style="padding: 15px 20px; border-bottom: 1px solid #e7eaec;">
					<span class="skin-name "> <a href="#"
						onclick="gotoLink('executebuild')" class="s-skin-2"
						style="color: aliceblue;"> Test Execution </a>
					</span>
				</div>
				<div id="coredashboardShortcut2" class="setings-item ultra-skin"
					style="padding: 15px 20px; border-bottom: 1px solid #e7eaec; background: #23c6c8; display: none;">
					<span class="skin-name "> <a href="#"
						onclick="gotoLink('coredashboard')" class="s-skin-2"
						style="color: aliceblue;"> Xenon Core </a>
					</span>
				</div>
				<div id="dashandrepoShortcut2" class="setings-item ultra-skin"
					style="padding: 15px 20px; border-bottom: 1px solid #e7eaec; background: #6c499b;">
					<span class="skin-name "> <a href="#"
						onclick="gotoLink('dashandrepo')" class="s-skin-2"
						style="color: aliceblue;"> Dashboard and Reports </a>
					</span>
				</div>
				<div id="admindashboardShortcut2" class="setings-item ultra-skin"
					style="padding: 15px 20px; border-bottom: 1px solid #e7eaec;background: #26519e;">
					<span class="skin-name "> <a href="#"
						onclick="gotoLink('admindashboard')" class="s-skin-4"
						style="color: aliceblue;"> Administration  </a>
					</span>
				</div>
				
<!-- 				<div id="supportDashboardShortcut" class="setings-item ultra-skin" -->
<!-- 					style="padding: 15px 20px; border-bottom: 1px solid #e7eaec; background: #CD5C5C;"> -->
<!-- 					<span class="skin-name "> <a href="#" -->
<!-- 						onclick="gotoLink('help')" class="s-skin-3" -->
<!-- 						style="color: aliceblue;"> Support </a> -->
<!-- 					</span> -->
<!-- 				</div> -->
		</div>
		</div>
	</div>

	<script>
	$("#notificationReadCount").text($( ".notificationDiv" ).length);
	$("#nfMsgText").text('You have '+$( ".notificationDiv" ).length+' new message.');
		$('.spin-icon').click(function() {
			$(".theme-config-box").toggleClass("show");
		});

		if (localStorage.getItem("tmStatus") == 1) {
			$("#tmdashboardShortcut2").css("display", "");
			$("#tmdashboardShortcut1").css("display", "");
		}

		if (localStorage.getItem("btStatus") == 1) {
			$("#btdashboardShortcut2").css("display", "");
			$("#btdashboardShortcut1").css("display", "");
		}

		if (localStorage.getItem("coreStatus") == 1) {
			$("#coredashboardShortcut2").css("display", "");
			$("#coredashboardShortcut1").css("display", "");
		}
		
		$(function() {
			$("#custLogo").attr(
					"src",
					"data:image/jpg;base64,"
							+ localStorage.getItem("customerLogo"));
			$('[data-toggle="tooltip"]').tooltip();  
			
			$('.scroll_content').slimscroll({
		        height: '200px'
		    })
		});

		$("#rightToggleLi").click(function(){
			var posting = $.post('updateNotificationReadCountFlag');
			
			posting.done(function(){
				$("#notificationReadCount").text(0);
			});
		});

		function gotoLink(link)//link
		{
			if( link == 'executebuild')
				sessionStorage.setItem("executionType",'executebuild');
			else if( link == 'autoexecute')
				sessionStorage.setItem("executionType",'autoexecute');
			
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
			window.location.href = link;
		}
		
		
		$("#socialFeedA").click(function(){
			$("#dropDown").toggle();
			$(".buildProgressDiv").hide();
			$(".activityDiv").show();
	});
	$("#activityBtn").click(function(){
		$("#dropDown").css("display","block");
		$(".buildProgressDiv").hide();
		$(".activityDiv").show();
	});


	$("#buildProgressBtn").click(function(){
		$(".activityDiv").hide();
		$(".buildProgressDiv").show();
		$("#dropDown").css("display","block");
	});

	function markAsRead(openLink,notificationId)
			{
				$('body').addClass("white-bg");
				$("#barInMenu").removeClass("hidden");
				$("#wrapper").addClass("hidden");	
						
				var posting = $.post('marknotificationasread',{
					notificationId : notificationId
				});
				
				posting.done(function(){
					$('body').removeClass("white-bg");
					$("#barInMenu").addClass("hidden");
					$("#wrapper").removeClass("hidden");
					
					window.open(openLink,'_self'); 				
				});
				
			}
	
	$('#topNav').click(function(){
		$('#logoDiv').toggleClass('hidden');
	});
	
	function setCurrentProject(id, name) {
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		  
		var posting = $.post('setCurrentProjectTM', {
			projectId : id,
			projectName : name
		});

		posting.done(function(data) {
			window.location.href="admindashboard";
		});
	}
	</script>
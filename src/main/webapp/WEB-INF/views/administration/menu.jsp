<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="bar" id="barInMenu">
	<p>loading</p>
</div>

<div id="wrapper" class="hidden">
	<nav class="navbar-default navbar-static-side " role="navigation" > <!-- style="min-height: 100%; background-color: transparent;" -->
		<div class="sidebar-collapse">
			<ul class="nav metismenu" id="side-menu">
				<%-- <li class="nav-header">
					<div class="dropdown profile-element">
						<span> <img alt="image" class="img-circle"
							style="width: 48px; height: 48px"
							src="data:image/jpg;base64,${Model.UserProfilePhoto }" />
						</span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
							class="clear"> <span class="block m-t-xs"> <strong
									class="font-bold">${Model.userName }</strong>
							</span>
						</span>
						</a> <span class="text-muted text-xs block">${Model.role}<b
							class=""></b></span>
					</div>
					<div class="logo-element" style="padding: 14px !important">
						<a href="#" onclick="gotoLinkInMenu('xedashboard')">
						<img alt="image" class=""
							src="<%=request.getContextPath()%>/resources/img/xenon.png" />
						</a>
					</div>
				</li> --%>
				<li class="nav-header">
                    <div class="dropdown profile-element text-center">
                        <a href="#" onclick="gotoLink('xedashboard')" >
                        <img src="<%=request.getContextPath()%>/resources/img/Jade-Logo.png" class="xenon-logo">
                        </a>
                    <!--    <img class="img-circle" style="width:48px; height:48px" src="resources/img/user.jpg">
                         <img alt="image" class="img-circle"
                            style="width: 48px; height: 48px"
                            src="data:image/jpg;base64,${Model.UserProfilePhoto }" />
                        </span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
                            class="clear"> <span class="block m-t-xs"> <strong
                                    class="font-bold">${Model.userName }</strong>
                            </span>
                        </span>
                        </a> <span class="text-muted text-xs block">${Model.role}<b
                            class=""></b></span>  -->
                    </div>
                    <div class="logo-element" style="padding: 14px !important">
                        <a href="#" onclick="gotoLinkInMenu('xedashboard')">
                        <img alt="image" class=""
                            src="<%=request.getContextPath()%>/resources/img/xenon.png" />
                        </a>
                    </div>
                </li>
				
				<li><a href="#" onclick="gotoLinkInMenu('admindashboard')"><i class="fa fa-tachometer"></i> <span
					class="nav-label">DashBoard</span></a></li>
				<c:if test="${Model.userAccess == 1}">
					<li class="mainLi"><a href=""><i class="fa fa-users"></i>
							<span class="nav-label">Users</span> <span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">
							<c:if test="${Model.createUser == 1}">
								<li class="LdapStatus"><a href="#" onclick="gotoLinkInMenu('createuser')">Add
										User</a></li>
							</c:if>
							<c:if test="${Model.viewUser == 1}">
								<li><a href="#" onclick="gotoLinkInMenu('viewuser')" >Users</a></li>
							</c:if>
							<%-- <c:if test="${Model.viewProjects == 1}">
								<li><a href="#" onclick="gotoLink('users')">Application Management</a></li>
							</c:if> --%>
						</ul></li>

				</c:if>
				<c:if test="${Model.projectAccess == 1}">
					<li class="mainLi"><a href="#"><i class="fa fa-puzzle-piece"></i>
							<span class="nav-label">Applications</span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">
							<c:if test="${Model.createProject == 1}">
								<li><a href="#" onclick="gotoLinkInMenu('createproject')">Add
										Application</a></li>
							</c:if>
							<c:if test="${Model.viewProjects == 1}">
								<li><a href="#" onclick="gotoLinkInMenu('viewproject')">Applications</a></li>
							</c:if>
						</ul></li>
				</c:if>

				<li class="mainLi"><a href="#"><i class="fa fa-cog"></i>
						<span class="nav-label">Settings</span><span class="fa arrow"></span></a>
					<ul class="nav nav-second-level collapse">
						<c:if test="${Model.mail == 1}">
							<li><a href="#" onclick="gotoLinkInMenu('mail')">Email</a></li>
							<li><a href="#" onclick="gotoLinkInMenu('mailgroup')">Mail
									Group</a></li>
						</c:if>
						<c:if test="${Model.accountsettings == 1}">
							<li><a href="#" onclick="gotoLinkInMenu('accountsettings')">Account</a></li>
						</c:if>
						<c:if test="${Model.accountsettings == 1}">
							<li><a href="#" onclick="gotoLinkInMenu('thirdpartyissuetracker')">Third Party Issue Tracker</a></li>
						</c:if>
						<c:if test="${Model.accountsettings == 1}">
							<li><a href="#" onclick="gotoLinkInMenu('accesscontrol')">Access Control</a></li>
						</c:if>
						<c:if test="${Model.customerLDAPStatus == 1}">
							<li><a href="#" onclick="gotoLinkInMenu('owntoldap')">LDAP users</a></li>
						</c:if>
						<li><a href="#" onclick="gotoLinkInMenu('soapapihttpsettings')">API Settings</a></li> 
					</ul></li>
					
					<li class="mainLi"><a href="#"><i class="fa fa-plug"></i>
						<span class="nav-label">Plugins</span><span class="fa arrow"></span></a>
					<ul class="nav nav-second-level collapse">
							<li><a href="#" onclick="gotoLinkInMenu('viewjenkinsurls')">Jenkins</a></li>
							<li><a href="#" onclick="gotoLinkInMenu('viewgitprojects')">Git</a></li>
					</ul></li>

			</ul>

		</div>
	</nav>
	

<input type="hidden" value="<%=session.getAttribute("menuLiText")%>"
		id="hiddenLiText">
	<script type="text/javascript">
		$(function() {

			var liText = $('#hiddenLiText').val();
			$('.nav-label').each(function() {
				var label = $(this).text().trim();
				if (label == liText) {
					var parentLi = $(this).parents('.mainLi');
					parentLi.addClass('active');
					parentLi.find('.collapse').addClass('in');
				}
			});
		});

		function gotoLinkInMenu(link)//link
		{
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
			window.location.href = link;
		}
		var LDAPStatus=<%=session.getAttribute("customerLDAPStatus")%>;
		
		if(LDAPStatus==1){
			$('.LdapStatus').addClass("hidden");
		}
		
	</script>
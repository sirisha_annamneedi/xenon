<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="wrapper wrapper-content animated fadeInRight" id="wrapperDiv">
	<div class="row">
		<div class="col-lg-4">
			<div class="ibox float-e-margins dashlink"  onclick="gotoLinkInMenu('viewuser')">
				<div class="ibox-title">
					<h5 title="Total No of Users">Total Users</h5>
					<!-- <i class="fa fa-user pull-right"></i> -->
				</div>
				<div class="ibox-content">
					<h1 class="no-margins text-success">${Model.userSize}</h1>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="ibox float-e-margins dashlink" onclick="gotoLinkInMenu('viewproject')">
				<div class="ibox-title">
					<h5 title="Total No of Applications">Total Applications</h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins text-success">${Model.Projects[0].proCount}</h1>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="ibox float-e-margins dashlink" onclick="gotoLinkInMenu('vmdetails')">
				<div class="ibox-title">
					<h5 title="Total No of virtual Machines">Total Virtual Machines</h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins text-success"> ${Model.Counts[0].vm_count}</h1>
				</div>
			</div>
		</div>
	
<!-- 		<div class="col-lg-3"> -->
<!-- 			<div class="ibox float-e-margins dashlink"> -->
<!-- 				<div class="ibox-title"> -->
<!-- 					<h5 title="Total Cloud Space">Total Cloud Space</h5> -->
<!-- 				</div> -->
<!-- 				<div class="ibox-content"> -->
<%-- 					<h1 class="no-margins text-success">${Model.Counts[0].used_space}/${Model.Counts[0].max_cloud_space} MB</h1> --%>
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 		</div> -->
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="ibox float-e-margins" style="background-color: white;">
				<div class="ibox-title">
					<h5>Users</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table
							class="table table-striped table-bordered table-hover"
							id="usersListTable">
							<thead>
								<tr>
									<th class="hidden">User Id</th>
									<th>Name</th>
									<th>Status</th>
									<th>Email Id</th>
									<th>Role</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="data" items="${Model.Users}">
									<tr style="cursor: pointer;" class="gradeX">
										<td class="hidden">${data.user_id}</td>
										<td class="textAlignment" data-original-title="${data.user_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.user_name}</td>
										<td><c:if test="${data.user_status == 1}">
												<span class="label label-primary">Active</span>
											</c:if> <c:if test="${data.user_status == 2}">
												<span class="label label-danger">Inactive</span>
											</c:if></td>
										<td class="textAlignment" data-original-title="${data.email_id}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.email_id}</td>
										<td class="textAlignment" data-original-title="${data.user_role}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.user_role}</td>
									</tr>
								</c:forEach>
							</tbody>

						</table>
					</div>

				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="ibox float-e-margins">
			<div class="client-detail" style="max-height: 406px">
				<div class="full-height-scroll">
	
				<!-- <div class="ibox-title">
					<h5>Recent Activities</h5>
					<div class="ibox-tools">
						<span class="label label-warning-light pull-right"> Recent
							&nbsp;${fn:length(Model.topActivities)} &nbsp;Activities</span>
					</div>
				</div>-->
				<div class="ibox-content">
					<div class="feed-activity-list">
						<c:forEach var="topActivities" items="${Model.topActivities}"
							varStatus="loop">
							<div class="feed-element">
								<a  class="pull-left"> <img alt="image"
									class="img-circle"
									src="data:image/jpg;base64,${topActivities.user_image}">
								</a>
								<div class="media-body ">


									<c:set var="date"
										value="${fn:split(topActivities.activity_date,' ')}" />


									<c:choose>

										<c:when test="${topActivities.TimeDiff < 60 }">
											<strong class="pull-right">${topActivities.TimeDiff}
												min ago</strong>
										</c:when>

										<c:when
											test="${topActivities.TimeDiff >= 60 && topActivities.TimeDiff < 1440 }">
											<fmt:parseNumber var="hour" integerOnly="true" type="number"
												value="${topActivities.TimeDiff/60}" />
											<strong class="pull-right">${hour} hr ago </strong>
										</c:when>


										<c:when
											test="${topActivities.TimeDiff >= 1440 && topActivities.TimeDiff < 2880 }">
											<strong class="pull-right"> Yesterday </strong>
										</c:when>
										<c:otherwise>
											<strong class="pull-right">${topActivities.activity_date}
											</strong>
										</c:otherwise>
									</c:choose>

									<strong>${topActivities.first_name}&nbsp;${topActivities.last_name}
									</strong>${topActivities.activityState}.<br> <small
										class="text-muted">${topActivities.activity_date}</small>

									<div class="well">
									<div class="client-detail" style="max-height: 80px">
										<div class="full-height-scroll">
											${topActivities.activity_desc}
										</div>
									</div>
									</div>


								</div>
							</div>
						</c:forEach>

					</div>

				</div>
				</div>
				</div>
			</div>

		</div>
	</div>

</div>
<script type="text/javascript">
	$(window).load(function() {
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
		
		 var page= <%= request.getParameter("page") %>;
		 if(page == null){
			 page = 1;
		 }
		 
		 var userSize='${Model.userSize}';
		 
		 var pageSize='${Model.pageSize}';
		 
		 var lastRec=((page-1)*pageSize+parseInt(pageSize));
		 
		 if(lastRec>userSize)
		 	lastRec=userSize;
		 var showCount = ((page-1)*pageSize+1);
		 
		 if(showCount <= lastRec){
			 $("#usersListTable_info").html("Showing "+showCount+" to "+lastRec+" of "+userSize+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="prevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="nextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
		 }else{
			 $("#usersListTable_info").html("");
		 }
		 
		 
		 if(page == 1){
			 $("#prevBtn").attr('disabled','disabled');
		 }
		 
		 if(lastRec == userSize){
			 $("#nextBtn").attr('disabled','disabled');
		 }
		 
		    $("#prevBtn").click(function() {
		    	$("#barInMenu").removeClass("hidden");
		    	$("#wrapper").addClass("hidden");
		    	
		    	if(page == 1)
		    		window.location.href = "admindashboard";
		    	else
		    		window.location.href = "admindashboard?page="+(page - 1);
			});
			
			$("#nextBtn").click(function(){
				$("#barInMenu").removeClass("hidden");
		    	$("#wrapper").addClass("hidden");
		    	
				window.location.href = "admindashboard?page="+(page + 1);
			});
	});
	
	$(document).ready(
			function() {
				
				
				$('.dataTables-example').DataTable(
						{
							dom : '<"html5buttons"B>lTfgitp',
							buttons : [
									{
										extend : 'copy',
										title : 'Users List',
										exportOptions: {
						                    columns: ':visible'
						                }
									},
									{
										extend : 'csv',
										title : 'Users List',
										exportOptions: {
						                    columns: ':visible'
						                }
									},
									{
										extend : 'excel',
										title : 'Users List',
										exportOptions: {
						                    columns: ':visible'
						                }
									},
									{
										extend : 'pdf',
										title : 'Users List',
										exportOptions: {
						                    columns: ':visible'
						                }
									},

									{
										extend : 'print',
										title : 'Users List',
										exportOptions: {
						                    columns: ':visible'
						                },
										customize : function(win) {
											$(win.document.body).addClass(
													'white-bg');
											$(win.document.body).css(
													'font-size', '10px');

											$(win.document.body).find('table')
													.addClass('compact').css(
															'font-size',
															'inherit');
										}
									} ],
								"aaSorting" : [ [ 0, "desc" ] ],
								"aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
								"iDisplayLength": 5,
								"pageLength": 5
						});

			
				$('#usersListTable').DataTable({
					dom : '<"html5buttons"B>lTfgitp', 
					buttons : [],
					"paging" : false,
					"lengthChange" : false,
					"searching" : false,
					"ordering" : false
				});
			});

	function gotoLinkInMenu(link)//link
	{
		$("#barInMenu").removeClass('hidden');
		$("#wrapper").addClass("hidden");
		window.location.href = link;
	}
</script>

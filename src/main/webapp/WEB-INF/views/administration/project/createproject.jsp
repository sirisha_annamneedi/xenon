<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Add Application</h2>
		<ol class="breadcrumb">
			<li><a href="admindashboard">Dashboard</a></li>
			<li class="active"><strong>Add Application</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form action="insertproject" id="createProjectForm"
						class="wizard-big wizard clearfix form-horizontal" method="POST">
						<div class="content clearfix">
							<fieldset class="body current">
							<div class="row">
										<label class="col-lg-4 pull-right text-right">* fields
											are mandatory</label>
									</div>
								<div class="row">
									<div class="col-lg-10">
										<c:if test="${Model.error == 1}">
											<div class="alert alert-danger">Application name already
												exists, Please try another.</div>
										</c:if>
										
										<c:if test="${Model.error == -1}">
											<div class="alert alert-danger">Application insertion
												failed.</div>
										</c:if>
										<div class="form-group">
											<label class="control-label col-sm-2">Name:* </label>
											<div class="col-sm-10">
												<input type="text" placeholder="Application Name"
													class="form-control characters" name="projectName" tabindex="1">
											</div>
										</div>
										<c:if test="${Model.jiraStatus == 'y'}">
										<div class="form-group">
											<label class="control-label col-sm-2">Jira Project Name:</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Jira Project Name"
													class="form-control characters" name="jiraprojectName" tabindex="2">
											</div>
										</div>
										</c:if>
										<div class="form-group">
											<label class="col-sm-2 control-label"> Description:</label>
											<div class="col-sm-10">
												<!-- <textarea class="form-control" style="max-width: 100%;"></textarea> -->
												<textarea  rows="5" class="form-control characters summernote"
													 name="projectDescription" tabindex="3"> </textarea>
											</div>
										</div>

										<c:if test="${Model.error == 2}">
											<div class="alert alert-danger">Test case prefix name
												already exists, Please try another.</div>
										</c:if>
										
										<c:if test="${Model.error == 3}">
											<div class="alert alert-danger">Bug prefix name
												already exists, Please try another.</div>
										</c:if>
										<div class="form-group">
											<label class="control-label col-sm-2">Test Case
												Prefix:* </label>
											<div class="col-sm-4">
												<input type="text" placeholder="Test Case Prefix"
													class="form-control characters" name="tcPrefix" tabindex="4">
											</div>	
										
											<label class="control-label col-sm-2">Bug Prefix:* </label>
											<div class="col-sm-4">
												<input type="text" placeholder="Bug Prefix"
													class="form-control characters" name="bugPrefix" tabindex="5">
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-2 control-label">Status:</label>
											<div class="col-sm-10">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="inlineRadio1" value="1"
														name="projectStatus" checked="" tabindex="6"> 
													<label for="inlineRadio1">Active</label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inlineRadio2" value="2"
														name="projectStatus" tabindex="7"> 
													<label for="inlineRadio2">Inactive</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="hr-line-solid"></div>
							</fieldset>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-white pull-left"
										style="margin-right: 15px;" type="button"
										id="createProCancelBtn" tabindex="8">Cancel</button>
									<button
										class="pull-left btn btn-success ladda-button ladda-button-demo"
										data-style="slide-up" type="submit" tabindex="9">Submit</button>
								</div>
							</div>

						</div>
					</form>
					<!-- end form -->
				</div>
				<!-- end ibox-content -->

			</div>
			<!-- end ibox -->
		</div>
		<!-- end col -->
	</div>
	<!-- end row -->
</div>
<!-- end wrapper -->
</div>
<script type="text/javascript">

$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});
</script>
<script type="text/javascript">
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});
</script>

<script>
	$(document).ready(function() {
		
		$('.summernote').summernote();
		$('.note-editor').css('background','white');
		$('button[ data-event="codeview"]').remove();
		
		 $('div.note-insert').remove();
         $('div.note-table').remove();
         $('div.note-help').remove();
         $('div.note-style').remove();
         $('div.note-color').remove();
         $('button[ data-event="removeFormat"]').remove();
         $('button[ data-event="insertUnorderedList"]').remove();
         $('button[ data-event="fullscreen"]').remove();
         $('button[ data-original-title="Line Height"]').remove();
         $('button[ data-original-title="Font Family"]').remove();
         $('button[ data-original-title="Paragraph"]').remove();

		$("#createProjectForm").validate({
			rules : {
				projectName : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				projectDescription : {
					minlength : 1,
					maxlength : 200
				},

				bugPrefix : {
					required : true,
					minlength : 2,
					maxlength : 10
				},
				tcPrefix : {
					required : true,
					minlength : 2,
					maxlength : 10
				}

			}
		});

		var l = $('.ladda-button-demo').ladda();
		l.click(function() {
			// Start loading
			l.ladda('start');
			// Do something in backend and then stop ladda
			// setTimeout() is only for demo purpose
			setTimeout(function() {
				
				 $('.summernote').each( function() {
					   $(this).val($(this).code());
					   });
				 
				$('#createProjectForm').submit();
				l.ladda('stop');
			}, 1000)
		});
	});

	$('#createProCancelBtn').click(function() {
		window.location.href="viewproject";
	});
</script>
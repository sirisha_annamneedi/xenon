<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Applications</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li class="active"><strong>Applications</strong></li>
			</ol>
		</div>
		<!-- end col-lg-10 -->
	</div>
	<!-- end row -->
	<!-- end wrapper -->
	<div class="row">
		<div class="col-lg-12">
			<div class="wrapper wrapper-content animated fadeInUp">

				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Applications</h5>
								<c:if test="${model.createProAccessStatus == 1}">
									<div class="ibox-tools">
										<button type="button" class="btn btn-success btn-xs"
											onclick="location.href = 'createproject';">Add Application</button>
									</div>
								</c:if>
							</div>
							<div class="ibox-content">

								<table class="footable table table-stripped toggle-arrow-tiny"
									data-page-size="8">
									<thead>
										<tr>
											<th data-toggle="true">Status</th>
											<th>Name</th>
											<th data-hide="all"></th>
											<th class="text-right" data-sort-ignore="true">Action</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="data" items="${model.projectDetails}">
											<tr>

												<td class="project-status"><span
													class="label projectStatusLabel">${data.projectStatus}</span>
												</td>
												<td class="project-title"><a href="#"
													style="pointer-events: none; cursor: default;">${data.project_name}</a>
													<br /> <small>Create Date: </small><small>${data.project_createdate}</small>
												</td>
												<td><c:if test="${model.createModuleAccessStatus == 1}">
														<div class="ibox-tools">
															<button type="button" class="btn btn-success btn-xs"
																onclick="setModuleApplicationId(${data.project_id})">Add
																Module</button>
														</div>
													</c:if>
													<table class="table datatable moduleTable">
														<thead>
															<tr>
																<th>Module Name</th>
																<th>Module Status</th>
																<!-- <th class="text-right" data-sort-ignore="true">Action</th> -->
															</tr>
														</thead>
														<tbody>
															<c:forEach var="module" items="${model.Modules}">
																<c:if test="${module.project_id == data.project_id}">
																	<tr>
																		<td>${module.module_name}</td>
																		<c:if test="${module.module_active == 1}">
																			<td><span class="label label-success">Active</span></td>
																		</c:if>
																		<c:if test="${module.module_active != 1}">
																			<td><span class="label label-danger">Inactive</span></td>
																		</c:if>
																	</tr>
																</c:if>
															</c:forEach>
														</tbody>
													</table></td>
												<td class="project-actions">
													<button class="btn btn-white btn-sm"
														onclick="applicationSettings(${data.project_id})">Settings</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="5">
												<ul class="pagination pull-right"></ul>
											</td>
										</tr>
									</tfoot>
								</table>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="hiddenInput"
	value='<%=session.getAttribute("proCreateStatus")%>'>

<input type="hidden" id="newProName"
	value='<%=session.getAttribute("newProjectName")%>'>
<script type="text/javascript">

$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	 var foo = $('.footable').footable();
	 foo.trigger('footable_initialize'); //Reinitialize
	 foo.trigger('footable_redraw'); //Redraw the table
	 foo.trigger('footable_resize'); //Resize the table
});
</script>
<script type="text/javascript">
  $(function () {
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	//tostr on redirect from create project
	var state = $('#hiddenInput').val();
	var newProjectName = $('#newProName').val();
	if(state == 1){
		var message =" application is successfully created";
		var toasterMessage = newProjectName +  message;
		showToster(toasterMessage);
	}
	else if(state == 2){
		var message =" application is successfully Updated";
		var toasterMessage = newProjectName +  message;
		showToster(toasterMessage);
	}
	
	$('#hiddenInput').val('11');
});
  function showToster(toasterMessage){
		 toastr.options = {
				 "closeButton": true,
				  "debug": true,
				  "progressBar": true,
				  "preventDuplicates": true,
				  "positionClass": "toast-top-right",
				  "showDuration": "400",
				  "hideDuration": "1000",
				  "timeOut": "9000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "slideDown",
				  "hideMethod": "slideUp"
          };
          toastr.success('',toasterMessage);
	}  
</script>
<script>
	$(function(){

$('.moduleTable').DataTable({
	"paging" : false,
	"lengthChange" : false,
	"searching" : false,
	"ordering" : false,
	"info" : false,
	"autoWidth" : true
});
		/* $('#projectTable').DataTable({
			"order": [[0, "desc"]],
			"paging" : true,
			"lengthChange" : true,
			"searching" : true,
			"ordering" : true,
			"info" : true,
			"autoWidth" : true
		}); */
		setStatusBackground();
		$('#projectTable_length select').on('change',function(){
			setStatusBackground();
		});
		$('#projectTable_filter input').on('keypress',function(){
			setStatusBackground();
		});
		$('#projectTable_paginate').on('click',function(){
			setStatusBackground();
		});
	});

	var setStatusBackground = function(){
		$('.projectStatusLabel').each(function(){
			var status = $(this).text().toLowerCase();
			if(status == 'active')
				$(this).addClass('label-primary');
			else
				$(this).addClass('label-danger');
		});
	}
	
	function viewProject(projectId,viewType){
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		var posting = $.post('setprojectid', {
			projectID : projectId,
			viewType: viewType
 		});
 		 posting.done(function(data) {
 			location.href ="editproject"; 

 		}); 
	}
	
	function assignProject(projectid,projectName){
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		var posting = $.post('setassignprojectid',{
			projectID : projectid,
			projectName : projectName
		});
		posting.done(function(data){
			 location.href = 'assignproject';
		});
	}
	
	function assignModule(id,projectID,moduleName){
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		var posting = $.post('setmoduleidforassign',{
			moduleID : id,
			projectID : projectID,
			moduleName:moduleName
		});
		posting.done(function(data){
			location.href  = "assignmodule";
		});
	}
	
	function applicationSettings(appId){
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		var posting = $.post('setapplicationid',{
			applicationID : appId
		});
		posting.done(function(data){
			location.href  = "applicationsettings";
		});
	}
	
	function setModuleApplicationId(appId){
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		var posting = $.post('setmoduleapplicationid',{
			applicationID : appId
		});
		posting.done(function(data){
			location.href  = "createmodule";
		});
	}
	
</script>

<%
	//set session variable to another value
	session.setAttribute("proCreateStatus", 11);
%>
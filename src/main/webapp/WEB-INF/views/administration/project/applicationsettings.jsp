<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Application Settings</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li><a href="viewproject">Applications</a></li>
				<li class="active"><strong>Application Settings</strong></li>
			</ol>
		</div>
	</div>

	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-lg-12">
				<div class="tabs-container">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#detailsTab">
								Edit </a></li>
						<c:if test="${applicationData[0].projectStatus == 'Active'}">
							<li class=""><a data-toggle="tab" href="#membersTab">
									Members </a></li>
						</c:if>
					</ul>
					<div class="tab-content">
						<div id="detailsTab" class="tab-pane active">
							<div class="panel-body">
								<div class="ibox">
									<div class="ibox-content">
										<form action="updateapplication" id="updateAppform"
											class="wizard-big wizard clearfix form-horizontal"
											method="POST">
											<div class="content clearfix">
												<fieldset class="body current" id="fieldset">
													<div class="row">
														<div class="col-lg-12">
															<div class="form-group hidden">
																<label class="control-label col-sm-2">Application
																	ID</label>
																<div class="col-sm-10">
																	<input type="text" placeholder="Application ID"
																		class="form-control" name="projectId"
																		value="${applicationData[0].project_id}">
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-sm-2">Application
																	Name</label>
																<div class="col-sm-10">
																	<input type="text" placeholder="Application Name"
																		class="form-control characters" name="projectName"
																		id="projectName" disabled
																		value="${applicationData[0].project_name}">
																</div>
															</div>
															<c:if test="${model.jiraStatus == 'y'}">
															<div class="form-group">
																<label class="control-label col-sm-2">Jira Project
																	Name</label>
																<div class="col-sm-10">
																	<input type="text"
																		class="form-control characters"
																		id="jiraProjectName" disabled
																		value="${applicationData[0].issue_tracker_project_key}">
																</div>
															</div>
															</c:if>
															<div class="form-group">
																<label class="col-sm-2 control-label">Application
																	Description</label>
																<div class="col-sm-10" id="">
																	<textarea class="summernote" name="projectDescription"
																		tabindex="1">${applicationData[0].project_description}</textarea>
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-sm-2">Test case
																	prefix</label>
																<div class="col-sm-2">
																	<input type="text" placeholder="Test case Prefix"
																		class="form-control characters" disabled
																		value="${applicationData[0].tm_prefix}">
																</div>

																<label class="control-label col-sm-2">Bug prefix</label>
																<div class="col-sm-2">
																	<input type="text" placeholder="Bug Prefix"
																		class="form-control characters" disabled
																		value="${applicationData[0].bt_prefix}">
																</div>
															</div>

															<div class="form-group">
																<label class="col-sm-2 control-label">Application
																	Status</label>
																<div class="col-sm-10">
																	<div class="radio radio-success radio-inline col-sm-2">
																		<input type="radio" id="inlineRadio1" value="1"
																			tabindex="2" name="projectStatus"
																			${applicationData[0].projectStatus == 'Active' ? 'checked="true"' : ""}>
																		<label for="inlineRadio1"> Active </label>
																	</div>
																	<div class="radio radio-danger radio-inline col-sm-2">
																		<input type="radio" id="inlineRadio2" value="2"
																			tabindex="3" name="projectStatus"
																			${applicationData[0].projectStatus == 'Inactive' ? 'checked="true"' : ""}>
																		<label for="inlineRadio2">Inactive </label>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="hr-line-solid"></div>
												</fieldset>
											</div>
											<div class="row">
												<div class="col-sm-12">
													<button class="btn btn-white" type="button"
														style="margin-right: 15px;" id="btnCancel" tabindex="4">Cancel</button>
													<button class="btn btn-success" id="btnUpdate"
														data-style="slide-up" tabindex="5">Update</button>
												</div>
											</div>
										</form>
										<!-- end form -->
									</div>
									<!-- end ibox-content -->

								</div>
								<!-- end ibox -->
							</div>
						</div>
						<div id="membersTab" class="tab-pane">
							<div class="panel-body">
								<div class="ibox ">
									<div class="ibox-title">
										<strong>Members</strong>
										<button class="btn btn-success btn-sm pull-right"
											id="showUsersBtn">Add users to application</button>
									</div>
									<div class="ibox-content">
										<div class="panel panel-default hidden">
											<div class="panel-heading">Add users</div>
											<div class="panel-body">
												<form action="assignapptousers" class="form-horizontal"
													method="POST" id="assignUsersForm">

													<div class="form-group hidden">
														<label class="control-label col-sm-2">Application
															ID</label>
														<div class="col-sm-10">
															<input type="text" placeholder="Application ID"
																class="form-control" name="appId"
																value="${applicationData[0].project_id}">
														</div>
													</div>

													<div class="form-group hidden">
														<label class="control-label col-sm-2">Application
															Name</label>
														<div class="col-sm-10">
															<input type="text" placeholder="Application Name"
																class="form-control characters" name="appName"
																id="appName" value="${applicationData[0].project_name}">
														</div>
													</div>

													<div class="form-group">
														<label class="col-lg-2 control-label">Users:*</label>
														<div class="col-lg-6">
															<select data-placeholder="Choose Users" id="usersSelect"
																class="chosen-select" name="users" multiple>
																<c:forEach var="users"
																	items="${model.unassignedUsersList}">
																	<option value="${users.user_id}::${users.email_id}">${users.first_name} ${users.last_name}</option>
																</c:forEach>
															</select> <label class="error hidden" id="validationLabel">This
																field is required.</label>
														</div>
													</div>

												</form>
											</div>
											<div class="panel-footer">
												<button class="btn btn-white" id="cancelAddUsersBtn">Cancel</button>
												<button class="btn btn-success" id="addUsersBtn">Submit</button>
											</div>
										</div>
										<div class="table-responsive">
											<table class="table table-hover" id="membersTable">
												<thead>
													<tr>
														<th>Image</th>
														<th>Name</th>
														<th>E-Mail</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data" items="${model.assignedUsersList}">
														<tr>
															<td class="client-avatar"><img alt="image"
																class="img-circle "
																src="data:image/jpg;base64,${data.photo}"></td>

															<td>${data.first_name}&nbsp;${data.last_name}</td>

															<td>${data.email_id}</td>

															<td><button class="btn btn-white btn-sm"
																	onclick="unAssignUser(${data.user_id},${applicationData[0].project_id},'${applicationData[0].project_name}')">Remove</button></td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="actionForTab"
	value='<%=session.getAttribute("actionForTab")%>'>

<input type="hidden" id="hiddenInput"
	value='<%=session.getAttribute("proCreateStatus")%>'>

<input type="hidden" id="newProName"
	value='<%=session.getAttribute("newProjectName")%>'>

<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});

	$(function () {
		  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
		//tostr on redirect from create project
		var state = $('#hiddenInput').val();
		var newProjectName = $('#newProName').val();
		if(state == 1){
			var message =" application is successfully created";
			var toasterMessage = newProjectName +  message;
			showToster(toasterMessage);
		}
		else if(state == 2){
			var message =" application is successfully Updated";
			var toasterMessage = newProjectName +  message;
			showToster(toasterMessage);
		}
		
		$('#hiddenInput').val('11');
	});
	
	 function showToster(toasterMessage){
		 toastr.options = {
				 "closeButton": true,
				  "debug": true,
				  "progressBar": true,
				  "preventDuplicates": true,
				  "positionClass": "toast-top-right",
				  "showDuration": "400",
				  "hideDuration": "1000",
				  "timeOut": "9000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "slideDown",
				  "hideMethod": "slideUp"
          };
          toastr.success('',toasterMessage);
	}
	
	
	$(function() {
		$('#membersTable').DataTable();
		
		//summernote
		$('.summernote').summernote();
		$('.note-editor').css('background','white');
		$('button[ data-event="codeview"]').remove();
		$('div.note-insert').remove();
		$('div.note-table').remove();
		$('div.note-help').remove();
		$('div.note-style').remove();
		$('div.note-color').remove();
		$('button[ data-event="removeFormat"]').remove();
		$('button[ data-event="fullscreen"]').remove();
		$('button[ data-original-title="Line Height"]').remove();
		$('button[ data-original-title="Font Family"]').remove();
		$('button[ data-original-title="Paragraph"]').remove();
		$('button[ data-event="insertUnorderedList"]').remove();

		var actionForTab = $('#actionForTab').val();
		if(actionForTab == "members"){
			//set the automation tab active
			$('a[href="#membersTab"]').click();
		}
	});

	$('#btnCancel').click(function() {
		window.location.href="viewproject";
	});
	
	
	$('#btnUpdate').click(function() {
		
		if ($("#updateAppform").valid()) {
			
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
			
			$("#projectName").removeAttr("disabled");
		
			$('.summernote').each(function() {
				$(this).val($(this).code());
			});
	
			$('#updateAppform').submit();
		}
	});

	function unAssignUser(ID,appID,name){
		
		swal({
			title : "Are you sure?",
			text : "Do you want to remove user from application?",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "Yes, Remove it!",
			cancelButtonText : "No, cancel!",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function() {
		
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
			
			$.ajax({
				url: "unassignapptouser",
				data: {
					appID: appID,
					userID: ID,
					appName: name
				},
				type: "POST",
			})
			.done(function(data){
				if(data == "500"){
					location.href  = "500";
				}else if(data == "200"){
					location.reload();
				}else{
					location.href  = "404";
				}
			});
		});
	}
	
	$('#showUsersBtn').click(function() {
		//to apply choosen
		$('.panel').removeClass('hidden');
		$(".chosen-select").chosen();
	});

	$('#addUsersBtn').click(function() {
       
		var values = $('#usersSelect').val();
        if(values!=null) {
        	
        	$('body').addClass("white-bg");
    		$("#barInMenu").removeClass("hidden");
    		$("#wrapper").addClass("hidden");
    		
        	$("#assignUsersForm").submit();
        }else {
       		$("#validationLabel").removeClass("hidden");
        } 

	});

	$('#cancelAddUsersBtn').click(function() {
		$('.panel').addClass('hidden');
		$("#validationLabel").addClass("hidden");
	});
</script>

<%
	//set session variable to another value
	session.setAttribute("proCreateStatus", 11);
%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<input type="hidden"
	value="<%=session.getAttribute("selectedProjectviewType")%>"
	id="hiddenInputType">

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Edit Application - ${projectDetails[0].project_name}</h2>
		<ol class="breadcrumb">
			<li><a href="admindashboard">Dashboard</a></li>
			<li><a href="viewproject">Applications</a></li>
			<li class="active"><strong>Edit Application</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form action="updateproject" id="form"
						class="wizard-big wizard clearfix form-horizontal" method="POST">
						<div class="content clearfix">
							<fieldset class="body current" id="fieldset" disabled>
								<div class="row">
									<div class="col-lg-10">
										<div class="form-group hidden">
											<label class="control-label col-sm-2">Application ID</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Application ID"
													class="form-control" name="projectId"
													value="${projectDetails[0].project_id}">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-2">Application Name</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Application Name"
													class="form-control characters" name="projectName" id="projectName"
													value="${projectDetails[0].project_name}"  disabled>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Application
												Description</label>
											<div class="col-sm-10" id="txtArea">
												<!-- <textarea class="form-control" style="max-width: 100%;"></textarea> -->
												<textarea rows="5" class="summernote"
													name="projectDescription" tabindex="1">${projectDetails[0].project_description}</textarea>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-sm-2">Test case
												prefix</label>
											<div class="col-sm-2">
												<input type="text" placeholder="Test case Prefix"
													class="form-control characters" value="${projectDetails[0].tm_prefix}"
													disabled>
											</div>
										
											<label class="control-label col-sm-2">Bug prefix</label>
											<div class="col-sm-2">
												<input type="text" placeholder="Bug Prefix"
													class="form-control characters" value="${projectDetails[0].bt_prefix}"
													disabled>
											</div>
										</div>


										


										<div class="form-group">
											<label class="col-sm-2 control-label">Application Status</label>
											<div class="col-sm-10">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="inlineRadio1" value="1" tabindex="2"
														name="projectStatus"
														${projectDetails[0].projectStatus == 'Active' ? 'checked="true"' : ""}>
													<label for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inlineRadio2" value="2" tabindex="3"
														name="projectStatus"
														${projectDetails[0].projectStatus == 'Inactive' ? 'checked="true"' : ""}>
													<label for="inlineRadio2">Inactive </label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</div>
						<div class="row">
							<div class="col-md-1">
								<button class="btn btn-success" type="button" id="btnEdit" tabindex="4">Edit</button>
							</div>
						</div>
						<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-white hidden" type="button"  style="margin-right: 15px;" id="btnCancel" tabindex="5">Cancel</button>
									<button class="btn btn-success hidden ladda-button ladda-button-demo"  id="btnUpdate" data-style="slide-up" tabindex="6" >Update</button>
								</div>
						</div>
					</form>
					<!-- end form -->
				</div>
				<!-- end ibox-content -->

			</div>
			<!-- end ibox -->
		</div>
		<!-- end col -->
	</div>
	<!-- end row -->
</div>
<!-- end wrapper -->
</div>

	
<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});
</script>
<script type="text/javascript">
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});
</script>
<script>

	$("#btnEdit").click(function() {
		$('#fieldset').removeAttr("disabled");
		$("#txtArea").find('button').removeAttr("disabled");
		$('#btnCancel').removeClass('hidden');
		$('#btnUpdate').removeClass('hidden');
		$("#btnEdit").addClass('hidden');
	});
	$('#btnCancel').click(function() {
		location.href  = 'viewproject';
	});
	$(document).ready(function() {
		
		var viewType = $('#hiddenInputType').val();
		if (viewType == 'Edit') {
			$('#fieldset').removeAttr("disabled");
			$("#txtArea").find('button').removeAttr("disabled");
			$('#btnCancel').removeClass('hidden');
			$('#btnUpdate').removeClass('hidden');
			$("#btnEdit").addClass('hidden');
		}
		
		$('.summernote').summernote();
		$('.note-editor').css('background','white');
		$('button[ data-event="codeview"]').remove();
		
		 $('div.note-insert').remove();
         $('div.note-table').remove();
         $('div.note-help').remove();
         $('div.note-style').remove();
         $('div.note-color').remove();
         $('button[ data-event="removeFormat"]').remove();
         $('button[ data-event="insertUnorderedList"]').remove();
         $('button[ data-event="fullscreen"]').remove();
         $('button[ data-original-title="Line Height"]').remove();
         $('button[ data-original-title="Font Family"]').remove();
         $('button[ data-original-title="Paragraph"]').remove();
		
		$("#form").validate({
			rules : {
				projectName : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				projectDescription : {
					minlength : 1,
					maxlength : 200
				}
			}
		});
	});
	var l = $('.ladda-button-demo').ladda();
	l.click(function() {
		// Start loading
		l.ladda('start');
		setTimeout(function() {
			//l.ladda('stop');
			if($("#form").valid()){
				$("#projectName").removeAttr("disabled");
		 		}
			
			$('.summernote').each( function() {
				   $(this).val($(this).code());
				   });
			
			$('#form').submit();
			l.ladda('stop');
		}, 1000)
	});
</script>



<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Assign Applications - ${model.projectNameForAssign}</h2>
		<ol class="breadcrumb">
			<li><a href="admindashboard">Dashboard</a></li>
			<li><a href="viewproject">Applications</a></li>
			<li class="active"><strong>Assign Application</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-content">
					<div class="full-height-scroll">
						<div class="table-responsive" style="overflow-x: visible;">
							<table class="table table-striped table-hover"
								id="assignProjectTable">
								<thead>
									<tr>
										<th class="checkAll" style="width: 20px;"><input
											type="checkbox" id="select-all" value="1"></th>
										<th class=""></th>
										<th>User</th>
										<th>Email</th>
										<th>State</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="data" items="${model.assignedUsersList}">
										<tr class="assignProRow">
											<td><input type="checkbox" checked class="checkedUser"
												value="${data.user_id}"></td>
											<td class="client-avatar"><img alt="image"
												class="img-circle "
												src="data:image/jpg;base64,${data.photo}"></td>
											<td>${data.first_name}&nbsp;${data.last_name}</td>
											<td>${data.email_id}</td>
											<td>Assigned</td>
										</tr>
									</c:forEach>
									<c:forEach var="data" items="${model.unAssignedUsersList}">
										<tr class="assignProRow">

											<td><input type="checkbox" class="checkedUser"
												value="${data.user_id}"></td>
											<td class="client-avatar "><img alt="image"
												class="img-circle "
												src="data:image/jpg;base64,${data.photo}"></td>
											<td>${data.first_name}&nbsp;${data.last_name}</td>
											<td>${data.email_id}</td>

											<td>Unassigned</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<div>
							<button class="btn btn-white" type="button"
								id="assignProjectCancelBtn">Cancel</button>
							<button class="btn btn-success ladda-button" type="button"
								id="assignProjectBtn" data-style="slide-up">Update</button>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">

$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});
</script>
<script type="text/javascript">
  $(function () {
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	});  
</script>
<script>
	$(function(){
		var table = $('#assignProjectTable').DataTable({
			"paging" : true,
			"lengthChange" : true,
			"searching" : true,
			"ordering" : true,
			"info" : true,
			"autoWidth" : true,
			
		});

		var l = $('#assignProjectBtn').ladda();
		l.click(function() {
			// Start loading
			l.ladda('start');
			// Do something in backend and then stop ladda
			
			var selectedUserList  = [];
		var allRows =  table.$(".checkedUser", {"page": "all"});
		/* get all checked data */
		allRows.each(function(){
			if(this.checked)
			{
				selectedUserList.push($(this).val());
				
			}
		});
		/* Post all checked data */
		var selectedUsers = selectedUserList.toString();
		$.post("assignorunassignprojecttouser",{selectedUsers : selectedUsers},function(data){
			if(data=="1")
			{
				l.ladda('stop');
				location.href = "assignproject";
			}	
			else{
				l.ladda('stop');
				location.href = "500";
			}
		});
		
		});
	
	$('#select-all').click(function(event) {  
		var allRows =  table.$(".checkedUser", {"page": "all"});	
	    if(this.checked) {
	    	allRows.each(function(){
				this.checked = true; 
			});
	    }
	    else{
	    	allRows.each(function(){
				this.checked = false; 
			});
	    } 	
	});
	// Handle click on checkbox to set state of "Select all" control
	   $('#assignProjectTable tbody').on('change', 'input[type="checkbox"]', function(){
	      // If checkbox is not checked
	      if(!this.checked){
	         var el = $('#select-all').get(0);
	         // If "Select all" control is checked and has 'indeterminate' property
	         if(el && el.checked && ('indeterminate' in el)){
	            // Set visual state of "Select all" control 
	            // as 'indeterminate'
	            el.indeterminate = true;
	         }
	      }
	   });
	});	
	
	$('#assignProjectCancelBtn').click(function(){
		location.href = 'viewproject';
	});
	
</script>
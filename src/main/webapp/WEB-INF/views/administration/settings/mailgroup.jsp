<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Applications</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li class="active"><strong>Mail Group Setting</strong></li>
			</ol>
		</div>
		<!-- end col-lg-10 -->
	</div>
	<!-- end row -->
	<div class="wrapper wrapper-content animated fadeInRight row">
		<div class="row hidden" id="divEditMailGroup">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content">
						<div class="ibox-title">
							<h5>Edit Mail Group</h5>
						</div>
						<form action="updatemailgroup"
							class="wizard-big wizard clearfix form-horizontal"
							id="updateMailGroup" method="POST"
							style="overflow: visible !important;">
							<div class="content clearfix"
								style="overflow: visible !important;">
								<fieldset class="body current">
									<div class="row">
										<label class="col-lg-4 pull-right text-right">* fields
											are mandatory</label>
									</div>

									<input id="mailGroupId" name="mailGroupId" type="text"
										class="form-control hidden">
									<div class="form-group">
										<label class="col-lg-3 control-label">Mail Group
											Name*:</label>
										<div class="col-lg-7">
											<input id="mailGroupName" name="mailGroupName" type="text"
												class="form-control characters" required>
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Select Users*:</label>
										<div class="col-lg-7">
											<select data-placeholder="Please Select Users"
												class="chosen-select-edit" multiple id="selectUsers"
												name="users" style="min-width: 100%">
												<c:forEach var="user" items="${Model.UserDetails}">
													<option value="${user.user_id}">${user.user_name}</option>
												</c:forEach>
											</select>
											<div class="hidden" id="multiSelectUpdate">
												<span class="error"
													style="font-weight: bold; color: #8a1f11;">This
													field is required.</span>
											</div>

										</div>
									</div>

								</fieldset>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-4">
										<button class="btn btn-white pull-left" id="btnCancelUpdate"
											style="margin-right: 15px;" type="button">Cancel</button>
										<button class="btn btn-success pull-left ladda-button"
											style="margin-right: 15px;" type="button"
											data-style="slide-up" id="btnUpdateMailGroup">Submit</button>
									</div>
								</div>
							</div>
						</form>
						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<div class="row hidden" id="divAddMailGroup">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content">
						<div class="ibox-title">
							<h5>Create New Mail Group</h5>
						</div>
						<form action="insertmailgroup"
							class="wizard-big wizard clearfix form-horizontal"
							id="insertMailGroup" method="POST"
							style="overflow: visible !important;">
							<div class="content clearfix"
								style="overflow: visible !important;">
								<fieldset class="body current">
									<div class="row">
										<label class="col-lg-4 pull-right text-right">* fields
											are mandatory</label>
									</div>

									<c:if test="${Model.insertError == 1}">
										<div class="alert alert-danger" id="insertErrorMsg">Mail
											group name already exists, Please try another.</div>
									</c:if>
									<div class="form-group">
										<label class="col-lg-3 control-label">Mail Group
											Name*:</label>
										<div class="col-lg-7">
											<input name="mailGroupName" type="text"
												class="form-control characters" required>
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Select Users*:</label>
										<div class="col-lg-7">
											<select data-placeholder="Please Select Users"
												class="chosen-select-add" multiple name="users"
												id="selectMultiCreate"
												style="min-width: 100%; z-index: 9999;">
												<c:forEach var="user" items="${Model.UserDetails}">
													<option value="${user.user_id}">${user.user_name}</option>
												</c:forEach>
											</select>

											<div class="hidden" id="multiSelectCreate">
												<span class="error"
													style="font-weight: bold; color: #8a1f11;">This
													field is required.</span>
											</div>
										</div>
									</div>

								</fieldset>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-4">
										<button class="btn btn-white pull-left"
											id="mailGroupCancelBtn" style="margin-right: 15px;"
											type="button">Cancel</button>
										<button class="btn btn-success pull-left ladda-button"
											style="margin-right: 15px;" type="button"
											data-style="slide-up" id="submitMailGroup">Submit</button>
									</div>
								</div>
							</div>
						</form>
						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<!-- end wrapper -->

		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Mail Groups</h5>
						<div class="ibox-tools">
							<button type="button" class="btn btn-success btn-xs"
								id="btnCreateMailGroup">Create New Mail Group</button>
						</div>
					</div>
					<div class="ibox-content">
						<table class="footable table table-stripped toggle-arrow-tiny"
							data-page-size="8">
							<thead>
								<tr>
									<th data-toggle="true">Name</th>
									<th data-hide="all"></th>
									<th class="text-right" data-sort-ignore="true">Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="data" items="${Model.mailGroups}">
									<tr>
										<td class="project-title"><a href="#"
											style="pointer-events: none; cursor: default;">${data.mail_group_name}</a>
										</td>
										<td>
											<table class="table datatable userTable">
												<thead>
													<tr>
														<th class="hidden">Id</th>
														<th></th>
														<th>User Name</th>
														<th>Email Id</th>
														<th>Role</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="user" items="${Model.mailGroupDetails}">
														<c:if test="${user.mail_group_id == data.mail_group_id}">
															<tr class="tr_mail_group_${data.mail_group_id}">
																<td class="hidden">${user.user_id}</td>
																<td><img alt="image" id="userPhotoId"
																	class="img-circle"
																	src="data:image/jpg;base64,${user.user_photo}"
																	style="width: 30px"></td>
																<td class="verticalMiddleAlign">${user.user_name}</td>
																<td class="verticalMiddleAlign">${user.email_id}</td>
																<td class="verticalMiddleAlign">${user.user_role}</td>
															</tr>
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</td>
										<td class="mail-group-actions pull-right">
											<button class="btn btn-white btn-sm btnEditMailGroup"
												onclick="editMailGroup(${data.mail_group_id},'${data.mail_group_name}')">
												Edit</button>
										</td>
									</tr>
								</c:forEach>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="5">
										<ul class="pagination pull-right"></ul>
									</td>
								</tr>
							</tfoot>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	 var foo = $('.footable').footable();
	 foo.trigger('footable_initialize'); //Reinitialize
	 foo.trigger('footable_redraw'); //Redraw the table
	 foo.trigger('footable_resize'); //Resize the table
});
</script>
<script type="text/javascript">
  $(function () {
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	
});
</script>
<script>
	$(function(){

$('.userTable').DataTable({
	"paging" : false,
	"lengthChange" : false,
	"searching" : false,
	"ordering" : false,
	"info" : false,
	"autoWidth" : true
});	
error = "${Model.insertError}";
if(error == 1)
	{
	$("#divAddMailGroup").removeClass('hidden');
	$(".chosen-select-add").chosen();
	//$("ul.chosen-results").css('max-height','150px');
	}
});
	$("#btnCreateMailGroup").click(function()
			{
		$("#insertErrorMsg").addClass('hidden');
			$("#divAddMailGroup").removeClass('hidden');
			$(".chosen-select-add").chosen();
			//$("ul.chosen-results").css('max-height','150px');
			$("#divEditMailGroup").addClass('hidden');
			});
	$("#submitMailGroup").click(function()
			{
			 if($("#insertMailGroup").valid())
				{
				 if($("#selectMultiCreate").val()!=null)
					 {
					    $("#insertMailGroup").submit();
					 }
				 else
					 {
					 $("#multiSelectCreate").removeClass("hidden");
					 }
				
				}
			});
	$("#btnUpdateMailGroup").click(function()
			{
			 if($("#updateMailGroup").valid())
				{
				    
				 if($("#selectUsers").val()!=null)
					 {
					 $("#updateMailGroup").submit();
					 }
				 else
				 {
				   $("#multiSelectUpdate").removeClass("hidden");
				 }
				 
				}
			 
			 
			});
	
	
	
	$("#selectMultiCreate").change(function() {
		$("#multiSelectCreate").addClass("hidden");
	});
	
	$("#selectUsers").change(function() {
		$("#multiSelectUpdate").addClass("hidden");
	});
	
	
	$("#mailGroupCancelBtn").click(function()
			{
		     $("#divAddMailGroup").addClass('hidden');
		     
			});
	
	$("#btnCancelUpdate").click(function()
			{
		$("#divEditMailGroup").addClass('hidden');
			});
	
	function editMailGroup(mailGroupId,mailGroupName){
		localStorage.removeItem('mailGroupName');
		$("#mailGroupName").val("");
		$("#mailGroupId").val("");
		$('#selectUsers').val('').trigger('chosen:updated');
		$(".tr_mail_group_"+mailGroupId).each(function(){
			value = $(this).children('td').first().html()
			 $("#selectUsers option[value='" + value + "']").prop("selected", true);
			$('#selectUsers').trigger("chosen:updated");

		});
		$("#mailGroupName").val(mailGroupName);
		$("#mailGroupId").val(mailGroupId);
		$("#divEditMailGroup").removeClass('hidden');
		$("#divAddMailGroup").addClass('hidden');
		$(".chosen-select-edit").chosen();
		//$("ul.chosen-results").css('max-height','130px');
	}
</script>
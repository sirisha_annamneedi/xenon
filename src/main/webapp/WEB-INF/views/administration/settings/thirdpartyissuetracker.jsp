<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:url var="home" value="/" scope="request" />
<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Third Party Issue Tracker</h2>
		<ol class="breadcrumb">
			<li><a href="admindashboard">Dashboard</a></li>
			<li class="active"><strong>Settings > Issue Tracker</strong></li>
		</ol>
	</div>

</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form id="updateThirdPartyIssueTrackerForm" action="editThirdPartyIssueTracker"
						enctype="multipart/form-data" method="post">
						<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Tracker Type</label>
										<div class="input-group col-sm-8">

											<select class="select-picker form-control"
												id="custTrackerType" name="custTrackerType">
												<c:forEach var="type" items="${Model.issueTrackerType}">
													<option value="${type.id}">${type.issue_tracker_type}</option>
												</c:forEach>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Host URL</label>
										<div class="input-group col-sm-8">
											<input id="custJiraHostURL"
										type="text" class="form-control characters" name="custJiraHostURL"
										value="${Model.CustDetails[0].issue_tracker_host}" placeholder="http://server.domain.com (:port number, if any)">
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Jira Integration User</label>
										<div class="input-group col-sm-8">
											<input id="custJiraIntegUser"
										type="text" class="form-control characters" name="custJiraIntegUser"
										value="${Model.JiraDetails[0].jira_integ_username}" placeholder="Xenon Integration User">
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Integration User Password</label>
										<div class="input-group col-sm-8">
											<input id="custJiraIntegPass"
										type="password" class="form-control characters" name="custJiraIntegPass"
										value="${Model.JiraDetails[0].jira_integ_password}" placeholder="Integration User Password">
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Connection Status</label>
										<div class="input-group col-sm-8">
											<input id="result"
										type="text" disabled="disabled"  class="form-control characters" name="connectionStatus"
										${Model.CustDetails[0].issue_tracker == 1 ? 'value="Connected"' : 'value="Not Connected"'}
										>
										</div>
									</div>
								</div>
							<div class="col-lg-6">
							<div class="form-group">
									<label class="col-sm-4 margine-lable">Enable Integration
											</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
											
												<input type="radio" value="1" name="issueTracker" id="issueTracker"
												${Model.CustDetails[0].issue_tracker == 1 ? 'checked="true"' : ""}>
												 <label>Active</label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="2" name="issueTracker" id="issueTracker"
												${Model.CustDetails[0].issue_tracker == 2 ? 'checked="true"' : ""}
													> <label>Inactive</label>
											</div>
										</div>
									</div>
								</div>
						<div class="col-lg-6">	
							<div class="form-group">	
							<input type="hidden" name = "jiraId" value = "${Model.JiraDetails[0].jira_id}">
							<button type="button" class="btn btn-w-m btn-success"
								id="testJiraConnection">Test Connection</button>
							<button type="button"
								class="btn btn-w-m btn-default" id="cancelCustInfo">
								Cancel</button>
							<button
								class="btn btn-w-m btn-success ladda-button" id="saveIssueTrackerInfo"
								data-style="slide-up">Save</button>
								</div>
							</div>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>
</div>
<style>
.margine-lable {
	margin-bottom: 2.0em !important;
}
</style>

<script>

	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
	
	$(document)
			.ready(
					function() {
						

					
	//validation rules for the #updateThirdPartyIssueTrackerForm
	 $("#updateThirdPartyIssueTrackerForm").validate({
			rules : {
				custJiraHostURL : {
					required : true,
					minlength : 1,
					maxlength : 300
				},
				custJiraIntegUser : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				custJiraIntegPass : {
					required : true,
					minlength : 1,
					maxlength : 40
				}
			}
		});
	
});

	
	
	

	$("#saveIssueTrackerInfo").click(function(event) {
		
		var formValid = $("#updateThirdPartyIssueTrackerForm").valid();
		if(formValid){
			
			$("#barInMenu > p").text('saving data');
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
	
			$("#updateThirdPartyIssueTrackerForm").submit();

		}
	});
	
	$("#cancelCustInfo").click(function(event) {
		window.location.href = "accountsettings";
	});
	
	
	
	$("#testJiraConnection").click(function(e){
        e.preventDefault();
        var formValid = $("#updateThirdPartyIssueTrackerForm").valid();
		if(formValid){
        connectJiraViaAjax();
		}
	});
	
	function connectJiraViaAjax(){
		try {
    	var data={};
    	var url = $("#custJiraHostURL").val();
    	var username=$("#custJiraIntegUser").val();
    	var password=$("#custJiraIntegPass").val();
    	console.log("SUCCESS: ", data);
    	var urlSpring = '${home}getjiraresultviaajax';
    	$.post("getJiraResultViaAjax",  {url:url,username:username,password:password},function( data ) {
    		  $( "#result" ).val( data );
    	});
		}catch(err) {
		    alert(err.message);
		}
    	}
	
	
</script>
<script type="text/javascript">
  $(function () {
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	});  
</script>
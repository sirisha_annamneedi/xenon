<div class="md-skin">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-11">
            <h2>
                <span class="lgMenuTextAlignment" style="width: 90% !important;">
				Http Settings
			</span>
            </h2>
            <!-- 			<ol class="breadcrumb"> -->
            <!-- 			<li><a href="tmdashboard">Dashboard</a></li>
 -->
            <!-- 				<li class="active"><a href="testcase">Test Cases</a></li> -->
            <!-- 			</ol> -->
        </div>
    </div>

    <%-- <div class="row sub-topmenu">
        <div class="col-md-12">
            <div class="">

                <div class="col-md-6 pd-l-0">
                    <ul>
                        <li><a href="javascript:void(0)" onclick="gotoLink('testcase')" id="homeMenu" class=" "><i class="fa fa-home"></i> Home</a></li>
                        <c:if test="${Model.scenarioDetails[0].scenario_id >0}">
                            <!-- <li><a href="javascript:void(0)" id="addTestCase"><i
									class="fa fa-plus"></i> Add Test Case</a></li>
							<li><a href="javascript:void(0)" id="uploadTestCaseMenu"><i
									class="fa fa-upload"></i> Upload Test Case</a></li>
							<li><a href="javascript:void(0)" id="addParameter"><i
									class="fa fa-share-alt"></i> Add Parameter</a></li> -->
                            <li><a href="apitest" id=""><i class="fa fa-plus"></i> Create API Test Case</a></li>
                            <li><a href="soapapi" id="" class="active"><i
									class="fa fa-plus"></i> Create Soap API Test Case</a></li>
                        </c:if>
                    </ul>
                </div>
            </div>
        </div>
    </div> --%>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <!-- <div class="row">
<div class="col-md-12 pd-3">
<nav class="navbar navbar-default white-bg soapui-nav">
  <div class="container-fluid">
   
    <ul class="nav navbar-nav">
      <li ><a href="#">Create Project</a></li>
      <li class="active"><a href="#">Settings</a></li>
     
    </ul>
  </div>
</nav>
</div>
</div> -->
    <div class="row">
        <div class="col-md-3 pd-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content pd-0">
                     <ul class="soapapi-s-m">
                        <li class="active"><a href="#" id="soapapihttpsettings" onclick="gotoLinkInMenu('soapapihttpsettings')">HTTP Settings</a></li>
                        <li><a href="#" id="soapapisettings" onclick="gotoLinkInMenu('soapapiproxysettings')">Proxy Settings</a></li>
                        <li><a href="#" onclick="gotoLinkInMenu('soapapisslsettings')">SSL Settings</a></li>
                        <li><a href="#" onclick="gotoLinkInMenu('soapapiwsdlsettings')">WSDL Settings</a></li>
                    </ul>

                </div>
            </div>
        </div>
        <div class="col-md-9 pd-3">
            <div class="ibox float-e-margins mb-0">
                <div class="ibox-content pb-10">
                    <form class="soapapi-settings">
                        <div class="form-group row">
                            <label class="col-md-4">HTTP Version</label>
                            <div class="col-md-8">
                                <select class="form-control m-b" name="account">
                                        <option>v1.1</option>
                                        <option>v1.22</option>
                                    </select>
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">User Agent Header</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="User Agent Header">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">Request Compression</label>
                            <div class="col-md-8">
                                <select class="form-control m-b" name="account">
                                        <option>None</option>
                                        <option>gzip</option>
                                        <option>deflate</option>
                                    </select>
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Response Compression</label>
                            <div class="col-md-8">
                                <div class="i-checks"><label> <input type="checkbox" value="" checked> <i></i>Accept compressed responses from hosts </label></div>
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Disable Response DeCompression</label>
                            <div class="col-md-8">
                                <div class="i-checks"><label> <input type="checkbox" value="" > <i></i>Disable decompression of compressed responses</label></div>
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Close the HTTP connection after each SOAP request</label>
                            <div class="col-md-8">
                                <div class="i-checks"><label> <input type="checkbox" value="" > <i></i>Disable decompression of compressed responses</label></div>
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Chunking Threshold</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="Chunking Threshold">
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Authenticate Preemptively</label>
                            <div class="col-md-8">
                                <div class="i-checks"><label> <input type="checkbox" value="" > <i></i>Add authentication information to outgoing request</label></div>
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Expect-Continue</label>
                            <div class="col-md-8">
                                <div class="i-checks"><label> <input type="checkbox" value="" > <i></i>Add Expect-Continue header to outgoing request</label></div>
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Pre-encoded Endpoints</label>
                            <div class="col-md-8">
                                <div class="i-checks"><label> <input type="checkbox" value="" > <i></i>URI contains encoded endpoints, don't try to encode re-encode</label></div>
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Normalize Forwarded Slashes</label>
                            <div class="col-md-8">
                                <div class="i-checks"><label> <input type="checkbox" value="" > <i></i>Replace duplicate forward slashes in HTTP request endpoints with a single slash</label></div>
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Bind Address</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="Bind Address">
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Include Request In Time Taken</label>
                            <div class="col-md-8">
                                <div class="i-checks"><label> <input type="checkbox" value="" checked> <i></i>Include the time it took to write request in time-taken</label></div>
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Include Response In Time Taken</label>
                            <div class="col-md-8">
                                <div class="i-checks"><label> <input type="checkbox" value="" checked> <i></i>Include the time it took to read the entire response in time-taken</label></div>
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Socket Timeout(ms)</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="Socket Timeout(ms)">
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Max Response Size</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="Max Response Size">
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Max Connection Per Host</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="Max Connection Per Host">
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Max Total Connections</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="2000">
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Leave MockEngine</label>
                            <div class="col-md-8">
                                <div class="i-checks"><label> <input type="checkbox" value="" checked> <i></i>Leave MockEngine running when stopping MockServices</label></div>
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Enable Mock HTTP Log</label>
                            <div class="col-md-8">
                                <div class="i-checks"><label> <input type="checkbox" value="" > <i></i>Log wire content of all mock requests</label></div>
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-4">Start REST MockService</label>
                            <div class="col-md-8">
                                <div class="i-checks"><label> <input type="checkbox" value="" checked> <i></i>Start REST MockService after creation</label></div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="button" class="btn btn-primary btn-sm">OK</button>
                            <button type="button" class="btn btn-default btn-sm">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>

<script type="text/javascript">
    $(window).load(function() {
        $('body').removeClass("white-bg");
        $("#barInMenu").addClass("hidden");
        $("#wrapper").removeClass("hidden");
        //$('.dataTables_length').addClass('bs-select');

        var foo = $('.footable').footable();
        foo.trigger('footable_initialize'); //Reinitialize
        foo.trigger('footable_redraw'); //Redraw the table
        foo.trigger('footable_resize'); //Resize the table

    });
</script>
<style>
    .ibox-content {
        overflow: hidden;
    }
    
    .ibox {
        margin-bottom: 10px;
    }
    
    .ibox-title h5 {
        font-weight: 600;
    }
    
    .lgMenuTextAlignment {
        overflow: visible !important;
    }
    
    .breadcrumb {
        background: transparent !important;
        height: 42px;
    }
    
    .md-skin .page-heading {
        height: 50px;
    }
    
    .breadcrumb>li {
        padding-top: 5px;
    }
    
    .wrapper-content {
        padding: 10px 10px 40px;
    }
    
    .sub-topmenu ul {
        list-style-type: none;
        padding-top: 8px;
        padding-left: 0;
        padding-bottom: 5px;
    }
    
    .soapui-nav {
        margin-bottom: 5px;
    }
    
    .soapui-nav li {
        border-left: none !important;
        background: transparent !important;
        margin-top: 7px;
        margin-right: 3px;
    }
    
    .soapui-nav li a {
        padding: 8px;
    }
    
    .soapui-nav.navbar-default .navbar-nav>.active>a,
    .soapui-nav.navbar-default .nav>li>a:hover {
        color: #fff;
        background-color: #1c84c6;
        border-radius: 3px;
    }
</style>
<%--  <script src="<%=request.getContextPath()%>/resources/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script> --%>
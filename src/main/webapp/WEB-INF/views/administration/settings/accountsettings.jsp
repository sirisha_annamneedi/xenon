<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:url var="home" value="/" scope="request" />
<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Account Settings</h2>
		<ol class="breadcrumb">
			<li><a href="admindashboard">Dashboard</a></li>
			<li class="active"><strong>Account Settings</strong></li>
		</ol>
	</div>

</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form id="updateCustInfoForm" action="editcustomeradmin"
						enctype="multipart/form-data" method="post">
						<div class="row">
							<div class="col-lg-6">
								<h3>Customer Information</h3>
								<div class="form-group">
									<input id="custId" type="hidden" class="form-control"
										value="${Model.CustDetails[0].customer_id}" name="custId"
										disabled>
								</div>

								<div class="form-group">
									<label>Customer Name *</label> <input id="custNameVerify"
										type="text" class="form-control characters" name="custName"
										value="${Model.CustDetails[0].customer_name}" disabled>
								</div>
								<div class="form-group">
									<label>Address </label>
									<textarea rows="5" cols="50" style="resize: none;"
										id="custAddressVerify" name="custAddress" class="form-control characters"
										placeholder="${Model.CustDetails[0].customer_address}"
										disabled>${Model.CustDetails[0].customer_address}</textarea>
								</div>

							</div>

							<div class="col-lg-6">
								<h3>Admin Information</h3>
								<div class="form-group">
									<label>First name *</label> <input id="firstNameVerify"
										name="firstName" type="text" class="form-control characters"
										value="${Model.custAdminDetails[0].first_name}" disabled>
								</div>
								<div class="form-group">
									<label>Last Name *</label> <input id="lastNameVerify"
										name="lastName" type="text" class="form-control characters"
										value="${Model.custAdminDetails[0].last_name}" disabled>
								</div>
								<div class="form-group">
									<label>Email *</label> <input id="emailIdVerify"
										value="${Model.custAdminDetails[0].email_id}" type="email"
										class="form-control characters" disabled>
								</div>

								<div class="form-group hidden" id="uploadCustLogoDiv">
									<label>Upload Logo</label>
									<div class="fileupload fileupload-new"
										data-provides="fileupload">
										<span class="btn btn-success btn-file"><span
											class="fileupload-new">Upload Logo</span> <span
											class="fileupload-exists">Change</span> <input type="file"
											accept='image/*' id="uploadCustLogo" name="uploadCustLogo" /></span>
										<span class="fileupload-preview"></span> <a href="#"
											class="close fileupload-exists" data-dismiss="fileupload"
											style="float: none">�</a>
									</div>
								</div>

							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<h3>Licence Information</h3>
								<div class="form-group">
									<div class="row">
										<label class="col-sm-4 margine-lable">Customer Type *</label>
										<div class="input-group col-sm-8">

											<select disabled class="select-picker form-control"
												id="custTypeVerify" name="custTypeVerify">
												<c:forEach var="type" items="${Model.custTypes}">
													<option value="${type.id}">${type.customer_type}</option>
												</c:forEach>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Customer Status</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" value="1" name="custStatusVerify"
													checked="" disabled> <label for="inlineRadio1">
													Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="2" name="custStatusVerify"
													disabled> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Bug Tracker
											Status</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" value="1" name="bugTrackerStatusVerify"
													checked="" disabled> <label for="inlineRadio1">
													Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="2" name="bugTrackerStatusVerify"
													disabled> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Test Manager
											Status</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" value="1" name="testManagerStatusVerify"
													checked="" disabled> <label for="inlineRadio1">
													Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="2" name="testManagerStatusVerify"
													disabled> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
								</div>
									<div class="row">
									<div class="form-group">
										<label class="col-sm-4 margine-lable">Automation
											Status</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-3">
												<input type="radio" value="1" name="automationStatusVerify"
													checked="" disabled> <label for="inlineRadio1">
													Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-3">
												<input type="radio" value="2" name="automationStatusVerify"
													disabled> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<h3>Account Information</h3>

								<div class="form-group">
									<label>Created Date</label> <input type="text"
										class="form-control"
										value="${Model.CustDetails[0].created_date} " disabled>
								</div>

								<div class="form-group">
									<label>Start Date</label> <input type="text"
										class="form-control"
										value="${Model.CustDetails[0].start_date} " disabled>
								</div>
								<div class="form-group">
									<label>End Date</label> <input type="text" class="form-control"
										value="${Model.CustDetails[0].end_date}" disabled>
								</div>
								<div class="form-group">
									<label>Cloud Space</label>
									<textarea rows="1"style="resize: none;"
									id="" name="" class="form-control"
									disabled>${Model.cloudSpace[0].Used_Space} MB used out of ${Model.CustDetails[0].max_cloud_space} MB</textarea>
								</div>
								
								
							</div>


						</div>

						<button type="button" class="btn btn-w-m btn-success"
							id="editCustInfo">Edit</button>
						<button style="visibility: hidden;" type="button"
							class="btn btn-w-m btn-default" id="cancelCustInfo">
							Cancel</button>
						<button style="visibility: hidden;"
							class="btn btn-w-m btn-success ladda-button" id="saveCustInfo"
							data-style="slide-up">Update</button>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>
</div>
<style>
.margine-lable {
	margin-bottom: 2.0em !important;
}
</style>

<script>

	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
	
	var customerType;
	var customerBTStatus;
	var customerTMStatus;
	var customerStatus;
	$(document)
			.ready(
					function() {
						customerType = "${Model.CustDetails[0].customer_type}";
						customerBTStatus = "${Model.CustDetails[0].bt_status}";
                        customerTMStatus = "${Model.CustDetails[0].tm_status}";
                        customerStatus = "${Model.CustDetails[0].customer_status}";
                        automationStatus = "${Model.CustDetails[0].automation_status}";
						var image = '${Model.customerLogo}';
						localStorage.setItem("customerLogo", image);
						$(
								"input[name=custStatusVerify][value="
										+ customerStatus + "]").prop('checked',
								true);
						$(
								"input[name=bugTrackerStatusVerify][value="
										+ customerBTStatus + "]").prop(
								'checked', true);
						$(
								"input[name=testManagerStatusVerify][value="
										+ customerTMStatus + "]").prop(
								'checked', true);
						$(
								"input[name=automationStatusVerify][value="
										+ automationStatus + "]").prop(
								'checked', true);

						$("#custTypeVerify").val(customerType);
						
						if($("#custIssueTracker").val()==0)$('.jira').hide();;

					
	//validation rules for the #updateCustInfoForm
	 $("#updateCustInfoForm").validate({
			rules : {
				firstName : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				lastName : {
					required : true,
					minlength : 1,
					maxlength : 40
				}
			}
		});
	
});

	
	
	$("#editCustInfo").click(function(event) {
		editCustInfo();
	});

	function editCustInfo() {
		
		
		$('#uploadCustLogoDiv').removeClass('hidden');
		$('#firstNameVerify').prop("disabled", false);
		$('#lastNameVerify').prop("disabled", false);
		$("#editCustInfo").hide();
		$("#saveCustInfo").css("visibility", "");
		$("#cancelCustInfo").css("visibility", "");
	}

	$("#saveCustInfo").click(function(event) {
		
		var formValid = $("#updateCustInfoForm").valid();
		if(formValid){
			
			$("#barInMenu > p").text('saving data');
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
	
			$("#updateCustInfoForm").submit();

		}
	});
	
	$("#cancelCustInfo").click(function(event) {
		window.location.href = "accountsettings";
	});

	
</script>
<script type="text/javascript">
  $(function () {
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	});  
</script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
<div class="row wrapper page-heading white-bg border-bottom">
	<div class="col-lg-10">
		<h2>Email Settings</h2>
		<ol class="breadcrumb">
			<li><a href="admindashboard">Dashboard</a></li>
			<li><Strong>Email Settings</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-3">
			<div class="ibox float-e-margins">
				<div class="ibox-content mailbox-content">
					<div class="file-manager">
					
						<div class="space-25"></div>
						<h5>E-Mail Settings</h5>
						<ul class="folder-list m-b-md" style="padding: 0px">
							<li class="createNewSmtp hidden"><a href="#"> <i
									class="fa fa-inbox"></i> Email-SMTP Settings
							</a></li>
							<li class="editSmtpDetails" id="editSmtpDetailsId"><a href="#"> <i
									class="fa fa-envelope-o"></i> Email-SMTP Settings
							</a></li>
							<c:if test="${Model.smtpDetails[0].is_enabled == 1}">
							<li class="editSmtpDetailsBm" id="editSmtpDetailsBmId" ><a href="#"> <i
									class="fa fa-envelope-o" ></i> Test Plan Manager </a></li>
							<li class="editSmtpDetailsBt" id="editSmtpDetailsBtId" ><a href="#"> <i
									class="fa fa-envelope-o"></i> Bug Tracker </a></li>
							</c:if>
							
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-9 animated fadeInRight createNewSmtp hidden"
			id="createSMTPForm">
			<div class="mail-box-header">
				<h2>Email Settings</h2>
			</div>
			<div class="mail-box ibox">
				<div class="ibox-content">
					<form action="insertsmtpdetails"
						class="wizard-big wizard clearfix form-horizontal" id="mailForm"
						method="POST">
						<div class="content clearfix">
							<div class="body current">
								<div class="row">
									<div class="form-group">
										<label class="control-label col-lg-2">Host:*</label>
										<div class="col-lg-8">
											<input type="text" placeholder="smtp host"
												class="form-control characters" name="smtpHost">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Port:*</label>
										<div class="col-lg-8">
											<input type="text" placeholder="smtp port"
												class="form-control" name="smtpPort">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">User ID:*</label>
										<div class="col-lg-8">
											<input type="email" placeholder="smtp user id"
												class="form-control characters" name="smtpUserID">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Password:*</label>
										<div class="col-lg-8">
											<input type="password" placeholder="smtp password"
												class="form-control" name="smtpPassword">
										</div>
									</div>
										<div class="form-group">
											<label class="control-label col-lg-2">Send Mail Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="sendMailActive" value="1" onchange="handleChange2();"
														name="mailStatus" > <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="sendMailInactive" value="2" onchange="handleChange2();"
														name="mailStatus" checked=""> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
								</div>
							</div>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-white pull-left mailCancelBtn"
										style="margin-right: 15px;" type="button" >Cancel</button>
									<button class="btn btn-success pull-left ladda-button"
										style="margin-right: 15px;" type="submit"
										data-style="slide-up" id="mailFormBtn">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-lg-9 animated fadeInRight editSmtpDetails"
			id="viewSMTPTable">
			<div class="mail-box-header">
				<h2>Edit Email Settings</h2>
			</div>
			<div class="mail-box ibox">
				<div class="ibox-content">
					<form action="updatesmtpdetails"
						class="wizard-big wizard clearfix form-horizontal" id="UpdateForm1"
						method="POST">
						<div class="content clearfix">
							<div class="body current">
								<div class="row">
									<div class="form-group hidden">
										<label class="control-label col-lg-2 hidden">ID:*</label>
										<div class="col-lg-8 hidden">
											<input type="text" placeholder="smtp ID" class="form-control"
												name="smtpID" value="${Model.smtpDetails[0].id}" id="smtpID">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Host:*</label>
										<div class="col-lg-8">
											<input type="text" placeholder="smtp host"
												class="form-control" name="smtpHost"
												value="${Model.smtpDetails[0].smtp_host}">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Port:*</label>
										<div class="col-lg-8">
											<input type="text" placeholder="smtp port"
												class="form-control" name="smtpPort"
												value="${Model.smtpDetails[0].smtp_port}">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">User ID:*</label>
										<div class="col-lg-8">
											<input type="email" placeholder="smtp user id"
												class="form-control" name="smtpUserID"
												value="${Model.smtpDetails[0].smtp_user_id}">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Password:*</label>
										<div class="col-lg-8">
											<input type="password" placeholder="smtp password"
												class="form-control" name="smtpPassword"
												value="${Model.smtpDetails[0].smtp_password}">
										</div>
									</div>
									<div class="form-group">
											<label class="control-label col-lg-2">Send Mail Status</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input class="statusRadioBtn" type="radio" value="1" id="sendMailActiveUpdate" onchange="handleChange1();"
														name="mailStatus" ${Model.smtpDetails[0].is_enabled == 1 ? 'checked="true"' : ""}> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio"  value="2" onchange="handleChange1();" id="sendMailInactiveUpdate" ${Model.smtpDetails[0].is_enabled == 2 ? 'checked="true"' : ""}
														name="mailStatus" class="statusRadioBtn"> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
								</div>
							</div>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-12">
									<button class="btn btn-white pull-left mailCancelBtn hidden"
										style="margin-right: 15px;" type="button" id="editCancelBtn" >Cancel</button>
									<button class="btn btn-success pull-left ladda-button hidden"
										style="margin-right: 15px;" type="submit"
										data-style="slide-up" id="UpdateFormBtn1">Update</button>
									<button class="btn btn-white pull-left"
										style="margin-right: 15px;" type="button"
										 id="editSettingsBtn">Edit</button>
									<button class="btn btn-success pull-left ladda-button"
										style="margin-right: 15px;" type="button"
										data-style="slide-up" id="testSettingsBtn">Test Settings</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		
		
		
		
		<div class="col-lg-9 animated fadeInRight editSmtpDetailsBm hidden"
			id="mailNotificationBmDiv">
			<div class="mail-box-header">
				<h2>Test Plan Manager E-Mail Settings</h2>
			</div>
			<div class="mail-box ibox">
				<div class="ibox-content">
					<form action="updateMailNotificationBm"
						class="wizard clearfix form-horizontal" id="UpdateForm2"
						method="POST">
						<div class="content clearfix">
							<div class="body current">
								<div class="row">
									<div class="form-group hidden">
										<label class="control-label col-lg-2 hidden">ID:*</label>
										<div class="col-lg-8 hidden">
											<input type="text" placeholder="smtp ID" class="form-control"
												name="mailNotificationId" value="${Model.mailNotificationDetails[0].mail_notification_id}" id="mailNotificationId">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Create User</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-2">
												<input type="radio" value="1" name="mailStatusCreateUser"
													${Model.mailNotificationDetails[0].create_user == 1 ? 'checked="true"' : ""}>
												<label for="inlineRadio1"> Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-2">
												<input type="radio" value="2"
													${Model.mailNotificationDetails[0].create_user == 2 ? 'checked="true"' : ""}
													name="mailStatusCreateUser"> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Assign Application</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-2">
												<input type="radio" value="1" name="mailStatusAssignProject"
													${Model.mailNotificationDetails[0].assign_project == 1 ? 'checked="true"' : ""}>
												<label for="inlineRadio1"> Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-2">
												<input type="radio" value="2"
													${Model.mailNotificationDetails[0].assign_project == 2 ? 'checked="true"' : ""}
													name="mailStatusAssignProject"> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="control-label col-lg-2">Assign Module</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-2">
												<input type="radio" value="1" name="mailStatusAssignModule"
													${Model.mailNotificationDetails[0].assign_module == 1 ? 'checked="true"' : ""}>
												<label for="inlineRadio1"> Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-2">
												<input type="radio" value="2"
													${Model.mailNotificationDetails[0].assign_module == 2 ? 'checked="true"' : ""}
													name="mailStatusAssignModule"> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="control-label col-lg-2">Assign Build</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-2">
												<input type="radio" value="1" name="mailStatusAssignBuild"
													${Model.mailNotificationDetails[0].assign_build == 1 ? 'checked="true"' : ""}>
												<label for="inlineRadio1"> Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-2">
												<input type="radio" value="2"
													${Model.mailNotificationDetails[0].assign_build == 2 ? 'checked="true"' : ""}
													name="mailStatusAssignBuild"> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="control-label col-lg-2">Build Execution Complete</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-2">
												<input type="radio" value="1" name="mailStatusBuildExecComplete"
													${Model.mailNotificationDetails[0].build_exec_complete == 1 ? 'checked="true"' : ""}>
												<label for="inlineRadio1"> Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-2">
												<input type="radio" value="2"
													${Model.mailNotificationDetails[0].build_exec_complete == 2 ? 'checked="true"' : ""}
													name="mailStatusBuildExecComplete"> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
									
									
								</div>
							</div>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-white pull-left mailCancelBtn"
										style="margin-right: 15px;" type="button" >Cancel</button>
									<button class="btn btn-success pull-left ladda-button"
										style="margin-right: 15px;" type="submit"
										data-style="slide-up" id="UpdateFormBtn2">Update</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-lg-9 animated fadeInRight editmailNotificationDetailsBt hidden"
			id="mailNotificationBtDiv">
			<div class="mail-box-header">
				<h2>Bug Tracker E-Mail Settings</h2>
			</div>
			<div class="mail-box ibox">
				<div class="ibox-content">
					<form action="updateMailNotificationBt" id="UpdateForm3"
						class="wizard clearfix form-horizontal" 
						method="POST">
						<div class="content clearfix">
							<div class="body current">
								<div class="row">
									<div class="form-group hidden">
										<label class="control-label col-lg-2 hidden">ID:*</label>
										<div class="col-lg-8 hidden">
											<input type="text" placeholder="smtp ID" class="form-control"
												name="mailNotificationId" value="${Model.mailNotificationDetails[0].mail_notification_id}" id="mailNotificationId">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Create Bug</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-2">
												<input type="radio" value="1" name="mailStatusCreateBug"
													${Model.mailNotificationDetails[0].create_bug == 1 ? 'checked="true"' : ""}>
												<label for="inlineRadio1"> Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-2">
												<input type="radio" value="2"
													${Model.mailNotificationDetails[0].create_bug == 2 ? 'checked="true"' : ""}
													name="mailStatusCreateBug"> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Update Bug</label>
										<div class="col-sm-8">
											<div class="radio radio-success radio-inline col-sm-2">
												<input type="radio" value="1" name="mailStatusUpdateBug"
													${Model.mailNotificationDetails[0].update_bug == 1 ? 'checked="true"' : ""}>
												<label for="inlineRadio1"> Active </label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-2">
												<input type="radio" value="2"
													${Model.mailNotificationDetails[0].update_bug == 2 ? 'checked="true"' : ""}
													name="mailStatusUpdateBug"> <label for="inlineRadio2">
													Inactive </label>
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-white pull-left mailCancelBtn"
										style="margin-right: 15px;" type="button" >Cancel</button>
									<button class="btn btn-success pull-left ladda-button"
										style="margin-right: 15px;" type="submit"
										data-style="slide-up" id="UpdateFormBtn3">Update</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		
	</div>
</div>
<!-- end .wrapper -->
</div>
<input type="hidden"
	value="<%=session.getAttribute("mailDetailsStatus")%>"
	id="hiddenInput">

<script>
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
	
	$(document).ready(function() {
		
		handleChange1();
		handleChange2();
		
		$("#UpdateForm1").find(':input.form-control').attr('readonly',"readonly");
		$('.statusRadioBtn').each(function(){
			$(this).attr('disabled',"disabled");
		});
		
		var hiddenInput = $('#hiddenInput').val();
		if (hiddenInput == 1) {
			/* no smtp record exist */
			$('.editSmtpDetails').addClass('hidden');
			$('.createNewSmtp').removeClass('hidden');

		}
		
		$("#editSmtpDetailsBmId").click(function(){
			$("#viewSMTPTable").addClass("hidden");
			$("#mailNotificationBtDiv").addClass("hidden");
			$("#mailNotificationBmDiv").removeClass("hidden");
		});
		
		$("#editSmtpDetailsId").click(function(){
			$("#viewSMTPTable").removeClass("hidden");
			$("#mailNotificationBtDiv").addClass("hidden");
			$("#mailNotificationBmDiv").addClass("hidden");
		});
		
		$("#editSmtpDetailsBtId").click(function(){
			$("#viewSMTPTable").addClass("hidden");
			$("#mailNotificationBtDiv").removeClass("hidden");
			$("#mailNotificationBmDiv").addClass("hidden");
		});
		
		$("#mailForm").validate({
			rules : {
				smtpHost : {
					required :  function (element) {
	                     if($("#sendMailActive").is(":checked")){
		                       return true;                        
		                     }
		                     else
		                     {
		                         return false;
		                     } } ,
					minlength : 3,
					maxlength : 25
				},
				smtpPort : {
					required : function (element) {
	                     if($("#sendMailActive").is(":checked")){
		                       return true;                        
		                     }
		                     else
		                     {
		                         return false;
		                     } },
					number : true,
					minlength : 1,
					maxlength : 5,
					min 	  : 0,
					max 	  : 65535
				},
				smtpUserID : {
					required : function (element) {
	                     if($("#sendMailActive").is(":checked")){
		                       return true;                        
		                     }
		                     else
		                     {
		                         return false;
		                     } },
					minlength : 5,
					maxlength : 40
				},
				smtpPassword : {
					required : function (element) {
	                     if($("#sendMailActive").is(":checked")){
		                       return true;                        
		                     }
		                     else
		                     {
		                         return false;
		                     } },
					minlength : 3,
					maxlength : 30
				}
			}
		});
		$("#UpdateForm1").validate({
			rules : {
				smtpHost : {
					required : function (element) {
	                     if($("#sendMailActiveUpdate").is(":checked")){
	                       return true;                        
	                     }
	                     else
	                     {
	                         return false;
	                     } } ,
					minlength : 3,
					maxlength : 25
				},
				smtpPort : {
					required :  function (element) {
	                     if($("#sendMailActiveUpdate").is(":checked")){
		                       return true;                        
		                     }
		                     else
		                     {
		                         return false;
		                     } } ,
					number : true,
					minlength : 1,
					maxlength : 5,
					min 	  : 0,
					max 	  : 65535
				},
				smtpUserID : {
					required :  function (element) {
	                     if($("#sendMailActiveUpdate").is(":checked")){
		                       return true;                        
		                     }
		                     else
		                     {
		                         return false;
		                     } } ,
					minlength : 5,
					maxlength : 40
				},
				smtpPassword : {
					required :  function (element) {
	                     if($("#sendMailActiveUpdate").is(":checked")){
		                       return true;                        
		                     }
		                     else
		                     {
		                         return false;
		                     } } ,
					minlength : 3,
					maxlength : 30
				}
			}
		});
	});

	$('.mailCancelBtn').click(function() {
		window.location.replace("mail");
	});

	var l = $('#UpdateFormBtn1').ladda();
	l.click(function() {
		// Start loading
		l.ladda('start');
		// Do something in backend and then stop ladda
		// setTimeout() is only for demo purpose
		setTimeout(function() {
			//l.ladda('stop');
			$('#UpdateForm1').submit();
			l.ladda('stop');
		}, 1000)
	});

	var l = $('#UpdateFormBtn2').ladda();
	l.click(function() {
		// Start loading
		l.ladda('start');
		// Do something in backend and then stop ladda
		// setTimeout() is only for demo purpose
		setTimeout(function() {
			//l.ladda('stop');
			$('#UpdateForm2').submit();
			l.ladda('stop');
		}, 1000)
	});
	
	var l = $('#UpdateFormBtn3').ladda();
	l.click(function() {
		// Start loading
		l.ladda('start');
		// Do something in backend and then stop ladda
		// setTimeout() is only for demo purpose
		setTimeout(function() {
			//l.ladda('stop');
			$('#UpdateForm3').submit();
			l.ladda('stop');
		}, 1000)
	});
	
	$('#mailForm').submit(function() {
		$("#barInMenu > p").text('saving data');
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		return true;
	});
	
	var la = $('#mailFormBtn').ladda();
	la.click(function() {
		
	
		
		la.ladda('start');
		
		setTimeout(function() {
			$('#mailForm').submit();
			la.ladda('stop');
		}, 1000)
	});
	
	function handleChange1()
	{
		
		if(!$("#sendMailActiveUpdate").is(":checked"))
		{
			$("#UpdateForm1").find(':input.form-control').attr('readonly',"readonly");
			//$("#UpdateForm1").find(':input.btn').attr('disabled',true);
			//Disabling the test settings btn
			$('#testSettingsBtn').attr('disabled','false');
		}
		if($("#sendMailActiveUpdate").is(":checked"))
		{
			$("#UpdateForm1").find(':input.form-control').removeAttr('readonly',"readonly");
			//$("#UpdateForm1").find(':input.btn').attr('disabled',false);
			//Enabling the test settings btn
			$('#testSettingsBtn').removeAttr('disabled');
		}
	}
	function handleChange2()
	{
		
		if($("#sendMailActive").is(":checked"))
		{
			$("#mailForm").find(':input.form-control').removeAttr('readonly',"readonly");
			//$("#mailForm").find(':input.btn').attr('disabled',false);
		}
		if(!$("#sendMailActive").is(":checked"))
		{
			$("#mailForm").find(':input.form-control').attr('readonly',"readonly");
			//$("#mailForm").find(':input.btn').attr('disabled',true);
		}
	}
	
	//Test Settings button click event
	$('#testSettingsBtn').click(function(){
		
		//For loading button
		var l = Ladda.create(this);
	 	l.start();
	 	
	 	//ajax call to check the smtp details
		 $.get("testemailsettings",
			  function(data, status){
	        	if (data == true){
	        		swal("Email settings are verified!", "Test mail is sent to you!", "success");
	        		l.stop();
	        	}else {
	        		swal("Email settings are not valid", "Please check your settings", "error");
	        		l.stop();
	        	}
	    }); 
	 	
	});
	
	$('#editSettingsBtn').click(function(){
		$(this).addClass('hidden');
		$('#testSettingsBtn').addClass('hidden');
		$('#editCancelBtn').removeClass('hidden');
		$('#UpdateFormBtn1').removeClass('hidden');
		$("#UpdateForm1").find(':input.form-control').removeAttr('readonly',"readonly");
		$('.statusRadioBtn').each(function(){
			$(this).removeAttr('disabled');
		});
	});
</script>
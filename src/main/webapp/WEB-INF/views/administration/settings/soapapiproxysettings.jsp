<div class="md-skin">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-11">
            <h2>
                <span class="lgMenuTextAlignment" style="width: 90% !important;">
				Proxy Settings
			</span>
            </h2>
            <!-- 			<ol class="breadcrumb"> -->
            <!-- 			<li><a href="tmdashboard">Dashboard</a></li>
 -->
            <!-- 				<li class="active"><a href="testcase">Test Cases</a></li> -->
            <!-- 			</ol> -->
        </div>
    </div>

    <%-- <div class="row sub-topmenu">
        <div class="col-md-12">
            <div class="">

                <div class="col-md-6 pd-l-0">
                    <ul>
                        <li><a href="javascript:void(0)" onclick="gotoLink('testcase')" id="homeMenu" class=" "><i class="fa fa-home"></i> Home</a></li>
                        <c:if test="${Model.scenarioDetails[0].scenario_id >0}">
                            <!-- <li><a href="javascript:void(0)" id="addTestCase"><i
									class="fa fa-plus"></i> Add Test Case</a></li>
							<li><a href="javascript:void(0)" id="uploadTestCaseMenu"><i
									class="fa fa-upload"></i> Upload Test Case</a></li>
							<li><a href="javascript:void(0)" id="addParameter"><i
									class="fa fa-share-alt"></i> Add Parameter</a></li> -->
                            <li><a href="apitest" id=""><i class="fa fa-plus"></i> Create API Test Case</a></li>
                            <li><a href="soapapi" id="" class="active"><i
									class="fa fa-plus"></i> Create Soap API Test Case</a></li>
                        </c:if>
                    </ul>
                </div>
            </div>
        </div>
    </div> --%>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <!-- <div class="row">
<div class="col-md-12 pd-3">
<nav class="navbar navbar-default white-bg soapui-nav">
  <div class="container-fluid">
   
    <ul class="nav navbar-nav">
      <li ><a href="#">Create Project</a></li>
      <li class="active"><a href="#">Settings</a></li>
     
    </ul>
  </div>
</nav>
</div>
</div> -->
    <div class="row">
        <div class="col-md-3 pd-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content pd-0">
                     <ul class="soapapi-s-m">
                        <li><a href="#" id="soapapihttpsettings" onclick="gotoLinkInMenu('soapapihttpsettings')">HTTP Settings</a></li>
                        <li class="active"><a href="#" id="soapapisettings" onclick="gotoLinkInMenu('soapapiproxysettings')">Proxy Settings</a></li>
                        <li><a href="#" onclick="gotoLinkInMenu('soapapisslsettings')">SSL Settings</a></li>
                        <li ><a href="#" onclick="gotoLinkInMenu('soapapiwsdlsettings')">WSDL Settings</a></li>
                    </ul>

                </div>
            </div>
        </div>
        <div class="col-md-9 pd-3">
            <div class="ibox float-e-margins mb-0">
                <div class="ibox-content pb-10">
                    <form class="soapapi-settings">
                        <div class="form-group row">
                            <label class="col-md-2">Proxy Setting</label>
                            <div class="col-md-10">
                                <div class="i-checks"><label> <input type="radio" value="option1" name="a"> <i></i> Automatic</label></div>
                                <div class="i-checks"><label> <input type="radio" value="option2" name="a"> <i></i> None</label></div>
                                <div class="i-checks"><label> <input type="radio" value="option3" name="a"> <i></i> Manual</label></div>
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-2">Host</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" placeholder="Host">
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-2">Port</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" placeholder="Port">
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-2">Excludes</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" placeholder="Excludes">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group mt-5 row">
                            <label class="col-md-2">Username</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group mt-5 row">
                            <label class="col-md-2">Password</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="button" class="btn btn-primary btn-sm">OK</button>
                            <button type="button" class="btn btn-default btn-sm">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>

<script type="text/javascript">
    $(window).load(function() {
        $('body').removeClass("white-bg");
        $("#barInMenu").addClass("hidden");
        $("#wrapper").removeClass("hidden");
        //$('.dataTables_length').addClass('bs-select');

        var foo = $('.footable').footable();
        foo.trigger('footable_initialize'); //Reinitialize
        foo.trigger('footable_redraw'); //Redraw the table
        foo.trigger('footable_resize'); //Resize the table

    });
</script>
<style>
    .ibox-content {
        overflow: hidden;
    }
    
    .ibox {
        margin-bottom: 10px;
    }
    
    .ibox-title h5 {
        font-weight: 600;
    }
    
    .lgMenuTextAlignment {
        overflow: visible !important;
    }
    
    .breadcrumb {
        background: transparent !important;
        height: 42px;
    }
    
    .md-skin .page-heading {
        height: 50px;
    }
    
    .breadcrumb>li {
        padding-top: 5px;
    }
    
    .wrapper-content {
        padding: 10px 10px 40px;
    }
    
    .sub-topmenu ul {
        list-style-type: none;
        padding-top: 8px;
        padding-left: 0;
        padding-bottom: 5px;
    }
    
    .soapui-nav {
        margin-bottom: 5px;
    }
    
    .soapui-nav li {
        border-left: none !important;
        background: transparent !important;
        margin-top: 7px;
        margin-right: 3px;
    }
    
    .soapui-nav li a {
        padding: 8px;
    }
    
    .soapui-nav.navbar-default .navbar-nav>.active>a,
    .soapui-nav.navbar-default .nav>li>a:hover {
        color: #fff;
        background-color: #1c84c6;
        border-radius: 3px;
    }
</style>
<%--  <script src="<%=request.getContextPath()%>/resources/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script> --%>
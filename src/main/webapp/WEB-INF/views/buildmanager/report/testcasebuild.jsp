<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Build Report - ${Model.selectedExecuteBuildName}</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li><a href="builddetails">Build</a></li>
			<li class="active"><strong>Build Report</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Test Cases  </h5>
				</div>

				<div class="ibox-content">
					<div class="table-responsive">
						<table
							class="table table-striped table-bordered table-hover dataTables-example"
							id="table" name="Total Test Cases">
							<thead>
								<tr>
									<th class="hidden">test Id</th>
									<th>Test Case Id</th>
									<th>Test Case Name</th>
									<!-- <th>Status</th> -->
									<th>Status</th>
								
								</tr>
							</thead>
							<tbody>
							<c:forEach var="data" items="${Model.BuildtestCase}">
							<c:if test="${Model.filterText == data.description}">
									<tr style="cursor: pointer" onclick ="dashLink(${data.status})">
										<td class="hidden">${data.testcase_id}</td>
										<td>${data.test_prefix}</td> 
										<td class="textAlignment" data-original-title="${data.testcase_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.testcase_name}</td>
										<%-- <td>${data.status}</td> --%>
										<td>${data.status}</td>
										
									</tr>
								</c:if>
							</c:forEach>
							</tbody>

						</table>
					</div>

				</div>
			</div>
		</div>
	</div>

	
	<!-- end row -->
</div>
</div>
<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	
	$('#table').DataTable({
		dom : '<"html5buttons"B>lTfgitp',
		buttons : [],
		"paging" : true,
		"lengthChange" : false,
		"searching" : true,
		"ordering" : true
	});
	
	
});




function dashLink(link) {

	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");

	var posting = $.post('setExtSysSession', {
		filterText : link,
		extSysName : "ProjectWise"
	});

	posting.done(function(data) {
		window.location.href = "testCasedata";
	});
}

function testcaseStepsTable(clicked_id) {
	
	console.log("In function");

	$('#testcaseListTable_' + clicked_id + ' tbody tr').click(
			function() {
				
				var id = $(this).first().find('td').first().text();
				
				$('div[id^="testcaseInfoDiv_"]').fadeOut("fast");
				$('table').removeClass("statusTable");
				currentTestcaseId=id;
				$("#testcaseInfoDiv_" + id).fadeIn("fast");
				$('#testcaseInfoDiv_' + id).addClass("statusTable");
				/* $("#testcaseInfoDiv_" + id).css("pointer-events", "none"); */
			});

}

function initializeSelectAll(clickedId) {
	var checkedTableId = $(".statusTable").attr("id");

	var tableInitialization;

	if ($.fn.dataTable.isDataTable('#' + checkedTableId)) {
		tableInitialization = $('#' + checkedTableId).DataTable();
	} else {
		tableInitialization = $('#' + checkedTableId).DataTable({
			"aoColumnDefs" : [ {
				'bSortable' : false,
				'aTargets' : [ 1 ]
			} ]
		});
	}

	return tableInitialization;
}
</script>

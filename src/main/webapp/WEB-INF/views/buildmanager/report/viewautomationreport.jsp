
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ page import="java.util.*" %>

<%-- <script
	src="<%=request.getContextPath()%>/resources/js/plugins/jsPDF/html2canvas.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/jsPDF/jspdf.js"></script> --%>
	<script	src="https://unpkg.com/html2canvas@1.0.0-rc.5/dist/html2canvas.js"></script>
	<script	src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
	<script src="https://unpkg.com/jspdf-autotable"></script>
	
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.2/jspdf.plugin.autotable.js"></script>
    <script src="https://unpkg.com/jspdf@1.3.3/dist/jspdf.min.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@2.3.1/dist/jspdf.plugin.autotable.js"></script>
    <script src="<%=request.getContextPath()%>/resources/js/html2pdf.bundle.min.js"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/blitzer/jquery-ui.min.css" rel="stylesheet" type="text/css" />


<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-10" >
			<h2 class="lgMenuTextAlignment" style="width: 90% !important;"  data-toggle="tooltip">Automation Test Set Report - ${Model.selectedExecuteBuildName}</h2>
		</div>
		<div>   
			  <table id="reportDetails" style="display: none;">
				  <tr>
				    <th>Test Set Name</th>
				    <th>User Name</th>
				    <th>Role</th>
				    <th>Export Date</th>
				  </tr>
			  <tr>
				    <td>${Model.selectedExecuteBuildName}</td>
			  </tr>
			  			  <tr>

				    <td>${Model.userName}</td>

			  </tr>
			  			  <tr>
				    <td>${Model.role}</td>
			  </tr>
			  <tr>
				    <td><%= (new java.util.Date()).toLocaleString()%></td>
			  </tr>
			</table>
            
		
		</div>
		</div>
		<div class="row sub-topmenu">
		<div class="col-md-12">
			<div class="">
				<div class="col-md-6 pd-l-0">
					<ul>
						<li><a href="autoexecute"><i class="fa fa-home"></i> Home</a></li>
						<li><a href="javascript:void(0)" id="homeMenu"
							class="active "><i class="fa fa-line-chart"></i> Report </a></li>
					</ul>
				</div>
				<button onclick="printFunction()" class="btn btn-info pull-right">Export
	Dashboard</button>
			</div>
		</div>
	</div>
	<!-- project wise report goes hear -->
	<div id="projectDiv">
		<div class="wrapper wrapper-content animated fadeInRight row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>project wise recent testset execution analysis</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a>
						</div>
						<!-- end .ibox-tools -->
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-12">
								<div>
									<canvas id="projectBarChart" height="80"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="wrapper wrapper-content animated fadeInRight row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Module Table</h5>
					</div>
					<div class="ibox-content">
						<table class="table table-stripped  table-bordered table-hover "
							id="moduleListTable" style="width: 100%;">
							<thead>
								<tr>
									<td>Application Name</td>
									<th>Module Name</th>
									<th>Total TC</th>
									<th>Executed TC</th>
									<th>Pass</th>
									<th>Fail</th>
									<th>Skip</th>
									<th>Block</th>
									<th>Not Run</th>
								</tr>

							</thead>
							<tbody>

								<c:set var="curProject" value="0" />

								<c:forEach var="projectsList" items="${Model.projectList}">
									<c:forEach var="modulesList" items="${Model.moduleList}">
										<c:if
											test="${projectsList.project_id==modulesList.project_id}">
											<tbody>
												<tr>
													<c:choose>
														<c:when test="${curProject!=modulesList.project_id}">
															<td class="totalProject">${projectsList.project_name}</td>
														</c:when>
														<c:otherwise>
															<td></td>
														</c:otherwise>
													</c:choose>

													<td style="width: 200px" class="textAlignment"
														data-original-title="${modulesList.module_name}"
														data-container="body" data-toggle="tooltip"
														data-placement="right" class="issue-info">${modulesList.module_name}</td>
													<c:forEach var="listCount" items="${Model.moduleWiseCount}">
														<c:if test="${listCount.module_id == modulesList.module}">
															<td>${listCount.TotalTcCount}</td>
															<c:set var="Total" value="${listCount.TotalTcCount}" />

															<c:set var="count" value="${listCount.TotalTcCount}"
																scope="page" />
														</c:if>
													</c:forEach>
													<td>${modulesList.ExecutedTcCount}</td>
													<td>${modulesList.PassCount}</td>
													<td>${modulesList.FailCount}</td>
													<td>${modulesList.SkipCount}</td>
													<td>${modulesList.BlockCount}</td>
													<td>${Total-(modulesList.PassCount+modulesList.FailCount+modulesList.SkipCount+modulesList.BlockCount)}</td>
												</tr>
												<c:set var="totalTCTemp" value="${Total}"/>
												<c:set var="curProject" value="${modulesList.project_id}" />
											</tbody>

											<%-- 												</c:if> --%>

										</c:if>
									</c:forEach>

									<tbody>
										<c:set var="Executed" value="${projectsList.ExecutedTcCount}" />
										<c:set var="Pass" value="${projectsList.PassCount}" />
										<c:set var="Fail" value="${projectsList.FailCount}" />
										<c:set var="Skip" value="${projectsList.SkipCount}" />
										<c:set var="Block" value="${projectsList.BlockCount}" />
										<tr style="font-weight: bold">
											<td></td>
											<td>Total</td>
											<%-- <td>${totalTCTemp}</td> --%>  
											<c:forEach var="listCount" items="${Model.projectWiseCount}">
											<c:if test="${listCount.project_id == projectsList.project}">
												<td>${listCount.TotalTcCount}</td>
												<c:set var="proCount" value="${listCount.TotalTcCount}"
													scope="page" />
											</c:if>
										</c:forEach>      
											<td>${ExecutedTcCount+Executed}</td>
											<td  class="totalPass">${Pass}</td>
												<td class="totalFail">${Fail}</td>
												<td class="totalSkip">${Skip}</td>  
												<td class="totalBlock">${Block}</td>
												<%-- <td class="totalNotRun">${Total-(Pass+Fail+Skip+Block)}</td> --%>
												<td class="totalNotRun">${proCount-(Pass+Fail+Skip+Block)}</td>
										</tr>
										<%-- <c:set var="TcCount" value="${TcCount +Total}" /> --%>
										<c:set var="TcCount" value="${TcCount +proCount}" />  
										<c:set var="ExecutedTcCount"
											value="${ExecutedTcCount + Executed}" />
										<c:set var="PassCount" value="${PassCount +Pass}" />
										<c:set var="FailCount" value="${FailCount +Fail}" />
										<c:set var="SkipCount" value="${SkipCount +Skip}" />
										<c:set var="BlockCount" value="${BlockCount + Block}" />
									</tbody>
								</c:forEach>
							<tbody>
								<tr style="font-weight: bold;">
									<td></td>
									<td>Grand Total</td>
									<td>${TcCount}</td>
									<td>${ExecutedTcCount}</td>
									<td>${PassCount}</td>
									<td>${FailCount}</td>
									<td>${SkipCount}</td>
									<td>${BlockCount}</td>
									<td>${TcCount-(PassCount+FailCount+SkipCount+BlockCount)}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="wrapper wrapper-content animated fadeInRight row"
			style="display: none">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Project Table</h5>
						<button type="button" class="btn btn-info pull-right"
							onclick="getAlltestCase();">Test Case Table</button>
					</div>
					<div class="ibox-content">
						<table class="table table-stripped table-hover"
							id="proDetailsTable">
							<thead>

								<tr>
									<th>Application Name</th>
									<th>Description</th>
									<th>Created Date</th>
									<th>Total TC</th>
									<th>Executed TC</th>
									<th>Pass</th>
									<th>Fail</th>
									<th>Skip</th>
									<th>Block</th>
									<th>Not Run</th>
									<th>project Status</th>
								</tr>

							</thead>
							<tbody>
								<c:forEach var="projectsList" items="${Model.projectList}">

									<tr class="allProTableRow"
										onclick="showModuleWiseReport(${projectsList.project})"
										style="cursor: pointer;">
										<td class="textAlignment"
											data-original-title="${projectsList.project_name}"
											data-container="body" data-toggle="tooltip"
											data-placement="right" class="issue-info">${projectsList.project_name}</td>
										<td>${projectsList.project_description}</td>
										<td class="textAlignment"
											data-original-title="${projectsList.project_createdate}"
											data-container="body" data-toggle="tooltip"
											data-placement="right" class="issue-info">${projectsList.project_createdate}</td>

										<c:forEach var="listCount" items="${Model.projectWiseCount}">
											<c:if test="${listCount.project_id == projectsList.project}">
												<td>${listCount.TotalTcCount}</td>
												<c:set var="proCount" value="${listCount.TotalTcCount}"
													scope="page" />
											</c:if>
										</c:forEach>

										<td>${projectsList.ExecutedTcCount}</td>
										<td>${projectsList.PassCount}</td>
										<td>${projectsList.FailCount}</td>
										<td>${projectsList.SkipCount}</td>
										<td>${projectsList.BlockCount}</td>
										<td><c:out
												value="${proCount - projectsList.ExecutedTcCount}" /></td>
										<c:if test="${projectsList.project_active == 1}">
											<td><span class="label label-success">Active</span></td>
										</c:if>
										<c:if test="${projectsList.project_active == 2}">
											<td><span class="label label-danger">Inactive</span></td>
										</c:if>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="wrapper wrapper-content animated fadeInRight row"
			id="allTestCase">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Test case Table</h5>
						<c:choose>
								<c:when test="${Model.testStepsList.size() > 0}">
									<button type="button" class="btn btn-info pull-right"
										onclick="getTestSteps();">Test Steps</button>
								</c:when>
								<c:otherwise>
									<button type="button" class="btn btn-info pull-right"
										disabled="disabled" title="Record not found">Test
										Steps</button>
								</c:otherwise>
							</c:choose>
					</div>
				</div>
				<div class="ibox-content">
					<table class="table table-stripped table-hover tableExport"
 						id="testCaseDetailsTable"> 
 						<thead> 
 							<tr> 
 							<th>Details </th>  
 								<th>Module Name </th> 
 								<th>Scenario Name</th> 
 								<th>Test Case Name</th> 
 								<th style="display: none;">Summary</th> 
 								<th>Start time</th> 
 								<th>End time</th> 
 								<th>Status</th>
							</tr> 

 						</thead> 
						<tbody> 
							<c:forEach var="testCaseList" items="${Model.testCaseList}"> 
 								<tr class="allTestCaseTableRow">
 									<c:forEach var="modulesList3"
 														items="${Model.moduleList}">
 										<td class="details-control" data-testcase-id="${testCaseList.testcase_id}"></td> 
 														<c:if
															test="${modulesList3.module_id==testCaseList.module_id}">
 													<td>${modulesList3.module_name}</td> 
													
 												</c:if> 
													</c:forEach> 
 												<c:forEach var="scenarioList3"
 														items="${Model.scenarioList}"> 
														<c:if 
															test="${scenarioList3.scenario==testCaseList.scenario_id}"> 
													<td>${scenarioList3.scenario_name}</td> 
 												</c:if> 
													</c:forEach> 
 									<td class="textAlignment"
 										data-original-title="${testCaseList.testcase_name}" 
 										data-container="body" data-toggle="tooltip" 
 										data-placement="right" class="issue-info">${testCaseList.testcase_name}</td> 
 									<td style="display: none;">${testCaseList.summary.replaceAll("\\<.*?>","")}</td> 
 									<td>${testCaseList.tcStartTime}</td> 
 									<td>${testCaseList.tcEndTime}</td> 
									<td>${testCaseList.description}</td> 
 								</tr> 
 							</c:forEach>
						</tbody> 
 					</table> 
				</div>
			</div>
		</div>
		<div class="wrapper wrapper-content animated fadeInRight row"
			id="testStepsReport" style="display: none;">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Test Steps</h5>
						<button type="button" class="btn btn-info pull-right"
							onclick="getAlltestCase();">Test Cases</button>
					</div>
					<div class="ibox-content">
						 <!-- <button id="cmd" class="btn btn-default buttons-pdf buttons-html5" style="display: none;">PDF</button> --> 
						<div id="editor"></div>
						<div id="dummydata"></div>
						<table id="testStepsDetailsTable1" style="display: none;">
							
							<thead>
								<tr>
									<!-- 									<th>Module Name</th> -->
									<th>Test Case Name</th>
									<th style="display: none;">Summary</th>
									<th>Start time</th>
									<th>End time</th>
									<th>Test Case Status</th>
									<th>Step Description</th>
									<th>Test/Execution Data</th>
									<th>Actual Result</th>
									<th>Status</th>
									<th class="text-right">Screenshot</th>
									<th class="text-right" style="display: none;">Id</th>
									<th class="text-right" style="display: none;">Blob</th>
									<th>Bug ID</th>
								</tr>

							</thead>
							<tbody>
								<c:set var="curItem" value="0" />

								<c:forEach var="testStepsList" items="${Model.testStepsList}">
									<c:forEach var="testCaseList2" items="${Model.testCaseList}">
										<c:if
											test="${testCaseList2.testcase_id==testStepsList.testcase_id}">
											<tr>
												<%-- 											<td><c:forEach var="modulesList3" --%>
												<%-- 														items="${Model.moduleList}"> --%>
												<%-- 														<c:if --%>
												<%-- 															test="${modulesList3.module_id==testcasesList.module_id}"> --%>
												<%-- 													${modulesList3.module_name} --%>

												<%-- 												</c:if> --%>
												<%-- 													</c:forEach></td> --%>
												<c:choose>
													<c:when test="${curItem!=testStepsList.testcase_id}">
														<td>${testCaseList2.testcase_name}</td>
														<td style="display: none;">${testCaseList2.summary.replaceAll("\\<.*?>","")}</td>
														<td>${testCaseList2.tcStartTime}</td>
														<td>${testCaseList2.tcEndTime}</td>
														<td>${testCaseList2.description}</td>
													</c:when>
													<c:otherwise>
														<td></td>
														<td style="display: none;"></td>
														<td></td>
														<td></td>
														<td></td>
													</c:otherwise>
												</c:choose>
												<td>${testStepsList.step_details}</td>
												<td>${testStepsList.step_value}</td>
												<td>${testStepsList.actual_result}</td>
												<td>${testStepsList.description}</td>
												<td class="text-right"><c:if
														test="${not empty testStepsList.screenshot_title}">
														<div class="btn-group">
															<%-- 															<button id="view_${testStepsList.trial_step_id}"
																class="btn-white btn btn-xs"
																onclick="viewScreenshot(${testStepsList.trial_step_id},'${testStepsList.screenshot_title}');"
																style="margin-right: 10px;">View</button> --%>
															<a href="#" id="view_${testStepsList.trial_step_id}"
																onclick="viewScreenshot(${testStepsList.trial_step_id},'${testStepsList.screenshot_title}');">View</a>
															<%-- <button class="btn btn-white btn-xs" 
													onclick="getScreenshot(${testStepsList.trial_step_id},'${testStepsList.screenshot_title}');">Download</button> --%>
														</div>


													</c:if> <c:if test="${empty testStepsList.screenshot_title}">
														<div class="btn-group">NA</div>
													</c:if></td>



												<td class="text-right" style="display: none;"><c:if
														test="${not empty testStepsList.screenshot_title}">
														
																${testStepsList.trial_step_id}
														
													</c:if> <c:if test="${empty testStepsList.screenshot_title}">
														<div class="btn-group">NA</div>
													</c:if></td>

												<td class="view_${testStepsList.trial_step_id}" style="display: none;"></td>


												<td><c:if test="${testStepsList.is_bug_raised=='1'}">
											   -
											  </c:if> <c:if test="${testStepsList.is_bug_raised=='2'}">
														<c:forEach var="testStepsBugList"
															items="${Model.testStepsBugList}">
															<c:if
																test="${testStepsList.build_id == testStepsBugList.build_id}">
																<c:if
																	test="${testStepsList.testcase_id == testStepsBugList.test_case_id}">
																	<c:if
																		test="${testStepsList.step_id == testStepsBugList.test_step_id}">
																		<a
																			href="bugsummarybyprefix?btPrefix=${testStepsBugList.bug_prefix}"
																			target="_blank"> Bug
																			#${testStepsBugList.bug_prefix}</a>
																	</c:if>
																</c:if>
															</c:if>
														</c:forEach>
													</c:if></td>
											</tr>
											<c:set var="curItem" value="${testStepsList.testcase_id}" />

										</c:if>
									</c:forEach>
								</c:forEach>
							</tbody>
						</table>
						
						<table
							class="table table-stripped table-hover testStepTable tableExport"
							id="testStepsDetailsTable">
							
							<thead>
								<button id="cmd1" class="btn btn-default buttons-pdf buttons-html5 pull-right" style="height: 30px;font-size:12px;">PDF</button>
							
								<tr>
									<!-- 									<th>Module Name</th> -->
									<th>Test Case Name</th>
									<th style="display: none;">Summary</th>
									<th>Start time</th>
									<th>End time</th>
									<th>Test Case Status</th>
									<th>Step Id</th>
									<th>Step Description</th>
									<th>Test/Execution Data</th>
									<th>Actual Result</th>
									<th>Status</th>
									<th class="text-right">Screenshot</th>
									<th class="text-right" style="display: none;">Id</th>
									<th class="text-right" style="display: none;">Blob</th>
									<th>Bug ID</th>
								</tr>

							</thead>
							<tbody>
								<c:set var="curItem" value="0" />

								<c:forEach var="testStepsList" items="${Model.testStepsList}">
									<c:forEach var="testCaseList2" items="${Model.testCaseList}">
										<c:if
											test="${testCaseList2.testcase_id==testStepsList.testcase_id}">
											<tr>
												<%-- 											<td><c:forEach var="modulesList3" --%>
												<%-- 														items="${Model.moduleList}"> --%>
												<%-- 														<c:if --%>
												<%-- 															test="${modulesList3.module_id==testcasesList.module_id}"> --%>
												<%-- 													${modulesList3.module_name} --%>

												<%-- 												</c:if> --%>
												<%-- 													</c:forEach></td> --%>
												<c:choose>
													<c:when test="${curItem!=testStepsList.testcase_id}">
														<td>${testCaseList2.testcase_name}</td>
														<td style="display: none;">${testCaseList2.summary.replaceAll("\\<.*?>","")}</td>
														<td>${testCaseList2.tcStartTime}</td>
														<td>${testCaseList2.tcEndTime}</td>
														<td>${testCaseList2.description}</td>
													</c:when>
													<c:otherwise>
														<td></td>
														<td style="display: none;"></td>
														<td></td>
														<td></td>
														<td></td>
													</c:otherwise>
												</c:choose>
												<td>${testStepsList.step_id}</td>
												<td>${testStepsList.step_details}</td>
												<td>${testStepsList.step_value}</td>
												<td>${testStepsList.actual_result}</td>
												<td>${testStepsList.description}</td>
												<td class="text-right"><c:if
														test="${not empty testStepsList.screenshot_title}">
														<div class="btn-group">
															<button id="view_${testStepsList.trial_step_id}"
																class="btn-white btn btn-xs"
																onclick="viewScreenshot(${testStepsList.trial_step_id},'${testStepsList.screenshot_title}');"
																style="margin-right: 10px;">View</button>
															<%-- <button class="btn btn-white btn-xs" 
													onclick="getScreenshot(${testStepsList.trial_step_id},'${testStepsList.screenshot_title}');">Download</button> --%>
														</div>


													</c:if> <c:if test="${empty testStepsList.screenshot_title}">
														<div class="btn-group">NA</div>
													</c:if></td>



												<td class="text-right" style="display: none;"><c:if
														test="${not empty testStepsList.screenshot_title}">
														
																${testStepsList.trial_step_id}
														
													</c:if> <c:if test="${empty testStepsList.screenshot_title}">
														<div class="btn-group">NA</div>
													</c:if></td>

												<td class="view_${testStepsList.trial_step_id}" style="display: none;"></td>


												<td><c:if test="${testStepsList.is_bug_raised=='1'}">
											   -
											  </c:if> <c:if test="${testStepsList.is_bug_raised=='2'}">
														<c:forEach var="testStepsBugList"
															items="${Model.testStepsBugList}">
															<c:if
																test="${testStepsList.build_id == testStepsBugList.build_id}">
																<c:if
																	test="${testStepsList.testcase_id == testStepsBugList.test_case_id}">
																	<c:if
																		test="${testStepsList.step_id == testStepsBugList.test_step_id}">
																		<a
																			href="bugsummarybyprefix?btPrefix=${testStepsBugList.bug_prefix}"
																			target="_blank"> Bug
																			#${testStepsBugList.bug_prefix}</a>
																	</c:if>
																</c:if>
															</c:if>
														</c:forEach>
													</c:if></td>
											</tr>
											<c:set var="curItem" value="${testStepsList.testcase_id}" />

										</c:if>
									</c:forEach>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end #projectDiv -->

	<!-- Module wise report goes hear -->
	<c:forEach var="projectsList" items="${Model.projectList}">
		<div id="moduleDiv_${projectsList.project}" class="hidden commonClass">
			<div class="wrapper wrapper-content animated fadeInRight row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Module wise recent testset execution analysis</h5>
							<div class="ibox-tools">
								<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
								</a>
							</div>
							<!-- end .ibox-tools -->
						</div>
						<div class="ibox-content">
							<div class="row">
								<div class="col-lg-12">
									<div>
										<canvas id="moduleBarChart_${projectsList.project}"
											height="80"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="wrapper wrapper-content animated fadeInRight row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Module Table</h5>
						</div>
						<div class="ibox-content">
							<table class="table table-stripped table-hover tableExport"
								id="moduleDetailsTable_${projectsList.project}">
								<thead>
									<tr>
										<th>Module Name</th>
										<th>Total TC</th>
										<th>Executed TC</th>
										<th>Pass</th>
										<th>Fail</th>
										<th>Skip</th>
										<th>Block</th>
										<th>Not Run</th>
									</tr>

								</thead>
								<tbody>
									<c:forEach var="moduleList" items="${Model.moduleList}">
										<c:if test="${projectsList.project == moduleList.project }">
											<tr class="allModuleTableRow"
												onclick="showScenarioWiseReport(${moduleList.module},${moduleList.project})"
												style="cursor: pointer;">
												<td class="textAlignment"
													data-original-title="${moduleList.module_name}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${moduleList.module_name}</td>
												<c:forEach var="listCount" items="${Model.moduleWiseCount}">
													<c:if test="${listCount.module_id == moduleList.module}">
														<td>${listCount.TotalTcCount}</td>
														<c:set var="count" value="${listCount.TotalTcCount}"
															scope="page" />
													</c:if>
												</c:forEach>
												<td>${moduleList.ExecutedTcCount}</td>
												<td>${moduleList.PassCount}</td>
												<td>${moduleList.FailCount}</td>
												<td>${moduleList.SkipCount}</td>
												<td>${moduleList.BlockCount}</td>
												<td><c:out
														value="${count - moduleList.ExecutedTcCount}" /></td>
											</tr>
										</c:if>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end #moduleDiv -->
	</c:forEach>


	<!-- scenario wise report -->
	<c:forEach var="moduleList" items="${Model.moduleList}">
		<div id="scenarioDiv_${moduleList.module}" class="hidden commonClass">
			<div class="wrapper wrapper-content animated fadeInRight row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Scenario wise recent testset execution analysis</h5>
							<div class="ibox-tools">
								<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
								</a>
							</div>
							<!-- end .ibox-tools -->
						</div>
						<div class="ibox-content">
							<div class="row">
								<div class="col-lg-12">
									<div>
										<canvas id="scenarioBarChart_${moduleList.module}" height="80"></canvas>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="wrapper wrapper-content animated fadeInRight row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Scenario Table</h5>
						</div>
						<div class="ibox-content">
							<table class="table table-stripped table-hover tableExport"
								id="scenarioDetailsTable_${moduleList.module}">
								<thead>
									<tr>
										<th>Scenario Name</th>
										<th>Total TC</th>
										<th>Executed TC</th>
										<th>Pass</th>
										<th>Fail</th>
										<th>Skip</th>
										<th>Block</th>
										<th>Not Run</th>
									</tr>

								</thead>
								<tbody>
									<c:forEach var="scenarioList" items="${Model.scenarioList}">
										<c:if test="${moduleList.module == scenarioList.module }">
											<tr class="allscenarioTableRow"
												onclick="showTCWiseReport(${scenarioList.scenario},${scenarioList.module})"
												style="cursor: pointer;">
												<td class="textAlignment"
													data-original-title="${scenarioList.scenario_name}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${scenarioList.scenario_name}</td>
												<c:forEach var="listCount"
													items="${Model.scenarioWiseCount}">
													<c:if
														test="${listCount.scenario_id == scenarioList.scenario}">
														<td>${listCount.TotalTcCount}</td>
														<c:set var="count" value="${listCount.TotalTcCount}"
															scope="page" />
													</c:if>
												</c:forEach>
												<td>${scenarioList.ExecutedTcCount}</td>
												<td>${scenarioList.PassCount}</td>
												<td>${scenarioList.FailCount}</td>
												<td>${scenarioList.SkipCount}</td>
												<td>${scenarioList.BlockCount}</td>
												<td><c:out
														value="${count - scenarioList.ExecutedTcCount}" /></td>
											</tr>
										</c:if>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end #scenarioDiv -->
	</c:forEach>

	<!-- Test case wise report -->
	<c:forEach var="scenarioList" items="${Model.scenarioList}">
		<div id="testCaseDiv_${scenarioList.scenario}"
			class="hidden commonClass">
			<div class="wrapper wrapper-content animated fadeInRight row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Test Case wise recent testset execution analysis</h5>
							<div class="ibox-tools">
								<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
								</a>
							</div>
							<!-- end .ibox-tools -->
						</div>
						<div class="ibox-content">
							<div class="row">
								<div class="col-lg-10">
									<div>
										<canvas id="doughnutChart_${scenarioList.scenario}"
											height="80"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="wrapper wrapper-content animated fadeInRight row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Test case Table</h5>
						</div>
						<div class="ibox-content">
							<table class="table table-stripped table-hover tableExport"
								id="testCaseDetailsTable_${scenarioList.scenario}">
								<thead>
									<tr>
										<th>Test Case Name</th>
										<th>Summary</th>
										<th>Start time</th>
										<th>End time</th>
										<th>Status</th>
									</tr>

								</thead>
								<tbody>
									<c:forEach var="testCaseList" items="${Model.testCaseList}">
										<c:if
											test="${scenarioList.scenario == testCaseList.scenario_id}">
											<tr class="allTestCaseTableRow"
												onclick="showTestStep(${testCaseList.testcase_id},${testCaseList.excel_file_present_status} )"
												style="cursor: pointer;">
												<td class="textAlignment"
													data-original-title="${testCaseList.testcase_name}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${testCaseList.testcase_name}</td>
												<td>${testCaseList.summary.replaceAll("\\<.*?>","")}</td>
												<td>${testCaseList.tcStartTime}</td>
												<td>${testCaseList.tcEndTime}</td>
												<td>${testCaseList.description}</td>
											</tr>
										</c:if>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end #testCaseDiv -->
	</c:forEach>

	<!-- Test case wise report -->
	<c:forEach var="testCaseList" items="${Model.testCaseList}">
		<div id="testStepDiv_${testCaseList.testcase_id}"
			class="hidden commonClass">
			<div class="wrapper wrapper-content animated fadeInRight row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Test Case Details</h5>
							<div class="ibox-tools">
								<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
								</a>
							</div>
							<!-- end .ibox-tools -->
						</div>
						<div class="ibox-content">
							<div class="row">
								<div class="col-lg-12">
									<form class="form-horizontal">
										<div class="form-group">
											<label class="control-label col-lg-1">Name:</label>
											<div class="col-lg-8">
												<input type="text" class="form-control"
													value="${testCaseList.testcase_name }" readonly>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-1">Summary:</label>
											<div class="col-lg-8">
												<textarea name="" rows="5" class="form-control"
													style="resize: none;" readonly>${testCaseList.summary } </textarea>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Test Steps</h5>
						</div>
						<div class="ibox-content">
							<table
								class="table table-stripped table-hover testStepTable tableExport"
								id="testStepsDetailsTable_${testCaseList.testcase_id}">
								<thead>
									<tr>
										<th>Step ID</th>
										<th>Step Description</th>
										<th>Test/Execution Data</th>
										<th>Actual Result</th>
										<th>Status</th>
										<th class="text-right">Screenshot</th>
										<th>Bug ID</th>
									</tr>

								</thead>
								<tbody>
									<c:forEach var="testStepsList" items="${Model.testStepsList}">
										<c:if
											test="${testCaseList.testcase_id == testStepsList.testcase_id}">
											<tr>
												<td>${testStepsList.Step_id}</td>
												<td>${testStepsList.step_details}</td>
												<td>${testStepsList.step_value}</td>
												<td>${testStepsList.actual_result}</td>
												<td>${testStepsList.description}</td>
												<td class="text-right"><c:if
														test="${not empty testStepsList.screenshot_title}">
														<div class="btn-group">
															<button id="view_${testStepsList.trial_step_id}"
																class="btn-white btn btn-xs"
																onclick="viewScreenshot(${testStepsList.trial_step_id},'${testStepsList.screenshot_title}');"
																style="margin-right: 10px;">View</button>
															<%-- <button class="btn btn-white btn-xs" 
													onclick="getScreenshot(${testStepsList.trial_step_id},'${testStepsList.screenshot_title}');">Download</button> --%>
														</div>


													</c:if> <c:if test="${empty testStepsList.screenshot_title}">
														<div class="btn-group">NA</div>
													</c:if></td>
												<td><c:if test="${testStepsList.is_bug_raised=='1'}">
											   -
											  </c:if> <c:if test="${testStepsList.is_bug_raised=='2'}">
														<c:forEach var="testStepsBugList"
															items="${Model.testStepsBugList}">
															<c:if
																test="${testStepsList.build_id == testStepsBugList.build_id}">
																<c:if
																	test="${testCaseList.testcase_id == testStepsBugList.test_case_id}">
																	<c:if
																		test="${testStepsList.step_id == testStepsBugList.test_step_id}">
																		<a
																			href="bugsummarybyprefix?btPrefix=${testStepsBugList.bug_prefix}"
																			target="_blank"> Bug
																			#${testStepsBugList.bug_prefix}</a>
																	</c:if>
																</c:if>
															</c:if>
														</c:forEach>
													</c:if></td>
											</tr>
										</c:if>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- end #testStepDiv -->
	</c:forEach>
</div>

<!-- <iframe id="frame1" height="500px" style="width: 100%" style="display: none;"></iframe> -->  
<div id="dialog" style="display: none;" height="600px">
    <div>
    	<iframe id="frame" height="500px" style="width: 100%"></iframe>
   		<!-- <img src="data:image/png;base64," alt="Red dot" /> -->
    </div>
</div>
  
<script>

function getImageBlob(image_id){
	var posting = $.post('getbuildscreenshot', {
		attchmentId : image_id,
	});
	 posting.done(function(data) {
		 id="td.view_"+image_id;
		 $(id).append( data );				 
	  });
}

function tableToJson(table) {
	var data = [];

	// first row needs to be headers
	var headers = [];
	for (var i=0; i<table.rows[0].cells.length; i++) {
	    headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi,' ');
	}

	// go through cells
	for (var i=1; i<table.rows.length; i++) {

	    var tableRow = table.rows[i];
	    var rowData = {};

	    for (var j=0; j<tableRow.cells.length; j++) {

	        rowData[ headers[j] ] = tableRow.cells[j].innerHTML;

	    }

	    data.push(rowData);
	}       

	return data; }
$(document).ready(function(){
	
    var table1 = tableToJson($('#testStepsDetailsTable1').get(0)); 
    $.each(table1, function (i, row)
     {
			var count=0;
         $.each(row, function (j, cellContent) {
     	count=count + 1;
     	
                 if(count==11){
                	            if(!Number.isNaN(parseInt(cellContent))){
                 				var image_id= parseInt(cellContent);
                 				getImageBlob(image_id);	  
                	            }
                 }
         })
     })
});
var colorPass,colorFail,colorSkip,colorNotRun,colorBlock;
$(window).load(function() {
	colorPass = "${Model.Pass}";
	colorFail = "${Model.Fail}";
	colorSkip = "${Model.Skip}";
	colorNotRun = "${Model.NotRun}";
	colorBlock = "${Model.Block}";
        
	var labelArray = [];
	var passArray = [];
	var failArray = [];
	var skippArray = [];
	var blockArray = [];
	var notRunArray = [];
	

	$('.totalProject').each(function(){
		labelArray.push(this.innerHTML); 
    });
	$('.totalPass').each(function(){
		passArray.push(this.innerHTML); 
    });
	$('.totalFail').each(function(){
		failArray.push(this.innerHTML); 
    });
	$('.totalSkip').each(function(){
		skippArray.push(this.innerHTML); 
    });
	$('.totalBlock').each(function(){
		blockArray.push(this.innerHTML); 
    });

	$('.totalNotRun').each(function(){
		notRunArray.push(this.innerHTML); 
    });
	var barData = {
	        labels:labelArray,
	        datasets: [
			            {
			                label: "Pass",
			                backgroundColor: colorPass,
				            hoverBackgroundColor: colorPass,
			                data: passArray
			            },
			            {
			                label: "Fail",
			                backgroundColor: colorFail,
				            hoverBackgroundColor: colorFail,
			                data:failArray
			            },
			            {
			                label: "Skip",
			                backgroundColor: colorSkip,
				            hoverBackgroundColor:colorSkip,
			                data: skippArray
			            },
			            {
			                label: "Block",
			                backgroundColor: colorBlock,
				            hoverBackgroundColor: colorBlock,
			                data: blockArray
			            },
			            {
			                label: "Not Run",
			                backgroundColor: colorNotRun,
				            hoverBackgroundColor: colorNotRun,
			                data: notRunArray
			            }
			            ]
	    };
	    var barOptions = {
	            scaleBeginAtZero: true,
	            scaleShowGridLines: true,
	            scaleGridLineColor: "rgba(0,0,0,.05)",
	            scaleGridLineWidth: 1,
	            barShowStroke: true,
	            barStrokeWidth: 2,
	            barValueSpacing: 5,
	            barDatasetSpacing: 1,
	            responsive: true,
	            scales: {
		            yAxes: [{
		                    ticks: {
		                        min: 0,
		                        callback: function(value) {if (value % 1 === 0) {return value;}}
		                    }
		            }]
		        }
	        }
	    
	    var ctx = document.getElementById("projectBarChart");
	  
	    var myNewChart = new Chart(ctx, {
	    	 type: 'bar',
			    data: barData,
			    options: barOptions
		});
	
	
	
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});

	function dashLink(link) {
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		var posting = $.post('setTestIdParameter', {
			filterText : link,
			
		});

		posting.done(function(data) {
			window.location.href = "automationtestcase";
		});
	}


	$(document).ready(function() {
		
		var datatable = $(".tableExport").DataTable({
			dom : '<"html5buttons"B>lTfgitp',
			buttons : [
					{
						extend : 'csv',
						title : 'Testset Report'
					},
					{
						extend : 'excel',
						title : 'Testset Report'
					},
					{
						extend : 'print',
						customize : function(win) {
							$(win.document.body).addClass('white-bg');
							$(win.document.body).css('font-size',
									'10px');

							$(win.document.body).find('table')
									.addClass('compact').css(
											'font-size', 'inherit');
						}
					} ],
			"paging" : true,
			"lengthChange" : true,
			"searching" : true,
			"ordering" : false,
			"info" : true,
			"autoWidth" : true
		});
		/* Formatting function for row details - modify as you need */
		function format ( d ) {
		    // `d` is the original data object for the row
			var table = '<table class="table" border="1">'+
						'<thead class="thead-light">'+
							'<tr>'+
					            '<th>Step Id</th>'+
					            '<th>Step Details </th>'+
					            '<th>Step Value </th>'+
					            '<th>Actual Result</th>'+
					            '<th>Status </th>'+
					        '</tr>'+
				        '</thead>';
			
			var iterationCount = d[0].iteration_number;
			table += '<tr class="table-dark">'+
			            '<td colspan=5>Test Execution Number : '+ (iterationCount) +'</td>'+
			        '</tr>';
//  			console.log(d );
		    $.each(d, function(i, object){
// 		    	console.log(iterationCount +' - '+ object.iteration_number);
		    	if( object.iteration_number != iterationCount ) {
		    		table += '<tr class="table-dark">'+ 
					            '<td colspan=5>Test Execution Number : '+ (++iterationCount) +'</td>'+
					        '</tr>';
		    	}
		    	table += '<tr>'+
				        	'<td>'+object.step_id+'</td>'+
				            '<td>'+object.step_details+'</td>'+
				            '<td>'+object.step_value+'</td>'+
				            '<td>'+object.actual_result+'</td>'+
				            '<td>'+(object.step_status == "1" ? "Pass" : "Fail")+'</td>'+
				        '</tr>'; 
		    });
		    table += '</table>';
		    return table;
		}
	    // Add event listener for opening and closing details
	    $('#testCaseDetailsTable tbody').on('click', 'td.details-control', function () {
	        var tr = $(this).closest('tr');
	        var row = datatable.row( tr );
	        var $this = $(this);
	        if ( row.child.isShown() ) {
	            // This row is already open - close it
	            row.child.hide();
	            tr.removeClass('shown');
	        }
	        else {
	            // Open this row
	            var execution_data = "";
	            $.ajax({
		            type : "POST",
		            url : "getAutoStepExecSummary",
		            data : {
		            	buildId: '${sessionScope.automationExecBuildId}',
		            	testCaseId: $this.data('testcase-id'),
		            }
		        }).done( function( response ){
		            //window.location.href = "autoexecute";
		        	execution_data = response;
		        	row.child( format(response) ).show();
		            tr.addClass('shown');
		        }).fail( function( jqXHR, testStatus, errorThrown ) {
		            return false;
		        }); 
	            
	        }
	    } );
		
		var proDetails = $('#proDetailsTable').DataTable({
			dom : '<"html5buttons"B>lTfgitp',
			buttons : [
					{
						extend : 'csv',
						title : 'Testset Report'
					},
					{
						extend : 'excel',
						title : 'Testset Report'
					},
					{
						extend : 'pdf',
						title : 'Testset Report'
					},

					{
						extend : 'print',
						customize : function(win) {
							$(win.document.body).addClass('white-bg');
							$(win.document.body).css('font-size',
									'10px');

							$(win.document.body).find('table')
									.addClass('compact').css(
											'font-size', 'inherit');
						}
					} ],
			"paging" : true,
			"lengthChange" : true,
			"searching" : true,
			"ordering" : false,
			"info" : true,
			"autoWidth" : true
		});
		
		
		//New Code For PDf generation.
		
		$('#cmd1').click(function(){
			var tC =JSON.parse('${Model.testCaseListJson}'); 
            var tS =JSON.parse('${Model.testStepsListJson}'); 
            var result=[["Test Case Name", "Step Description", "Status", "Screenshot"]];
            var oldTcId="";
            var testCName="-";
            for(var i=0;i<tS.length;i++){
            	let tcId= tS[i].testcase_id;
            	//oldTcId =tS[i].testcase_id; 
                if(oldTcId != tcId){
                	
                	for(var j=0;j<tC.length;j++){
                       if(tcId == tC[j].testcase_id){
                    	   testCName = tC[j].testcase_name;
                    	   oldTcId = tcId;
                    	   break;
                       }
                		
                	}  
                }else{
                	testCName="-";
                }
                result.push([testCName,tS[i].step_details,tS[i].description,"data:image/png;base64,"+tS[i].screenshot]);
                        	
            }
            //alert(JSON.stringify(result));
            var div = document.createElement("div");
            div.setAttribute("id", "div1");
            var table = document.createElement('table');
            table.setAttribute("id", "table1");
            table.style.width = '100%';
            table.style.textAlign = "center";
            table.style.marginTop = "40px";
            table.setAttribute('border', '1');
           
			
			var columnCount = result[0].length;

            //Add the header row.
            var row = table.insertRow(-1);
            for (var i = 0; i < columnCount; i++) {
                var headerCell = document.createElement("TH");
                headerCell.innerHTML = result[0][i];
                headerCell.style.backgroundColor = "#1AA0F5";
                headerCell.style.color = "white";
                headerCell.style.height = "50px";
                headerCell.style.textAlign = "center";
                row.appendChild(headerCell);
            }

            //Add the data rows.
            for (var i = 1; i < result.length; i++) {
                row = table.insertRow(-1);
                for (var j = 0; j < columnCount; j++) {
                    var cell = row.insertCell(-1);
                    cell.style.height = "30px";
                    if (j == 3) {
                       cell.innerHTML='<a href="'+result[i][j]+'">View</a>';
                    } else {
                        cell.innerHTML = result[i][j];
                    }
                }
            }
            // report details table
   			 var rdJsonArr = tableToJsonArr(document.getElementById('reportDetails'));
   			 var table2 = document.createElement('table');
   		     var tblBody = document.createElement("tbody");
             //alert(rdTableJson);
   			 table2.setAttribute("id", "table2");
   			 table2.style.width = '100%';
   			 table2.setAttribute('border', '1');
   			 table2.style.marginTop = "20px";
   			 table2.style.textAlign = "center";
   			 //table2.style.marginBottom = "30px";
   			 var rdData=[["TestSet Name","User Name","Role","Export Date"]];
   			 var rdArr=[];
   			 for(var p = 0; p < rdJsonArr.length; p++){
   				 var keys = Object.keys(rdJsonArr[p]);
   				 for(var q = 0; q < 1; q++){   					 
   					rdArr.push(rdJsonArr[p][keys[q]]); 
   				 }  				 
   			 }
   			 rdData.push(rdArr);
   			 
             //Add the report detail header row.
            var columnCnt = result[0].length;

            //Add the header row.
            var row1 = table2.insertRow(-1);
            for (var i = 0; i < columnCnt; i++) {
                var headerCell = document.createElement("TH");
                headerCell.innerHTML = rdData[0][i];
                headerCell.style.backgroundColor = "#1AA0F5";
                headerCell.style.color = "white";
                headerCell.style.height = "50px";
                headerCell.style.textAlign = "center";
                row1.appendChild(headerCell);
            }

            //Add the data rows.
            for (var i = 1; i < rdData.length; i++) {
                row1 = table2.insertRow(-1);
                for (var j = 0; j < columnCnt; j++) {
                    var cell = row1.insertCell(-1);                   
                        cell.innerHTML = rdData[i][j];
                        cell.style.height = "50px";
                    
                }
            }     
         
            var heading = createH1Element('Automation Report'); 
            div.appendChild(heading);
            div.appendChild(table2);
            var barchrt = document.getElementById("projectBarChart");
	        var imgData = barchrt.toDataURL('image/png');
	        var sourceImage = document.createElement('img');	        
	        sourceImage.src = imgData;
	        sourceImage.width="400";
	        sourceImage.height ="200";
	        sourceImage.style.marginTop="30px";
	        sourceImage.style.marginBottom="30px";
	        sourceImage.style.marginLeft="300px";
            div.appendChild(sourceImage);
            var modListTbl = document.getElementById("moduleListTable");
            var modListTblCln = modListTbl.cloneNode(true);
            modListTblCln.style.marginTop="25px";
            div.appendChild(modListTblCln);
            div.appendChild(table);
            document.body.appendChild(div);

            //mode: ['css', 'legacy'],
            var element = document.getElementById('div1');
            var opt = {
                margin: 10,
                filename: 'AutomationReport.pdf',
                enableLinks:true,
                pagebreak: {mode: 'avoid-all' },
                image: {
                    type: 'png',
                    quality: 1
                },
                html2canvas: {dpi: 72, letterRendering: true},
                jsPDF: {
                    unit: 'mm',
                    format: 'a4',
                    orientation: 'landscape',                   
                },
                pdfCallback: pdfCallback
            };

            // New Promise-based usage:
            html2pdf().set(opt).from(element).save();
            var myobj = document.getElementById("div1");
            myobj.remove();
           
			
			
		});
		
		function pdfCallback(pdfObject) {
		    var number_of_pages = pdfObject.internal.getNumberOfPages()
		    var pdf_pages = pdfObject.internal.pages
		    var myHeader = "Automation Report.";
		    var myFooter = "Copyright@2021 Jade Global Inc. All rights reserved.";
		    for (var i = 1; i < pdf_pages.length; i++) {
		        // We are telling our pdfObject that we are now working on this page
		        pdfObject.setPage(i);
		        pdfObject.text(myHeader, 10, 10)
		        // The 10,200 value is only for A4 landscape. You need to define your own for other page sizes
		        pdfObject.text(myFooter, 10, 150)
		    }
		}
		
		function createH1Element(text){
			var h1=document.createElement("H3");
			var txt = document.createTextNode(text);
			h1.appendChild(txt);
			h1.style.textAlign="center";
			return h1;		
		}
		
		function tableToJsonArr(table) {
		    var data = [];

		    // first row needs to be headers
		    var headers = [];
		    for (var i=0; i<table.rows[0].cells.length; i++) {
		        headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi,'');
		    }

		    // go through cells
		    for (var i=1; i<table.rows.length; i++) {

		        var tableRow = table.rows[i];
		        var rowData = {};

		        for (var j=0; j<tableRow.cells.length; j++) {

		            rowData[ headers[j] ] = tableRow.cells[j].innerHTML;

		        }

		        data.push(rowData);
		    }       

		    return data;
		}
		//////
		
		
		
		
		
				$('#cmd').click(function ()  {
            
	        var table1 = 
	            tableToJson($('#testStepsDetailsTable1').get(0)),
	            
	            cellWidth = 50 ,
	            rowCount = -1,
	            rowCount1 = -1,
	            cellContents,
	            leftMargin = 5,
	            topMargin = 12,
	            topMarginTable = 55,
	            headerRowHeight = 11,
	            rowHeight = 11
	            
	            var table2 = tableToJson($('#reportDetails').get(0)),
	            
		            cellWidth = 80 ,
		            rowCount = -1,
		            rowCount1 = -1,
		            cellContents,
		            leftMargin = 5,
		            topMargin = 12,
		            topMarginTable = 55,
		            headerRowHeight = 11,
		            rowHeight = 11

	             l = {
	             orientation: 'p',
	             unit: 'mm',
	             format: 'a4',
	             compress: true ,
	             fontSize: 10,
	             lineHeight: 1,
	             autoSize: false,
	             printHeaders: true,
	             align: 'center',
	             
	         };

	        var doc = new jsPDF(l, '', '', '');

	        doc.setProperties({
	            title: 'Test PDF Document',
	            subject: 'This is the subject',
	            author: 'author',
	            keywords: 'generated, javascript, web 2.0, ajax',
	            creator: 'author'
	        });
	       // 
	       
	       
	        var imgData = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCABYAJoDASIAAhEBAxEB/8QAHQAAAQUBAQEBAAAAAAAAAAAAAAEGBwgJBQQDAv/EAE4QAAECBAMDBAkPCQkAAAAAAAIDBAABBQYHCBIREyIUITJSMTdBQlFicnWRFhhWV2FxgpKUlbO0wtLTIyQnOGWBk6KyCRUXM0NUY3OE/8QAGwEAAgIDAQAAAAAAAAAAAAAAAAYEBwEDBQL/xAA5EQABAwICBgYJAgcAAAAAAAACAAMEAQUGEhEiMkJSchMWUWKiwRQVISM0QVPC0jGScYKhsbLR8v/aAAwDAQACEQMRAD8A02uS4qNaNFdXBX3wMqUwTmo4XICKQB4eHniOpZsMv8tk5YjNZbO5yRx9yPvmklKWAF57ZbNlP5vjjGV8N+GsNx7zHN54ypoLR7EjYlxLIscgGWRGuqtSvXYZf/bIafJHH4cHrsMv/tkNPkjj8OMtenHoBsZwx9Q4X1D/AKf6S31+uP0h8X5LUT12OX/2yWnyRz+HB67HL/7ZLT5I5/DjMAGEBsIOo0HjPwr117uP0h8X5LT/ANdjl/8AbJafJHP4cHrscv8A7ZLT5I5/DjLs2xhHn6EHUWDxn4V56+3H6Q+L8lqV67HL/OfbHay/8jj7kSPQq9SrnozO4KC7B2xfpAu2XAZjrAuxPijG6NXsuYy/wNscpS2znRWu3+HKFzEuG49laA2TKubtTPhnE0i+Pmy8A0yqTYISULCgnZEEEECEQQQQIRBBBAhEEEECFE+aTnwBvSf7O+2MZWAGqNVM0f6v96ebvtjGWzNHXFo4F+Cc5/KiqXHoZ5zXKvQ2Zx0kWcPbDTB29sUAe+o1kzeFTNEnKSrsET4/EOONyBdmuq0doGiqgZgsB9MDDvIZimtuOG0B66WAhm2AOGGoS5oMIDYRJFjYQ3xiDT39XtimNiZ00924XcOQSEeCRd/4k5QzzRDWYcB+OEaW5QOmbYH7QW5yMbYAZhtpuLM45rlnDtWbRynjaJrbyjPMpqGGmNYcunaMsbzI1+jlGVbxHRGqmXPtG2N5kbfRyhQx58G1zeSbMAhknO8qkqCE2yg2yisFbCWCEggQlghINsvDAhLBBBAhEEEECFFGaTtA3r5un/WMZhsAjTzNF2g7083/AGwjMhjFmYI+Cc5/KiqzHHxzXL9ykzBnEN9hXfDC7Gus24HuXzcP9VsfTD7fwImXMhhXOpXfRr7w+QB+wvxZBMJpT4JvTlwKeQYcfwD8MVuZxezLBKpULCeioX07bJJVSoH6nUHOze7rQZhINvkKGHiR6xAdYDgT2NrZ5lpsIesQOC/sbXL/ANKOcdK/TcHcNaVgJZ7qXLnTaatacB092fT2/wDce34AziudIpFUr9TQotFp6r5+5PSkggOsylDmxgplz0rEu4G14rcoqajs1yX2bAVQPoEEu4GjuRNuVi1Kozw3u7ES2WCLq5HM1KbSd6QgAbtMNnOXYlrPu9SMNvN2m3+kacxn8+8Sw4wd4uPQbIh4RFMQcpOMpMuUchpIL6dXJSqA778OIXuWgVe3qk5otdp6rJ+2LSqgqO0xidQy8ZmJVud0AJzq++5Ty6daDe6/jx3c6VBIaTZV31Juk3rblubCoClPbxzAD5vcA9ceod2dpLBhxwTofB8v4rMm1t1iG822Q5OLeVPn4RopY2LFl4R5dMPq9e7tw2ZOKa1bAaTc1p7yaervJeLOM8H0Wlx8/UywyU8dj9VWiViSOM30aO7skfkomHZZwvSJLW2I/cp8xEzV4Q4a1GdCrNcXeVEExNZBghNY0fcPZwgXi7dsO3DDGTD/ABepqlQsyuSc8nnsct1RmmsjPxwn2IbOBODlmWlhpRZ/3Exfv6swReVF46QFVVyssEjUmZlLbMdpT5ogq16Gywpz1ztu0w5JR6y3VI2qcuABVbTV0S8XfhrhHC3wZdHmY2bOA1LNxZf7J5KfcIhMvScuR0qDl4cytNemK1iYfvqdTrqr6LJepnOSIlPojLbxn1A7mqcV6rWbyoU7FVRNi6QfWQgro/NWm1ZUd309Z7O/hsZibztrF3ECj2rbVHWRqrJ+dJc1BwGjX+W0AAcfQ16zizVnYJYdWbQkqQ0tanPD0SFdy8bgqquXXOZS55xsGPEtkcHJg1Iz06vmg5M65yDZhkIgG8mXhpmaZ4l4kLWbTLacDTDSJZo9mctcgAeMlg7znnsl78Svel30PD+13143CqqlTaWAmuaSczOUpnIej3ecpRVnMJa1By+XFTsWbOdKUeTiZoI0toWjfOdGzQHcBGYS2n5HB04dd5liQtk2uBbFgkzr6jRRZWYmme1EnIkGrRwbdGz0Rh62xnzYeY1WjqI97vIjXOVHB+PJ1ngzF3e6nxcuarBi1rdpVw1C4VSCstuVM2qTYzcmj1yT7zo9/sj34V5kML8X3a1KtSsKhUUQkpNm9S3Kph1wlPpS96IxyV4V2gOFbO/KjS29RrFXWWHlDoN7NugmegEQ19CXBKGFmXtyk4WZhMPL4s1mjTHFTcgTtFuGgDMFgAzmEuumpoj3S2W96W7AZzZxzaC5VrK6XBmK1cHcuQsurzK8UoWEHoy96FhYTdT20UUZo+0Devm6f9YRmAwWjT/NJ2gL183fbCMs2a2iLOwMOmE5z+VFVWPDyTmuX7lOeX3DFxizfbWkLJHOksvzyqK/8PU8s+h/Eh7Zi8V17kxEaMLUc8mptmK7inmhs08qDpqhs8GmYfAiHbHxbv2wqc9pVoXKrS29QPeOQSRR1lwaOmYa45aLmOq5bnHZtZL+wOxT/Jcpu4A1B9HY2y2vtVtcTaYzzC4KsMWqA2H1S28kaVQbpbNZph/npy/rD345uWypMr1wuvDBedU5BU3gqPaepItMy3iYcYe4BgHxogqzsTb1sMHSdo3A4p4PJhNwApgYFMPLCOG2rb5jUQrFOfLM3oLb4HDc9yYH4mjoRzfUz/QnFoWpmzB3VOpeG6SAkZdfLlPvJ61qwsaLdqBUup0C6ZrgeiU0d8smfkGHThnXpSbyoK7dleTOqNF1kpuUW79Q5nol3+g4kFPNdjezZi0G8gX0cG9WYImXx9ERdfN/3XfdSGr3bXXFTcgM0QJSQS0BPsgAB0I6MFqdRz34BoXPmuwhD3Jn/Mmm/Wi1mPE9uS/DOXgNh9VWiozxbWcaYYW4d2ZiTl5sOiXvRUqozTpbRwmiZEEpKST06+AvAU+73Y0YokDCpHkFun5KTheOVwrIjhvCpLw2l+jy1p/sZn9AEViubm/tBqFt7lPD6qcW3pzFpSmDamMkZJN2iQN0U5d4AcIyl+6UNpzhbYbu+0cTXNvIncrYJgk/mqesB0aNmnVo6HN2IrmDcBhvOuV3xIf3Ky51tKUy0FNwhL9qq/mwY27aOIFvu7Pts6dW9Z1RZ2kjoRcKa+Dyz1h/PEt2lmvwzqdoHX7req0FdkKYPElUTMJqn3EjDbr/AHRMTqk0x+si7eU5qss0LUgqqiJEl7oT72IHuDKnTruxTldFx1Ri5s8FFXAW6k1NH8uYc5mYHx8XP2Oxw9iJbUyFMjizM06Q3vnyrnvQJ0GQUiDlLPu/coDxhvi4c4uIVPsfCekkdKoaS7lM3Z7nedDWsfUDoAHlzibrxoN9W1kxrdHxKqc6hcCFOnNyZHr3YzXHQGrv9AaeeHhhtlesDCzECoYgW+q/FZ0Jg1Z76cm7MD6YBKXOXwtsShc9u0S8KG7tu4mAvqdUE5puECMhkoE+5w88SJt6jUOOzEH3LVRLvaf1JaINjkZZEiWXvncw93uqJ8mX6vNt+W6+sqRE+eGX6ScLNv8Auj+mQi1dn2db1jUFvblqUwKfTW0z3KAERSDWeo+lz9me2OTeeFVgYh1GmVS77eSfu6QWtiqShjNGesD5tBS7oB6Ihx7o0xcznlTVrm8SnyLS69awhDXWDL4U9Q6Mvej9QkpbJSlCxw0wU9lFFGaTtA3pL9nfbGMq41WzNtnTzAe8GzJqs5WUYbASSHUZz1j2JRmD6jLx9iNY+b1vuRZ2BnW24p9JXf8AKiqfHrTjk1ro6bq8jZzojps3KiyoIIazMz0AAd+cef1GXj7Eax83rfcj6o2leKKoGdnVhYAPjDkawa/5IdHDY40lNg+G4umsbtmQA+QWRMw16Fg0R8jfw+Dv/E1YnDpTDx4Dt0sCyywMFuMwDQHeeIHQ+3HxO9sSjQNNDD2pIuDR3HKAbOeDgMN+HBwLcZ6z7/giB0teHxLq9EHF4Uwln8cxy81x2Kxb16VWqPaqpZ1VR5a5N1ukmC2gNZ69AcEeL1GXj7Eax83rfcieDrXaua8D/AuPGsOXTtGWL5kbfRyjLb1GXj7Eax83rfcjUvL61cM8ErLbOkFEVU6OgBgqOkx4JdyEvHbjbkRro67ydcBNONS3ekpuqSIIIIrFWtRJsl4INkvBCwQIRCbJeCFggQiE2S8ELBAhEEEECEk5SnzTlCaA6g+iCCBYrSlUaA6kvRBoDqD6IIIzpqjLTsRoDqD6INAdQfRBBBpqsZadiNAdWXog0B1B9EEEGmqzlp2I0B1B9ELKUpc0pQQRhFKUp+iWCCCBZRBBBAhEEEECEQQQQIRBBBAhf//Z';
	        doc.addImage(imgData, 'JPEG', 3, 3, 40, 20);
	        doc.text("Automation Report", 150, 12);

	       
	       
	     doc.autoTable({html: '#moduleListTable',margin: {top: 240}});
	
	       
	       
	       
	       
       
	        doc.printingHeaderRow = true;
	        var size=   20 ;
	        
        	var count=0; 
        	var count1=-1; 
        	var headers=["Testset Name","User Name","Role","Export Date"];
	        $.each(table2, function (i, row){

        		count1= count1 + 1;
	        	$.each(row, function (j, cellContent) {

	        		//console.log("count1: "+count1);

	                    doc.margins = 1;
	                    doc.setFont("helvetica");
	                   // doc.setFontType("bold");
	                    doc.setFontSize(8);

							doc.setFillColor(232,232,232);
							doc.setTextColor(34, 115, 220) ;
							doc.cell(25, 45,cellWidth , headerRowHeight, headers[count1], i);
							doc.setFillColor(255, 255, 255);
							doc.setTextColor( 0, 0,0) ;
							doc.cell(25, 45,cellWidth , headerRowHeight, cellContent, i);
							doc.setTextColor( 0, 0,0) ;
						
	                    
	        	});
	        });
	        
	       // doc.text("Bar Chart", 25, 127);
	       
	       
	        
	        Canvas = document.getElementById("projectBarChart");
	        Context = Canvas.getContext("2d");
	        var imgData = Canvas.toDataURL('image/png');
	        doc.addImage(imgData, 'png', 25, 125, 160, 80);
	        
	        
	        
	        doc.addPage();
	        
	       
	        
	        doc.cellInitialize();
	        
	        
	        
	        
	        $.each(table1, function (i, row){


	        	rowCount1=rowCount1 + 1; 
	        	var count=0; 
	        	
	        	$.each(row, function (j, cellContent) {
	        		count=count + 1;
	                if (rowCount1 ==0) {
	                    doc.margins = 1;
	                    doc.setFont("helvetica");
	                    //doc.setFontType("bold");
	                    doc.setFontSize(8);
	                    
						if(count==1||count==6 ||count==9|| count==10){ 
							if(count==6){
								cellWidth=70;
							}
							else if(count==9){
								cellWidth=30;
							}else{
								cellWidth=50;
							}		
						doc.setFillColor(3, 44, 60);
						doc.setTextColor( 250, 252, 254) ;
						doc.cell(leftMargin, topMargin,cellWidth , headerRowHeight, j.toUpperCase(), i-1);
						doc.setTextColor( 0, 0,0) ;
						}
                }
	        	});
	        });
	        var PageSize= 25;     
	       $.each(table1, function (i, row)
	        {

	            rowCount++;
				var count=0;
				PageSize=PageSize + 15; 
				size= size + rowHeight ;
	            if(PageSize>=280 ){
	    	         size= 20  ;
	    	         PageSize= 25; 
	    	          doc.cellAddPage();	            	
	            }

	            $.each(row, function (j, cellContent) {
		            	count=count + 1 ;
		            	if (rowCount ==0) {

									if(count==1 || count==6 || count==9 ){
							  if(count==6){
									cellWidth=70;
								}
								else if(count==9){
									cellWidth=30;
								}else{
									cellWidth=50;
								}
								doc.margins = 1; 
			                    doc.setFont("Calibri ");
			                   // doc.setFontType("italic");  // or for normal font type use ------ doc.setFontType("normal");
			                    doc.setFontSize(8);
			                    if(cellContent==""|| cellContent==null){
			                    	cellContent="-";
			                    }
		                		  if(rowCount % 2==0){
		                			  doc.printingHeaderRow = true;
		                			  doc.setFillColor(232,232,232);
		                		  }else{
		                			  doc.printingHeaderRow = false;
		                		  }
		                		doc.setTextColor( 0, 0, 0) ;
								doc.cell(leftMargin, topMargin, cellWidth, rowHeight, doc.splitTextToSize(cellContent,cellWidth-5), i);
								
							}
							
							if(count==12){
			                    doc.margins = 1;
			                    doc.setFont("Calibri ");
			                   // doc.setFontType("italic");  // or for normal font type use ------ doc.setFontType("normal");
			                    doc.setFontSize(8);
		                		var specialElementHandlers = {
		                			    '#editor': function (element, renderer) {
		                			        return true;
		                			    }
		                			};
		                		    doc.fromHTML($('#dummydata').html(), 21, size, {
		                		        'width': 180,	                		    
		                	            'elementHandlers': specialElementHandlers
		                		    });
		                		   
		                		  if(rowCount % 2==0){
		                			  doc.printingHeaderRow = true;
		                			  doc.setFillColor(232,232,232);
		                		  }else{
		                			  doc.printingHeaderRow = false;
		                		  }
		                		 
		                		  cellWidth=50;
		                		  doc.cell(leftMargin, topMargin, cellWidth, rowHeight, " ", i); 
		                		  doc.setTextColor( 0, 0,255) ;
		                		 // doc.textWithLink("Click here "+ PageSize ,170, size, { url:"data:image/png;base64,"+ cellContent });;
		                		  doc.textWithLink("View " ,170, size, { url:"data:image/png;base64,"+ cellContent });;
		                		  doc.setTextColor( 0, 0,0) ;
		                		  //doc.cell(leftMargin, topMargin,cellWidth , headerRowHeight, doc.textWithLink("Click here "+ size ,180, size, { url:"data:image/png;base64,"+ cellContent },'left'), 1);
		                   
							}
	                }  
	                else {
	                    doc.margins = 1;
	                    doc.setFont("Calibri ");
	                    //doc.setFontType("italic");  // or for normal font type use ------ doc.setFontType("normal");
	                    doc.setFontSize(8); 
	                    if(count==1 || count==6 || count==9 ){
							
	                    	if(count==6){
								cellWidth=70;
							}
							else if(count==9){
								cellWidth=30;
							}else{
								cellWidth=50;
							}
		                    if(cellContent==""|| cellContent==null){
		                    	cellContent="-";   	
		                    	
		                    }
	                		  if(rowCount % 2==0){
	                			  doc.printingHeaderRow = true;
	                			  doc.setFillColor(232,232,232);
	                		  }else{
	                			  doc.printingHeaderRow = false;
	                		  }
	                    	doc.cell(leftMargin, topMargin  , cellWidth, rowHeight, doc.splitTextToSize(cellContent,cellWidth-3), i);  // 1st=left margin    2nd parameter=top margin,     3rd=row cell width      4th=Row height
	                    }
	                    if(count==12) { 
	                    	cellWidth=50;
		                    doc.margins = 1;
		                    doc.setFont("Calibri ");
		                    //doc.setFontType("italic");  // or for normal font type use ------ doc.setFontType("normal");
		                    doc.setFontSize(8);
	                		var specialElementHandlers = {
	                			    '#editor': function (element, renderer) {
	                			        return true;
	                			    }
	                			};
	                		    doc.fromHTML($('#dummydata').html(), 21, size, {
	                		        'width': 50,	                		    
	                	            'elementHandlers': specialElementHandlers
	                		    }); 
		                		  if(rowCount % 2==0){
		                			  doc.printingHeaderRow = true;
		                			  doc.setFillColor(232,232,232);
		                		  }else{
		                			  doc.printingHeaderRow = false;
		                		  }
							  doc.cell(leftMargin, topMargin, cellWidth, rowHeight, " ", i);
							  doc.setTextColor( 0, 0, 255) ; 
							  var w=doc.textWithLink("View " ,170, size, { url:"data:image/png;base64,"+ cellContent });
							  doc.setTextColor( 0, 0, 0) ; 
	                		  //doc.cell(leftMargin, topMargin,cellWidth , headerRowHeight, doc.textWithLink("Click here "+ size ,180, size, { url:"data:image/png;base64,"+ cellContent },'left'), 1);
	                    }
	                   }
			            

	            }); 


	        });
	       doc.setTextColor( 0, 0, 0) ;
	       doc.setTextColor(232,232,232) ;
 		  doc.text("Copyright@2020 Jade Global Inc. All rights reserved.", 67, 290);
 		  
         // Before adding new content
	    //doc.save('sample Report.pdf');	
 		 doc.save('${Model.selectedExecuteBuildName}_<%= (new java.util.Date()).toLocaleString()%>.pdf');
		});
		
	});
	
	function showModuleWiseReport(projectID){
		
		$('.commonClass').each(function(){
			$(this).addClass('hidden');
		});
		$('#projectDiv').addClass('hidden');
		$('#moduleDiv_'+projectID).removeClass('hidden');
		
		var moduleDetails = $('#moduleDetailsTable_'+projectID).DataTable();
		
		var labelArray = [];
		var passArray = [];
		var failArray = [];
		var skippArray = [];
		var blockArray = [];
		var notRunArray = [];
		
		var allTableRow = moduleDetails.$(".allModuleTableRow", {
			"page" : "all"
		});
		
		allTableRow.each(function() {
			labelArray.push($(this).find("td:eq(0)").html());
			passArray.push($(this).find("td:eq(3)").html());
			failArray.push($(this).find("td:eq(4)").html());
			skippArray.push($(this).find("td:eq(5)").html());
			blockArray.push($(this).find("td:eq(6)").html());
			notRunArray.push($(this).find("td:eq(7)").html());
		});

		var barData = {
			labels : labelArray,
			datasets : [ {
				label : "Passed",
				backgroundColor : colorPass,
				data : passArray
			}, {
				label : "Failed",
				backgroundColor : colorFail,
				data : failArray
			}, {
				label : "Skipped",
				backgroundColor : colorSkip,
				data : skippArray
			}, {
				label : "Blocked",
				backgroundColor : colorBlock,
				data : blockArray
			}, {
				label : "Not Run",
				backgroundColor : colorNotRun,
				data : notRunArray
			} ]
		};

		var barOptions = {
			scaleBeginAtZero : true,
			scaleShowGridLines : true,
			scaleGridLineColor : "rgba(0,0,0,.05)",
			scaleGridLineWidth : 1,
			barShowStroke : true,
			barStrokeWidth : 2,
			barValueSpacing : 5,
			barDatasetSpacing : 1,
			responsive : true,
			scales: {
	            yAxes: [{
	                    ticks: {
	                        min: 0,
	                        callback: function(value) {if (value % 1 === 0) {return value;}}
	                    }
	            }]
	        }
		}
		var ctx = document.getElementById("moduleBarChart_"+projectID);
		
	    var myNewChart1 = new Chart(ctx, {
		    type: 'bar',
		    data: barData,
		    options: barOptions
		});

	}
	

	//creating the Testset wise reports
	function showScenarioWiseReport(moduleID,projectID){
		
		$('.commonClass').each(function(){
			$(this).addClass('hidden');
		});
		$('#projectDiv').addClass('hidden');
		$('#scenarioDiv_'+moduleID).removeClass('hidden');
		
		var scenarioDetails = $('#scenarioDetailsTable_'+moduleID).DataTable();
		
		var labelArray = [];
		var passArray = [];
		var failArray = [];
		var skippArray = [];
		var blockArray = [];
		var notRunArray = [];
		
		var allTableRow = scenarioDetails.$(".allscenarioTableRow", {
			"page" : "all"
		});
		
		allTableRow.each(function() {
			labelArray.push($(this).find("td:eq(0)").html());
			passArray.push($(this).find("td:eq(3)").html());
			failArray.push($(this).find("td:eq(4)").html());
			skippArray.push($(this).find("td:eq(5)").html());
			blockArray.push($(this).find("td:eq(6)").html());
			notRunArray.push($(this).find("td:eq(7)").html());
		});

		var barData = {
			labels : labelArray,
			datasets : [ {
				label : "Passed",
				backgroundColor : colorPass,
				data : passArray
			}, {
				label : "Failed",
				backgroundColor : colorFail,
				data : failArray
			}, {
				label : "Skipped",
				backgroundColor : colorSkip,
				data : skippArray
			}, {
				label : "Blocked",
				backgroundColor : colorBlock,
				data : blockArray
			}, {
				label : "Not Run",
				backgroundColor : colorNotRun,
				data : notRunArray
			} ]
		};

		var barOptions = {
			scaleBeginAtZero : true,
			scaleShowGridLines : true,
			scaleGridLineColor : "rgba(0,0,0,.05)",
			scaleGridLineWidth : 1,
			barShowStroke : true,
			barStrokeWidth : 2,
			barValueSpacing : 5,
			barDatasetSpacing : 1,
			responsive : true,
			scales: {
	            yAxes: [{
	                    ticks: {
	                        min: 0,
	                        callback: function(value) {if (value % 1 === 0) {return value;}}
	                    }
	            }]
	        }
		}
		var ctx = document.getElementById("scenarioBarChart_"+moduleID);
		 var myNewChart1 = new Chart(ctx, {
			    type: 'bar',
			    data: barData,
			    options: barOptions
			});

	}
	
	
	
	
	
	//test case wise  report
	function showTCWiseReport(scenarioID,moduleID){
		
		$('.commonClass').each(function(){
			$(this).addClass('hidden');
		});
		$('#projectDiv').addClass('hidden');
		$('#testCaseDiv_'+scenarioID).removeClass('hidden');
		
		var testCaseDetails = $('#testCaseDetailsTable_'+scenarioID).DataTable();

		var allTableRow = testCaseDetails.$(".allTestCaseTableRow", {
			"page" : "all"
		});
		
		var fail=0;
		var pass=0;
		var skipp=0;
		var block=0;
		var notRun=0;
		
		allTableRow.each(function() {
			var status = $.trim($(this).find("td:eq(4)").html().toLowerCase());
			if(status == "not run"){
				notRun++;
			}
			else if(status == "pass"){
				pass++;
			}
			else if(status == "skip"){
				skipp++;
			}
			else if(status == "fail"){
				fail++;
			}
			else if(status == "block"){
				block++;
			}
		});
		
		
		var doughnutData = {
			    labels: ["Pass","Fail","Skip","Block","Not Run"],
			    datasets: [
			        {
			            data: [pass,fail,skipp,block,notRun],
			            backgroundColor: [
			                colorPass,
			                colorFail,
			                colorSkip,
			                colorBlock,
			                colorNotRun
			            ],
			            hoverBackgroundColor: [
							colorPass,
							colorFail,
							colorSkip,
							colorBlock,
							colorNotRun
			            ]
			        }]
			};
		
		var doughnutOptions = {
			segmentShowStroke : true,
			segmentStrokeColor : "#fff",
			segmentStrokeWidth : 2,
			percentageInnerCutout : 45, // This is 0 for Pie charts
			animationSteps : 100,
			animationEasing : "easeOutBounce",
			animateRotate : true,
			animateScale : true,
			responsive : true,
		};
		 	    var ctx = document.getElementById("doughnutChart_"+scenarioID)	;
		 	   var myDoughnutChart = new Chart(ctx, {
				    type: 'doughnut',
				    data: doughnutData,
				    options: doughnutOptions
				});

	}
	
	function showTestStep(testCaseID,excelStatus){
		var selectedProject='',selectedModule;
		 $('.nav-label').each(function() {
				var parentLi = $(this).parents('.mainLi');
				if(parentLi.hasClass('active')){
					selectedProject = $(this).text().trim();
					
				}
		}); 
		$('.moduleNav').each(function(){
		var parentLi = $(this).parents('.moduleList');
		if(parentLi.hasClass('active')){
			 selectedModule=$(this).attr('data-original-title').trim();
			
		}
	
		});
		 if(excelStatus==1){
			 $('body').addClass("white-bg");
			 $("#barInMenu").removeClass("hidden");
			 $("#wrapper").addClass("hidden");
			var posting = $.post('settcid',{
				testcaseId : testCaseID,
				selectedProject:selectedProject,
				selectedModule:selectedModule
				
			});
			posting.done(function(data){
				window.location.href="viewdataset";
			});
		 }
		 else if(excelStatus==2){
			 $('.commonClass').each(function(){
					$(this).addClass('hidden');
				});
				 $('#projectDiv').addClass('hidden');
				
				 $('#testStepDiv_'+testCaseID).removeClass('hidden'); 
		 }
			 
		 
	} 
	
	function getScreenshot(exe_id,title)
	{
		var posting = $.post('getbuildscreenshot', {
			 attchmentId : exe_id,
			});
			 posting.done(function(data) {
			
				 var a = window.document.createElement('a');
				 link =  "data:image/png;base64,"+data;
				 a.href=link;
				 a.download = title+".PNG";
				 document.body.appendChild(a)
				 a.click();
				 document.body.removeChild(a)
			});
	}
	function viewScreenshot(exe_id,title)
	{
		var posting = $.post('getbuildscreenshot', {
			attchmentId : exe_id,
			});
			 posting.done(function(data) {
				/*  var a = window.document.createElement('a');
				 a.href= "data:image/png;base64,"+data;
				 a.target ="_blank";
				 document.body.appendChild(a)
				 a.click();
				 document.body.removeChild(a) */
				  
				 $("#dialog").dialog();
					if(${sessionScope.automationExecBuildType==2}){ 
						 $("#frame").attr("src", "data:image/png;base64,"+data); 
					 }else if(${sessionScope.automationExecBuildType==5}){
					        var imgdata=data;
						$("#frame").attr("src", "data:image/png;base64,"+imgdata);       
					 }else{  
			         	$("#frame").attr("src", "data:text/plain;base64,"+data); 
					 }  
			});
	}
	 function getAlltestCase() {
		  document.getElementById("testStepsReport").style.display = "none";
		  document.getElementById("allTestCase").style.display = "block";

		}  
	
		
		 function getTestSteps() {
		  document.getElementById("allTestCase").style.display = "none";
		  document.getElementById("testStepsReport").style.display = "block";

		} 
	$(function(){
			$('.testStepTable').DataTable();
		});
	function printFunction() {
		window.print();
		}
</script>
<style type="text/css">
.lgMenuTextAlignment {
    width: 150px !important;
/*     display: inline-block !important;
    overflow: hidden !important;
    white-space: nowrap !important;
    text-overflow: ellipsis !important; */
}
td.details-control {
    background: url('resources/img/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('resources/img/details_close.png') no-repeat center center;
}
.ui-dialog{
width:80vw !important;
padding:0;
}
.ui-widget-header{
background: #1c84c6 !important;
border-radius:0;
margin:0;
}
</style>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Jenkins Reports</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li><a href="viewjenkinsurls">Jenkins</a></li>
				<li><a href="jenkinsreports">View Jobs</a></li>
				<li class="Active"><strong>View Reports</strong></li>
			</ol>
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
	
	<div class="row" id="chartsDiv">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-title">
						 <h5>Build Analysis</h5> 
					</div>
					<div class="ibox-content"  style="height: 500px;">
						<div class="col-md-8">
							<div id="buildBarChart" class="chart"  style="height: 425px;"></div>
						</div>
						<div class="col-md-4">
							<div id="buildPieChart" class="chart"  style="height: 400px;"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-title">
						 <h5>Details</h5> 
					</div>
					<div class="ibox-content">
						<table class="table table-stripped" id="table">
							<thead>
								<tr>
									<th>Status</th>
									<th>Build No.</th>
									<th>Build Name</th>
									<th>Created Date</th>
									<th>Total Count</th>
									<th>Fail Count</th>
									<th>Skip Count</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="data" items="${Model.buildData}">
                                      <tr>

										<td><c:choose>
												<c:when test="${data.result==1}">
													<span class="label label-primary">SUCCESS</span>
												</c:when>
												<c:when test="${data.result==2}">
													<span class="label label-warning">UNSTABLE</span>
												</c:when>
												<c:when test="${data.result==3}">
													<span class="label label-danger">FAILURE</span>
												</c:when>
												<c:when test="${data.result==4}">
													<span class="label label-default">NOT BUILT</span>
												</c:when>
												<c:otherwise>
													<span class="label label-default">IN PROGRESS</span>
												</c:otherwise>
											</c:choose></td>
										<td style="text-transform: capitalize;">${data.build_id}</td>
                                             <td style="text-transform: capitalize;">${data.build_name}</td>
                                             <td>${data.timestamp}</td>
                                              <td>${data.total_count}</td>
											  <td>${data.fail_count}</td>
											   <td>${data.skip_count}</td>
											  <td> 
                                          		 <button type="button" onclick="window.open('${data.url}', '_blank')"  class="btn btn-warning btn-xs">View</button></td>
                                              </tr>
                                </c:forEach>
								
							</tbody>

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script>
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");

		$(".chosen-select").chosen().trigger('chosen:updated');

	});

	var stableCount = 0;
	var unstableCount = 0;
	var failureCount = 0;
	google.load("visualization", "1", {
		packages : [ "corechart" ]
	});
	google.setOnLoadCallback(drawBarchart);
	function drawBarchart() {

		var barChartData = [ [ 'Build', 'Total', 'Fail', 'Skip' ] ];
		var graphData = ${Model.buildDataJson};

		var dataLength = 0;
		dataLength = graphData.length;

		if (dataLength == 0) {
			$("#chartsDiv").addClass("hidden");
		} else if (graphData.length >= 10)
			dataLength = 10;
		else
			dataLength = graphData.length;
		for (i = 0; i < dataLength; i++) {
			var barChartCounts = [];
			barChartCounts.push(" " + graphData[i].build_name);
			barChartCounts.push(graphData[i].total_count);
			barChartCounts.push(graphData[i].fail_count);
			barChartCounts.push(graphData[i].skip_count);
			if ((graphData[i].result).localeCompare('1') == 0) {
				stableCount++;
			} else if ((graphData[i].result).localeCompare('2') == 0) {
				unstableCount++;
			} else if ((graphData[i].result).localeCompare('3') == 0) {
				failureCount++;
			}
			barChartData.push(barChartCounts);
		}

		var data = google.visualization.arrayToDataTable(barChartData);

		var options = {
			legend : {
				position : 'top'
			},
			title : 'Last 10 Build Analysis',
			vAxis : {
				maxValue : 10,
				format : '0'
			}
		/* 	hAxis : {
				title : 'Builds',
				titleTextStyle : {
					'color' : 'black',
					'font-weight': 'bold'
				}
			} */
		};

		var barChart = new google.visualization.ColumnChart(document
				.getElementById('buildBarChart'));
		barChart.draw(data, options);

		/* 		google.visualization.events.addListener(barChart, 'select', selectHandler);

		function selectHandler() {
			
		var selection = barChart.getSelection();
		
		console.log(barChart);
		if(selection.length){
			var message = '';
			for (var i = 0; i < selection.length; i++) {
			var item = selection[i];
			if (item.row != null && item.column != null) {
			var str = data.getFormattedValue(item.row, item.column);
			var category = data
			.getValue(barChart.getSelection()[0].row, 0)
		
			} 
			}
			//alert(data.getValue(barChart.getSelection()[0].row, 0));
			window.open("http://192.168.2.113:8080/");
			} 
		} */

	}

	google.load("visualization", "1", {
		packages : [ "corechart" ]
	});
	google.setOnLoadCallback(drawPieChart);
	function drawPieChart() {
		var data = google.visualization.arrayToDataTable([
				[ 'Result', 'Test case' ], [ 'Stable', stableCount ],
				[ 'Failure', failureCount ], [ 'Unstable', unstableCount ]

		]);

		var options = {
			title : 'Build Results'
		}

		var chart = new google.visualization.PieChart(document
				.getElementById('buildPieChart'));
		chart.draw(data, options);
	}

	$(window).resize(function() {
		drawBarchart();
		drawPieChart();
	});

	$(function() {
		//to apply choosen
		$(".chosen-select").chosen();
		//collpsing filter Ibox
		//$("#filterIbox").addClass('collapsed');
		dt = $('#table').DataTable(
				{
					dom : '<"html5buttons"B>lTfgitp',
					buttons : [
							{
								extend : 'csv',
								title : 'Build History'
							},
							{
								extend : 'excel',
								title : 'Build History'
							},
							{
								extend : 'pdf',
								title : 'Build History'
							},

							{
								extend : 'print',
								customize : function(win) {
									$(win.document.body).addClass('white-bg');
									$(win.document.body).css('font-size',
											'10px');

									$(win.document.body).find('table')
											.addClass('compact').css(
													'font-size', 'inherit');
								}
							} ],
					"aaSorting" : [ [ 1, "desc" ] ]

				});
	});
</script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
<div class="bar" id="bar">
		<p>loading Reports</p>
	</div>
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Jenkins Jobs</h2>
			<ol class="breadcrumb">
				<li><a href="admindashboard">Dashboard</a></li>
				<li><a href="viewjenkinsurls">Jenkins</a></li>
				<li class="Active"><strong>View Jobs</strong></li>
			</ol>
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>Details</h5>
						<div class="ibox-tools">
						<c:if test="${Model.isAdminUser == 'true'}">
							<button type="button" class="btn btn-success btn-xs"
								onclick="location.href = 'createjenkinsjob';">Add New
								Job</button>
						</c:if>
						</div>
					</div>
					<div class="ibox-content">
						<table class="table table-stripped" id="table">
							<thead>
								<tr>
									<th>Status</th>
									<th>Job Name</th>
									<th>Instance</th>
									<th>Last Success</th>
									<th>Last Failure</th>
									<th>Last Duration</th>
									<th>Built On</th>
									<th>Action</th>

								</tr>
							</thead>
							<tbody>
								<c:forEach var="data" items="${Model.JobData}">
									<tr>
										<td><c:choose>
												<c:when test="${data.statusId==1}">
													<span class="label label-primary">STABLE</span>
												</c:when>
												<c:when test="${data.statusId==2}">
													<span class="label label-warning">UNSTABLE</span>
												</c:when>
												<c:when test="${data.statusId==3}">
													<span class="label label-danger">FAILURE</span>
												</c:when>
												<c:when test="${data.statusId==4}">
													<span class="label label-default">NOT BUILT</span>
												</c:when>
												<c:otherwise>
													<span class="label label-default">IN PROGRESS</span>
												</c:otherwise>
											</c:choose></td>
										<td style="text-transform: capitalize;">${data.job_name}</td>
										<td style="text-transform: capitalize;">${data.jenkin_name}</td>
										<td>1 day 16 hr</td>
										<td>N/A</td>
										<td>31 Sec</td>
										<td>Jenkins</td>
										<form id="settingForm" action="jenkinsJobSettings"
											method="GET">
											<td><%-- <c:if test="${data.isExecuteEnabled == 1}">
													<button type="button" class="btn btn-info btn-xs"
														onclick="buildNow(${data.jenkin_jobId})">Build
														Now</button>
												</c:if> --%>
												<button type="button"
													onclick="buildHistory(${data.jenkin_jobId},'${data.jenkin_job}','${data.build_trigger_token}')"
													class="btn btn-warning btn-xs" id="view_report">View Reports</button> <input
												type="text" name="jobId" value="${data.jenkin_jobId}"
												hidden="true"> <c:if
													test="${data.isUpdateEnabled == 1}">
													<button class="btn btn-white btn-sm"
														onclick="jenkinsJobSettings();">Settings</button>
												</c:if></td>
										</form>
									</tr>
								</c:forEach>
							</tbody>

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var dt;
	$(window).load(function() {
		$("#bar").fadeOut();
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
		
		$(".chosen-select").chosen().trigger('chosen:updated');
		

	});

	function jenkinsJobSettings(jobId) {
		$("#settingForm").submit();		
	}	
	
	function buildNow(jenkins_JobId){
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");
		var posting = $.post('triggerbuild',{
			JenkinsJobId : jenkins_JobId
		
		});
		posting.done(function(data){
			var message="";
			var textData="";
			var alertType="";

		      if(data==401){
                  message="Unauthorized";
                  textData="Build trigger failed";
                  alertType="error";
            }else if(data==201){
                  
                  message="Success";
                  textData="Build triggerred successfully";
                  alertType="success";
            }else if(data==500){
                  message="Failure";
                  textData="Build trigger failed,Something went wrong";
                  alertType="error";
            }else if(data==404){
                  message="Unauthorized";
                  textData="Build trigger failed";
                  alertType="error";
            }
            else{
                  message="Unauthorized";
                  textData="Build trigger failed";
                  alertType="error";
            }

			swal({
                title: message,
                text: textData,
                type: alertType,
                closeOnConfirm : true},
			function (isConfirm) { window.location.href="jenkinsreports"; }
			
            );
		  });			
		}
	
	function buildHistory(id, jobName,apiToken){
		
		var posting = $.post('setviewjobid',{
			jobId : id
		});
		posting.done(function(data){
			
			$("#bar").fadeIn();
			
			fetchReport(jobName,apiToken);
			
		});
	}
	function fetchReport(jobName,apiToken){
		var posting = $.post('jenkinfetchreportaction',{
			apiToken : apiToken,
			jobName : jobName
		});
		posting.done(function(data){
			window.location.href="buildhistory";
		  });
		
		}
</script>
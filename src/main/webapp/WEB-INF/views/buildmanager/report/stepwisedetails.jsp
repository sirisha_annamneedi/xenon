<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Test case step wise details</h5>
					<div class="ibox-tools">
						<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
						</a> <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i
							class="fa fa-wrench"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover dataTable"
							id="stepWiseDetailsTable">
							<thead>
								<tr>
									<th >Step Id</th>
									<th>Step Details</th>
									<th>Step Value</th>
									<th class="hidden">Step value</th>
									<th>Step status</th>
									<th>Screenshot</th>
									<th class="text-right">Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="executeBuild" items="${Model.ExecuteBuild}">
									<tr style="cursor: pointer;">
										<td>${executeBuild.step_id}</td>
										<td>${executeBuild.step_details}</td>
										<td>${executeBuild.step_input_type}</td>
										<td class="hidden">${executeBuild.step_value}</td>
										<td>${executeBuild.step_status}</td>
										<td>${executeBuild.screenshot_title}</td>
										<td class="text-right">
											<div class="btn-group">
												<button id="view_${executeBuild.exe_id}"
													class="btn-white btn btn-xs"
													onclick="viewScreenshot(${executeBuild.exe_id},'${executeBuild.screenshot_title}');"
													style="margin-right: 10px;">View</button>
												<button class="btn btn-white btn-xs"
													onclick="getScreenshot(${executeBuild.exe_id},'${executeBuild.screenshot_title}');">Download</button>
											</div>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Page-Level Scripts -->
<script>
	$(document).ready(
			function() {
				$('#stepWiseDetailsTable').DataTable(
						{
							dom : '<"html5buttons"B>lTfgitp',
							buttons : [
									{
										extend : 'copy',
										title : 'Step Wise Details',
										exportOptions: {
						                    columns: ':visible'
						                }
									},
									{
										extend : 'csv',
										title : 'Step Wise Details',
										exportOptions: {
						                    columns: ':visible'
						                }
									},
									{
										extend : 'excel',
										title : 'Step Wise Details',
										exportOptions: {
						                    columns: ':visible'
						                }
									},
									{
										extend : 'pdf',
										title : 'Step Wise Details',
										exportOptions: {
						                    columns: ':visible'
						                }
									},

									{
										extend : 'print',
										exportOptions: {
						                    columns: ':visible'
						                },
										customize : function(win) {
											$(win.document.body).addClass(
													'white-bg');
											$(win.document.body).css(
													'font-size', '10px');

											$(win.document.body).find('table')
													.addClass('compact').css(
															'font-size',
															'inherit');
										}
									} ],
										"aoColumnDefs" : [ {
											'bSortable' : false,
											'aTargets' : [ 6 ]
										} ]

						});
			});
	function getScreenshot(exe_id,title)
	{
		var posting = $.post('getbuildscreenshot', {
			exe_id : exe_id,
			});
			 posting.done(function(data) {
				 var a = window.document.createElement('a');
				 //a.href = window.URL.createObjectURL(new Blob([byteArray], { type: 'application/octet-stream' }));
				 a.href= "data:image/png;base64,"+data;
				 a.download = title;
				 // Append anchor to body.
				 document.body.appendChild(a)
				 a.click();
				 document.body.removeChild(a)
			});
	}
	function viewScreenshot(exe_id,title)
	{
		var posting = $.post('getbuildscreenshot', {
			exe_id : exe_id,
			});
			 posting.done(function(data) {
				 var a = window.document.createElement('a');
				 a.href= "data:image/png;base64,"+data;
				 a.target ="_blank";
				 document.body.appendChild(a)
				 a.click();
				 document.body.removeChild(a)
			});
	}
	</script>
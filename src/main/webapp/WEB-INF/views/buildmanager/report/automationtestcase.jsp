
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Automation Summary</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li><a href="builddetails?manual=1&auto=1&sc=1&buildType=automation">Build</a></li>
			<li class="active"><strong>Automation Summary</strong></li>
		</ol>
	</div>
</div>
	<div class="wrapper wrapper-content animated fadeInRight row">
	
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Test case Table</h5>
				</div>
				<div class="ibox-content">
					<table class="table table-stripped table-hover" id="table">
						<thead>
							<tr>
								<th>Test Case Name</th>
								<th>Summary</th>
								<th>Start time</th>
								<th>End time</th>
								<th>Status</th>
							</tr>

						</thead>
						<tbody>
							<c:forEach var="testCaseList" items="${Model.testCaseList}">
								<c:if test="${Model.filterText == testCaseList.description}">
									<tr style="cursor: pointer" onclick ="dashLink(${testCaseList.testcase_id})">
										<td class="textAlignment"
											data-original-title="${testCaseList.testcase_name}"
											data-container="body" data-toggle="tooltip"
											data-placement="right" class="issue-info">${testCaseList.testcase_name}</td>
										<td>${testCaseList.summary}</td>
										<td>${testCaseList.tcStartTime}</td>
										<td>${testCaseList.tcEndTime}</td>
										<td>${testCaseList.description}</td>
									</tr>
								</c:if>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>

$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});

function dashLink(link) {
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setTestIdParameter', {
		filterText : link
		
	});

	posting.done(function(data) {
		window.location.href = "automationtestcasereport";
	});
}
</script>
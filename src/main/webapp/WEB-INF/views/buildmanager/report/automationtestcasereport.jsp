
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Automation Summary</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li><a href="builddetails?manual=1&auto=1&sc=1&buildType=automation">Build</a></li>
			<li class="active"><strong>Automation Summary</strong></li>
		</ol>
	</div>
</div>

<c:forEach var="testCaseList" items="${Model.testCaseList}">
<c:if test="${Model.filterText==testCaseList.testcase_id}">
		<div class="wrapper wrapper-content animated fadeInRight row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Test Case Details</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a>
						</div>
						<!-- end .ibox-tools -->
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-12">
								<form class="form-horizontal">
									<div class="form-group">
										<label class="control-label col-lg-1">Name:</label>
										<div class="col-lg-8">
											<input type="text" class="form-control"
												value="${testCaseList.testcase_name }" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-1">Summary:</label>
										<div class="col-lg-8">
											<textarea name=""
											rows="5" class="form-control"
											style="resize: none;" readonly>${testCaseList.summary } </textarea>
										</div>
									</div>
								</form>
							</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		 <div class="wrapper wrapper-content animated fadeInRight row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Test Steps</h5>
					</div>
					<div class="ibox-content">
						<table class="table table-stripped table-hover testStepTable"
							id="table">
							<thead>
								<tr>
									<th>Step ID</th>
									<th>Step Description</th>
									<th>Test/Execution Data</th>
									<th>Actual Result</th>
									<th>Status</th>
									<th class="text-right">Screenshot</th>
									<th>Bug ID</th>
								</tr>

							</thead>
							<tbody>
								<c:forEach var="testStepsList" items="${Model.testStepsList}">
									<c:if
										test="${testCaseList.testcase_id == testStepsList.testcase_id}">
										<tr>
											<td>${testStepsList.Step_id}</td>
											<td>${testStepsList.step_details}</td>
											<td>${testStepsList.step_value}</td>
											<td>${testStepsList.actual_result}</td>
											<td>${testStepsList.description}</td>
											<td class="text-right">
											<c:if
													test="${not empty testStepsList.screenshot_title}">
													<div class="btn-group">
														<button id="view_${testStepsList.trial_step_id}"
															class="btn-white btn btn-xs"
															onclick="viewScreenshot(${testStepsList.trial_step_id},'${testStepsList.screenshot_title}');"
															style="margin-right: 10px;">View</button>
														<%-- <button class="btn btn-white btn-xs" 
													onclick="getScreenshot(${testStepsList.trial_step_id},'${testStepsList.screenshot_title}');">Download</button> --%>
													</div>


												</c:if> <c:if test="${empty testStepsList.screenshot_title}">
													<div class="btn-group">NA</div>
												</c:if>
												</td>
											<td><c:if test="${testStepsList.is_bug_raised=='1'}">
											   -
											  </c:if> <c:if test="${testStepsList.is_bug_raised=='2'}">
													<c:forEach var="testStepsBugList"
														items="${Model.testStepsBugList}">
														<c:if
															test="${testStepsList.build_id == testStepsBugList.build_id}">
															<c:if
																test="${testCaseList.testcase_id == testStepsBugList.test_case_id}">
																<c:if
																	test="${testStepsList.step_id == testStepsBugList.test_step_id}">
																	<a
																		href="bugsummarybyprefix?btPrefix=${testStepsBugList.bug_prefix}"
																		target="_blank">
																		Bug #${testStepsBugList.bug_prefix}</a>
																</c:if>
															</c:if>
														</c:if>
													</c:forEach>
												</c:if></td>
										</tr>
									</c:if>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div> 
	

	<!-- end #testStepDiv -->
	</c:if>
</c:forEach>
</div>
<script>

$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});
function getScreenshot(exe_id,title)
{
	var posting = $.post('getbuildscreenshot', {
		 attchmentId : exe_id,
		});
		 posting.done(function(data) {
		
			 var a = window.document.createElement('a');
			 link =  "data:image/png;base64,"+data;
			 a.href=link;
			 a.download = title+".PNG";
			 document.body.appendChild(a)
			 a.click();
			 document.body.removeChild(a)
		});
}
function viewScreenshot(exe_id,title)
{
	var posting = $.post('getbuildscreenshot', {
		attchmentId : exe_id,
		});
		 posting.done(function(data) {
			 var a = window.document.createElement('a');
			 a.href= "data:image/png;base64,"+data;
			 a.target ="_blank";
			 document.body.appendChild(a)
			 a.click();
			 document.body.removeChild(a)
		});
}

</script>
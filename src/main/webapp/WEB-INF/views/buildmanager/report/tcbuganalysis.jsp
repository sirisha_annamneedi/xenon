<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Test Case Analysis</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li class="active"><strong>Test Case Analysis</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row m-b-lg">
		<div class="col-lg-9">
			<div class="ibox" id="filterIbox">
				<!-- collapsed -->
				<div class="ibox-title">
					<div class="ibox-tools">
						<a class="collapse-link"> <b><i
								class="fa fa-chevron-right pull-left"></i></b>
							<h5>Filters</h5>
						</a>
					</div>

				</div>
				<div class="ibox-content">
					<form action="" class="form-horizontal" method="GET" id="filterForm">
						<div class="form-group">
							<label class="col-lg-2 control-label">Project:</label>
							<div class="col-lg-2">
								<select data-placeholder="status" class="chosen-select" name="projectState"
									tabindex="1" id="projectStatus" style="width: 100%;">
									<option value="1" class="is">is</option>
									<option value="2" class="isNot">is not</option>
									<option value="3" class="all">all</option>
								</select>
							</div>
							<div class="col-lg-6">
								<select data-placeholder="Choose a Project" id="projectSelect"
									class="chosen-select requiredData" name="project" tabindex="2"
									style="width: 100%;" multiple>
									<c:forEach var="project" items="${Model.projectList}">
										<option value="${project.project_id}">${project.project_name}</option>
									</c:forEach>
								</select>
								<label class="error hidden">This field is required.</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Module:</label>
							<div class="col-lg-2">
								<select data-placeholder="" class="chosen-select" name="moduleState"
									tabindex="3" style="width: 100%;" id="moduleStatusSelect">
									<option value="1">is</option>
									<option value="2">is not</option>
									<option value="3">all</option>
								</select>
							</div>
							<div class="col-lg-6">
								<select data-placeholder="Choose a Module" class="chosen-select requiredData"
									name="module" tabindex="4" style="width: 100%;" multiple
									id="moduleNameSelect">
									<c:forEach var="module" items="${Model.moduleList}">
										<option value="${module.module_id}" class="rel_${module.project_id} hidden buildName">${module.module_name}</option>
									</c:forEach>
								</select>
								<label class="error hidden">This field is required.</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Status:</label>
							<div class="col-lg-2">
								<select data-placeholder="" class="chosen-select" name="statusState"
									tabindex="5" style="width: 100%;" id="statusState">
									<option value="1">is</option>
									<option value="2">is not</option>
									<option value="3">all</option>
								</select>
							</div>
							<div class="col-lg-6">
								<select data-placeholder="Choose a Status" class="chosen-select requiredData"
									name="status" tabindex="6" style="width: 100%;" multiple id="status">
									<c:forEach var="status" items="${Model.statusList}">
										<option value="${status.status_id}">${status.bug_status}</option>
									</c:forEach>
								</select>
								<label class="error hidden">This field is required.</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Priority:</label>
							<div class="col-lg-2">
								<select data-placeholder="" class="chosen-select" name="priorityState"
									tabindex="7" style="width: 100%;" id="priorityState">
									<option value="1">is</option>
									<option value="2">is not</option>
									<option value="3">all</option>
								</select>
							</div>
							<div class="col-lg-6">
								<select data-placeholder="Choose a priority"
									class="chosen-select requiredData" name="priority" tabindex="8"
									style="width: 100%;" multiple id="priority">
									<c:forEach var="priority" items="${Model.priorityList}">
										<option value="${priority.priority_id}">${priority.bug_priority}</option>
									</c:forEach>
								</select>
								<label class="error hidden">This field is required.</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Severity:</label>
							<div class="col-lg-2">
								<select data-placeholder="" class="chosen-select" name="severityState"
									tabindex="9" style="width: 100%;" id="severityState">
									<option value="1">is</option>
									<option value="2">is not</option>
									<option value="3">all</option>
								</select>
							</div>
							<div class="col-lg-6">
								<select data-placeholder="Choose a severity"
									class="chosen-select requiredData" name="severity" tabindex="10"
									style="width: 100%;" multiple id="severity">
									<c:forEach var="severity" items="${Model.severityList}">
										<option value="${severity.severity_id}">${severity.bug_severity}</option>
									</c:forEach>
								</select>
								<label class="error hidden">This field is required.</label>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
								<button class="btn btn-white" id="saveBtn"  type="button" style="margin-right: 15px;">Save</button>
								<button class="btn btn-success" id="executeBtn" type="button">Execute</button>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
								<button class="btn btn-success hidden" id="editBtn" name="editBtn"  type="button" style="margin-right: 15px;">Edit</button>
								<button class="btn btn-success hidden" id="executeSavedQueryBtn" type="button" name="execute" value="1">Execute</button>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
								<button class="btn btn-success hidden" id="createNewBtn" type="button" style="margin-right: 15px;">Create New</button>
								<button class="btn btn-success hidden" id="updateQueryBtn" type="button">Update</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="ibox">

				<div class="ibox-content">
					<div class="row m-b-sm">
						<div class="col-lg-12">
							<h2>Customised Queries</h2>
						</div>
					</div>
					<div class="client-detail" style="max-height: 225px">
						<div class="full-height-scroll">
							<ul class="list-group clear-list">
								<c:forEach var="query" items="${Model.queryData}">
									<li class="list-group-item"><a onclick="showQuery('${query.query_id}','${query.query_variables}')">${query.query_name}</a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<!-- <h5>Table</h5> -->
				</div>
				<div class="ibox-content">
					<table class="table table-stripped" id="table">
						<thead>
							<tr>
								<th>Bug ID</th>
								<th>Bug Title</th>
								<th>Build</th>
								<th>Release</th>
								<th>Bug Status</th>
								<th>Bug Priority</th>
								<th>Bug Severity</th>
								<th>Test Case Title</th>
								<th>Project</th>
								<th>Module</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script>
var dt;
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
		
		$(".chosen-select").chosen().trigger('chosen:updated');
	});
	
	$(function() {
		//to apply choosen
		$(".chosen-select").chosen();
		//collpsing filter Ibox
		//$("#filterIbox").addClass('collapsed');
		dt = $('#table').DataTable(
				{
					dom : '<"html5buttons"B>lTfgitp',
					buttons : [
							{
								extend : 'csv',
								title : 'Test Case Analysis'
							},
							{
								extend : 'excel',
								title : 'Test Case Analysis'
							},
							{
								extend : 'pdf',
								title : 'Test Case Analysis'
							},

							{
								extend : 'print',
								customize : function(win) {
									$(win.document.body).addClass('white-bg');
									$(win.document.body).css('font-size',
											'10px');

									$(win.document.body).find('table')
											.addClass('compact').css(
													'font-size', 'inherit');
								}
							} ],
					"aaSorting" : [ [ 0, "desc" ] ]

				});
	});
	
	//on selecting and deselecting release
	$('#projectSelect').on('change',function(){
		$('#moduleStatusSelect option').first().prop('selected', true).trigger('chosen:updated');  
		var status = $('#projectStatus').val();
		if(status == 3){
			$('#projectStatus option').first().prop('selected', true);  
			$('#projectStatus').trigger('chosen:updated');
		} 
		//console.log("release status is "+status);
		var relID = $(this).val();
		//remove previously selected values
		$('#moduleNameSelect').val('').trigger('chosen:updated');
		//1:Add hidden to all build Names
		$("#moduleNameSelect .buildName").each(function(){
			$(this).addClass('hidden');
			$('#moduleNameSelect').trigger("chosen:updated");
		});
		//2:Show build names for selected Release
		if(status != 2){
		if(relID != null){
			for (var i = 0; i < relID.length; i++) {
				$("#moduleNameSelect .rel_"+relID[i]).each(function(){
					$(this).removeClass('hidden');
					$('#moduleNameSelect').trigger("chosen:updated");
				});
			}
		}else{
			$('#moduleNameSelect').val('').trigger('chosen:updated');
		}
		}else if(status == 2){
			//status is release is not
			if(relID != null){
				for (var i = 0; i < relID.length; i++) {
					$("#moduleNameSelect option").each(function(){
						if(!$(this).hasClass('rel_'+relID[i])){
							$(this).removeClass('hidden');
							$('#moduleNameSelect').trigger("chosen:updated");
						}
					});
				}
			}else{
				$('#moduleNameSelect').val('').trigger('chosen:updated');
			}
		}
	});
	
	//On change of release include status
	$('#projectStatus').on('change',function(){
		$('#moduleNameSelect').val('').trigger('chosen:updated');
		$('#moduleStatusSelect option').first().prop('selected', true).trigger('chosen:updated');
		var relID = $('#projectSelect').val();
		var status = $(this).val();
		if(status == 1){
			for (var i = 0; i < relID.length; ++i) {
				$("#moduleNameSelect .rel_"+relID[i]).each(function(){
					$(this).removeClass('hidden');
					$('#moduleNameSelect').trigger("chosen:updated");
					//console.log("Removed hidden -- > "+$(this).text());
				});
			}
		}else if(status == 2){
			$("#moduleNameSelect option").each(function(){
				$(this).removeClass('hidden');
				$('#moduleNameSelect').trigger("chosen:updated");
			});
			for (var i = 0; i < relID.length; i++) {
				$("#moduleNameSelect option").each(function(){
					if($(this).hasClass('rel_'+relID[i])){
						$(this).addClass('hidden');
						$('#moduleNameSelect').trigger("chosen:updated");
					}
				});
			}
		}else{
			$('#projectSelect option').prop('selected', true);  
			$('#projectSelect').trigger('chosen:updated');
			var allRelID = $('#projectSelect').val();
			for (var i = 0; i < allRelID.length; i++) {
				$("#moduleNameSelect .rel_"+allRelID[i]).each(function(){
					$(this).removeClass('hidden');
					$('#moduleNameSelect').trigger("chosen:updated");
					//console.log("Removed hidden -- > "+$(this).text());
				});
			}
		}
	});
	
	//on change of buils include status
	$('#moduleStatusSelect').on('change',function(){
		var status = $(this).val();
		var projectStatus = $('#projectStatus').val();
		if(projectStatus!=2){
		if(status == 3){
			var allRelID = $('#projectSelect').val();
			for (var i = 0; i < allRelID.length; i++) {
				$("#moduleNameSelect .rel_"+allRelID[i]).each(function(){
					$(this).removeClass('hidden');
					$(this).prop('selected', true);  
					$('#moduleNameSelect').trigger("chosen:updated");
					//console.log("Removed hidden -- > "+$(this).text());
				});
			}
		} 
		}else if(projectStatus ==2){
			if(status == 3){
				$("#moduleNameSelect option").each(function(){
					$(this).removeClass('hidden');
					$('#moduleNameSelect').trigger("chosen:updated");
				});
				var allRelID = $('#projectSelect').val();
				for (var i = 0; i < allRelID.length; i++) {
					$("#moduleNameSelect option").each(function(){
						$("#moduleNameSelect .rel_"+allRelID[i]).each(function(){
							$(this).addClass('hidden');
							$('#moduleNameSelect').trigger("chosen:updated");
							//console.log("Removed hidden -- > "+$(this).text());
						});
					});
				}
				$("#moduleNameSelect option").each(function(){
					if($(this).hasClass('hidden')){
						
					}else{
						$(this).prop('selected', true);  
						$('#moduleNameSelect').trigger("chosen:updated");
					}
				});
			} 
		}	
	});
	
	$('#statusState').on('change',function(){
		var status = $(this).val();
		if(status == 3){
			$('#status option').prop('selected', true);  
			$('#status').trigger('chosen:updated');
		} 
	});

	$('#priorityState').on('change',function(){
		var status = $(this).val();
		if(status == 3){
			$('#priority option').prop('selected', true);  
			$('#priority').trigger('chosen:updated');
		} 
	});
	$('#severityState').on('change',function(){
		var status = $(this).val();
		if(status == 3){
			$('#severity option').prop('selected', true);  
			$('#severity').trigger('chosen:updated');
		} 
	});
	
	//on save button click
	$('#saveBtn,#createNewBtn').click(function(){
		swal({   
			title: "Query Name!",  
			text: "Write query name:",  
			type: "input",   
			showCancelButton: true,   
			closeOnConfirm: false, 
			showLoaderOnConfirm: true,
			animation: "slide-from-top",   
			inputPlaceholder: "Write something" }, 
			function(inputValue){  
				 if (inputValue === false) 
					 return false;    
				 if (inputValue === "") {
				 	swal.showInputError("You need to write something!");  
				 	return false
				 	}
				/*  swal("Nice!", "You wrote: " + inputValue, "success"); */
				 $('#filterForm').append('<input type="hidden" value="'+inputValue+'" name="queryName" />');
				 $('#filterForm').attr("action","savetcquery");
				 $('#filterForm').attr("method","POST");
				 $('#filterForm').submit();
			 });
	});
	
	//show query
	function showQuery(id,queryVariable){
		$('.requiredData').each(function(){
			$(this).parents('.form-group').removeClass('has-error');
			$(this).parents('.form-group').find('.error').addClass('hidden');
		});
		//set all dropdown to null
		$('#queryID').remove();
		$('#filterForm').append('<input type="hidden" name="queryID" value="" id="queryID" />');
		$('#queryID').val(id);
		$('#executeBtn').addClass('hidden');
		$('#saveBtn').addClass('hidden');
		$('#editBtn').removeClass('hidden');
		$('#executeSavedQueryBtn').removeClass('hidden');
		$('#createNewBtn').addClass('hidden');
		$('#updateQueryBtn').addClass('hidden');
		$('.chosen-select').each(function(){
			$(this).val('').trigger('chosen:updated');
			$(this).attr("disabled","true").trigger('chosen:updated');
		});
		//split the string
		var res = queryVariable.split("#");
		for(var i=0;i<res.length;i++){
			//console.log(res[i]);
			displayQuery(res[i])
		}
	};
	
	//show on choosen-select
	function displayQuery(text){
		
		if(text.toLowerCase().indexOf("project") >= 0){

			var startPos = text.toLowerCase().indexOf("(");
			var endPos = text.toLowerCase().indexOf(")");
			var arr = text.substring(startPos+1, endPos);
			//console.log("My release ID's are ----> "+arr);
			//make multiselected
			$('#projectSelect option').each(function(){
				if (arr.indexOf($(this).val()) > -1)
				{
					$(this).prop('selected', true);  
					$('#projectSelect').trigger('chosen:updated');
				}
			});
			if(text.toLowerCase().indexOf("not in") >= 0){
				$('#projectStatus option').each(function(){
					if ($(this).val() == 2){
						$(this).prop('selected', true);  
						$('#projectStatus').trigger('chosen:updated');
					}
				});
			}else{
				$('#projectStatus option').each(function(){
					if ($(this).val() == 1){
						$(this).prop('selected', true);  
						$('#projectStatus').trigger('chosen:updated');
					}
				});
			}
		}else if(text.toLowerCase().indexOf("module") >= 0){

			var startPos = text.toLowerCase().indexOf("(");
			var endPos = text.toLowerCase().indexOf(")");
			var arr = text.substring(startPos+1, endPos);
			//console.log("My Build ID's are ----> "+arr);
			//make multiselected
			$('#moduleNameSelect option').each(function(){
				if (arr.indexOf($(this).val()) > -1)
				{
					$(this).prop('selected', true);  
					$('#moduleNameSelect').trigger('chosen:updated');
				}
			});
			if(text.toLowerCase().indexOf("not in") >= 0){
				$('#moduleStatusSelect option').each(function(){
					if ($(this).val() == 2){
						$(this).prop('selected', true);  
						$('#moduleStatusSelect').trigger('chosen:updated');
					}
				});
			}else{
				$('#moduleStatusSelect option').each(function(){
					if ($(this).val() == 1){
						$(this).prop('selected', true);  
						$('#moduleStatusSelect').trigger('chosen:updated');
					}
				});
			}
		}else if(text.toLowerCase().indexOf("status") >= 0){

			var startPos = text.toLowerCase().indexOf("(");
			var endPos = text.toLowerCase().indexOf(")");
			var arr = text.substring(startPos+1, endPos);
			//console.log("My Status ID's are ----> "+arr);
			//make multiselected
			$('#status option').each(function(){
				if (arr.indexOf($(this).val()) > -1)
				{
					$(this).prop('selected', true);  
					$('#status').trigger('chosen:updated');
				}
			});
			if(text.toLowerCase().indexOf("not in") >= 0){
				$('#statusState option').each(function(){
					if ($(this).val() == 2){
						$(this).prop('selected', true);  
						$('#statusState').trigger('chosen:updated');
					}
				});
			}else{
				$('#statusState option').each(function(){
					if ($(this).val() == 1){
						$(this).prop('selected', true);  
						$('#statusState').trigger('chosen:updated');
					}
				});
			}
		}else if(text.toLowerCase().indexOf("priority") >= 0){

			var startPos = text.toLowerCase().indexOf("(");
			var endPos = text.toLowerCase().indexOf(")");
			var arr = text.substring(startPos+1, endPos);
			//console.log("My Priority ID's are ----> "+arr);
			//make multiselected
			$('#priority option').each(function(){
				if (arr.indexOf($(this).val()) > -1)
				{
					$(this).prop('selected', true);  
					$('#priority').trigger('chosen:updated');
				}
			});
			if(text.toLowerCase().indexOf("not in") >= 0){
				$('#priorityState option').each(function(){
					if ($(this).val() == 2){
						$(this).prop('selected', true);  
						$('#priorityState').trigger('chosen:updated');
					}
				});
			}else{
				$('#priorityState option').each(function(){
					if ($(this).val() == 1){
						$(this).prop('selected', true);  
						$('#priorityState').trigger('chosen:updated');
					}
				});
			}
		}else if(text.toLowerCase().indexOf("severity") >= 0){

			var startPos = text.toLowerCase().indexOf("(");
			var endPos = text.toLowerCase().indexOf(")");
			var arr = text.substring(startPos+1, endPos);
			//console.log("My Priority ID's are ----> "+arr);
			//make multiselected
			$('#severity option').each(function(){
				if (arr.indexOf($(this).val()) > -1)
				{
					$(this).prop('selected', true);  
					$('#severity').trigger('chosen:updated');
				}
			});
			if(text.toLowerCase().indexOf("not in") >= 0){
				$('#severityState option').each(function(){
					if ($(this).val() == 2){
						$(this).prop('selected', true);  
						$('#severityState').trigger('chosen:updated');
					}
				});
			}else{
				$('#severityState option').each(function(){
					if ($(this).val() == 1){
						$(this).prop('selected', true);  
						$('#severityState').trigger('chosen:updated');
					}
				});
			}
		}				
	}
	
	//edit btn 
	$('#editBtn').click(function(){
		$('.chosen-select').each(function(){
			$(this).removeAttr("disabled");
			$(this).trigger('chosen:updated');
		});
		$(this).addClass('hidden');
		$('#executeSavedQueryBtn').addClass('hidden');
		$("#createNewBtn").removeClass("hidden");
		$("#updateQueryBtn").removeClass("hidden");
		
		//remove the hidden from the modules of previously selected projects.
		//get the selected project Id's
		var proID = $('#projectSelect').val();
		//get the project status
		var status = $('#projectStatus').val();
		
		if(status == 2){
			//project status is not in
			if(proID != null){
				//some pojects are selected
				//add the hidden for modules for selected projects
				for (var i = 0; i < proID.length; i++) {
					$("#moduleNameSelect option").each(function(){
						if(!$(this).hasClass('rel_'+proID[i])){
							$(this).removeClass('hidden');
							$('#moduleNameSelect').trigger("chosen:updated");
						}
					});
				}
			}
		}else{
			//project status is in
			if(proID != null){
				//some pojects are selected
				//remove the hidden for modules for selected projects
				for (var i = 0; i < proID.length; i++) {
				$("#moduleNameSelect .rel_"+proID[i]).each(function(){
					$(this).removeClass('hidden');
					$('#moduleNameSelect').trigger("chosen:updated");
				});
				}
			}
		}
	});
		
	//execute saved query btn click
	$('#executeSavedQueryBtn').click(function(){
		//remove disabled choosen to send values
		$('.chosen-select').each(function(){
			$(this).removeAttr("disabled");
			$(this).trigger('chosen:updated');
		});
		//clear previous table data
		dt.clear().draw();
		var status = true;
		//check for the validity of select
		$('.requiredData').each(function(){
			var value = $(this).val();	
			if(value == null){
				$(this).parents('.form-group').addClass('has-error');
				status = false;
				$(this).parents('.form-group').find('.error').removeClass('hidden');
			}
			else{
				$(this).parents('.form-group').removeClass('has-error');
				$(this).parents('.form-group').find('.error').addClass('hidden');
			}
				
		});
		//if form is valid
		if(status == true){
		//get the values 
		var new_rel = getMyValue($('#projectSelect').val());
		var new_bld = getMyValue($('#moduleNameSelect').val());
		var new_stat = getMyValue($('#status').val());
		var new_prio = getMyValue($('#priority').val());
		var new_seve = getMyValue($('#severity').val());
		//add disabled choosen values
		$('.chosen-select').each(function(){
			$(this).attr("disabled","true");
			$(this).trigger('chosen:updated');
		});
		
		//post the data
			 $.post("tcbugreport",
			    {
				 projectState : $('#projectStatus').val(),
				 project :  new_rel ,
				 moduleState  : $('#moduleStatusSelect').val(),
				 module  : new_bld,
				 statusState  : $('#statusState').val(),
				 status  : new_stat,
				 priorityState  : $('#priorityState').val(),
				 priority  : new_prio,
				 severityState  : $('#severityState').val(),
				 severity  : new_seve
			    },
			    function(data, status){
			    	console.log("1st");
			    	console.log(data);
			    	var arr=JSON.parse(JSON.stringify(data));
			    	if(arr == 0){
			    		dt.clear().draw();
			    	}else{
			    	$.each(arr, function(index,jsonObject){
						var rowNode = dt.row.add([jsonObject.bug_prefix,jsonObject.bug_title,jsonObject.build_name,jsonObject.release_name,jsonObject.bugStatus,jsonObject.bug_priority,jsonObject.bug_severity,jsonObject.testcase_name,jsonObject.project_name,jsonObject.module_name])
						.draw().node();
			    		$( rowNode )
			    	    .css( 'cursor', 'pointer')
			    	    .attr('onclick','location.href="bugsummarybyprefix?btPrefix='+jsonObject.bug_prefix+'"');
			    	});
			    	}
			    });
		}else{
			//add disabled choosen values
			$('.chosen-select').each(function(){
				$(this).attr("disabled","true");
				$(this).trigger('chosen:updated');
			});
		}
	});
	
	$('#updateQueryBtn').click(function(){		
		$('#filterForm').attr("action","updatetcquery");
		$('#filterForm').attr("method","POST");
		$('#filterForm').submit();
		
	});
	
$('#executeBtn').click(function(){
	//clear previous table data
	dt.clear().draw();
	var status = true;
	//check for the validity of select
	$('.requiredData').each(function(){
		var value = $(this).val();	
		if(value == null){
			$(this).parents('.form-group').addClass('has-error');
			status = false;
			$(this).parents('.form-group').find('.error').removeClass('hidden');
		}
		else{
			$(this).parents('.form-group').removeClass('has-error');
			$(this).parents('.form-group').find('.error').addClass('hidden');
		}
			
	});
	//if form is valid
	if(status == true){
	var new_rel = getMyValue($('#projectSelect').val());
	var new_bld = getMyValue($('#moduleNameSelect').val());
	var new_stat = getMyValue($('#status').val());
	var new_prio = getMyValue($('#priority').val());
	var new_seve = getMyValue($('#severity').val());
	
		 /*$.post("tcbugreport",
		    {
			 projectState : $('#projectStatus').val(),
			 project :  new_rel ,
			 moduleState  : $('#moduleStatusSelect').val(),
			 module  : new_bld,
			 statusState  : $('#statusState').val(),
			 status  : new_stat,
			 priorityState  : $('#priorityState').val(),
			 priority  : new_prio,
			 severityState  : $('#severityState').val(),
			 severity  : new_seve
		    },
		    function(data, status){
		    	console.log(data);
		    	var arr=JSON.parse(JSON.stringify(data));
		    	console.log("arr");
		    	console.log(arr);
		    	console.log(status);
		    	if(arr == 0){
		    		dt.clear().draw();
		    	}else{
		    	$.each(arr, function(index,jsonObject){
		    		var rowNode = dt.row.add([jsonObject.bug_prefix,jsonObject.bug_title,jsonObject.build_name,jsonObject.release_name,jsonObject.bugStatus,jsonObject.bug_priority,jsonObject.bug_severity,jsonObject.testcase_name,jsonObject.project_name,jsonObject.module_name])
					.draw().node();
		    		$( rowNode )
		    	    .css( 'cursor', 'pointer')
		    	    .attr('onclick','location.href="bugsummarybyprefix?btPrefix='+jsonObject.bug_prefix+'"');
		    	});
		    	 }
		    }); */
		    
		    $.ajax({
		    	url: 'tcbugreport',
		    	type: 'post',
		    	data: {
		    		projectState : $('#projectStatus').val(),
					 project :  new_rel ,
					 moduleState  : $('#moduleStatusSelect').val(),
					 module  : new_bld,
					 statusState  : $('#statusState').val(),
					 status  : new_stat,
					 priorityState  : $('#priorityState').val(),
					 priority  : new_prio,
					 severityState  : $('#severityState').val(),
					 severity  : new_seve
		    	},
		    	dataType: 'json',
		    	success: function(data){
			    	var arr=JSON.parse(JSON.stringify(data));
			    	//console.log(status);
			    	if(arr == 0){
			    		dt.clear().draw();
			    	}else{
			    	$.each(arr, function(index,jsonObject){
			    		var rowNode = dt.row.add([jsonObject.bug_id,jsonObject.bug_title,jsonObject.build_name,jsonObject.release_name,jsonObject.bug_status,jsonObject.bug_priority,jsonObject.bug_severity,jsonObject.testcase_name,jsonObject.project_name,jsonObject.module_name])
						.draw().node();
			    		$( rowNode )
			    	    .css( 'cursor', 'pointer')
			    	    .attr('onclick','location.href="bugsummarybyprefix?btPrefix='+jsonObject.bug_prefix+'"');
			    	});
			    	 }
		    	}
		    	
		    });//end ajax
		}
	});

function getMyValue(value){
	if(value == null)
		return value;
	else
		return value.join(',');
}

$('#status').on('change',function(){
	var status = $('#statusState').val();
	if(status == 3){
		$('#statusState option').first().prop('selected', true);  
		$('#statusState').trigger('chosen:updated');
	} 
});

$('#priority').on('change',function(){
	var status = $('#priorityState').val();
	if(status == 3){
		$('#priorityState option').first().prop('selected', true);  
		$('#priorityState').trigger('chosen:updated');
	} 
});
$('#severity').on('change',function(){
	var status = $('#severityState').val();
	if(status == 3){
		$('#severityState option').first().prop('selected', true);  
		$('#severityState').trigger('chosen:updated');
	} 
});
$('#moduleNameSelect').on('change',function(){
	var status = $('#moduleStatusSelect').val();
	if(status == 3){
		$('#moduleStatusSelect option').first().prop('selected', true);  
		$('#moduleStatusSelect').trigger('chosen:updated');
	} 
});
</script>
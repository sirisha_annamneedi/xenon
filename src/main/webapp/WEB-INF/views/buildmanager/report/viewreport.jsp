<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Manual Test Set Report - ${Model.selectedExecuteBuildName}</h2>
		</div>
		</div>
		<div class="row sub-topmenu">
		<div class="col-md-12">
			
				<div class="col-md-6 pd-l-0">
					<ul>
						<li><a href="executebuild"><i class="fa fa-home"></i> Home</a></li>
						<li><a href="javascript:void(0)" id="homeMenu"
							class="active "><i class="fa fa-line-chart"></i> Report </a></li>
					</ul>
				</div>
				<div class="col-md-6">
				<button onclick="printFunction()" class="btn btn-info btn-sm pull-right mt-5">Export
	Dashboard</button>
			</div>
		</div>
	</div>
	
	<div class="wrapper wrapper-content animated fadeInRight">
		<div id="ProjectListDiv">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox">
						
						<!-- end .ibox-title -->
						<div class="ibox-content">
							<div class="row">
								<div class="col-lg-8 col-lg-offset-2">
									<canvas id="projectReportBarChart" height="125"></canvas>
								</div>
							</div>
						</div>
						<!-- end .ibox-content -->
					</div>
					<!-- end .ibox -->
				</div>
				<!-- end .col -->
			</div>
			<!-- end .row -->
			<%-- 			<c:forEach var="projectsList" items="${Model.projectsList}"> --%>
			<!-- 			<div id="projectInfoDiv}"> -->
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Project & Module wise test case details</h5>
							<!-- 							<button id="btnExport" class="btn btn-info pull-right" onclick="fnExcelReport();"> EXPORT </button> -->
						</div>

						<div class="ibox-content">
							<c:set var="TcCount" value="${0}" />
							<c:set var="ExecutedTcCount" value="${0}" />
							<c:set var="PassCount" value="${0}" />
							<c:set var="FailCount" value="${0}" />
							<c:set var="SkipCount" value="${0}" />
							<c:set var="BlockCount" value="${0}" />
							<c:set var="NotRunTotal" value="${0}" />
							<div class="table-responsive" style="overflow-x: visible;">
								<table class="table table-stripped  table-bordered table-hover "
									id="moduleListTable" style="width: 100%;">
									<thead>
										<tr>
											<td><b>Application Name</b></td>
											<td><b>Module Name</b></td>
											<td><b>Total TC</b></td>
											<td><b>Executed TC</b></td>
											<td><b>Pass</b></td>
											<td><b>Fail</b></td>
											<td><b>Skip</b></td>
											<td><b>Block</b></td>
											<td><b>Not Run</b></td>

										</tr>
									</thead>
									<c:set var="curProject" value="0" />

									<c:forEach var="projectsList" items="${Model.projectsList}">
										<c:forEach var="modulesList" items="${Model.modulesList}">
											<c:if
												test="${projectsList.project_id==modulesList.project_id}">
												<tbody>
													<tr>
														<c:choose>
															<c:when test="${curProject!=modulesList.project_id}">
																<td class="totalProject">${projectsList.project_name}</td>
															</c:when>
															<c:otherwise>
																<td></td>
															</c:otherwise>
														</c:choose>

														<td style="width: 200px" class="textAlignment"
															data-original-title="${modulesList.module_name}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${modulesList.module_name}</td>
														<td>${modulesList.TcCount}</td>
														<td>${modulesList.ExecutedTcCount}</td>
														<td>${modulesList.PassCount}</td>
														<td>${modulesList.FailCount}</td>
														<td>${modulesList.SkipCount}</td>
														<td>${modulesList.BlockCount}</td>
														<td>${modulesList.TcCount-(modulesList.PassCount+modulesList.FailCount+modulesList.SkipCount+modulesList.BlockCount)}</td>
													</tr>

													<c:set var="curProject" value="${modulesList.project_id}" />
												</tbody>

												<%-- 												</c:if> --%>

											</c:if>
										</c:forEach>

										<tbody>
											<c:set var="Total" value="${projectsList.TcCount}" />
											<c:set var="Executed" value="${projectsList.ExecutedTcCount}" />
											<c:set var="Pass" value="${projectsList.PassCount}" />
											<c:set var="Fail" value="${projectsList.FailCount}" />
											<c:set var="Skip" value="${projectsList.SkipCount}" />
											<c:set var="Block" value="${projectsList.BlockCount}" />
											<tr style="font-weight: bold">
												<td></td>
												<td>Total</td>
												<td>${Total}</td>
												<td>${Executed}</td>
												<td  class="totalPass">${Pass}</td>
												<td class="totalFail">${Fail}</td>
												<td class="totalSkip">${Skip}</td>
												<td class="totalBlock">${Block}</td>
												<td class="totalNotRun">${Total-(Pass+Fail+Skip+Block)}</td>
											</tr>
											<c:set var="TcCount" value="${TcCount +Total}" />
											<c:set var="ExecutedTcCount"
												value="${ExecutedTcCount + Executed}" />
											<c:set var="PassCount" value="${PassCount +Pass}" />
											<c:set var="FailCount" value="${FailCount +Fail}" />
											<c:set var="SkipCount" value="${SkipCount +Skip}" />
											<c:set var="BlockCount" value="${BlockCount + Block}" />
										</tbody>


									</c:forEach>
									<tbody>
										<tr style="font-weight: bold;">
											<td></td>
											<td>Grand Total</td>
											<td>${TcCount}</td>
											<td>${ExecutedTcCount}</td>
											<td>${PassCount}</td>
											<td>${FailCount}</td>
											<td>${SkipCount}</td>
											<td>${BlockCount}</td>
											<td>${TcCount-(PassCount+FailCount+SkipCount+BlockCount)}</td>
										</tr>
									</tbody>
								</table>


							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-2"></div>
								</div>
							</div>

						</div>
					</div>


				</div>
			</div>
			<!-- 			</div> -->
			<%-- 		</c:forEach> --%>
			<div class="row" id="allTestCase">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>All Test cases</h5>
								<c:choose>
								<c:when test="${Model.testcaseStepsList.size() > 0}">
									<button type="button" class="btn btn-info btn-sm pull-right"
										onclick="getTestSteps();">Test Steps</button>
								</c:when>
								<c:otherwise>
									<button type="button" class="btn btn-info pull-right"
										disabled="disabled" title="Record not found">Test
										Steps</button>
								</c:otherwise>
							</c:choose>
						</div>
						<div class="ibox-content">
							<div class="table-responsive" style="overflow-x: visible;">
								<table
									class="table table-striped table-bordered table-hover dataTables-example tableExport"
									id="testcaseListTable" style="width: 100%;">
									<thead>
										<tr>
											<th style="display: none;">Module Id</th>
											<th style="width: 150px">Module Name</th>
											<th style="display: none;">Scenario Id</th>
											<th>Scenario Name</th>
											<th style="width: 100px">Test Case Id</th>
											<th style="width: auto;">Name</th>
											<th style="display: none;">Summary</th>
											<th style="width: 200px;">Tester</th>
											<th style="width: 150px">Status</th>

										</tr>
									</thead>

									<tbody>
										<c:forEach var="testcasesList" items="${Model.testcasesList}">
											<tr>

												<c:forEach var="modulesList3" items="${Model.modulesList}">
													<c:if
														test="${modulesList3.module_id==testcasesList.module_id}">
														<td style="display: none;">${modulesList3.module_id}</td>
														<td>${modulesList3.module_name}</td>

													</c:if>
												</c:forEach>
												<c:forEach var="scenarioList3" items="${Model.scenarioList}">
													<c:if
														test="${scenarioList3.scenario_id==testcasesList.scenario_id}">
														<td style="display: none;">${scenarioList3.scenario_id}</td>
														<td>${scenarioList3.scenario_name}</td>

													</c:if>
												</c:forEach>
												<td>${testcasesList.test_prefix}</td>
												<td class="textAlignment"
													data-original-title="${testcasesList.testcase_name}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${testcasesList.testcase_name}</td>
												<td style="display: none;">${testcasesList.summary}</td>
												<td><c:forEach var="testerDeatils"
														items="${Model.testerDeatils}">
														<c:if
															test="${testerDeatils.tc_id==testcasesList.testcase_id}">
													${testerDeatils.name}
													
												</c:if>
													</c:forEach></td>
												<td id="tcStatusPut_${testcasesList.testcase_id}" class="statusclass">
												<c:forEach var="testCase" items="${Model.tcExecDetailsList1}">
															
														<c:choose>
															<c:when test="${testcasesList.testcase_id == testCase.tc_id}">
														   		${testCase.description}
														  	</c:when>
														  	<%--  <c:otherwise> 
																sds													 
															 </c:otherwise> --%> 
															 
														</c:choose>
															 
													</c:forEach></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row" id="testStepsReport" style="display: none">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Test Steps</h5>
							<button type="button" class="btn btn-info pull-right"
								onclick="getAlltestCase();">All Test Cases</button>
						</div>

						<div class="ibox-content">

							<div class="table-responsive" style="overflow-x: visible;">
								<table
									class="table table-striped table-bordered table-hover dataTables-example tableExport"
									id="testcaseStepListTable" style="width: 100%;">
									<thead>
										<tr>
											<th class="hidden">Step Id</th>
											<th>Test Case Id</th>
											<th>Test Case Name</th>
											<th>Summary</th>
											<th>Test Case Status</th>
											<th>Tester</th>
											<th>Action</th>
											<th>Result</th>
										</tr> 
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-2">

										<!-- <button class="btn btn-danger" type="submit">Remove</button> -->
									</div>
								</div>
							</div>

						</div>
					</div>


				</div>
			</div>

		</div>

		<!-- end row -->
	</div>
</div>

<script>

function dashLink(link) {
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setTestIdParameter', {
		filterText : link,
		
	});

	posting.done(function(data) {
		window.location.href = "testcasebuild";
	});
}


$(window).load(function() {
	colorPass = "${Model.Pass}";
	colorFail = "${Model.Fail}";
	colorSkip = "${Model.Skip}";
	colorNotRun = "${Model.NotRun}";
	colorBlock = "${Model.Block}";
        
	var labelArray = [];
	var passArray = [];
	var failArray = [];
	var skippArray = [];
	var blockArray = [];
	var notRunArray = [];
	

	$('.totalProject').each(function(){
		labelArray.push(this.innerHTML); 
    });
	$('.totalPass').each(function(){
		passArray.push(this.innerHTML); 
    });
	$('.totalFail').each(function(){
		failArray.push(this.innerHTML); 
    });
	$('.totalSkip').each(function(){
		skippArray.push(this.innerHTML); 
    });
	$('.totalBlock').each(function(){
		blockArray.push(this.innerHTML); 
    });

	$('.totalNotRun').each(function(){
		notRunArray.push(this.innerHTML); 
    });
	var barData = {
	        labels:labelArray,
	        datasets: [
			            {
			                label: "Pass",
			                backgroundColor: colorPass,
				            hoverBackgroundColor: colorPass,
			                data: passArray
			            },
			            {
			                label: "Fail",
			                backgroundColor: colorFail,
				            hoverBackgroundColor: colorFail,
			                data:failArray
			            },
			            {
			                label: "Skip",
			                backgroundColor: colorSkip,
				            hoverBackgroundColor:colorSkip,
			                data: skippArray
			            },
			            {
			                label: "Block",
			                backgroundColor: colorBlock,
				            hoverBackgroundColor: colorBlock,
			                data: blockArray
			            },
			            {
			                label: "Not Run",
			                backgroundColor: colorNotRun,
				            hoverBackgroundColor: colorNotRun,
			                data: notRunArray
			            }
			            ]
	    };
	    var barOptions = {
	            scaleBeginAtZero: true,
	            scaleShowGridLines: true,
	            scaleGridLineColor: "rgba(0,0,0,.05)",
	            scaleGridLineWidth: 1,
	            barShowStroke: true,
	            barStrokeWidth: 2,
	            barValueSpacing: 5,
	            barDatasetSpacing: 1,
	            responsive: true,
	            scales: {
		            yAxes: [{
		                    ticks: {
		                        min: 0,
		                        callback: function(value) {if (value % 1 === 0) {return value;}}
		                    }
		            }]
		        }
	        }
	    
	    var ctx = document.getElementById("projectReportBarChart");
	  
	    var myNewChart = new Chart(ctx, {
	    	 type: 'bar',
			    data: barData,
			    options: barOptions
		});
	
	
	
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});

		$(document).ready(function() {
			
			 $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
			$(":radio[value=1]").parent().addClass("radio-success");
			$(":radio[value=2]").parent().addClass("radio-danger");
			$(":radio[value=3]").parent().addClass("radio-info");
			
			
			
			var allProTCDetailsTable = $('#testcaseListTable').DataTable({
				dom : '<"html5buttons"B>lTfgitp',
				buttons : [
						{
							extend : 'csv',
							title : 'Build Report'
						},
						{
							extend : 'excel',
							title : 'Build Report'
						},
						{
							extend : 'pdf',
							title : 'Build Report'
						},

						{
							extend : 'print',
							customize : function(win) {
								$(win.document.body).addClass('white-bg');
								$(win.document.body).css('font-size',
										'10px');

								$(win.document.body).find('table')
										.addClass('compact').css(
												'font-size', 'inherit');
							}
						} ], 
						"columnDefs": [ {
						    "targets": 8 ,
						    "data": "Not Run",
						    "defaultContent": "Click to edit"
						  } ],
				"paging" : true,
				"lengthChange" : true,
				"searching" : true,
				"ordering" : true,
				"info" : true,
				"autoWidth" : true
			});
			
 			$('#testcaseListTable tr').each(function() {
 			    var cs = $(this).find(".statusclass").html();
 			    if(cs==""){
 			    	allProTCDetailsTable.cell($(this).find(".statusclass")).data('Not Run');    
 			    }
			    
 			 }); 
		});

		

		 function initializeSelectAll(clickedId) {
			var checkedTableId = $(".statusTable").attr("id");

			var tableInitialization;

			if ($.fn.dataTable.isDataTable('#' + checkedTableId)) {
				tableInitialization = $('#' + checkedTableId).DataTable();
			} else {
				tableInitialization = $('#' + checkedTableId).DataTable({
					"aoColumnDefs" : [ {
						'bSortable' : false,
						'aTargets' : [ 1 ]
					} ]
				});
			}

			return tableInitialization;
		}

		$(".select2_demo_1").select2();
		
		function saveExecClicked(testcase_id)
		{
				
				var notes=$("#testcaseSummaryForm_"+testcase_id).find("textarea[name='notes']").val();
				var tcStatus=$('input[name=tcStatus]:checked', "#testcaseSummaryForm_"+testcase_id).val();
				var posting = $.ajax({
		           type: "POST",
		           async: true,
		           url: "insertTcExecSummary",
		           data: {tcId:testcase_id,notes:notes,tcStatus:tcStatus},
		           success: function(data)
		           {
		               if(data==1)
		            	   {
			            	   var rows = $('#testcaseStepListTable_'+testcase_id).dataTable().fnGetNodes();
			                   for(var i=0;i<rows.length;i++)
			                   {
			                	  
			                	   var rowId=$(rows[i]).find("td:eq(0)").html();
			                	   var testStepId=$(rows[i]).find("td:eq(1)").html();
			                	   
			                	  $.ajax({
				               			type: "POST",
				               			url: 'insertStepExecSummary',
				               		 	async: false,
				               			data:{stepId:$("#stepId_"+rowId).val(),stepStatus:$("#stepStatus_"+rowId).val(),stepComment:$("#stepComment_"+rowId).val()}				               			
				                   });
			                	
			                       
			                   } 
			                   swal({
              		                title: "Success!",
              		                text: "Execution saved successfully!",
              		                type: "success"
              		            }, function () {
              		            	 window.location.href="executebuild";
              		            });
			                  
		            	   }
		           }
		         }); 
				 
		}
		
		function enterKeyPress(e) {
		    if (e.keyCode === 13) {
		    	 return false;
		    }
		   
		}
		
		$('#executeBuildLink').click(function(e){
			 e.preventDefault();
			 window.location.href = "buildreport";
		});
		
	 function downloadFile(attachID,title){
		  var posting = $.post('gettcattchment', {
				 attchmentId : attachID,
				});
		  posting.done(function(data){
			  	 var fileNameExt = title.substr(title.lastIndexOf('.') + 1);
				 var a = window.document.createElement('a');
				 link =  "data:"+fileNameExt+";base64,"+data;
				 a.href=link;
				 a.download = title;
				 document.body.appendChild(a)
				 a.click();
				 document.body.removeChild(a)
				
		  });
	  }   
	 
	 function fnExcelReport()
	 {
	     var tab_text="<table border='2px'><tr>";
	     var textRange; var j=0;
	     tab = document.getElementById('moduleListTable'); // id of table

	     for(j = 0 ; j < tab.rows.length ; j++) 
	     {     
	         tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
	         //tab_text=tab_text+"</tr>";
	     }

	     tab_text=tab_text+"</table>";
	     tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
	     tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
	     tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

	     var ua = window.navigator.userAgent;
	     var msie = ua.indexOf("MSIE "); 

	     if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
	     {
	         txtArea1.document.open("txt/html","replace");
	         txtArea1.document.write(tab_text);
	         txtArea1.document.close();
	         txtArea1.focus(); 
	         sa=txtArea1.document.execCommand("SaveAs",true,"tc.xls");
	     }  
	     else                 //other browser not tested on IE 11
	         sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

	     return (sa);
	 }
	 function getAlltestCase() {
		  document.getElementById("testStepsReport").style.display = "none";
		  document.getElementById("allTestCase").style.display = "block";

		}  
	
	 function strip(html){
		   var doc = new DOMParser().parseFromString(html, 'text/html');
		   return doc.body.textContent || "";
		}
	 function getTestSteps() {
		  document.getElementById("allTestCase").style.display = "none";
		  var testcaseStepsList=${Model.testcaseStepsList};
		  var testCase2=${Model.tcExecDetailsList};
		  var testerDeatils=${Model.testerDeatils1};
		  console.log(testerDeatils); 
		  console.log(testCase2);
		  console.log(testcaseStepsList);
		 var tdata ="";
		  var testCaseId=0;var descIsAv='';var tctester;
			  document.getElementById("testStepsReport").style.display = "block";
			  var tbody = $("#testcaseStepListTable tbody");

			  if (tbody.children().length == 0) {
				  
		 				for(var i=0;i<testcaseStepsList.length;i++)
		                 {
						
		 					var tbody = "<tr><td class='hidden'>"+testcaseStepsList[i].step_id+"</td>";
							
		 				if(testCaseId!=testcaseStepsList[i].testcase_id){
		 				tbody+= "<td>"+testcaseStepsList[i].test_prefix+"</td><td>"+testcaseStepsList[i].testcase_name+"</td><td>"+testcaseStepsList[i].summary+"</td>";
		 				 for(var j=0;j<testCase2.length;j++){
				 				if(testcaseStepsList[i].testcase_id==testCase2[j].tc_id){
				 				descIsAv=testCase2[j].description;
				 				break;
				 				}else{
				 					descIsAv="Not Run";
				 				}
		 				 }
		 				tbody+="<td>"+descIsAv+"</td>";
		 				}else{
		 					tbody+="<td></td><td></td><td></td><td></td>";
		 				}
		 				
		 				 for(var j=0;j<testCase2.length;j++){
		 					for(var k=0;k<testerDeatils.length;k++){
				 				if(testcaseStepsList[i].testcase_id==testCase2[j].tc_id && testerDeatils[k].tester_id==testCase2[j].tester_id){
				 					tctester=testerDeatils[k].name;
				 					//break;
				 				} 
		 				 }
		 				}
		 				 if(descIsAv=="Not Run"){
		 					tctester=""; 
		 				 }
		 				//tbody+="<td>"+testerDeatils[0].name+"</td>";
		 				tbody+="<td>"+tctester+"</td>";
		 				tbody+="<td>"+testcaseStepsList[i].action+"</td><td>"+testcaseStepsList[i].result+"</td>";
		 				testCaseId=testcaseStepsList[i].testcase_id; 
// 		 				alert(tbody);
		 				tdata+=tbody;
		 				}
//		 				alert(tdata);
		 				document.getElementById("testcaseStepListTable").tBodies[0].innerHTML= tdata;
			
//		 				 Table.clear().draw();
//			 	            Table.rows.add(tdata).draw();
//		 				  var data = table.row( this ).data();
//		 			        alert( 'You clicked on '+data[0]+'\'s row' )
//		  document.getElementById("testcaseStepListTbody").innerHTML = tdata;			 
		 				 var allProTCDetailsTable1 = $('#testcaseStepListTable').DataTable({
		 					dom : '<"html5buttons"B>lTfgitp',
		 					buttons : [
		 							{
		 								extend : 'csv',
		 								title : 'Build Report'
		 							},
		 							{
		 								extend : 'excel',
		 								title : 'Build Report'
		 							},
		 							{
		 								extend : 'pdf',
		 								title : 'Build Report'
		 							},

		 							{
		 								extend : 'print',
		 								customize : function(win) {
		 									$(win.document.body).addClass('white-bg');
		 									$(win.document.body).css('font-size',
		 											'10px');

		 									$(win.document.body).find('table')
		 											.addClass('compact').css(
		 													'font-size', 'inherit');
		 								}
		 							} ], 
		 					"paging" : true,
		 					"lengthChange" : true,
		 					"searching" : true,
		 					"ordering" : true,
		 					"info" : true,
		 					"autoWidth" : true
		 				});
			  }
		} 
		 function printFunction() {
			 window.print();
			 }
		 
		 
	</script>
<style>
.md-skin .ibox{
margin-bottom:10px;
}
.float-e-margins .btn{
margin-bottom:0;
margin-top:-5px;
}
.ibox-content {
padding: 10px;
}
.dataTables_wrapper {
padding-bottom: 0;
}
#testcaseListTable_paginate .pagination{
margin-top:10px;
}
</style>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="bar" id="barInMenu">
  <p>loading</p>
</div>

<div id="wrapper">
	<nav class="navbar-default navbar-static-side" role="navigation">
		<div class="sidebar-collapse">
			<ul class="nav metismenu" id="side-menu">
				<li class="nav-header">
                    <div class="dropdown profile-element text-center">
                        <a href="#" onclick="gotoLink('xedashboard')" >
                        <img src="<%=request.getContextPath()%>/resources/img/Jade-Logo.png" class="xenon-logo">
                        </a>
                    <!--    <img class="img-circle" style="width:48px; height:48px" src="resources/img/user.jpg">
                         <img alt="image" class="img-circle"
                            style="width: 48px; height: 48px"
                            src="data:image/jpg;base64,${Model.UserProfilePhoto }" />
                        </span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
                            class="clear"> <span class="block m-t-xs"> <strong
                                    class="font-bold">${Model.userName }</strong>
                            </span>
                        </span>
                        </a> <span class="text-muted text-xs block">${Model.role}<b
                            class=""></b></span>  -->
                    </div>
                    <div class="logo-element" style="padding: 14px !important">
                        <a href="#" onclick="gotoLinkInMenu('xedashboard')">
                        <img alt="image" class=""
                            src="<%=request.getContextPath()%>/resources/img/xenon.png" />
                        </a>
                    </div>
                </li>
				
				<li class="mainLi"><a href="" id=""><i
						class="fa fa-th-large"></i> Applications</a></li>
				<c:forEach var="projectsList" items="${Model.projectList}">
					<li class="mainLi"><a href="#"
					data-original-title='${projectsList.project_name}' data-container='body' data-toggle='tooltip' data-placement='right'
						onclick="showModuleWiseReport(${projectsList.project})"><i
							class="fa fa-th-large"></i> <span class="nav-label" style="margin-right: 10px;">${fn:substring(projectsList.project_name,0,13)}</span><span
							class="label label-info "></span>
					<span class="fa arrow pull-right"></span>
					</a>

						<ul class="nav nav-second-level collapse">
							<c:forEach var="moduleList" items="${Model.moduleList}"
								varStatus="moduleCounter">
								<c:if test="${projectsList.project==moduleList.project}">
									<li class="moduleList"><a href="#" class="moduleNav"
									data-original-title='${moduleList.module_name}' data-container='body' data-toggle='tooltip' data-placement='right'
										onclick="showScenarioWiseReport(${moduleList.module},${moduleList.project})">${fn:substring(moduleList.module_name,0,13)}<span
											class="label label-warning "></span>
											<span class="fa arrow"></span></a>

										<ul class="nav nav-third-level" style="margin-left:10px;">
											<c:forEach var="scenarioList" items="${Model.scenarioList}">
												<c:if
													test="${moduleList.module==scenarioList.module}">
													<li><a
														href="#"
														data-original-title='${scenarioList.scenario_name}' data-container='body' data-toggle='tooltip' data-placement='right'
														onclick="showTCWiseReport(${scenarioList.scenario},${scenarioList.module})">${fn:substring(scenarioList.scenario_name,0,13)}<span
															class="label label-success"></span></a></li>
												</c:if>
											</c:forEach>


										</ul></li>
								</c:if>
							</c:forEach>
						</ul></li>
				</c:forEach>
				<li class="mainLi"><a href="documentlibrary"><i
						class="fa fa-files-o"></i> <span class="nav-label">Document
							Library</span></a></li>
							
			</ul>

		</div>
	</nav>
</div>	
	<input type="hidden" value="<%=session.getAttribute("menuLiText")%>"
		id="hiddenLiText">
		<input type="hidden" value="<%=session.getAttribute("menuLiText1")%>"
		id="hiddenLiText1">
<script type="text/javascript">
function gotoLinkInMenu(link)//link
{
	$('body').addClass('white-bg');
	$("#barInMenu").removeClass('hidden');
	$("#wrapper").addClass("hidden");
	window.location.href = link; 
}
$(function() {

	var liText = $('#hiddenLiText').val();
	var secondNav = $('#hiddenLiText1').val();
	$('.moduleNav').each(function(){
		var moduleName=$(this).attr('data-original-title').trim();
		if(moduleName==secondNav){
			var parentLi = $(this).parents('.moduleList');
			parentLi.addClass('active');
			parentLi.find('.collapse').addClass('in');
		}
	});
	
	  $('.nav-label').each(function() {
			var label = $(this).text().trim();
			if (label == liText) {
						var parentLi = $(this).parents('.mainLi');
						parentLi.addClass('active');
						parentLi.find('.nav-second-level').addClass('in');
					}
	}); 
	  $('.nav-label').each(function() {
			
						var parentLi = $(this).parents('.mainLi');
						if(parentLi.hasClass('active')){
							var label = $(this).text().trim();	
						}
	}); 
	  $('.moduleNav').each(function(){
				var parentLi = $(this).parents('.moduleList');
				if(parentLi.hasClass('active')){
					var moduleName=$(this).attr('data-original-title').trim();
				}
			
		});
				

});
	
	$(function () {
		//tooltip
		$('[data-toggle="tooltip"]').tooltip(); 
	});
</script>
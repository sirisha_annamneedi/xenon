<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Application Wise Report for Build</h2>
		<ol class="breadcrumb">
			<li><a href="xedashboard">Home</a></li>
			<li><a href="bmdashboard">Dashboard</a></li>
			<li class="active"><strong>Application Wise Report</strong></li>
		</ol>
	</div>

</div>
<!-- end .page-heading -->

<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-8 col-lg-offset-2">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Bar Chart</h5>
				</div>
				<!-- end .ibox-title -->
				<div class="ibox-content">
					<div>
						<canvas id="barChart" height="140"></canvas>
					</div>
				</div>
				<!-- end .ibox-content -->
			</div>
			<!-- end ibox -->
		</div>
		<!-- end col -->
	</div>
	<!-- end row -->
</div>
<!-- end .wrapper-content -->


<!-- ChartJS-->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/chartJs/Chart.min.js"></script>

<script>
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));

		var barData = {
			labels : [ "Sarvice-Now", "SAP", "Rovi", "Sales-Force" ],
			datasets : [ {
				label : "Passed",
				fillColor : "rgba(220,220,220,0.5)",
				strokeColor : "rgba(220,220,220,0.8)",
				highlightFill : "rgba(220,220,220,0.75)",
				highlightStroke : "rgba(220,220,220,1)",
				data : [ 65, 59, 80, 81 ]
			}, {
				label : "Failed",
				fillColor : "rgba(26,179,148,0.5)",
				strokeColor : "rgba(26,179,148,0.8)",
				highlightFill : "rgba(26,179,148,0.75)",
				highlightStroke : "rgba(26,179,148,1)",
				data : [ 65, 48, 40, 19 ]
			}, {
				label : "Skipped",
				fillColor : "rgba(181,184,207,0.5)",
				strokeColor : "rgba(181,184,207,0.8)",
				highlightFill : "rgba(181,184,207,0.75)",
				highlightStroke : "rgba(181,184,207,1)",
				data : [ 65, 40, 30, 59 ]
			} ]
		};

		var barOptions = {
			scaleBeginAtZero : true,
			scaleShowGridLines : true,
			scaleGridLineColor : "rgba(0,0,0,.05)",
			scaleGridLineWidth : 1,
			barShowStroke : true,
			barStrokeWidth : 2,
			barValueSpacing : 5,
			barDatasetSpacing : 1,
			responsive : true
		}

		var ctx = document.getElementById("barChart").getContext("2d");
		var myNewChart = new Chart(ctx).Bar(barData, barOptions);

	});
</script>

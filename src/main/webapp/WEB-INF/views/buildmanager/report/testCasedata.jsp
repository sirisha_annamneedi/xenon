
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Build Report - ${Model.selectedExecuteBuildName}</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li><a href="builddetails">Build</a></li>
			<li class="active"><strong>Build Report</strong></li>
		</ol>
	</div>
</div>

<c:forEach var="testcasesList" items="${Model.testcasesList}">

	<c:if test ="${Model.filterText == testcasesList.testcase_id }">
	
			<div class="row">
			
				<div class="ibox m-b-sm">
							<div class="ibox">
								<div class="ibox-title">
									<div class="ibox-tools">
								<a class="collapse-link"> <b><i
										class="fa fa-chevron-right pull-left"></i></b>
									<h5>Test case Information</h5>
								</a>
							</div>
								</div>
								<div class="ibox-content">
									<form action="#"
										class="wizard-big wizard clearfix form-horizontal">
										<div class="content clearfix">
											<fieldset class="body current" disabled>
												<div class="row">
													<div class="col-lg-10">
														<div class="form-group">
															<label class="control-label col-sm-2">Name:</label>
															<div class="col-sm-10">
																<input type="text" class="form-control"
																	name="scenarioName"
																	value="${testcasesList.testcase_name }">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label">
																Description:</label>
															<div class="col-sm-10">
																<!-- <textarea class="form-control" style="max-width: 100%;"></textarea> -->
																<textarea name="scenarioDescription"
																	rows="5" class="form-control"
																	style="resize: none;  border: 1px solid #ccc;">${testcasesList.summary } </textarea>
															</div>
														</div>
													</div>
												</div>


											</fieldset>
										</div>
									</form>
									<!-- end form -->
								</div>
							</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Steps</h5>
						</div>

						<div class="ibox-content">

							<div class="table-responsive" style="overflow-x: visible;">
								<table
									class="table table-striped table-bordered table-hover dataTables-example"
									id="testcaseStepListTable_${testcasesList.testcase_id }">
									<thead>
										<tr>
											<td class="hidden">Step Id</td>
											<td style="width: 50px;!important">Step Id</td>

											<td><b>Action</b></td>
											<td><b>Result</b></td>
											<td style="width: 124px;!important"><b>Status</b></td>
											<td style="width: 200px;!important"><b>Comment</b></td>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="testcaseStepsList"
											items="${Model.testcaseStepsList}">
											<c:if
												test="${testcaseStepsList.testcase_id==Model.filterText}">
												<tr>
													<form name="stepSummaryForm_${testcaseStepsList.step_id}"
														id="stepSummaryForm_${testcaseStepsList.step_id}"
														action="insertStepExecSummary" method="post"
														enctype="multipart/form-data">
														<input type="hidden"
															id="stepId_${testcaseStepsList.step_id}"
															value="${testcaseStepsList.step_id}" name="stepId">
														<td class="hidden">${testcaseStepsList.step_id}</td>
														<td name="stepId">${testcaseStepsList.test_step_id}</td>

														<td>${testcaseStepsList.action}</td>
														<td>${testcaseStepsList.result}</td>
														<td class="stepStatus_td" id="stepStatus_td_${testcaseStepsList.step_id}"><select disabled
															id="stepStatus_${testcaseStepsList.step_id}"
															name="stepStatus" class="form-control m-b"
															style="margin-bottom: 0px;">
																<c:forEach var="statusList" items="${Model.statusList}">
																	<option value="${statusList.exst_id}" >${statusList.description}</option>
																</c:forEach>
														</select></td>
														<td>
														<c:forEach var="raised_bug" items="${Model.step_bug}">
															<c:if test="${raised_bug.test_step_id==testcaseStepsList.step_id}">
															    <div id="divBug_"${raised_bug.bug_id}><a href='bugsummarybyprefix?btPrefix=${raised_bug.bug_prefix}' target="_blank"> Bug #${raised_bug.bug_prefix}</a></div>
															</c:if>
														</c:forEach>
														
														<span id="stepComment_${testcaseStepsList.step_id}"></span>
														<%-- <input name="stepComment"
															onkeydown="if (event.keyCode == 13) return false;"
															id="stepComment_${testcaseStepsList.step_id}" type="text" disabled></td> --%>


													</form>
												</tr>
											</c:if>
										</c:forEach>



									</tbody>
								</table>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-2">

										<!-- <button class="btn btn-danger" type="submit">Remove</button> -->
									</div>
								</div>
							</div>

						</div>
					</div>


				</div>
			</div>


			<div class="row" style="pointer-events1:none">
				<div class="col-lg-12">
					<div class="ibox float-e-margins disabled">
						<div class="ibox-title">
							<h5>Enter execution summary</h5>
						</div>

						<div class="ibox-content">

							<div class="content clearfix">
								<div class="row">
									<form id="testcaseSummaryForm_${testcasesList.testcase_id }"
										action="">
										<input type="hidden" name="tcId"
											value="${testcasesList.testcase_id }">
										<div class="form-group">
											<div class="col-sm-6">
												<div class="col-sm-3">
													<label for="comment">Notes :</label>
												</div>
												<div class="col-sm-9">
													<textarea id="notes" name="notes" class="form-control"
														rows="5" style="resize: none;" disabled></textarea>
												</div>
											</div>

											<div class="col-sm-6">
												<div class="col-sm-3">
													<label for="comment">Test case Status</label>
												</div>
												<div class="col-sm-9">

													<c:forEach var="statusList" items="${Model.statusList}"
														varStatus="loop">
														<div class="radio" style="padding-bottom: 1%;">
															<c:if test="${!loop.last}">
																<input type="radio" name="tcStatus"
																	value="${statusList.exst_id }"
																	aria-label="Single radio Two" disabled>
															</c:if>

															<c:if test="${loop.last}">
																<input type="radio" name="tcStatus"
																	value="${statusList.exst_id }"
																	aria-label="Single radio Two" checked disabled>
															</c:if>

															<label>${statusList.description}</label>
														</div>


													</c:forEach>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>



							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-2">
										<button type="button" disabled
											<%-- onclick="saveExecClicked(${testcasesList.testcase_id })" --%>
											class="btn btn-success">Save Execution</button>
									</div>
								</div>
							</div>
							<div class="row">
                             	<div class="col-lg-4">
                             		<table class="table">
                             			<tbody>
                             				<c:forEach var="attachmentsList" items="${Model.attachmentsList}">
                                             <c:if  test="${attachmentsList.testcase_id == testcasesList.testcase_id}">
                                             	<tr>
                                             		<td>${attachmentsList.attachment_title}</td>
                                             		<td><label onclick="downloadFile(${attachmentsList.attach_id},'${attachmentsList.attachment_title}')" class="btn btn-xs btn-white">Download</label></td>
                                             	</tr>
                                                </c:if>
                                            </c:forEach>
                             			</tbody>
                             		</table>
                             	</div>
                             </div>
						</div>
					</div>
				</div>
			</div>
			</c:if>
	</c:forEach>
</div>

<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});
</script>

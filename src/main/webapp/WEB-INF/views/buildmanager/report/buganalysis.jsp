<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Bug Analysis / ${sessionScope.UserCurrentProjectName }</h2>
			<!-- <ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li class="active"><strong>Bug Analysis</strong></li>
		</ol> -->
		</div>
	</div>
	<input type="hidden" id="jsonData" value="" />
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="col-md-3 secondlevel-sidebar">
			<div class="pt-20">
				<form>
					<div class="form-group">
						<label for="columns">Select Columns</label> <br> <select
							class="custom-select" onchange="allFilter(this)">
							<option>Select Column</option>
							<option>Release</option>
							<option>TestSet</option>
							<option>Status</option>
						</select>

					</div>


					<div style="display: none;" id="Status">

						<input type="checkbox" id="col5_regex" checked="checked"
							style="display: none"> <input type="checkbox"
							id="col5_smart" checked="checked" style="display: none">

						<h4>Value</h4>
						<div style="display: none;" id="Status1">
							<select class="select2-multiple2" id="selectedStatus"
								name="selectedStatus" multiple onchange="statusFilter(this)">

							</select>
						</div>
						<div style="display: none;" id="Status2">
							<select class="select2-multiple2" multiple
								onchange="statusFilter(this)">
								<option value="New">New</option>
								<option value="In Progress">In Progress</option>
								<option value="Resolved">Resolved</option>
								<option value="Closed">Closed</option>
								<option value="Reopened">Reopened</option>
							</select>


						</div>
						<BR> <input type="text" id="showingStatus"
						class="form-control" placeholder="Selected Status display here"text-align:center" >
					</div>


					<div style="display: none;" id="Release">
						<input type="checkbox" id="col4_regex" checked="checked"
							style="display: none"> <input type="checkbox"
							id="col4_smart" checked="checked" style="display: none">


						<h4>Value</h4>
						<div>
							<select class="select2-multiple21" name="selectedRelease"
								id="selectedRelease" multiple onchange="releaseFilter(this)">
							</select>
						</div>
						<BR> <input type="text" id="showingRelease"
						class="form-control" placeholder="Selected Release display here"text-align:center">

					</div>


					<div style="display: none;" id="TestSet">

						<input type="checkbox" id="col2_regex" checked="checked"
							style="display: none"> <input type="checkbox"
							id="col2_smart" checked="checked" style="display: none">

						<h4>Value</h4>

						<div style="display: none;" id="Testset1">
							<select class="select2-multiple22" name="selectedTestset1"
								id="selectedTestset1" multiple onchange="testsetFilter(this)">
							</select>
						</div>
						<div style="display: none;" id="Testset2">
							<select class="select2-multiple22" name="selectedTestset"
								id="selectedTestset" multiple onchange="testsetFilter(this)">
							</select>
						</div>
						
					<BR> <input type="text" id="showingTestSet"
						class="form-control" placeholder=" Selected TestSet display here"text-align:center">
					</div>



					
					
					<BR> 
					<input type="button" id="runBtn" disabled value="Run" 
						class="btn btn-primary" />
					<!-- 						<input type="button" id="saveBtn" -->
					<!-- 						value="Save" class="btn btn-primary" /> -->

					<input type="button" id="saveBtn1" disabled value="Save" 
						class="btn btn-primary" /> <BR> <BR> <label
						for="columns">Your Saved Queries</label> <BR>

					<div>
						<select class="select2-multiple33"
							onchange="getQuery(this.value);">
							<c:forEach var="filterVal" items="${Model.getQueryforBugTracker}">
								<option value="${filterVal.query_name}">${filterVal.query_text} </option>
							</c:forEach>
						</select>

					</div>
				</form>
			</div>
		</div>
		<div class="col-md-9 pr-0">
<!-- 			<div class="graph-dashboard"> -->
<!-- 				<div class="col-md-12"> -->
<!-- 					<div class="gd-title">Bar Chart</div> -->
<!-- 				</div> -->
				
<!-- 				<div class="row"> -->
<!-- 					<div class="col-md-12"> -->
<%-- 						<canvas id="bugsReportBarChart" height="140"></canvas> --%>
<%-- 						<canvas id="bugsReportBarChart1" height="140"></canvas> --%>
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
			<%-- <div class="row"> 
			<div class="col-lg-8 col-lg-offset-2">
				<div class="ibox">
					<div class="ibox-title">
						<h5>Bar Chart</h5>
						<div class="ibox-tools">
							<a class="collapse-link">
								<i class="fa fa-chevron-up"></i>
							</a>
						</div><!-- end .ibox-tools -->
					</div><!-- end .ibox-title -->
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-12">
								<canvas id="bugsReportBarChart" height="140"></canvas>
							</div>
						</div>
					</div><!-- end .ibox-content -->
				</div><!-- end .ibox -->
			</div><!-- end .col --> 
		</div><!-- end .row -->--%>
			<div class="row m-b-lg" id="id-filter-ibox-div">
				<div class="col-lg-9">
					<div class="ibox" id="filterIbox">
						<!-- collapsed -->
						<div class="ibox-title">
							<div class="ibox-tools">
								<a class="collapse-link"> <b><i
										class="fa fa-chevron-right pull-left"></i></b>
									<h5>Filters</h5>
								</a>
							</div>

						</div>
						<div class="ibox-content">
							<form action="" class="form-horizontal" method="GET"
								id="filterForm">
								<div class="form-group">
									<label class="col-lg-2 control-label">Release:</label>
									<div class="col-lg-2">
										<select data-placeholder="" class="chosen-select"
											name="releaseState" tabindex="1" id="releaseStatus"
											style="width: 100%;">
											<option value="1" class="is">is</option>
											<option value="2" class="isNot">is not</option>
											<option value="3" class="all">all</option>
										</select>
									</div>
									<div class="col-lg-6">
										<select data-placeholder="Choose a Release" id="releaseSelect"
											class="chosen-select" name="release" tabindex="2"
											style="width: 100%;" multiple>
											<c:forEach var="releases" items="${Model.releaseList}">
												<option value="${releases.release_id}">${releases.release_name}</option>
											</c:forEach>
										</select> <label class="error hidden">This field is required.</label>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Test Set Type:</label>
									<div class="col-lg-2">
										<select data-placeholder="" class="chosen-select"
											name="buildTypeStatus" tabindex="3" id="buildTypeStatus"
											style="width: 100%;">
											<option value="1" class="is">is</option>
											<option value="2" class="isNot">is not</option>
											<option value="3" class="all">all</option>
										</select>
									</div>
									<div class="col-lg-6">
										<select data-placeholder="Choose a Test Set type"
											id="buildTypeValues" class="chosen-select"
											name="buildTypeValues" tabindex="4" style="width: 100%;"
											multiple>
											<c:forEach var="buildExeType" items="${Model.buildExeType}">
												<option value="${buildExeType.execution_type_id}">${buildExeType.description}</option>
											</c:forEach>
										</select> <label class="error hidden">This field is required.</label>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Test Set:</label>
									<div class="col-lg-2">
										<select data-placeholder="" class="chosen-select"
											name="buildState" tabindex="5" style="width: 100%;"
											id="buildStatusSelect">
											<option value="1">is</option>
											<option value="2">is not</option>
											<option value="3">all</option>
										</select>
									</div>
									<div class="col-lg-6">
										<select data-placeholder="Choose a Test Set"
											class="chosen-select" name="build" tabindex="6"
											style="width: 100%;" multiple id="buildNameSelect">
											<c:forEach var="build" items="${Model.buildList}">
												<option value="${build.build_id}"
													class="rel_${build.release_id} hidden buildName manualBuild">${build.build_name}</option>
											</c:forEach>
											<c:forEach var="autoBuild" items="${Model.autoBuildList}">
												<option value="${autoBuild.build_id}"
													class="rel_${autoBuild.release_id} hidden buildName autoBuild">${autoBuild.build_name}</option>
											</c:forEach>
										</select> <label class="error hidden">This field is required.</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 control-label">Status:</label>
									<div class="col-lg-2">
										<select data-placeholder="" class="chosen-select"
											name="statusState" tabindex="7" style="width: 100%;"
											id="statusState">
											<option value="1">is</option>
											<option value="2">is not</option>
											<option value="3">all</option>
										</select>
									</div>
									<div class="col-lg-6">
										<select data-placeholder="Choose a Status"
											class="chosen-select" name="status" tabindex="8"
											style="width: 100%;" multiple id="status">
											<c:forEach var="status" items="${Model.statusList}">
												<option value="${status.status_id}">${status.bug_status}</option>
											</c:forEach>
										</select> <label class="error hidden">This field is required.</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 control-label">Priority:</label>
									<div class="col-lg-2">
										<select data-placeholder="" class="chosen-select"
											name="priorityState" tabindex="9" style="width: 100%;"
											id="priorityState">
											<option value="1">is</option>
											<option value="2">is not</option>
											<option value="3">all</option>
										</select>
									</div>
									<div class="col-lg-6">
										<select data-placeholder="Choose a priority"
											class="chosen-select" name="priority" tabindex="10"
											style="width: 100%;" multiple id="priority">
											<c:forEach var="priority" items="${Model.priorityList}">
												<option value="${priority.priority_id}">${priority.bug_priority}</option>
											</c:forEach>
										</select> <label class="error hidden">This field is required.</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 control-label">Severity:</label>
									<div class="col-lg-2">
										<select data-placeholder="" class="chosen-select"
											name="severityState" tabindex="11" style="width: 100%;"
											id="severityState">
											<option value="1">is</option>
											<option value="2">is not</option>
											<option value="3">all</option>
										</select>
									</div>
									<div class="col-lg-6">
										<select data-placeholder="Choose a severity"
											class="chosen-select" name="severity" tabindex="12"
											style="width: 100%;" multiple id="severity">
											<c:forEach var="severity" items="${Model.severityList}">
												<option value="${severity.severity_id}">${severity.bug_severity}</option>
											</c:forEach>
										</select> <label class="error hidden">This field is required.</label>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<button class="btn btn-white" id="saveBtn" type="button"
											style="margin-right: 15px;">Save</button>
										<button class="btn btn-success" id="executeBtn" type="button">Execute</button>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<button class="btn btn-success hidden" id="editBtn"
											name="editBtn" type="button" style="margin-right: 15px;">Edit</button>
										<button class="btn btn-success hidden"
											id="executeSavedQueryBtn" type="button" name="execute"
											value="1">Execute</button>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<button class="btn btn-success hidden" id="createNewBtn"
											type="button" style="margin-right: 15px;">Create New</button>
										<button class="btn btn-success hidden" id="updateQueryBtn"
											type="button">Update</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<%-- <div class="col-lg-3">
			<div class="ibox">

				<div class="ibox-content">
					<div class="row m-b-sm">
						<div class="col-lg-12">
							<h2>Customised Queries</h2>
						</div>
					</div>
					<div class="client-detail" style="max-height: 276px">
						<div class="full-height-scroll">
							<ul class="list-group clear-list">
								<c:forEach var="query" items="${Model.queryData}">
									<li class="list-group-item"><a onclick="showQuery('${query.query_id}','${query.query_variables}')">${query.query_name}</a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div> --%>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox">
						<div class="ibox-title">
							 <h5>Bug Report</h5> 
						</div>
						<div class="ibox-content" style="overflow-x:auto;max-height: 400px;">
							<table class="display table table-striped table-bordered table-hover dataTables-example" id="table">
								<thead>
									<tr>
										<th>Bug ID</th>
										<th>Bug Title</th>
										<th>Test Set</th>
										<th>Test Set Type</th>
										<th>Release</th>
										<th>Bug Status</th>
										<th>Bug Priority</th>
										<th>Bug Severity</th>
										<th>Reporter</th>
										<th>Create Date</th>
										<th>Update Date</th>
									</tr>
								</thead>
								<tbody>

								</tbody>

							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var dt;
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");

		$(".chosen-select").chosen().trigger('chosen:updated');

	});
	$(function() {
		$(".chosen-select").chosen();
		dt = $('#table').DataTable(
				{
                    //"scrollY": 250,
                    "scroller": true,
                    "pageLength": 1000,

					dom : '<"html5buttons"B>lTfgitp',
					buttons : [
							{
								extend : 'csv',
								title : 'Bug Analysis'
							},
							{
								extend : 'excel',
								title : 'Bug Analysis'
							},
							{
								extend : 'pdf',
								title : 'Bug Analysis'
							},

							{
								extend : 'print',
								customize : function(win) {
									$(win.document.body).addClass('white-bg');
									$(win.document.body).css('font-size',
											'10px');

									$(win.document.body).find('table')
											.addClass('compact').css(
													'font-size', 'inherit');
								}
							} ],
					"aaSorting" : [ [ 0, "desc" ] ]

				});

		$('#id-filter-ibox-div').addClass('hidden');
		if ('${sessionScope.automationExecReleaseId}' != '') {
			// 			$('#releaseStatus').val( 3 ).trigger('chosen:updated');
			// 			$('#releaseSelect option').prop('selected', true).trigger('chosen:updated');
			$('#releaseSelect').val('${sessionScope.automationExecReleaseId}')
					.trigger('chosen:updated');
			$('#buildTypeStatus').val(3).trigger('chosen:updated');
			buildTypeStatusChange();
			$('#executeBtn').trigger('click');
		}
		// 		alert('${sessionScope.automationExecReleaseId}');

// 		var ctx = document.getElementById('bugsReportBarChart')
// 				.getContext('2d');

		/* var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
		        datasets: [{
		            label: '# of Votes',
		            data: [12, 19, 3, 5, 2, 3],
		            backgroundColor: [
		                'rgba(255, 99, 132, 0.2)',
		                'rgba(54, 162, 235, 0.2)',
		                'rgba(255, 206, 86, 0.2)',
		                'rgba(75, 192, 192, 0.2)',
		                'rgba(153, 102, 255, 0.2)',
		                'rgba(255, 159, 64, 0.2)'
		            ],
		            borderColor: [
		                'rgba(255, 99, 132, 1)',
		                'rgba(54, 162, 235, 1)',
		                'rgba(255, 206, 86, 1)',
		                'rgba(75, 192, 192, 1)',
		                'rgba(153, 102, 255, 1)',
		                'rgba(255, 159, 64, 1)'
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		}); */

	});
	var bugCountMap = {
		"New" : 0,
		"Assigned" : 0,
		"In Progress" : 0,
		"Resolved" : 0,
		"Closed" : 0,
		"Reject" : 0,
		"Open" : 0,
		"Reopened" : 0
	};
	function renderBarChart(bugCountMap) {
		var baBarData = {
			labels : [ "New", "In Progress", "Resolved", "Closed", "Reopened" ],//["","","","",""],
			datasets : [ {
				label : "New",
				backgroundColor : '${BugColorNew}',
				hoverBackgroundColor : '${BugColorNew}',
				data : [ bugCountMap["New"], 0, 0, 0, 0 ]
			}, {
				label : "In Progress",
				backgroundColor : '${BugColorInProgress}',
				hoverBackgroundColor : '${BugColorInProgress}',
				data : [ 0, bugCountMap["In Progress"], 0, 0, 0 ]
			}, {
				label : "Resolved",
				backgroundColor : '${BugColorResolved}',
				hoverBackgroundColor : '${BugColorResolved}',
				data : [ 0, 0, bugCountMap["Resolved"], 0, 0 ]
			}, {
				label : "Closed",
				backgroundColor : '${BugColorClosed}',
				hoverBackgroundColor : '${BugColorClosed}',
				data : [ 0, 0, 0, bugCountMap["Closed"], 0 ]
			}, {
				label : "Reopened",
				backgroundColor : '${BugColorReopened}',
				hoverBackgroundColor : '${BugColorReopened}',
				data : [ 0, 0, 0, 0, bugCountMap["Reopened"] ]
			} ]
		};

		var baBarOptions = {
			responsive : true,
			scales : {
				yAxes : [ {
					ticks : {
						min : 0,
						callback : function(value) {
							if (value % 1 === 0) {
								return value;
							}
						}
					}
				} ]
			},
			title : {

				display : true,
				//text: weekday,
				position : 'bottom',
				fontStyle : 'normal'
			}
		};


	}

	//on selecting and deselecting release
	$('#releaseSelect')
			.on(
					'change',
					function() {
						$('#buildStatusSelect option').first().prop('selected',
								true).trigger('chosen:updated');
						var status = $('#releaseStatus').val();
						if (status == 3) {
							$('#releaseStatus option').first().prop('selected',
									true);
							$('#releaseStatus').trigger('chosen:updated');
						}
						//console.log("release status is "+status);
						var relID = $(this).val();
						//remove previously selected values
						$('#buildNameSelect').val('').trigger('chosen:updated');
						$('#buildTypeStatus option').first().prop('selected',
								true).trigger('chosen:updated');
						$('#buildTypeValues').val('').trigger('chosen:updated');
						//1:Add hidden to all build Names
						$("#buildNameSelect .buildName").each(function() {
							$(this).addClass('hidden');
							$('#buildNameSelect').trigger("chosen:updated");
						});
						//2:Show build names for selected Release
						if (status != 2) {
							if (relID != null) {
								for (var i = 0; i < relID.length; i++) {
									$("#buildNameSelect .rel_" + relID[i])
											.each(
													function() {
														$(this).removeClass(
																'hidden');
														$('#buildNameSelect')
																.trigger(
																		"chosen:updated");
													});
								}
							} else {
								$('#buildNameSelect').val('').trigger(
										'chosen:updated');
							}
						} else if (status == 2) {
							//status is release is not
							if (relID != null) {
								for (var i = 0; i < relID.length; i++) {
									$("#buildNameSelect option")
											.each(
													function() {
														if (!$(this)
																.hasClass(
																		'rel_'
																				+ relID[i])) {
															$(this)
																	.removeClass(
																			'hidden');
															$(
																	'#buildNameSelect')
																	.trigger(
																			"chosen:updated");
														}
													});
								}
							} else {
								$('#buildNameSelect').val('').trigger(
										'chosen:updated');
							}
						}
					});

	//On change of release include status
	$('#releaseStatus').on(
			'change',
			function() {
				$('#buildNameSelect').val('').trigger('chosen:updated');
				$('#buildStatusSelect option').first().prop('selected', true)
						.trigger('chosen:updated');
				$('#buildTypeStatus option').first().prop('selected', true)
						.trigger('chosen:updated');
				$('#buildTypeValues').val('').trigger('chosen:updated');
				var relID = $('#releaseSelect').val();
				var status = $(this).val();
				if (status == 1) {
					for (var i = 0; i < relID.length; ++i) {
						$("#buildNameSelect .rel_" + relID[i]).each(function() {
							$(this).removeClass('hidden');
							$('#buildNameSelect').trigger("chosen:updated");
							//console.log("Removed hidden -- > "+$(this).text());
						});
					}
				} else if (status == 2) {
					$("#buildNameSelect option").each(function() {
						$(this).removeClass('hidden');
						$('#buildNameSelect').trigger("chosen:updated");
					});
					for (var i = 0; i < relID.length; i++) {
						$("#buildNameSelect option").each(
								function() {
									if ($(this).hasClass('rel_' + relID[i])) {
										$(this).addClass('hidden');
										$('#buildNameSelect').trigger(
												"chosen:updated");
									}
								});
					}
				} else {
					$('#releaseSelect option').prop('selected', true);
					$('#releaseSelect').trigger('chosen:updated');
					var allRelID = $('#releaseSelect').val();
					for (var i = 0; i < allRelID.length; i++) {
						$("#buildNameSelect .rel_" + allRelID[i]).each(
								function() {
									$(this).removeClass('hidden');
									$('#buildNameSelect').trigger(
											"chosen:updated");
									//console.log("Removed hidden -- > "+$(this).text());
								});
					}
				}
			});

	//on change of buils include status
	$('#buildStatusSelect').on(
			'change',
			function() {
				var status = $(this).val();
				if (status == 3) {
					buildTypeStatusChange();
					$("#buildNameSelect .buildName").each(function() {
						if (!$(this).hasClass('hidden')) {
							$(this).prop('selected', true);
							$('#buildNameSelect').trigger("chosen:updated");
						}
					});
					$('#buildStatusSelect option').last()
							.prop('selected', true).trigger('chosen:updated');
				}
			});

	$('#statusState').on('change', function() {
		var status = $(this).val();
		if (status == 3) {
			$('#status option').prop('selected', true);
			$('#status').trigger('chosen:updated');
		}
	});

	$('#priorityState').on('change', function() {
		var status = $(this).val();
		if (status == 3) {
			$('#priority option').prop('selected', true);
			$('#priority').trigger('chosen:updated');
		}
	});
	$('#severityState').on('change', function() {
		var status = $(this).val();
		if (status == 3) {
			$('#severity option').prop('selected', true);
			$('#severity').trigger('chosen:updated');
		}
	});

	//on save button click
	$('#saveBtn,#createNewBtn')
			.click(
					function() {
						swal(
								{
									title : "Query Name!",
									text : "Write query name:",
									type : "input",
									showCancelButton : true,
									closeOnConfirm : false,
									showLoaderOnConfirm : true,
									animation : "slide-from-top",
									inputPlaceholder : "Write something"
								},
								function(inputValue) {
									if (inputValue === false)
										return false;
									if (inputValue === "") {
										swal
												.showInputError("You need to write something!");
										return false
									}
									/*  swal("Nice!", "You wrote: " + inputValue, "success"); */
									$('#filterForm')
											.append(
													'<input type="hidden" value="'+inputValue+'" name="queryName" />');
									$('#filterForm').attr("action",
											"savebugquery");
									$('#filterForm').attr("method", "POST");
									$('#filterForm').submit();
								});
					});

	//show query
	function showQuery(id, queryVariable) {
		$('.requiredData').each(function() {
			$(this).parents('.form-group').removeClass('has-error');
			$(this).parents('.form-group').find('.error').addClass('hidden');
		});
		//set all dropdown to null
		$('#queryID').remove();
		$('#filterForm').append(
				'<input type="hidden" name="queryID" value="" id="queryID" />');
		$('#queryID').val(id);
		$('#executeBtn').addClass('hidden');
		$('#saveBtn').addClass('hidden');
		$('#createNewBtn').addClass('hidden');
		$('#updateQueryBtn').addClass('hidden');
		$('#editBtn').removeClass('hidden');
		$('#executeSavedQueryBtn').removeClass('hidden');
		$('.chosen-select').each(function() {
			$(this).val('').trigger('chosen:updated');
			$(this).attr("disabled", "true").trigger('chosen:updated');
		});
		//split the string
		//	alert(queryVariable);
		var res = queryVariable.split("#");
		for (var i = 0; i < res.length; i++) {
			//console.log(res[i]);
			displayQuery(res[i])
		}
	};

	//show on choosen-select
	function displayQuery(text) {
		if (text.toLowerCase().indexOf("release") >= 0) {
			//alert(text);
			var startPos = text.toLowerCase().indexOf("(");
			var endPos = text.toLowerCase().indexOf(")");
			var arr = text.substring(startPos + 1, endPos);
			//console.log("My release ID's are ----> "+arr);
			//make multiselected
			$('#releaseSelect option').each(function() {
				if (arr.indexOf($(this).val()) > -1) {
					$(this).prop('selected', true);
					$('#releaseSelect').trigger('chosen:updated');
				}
			});
			if (text.toLowerCase().indexOf("not in") >= 0) {
				$('#releaseStatus option').each(function() {
					if ($(this).val() == 2) {
						$(this).prop('selected', true);
						$('#releaseStatus').trigger('chosen:updated');
					}
				});
			} else {
				$('#releaseStatus option').each(function() {
					if ($(this).val() == 1) {
						$(this).prop('selected', true);
						$('#releaseStatus').trigger('chosen:updated');
					}
				});
			}
		} else if (text.toLowerCase().indexOf("buildtype") >= 0) {
			var startPos = text.toLowerCase().indexOf("(");
			var endPos = text.toLowerCase().indexOf(")");
			var arr = text.substring(startPos + 1, endPos);
			//make multiselected
			$('#buildTypeValues option').each(function() {
				if (arr.indexOf($(this).val()) > -1) {
					$(this).prop('selected', true);
					$('#buildTypeValues').trigger('chosen:updated');
				}
			});
			if (text.toLowerCase().indexOf("not in") >= 0) {
				$('#buildTypeStatus option').each(function() {
					if ($(this).val() == 2) {
						$(this).prop('selected', true);
						$('#buildTypeStatus').trigger('chosen:updated');
					}
				});
			} else {
				$('#buildTypeStatus option').each(function() {
					if ($(this).val() == 1) {
						$(this).prop('selected', true);
						$('#buildTypeStatus').trigger('chosen:updated');
					}
				});
			}
		} else if (text.toLowerCase().indexOf("build") >= 0) {

			var startPos = text.toLowerCase().indexOf("(");
			var endPos = text.toLowerCase().indexOf(")");
			var arr = text.substring(startPos + 1, endPos);
			//console.log("My Build ID's are ----> "+arr);
			//make multiselected
			$('#buildNameSelect option').each(function() {
				if (arr.indexOf($(this).val()) > -1) {
					$(this).prop('selected', true);
					$('#buildNameSelect').trigger('chosen:updated');
				}
			});
			if (text.toLowerCase().indexOf("not in") >= 0) {
				$('#buildStatusSelect option').each(function() {
					if ($(this).val() == 2) {
						$(this).prop('selected', true);
						$('#buildStatusSelect').trigger('chosen:updated');
					}
				});
			} else {
				$('#buildStatusSelect option').each(function() {
					if ($(this).val() == 1) {
						$(this).prop('selected', true);
						$('#buildStatusSelect').trigger('chosen:updated');
					}
				});
			}
		} else if (text.toLowerCase().indexOf("status") >= 0) {

			var startPos = text.toLowerCase().indexOf("(");
			var endPos = text.toLowerCase().indexOf(")");
			var arr = text.substring(startPos + 1, endPos);
			//console.log("My Status ID's are ----> "+arr);
			//make multiselected
			$('#status option').each(function() {
				if (arr.indexOf($(this).val()) > -1) {
					$(this).prop('selected', true);
					$('#status').trigger('chosen:updated');
				}
			});
			if (text.toLowerCase().indexOf("not in") >= 0) {
				$('#statusState option').each(function() {
					if ($(this).val() == 2) {
						$(this).prop('selected', true);
						$('#statusState').trigger('chosen:updated');
					}
				});
			} else {
				$('#statusState option').each(function() {
					if ($(this).val() == 1) {
						$(this).prop('selected', true);
						$('#statusState').trigger('chosen:updated');
					}
				});
			}
		} else if (text.toLowerCase().indexOf("priority") >= 0) {

			var startPos = text.toLowerCase().indexOf("(");
			var endPos = text.toLowerCase().indexOf(")");
			var arr = text.substring(startPos + 1, endPos);
			//console.log("My Priority ID's are ----> "+arr);
			//make multiselected
			$('#priority option').each(function() {
				if (arr.indexOf($(this).val()) > -1) {
					$(this).prop('selected', true);
					$('#priority').trigger('chosen:updated');
				}
			});
			if (text.toLowerCase().indexOf("not in") >= 0) {
				$('#priorityState option').each(function() {
					if ($(this).val() == 2) {
						$(this).prop('selected', true);
						$('#priorityState').trigger('chosen:updated');
					}
				});
			} else {
				$('#priorityState option').each(function() {
					if ($(this).val() == 1) {
						$(this).prop('selected', true);
						$('#priorityState').trigger('chosen:updated');
					}
				});
			}
		} else if (text.toLowerCase().indexOf("severity") >= 0) {

			var startPos = text.toLowerCase().indexOf("(");
			var endPos = text.toLowerCase().indexOf(")");
			var arr = text.substring(startPos + 1, endPos);
			//console.log("My Priority ID's are ----> "+arr);
			//make multiselected
			$('#severity option').each(function() {
				if (arr.indexOf($(this).val()) > -1) {
					$(this).prop('selected', true);
					$('#severity').trigger('chosen:updated');
				}
			});
			if (text.toLowerCase().indexOf("not in") >= 0) {
				$('#severityState option').each(function() {
					if ($(this).val() == 2) {
						$(this).prop('selected', true);
						$('#severityState').trigger('chosen:updated');
					}
				});
			} else {
				$('#severityState option').each(function() {
					if ($(this).val() == 1) {
						$(this).prop('selected', true);
						$('#severityState').trigger('chosen:updated');
					}
				});
			}
		}
	}

	//edit btn 
	$('#editBtn').click(function() {
		$('.chosen-select').each(function() {
			$(this).removeAttr("disabled");
			$(this).trigger('chosen:updated');
		});
		$(this).addClass('hidden');
		$('#executeSavedQueryBtn').addClass('hidden');
		$("#createNewBtn").removeClass("hidden");
		$("#updateQueryBtn").removeClass("hidden");

		//remove the hidden from the builds of previously selected releases and build types.
		//get the selected release Id's
		var relID = $('#releaseSelect').val();
		//get the release status
		var releaseStatus = $('#releaseStatus').val();

		if (releaseStatus == 2) {
			//release status is not in
			if (relID != null) {
				//some releases are selected
				//add the hidden for builds for selected releases
				for (var i = 0; i < relID.length; i++) {
					$("#buildNameSelect option").each(function() {
						if (!$(this).hasClass('rel_' + relID[i])) {
							$(this).removeClass('hidden');
							$('#buildNameSelect').trigger("chosen:updated");
						}
					});
				}
			}
		} else {
			//release status is in
			if (relID != null) {
				//some releases are selected
				//remove the hidden for builds for selected releases
				for (var i = 0; i < relID.length; i++) {
					$("#buildNameSelect .rel_" + relID[i]).each(function() {
						$(this).removeClass('hidden');
						$('#buildNameSelect').trigger("chosen:updated");
					});
				}
			}
		}

		//get the selected build type value
		var buildType = $('#buildTypeValues').val();
		//get the build type status value
		var status = $('#buildTypeStatus').val();

		if ((status == 1) || (status == 3)) {
			//build type status is in
			if (buildType != null) {
				if (buildType.length == 1) {
					//check for the build type
					if (buildType == 1) {
						//manual is selected so hide the automation
						$("#buildNameSelect .autoBuild").each(function() {
							$(this).addClass('hidden');
							$('#buildNameSelect').trigger("chosen:updated");
						});
					} else if (buildType == 2) {
						//automation is selected so hide the manual
						$("#buildNameSelect .manualBuild").each(function() {
							$(this).addClass('hidden');
							$('#buildNameSelect').trigger("chosen:updated");
						});
					}
				} else {
					//as both the build types are selected no need to change the hidden in builds
				}
			}
		} else {
			//build type status is not in
			if (buildType != null) {
				if (buildType == 1) {
					//manual is selected so hide the manual
					$("#buildNameSelect .manualBuild").each(function() {
						$(this).addClass('hidden');
						$('#buildNameSelect').trigger("chosen:updated");
					});
				} else {
					//automation is selected so hide the automation
					$("#buildNameSelect .autoBuild").each(function() {
						$(this).addClass('hidden');
						$('#buildNameSelect').trigger("chosen:updated");
					});
				}
			}
		}
	});

	//execute saved query btn click
	$('#executeSavedQueryBtn')
			.click(
					function() {
						//remove disabled choosen to send values
						$('.chosen-select').each(function() {
							$(this).removeAttr("disabled");
							$(this).trigger('chosen:updated');
						});
						//clear previous table data
						dt.clear().draw();
						var status = true;
						//check for the validity of select
						$('.requiredData')
								.each(
										function() {
											var value = $(this).val();
											if (value == null) {
												$(this).parents('.form-group')
														.addClass('has-error');
												status = false;
												$(this).parents('.form-group')
														.find('.error')
														.removeClass('hidden');
											} else {
												$(this).parents('.form-group')
														.removeClass(
																'has-error');
												$(this).parents('.form-group')
														.find('.error')
														.addClass('hidden');
											}

										});
						//if form is valid
						if (status == true) {
							//get the values 
							var new_rel = getMyValue($('#releaseSelect').val());
							var new_bld = getMyValue($('#buildNameSelect')
									.val());
							var new_stat = getMyValue($('#status').val());
							var new_prio = getMyValue($('#priority').val());
							var new_seve = getMyValue($('#severity').val());
							var new_build_type = getMyValue($(
									'#buildTypeValues').val());

							//add disabled choosen values
							$('.chosen-select').each(function() {
								$(this).attr("disabled", "true");
								$(this).trigger('chosen:updated');
							});

							//post the data
							$
									.post(
											"bugreport",
											{
												releaseState : $(
														'#releaseStatus').val(),
												release : new_rel,
												buildState : $(
														'#buildStatusSelect')
														.val(),
												build : new_bld,
												statusState : $('#statusState')
														.val(),
												status : new_stat,
												priorityState : $(
														'#priorityState').val(),
												priority : new_prio,
												severityState : $(
														'#severityState').val(),
												severity : new_seve,
												buildTypeState : $(
														'#buildTypeStatus')
														.val(),
												buildType : new_build_type
											},
											function(data, status) {
												var arr = JSON.parse(JSON
														.stringify(data));
												if (arr == 0) {
													dt.clear().draw();
												} else {
													$
															.each(
																	arr,
																	function(
																			index,
																			jsonObject) {
																		var rowNode = dt.row
																				.add(
																						[
																							
																								jsonObject.bug_prefix,
																								jsonObject.bug_title,
																								jsonObject.build_name,
																								jsonObject.buildExecutionType,
																								jsonObject.release_name,
																								jsonObject.bugStatus,
																								jsonObject.bug_priority,
																								jsonObject.bug_severity,
																								jsonObject.reporterFN
																										+ " "
																										+ jsonObject.reporterLN,
																								moment(
																										new Date(
																												parseInt(jsonObject.create_date)))
																										.format(
																												"MMM Do YYYY"),
																								moment(
																										new Date(
																												parseInt(jsonObject.create_date)))
																										.format(
																												"MMM Do YYYY") ])
																				.draw()
																				.node();
																		$(
																				rowNode)
																				.css(
																						'cursor',
																						'pointer')
// 																				.attr(
// 																						'onclick',
// 																						'location.href="bugsummarybyprefix?btPrefix='
// 																								+ jsonObject.bug_prefix
// 																								+ '"');
																	});
												}
											});
						} else {
							//add disabled choosen values
							$('.chosen-select').each(function() {
								$(this).attr("disabled", "true");
								$(this).trigger('chosen:updated');
							});
						}
					});

	$('#updateQueryBtn').click(function() {
		$('#filterForm').attr("action", "updatebugquery");
		$('#filterForm').attr("method", "POST");
		$('#filterForm').submit();

	});

	$('#executeBtn')
			.click(
					function() {
						//clear previous table data
						dt.clear().draw();
						var status = true;
						//check for the validity of select
						$('.requiredData')
								.each(
										function() {
											var value = $(this).val();
											if (value == null) {
												$(this).parents('.form-group')
														.addClass('has-error');
												status = false;
												$(this).parents('.form-group')
														.find('.error')
														.removeClass('hidden');
											} else {
												$(this).parents('.form-group')
														.removeClass(
																'has-error');
												$(this).parents('.form-group')
														.find('.error')
														.addClass('hidden');
											}

										});
						//if form is valid
						if (status == true) {
							var new_rel = getMyValue($('#releaseSelect').val());
							var new_bld = getMyValue($('#buildNameSelect')
									.val());
							var new_stat = getMyValue($('#status').val());
							var new_prio = getMyValue($('#priority').val());
							var new_seve = getMyValue($('#severity').val());
							var new_build_type = getMyValue($(
									'#buildTypeValues').val());

							$('body').addClass("white-bg");
							$("#barInMenu").removeClass("hidden");
							$("#wrapper").addClass("hidden");

							$
									.post(
											"bugreport",
											{
												releaseState : $(
														'#releaseStatus').val(),
												release : new_rel,
												buildState : $(
														'#buildStatusSelect')
														.val(),
												build : new_bld,
												statusState : $('#statusState')
														.val(),
												status : new_stat,
												priorityState : $(
														'#priorityState').val(),
												priority : new_prio,
												severityState : $(
														'#severityState').val(),
												severity : new_seve,
												buildTypeState : $(
														'#buildTypeStatus')
														.val(),
												buildType : new_build_type
											},
											function(data, status) {
												//alert(JSON.stringify(data));
												$('body').removeClass(
														"white-bg");
												$("#barInMenu").addClass(
														"hidden");
												$("#wrapper").removeClass(
														"hidden");
												var strRelease = '';
												var strTestset = '';
												var strCount = '';
												var arr = JSON.parse(JSON
														.stringify(data));
												document
														.getElementById("jsonData").value = encodeURIComponent(JSON
														.stringify(data));
												if (arr == 0) {
													dt.clear().draw();
												} else {
													$
															.each(
																	arr,
																	function(
																			index,
																			jsonObject) {
																		// 		    		console.log( jsonObject );
																		var release=jsonObject.release_name;
																		var testSet=jsonObject.build_name;
																		strRelease += "<option value="+release+">"
																				+ jsonObject.release_name
																				+ "</option>";
																		strTestset += "<option value="+testSet+">"
																				+ jsonObject.build_name
																				+ "</option>";
																		var rowNode = dt.row
																				.add(
																						[
																								jsonObject.bug_prefix,
																								jsonObject.bug_title,
																								jsonObject.build_name,
																								jsonObject.buildExecutionType,
																								jsonObject.release_name,
																								jsonObject.bugStatus,
																								jsonObject.bug_priority,
																								jsonObject.bug_severity,
																								jsonObject.reporterFN
																										+ " "
																										+ jsonObject.reporterLN,
																								moment(
																										new Date(
																												parseInt(jsonObject.create_date)))
																										.format(
																												"MMM Do YYYY"),
																								moment(
																										new Date(
																												parseInt(jsonObject.create_date)))
																										.format(
																												"MMM Do YYYY") ])
																				.draw()
																				.node();
																		$(
																				rowNode)
																				.css(
																						'cursor',
																						'pointer')
// 																				.attr(
// 																						'onclick',
// 																						'location.href="bugsummarybyprefix?btPrefix='
// 																								+ jsonObject.bug_prefix
// 																								+ '"');
																		++bugCountMap[jsonObject.bugStatus];
																		// 		    		console.log(jsonObject.bugStatus); 
																	});
													document
															.getElementById("selectedRelease").innerHTML = strRelease;
													document
															.getElementById("selectedTestset").innerHTML = strTestset;
													//document.getElementById("selectedCount").innerHTML=strCount;
													//alert(strRelease);
												}
												renderBarChart(bugCountMap);
											});
						}
					});

	function getMyValue(value) {
		if (value == null)
			return value;
		else
			return value.join(',');
	}

	$('#buildNameSelect').on('change', function() {
		var status = $('#buildStatusSelect').val();
		if (status == 3) {
			$('#buildStatusSelect option').first().prop('selected', true);
			$('#buildStatusSelect').trigger('chosen:updated');
		}
	});

	$('#status').on('change', function() {
		var status = $('#statusState').val();
		if (status == 3) {
			$('#statusState option').first().prop('selected', true);
			$('#statusState').trigger('chosen:updated');
		}
	});

	$('#priority').on('change', function() {
		var status = $('#priorityState').val();
		if (status == 3) {
			$('#priorityState option').first().prop('selected', true);
			$('#priorityState').trigger('chosen:updated');
		}
	});
	$('#severity').on('change', function() {
		var status = $('#severityState').val();
		if (status == 3) {
			$('#severityState option').first().prop('selected', true);
			$('#severityState').trigger('chosen:updated');
		}
	});

	//on change of build type status
	$('#buildTypeStatus').on('change', function() {
		buildTypeStatusChange();
	});

	//on change of build type
	$('#buildTypeValues').on('change', function() {
		var status = $('#buildTypeStatus').val();
		if (status == 3) {
			$('#buildTypeStatus option').first().prop('selected', true);
			$('#buildTypeStatus').trigger('chosen:updated');
		}

		buildTypeStatusChange();
	});

	function buildTypeStatusChange() {
		//get the status value
		var status = $('#buildTypeStatus').val();
		//reset the build selection
		$('#buildNameSelect').val('').trigger('chosen:updated');
		$('#buildStatusSelect option').first().prop('selected', true).trigger(
				'chosen:updated');

		//remove hidden from builds related to selected release
		var relID = $('#releaseSelect').val();
		//get the release status and check
		var releaseStatus = $('#releaseStatus').val();
		if (releaseStatus != 2) {
			$("#buildNameSelect .buildName").each(function() {
				$(this).addClass('hidden');
				$('#buildNameSelect').trigger("chosen:updated");
			});

			if (relID != null) {
				for (var i = 0; i < relID.length; i++) {
					$("#buildNameSelect .rel_" + relID[i]).each(function() {
						$(this).removeClass('hidden');
						$('#buildNameSelect').trigger("chosen:updated");
					});
				}
			}
		} else if (releaseStatus == 2) {
			//remove all hidden
			$("#buildNameSelect .buildName").each(function() {
				$(this).removeClass('hidden');
				$('#buildNameSelect').trigger("chosen:updated");
			});

			if (relID != null) {
				for (var i = 0; i < relID.length; i++) {
					$("#buildNameSelect .rel_" + relID[i]).each(function() {
						$(this).addClass('hidden');
						$('#buildNameSelect').trigger("chosen:updated");
					});
				}
			}

		}
		//get the selected build type value
		var buildType = $('#buildTypeValues').val();
		if ((status == 1) && (buildType != null)) {
			if (buildType.length == 1) {
				//check for the build type
				if (buildType == 1) {
					//manual is selected so hide the automation
					$("#buildNameSelect .autoBuild").each(function() {
						$(this).addClass('hidden');
						$('#buildNameSelect').trigger("chosen:updated");
					});
				} else if (buildType == 2) {
					//automation is selected so hide the manual
					$("#buildNameSelect .manualBuild").each(function() {
						$(this).addClass('hidden');
						$('#buildNameSelect').trigger("chosen:updated");
					});
				}
			} else {
				$('#buildTypeValues option').prop('selected', true).trigger(
						'chosen:updated');
			}
		} else if ((status == 2) && (buildType != null)) {
			if (buildType.length == 1) {
				if (buildType == 1) {
					//manual is selected so hide the manual as status is not in
					$("#buildNameSelect .manualBuild").each(function() {
						$(this).addClass('hidden');
						$('#buildNameSelect').trigger("chosen:updated");
					});
				} else if (buildType == 2) {
					//automation is selected so hide the automation as status is not in
					$("#buildNameSelect .autoBuild").each(function() {
						$(this).addClass('hidden');
						$('#buildNameSelect').trigger("chosen:updated");
					});
				}
			} else {
				//manual and automation build types are selected so hide the manual and automation builds
				$("#buildNameSelect .buildName").each(function() {
					$(this).addClass('hidden');
					$('#buildNameSelect').trigger("chosen:updated");
				});
			}
		} else if (status == 3) {
			//select all build types
			$('#buildTypeValues option').prop('selected', true).trigger(
					'chosen:updated');
		}
	}
</script>
<style>
.md-skin .ibox {
	margin-bottom: 0;
}

.md-skin .wrapper-content {
	padding-bottom: 50px;
	overflow: hidden;
}
</style>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<script>
	$(function() {
		$("#fromDate").datepicker({
			autoclose : true,
			todayHighlight : true
		});
		$("#toDate").datepicker({
			autoclose : true,
			todayHighlight : true
		});
	});

	$(document).ready(function() {
		$(".selectpicker").selectpicker();
		$(".bootstrap-select").click(function() {
			$(this).addClass("open");
		});
	});

	$('.filterColumns')
			.change(
					function() {
						if ($("#" + $(this).val()).length == 0) {
							/* alert($(this).val()); */
							$('#selectColumns')
									.append(
											'<p class="columnsSelectP" id="'
													+ $(this).val().replace(
															/ /g, "")
													+ '">'
													+ $(this).val()
													+ '<a id="'
													+ $(this).val().replace(
															/ /g, "")
													+ '" class="columnsSelect" href="#" style="float:right">X</a></p>');
							$('.columnsSelect').click(function() {
								$('#' + $(this).attr("id")).remove();
							});
						}
					});
	$('.filterContains')
			.change(
					function() {
						if ($("#" + $(this).val()).length == 0) {
							/* alert($(this).val()); */
							$('#selectContains')
									.append(
											'<p class="columnsSelectP" id="'
													+ $(this).val().replace(
															/ /g, "")
													+ '">'
													+ $(this).val()
													+ '<a id="'
													+ $(this).val().replace(
															/ /g, "")
													+ '" class="columnsSelect" href="#" style="float:right">X</a></p>');
							$('.columnsSelect').click(function() {
								$('#' + $(this).attr("id")).remove();
							});
						}
					});
	$('.filterValue')
			.change(
					function() {
						if ($("#" + $(this).val()).length == 0) {
							/* alert($(this).val()); */
							$('#selectValue')
									.append(
											'<p class="columnsSelectP" id="'
													+ $(this).val().replace(
															/ /g, "")
													+ '">'
													+ $(this).val()
													+ '<a id="'
													+ $(this).val().replace(
															/ /g, "")
													+ '" class="columnsSelect" href="#" style="float:right">X</a></p>');
							$('.columnsSelect').click(function() {
								$('#' + $(this).attr("id")).remove();
							});
						}
					});
	$('.filterGroup')
			.change(
					function() {
						if ($("#" + $(this).val()).length == 0) {
							/* alert($(this).val()); */
							$('#selectGroup')
									.append(
											'<p class="columnsSelectP" id="'
													+ $(this).val().replace(
															/ /g, "")
													+ '">'
													+ $(this).val()
													+ '<a id="'
													+ $(this).val().replace(
															/ /g, "")
													+ '" class="columnsSelect" href="#" style="float:right">X</a></p>');
							$('.columnsSelect').click(function() {
								$('#' + $(this).attr("id")).remove();
							});
						}
					});
	/* 
	 function destroy(){
	 //console.log();
	
	 [...document.getElementsById($(this).attr("id"))].map(n => n && n.remove());
	 /* var remove = function(){
	 this.parentNode.remove();
	 };

	 var lis = document.querySelectorAll('columnsSelect');
	 var button = document.querySelectorAll('a');

	 for (var i = 0, len = lis.length; i < len; i++) {
	 button[i].addEventListener('click', remove, false);
	 } 
	 } */
</script>

<script>
	var allData1 = '';
	var allData2 = '';
	var allData3 = '';
	var commonData = '';

	function allFilter(sel) {
		var opts = '', opt;
		var data = JSON.parse(decodeURIComponent(document
				.getElementById("jsonData").value));
		var len = sel.options.length;
		var statusName = '';
		var buildName = '';
		for (var i = 0; i < len; i++) {
			opt = sel.options[i];
			if (opt.selected) {

				opts = opt.value;
				switch (opts) {
				case "Release":
					document.getElementById("Release").style.display = 'block';
					document.getElementById("TestSet").style.display = 'none';
					document.getElementById("Status").style.display = 'none';
					var showingRelease = document
							.getElementById("showingRelease").value;
					var code = {};
					$("select[name='selectedRelease'] > option").each(
							function() {
								if (code[this.text]) {
									$(this).remove();
								} else {
									code[this.text] = this.value;
								}
							});
					break;
				case "TestSet":
					document.getElementById("TestSet").style.display = 'block';
					document.getElementById("Release").style.display = 'none';
					document.getElementById("Status").style.display = 'none';
					var showingTestSet = document
							.getElementById("showingTestSet").value;
					var showingRelease = document
							.getElementById("showingRelease").value;
					if (showingRelease.trim() != "") {
						//document.getElementById("Testset2").style.display = 'none';
						//document.getElementById("Testset1").style.display = 'block';
						document.getElementById("Testset2").style.display = 'block';
						$
								.each(
										data,
										function(index, jsonObject) {
											if (showingRelease.includes(",")) {
												var releaseAry = showingRelease
														.split(",");
												for (var a = 0; a < releaseAry.length; a++) {
													if (releaseAry[a] == jsonObject.release_name) {
														if (buildName != jsonObject.build_name) {
															jQuery(
																	'#selectedTestset1')
																	.append(
																			jQuery(
																					"<option></option>")
																					.val(
																							jsonObject.build_name)
																					.text(
																							jsonObject.build_name));
															buildName = jsonObject.build_name;
														}
													}
												}
											} else {
												if (showingRelease == jsonObject.release_name) {
													if (buildName != jsonObject.build_name) {
														jQuery(
																'#selectedTestset1')
																.append(
																		jQuery(
																				"<option></option>")
																				.val(
																						jsonObject.build_name)
																				.text(
																						jsonObject.build_name));
														buildName = jsonObject.build_name;
													}
												}
											}
										});
						var code = {};
						$("select[name='selectedTestset1'] > option").each(
								function() {
									if (code[this.text]) {
										$(this).remove();
									} else {
										code[this.text] = this.value;
									}
								});
					} else {
						//document.getElementById("Testset1").style.display = 'none';
						document.getElementById("Testset2").style.display = 'block';
					}

					break;
				case "Status":
					document.getElementById("Status").style.display = 'block';
					document.getElementById("TestSet").style.display = 'none';
					document.getElementById("Release").style.display = 'none';
					var showingStatus = document
							.getElementById("showingStatus").value;
					var showingRelease = document
							.getElementById("showingRelease").value;
					if (showingRelease.trim() != "") {
						//document.getElementById("Status2").style.display = 'none';
						//document.getElementById("Status1").style.display = 'block';
						document.getElementById("Status2").style.display = 'block';
						$
								.each(
										data,
										function(index, jsonObject) {
											if (showingRelease.includes(",")) {
												var releaseAry = showingRelease
														.split(",");
												for (var a = 0; a < releaseAry.length; a++) {
													if (releaseAry[a] == jsonObject.release_name) {
														if (statusName != jsonObject.bugStatus) {
															jQuery(
																	'#selectedStatus')
																	.append(
																			jQuery(
																					"<option></option>")
																					.val(
																							jsonObject.bugStatus)
																					.text(
																							jsonObject.bugStatus));
															statusName = jsonObject.bugStatus;
														}
													}
												}
											} else {
												if (showingRelease == jsonObject.release_name) {
													if (statusName != jsonObject.bugStatus) {
														jQuery(
																'#selectedStatus')
																.append(
																		jQuery(
																				"<option></option>")
																				.val(
																						jsonObject.bugStatus)
																				.text(
																						jsonObject.bugStatus));
														statusName = jsonObject.bugStatus;
													}
												}
											}
										});
						var code = {};
						$("select[name='selectedStatus'] > option").each(
								function() {
									if (code[this.text]) {
										$(this).remove();
									} else {
										code[this.text] = this.value;
									}
								});
					} else {
						//document.getElementById("Status1").style.display = 'none';
						document.getElementById("Status2").style.display = 'block';
					}

					break;

				default:

				}
			}

		}
	}

	function statusFilter(sel) {
		var opts = '', opt='', opt1='',opts2='',valOpt ='';
		var len = sel.options.length;

	 var optVal=[];var withoutSpace;

	for (var i = 0; i < len; i++) {
					opt = sel.options[i];

					if (opt.selected) {
						 valOpt=opt.value;
						
						if(valOpt.indexOf(' ') >= 0){
							optVal=valOpt.split(" ");
							
							withoutSpace=optVal[0];
						}else{
							withoutSpace=opt.value;
						}
						if (opts != "") {
							
							opts = opts + "|" + withoutSpace;
							
							opts2=opts2 + "," + valOpt;
							
							

						} 
						else {

							opts = withoutSpace;
							opts2=valOpt;

						}
					}
					
				}
	   
		document.getElementById("Status").value = opts;

		document.getElementById("saveBtn1").disabled = false;
		document.getElementById("runBtn").disabled = false;
		var str1 = document.getElementById("Status").value;
		
		
		var str2 = opts2;
		
		
		document.getElementById("showingStatus").value = str2;
		allData1 = str2 + "|";
		return false;

	}

	function releaseFilter(sel) {
		var opts = '', opt='', opt1='',opts2='',valOpt ='';
		var len = sel.options.length;

	 var optVal=[];var withoutSpace;

	for (var i = 0; i < len; i++) {
					opt = sel.options[i];

					if (opt.selected) {
						 valOpt=opt.value;
						
						if(valOpt.indexOf(' ') >= 0){
							optVal=valOpt.split(" ");
							
							withoutSpace=optVal[0];
						}else{
							withoutSpace=opt.value;
						}
						if (opts != "") {
							
							opts = opts + "|" + withoutSpace;
							
							opts2=opts2 + "," + valOpt;
							
							

						} 
						else {

							opts = withoutSpace;
							opts2=valOpt;

						}
					}
					
				}
	   
		document.getElementById("Release").value = opts;

		document.getElementById("saveBtn1").disabled = false;
		document.getElementById("runBtn").disabled = false;
		var str1 = document.getElementById("Release").value;
		
		
		var str2 = opts2;
		
		
		document.getElementById("showingRelease").value = str2;
		allData2 = str2 + "|";
		return false;
	}

	function testsetFilter(sel) {
		var opts = '', opt='', opt1='',opts2='',valOpt ='';
		var len = sel.options.length;

	 var optVal=[];var withoutSpace;

	for (var i = 0; i < len; i++) {
					opt = sel.options[i];

					if (opt.selected) {
						 valOpt=opt.value;
						
						if(valOpt.indexOf(' ') >= 0){
							optVal=valOpt.split(" ");
							
							withoutSpace=optVal[0];
						}else{
							withoutSpace=opt.value;
						}
						if (opts != "") {
							
							opts = opts + "|" + withoutSpace;
							
							opts2=opts2 + "," + valOpt;
							
							

						} 
						else {

							opts = withoutSpace;
							opts2=valOpt;

						}
					}
					
				}
	   
		document.getElementById("TestSet").value = opts;

		document.getElementById("saveBtn1").disabled = false;
		document.getElementById("runBtn").disabled = false;
		var str1 = document.getElementById("TestSet").value;
		
		
		var str2 = opts2;
		
		
		document.getElementById("showingTestSet").value = str2;
		allData3 = str2 + "|";
		return false;

	}
</script>


<script>
	$(function() {
		$('.button-checkbox')
				.each(
						function() {

							var $widget = $(this), $button = $widget
									.find('button'), $checkbox = $widget
									.find('input:checkbox'), color = $button
									.data('color'), settings = {
								on : {
									icon : 'glyphicon glyphicon-check'
								},
								off : {
									icon : 'glyphicon glyphicon-unchecked'
								}
							};

							$button.on('click', function() {
								$checkbox.prop('checked', !$checkbox
										.is(':checked'));
								$checkbox.triggerHandler('change');
								updateDisplay();
							});
							$checkbox.on('change', function() {
								updateDisplay();
							});

							function updateDisplay() {
								var isChecked = $checkbox.is(':checked');

								$button.data('state', (isChecked) ? "on"
										: "off");

								$button
										.find('.state-icon')
										.removeClass()
										.addClass(
												'state-icon '
														+ settings[$button
																.data('state')].icon);

								if (isChecked) {
									$button.removeClass('btn-default')
											.addClass(
													'btn-' + color + ' active');
								} else {
									$button.removeClass(
											'btn-' + color + ' active')
											.addClass('btn-default');
								}
							}

							function init() {

								updateDisplay();

								if ($button.find('.state-icon').length == 0) {
									$button
											.prepend('<i class="state-icon '
													+ settings[$button
															.data('state')].icon
													+ '"></i>�');
								}
							}
							init();
						});
	});

	jQuery(function($) {
		$.fn.select2.amd.require([ 'select2/selection/single',
				'select2/selection/placeholder',
				'select2/selection/allowClear', 'select2/dropdown',
				'select2/dropdown/search', 'select2/dropdown/attachBody',
				'select2/utils' ],
				function(SingleSelection, Placeholder, AllowClear, Dropdown,
						DropdownSearch, AttachBody, Utils) {

					var SelectionAdapter = Utils.Decorate(SingleSelection,
							Placeholder);

					SelectionAdapter = Utils.Decorate(SelectionAdapter,
							AllowClear);

					var DropdownAdapter = Utils.Decorate(Utils.Decorate(
							Dropdown, DropdownSearch), AttachBody);

					var base_element = $('.select2-multiple2')
					$(base_element)
							.select2(
									{
										placeholder : 'Select multiple items',
										selectionAdapter : SelectionAdapter,
										dropdownAdapter : DropdownAdapter,
										allowClear : true,
										templateResult : function(data) {

											if (!data.id) {
												return data.text;
											}

											var $res = $('<div></div>');

											$res.text(data.text);
											$res.addClass('wrap');

											return $res;
										},
										templateSelection : function(data) {
											if (!data.id) {
												return data.text;
											}
											var selected = ($(base_element)
													.val() || []).length;
											var total = $('option',
													$(base_element)).length;

											return "Select Column ";
										}
									})

				});

	});
</script>


<script>
	$(function() {
		$('.button-checkbox')
				.each(
						function() {

							var $widget = $(this), $button = $widget
									.find('button'), $checkbox = $widget
									.find('input:checkbox'), color = $button
									.data('color'), settings = {
								on : {
									icon : 'glyphicon glyphicon-check'
								},
								off : {
									icon : 'glyphicon glyphicon-unchecked'
								}
							};

							$button.on('click', function() {
								$checkbox.prop('checked', !$checkbox
										.is(':checked'));
								$checkbox.triggerHandler('change');
								updateDisplay();
							});
							$checkbox.on('change', function() {
								updateDisplay();
							});

							function updateDisplay() {
								var isChecked = $checkbox.is(':checked');

								$button.data('state', (isChecked) ? "on"
										: "off");

								$button
										.find('.state-icon')
										.removeClass()
										.addClass(
												'state-icon '
														+ settings[$button
																.data('state')].icon);

								if (isChecked) {
									$button.removeClass('btn-default')
											.addClass(
													'btn-' + color + ' active');
								} else {
									$button.removeClass(
											'btn-' + color + ' active')
											.addClass('btn-default');
								}
							}

							function init() {

								updateDisplay();

								if ($button.find('.state-icon').length == 0) {
									$button
											.prepend('<i class="state-icon '
													+ settings[$button
															.data('state')].icon
													+ '"></i>�');
								}
							}
							init();
						});
	});

	jQuery(function($) {
		$.fn.select2.amd.require([ 'select2/selection/single',
				'select2/selection/placeholder',
				'select2/selection/allowClear', 'select2/dropdown',
				'select2/dropdown/search', 'select2/dropdown/attachBody',
				'select2/utils' ],
				function(SingleSelection, Placeholder, AllowClear, Dropdown,
						DropdownSearch, AttachBody, Utils) {

					var SelectionAdapter = Utils.Decorate(SingleSelection,
							Placeholder);

					SelectionAdapter = Utils.Decorate(SelectionAdapter,
							AllowClear);

					var DropdownAdapter = Utils.Decorate(Utils.Decorate(
							Dropdown, DropdownSearch), AttachBody);

					var base_element = $('.select2-multiple22')
					$(base_element)
							.select2(
									{
										placeholder : 'Select multiple items',
										selectionAdapter : SelectionAdapter,
										dropdownAdapter : DropdownAdapter,
										allowClear : true,
										templateResult : function(data) {

											if (!data.id) {
												return data.text;
											}

											var $res = $('<div></div>');

											$res.text(data.text);
											$res.addClass('wrap');

											return $res;
										},
										templateSelection : function(data) {
											if (!data.id) {
												return data.text;
											}
											var selected = ($(base_element)
													.val() || []).length;
											var total = $('option',
													$(base_element)).length;

											return "You Selected ";
										}
									})

				});

	});
</script>

<script>
	$(function() {
		$('.button-checkbox')
				.each(
						function() {

							var $widget = $(this), $button = $widget
									.find('button'), $checkbox = $widget
									.find('input:checkbox'), color = $button
									.data('color'), settings = {
								on : {
									icon : 'glyphicon glyphicon-check'
								},
								off : {
									icon : 'glyphicon glyphicon-unchecked'
								}
							};

							$button.on('click', function() {
								$checkbox.prop('checked', !$checkbox
										.is(':checked'));
								$checkbox.triggerHandler('change');
								updateDisplay();
							});
							$checkbox.on('change', function() {
								updateDisplay();
							});

							function updateDisplay() {
								var isChecked = $checkbox.is(':checked');

								$button.data('state', (isChecked) ? "on"
										: "off");

								$button
										.find('.state-icon')
										.removeClass()
										.addClass(
												'state-icon '
														+ settings[$button
																.data('state')].icon);

								if (isChecked) {
									$button.removeClass('btn-default')
											.addClass(
													'btn-' + color + ' active');
								} else {
									$button.removeClass(
											'btn-' + color + ' active')
											.addClass('btn-default');
								}
							}

							function init() {

								updateDisplay();

								if ($button.find('.state-icon').length == 0) {
									$button
											.prepend('<i class="state-icon '
													+ settings[$button
															.data('state')].icon
													+ '"></i>�');
								}
							}
							init();
						});
	});

	jQuery(function($) {
		$.fn.select2.amd.require([ 'select2/selection/single',
				'select2/selection/placeholder',
				'select2/selection/allowClear', 'select2/dropdown',
				'select2/dropdown/search', 'select2/dropdown/attachBody',
				'select2/utils' ],
				function(SingleSelection, Placeholder, AllowClear, Dropdown,
						DropdownSearch, AttachBody, Utils) {

					var SelectionAdapter = Utils.Decorate(SingleSelection,
							Placeholder);

					SelectionAdapter = Utils.Decorate(SelectionAdapter,
							AllowClear);

					var DropdownAdapter = Utils.Decorate(Utils.Decorate(
							Dropdown, DropdownSearch), AttachBody);

					var base_element = $('.select2-multiple21')
					$(base_element)
							.select2(
									{
										placeholder : 'Select multiple items',
										selectionAdapter : SelectionAdapter,
										dropdownAdapter : DropdownAdapter,
										allowClear : true,
										templateResult : function(data) {

											if (!data.id) {
												return data.text;
											}

											var $res = $('<div></div>');

											$res.text(data.text);
											$res.addClass('wrap');

											return $res;
										},
										templateSelection : function(data) {
											if (!data.id) {
												return data.text;
											}
											var selected = ($(base_element)
													.val() || []).length;
											var total = $('option',
													$(base_element)).length;

											return "You Selected ";
										}
									})

				});

	});
</script>


<script>
	$("#runBtn").on('click', function() {
		filterColumn1(5);
		filterColumn(4);
		//filterColumn(4);
		filterColumn2(2);
		filterChart();
	});

	function filterColumn1(i) {
		$('#table').DataTable().column(i).search($('#Status').val(),
				$('#col' + i + '_regex').prop('checked'),
				$('#col' + i + '_smart').prop('checked')).draw();
	}

	function filterColumn(i) {
		$('#table').DataTable().column(i).search($('#Release').val(),
				$('#col' + i + '_regex').prop('checked'),
				$('#col' + i + '_smart').prop('checked')).draw();
	}

	function filterColumn2(i) {
		$('#table').DataTable().column(i).search($('#TestSet').val(),
				$('#col' + i + '_regex').prop('checked'),
				$('#col' + i + '_smart').prop('checked')).draw();
	}

	function filterChart() {
// 		document.getElementById("bugsReportBarChart").style.display = 'none';
		var New = 0, Assigned = 0, InProgress = 0, Resolved = 0, Closed = 0, Reopened = 0;
		var myTab = document.getElementById('table');
		
		for (i = 1; i < myTab.rows.length; i++) {
			var objCells = myTab.rows.item(i).cells;
			if (objCells.item('5').innerHTML == "New") {
				New++;
			}
			if (objCells.item('5').innerHTML == "In Progress") {
				InProgress++;
			}
			if (objCells.item('5').innerHTML == "Resolved") {
				Resolved++;
			}
			if (objCells.item('5').innerHTML == "Closed") {
				Closed++;
			}
			if (objCells.item('5').innerHTML == "Reopened") {
				Reopened++;
			}

		}

		//var bugCountMap = {"New":0,"Assigned":0,"In Progress":0,"Resolved":0,"Closed":0,"Reject":0,"Open":0,"Reopened":0};
		var baBarData = {
			labels : [ "New", "In Progress", "Resolved", "Closed", "Reopened" ],//["","","","",""],
			datasets : [ {
				label : "New",
				backgroundColor : '${BugColorNew}',
				hoverBackgroundColor : '${BugColorNew}',
				//data: [bugCountMap["New"],0,0,0,0]
				data : [ New, 0, 0, 0, 0 ]

			},

			{
				label : "In Progress",
				backgroundColor : '${BugColorInProgress}',
				hoverBackgroundColor : '${BugColorInProgress}',
				// data: [0,bugCountMap["In Progress"],0,0,0]
				data : [ 0, InProgress, 0, 0, 0 ]

			}, {
				label : "Resolved",
				backgroundColor : '${BugColorResolved}',
				hoverBackgroundColor : '${BugColorResolved}',
				//data: [0,0,bugCountMap["Resolved"],0,0]
				data : [ 0, 0, Resolved, 0, 0 ]

			}, {
				label : "Closed",
				backgroundColor : '${BugColorClosed}',
				hoverBackgroundColor : '${BugColorClosed}',
				// data: [0,0,0,bugCountMap["Closed"],0]
				data : [ 0, 0, 0, Closed, 0 ]

			}, {
				label : "Reopened",
				backgroundColor : '${BugColorReopened}',
				hoverBackgroundColor : '${BugColorReopened}',
				//data: [0,0,0,0,bugCountMap["Reopened"]]
				data : [ 0, 0, 0, 0, Reopened ]
			} ]
		};

		var baBarOptions = {
			responsive : true,
			scales : {
				yAxes : [ {
					ticks : {
						min : 0,
						callback : function(value) {
							if (value % 1 === 0) {
								return value;
							}
						}
					}
				} ]
			},
			title : {

				display : true,
				//text: weekday,
				position : 'bottom',
				fontStyle : 'normal'
			}
		};

	
	}
</script>


<script>
	$(function() {
		$('.button-checkbox')
				.each(
						function() {

							var $widget = $(this), $button = $widget
									.find('button'), $checkbox = $widget
									.find('input:checkbox'), color = $button
									.data('color'), settings = {
								on : {
									icon : 'glyphicon glyphicon-check'
								},
								off : {
									icon : 'glyphicon glyphicon-unchecked'
								}
							};

							$button.on('click', function() {
								$checkbox.prop('checked', !$checkbox
										.is(':checked'));
								$checkbox.triggerHandler('change');
								updateDisplay();
							});
							$checkbox.on('change', function() {
								updateDisplay();
							});

							function updateDisplay() {
								var isChecked = $checkbox.is(':checked');

								$button.data('state', (isChecked) ? "on"
										: "off");

								$button
										.find('.state-icon')
										.removeClass()
										.addClass(
												'state-icon '
														+ settings[$button
																.data('state')].icon);

								if (isChecked) {
									$button.removeClass('btn-default')
											.addClass(
													'btn-' + color + ' active');
								} else {
									$button.removeClass(
											'btn-' + color + ' active')
											.addClass('btn-default');
								}
							}

							function init() {

								updateDisplay();

								if ($button.find('.state-icon').length == 0) {
									$button
											.prepend('<i class="state-icon '
													+ settings[$button
															.data('state')].icon
													+ '"></i>�');
								}
							}
							init();
						});
	});

	jQuery(function($) {
		$.fn.select2.amd.require([ 'select2/selection/single',
				'select2/selection/placeholder',
				'select2/selection/allowClear', 'select2/dropdown',
				'select2/dropdown/search', 'select2/dropdown/attachBody',
				'select2/utils' ],
				function(SingleSelection, Placeholder, AllowClear, Dropdown,
						DropdownSearch, AttachBody, Utils) {

					var SelectionAdapter = Utils.Decorate(SingleSelection,
							Placeholder);

					SelectionAdapter = Utils.Decorate(SelectionAdapter,
							AllowClear);

					var DropdownAdapter = Utils.Decorate(Utils.Decorate(
							Dropdown, DropdownSearch), AttachBody);

					var base_element = $('.select2-multiple33')
					$(base_element)
							.select2(
									{
										placeholder : 'Select Column',
										selectionAdapter : SelectionAdapter,
										dropdownAdapter : DropdownAdapter,
										allowClear : true,
										templateResult : function(data) {

											if (!data.id) {
												return data.text;
											}

											var $res = $('<div></div>');

											$res.text(data.text);
											$res.addClass('wrap');

											return $res;
										},
										templateSelection : function(data) {
											if (!data.id) {
												return data.text;
											}
											var selected = ($(base_element)
													.val() || []).length;
											var total = $('option',
													$(base_element)).length;

											return data.text;
										}
									})

				});

	});

	function openpopUp() {
		swal({
			title : "Save your query",
			text : "Enter the name of query",
			type : "input",
			showCancelButton : true,
			closeOnConfirm : false,
			animation : "slide-from-left",
			inputPlaceholder : "The name of query"
		},

		function(inputValue) {
			if (inputValue === false)
				return false;
			if (inputValue === "") {
				swal.showInputError("Please enter name of the query!");
				return false
			}
			$('#queryName').val(inputValue);
			var saveFlag = true;
			commonData = $("#showingStatus").val() + "|"
					+ $("#showingRelease").val() + "|"
					+ $("#showingTestSet").val();
			saveOrExecuteQuery(inputValue, commonData);
		});

		function saveOrExecuteQuery(inputValue, commonData) {
			commonData = allData1 + allData2 + allData3;
			//alert(commonData);
			$.ajax({
				type : "POST",
				url : "insertFilterQueryForBugTracker",
				data : {
					inputValue : inputValue,
					commonData : commonData

				}
			}).done(function(response) {
				window.location.href = "buganalysis";
			}).fail(function(jqXHR, testStatus, errorThrown) {
				return false;
			});
		}

	}

	function getQuery(query) {
		
		document.getElementById("runBtn").disabled = false;
		// document.getElementById("saveBtn1").disabled = false;
		
		var res = query.split("|");
		document.getElementById("showingStatus").value = res[0];
		var str1 = document.getElementById("showingStatus").value;
		var str2 = str1.split(",").join("|");
		document.getElementById("Status").value = str2;

		document.getElementById("showingRelease").value = res[1];
		var str1 = document.getElementById("showingRelease").value;
		var str2 = str1.split(",").join("|");
		document.getElementById("Release").value = str2;

		document.getElementById("showingTestSet").value = res[2];
		var str1 = document.getElementById("showingTestSet").value;
		var str2 = str1.split(",").join("|");
		document.getElementById("TestSet").value = str2;
	}

	$("#saveBtn1").on('click', function() {

		openpopUp();

		commonData = allData1 + allData2 + allData3;
		//alert(commonData);
	});
</script>

<style>
#overflowTest {
	color: white;
	padding: 15px;
	width: 100%;
	height: 130px;
	overflow: scroll;
	border: 1px solid #ccc;
}
</style>
<style>
.select2-results__option .wrap:before {
	font-family: fontAwesome;
	color: #999;
	content: "\f096";
	width: 25px;
	height: 25px;
	padding-right: 10px;
}

.select2-results__option[aria-selected=true] .wrap:before {
	content: "\f14a";
}

/* not required css */
.select2-multiple, .select2-multiple2 {
	width: 220%
}

.select2-multiple, .select2-multiple21 {
	width: 220%
}

.select2-multiple, .select2-multiple22 {
	width: 220%
}

.select2-multiple, .select2-multiple33 {
	width: 100%
}
</style>
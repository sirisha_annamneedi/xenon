<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Test Set </h2>
		<ol class="breadcrumb">
			<li><a href="builddetails">View Test Set</a></li>
			<li class="active"><strong>Report</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
	<c:if test="${Model.automationStatus == 1}">
		<div class="tabs-container">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#tab-1">Manual
						Test Set </a></li>
				<li class=""><a data-toggle="tab" href="#tab-2" id="automationBuildTabLink">Automation
						Test Set </a></li>
						<!-- Commenting scriptless tab -->
						<!-- 
						<c:if test="${Model.scriptlessStatus == 1}">
				<li class=""><a data-toggle="tab" href="#tab-3" id="scriptlessBuildTabLink">Scriptless
						Builds</a></li></c:if>
						-->
			</ul>
			<div class="tab-content">
				<div id="tab-1" class="tab-pane active">
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-8">
								<div class="ibox">
									<div class="ibox-content">
										<div class="full-height-scroll">
											<div class="table-responsive" style="overflow-x: visible;">
												<table class="table table-striped table-hover"
													id="allBuildTable">
													<thead>
														<tr>
															<th class="hidden">Build Id</th>
															<th style="width: 250px">Test Set Name</th>
															<th>Test Set State</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="data" items="${allBuildDetails}">
															<tr data-toggle="tab"
																href="#manual_build_${data.build_id}"
																style="cursor: pointer">
																<td class="hidden">${data.build_id}</td>
																<td class="lgTextAlignment" data-original-title="${data.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_name}</td>
																<td class="client-status"><label
																	class=" label ${data.build_state}">${data.buildState}</label></td>
																<td class="project-actions">
																	<button class="btn btn-white btn-xs"
																		onclick="viewBuild(${data.build_id},'View','${data.build_name}')">Report</button>
																</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
							<c:if test="${fn:length(allBuildDetails) > 0}">
								<div class="ibox">
									<div class="ibox-content">
										<div class="tab-content" id="extraBuildDetails">
											<c:forEach var="data" items="${allBuildDetails}">
												<div id="manual_build_${data.build_id}" class="tab-pane">
													<div class="row m-b-lg">
														<div class="col-lg-6 text-center">

															<div class="m-b-sm">
																<img alt="image" class="img-circle "
																	src="data:image/jpg;base64,${data.photo}"
																	style="width: 62px">
															</div>
														</div>
														<div class="col-lg-6">
															<h4
																style="vertical-align: middle; text-align: center; margin-top: 20px;">${data.creator}</h4>
														</div>
													</div>
													<div class="">
														<div class="full-height-scroll">
															<strong>Test Set Information</strong>
															<ul class="list-group clear-list">
																<li class="list-group-item fist-item"><span
																	class="pull-right"> ${data.build_name} </span>Build
																	Name</li>
																<li class="list-group-item "><span
																	class="pull-right label statusLabels">
																		${data.buildStatus} </span>Build Status</li>
																<li class="list-group-item"><span
																	class="pull-right"> ${data.releaseName} </span>Build
																	Release</li>
																<li class="list-group-item"><span
																	class="pull-right"> ${data.build_createdate} </span>Build
																	Create Date</li>
															</ul>
															<strong>Test Set Description </strong>
															<p>${data.build_desc}</p>
														</div>
													</div>
												</div>
											</c:forEach>
										</div>
									</div>
								</div>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			<div id="tab-2" class="tab-pane">
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-8">
							<div class="ibox">
								<div class="ibox-content">
									<div class="full-height-scroll">
										<div class="table-responsive" style="overflow-x: visible;">
											<table class="table table-striped table-hover"
												id="automationBuildTable">
												<thead>
													<tr>
														<th class="hidden">Build Id</th>
														<th style="width: 250px">Test Set Name</th>
														<th>Test Set State</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data" items="${autoBuildDetails}">
														<tr data-toggle="tab" href="#automation_build_${data.build_id}"
															style="cursor: pointer">
															<td class="hidden">${data.build_id}</td>
															<td class="lgTextAlignment" data-original-title="${data.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_name}</td>
															<td class="client-status"><label
																class=" label ${data.build_state}">${data.buildState}</label></td>
															<td class="project-actions">
															<c:if test="${data.build_state==4}">
																
																<button class="btn btn-white btn-xs"
																	onclick="stopBuildExecution(${data.build_id})">Stop</button>
																
															</c:if>
																<button class="btn btn-white btn-xs"
																	onclick="viewAutomationReport(${data.build_id},'View','${data.build_name}')">Report</button>
																<button class="btn btn-white btn-xs"
																	onclick="viewBuildLog(${data.build_id},'View','${data.build_name}','${data.build_logpath}')">Log</button>
															</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<c:if test="${fn:length(autoBuildDetails) > 0}">
								<div class="ibox">
									<div class="ibox-content">
										<div class="tab-content" id="extraBuildDetails">
											<c:forEach var="data" items="${autoBuildDetails}">
												<div id="automation_build_${data.build_id}" class="tab-pane">
													<div class="row m-b-lg">
														<div class="col-lg-6 text-center">

															<div class="m-b-sm">
																<img alt="image" class="img-circle "
																	src="data:image/jpg;base64,${data.photo}"
																	style="width: 62px">
															</div>
														</div>
														<div class="col-lg-6">
															<h4
																style="vertical-align: middle; text-align: center; margin-top: 20px;">${data.creator}</h4>
														</div>
													</div>
													<div class="">
														<div class="full-height-scroll">
															<strong>Test Set Information</strong>
																	<ul class="list-group clear-list">
																		<li class="list-group-item fist-item"><span
																			class="pull-right"> ${data.build_name} </span>Test Set
																			Name</li>
																		<li class="list-group-item "><span
																			class="pull-right label statusLabels">
																				${data.buildStatus} </span>Test Set Status</li>
																		<li class="list-group-item"><span
																			class="pull-right"> ${data.releaseName} </span>Test Set
																			Release</li>
																		<li class="list-group-item"><span
																			class="pull-right"> ${data.build_createdate} </span>Test Set
																			Create Date</li>
																		<li class="list-group-item"><span
																			class="pull-right"> <c:choose>
																					<c:when test="${data.ExecTimeSec % 60==0 }">
																						<strong class="pull-right">${data.ExecTimeSec/60}
																							min</strong>
																					</c:when>

																					<c:when
																						test="${data.ExecTimeSec % 60!=0 }">
																						<fmt:parseNumber var="hour" integerOnly="true"
																							type="number"
																							value="${data.ExecTimeSec/60}" />
																						<strong class="pull-right">${hour} min ${data.ExecTimeSec%60} sec
																						</strong>
																					</c:when>
																				</c:choose> 
																		</span>Build Execution Time</li>


																	</ul>
																	<strong>Build Description</strong>
															<p>${data.build_desc}</p>
														</div>
													</div>
												</div>
											</c:forEach>
										</div>
									</div>
								</div>
								</c:if>
							</div>
						</div>
					</div>
				</div>
				
				
				<div id="tab-3" class="tab-pane">
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-8">
							<div class="ibox">
								<div class="ibox-content">
									<div class="full-height-scroll">
										<div class="table-responsive" style="overflow-x: visible;">
											<table class="table table-striped table-hover"
												id="scriptBuildTable">
												<thead>
													<tr>
														<th class="hidden">Build Id</th>
														<th style="width: 250px">Test Set Name</th>
														<th>Test Set State</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data" items="${scriptBuildDetails}">
														<tr data-toggle="tab" href="#script_build_${data.build_id}"
															style="cursor: pointer">
															<td class="hidden">${data.build_id}</td>
															<td class="lgTextAlignment" data-original-title="${data.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_name}</td>
															<td class="client-status"><label
																class=" label ${data.build_state}">${data.buildState}</label></td>
															<td class="project-actions">
															
																<button class="btn btn-white btn-xs"
																	onclick="viewScriptlessReport(${data.build_id},'View','${data.build_name}')">Report</button>
																<button class="btn btn-white btn-xs"
																	onclick="viewBuildLog(${data.build_id},'View','${data.build_name}','${data.build_logpath}')">Log</button>
															</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<c:if test="${fn:length(scriptBuildDetails) > 0}">
								<div class="ibox">
									<div class="ibox-content">
										<div class="tab-content" id="extraBuildDetails">
											<c:forEach var="data" items="${scriptBuildDetails}">
												<div id="script_build_${data.build_id}" class="tab-pane">
													<div class="row m-b-lg">
														<div class="col-lg-6 text-center">

															<div class="m-b-sm">
																<img alt="image" class="img-circle "
																	src="data:image/jpg;base64,${data.photo}"
																	style="width: 62px">
															</div>
														</div>
														<div class="col-lg-6">
															<h4
																style="vertical-align: middle; text-align: center; margin-top: 20px;">${data.creator}</h4>
														</div>
													</div>
													<div class="">
														<div class="full-height-scroll">
															<strong>Test Set Information</strong>
															<ul class="list-group clear-list">
																<li class="list-group-item fist-item"><span
																	class="pull-right"> ${data.build_name} </span>Build
																	Name</li>
																<li class="list-group-item "><span
																	class="pull-right label statusLabels">
																		${data.buildStatus} </span>Build Status</li>
																<li class="list-group-item"><span
																	class="pull-right"> ${data.releaseName} </span>Build
																	Release</li>
																<li class="list-group-item"><span
																	class="pull-right"> ${data.build_createdate} </span>Build
																	Create Date</li>
															</ul>
															<strong>Test Set Description</strong>
															<p>${data.build_desc}</p>
														</div>
													</div>
												</div>
											</c:forEach>
										</div>
									</div>
								</div>
								</c:if>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>
		</div>
	</c:if>
	<c:if test="${Model.automationStatus != 1}">
		<div class="row">
			<div class="col-sm-8">
				<div class="ibox">
					<div class="ibox-content">
						<div class="full-height-scroll">
							<div class="table-responsive" style="overflow-x: visible;">
								<table class="table table-striped table-hover"
									id="allBuildTable">
									<thead>
										<tr>
											<th class="hidden">Build Id</th>
											<th>Test Set Name</th>
											<th>Test Set State</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="data" items="${allBuildDetails}">
											<tr data-toggle="tab" href="#manual_build_${data.build_id}"
												style="cursor: pointer">
												<td class="hidden">${data.build_id}</td>
												<td>${data.build_name}</td>
												<td class="client-status"><label
													class=" label ${data.build_state}">${data.buildState}</label></td>
												<td class="project-actions">
													<button class="btn btn-white btn-xs"
														onclick="viewBuild(${data.build_id},'View','${data.build_name}')">Report</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<c:if test="${fn:length(allBuildDetails) > 0}">
				<div class="ibox">
					<div class="ibox-content">
						<div class="tab-content" id="extraBuildDetails">
							<c:forEach var="data" items="${allBuildDetails}">
								<div id="manual_build_${data.build_id}" class="tab-pane">
									<div class="row m-b-lg">
										<div class="col-lg-6 text-center">

											<div class="m-b-sm">
												<img alt="image" class="img-circle "
													src="data:image/jpg;base64,${data.photo}"
													style="width: 62px">
											</div>
										</div>
										<div class="col-lg-6">
											<h4
												style="vertical-align: middle; text-align: center; margin-top: 20px;">${data.creator}</h4>
										</div>
									</div>
									<div class="">
										<div class="full-height-scroll">
											<strong>Test Set Information</strong>
											<ul class="list-group clear-list">
												<li class="list-group-item fist-item"><span
													class="pull-right"> ${data.build_name} </span>Build Name</li>
												<li class="list-group-item "><span
													class="pull-right label statusLabels">
														${data.buildStatus} </span>Build Status</li>
												<li class="list-group-item"><span class="pull-right">
														${data.releaseName} </span>Build Release</li>
												<li class="list-group-item"><span class="pull-right">
														${data.build_createdate} </span>Build Create Date</li>
											</ul>
											<strong>Test Set Description</strong>
											<p>${data.build_desc}</p>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
				</c:if>
			</div>
		</div>
	</c:if>

</div>
</div>
<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	var buildType= "<%= request.getParameter("buildType") %>";
	 if(buildType != null){
		 if(buildType == "automation"){
			 $('#automationBuildTabLink').click();
		 }else  if(buildType == "scriptless"){
			 $('#scriptlessBuildTabLink').click();
		 }
	 }
});

  $(function () {
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	  var remoteMachineStatus=${Model.remoteMachineStatus};
	  var messageText="";
	  
	  if(remoteMachineStatus==1)
	  	{
		  messageText="Remote Machine Started!";
	  	}
	  /* else if(remoteMachineStatus==2)
	  	{
		  messageText="Remote Machine Ready For Execution!";
	  	}
	  else if(remoteMachineStatus==3)
	  	{
		  messageText="Remote Machine Execution In Progress!";
	  	} */
	  else if(remoteMachineStatus==4)
	  	{
		  messageText="Remote machine stopped!";
	  	}

	  if(remoteMachineStatus!=0)
		  {
		  swal({
		        title: "",
		        text: messageText,
		        type: "warning",
		        showCancelButton: false,
		        confirmButtonColor: "#DD6B55",
		        confirmButtonText: "Ok!",
		        closeOnConfirm: false
		    }, function () {
		        //swal("Deleted!", "Your imaginary file has been deleted.", "success");
		        <%session.setAttribute("remoteMachineStatus", 0);%>
		    	window.location.href="builddetails";
		    });
		  }
	  	
	});  
</script>
<script>
$(function() {
	var manualBuild = $('#allBuildTable').DataTable({
		dom : '<"html5buttons"B>lTfgitp',
		buttons : [],
		"paging" : false,
		"lengthChange" : false,
		"searching" : false,
		"ordering" : false
	});
	
	var autoBuild = $('#automationBuildTable').DataTable({
		dom : '<"html5buttons"B>lTfgitp',
		buttons : [],
		"paging" : false,
		"lengthChange" : false,
		"searching" : false,
		"ordering" : false
	});
	
	var autoBuild = $('#scriptBuildTable').DataTable({
		dom : '<"html5buttons"B>lTfgitp',
		buttons : [],
		"paging" : false,
		"lengthChange" : false,
		"searching" : false,
		"ordering" : false
	});
	
	$('#extraBuildDetails > :first-child').addClass('active');
	$('.statusLabels').each(function() {
		var status = $.trim($(this).text());
		if (status.toLowerCase() == 'active') {
			$(this).addClass('label-primary');
		} else {
			$(this).addClass('label-danger');
		}
	});
	$('.1').each(function(){
		$(this).addClass('label-success');
	});
	$('.2').each(function(){
		$(this).addClass('label-info');
	});
	$('.3').each(function(){
		$(this).addClass('label-warning');
	});
	$('.4').each(function(){
		$(this).addClass('label-danger');
	});
	$('.5').each(function(){
		$(this).addClass('label-primary');
	});
	
	var allManualRows =  manualBuild.$(".label", {"page": "all"});
	allManualRows.each(function(){
		if($(this).hasClass('1'))
			$(this).addClass('label-success');
		else if($(this).hasClass('2'))
			$(this).addClass('label-info');
		else if($(this).hasClass('3'))
			$(this).addClass('label-warning');
		else if($(this).hasClass('4'))
			$(this).addClass('label-danger');
		else if($(this).hasClass('5'))
			$(this).addClass('label-primary');
	});
	var allAutoRows =  autoBuild.$(".label", {"page": "all"});
	allAutoRows.each(function(){
		if($(this).hasClass('1'))
			$(this).addClass('label-success');
		else if($(this).hasClass('2'))
			$(this).addClass('label-info');
		else if($(this).hasClass('3'))
			$(this).addClass('label-warning');
		else if($(this).hasClass('4'))
			$(this).addClass('label-danger');
		else if($(this).hasClass('5'))
			$(this).addClass('label-primary');
	});
	
	var manual= <%= request.getParameter("manual") %>;
	 if(manual == null){
		 manual = 1;
	 }
	 
	 var auto= <%= request.getParameter("auto") %>;
	 if(auto == null){
		 auto = 1;
	 }
		 
	 var sc= <%= request.getParameter("sc") %>;
	 if(sc == null){
		 sc = 1;
	 }
	 
	 var manualBuildCount='${Model.manualBuildCount}';
	 
	 var pageSize='${Model.pageSize}';
	 
	 var manualLastRec=((manual-1)*pageSize+parseInt(pageSize));

	 if(manualLastRec>manualBuildCount)
	 	manualLastRec=manualBuildCount;
	 var manualShowCount = ((manual-1)*pageSize+1);
	 if(manualShowCount <= manualLastRec){
		 $("#allBuildTable_info").html("Showing "+manualShowCount+" to "+manualLastRec+" of "+manualBuildCount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="manualPrevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="manualNextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
	 }else{
		 $("#allBuildTable_info").html("");
	 }
	 
	 if(manual == 1){
		 $("#manualPrevBtn").attr('disabled','disabled');
	 }
	 
	 if(manualLastRec == manualBuildCount){
		 $("#manualNextBtn").attr('disabled','disabled');
	 }
	 
	    $("#manualPrevBtn").click(function() {
	    	$("#barInMenu").removeClass("hidden");
	    	$("#wrapper").addClass("hidden");
	    	
	    	if(manual == 1)
	    		window.location.href = "builddetails?manual=1&auto="+auto+"&sc="+sc;
	    	else
	    		window.location.href = "builddetails?manual="+(manual - 1)+"&auto="+auto+"&sc="+sc;
		});
		
		$("#manualNextBtn").click(function(){
			$("#barInMenu").removeClass("hidden");
	    	$("#wrapper").addClass("hidden");
	    	
			window.location.href = "builddetails?manual="+(manual + 1)+"&auto="+auto+"&sc="+sc;
		});
		
		/**
		* this is code for automation builds
		*
		*/
		
		var autobuildcount='${Model.autobuildcount}';

		 var pageSize='${Model.pageSize}';
		 
		 var autoLastRec=((auto-1)*pageSize+parseInt(pageSize));

		 if(autoLastRec>autobuildcount)
		 	autoLastRec=autobuildcount;
		 var autoShowCount = ((auto-1)*pageSize+1);
		 if(autoShowCount <= autoLastRec){
			 $("#automationBuildTable_info").html("Showing "+autoShowCount+" to "+autoLastRec+" of "+autobuildcount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="autoPrevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="autoNextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
		 }else{
			 $("#automationBuildTable_info").html("");
		 }
		 
		 if(auto == 1){
			 $("#autoPrevBtn").attr('disabled','disabled');
		 }
		 
		 if(autoLastRec == autobuildcount){
			 $("#autoNextBtn").attr('disabled','disabled');
		 }
		 
		    $("#autoPrevBtn").click(function() {
		    	$("#barInMenu").removeClass("hidden");
		    	$("#wrapper").addClass("hidden");
		    	
		    	if(auto == 1)
		    		window.location.href = "builddetails?manual="+manual+"&auto=1&sc="+sc+"&buildType=automation";
		    	else
		    		window.location.href = "builddetails?manual="+manual+"&auto="+(auto - 1)+"&sc="+sc+"&buildType=automation";
			});
			
			$("#autoNextBtn").click(function(){
				$("#barInMenu").removeClass("hidden");
		    	$("#wrapper").addClass("hidden");
		  		
				window.location.href = "builddetails?manual="+manual+"&auto="+(auto + 1)+"&sc="+sc+"&buildType=automation";
			});
			
			/**
			* this code is for scriptles
			*/
			
			var scbuildcount='${Model.scbuildcount}';

			 var pageSize='${Model.pageSize}';
			 
			 var scLastRec=((sc-1)*pageSize+parseInt(pageSize));

			 if(scLastRec>scbuildcount)
			 	scLastRec=scbuildcount;
			 var scShowCount = ((sc-1)*pageSize+1);
			 if(scShowCount <= scLastRec){
				 $("#scriptBuildTable_info").html("Showing "+scShowCount+" to "+scLastRec+" of "+scbuildcount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="scPrevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="scNextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
			 }else{
				 $("#scriptBuildTable_info").html("");
			 }
			 
			 if(sc == 1){
				 $("#scPrevBtn").attr('disabled','disabled');
			 }
			 
			 if(scLastRec == scbuildcount){
				 $("#scNextBtn").attr('disabled','disabled');
			 }
			 
			    $("#scPrevBtn").click(function() {
			    	$("#barInMenu").removeClass("hidden");
			    	$("#wrapper").addClass("hidden");
			    	
			    	if(sc == 1)
			    		window.location.href = "builddetails?manual="+manual+"&auto="+auto+"&sc=1"+"&buildType=scriptless";
			    	else
			    		window.location.href = "builddetails?manual="+manual+"&auto="+auto+"&sc="+(sc - 1)+"&buildType=scriptless";
				});
				
				$("#scNextBtn").click(function(){
					$("#barInMenu").removeClass("hidden");
			    	$("#wrapper").addClass("hidden");
			    	
					window.location.href = "builddetails?manual="+manual+"&auto="+auto+"&sc="+(sc + 1)+"&buildType=scriptless";
				});
});

function viewBuild(id,viewType,buildName){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	 
	var posting = $.post('setexecutebuildid',{
		buildID : id,
		buildName : buildName
	});
	posting.done(function(data){
		window.location.href="buildreport";
	});
}


function viewBuildLog(id,viewType,buildName,log_Path){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	 
	var posting = $.post('setlogpath',
	{
		logPath : log_Path
	});
	posting.done(function(data){
		window.location.href="exelog";
	});
}

function viewAutomationReport(id){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	 
	 var posting = $.post('setautoexecutebuildid',{
			buildID : id
		});
		posting.done(function(data){
			window.location.href="automationsummary";
		});
}

function viewScriptlessReport(id){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	 
	 var posting = $.post('setscriptreportbuildid',{
			buildID : id
		});
		posting.done(function(data){
			window.location.href="sfdcreport";
		});
}

function stopBuildExecution(id){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	 
	 var posting = $.post('stopbuildexecution',{
			buildID : id
		});
	 
		posting.done(function(data){
			window.location.href="viewbuild";
		});
}
function executeAutomationBuild(buildId,buildName){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setautobuildsession', {
		buildId : buildId,
		buildName:buildName
	});
	
	 posting.done(function(data) {
			window.location.href="autoexecute"; 
	 });
}
function remoteSessionBuild(buildId,buildName){
	
	var posting = $.post('setremoteautobuildsession', {
		buildId : buildId,
		buildName:buildName
	});
	
	 posting.done(function(data) {
		 window.open('remotedesktop');
	 });
}

</script>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>
				Automation Summary / <strong>${UserCurrentProjectName}

				</strong>
<%-- 				<c:forEach var="state" items="${Model.state}"> --%>
<%-- 					<c:choose> --%>
<%-- 						<c:when test="${state.build_state ==1 }"> --%>
<!-- 							<small><label class="label 4 label-success"> -->
<%-- 									${state.desc}</label></small> --%>
<%-- 						</c:when> --%>
<%-- 						<c:when test="${state.build_state ==2 }"> --%>
<!-- 							<small><label class="label 4 label-info"> -->
<%-- 									${state.desc}</label></small> --%>
<%-- 						</c:when> --%>
<%-- 						<c:when test="${state.build_state ==3 }"> --%>
<!-- 							<small><label class="label 4 label-warning"> -->
<%-- 									${state.desc}</label></small> --%>
<%-- 						</c:when> --%>
<%-- 						<c:when test="${state.build_state ==4 }"> --%>
<!-- 							<small><label class="label 4 label-danger"> -->
<%-- 									${state.desc}</label></small> --%>
<%-- 						</c:when> --%>
<%-- 						<c:when test="${state.build_state ==5 }"> --%>
<!-- 							<small><label class="label 4 label-primary"> -->
<%-- 									${state.desc}</label></small> --%>
<%-- 						</c:when> --%>
<%-- 						<c:when test="${state.build_state ==6 }"> --%>
<!-- 							<small><label class="label 4 label-warning"> -->
<!-- 									Ready for Execution</label></small> -->
<%-- 						</c:when> --%>
<%-- 					</c:choose> --%>
<%-- 				</c:forEach> --%>
			</h2>
			<!-- <ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li><a href="builddetails?manual=1&auto=1&sc=1&buildType=automation">View Test Set</a></li>
			<li class="active"><strong>View Report</strong></li>
		</ol> -->
		</div>
	</div>


	<div class="wrapper wrapper-content animated fadeInRight row">
		<div class="col-md-3 secondlevel-sidebar">
			<div class="pt-20">
				<form>
					<div class="form-group">
						<label for="columns">Select Columns</label> <br> <select
							class="custom-select" onchange="allFilter(this)">
							<option>Select Column</option>
							<option>Module</option>
							<option>Release</option>
							<option>Status</option>
							<option>testset</option>
						</select>

					</div>



					<div style="display: none;" id="Status">
						<h4>Value</h4>
						<input type="checkbox" id="col8_regex" checked="checked"
							style="display: none"> 
						<div style="display: none;" id="Status1">
							<select class="select2-multiple2" id="selectedStatus"
								name="selectedStatus" multiple onchange="statusFilter(this)">

							</select>
						</div>
						<div style="display: none;" id="Status2">
							<select class="select2-multiple2" multiple
								onchange="statusFilter(this)">
								<option value="Block">Block</option>
								<option value="Not run">Not Run</option>
								<option value="Fail">Fail</option>
								<option value="Skip">Skip</option>
								<option value="Pass">Pass</option>
							</select>
						</div>
						<BR> <input type="text" id="showingStatus"
						class="form-control" placeholder="Selected Status display here"text-align:center">
					</div>


					<div style="display: none;" id="Module">
						<input type="checkbox" id="col5_regex" checked="checked"
							style="display: none"> <input type="checkbox"
							id="col5_smart" checked="checked" style="display: none">
							<input type="checkbox" id="col2_regex" checked="checked"
							style="display: none"> <input type="checkbox"
							id="col2_smart" checked="checked" style="display: none">

						<h4>Value</h4>

						<div style="display: none;" id="Module1">
							<select class="select2-multiple21" id="selectedModule"
								name="selectedModule" multiple onchange="moduleFilter(this)">
							</select>
						</div>
						<div style="display: none;" id="Module2">
							<select class="select2-multiple21" id="selectedModule1"
								name="selectedModule1" style="display: none;" multiple
								onchange="moduleFilter(this)">
								<c:forEach var="modulefilter"
									items="${Model.ExecutionAytomationQueryFilter}">

									<option value="${modulefilter.module_name}">${modulefilter.module_name}</option>
								</c:forEach>
							</select>
						</div>
						<BR> <input type="text" id="showingModule"
						class="form-control" placeholder="Selected Module display here"text-align:center">
					</div>



					<div style="display: none;" id="Release">
						<input type="checkbox" id="col3_regex" checked="checked"
							style="display: none"> <input type="checkbox"
							id="col3_smart" checked="checked" style="display: none">
							<input type="checkbox" id="col1_regex" checked="checked"
							style="display: none"> <input type="checkbox"
							id="col1_smart" checked="checked" style="display: none">
						<h4>Release</h4>
						<div style="display: none;" id="Release1">
							<select class="select2-multiple21" id="selectedRelease"
								name="selectedModule" multiple onchange="moduleFilter(this)">
							</select>
						</div>
						<div style="display: none;" id="Release2">
							<select class="select2-multiple22" id="selectedRelease"
								name="selectedRelease" multiple onchange="releaseFilter(this)">
								<c:forEach var="modulefilter"
									items="${Model.ExecutionAytomationQueryFilter}">

									<option value="${modulefilter.release_name}">${modulefilter.release_name}</option>
								</c:forEach>
							</select>
						</div>
						<BR> <input type="text" id="showingRelease"
						class="form-control" placeholder=" Selected Release display here"text-align:center">
					</div>

					 <div style="display: none;" id="testset">
                                <input type="checkbox" id="col4_regex" checked="checked"
							style="display: none"> <input type="checkbox"
							id="col4_smart" checked="checked" style="display: none">
							 <input type="checkbox" id="col0_regex" checked="checked"
							style="display: none"> <input type="checkbox"
							id="col0_smart" checked="checked" style="display: none">
								<h4>Testset</h4>
								<div style="display: none;" id="testset1">
									<select class="select2-multiple211" id="selectedTestset"
										name="selectedTestset" multiple onchange="testSetFilter(this)">
									</select>
								</div>
								<div style="display: none;" id="testset2">
									<select class="select2-multiple222" id="selectedTestset1" name="selectedTestset1"
										multiple onchange="testSetFilter(this)">
										<c:forEach var="modulefilter"
											items="${Model.ExecutionAytomationQueryFilter}">

											<option value="${modulefilter.build_name}">${modulefilter.build_name}</option>
										</c:forEach>
									</select>
								</div>
								<BR> <input type="text" id="showingTestset"
								class="form-control"
								placeholder=" Selected Testset display here"text-align:center">
							</div>
					
					

					<input type="button" id="runBtn" disabled value="Run"
						class="btn btn-primary" /> <input type="button" id="saveBtn"
						disabled value="Save" class="btn btn-primary" /> <br> <br>
					<br> <label for="columns">Your Saved Queries</label> <br>


					<div>


						<select class="select2-multiple33"
							onchange="getQuery(this.value);">
							<c:forEach var="filterVal" items="${Model.getQueryforTEATracker}">
								<option value="${filterVal.query_name}">${filterVal.query_text} </option>
							</c:forEach>
						</select>

					</div>
				</form>
			</div>
		</div>


          

		<div class="col-md-9 pr-0">

			<!-- 			<div class="graph-dashboard"> -->
			<!-- 				<div class="row"> -->
			<!-- 					<div class="ibox-content" style="border: none"> -->
			<!-- 						<table id="id-report-table" class="display table table-striped table-bordered table-hover dataTables-example" width="100%" style="table-layout:fixed;"></table> -->
			<!-- 					</div> -->
			<!-- 				</div> -->
			<!-- 			</div> -->
			<div class="graph-dashboard">
			
			  			 <div class="gd-title">Summary Status</div>
				<div class="row">
					<div class="ibox-content" style="overflow-x: visible;">
						<table id="table1" class="table table-striped table-bordered table-hover dataTables-example">
						 	<thead>
												<tr>
													<th>Testset Name</th>
												    <th>Release Name</th>
													<th>Module Name</th>
													<th>Pass Count</th>
													<th>Fail Count</th>
													<th>Skip Count</th>
													<th>Block Count</th>
													<th>Not Run Count</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="data" items="${Model.SummaryExecutionAutomationQueryFilter}">
													<tr class="gradeX">

														<td>${data.build_name}</td>
                                                        <td>${data.release_name}</td>
														<td>${data.module_name}</td>
														<td>${data.Pass}</td>
														<td>${data.Fail}</td>
														<td>${data.Skip}</td>
														<td>${data.Block}</td>
														<td>${data.notrun}</td>
														
													</tr>
												</c:forEach>
											</tbody>

						
						</table>
					</div>
				</div>
			
			     <div class="gd-title">Test Execution-Automation Report</div>
				<div class="row">
					<div class="ibox-content" style="border: none">
						<table id="table"
							class="display table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>

									<th style="display: none">TestCase Name</th>
									<th>Test Prefix</th>
									<th>Project Name</th>
									<th>Release Name</th>
									<th>Test Set</th>
									<th>Module Name</th>
									<th>Scenario Name</th>
									<th style="display: none">Tester Name</th>
									<th>Status</th>

								</tr>
							</thead>
							<tbody>
								<c:forEach var="data"
									items="${Model.ExecutionAytomationQueryFilter}">
									<tr class="gradeX">



										<td style="display: none">${data.testcase_name}</td>
										<td>${data.test_prefix}</td>
										<td>${data.project_name}</td>
										<td>${data.release_name}</td>
										<td>${data.build_name}</td>

										<td>${data.module_name}</td>
										<td>${data.scenario_name}</td>
										<td style="display: none">${data.tester_name}</td>
										<td>${data.description}</td>


									</tr>
								</c:forEach>
							</tbody>


						</table>
					</div>
				</div>
			</div>
		</div>



	</div>
	<script>
var colorPass,colorFail,colorSkip,colorNotRun,colorBlock;
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
	
	function dashLink(link) {
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		var posting = $.post('setTestIdParameter', {
			filterText : link,
			
		});

		posting.done(function(data) {
			window.location.href = "automationtestcase";
		});
	}


	$(document).ready(function() {
		colorPass = "${Model.Pass}";
		colorFail = "${Model.Fail}";
		colorSkip = "${Model.Skip}";
		colorNotRun = "${Model.NotRun}";
		colorBlock = "${Model.Block}";
		
	});
	
	
	
	$(function() {
		$(".chosen-select").chosen();
		dt = $('#table').DataTable(
				{
	                //"scrollY": 250,
	                //"scroller": true,
	                "scrollX": true,
	                

					dom : '<"html5buttons"B>lTfgitp',
					buttons : [
							{
								extend : 'csv',
								title : 'Bug Analysis'
							},
							{
								extend : 'excel',
								title : 'Bug Analysis'
							},
							{
								extend : 'pdf',
								title : 'Bug Analysis'
							},

							{
								extend : 'print',
								customize : function(win) {
									$(win.document.body).addClass('white-bg');
									$(win.document.body).css('font-size',
											'10px');

									$(win.document.body).find('table')
											.addClass('compact').css(
													'font-size', 'inherit');
								}
							} ],
					"aaSorting" : [ [ 0, "desc" ] ]

				});
		
		dt = $('#table1').DataTable(
				{
	                //"scrollY": 250,
	                //"scroller": true,
	                "scrollX": true,
	                

					dom : '<"html5buttons"B>lTfgitp',
					buttons : [
							{
								extend : 'csv',
								title : 'Bug Analysis'
							},
							{
								extend : 'excel',
								title : 'Bug Analysis'
							},
							{
								extend : 'pdf',
								title : 'Bug Analysis'
							},

							{
								extend : 'print',
								customize : function(win) {
									$(win.document.body).addClass('white-bg');
									$(win.document.body).css('font-size',
											'10px');

									$(win.document.body).find('table')
											.addClass('compact').css(
													'font-size', 'inherit');
								}
							} ],
					"aaSorting" : [ [ 0, "desc" ] ]

				});
	});
	
	
	
</script>


	<style>
.float-e-margins .btn {
	margin-bottom: 0;
}

.md-skin .ibox {
	margin-bottom: 0;
}
</style>




	<script>
var allData1 = '';
var allData2 = '';
var allData3 = '';
var allData4 = '';
var commonData = '';
var mod = '';
var sce = '';
function allFilter(sel) {
	
	var opts = '', opt;
	var totalTestJson=${Model.ExecutionAytomationQueryFilter1};
	var moduleName = '';
	var releaseName = '';
	var statusName = '';
	var len = sel.options.length;
	for (var i = 0; i < len; i++) {
		opt = sel.options[i];
		if (opt.selected) {

			opts = opt.value;
			switch (opts) {
			case "Module":
				document.getElementById("Module").style.display = 'block';
				document.getElementById("Release").style.display = 'none';
				document.getElementById("Status").style.display = 'none';
				document.getElementById("testset").style.display = 'none';
				var showingStatus = document
						.getElementById("showingStatus").value;
				
				if (showingStatus.trim() != "") {
					//document.getElementById("Module2").style.display = 'none';
					//document.getElementById("Module1").style.display = 'block';
					document.getElementById("Module2").style.display = 'block';
					for (var j = 0; j < totalTestJson.length; j++) {
						
						if (showingStatus.includes(",")) {
							var statusAry = showingStatus.split(",");
							for (var a = 0; a < statusAry.length; a++) {
								if (statusAry[a] == totalTestJson[a].description) {
								  mod =totalTestJson[j].module_name;
									if (moduleName != mod) {
										jQuery('#selectedModule')
												.append(
														jQuery(
																"<option></option>")
																.val(
																		mod)
																.text(
																		mod));
										moduleName = mod;
									}
								}
							}

						} else {
							
							
							if (showingStatus == totalTestJson[j].description) {
								
								  mod =totalTestJson[j].module_name;	
								 
								  
								if (moduleName != mod) {
									jQuery('#selectedModule')
											.append(
													jQuery(
															"<option></option>")
															.val(
																	mod)
															.text(
																	mod));
									moduleName = mod;
								}
							}
						}
						var code = {};
						$("select[name='selectedModule'] > option")
								.each(function() {
									if (code[this.text]) {
										$(this).remove();
									} else {
										code[this.text] = this.value;
									}
								});
					}

				} else {
					//document.getElementById("Module1").style.display = 'none';
					document.getElementById("Module2").style.display = 'block';
				}

				break;
			case "Release":
				document.getElementById("Release").style.display = 'block';
				document.getElementById("Module").style.display = 'none';
				document.getElementById("Status").style.display = 'none';
				document.getElementById("testset").style.display = 'none';
				var showingStatus = document
						.getElementById("showingStatus").value;
				if (showingStatus.trim() != "") {
					//document.getElementById("Release2").style.display = 'none';
					//document.getElementById("Release1").style.display = 'block';
					document.getElementById("Release2").style.display = 'block';
					for (var j = 0; j < totalTestJson.length; j++) {
						if (showingStatus.includes(",")) {
							var statusAry = showingStatus.split(",");
							for (var a = 0; a < statusAry.length; a++) {
								if (statusAry[a] == totalTestJson[a].description) {
									sce =totalTestJson[j].release_name;
									if (releaseName != sce) {
										jQuery('#selectedRelease')
												.append(
														jQuery(
																"<option></option>")
																.val(
																		sce)
																.text(
																		sce));
										releaseName = sce;
									}
								}

							}

						} else {
							if (showingStatus == totalTestJson[j].description) {
								if (releaseName != sce) {
									jQuery('#selectedRelease')
											.append(
													jQuery(
															"<option></option>")
															.val(
																	sce)
															.text(
																	sce));
									releaseName = sce;
								}
							}
						}
					}
					var code = {};
					$("select[name='selectedRelease'] > option").each(
							function() {
								if (code[this.text]) {
									$(this).remove();
								} else {
									code[this.text] = this.value;
								}
							});
				} else {
					document.getElementById("Release1").style.display = 'none';
					document.getElementById("Release2").style.display = 'block';
				}

				break;
				
			case "testset":
				document.getElementById("testset").style.display = 'block';
				document.getElementById("Release").style.display = 'none';
				document.getElementById("Status").style.display = 'none';
				document.getElementById("Module").style.display = 'none';
				var showingStatus = document
						.getElementById("showingStatus").value;
				//alert(showingStatus);
				if (showingStatus.trim() != "") {
					//document.getElementById("testset2").style.display = 'none';
					//document.getElementById("testset1").style.display = 'block';
					document.getElementById("testset2").style.display = 'block';
					for (var j = 0; j < totalTestJson.length; j++) {
						//alert(JSON.stringify(totalTestJson[0]));
						if (showingStatus.includes(",")) {
							var statusAry = showingStatus.split(",");
							for (var a = 0; a < statusAry.length; a++) {
								if (statusAry[a] == totalTestJson[a].description) {
								  mod =totalTestJson[j].build_name;
									if (moduleName != mod) {
										jQuery('#selectedTestset')
												.append(
														jQuery(
																"<option></option>")
																.val(
																		mod)
																.text(
																		mod));
										moduleName = mod;
									}
								}
							}

						} else {
							//alert(showingStatus);
							//alert(totalTestJson[0].module_name);
							
							if (showingStatus == totalTestJson[j].description) {
								
								  mod =totalTestJson[j].build_name;	
								 // alert(totalTestJson[j].module_name);
								  
								if (moduleName != mod) {
									jQuery('#selectedTestset')
											.append(
													jQuery(
															"<option></option>")
															.val(
																	mod)
															.text(
																	mod));
									moduleName = mod;
								}
							}
						}
						var code = {};
						$("select[name='selectedTestset'] > option")
								.each(function() {
									if (code[this.text]) {
										$(this).remove();
									} else {
										code[this.text] = this.value;
									}
								});
					}

				} else {
					//document.getElementById("testset1").style.display = 'none';
					document.getElementById("testset2").style.display = 'block';
				}

				break;
				
			case "Status":
				document.getElementById("Status").style.display = 'block';
				document.getElementById("Release").style.display = 'none';
				document.getElementById("Module").style.display = 'none';
				document.getElementById("testset").style.display = 'none';
				var showingModule = document
						.getElementById("showingModule").value;
				if (showingModule.trim() != "") {
					//document.getElementById("Status2").style.display = 'none';
					//document.getElementById("Status1").style.display = 'block';
					document.getElementById("Status2").style.display = 'block';
					for (var j = 0; j < totalTestJson.length; j++) {
						if (showingModule.includes(",")) {
							var moduleAry = showingModule.split(",");
							for (var a = 0; a < moduleAry.length; a++) {
								if (moduleAry[a] == totalTestJson[a].module_name) {
									if (statusName != totalTestJson[j].description) {
										jQuery('#selectedStatus')
												.append(
														jQuery(
																"<option></option>")
																.val(
																		totalTestJson[j].description)
																.text(
																		totalTestJson[j].description));
										statusName = totalTestJson[j].description;
									}
								}
							}

						} else {
							if (showingModule == totalTestJson[j].module_name) {
								var exists = false;
								if (statusName != totalTestJson[j].description) {
									jQuery('#selectedStatus')
											.append(
													jQuery(
															"<option></option>")
															.val(
																	totalTestJson[j].description)
															.text(
																	totalTestJson[j].description));
									statusName = totalTestJson[j].description;
								}
							}
						}
					}
					var code = {};
					$("select[name='selectedStatus'] > option").each(
							function() {
								if (code[this.text]) {
									$(this).remove();
								} else {
									code[this.text] = this.value;
								}
							});
				} else {
					//document.getElementById("Status1").style.display = 'none';
					document.getElementById("Status2").style.display = 'block';
				}

				break;

			default:
			}

		}
	}

}


function statusFilter(sel) {
	var opts = '', opt='', opt1='',opts2='',valOpt ='';
	var len = sel.options.length;

 var optVal=[];var withoutSpace;

for (var i = 0; i < len; i++) {
				opt = sel.options[i];

				if (opt.selected) {
					 valOpt=opt.value;
					
					if(valOpt.indexOf(' ') >= 0){
						optVal=valOpt.split(" ");
						
						withoutSpace=optVal[0];
					}else{
						withoutSpace=opt.value;
					}
					if (opts != "") {
						
						opts = opts + "|" + withoutSpace;
						
						opts2=opts2 + "," + valOpt;
						
						

					} 
					
					
					
					else {

						opts = withoutSpace;
						opts2=valOpt;

					}
				}
				
			}
   
	document.getElementById("Status").value = opts;

	document.getElementById("saveBtn").disabled = false;
	document.getElementById("runBtn").disabled = false;
	var str1 = document.getElementById("Status").value;
	
	
	var str2 = opts2;
	
	
	document.getElementById("showingStatus").value = str2;
	allData1 = str2 + "|";
	return false;

}

function testSetFilter(sel) {
	var opts = '', opt='', opt1='',opts2='',valOpt ='';
	var len = sel.options.length;

 var optVal=[];var withoutSpace;

for (var i = 0; i < len; i++) {
				opt = sel.options[i];

				if (opt.selected) {
					var valOpt=opt.value;
					
					if(valOpt.indexOf(' ') >= 0){
						optVal=valOpt.split(" ");
						
						withoutSpace=optVal[0];
					}else{
						withoutSpace=opt.value;
					}
					if (opts != "") {
						
						opts = opts + "|" + withoutSpace;
						
						opts2=opts2 + "," + valOpt;
						
						

					} else {

						opts = withoutSpace;
						opts2=valOpt;

					}
				}
				
			}
	document.getElementById("testset").value = opts;
	document.getElementById("saveBtn").disabled = false;
	document.getElementById("runBtn").disabled = false;
	var str1 = document.getElementById("testset").value;
	var str2 = opts2;
	
	document.getElementById("showingTestset").value = str2;
	allData4 = str2 + "|";
	return false;

}

	
	


function moduleFilter(sel) {

	var opts = '', opt='', opt1='',opts2='',valOpt ='';
	var len = sel.options.length;

 var optVal=[];var withoutSpace;

for (var i = 0; i < len; i++) {
				opt = sel.options[i];

				if (opt.selected) {
					var valOpt=opt.value;
					
					if(valOpt.indexOf(' ') >= 0){
						optVal=valOpt.split(" ");
						
						withoutSpace=optVal[0];
					}else{
						withoutSpace=opt.value;
					}
					if (opts != "") {
						
						opts = opts + "|" + withoutSpace;
						
						opts2=opts2 + "," + valOpt;
						
						

					} else {

						opts = withoutSpace;
						opts2=valOpt;

					}
				}
				
			}
	document.getElementById("Module").value = opts;
	document.getElementById("saveBtn").disabled = false;
	document.getElementById("runBtn").disabled = false;
	var str1 = document.getElementById("Module").value;
	var str2 = opts2;
	
	document.getElementById("showingModule").value = str2;
	allData2 = str2 + "|";
	return false;
}


function releaseFilter(sel) {
	var opts = '', opt='', opt1='',opts2='',valOpt ='';
	var len = sel.options.length;

 var optVal=[];var withoutSpace;

for (var i = 0; i < len; i++) {
				opt = sel.options[i];

				if (opt.selected) {
					var valOpt=opt.value;
					
					if(valOpt.indexOf(' ') >= 0){
						optVal=valOpt.split(" ");
						
						withoutSpace=optVal[0];
					}else{
						withoutSpace=opt.value;
					}
					if (opts != "") {
						
						opts = opts + "|" + withoutSpace;
						
						opts2=opts2 + "," + valOpt;
						
						

					} else {

						opts = withoutSpace;
						opts2=valOpt;

					}
				}
				
			}
	document.getElementById("Release").value = opts;
	document.getElementById("saveBtn").disabled = false;
	document.getElementById("runBtn").disabled = false;
	var str1 = document.getElementById("Release").value;
	var str2 = opts2;
	
	document.getElementById("showingRelease").value = str2;
	allData3 = str2 + "|";
	return false;
}

</script>

		<script>
	$(function() {
		$('.button-checkbox')
				.each(
						function() {

							
							var $widget = $(this), $button = $widget
									.find('button'), $checkbox = $widget
									.find('input:checkbox'), color = $button
									.data('color'), settings = {
								on : {
									icon : 'glyphicon glyphicon-check'
								},
								off : {
									icon : 'glyphicon glyphicon-unchecked'
								}
							};

							
							$button.on('click', function() {
								$checkbox.prop('checked', !$checkbox
										.is(':checked'));
								$checkbox.triggerHandler('change');
								updateDisplay();
							});
							$checkbox.on('change', function() {
								updateDisplay();
							});

							
							function updateDisplay() {
								var isChecked = $checkbox.is(':checked');

								
								$button.data('state', (isChecked) ? "on"
										: "off");

								
								$button
										.find('.state-icon')
										.removeClass()
										.addClass(
												'state-icon '
														+ settings[$button
																.data('state')].icon);

								
								if (isChecked) {
									$button.removeClass('btn-default')
											.addClass(
													'btn-' + color
															+ ' active');
								} else {
									$button.removeClass(
											'btn-' + color + ' active')
											.addClass('btn-default');
								}
							}

							
							function init() {

								updateDisplay();

								
								if ($button.find('.state-icon').length == 0) {
									$button.prepend('<i class="state-icon '
											+ settings[$button
													.data('state')].icon
											+ '"></i>�');
								}
							}
							init();
						});
	});

	jQuery(function($) {
		$.fn.select2.amd
				.require(
						[ 'select2/selection/single',
								'select2/selection/placeholder',
								'select2/selection/allowClear',
								'select2/dropdown',
								'select2/dropdown/search',
								'select2/dropdown/attachBody',
								'select2/utils' ],
						function(SingleSelection, Placeholder, AllowClear,
								Dropdown, DropdownSearch, AttachBody, Utils) {

							var SelectionAdapter = Utils.Decorate(
									SingleSelection, Placeholder);

							SelectionAdapter = Utils.Decorate(
									SelectionAdapter, AllowClear);

							var DropdownAdapter = Utils.Decorate(Utils
									.Decorate(Dropdown, DropdownSearch),
									AttachBody);

							var base_element = $('.select2-multiple211')
							$(base_element)
									.select2(
											{
												placeholder : 'Select multiple items',
												selectionAdapter : SelectionAdapter,
												dropdownAdapter : DropdownAdapter,
												allowClear : true,
												templateResult : function(
														data) {

													if (!data.id) {
														return data.text;
													}

													var $res = $('<div></div>');

													$res.text(data.text);
													$res.addClass('wrap');

													return $res;
												},
												templateSelection : function(
														data) {
													if (!data.id) {
														return data.text;
													}
													var selected = ($(
															base_element)
															.val() || []).length;
													var total = $('option',
															$(base_element)).length;
													
													return "You Selected ";
												}
											})

						});

	});
	</script>
	
		<script>
	$(function() {
		$('.button-checkbox')
				.each(
						function() {

							
							var $widget = $(this), $button = $widget
									.find('button'), $checkbox = $widget
									.find('input:checkbox'), color = $button
									.data('color'), settings = {
								on : {
									icon : 'glyphicon glyphicon-check'
								},
								off : {
									icon : 'glyphicon glyphicon-unchecked'
								}
							};

							
							$button.on('click', function() {
								$checkbox.prop('checked', !$checkbox
										.is(':checked'));
								$checkbox.triggerHandler('change');
								updateDisplay();
							});
							$checkbox.on('change', function() {
								updateDisplay();
							});

							
							function updateDisplay() {
								var isChecked = $checkbox.is(':checked');

								
								$button.data('state', (isChecked) ? "on"
										: "off");

								
								$button
										.find('.state-icon')
										.removeClass()
										.addClass(
												'state-icon '
														+ settings[$button
																.data('state')].icon);

								
								if (isChecked) {
									$button.removeClass('btn-default')
											.addClass(
													'btn-' + color
															+ ' active');
								} else {
									$button.removeClass(
											'btn-' + color + ' active')
											.addClass('btn-default');
								}
							}

							
							function init() {

								updateDisplay();

								
								if ($button.find('.state-icon').length == 0) {
									$button.prepend('<i class="state-icon '
											+ settings[$button
													.data('state')].icon
											+ '"></i>�');
								}
							}
							init();
						});
	});

	jQuery(function($) {
		$.fn.select2.amd
				.require(
						[ 'select2/selection/single',
								'select2/selection/placeholder',
								'select2/selection/allowClear',
								'select2/dropdown',
								'select2/dropdown/search',
								'select2/dropdown/attachBody',
								'select2/utils' ],
						function(SingleSelection, Placeholder, AllowClear,
								Dropdown, DropdownSearch, AttachBody, Utils) {

							var SelectionAdapter = Utils.Decorate(
									SingleSelection, Placeholder);

							SelectionAdapter = Utils.Decorate(
									SelectionAdapter, AllowClear);

							var DropdownAdapter = Utils.Decorate(Utils
									.Decorate(Dropdown, DropdownSearch),
									AttachBody);

							var base_element = $('.select2-multiple222')
							$(base_element)
									.select2(
											{
												placeholder : 'Select multiple items',
												selectionAdapter : SelectionAdapter,
												dropdownAdapter : DropdownAdapter,
												allowClear : true,
												templateResult : function(
														data) {

													if (!data.id) {
														return data.text;
													}

													var $res = $('<div></div>');

													$res.text(data.text);
													$res.addClass('wrap');

													return $res;
												},
												templateSelection : function(
														data) {
													if (!data.id) {
														return data.text;
													}
													var selected = ($(
															base_element)
															.val() || []).length;
													var total = $('option',
															$(base_element)).length;
													
													return "You Selected ";
												}
											})

						});

	});
	</script>

	<script>
		$(function() {
			$('.button-checkbox')
					.each(
							function() {

								
								var $widget = $(this), $button = $widget
										.find('button'), $checkbox = $widget
										.find('input:checkbox'), color = $button
										.data('color'), settings = {
									on : {
										icon : 'glyphicon glyphicon-check'
									},
									off : {
										icon : 'glyphicon glyphicon-unchecked'
									}
								};

								
								$button.on('click', function() {
									$checkbox.prop('checked', !$checkbox
											.is(':checked'));
									$checkbox.triggerHandler('change');
									updateDisplay();
								});
								$checkbox.on('change', function() {
									updateDisplay();
								});

								
								function updateDisplay() {
									var isChecked = $checkbox.is(':checked');

									
									$button.data('state', (isChecked) ? "on"
											: "off");

									
									$button
											.find('.state-icon')
											.removeClass()
											.addClass(
													'state-icon '
															+ settings[$button
																	.data('state')].icon);

									
									if (isChecked) {
										$button.removeClass('btn-default')
												.addClass(
														'btn-' + color
																+ ' active');
									} else {
										$button.removeClass(
												'btn-' + color + ' active')
												.addClass('btn-default');
									}
								}

								
								function init() {

									updateDisplay();

									
									if ($button.find('.state-icon').length == 0) {
										$button.prepend('<i class="state-icon '
												+ settings[$button
														.data('state')].icon
												+ '"></i>�');
									}
								}
								init();
							});
		});

		jQuery(function($) {
			$.fn.select2.amd
					.require(
							[ 'select2/selection/single',
									'select2/selection/placeholder',
									'select2/selection/allowClear',
									'select2/dropdown',
									'select2/dropdown/search',
									'select2/dropdown/attachBody',
									'select2/utils' ],
							function(SingleSelection, Placeholder, AllowClear,
									Dropdown, DropdownSearch, AttachBody, Utils) {

								var SelectionAdapter = Utils.Decorate(
										SingleSelection, Placeholder);

								SelectionAdapter = Utils.Decorate(
										SelectionAdapter, AllowClear);

								var DropdownAdapter = Utils.Decorate(Utils
										.Decorate(Dropdown, DropdownSearch),
										AttachBody);

								var base_element = $('.select2-multiple2')
								$(base_element)
										.select2(
												{
													placeholder : 'Select multiple items',
													selectionAdapter : SelectionAdapter,
													dropdownAdapter : DropdownAdapter,
													allowClear : true,
													templateResult : function(
															data) {

														if (!data.id) {
															return data.text;
														}

														var $res = $('<div></div>');

														$res.text(data.text);
														$res.addClass('wrap');

														return $res;
													},
													templateSelection : function(
															data) {
														if (!data.id) {
															return data.text;
														}
														var selected = ($(
																base_element)
																.val() || []).length;
														var total = $('option',
																$(base_element)).length;
														
														return "You Selected ";
													}
												})

							});

		});
	</script>
	<script>
	$(function() {
		$('.button-checkbox')
				.each(
						function() {

							
							var $widget = $(this), $button = $widget
									.find('button'), $checkbox = $widget
									.find('input:checkbox'), color = $button
									.data('color'), settings = {
								on : {
									icon : 'glyphicon glyphicon-check'
								},
								off : {
									icon : 'glyphicon glyphicon-unchecked'
								}
							};

							
							$button.on('click', function() {
								$checkbox.prop('checked', !$checkbox
										.is(':checked'));
								$checkbox.triggerHandler('change');
								updateDisplay();
							});
							$checkbox.on('change', function() {
								updateDisplay();
							});

							
							function updateDisplay() {
								var isChecked = $checkbox.is(':checked');

								
								$button.data('state', (isChecked) ? "on"
										: "off");

								
								$button
										.find('.state-icon')
										.removeClass()
										.addClass(
												'state-icon '
														+ settings[$button
																.data('state')].icon);

								
								if (isChecked) {
									$button.removeClass('btn-default')
											.addClass(
													'btn-' + color
															+ ' active');
								} else {
									$button.removeClass(
											'btn-' + color + ' active')
											.addClass('btn-default');
								}
							}

							
							function init() {

								updateDisplay();

								
								if ($button.find('.state-icon').length == 0) {
									$button.prepend('<i class="state-icon '
											+ settings[$button
													.data('state')].icon
											+ '"></i>�');
								}
							}
							init();
						});
	});

	jQuery(function($) {
		$.fn.select2.amd
				.require(
						[ 'select2/selection/single',
								'select2/selection/placeholder',
								'select2/selection/allowClear',
								'select2/dropdown',
								'select2/dropdown/search',
								'select2/dropdown/attachBody',
								'select2/utils' ],
						function(SingleSelection, Placeholder, AllowClear,
								Dropdown, DropdownSearch, AttachBody, Utils) {

							var SelectionAdapter = Utils.Decorate(
									SingleSelection, Placeholder);

							SelectionAdapter = Utils.Decorate(
									SelectionAdapter, AllowClear);

							var DropdownAdapter = Utils.Decorate(Utils
									.Decorate(Dropdown, DropdownSearch),
									AttachBody);

							var base_element = $('.select2-multiple21')
							$(base_element)
									.select2(
											{
												placeholder : 'Select multiple items',
												selectionAdapter : SelectionAdapter,
												dropdownAdapter : DropdownAdapter,
												allowClear : true,
												templateResult : function(
														data) {

													if (!data.id) {
														return data.text;
													}

													var $res = $('<div></div>');

													$res.text(data.text);
													$res.addClass('wrap');

													return $res;
												},
												templateSelection : function(
														data) {
													if (!data.id) {
														return data.text;
													}
													var selected = ($(
															base_element)
															.val() || []).length;
													var total = $('option',
															$(base_element)).length;
													
													return "You Selected ";
												}
											})

						});

	});
	</script>

	<script>
	$(function() {
		$('.button-checkbox')
				.each(
						function() {

							
							var $widget = $(this), $button = $widget
									.find('button'), $checkbox = $widget
									.find('input:checkbox'), color = $button
									.data('color'), settings = {
								on : {
									icon : 'glyphicon glyphicon-check'
								},
								off : {
									icon : 'glyphicon glyphicon-unchecked'
								}
							};

							
							$button.on('click', function() {
								$checkbox.prop('checked', !$checkbox
										.is(':checked'));
								$checkbox.triggerHandler('change');
								updateDisplay();
							});
							$checkbox.on('change', function() {
								updateDisplay();
							});

							
							function updateDisplay() {
								var isChecked = $checkbox.is(':checked');

								
								$button.data('state', (isChecked) ? "on"
										: "off");

								
								$button
										.find('.state-icon')
										.removeClass()
										.addClass(
												'state-icon '
														+ settings[$button
																.data('state')].icon);

								
								if (isChecked) {
									$button.removeClass('btn-default')
											.addClass(
													'btn-' + color
															+ ' active');
								} else {
									$button.removeClass(
											'btn-' + color + ' active')
											.addClass('btn-default');
								}
							}

							
							function init() {

								updateDisplay();

								
								if ($button.find('.state-icon').length == 0) {
									$button.prepend('<i class="state-icon '
											+ settings[$button
													.data('state')].icon
											+ '"></i>�');
								}
							}
							init();
						});
	});

	jQuery(function($) {
		$.fn.select2.amd
				.require(
						[ 'select2/selection/single',
								'select2/selection/placeholder',
								'select2/selection/allowClear',
								'select2/dropdown',
								'select2/dropdown/search',
								'select2/dropdown/attachBody',
								'select2/utils' ],
						function(SingleSelection, Placeholder, AllowClear,
								Dropdown, DropdownSearch, AttachBody, Utils) {

							var SelectionAdapter = Utils.Decorate(
									SingleSelection, Placeholder);

							SelectionAdapter = Utils.Decorate(
									SelectionAdapter, AllowClear);

							var DropdownAdapter = Utils.Decorate(Utils
									.Decorate(Dropdown, DropdownSearch),
									AttachBody);

							var base_element = $('.select2-multiple22')
							$(base_element)
									.select2(
											{
												placeholder : 'Select multiple items',
												selectionAdapter : SelectionAdapter,
												dropdownAdapter : DropdownAdapter,
												allowClear : true,
												templateResult : function(
														data) {

													if (!data.id) {
														return data.text;
													}

													var $res = $('<div></div>');

													$res.text(data.text);
													$res.addClass('wrap');

													return $res;
												},
												templateSelection : function(
														data) {
													if (!data.id) {
														return data.text;
													}
													var selected = ($(
															base_element)
															.val() || []).length;
													var total = $('option',
															$(base_element)).length;
													
													return "You Selected ";
												}
											})

						});

	});
	</script>


	<script>
	
	$("#runBtn").on('click', function() {
		filterColumn1(5);
		filterColumn2(3);
		filterColumn(8);
		filterColumn4(4);
		

	});
	
	function filterColumn(i) {
		$('#table').DataTable().column(i).search($('#Status').val(),
				$('#col' + i + '_regex').prop('checked'),
				$('#col' + i + '_smart').prop('checked')).draw();

	}
	
	function filterColumn1(i) {
		$('#table').DataTable().column(i).search($('#Module').val(),
				$('#col' + i + '_regex').prop('checked'),
				$('#col' + i + '_smart').prop('checked')).draw(),
		
		$('#table1').DataTable().column(2).search($('#Module').val(),
				$('#col2_regex').prop('checked'),
				$('#col2_smart').prop('checked')).draw();
	}
	
	function filterColumn2(i) {
		$('#table').DataTable().column(i).search($('#Release').val(),
				$('#col' + i + '_regex').prop('checked'),
				$('#col' + i + '_smart').prop('checked')).draw(),
				
				$('#table1').DataTable().column(1).search($('#Release').val(),
						$('#col1_regex').prop('checked'),
						$('#col1_smart').prop('checked')).draw();
				

	}
	
	function filterColumn4(i) {
		$('#table').DataTable().column(i).search($('#testset').val(),
				$('#col' + i + '_regex').prop('checked'),
				$('#col' + i + '_smart').prop('checked')).draw(),
		
		$('#table1').DataTable().column(0).search($('#testset').val(),
				$('#col0_regex').prop('checked'),
				$('#col0_smart').prop('checked')).draw();

	}
	</script>

	<script>
	var code9 = {};
	$("select[name='selectedTestset'] > option").each(
			function() {
				if (code9[this.text]) {
					$(this).remove();
				} else {
					code9[this.text] = this.value;
				}
			});
	
	
	var code10 = {};
	$("select[name='selectedTestset1'] > option").each(
			function() {
				if (code10[this.text]) {
					$(this).remove();
				} else {
					code10[this.text] = this.value;
				}
			});
	var code2 = {};
	$("select[name='selectedModule1'] > option").each(
			function() {
				if (code2[this.text]) {
					$(this).remove();
				} else {
					code2[this.text] = this.value;
				}
			});
	
	var code = {};
	$("select[name='selectedModule'] > option").each(
			function() {
				if (code[this.text]) {
					$(this).remove();
				} else {
					code[this.text] = this.value;
				}
			});
	
	var code1 = {};
	$("select[name='selectedRelease'] > option").each(
			function() {
				if (code1[this.text]) {
					$(this).remove();
				} else {
					code1[this.text] = this.value;
				}
			});
			
	</script>

	<script>
		$(function() {
			$('.button-checkbox')
					.each(
							function() {

								
								var $widget = $(this), $button = $widget
										.find('button'), $checkbox = $widget
										.find('input:checkbox'), color = $button
										.data('color'), settings = {
									on : {
										icon : 'glyphicon glyphicon-check'
									},
									off : {
										icon : 'glyphicon glyphicon-unchecked'
									}
								};

								
								$button.on('click', function() {
									$checkbox.prop('checked', !$checkbox
											.is(':checked'));
									$checkbox.triggerHandler('change');
									updateDisplay();
								});
								$checkbox.on('change', function() {
									updateDisplay();
								});

								
								function updateDisplay() {
									var isChecked = $checkbox.is(':checked');

									
									$button.data('state', (isChecked) ? "on"
											: "off");

									
									$button
											.find('.state-icon')
											.removeClass()
											.addClass(
													'state-icon '
															+ settings[$button
																	.data('state')].icon);

									
									if (isChecked) {
										$button.removeClass('btn-default')
												.addClass(
														'btn-' + color
																+ ' active');
									} else {
										$button.removeClass(
												'btn-' + color + ' active')
												.addClass('btn-default');
									}
								}

								
								function init() {

									updateDisplay();

									
									if ($button.find('.state-icon').length == 0) {
										$button.prepend('<i class="state-icon '
												+ settings[$button
														.data('state')].icon
												+ '"></i>�');
									}
								}
								init();
							});
		});

		jQuery(function($) {
			$.fn.select2.amd
					.require(
							[ 'select2/selection/single',
									'select2/selection/placeholder',
									'select2/selection/allowClear',
									'select2/dropdown',
									'select2/dropdown/search',
									'select2/dropdown/attachBody',
									'select2/utils' ],
							function(SingleSelection, Placeholder, AllowClear,
									Dropdown, DropdownSearch, AttachBody, Utils) {

								var SelectionAdapter = Utils.Decorate(
										SingleSelection, Placeholder);

								SelectionAdapter = Utils.Decorate(
										SelectionAdapter, AllowClear);

								var DropdownAdapter = Utils.Decorate(Utils
										.Decorate(Dropdown, DropdownSearch),
										AttachBody);

								var base_element = $('.select2-multiple22')
								$(base_element)
										.select2(
												{
													placeholder : 'Select multiple items',
													selectionAdapter : SelectionAdapter,
													dropdownAdapter : DropdownAdapter,
													allowClear : true,
													templateResult : function(
															data) {

														if (!data.id) {
															return data.text;
														}

														var $res = $('<div></div>');

														$res.text(data.text);
														$res.addClass('wrap');

														return $res;
													},
													templateSelection : function(
															data) {
														if (!data.id) {
															return data.text;
														}
														var selected = ($(
																base_element)
																.val() || []).length;
														var total = $('option',
																$(base_element)).length;
														
														return "You Selected ";
													}
												})

							});

		});

		function openpopUp() {
			swal({
				title : "Save your query",
				text : "Enter the name of query",
				type : "input",
				showCancelButton : true,
				closeOnConfirm : false,
				animation : "slide-from-left",
				inputPlaceholder : "The name of query"
			},

			function(inputValue) {
				if (inputValue === false)
					return false;
				if (inputValue === "") {
					swal.showInputError("Please enter name of the query!");
					return false
				}
				$('#queryName').val(inputValue);
				var saveFlag = true;
				//commonData = allData1+allData2+allData3;
				commonData = $("#showingStatus").val() + "|"
						+ $("#showingModule").val() + "|"
						+ $("#showingRelease").val()+ "|"
						+$("#showingTestset").val();
				saveOrExecuteQuery(inputValue, commonData);
			});

			function saveOrExecuteQuery(inputValue, commonData) {
				//alert(inputValue);
				//commonData = allData1+allData2+allData3;
				//alert(commonData);
				$.ajax({
					type : "POST",
					url : "insertFilterQueryForTEAutomation",
					data : {
						inputValue : inputValue,
						commonData : commonData

					}
				}).done(function(response) {
					window.location.href = "buildreportautomation";
				}).fail(function(jqXHR, testStatus, errorThrown) {
					return false;
				});
			}

		}

		$("#saveBtn").on('click', function() {

			openpopUp();

			//commonData = allData1+allData2+allData3;
			//alert(commonData);
		});
	</script>


	<script>
	$(function() {
		$('.button-checkbox')
				.each(
						function() {

							
							var $widget = $(this), $button = $widget
									.find('button'), $checkbox = $widget
									.find('input:checkbox'), color = $button
									.data('color'), settings = {
								on : {
									icon : 'glyphicon glyphicon-check'
								},
								off : {
									icon : 'glyphicon glyphicon-unchecked'
								}
							};

							
							$button.on('click', function() {
								$checkbox.prop('checked', !$checkbox
										.is(':checked'));
								$checkbox.triggerHandler('change');
								updateDisplay();
							});
							$checkbox.on('change', function() {
								updateDisplay();
							});

							
							function updateDisplay() {
								var isChecked = $checkbox.is(':checked');

								
								$button.data('state', (isChecked) ? "on"
										: "off");

								
								$button
										.find('.state-icon')
										.removeClass()
										.addClass(
												'state-icon '
														+ settings[$button
																.data('state')].icon);

								
								if (isChecked) {
									$button.removeClass('btn-default')
											.addClass(
													'btn-' + color
															+ ' active');
								} else {
									$button.removeClass(
											'btn-' + color + ' active')
											.addClass('btn-default');
								}
							}

							
							function init() {

								updateDisplay();

								
								if ($button.find('.state-icon').length == 0) {
									$button.prepend('<i class="state-icon '
											+ settings[$button
													.data('state')].icon
											+ '"></i>�');
								}
							}
							init();
						});
	});

	jQuery(function($) {
		$.fn.select2.amd
				.require(
						[ 'select2/selection/single',
								'select2/selection/placeholder',
								'select2/selection/allowClear',
								'select2/dropdown',
								'select2/dropdown/search',
								'select2/dropdown/attachBody',
								'select2/utils' ],
						function(SingleSelection, Placeholder, AllowClear,
								Dropdown, DropdownSearch, AttachBody, Utils) {

							var SelectionAdapter = Utils.Decorate(
									SingleSelection, Placeholder);

							SelectionAdapter = Utils.Decorate(
									SelectionAdapter, AllowClear);

							var DropdownAdapter = Utils.Decorate(Utils
									.Decorate(Dropdown, DropdownSearch),
									AttachBody);

							var base_element = $('.select2-multiple33')
							$(base_element)
									.select2(
											{
												placeholder : 'Select Column',
												selectionAdapter : SelectionAdapter,
												dropdownAdapter : DropdownAdapter,
												allowClear : true,
												templateResult : function(
														data) {

													if (!data.id) {
														return data.text;
													}

													var $res = $('<div></div>');

													$res.text(data.text);
													$res.addClass('wrap');

													return $res;
												},
												templateSelection : function(
														data) {
													if (!data.id) {
														return data.text;
													}
													var selected = ($(
															base_element)
															.val() || []).length;
													var total = $('option',
															$(base_element)).length;
													
													return data.text;
												}
											})

						});

	});
	</script>

	<script>
	function getQuery(query) {
		document.getElementById("runBtn").disabled = false;
		var res = query.split("|");
		document.getElementById("showingStatus").value = res[0];
		var str1 = document.getElementById("showingStatus").value;
		var str2 = str1.split(",").join("|");
		document.getElementById("Status").value = str2;

		document.getElementById("showingModule").value = res[1];
		var str1 = document.getElementById("showingModule").value;
		var str2 = str1.split(",").join("|");
		document.getElementById("Module").value = str2;

		document.getElementById("showingRelease").value = res[2];
		var str1 = document.getElementById("showingRelease").value;
		var str2 = str1.split(",").join("|");
		document.getElementById("Release").value = str2;
		
		document.getElementById("showingTestset").value = res[3];
		var str1 = document.getElementById("showingTestset").value;
		var str2 = str1.split(",").join("|");
		document.getElementById("testset").value = str2;
	}
	</script>

	<style>
.select2-results__option .wrap:before {
	font-family: fontAwesome;
	color: #999;
	content: "\f096";
	width: 25px;
	height: 25px;
	padding-right: 10px;
}

.select2-results__option[aria-selected=true] .wrap:before {
	content: "\f14a";
}

/* not required css */
.select2-multiple, .select2-multiple2 {
	width: 220%
}

.select2-multiple, .select2-multiple21 {
	width: 220%
}

.select2-multiple, .select2-multiple211 {
	width: 220%
}

.select2-multiple, .select2-multiple222 {
	width: 220%
}

.select2-multiple, .select2-multiple22 {
	width: 220%
}

.select2-multiple, .select2-multiple33 {
	width: 100%
}
</style>
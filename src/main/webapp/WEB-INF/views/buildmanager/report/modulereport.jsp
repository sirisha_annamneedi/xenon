<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Module report</h2>
		<ol class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li class="active"><strong>Module Report</strong></li>
		</ol>
	</div>
	
</div>

<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Pie</h5>

				</div>
				<div class="ibox-content">
					<canvas id="doughnutChart" height="140"></canvas>
					<hr />
					<div class="row">
						<div class="col-xs-1"></div>
						<div class="col-xs-2">
							<button class="btn"
								style="background-color: rgba(220, 220, 220, 0.5)">Pass</button>
						</div>
						<div class="col-xs-2">
							<button class="btn"
								style="background-color: rgba(26, 179, 148, 0.5)">Fail</button>
						</div>
						<div class="col-xs-2">
							<button class="btn"
								style="background-color: rgba(181, 184, 207, 0.5)">Skipp</button>
						</div>
						<div class="col-xs-2">
							<button class="btn"
								style="background-color: rgba(30, 198, 200, 0.5)">Block</button>
						</div>
						<div class="col-xs-2">
							<button class="btn"
								style="background-color: rgba(247, 165, 74, 0.5)">Not
								Run</button>
						</div>
						<div class="col-xs-1"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-content">

					<div class="clients-list">
						<div>
							<div>
								<div class="full-height-scroll">
									<div class="table-responsive">
										<table class="table table-striped table-hover"
											id="moduleReportTable">
											<thead>
												<tr>
													<td></td>
													<td><b>Name</b></td>
													<td></td>
													<td><b>Status</b></td>
												</tr>
											</thead>
											<tbody>
												<tr>


													<td>TC-1</td>
													<td><a data-toggle="tab" href="#contact-1"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"><span
														class="label label-success">Active</span></td>
												</tr>
												<tr>


													<td>TC-2</td>
													<td><a data-toggle="tab" href="#contact-2"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"><span
														class="label label-success">Active</span></td>
												</tr>
												<tr>


													<td>TC-3</td>
													<td><a data-toggle="tab" href="#contact-3"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"></td>
												</tr>
												<tr>


													<td>TC-4</td>
													<td><a data-toggle="tab" href="#contact-4"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"><span
														class="label label-warning">Waiting</span></td>
												</tr>
												<tr>


													<td>TC-5</td>
													<td><a data-toggle="tab" href="#contact-2"
														class="client-link"> TC-5 Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-3"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"><span
														class="label label-info">Phoned</span></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-1"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"><span
														class="label label-success">Active</span></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-2"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"><span
														class="label label-warning">Waiting</span></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-3"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-4"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-1"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"><span
														class="label label-danger">Deleted</span></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-2"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"><span
														class="label label-success">Active</span></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-3"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"><span
														class="label label-info">Phoned</span></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-4"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"><span
														class="label label-success">Active</span></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-2"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"><span
														class="label label-success">Active</span></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-1"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-3"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-4"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"><span
														class="label label-success">Active</span></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-1"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"><span
														class="label label-info">Phoned</span></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-2"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"><span
														class="label label-warning">Waiting</span></td>
												</tr>
												<tr>



													<td>TC-6</td>
													<td><a data-toggle="tab" href="#contact-4"
														class="client-link">Test case title-1</a></td>

													<td><i> </i></td>

													<td class="client-status"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
  $(function () {
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	});  
</script>
<script>
 	$(function(){
 		$('#moduleReportTable').DataTable({
 			"paging": true,
 	 	    "lengthChange": true,
 	 	    "searching": true,
 	 	    "ordering": true,
 	 	    "info": true,
 	        "autoWidth": true
 		});
 	});
 	
 	var doughnutData = [
 	                   {
 	                       value: 300,
 	                       color: "#a3e1d4",
 	                       highlight: "#1ab394",
 	                       label: "Fail"
 	                   },
 	                   {
 	                       value: 50,
 	                       color: "#dedede",
 	                       highlight: "#1ab394",
 	                       label: "Pass"
 	                   },
 	                   {
 	                       value: 100,
 	                       color: "#b5b8cf",
 	                       highlight: "#1ab394",
 	                       label: "Skip"
 	                   },
 	                   {
 	                       value: 30,
 	                       color: "rgba(30,198,200,0.5)",
 	                       highlight: "#1ab394",
 	                       label: "Block"
 	                   },
 	                   {
 	                       value: 80,
 	                       color: "rgba(247,165,74,0.5)",
 	                       highlight: "#1ab394",
 	                       label: "Not Run"
 	                   }
 	               ];
 	var doughnutOptions = {
 	        segmentShowStroke: true,
 	        segmentStrokeColor: "#fff",
 	        segmentStrokeWidth: 2,
 	        percentageInnerCutout: 45, // This is 0 for Pie charts
 	        animationSteps: 100,
 	        animationEasing: "easeOutBounce",
 	        animateRotate: true,
 	        animateScale: false,
 	        responsive: true,
 	    };


 	    var ctx = document.getElementById("doughnutChart").getContext("2d");
 	    var myNewChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);
 	
 </script>
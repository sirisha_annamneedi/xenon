<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/jstree/3.3.8/themes/default/style.min.css" />
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jstree/3.3.8/jstree.min.js"></script>
<style>
.table-striped>tbody>tr:hover {
	cursor: pointer;
}
</style>

<style>
.table {
	table-layout: fixed;
}

.topright {
	position: relative;
	text-align:right;
}

.btn-margin {
	margin-top: -11px;
	height: 28px;
}
</style>

<div class="md-skin">
	<script
		src="<%=request.getContextPath()%>/resources/js/jquery.dataTables.min.js"></script>
	<div class="modal inmodal fade" id="selectProjectModal" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h5 class="modal-title" style="font-weight: 500;">Select
						Project for Build</h5>

				</div>
				<div class="modal-body">
					<div class="dropdown">
						<div class="row">
							<div class="col-sm-6">
								<label style="font-weight: 500;">Please select project
									to add test cases </label>
							</div>
							<div class="col-sm-5">
								<select data-placeholder="Select"
									style="min-width: 50% !important" class="chosen-select"
									tabindex="2" name="selectProject" id="selectProject">
									<c:forEach var="projectList"
										items="${Model.selectProjectsList}">
										<option value="${projectList.project_id}">${projectList.project_name}</option>
									</c:forEach>

								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success"
						onclick="setCurrentProject1()">Save changes</button>
				</div>
			</div>
		</div>
	</div>



	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Update Test Set - ${Model.automationExecBuildName}</h2>

			<!--  <button type="button" id="autoexecute"
				class="btn btn-info pull-right ladda-button ladda-button-demo"
				style="margin-right: 20px">Execute</button>
			<!-- <button type="button" style="    margin-right: 10px;" id="switchProjectButton"
			class="btn btn-info pull-right hidden">Switch Project</button> -->
			<ol class="breadcrumb">
				<!-- 				<li><a href="viewbuild">Test Set</a></li> -->
				<!--  <li class="active"><strong>Add Test Set</strong></li> -->

			</ol>
		</div>
	</div>


	<div class="row sub-topmenu">
		<div class="col-md-12">
			<div class="">

				<div class="col-md-6 pd-l-0">
					<ul>
						<li><a href="autoexecute" id="homeMenu"
							><i class="fa fa-home"></i> Home</a></li>
						<li><a href="javascript:void(0)" id="winOpener" class="active "><i
								class="fa fa-plus"></i> Update Test Set</a></li>
						<!-- <li><a href="javascript:void(0)" data-toggle="modal"
							data-target="#uploadTestCase"><i class="fa fa-list"></i>
								Execute</a></li>
						<li><a href="javascript:void(0)" id="addParameter"><i
								class="fa fa-line-chart"></i> Report</a></li>-->


					</ul>
				</div>

			</div>
		</div>
	</div>


	<!-- Window -->
	<div id="window" title="Add Test Cases">
		<div id="id-modules-scenario-jstree-div"></div>
	</div>
	<!-- Window End -->

	<div class="wrapper wrapper-content animated fadeInRight">
	<div class="col-md-12">
		<div class="topright mb-10">
			<button type="button" disabled onclick="addTestCase1()" class="btn btn-info" id = "check">Save and Submit</button>
		</div>
	</div>
		<!-- Displaying Which Node is selected while clicking the JS Tree -->
		<!--  <div id="id-test-case-show-div"></div> -->

		
		<div class="table-responsive" style="overflow-x: visible;table-layout:fixed; width:100%;">
			<table
				class="table table-striped table-bordered table-hover dataTables-example" id="forappend"
				style="max-width: 100%; width: 100%;background:#fff">
				<div id="id-test-case-show-div" style="max-height: 400px;overflow-y: auto;clear: both;/* margin-bottom: 20px; */"></div>
				<br>
				<thead id="forappendHeader">
					<tr>
						<th>TestCase Id</th>
						<th>TestCase Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>

					<c:forEach var="testcasesList" items="${Model.testcasesList}">
						<c:if test="${testcasesList.testStatus==2}">
							<tr>
								<td>${testcasesList.test_prefix}</td>
								<td>${testcasesList.testcase_name}</td>
								<td>

									<button class="btn-danger btn btn-xs del-btn"
										id='${testcasesList.testcase_id}'
										data-tc-id='${testcasesList.testcase_id}'
										onclick="dropTestCase(${testcasesList.testcase_id})">
										<i class="fa fa-trash"></i>
									</button>
								</td>

							</tr>
						</c:if>

						<c:if test="${testcasesList.testStatus==1}">
							<tr>
								<td>${testcasesList.test_prefix}</td>
								<td>${testcasesList.testcase_name}</td>
								<td>-</td>

							</tr>
						</c:if>

					</c:forEach>

				</tbody>

			</table>
			</div>
			
		</div>
</div>

		<!-- end wrapper -->
		<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	var status=${Model.documentUploadStatus};
	if(status==417){
		swal({
            title: "Failure!",
            text: "Sample datasheet and uploaded datasheet does not match, Please verify sheets/columns",
            type: "warning"
        });
	}
	
});
var l = $('#submitDetailsBtn').ladda();
l.click(function() {
	// Start loading
	l.ladda('start');
	// Do something in backend and then stop ladda
	// setTimeout() is only for demo purpose
	setTimeout(function() {
		
		$('#uploadDatasheetForm').submit();
		l.ladda('stop');
	}, 1000)
});
$("#uploadDatasheetForm").submit(function(e){
	 $("#fileError").addClass('hidden');
	var imgVal = $('#uploadDatasheet').val(); 
   if(imgVal=='') 
   {
   	 $("#sizeError").addClass('hidden');
   	 $("#fileError").removeClass('hidden');
       
       $("#fileError").removeClass('hidden');
       return false;
   } else{
	   $('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");
   }
});

function downloadDocument(testDsId)
{
	var posting = $.post('getDocumentByTcDsId', {
		testDsId : testDsId
		});
		 posting.done(function(data) {
			 var jsonData=JSON.parse(data);
			 if(jsonData[0].status){
				 var a = window.document.createElement('a');
				 a.href= "data:"+";base64,"+jsonData[0].datasheetFile;
				 a.download = jsonData[0].datasheet_filename;
				 // Append anchor to body.
				 document.body.appendChild(a)
				 a.click();
				 document.body.removeChild(a)
			 }else{
				 swal({
		                title: "Failure!",
		                text: "Sample datasheet not found for testcase!",
		                type: "warning"
		            });

			 }
			 
		});
}

$('#uploadDatasheet').bind('change', function(e) {
	  $("#sizeError").addClass('hidden');
	  $("#fileError").addClass('hidden');
	if(this.files[0]){
	  var fileSize=this.files[0].size;
	  if(fileSize>1048576)
		{
		  $("#sizeError").removeClass('hidden');
		  $(this).val(null);
		}
	}
	});
	
$( document ).ready(function() {
	 var clickedProject= localStorage.getItem("AutoSelectedProject");
	 var clickedModule= localStorage.getItem("AutoSelectedModule");
	 var clickedScenario= localStorage.getItem("AutoSelectedScenario");
		/* if(clickedProject!=0)
		{
			$("#projectHref_"+clickedProject).trigger("click"); 
			
		    if(clickedModule!=0)
		    {
		         $("#moduleMenu_"+clickedModule).addClass("active");
		         $("#moduleUl_"+clickedModule).addClass("in");
		         moduleClick(clickedModule);
		         
		         if(clickedScenario!=0)
		         {
		              scenarioClick(clickedScenario);
		         }
		    }
		
		} */
		
});

function uploadDocument(tempTcId) {
	$("#uploadDatasheetTcId").val(tempTcId);
	$('#uploadDocModal').modal('show'); 
}
var addedTCFlag=${Model.addedTcCount};
var automationProjectId=${Model.automationProjectId};
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
		$(".chosen-select").chosen();
		$('.chosen-container').css('width','60%');
		$('.chosen-container').css('position','absolute');
// 		if(addedTCFlag==0  &&  automationProjectId==0)
// 		{
// 			$('#selectProjectModal').modal({
// 			    backdrop: 'static',
// 			    keyboard: false
// 			});
// 			$('#selectProjectModal').modal('show');
// 			$(".chosen-select").chosen();
// 			$('.chosen-container').css('width','60%');
// 			$('.chosen-container').css('position','absolute');
			
// 		}
		
	});
	function setCurrentProject1()
	{
		var id=$("#selectProject").val();
		var posting = $.post('setAutomationBuildProjectId',{
			projectId : id
		});
		 posting.done(function(data){
			window.location.href = "buildexecute";
		}); 
	}
	function setCurrentProject(id, name) {
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		  
		var posting = $.post('setCurrentProjectTM', {
			projectId : id,
			projectName : name
		});
		posting.done(function(data) {
			window.location.href = 'buildexecute';
		});
	}
	var flag = 0;
	var flagCheck = 0;
	var checkedModuleArray = [];
	var checkedScenarioArray = [];
	var checkedTestcaseArray = [];
	var tableClick;
	var TCArray=new Array();
	var RedwoodTCArray=new Array();
	var removeStatus ="${Model.removeStatus}";
	var TCArrayforRemove=new Array();

	var projectId = 0;
	var moduleId = 0;
	var scenarioId = 0;

	var totalModules = ${Model.modulesList.size()};
	var totalScenarios = ${Model.scenarioList.size()};
	var totalTestcases = ${Model.testcasesList.size()};

	var moduleTcCount = [];
	var scenarioTcCount = [];
	var projectTcCount = [];

	function commonFunction(fadeStatus, clickedId) {

		//flag = 0;

		var currentTableId = $(".statusTable").attr("id");

		 if ($.fn.dataTable.isDataTable("#" + currentTableId)) {
			tableInitialize = $("#" + currentTableId).DataTable();

		} else {
			tableInitialize = $("#" + currentTableId).DataTable({
				"aoColumnDefs" : [ {
					'bSortable' : false,
					'aTargets' : [ 1 ]
				} ]

			});
		} 
		if (flag == 0)
			{
			  //showAlert(fadeStatus, tableInitialize, clickedId);
			}
			
		else {
			handleCascade(fadeStatus, clickedId);
		}
	}
	

	$("button[name = 'addTcButton']").on('click', function() {
		
		$('body').addClass('white-bg');
		$("#barInMenu").removeClass('hidden');
		$("#wrapper").addClass("hidden");
		
		var checkedTableId = $(".statusTable").attr("id");

		if ($.fn.dataTable.isDataTable("#" + checkedTableId)) {
			tableInitialize = $("#" + checkedTableId).DataTable();

		} else {
			tableInitialize = $("#" + checkedTableId).DataTable({
				"aoColumnDefs" : [ {
					'bSortable' : false,
					'aTargets' : [ 1 ]
				} ]

			});
		}
		if (checkedTableId.indexOf("module") == 0) {
			tableClick = "module";
			addModulesToArray(tableInitialize);
			
			if(selectModuleFlag==1)
			{
				var clkId = $(".statusTable").attr("name");
				
				calculeteCount(tableClick, clkId,tableInitialize);
				
				addTestCase(checkedTestcaseArray);
				flag = 1;
			}
			else
				{
					$('body').removeClass('white-bg');
					$("#barInMenu").addClass('hidden');
					$("#wrapper").removeClass("hidden");
				}

		} else if (checkedTableId.indexOf("scenario") == 0) {
			tableClick = "scenario";
			addScenariosToArray(tableInitialize);
			
			if(selectScenarioFlag==1)
			{
				var clkId = $(".statusTable").attr("name");
				
				calculeteCount(tableClick, clkId,tableInitialize);
				
				addTestCase(checkedTestcaseArray);
				flag = 1;
			}
			else
			{
				$('body').removeClass('white-bg');
				$("#barInMenu").addClass('hidden');
				$("#wrapper").removeClass("hidden");
			}
		} else if (checkedTableId.indexOf("testcase") == 0) {
			tableClick = "testcase";
			addTestcasesToArray(tableInitialize);
			
			if(selectTcFlag==1)
			{
				var clkId = $(".statusTable").attr("name");
				
				calculeteCount(tableClick, clkId,tableInitialize);
				
				addTestCase(checkedTestcaseArray);
				flag = 1;
			}
			else
			{
				$('body').removeClass('white-bg');
				$("#barInMenu").addClass('hidden');
				$("#wrapper").removeClass("hidden");
			}
		}
		
});

	function showAlert(fadeStatus, tableInitialize, clickedId) {

		var checkedTableId1 = $(".statusTable").attr("id");
		if (checkedTableId1.indexOf("module") == 0) {
			tableClick = "module";
			checkModuleSelected(tableInitialize);

		} else if (checkedTableId1.indexOf("scenario") == 0) {
			tableClick = "scenario";
			checkScenarioSelected(tableInitialize);
		} else if (checkedTableId1.indexOf("testcase") == 0) {
			tableClick = "testcase";
			checkTestcaseSelected(tableInitialize);
		}

		 if (flag == 0) {
			handleCascade(fadeStatus, clickedId);
		} else
		{ 
			
			swal({
				title : "Are you sure?",
				text : "Do you want to add/remove selected Test Cases to build!",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "Yes, Add it!",
				cancelButtonText : "No, cancel!",
				closeOnConfirm : true,
				closeOnCancel : true
			}, function(isConfirm) {

				if (isConfirm) {

					var checkedTableId = $(".statusTable").attr("id");
					if (checkedTableId.indexOf("module") == 0) {
						addModulesToArray(tableInitialize);
					} else if (checkedTableId.indexOf("scenario") == 0) {
						addScenariosToArray(tableInitialize);
					} else if (checkedTableId.indexOf("testcase") == 0) {
						addTestcasesToArray(tableInitialize);
					}

					flag=0;
					
					intial();
					calculeteCount(tableClick, clickedId,tableInitialize);
					
					addTestCase(checkedTestcaseArray);
					dropTestCase();
					handleCascade(fadeStatus, clickedId);
					
					
				} else {

					var checkedTableId = $(".statusTable").attr("id");
					if (checkedTableId.indexOf("module") == 0) {
						setDefaultModuleBehaviour(tableInitialize);

					} else if (checkedTableId.indexOf("scenario") == 0) {
						setDefaultScenarioBehaviour(tableInitialize);

					} else if (checkedTableId.indexOf("testcase") == 0) {
						setDefaultTestcaseBehaviour(tableInitialize);
					}
					handleCascade(fadeStatus, clickedId);
					flag=1;

				}
			});

		}

	}



	var moduleCount = 0;
	var scenarioCount = 0;
	var testCount = 0;
	var table;
	function calculeteCount(tableClick, clickedId,tableInitialize) {

		var numberOfChecked;
		clickedId = $(".statusTable").attr("name");

		if (tableClick == "module") {

			projectId = clickedId;
			var temp = 0;
			$(
					'#moduleListTable_' + clickedId + ' select[name=selectModuleDropdown]').each(function(item) {
				var moduleCount = 0;

				setModuleCount($(this).attr("moduleId"), moduleCount);
				moduleCount = getScenarioCountbyID($(this).attr("moduleId"));
				setModuleCount($(this).attr("moduleId"), moduleCount);
				temp = temp + moduleCount;
			});

			resetProject(projectId);

		} else if (tableClick == "scenario") {
			var temp = 0;
			
			moduleId = clickedId;
			$('#scenarioListTable_' + clickedId + ' select[name=selectScenarioDropdown]').each(function(item) {
				var scenarioCount = 0;
				setScenarioCount($(this).attr("scenarioId"), scenarioCount);
				scenarioCount = getTesecaseCountbyID($(this).attr("scenarioId"));
				setScenarioCount($(this).attr("scenarioId"), scenarioCount);
				temp = temp + scenarioCount;

			});
			setModuleCount(moduleId, temp);
			resetProject(projectId);

		}

		else if (tableClick == "testcase") {
			scenarioId = clickedId;
			setScenarioCount(scenarioId, 0);
			 $('.testCaselist_'+scenarioId+'.removeTestcaseCheck').each(function () {
		         		 testCount++;
					});

			setScenarioCount(scenarioId, testCount);
			resetProject(projectId);
		}
	}
	

	function getTesecaseCountbyID(scenarioId) {
		
		var testCount = 0;
		  
		   $('.testCaselist_'+scenarioId+'.removeTestcaseCheck').each(function () {
	         		 testCount++;
				}); 
		   
		   /* var rowCount =$('#testcaseListTable_'+scenarioId+' input[class=testcaseCheck]').length;
		   if(rowCount==0)
			   {
			    $('#selectAllTestcases_'+scenarioId).attr('disabled',true);
			    $('#addTcTestcaseButton_'+scenarioId).attr('disabled',true);
			   } */
	
		return testCount;

	}

	function getScenarioCountbyID(moduleId) {
		var scenarioCount = 0;
		$(
				'#scenarioListTable_' + moduleId + ' select[name=selectScenarioDropdown]').each(function(item) {
			var temp = 0;
			setScenarioCount($(this).attr("scenarioId"), temp);
			temp = getTesecaseCountbyID($(this).attr("scenarioId"));
			setScenarioCount($(this).attr("scenarioId"), temp);
			scenarioCount = scenarioCount + temp;

		});
		return (scenarioCount);
	}
	var projectCount = 0;

	var projectCount = 0;

	function setProjectCount(projectId, numberOfChecked) {

		$('#projectCount_' + projectId).text(numberOfChecked);
	}

	function setModuleCount(moduleId, numberOfChecked) {

		$('#moduleCount_' + moduleId).text(numberOfChecked);
		$('#moduleRowCount_' + moduleId).text(numberOfChecked);
		var totalModTc=$('#moduleTotal_' + moduleId).text();
		var addedModTc=$('#moduleRowCount_' + moduleId).text();
		if(totalModTc==0)
		{
			$("#selectModuleDrop_"+moduleId+" option[value='1']").addClass("hidden");
			$("#selectModuleDrop_"+moduleId+" option[value='2']").addClass("hidden");
		} 
		 else if(totalModTc==addedModTc)
			{
				$("#selectModuleDrop_"+moduleId+" option[value='1']").addClass("hidden");
				$("#selectModuleDrop_"+moduleId+" option[value='2']").removeClass("hidden");
			}
		 else if(addedModTc==0)
			{
				$("#selectModuleDrop_"+moduleId+" option[value='1']").removeClass("hidden");
				$("#selectModuleDrop_"+moduleId+" option[value='2']").addClass("hidden");
			}
		 else 
		 {
				$("#selectModuleDrop_"+moduleId+" option[value='1']").removeClass("hidden");
				$("#selectModuleDrop_"+moduleId+" option[value='2']").removeClass("hidden");
			}
		setModuleCheck(moduleId);

	}

	function setScenarioCount(scenarioId, numberOfChecked) {
		$('#scenarioCount_' + scenarioId).text(numberOfChecked);
		$('#sceRowCount_' + scenarioId).text(numberOfChecked);
		
		var totalSceTc=$('#sceTotal_' + scenarioId).text();
		var addedSceTc=$('#sceRowCount_' + scenarioId).text();
		if(totalSceTc==0)
		{
			$("#selectScenarioDrop_"+scenarioId+" option[value='1']").addClass("hidden");
			$("#selectScenarioDrop_"+scenarioId+" option[value='2']").addClass("hidden");
		} 
		 else if(totalSceTc==addedSceTc)
			{
				$("#selectScenarioDrop_"+scenarioId+" option[value='1']").addClass("hidden");
				$("#selectScenarioDrop_"+scenarioId+" option[value='2']").removeClass("hidden");
			}
		 else if(addedSceTc==0)
			{
				$("#selectScenarioDrop_"+scenarioId+" option[value='2']").addClass("hidden");
				$("#selectScenarioDrop_"+scenarioId+" option[value='1']").removeClass("hidden");
			}
		 else 
		 {
				$("#selectScenarioDrop_"+scenarioId+" option[value='1']").removeClass("hidden");
				$("#selectScenarioDrop_"+scenarioId+" option[value='2']").removeClass("hidden");
			}
		setScenarioCheck(scenarioId);

	}

	function resetProject(projctId) {
		
		var temp = 0;
		
		$('select.moduleList_' + projectId).each(function(item) {
			
			var moduleCount = 0;
			setModuleCount($(this).attr("moduleId"), moduleCount);
			moduleCount = resetModule($(this).attr("moduleId"));
			setModuleCount($(this).attr("moduleId"), moduleCount);
			temp = temp + moduleCount;
		});
		$('#projectCount_' + projctId).text(temp);

	}
	function resetModule(moduleId) {
		var temp = 0;
		 /*  var rowCount =$('#scenarioListTable_'+moduleId+' input[name=scenarioCheck]').length;
		  if(rowCount==0)
			  {
			    $('#selectAllScenarios_'+moduleId).attr('disabled',true);
			    $('#addTcScenarioButton_'+moduleId).attr('disabled',true);
			  } */

		$('select.scenarioList_' + moduleId).each(function(item) {
			var scenarioCount = 0;
			setScenarioCount($(this).attr("scenarioId"), scenarioCount);
			scenarioCount = resetScenario($(this).attr("scenarioId"));
			setScenarioCount($(this).attr("scenarioId"), scenarioCount);
			temp = temp + scenarioCount;
		});

		return temp;
	}

	function resetScenario(scenarioId) {
		testCount = getTesecaseCountbyID(scenarioId);
		return testCount;

	}
	
	var selectModuleFlag=0;
	function addModulesToArray(tableInitialize) {
		var rows = tableInitialize.rows({
			'search' : 'applied'
		}).nodes();

		$('select[name="selectModuleDropdown"]', rows)
				.each(
						function() {
							if($("#selectModuleDrop_"+$(this).attr("moduleId")).val()==1)
							{
								selectModuleFlag=1;
								$('select.scenarioList_'+$(this).attr("moduleId")).each(function() {
									 $('.testCaseCheck.testCaselist_'+$(this).attr("scenarioId")).each(function() {
										 $(this).prop('checked', true);
										 checkedTestcaseArray
											.push(this.value);
									 });
								 });
							}
							if($("#selectModuleDrop_"+$(this).attr("moduleId")).val()==2)
							{
								selectModuleFlag=1;
								$('select.scenarioList_'+$(this).attr("moduleId")).each(function() {
									$('.removeTestcaseCheck.testCaselist_'+$(this).attr("scenarioId")).each(function() {
										if(!$(this).is(':disabled'))
										 $(this).prop('checked', true);
									 });
								 });
							}							
						});
		if(selectModuleFlag==0)
			{
				swal("Alert!", "Please select atleast one option to add or remove.","warning");
				return false;
			}
		
	}

	var selectScenarioFlag=0;
	function addScenariosToArray(tableInitialize) {

		var rows = tableInitialize.rows({
			'search' : 'applied'
		}).nodes();
		$('select[name="selectScenarioDropdown"]', rows)
				.each(
						function() { 
							
							if($("#selectScenarioDrop_"+$(this).attr("scenarioId")).val()==1)
							{
								selectScenarioFlag=1;
								$('.testCaseCheck.testCaselist_'+$(this).attr("scenarioId")).each(function() {
									 $(this).prop('checked', true);
										
											checkedTestcaseArray
													.push(this.value);
								 });
							}
							if($("#selectScenarioDrop_"+$(this).attr("scenarioId")).val()==2)
							{
								selectScenarioFlag=1;
								$('.removeTestcaseCheck.testCaselist_'+$(this).attr("scenarioId")).each(function() {
									if(!$(this).is(':disabled'))
									 $(this).prop('checked', true);
								 });
							}							
						});
		if(selectScenarioFlag==0)
		{
			swal("Alert!", "Please select atleast one option to add or remove.","warning");
			return false;
		}
		
	}

	var selectTcFlag=0;
	function addTestcasesToArray(tableInitialize) {
		
		var rows = tableInitialize.rows({
			'search' : 'applied'
		}).nodes();
		$('input[type="checkbox"].testCaseCheck', rows).each(
				function() {
					
					if ($(this).prop('checked') == true) {
						selectTcFlag=1;
						if ($.inArray(this.value, checkedTestcaseArray) == -1) {
							checkedTestcaseArray.push(this.value);
						}
					}
					/* else {
						if ($.inArray(this.value, checkedTestcaseArray) > -1) {
							checkedTestcaseArray.splice(checkedTestcaseArray
									.indexOf(this.value), 1);
						}
					} */
				});
		
		$('input[type="checkbox"].removeTestcaseCheck', rows).each(
				function() {
					
					if ($(this).prop('checked') == true) {
						selectTcFlag=1;
					}
					/* else {
						if ($.inArray(this.value, checkedTestcaseArray) > -1) {
							checkedTestcaseArray.splice(checkedTestcaseArray
									.indexOf(this.value), 1);
						}
					} */
				});
		if(selectTcFlag==0)
		{
			swal("Alert!", "Please select atleast one test case to add or remove.","warning");
			return false;
		}
	}

	function checkScenarioSelected(tableInitialize) {
		var rows = tableInitialize.rows({
			'search' : 'applied'
		}).nodes();
		$('input[type="checkbox"]', rows).each(function() {

			if ($(this).prop('checked') == true) {

				if ($.inArray(this.value, checkedScenarioArray) == -1) {
					flag = 1;

				}
				
			} else {
				if ($.inArray(this.value, checkedScenarioArray) > -1) {
					flag = 1;
				}
				
			}
		});
	}

	function checkModuleSelected(tableInitialize) {
		var rows = tableInitialize.rows({
			'search' : 'applied'
		}).nodes();
		$('input[type="checkbox"]', rows).each(function() {

			if ($(this).prop('checked') == true) {

				if ($.inArray(this.value, checkedModuleArray) == -1) {
					flag = 1;

				}
				/* else
					addedNewScenario=0; */
			} else {
				if ($.inArray(this.value, checkedModuleArray) > -1) {
					flag = 1;
				}
				/* else 
					removedScenario=0; */
			}
		});

	}

	function checkTestcaseSelected(tableInitialize) {
		var rows = tableInitialize.rows({
			'search' : 'applied'
		}).nodes();
		$('input[type="checkbox"]', rows).each(function() {

			if ($(this).prop('checked') == true) {

				if ($.inArray(this.value, checkedTestcaseArray) == -1) {
					flag = 1;

				}
				/* else
					addedNewTestcase=0; */
			} else {
				if ($.inArray(this.value, checkedTestcaseArray) > -1) {
					flag = 1;
				}
				/* else 
					removedTestcase=0; */
			}
		});
	}

	function setDefaultModuleBehaviour(tableInitialize) {
		var rows = tableInitialize.rows({
			'search' : 'applied'
		}).nodes();
		$('input[type="checkbox"]', rows).each(function() {

			if ($(this).prop('checked') == true) {

				if ($.inArray(this.value, checkedModuleArray) == -1) {
					$(this).prop('checked', false);
				}

			} else {
				if ($.inArray(this.value, checkedModuleArray) > -1) {
					$(this).prop('checked', true);
				}

			}
		});
	}
	function setDefaultScenarioBehaviour(tableInitialize) {
		var rows = tableInitialize.rows({
			'search' : 'applied'
		}).nodes();
		$('input[type="checkbox"]', rows).each(function() {

			if ($(this).prop('checked') == true) {

				if ($.inArray(this.value, checkedScenarioArray) == -1) {
					$(this).prop('checked', false);
				}

			} else {
				if ($.inArray(this.value, checkedScenarioArray) > -1) {
					$(this).prop('checked', true);
				}

			}
		});
	}

	function setDefaultTestcaseBehaviour(tableInitialize) {
		var rows = tableInitialize.rows({
			'search' : 'applied'
		}).nodes();
		$('input[type="checkbox"]', rows).each(function() {

			if ($(this).prop('checked') == true) {

				if ($.inArray(this.value, checkedTestcaseArray) == -1) {
					$(this).prop('checked', false);
				}

			} else {
				if ($.inArray(this.value, checkedTestcaseArray) > -1) {
					$(this).prop('checked', true);
				}

			}
		});
	}

	function handleCascade(fadeStatus, clickedId) {

		flag = 1;
		if (fadeStatus == "project") {

			projectTable(clickedId);
		} else if (fadeStatus == "module") {

			moduleTable(clickedId);
		} else if (fadeStatus == "scenario") {

			scenarioTable(clickedId);

		}

	}

	function projectTable(clickedId) {

		projectId = clickedId;

		$('div[id^="moduleInfoDiv_"]').fadeOut("fast");
		$('div[id^="scenarioInfoDiv_"]').fadeOut("fast");
		$('div[id^="projectInfoDiv_"]').fadeOut("fast");
		$("#projectInfoDiv_" + clickedId).fadeIn("fast");

		$("#ProjectListDiv").fadeOut("fast");

		$('table').removeClass("statusTable");
		$('#moduleListTable_' + clickedId).addClass("statusTable");
		initializeSelectAll(clickedId);

	}

	function moduleTable(clickedId) {

		moduleId = clickedId;

		$('div[id^="projectInfoDiv_"]').fadeOut("fast");
		$('div[id^="moduleInfoDiv_"]').fadeOut("fast");
		$('div[id^="scenarioInfoDiv_"]').fadeOut("fast");
		$("#moduleInfoDiv_" + clickedId).fadeIn("fast");

		$("#ProjectListDiv").fadeOut("fast");

		$('table').removeClass("statusTable");
		$('#scenarioListTable_' + clickedId).addClass("statusTable");
		initializeSelectAll(clickedId);

	}

	function scenarioTable(clickedId) {

		scenarioId = clickedId;

		$('div[id^="projectInfoDiv_"]').fadeOut("fast");
		$('div[id^="moduleInfoDiv_"]').fadeOut("fast");
		$('div[id^="scenarioInfoDiv_"]').fadeOut("fast");
		$("#scenarioInfoDiv_" + clickedId).fadeIn("fast");
		$("#ProjectListDiv").fadeOut("fast");

		$('table').removeClass("statusTable");
		$('#testcaseListTable_' + clickedId).addClass("statusTable");
		initializeSelectAll(clickedId);

	}

	function initializeSelectAll(clickedId) {

		//	alert("In initializeSelectAll()");
		var checkedTableId = $(".statusTable").attr("id");
		if (checkedTableId.indexOf("module") == 0) {
			//	addModulesToArray(tableInitialize);

			selectAllId = "selectAllModules_";
		} else if (checkedTableId.indexOf("scenario") == 0) {
			//addScenariosToArray(tableInitialize);
			selectAllId = "selectAllScenarios_";
		} else if (checkedTableId.indexOf("testcase") == 0) {
			//addTestcasesToArray(tableInitialize);
			selectAllId = "selectAllTestcases_";
		}
		var tableInitialization;

		if ($.fn.dataTable.isDataTable('#' + checkedTableId)) {


			tableInitialization = $('#' + checkedTableId).DataTable();
		} else {

			tableInitialization = $('#' + checkedTableId).DataTable({
				"aoColumnDefs" : [ {
					'bSortable' : false,
					'aTargets' : [ 1 ]
				} ]
			});

			$('#' + selectAllId + clickedId).on('click',function() {
						var rows = tableInitialization.rows({
							'search' : 'applied'
						}).nodes();
						$('input[type="checkbox"]', rows).prop('checked',this.checked);
						//setTestcaseStatus();
					});

			$('#' + checkedTableId + ' tbody').on('change',
					'input[type="checkbox"]', function() {
						if (!this.checked) {
							var el = $('#' + selectAllId + clickedId).get(0);
							if (el && el.checked && ('indeterminate' in el)) {
							
								el.indeterminate = true;
							}
						}

					});

		}

		return tableInitialization;

	}

	function projectClick(clickedId) {
		localStorage.setItem("AutoSelectedProject",clickedId);
		localStorage.setItem("AutoSelectedModule",0);
		localStorage.setItem("AutoSelectedScenario",0);
		if (flag == 1) {

			commonFunction("project", clickedId);

		} else {
			flag = 1;

			projectTable(clickedId);

		}

	}

	function moduleClick(clickedId) {
		localStorage.setItem("AutoSelectedModule",clickedId);
		localStorage.setItem("AutoSelectedScenario",0);
		if (flag == 1) {

			commonFunction("module", clickedId);

		} else {
			flag = 1;
			moduleTable(clickedId);

		}

	}

	function scenarioClick(clickedId) {
		localStorage.setItem("AutoSelectedScenario",clickedId);
		if (flag == 1) {

			commonFunction("scenario", clickedId);

		} else {
			flag = 1;
			scenarioTable(clickedId)

		}

	}
	var testCaseArray="";
	$("#autoexecute").click(function (){
			
			if(testCaseArray==''  &&  $('[name="removeTestcaseCheck"]').length==0 )
			{
				swal("Alert!", "Please add test case's in build to execute.","warning");
				setTestcaseStatus();
			}
		else
			{
				var ladda_autoexecute = $('#autoexecute').ladda();
				ladda_autoexecute.ladda('start');
				var posting = $.post('validatetestcases',{
				});
				posting.done(function(data){
					ladda_autoexecute.ladda('stop');
					if(data == 'true'){
						window.location.href="autoexecute";
					}else{
						swal("Alert!", "Please upload datasheet for all necessary testcases.","warning");
					}
				});
			}
		});
	
function addTestCase(testCaseArray) {
	var l = $('.ladda-button-demo').ladda();

	if(testCaseArray=='' && testClickFlag==false)
		{
			swal("Alert!", "Please Select Atlest One Test Case.","warning");
			setTestcaseStatus();
		}
	else
		{
		
		l.ladda('start');
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");
			 var posting = $.post('addautomationtestcase', {
					testCaseArray : testCaseArray
				});
				 
				setTestcaseStatus();
				
				posting.done(function(data) {
					var dropTestArray=[];
					$('input[name="removeTestcaseCheck"]').each(function(item){
						
						if(($(this).is(":checked")))
							{
							  dropTestArray.push(this.value);
							}
					});
					if(dropTestArray.length>0)
						{
							var posting = $.post('dropautomationtestcase', {dropTestArray : dropTestArray});
						}
					testClickFlag=false;
					
					l.ladda('stop');
					
					$('body').removeClass("white-bg");
					$("#barInMenu").addClass("hidden");
					$("#wrapper").removeClass("hidden");
					
					swal({
						title: "Success",
		                text: "Selected Test Cases Added/Removed.",
		                type: "success",
		                showCancelButton: false,
		                confirmButtonColor: "#DD6B55",
		                confirmButtonText: "Ok",
		                closeOnConfirm: false
		            }, function () {
		            	window.location.href="buildexecute";
		            });
				}); 
				
				flag=0;
		}

}



function addTestCase1() {
	var l = $('.ladda-button-demo').ladda();

	if(TCArray=='' && testClickFlag==false)
		{
			swal("Alert!", "Please Select Atlest One Test Case.","warning");
			setTestcaseStatus();
		}
	else
		{
		
		l.ladda('start');
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");
			 var posting = $.post('addautomationtestcase', {
					testCaseArray : TCArray,
					redwoodTestCases : RedwoodTCArray
				});
				 
				setTestcaseStatus();
				
				posting.done(function(data) {
					var dropTestArray=[];
					$('input[name="removeTestcaseCheck"]').each(function(item){
						
						if(($(this).is(":checked")))
							{
							  dropTestArray.push(this.value);
							}
					});
					if(dropTestArray.length>0)
						{
							var posting = $.post('dropautomationtestcase', {dropTestArray : dropTestArray});
						}
					testClickFlag=false;
					
					l.ladda('stop');
					
					$('body').removeClass("white-bg");
					$("#barInMenu").addClass("hidden");
					$("#wrapper").removeClass("hidden");
					
					swal({
						title: "Success",
		                text: "Selected Test Cases Added/Removed.",
		                type: "success",
		                showCancelButton: false,
		                confirmButtonColor: "#DD6B55",
		                confirmButtonText: "Ok",
		                closeOnConfirm: false
		            }, function () {
		    				var ladda_autoexecute = $('#autoexecute').ladda();
		    				ladda_autoexecute.ladda('start');
		    				var posting = $.post('validatetestcases',{
		    				});
		    				posting.done(function(data){
		    					ladda_autoexecute.ladda('stop');
		    					if(data == 'true'){
		    						window.location.href="buildexecute";
		    					}else{
		    						swal("Alert!", "Please upload datasheet for all necessary testcases.","warning");
		    					}
		    				});
		            	window.location.href="buildexecute";
		            });
				}); 
				
				flag=0;
		}

}

	/* function addTestCase(testCaseArray) {
		var l = $('.ladda-button-demo').ladda();
	
		if(testCaseArray=='')
			{
				/* $("#switchProjectButton").removeClass("hidden");
				$(".chosen-select").chosen();
				$('.chosen-container').css('width','60%');
				$('.chosen-container').css('position','absolute');
				swal("Alert!", "No test case present in build.","warning");
				setTestcaseStatus();
			}
		else
			{
			
			l.ladda('start');
			
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
			
				var posting = $.post('addautomationtestcase', {
						testCaseArray : testCaseArray
					});
					
					setTestcaseStatus();
					
					posting.done(function(data) {
						l.ladda('stop');
						
						$('body').removeClass("white-bg");
						$("#barInMenu").addClass("hidden");
						$("#wrapper").removeClass("hidden");
						
						/* swal("Selected Test Cases Added/Removed.", "",
						"success"); 
						
						swal({
					        title: "",
					        text: "Selected Test Cases Added/Removed.",
					        type: "success",
					        showCancelButton: false,
					        confirmButtonColor: "#DD6B55",
					        confirmButtonText: "Ok",
					        closeOnConfirm: true
					    }, function () {
					      
					    });
						
					}); 
					
					flag=0;
			}
	
	} */

	/*function dropTestCase() {
		
		var dropTestArray=[];
		$('input[name="testcaseCheck"]').each(function(item){
			
			if(!($(this).is(":checked")))
				{
				  dropTestArray.push(this.value);
				}
		});
		
		if(dropTestArray.length>0)
			{
			  var posting = $.post('dropautomationtestcase', {dropTestArray : dropTestArray});
			}
		

	}
	*/
	
	
	function dropTestCase(tcID){
		TCArrayforRemove.push(tcID);
		//alert(TCArrayforRemove);
		
			if(TCArrayforRemove.length>0)
			{

				var posting = $.post('dropautomationtestcase', {dropTestArray : TCArrayforRemove});
			}
			swal({
				title: "Success",
	            text: "Selected Test Cases Removed.",
	            type: "success",
	            showCancelButton: false,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "Ok",
	            closeOnConfirm: false
	        }, function () {
	        	window.location.href="buildexecute";
	        });
	}
	
	
	function setModuleCheck(moduleId)
	{
		var addModule=$("#moduleRowCount_"+moduleId).text();
		var totalmodule=$("#moduleTotal_"+moduleId).text();
		if(totalmodule!=0)
			{
			
				if(addModule==totalmodule)
				{
				
					if ($.inArray(this.value,checkedModuleArray) == -1) {					
						checkedModuleArray.push(moduleId);
					}

				   $("#mdlRow_"+moduleId+" input:checkbox").prop('checked', true);
				   
				   if(removeStatus==2)
					{
					   $("#mdlRow_"+moduleId+" input:checkbox").attr('disabled',true);
					}
				}
				else
					{
					  $("#mdlRow_"+moduleId+" input:checkbox").prop('checked', false);
					}
			}

	}
	
	function setScenarioCheck(scenarionId)
	{
		var addScenario=$("#sceRowCount_"+scenarionId).text();
		var totalScenarion=$("#sceTotal_"+scenarionId).text();
		
		if(totalScenarion!=0)
		{
			if(addScenario==totalScenarion)
				{
					if ($.inArray(this.value,checkedScenarioArray) == -1) {
						//checkedModuleArray.splice(checkedModuleArray.indexOf(this.value),1);
						checkedScenarioArray.push(scenarionId);
					}
					
					$("#sceRow_"+scenarionId+" input:checkbox").prop('checked', true);
					if(removeStatus==2)
					{
						$("#sceRow_"+scenarionId+" input:checkbox").attr('disabled',true);
					}
				   
				}
			else
				{
				$("#sceRow_"+scenarionId+" input:checkbox").prop('checked', false);
				
				}
		}
	}
	
	function intial()
	{
		
		 $('.projectNameClick').each(function(item){
				
				var projectId=$(this).attr("name");		
				
			  var temp=0;
			  
			    $('select.moduleList_' + projectId).each(function(item) {
					
					var moduleCount = 0;
					setModuleCount($(this).attr("moduleId"), moduleCount);
					moduleCount = resetModule($(this).attr("moduleId"));
					setModuleCount($(this).attr("moduleId"), moduleCount);
					temp = temp + moduleCount;
				});
			    

			     $('#projectCount_' + projectId).text(temp); 
			
			  }); 
	}
	$(window).bind("load", function() {
		
		intial();
			
			   $('input[name="removeTestcaseCheck"]').each(function(item) {
					checkedTestcaseArray.push(this.value);
				});
		});
	
	$(document).ready(function() {
	//	console.log(JSON.stringify('${projectDetailsJSONArray}'));
		$('.footable').footable();
		setDisable();
		
		 $('.testCaseCheck').click(function(){
			
			var checkVal=$(this).attr('value');
			
			if($(this).is(":checked")) {
				 	$('#testCaseStatus_'+checkVal).text('Marked for Addition');
	        }
			else
				{
			 	  	$('#testCaseStatus_'+checkVal).text('Marked for Removal');
				}
		});
		
		 
		 
		if(removeStatus==2)
			{
	            $('input[type=checkbox]:checked').each(function () {
				    $(this).attr('disabled',true);
				});
			}

		
		/*  JS Tree Functionality */
		$jsTree = $('#id-modules-scenario-jstree-div').jstree({
        	'core' : {
            	'data' : JSON.parse('${projectDetailsJSONArray}')
            }, 
            "checkbox": {
                "tie_selection": true
            },
            "plugins" : [ "checkbox" ]
		}).on('changed.jstree', function(e, data) {
        	TCArray.length = 0;
        	RedwoodTCArray.length = 0;
        	var arr = new Array();
        	$("#testtree").jstree('get_checked').each(function(index) {
        	arr.push($(this).attr('id') +" : "+$(this).attr('id'));
        	});
        	
        	$('#id-test-case-show-div').append(arr);
        	var tableHTML ="";       	
        	var currentNode = $("#id-modules-scenario-jstree-div").jstree("get_bottom_selected");
        	/* var tableHTML = '<table class="table table-striped table-bordered table-hover dataTables-example " style="width: 85%; max-width: 85%; width: 100"><thead><tr>'+
			'<th>TestCase Id</th><th>TestCase Name</th><th>Action</th></tr></thead><tbody>'; */
			var tableHTML = '<table class="table table-striped table-bordered table-hover dataTables-example " style="max-width: 100%; width: 100%;background:#fff"><thead><tr>'+
			'<th>TestCase Id</th><th>TestCase Name</th><th>Action</th></tr></thead><tbody>';
        	var isExist=0;
        	var addedTestCases = [];
        	$(currentNode).each( function(i, node ){
        		//console.log( i+" : "+node);
        		document.getElementById("forappendHeader").style.display='none';
        		if( $.isNumeric( node ) ) {
        			document.getElementById("check").disabled = false;
	        		$.each(JSON.parse( '${testCaseIdsJSON}'), function( i, value ) {
	        			if( node == value ){
	        				addedTestCases.push( $('#id-modules-scenario-jstree-div').jstree(true).get_node(node).text ); 
		        			isExist=1;
		        			return false;
	        			}else{
	        				isExist=0;
	        			}
	        		});
	        		if(isExist==0){
	    				tableHTML += '<tr><td>'+node +'</td>'
	  				  +'<td>'+$('#id-modules-scenario-jstree-div').jstree(true).get_node(node).text +'</td><td></td></tr>';
	    				TCArray.push(node);
	    				if( '${sessionScope.automationExecBuildType}' == 5 )
	    					RedwoodTCArray.push( $('#id-modules-scenario-jstree-div').jstree(true).get_node(node).data.redwood_tc_id );
	        		}
        		}
        	});
        	tableHTML += '</tbody></table>';
        	//alert(tableHTML);
        	if( addedTestCases.length > 0 )
        		alert( addedTestCases.join() +" testcases already added.");
        	$('#id-test-case-show-div').html( tableHTML );
        }).on('ready.jstree', function (event, data) {
        	// Code To Hide Modules and Scenario That Doesn't Have Testcases Added
        	$($jsTree.jstree().get_json('#', {
        	    flat: true
        	})).each(function(index, value) {
//         		console.log( value );
        		var node = $jsTree.jstree().get_node(value.id);
//          	console.log( node );
        	    if( node.id.startsWith("module_") )
        	    	if( node.children.length == 0 )
        	    		$jsTree.jstree(true).hide_node(node);
        	    	else {
        	    		var is_all_scenario_has_children = false;
	              		node.children.forEach(element => {
              	    		var node_scenario = $jsTree.jstree().get_node(element);
							if( node_scenario.children.length == 0 ) {
								$jsTree.jstree(true).hide_node(node_scenario);
							} else {
								is_all_scenario_has_children = true;
							}
              	    	});
              	      	if( !is_all_scenario_has_children )
							$jsTree.jstree(true).hide_node(node);
        	    	}
//         	    	if( node.id.startsWith("scenario_") && node.children.length == 0 ) {
//         	    		$jsTree.jstree(true).hide_node(node);
//         	    	}
        	    });
        	});
		$('#id-add-all-btn').click( function(){
			alert( $('#id-modules-scenario-jstree-div').jstree(true).get_bottom_checked() );
		});
		
	});
	
	
	
	
	
	
	
function setTestcaseStatus()
{
	$('.testCaseCheck').each(function() {
			if ($(this).is(":checked")) {
				if ($(this).is(":disabled")) 
					{
					 $('#testCaseStatus_' + $(this).attr('value')).text('Executed');
					}
				else
					{
					$('#testCaseStatus_' + $(this).attr('value')).text('Added');
					}
				
			} else {
				$('#testCaseStatus_' + $(this).attr('value')).text('-');
			}
		
		});
	}
	
	
	function setDisable() {
		
		$('.testCaseCheck').each(function() {
			if ($(this).is(':disabled')) {
				 $('#selectAllTestcases_'+$(this).closest('table').attr('name')).attr('disabled',true);
				 setScenarioDisable($(this).closest('table').attr('name'));
			}
		});
	}
	
	function setScenarioDisable(sceId)
	{
		
		 $('#selectAllScenarios_'+sceId).attr('disabled',true);
		 $('.scenarioList_'+sceId).each(function() {
			 $(this).attr('disabled',true);
			 setModuleDisable($(this).closest('table').attr('name'));
			
		 });
	}
	
	function setModuleDisable(mdlId)
	{
		
		 $('#selectAllModules_'+mdlId).attr('disabled',true);
		 $('.moduleList_'+mdlId).each(function() {
			 $(this).attr('disabled',true);
			
		 });
		
	}
	
	
	function scenarioAddRemoveClicked(scenarioId)
	{
		 if($("#selectScenarioDrop_"+scenarioId).val()==1)
		{
			$('.testCaseCheck.testCaselist_'+scenarioId).each(function() {
				 //$(this).prop('checked', true);
			 });
		}
		if($("#selectScenarioDrop_"+scenarioId).val()==2)
		{
			$('.removeTestcaseCheck.testCaselist_'+scenarioId).each(function() {
				/* if(!$(this).is(':disabled'))
				 $(this).prop('checked', true); */
			 });
		} 
	}

	function moduleAddRemoveClicked(moduleId)
	{
		if($("#selectModuleDrop_"+moduleId).val()==1)
		{
			$('select.scenarioList_'+moduleId).each(function() {
				 $('.testCaseCheck.testCaselist_'+$(this).attr("scenarioId")).each(function() {
					// $(this).prop('checked', true);
				 });
			 });
		}
		if($("#selectModuleDrop_"+moduleId).val()==2)
		{
			$('select.scenarioList_'+moduleId).each(function() {
				$('.removeTestcaseCheck.testCaselist_'+$(this).attr("scenarioId")).each(function() {
					/* if(!$(this).is(':disabled'))
					 $(this).prop('checked', true); */
				 });
			 });
			
		}
	}
</script>
		<link rel="stylesheet"
			href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script>
$(function() {
	  function addButtons(dlg) {
	    // Define Buttons
	    var $close = dlg.find(".ui-dialog-titlebar-close");
	    var $min = $("<button>", {
	      class: "ui-button ui-corner-all ui-widget ui-button-icon-only ui-window-minimize",
	      type: "button",
	      title: "Minimize"
	    }).insertBefore($close);
	    $min.data("isMin", false);
	    $("<span>", {
	      class: "ui-button-icon ui-icon ui-icon-minusthick"
	    }).appendTo($min);
	    $("<span>", {
	      class: "ui-button-icon-space"
	    }).html(" ").appendTo($min);
	    var $max = $("<button>", {
	      class: "ui-button ui-corner-all ui-widget ui-button-icon-only ui-window-maximize",
	      type: "button",
	      title: "Maximize"
	    }).insertBefore($close);
	    $max.data("isMax", false);
	    $("<span>", {
	      class: "ui-button-icon ui-icon ui-icon-plusthick"
	    }).appendTo($max);
	    $("<span>", {
	      class: "ui-button-icon-space"
	    }).html(" ").appendTo($max);
	    // Define Function
	    $min.click(function(e) {
	      if ($min.data("isMin") === false) {
	        console.log("Minimize Window");
	        $min.data("original-pos", dlg.position());
	        $min.data("original-size", {
	          width: dlg.width(),
	          height: dlg.height()
	        });
	        $min.data("isMin", true);
	        dlg.animate({
	          height: '40px',
	          top: $(window).height() - 50
	        }, 200);
	        dlg.find(".ui-dialog-content").hide();
	      } else {
	        console.log("Restore Window");
	        $min.data("isMin", false);
	        dlg.find(".ui-dialog-content").show();
	        dlg.animate({
	          height: $min.data("original-size").height + "px",
	          top: $min.data("original-pos").top + "px"
	        }, 200);
	      }
	    });
	    $max.click(function(e) {
	      if ($max.data("isMax") === false) {
	        console.log("Maximize Window");
	        $max.data("original-pos", dlg.position());
	        $max.data("original-size", {
	          width: dlg.width(),
	          height: dlg.height()
	        });
	        $max.data("isMax", true);
	        dlg.animate({
	          height: $(window).height() + "px",
	          width: $(window).width() - 20 + "px",
	          top: 0,
	          left: 0
	        }, 200);
	      } else {
	        console.log("Restore Window");
	        $max.data("isMax", false);
	        dlg.animate({
	          height: $max.data("original-size").height + "px",
	          width: $max.data("original-size").width + "px",
	          top: $max.data("original-pos").top + "px",
	          left: $max.data("original-pos").top + "px"
	        }, 200);
	      }
	    });
	  }

	  $('#window').dialog({
	    draggable: true,
	    autoOpen: true,
	    classes: {
	      "ui-dialog": "ui-window-options",
	      "ui-dialog-titlebar": "ui-window-bar"
	    },
	    modal: true,
	    responsive: true,
	  });

	  addButtons($(".ui-window-options"));

	  $("#winOpener").click(function() {
		
	    $("#window").dialog("open");
	  })
	});
</script>
<style>
.ui-window-bar .ui-button {
	position: absolute;
	top: 50%;
	width: 20px;
	margin: -10px 0 0 0;
	padding: 1px;
	height: 20px;
}

.ui-window-bar .ui-window-minimize {
	right: calc(.3em + 40px);
}

.ui-window-bar .ui-window-maximize {
	right: calc(.3em + 20px);
}

.ui-widget-overlay {
	background: none !important;
	opacity: 1 !important;
	display: none;
}
.ui-draggable .ui-dialog-titlebar {
    background: #1c84c6;
    color: #fff;
}

</style>
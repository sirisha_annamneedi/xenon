<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
.table-striped>tbody>tr:hover {
	cursor: pointer;
}
</style>
<div class="md-skin">
	<script
		src="<%=request.getContextPath()%>/resources/js/jquery.dataTables.min.js"></script>
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>
		    <ol class="breadcrumb">
	 			<li><strong>${UserCurrentProjectName}</strong></li>
				<li>Automation Execution</li>
			</ol>
			</h2>
		</div>
	</div>
	
	<div class="row sub-topmenu">
	<input type="hidden" id="buildExist" value="${sessionScope.duplicateBuildError}">   
		<div class="col-md-12">
			<div class="">
				<div class="col-md-8 pd-l-0">
					<ul>
						<li><a href="autoexecute" id="homeMenu" class="active "><i class="fa fa-home"></i> Home</a></li>
						<li><a href="buildexecute"><i class="fa fa-plus"></i> Add/Remove Test Cases</a></li>
						<!-- <li><a href="javascript:void(0)" data-toggle="modal" data-target="#uploadTestCase"><i class="fa fa-list"></i>
				 		 Execute</a></li> -->
						<li><a href="viewautomationreport" id="addParameter"><i class="fa fa-line-chart"></i>
							 Report</a></li>
						<li><a href="javascript:void(0)" id="refreshState" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh </a></li>
						<c:if test="${sessionScope.issue_tracker==1}">
						<li><a href="javascript:void(0)" id="syncToJira" onclick="createBugToJiraByBuildId()"><i class="fa fa-refresh"></i> SyncToJira </a></li>
						</c:if>
					
					</ul>
				</div>
			</div>
		</div>
</div>
	
<div class="row">
	<div class="col-md-12">
		<div class="white-bg usr-dtl" id="vmBlock">
			<div class="ibox-title" style="padding-left: 0px">
			<h5>Execute Test Set - ${Model.automationExecBuildName}</h5>
				<!-- <h2>Select VM</h2>-->
				<c:forEach var="state" items="${Model.state}">
					<c:choose>
						<c:when test="${state.build_state ==1 }">
							<small><label id="id-state" class="label 4 label-success">
										${state.desc}</label></small>
						</c:when>
						<c:when test="${state.build_state ==2 }">
							<small><label id="id-state" class="label 4 label-info">
										${state.desc}</label></small>
						</c:when>
						<c:when test="${state.build_state ==3 }">
							<small><label id="id-state" class="label 4 label-warning">
										${state.desc}</label></small>
						</c:when>
						<c:when test="${state.build_state ==4 }">
							<small><label id="id-state" class="label 4 label-danger">
										${state.desc}</label></small>
						</c:when>
						<c:when test="${state.build_state ==5 }">
							<small><label id="id-state" class="label 4 label-primary">
										${state.desc}</label></small>
						</c:when>
						<c:when test="${state.build_state ==6 }">
							<small><label id="id-state" class="label 4 label-warning">
										Ready for Execution</label></small>
						</c:when>
						<c:when test="${state.build_state ==7 }">
							<small><label id="id-state" class="label 4 label-warning">
										${state.desc}</label></small>
						</c:when>
						<c:when test="${state.build_state ==8 }">
							<small><label id="id-state" class="label 4 label-danger">
										${state.desc}</label></small>
						</c:when>
						<c:when test="${state.build_state ==11 }">
							<small><label id="id-state" class="label 4 label-danger">
										${state.desc}</label></small>
						</c:when>
					</c:choose>
				</c:forEach>
				</div>
<%-- 			<c:if test="${sessionScope.automationExecBuildType != 5 }">		 --%>
				<div class="row">
					<form class="form-inline">
						<div class="form-group col-md-3">
						    <label for="user-id">Virtual Machine:</label>
						    <select class="form-control" id="selectVM" required>
						    	<option value="0">Select VM</option>>
							    <c:forEach var="vmList" items="${Model.vmList}">
							        <option value="${vmList.cust_vm_id}">${vmList.hostname}</option>
							      </c:forEach>
						      </select>
						  </div>
						  <div class="form-group col-md-3">
						    <label for="user-id">Status:</label>
						    <input type="text" class="form-control" placeholder="Status" id="vmStatus" readonly>
						  </div>
						  <div class="form-group col-md-3">
						    <label for="updated-date">Execution:</label>
						     <input type="text" class="form-control" placeholder="Execution" id="vmExecution" readonly>
						  </div>
						  <div class="form-group col-md-3">
						    <label for="updated-date">Repository:</label>
						     <input type="text" class="form-control" placeholder="Repository" id="vmRepository" title="" readonly>
						  </div>
						  <input type="hidden" name="selectVMExecCount" id="selectVMExecCount" />
						  <input type="hidden" name="selectVMIPAddress" id="selectVMIPAddress" />
					</form>
				</div>
<%-- 			</c:if> --%>
		</div>
	</div>
</div> 

<div class="row">
	<div class="col-md-12">
		<div class="white-bg vm-dtl" id="vmPropBlock">
		<div class="row">
		<form class="form-inline pt-15">
		
		   <div class="form-group col-md-4" id="browserDiv">
		    <label for="status">Browser:</label>
		    <select class="form-control" id="selectBrowser">
		    	<option value="">Select</option>
		    	<c:choose>
			    	<c:when test="${sessionScope.automationExecBuildType == 5 }">
			    		<option value="Internet Explorer">IE</option>
			    	</c:when>
			    	<c:otherwise>
			    		<option value="IE">IE</option>
			    	</c:otherwise>
		    	</c:choose>
		    	<option value="Chrome">Chrome</option>
		    	<option value="Firefox">Firefox</option>
		    	<option value="Safari">Safari</option>
		    </select>
		    <label class="error hidden" style="margin-top: 2%; margin-left: 3%;">This field is required.</label>
		  </div>
		  <%-- <c:if test="${sessionScope.automationExecBuildType == 5 }">
			  <div class="form-group col-md-4">
			    <label for="priority">Select VM:</label>
			    <select class="form-control" id="id-vm-select">
			        <option value="">Select</option>
			      </select>
			  </div>
		  </c:if> --%>
		  <div class="form-group col-md-4">
		    <label for="priority">Mail Group:</label>
		    <select class="form-control" name="selectMailGroup" id="selectMailGroup">
					<option value="0">Select</option>
							<c:forEach var="mailGroupList"
									items="${Model.mailGroupList}">
								<option value="${mailGroupList.mail_group_id}">${mailGroupList.mail_group_name}</option>
							</c:forEach>
			</select>
		  </div>
		  <c:if test="${sessionScope.automationExecBuildType != 5 }">
			   <div class="form-group col-md-4">
			    <label for="priority">Environment:</label>
			    <select data-placeholder="Select" class="chosen-select" tabindex="2" name="selectEnv" id="selectEnv" multiple="multiple">
						<c:forEach var="envListData" items="${Model.envList}">
								<option value="${envListData.env_id}">${envListData.env_name}</option>
						</c:forEach>
				</select>
			  </div>
		  </c:if>
		  <div class="form-group col-md-4" id="screenshotDiv">
		    <label for="priority">Stepwise Screenshot:</label>
		    <select class="form-control" id="stepwiseScreenshot">
		        <option value="1">All Cases</option>
		        <option value="2">failed cases</option>
		        <option value="3">Disable</option>
		      </select>
		  </div>
		  <c:if test="${sessionScope.automationExecBuildType != 5 && sessionScope.automationExecBuildType != 4 }">
			  <div class="form-group col-md-4" id="dataDiv">
			    <label for="priority">Data:</label>
			    <select class="form-control" id="sel1">
			    	<option>Select</option>
			        <option value="download">Download</option>
			        <option value="upload">Upload</option> 
			      </select>
			  </div>
		  </c:if>
		  <div class="form-group col-md-4">
		    <label for="priority">Report Bug:</label>
		    <select class="form-control" id="reportBug">
		        <option value="1">Active</option>
		        <option value="2">Inactive</option>
		      </select>
		  </div>
		  <%-- <c:if test="${sessionScope.automationExecBuildType != 5 }"> --%>
		   <%-- <c:if test="${sessionScope.automationExecBuildType == 4 }"> --%>   
			  <div class="form-group col-md-4" id="authDiv">
			    <label for="priority">Authentication:</label>
			    <select class="form-control" id="authentication">
			    	<option value="Select">Select</option>
			        <option value="Basic">Basic</option>
			        <option value="OAuth1">OAuth 1.0</option>
			        <option value="OAuth2">OAuth 2.0</option>
			      </select>
			  </div>
		  <%-- </c:if> --%>
		  <div class="form-group col-md-4 hidden" id="selectRepository">
				
						<label>Repository : </label>
							<select name="selectRepo" id="selectRepo">
											<c:forEach var="repoListData" items="${Model.repoList}">
												<option value="0">Select</option>
												<option value="${repoListData.repo_id}">${repoListData.repo_name}</option>
											</c:forEach>
							</select>
					
			</div>
		  <div class="form-group col-md-4">
		  <c:choose>
		  	<c:when test="${sessionScope.automationExecBuildType == 5 }">
		  		<button type="button" class="btn btn-primary" style="margin-right:5px;" id="executeRHQBtnModal" disabled="disabled">Execute</button>
		  	</c:when>
		  	<c:otherwise>
		  		<button type="button" class="btn btn-primary" style="margin-right:5px;" id="executeBtnModal"  disabled="disabled">Execute</button>
		  	</c:otherwise>
		  </c:choose>
		 <!--  <button type="button" class="btn btn-secondary">Cancel</button>-->
		  </div>
		</form>
		
		</div>
		</div>
	</div>
</div> 

			<div class="row">
			<div class="col-md-12">
        
                <div class="mt-10 float-e-margins white-bg usr-dtl">
                <!-- <div class="title">
					<h2>Preview</h2>
					</div>-->
                    
                    <div class="ibox-content">
                        <!-- <a href="buildexecute" class="btn btn-success">Add/Remove
                            TestCase</a>-->
                       <div class="table-responsive" style="overflow-x: visible;table-layout:fixed; width:100%">
                        <table class="table table-striped table-bordered table-hover dataTables-example" id="testDataExecution" style="width:100%">
							<thead>
                                <tr>
                                    <!-- <th>Test Case Id</th> -->
                                    <th>Test ID</th>
                                    <th>Test Case Name</th>
                                    <th>Summary</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="previewDetails" items="${Model.previewDetails}">
                                    <tr>
                                        <!--<td>${previewDetails.testcase_id}</td>-->
                                        <td>${previewDetails.test_prefix}</td>
                                        <td>${previewDetails.testcase_name}</td>
                                        <td>${previewDetails.summary }</td>
                                        </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
           
         
</div>
</div>
</div>

<div class="modal inmodal" id="uploadDocModal" tabindex="-1"
	role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-md">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<h6 class="modal-title pull-left col-sm-10">Upload Document
					Form</h6>
				<button type="button" class="close pull-right" data-dismiss="modal">
					<span aria-hidden="false">�</span><span class="sr-only">Close</span>
				</button>

			</div>

			<div class="modal-body">
					<div class="form-group" id="uploadDatasheetDiv">
						<label class="control-label col-lg-4">Upload Document</label>
						<div class="fileupload fileupload-new" data-provides="fileupload">
							<span class="btn btn-success btn-file"><span
								class="fileupload-new"> <i class="fa fa-upload"></i>
									Choose File
							</span><input type="file"
								accept=".xls,.xlsx" id="uploadDatasheet" name="uploadDatasheet" /></span>
							<span class="fileupload-preview"></span> <a href="#"
								class="close fileupload-exists" data-dismiss="fileupload"
								style="float: none">�</a> <label class="error hidden"
								id="sizeError">Please upload file less than 2 MB</label> <label
								class="error hidden" id="fileError">Please select file
								to upload</label>

						</div>
					</div>
					<!-- <input name="uploadDatasheetBuildId" id="uploadDatasheetBuildId"
						class="hidden" value=""> -->

<!-- 					<div class="modal-footer"> -->
<!-- 						<button type="button" id="btnCancel" -->
<!-- 							class="btn btn-white pull-left" data-dismiss="modal">Cancel</button> -->
<!-- 						<button type="button" -->
<!-- 							class="btn btn-success pull-left ladda-button ladda-button-demo" -->
<!-- 							data-dismiss="modal" id="submitDetailsBtn" onclick="submitFile()" data-style="slide-up">Submit</button> -->
<!-- 					</div> -->
			</div>

		</div>
	</div>
</div>

<div class="modal fade" id="downloadDocModal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<h6 class="modal-title pull-left col-sm-10">Document Download Form</h6>
				<button type="button" class="close pull-right" data-dismiss="modal">
					<span aria-hidden="false">�</span><span class="sr-only">Close</span>
				</button>

			</div>

			<div class="modal-body">
					<div class="form-group" id="uploadDatasheetDiv">
						<label class="control-label col-lg-4">Do you want to download template?</label>
						<div class="fileupload fileupload-new" data-provides="fileupload">
							<!--  <button class="btn btn-success btn-file" onclick="downloadTemplate()"><span
								class="fileupload-new"> <i class="fa fa-download"></i>
									Download Template
							</span>
							</button>
							
							<button class="btn btn-success btn-file"><span
								class="fileupload-new"> <i class="fa fa-download"></i>
									Download File
							</span>
							</button>-->
							
							<button class="btn btn-success btn-file" onclick="downloadTemplate()"><span
								class="fileupload-new">
									Yes
							</span>
							</button>
							
							<button class="btn btn-success btn-file" onclick="closeTemplate()"><span
								class="fileupload-new">
									No
							</span>
							</button>
						</div>
						
					</div>
					
					
					<!-- <input name="uploadDatasheetBuildId" id="uploadDatasheetBuildId"
						class="hidden" value=""> -->

<!-- 					<div class="modal-footer"> -->
<!-- 						<button type="button" id="btnCancel" -->
<!-- 							class="btn btn-white pull-left" data-dismiss="modal">Cancel</button> -->
<!-- 						<button type="button" -->
<!-- 							class="btn btn-success pull-left ladda-button ladda-button-demo" -->
<!-- 							data-dismiss="modal" id="submitDetailsBtn" onclick="submitFile()" data-style="slide-up">Submit</button> -->
<!-- 					</div> -->
			</div>

		</div>
	</div>
</div>

<div class="modal inmodal" id="basicAuthDocModal" tabindex="-1"
	role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-md">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<h6 class="modal-title pull-left col-sm-10">Basic Authentication
					Form</h6>
				<button type="button" class="close pull-right" data-dismiss="modal">
					<span aria-hidden="false">�</span><span class="sr-only">Close</span>
				</button>

			</div>

			<div class="modal-body">
				<!-- <form class="form-inline">
					<div class="form-group col-md-3" id="">
						<label class="control-label col-lg-4">Username:</label>
						 <input type="text" class="form-control" placeholder="Enter Username" id="basicUsername" title="" >
					</div>
				</form> -->
				<div class="row">
					<div class="col-md-12">
						<div class="form-group form-inline">  
							<lable class="">Username:</lable><input type="text" placeholder="Enter Username" id="basicUsername" class="form-control" style="width:60%"/><br><br>      
							<lable class="">Password:</lable><input type="password" placeholder="Enter Password" id="basicPassword" class="form-control" style="width:60%"/><br>      
							<button type="button" class="btn btn-success pull-right" data-dismiss="modal">Ok</button>       
						</div>   
					</div>
				</div>  
			</div>

		</div>
	</div>
</div>

<div  id="div_testData">
</div>
<div id="redwood">
<iframe name="redowoodWindow" frameborder="1" style="overflow:hidden;height:800;width:100%" height="500" width="100%">

</iframe>

</div>

<script type="text/javascript">
	$(window).load(function() {
		sessionStorage.setItem('selectedExecuteBuildName','${Model.selectedExecuteBuildName}');
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");

		var foo = $('.footable').footable();
		foo.trigger('footable_initialize'); //Reinitialize
		foo.trigger('footable_redraw'); //Redraw the table
		foo.trigger('footable_resize'); //Resize the table

		var addedTcCount = '${Model.addedTcCount}';

		if (addedTcCount > 0) {
			$(".wrapper").fadeIn();
		} else {
			$("#barInMenu").fadeIn();
			$(".wrapper").fadeOut();
			
// 			swal({
// 				title : "Test case is not added to build for execution",
// 				text : "Please add test cases to build and then execute!",
// 				type : "warning",
// 				showCancelButton : true,
// 				confirmButtonColor : "#DD6B55",
// 				confirmButtonText : "Add Test Cases",
// 				cancelButtonText : "Cancel",
// 				closeOnConfirm : false,
// 				closeOnCancel : false
// 			}, function(isConfirm) {
// 				if (isConfirm) {
// 					window.location.href = "buildexecute";
// 				} else {
<%-- 					<%session.setAttribute("newBuildTypeForTab", "automation");%> --%>
// 					window.location.href = "viewbuild";
// 				}
// 			});
			
			/* 
			 swal({
			        title: "Test case is not added to build for execution",
			        text: "Please add test cases to build and then execute!",
			        type: "error",
			        showCancelButton: true,
			        confirmButtonColor: "#AEDEF4",
			        confirmButtonText: "Add Test Case",
			        closeOnConfirm: false
			    }, function () {
			    	window.location.href="viewbuild";
			    }); */
			//swal("No test case is added to execute", "Please first add test cases to build and then execute", "error");
		}
		
		if(${sessionScope.automationExecBuildType==4}){ 
	    	 $('#authDiv').show();
	    	 $('#browserDiv').hide();
	    	 $('#screenshotDiv').hide();
	    	 $('#dataDiv').hide();
	    	  
	     }else{
	    	 $('#authDiv').hide();	 
	     }   

	});
	function toLocation(url) {
	    var a = document.createElement('a');
	    a.href = url;
	    return a;
	};
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
		$(".chosen-select").chosen();
		$('.chosen-container').css('width', '100%');
		$('.chosen-container').css('position', 'absolute');
		if( '${addedTcCount  }' == 0 ) {
	    	 $('#vmPropBlock').find('input, textarea, button, select').prop('disabled','disabled');
			 $('#vmBlock').find('input, textarea, button, select').prop('disabled','disabled');
		}
		/* if( '${sessionScope.automationExecBuildType }' == 5  ) {
			var vm = toLocation('${sessionScope.redwood_url }').hostname;
			$('#id-vm-select').append($("<option/>", {
			    value: vm+':1',
			    text: vm
			}));
		} */
	});

	function validateDropdown(idDropdown) {
		var value = $("#" + idDropdown).val();
		if (value == null || value == 0) {

			$("#" + idDropdown).parents('.dropdown').addClass('has-error');
			status = false;
			$("#" + idDropdown).parents('.dropdown').next('.error')
					.removeClass('hidden');
			if (idDropdown.localeCompare("selectVM") == 0) {
				$("#viewVmDetails").addClass('hidden');
			}
		} else {
			$("#" + idDropdown).parents('.dropdown').removeClass('has-error');
			$("#" + idDropdown).parents('.dropdown').next('.error').addClass(
					'hidden');
			if (idDropdown.localeCompare("selectVM") == 0) {
				$("#viewVmDetails").removeClass('hidden');
			}
		}
	}

	$('#executeRHQBtnModal').click( function( e ) {
		e.preventDefault();
		var formData = {};
		formData['selectVM'] = $("#selectVM option:selected").val();
		formData['selectVMExecCount'] = $("#selectVMExecCount").val();
		if( $('#selectBrowser').val() != '' && $('#selectBrowser').val() != 0 ) {
// 			formData['browser'] = $('#selectBrowser').val();
			formData['selectBrowser'] = $("#selectBrowser").val();
			formData['exebrowser'] = $("#selectBrowser :selected").text();
		}
		//formData['machine'] = $("#selectVMIPAddress").val()+":1"; 
		var vm = toLocation('${sessionScope.redwood_url }').hostname;
		formData['machine'] = vm+":1";
		//alert(formData);
// 		console.log(JSON.stringify(formData)); 
		//return false;    
		/*
		formData['reportBugStatus'] = $("#reportBug option:selected").val();
		formData['stepwiseScreenshot'] = $("#stepwiseScreenshot option:selected").val();
		formData['mailGroupId'] = $("#selectMailGroup").val(); 
		*/
        $.ajax({
            type : "POST",
            url : "executeRHQTesSet",
            data : formData

        }).done( function( response ){
        	
            //window.location.href = "autoexecute";
        }).fail( function( jqXHR, testStatus, errorThrown ) {
            return false;
        });
        swal("Added!", "Test Cases Has Been Submitted For Execution.", "success");
        window.location.href= "bpftVnc";
        formData = {};
	});
	$('#executeBtnModal')
			.click(
					function() {
						
						var status = true;
						//check for the validity of select
						/*$('.validateClass')
								.each(
										function() {
											var value = $(this).val();
											if (value == null || value == 0) {
												$(this).parents('.dropdown')
														.addClass('has-error');
												status = false;
												$(this).parents('.dropdown')
														.next('.error')
														.removeClass('hidden');
												$(this).parents('.dropdown')
														.next('.error').css(
																"margin-top",
																'7%');

											} else {
												$(this).parents('.dropdown')
														.removeClass(
																'has-error');
												$(this).parents('.dropdown')
														.next('.error')
														.addClass('hidden');
											}

										});

						$('.validateClass').each(function() {
							var value = $(this).val();
							if (value == null || value == 0) {
								$(this).addClass('has-error');
								status = false;
								$(this).next('.error').removeClass('hidden');
								$(this).next('.error').css("margin-top", '7%');

							} else {
								$(this).removeClass('has-error');
								$(this).next('.error').addClass('hidden');
							}

						});*/
						/*var onPremiseStatus = ${Model.isPremise};
						if(onPremiseStatus==1){
							if($('input[id^=selectedVm_]:checked').length==0)
							{
								$("#SelectedVmNameTextbox").addClass('has-error');
								status = false;
								$("#SelectedVmNameTextboxErr").removeClass('hidden');

							}
							else{
								$("#SelectedVmNameTextbox").removeClass('has-error');
								$("#SelectedVmNameTextboxErr").addClass('hidden');
							}
						}*/
						
						if (status == true) {
							//$('#executeBuildForm').submit();

							$('#executeModal').modal('hide');

							$('body').addClass("white-bg");
							$("#barInMenu").removeClass("hidden");
							$("#wrapper").addClass("hidden");

							var customerStatus = '${Model.isPremise}';
							
							var onstartURL = "startoncloudeexecution";
							var executeBuildURL = "executeoncloudebuild";
							
							if (customerStatus == '1') {
								onstartURL = "startonpremiseexecution";
								executeBuildURL = "executeonpremisebuild";
							}
						
							$
									.ajax({
										type : "POST",
										async : true,
										url : onstartURL,
										data : {
											selectEnv : $("#selectEnv").val(),
											selectVM :$("#selectVM option:selected").val(),
											selectVMExecCount :$("#selectVMExecCount").val(),
											selectBrowser : $("#selectBrowser").val(),
											exebrowser : $("#selectBrowser :selected").text(),
											selectMailGroup : $("#selectMailGroup").val(),
											stepwiseScreenshotStatus:$("#stepwiseScreenshot option:selected").val(),
											reportBugStatus : $("#reportBug option:selected").val(),
											selectRepo : $("#selectRepo option:selected").val(),
											selectAuthentication: $("#authentication option:selected").val(),
											basicUsername: $("#basicUsername").val(),
											basicPassword: $("#basicPassword").val(),
										},
										success : function(data) {

											if (data == 'SERVER NOT RUNNING') {
												swal(
														"Alert!",
														"Server VM Is Not Running At This Moment, Please Contact Admin.",
														"error");
												$('.confirm').click(function() {
												location.reload(true);
												});
											}
											
											else if (data == 'EXECUTION_ADDED_IN_QUEUE') {
												swal(
														"Added!",
														"Execution successfully added in queue",
														"success");
												$('.confirm').click(function() {
													window.location.href = "autoexecute";
												});
											}
											
											else if (data == 'EXECUTION_ADD_UNSUCCESSFUL') {
												swal(
														"Alert!",
														"Execution add in queue unsuccessful",
														"success");
												$('.confirm').click(function() {
													location.reload(true);
												});
											}
											
											else if (data == 'VM SERVER NOT RUNNING') {
												swal(
														"Alert!",
														"VM Server Is Not Running At This Moment, Please Contact Admin.",
														"error");
												$('.confirm').click(function() {
													location.reload(true);
												});
											}

											else if (data == 'CLIENT NOT RUNNING') {
												swal(
														"Alert!",
														"Client VM Is Not Running At This Moment, Please Contact Admin.",
														"error");
												$('.confirm').click(function() {
													location.reload(true);
												});
											} else if (data == 'STARTING CLIENT') {
												
																
																	window.location.href = "autoexecute";
																
											} else if (data == 'EXECUTION_IN_PROGRESS') {
												swal(
														"Alert!",
														"Concurrent VM Execution in Progress, Please start your execution again",
														"error");
												$('.confirm').click(function() {
													window.location.href = "autoexecute";
												});
											}else if (data == 'FREE VM NOT FOUND') {
												swal(
														"Alert!",
														"No free VM found at this moment, Please contact admin for more details.",
														"error");
												$('.confirm').click(function() {
													location.reload(true);
												});
											} else if (data == 'PROJECT LOCATION NOT EXISTS') {
												swal(
														"Alert!",
														"Project Location Not Exist, Please Enter Valid Location.",
														"error");
												$('.confirm').click(function() {
													location.reload(true);
												});
											} else if (data == 'OK') {

												$(this).prop('disabled', true);

												swal(
														"Added!",
														"Test Cases Has Been Submitted For Execution.",
														"success");

												$('.confirm')
														.click(
																function() {
																	window.location.href = "autoexecute";
																});

												$
														.ajax({
															type : "POST",
															async : true,
															url : "buildautomation",
															data : {
																exebrowser : $(
																		"#selectBrowser :selected")
																		.text()
															},
															success : function(
																	data) {

																/* if (data == 'Fail')
																	{
																	 swal("Alert!", "Internal Server Error. Execution Process Interrupted.", "warning");
																	 $('.confirm').click(function() {
																          location.reload(true);
																      });
																	} */
															}
														});

											} else if (data == 'INTERNAL SERVER ERROR') {
												swal(
														"Alert!",
														"Internal Server Error.",
														"error");
												$('.confirm').click(function() {
													location.reload(true);
												});
											} else {
												$('body').removeClass(
														"white-bg");
												$("#barInMenu").addClass(
														"hidden");
												$("#wrapper").removeClass(
														"hidden");
											}
										}

									});
						}

					});

	$("#viewVmDetails").click(function() {
		window.location.href = "vmexecdetails";
	});

	$("#cancelBtn").click(function() {
		window.location.href = "viewbuild";
	});

	
	$(window)
			.load(
					function() {
						/*$('div[id^=testcaseInfoDiv_]').fadeOut("fast");
						var foundFlag = 0;
						var vmJSONList = ${Model.vmJSONList};
						var onPremiseStatus = ${Model.isPremise};
						vmJSONList = vmJSONList[0];
						var sizeDropdown = vmJSONList.length;
						if (sizeDropdown == 0) {
							swal(
									"Alert!",
									"No free VM found at this moment, Please contact to administrator for more details.",
									"error");
							$('.confirm').click(function() {
								window.location.href = "viewbuild";
							});
						}

						var mailGroupList = ${Model.mailGroupList.size()};
						if (mailGroupList > 0) {
							$("#selectMailGroupDiv").removeClass("hidden");
						} else {
							$("#selectMailGroupDiv").addClass("hidden");
						}

						if (onPremiseStatus == 2) {
							var ie_status = vmJSONList[0].ie_status;
							var chrome_status = vmJSONList[0].chrome_status;
							var firefox_status = vmJSONList[0].firefox_status;
							var safari_status = vmJSONList[0].safari_status;
							$("#selectBrowser option").remove();
							$("#selectBrowser").append(
									"<option value='0'>Select</option>");
							if (ie_status == '1') {
								$("#selectBrowser")
										.append(
												"<option value='1'>IE</option>");
							}
							if (chrome_status == 1)
								$("#selectBrowser").append(
										"<option  value='2'>Chrome</option>");

							if (firefox_status == 1)
								$("#selectBrowser").append(
										"<option  value='3'>Firefox</option>");

							if (safari_status == 1)
								$("#selectBrowser").append(
										"<option  value='4'>Safari</option>");

							foundFlag = 1;

							if (foundFlag == 1) {
								$("#hostNameDiv").css('margin-bottom', '1%');
								$("#browserDiv").removeClass("hidden");
								$("#selectBrowser").trigger('chosen:updated');
								$(".chosen-select").chosen();
								$('.chosen-container').css('width', '100%');
								$('.chosen-container').css('position',
										'absolute');
								$("#vmTableDiv").addClass("hidden");

								if ($('input[id^=selectedVm_]:checked').length == 0) {
									$("#SelectedVmNameTextbox").addClass(
											'has-error');
									status = false;
									$("#SelectedVmNameTextboxErr").removeClass(
											'hidden');

								} else {
									$("#SelectedVmNameTextbox").removeClass(
											'has-error');
									$("#SelectedVmNameTextboxErr").addClass(
											'hidden');
								}
							}
						}
*/
					});

	/*$("#vmListTable tr")
			.click(
					function() {
						if (!$(this).first("td").find("input[id^=selectedVm_]")
								.prop("disabled")) {
							//alert($(this).first("td").find("input[id^=selectedVm_]").val());
							$("input[id^=selectedVm_]").prop("checked", false);
							var value = $(this).first("td").find(
									"input[id^=selectedVm_]").val();
							$("#selectedVm_" + value).prop("checked", true);
							$("#SelectedVmNameTextbox").val(
									$('input[id^=selectedVm_]:checked').attr(
											'name'));
							$("#selectVM").val(
									$('input[id^=selectedVm_]:checked').val());
							$("#selectVMExecCount").val(
									$('input[id^=selectedVm_]:checked').attr("execCount"));
							var foundFlag = 0;
							var vmJSONList = ${Model.vmJSONList};
							vmJSONList = vmJSONList[0];
							var sizeDropdown = vmJSONList.length;
							for (var i = 0; i < sizeDropdown; i++) {
								if (vmJSONList[i].cust_vm_id == $(
										'input[id^=selectedVm_]:checked').val()) {
									var ie_status = vmJSONList[i].ie_status;
									var chrome_status = vmJSONList[i].chrome_status;
									var firefox_status = vmJSONList[i].firefox_status;
									var safari_status = vmJSONList[i].safari_status;
									$("#selectBrowser option").remove();
									$("#selectBrowser")
											.append(
													"<option value='0'>Select</option>");
									if (ie_status == '1') {
										$("#selectBrowser")
												.append(
														"<option value='1'>IE</option>");
									}
									if (chrome_status == 1)
										$("#selectBrowser")
												.append(
														"<option  value='2'>Chrome</option>");

									if (firefox_status == 1)
										$("#selectBrowser")
												.append(
														"<option  value='3'>Firefox</option>");

									if (safari_status == 1)
										$("#selectBrowser")
												.append(
														"<option  value='4'>Safari</option>");

									foundFlag = 1;

									break;
								} else
									foundFlag = 0;

							}

							if (foundFlag == 1) {
								$("#hostNameDiv").css('margin-bottom', '1%');
								$("#browserDiv").removeClass("hidden");
								$("#selectBrowser").trigger('chosen:updated');
								$(".chosen-select").chosen();
								$('.chosen-container').css('width', '100%');
								$('.chosen-container').css('position',
										'absolute');
								$("#vmTableDiv").addClass("hidden");

								if ($('input[id^=selectedVm_]:checked').length == 0) {
									$("#SelectedVmNameTextbox").addClass(
											'has-error');
									status = false;
									$("#SelectedVmNameTextboxErr").removeClass(
											'hidden');

								} else {
									$("#SelectedVmNameTextbox").removeClass(
											'has-error');
									$("#SelectedVmNameTextboxErr").addClass(
											'hidden');
								}
							} else {
								$("#browserDiv").addClass("hidden");
							}
						} else {
							swal(
									"Not Available",
									"Selected virtual machine is not available for execution. Please select another",
									"error");
						}
						

					});*/

	$('input[id^=selectedVm_]')
			.change(
					function() {
						if ($(this).is(':checked')) {
							$('div[id^=testcaseInfoDiv_').fadeOut("fast");
							$("input[id^=selectedVm_]").prop("checked", false);
							$(this).prop("checked", true);
							$("#SelectedVmNameTextbox").val(
									$('input[id^=selectedVm_]:checked').attr(
											'name'));
							$("#selectVM").val(
									$('input[id^=selectedVm_]:checked').val());
							$("#selectVMExecCount").val(
									$('input[id^=selectedVm_]:checked').attr("execCount"));
							var foundFlag = 0;
							var vmJSONList = ${Model.vmJSONList}	;
							vmJSONList = vmJSONList[0];
							var sizeDropdown = vmJSONList.length;
							for (var i = 0; i < sizeDropdown; i++) {
								if (vmJSONList[i].cust_vm_id == $(
										'input[id^=selectedVm_]:checked').val()) {
									var ie_status = vmJSONList[i].ie_status;
									var chrome_status = vmJSONList[i].chrome_status;
									var firefox_status = vmJSONList[i].firefox_status;
									var safari_status = vmJSONList[i].safari_status;
									$("#selectBrowser option").remove();
									$("#selectBrowser")
											.append(
													"<option value='0'>Select</option>");
									if (ie_status == '1') {
										$("#selectBrowser")
												.append(
														"<option value='1'>IE</option>");
									}
									if (chrome_status == 1)
										$("#selectBrowser")
												.append(
														"<option  value='2'>Chrome</option>");

									if (firefox_status == 1)
										$("#selectBrowser")
												.append(
														"<option  value='3'>Firefox</option>");

									if (safari_status == 1)
										$("#selectBrowser")
												.append(
														"<option  value='4'>Safari</option>");

									foundFlag = 1;

									break;
								} else
									foundFlag = 0;

							}
							if (foundFlag == 1) {
								$("#hostNameDiv").css('margin-bottom', '1%');
								$("#browserDiv").removeClass("hidden");
								$("#selectBrowser").trigger('chosen:updated');
								$(".chosen-select").chosen();
								$('.chosen-container').css('width', '100%');
								$('.chosen-container').css('position',
										'absolute');
								$("#vmTableDiv").addClass("hidden");

								if ($('input[id^=selectedVm_]:checked').length == 0) {
									$("#SelectedVmNameTextbox").addClass(
											'has-error');
									status = false;
									$("#SelectedVmNameTextboxErr").removeClass(
											'hidden');

								} else {
									$("#SelectedVmNameTextbox").removeClass(
											'has-error');
									$("#SelectedVmNameTextboxErr").addClass(
											'hidden');
								}
							} else {
								$("#browserDiv").addClass("hidden");
							}
							
							var repository= $(this).attr('gitStatus');
							if(repository==1){
								$("#selectRepository").removeClass("hidden");
								
							}
							else{
								$("#selectRepository").addClass("hidden");
								$("#selectRepo").removeClass("validateClass");
							}
						}
					});
	/* 
	function showrepo(radioClass){
		alert($(this).parent('.Row').children('td:nth-child(3)').html());
	} */
	function uploadDocument() {
		$('#uploadDocModal').modal('show'); 
	}
	
	//$("#submitDetailsBtn").click(function(){
		$("#uploadDatasheet").change(function(){
		 var formData=new FormData();
			formData.append('uploadForm',this.files[0]);
		var res=$.ajax({
			type:"POST",
	 		url:"uploadDataFile",
	 		contentType:false,
	 		processData:false,
	 		data:formData,
			 success: function(data,e){
				 if(data=="success"){
					 toastr.options = {
							 "closeButton": true,
							  "debug": true,
							  "progressBar": true,
							  "preventDuplicates": true,
							  "positionClass": "toast-top-right",
							  "showDuration": "400",
							  "hideDuration": "1000",
							  "timeOut": "1500",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "slideDown",
							  "hideMethod": "slideUp"
				     };
				     toastr.success('',"File has been uploaded");
				     $('#uploadDocModal').modal('hide'); 
				    
				 }else{
					 toastr.options = {
							 "closeButton": true,
							  "debug": true,
							  "progressBar": true,
							  "preventDuplicates": true,
							  "positionClass": "toast-top-right",
							  "showDuration": "400",
							  "hideDuration": "1000",
							  "timeOut": "1500",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "slideDown",
							  "hideMethod": "slideUp"
				     };
				     toastr.error('',"Upload Error!");
				     $('#uploadDocModal').modal('hide'); 
				 }
			 }
			});
		});
	//});
</script>

<script>
    function autoreport() {
        document.getElementById("testData").style.display = 'block';
    }
</script>

<script>
function closeTemplate(){
	$('#downloadDocModal').modal('hide');
}
function createBugToJiraByBuildId(){
	var buildId=${sessionScope.automationExecBuildId};
	var l = $('.ladda-button-demo').ladda();
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");
	document.getElementById('syncToJira').href = "javascript:void(0)";
	$.ajax({        
	    url: 'createBugToJiraByBuildId/'+buildId,
	    type: 'GET',
	    success: function (resp) { 
	    	l.ladda('stop');
			$('body').removeClass("white-bg");
			$("#barInMenu").addClass("hidden");
			$("#wrapper").removeClass("hidden");
	    	toastr.options = {
					 "closeButton": true,
					  "debug": true,
					  "progressBar": true,
					  "preventDuplicates": true,
					  "positionClass": "toast-top-right",
					  "showDuration": "400",
					  "hideDuration": "1000",
					  "timeOut": "1500",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "slideDown",
					  "hideMethod": "slideUp"
		     };
	    	
		     if(resp.includes(',')){
		    	
			     toastr.success(' ',resp);
		     }
		     toastr.error(' ',resp);
	    }
		});
}
function downloadTemplate(){
	var buildId = '${sessionScope.BuildId}';
	
	var res=$.ajax({
		type:"GET",
 		url:"checkTestData/"+buildId,
		 success: function(data,e){
			 if(data=="DataSuccess"){
				 toastr.options = {
						 "closeButton": true,
						  "debug": true,
						  "progressBar": true,
						  "preventDuplicates": true,
						  "positionClass": "toast-top-right",
						  "showDuration": "400",
						  "hideDuration": "1000",
						  "timeOut": "1500",
						  "extendedTimeOut": "1000",
						  "showEasing": "swing",
						  "hideEasing": "linear",
						  "showMethod": "slideDown",
						  "hideMethod": "slideUp"
			     };
			     //toastr.success('',"File has been uploaded");
				 $('#downloadDocModal').modal('hide'); 
				 window.location.href='getTestData/'+ buildId; 
			    
			 }else{
				 toastr.options = {
						 "closeButton": true,
						  "debug": true,
						  "progressBar": true,
						  "preventDuplicates": true,
						  "positionClass": "toast-top-right",
						  "showDuration": "400",
						  "hideDuration": "1000",
						  "timeOut": "1500",
						  "extendedTimeOut": "1000",
						  "showEasing": "swing",
						  "hideEasing": "linear",
						  "showMethod": "slideDown",
						  "hideMethod": "slideUp"
			     };
			     toastr.error('',"Please add parameters to test cases!");
			     $('#downloadDocModal').modal('hide');
			 }
		 }
		});
}
</script>

<script>
$("#selectVM").change(function(){
	var vmId=$("#selectVM option:selected").val();  
	if(vmId=="0"){  
		$("#vmRepository").val("");
		$("#vmStatus").val("");
		$("#vmExecution").val("");
		$('#executeRHQBtnModal').prop('disabled', true);      
		$('#executeBtnModal').prop('disabled', true);
	}else{
		$.ajax({        
		    url: 'getVMDetails/'+ vmId,
		    type: 'GET',
		    success: function (resp) {
		    	var vmData=JSON.parse(resp);
		    	$("#selectVM").attr("title",vmData.ip_address);
		    	if(vmData.git_status==1){
		    		$("#vmRepository").val("Git");
		    		$("#selectRepository").removeClass("hidden");
		    	}
		    	if(vmData.git_status==2){
		    		$("#vmRepository").val(vmData.project_location);
			    	$("#vmRepository").attr("title",vmData.project_location);
			    	$("#selectRepository").addClass("hidden");
					$("#selectRepo").removeClass("validateClass");
		    	}
		    	
		    	if(vmData.current_execs >= vmData.concurrent_execs){
		    		$("#vmStatus").val("Not Available");
		    	}
		    	if(vmData.current_execs==0){
		    		$("#vmStatus").val("Available");
		    		
		    	}
		    	if(vmData.current_execs>0  && vmData.current_execs < vmData.concurrent_execs){
		    		$("#vmStatus").val("Busy");
		    	}
		    	
		    	$("#vmExecution").val(vmData.current_execs+"/"+vmData.concurrent_execs);
		    	
		    	$("#selectBrowser option").remove();
		    	$("#selectBrowser").append(
				"<option value='0'>Select</option>");
					if (vmData.ie_status == '1') {
						$("#selectBrowser")
								.append(
										"<option value='1'>IE</option>");
					}
					if (vmData.chrome_status == 1)
						$("#selectBrowser").append(
								"<option  value='2'>Chrome</option>");
				
					if (vmData.firefox_status == 1)
						$("#selectBrowser").append(
								"<option  value='3'>Firefox</option>");
				
					if (vmData.safari_status == 1)
						$("#selectBrowser").append(
								"<option  value='4'>Safari</option>");
					
					$("#selectVMExecCount").val(vmData.current_execs);
					$("#selectVMIPAddress").val(vmData.ip_address);
					
					$('#executeRHQBtnModal').prop('disabled', false);
		    		$('#executeBtnModal').prop('disabled', false);
			}
		  
		});
	}//end else
});
	



</script>


<script>
$("#testDataExecution").DataTable({
	dom : 'lTfgitp',
	/* autoFill: false, */
	/* searching:false, */
	"paging" : true,
	"lengthChange" : true,
	"pageLength": 10,
	"scrollX": true,
	initComplete: function() {
        $(this.api().table().container()).find('input').parent().wrap('<form>').parent().attr('autocomplete', 'off');
    }
	
});

$( document ).ready(function() {
    var autoBuildState=${sessionScope.autoBuildState};
     if(autoBuildState==4 || autoBuildState==5 || autoBuildState==7 || autoBuildState==8 || autoBuildState==10 || autoBuildState==11){
    	 $('#vmPropBlock').find('input, textarea, button, select').prop('disabled','disabled');
    	 $('#selectEnv').prop('disabled', true).trigger("chosen:updated");
		 $('#vmBlock').find('input, textarea, button, select').prop('disabled','disabled');
    }
     /* if(${sessionScope.automationExecBuildType==4}){
    	 $('#authDiv').show();
    	 $('#browserDiv').hide();
    	 $('#screenshotDiv').hide();
    	 $('#dataDiv').hide();
     }else{
    	 $('#authDiv').hide();	 
     } */  
     
     //var duplicateBuild=${sessionScope.duplicateBuildError}; 
     var duplicateBuild=$("#buildExist").val();
     if(duplicateBuild=="1"){
	    $.ajax({        
	 	    url: 'setDuplicateBuildSession',
	 	    type: 'POST',
	 	    success: function (resp) {
	 	    	if(resp=="Success"){
	 	    		 toastr.options = {
	 	   				 "closeButton": true,
	 	   				  "debug": true,
	 	   				  "progressBar": true,
	 	   				  "preventDuplicates": true,
	 	   				  "positionClass": "toast-top-right",
	 	   				  "showDuration": "400",
	 	   				  "hideDuration": "1000",
	 	   				  "timeOut": "1500",
	 	   				  "extendedTimeOut": "1000",
	 	   				  "showEasing": "swing",
	 	   				  "hideEasing": "linear",
	 	   				  "showMethod": "slideDown",
	 	   				  "hideMethod": "slideUp"
	 	   	     };   
	 	   	     toastr.error('',"Duplicate Test Set!"); 
	 	    	}
	 	    }
	     });       
     }
     
});

function refresh(){

	var buildId=${sessionScope.automationExecBuildId};
	$.ajax({        
	    url: 'refreshBuildState/'+buildId,
	    type: 'GET',
	    success: function (resp) {  
	    	switch (resp) {
	    		case 0:
	    			 toastr.options = {
						 "closeButton": true,
						  "debug": true,
						  "progressBar": true,
						  "preventDuplicates": true,
						  "positionClass": "toast-top-right",
						  "showDuration": "400",
						  "hideDuration": "1000",
						  "timeOut": "1500",
						  "extendedTimeOut": "1000",
						  "showEasing": "swing",
						  "hideEasing": "linear",
						  "showMethod": "slideDown",
						  "hideMethod": "slideUp"
			     };   
			     toastr.error('',"Session time out!");
	    	    break;
	    	    
	    		case 1:
	    		  $("#id-state").text("Created").removeClass().addClass('label 4 label-success');
	    	    break;
	    	    
	    		case 2:
		    		  $("#id-state").text("In Planning").removeClass().addClass('label 4 label-info');
		    	    break;
		    	    
	    		case 3:
		    		  $("#id-state").text("Ready for execution").removeClass().addClass('label 4 label-warning');
		    	    break;
		    	    
	    		case 4:
		    		  $("#id-state").text("Execution in Progress").removeClass().addClass('label 4 label-danger');
		    		  $('#vmPropBlock').find('input, textarea, button, select').prop('disabled','disabled');
		    	      $('#selectEnv').prop('disabled', true).trigger("chosen:updated");
		    		  $('#vmBlock').find('input, textarea, button, select').prop('disabled','disabled');
		    	    break;
		    	    
	    		case 5: 
		    		  $("#id-state").text("Completed").removeClass().addClass('label 4 label-primary');;
		    		  $('#vmPropBlock').find('input, textarea, button, select').prop('disabled','disabled');
		    	      $('#selectEnv').prop('disabled', true).trigger("chosen:updated");
		    		  $('#vmBlock').find('input, textarea, button, select').prop('disabled','disabled');
		    	    break;
		    	    
	    		case 6:
		    		  $("#id-state").text("Ready for Execution").removeClass().addClass('label 4 label-warning');
		    	    break;
		    	    
	    		case 7:
		    		  $("#id-state").text("Stop By User").removeClass().addClass('label 4 label-warning');
		    		  $('#vmPropBlock').find('input, textarea, button, select').prop('disabled','disabled');
		    	      $('#selectEnv').prop('disabled', true).trigger("chosen:updated");
		    		  $('#vmBlock').find('input, textarea, button, select').prop('disabled','disabled');
		    	    break;
		    	    
	    		case 8: 
		    		  $("#id-state").text("Execution Failed").removeClass().addClass('label 4 label-danger');
		    		  $('#vmPropBlock').find('input, textarea, button, select').prop('disabled','disabled');
		    	      $('#selectEnv').prop('disabled', true).trigger("chosen:updated");
		    		  $('#vmBlock').find('input, textarea, button, select').prop('disabled','disabled');
		    	    break;
		    	    
	    		case 11:
		    		  $("#id-state").text("Build Failure").removeClass().addClass('label 4 label-danger');
		    		  $('#vmPropBlock').find('input, textarea, button, select').prop('disabled','disabled');
		    	      $('#selectEnv').prop('disabled', true).trigger("chosen:updated");
		    		  $('#vmBlock').find('input, textarea, button, select').prop('disabled','disabled');
		    	    break;   
	    	}
	    }
	});
	
}
 </script>

<style>
div.ex3 { {
    overflow-y: scroll;
    margin-bottom: 120px;
    height: 50px;
    overflow-x: scroll;
}
}
.chosen-container-multi{
position:relative !important; 
width:74% !important;
min-width:auto !important;
}

.chosen-disabled .chosen-choices, .chosen-disabled .chosen-choices .search-field {
background-color: #eee !important;
	opacity: 1;
	cursor: not-allowed;
}

</style>


<script>
$('#sel1').change(function(){
	  //this is just getting the value that is selected
	  var title = $(this).val();
	  if(title=="download"){
	  	$('#downloadDocModal').modal('show');
	  }
	  if(title=="upload"){
		  $('#uploadDocModal').modal('show');   
	  }
	});
	
$('#authentication').change(function(){
	  //this is just getting the value that is selected
	  var title = $(this).val();
	  if(title=="Basic"){
	  	$('#basicAuthDocModal').modal('show');
	  }
	  if(title=="OAuth1"){
		  $('#oAuth1DocModal').modal('show');   
	  }
	});
</script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Virtual Machines</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li class="active"><strong>Execution Details</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Virtual Machines</h5>
					
				</div>

				<div class="ibox-content">
					<input type="text" class="form-control input-sm m-b-xs" id="filter"
						placeholder="Search in Module">
					<table class="footable table table-stripped toggle-arrow-tiny"
						data-page-size="10" data-filter="#filter">
						<thead>
							<tr>
								<th >Build name</th>
								<th data-toggle="true">VM Name</th>
								<!-- <th data-hide="all">Description</th> -->
								<th>Executed By</th>
								<th>Total TC</th>
								<th>Executed TC</th>
							</tr>
						</thead>
						<tbody>
							<%-- <c:forEach var="modules" items="${model.module}">
								<tr>
									<td>${modules.module_name}</td>
									<td>${modules.project_name}</td>
									<td>${modules.module_description}</td>
									<td><span class="label label-danger">Inactive</span></td>
									<td class="text-right">
									</td>
								</tr>
							</c:forEach> --%>
							<tr>
									<td>Salesforce</td>
									<td>Salesforce</td>
									<td>Bhagyashri</td>
									<td>10</td>
									<td>5
									</td>
							<tr><tr>
									<td>Salesforce</td>
									<td>Salesforce</td>
									<td>Bhagyashri</td>
									<td>10</td>
									<td>4
									</td>
							<tr><tr>
									<td>Salesforce</td>
									<td>Salesforce</td>
									<td>Bhagyashri</td>
									<td>10</td>
									<td>3
									</td>
							<tr><tr>
									<td>SAP</td>
									<td>11</td>
									<td>Bhagyashri</td>
									<td>10</td>
									<td>1
									</td>
							<tr><tr>
									<td>SAP</td>
									<td>11</td>
									<td>Bhagyashri</td>
									<td>10</td>
									<td>9
									</td>
							<tr><tr>
									<td>Infinera</td>
									<td>11</td>
									<td>Bhagyashri</td>
									<td>20</td>
									<td>14
									</td>
							<tr>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="10">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page-Level Scripts -->

<input type="hidden" id="hiddenInput"
	value=<%=session.getAttribute("moduleCreateStatus")%>>
	
<input type="hidden" id="newModuleName"
	value=<%=session.getAttribute("newModuleName")%>>

<script type="text/javascript">
$(window).load(function() {
	$("#barInMenu").fadeOut();
});
  $(function () {
	  
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	});  
</script>
<script>
	$(document).ready(function() {

		$('.footable').footable({
			
			"aaSorting": [[1, "desc"]]
		});
	});
	
</script>
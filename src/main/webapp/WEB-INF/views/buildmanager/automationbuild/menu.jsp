<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="bar" id="barInMenu">
	<p>loading</p>
</div>

<div id="wrapper">
	<nav class="navbar-default navbar-static-side" role="navigation">
		<div class="sidebar-collapse">
			<ul class="nav metismenu" id="side-menu">
				<%-- <li class="nav-header">
					<div class="dropdown profile-element">
						<span> <img alt="image" class="img-circle"
							style="width: 48px; height: 48px"
							src="data:image/jpg;base64,${Model.UserProfilePhoto }" />
						</span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
							class="clear"> <span class="block m-t-xs"> <strong
									class="font-bold">${Model.userName }</strong>
							</span>
						</span>
						</a> <span class="text-muted text-xs block">${Model.role}<b
							class=""></b></span>
					</div>
					<div class="logo-element" style="padding: 14px !important">
						<a href="#" onclick="gotoLinkInMenu('xedashboard')">
						<img alt="image" class=""
							src="<%=request.getContextPath()%>/resources/img/xenon.png" />
						</a>
					</div>
				</li> --%>
				<li class="nav-header">
					<div class="dropdown profile-element text-center">
						<a href="#" onclick="gotoLink('xedashboard')"> <img
							src="<%=request.getContextPath()%>/resources/img/Jade-Logo.png"
							class="xenon-logo">
						</a>
						<!--    <img class="img-circle" style="width:48px; height:48px" src="resources/img/user.jpg">
                         <img alt="image" class="img-circle"
                            style="width: 48px; height: 48px"
                            src="data:image/jpg;base64,${Model.UserProfilePhoto }" />
                        </span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
                            class="clear"> <span class="block m-t-xs"> <strong
                                    class="font-bold">${Model.userName }</strong>
                            </span>
                        </span>
                        </a> <span class="text-muted text-xs block">${Model.role}<b
                            class=""></b></span>  -->
					</div>
					<div class="logo-element" style="padding: 14px !important">
						<a href="#" onclick="gotoLinkInMenu('xedashboard')"> <img
							alt="image" class=""
							src="<%=request.getContextPath()%>/resources/img/xenon.png" />
						</a>
					</div>
				</li>
				<!-- <li><a href="buildexecute"><i class="fa fa-th-large"></i>
						<span class="nav-label">Applications</span>
					</a>
				</li> -->

				<li id="id-crud-btns-li">
					<div class="class-crud-btns-div">
						<ul>
							<li>
								<button id="id-add-release-btn" type="button"
									class="btn btn-success btn-xs" data-toggle="modal"
									data-target="#addReleaseModal">
									<i class="fa fa-plus" data-toggle="tooltip" title=""
										data-original-title="Add Release" data-placement="bottom"></i>
									<span>Add Release</span>
								</button>
							</li>
							<li>
								<button id="id-add-test-set-btn" type="button" disabled
									class="btn btn-success btn-xs" data-toggle="modal"
									data-target="#addTestSetModal">
									<i class="fa fa-plus" data-toggle="tooltip" title=""
										data-original-title="Add Test Set" data-placement="bottom"></i>
									<span>Add Test Set</span>
								</button>
							</li>
							<!-- <li id="id-update-module-li" data-toggle="modal" data-target="#updateModuleModal" class="disabled">
			                	<a href="#" data-toggle="tooltip" title="" data-original-title="Edit Module"><i class="fa fa-edit"></i></a>
			                </li> -->
							<!-- <li id="id-update-scenario-li" data-toggle="modal" data-target="#updateScenarioModal" style="display: none;">
			                	<a href="#" data-toggle="tooltip" title="" data-original-title="Edit Scenario"><i class="fa fa-edit"></i></a>
			                </li> -->
						</ul>
					</div>
				</li>
	<%-- 				<c:if test="${not empty sessionScope.redwood_url}"> --%>
<!-- 				<li class="mainLi"><a id='red' href="#">RedWood</a></li> -->
<%-- 				</c:if> --%>
				<c:forEach var="release" items="${releases }">
					<li class="mainLi class-release-li"
						data-release-name="${release.release_name }"
						data-release-id="${release.release_id }"
						data-release-description="${release.release_description }"
						data-release-end-date="<fmt:formatDate value="${release.release_end_date }" pattern="MM/dd/yyyy"/>"
						id="id-release-id-li-${release.release_id }">
						<%-- <li class="mainLi class-release-li" data-release-name="${release.release_name }" data-release-id="${release.release_id }"
							data-release-description="${release.release_description }" data-release-end-date="<fmt:formatDate value="${release.release_end_date }" pattern="MM/dd/yyyy"/>" 
							id="id-release-id-li-${release.release_id }"> --%> <a href="#"><i
							class="fa fa-th-large"></i> <span class="nav-label">${release.release_name }</span>
							<span class="fa arrow"></span> </a>
						<ul class="nav nav-second-level collapse">
							<c:forEach var="build" items="${builds }">
								<c:if test="${build.release_id == release.release_id }">
									<li id="build_${build.build_id }" class="class-test-set-li">
										<a data-original-title="${build.build_name }"
										class="customeTooltip menuTooltip lgMenuTextAlignment class-build-name-a"
										data-build-name="${build.build_name }"
										data-build-id="${build.build_id }" 
										data-release-id="${build.release_id}"
										data-build-type="${build.execution_type }" href="#">
											${build.build_name } </a>
									</li>
								</c:if>
							</c:forEach>
						</ul>
					</li>
				</c:forEach>

				<c:forEach var="projectsList" items="${Model.projectsList}">
					<li class="projectNameClick"
						id="projectMenu_${projectsList.project_id }"
						name="${projectsList.project_id }"><a href="#"
						id="projectHref_${projectsList.project_id}"
						data-original-title='${projectsList.project_name}'
						data-container='body' data-toggle='tooltip' data-placement='right'
						onclick="projectClick(${projectsList.project_id })"><i
							class="fa fa-th-large"></i> <span class="nav-label"
							style="margin-right: 10px;">${fn:substring(projectsList.project_name,0,15)}</span>

							<span class="fa arrow pull-right"></span></a>

						<ul class="nav nav-second-level collapse">
							<c:forEach var="modulesList" items="${Model.modulesList}"
								varStatus="moduleCounter">
								<c:if test="${projectsList.project_id==modulesList.project_id}">
									<li class="" id="moduleMenu_${modulesList.module_id}"><a
										href="#" id="moduleHref_${modulesList.module_id}"
										data-original-title='${modulesList.module_name}'
										data-container='body' data-toggle='tooltip'
										data-placement='right'
										onclick="moduleClick(${modulesList.module_id})">${fn:substring(modulesList.module_name,0,15)}<%-- <span
											class="label label-success "
											id="moduleCount_${modulesList.module_id}">0</span> --%> <span
											class="fa arrow pull-right"></span></a>

										<ul class="nav nav-third-level "
											id="moduleUl_${modulesList.module_id}"
											style="margin-left: 10px;">
											<c:forEach var="scenarioList" items="${Model.scenarioList}">
												<c:if
													test="${modulesList.module_id==scenarioList.module_id}">
													<li class="" id="scenarioMenu_${scenarioList.scenario_id}"><a
														href="#" id="scenarioHref_${scenarioList.scenario_id}"
														data-original-title='${scenarioList.scenario_name}'
														data-container='body' data-toggle='tooltip'
														data-placement='right'
														onclick="scenarioClick(${scenarioList.scenario_id})">${fn:substring(scenarioList.scenario_name,0,13)}<%-- <span
															class="label label-warning"
															id="scenarioCount_${scenarioList.scenario_id}">0</span> --%></a></li>
												</c:if>
											</c:forEach>


										</ul></li>
								</c:if>
							</c:forEach>
						</ul></li>
				</c:forEach>
				<!-- commenting document library -->
				<!-- 
				<li class="mainLi"><a href="documentlibrary"><i
						class="fa fa-files-o"></i> <span class="nav-label">Document
							Library</span></a></li>
				 -->
			</ul>

		</div>
	</nav>
	<div class="modal fade" id="addReleaseModal" tabindex="-1"
		role="dialog" aria-labelledby="addReleaseModalLabel"
		aria-hidden="false" style="display: none;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="addReleaseModalLabel">Add Release</h4>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="fasle">�</span>
					</button>
				</div>
				<div class="modal-body">
					<!-- 	      {"Form data":{"releaseName":"Test+Release","releaseDescription":"+","files":"","startDate":"10/18/2019","endDate":"10/18/2019","releaseStatus":"1"}} -->
					<form action="insertrelease" method="post" id="id-release-add-form">
						<input type="text" id="id-add-release-name-text"
							class="form-control" name="releaseName" placeholder="Add Release"
							required="required" pattern="^[^ ].+[^ ]$"
							oninvalid="setCustomValidity('Please do not enter leading and/or trailing spaces')"
							oninput="setCustomValidity('')">
						<jsp:useBean id="now" class="java.util.Date" />
						<input type="hidden" id="id-add-release-start-date-hidden"
							name='startDate'
							value='<fmt:formatDate value="${now }" pattern="MM/dd/yyyy"/>'>
						<input type="hidden" id="id-add-release-start-date-hidden"
							name='endDate'
							value='<fmt:formatDate value="${now }" pattern="MM/dd/yyyy"/>'>
						<input type="hidden" id="id-add-release-status-hidden"
							name='releaseStatus' value="1"> <input type="hidden"
							id="id-add-release-description-hidden" name='releaseDescription'
							value=" ">
						<!--  intentionally left blank. Dont remove it -->
						<input type="hidden" id="id-add-release-source-hidden"
							name='source' value="LeftNavMenuAutomation">
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary"
								data-dismiss="modal">Cancel</button>
							<!--         <input type="submit" class="btn btn-success" >Add Release<> -->
							<input type="submit" value="Add" class="btn btn-success">
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
	<div class="modal fade" id="updateReleaseModal" tabindex="-1"
		role="dialog" aria-labelledby="updateReleaseModalLabel"
		aria-hidden="false" style="display: none;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="updateReleaseModalLabel">Update
						Release</h4>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="fasle">�</span>
					</button>
				</div>
				<div class="modal-body">
					<!-- 	      {"Form data":{"releaseName":"Test+Release","releaseDescription":"+","files":"","startDate":"10/18/2019","endDate":"10/18/2019","releaseStatus":"1"}} -->
					<form action="updatereleasename" method="post"
						id="id-release-update-form">
						<input type="text" id="id-update-release-name-text"
							class="form-control" name="releaseName"
							placeholder="Update Release" required="required"
							pattern="^[^ ].+[^ ]$"
							oninvalid="setCustomValidity('Please do not enter leading and/or trailing spaces')"
							oninput="setCustomValidity('')">
						<%-- 		       <jsp:useBean id="nowUpdate" class="java.util.Date" /> --%>
						<%-- 		       <input type="hidden" id="id-update-release-start-date-hidden" name='startDate' value='<fmt:formatDate value="${nowUpdate }" pattern="MM/dd/yyyy"/>'> --%>
						<!-- 		       <input type="hidden" id="id-update-release-end-date-hidden" name='endDate'> -->
						<input type="hidden" id="id-update-release-release-id-hidden"
							name='releaseId'>
						<!-- 		       <input type="hidden" id="id-update-release-status-hidden" name='releaseStatus' value="1"> -->
						<!-- 		       <input type="hidden" id="id-update-release-description-hidden" name='releaseDescription' value=" "> intentionally left blank. Dont remove it -->
						<input type="hidden" id="id-update-release-source-hidden"
							name='source' value="LeftNavMenuAutomation">
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary"
								data-dismiss="modal">Cancel</button>
							<!--         <input type="submit" class="btn btn-success" >Update Release<> -->
							<input type="submit" value="Update" class="btn btn-success">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="deleteReleaseModal" tabindex="-1"
		role="dialog" aria-labelledby="deleteReleaseModalLabel"
		aria-hidden="false" style="display: none;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="deleteReleaseModalLabel">Delete
						Release</h4>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="fasle">x</span>
					</button>
				</div>
				<div class="modal-body">
					<div id="id-release-delete-confirm-div" class="alert alert-danger"></div>
					<form action="deleterelease" method="post"
						id="id-release-delete-form">
						<input type="hidden" id="id-delete-release-release-id-hidden"
							name='releaseId'>
						<%--        <input type="hidden" id="id-delete-release-project-id-hidden" name='projectId' value="${UserCurrentProjectId }"> --%>
						<input type="hidden" id="id-delete-release-release-status-hidden"
							name='releaseStatus' value="2"> <input type="hidden"
							id="id-delete-release-release-source-hidden" name='source'
							value="LeftNavMenuAutomation">
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary"
								data-dismiss="modal">Cancel</button>
							<!--         <input type="submit" class="btn btn-success" >Add Release<> -->
							<input type="submit" value="Delete" class="btn btn-danger">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="addTestSetModal" tabindex="-1"
		role="dialog" aria-labelledby="addTestSetModalLabel"
		aria-hidden="false" style="display: none;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="addTestSetModalLabel">Add Test Set</h4>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="fasle">�</span>
					</button>
				</div>
				<div class="modal-body">
					<!-- insertbuild
	{"Form data":{"buildReleaseId":"28","buildName":"TestSet1","buildDescription":"+","files":"","buildExecutionType":"1","buildEnv":"1","buildStatus":"1"}} -->
					<!--{"Form data":{"buildReleaseId":"2","buildName":"TestSet11","buildDescription":"+","files":"","buildExecutionType":"2","buildEnv":"1","buildStatus":"1"}} -->
					<form action="insertbuild" method="post" id="id-test-set-add-form">
						<input type="text" id="id-add-test-set-name-text"
							class="form-control" name="buildName" placeholder="Add Test Set"
							required="required" pattern="^[^ ].+[^ ]$"
							oninvalid="setCustomValidity('Please do not enter leading and/or trailing spaces')"
							oninput="setCustomValidity('')"> <input type="hidden"
							id="id-add-test-set-release-id-hidden" name='buildReleaseId'>
						<input type="hidden" id="id-add-test-set-description-hidden"
							name='buildDescription'> <input type="hidden"
							id="id-add-test-set-status-hidden" name='buildStatus' value="1">
						<input type="hidden"
							id="id-add-test-set-status-hidden" name='buildEnv' value="1">
						<input type="hidden" id="id-add-test-set-source-hidden"
							name='source' value="LeftNavMenuAutomation">
							<div class="form-group d-flex mt-10">
						<label>Type:</label>
						<select id="execType" class="form-control" name="buildExecutionType">
							<option value="2">Select Type</option>
							<option value="4">API</option>
							<c:if test="${sessionScope.redwoodStatus==1}">
								<option value="5">BPFT</option>
							</c:if>
						</select> 
						</div>
<!-- 						<input type="hidden" id="TStype" value="2" name="testSetExecType"> -->
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary"
								data-dismiss="modal">Cancel</button>
							<!--         <input type="submit" class="btn btn-primary" >Add Release<> -->
							<input type="submit" value="Add" class="btn btn-success">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="updateTestSetModal" tabindex="-1"
		role="dialog" aria-labelledby="updateTestSetModalLabel"
		aria-hidden="false" style="display: none;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="updateTestSetModalLabel">Update
						Test Set</h4>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="fasle">�</span>
					</button>
				</div>
				<div class="modal-body">
					<!-- insertbuild
	{"Form data":{"buildReleaseId":"28","buildName":"TestSet1","buildDescription":"+","files":"","buildExecutionType":"1","buildEnv":"1","buildStatus":"1"}} -->
					<form action="updatebuildname" method="post"
						id="id-test-set-update-form">
						<input type="text" id="id-update-test-set-name-text"
							class="form-control" name="buildName"
							placeholder="Update Test Set" required="required"
							pattern="^[^ ].+[^ ]$"
							oninvalid="setCustomValidity('Please do not enter leading and/or trailing spaces')"
							oninput="setCustomValidity('')"> <input type="hidden"
							id="id-update-test-set-id-hidden" name='buildId'>
						<!-- 	       <input type="hidden" id="id-update-test-set-description-hidden" name='buildDescription'> -->
						<!-- 	       <input type="hidden" id="id-update-test-set-status-hidden" name='buildStatus' value="1"> -->
						<input type="hidden" id="id-update-test-set-source-hidden"
							name='source' value="LeftNavMenuAutomation">
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary"
								data-dismiss="modal">Cancel</button>
							<!--         <input type="submit" class="btn btn-primary" >Update Release<> -->
							<input type="submit" value="Update" class="btn btn-success">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="cloneTestSetModal" tabindex="-1"
		role="dialog" aria-labelledby="cloneTestSetModalLabel"
		aria-hidden="false" style="display: none;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="cloneTestSetModalLabel">Clone
						Test Set</h4>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="fasle">�</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="clonebuild1" method="post"
						id="id-test-set-clone-form">
						<div class="form-group d-flex mt-10">
							<label for="user-id">Releases:</label> <select
								class="form-control" id="selectRelease" name="selectedReleaseName" required>
								<option value="0">Select Release</option>
								<c:forEach var="release" items="${releases }">
									<option value="${release.release_name}">${release.release_name }</option>
								</c:forEach>
							</select>
						</div>	
						<label for="user-id">Enter new Test Set name:</label>					
						<input type="text" id="id-clone-test-set-name-text"
							class="form-control" name="buildName"
							placeholder="Add Test Set" required="required"
							pattern="^[^ ].+[^ ]$"
							oninvalid="setCustomValidity('Please do not enter leading and/or trailing spaces')"
							oninput="setCustomValidity('')"> 
							<input type="hidden" id="id-clone-test-set-release-id-hidden" name='buildReleaseId'>
							<input type="hidden" id="id-clone-test-set-id-hidden" name='buildId'>
						    <input type="hidden" id="id-clone-test-set-description-hidden" name='buildDescription'>
							<input type="hidden" id="id-clone-test-set-status-hidden" name='buildStatus' value="1">
						    <input type="hidden" id="id-clone-test-set-status-hidden" name='buildEnv' value="1">
							<input type="hidden" id="id-clone-test-set-source-hidden" name='source' value="LeftNavMenuAutomation">
							<input type="hidden" id="id-clone-test-set-execType-hidden" name='buildExecutionType' value="2">
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary"
								data-dismiss="modal">Cancel</button>
							<!--         <input type="submit" class="btn btn-primary" >Update Release<> -->
							<input type="submit" value="Clone" class="btn btn-success">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</div>
	
	<div class="modal fade" id="deleteTestSetModal" tabindex="-1"
		role="dialog" aria-labelledby="deleteTestSetModalLabel"
		aria-hidden="false" style="display: none;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="deleteTestSetModalLabel">Delete
						Test Set</h4>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="fasle">x</span>
					</button>
				</div>
				<div class="modal-body">
					<div id="id-test-set-delete-confirm-div" class="alert alert-danger"></div>
					<form action="deletetestset" method="post"
						id="id-test-set-delete-form">
						<input type="hidden" id="id-delete-test-set-id-hidden"
							name='buildId'>
						<%--        <input type="hidden" id="id-delete-test-set-project-id-hidden" name='projectId' value="${UserCurrentProjectId }"> --%>
						<!-- 	       <input type="hidden" id="id-delete-test-set-status-hidden" name='buildStatus' value="2"> -->
						<input type="hidden" id="id-delete-test-set-source-hidden"
							name='source' value="LeftNavMenuAutomation">
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary"
								data-dismiss="modal">Cancel</button>
							<!--         <input type="submit" class="btn btn-success" >Add Test Set<> -->
							<input type="submit" value="Delete" class="btn btn-danger">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.ui.position.js"></script>

<script type="text/javascript">
function gotoLinkInMenu(link)//link
{
	$('body').addClass('white-bg');
	$("#barInMenu").removeClass('hidden');
	$("#wrapper").addClass("hidden");
	window.location.href = link; 
}
	
$(function () {
	//$("#custLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	
	//tooltip
	$('[data-toggle="tooltip"]').tooltip(); 
	
	$('.class-release-li').click( function(){
		if( $(this).hasClass('active') ) {
			$('#id-add-test-set-btn').prop('disabled', false);
		}
		else {
			$('#id-add-test-set-btn').prop('disabled', true);
		}
	});
	$('#id-add-test-set-btn').click( function( e ){
		if( $(this).hasClass('disabled') )
			return false;
		var $selectedRelease = $('.class-release-li.active');
// 		$('#addTestSetModalLabel').html("Add Test Set Under " + $selectedRelease.data('release-name') );
		$('#id-add-test-set-release-id-hidden').val( $selectedRelease.data('release-id') );
		$('#id-add-test-set-release-name-hidden').val( $selectedRelease.data('release-name') );
	});
	$('.class-build-name-a').click( function( e ){
        $.ajax({
            type : "POST",
            url : "setSessionAttributes",
            data : {
            	automationExecReleaseId: $('.class-release-li.active').data('release-id'),
            	automationExecBuildId: $(this).data('build-id'),
            	selectedAutoReportBuildId: $(this).data('build-id'),
            	automationExecBuildName: $(this).data('build-name'),
            	automationExecBuildType: $(this).data('build-type')
            	//selectedExecuteBuildName: $(this).data('build-name')
            }
        }).done( function( response ){
            window.location.href = "autoexecute";
        }).fail( function( jqXHR, testStatus, errorThrown ) {
            return false;
        }); 
    });
	$('#id-release-id-li-'+'${sessionScope.automationExecReleaseId } > a').trigger('click');
	
	$.contextMenu({
        selector: '.class-release-li', 
        callback: function(key, options) {
            var m = "clicked: " + key;
//             window.console && console.log(m) || alert(m); 
            switch(key) {
		        // A case for each action. Your actions here
		        case "edit": 
		    	    	$('#id-update-release-name-text').val( this.data('release-name') );
		    	    	$('#id-update-release-release-id-hidden').val( this.data('release-id') );
// 		    	    	$('#id-update-release-description-hidden').val( this.data('release-description') );
// 		    	    	$('#id-update-release-end-date-hidden').val( this.data('release-end-date') );
	        			$('#updateReleaseModal').modal('show');
		        	break;
		        case "delete": 
			        	$('#id-delete-release-release-id-hidden').val( this.data('release-id') );
			        	$('#id-release-delete-confirm-div').html("Are you sure you want to delete <b>"+ this.data('release-name')+"</b> ?" );
			        	$('#deleteReleaseModal').modal('show');
		        	break;
		    }
        },
        items: {
            "edit": {name: "Edit Release", icon: "edit"},
            /* "cut": {name: "Cut", icon: "cut"},
           copy: {name: "Copy", icon: "copy"},
            "paste": {name: "Paste", icon: "paste"}, */
            "delete": {name: "Delete Release", icon: "delete"},
            "sep1": "---------",
           /*  "quit": {name: "Quit", icon: function(){
                return 'context-menu-icon context-menu-icon-quit';
            }} */
        }
    });
	$.contextMenu({
        selector: '.class-test-set-li', 
        callback: function(key, options) {
            var m = "clicked: " + key;
//             window.console && console.log(m) || alert(m); 
            switch(key) {
		        // A case for each action. Your actions here
		        case "edit": 
		    	    	$('#id-update-test-set-name-text').val( this.find('a.class-build-name-a').data('build-name') );
		    	    	$('#id-update-test-set-id-hidden').val( this.find('a.class-build-name-a').data('build-id') );
// 		    	    	$('#id-update-scenario-scenario-release-name-hidden').val( this.parent('.class-releases-li').data('release-name') );
	        			$('#updateTestSetModal').modal('show');
		        	break;
		        case "clone":
		        		$('#id-clone-test-set-name-text').val(this.find('a.class-build-name-a').data('build-name'));
		        		$('#id-clone-test-set-id-hidden').val(this.find('a.class-build-name-a').data('build-id'));
		        		$('#id-clone-test-set-release-id-hidden').val(this.find('a.class-build-name-a').data('release-id'));
		        		$('#cloneTestSetModal').modal('show');
		        	break;	
		        case "delete":
		        	$('#id-delete-test-set-id-hidden').val( this.find('a.class-build-name-a').data('build-id') );
		        	$('#id-test-set-delete-confirm-div').html("Are you sure you want to delete <b>"+ this.find('a.class-build-name-a').data('build-name')+"</b> ?" );
		        	$('#deleteTestSetModal').modal('show');
		        	break;
		    }
        },
        items: {
            "edit": {name: "Edit Test Set", icon: "edit"},
            "clone": {name: "Clone Test Set", icon: "copy"}, //changed from clone to copy based on cdnjs library
            /* "cut": {name: "Cut", icon: "cut"},
           copy: {name: "Copy", icon: "copy"},
            "paste": {name: "Paste", icon: "paste"}, */
            "delete": {name: "Delete Test Set", icon: "delete"},
            "sep1": "---------",
            /* "quit": {name: "Quit", icon: function(){
                return 'context-menu-icon context-menu-icon-quit';
            }} */
        }
    });
	$(".md-skin").removeClass("hidden");
	$("#redwood").addClass("hidden");
	$("#red").click(function(){
		$(".md-skin").addClass("hidden");
		$(".page-wrapper").attr('id', 'Redwood_hide');
		$("#Redwood_hide").show();
		$("#redwood").removeClass("hidden");
	//	$(".navbar-minimalize").click();//trigger('click');
		var userDetails=$("#userName").text();
		var username=userDetails.split(" ");
		var fisrtname=username[0].toLowerCase();
		//console.log("username"+fisrtname); 
		//console.log(CryptoJS.AES.encrypt(username,"/"));
	      var role='${sessionScope.roleId}';
	      var email='${sessionScope.userEmailId}';
	      var redwoodUrl='${Model.redwood_url}';
	    if(role=='2'){
		window.open(redwoodUrl+"/login.html?username=admin&password=admin", "redowoodWindow");
	    }else{
		window.open(redwoodUrl+"/login.html?username="+email+"&password=Welcome@123", "redowoodWindow");
		}
		//alert("hi");
		var name= $('#textfield-1009-inputEl').val();
// 		console.log("name :"+name);
		document.addEventListener('contextmenu', event => event.preventDefault());
		$('iframe[name="redowoodWindow"]').contents().find('#button-1012-btnInnerEl').click(function(){
			alert("saacs");
		});
	});
	
	
});
</script>
<script>

function crossDomainCall(url,data,fnSuccess,fnError){
    $.ajax({
        type:'POST',
        url:url,
        contentType:"application/json",
        async:true,
        dataType:'jsonp',
        crossDomain:true,
        data:data,
        success:fnSuccess,
        error: fnError
    });
}
</script>
<script>
function selectExecType(){
	
	$("#TStype").val($("#execType option:selected").val());
}

</script>
<style>
.class-crud-btns-div {
	text-align: center;
	justify-content: center;
	display: flex;
}

.class-crud-btns-div ul {
	display: flex;
	list-style-type: none;
	padding-left: 0;
}

.class-crud-btns-div li {
	background: #1c84c6;
	/*     padding:8px 12px; */
	margin: 15px 5px;
	/*     border-radius:50%; */
}

.class-crud-btns-div li.disabled {
	background: #ddd;
}

.class-crud-btns-div li a {
	color: #fff;
}

@media screen and (max-width:768px) {
	.class-crud-btns-div {
		display: block;
	}
}

.mini-navbar .class-crud-btns-div {
	display: block;
}

.mini-navbar .class-crud-btns-div li {
	width: 38px;
	/*     margin:15px; */
}

.class-crud-btns-div button i {
	display: none;
}

.mini-navbar .class-crud-btns-div button i {
	display: block;
}

.mini-navbar .class-crud-btns-div button span {
	display: none;
}

.tooltip.bottom .tooltip-inner {
	background-color: #1c84c6;
}

.tooltip.bottom .tooltip-arrow {
	border-bottom-color: #1c84c6;
}
.d-flex{
display:flex;
}
.d-flex label{
width:100px
}
</style>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Builds</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li class="active"><strong>View Build</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>Builds</h5>

				</div>
				<div class="ibox-content">
					<div class="full-height-scroll">
						<div class="table-responsive" style="overflow-x: visible;">
							<table class="table table-striped table-hover" id="allBuildTable">
								<thead>
									<tr>
										<th class="hidden">Build Id</th>
										<th>Build Name</th>
										<th>Build Create Date</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="data" items="${allBuildDetails}">
										<tr data-toggle="tab" href="#${data.build_id}"
											style="cursor: pointer">
											<td class="hidden">${data.build_id}</td>
											<td>${data.build_name}</td>
											<td>${data.build_createdate}</td>
											<td><button class="btn btn-warning btn-sm"
													onclick="viewDetails(${data.build_id})">Step wise
													details</button></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-content">
					<div class="tab-content" id="extraBuildDetails">
						<c:forEach var="data" items="${allBuildDetails}">
							<div id="${data.build_id}" class="tab-pane">
							
							
							<div class="row m-b-lg">
									<div class="col-lg-4 text-center">
										 
										<div class="m-b-sm">
											<img alt="image" class="img-circle"
												src="<%=request.getContextPath()%>/resources/img/photo.png"
												style="width: 62px">
										</div>
									</div>
									<div class="col-lg-8">
										<h2 style="vertical-align: middle;text-align:center;margin-top: 20px;">${data.creator}</h2>
									</div>
								</div>
								
								<div class="">
									<div class="full-height-scroll">
										<strong>Build Information</strong>
										<ul class="list-group clear-list">
											<li class="list-group-item fist-item"><span
												class="pull-right"> ${data.build_name} </span>Build Name</li>
											<li class="list-group-item"><span class="pull-right">
													${data.build_createdate} </span>Build Create Date</li>
										</ul>

									</div>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});
  $(function () {
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	});  
</script>
<script>
$(function() {
	$('#allBuildTable').DataTable({
		"paging" : true,
		"lengthChange" : true,
		"searching" : true,
		"ordering" : false,
		"info" : true,
		"autoWidth" : true
	});
	$('#extraBuildDetails > :first-child').addClass('active');
	$('.statusLabels').each(function() {
		var status = $.trim($(this).text());
		if (status.toLowerCase() == 'active') {
			$(this).addClass('label-success');
		} else {
			$(this).addClass('label-danger');
		}
	});
	$('.1').each(function(){
		$(this).addClass('label-success');
	});
	$('.2').each(function(){
		$(this).addClass('label-info');
	});
	$('.3').each(function(){
		$(this).addClass('label-warning');
	});
	$('.4').each(function(){
		$(this).addClass('label-danger');
	});
	$('.5').each(function(){
		$(this).addClass('label-success');
	});
	
});

function viewDetails(id){
	var posting = $.post('setAutomationBuildId',{
		buildId : id
	});
	posting.done(function(data){
		window.location.href = "stepwisedetails";
	});
}


</script>
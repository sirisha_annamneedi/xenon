<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
<div class="wrapper wrapper-content">
	<c:if test="${Model.filterText=='ActRelease'}">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Test Plan Manager : Active Releases</h5>
					</div>
					<div class="ibox-content">
						<div class="table-responsive">
							<table
								class="table table-striped table-bordered table-hover dataTables-example"
								id="openBugList" name="Active Releases">
								<thead>
									<tr>
										<th class="hidden">Release Id</th>
										<th>Name</th>
										<th>Status</th>
										<th class="hidden">Description</th>
										<th>Start Date</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="data" items="${Model.activeRelease}">
										<tr class="gradeX">
											<td class="hidden">${data.release_id}</td>
											<td class="xlTextAlignment" data-original-title="${data.release_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.release_name}</td>

											<td><c:if test="${data.release_active == 1}">
													<span class="label label-primary">Active</span>
												</c:if> <c:if test="${data.release_active == 2}">
													<span class="label label-danger">Inactive</span>
												</c:if></td>
											<td class="hidden">${data.release_description}</td>
											<td class="textAlignment" data-original-title="${data.release_start_date}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.release_start_date}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

					</div>
				</div>


			</div>
		</div>
	</c:if>

	<c:if test="${Model.filterText=='ActBuild'}">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Test Plan Manager : Active Builds</h5>
					</div>
					<div class="ibox-content">
						<div class="table-responsive">
							<table
								class="table table-striped table-bordered table-hover dataTables-example"
								id="openBugList" name="Active Builds">
								<thead>
									<tr>
										<th class="hidden">Build Id</th>
										<th>Name</th>
										<th>Status</th>
										<th class="hidden">Description</th>
										<th>Created Date</th>
										<th>Author</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="data" items="${Model.activeBuild}">
										<tr class="gradeX">
											<td class="hidden">${data.build_id}</td>
											<td class="xlTextAlignment" data-original-title="${data.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_name}</td>
											<td><c:if test="${data.build_status == 1}">
													<span class="label label-primary">Active</span>
												</c:if> <c:if test="${data.build_status == 2}">
													<span class="label label-danger">Inactive</span>
												</c:if></td>
											<td class="hidden">${data.build_desc}</td>
											<td class="textAlignment" data-original-title="${data.build_createdate}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_createdate}</td>
											<td class="textAlignment" data-original-title="${data.creator}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.creator}</td>
										</tr>
									</c:forEach>
								</tbody>

							</table>
						</div>

					</div>
				</div>


			</div>
		</div>
	</c:if>


	<c:set var = "string1" value = '${Model.filterText}'/>
    <c:set var = "string2" value = "${fn:substring(string1, 0, 8)}" />
	<c:set var = "releaseId" value ="${fn:substring(string1, 8, 9)}" />
	
	
	<c:if test="${string2=='TestCase'}">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Test Plan Manager : Test Cases</h5>
					</div>
					<div class="ibox-content">
						<div class="table-responsive">
							<table
								class="table table-striped table-bordered table-hover dataTables-example"
								id="openBugList" name="Test cases">
								<thead>
									<tr>
										<th class="hidden">TestCase Id</th>
										<th class="hidden">Id</th>
										<th>Id</th>
										<th>Build Name</th>
										<th>Name</th>
										<th class="hidden">Summary</th>
										<th class="hidden">Precondition</th>
										<th>Created Date</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="data" items="${Model.testCases}">
									<%-- <c:if test="${releseId== 0}"> --%>
										<tr class="gradeX" style="cursor: pointer"
											onclick="location.href='testcasebyprefix?tcPrefix=${data.test_prefix}'">
											<td class="hidden">${data.testcase_id}</td>
											<td class="hidden">${data.testcase_id}</td>
											<td>${data.test_prefix}</td>
										
											<td class="xlTextAlignment" data-original-title="${data.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_name}</td>
											<td class="xlTextAlignment" data-original-title="${data.testcase_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.testcase_name}</td>
											<td class="hidden">${data.summary}</td>
											<td class="hidden">${data.precondition}</td>
											<td class="textAlignment" data-original-title="${data.created_date}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.created_date}</td>
										</tr>
										<%-- </c:if> --%>
										<c:forEach var="data1" items="${Model.BRid}">
											<c:if test="${releaseId == data1.release_id}">
												<c:if test="${data.build_id == data1.build_id}">
													<tr class="gradeX" style="cursor: pointer"
														onclick="location.href='testcasebyprefix?tcPrefix=${data.test_prefix}'">
														<td class="hidden">${data.testcase_id}</td>
														<td class="hidden">${data.testcase_id}</td>
														<td>${data.test_prefix}</td>
														<td class="xlTextAlignment" data-original-title="${data.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_name}</td>
														<td class="xlTextAlignment" data-original-title="${data.testcase_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.testcase_name}</td>
														<td class="hidden">${data.summary}</td>
														<td class="hidden">${data.precondition}</td>
														<td class="textAlignment" data-original-title="${data.created_date}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.created_date}</td>
													</tr>	
												</c:if>
											</c:if>	
										</c:forEach>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:if>


	<c:if test="${Model.filterText=='ActBug'}">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Test Plan Manager : Active Bugs</h5>
					</div>

					<div class="ibox-content">

						<div class="table-responsive">
							<table
								class="table table-striped table-bordered table-hover dataTables-example"
								id="openBugList" name="Active Bugs">
								<thead>
									<tr>
										<th class="hidden">Bug ID</th>
										<th>Id</th>
										<th style="width: 150px">Title</th>
										<th class="hidden">Description</th>
										<th>Created Date</th>
										<th>Created By</th>
										<th>Build Name</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="data" items="${Model.activeBug}">
										<%-- <tr class="gradeX" style="cursor: pointer" onclick="location.href='bugsummarybyprefix?btPrefix=${data.bug_prefix}'"> --%>
										<tr class="gradeX">
											<td class="hidden">${data.bug_id}</td>
											<td>${data.bug_prefix}</td>
											<td class="lgTextAlignment" data-original-title="${data.bug_title}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.bug_title}</td>
											<td class="hidden">${data.bug_desc}</td>
											<td class="textAlignment" data-original-title="${data.create_date}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.create_date}</td>
											<td class="textAlignment" data-original-title="${data.creator}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.creator}</td>
											<td class="textAlignment" data-original-title="${data.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_name}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</c:if>
</div>
</div>
<script type="text/javascript">
	$(window).load(function() {
		 $('body').removeClass("white-bg");
		 $("#barInMenu").addClass("hidden");
		 $("#wrapper").removeClass("hidden");
	});

	$(document).ready(function() {
		setSpanColor();
		$('#viewBugTable_paginate').on('click', function() {
			setSpanColor();
		});
		$('#viewBugTable_length select').on('change', function() {
			setSpanColor();
		});
		$('#viewBugTable_filter input').on('keypress', function() {
			setSpanColor();
		});
		$('#viewBugTable thead tr').on('click', function() {
			setSpanColor();
		});

	});
	var setSpanColor = function() {
		$('.td_bugStatus').each(function() {
			var stat = $(this).text();
			if (stat == 'New') {
				$(this).css("background-color", "${Model.New}");
			} else if (stat == 'Assigned') {
				$(this).css("background-color", "${Model.Assigned}");
			} else if (stat == 'Dev in Progress') {
				$(this).css("background-color", "${Model.Fixed}");
			} else if (stat == 'Ready for QA') {
				$(this).css("background-color", "${Model.Verified}");
			} else if (stat == 'Closed') {
				$(this).css("background-color", "${Model.Closed}");
			} else if (stat == 'Reject') {
				$(this).css("background-color", "${Model.Reject}");
			}
		});
	}
</script>

<script>
	$(document).ready(
			function() {
				var templateName = $(".dataTables-example").attr("name");
				 /* $('.dataTables-example').DataTable({
						"paging" : false,
						"lengthChange" : false,
						"searching" : false,
						"ordering" : false,
						"info" : true,
						"autoWidth" : true
					}); */
				 $('.dataTables-example').DataTable(
						{
							dom : '<"html5buttons"B>lTfgitp',
							buttons : [
									{
										extend : 'csv',
										title : templateName,
										exportOptions : {
											columns : ':visible'
										}
									},
									{
										extend : 'excel',
										title : templateName,
										exportOptions : {
											columns : ':visible'
										}
									},
									{
										extend : 'pdf',
										title : templateName,
										exportOptions : {
											columns : ':visible'
										}
									},

									{
										extend : 'print',
										exportOptions : {
											columns : ':visible'
										},
										customize : function(win) {
											$(win.document.body).addClass(
													'white-bg');
											$(win.document.body).css(
													'font-size', '10px');

											$(win.document.body).find('table')
													.addClass('compact').css(
															'font-size',
															'inherit');
										}
									} ],
							"aaSorting" : [ [ 0, "desc" ] ]

						}); 

			});
</script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="md-skin">
	<c:set var="extSys" value='${Model.extSysName}' />
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<c:if test="${extSys == 'QTP'}">
				<h2>QTP Report Analysis</h2>
			</c:if>
			<c:if test="${extSys == 'Selenium'}">
				<h2>Selenium Report Analysis</h2>
			</c:if>
			<ol class="breadcrumb">
				<li><a href="bmdashboard">Dashboard</a></li>
				<c:if test="${extSys == 'QTP'}">
					<li><a href="extSysReportsQTP">QTP Releases</a></li>
					<li><a href="extSysReportsBuilds">QTP Builds</a></li>
					<li><a href="extSysReportsTC">QTP Test Cases</a></li>
				</c:if>
				<c:if test="${extSys == 'Selenium'}">
					<li><a href="extSysReportsSelenium">Selenium Releases</a></li>
					<li><a href="extSysReportsBuilds">Selenium Builds</a></li>
					<li><a href="extSysReportsTC">Selenium Test Cases</a></li>
				</c:if>
				<li class="active"><strong>Test Case Steps</strong></li>
			</ol>
		</div>
	</div>
	<c:set var="filterText" value='${Model.filterText}' />
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<div class="ibox-title">
						<c:if test="${extSys == 'QTP'}">
							<h5>Test Plan Manager : QTP Test Steps</h5>
						</c:if>
						<c:if test="${extSysName == 'Selenium'}">
							<h5>Test Plan Manager : Selenium Test Steps</h5>
						</c:if>
					</div>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table
							class="table table-striped table-bordered table-hover dataTables-example"
							id="table" name="External System Test Steps">
							<thead>
								<tr>
									<th class="hidden">Test Step ID</th>
									<th>External System Test Step Number</th>
									<th class="hidden">External System Test Step ID</th>
									<th class="hidden">Test Case ID</th>
									<th>Test Step Name</th>
									<th>Test Step Description</th>
									<th>Test Step Status</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="testSteps" items="${Model.ExtSysTeststeps}">
									<c:if test="${testSteps.test_case_id == filterText}">
										<tr class="gradeX" style="cursor: pointer">
											<td class="hidden">${testSteps.test_step_id}</td>
											<td class="xlTextAlignment"
												data-original-title="${testSteps.qtp_test_step_number}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${testSteps.qtp_test_step_number}</td>
											<td class="hidden">${testSteps.qtp_test_case_id}</td>
											<td class="hidden">${testSteps.test_case_id}</td>
											<td class="xlTextAlignment"
												data-original-title="${testSteps.step_title}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${testSteps.step_title}</td>
											<td class="xlTextAlignment"
												data-original-title="${testSteps.step_description}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${testSteps.step_description}</td>
											<td class="xlTextAlignment"
												data-original-title="${testSteps.description}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${testSteps.description}</td>
										</tr>
									</c:if>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var dt;
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");

		$(".chosen-select").chosen().trigger('chosen:updated');
	});

	$(function() {
		//to apply choosen
		$(".chosen-select").chosen();
		//collpsing filter Ibox
		//$("#filterIbox").addClass('collapsed');
		dt = $('#table').DataTable(
				{
					dom : '<"html5buttons"B>lTfgitp',
					buttons : [
							{
								extend : 'csv',
								title : 'Test Case Analysis'
							},
							{
								extend : 'excel',
								title : 'Test Case Analysis'
							},
							{
								extend : 'pdf',
								title : 'Test Case Analysis'
							},

							{
								extend : 'print',
								customize : function(win) {
									$(win.document.body).addClass('white-bg');
									$(win.document.body).css('font-size',
											'10px');

									$(win.document.body).find('table')
											.addClass('compact').css(
													'font-size', 'inherit');
								}
							} ],
					"aaSorting" : [ [ 0, "desc" ] ]

				});
	});
</script>
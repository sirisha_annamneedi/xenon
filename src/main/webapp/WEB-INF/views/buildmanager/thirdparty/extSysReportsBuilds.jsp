<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="md-skin">
	<c:set var="extSys" value='${Model.extSysName}' />
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<c:if test="${extSys == 'QTP'}">
				<h2>QTP Report Analysis</h2>
			</c:if>
			<c:if test="${extSys == 'Selenium'}">
				<h2>Selenium Report Analysis</h2>
			</c:if>
			<ol class="breadcrumb">
				<li><a href="bmdashboard">Dashboard</a></li>
				<c:if test="${extSys == 'QTP'}">
					<li><a href="extSysReportsQTP">QTP Releases</a></li>
				</c:if>
				<c:if test="${extSys == 'Selenium'}">
					<li><a href="extSysReportsSelenium">Selenium Releases</a></li>
				</c:if>
				<li class="active"><strong>Build Details</strong></li>
			</ol>
		</div>
	</div>
	<script>
		var TestCount = [];
	</script>
	<c:set var="legend" value="Pass,Fail,Not run" />
	<c:forEach var="leg" items="${legend.split(',')}">
		<script>
			var current = '${leg}';
			var temp = 0;
		</script>
		<c:forEach var="TCcount" items="${Model.ExtSysReleaseTCCount}">
			<script>
			var status = '${TCcount.status}';
			var count = '${TCcount.tcCount}';
			if (current == status){
				temp = count;
			}
			</script>
		</c:forEach>
		<script>
			TestCount.push(temp);
		</script>
	</c:forEach>
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="ibox-title">
								<h5>Test Case Status</h5>
								<div class="pull-right"></div>
							</div>
							<canvas id="TestCaseChart"></canvas>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:set var="text" value='${Model.uniqueId}' />	
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<c:if test="${extSys == 'QTP'}">
						<h5>Test Plan Manager : QTP Builds</h5>
					</c:if>
					<c:if test="${extSys == 'Selenium'}">
						<h5>Test Plan Manager : Selenium Builds</h5>
					</c:if>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table
							class="table table-striped table-bordered table-hover dataTables-example"
							id="table" name="External System Builds">
							<thead>
								<tr>
									<th class="hidden">Build ID</th>
									<th>Build Name</th>
									<th class="hidden">Release ID</th>
									<th>Build Start Date Time</th>
									<th>Build End Date Time</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="builds" items="${Model.ExtSysBuilds}">
									<c:if test="${builds.release_id == text}">
										<tr class="gradeX" style="cursor: pointer"
											onclick="dashLink(${builds.build_id},'${extSys}')">
											<td class="hidden">${builds.build_id}</td>
											<td class="xlTextAlignment"
												data-original-title="${builds.build_name}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${builds.build_name}</td>
											<td class="hidden">${builds.release_id}</td>
											<td class="textAlignment"
												data-original-title="${builds.build_startdatetime}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${builds.build_startdatetime}</td>
											<td class="textAlignment"
												data-original-title="${builds.build_enddatetime}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${builds.build_enddatetime}</td>
										</tr>
									</c:if>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var dt;
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");

		$(".chosen-select").chosen().trigger('chosen:updated');
	});

	$(function() {
		//to apply choosen
		$(".chosen-select").chosen();
		//collpsing filter Ibox
		//$("#filterIbox").addClass('collapsed');
		dt = $('#table').DataTable(
				{
					dom : '<"html5buttons"B>lTfgitp',
					buttons : [
							{
								extend : 'csv',
								title : 'Test Case Analysis'
							},
							{
								extend : 'excel',
								title : 'Test Case Analysis'
							},
							{
								extend : 'pdf',
								title : 'Test Case Analysis'
							},

							{
								extend : 'print',
								customize : function(win) {
									$(win.document.body).addClass('white-bg');
									$(win.document.body).css('font-size',
											'10px');

									$(win.document.body).find('table')
											.addClass('compact').css(
													'font-size', 'inherit');
								}
							} ],
					"aaSorting" : [ [ 0, "desc" ] ]

				});
	});

	function dashLink(link, extSys) {

		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");

		var posting = $.post('setExtSysSession', {
			filterText : link,
			extSysName : extSys
		});

		posting.done(function(builds) {
			window.location.href = "extSysReportsTC";
		});
	}
	
	var ChartColor = ["#2ECC71","#F1948A","#1EC0FB", "#85C1E9","#C39BD3","#AA85E9", "#2EC7CC","#9C2ECC"];

	$(document).ready(
	  function() {
		  
		var data = {
			datasets: [{
				data: TestCount ,
				backgroundColor: ChartColor,
			}],
			labels: [
					"Pass",
					"Fail",
					"Not Run"
				]
		};  
		  
	    var canvas = document.getElementById("TestCaseChart");
	    var ctx = canvas.getContext("2d");
	    var myNewChart = new Chart(ctx, {
	      type: 'doughnut',
	      data: data,
	      options: {
		      pieceLabel: {
		         render: 'value' //show values
		      },
		      legend: {
		            display: true,
		            position:'right',
		   }
	      }
	      
	    });
	  }
	);
</script>
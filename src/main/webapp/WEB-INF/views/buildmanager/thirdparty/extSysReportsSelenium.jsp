<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Selenium Report Analysis</h2>
			<ol class="breadcrumb">
				<li><a href="bmdashboard">Dashboard</a></li>
				<li class="active"><strong>Selenium Releases</strong></li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Test Plan Manager : Selenium Releases</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table
							class="table table-striped table-bordered table-hover dataTables-example"
							id="table" name="External System Reports">
							<thead>
								<tr>
									<th class="hidden">Release ID</th>
									<th>Release Name</th>
									<th>Release Description</th>
									<!-- 					<th>Build Name</th> -->
									<!-- 					<th>Application Name</th> -->
									<th>Execution Start Date Time</th>
									<th>Execution End Date Time</th>
									<th>Total Test Cases</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="data" items="${Model.ExtSysReleases}">
									<c:if test="${data.extsys_name == 'Selenium'}">
										<tr class="gradeX" style="cursor: pointer"
											onclick="dashLink(${data.release_id})">
											<td class="hidden">${data.release_id}</td>
											<td class="xlTextAlignment"
												data-original-title="${data.release_title}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${data.release_title}</td>
											<td>${data.release_description}</td>
											<%-- 				<td class="hidden">${data.summary}</td> --%>
											<%-- 				<td class="hidden">${data.precondition}</td> --%>
											<td class="textAlignment"
												data-original-title="${data.release_startdatetime}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${data.release_startdatetime}</td>
											<td class="textAlignment"
												data-original-title="${data.release_enddatetime}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${data.release_enddatetime}</td>
											<td><c:forEach var="TCCount" items="${Model.ExtSysTCCount}">
												<c:if test="${data.release_id == TCCount.release_id}">
													${TCCount.tcCount}
												</c:if>
											</c:forEach></td>
										</tr>
									</c:if>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var dt;
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");

		$(".chosen-select").chosen().trigger('chosen:updated');
	});

	$(function() {
		//to apply choosen
		$(".chosen-select").chosen();
		//collpsing filter Ibox
		//$("#filterIbox").addClass('collapsed');
		dt = $('#table').DataTable(
				{
					dom : '<"html5buttons"B>lTfgitp',
					buttons : [
							{
								extend : 'csv',
								title : 'Test Case Analysis'
							},
							{
								extend : 'excel',
								title : 'Test Case Analysis'
							},
							{
								extend : 'pdf',
								title : 'Test Case Analysis'
							},

							{
								extend : 'print',
								customize : function(win) {
									$(win.document.body).addClass('white-bg');
									$(win.document.body).css('font-size',
											'10px');

									$(win.document.body).find('table')
											.addClass('compact').css(
													'font-size', 'inherit');
								}
							} ],
					"aaSorting" : [ [ 0, "desc" ] ]

				});
	});

	function dashLink(link) {
		
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		var posting = $.post('setExtSysSession', {
			filterText : link,
			extSysName : "Selenium"
		});

		posting.done(function(data) {
			window.location.href = "extSysReportsBuilds";
		});
	}
</script>
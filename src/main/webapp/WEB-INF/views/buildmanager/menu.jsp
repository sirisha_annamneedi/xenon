<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="bar" id="barInMenu" class="">
	<p>loading</p>
</div>
<div id="wrapper" class="hidden">
	<nav class="navbar-default navbar-static-side " role="navigation">
		<!-- style="min-height: 100%; background-color: transparent;" -->
		<div class="sidebar-collapse">
			<ul class="nav metismenu" id="side-menu">
				<%-- <li class="nav-header">
					<div class="dropdown profile-element">
						<span> <img alt="image" class="img-circle"
							style="width: 48px; height: 48px"
							src="data:image/jpg;base64,${model.UserProfilePhoto }" />
						</span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
							class="clear"> <span class="block m-t-xs"> <strong
									class="font-bold">${model.userName }</strong>
							</span>
						</span>
						</a> <span class="text-muted text-xs block">${model.role}<b
							class=""></b></span>
					</div>
					<div class="logo-element" style="padding: 14px !important">
						<a href="#" onclick="gotoLinkInMenu('xedashboard')"> <img
							alt="image" class=""
							src="<%=request.getContextPath()%>/resources/img/xenon.png" />
						</a>
					</div>
				</li> --%>
				<li class="nav-header">
                    <div class="dropdown profile-element text-center">
                        <a href="#" onclick="gotoLink('xedashboard')" >
                        <img src="<%=request.getContextPath()%>/resources/img/Jade-Logo.png" class="xenon-logo">
                        </a>
                    <!--    <img class="img-circle" style="width:48px; height:48px" src="resources/img/user.jpg">
                         <img alt="image" class="img-circle"
                            style="width: 48px; height: 48px"
                            src="data:image/jpg;base64,${Model.UserProfilePhoto }" />
                        </span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
                            class="clear"> <span class="block m-t-xs"> <strong
                                    class="font-bold">${Model.userName }</strong>
                            </span>
                        </span>
                        </a> <span class="text-muted text-xs block">${Model.role}<b
                            class=""></b></span>  -->
                    </div>
                    <div class="logo-element" style="padding: 14px !important">
                        <a href="#" onclick="gotoLinkInMenu('xedashboard')">
                        <img alt="image" class=""
                            src="<%=request.getContextPath()%>/resources/img/xenon.png" />
                        </a>
                    </div>
                </li>
				<!-- <li class="mainLi"><a href="#"
					onclick="gotoLinkInMenu('bmdashboard')"><i
						class="fa fa-diamond"></i> <span class="nav-label">DashBoard</span></a></li> -->
				<%-- <c:if test="${model.moduleAccess == 1}">
					<li class="mainLi"><a href="#"><i class="fa fa-edit"></i>
							<span class="nav-label">Modules</span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<c:if test="${model.createModule == 1}">
								<li><a href="#" onclick="gotoLinkInMenu('createmodule')">New
										Module</a></li>
							</c:if>
							<c:if test="${model.viewModules == 1}">
								<li><a href="#" onclick="gotoLinkInMenu('viewmodules')">Modules</a></li>
							</c:if>
						</ul></li>
				</c:if> --%>

				<%-- <c:if test="${model.releaseAccess == 1}">
					<li class="mainLi"><a href="#"><i class="fa fa-pie-chart"></i>
							<span class="nav-label"> Release </span><span class="fa arrow"></span>
					</a>
						<ul class="nav nav-second-level">
							<c:if test="${model.createRelease == 1}">
								<li><a href="#" onclick="gotoLinkInMenu('createrelease')">Add
										Release</a></li>
							</c:if>
							<c:if test="${model.viewRelease == 1}">
								<li><a href="#" onclick="gotoLinkInMenu('viewrelease')">Releases</a></li>
							</c:if>
						</ul></li>
				</c:if> --%> 


				<!--<c:if test="${model.buildAccess == 1}">
					<li class="mainLi"><a href="#"><i class="fa fa-desktop"></i>
							<span class="nav-label">Test Set</span> <span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">-->
						<!-- Commenting New Build -->
						<!-- 
							<c:if test="${model.createbuild == 1}">
								<li><a href="#" onclick="gotoLinkInMenu('createbuild')">New
										Build</a></li>
							</c:if>
						-->
							<!--<c:if test="${model.autoexe_build == 1}">
								<li><a href="#" onclick="gotoLinkInMenu('autoexe')">Automation
										Test Set</a></li>
							</c:if>-->
							<%-- <c:if test="${model.addRemoveStatus == 1}">
								<li><a href="#"
									onclick="gotoLinkInMenu('addremovetestcase')">Amend Build</a></li>
							</c:if> --%>
							<!--<c:if test="${model.createbuild == 1}">
								<li><a href="#" onclick="gotoLinkInMenu('createbuild')">New Test Set
										 </a></li>
							</c:if>
							<c:if test="${model.createbuild == 1}">
								<li><a href="#" onclick="gotoLinkInMenu('clonebuild')">Clone
										Test Set</a></li>
							</c:if>-->
							
							<!-- 
							<c:if test="${model.viewbuild == 1}">
								<li><a href="#" onclick="gotoLinkInMenu('viewbuild')">View
										Builds</a></li>
							</c:if>
							<c:if test="${model.viewbuild == 1}">
								<li><a href="#"
									onclick="gotoLinkInMenu('executebuildlist')">Build
										execution</a></li>
							</c:if>
							 -->
							 
							 <!--<c:if test="${model.viewbuild == 1}">
								<li><a href="#" onclick="gotoLinkInMenu('viewbuild')">Execution</a></li>
							</c:if>
						</ul></li>
				</c:if>-->
				<%-- <li class="mainLi"><a href="#"><i class="fa fa-bar-chart-o"></i>
						<span class="nav-label">Live Reports</span><span
						class="fa arrow"></span></a>
					<ul class="nav nav-second-level collapse">
						<li><a href="#" onclick="gotoLinkInMenu('builddetails')">Build
								Report</a></li>
						<c:if test="${model.autoexe_build == 1}">
							<li><a href="#"
								onclick="gotoLinkInMenu('viewautomationbuild')">Automation
									Build Report</a></li>
						</c:if>
						<!-- Commenting Test case reports and Bug reports -->
						<!-- 
						<li><a href="#" onclick="gotoLinkInMenu('tcbuganalysis')">Test
								Case Report</a></li>
						<li><a href="#" onclick="gotoLinkInMenu('buganalysis')">Bug
								Report</a></li>
						-->
					</ul></li> --%>
				<!-- <li class="mainLi"><a href="#"><i class="fa fa-bar-chart-o"></i>
						<span class="nav-label">Third Party Automation Reports</span><span
						class="fa arrow"></span></a>
					<ul class="nav nav-second-level collapse">
						<li><a href="#" onclick="gotoLinkInMenu('extSysReportsQTP')">QTP
								Reports</a></li>
						<li><a href="#" onclick="gotoLinkInMenu('extSysReportsSelenium')">Selenium
								Reports</a></li>
					</ul></li> -->	
				<%-- <c:if test="${model.viewvm == 1 &&  model.automationStatus == 1 &&  model.customerOnPremiseStatus == 1}">
					<li class="mainLi"><a href="#" onclick ="gotoLinkInMenu('vmdetails')"><i class="fa fa-desktop"></i>
						<span class="nav-label">Virtual Machine</span><span
						class="fa arrow"></span></a>
						<ul id ="vmList" class="nav nav-second-level collapse">
						<c:forEach var="vmList" items="${Model.vmList}">
							<li><a href="#" onclick="gotoLinkInMenu('vmdetails')">${vmList.hostname}</a></li>
						</c:forEach>
						</ul>
					</li>
				</c:if> --%>
				
				<!-- <li class="mainLi"><a href="#"
					onclick="gotoLinkInMenu('documentlibrary')"><i
						class="fa fa-files-o"></i> <span class="nav-label">Document
							Library</span></a></li>

				<li class="mainLi"><a href="#"
					onclick="gotoLinkInMenu('jenkinservers')"><i
						class="fa fa-gears"></i> <span class="nav-label">
							Continuous Integration </span> </a></li> -->
				<!-- <li class="mainLi"><a href="#"><i class="fa fa-cog"></i> <span
						class="nav-label">Settings</span><span class="fa arrow"></span></a>
					<ul class="nav nav-second-level collapse">
						<li><a href="#" onclick="gotoLinkInMenu('viewenvironment')">Environments
						</a></li>
						<li><a href="#" onclick="gotoLinkInMenu('viewgitprojects')">Git</a></li>

					</ul></li>-->
			</ul>

		</div>
	</nav>

	<input type="hidden" value="<%=session.getAttribute("menuLiText")%>"
		id="hiddenLiText">

	<script type="text/javascript">
		$(function() {

			var liText = $('#hiddenLiText').val();
			$('.nav-label').each(function() {
				var label = $(this).text().trim();
				if (label == liText) {
					var parentLi = $(this).parents('.mainLi');
					parentLi.addClass('active');
					parentLi.find('.collapse').addClass('in');
				}
			});

		});

		function gotoLinkInMenu(link)//link
		{
			$('body').addClass('white-bg');
			$("#barInMenu").removeClass('hidden');
			$("#wrapper").addClass("hidden");
			
			window.location.href = link;		
		}
	</script>
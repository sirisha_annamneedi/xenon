<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>
				<ol class="breadcrumb">
					<li><strong>Test Execution Dashboard</strong></li>
					<li>${UserCurrentProjectName}</li>
					<li id="releaseNameLi">${Model.releaseDetails[0].release_name}</li>
				</ol>
			</h2>
		</div>

	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="wrapper wrapper-content " id="wrapperDiv">
				<%-- <div class="row">
	<c:if test="${Model.automationStatus != 1}">
		<div class="col-lg-3">
			<div class="ibox float-e-margins dashlink"
				onclick="dashLink('ActRelease')">
				<div class="ibox-title">
					<h5>Active Releases</h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins text-success">${Model.Count[0].total_release}</h1>
				</div>
			</div>
		</div> </c:if>
		<div class="col-lg-6">
			<div class="ibox float-e-margins dashlink"
				onclick="dashLink('ActBuild')">
				<div class="ibox-title">
					<h5>Active Builds</h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins text-success">${Model.Count[0].total_build}</h1>
				</div>
			</div>
		</div>

		<div class="col-lg-6">
			<div class="ibox float-e-margins dashlink"
				onclick="dashLink('TestCase')">
				<div class="ibox-title">
					<h5>Test Cases</h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins text-success">${Model.Count[0].total_test}</h1>
				</div>
			</div>
		</div>
	
		<c:if test="${Model.automationStatus == 1}">
<!-- 		<div class="col-lg-3"> -->
<!-- 					<div class="ibox float-e-margins dashlink" -->
<!-- 						onclick="gotoLinkInMenu('builddetails?manual=1&auto=1&sc=1&buildType=automation')"> -->
<!-- 						<div class="ibox-title"> -->
<!-- 							<h5>Remaining Automation Minutes</h5> -->
<!-- 						</div> -->
						<c:set var="total" value="${1000}"/>
<!-- 						<div class="ibox-content"> -->
<!-- 							<h1 class="no-margins text-success"> -->
										${total-(Model.Count[0].ExecTimeSec/60)}/${total}
<!-- 							</h1> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
				</c:if>
	</div> --%>
				<script>
					var release_name = [];
				</script>
				<div class="float-e-margins">

					<div class="graph-dashboard">
						<div class="col-md-12">
							<div class="gd-title">Recent Release Execution Analysis</div>
							<div class="row">
								<div class="col-md-12">
									<div class="testc-status mt-20" style="pointer-events: none;">
										<canvas id="stackedBarChart" height=""></canvas>
									</div>
								</div>

							</div>

						</div>
					</div>

					<div class="graph-dashboard">
						<div class="col-md-12">
							<div class="gd-title row">
								<div class="col-md-2">Recent Release</div>
								<div class="col-md-3">
									<div class="form-group">

										<select id="releasefilterondropdown" class="form-control" onchange="getReleaseData(this.value)">
											<c:forEach var="release" items="${Model.releaseDetails}">
												<c:if test="${release.releaseStatus == 'Active'}">
													<option value="${release.release_id }"
														${Model.lastRelease == release.release_id ? 'selected="selected"' : ''}>${release.release_name}</option>
												</c:if>
											</c:forEach>
										</select>
										<!--  <button type="button" id="releasewisefilter" class="btn">Click</button> -->

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="testc-status mt-20">
										<table id="bothdata" class="table table-bordered table-hover">
											<thead>
												<tr>
													<th scope="col">Release Name</th>
													<th scope="col">Test Set Name</th>
													<th scope="col">Type</th>
													<th scope="col">Total Test Cases</th>
													<th scope="col">Executed</th>

													<th scope="col">Pass</th>
													<th scope="col">Fail</th>
													<th scope="col">Skip</th>
													<th scope="col">Blocked</th>

												</tr>
											</thead>
											<tbody>
												<c:set var="lastRelease" value="0" />
												<c:forEach var="manual"
													items="${Model.totalReleaseTestData}">
													<c:if test="${manual.release_id==Model.lastRelease}">
														<tr>
															<c:choose>
																<c:when test="${curItem==fn:trim(manual.release_name)}">
																	<td></td>
																</c:when>
																<c:otherwise>
																	<td>${manual.release_name}</td>
																</c:otherwise>
															</c:choose>
															<td>${manual.build_name }</td>
															<td>${manual.type }</td>
															<td>${manual.PassCount+manual.FailCount+manual.SkipCount+manual.BlockCount}</td>
															<td>${manual.ExecutedTcCount }</td>
															<td>${manual.PassCount }</td>
															<td>${manual.FailCount }</td>
															<td>${manual.SkipCount }</td>
															<td>${manual.BlockCount }</td>
														</tr>

														<c:set var="curItem"
															value="${fn:trim(manual.release_name)}" />
													</c:if>
												</c:forEach>


											</tbody>
										</table>
										<!--  <script>
$(document).ready(function() {
    $('#bothdata').DataTable();
});

</script> -->
									</div>
								</div>

							</div>






						</div>
					</div>

					<div class=" col-md-6 mb-50" style="padding-left:0">
						<div class="graph-dashboard col-md-12">
							<div class="gd-title">Manual Builds</div>
							<div class="row">
								<div class="col-md-12">
									<div class="testc-status mt-20">
										<table class="table table-bordered table-hover no-margins"
											id="buildListTable">
											<thead>
												<tr>
													<th>Build Name</th>
													<th>Status</th>
													<th>% Completed</th>
													<th>Start Date</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="buildList" items="${Model.topBuilds}">
													<script>
														//var id = $
														//{
															//buildList.build_id
														//};
														//var name = '${buildList.build_name}';
														//BID.push(id);
														//BNAME.push(name);
													</script>

													<!--  onclick="viewBuild(${buildList.build_id} -->
													<tr class="gradeX" style="cursor: pointer",'View','${buildList.build_name}')">
														<td class="textAlignment"
															data-original-title="${buildList.build_name}"
															data-container="body" data-toggle="tooltip"
															data-placement="right">${buildList.build_name}</td>
														<c:choose>
															<c:when test="${buildList.build_state ==1 }">
																<td><small><label
																		class="label 4 label-success">
																			${buildList.desc}</label></small></td>
															</c:when>
															<c:when test="${buildList.build_state ==2 }">
																<td><small><label
																		class="label 4 label-info"> ${buildList.desc}</label></small></td>
															</c:when>
															<c:when test="${buildList.build_state ==3 }">
																<td><small><label
																		class="label 4 label-warning">
																			${buildList.desc}</label></small></td>
															</c:when>
															<c:when test="${buildList.build_state ==4 }">
																<td><small><label
																		class="label 4 label-danger">
																			${buildList.desc}</label></small></td>
															</c:when>
															<c:when test="${buildList.build_state ==5 }">
																<td><small><label
																		class="label 4 label-primary">
																			${buildList.desc}</label></small></td>
															</c:when>
															<c:when test="${buildList.build_state ==6 }">
																<td><small><label
																		class="label 4 label-warning"> Ready for
																			Execution</label></small></td>
															</c:when>
														</c:choose>

														<td class="text-navy">${buildList.percentCompleted}%</td>
														<td class="textAlignment"
															data-original-title="${buildList.build_createdate}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">
															<!-- <i class="fa fa-clock-o"></i> -->
															${buildList.build_createdate}
														</td>


													</tr>
												</c:forEach>


											</tbody>
										</table>
									</div>
								</div>

							</div>



						</div>
					</div>

					<div class="graph-dashboard col-md-6 mb-50">
						<div class="col-md-12">
							<div class="gd-title">Automation Builds</div>
							<div class="row">
								<div class="col-md-12">
									<div class="testc-status mt-20">
										<table class="table table-bordered table-hover no-margins"
											id="autoBuildListTable">
											<thead>
												<tr>
													<th>Build Name11</th>
													<th>Status</th>
													<th>% Completed</th>
													<th>Start Date</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="buildList" items="${Model.topAutoBuilds}">
<!-- 													<!--<script>
// 														var ida = $
// 														{
// 															buildList.build_id
// 														};
// 														var namea = '${buildList.build_name}';
// 														ABID.push(ida);
// 														ABNAME.push(namea);
<!-- 													</script> -->


													<!-- onclick="viewBuild(${buildList.build_id} -->
													<tr class="gradeX" style="cursor: pointer",'View','${buildList.build_name}')">
														<td class="textAlignment"
															data-original-title="${buildList.build_name}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${buildList.build_name}</td>
														<c:choose>
															<c:when test="${buildList.build_state ==1 }">
																<td><small><label
																		class="label 4 label-success">
																			${buildList.desc}</label></small></td>
															</c:when>
															<c:when test="${buildList.build_state ==2 }">
																<td><small><label
																		class="label 4 label-info"> ${buildList.desc}</label></small></td>
															</c:when>
															<c:when test="${buildList.build_state ==3 }">
																<td><small><label
																		class="label 4 label-warning">
																			${buildList.desc}</label></small></td>
															</c:when>
															<c:when test="${buildList.build_state ==4 }">
																<td><small><label
																		class="label 4 label-danger">
																			${buildList.desc}</label></small></td>
															</c:when>
															<c:when test="${buildList.build_state ==5 }">
																<td><small><label
																		class="label 4 label-primary">
																			${buildList.desc}</label></small></td>
															</c:when>
															<c:when test="${buildList.build_state ==6 }">
																<td><small><label
																		class="label 4 label-default">
																			${buildList.desc}</label></small></td>
															</c:when>
															<c:otherwise>
																<td><small><label
																		class="label 4 label-default">
																			${buildList.desc}</label></small>
															</c:otherwise>
														</c:choose>

														<td class="text-navy">${buildList.percentCompleted}%</td>
														<td class="textAlignment"
															data-original-title="${buildList.build_createdate}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">
															<!-- <i class="fa fa-clock-o"></i> -->
															${buildList.build_createdate}
														</td>


													</tr>
												</c:forEach>


											</tbody>
										</table>
									</div>
								</div>

							</div>



						</div>
					</div>

					<%-- <div class="ibox-content">
<!-- 				<div class ="row"> -->
<!-- 					<div class="col-md-6"> -->
<!-- 						<div class="ibox-title"> -->
<!-- 							<h5>Release wise update</h5> -->
<!-- 							<div class="pull-right"></div> -->
<!-- 							</div> -->
							<canvas id="myReleaseChart"></canvas>
<!-- 					 </div> -->
<!-- 				</div> -->
				<div class="row">
					<div class="col-lg-12">
					<h5>Recent Builds Execution Analysis</h5>
						<div>
							<canvas id="stackedBarChartt" height=""></canvas>
						</div>
					</div>
					
				</div>
			</div> --%>
					<%-- <div class="ibox-content">
					<table class="table table-stripped"
						id="allReleasetable">
						
					
						<tbody>
							<c:forEach var="data" items="${Model.releaseDetails}">
								
								<script> 
											var d = '${data.release_name}';
											release_name.push(d);
											//document.write("Release Name"+release_name);
								</script>
							</c:forEach>
						</tbody>
					</table>
				</div> --%>

				</div>

				<script type="text/javascript">
					var AutoStatus;
					var BID = [];
					var BNAME = [];
					var ABID = [];
					var ABNAME = [];
				</script>
				<%-- <div class="row">
				<c:if test="${Model.automationStatus == 1}">
				<script>AutoStatus = '${Model.automationStatus}'
				console.log(AutoStatus);</script>
					<div class="col-lg-6">
						<div class="tabs-container">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#tab-1">Manual
										Builds</a></li>
								<li class=""><a data-toggle="tab" href="#tab-2">Automation
										Builds</a></li>
							</ul>
							<div class="tab-content">
								<div id="tab-1" class="tab-pane active">
									<div class="panel-body">
										<div class="ibox float-e-margins">
											<div class="ibox-title">
												<h5>Recent Builds</h5>
												<button type="button" id="manualBuildLoadingBtn"
													class="btn btn-white btn-sm pull-right">
													<i class="fa fa-refresh"></i> Refresh
												</button>
											</div>
											<div class="ibox-content"
												style="padding-right: 5px; padding-left: 5px">
												<table class="table table-hover no-margins"
													id="buildListTable">
													<thead>
														<tr>
															<th>Build Name</th>
															<th>Status</th>
															<th>% Completed</th>
															<th>Start Date</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="buildList" items="${Model.topBuilds}">
															<script>var id =${buildList.build_id};
																	var name ='${buildList.build_name}';
																	BID.push(id);
																	BNAME.push(name);
																	</script>
															<tr class="gradeX" style="cursor: pointer" onclick="viewBuild(${buildList.build_id},'View','${buildList.build_name}')">
																<td class="textAlignment" data-original-title="${buildList.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" >${buildList.build_name}</td>
																<c:choose>
																	<c:when test="${buildList.build_state ==1 }">
																		<td><small><label
																				class="label 4 label-success">
																					${buildList.desc}</label></small></td>
																	</c:when>
																	<c:when test="${buildList.build_state ==2 }">
																		<td><small><label
																				class="label 4 label-info">
																					${buildList.desc}</label></small></td>
																	</c:when>
																	<c:when test="${buildList.build_state ==3 }">
																		<td><small><label
																				class="label 4 label-warning">
																					${buildList.desc}</label></small></td>
																	</c:when>
																	<c:when test="${buildList.build_state ==4 }">
																		<td><small><label
																				class="label 4 label-danger">
																					${buildList.desc}</label></small></td>
																	</c:when>
																	<c:when test="${buildList.build_state ==5 }">
																		<td><small><label
																				class="label 4 label-primary">
																					${buildList.desc}</label></small></td>
																	</c:when>
																	<c:when test="${buildList.build_state ==6 }">
																		<td><small><label
																				class="label 4 label-warning">
																					Ready for Execution</label></small></td>
																	</c:when>
																</c:choose>

																<td class="text-navy">${buildList.percentCompleted}%</td>
																<td class="textAlignment" data-original-title="${buildList.build_createdate}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">
																	<!-- <i class="fa fa-clock-o"></i> -->
																	${buildList.build_createdate}
																</td>


															</tr>
														</c:forEach>


													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div id="tab-2" class="tab-pane">
									<div class="panel-body">
										<div class="ibox float-e-margins">
											<div class="ibox-title">
												<h5>Recent Builds</h5>
												<button type="button" id="automationBuildLoadingBtn"
													class="btn btn-white btn-sm pull-right">
													<i class="fa fa-refresh"></i> Refresh
												</button>
											</div>
											<div class="ibox-content"
												style="padding-right: 5px; padding-left: 5px">
												<table class="table table-hover no-margins"
													id="autoBuildListTable">
													<thead>
														<tr>
															<th>Build Name11</th>
															<th>Status</th>
															<th>% Completed</th>
															<th>Start Date</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="buildList" items="${Model.topAutoBuilds}">
															<script>
																	var ida =${buildList.build_id};
																	var namea ='${buildList.build_name}';
																	ABID.push(ida);
																	ABNAME.push(namea);
															</script>
														
															<tr class="gradeX" style="cursor: pointer" onclick="viewBuild(${buildList.build_id},'View','${buildList.build_name}')">
																<td class="textAlignment" data-original-title="${buildList.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${buildList.build_name}</td>
																<c:choose>
																	<c:when test="${buildList.build_state ==1 }">
																		<td><small><label
																				class="label 4 label-success">
																					${buildList.desc}</label></small></td>
																	</c:when>
																	<c:when test="${buildList.build_state ==2 }">
																		<td><small><label
																				class="label 4 label-info">
																					${buildList.desc}</label></small></td>
																	</c:when>
																	<c:when test="${buildList.build_state ==3 }">
																		<td><small><label
																				class="label 4 label-warning">
																					${buildList.desc}</label></small></td>
																	</c:when>
																	<c:when test="${buildList.build_state ==4 }">
																		<td><small><label
																				class="label 4 label-danger">
																					${buildList.desc}</label></small></td>
																	</c:when>
																	<c:when test="${buildList.build_state ==5 }">
																		<td><small><label
																				class="label 4 label-primary">
																					${buildList.desc}</label></small></td>
																	</c:when>
																	<c:when test="${buildList.build_state ==6 }">
																		<td><small><label
																				class="label 4 label-default">
																					${buildList.desc}</label></small></td>
																	</c:when>
																	<c:otherwise>
																	<td><small><label
																				class="label 4 label-default">
																					${buildList.desc}</label></small></c:otherwise>
																</c:choose>

																<td class="text-navy">${buildList.percentCompleted}%</td>
																<td class="textAlignment" data-original-title="${buildList.build_createdate}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">
																	<!-- <i class="fa fa-clock-o"></i> -->
																	${buildList.build_createdate}
																</td>


															</tr>
														</c:forEach>


													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</c:if>
				<c:if test="${Model.automationStatus != 1}">
					<div class="col-lg-6">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Recent Builds</h5>
								<button type="button" id="manualBuildLoadingBtn"
									class="btn btn-white btn-sm pull-right">
									<i class="fa fa-refresh"></i> Refresh
								</button>
							</div>
							<div class="ibox-content">
								<table class="table table-hover no-margins" id="buildListTable">
									<thead>
										<tr>
											<th>Build Name</th>
											<th>Status</th>
											<th>% Completed</th>
											<th>Start Date</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="buildList" items="${Model.topBuilds}">
											<tr class="gradeX" style="cursor: pointer" onclick="viewBuild(${buildList.build_id},'View','${buildList.build_name}')">
												<td>${buildList.build_name}</td>
												<c:choose>
													<c:when test="${buildList.build_state ==1 }">
														<td><small><label
																class="label 4 label-success"> ${buildList.desc}</label></small></td>
													</c:when>
													<c:when test="${buildList.build_state ==2 }">
														<td><small><label class="label 4 label-info">
																	${buildList.desc}</label></small></td>
													</c:when>
													<c:when test="${buildList.build_state ==3 }">
														<td><small><label
																class="label 4 label-warning"> ${buildList.desc}</label></small></td>
													</c:when>
													<c:when test="${buildList.build_state ==4 }">
														<td><small><label
																class="label 4 label-danger"> ${buildList.desc}</label></small></td>
													</c:when>
													<c:when test="${buildList.build_state ==5 }">
														<td><small><label
																class="label 4 label-primary"> ${buildList.desc}</label></small></td>
													</c:when>
													<c:when test="${buildList.build_state ==6 }">
														<td><small><label
																class="label 4 label-warning"> Ready for Execution</label></small></td>
													</c:when>
												</c:choose>

												<td class="text-navy">${buildList.percentCompleted}%</td>
												<td>
													${buildList.build_createdate}
												</td>
											</tr>
										</c:forEach>


									</tbody>
								</table>
							</div>
						</div>
					</div>
				</c:if>
				<!-- <div class="col-lg-6">
					<div class="ibox float-e-margins">
					<div class="client-detail" style="max-height: 473px">
						<div class="full-height-scroll">
						<div class="ibox-title">
							<h5>Recent Activities</h5>
							<div class="ibox-tools">
								<span class="label label-warning-light pull-right">
									Recent &nbsp;${fn:length(Model.topActivities)} &nbsp;Activities</span>
							</div>
						</div>
						<div class="ibox-content">
							<div class="feed-activity-list">
								<c:forEach var="topActivities" items="${Model.topActivities}"
									varStatus="loop">
									<div class="feed-element">
										<a class="pull-left"> <img alt="image" class="img-circle"
											src="data:image/jpg;base64,${topActivities.user_image}">
										</a>
										<div class="media-body ">


											<c:set var="date"
												value="${fn:split(topActivities.activity_date,' ')}" />


											<c:choose>

												<c:when test="${topActivities.TimeDiff < 60 }">
													<strong class="pull-right">${topActivities.TimeDiff}
														min ago</strong>
												</c:when>

												<c:when
													test="${topActivities.TimeDiff >= 60 && topActivities.TimeDiff < 1440 }">
													<fmt:parseNumber var="hour" integerOnly="true"
														type="number" value="${topActivities.TimeDiff/60}" />
													<strong class="pull-right">${hour} hr ago </strong>
												</c:when>


												<c:when
													test="${topActivities.TimeDiff >= 1440 && topActivities.TimeDiff < 2880 }">
													<strong class="pull-right"> Yesterday </strong>
												</c:when>
												<c:otherwise>
													<strong class="pull-right">${topActivities.activity_date}
													</strong>
												</c:otherwise>
											</c:choose>

											<strong>${topActivities.first_name}&nbsp;${topActivities.last_name}
											</strong>${topActivities.activityState}.<br> <small
												class="text-muted">${topActivities.activity_date}</small>

											<div class="well">
												<div class="client-detail" style="max-height: 80px">
													<div class="full-height-scroll">
														${topActivities.activity_desc}
													</div>
												</div>
											</div>
										</div>
									</div>
								</c:forEach>

							</div>

						</div>
						</div>
						</div>
					</div>

				</div>-->
			</div> --%>

			</div>
		</div>
	</div>
</div>
<script>
	var StackedChartLabels = [];
	var listForJavascript = [];
	var ExecutedTcCountArray = [];
	var PassCountArray = [];
	var FailCountArray = [];
	var SkipCountArray = [];
	var BlockCountArray = [];

	<c:forEach items='${Model.totalReleaseTestData}' var='listItem'>
	//alert('${Model.totalReleaseTestData}');
	//alert( '${listItem.release_id}'+ ' : '+ ' ${Model.lastRelease}' )

	if ('true' == '${listItem.release_id == Model.lastRelease}') {
		// 	 console.log('${listItem.release_id == Model.lastRelease}');  
		StackedChartLabels.push("<c:out value='${listItem.build_name}' />");
		ExecutedTcCountArray
				.push("<c:out value='${listItem.ExecutedTcCount}' />");
		PassCountArray.push("<c:out value='${listItem.PassCount}' />");
		FailCountArray.push("<c:out value='${listItem.FailCount}' />");
		SkipCountArray.push("<c:out value='${listItem.SkipCount}' />");
		BlockCountArray.push("<c:out value='${listItem.BlockCount}' />");
	}
	</c:forEach>
	// alert(ExecutedTcCountArray);
</script>
<script type="text/javascript">
	$(document).ready(function() {
		
	});

	
	function viewmanualBuild(id, viewType, buildName) {

		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");

		var posting = $.post('setexecutebuildid', {
			buildID : id,
			buildName : buildName,

		});
		posting.done(function(data) {
			window.location.href = "buildreport";
		});
	}

	function viewAutomationReport(id) {

		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");

		var posting = $.post('setautoexecutebuildid', {
			buildID : id
		});
		posting.done(function(data) {
			window.location.href = "automationsummary";
		});
	}

	$(window)
			.load(
					function() {

						$("#barInMenu").addClass("hidden");
						$("#wrapper").removeClass("hidden");

						var labels = [];
						var passData = [];
						var failData = [];
						var skipData = [];
						var runData = [];
						var graphData = '${Model.GraphData}';
						
						var builddetails = '${allBuildDetails}';
						//console..log("Graph Data "+graphData);
						graphData = JSON.parse(graphData);

						for (i = 0; i < graphData.length; i++) {

							var testArray = graphData[i].testcase_status;
							var p = 0;
							var f = 0;
							var s = 0;
							var r = 0;

							for (j = 0; j < testArray.length; j++) {
								if (testArray[j].tc_status == "Pass") {

									passData.push(testArray[j].tc_count);
									p = 1;

								}
								if (testArray[j].tc_status == "Fail") {

									failData.push(testArray[j].tc_count);
									f = 1;

								}
								if (testArray[j].tc_status == "Skip") {

									skipData.push(testArray[j].tc_count);
									s = 1;

								}
								if (testArray[j].tc_status == "Not run") {
									runData.push(testArray[j].tc_count);
									r = 1;

								}

							}

							if (testArray.length == 0) {
								passData.push(0);
								failData.push(0);
								skipData.push(0);
								runData.push(0);
							} else {
								if (p == 0) {
									passData.push(0);

								}
								if (f == 0) {
									failData.push(0);

								}
								if (s == 0) {
									skipData.push(0);

								}
								if (r == 0) {
									runData.push(0);
								}
							}
						}

						for (i = 0; i < graphData.length; i++) {
							labels.push(graphData[i].build_name);
						}

						colorPass = "${Model.Pass}";
						colorFail = "${Model.Fail}";
						colorSkip = "${Model.Skip}";
						colorNotRun = "${Model.NotRun}";

						var barOptions_stacked = {
							scaleBeginAtZero : true,
							scaleShowGridLines : true,
							scaleGridLineColor : "rgba(0,0,0,.05)",
							scaleGridLineWidth : 1,
							barShowStroke : true,
							barStrokeWidth : 2,
							barValueSpacing : 5,
							barDatasetSpacing : 1,
							responsive : true,
							scales : {
								xAxes : [ {
									stacked : true,
									barThickness : 60,
									ticks : {
										callback : function(value) {
											if (value.length <= 5) {
												return value;
											} else {
												return value.substr(0, 5)
														+ "...";//truncate
											}
										},
									}
								} ],
								yAxes : [ {
									stacked : true,
									ticks : {
										min : 0,
										callback : function(value) {
											if (value % 1 === 0) {
												return value;
											}
										}
									}
								} ]
							},
							tooltips : {
								enabled : true,
								mode : 'label',
								callbacks : {
									title : function(tooltipItems, data) {
										var idx = tooltipItems[0].index;
										return data.labels[idx];//title
									}
								}
							}
						};

						var stackedData = {
							labels : StackedChartLabels,

							datasets : [
									{
										label : "Executed",
										data : ExecutedTcCountArray,
										backgroundColor : "rgba(255, 99, 132, 0.9)",
										hoverBackgroundColor : "rgba(255, 99, 132, 1)"
									},
									{
										label : "Pass",
										data : PassCountArray,
										backgroundColor : "rgba(54, 162, 235, 0.9)",
										hoverBackgroundColor : "rgba(54, 162, 235, 1)"
									},
									{
										label : "Fail",
										data : FailCountArray,
										backgroundColor : "rgba(255, 206, 86,0.9)",
										hoverBackgroundColor : "rgba(255, 206, 86,1)"
									},
									{
										label : "Skip",
										data : SkipCountArray,
										backgroundColor : "rgba(255, 99, 132, 0.9)",
										hoverBackgroundColor : "rgba(255, 99, 132, 0.9)"
									},
									{
										label : "Blocked",
										data : BlockCountArray,
										backgroundColor : "rgba(75, 192, 192,0.9)",
										hoverBackgroundColor : "rgba(75, 192, 192, 1)"
									} ]
						}
						var ctx = document.getElementById("stackedBarChart");
						var myChart = new Chart(ctx, {
							type : 'bar',
							data : stackedData,
							options : barOptions_stacked,
						});

						
						var projectBarData = [];
						var projectGraphData = '${Model.allProjectBug}';

						projectGraphData = JSON.parse(projectGraphData);

						var newBug = [];
						var DevinProgress = [];
						var ReadyforQA = [];
						var Closed = [];
						var projectName = [];

						for (i = 0; i < projectGraphData.length; i++) {

							projectName.push(projectGraphData[i].projectName);
							newBug.push(projectGraphData[i].New);
							DevinProgress
									.push(projectGraphData[i].DevinProgress);
							ReadyforQA.push(projectGraphData[i].ReadyforQA);
							Closed.push(projectGraphData[i].Closed);
						}

						var lineData = {
							labels : projectName,
							datasets : [

							{
								label : "New",
								backgroundColor : 'rgba(100,149,237,0.5)',
								borderColor : "rgba(100,149,237,0.7)",
								pointBorderColor : "#fff",
								data : newBug
							}, {
								label : "Dev in Progress",
								backgroundColor : 'rgba(205,205,0,0.5)',
								borderColor : "rgba(205,205,0,0.7)",
								pointBorderColor : "#fff",
								data : DevinProgress
							}, {
								label : "Ready for QA",
								backgroundColor : 'rgba(255,127,36,0.5)',
								borderColor : "rgba(255,127,36,0.7)",
								pointBorderColor : "#fff",
								data : ReadyforQA
							}, {
								label : "Closed",
								backgroundColor : 'rgba(26,179,148,0.5)',
								borderColor : "rgba(26,179,148,0.7)",
								pointBorderColor : "#fff",
								data : Closed
							} ]
						};

						var lineOptions = {
							responsive : true,
							scales : {
								xAxes : [ {
									ticks : {
										callback : function(value) {
											if (value.length <= 5) {
												return value;
											} else {
												return value.substr(0, 5)
														+ "...";//truncate
											}
										}
									}
								} ],
								yAxes : [ {
									ticks : {
										min : 0,
										callback : function(value) {
											if (value % 1 === 0) {
												return value;
											}
										}
									}
								} ]
							},
							tooltips : {
								enabled : true,
								mode : 'label',
								callbacks : {
									title : function(tooltipItems, data) {
										var idx = tooltipItems[0].index;
										return data.labels[idx];//title
									}
								}
							}
						};

// 						var ctx = document.getElementById("lineChart")
// 								.getContext("2d");
// 						new Chart(ctx, {
// 							type : 'line',
// 							data : lineData,
// 							options : lineOptions
// 						});

						var readyQA = [];
						var newBug = [];
						var assignBug = [];
						var devProgess = [];

						var bugData = '${Model.bugGraph}';
						bugData = JSON.parse(bugData);

						for (i = 0; i < bugData.length; i++) {
							if (bugData[i].bug_status == "New") {
								newBug.push(bugData[i].bug_count);
							} else if (bugData[i].bug_status == "Assigned") {
								assignBug.push(bugData[i].bug_count);
							} else if (bugData[i].bug_status == "Ready for QA") {
								readyQA.push(bugData[i].bug_count);
							} else if (bugData[i].bug_status == "Dev in Progress") {
								devProgess.push(bugData[i].bug_count);
							}
						}

						if (newBug.length == 0) {
							newBug[0] = 0;
						}
						if (assignBug.length == 0) {
							assignBug[0] = 0;
						}
						if (readyQA.length == 0) {
							readyQA[0] = 0;
						}
						if (devProgess.length == 0) {
							devProgess[0] = 0;
						}

						var doughnutData = {
							labels : [ "New", "Assigned", "Ready For QA",
									"Dev in Progress" ],
							datasets : [ {
								data : [ newBug[0], assignBug[0], readyQA[0],
										devProgess[0] ],
								backgroundColor : [ "#a3e1d4", "#dedede",
										"#b5b8cf", "rgb(128, 64, 0)" ],
								hoverBackgroundColor : [ "#a3e1d4", "#dedede",
										"#b5b8cf", "rgb(128, 64, 0)" ]
							} ]
						};

						var doughnutOptions = {
							segmentShowStroke : true,
							segmentStrokeColor : "#fff",
							segmentStrokeWidth : 2,
							percentageInnerCutout : 45, // This is 0 for Pie charts
							animationSteps : 100,
							animationEasing : "easeOutBounce",
							animateRotate : true,
							animateScale : true,
							responsive : true,
							legend : {
								position : 'top'
							}
						};

						var ctx = document.getElementById("doughnutChart");
						var myDoughnutChart = new Chart(ctx, {
							type : 'doughnut',
							data : doughnutData,
							options : doughnutOptions
						});

						var foo = $('.footable').footable();
						foo.trigger('footable_initialize'); //Reinitialize
						foo.trigger('footable_redraw'); //Redraw the table
						foo.trigger('footable_resize'); //Resize the table
					});

	function gd(year, month, day) {
		return new Date(year, month - 1, day).getTime();
	}

	var previousPoint = null, previousLabel = null;

	$(document)
			.ready(
					function() {

						$('.dataTables-example')
								.DataTable(
										{
											dom : '<"html5buttons"B>lTfgitp',
											buttons : [
													{
														extend : 'copy',
														exportOptions : {
															columns : ':visible'
														}
													},
													{
														extend : 'csv',
														exportOptions : {
															columns : ':visible'
														}
													},
													{
														extend : 'excel',
														title : 'ExampleFile',
														exportOptions : {
															columns : ':visible'
														}
													},
													{
														extend : 'pdf',
														title : 'ExampleFile',
														exportOptions : {
															columns : ':visible'
														}
													},

													{
														extend : 'print',
														exportOptions : {
															columns : ':visible'
														},
														customize : function(
																win) {
															$(win.document.body)
																	.addClass(
																			'white-bg');
															$(win.document.body)
																	.css(
																			'font-size',
																			'10px');

															$(win.document.body)
																	.find(
																			'table')
																	.addClass(
																			'compact')
																	.css(
																			'font-size',
																			'inherit');
														}
													} ]

										});

						var manualBuildTable = $('#buildListTable').DataTable({
							"paging" : false,
							"lengthChange" : false,
							"searching" : false,
							"ordering" : false,
							"info" : true,
							"autoWidth" : true
						});

						var automationBuildTable = $('#autoBuildListTable')
								.DataTable({
									"paging" : false,
									"lengthChange" : false,
									"searching" : false,
									"ordering" : false,
									"info" : true,
									"autoWidth" : true
								});

						$('#manualBuildLoadingBtn')
								.click(
										function() {
											btn = $(this);
											//Start the loading
											simpleLoad(btn, true);

											// Ajax call to load the data for manual builds
											//step 1 : Get the data using ajax
											$
													.ajax(
															{
																url : "recentmanualbuilds",
																type : "GET",
															})
													// The response is passed to the function
													.done(
															function(json) {

																var arr = JSON
																		.parse(JSON
																				.stringify(json));

																if (arr == 0) {
																	//Step 2: Clear the previous data
																	manualBuildTable
																			.clear()
																			.draw();
																} else {
																	//Step 2: Clear the previous data
																	manualBuildTable
																			.clear()
																			.draw();

																	$
																			.each(
																					arr,
																					function(
																							index,
																							jsonObject) {
																						if (jsonObject.build_state == 1) {
																							manualBuildTable.row
																									.add(
																											[
																													jsonObject.build_name,
																													'<label class="label label-success">'
																															+ jsonObject.desc
																															+ '</label>',
																													'<span class="text-navy">'
																															+ jsonObject.percentCompleted
																															+ '%</span>',
																													jsonObject.build_createdate ])
																									.draw()
																									.node();
																						} else if (jsonObject.build_state == 2) {
																							manualBuildTable.row
																									.add(
																											[
																													jsonObject.build_name,
																													'<label class="label label-info">'
																															+ jsonObject.desc
																															+ '</label>',
																													'<span class="text-navy">'
																															+ jsonObject.percentCompleted
																															+ '%</span>',
																													jsonObject.build_createdate ])
																									.draw()
																									.node();
																						} else if (jsonObject.build_state == 3) {
																							manualBuildTable.row
																									.add(
																											[
																													jsonObject.build_name,
																													'<label class="label label-warning">'
																															+ jsonObject.desc
																															+ '</label>',
																													'<span class="text-navy">'
																															+ jsonObject.percentCompleted
																															+ '%</span>',
																													jsonObject.build_createdate ])
																									.draw()
																									.node();
																						} else if (jsonObject.build_state == 4) {
																							manualBuildTable.row
																									.add(
																											[
																													jsonObject.build_name,
																													'<label class="label label-danger">'
																															+ jsonObject.desc
																															+ '</label>',
																													'<span class="text-navy">'
																															+ jsonObject.percentCompleted
																															+ '%</span>',
																													jsonObject.build_createdate ])
																									.draw()
																									.node();
																						} else if (jsonObject.build_state == 5) {
																							manualBuildTable.row
																									.add(
																											[
																													jsonObject.build_name,
																													'<label class="label label-primary">'
																															+ jsonObject.desc
																															+ '</label>',
																													'<span class="text-navy">'
																															+ jsonObject.percentCompleted
																															+ '%</span>',
																													jsonObject.build_createdate ])
																									.draw()
																									.node();
																						} else if (jsonObject.build_state == 6) {
																							manualBuildTable.row
																									.add(
																											[
																													jsonObject.build_name,
																													'<label class="label label-warning">'
																															+ "Ready for Execution"
																															+ '</label>',
																													'<span class="text-navy">'
																															+ jsonObject.percentCompleted
																															+ '%</span>',
																													jsonObject.build_createdate ])
																									.draw()
																									.node();
																						}
																					});
																}

															}).always(
															function() {
																//stop the loading
																simpleLoad(btn,
																		false);
															});
										});

						$('#automationBuildLoadingBtn')
								.click(
										function() {
											btn = $(this);
											//Start the loading
											simpleLoad(btn, true);

											// Ajax call to load the data for automation builds
											//step 1 : Get the data using ajax
											$
													.ajax(
															{
																url : "recentautobuilds",
																type : "GET",
															})
													// The response is passed to the function
													.done(
															function(json) {

																var arr = JSON
																		.parse(JSON
																				.stringify(json));

																if (arr == 0) {
																	//Step 2: Clear the previous data
																	automationBuildTable
																			.clear()
																			.draw();
																} else {
																	//Step 2: Clear the previous data
																	automationBuildTable
																			.clear()
																			.draw();

																	$
																			.each(
																					arr,
																					function(
																							index,
																							jsonObject) {
																						if (jsonObject.build_state == 1) {
																							automationBuildTable.row
																									.add(
																											[
																													jsonObject.build_name,
																													'<label class="label label-success">'
																															+ jsonObject.desc
																															+ '</label>',
																													'<span class="text-navy">'
																															+ jsonObject.percentCompleted
																															+ '%</span>',
																													jsonObject.build_createdate ])
																									.draw()
																									.node();
																						} else if (jsonObject.build_state == 2) {
																							automationBuildTable.row
																									.add(
																											[
																													jsonObject.build_name,
																													'<label class="label label-info">'
																															+ jsonObject.desc
																															+ '</label>',
																													'<span class="text-navy">'
																															+ jsonObject.percentCompleted
																															+ '%</span>',
																													jsonObject.build_createdate ])
																									.draw()
																									.node();
																						} else if (jsonObject.build_state == 3) {
																							automationBuildTable.row
																									.add(
																											[
																													jsonObject.build_name,
																													'<label class="label label-warning">'
																															+ jsonObject.desc
																															+ '</label>',
																													'<span class="text-navy">'
																															+ jsonObject.percentCompleted
																															+ '%</span>',
																													jsonObject.build_createdate ])
																									.draw()
																									.node();
																						} else if (jsonObject.build_state == 4) {
																							automationBuildTable.row
																									.add(
																											[
																													jsonObject.build_name,
																													'<label class="label label-danger">'
																															+ jsonObject.desc
																															+ '</label>',
																													'<span class="text-navy">'
																															+ jsonObject.percentCompleted
																															+ '%</span>',
																													jsonObject.build_createdate ])
																									.draw()
																									.node();
																						} else if (jsonObject.build_state == 5) {
																							automationBuildTable.row
																									.add(
																											[
																													jsonObject.build_name,
																													'<label class="label label-primary">'
																															+ jsonObject.desc
																															+ '</label>',
																													'<span class="text-navy">'
																															+ jsonObject.percentCompleted
																															+ '%</span>',
																													jsonObject.build_createdate ])
																									.draw()
																									.node();
																						} else if (jsonObject.build_state == 6) {
																							automationBuildTable.row
																									.add(
																											[
																													jsonObject.build_name,
																													'<label class="label label-default">'
																															+ jsonObject.desc
																															+ '</label>',
																													'<span class="text-navy">'
																															+ jsonObject.percentCompleted
																															+ '%</span>',
																													jsonObject.build_createdate ])
																									.draw()
																									.node();
																						}
																					});
																}

															}).always(
															function() {
																//stop the loading
																simpleLoad(btn,
																		false);
															});
										});

						manualBuildTable.on('draw', function() {
							$('tbody tr td:nth-child(1)')
									.each(
											function() {
												$(this).addClass(
														'textAlignment');
												$(this).attr(
														'data-original-title',
														$(this).text());
												$(this).attr('data-container',
														'body');
												$(this).attr('data-toggle',
														'tooltip');
												$(this).attr('data-placement',
														'right');
											});

							$('tbody tr td:nth-child(4)')
									.each(
											function() {
												$(this).addClass(
														'textAlignment');
												$(this).attr(
														'data-original-title',
														$(this).text());
												$(this).attr('data-container',
														'body');
												$(this).attr('data-toggle',
														'tooltip');
												$(this).attr('data-placement',
														'right');
											});

							//tooltip
							$('[data-toggle="tooltip"]').tooltip();
						});

						automationBuildTable.on('draw', function() {
							$('tbody tr td:nth-child(1)')
									.each(
											function() {
												$(this).addClass(
														'textAlignment');
												$(this).attr(
														'data-original-title',
														$(this).text());
												$(this).attr('data-container',
														'body');
												$(this).attr('data-toggle',
														'tooltip');
												$(this).attr('data-placement',
														'right');
											});

							$('tbody tr td:nth-child(4)')
									.each(
											function() {
												$(this).addClass(
														'textAlignment');
												$(this).attr(
														'data-original-title',
														$(this).text());
												$(this).attr('data-container',
														'body');
												$(this).attr('data-toggle',
														'tooltip');
												$(this).attr('data-placement',
														'right');
											});

							//tooltip
							$('[data-toggle="tooltip"]').tooltip();
						});

					});

	function simpleLoad(btn, state) {
		if (state) {
			btn.children().addClass('fa-spin');
			btn.contents().last().replaceWith(" Loading");
		} else {
			btn.children().removeClass('fa-spin');
			btn.contents().last().replaceWith(" Refresh");
		}
	}

	function dashLink(link) {

		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");

		var posting = $.post('setBMLinkSession', {
			filterText : link
		});

		posting.done(function(data) {
			window.location.href = "buildmanagerlink";
		});
	}

	function viewBuild(id, viewType, buildName) {

		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");

		var posting = $.post('setbuildid', {
			buildID : id,
			viewType : viewType,
			buildName : buildName
		});
		posting.done(function(data) {
			window.location.href = "editbuild";
		});
	}

	function getReleaseData(releaseID){
		var totalReleaseTestData = ${Model.totalReleaseTestData1};
		var tdata = '';
		var StackedChartLabels = [];
		var listForJavascript = [];
		var ExecutedTcCountArray = [];
		var PassCountArray = [];
		var FailCountArray = [];
		var SkipCountArray = [];
		var BlockCountArray = [];
		for (var i = 0; i < totalReleaseTestData.length; i++) {
			if (releaseID == totalReleaseTestData[i].release_id) {
				document.getElementById('releaseNameLi').innerHTML=totalReleaseTestData[i].release_name;
			var tbody = "<tr>";
		
				tbody += "<td>"
						+ totalReleaseTestData[i].release_name
						+ "</td>";
			
			tbody += "<td>"
					+ totalReleaseTestData[i].build_name
					+ "</td>";
			tbody += "<td>"
					+ totalReleaseTestData[i].type
					+ "</td>";
			tbody += "<td>"+totalReleaseTestData[i].ExecutedTcCount+"</td>";
			tbody += "<td>"+ totalReleaseTestData[i].ExecutedTcCount+ "</td>";
			tbody += "<td>" + totalReleaseTestData[i].PassCount
					+ "</td>";
			tbody += "<td>" + totalReleaseTestData[i].FailCount
					+ "</td>";
			tbody += "<td>" + totalReleaseTestData[i].SkipCount
					+ "</td>";
			tbody += "<td>"
					+ totalReleaseTestData[i].BlockCount
					+ "</td>";
			StackedChartLabels.push(totalReleaseTestData[i].build_name);
			ExecutedTcCountArray.push(totalReleaseTestData[i].ExecutedTcCount);
			PassCountArray.push(totalReleaseTestData[i].PassCount);
			FailCountArray.push(totalReleaseTestData[i].FailCount);
			SkipCountArray.push(totalReleaseTestData[i].SkipCount);
			BlockCountArray.push(totalReleaseTestData[i].BlockCount);
			tdata += tbody;
			}
		}
		document.getElementById("bothdata").tBodies[0].innerHTML = tdata;
	/// for graph	
		var barOptions_stacked = {
				scaleBeginAtZero : true,
				scaleShowGridLines : true,
				scaleGridLineColor : "rgba(0,0,0,.05)",
				scaleGridLineWidth : 1,
				barShowStroke : true,
				barStrokeWidth : 2,
				barValueSpacing : 5,
				barDatasetSpacing : 1,
				responsive : true,
				scales : {
					xAxes : [ {
						stacked : true,
						barThickness : 60,
						ticks : {
							callback : function(value) {
								if (value.length <= 5) {
									return value;
								} else {
									return value.substr(0, 5)
											+ "...";//truncate
								}
							},
						}
					} ],
					yAxes : [ {
						stacked : true,
						ticks : {
							min : 0,
							callback : function(value) {
								if (value % 1 === 0) {
									return value;
								}
							}
						}
					} ]
				},
// 				tooltips : {
// 					enabled : true,
// 					mode : 'label',
// 					callbacks : {
// 						title : function(tooltipItems, data) {
// 							var idx = tooltipItems[0].index;
// 							return data.labels[idx];//title
// 						}
// 					}
// 				}
			};

		var stackedData = {
				labels : StackedChartLabels,

				datasets : [
						{
							label : "Executed",
							data : ExecutedTcCountArray,
							backgroundColor : "rgba(255, 99, 132, 0.9)",
							hoverBackgroundColor : "rgba(255, 99, 132, 1)"
						},
						{
							label : "Pass",
							data : PassCountArray,
							backgroundColor : "rgba(54, 162, 235, 0.9)",
							hoverBackgroundColor : "rgba(54, 162, 235, 1)"
						},
						{
							label : "Fail",
							data : FailCountArray,
							backgroundColor : "rgba(255, 206, 86,0.9)",
							hoverBackgroundColor : "rgba(255, 206, 86,1)"
						},
						{
							label : "Skip",
							data : SkipCountArray,
							backgroundColor : "rgba(255, 99, 132, 0.9)",
							hoverBackgroundColor : "rgba(255, 99, 132, 0.9)"
						},
						{
							label : "Blocked",
							data : BlockCountArray,
							backgroundColor : "rgba(75, 192, 192,0.9)",
							hoverBackgroundColor : "rgba(75, 192, 192, 1)"
						} ]
			}
			var ctx = document.getElementById("stackedBarChart");
			var myChart = new Chart(ctx, {
				type : 'bar',
				data : stackedData,
				options : barOptions_stacked,
			});

	}

	$(document).ready(function() {

		/*   var release = '${Model.releaseDetails}';
		  release = JSON.parse('${Model.releaseDetails}');
		  document.write("This is release "+release.length);
		  var n ; */

		/*   var letters = '0123456789ABCDEF'.split('');
		    var color = [];
		    for (var x = 0; x < release_name.length; x++ ) {
		        color[x] += letters[Math.floor(Math.random() * 16)];
		    } */

		/* 	  var releasetestData = '${Model.testCountRelease}';
			  var attrModule =[];
			  for (var i = 0; i < releasetestData.length; i++){
				    var obj  = releasetestData[i];
				     for (var key in obj){
				        var attrName = key;
				        attrModule.push(obj[key]);
				        console.log ("N: "+attrName+ ", V: "+attrModule);
				     
				     }
			  } */

		var color = [];
		var c = 0;
		var b = 131;
		var a = 255;
		for (var x = 0; x < release_name.length; x++) {
			color[x] = "rgb(" + a + "," + b + "," + c + ")";
			//document.write("color"+color[x]);
			c = c + 120;
			a = a + 10;
			b = b + 80;
		}
		var data = {
			datasets : [ {
				data : [ 50, 100, 80, 10 ],
				backgroundColor : color,
			} ],
			labels : release_name,

		};

// 		var canvas = document.getElementById("myReleaseChart");
// 		var ctx = canvas.getContext("2d");
// 		var myNewChart = new Chart(ctx, {
// 			type : 'doughnut',
// 			data : data,
// 			options : {
// 				pieceLabel : {
// 					render : 'value' //show values
// 				}
// 			}

// 		});

// 		canvas.onclick = function(evt) {
// 			var activePoints = myNewChart.getElementsAtEvent(evt);
// 			if (activePoints[0]) {
// 				var chartData = activePoints[0]['_chart'].config.data;
// 				var idx = activePoints[0]['_index'];

// 				var label = chartData.labels[idx];
// 				var value = chartData.datasets[0].data[idx];

// 				if (label == "Draft") {
// 					alert("link direction work in progress");
// 					//dashLink('totalDraft');
// 				} else if (label == "In Review") {
// 					//dashLink('review');
// 				} else if (label == "In Progress Review") {
// 					//dashLink('progressReview');
// 				} else if (label == "Review Completed") {
// 					//dashLink('reviewcompleted');
// 				} else {
// 					//dashLink('final');
// 				}
// 			}
// 		};
	});
</script>




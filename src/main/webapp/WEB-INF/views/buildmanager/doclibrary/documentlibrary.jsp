<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Document Library</h2>
		<!-- <ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li class="active"><strong>Document Library</strong></li>
		</ol> -->
	</div>
	<!-- end col-lg-10 -->
</div>
<!-- end row -->
<div class="row">
	<div class="col-lg-12">
		<div class="wrapper wrapper-content animated fadeInRight">
			<div class="" id="docLibraryTab">

				<div class="ibox">
					<div class="ibox-title">
					<%
					if(session.getAttribute("uploadProjectName")!=null){
					%>
						<h5><%=session.getAttribute("uploadProjectName")%> - Documents</h5>	<%}%>
					
					
						<div class="ibox-tools">
						
						<c:if test="${Model.NoProjectFound!=1}">
						<button type="button"
													class="btn btn-success" id="uploadDocumentBtn" class="btn btn-success btn-xs"><i
								class="fa fa-upload"></i> Upload New Document</button>
						
						</c:if>

						</div>
						<!-- Peity -->
					</div>
					<div class="ibox-content">
						<div class="project-list">
							<table class="table table-hover" id="documentsTable">
								<thead class="">
									<tr>
										<th class="hidden">Document Id</th>
										<th>Name</th>
										<th>Uploaded By</th>
										<th class="project-actions">Action</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="attachment" items="${Model.listFiles}"
										varStatus="loopCounter">
										<tr>
											<td class="hidden">${attachment.xe_doc_library_id}</td>

											<td class="project-title"><b>${attachment.doc_title}</b>
												<br /> <small>Upload Date: </small><small>${attachment.upload_time}</small>
											</td>
											<td class="project-title">${attachment.uploaded_by}</td>
											<td class="document-actions">
												<button class="btn btn-white btn-sm pull-right"
													onclick="downloadDocument(${attachment.xe_doc_library_id},'${attachment.doc_title }','${attachment.doc_type}');">
													<i class="fa fa-download"></i> Download
												</button>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>




				<!-- end  -->
			</div>
		</div>
		<!-- end wrapper -->
	</div>

</div>
</div>

						<div class="modal inmodal" id="uploadDocModal" tabindex="-1"
							role="dialog" aria-hidden="true" style="display: none;">
							<div class="modal-dialog modal-md">
								<div class="modal-content animated fadeIn">
									<div class="modal-header">
										<h6 class="modal-title pull-left col-sm-10">Upload
											Document Form</h6>
										<button type="button" class="close pull-right"
											data-dismiss="modal">
											<span aria-hidden="false">�</span><span class="sr-only">Close</span>
										</button>

									</div>

									<div class="modal-body">
										<form class="form-horizontal" id="documentlibraryuploadForm"
											enctype="multipart/form-data" action="documentlibraryupload"
											method="POST">

											<div class="form-group" id="uploadImageDiv">
												<label class="control-label col-lg-4">Upload
													Document</label>
												<div class="fileupload fileupload-new"
													data-provides="fileupload">
													<span class="btn btn-success btn-file"><span
														class="fileupload-new"> <i class="fa fa-upload"></i>
															Choose File
													</span> <span class="fileupload-exists">Change</span> <input
														type="file"
														accept='application/pdf,application/msword,image/*,.docx'
														id="uploadImage" name="uploadImage" /></span> <span
														class="fileupload-preview"></span> <a href="#"
														class="close fileupload-exists" data-dismiss="fileupload"
														style="float: none">�</a>
														<label class="error hidden" id="sizeError">Please upload file less than 2 MB</label>
														<label class="error hidden" id="fileError">Please select file to upload</label>
														
												</div>
											</div>


											<div class="modal-footer">
												<button type="button" id="btnCancel" class="btn btn-white pull-left"
													data-dismiss="modal">Cancel</button>
												<button type="button"
													class="btn btn-success pull-left ladda-button ladda-button-demo"
													data-dismiss="modal" id="submitDetailsBtn" data-style="slide-up">Submit</button>
											</div>
										</form>
									</div>

								</div>
							</div>
						</div>
<style>
.error {
	color: #8a1f11;
	margin-left: 1.5em;
	font-weight: bold;
}
</style>
<script>
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
	var l = $('.ladda-button-demo').ladda();
	l.click(function() {
		// Start loading
		l.ladda('start');
		// Do something in backend and then stop ladda
		// setTimeout() is only for demo purpose
		setTimeout(function() {
			
			
			//l.ladda('stop');
			$('#documentlibraryuploadForm').submit();
			l.ladda('stop');
		}, 1000)
	});
	$(document).ready(function() {

		$('#uploadDocumentBtn').click(function() {
			$('#uploadDocModal').modal('show'); 
		});
		$('#cancelDetailsBtn').click(function() {
			window.location.href="documentlibrary";
		});
		$('#passCancelBtn').click(function() {
			window.location.href="documentlibrary";
		});
	});

	
	$("#documentlibraryuploadForm").submit(function(e){
		 $("#fileError").addClass('hidden');
		var imgVal = $('#uploadImage').val(); 
	    if(imgVal=='') 
	    {
	    	 $("#sizeError").addClass('hidden');
	    	 $("#fileError").removeClass('hidden');
	        
	        $("#fileError").removeClass('hidden');
	        //e.preventDefault();
	        return false;
	    } 
	});
	
	$('#uploadImage').bind('change', function(e) {
		  $("#sizeError").addClass('hidden');
		  $("#fileError").addClass('hidden');
		if(this.files[0]){
		  var fileSize=this.files[0].size;
		  if(fileSize>1048576)
			{
			  $("#sizeError").removeClass('hidden');
			  $(this).val(null);
			}
		}
		});
	
	$("#btnCancel").click(function(){
		 
		$("a[data-dismiss='fileupload']").trigger('click');
    });
	
	function downloadDocument(doclibraryId,title,docType)
	{
		var posting = $.post('getDocumentById', {
			doclibraryId : doclibraryId
			});
			 posting.done(function(data) {
				 var a = window.document.createElement('a');
				 //a.href = window.URL.createObjectURL(new Blob([byteArray], { type: 'application/octet-stream' }));
				 a.href= "data:"+docType+";base64,"+data;
				 a.download = title;
				 // Append anchor to body.
				 document.body.appendChild(a)
				 a.click();
				 document.body.removeChild(a)
			});
			
	}
	function viewDocument(doclibraryId,title,docType)
	{
		var posting = $.post('getDocumentById', {
			doclibraryId : doclibraryId
			});
			 posting.done(function(data) {
				 var a = window.document.createElement('a');
				 //a.href = window.URL.createObjectURL(new Blob([byteArray], { type: 'application/octet-stream' }));
				 a.href= "data:"+docType+";base64,"+data;
				 a.target = "_blank";
				 // Append anchor to body.
				 document.body.appendChild(a)
				 a.click();
				 document.body.removeChild(a)
			});
			
	}
	
	$('#documentsTable').DataTable({
		"paging" : true,
		"lengthChange" : true,
		"searching" : true,
		"ordering" : true,
		"info" : true,
		"autoWidth" : true
	});
	
	
	function projectClick(projectId,projectName)
	{
		var posting = $.post('setuploadprojectid',{
			projectId : projectId,
			projectName : projectName
		});
		
		posting.done(function(data){
			window.location.href="documentlibrary";
		});
	}
</script>

<%
	//request.getSession().removeAttribute("uploadProjectId");
//	request.getSession().removeAttribute("uploadProjectName");
%>
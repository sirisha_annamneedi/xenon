<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Clone Build</h2>
		<ol class="breadcrumb">
			<li><a href="viewbuild">Test Set</a></li>
			<li class="active"><strong>Clone build</strong></li>
		</ol>
	</div>
	
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">

					<form id="form" action="inserclonebuilddata" method="post" class="">
						<h1>Source</h1>
						<fieldset>
							<h2>Select Source Build</h2>
							<div class="row ">
							<div class="form-group row">
											<label class="col-sm-2 control-label text-right">Release:</label>
											
											<div class="col-sm-8">
												<select data-placeholder="Choose a Release..." class="form-control m-b"  style="width:80%;" id="sourceReleaseId" tabindex="2">
													<c:forEach var="releaseDetails" items="${Model.releaseDetails}">
													<option value=${releaseDetails.release_id}>${releaseDetails.release_name}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										
										<c:if test="${Model.automationStatus == 1}">
										<div class="form-group row" >
											<label class="col-sm-2 control-label text-right">Type: </label>
											<div class="col-sm-8" style="padding-bottom: 1%;" >

												<div class="radio radio-primary radio-inline col-sm-2 ">
													<input type="radio"  value="${Model.exeTypes[0].execution_type_id}"
														name="buildExecutionType" checked="" tabindex="3"> <label
														for="buildExecutionType"> ${Model.exeTypes[0].description} </label>
												</div>
												<div class="radio radio-warning radio-inline col-sm-2">
													<input type="radio" id="inlineRadio2" value="${Model.exeTypes[1].execution_type_id}"
														name="buildExecutionType" tabindex="4"> <label
														for="buildExecutionType"> ${Model.exeTypes[1].description}  </label>
												</div>
											</div>
										</div>
										</c:if>
										<label class="alert alert-danger" style="display: none;" id="sourceBuildErr">No builds found for selected release, Please try another</label>
								  		<div class="form-group row" id="manualBuildDiv">
											<label class="col-sm-2 control-label text-right">Build:</label>
											<div class="col-sm-8">
												<select data-placeholder="Choose a Build..." class="form-control m-b" style="width:80%;" name="sourceBuildId" id="sourceManualBuildId" tabindex="3">
												<c:forEach var="manualBuildDetails" items="${Model.manualBuildDetails }">
												<c:if test="${manualBuildDetails.release_id == Model.releaseDetails[0].release_id}">
													<option value="${manualBuildDetails.build_id }">${manualBuildDetails.build_name}</option>
													</c:if>
												</c:forEach>
												</select>
											</div>
										</div>
										
										<div class="form-group row" style="display:none;" id="automationBuildDiv">
											<label class="col-sm-2 control-label text-right">Build:</label>
											
											<div class="col-sm-8">
												<select data-placeholder="Choose a Build..." class="form-control m-b" style="width:80%;"  name="sourceBuildId"  id="sourceAutomationBuildId" tabindex="3">
												<c:forEach var="automationBuildDetails" items="${Model.automationBuildDetails }">
												<c:if test="${automationBuildDetails.release_id == Model.releaseDetails[0].release_id}">
													<option value="${automationBuildDetails.build_id }">${automationBuildDetails.build_name} </option>
													</c:if>
												</c:forEach>
												</select>
											</div>
										</div>
							</div>
						</fieldset>
						<h1>New Build Info</h1>
						<fieldset>
							<h2>New Build Information</h2>
						<div class="row">
									<div class="col-lg-10">
                                       <div class="form-group row">
											<label class="col-sm-2 control-label text-right">Release:</label>
											
											<div class="col-sm-8">
												<select data-placeholder="Choose a Release..." class="form-control m-b"  style="width:80%;" name="destiReleaseId" id="destiReleaseId" tabindex="2">
														<c:forEach var="releaseDetails" items="${Model.releaseDetails}">
													<option value=${releaseDetails.release_id}>${releaseDetails.release_name}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										
										<div class="form-group row">
										
											<label class="control-label col-sm-2 text-right">Name:</label>
											<div class="col-sm-8" style="padding-bottom: 1%;" autocomplete="off">
												<input type="text" placeholder="Build Name"
													class="form-control characters" name="destiBuildName" id="destiBuildName" required>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-2 control-label text-right">
												Description:</label>
											<div class="col-sm-8"  style="padding-bottom: 1%;">
												<!-- <textarea class="form-control" style="max-width: 100%;"></textarea> -->
												<textarea name="buildDescription"   id="buildDescription"
													rows="5" class="summernote"  tabindex="3"> </textarea>
												<!-- <textarea name="buildDescription" id="buildDescription" rows="5"
													class="form-control characters" style="resize: none;"> </textarea> -->
											</div>
										</div>
										<c:if test="${Model.automationStatus == 1}">
										<div class="form-group row" >
											<label class="col-sm-2 control-label text-right">Type: </label>
											<div class="col-sm-8" style="padding-bottom: 1%;" >

												<div class="radio radio-primary radio-inline col-sm-2 ">
													<input type="radio"  value="${Model.exeTypes[0].execution_type_id}"
														name="buildType" checked="" tabindex="3"> <label
														for="buildType"> ${Model.exeTypes[0].description} </label>
												</div>
												<div class="radio radio-warning radio-inline col-sm-2">
													<input type="radio" value="${Model.exeTypes[1].execution_type_id}"
														name="buildType" tabindex="4"> <label
														for="buildType"> ${Model.exeTypes[1].description}  </label>
												</div>
											</div>
										</div>
										</c:if>
										<div class="form-group row hidden">
											<label class="col-sm-2 control-label text-right" >Status:</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio"  value="1"
														name="destiBuildStatus" checked=""> <label
														for="inlineRadio3"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio"  value="2"
														name="destiBuildStatus"> <label for="inlineRadio4">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>
								</div>
						</fieldset>
						<h1>Finish</h1>
						<fieldset>
							<div class="row">
								<div class="col-lg-6">
								<h2>Source Build</h2>
										<div class="row">
										<div class="form-group">
											<label class="col-sm-3 control-label text-right">Release:</label>
											
											<div class="col-sm-8">
												<select data-placeholder="Choose a Release..." class="form-control m-b"  style="width: 80%;" id="selectedSourceReleaseId" tabindex="2">
													<c:forEach var="releaseDetails" items="${Model.releaseDetails}">
													<option value=${releaseDetails.release_id}>${releaseDetails.release_name}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										
										<c:if test="${Model.automationStatus == 1}">
										<div class="form-group" >
											<label class="col-sm-3 control-label text-right">Type: </label>
											<div class="col-sm-8" style="padding-bottom: 1%;" >

												<div class="radio radio-primary radio-inline col-sm-3">
													<input type="radio"  value="${Model.exeTypes[0].execution_type_id}"
														name="buildExecutionTypeChosenSrc" checked="" tabindex="3"> <label
														for="buildExecutionTypeChosenSrc"> ${Model.exeTypes[0].description} </label>
												</div>
												<div class="radio radio-warning radio-inline col-sm-2">
													<input type="radio" id="inlineRadio2" value="${Model.exeTypes[1].execution_type_id}"
														name="buildExecutionTypeChosenSrc" tabindex="4"> <label
														for="buildExecutionTypeChosenSrc"> ${Model.exeTypes[1].description}  </label>
												</div>
											</div>
										</div>
										</c:if>
								  <div class="form-group" id="selectedManualBuildDiv">
											<label class="col-sm-3 control-label text-right">Build:</label>
											
											<div class="col-sm-8">
												<select data-placeholder="Choose a Build..." class="form-control m-b" style="width:80%;" id="selectedSourceManualBuildId" tabindex="3">
												<c:forEach var="manualBuildDetails" items="${Model.manualBuildDetails }">
													<option value="${manualBuildDetails.build_id }">${manualBuildDetails.build_name}</option>
												</c:forEach>
												</select>
											</div>
										</div>
										
										<div class="form-group" style="display:none;" id="selectedAutomationBuildDiv">
											<label class="col-sm-3 control-label text-right">Build:</label>
											
											<div class="col-sm-8">
												<select data-placeholder="Choose a Build..." class="form-control m-b" style="width:80%;" id="selectedSourceAutomationBuildId" tabindex="3">
												<c:forEach var="automationBuildDetails" items="${Model.automationBuildDetails }">
												<c:if test="${automationBuildDetails.release_id == Model.releaseDetails[0].release_id}">
													<option value="${automationBuildDetails.build_id }">${automationBuildDetails.build_name} </option>
													</c:if>
												</c:forEach>
												</select>
											</div>
										</div>
							     </div>
								</div>
								<div class="col-lg-6">
								<h2>Destination Build</h2>
										<div class="row">
									<div class="col-lg-10">
									  <div class="form-group">
											<label class="col-sm-3 control-label text-right">Release:</label>
											
											<div class="col-sm-8">
												<select data-placeholder="Choose a Release..." class="form-control m-b" id="selectedDstBuildId" name = "selectedDstBuildId" style="width:110%;" name="submittedReleaseId" tabindex="2">
													<c:forEach var="releaseDetails" items="${Model.releaseDetails}">
													<option value=${releaseDetails.release_id}>${releaseDetails.release_name}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<label class="alert alert-danger" style="display: none;" id="buildNameErr">Build name already
													exists, Please try another.</label>
										<div class="form-group">
											<label class="control-label col-sm-3 text-right" >Name:</label>
											<div class="col-sm-8" style="padding-bottom: 5%;">
												<input type="text" placeholder="Build Name"
													class="form-control characters" name="buildName" id="buildName" required>
											</div>
										</div>
										<c:if test="${Model.automationStatus == 1}">
										<div class="form-group" >
											<label class="col-sm-3 control-label text-right">Type: </label>
											<div class="col-sm-8" style="padding-bottom: 1%;" >

												<div class="radio radio-primary radio-inline col-sm-4">
													<input type="radio"  value="${Model.exeTypes[0].execution_type_id}"
														name="buildExecutionTypeChosenDst" checked="" tabindex="3"> <label
														for="buildExecutionTypeChosenDst"> ${Model.exeTypes[0].description} </label>
												</div>
												<div class="radio radio-warning radio-inline col-sm-2">
													<input type="radio" id="inlineRadio2" value="${Model.exeTypes[1].execution_type_id}"
														name="buildExecutionTypeChosenDst" tabindex="4"> <label
														for="buildExecutionTypeChosenDst"> ${Model.exeTypes[1].description}  </label>
												</div>
											</div>
										</div>
										</c:if>
										
										<<div class="form-group hidden">
											<label class="col-sm-3 control-label text-right">Status:</label>
											<div class="col-sm-8">
												<div class="radio radio-success radio-inline col-sm-4">
													<input type="radio" id="inlineRadio1" value="1"
														name="buildStatus" checked=""> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-4">
													<input type="radio" id="inlineRadio2" value="2"
														name="buildStatus"> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>

</div>
</div>
<style>
.margine-lable {
	margin-bottom: 2.0em !important;
}
</style>
<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});

  $(function () {
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	  $('.summernote').summernote();
		$('.note-editor').css('background','white');
		$('button[ data-event="codeview"]').remove();
		
		 $('div.note-insert').remove();
       $('div.note-table').remove();
       $('div.note-help').remove();
       $('div.note-style').remove();
       $('div.note-color').remove();
       $('button[ data-event="removeFormat"]').remove();
       $('button[ data-event="insertUnorderedList"]').remove();
       $('button[ data-event="fullscreen"]').remove();
       $('button[ data-original-title="Line Height"]').remove();
       $('button[ data-original-title="Font Family"]').remove();
       $('button[ data-original-title="Paragraph"]').remove();
	  var releaseCount = ${Model.releaseDetails.size()};
	  if(releaseCount == 0){
		  swal(
				{
					title : "You need a minimun 1 relase to create a build.",
						showCancelButton : true,
						confirmButtonColor : "#DD6B55",
						confirmButtonText : "Create Release",
						cancelButtonText : "Ok",
						closeOnConfirm : true,
						closeOnCancel : false
				}, function(isConfirm) {
					if (isConfirm) {
						window.location.href="createrelease";
					}
					else
						{
						window.location.href="bmdashboard";
						}
				}		
		  );
	  }
	});  
</script>

<script>


	$(document)
			.ready(
					function() {
						$("#form")
								.steps(
										{
											bodyTag : "fieldset",
											onStepChanging : function(event,
													currentIndex, newIndex) {
												// Always allow going backward even if the current step contains invalid fields!
												if (currentIndex > newIndex) {
													return true;
												}
												var form = $(this);

												// Clean up if user went backward before
												if (currentIndex < newIndex) {
													// To remove error styles
													$(
															".body:eq("
																	+ newIndex
																	+ ") label.error",
															form).remove();
													$(
															".body:eq("
																	+ newIndex
																	+ ") .error",
															form).removeClass(
															"error");
												}

												// Disable validation on fields that are disabled or hidden.
												form.validate().settings.ignore = ":disabled,:hidden";

												// Start validation; Prevent going forward if false
												return form.valid();
											},
											onStepChanged : function(event,
													currentIndex, priorIndex) {

												// Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
												if ( currentIndex === 2
														&& priorIndex === 3) {
													$(this).steps("previous");
												}
												if (currentIndex === 1) {
													$("#sourceBuildErr").css("display","none");
													$("#sourceManualBuildId").removeClass("error");
													if($('input[type=radio][name=buildExecutionType]:checked').val()==1)
													{
														if($("#sourceManualBuildId").val()==null)
														{
															$("#sourceManualBuildId").addClass("error");
															$("#sourceBuildErr").fadeIn();
															$(this).steps("previous");
														}
													}
													else if($('input[type=radio][name=buildExecutionType]:checked').val()==2)
													{
														if($("#sourceAutomationBuildId").val()==null)
														{
															$("#sourceAutomationBuildId").addClass("error");
															$("#sourceBuildErr").fadeIn();
															$(this).steps("previous");
														}
													}
													
													
												}
												if (currentIndex === 2) {
													$("#buildNameErr").css("display","none");
													$("#buildName").removeClass("error");
													$("#selectedSourceReleaseId").val($("#sourceReleaseId").val());
													if($('input[type=radio][name=buildExecutionType]:checked').val()==1)
													{
														$("#selectedAutomationBuildDiv").fadeOut();
														$("#selectedManualBuildDiv").fadeIn();
														$("#selectedSourceManualBuildId").val($("#sourceManualBuildId").val());
														$("#selectedSourceManualBuildId").prop("disabled",true);
													}
													else if($('input[type=radio][name=buildExecutionType]:checked').val()==2)
													{
														$("#selectedAutomationBuildDiv").fadeIn();
														$("#selectedManualBuildDiv").fadeOut();
														$("#selectedSourceAutomationBuildId").val($("#sourceAutomationBuildId").val());
														$("#selectedSourceAutomationBuildId").prop("disabled",true);
													}

													$("#buildName").val($("#destiBuildName").val());
													
													$("#selectedDstBuildId").val($("#destiReleaseId").val());
													$("input:radio[name=buildStatus][value="+ $("input:radio[name=destiBuildStatus]:checked").val()+ "]")
															.prop("checked",true);
													
													$("input:radio[name=buildExecutionTypeChosenSrc][value="+ $("input:radio[name=buildExecutionType]:checked").val()+ "]")
													.prop("checked",true);
													
													$("input:radio[name=buildExecutionTypeChosenDst][value="+ $("input:radio[name=buildType]:checked").val()+ "]")
													.prop("checked",true);
													
													//Disable all
													$("#selectedSourceReleaseId").prop("disabled",true);
													$("#selectedSourceAutomationBuildId").prop("disabled",true);
													$("#buildName").prop("disabled",true);
													$("#selectedDstBuildId").prop("disabled",true);
													$("input:radio[name=buildStatus]").prop("disabled",true);
													$("input:radio[name=buildExecutionTypeChosenSrc]").prop("disabled",true);
													$("input:radio[name=buildExecutionTypeChosenDst]").prop("disabled",true);
													
												}
											},
											onCanceled : function(event,
													currentIndex) {
												window.location.href = "viewbuild";
											},
											onFinishing : function(event,
													currentIndex) {
												var form = $(this);

												// Disable validation on fields that are disabled.
												// At this point it's recommended to do an overall check (mean ignoring only disabled fields)
												
												
												form.validate().settings.ignore = ":disabled";

												// Start validation; Prevent form submission if false
												return form.valid();
											},
											onFinished : function(event,
													currentIndex) {
												var form = $(this);
												var invalidateForm=false;
												$("#buildNameErr").css("display","none");
												$("#buildName").removeClass("error");
												if($('input[type=radio][name=buildExecutionType]:checked').val()==1)
												{
													var manualBuilds=${Model.manualBuildDetailsJSON};
													var manualBuildsSize=${Model.manualBuildDetailsJSON.size()};
													
													var buildName=$("#destiBuildName").val();
													var relId = $('#destiReleaseId').val();

													$.ajax({
													  type: 'POST',
													  url: 'checkmanualbuildname',
													  data: {
														relId : relId,
														buildName : buildName
													  },
													  async:false
													}).done(function( data ) {
													   if(data) {
														$("#buildNameErr").css("display","");
														$("#buildName").addClass("error");
														invalidateForm=true;
													   }
													});
												}
												else if($('input[type=radio][name=buildExecutionType]:checked').val()==2)
												{
													//var automationBuildDetailsJSON=${Model.automationBuildDetailsJSON};
													//var automationBuildDetailsJSONSize=${Model.automationBuildDetailsJSON.size()};
													var buildName=$("#destiBuildName").val();
													var relId = $('#destiReleaseId').val();
													$.ajax({
														  type: 'POST',
														  url: 'checkautobuildname',
														  data: {
															relId : relId,
															buildName : buildName
														  },
														  async:false
														}).done(function( data ) {
														   if(data) {
															$("#buildNameErr").css("display","");
															$("#buildName").addClass("error");
															invalidateForm=true;
														   }
														});
												}
												if(invalidateForm)
												{
													
												}
												else
												{
													$('body').addClass("white-bg");
													$("#barInMenu").removeClass("hidden");
													$("#wrapper").addClass("hidden");	
													//return false;
													$('.summernote').each( function() {
														$(this).val($(this).code());
														});
													form.submit();
												}
											}
										}).validate({
									errorPlacement : function(error, element) {
										element.before(error);
									},
									rules : {
										confirm : {
											equalTo : "#password"
										}
									}
								});

					});
	$(document).ready(function() {
		$('#sourceReleaseId').change(function() {
			$("#sourceBuildErr").css("display","none");
			$("#sourceManualBuildId").removeClass("error");
			
			if($('input[type=radio][name=buildExecutionType]:checked').val()==1)
			{
				var chosenReleaseId=$('#sourceReleaseId').val();
				var manualBuilds=${Model.manualBuildDetailsJSON};
				var manualBuildsSize=${Model.manualBuildDetailsJSON.size()};
				$("#sourceManualBuildId option").remove();
				
				for(var i=0;i<manualBuildsSize;i++)
					{
						if(manualBuilds[i].release_id==chosenReleaseId)
							{
								$("#sourceManualBuildId").append("<option value="+manualBuilds[i].build_id+">"+manualBuilds[i].build_name+"</option>");
							}
					}
			}
			else if($('input[type=radio][name=buildExecutionType]:checked').val()==2)
				{
					var chosenReleaseId=$('#sourceReleaseId').val();
					var automationBuildDetailsJSON=${Model.automationBuildDetailsJSON};
					var automationBuildDetailsJSONSize=${Model.automationBuildDetailsJSON.size()};
					$("#sourceAutomationBuildId option").remove();
					
					for(var i=0;i<automationBuildDetailsJSONSize;i++)
						{
							if(automationBuildDetailsJSON[i].release_id==chosenReleaseId)
								{
									$("#sourceAutomationBuildId").append("<option value="+automationBuildDetailsJSON[i].build_id+">"+automationBuildDetailsJSON[i].build_name+"</option>");
								}
						}
				}
		});
		
		
		
	    $('input[type=radio][name=buildExecutionType]').change(function() {
				if(this.value=="2")
				{
					$("#automationBuildDiv").fadeIn();
					$("#manualBuildDiv").fadeOut();
					
					var chosenReleaseId=$('#sourceReleaseId').val();
					var automationBuildDetailsJSON=${Model.automationBuildDetailsJSON};
					var automationBuildDetailsJSONSize=${Model.automationBuildDetailsJSON.size()};
					$("#sourceAutomationBuildId option").remove();
					
					for(var i=0;i<automationBuildDetailsJSONSize;i++)
						{
							if(automationBuildDetailsJSON[i].release_id==chosenReleaseId)
								{
									$("#sourceAutomationBuildId").append("<option value="+automationBuildDetailsJSON[i].build_id+">"+automationBuildDetailsJSON[i].build_name+"</option>");
								}
						}
				}
				if(this.value=="1")
				{
					$("#automationBuildDiv").fadeOut();
					$("#manualBuildDiv").fadeIn();
					var chosenReleaseId=$('#sourceReleaseId').val();
					var manualBuilds=${Model.manualBuildDetailsJSON};
					var manualBuildsSize=${Model.manualBuildDetailsJSON.size()};
					$("#sourceManualBuildId option").remove();
					
					for(var i=0;i<manualBuildsSize;i++)
						{
							if(manualBuilds[i].release_id==chosenReleaseId)
								{
									$("#sourceManualBuildId").append("<option value="+manualBuilds[i].build_id+">"+manualBuilds[i].build_name+"</option>");
								}
						}
				}
		});
	});
</script>
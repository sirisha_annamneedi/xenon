
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>
				<ol class="breadcrumb">
					<li><strong>${UserCurrentProjectName}</strong></li>
					<li>Manual Execution</li>
				</ol>
			</h2>
		</div>
	</div>

	<div class="row sub-topmenu">
	<input type="hidden" id="buildExist" value="${sessionScope.duplicateBuildError}"> 
		<div class="col-md-12">
			<div class="">
				<div class="col-md-6 pd-l-0">
					<ul>
						<li><a href="executebuild" id="homeMenu" class="active "><i
								class="fa fa-home"></i> Home</a></li>
						<li><a href="buildproject"><i class="fa fa-plus"></i>
								Add/Remove Test Cases</a></li>
						<!-- <li><a href="javascript:void(0)" data-toggle="modal" data-target="#uploadTestCase"><i class="fa fa-list"></i>
				 		 Execute</a></li> -->
						<li><a href="viewreport" id="addParameter"><i
								class="fa fa-line-chart"></i> Report</a></li>

					</ul>
				</div>
				<button type="button" id="rt" class="btn btn-info btn-sm pull-right"
					style="margin-top: 6px" disabled>Submit Execution</button>
			</div>
		</div>
	</div>



	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row" id="TCase">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Execute Test Set - ${Model.selectedExecuteBuildName}</h5>
						<!-- <button type="button" id="rt"
				class="btn btn-info pull-right" disabled>Submit Execution</button>-->
					</div>

					<div class="ibox-content">
						<div class="row">
							<div class="col-md-3 py-2 mb-10">
								<h4>All Filter</h4>
								<select class="custom-select" onchange="allFilter(this)">
									<option value="">Select Filter</option>
									<option value="Status">Status</option>
									<option value="Tester">Tester</option>
									<option value="Module">Module</option>
									<option value="Application">Application</option>
								</select>
							</div>

							<div class="col-md-2 py-2 mb-5" style="display: none;"
								id="Status">
								<h4>Status Filter</h4>
								 <select class="select2-multiple2" multiple
								    onchange="statusFilter(this)">
									<option value="Block">Block</option>
									<option value="Fail">Fail</option>
									<option value="Not">Not Run</option>
									<option value="Pass">Pass</option>
									<option value="Skip">Skip</option>

								</select>


							</div>
							<div class="col-md-2 py-2 mb-5" style="display: none;"
								id="Tester">
								<h4>Tester Filter</h4>
								<c:set var="idTester" value="0" />
								<select class="select2-multiple2" multiple
									name="testeruser" onchange="userFilter(this)">
									<c:forEach var="assignedUser" items="${Model.assignedUser}">
										<c:if test="${idTester!=assignedUser.user_id}">
											<option value="${assignedUser.first_name}">${assignedUser.first_name} ${assignedUser.last_name}</option>
										</c:if>
										<c:set var="idTester" value="${assignedUser.user_id }" />
									</c:forEach>
								</select> <input type="hidden" name="tcStatusHidden" class="nrStatus"
									id="tcStatusHidden" value="" />

							</div>



							<div class="col-md-2 py-2 mb-5" style="display: none;"
								id="Module">
								<h4>Module Filter</h4>
								<select class="select2-multiple2" multiple
									onchange="moduleFilter(this)">
									<c:forEach var="modulefilter" items="${Model.modulesList}">
										<option value="${modulefilter.module_name}">${modulefilter.module_name}</option>

									</c:forEach>
								</select>

							</div>

							<div class="col-md-2 py-2 mb-5" style="display: none;"
								id="Application">
								<h4>Application Filter</h4>
								<select class="select2-multiple2" multiple
									onchange="applicationFilter(this)">
									<c:forEach var="projectfilter" items="${Model.projectsList}">
										<option value="${projectfilter.project_name}">${projectfilter.project_name}</option>

									</c:forEach>
								</select>

							</div>
						</div>

						<div class="table-responsive"
							style="overflow-x: visible; table-layout: fixed; width: 100%">

							<table
								class="table table-striped table-bordered table-hover dataTables-example"
								id="tblTC">
								<thead>
									<tr>
										<td style="display: none">Test Case Id</td>
										<td style="display: none">Build Id</td>
										<td style="display: none">Method Type</td>
										<td><input type="checkbox" id="checkAll"></td>
										<td style="text-align: center"><b>Id</b></td>
										<td style="text-align: center"><b>Title</b></td>
										<td style="text-align: center"><b>Summary</b></td>
										<td style="text-align: center"><b>Status</b></td>
										<td style="display: none">Tc Status</td>
										<td style="text-align: center"><b>Tester</b></td>
										<td style="display: none"><b>Tester Name</b></td>
										<td style="text-align: center"><b>Steps</b></td>
										<td style="text-align: center"><b>Bugs</b></td>
										<td style="text-align: center"><b>Application</b></td>
										<td style="text-align: center"><b>Module</b></td>
										<td style="text-align: center"><b>Scenario</b></td>
										<td style="text-align: center"><b>Attachments</b></td>
										<td style="display: none">Operation</td>
									</tr>
									<tr>
										<th style="display: none"></th>
										<th style="display: none"></th>
										<th style="display: none"></th>
										<th><span class="glyphicon glyphicon-search"
											style="vertical-align: middle"></span></th>
										<th><input type="text" id="searchTcId"
											class="form-control" style="width: 100%; text-align: center"
											placeholder="Id"></th>
										<th><input type="text" id="searchTcName"
											class="form-control" style="width: 100%; text-align: center"
											placeholder="Title"></th>
										<th><input type="text" id="searchSummary"
											class="form-control" style="width: 100%; text-align: center"
											placeholder="Summary"></th>
										<th id="filter_col5" data-column="4"><input type="text"
											id="col8_filter" class="column_filter"
											style="width: 100%; text-align: center" placeholder="Status">
											<input type="checkbox" class="column_filter" id="col8_regex"
											checked="checked" style="display: none"> <input
											type="checkbox" class="column_filter" id="col8_smart"
											checked="checked" style="display: none"> <input
											type="text" id="searchStatus" class="form-control"
											style="width: 100%; display: none; text-align: center"
											placeholder="Status"></th>
										<th style="display: none"></th>
										<th><input type="text" id="col10_filter"
											class="column_filter" style="width: 100%; text-align: center"
											placeholder="Tester"> <input type="checkbox"
											class="column_filter" id="col10_regex" checked="checked"
											style="display: none"> <input type="checkbox"
											class="column_filter" id="col10_smart" checked="checked"
											style="display: none"> <input type="text"
											id="testerStatus" class="form-control"
											style="width: 100%; display: none; text-align: center"
											placeholder="Tester"></th>
										<th style="display: none"></th>

										<th></th>
										<th><input type="text" id="searchBugs"
											class="form-control" style="width: 100%; text-align: center"
											placeholder="Bugs"></th>
										<th><input type="text" id="col13_filter"
											class="column_filter" style="width: 100%; text-align: center"
											placeholder="Application"> <input type="checkbox"
											class="column_filter" id="col13_regex" checked="checked"
											style="display: none"> <input type="checkbox"
											class="column_filter" id="col13_smart" checked="checked"
											style="display: none"> <input type="text"
											id="applicationName" class="form-control"
											style="width: 100%; display: none; text-align: center"
											placeholder="Application"></th>
										<th><input type="text" id="col14_filter"
											class="column_filter" style="width: 100%; text-align: center"
											placeholder="Module"> <input type="checkbox"
											class="column_filter" id="col14_regex" checked="checked"
											style="display: none"> <input type="checkbox"
											class="column_filter" id="col14_smart" checked="checked"
											style="display: none"> <input type="text"
											id="moduleName" class="form-control"
											style="width: 100%; display: none; text-align: center"
											placeholder="Module"></th>
										<th><input type="text" id="searchScenario"
											class="form-control" style="width: 100%; text-align: center"
											placeholder="Scenario"></th>
										<th></th>
										<th style="display: none"></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="projectsList" items="${Model.projectsList}">
										<c:forEach var="modulesList" items="${Model.modulesList}">
											<c:forEach var="scenarioList" items="${Model.scenarioList}">
												<c:forEach var="testcasesList"
													items="${Model.testcasesList}" varStatus="testCase">

													<c:if
														test="${projectsList.project_id==modulesList.project_id}">

														<c:if
															test="${modulesList.module_id==scenarioList.module_id}">
															<c:if
																test="${scenarioList.scenario_id==testcasesList.scenario_id}">
																<tr
																	id="scenario_${testcasesList.scenario_id}_testCase_${testcasesList.testcase_id}">

																	<td style="display: none"
																		id="tcid_${testcasesList.testcase_id}"
																		value="${testcasesList.testcase_id}" name="testcaseid">${testcasesList.testcase_id}</td>

																	<td style="display: none"><c:forEach
																			var="buildList" items="${Model.buildList}"
																			varStatus="loop">
																			<span id="buildid_${testcasesList.testcase_id}"
																				value="${buildList.build_id}">${buildList.build_id}</span>
																		</c:forEach></td>

																	<td style="display: none"><input type="hidden"
																		id="methodType_${testcasesList.testcase_id }"
																		value="insert"></td>

																	<td><input type="checkbox"
																		id="checkTc_${testcasesList.testcase_id}"
																		class="checkClass" style="cursor: pointer" /><br>
																		<span class="glyphicon glyphicon-paperclip"
																		style="cursor: pointer"
																		onclick="tcAttchFile(${testcasesList.testcase_id})"></span></td>

																	<td>${testcasesList.test_prefix}</td>

																	<td>${testcasesList.testcase_name}</td>

																	<td>${testcasesList.summary}</td>

																	<td><select class="form-control selcls"
																		id="tcStatusPut_${testcasesList.testcase_id}"
																		name="tcstatus"
																		onchange="changeTcStatus(${testcasesList.testcase_id})">
																			<c:forEach var="statusList"
																				items="${Model.statusList}" varStatus="loop">
																				<option value="${statusList.exst_id}">${statusList.description}</option>
																			</c:forEach>
																	</select></td>

																	<td style="display: none"
																		id="tcStatuses_${testcasesList.testcase_id}"
																		name="tcStatusResult"></td>

																	<%-- 																		<td><c:forEach var="assignedUser" items="${Model.assignedUser}"><span id="userid_${testcasesList.testcase_id}" value="${assignedUser.user_id }">${assignedUser.first_name}&nbsp;${assignedUser.last_name}</span><br></c:forEach></td> --%>
																	<td><select class="form-control selcls"
																		id="userid_${testcasesList.testcase_id}"
																		name="testeruser"
																		onchange="changeTester(${testcasesList.testcase_id})">
																			<c:forEach var="assignedUser"
																				items="${Model.assignedUser}">
																				<c:if
																					test="${assignedUser.project_id == projectsList.project_id}">
																					<option value="${assignedUser.user_id }">
																								${assignedUser.first_name}&nbsp;${assignedUser.last_name}
																						</option>
																				</c:if>
																				<br>
																			</c:forEach>
																	</select></td>

																	<td style="display: none"
																		id="testerNames_${testcasesList.testcase_id}"></td>

																	<td><button class="btn btn-info"
																			style="cursor: pointer;"
																			onclick="testCaseTableClick1(${testcasesList.testcase_id},${testcasesList.scenario_id},${projectsList.project_id},${modulesList.module_id},this)">Run</button></td>

																	<td><ul
																			style="padding: 0; list-style-position: inside;">
																			<c:forEach var="step_bug" items="${Model.step_bug}">
																				<c:if
																					test="${step_bug.test_case_id == testcasesList.testcase_id}">
																					<li><a
																						href="bugsummarybyprefix?btPrefix=${step_bug.bug_prefix}"
																						target="_blank">Bug # ${step_bug.bug_prefix}</a><br></li>
																				</c:if>
																			</c:forEach>
																		</ul>
																		<div class="form-group col-lg-12">
																			<span id="bugTable_${testcasesList.testcase_id}"
																				class="pull-left"></span>
																		</div></td>

																	<td id="projectid_${testcasesList.testcase_id}"
																		value="${projectsList.project_id}">${projectsList.project_name}</td>

																	<td id="moduleid_${testcasesList.testcase_id}"
																		value="${modulesList.module_id}">${modulesList.module_name}</td>

																	<td id="scenarioid_${testcasesList.testcase_id}"
																		value="${scenarioList.scenario_id}">${scenarioList.scenario_name}</td>

																	<td>

																		<div class="form-group col-lg-12">
																			<span id="tcFileTable_${testcasesList.testcase_id}"
																				class="pull-left"></span>
																		</div> <c:forEach var="attachmentsList"
																			items="${Model.attachmentsList}">
																			<c:if
																				test="${attachmentsList.testcase_id == testcasesList.testcase_id}">

																				<span class="glyphicon glyphicon-download-alt"
																					onclick="downloadFile(${attachmentsList.attach_id},'${attachmentsList.attachment_title}')"
																					style="cursor: pointer">&nbsp;${attachmentsList.attachment_title}</span>

																			</c:if>
																		</c:forEach>
																	</td>
																	<td style="display: none"
																		id="tcOp_${testcasesList.testcase_id}"
																		name="tcOperation">insert</td> 
																</tr>
															</c:if>
														</c:if>
													</c:if>

												</c:forEach>
											</c:forEach>
										</c:forEach>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<c:forEach var="testcasesList" items="${Model.testcasesList}">
		<div id="testcaseInfoDiv_${testcasesList.testcase_id }"
			style="display: none;">
			<div class="row">
				<div class="ibox m-b-sm">
					<div class="ibox collapsed">
						<div class="ibox-title">
							<div class="ibox-tools">
								<!-- <a class="collapse-link"> <b><i
											class="fa fa-chevron-right pull-left"></i></b>
									</a>-->
								<h5>${testcasesList.test_prefix }-
									${testcasesList.testcase_name }</h5>
							</div>
						</div>
						<%-- <div class="ibox-content">
								<form action="#"
									class="wizard-big wizard clearfix form-horizontal">
									<div class="content clearfix">
										<fieldset class="body current" disabled>
											<div class="row">
												<div class="col-lg-10">
													<div class="form-group">
														<label class="col-sm-2 control-label"> Summary:</label>
														<div class="col-sm-10">
															<!-- <textarea class="form-control" style="max-width: 100%;"></textarea> -->
															<textarea name="scenarioDescription" rows="10"
																class="form-control summernote"
																style="resize: none; border: 1px solid #ccc;">${testcasesList.summary } </textarea>
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-2 control-label">
															Precondition:</label>
														<div class="col-sm-10">
															<!-- <textarea class="form-control" style="max-width: 100%;"></textarea> -->
															<textarea name="scenarioDescription" rows="10"
																class="form-control summernote"
																style="resize: none; border: 1px solid #ccc;">${testcasesList.precondition } </textarea>
														</div>
													</div>

												</div>
											</div>


										</fieldset>
									</div>
								</form>
								<!-- end form -->
							</div>--%>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Test Case Steps</h5>
						</div>

						<div class="ibox-content">

							<div class="table-responsive" style="overflow-x: visible;">
								<table
									class="table table-striped table-bordered table-hover dataTables-example"
									id="testcaseStepListTable_${testcasesList.testcase_id }">
									<thead>
										<tr>
											<td class="hidden">Step Id</td>
											<td style="width: 50px;!important"><b>Step Id</b></td>

											<td><b>Steps</b></td>
											<td><b>Expected Result</b></td>
											<td style="width: 124px;!important"><b>Step Status</b></td>
											<!-- <td style="width: 200px;!important"><b>Comment</b></td>-->
											<td style="width: 200px;!important"><b>Actual Result</b></td>
										</tr>
									</thead>
									<tbody>



									</tbody>
								</table>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-2">

										<!-- <button class="btn btn-danger" type="submit">Remove</button> -->
									</div>
								</div>
							</div>

						</div>
					</div>


				</div>
			</div>


			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Enter execution summary</h5>
						</div>

						<div class="ibox-content">

							<div class="content clearfix">
								<div class="row">
									<form id="testcaseSummaryForm_${testcasesList.testcase_id }"
										action="">
										<input type="hidden" name="tcId"
											value="${testcasesList.testcase_id }"> <input
											type="hidden" id="insertMethod_${testcasesList.testcase_id }"
											value="insert">
										<div class="form-group">
											<div class="col-sm-6">
												<div class="col-sm-3">
													<!-- <label for="comment">Notes :</label> -->
													<label for="comment">Comments :</label>
												</div>
												<div class="col-sm-9">
													<textarea id="notes" name="notes" class="form-control"
														rows="5" style="resize: none;"></textarea>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="col-sm-3" style="display: none;">
													<label for="comment">Test case Status</label>
												</div>
												<div class="col-sm-9" style="display: none;">
													<c:forEach var="statusList" items="${Model.statusList}"
														varStatus="loop">
														<div class="radio" style="padding-bottom: 1%;">

															<c:if test="${loop.first}">
																<input type="radio" name="tcStatus" class="nrStatus"
																	id="nrid" value="${statusList.exst_id }"
																	aria-label="Single radio Two" checked>
															</c:if>

															<c:if test="${!loop.first}">
																<input type="radio" name="tcStatus" class="status"
																	value="${statusList.exst_id }"
																	aria-label="Single radio Two">
															</c:if>


															<label name="statusName">${statusList.description}</label>

														</div>


													</c:forEach>


												</div>
											</div>
										</div>
									</form>
								</div>
							</div>


							<br>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-12">
										<button type="button" data-style="slide-up"
											onclick="saveExecClicked(${testcasesList.testcase_id },'2')"
											class="btn btn-primary ladda-button ladda-button-demo"
											style="margin-right: 25px;">Save Execution</button>
										<button type="button"
											onclick="tcAttchFile(${testcasesList.testcase_id })"
											class="btn btn-success ladda-button-demo ladda-button"
											data-style="slide-up">Attach File</button>
										<button type="button" data-style="slide-up"
											onclick="saveExecClicked(${testcasesList.testcase_id },'3')"
											class="btn btn-primary ladda-button ladda-button-demo"
											style="margin-right: 15px; visibility: hidden;">Save
											& Move to Next</button>
										<!--<button type="button" data-style="slide-up" id="btnMark"
												onclick="saveExecClicked(${testcasesList.testcase_id },'1')"
												class="btn btn-success ladda-button ladda-button-demo"
												style="margin-right: 15px;">Mark Complete</button> -->



									</div>
								</div>
								<div class="row" style="margin-top: 10px;">
									<div class="col-lg-12">
										<form:form action="tcattachonbuildexe" class="form-horizontal"
											modelAttribute="uploadForm" enctype="multipart/form-data"
											method="POST"
											id="tcattachonbuildexeForm_${testcasesList.testcase_id}">
											<div class="form-group col-lg-12">
												<table id="tcFileTable_${testcasesList.testcase_id}"
													class="pull-left"></table>
											</div>
										</form:form>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-4">

										<table class="table">
											<tbody>
												<c:forEach var="attachmentsList"
													items="${Model.attachmentsList}">
													<c:if
														test="${attachmentsList.testcase_id == testcasesList.testcase_id}">
														<tr>
															<td>${attachmentsList.attachment_title}</td>
															<td><label
																onclick="downloadFile(${attachmentsList.attach_id},'${attachmentsList.attachment_title}')"
																class="btn btn-xs btn-white">Download</label></td>
														</tr>
													</c:if>
												</c:forEach>
											</tbody>
										</table>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:forEach>

</div>

<div class="modal inmodal" id="createBug" tabindex="-1" role="dialog"
	aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-md">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<h6 class="modal-title pull-left">Create a New Bug</h6>
				<button type="button" class="close pull-right" data-dismiss="modal">
					<span aria-hidden="false">x</span><span class="sr-only">Close</span>
				</button>
			</div>

			<div class="modal-body">
				<div class="row">
					<label class="col-lg-8 pull-right text-right">* Step will
						not be editable once bug is created</label>
				</div>
				<form:form action="raisebug"
					class="wizard-big wizard clearfix form-horizontal"
					modelAttribute="uploadForm" enctype="multipart/form-data"
					method="POST" id="createBugForm">
					<div class="form-group">
						<label class="control-label col-lg-2">Summary*:</label>
						<div class="col-lg-8">
							<input type="text" placeholder="Summary"
								class="form-control characters" required aria-required="true"
								name="bugSummary">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-2">Description:</label>
						<div class="col-lg-8">
							<textarea name="bugDescription" id="bugDescription"
								class="form-control summernote" rows="3"
								style="max-width: 100%; max-height: 100px;"
								placeholder="Comment"></textarea>
						</div>
					</div>
					<input type="text" class="form-control hidden" name="testcaseId">
					<input type="text" class="form-control hidden"
						name="testcaseStatusId">
					<input type="text" class="form-control hidden" name="testcaseNotes">
					<input type="text" class="form-control hidden" name="testStepId">
					<input type="text" class="form-control hidden" name="stepStatusId">
					<input type="text" class="form-control hidden"
						name="testStepComment">
					<input type="text" class="form-control hidden" name="projectId">
					<input type="text" class="form-control hidden" name="moduleId">
					<input type="text" class="form-control hidden"
						name="stepInsertMethod" value="insert">
					<input type="text" class="form-control hidden"
						name="testCaseInsertMethod" value="insert">
					<div class="form-group">
						<div class="col-lg-9 col-lg-offset-2">
							<input id="addFile" type="button" class="btn btn-success"
								value="Choose File" />
							<table id="fileTable">
							</table>
						</div>
					</div>
				</form:form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white pull-left"
					data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-success pull-left"
					id="btnSubmitBug">Submit</button>
			</div>
		</div>
	</div>
</div>


<!-- Create New Bug for test case level -->
<div class="modal inmodal" id="createBugTC" tabindex="-1" role="dialog"
	aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-md">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<h6 class="modal-title pull-left">Create a New Bug</h6>
				<button type="button" class="close pull-right" data-dismiss="modal">
					<span aria-hidden="false">x</span><span class="sr-only">Close</span>
				</button>
			</div>

			<div class="modal-body">
				<div class="wizard-big wizard clearfix form-horizontal">
					<div class="form-group">
						<label class="control-label col-lg-2">Summary*:</label>
						<div class="col-lg-8">
							<input type="text" placeholder="Summary"
								class="form-control characters" required aria-required="true"
								name="bugSummaryTC">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-2">Description:</label>
						<div class="col-lg-8">
							<textarea name="bugDescriptionTC" id="bugDescriptionTC"
								class="form-control summernote" rows="3"
								style="max-width: 100%; max-height: 100px;"
								placeholder="Comment"></textarea>
						</div>
					</div>
					<input type="text" class="form-control hidden" name="testcaseIdTC">
					<input type="text" class="form-control hidden"
						name="testcaseStatusIdTC"> <input type="text"
						class="form-control hidden" name="testcaseNotesTC"> <input
						type="text" class="form-control hidden" name="testStepIdTC">
					<input type="text" class="form-control hidden"
						name="stepStatusIdTC"> <input type="text"
						class="form-control hidden" name="testStepCommentTC"> <input
						type="text" class="form-control hidden" name="projectIdTC">
					<input type="text" class="form-control hidden" name="moduleIdTC">
					<input type="text" class="form-control hidden"
						name="stepInsertMethodTC" value="insert"> <input
						type="text" class="form-control hidden"
						name="testCaseInsertMethodTC" value="insert">
					<div class="form-group">
						<div class="col-lg-9 col-lg-offset-2">
							<input id="addFileTC" type="button" class="btn btn-success"
								value="Choose File" />
							<table id="fileTableTC">
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white pull-left"
					data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-success pull-left"
					id="btnSubmitBugTC">Submit</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="columnModal" tabindex="-1" role="dialog"
	aria-labelledby="columnModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">Select checkbox to show column</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4 mt-10">
						<input type="checkbox" class="togg" id="col_1"
							name="col_Test_Case_Id" value="4" disabled="disabled" checked>&nbsp;&nbsp;Id
					</div>
					<div class="col-md-4 mt-10">
						<input type="checkbox" class="togg" id="col_2"
							name="col_Test_Case_Name" value="5" disabled="disabled" checked>&nbsp;&nbsp;Title
					</div>
					<div class="col-md-4 mt-10">
						<input type="checkbox" class="togg" id="col_3" name="col_Summary"
							value="6" disabled="disabled" checked>&nbsp;&nbsp;Summary
					</div>
					<div class="col-md-4 mt-10">
						<input type="checkbox" class="togg" id="col_4" name="col_Status"
							value="7" disabled="disabled" checked>&nbsp;&nbsp;Status
					</div>
					<div class="col-md-4 mt-10">
						<input type="checkbox" class="togg" id="col_5" name="col_Tester"
							value="9" disabled="disabled" checked>&nbsp;&nbsp;Tester
					</div>
					<div class="col-md-4 mt-10">
						<input type="checkbox" class="togg" id="col_6" name="col_Steps"
							value="11" disabled="disabled" checked>&nbsp;&nbsp;Steps
					</div>
					<div class="col-md-4 mt-10">
						<input type="checkbox" class="togg" id="col_7"
							name="col_Bug_Raised" value="12" disabled="disabled" checked>&nbsp;&nbsp;Bugs
					</div>
					<div class="col-md-4 mt-10">
						<input type="checkbox" class="togg" id="col_8"
							name="col_Application" value="13" disabled="disabled" checked>&nbsp;&nbsp;Application
					</div>
					<div class="col-md-4 mt-10">
						<input type="checkbox" class="togg" id="col_9" name="col_Module"
							value="14" disabled="disabled" checked>&nbsp;&nbsp;Module
					</div>
					<div class="col-md-4 mt-10">
						<input type="checkbox" class="togg" id="col_10"
							name="col_Scenario" value="15">&nbsp;&nbsp;Scenario
					</div>
					<div class="col-md-4 mt-10">
						<input type="checkbox" class="togg" id="col_11"
							name="col_Attachment" value="16">&nbsp;&nbsp;Attachment
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	//$('#tblTC').DataTable();
	//$('.dataTables_length').addClass('bs-select');
	
	 var foo = $('.footable').footable();
	 foo.trigger('footable_initialize'); //Reinitialize
	 foo.trigger('footable_redraw'); //Redraw the table
	 foo.trigger('footable_resize'); //Resize the table
	 
});
  $(function () {
	  
	    $('.summernote').summernote('disable');
	    $('.note-editor').css('background','white');
        $('button[ data-event="codeview"]').remove();
        $('.note-editor').css('border','1px solid #CBD5DD');
       // $('.note-editor').css('margin-left','10px');
        
        $('div.note-insert').remove();
        $('div.note-table').remove();
        $('div.note-help').remove();
        $('div.note-style').remove();
        $('div.note-color').remove();
        $('button[ data-event="removeFormat"]').remove();
        $('button[ data-event="insertUnorderedList"]').remove();
        $('button[ data-event="fullscreen"]').remove();
        $('button[ data-original-title="Line Height"]').remove();
        $('button[ data-original-title="Font Family"]').remove();
        $('button[ data-original-title="Paragraph"]').remove();
        
	  
	  
       $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
        
       	  var value = $('#hiddenSweetAlertStatus').val();
          if(value == 1){
        	  swal("Success", "Execution saved successfully", "success");
          }
          $('#hiddenSweetAlertStatus').val('99');
		
          $('#buildLearningForm').validate({
              rules : {
                   buildLearning : {
                        required : true,
                        minlength : 1,
                   }
              }
         });
          
          $('#createBugForm').validate({
              rules : {
            	  bugSummary : {
                        required : true,
                        minlength : 1,
                        maxlength : 250
                   }
              }
         });
          
         
         
  });  
</script>

<!-- Manual Execution MarkComplete issue fix -->
<script>
	$(document).ready(function(){
		
		//var duplicateBuild=${sessionScope.duplicateBuildError};
		var duplicateBuild=$("#buildExist").val();   
        if(duplicateBuild=="1"){
   	    $.ajax({        
   	 	    url: 'setDuplicateBuildSession',
   	 	    type: 'POST',
   	 	    success: function (resp) {  
   	 	    	if(resp=="Success"){
   	 	    		 toastr.options = {
   	 	   				 "closeButton": true,
   	 	   				  "debug": true,
   	 	   				  "progressBar": true,
   	 	   				  "preventDuplicates": true,
   	 	   				  "positionClass": "toast-top-right",
   	 	   				  "showDuration": "400",
   	 	   				  "hideDuration": "1000",
   	 	   				  "timeOut": "1500",
   	 	   				  "extendedTimeOut": "1000",
   	 	   				  "showEasing": "swing",
   	 	   				  "hideEasing": "linear",
   	 	   				  "showMethod": "slideDown",
   	 	   				  "hideMethod": "slideUp"
   	 	   	     };   
   	 	   	     toastr.error('',"Duplicate Test Set!"); 
   	 	    	}
   	 	    }
   	     });
   	      
        }    
		
       
	});
</script>


<script>
var moduleClickId;
var sceClickId;
var testClickId;
          $(document).ready(function() {
              var activeProject = sessionStorage.getItem("ActiveProjectId");
              var activeModule= sessionStorage.getItem("ActiveModuleId");
              var activeScenario= sessionStorage.getItem("ActiveScenarioId");
              var activeTestCaseId= sessionStorage.getItem("ActiveTestCaseId");
              var nextTestCaseId= sessionStorage.getItem("nextTestCaseId");
              var moveNextFlag= sessionStorage.getItem("saveAndMoveNextFlag");
              
              var currentTestcaseId=0;
              
              if(activeProject!=0)
                   {
                        $("#projectMenu_"+activeProject).addClass("active");
                        $("#projectUl_"+activeProject).addClass("in");
                        projectClick(activeProject);
                        if(activeModule!=0)
                        {
                             $("#moduleMenu_"+activeModule).addClass("active");
                             $("#moduleUl_"+activeModule).addClass("in");
                             moduleClick(activeModule);
                             
                             if(activeScenario!=0)
                             {
                                  scenarioClick(activeScenario);
                                  if(nextTestCaseId!=0 && moveNextFlag == 1)
                                  {
                                      element = document.getElementById("scenario_"+activeScenario+"_testCase_"+nextTestCaseId);
                                     testCaseTableClick(nextTestCaseId,activeScenario,element);
                                     
                                  }
                                  if(activeTestCaseId!=0 && moveNextFlag == 0)
                                  {
                                         element = document.getElementById("scenario_"+activeScenario+"_testCase_"+activeTestCaseId);
                                         testCaseTableClick(activeTestCaseId,activeScenario,element);
                                  } 
                             } 
                        }
                   }
              
            
              $(":radio[value=1]").parent().addClass("radio-success");
              $(":radio[value=2]").parent().addClass("radio-danger");
              $(":radio[value=3]").parent().addClass("radio-info");
             
              var tcExecDetailsList=[];
              tcExecDetailsList=${Model.tcExecDetailsList};
              
              for(var i=0;i<tcExecDetailsList.length;i++)
                   {
            	  
                   $("#testcaseSummaryForm_"+tcExecDetailsList[i].tc_id).find('textarea[name="notes"]').text(tcExecDetailsList[i].notes);
                   $("#testcaseSummaryForm_"+tcExecDetailsList[i].tc_id).find(':radio[value='+tcExecDetailsList[i].status_id+']').attr('checked',true);
                   
                   if(tcExecDetailsList[i].tc_id==$("#testcaseSummaryForm_"+tcExecDetailsList[i].tc_id).find('input[name="tcId"]').val()){
                        $('#insertMethod_'+tcExecDetailsList[i].tc_id).val("update");
                        
                        if(tcExecDetailsList[i].complete_status == 1)
                        {
                        	
                            if(tcExecDetailsList[i].description != "Not run")
                            	{
                            	 $("#testcaseInfoDiv_"+tcExecDetailsList[i].tc_id).find("input").attr("disabled",true);
                                 $("#testcaseInfoDiv_"+tcExecDetailsList[i].tc_id).find("select").attr("disabled",true);
                                 $("#testcaseInfoDiv_"+tcExecDetailsList[i].tc_id).find("textarea").attr("disabled",true);
                                 $("#testcaseInfoDiv_"+tcExecDetailsList[i].tc_id).find(":radio").attr("disabled",true);
                                 $("#testcaseInfoDiv_"+tcExecDetailsList[i].tc_id).find("button").attr("disabled",true);
                            	}
                          
                         }
                        
                        
                   }
                   
                   }

            
              $('#addFile').click(function() {
                   var fileIndex = $('#fileTable tr').children().length;
                   if(fileIndex>0){
                        lastIndex = $('#fileTable tr:last').attr("id").length;
                        fileIndex = parseInt($('#fileTable tr:last').attr("id").substring(9,lastIndex)) + parseInt(1);
                   }
                   $('#fileTable').append(
                             '<tr id="tableRow_'+ fileIndex +'"><td>'+
                             '    <input type="file"  accept=".png,.jpg,.pdf,.docx" class="file hidden" id="file_'+ fileIndex +'" />'+
                             '<label class="fileName" id="fileName_'+ fileIndex +'"></label>'+
                             '<a id="close_'+ fileIndex +'" class="close hidden"  style="float: none">x</a>'+
                             '<label class="error hidden" id="uploadSizeError_'+ fileIndex +'">Please upload less than 1 MB</label>'+
                             '<label class="error hidden" id="uploadTypeError_'+ fileIndex +'">Valid only (png, jpg, pdf, docx)</label>'+
                             '</td></tr>');
                   
                   $("#file_"+fileIndex).click();
                   $("#file_"+fileIndex).change(function(){
                        if(this.files[0] ==null || this.files[0] == undefined)
                             {
                             $("#fileName_"+fileIndex).html("");
                             $('#tableRow_'+ fileIndex ).remove();
                             }
                        else
                             {
                             $("#fileName_"+fileIndex).html(this.files[0].name);
                             $("#close_"+fileIndex).removeClass('hidden');
                             $('.close').each(function()
                             {
                                  if($(this).hasClass('hidden'))
                                      {
                                      lastIndex = $(this).attr('id').length;
                                      id = $(this).attr('id').substring(5,lastIndex);
                                     $('#tableRow'+ id ).remove();
                                      }
                             }); 
                             } 
                   });
                        
                   $(".close").click(function(){
                        lastIndex = $(this).attr('id').length;
                        id =  $(this).attr('id').substring(6,lastIndex);
                        $('#tableRow_'+ id ).remove();
                  });
                   
              });
                
              
              
              //dumping non executed test cases
              /*var execData=new Array();
      		
       		 var testcasesListing= ${Model.testcasesListing};
       		 var buildListing=${Model.buildListing};
       		 var build_Id=buildListing[0].build_id;
       		 var tcExecDetailsList=[];
            	 tcExecDetailsList=${Model.tcExecDetailsList};
            	 var user=${Model.user_Id};
            	 var count=0;
            	 var index;
       		 for(var i = 0, len = tcExecDetailsList.length; i < len; i++){
       			for (var j = 0, len2 = testcasesListing.length; j < len2; j++) {
       				if (tcExecDetailsList[i].tc_id === testcasesListing[j].testcase_id) {
       					testcasesListing.splice(j, 1);
       	                len2=testcasesListing.length;
       	            }
       			}
       		  }//end for loop
       		  for(var k=0;k<testcasesListing.length;k++){
       			var tcId=testcasesListing[k].testcase_id+"";
      				var buildId=build_Id+"";
      				var projectId=testcasesListing[k].projectid+"";
      				var moduleId=testcasesListing[k].module_id+"";
      				var scenarioId=testcasesListing[k].scenario_id+"";
      				var testerId=user+"";
      				var statusId="5";
      				var notes="";
      				var completeStatus="2";
      				//var typeMethod=$("#tcOp_"+testcasesListing[i].testcase_id+"").text();
      				var typeMethod="insert";
      				
      				execData.push({tcId, buildId, projectId, moduleId, scenarioId, testerId, statusId, notes, completeStatus, typeMethod});
       		  }
       		 var str=JSON.stringify(execData);
       		  var res=$.ajax({
       			 type:"POST",
       			 url:"dump",
       			 dataType:"json",
       			 data:{execData:str}
       		  });*/
              
          });
          
          function projectClick(clickedId) {
              sessionStorage.setItem("ActiveProjectId",clickedId);
              sessionStorage.setItem("ActiveModuleId",0);
              projectTable(clickedId);
              moduleClickId=clickedId;
              
          }

		function moduleClick(clickedId) {
			sessionStorage.setItem("ActiveModuleId",clickedId);
			sessionStorage.setItem("ActiveScenarioId",0);
			
			moduleTable(clickedId);
			sceClickId=clickedId;
			
		}
		function moduleBuildClick(moduleId,projectId) {
			sessionStorage.setItem("ActiveModuleId",moduleId);
			sessionStorage.setItem("ActiveProjectId",projectId);
			sessionStorage.setItem("ActiveScenarioId",0);
			localStorage.setItem("currentProjectId",projectId);
			moduleTable(moduleId);
			sceClickId=moduleId;
			
		}


          function scenarioClick(clickedId) {
              sessionStorage.setItem("ActiveScenarioId",clickedId);
              sessionStorage.setItem("ActiveTestCaseId",0);
              sessionStorage.setItem("nextTestCaseId",0);
             // scenarioTable(clickedId);
              testClickId=clickedId;
              
          }
          function excuteBuildProjects() {
              sessionStorage.setItem("ActiveProjectId",0);
          }
          
          
          function testCaseTableClick1(clickedId,scenarioId,projectId,moduleId,element)
          {
        	  $("#TCase").hide();
        	  localStorage.setItem("currentProjectId",projectId);
        	  localStorage.setItem("currentModuleId",moduleId);
        	  localStorage.setItem("currentTestcaseId",clickedId);
        	  var tempval=$("#tcStatusHidden").val();
        	  
        	  //code for disable mark complete
        	  var flag=0;
            	var ch=$('input[name=tcStatus]:checked', "#testcaseSummaryForm_"+clickedId).val();
            	if(ch==5){
            	
            		$("button[id=btnMark]").attr("disabled",true);
            		}else{
            			$("button[id=btnMark]").attr("disabled",false);
          
            		}
            	 if(tempval==''){
           		  tempval=  ch;

           	  }
               $('input[name=tcStatus]').click(function(){
            	   clickedValue=$(this).val();
            	   if(clickedValue==5){
            		   $("button[id=btnMark]").attr("disabled",true);
            	   }else{
            		   $("button[id=btnMark]").attr("disabled",false);
            	   }
               });
               
        	  
        	  /* 
        	  
              table = $(element).closest('table').attr('id');
              flag = 0;
              $("#"+table +" tbody tr").each(function () {
                   testCaseId = $(this).children('td').first().html();
                   status = $("#complStatus_sce_"+scenarioId+"_tc_"+testCaseId).text();
                   if(status !=1)
                        {
                        flag = 1;
                        }
              });
              var nextTestCaseId =""; 
              if(flag == 1){
              if($(element).is(':last-child'))
                   {
                   tableId = $(element).closest('table').attr('id');
                   nextTestCaseId = $("#"+tableId+" tbody tr:first td:first").html();
                   }
              else
                   {
                   nextTestCaseId = $(element).next('tr').children('td').first().html();
                   }
              
              completeStatus = $("#complStatus_sce_"+scenarioId+"_tc_"+nextTestCaseId).text();
              while (completeStatus != 0) {
                 ele = document.getElementById("scenario_"+scenarioId+"_testCase_"+nextTestCaseId);
                 if($(ele).is(':last-child'))
                   {
                   tableId = $(element).closest('table').attr('id');
                   nextTestCaseId = $("#"+tableId+" tbody tr:first td:first").html();
                   }
                 else
                      {
                      nextTestCaseId = $(ele).next('tr').children('td').first().html();
                      }
                 completeStatus = $("#complStatus_sce_"+scenarioId+"_tc_"+nextTestCaseId).text();
              } 
              } */
               
             var testcasesStepList1=[];
              testcasesStepList1=${Model.testcasesStepList1};
              var statusList1=[];
              statusList1=${Model.statusList1};
              var tdata="";var statusList='';
              //alert(testcasesStepList1);
              var flag=0;
              var tbody = $("#testcaseStepListTable_"+clickedId+" tbody");
              for(var stindex=0;stindex<statusList1.length;stindex++){
    			  statusList+="<option value='"+statusList1[stindex].exst_id+"'>"+statusList1[stindex].description+"</option>";
				}
              if (tbody.children().length == 0) {
	              for(var i=0;i<testcasesStepList1.length;i++){
	            	  if(testcasesStepList1[i].testcase_id==clickedId){
	            		  //console.log(testcasesStepList1[i].step_id+" "+testcasesStepList1[i].test_step_id+" "+testcasesStepList1[i].action+" "+testcasesStepList1[i].result);
	            		  
	            		  
	            		  var tbody = "<tr>"+"<form name='stepSummaryForm_"+testcasesStepList1[i].step_id+"'"+
								" id='stepSummaryForm_"+testcasesStepList1[i].step_id+"'"+
									" action='insertStepExecSummary' method='post'"+
									" enctype='multipart/form-data'>"+
									"<input type='hidden'"+
										" id='stepId_"+testcasesStepList1[i].step_id+"'"+
										" value='"+testcasesStepList1[i].step_id+"'"+
										" name='stepId'>"+
									"<input type='hidden'"+
										" id='insertTestStep_"+testcasesStepList1[i].step_id+"'"+
										" value='insert'>"+
	            		  				
	            		  				"<td class='hidden'>"+testcasesStepList1[i].step_id+"</td>"+
	            		  					"<td name='test_step_Id'>"+testcasesStepList1[i].test_step_id+"</td>"+
	            		  					"<td>"+testcasesStepList1[i].action+"</td>"+
	            		  					"<td>"+testcasesStepList1[i].result+"</td>"+
	            		  					"<td><select"+
											" id='stepStatus_"+testcasesStepList1[i].step_id+"'"+
												" name='stepStatus' class='form-control m-b'"+
												" style='margin-bottom: 0px;'>"+
												statusList+
											"</select></td>"+
											"<td><input name='stepComment'"+
												" onkeydown='if (event.keyCode == 13) return false;'"+
												" bug='2' id='stepComment_"+testcasesStepList1[i].step_id+"'"+
												" type='text'>"+
												"<div id='divBug_"+testcasesStepList1[i].step_id+"'></div></td>"+
	            		  					
	            		  			"</form></tr>";
	            		  tdata+=tbody;
	            		  flag=1
	            	  }
            		  //console.log(testcasesStepList1[i].step_id);
	              }
	              document.getElementById("testcaseStepListTable_"+clickedId).tBodies[0].innerHTML= tdata;
	              var tcStepStatus=[];
	               tcStepStatus=${Model.tcStepStatus};
	               
	               for(var k=0;k<tcStepStatus.length;k++){
	                         if(tcStepStatus[k].bug_raised == 1)
	                         {
	                         $("#stepStatus_"+tcStepStatus[k].step_id).attr("disabled",true);
	                         $("#stepComment_"+tcStepStatus[k].step_id).attr("disabled",true);
	                         var executeBugs=[];
	                         executeBugs=${Model.executeBugs};
	                         for(var j=0;j<executeBugs.length;j++){
	                              if(tcStepStatus[k].step_id == executeBugs[j].test_step_id){
	                             $("#divBug_"+tcStepStatus[k].step_id).append("<a href='bugsummarybyprefix?btPrefix="+executeBugs[j].bug_prefix+"' target='_blank'> Bug #"+executeBugs[j].bug_prefix+"</a>");
	                              }
	                         }
	                         }
	                         $("#insertTestStep_"+tcStepStatus[k].step_id).val("update");
	                    $("#stepComment_"+tcStepStatus[k].step_id).attr("bug",tcStepStatus[k].bug_raised);
	                         $("#stepStatus_"+tcStepStatus[k].step_id+" option[value='"+tcStepStatus[k].status+"']").attr("selected", "selected");
	                         $("#stepComment_"+tcStepStatus[k].step_id).val(tcStepStatus[k].comments);
	                        
	                         
	                    }
	              
              }
              
              $("select[name=stepStatus]").on('change',function()
            	        {
            	   $("input[name='testStepId']").val("");
            	         lastIndex = $(this).attr('id').length;
            	             id = $(this).attr('id').substring(11,lastIndex);
            	        if($(this).val()==2){
            	             $("input[name='testStepId']").val(id);
            	         //$('#statusFail_'+ id ).removeClass('hidden');
            	         btStatus = ${Model.bugAccess};
            	         if(btStatus == 1){
            	         	
            	             $('#createBug').modal('show'); 
            	         }
            	        }
            	        });

            	$('#btnSubmitBug').on('click',function() {
            		var formStat = $("#createBugForm").valid();
            		 
            	    var counter = 0;
            	    var statusUpload = 1;
            	       $('.file').each(function(){
            	            $(this).attr( "name","files["+ counter +"]");
            	            lastIndex = $(this).attr('id').length;
            	            id=$(this).attr('id').substring(5,lastIndex);
            	            var fsize = 0;
            	              var filetype = 1;
            	              if($(this)[0].files[0]){
            	                   file = $(this)[0].files[0];
            	                fsize = file.size;
            	                var fileName = file.name;
            	              var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
            	              if(!(fileNameExt.toLowerCase() == "jpg" || fileNameExt.toLowerCase() == "png" || fileNameExt.toLowerCase() == "docx" || fileNameExt.toLowerCase() == "pdf"))
            	                {
            	                filetype = 0;
            	                statusUpload = 0;
            	                }
            	              $(this).attr( "name","files["+ counter +"]");
            	                   counter++;
            	              }
            	              else
            	                   {
            	                   $(this).attr('name','');
            	                   }
            	              if(filetype == 0){
            	                   statusUpload = 0;
            	                   $("#uploadTypeError_"+id).css("display","");
            	                   $("#uploadTypeError_"+id).text("Please upload file with one of type (png, jpg, pdf, docx)");
            	                   $("#uploadTypeError_"+id).removeClass('hidden');
            	              }
            	              else if(fsize> 1048576)
            	                   {
            	                   statusUpload = 0;
            	                   $("#uploadSizeError_"+id).css("display","");
            	                   $("#uploadSizeError_"+id).text("Please upload file with size less than 1MB");
            	                   $("#uploadSizeError_"+id).removeClass('hidden');
            	                   }
            	         });
            	    
            	       $("input[name='projectId']").val(localStorage.getItem("currentProjectId"));
            	       //alert(localStorage.getItem("currentProjectId"));
            	       $("input[name='moduleId']").val(localStorage.getItem("currentModuleId"));
            	       StepId = $("input[name='testStepId']").val();
            	       $("input[name='stepStatusId']").val($("#stepStatus_"+StepId).val());
            	       $("input[name='testStepComment']").val($("#stepComment_"+StepId).val());
            	      
            	      var currentTestcaseId=localStorage.getItem("currentTestcaseId");
            	       $("input[name='testcaseId']").val(currentTestcaseId);
            	       var tcStatus=$('input[name=tcStatus]:checked', "#testcaseSummaryForm_"+currentTestcaseId).val();
            	       $("input[name='testcaseStatusId']").val(tcStatus);
            	       var notes=$("#testcaseSummaryForm_"+currentTestcaseId).find("textarea[name='notes']").val();
            	       $("input[name='testcaseNotes']").val(notes);
            	            if($('#insertMethod_'+currentTestcaseId).val()=="update"){
            	                 $("input[name='testCaseInsertMethod']").val("update");
            	                 }
            	            if($('#insertTestStep_'+StepId).val()=="update"){
            	              $("input[name='stepInsertMethod']").val("update");
            	            }
            	           if(statusUpload == 1 && formStat){
            	           // if(statusUpload == 1){ 
            	             $('#createBug').modal('hide');
            	                 
            	            $('body').addClass("white-bg");
            	            $("#barInMenu").removeClass("hidden");
            	            $("#wrapper").addClass("hidden");
            	       
            	            $('#createBugForm').submit();
            	           
            	           
            	            }
            	});
              
              
              
              
              $('div[id^="projectInfoDiv_"]').fadeOut("fast");
              $('div[id^="moduleInfoDiv_"]').fadeOut("fast");
              $('div[id^="scenarioInfoDiv_"]').fadeOut("fast");
              $('div[id^="testcaseInfoDiv_"]').fadeOut("fast");
              $('table').removeClass("statusTable");
              $("#testcaseInfoDiv_" + clickedId).fadeIn("fast");
              $('#testcaseInfoDiv_' + clickedId).addClass("statusTable");
          }

          function projectTable(clickedId) {
              moduleClickId=clickedId;
              sessionStorage.setItem("ActiveModuleId",0);
              sessionStorage.setItem("ActiveScenarioId",0);
              
              $('div[id^=testcaseInfoDiv_').fadeOut("fast");
              $('div[id^="moduleInfoDiv_"]').fadeOut("fast");
              $('div[id^="scenarioInfoDiv_"]').fadeOut("fast");
              $('div[id^="projectInfoDiv_"]').fadeOut("fast");
              $("#projectInfoDiv_" + clickedId).fadeIn("fast");

              $("#ProjectListDiv").fadeOut("fast");

              $('table').removeClass("statusTable");
              $('#moduleListTable_' + clickedId).addClass("statusTable");
              
              localStorage.setItem("currentProjectId",clickedId);
              datatable = initializeSelectAll(clickedId);
              
          }

          function moduleTable(clickedId) {
              sessionStorage.setItem("ActiveScenarioId",0);
              $('div[id^=testcaseInfoDiv_').fadeOut("fast");
              $('div[id^="projectInfoDiv_"]').fadeOut("fast");
              $('div[id^="moduleInfoDiv_"]').fadeOut("fast");
              $('div[id^="scenarioInfoDiv_"]').fadeOut("fast");
              $("#moduleInfoDiv_" + clickedId).fadeIn("fast");

               $("#ProjectListDiv").fadeOut("fast");

              $('table').removeClass("statusTable");
              $('#scenarioListTable_' + clickedId).addClass("statusTable");
              localStorage.setItem("currentModuleId",clickedId);
              datatable = initializeSelectAll(clickedId);
              sceClickId=clickedId;

              
          }
          
          var passColor='${Model.Pass_Color}';
          var failColor='${Model.Fail_Color}';
          var skipColor='${Model.Skip_Color}';
          var blockColor='${Model.Block_Color}';
          var notRunColor='${Model.NotRun_Color}';
          var color;
          var datatable;
          var testcaseListTable;
          var currentScenarioId;
          /*function scenarioTable(clickedId) {
              $('div[id^=testcaseInfoDiv_').fadeOut("fast");
              $('div[id^="projectInfoDiv_"]').fadeOut("fast");
              $('div[id^="moduleInfoDiv_"]').fadeOut("fast");
              $('div[id^="scenarioInfoDiv_"]').fadeOut("fast");
              $("#scenarioInfoDiv_" + clickedId).fadeIn("fast");
              $("#ProjectListDiv").fadeOut("fast");

              $('table').removeClass("statusTable");
              $('#testcaseListTable_' + clickedId).addClass("statusTable");
              
              localStorage.setItem("currentScenarioId",clickedId);
              testcaseListTable = initializeSelectAll(clickedId);*/
              
              var tcExecDetailsList=[];
              tcExecDetailsList=${Model.tcExecDetailsList};
                  for(var i=0;i<tcExecDetailsList.length;i++)
                   {
                        if(tcExecDetailsList[i].description=="Pass")
                             color=passColor;
                        if(tcExecDetailsList[i].description=="Fail")
                             color=failColor;
                        if(tcExecDetailsList[i].description=="Skip")
                             color=skipColor;
                        if(tcExecDetailsList[i].description=="Block")
                             color=blockColor;
                        if(tcExecDetailsList[i].description=="Not run")
                             color=notRunColor;
                      
//                        $("#tcStatusPut_"+tcExecDetailsList[i].tc_id).html("<option><span class='label' style='background-color:"+color+";color:whiteSmoke;'>"+tcExecDetailsList[i].description+"</span></option>");
                       $("#tcStatusPut_"+tcExecDetailsList[i].tc_id+" option[value='"+tcExecDetailsList[i].status_id+"']").attr("selected", "selected");
                       var temp=$("#tcStatusPut_"+tcExecDetailsList[i].tc_id+" option[value='"+tcExecDetailsList[i].status_id+"']").attr("selected", "selected").text();
                       //alert(temp);
                   		$("#tcStatuses_"+tcExecDetailsList[i].tc_id+"").text(temp);
                   		
                   		$("#userid_"+tcExecDetailsList[i].tc_id+" option[value='"+tcExecDetailsList[i].tester_id+"']").attr("selected", "selected");
                   		var testerName=$("#userid_"+tcExecDetailsList[i].tc_id+" option[value='"+tcExecDetailsList[i].tester_id+"']").attr("selected", "selected").text();
                   		$("#testerNames_"+tcExecDetailsList[i].tc_id+"").text(testerName);
                		
                   }
              currentScenarioId = clickedId;
              
              testClickId=clickedId;
              
          //}
          
          
          var prevFadeOutId;
     

          function initializeSelectAll(clickedId) {
              var checkedTableId = $(".statusTable").attr("id");

              var tableInitialization;

              if ($.fn.dataTable.isDataTable('#' + checkedTableId)) {
                   tableInitialization = $('#' + checkedTableId).DataTable();
              } else {
                   tableInitialization = $('#' + checkedTableId).DataTable({
                        "aoColumnDefs" : [ {
                             'bSortable' : false,
                             'aTargets' : [ 1 ]
                        } ]
                   });
              }

              return tableInitialization;
          }

          $(".select2_demo_1").select2();
          
          function saveExecClicked(testcase_id,completeStatus)
          {
        	  var l = $('.ladda-button-demo').ladda();
              l.ladda('start');
              //check if attachments are added.
              //if added check for its validation of size and file type
              var uploadStatus = true;
              var counter = 0;
              
              //through each file attached
              $('.tcFile').each(function(){
                   lastIndex = $(this).attr('id').length;
                   id=$(this).attr('id').substring(7,lastIndex);
                   //check for the file upload
                   if($(this)[0].files[0]){
                	   var file = $(this)[0].files[0];
                       var fsize = file.size;
                       var fileName = file.name;
                       var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
                   //check for the supported file types
                     if((fileNameExt.toLowerCase() == "jpg") || (fileNameExt.toLowerCase() == "png") || (fileNameExt.toLowerCase() == "docx") || (fileNameExt.toLowerCase() == "pdf")){
                          //we support this file type
                          $(this).attr( "name","files["+ counter +"]");
                          counter++;
                     }else{
                          //we do not support
                          uploadStatus = false;
                          $("#tcUploadTypeError_"+id).text("Please upload file with one of type (png, jpg, pdf, docx)");
                          $("#tcUploadTypeError_"+id).removeClass('hidden');
                     }
              		}else{
              			$(this).attr('name','');
              		}
                   
                   //Check for the size of uploaded attachment
                   if(fsize> 1048576){
                        uploadStatus = false;
                        $("#tcUploadSizeError_"+id).text("Please upload file with size less than 1MB");
                        $("#tcUploadSizeError_"+id).removeClass('hidden');
                   }
              });

              if(uploadStatus == true){
      			var totalTestCaseCount = ${Model.totalTestCaseCount};
    			var tcExecDetailsList=[];
    			tcExecDetailsList=${Model.tcExecDetailsList};
    			var count=0;
    			for(var i=0;i<tcExecDetailsList.length;i++){
    				if(tcExecDetailsList[i].complete_status == 1){
    					count++;
    				}
    			}
    			var remainingTestCaseCount = totalTestCaseCount-count;
    			
    			//show  the modal if last test case is marked as complete.
              if((remainingTestCaseCount == 1) && (completeStatus == 1)){
                 
            	$('#buildLearning').summernote();
            	$('.note-editor').css('background','white');
        		$('button[ data-event="codeview"]').remove();
        		
        		 $('div.note-insert').remove();
                 $('div.note-table').remove();
                 $('div.note-help').remove();
                 $('div.note-style').remove();
                 $('div.note-color').remove();
                 $('button[ data-event="removeFormat"]').remove();
                 $('button[ data-event="insertUnorderedList"]').remove();
                 $('button[ data-event="fullscreen"]').remove();
                 $('button[ data-original-title="Line Height"]').remove();
                 $('button[ data-original-title="Font Family"]').remove();
                 $('button[ data-original-title="Paragraph"]').remove();
                 
            	$('#buildLearningModal').modal({
                	    backdrop: 'static',   // This disable for click outside event
                	    keyboard: false        // This for keyboard event
                	});
                 
               //on click of the save button
                 $('#saveBuildLearningBtn').click(function(){
               	 var validStatus = $('#buildLearningForm').valid();
               	 var textValue = $('#buildLearningForm .note-editable').text();
				
               	 if(textValue.length == 0 ){
               		validStatus = false;
               		//error messages
               		$('#learningErrorMessage').text('This field is required.');
               		$('#learningErrorMessage').show();
               	 }
               	 if(validStatus == true){
               		 //call the method to submit the details
               		 
               		 updateBuildExecution(testcase_id,completeStatus,1);
               		 $('#buildLearningModal').modal('hide'); 
               	 }
                 });
               
              }else{
            	  updateBuildExecution(testcase_id,completeStatus,2);
              }
          }
              
              l.ladda('stop');
          }
          
          function updateBuildExecution(testcase_id,completeStatus,buildExeStatus){
        	  
        	  $('body').addClass("white-bg");
        	  $("#barInMenu").removeClass("hidden");
        	  $("#wrapper").addClass("hidden");	
          
          var notes=$("#testcaseSummaryForm_"+testcase_id).find("textarea[name='notes']").val();
          //var tcStatus=$('input[name=tcStatus]:checked', "#testcaseSummaryForm_"+testcase_id).val();
          //alert('-'+$("#tcStatusHidden").val()+'-');
          var tcStatus = "";
         // alert($("#tcStatusHidden").val().length);
          if($("#tcStatusHidden").val().length > 0){
        	  tcStatus=$("#tcStatusHidden").val();
        	  
          }else{
        	  tcStatus=$('input[name=tcStatus]:checked', "#testcaseSummaryForm_"+testcase_id).val();
        	 // alert(tcStatus);
          }
          
          var testCaseInsertMethod;
               if($('#insertMethod_'+testcase_id).val()=="insert"){
                    testCaseInsertMethod = "insert";
               }
               else if($('#insertMethod_'+testcase_id).val()=="update"){
                    testCaseInsertMethod = "update";
               }
               var SvaeAndContinueFlag = completeStatus;
               if(completeStatus == 3)
                    {
                    completeStatus =2;
                    }
                    var posting = $.ajax({
                     type: "POST",
                     async: true,
                     url: "insertTcExecSummary",
                     data: {tcId:testcase_id,notes:notes,tcStatus:tcStatus,completeStatus:completeStatus,testCaseInsertMethod:testCaseInsertMethod},
                     success: function(data)
                     {
                    	 if(data==-1)
                    	 {
                    		 swal({
                    		        title: "Action can not completed",
                    		        text: "Build status is inactive!",
                    		        type: "warning",
                    		        showCancelButton: false,
                    		        confirmButtonColor: "#DD6B55",
                    		        confirmButtonText: "Ok",
                    		        closeOnConfirm: false
                    		    }, function () {
                    		        window.location.href="viewbuild";
                    		    });
                    	 }
                    	 
                    	 else if(data!=0)
                           {
                                var rows = $('#testcaseStepListTable_'+testcase_id).dataTable().fnGetNodes();
                                  for(var i=0;i<rows.length;i++)
                                  {
                                    
                                     var rowId=$(rows[i]).find("td:eq(0)").html();
                                     var testStepId=$(rows[i]).find("td:eq(1)").html();
                                   
                                     if( $("#stepComment_"+rowId).attr("bug")== 2)
                                        {
                                           var stepInsertMethod;
                                            if($('#insertTestStep_'+rowId).val()=="insert"){
                                                 stepInsertMethod = 'insert';
                                            }
                                            else if($('#insertTestStep_'+rowId).val()=="update"){
                                                 stepInsertMethod = 'update';
                                            }
                                    $.ajax({
                                                 type: "POST",
                                                 url: "insertStepExecSummary",
                                                 async: false,
                                                 data:{stepId:rowId,stepStatus:$("#stepStatus_"+rowId).val(),stepComment:$("#stepComment_"+rowId).val(),bugRaised:2,stepInsertMethod:stepInsertMethod}
                                                
                                       }); 
                                   }
                                      
                                  }  
                                
                                 if(SvaeAndContinueFlag==3)
                                  {
                                  sessionStorage.setItem("saveAndMoveNextFlag",1);
                                  }
                             else
                                  {
                                  sessionStorage.setItem("ActiveTestCaseId",0);
                                  sessionStorage.setItem("saveAndMoveNextFlag",0);
                                  }
                                
                               if(buildExeStatus == 1){
                            	 //upload the build learning and build status through ajax
                            	 //Step 1 : Get the build learning

                              	var textareaValue = $("#buildLearning").code();
                              	
                            	 $.ajax({
                               		url: "uploadbuildlearning",
                               		type: "POST",
                               		data: {
                               	        buildLearning: textareaValue
                               	    }
                               	})
                               	 .done(function( json ) {
                               		 //upload the attachments
                                     $('#tcattachonbuildexeForm_'+testcase_id).append(' <input name="exeStatus" type="hidden" value="'+data+'" />');
                                     $('#tcattachonbuildexeForm_'+testcase_id).append(' <input name="tcId" type="hidden" value="'+testcase_id+'" />');
                                     $('#tcattachonbuildexeForm_'+testcase_id).submit();
								  });
                               }else{
                            	   //upload the attachments
                                   $('#tcattachonbuildexeForm_'+testcase_id).append(' <input name="exeStatus" type="hidden" value="'+data+'" />');
                                   $('#tcattachonbuildexeForm_'+testcase_id).append(' <input name="tcId" type="hidden" value="'+testcase_id+'" />');
                                   $('#tcattachonbuildexeForm_'+testcase_id).submit();
                               }
                           }
                     }
                   }); 
          }   
            function enterKeyPress(e) {
              if (e.keyCode === 13) {
               return false;
              }
             
          }  
           
       
        
          
          
          
     function tcAttchFile(testCaseID){
          var fileIndex = $('#tcFileTable_'+testCaseID+' tr').children().length;
          if(fileIndex>0){
              lastIndex = $('#tcFileTable_'+testCaseID+' tr:last').attr("id").length;
              fileIndex = parseInt($('#tcFileTable_'+testCaseID+' tr:last').attr("id").substring(11,lastIndex)) + parseInt(1);
          }
          $('#tcFileTable_'+testCaseID).append(
                   '<tr id="tcTableRow_'+ fileIndex +'"><td>'+
                   '    <input type="file"  accept=".png,.jpg,.pdf,.docx" class="tcFile hidden" id="tcFile_'+ fileIndex +'" />'+
                   '<label class="fileName" style="font-weight: 400;" id="tcFileName_'+ fileIndex +'"></label>'+
                   '<a id="tcClose_'+ fileIndex +'" class="tcClose hidden btn-xs btn-danger"  style="float: none">x</a>'+
                   '<label class="error hidden" id="tcUploadSizeError_'+ fileIndex +'"></label>'+
                   '<label class="error hidden" id="tcUploadTypeError_'+ fileIndex +'"></label>'+
                   '</td></tr>');
          
          
          $("#tcFile_"+fileIndex).click();
          $("#tcFile_"+fileIndex).change(function(){
              
              if(this.files[0] ==null || this.files[0] == undefined)
                   {
                   $("#tcFileName_"+fileIndex).html("");
                   $('#tcTableRow_'+ fileIndex ).remove();
                   }
              else
                   {
                   $("#tcFileName_"+fileIndex).html(this.files[0].name);
                   $("#tcClose_"+fileIndex).removeClass('hidden');
                   } 
          });                

          $(".tcClose").click(function(){
              lastIndex = $(this).attr('id').length;
              id =  $(this).attr('id').substring(8,lastIndex);
              $('#tcTableRow_'+id ).remove();
          });
          
     }
     
  function downloadFile(attachID,title){
	  var posting = $.post('gettcattchment', {
			 attchmentId : attachID,
			});
	  posting.done(function(data){
		  	 var fileNameExt = title.substr(title.lastIndexOf('.') + 1);
			 var a = window.document.createElement('a');
			 link =  "data:"+fileNameExt+";base64,"+data;
			 a.href=link;
			 a.download = title;
			 document.body.appendChild(a)
			 a.click();
			 document.body.removeChild(a)
			
	  });
  }   

</script>

<script>
	//Enable-Disable submit button
	var flag=0;
	 $("#checkAll").click(function () {
	        $(".checkClass").prop('checked', $(this).prop('checked'));
	        if($("#checkAll").prop("checked")==true){
	        	$("#rt").attr("disabled",false);
	        }else{
	        	$("#rt").attr("disabled",true);
	        }
	    });
	 $(".checkClass").click(function(){
		 if($(this).prop("checked")==true || $("#checkAll").prop("checked")==true){
	        	flag=flag+1;
				$("#rt").attr("disabled",false);
	        }else{
	        	flag=flag-1;
	        	$("#rt").attr("disabled",true);
	        }   
		 
		 
			 if(flag>0){
				 $("#rt").attr("disabled",false);
			 }else{
				 $("#rt").attr("disabled",true);
			 }
	 });
	
	 //Multiple Test case execution
	$("#rt").click(function(){
		var execData=new Array();
		
  		 var testcasesListing= ${Model.testcasesListing};
  		 var buildListing=${Model.buildListing};
  		 var tcExecDetailsList=[];
       	 tcExecDetailsList=${Model.tcExecDetailsList};
       
  		 for(var i=0;i<testcasesListing.length;i++){
  			
  			  if($("input[id=checkTc_"+testcasesListing[i].testcase_id+"]").prop("checked")==true){
  				  
  				var tcId=$("#tcid_"+testcasesListing[i].testcase_id+"").attr("value");
  				var buildId=$("#buildid_"+testcasesListing[i].testcase_id+"").attr("value");
  				var projectId=$("#projectid_"+testcasesListing[i].testcase_id+"").attr("value");
  				var moduleId=$("#moduleid_"+testcasesListing[i].testcase_id+"").attr("value");
  				var scenarioId=$("#scenarioid_"+testcasesListing[i].testcase_id+"").attr("value");
  				var testerId=$("#userid_"+testcasesListing[i].testcase_id+" option:selected").val();
  				var statusId=$("#tcStatusPut_"+testcasesListing[i].testcase_id+" option:selected").val();
  				var notes="";
  				var completeStatus="2";
  				//var typeMethod=$("#tcOp_"+testcasesListing[i].testcase_id+"").text();
  				var typeMethod=$("#tcOp_"+tcId+"").text();
  				
  				for(var j=0;j<tcExecDetailsList.length;j++){
						var temp=tcExecDetailsList[j].tc_id;
						if(temp==tcId || $("#tcOp_"+tcId+"").text()=="update"){
							typeMethod="update";
							$("#tcOp_"+tcId+"").text("update");
						}
						
   				}
  				
  				execData.push({tcId, buildId, projectId, moduleId, scenarioId, testerId, statusId, notes, completeStatus, typeMethod});

  			  }//end if 
  		  }//end for loop
  		  
		  //console.log(execData);
  		  var str=JSON.stringify(execData);
  		  var res=$.ajax({
  			 type:"POST",
  			 url:"insertTcExecSummaryMultiple",
  			 dataType:"json",
  			 data:{execData:str},
	  			success: function( data ) {
	  				if(data==1)
		           	 {
	  					$("#tcOp_"+tcId+"").text("update");
		           		 swal({
		           		        title: "Success",
		           		        text: "Test Cases Executed Successfully",
		           		        type: "success",
		           		        showCancelButton: false,
		           		        confirmButtonColor: "#DD6B55",
		           		        confirmButtonText: "Ok",
		           		        closeOnConfirm: false
		           		    }, function () {
		           		    	
		           		        window.location.href="executebuild";
		           		    });
		           	 }else{
		           		swal({
	           		        title: "Failed",
	           		        text: "Execution Error!",
	           		        type: "error",
	           		        showCancelButton: false,
	           		        confirmButtonColor: "#DD6B55",
	           		        confirmButtonText: "Ok",
	           		        closeOnConfirm: false
	           		    });
		           	 }
	  				
	  		   	}
  		  });
  		   
	});
	//Code to fiter out the individual columns
	var table=$("#tblTC").DataTable(
			{	
				dom : '<"toolbar">lTfgitp',
				 language: {
				        search: "_INPUT_",
				        searchPlaceholder: "Search..."
				    },
				    columnDefs: [
			            {
			                //"targets": [13,14,15,16],
			                "targets": [15,16],
			                "visible": false
			        }],
				"paging" : true,
				"lengthChange" : true,
				"pageLength": 10,
				"scrollX": true,
				"bSortCellsTop":true,
				"fnDrawCallback": function( oSettings ) {
					var testcasesListing= ${Model.testcasesListing};
            		var tcExecDetailsList=[];
          		 	tcExecDetailsList=${Model.tcExecDetailsList};
          			 for(var i=0;i<testcasesListing.length;i++){
   							for(var j=0;j<tcExecDetailsList.length;j++){
 									var temp=tcExecDetailsList[j].tc_id;
 									if(temp==testcasesListing[i].testcase_id){
 										$("#tcOp_"+testcasesListing[i].testcase_id+"").text("update");
 									}
    						}
   			 		 }
				 }
			});
	
	
	
	/*Dropdown*/
    $("div.toolbar").html('<div class="pull-right btn-group"><div class="btn-toolbar">' +
    		'<button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
    'Action <span class="caret"></span></button>'+
  '<ul class="dropdown-menu animated fadeInRight">'+
              '<li><a href="#" data-toggle="modal" data-target="#columnModal">Select Column</a></li>'+
              //'<li><a href="#" data-toggle="modal" data-target="#filterModal">Filter</a></li>'+
              '<li><a href="exportexecution" target="_blank">Export</a></li>'+
                '</ul></div>' +
                //'<button type="button" id="rt"'+
				//'class="btn btn-primary btn-sm" disabled>Submit Execution</button>'+
				'</div>'+
        '</div>');
	
	
    /* Filtering */
	$('.togg').on( 'change', function (e) {
		
        e.preventDefault();
        
        var column = table.column( $(this).val());
        
        column.visible( ! column.visible() );
        
    } );
	
		$("#searchTcId").on('keyup',function(){
			table
				.columns(4)
				.search(this.value)
				.draw(); 
		});
		
		$("#searchTcName").on('keyup',function(){
			table
				.columns(5)
				.search(this.value)
				.draw(); 
		});
		
		$("#searchSummary").on('keyup',function(){
			table
				.columns(6)
				.search(this.value)
				.draw(); 
		});
		
		$("#col8_filter").on('keyup focus',function(){
			filterColumn(8);
		});
		
		
		
		$("#col10_filter").on('keyup focus',function(){
			filterColumn1(10);
		});
		
		$("#col14_filter").on('keyup focus',function(){
			filterColumn2(14);
		});
		
		$("#col13_filter").on('keyup focus',function(){
			filterColumn3(13);
		});
		
		
		function filterColumn ( i ) {
		    $('#tblTC').DataTable().column( i ).search(
		        $('#col'+i+'_filter').val(),
		        $('#col'+i+'_regex').prop('checked'),
		        $('#col'+i+'_smart').prop('checked')
		    ).draw();
            var str=document.getElementById("col8_filter").value;
            document.getElementById("col8_filter").style.display='none';
            document.getElementById("searchStatus").style.display='block';
            document.getElementById("searchStatus").value =str.split("|").join(",");
		}
		

		function allFilter(sel) {
			var opts = '',
		    opt;
		  var len = sel.options.length;
		  for (var i = 0; i < len; i++) {
		    opt = sel.options[i];
		    
		    if (opt.selected) {

		          opts=opt.value;
// 		          if(opts == "Status"){
// 		        	  document.getElementById("Status").style.display='block'; 
// 		          } 
		          
// 		          if(opts == "Tester"){
// 		        	  document.getElementById("Tester").style.display='block'; 
// 		          } 
		          
		          switch (opts) {
		          case "Status":
		        	  document.getElementById("Status").style.display='block';
		        	  document.getElementById("Tester").style.display='none';
		        	  document.getElementById("Module").style.display='none';
		        	  document.getElementById("Application").style.display='none';
		        	  
		            break;
		          case "Tester":
		        	  document.getElementById("Tester").style.display='block';
		        	  document.getElementById("Status").style.display='none';
		        	  document.getElementById("Module").style.display='none';
		        	  document.getElementById("Application").style.display='none';
		        	  
		            break;
		          case "Module":
		        	  document.getElementById("Module").style.display='block';
		        	  document.getElementById("Status").style.display='none';
		        	  document.getElementById("Tester").style.display='none';
		        	  document.getElementById("Application").style.display='none';
		            break;
		          case "Application":
		        	  document.getElementById("Application").style.display='block';
		        	  document.getElementById("Status").style.display='none';
		        	  document.getElementById("Tester").style.display='none';
		        	  document.getElementById("Module").style.display='none';
		            break;
		          
		          
		          default:
		        }
		          
		      
		    }
		  }

		}
		

		function statusFilter(sel) {
			document.getElementById("col8_filter").style.display='block';
			var opts = '',
		    opt;
		  var len = sel.options.length;
		  for (var i = 0; i < len; i++) {
		    opt = sel.options[i];
		    
		    if (opt.selected) {
		    	
		      if(opts!=""){
		      opts=opts+"|"+opt.value;
		      
		      }else{
		          opts=opt.value;
		      }
		    }
		  }
		  document.getElementById("col8_filter").value =opts;
		  document.getElementById("col8_filter").focus();
		  document.getElementById("searchStatus").style.borderColor = "#33B7FF";
		  return false;
		}
		
		
		function userFilter(sel) {
			document.getElementById("col10_filter").style.display='block';
			var opts = '',
		    opt;
		  var len = sel.options.length;
		  for (var i = 0; i < len; i++) {
		    opt = sel.options[i];
		    
		    if (opt.selected) {
		    	
		      if(opts!=""){
		      opts=opts+"|"+opt.value;
		      
		      }else{
		          opts=opt.value;
		      }
		    }
		  }
		  document.getElementById("col10_filter").value =opts;
		  document.getElementById("col10_filter").focus();
		  document.getElementById("testerStatus").style.borderColor = "#33B7FF";
		  return false;
		}
		
		function moduleFilter(sel) {
			document.getElementById("col14_filter").style.display='block';
			var opts = '',
		    opt;
		  var len = sel.options.length;
		  for (var i = 0; i < len; i++) {
		    opt = sel.options[i];
		    
		    if (opt.selected) {
		    	
		      if(opts!=""){
		      opts=opts+"|"+opt.value;
		      
		      }else{
		          opts=opt.value;
		      }
		    }
		  }
		  document.getElementById("col14_filter").value =opts;
		  document.getElementById("col14_filter").focus();
		  document.getElementById("moduleName").style.borderColor = "#33B7FF"
		  return false;
		}
		
		function applicationFilter(sel) {
			document.getElementById("col13_filter").style.display='block';
			var opts = '',
		    opt;
		  var len = sel.options.length;
		  for (var i = 0; i < len; i++) {
		    opt = sel.options[i];
		    
		    if (opt.selected) {
		    	
		      if(opts!=""){
		      opts=opts+"|"+opt.value;
		      
		      }else{
		          opts=opt.value;
		      }
		    }
		  }
		  document.getElementById("col13_filter").value =opts;
		  document.getElementById("col13_filter").focus();
		  document.getElementById("applicationName").style.borderColor = "#33B7FF";
		  return false;
		}
		
		
		function filterColumn1 ( i ) {
		    $('#tblTC').DataTable().column( i ).search(
		        $('#col'+i+'_filter').val(),
		        $('#col'+i+'_regex').prop('checked'),
		        $('#col'+i+'_smart').prop('checked')
		    ).draw();
            var str=document.getElementById("col10_filter").value;
            document.getElementById("col10_filter").style.display='none';
            document.getElementById("testerStatus").style.display='block';
            document.getElementById("testerStatus").value =str.split("|").join(",");
		}
		
		function filterColumn2 ( i ) {
		    $('#tblTC').DataTable().column( i ).search(
		        $('#col'+i+'_filter').val(),
		        $('#col'+i+'_regex').prop('checked'),
		        $('#col'+i+'_smart').prop('checked')
		    ).draw();
            var str=document.getElementById("col14_filter").value;
            document.getElementById("col14_filter").style.display='none';
            document.getElementById("moduleName").style.display='block';
            document.getElementById("moduleName").value =str.split("|").join(",");
		}
		
		function filterColumn3 ( i ) {
		    $('#tblTC').DataTable().column( i ).search(
		        $('#col'+i+'_filter').val(),
		        $('#col'+i+'_regex').prop('checked'),
		        $('#col'+i+'_smart').prop('checked')
		    ).draw();
            var str=document.getElementById("col13_filter").value;
            document.getElementById("col13_filter").style.display='none';
            document.getElementById("applicationName").style.display='block';
            document.getElementById("applicationName").value =str.split("|").join(",");
		}
		
		
		
		$("#searchBugs").on('keyup',function(){
			table
				.columns(12)
				.search(this.value)
				.draw(); 
		});
		
		$("#searchApplication").on('keyup',function(){
			table
				.columns(13)
				.search(this.value)
				.draw(); 
		});
		
		$("#searchModule").on('keyup',function(){
			table
				.columns(14)
				.search(this.value)
				.draw(); 
		});
		
		$("#searchScenario").on('keyup',function(){
			table
				.columns(15)
				.search(this.value)
				.draw(); 
		});
		
	
</script>

<script>

/* $("select[name=stepStatus]").change(function()
        {
   $("input[name='testStepId']").val("");
         lastIndex = $(this).attr('id').length;
             id = $(this).attr('id').substring(11,lastIndex);
        if($(this).val()==2){
             $("input[name='testStepId']").val(id);
         //$('#statusFail_'+ id ).removeClass('hidden');
         btStatus = ${Model.bugAccess};
         if(btStatus == 1){
         	
             $('#createBug').modal('show'); 
         }
        }
        });

$('#btnSubmitBug').click(function() {
	var formStat = $("#createBugForm").valid();
	 
    var counter = 0;
    var statusUpload = 1;
       $('.file').each(function(){
            $(this).attr( "name","files["+ counter +"]");
            lastIndex = $(this).attr('id').length;
            id=$(this).attr('id').substring(5,lastIndex);
            var fsize = 0;
              var filetype = 1;
              if($(this)[0].files[0]){
                   file = $(this)[0].files[0];
                fsize = file.size;
                var fileName = file.name;
              var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
              if(!(fileNameExt.toLowerCase() == "jpg" || fileNameExt.toLowerCase() == "png" || fileNameExt.toLowerCase() == "docx" || fileNameExt.toLowerCase() == "pdf"))
                {
                filetype = 0;
                statusUpload = 0;
                }
              $(this).attr( "name","files["+ counter +"]");
                   counter++;
              }
              else
                   {
                   $(this).attr('name','');
                   }
              if(filetype == 0){
                   statusUpload = 0;
                   $("#uploadTypeError_"+id).css("display","");
                   $("#uploadTypeError_"+id).text("Please upload file with one of type (png, jpg, pdf, docx)");
                   $("#uploadTypeError_"+id).removeClass('hidden');
              }
              else if(fsize> 1048576)
                   {
                   statusUpload = 0;
                   $("#uploadSizeError_"+id).css("display","");
                   $("#uploadSizeError_"+id).text("Please upload file with size less than 1MB");
                   $("#uploadSizeError_"+id).removeClass('hidden');
                   }
         });
    
       $("input[name='projectId']").val(localStorage.getItem("currentProjectId"));
       //alert(localStorage.getItem("currentProjectId"));
       $("input[name='moduleId']").val(localStorage.getItem("currentModuleId"));
       StepId = $("input[name='testStepId']").val();
       $("input[name='stepStatusId']").val($("#stepStatus_"+StepId).val());
       $("input[name='testStepComment']").val($("#stepComment_"+StepId).val());
      
      var currentTestcaseId=localStorage.getItem("currentTestcaseId");
       $("input[name='testcaseId']").val(currentTestcaseId);
       var tcStatus=$('input[name=tcStatus]:checked', "#testcaseSummaryForm_"+currentTestcaseId).val();
       $("input[name='testcaseStatusId']").val(tcStatus);
       var notes=$("#testcaseSummaryForm_"+currentTestcaseId).find("textarea[name='notes']").val();
       $("input[name='testcaseNotes']").val(notes);
            if($('#insertMethod_'+currentTestcaseId).val()=="update"){
                 $("input[name='testCaseInsertMethod']").val("update");
                 }
            if($('#insertTestStep_'+StepId).val()=="update"){
              $("input[name='stepInsertMethod']").val("update");
            }
           if(statusUpload == 1 && formStat){
           // if(statusUpload == 1){ 
             $('#createBug').modal('hide');
                 
            $('body').addClass("white-bg");
            $("#barInMenu").removeClass("hidden");
            $("#wrapper").addClass("hidden");
       
            $('#createBugForm').submit();
           
           
            }
}); */


</script>



<script>
//code to attach file
function tcAttchFile(testCaseID){
          var fileIndex = $('#tcFileTable_'+testCaseID+' tr').children().length;
          if(fileIndex>0){
              lastIndex = $('#tcFileTable_'+testCaseID+' tr:last').attr("id").length;
              fileIndex = parseInt($('#tcFileTable_'+testCaseID+' tr:last').attr("id").substring(11,lastIndex)) + parseInt(1);
          }
          $('#tcFileTable_'+testCaseID).append(
                   '<tr id="tcTableRow_'+ fileIndex +'"><td>'+
                   '    <input type="file"  accept=".png,.jpg,.pdf,.docx" class="tcFile hidden" id="tcFile_'+ fileIndex +'" />'+
                   '<label class="fileName" style="font-weight: 400;" id="tcFileName_'+ fileIndex +'"></label>'+
                   '<label class="error hidden" id="tcUploadSizeError_'+ fileIndex +'"></label>'+
                   '<label class="error hidden" id="tcUploadTypeError_'+ fileIndex +'"></label>'+
                   '</td></tr>');
          
          
          $("#tcFile_"+fileIndex).click();
          $("#tcFile_"+fileIndex).change(function(){
              
              if(this.files[0] ==null || this.files[0] == undefined)
                   {
                   $("#tcFileName_"+fileIndex).html("");
                   $('#tcTableRow_'+ fileIndex ).remove();
                   }
              else
               {
            	  if(this.files[0].size > 1048576){
            		  swal({
	           		        title: "Upload error",
	           		        text: "File Size should be less than 1 MB",
	           		        type: "error",
	           		        showCancelButton: false,
	           		        confirmButtonColor: "#DD6B55",
	           		        confirmButtonText: "Ok",
	           		        closeOnConfirm: false
	           		    });
            		  $("#tcFileName_"+fileIndex).html("");
                      $('#tcTableRow_'+ fileIndex ).remove();
                      
            	  }else{
                   $("#tcFileName_"+fileIndex).html(this.files[0].name);
                   //$("#tcClose_"+fileIndex).removeClass('hidden');
                   		var es="2";
		                var formData=new FormData();
		       			formData.append('exeStatus',es);
		       			formData.append('tcId',testCaseID);
		       			formData.append('uploadForm',this.files[0]);
		       			var res=$.ajax({
		       	 			 type:"POST",
		       	 			 url:"tcattachonbuildexecution",
		       	 			 contentType:false,
		       	 			 processData:false,
		       	 			 data:formData,
		       		  			success: function( data ) {
		       		  				if(data==1)
		       			           	 {
		       			           		 swal({
		       			           		        title: "Success",
		       			           		        text: "File Attached Successfully",
		       			           		        type: "success",
		       			           		        showCancelButton: false,
		       			           		        confirmButtonColor: "#DD6B55",
		       			           		        confirmButtonText: "Ok",
		       			           		        closeOnConfirm: false
		       			           		    });
		       			           	 }
		       		  				
		       		  		   	}
		       	 		  });
                   
            	  		}
                   } 
          });                

          /*$(".tcClose").click(function(){
              lastIndex = $(this).attr('id').length;
              id =  $(this).attr('id').substring(8,lastIndex);
              $('#tcTableRow_'+id ).remove();
          });*/
          
     }
     
  function downloadFile(attachID,title){
	  var posting = $.post('gettcattchment', {
			 attchmentId : attachID,
			});
	  posting.done(function(data){
		  	 var fileNameExt = title.substr(title.lastIndexOf('.') + 1);
			 var a = window.document.createElement('a');
			 link =  "data:"+fileNameExt+";base64,"+data;
			 a.href=link;
			 a.download = title;
			 document.body.appendChild(a)
			 a.click();
			 document.body.removeChild(a)
			
	  });
  }   
  
  
  function saveAttachment(testcase_id)
  {
	 
  }

	
</script>

<script>

$('#addFileTC').click(function() {
    var fileIndex = $('#fileTableTC tr').children().length;
    if(fileIndex>0){
         lastIndex = $('#fileTableTC tr:last').attr("id").length;
         fileIndex = parseInt($('#fileTableTC tr:last').attr("id").substring(9,lastIndex)) + parseInt(1);
    }
    $('#fileTableTC').append(
              '<tr id="tableRowTC_'+ fileIndex +'"><td>'+
              '    <input type="file"  accept=".png,.jpg,.pdf,.docx" class="filetc hidden" id="fileTC_'+ fileIndex +'" />'+
              '<label class="fileName" id="fileNameTC_'+ fileIndex +'"></label>'+
              '<a id="closeTC_'+ fileIndex +'" class="close hidden"  style="float: none">x</a>'+
              '<label class="error hidden" id="uploadSizeErrorTC_'+ fileIndex +'">Please upload less than 1 MB</label>'+
              '<label class="error hidden" id="uploadTypeErrorTC_'+ fileIndex +'">Valid only (png, jpg, pdf, docx)</label>'+
              '</td></tr>');
    
    $("#fileTC_"+fileIndex).click();
    $("#fileTC_"+fileIndex).change(function(){
         if(this.files[0] ==null || this.files[0] == undefined)
              {
              $("#fileNameTC_"+fileIndex).html("");
              $('#tableRowTC_'+ fileIndex ).remove();
              }
         else
              {
              $("#fileNameTC_"+fileIndex).html(this.files[0].name);
              $("#closeTC_"+fileIndex).removeClass('hidden');
              $('.close').each(function()
              {
                   if($(this).hasClass('hidden'))
                       {
                       lastIndex = $(this).attr('id').length;
                       id = $(this).attr('id').substring(5,lastIndex);
                      $('#tableRowTC'+ id ).remove();
                       }
              }); 
              } 
    });
         
    $(".close").click(function(){
         lastIndex = $(this).attr('id').length;
         id =  $(this).attr('id').substring(6,lastIndex);
         $('#tableRowTC_'+ id ).remove();
   });
    
});

	$("#tblTC").on("change","select[name=tcstatus]",function(){
		var selectedStatusId=$("select[name=tcstatus]").val();
		var totalLength=$("td[name=tcStatusResult]").attr('id').length;
		var ids=$(this).attr('id').substring(12,totalLength+1);
		localStorage.setItem("currentTestCaseIdTC",ids);
		
		var tempProjectId=$("#projectid_"+ids+"").attr("value");
		localStorage.setItem("currentProjectIdTC",tempProjectId);
		
		var tempModuleId=$("#moduleid_"+ids+"").attr("value");
		localStorage.setItem("currentModuleIdTC",tempModuleId);
		  
		var tep=$("#tcStatusPut_"+ids+" option:selected").val(); 
		if(tep==2){
			btStatus = ${Model.bugAccess};
		    if(btStatus == 1){
		       		 $('#createBugTC').modal('show'); 
		    	}
		}
	});


$('#btnSubmitBugTC').click(function() {
	  //var formStat = $("#createBugForm").valid();
	 
  var counter = 0;
  var statusUpload = 1;
  var f;
     $('.filetc').each(function(){
          $(this).attr( "name","files["+ counter +"]");
          lastIndex = $(this).attr('id').length;
          id=$(this).attr('id').substring(5,lastIndex);
          var fsize = 0;
            var filetype = 1;
            if($(this)[0].files[0]){
                 file = $(this)[0].files[0];
                 f=file;
              fsize = file.size;
              var fileName = file.name;
            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
            if(!(fileNameExt.toLowerCase() == "jpg" || fileNameExt.toLowerCase() == "png" || fileNameExt.toLowerCase() == "docx" || fileNameExt.toLowerCase() == "pdf"))
              {
              filetype = 0;
              statusUpload = 0;
              }
            $(this).attr( "name","files["+ counter +"]");
                 counter++;
            }
            else
                 {
                 $(this).attr('name','');
                 }
            if(filetype == 0){
                 statusUpload = 0;
                 $("#uploadTypeErrorTC_"+id).css("display","");
                 $("#uploadTypeErrorTC_"+id).text("Please upload file with one of type (png, jpg, pdf, docx)");
                 $("#uploadTypeErrorTC_"+id).removeClass('hidden');
            }
            else if(fsize> 1048576)
                 {
                 statusUpload = 0;
                 $("#uploadSizeErrorTC_"+id).css("display","");
                 $("#uploadSizeErrorTC_"+id).text("Please upload file with size less than 1MB");
                 $("#uploadSizeErrorTC_"+id).removeClass('hidden');
                 }
            
       });
  
     $("input[name='projectIdTC']").val(localStorage.getItem("currentProjectIdTC"));
     $("input[name='moduleIdTC']").val(localStorage.getItem("currentModuleIdTC"));
     StepId="0";
     $("input[name='stepStatusIdTC']").val("2");
     $("input[name='testStepCommentTC']").val("");
     
     $("input[name='testcaseIdTC']").val(localStorage.getItem("currentTestCaseIdTC"));
     //var tcStatus=$('input[name=tcStatus]:checked', "#testcaseSummaryForm_"+localStorage.getItem("currentTestCaseIdTC")).val();
     $("input[name='testcaseStatusIdTC']").val("2");
     
     $("input[name='testcaseNotesTC']").val(notes);
          if($('#insertMethod_'+localStorage.getItem("currentTestCaseIdTC")).val()=="update"){
               $("input[name='testCaseInsertMethodTC']").val("update");
               }
          if($('#insertTestStep_'+StepId).val()=="update"){
            $("input[name='stepInsertMethodTC']").val("update");
          }
          if(statusUpload == 1){
           $('#createBugTC').modal('hide');
               
          //$('body').addClass("white-bg");
          //$("#barInMenu").removeClass("hidden");
          //$("#wrapper").addClass("hidden");
          var arr=new Array();
          var projectId=$("input[name='projectIdTC']").val();
          var moduleId=$("input[name='moduleIdTC']").val();
          var bugSummary=$("input[name='bugSummaryTC']").val();
          var bugDescription=$("#bugDescriptionTC").code();
          var testcaseId=$("input[name='testcaseIdTC']").val();
          var testcaseStatusId=$("input[name='testcaseStatusIdTC']").val();
          var notes="";
          var stepId="0";
          var stepStatusId=$("input[name='stepStatusIdTC']").val();
          var stepComment=$("input[name='testStepCommentTC']").val();
          var testcaseInserMethod=$("input[name='testCaseInsertMethodTC']").val();
          var stepInsertMethod=$("input[name='stepInsertMethodTC']").val();
          
          var formData=new FormData();
          formData.append('projectId',projectId);
          formData.append('moduleId',moduleId);
          formData.append('bugSummary',bugSummary);
          formData.append('bugDescription',bugDescription);
          formData.append('testcaseId',testcaseId);
          formData.append('testcaseStatusId',testcaseStatusId);
          formData.append('testcaseNotes',notes);
          formData.append('testStepId',stepId);
          formData.append('stepStatusId',stepStatusId);
          formData.append('testStepComment',stepComment);
          formData.append('testCaseInsertMethod',testcaseInserMethod);
          formData.append('stepInsertMethod',stepInsertMethod);
          
          if(f){
        	  formData.append('uploadForm',f);
          }
          

          //console.log(formData);
          var res=$.ajax({
   			 type:"POST",
   			 url:"raisebugfortc",
   			contentType:false,
	 		processData:false,
   			data:formData,
   			 success: function( data ) {
	  				if(data!="error")
		           	 {
	  					$('#bugTable_'+testcaseId).append(
	  		                   '<tr><td><lable>Bug # '+data+'</lable></<td></tr>');
	  					
		           	 }else{
		           		swal({
		           		        title: "Failed",
		           		        text: "Can not create bug",
		           		        type: "error",
		           		        showCancelButton: false,
		           		        confirmButtonColor: "#DD6B55",
		           		        confirmButtonText: "Ok",
		           		        closeOnConfirm: false
		           		    });
		           	 }
	  				
	  		   	}
   		  });
        }
});
</script>

<script>
//Test Case execution on tester change
function changeTester(t_id){
				$("#checkTc_"+t_id+"").prop("checked","checked");
				flag=1;
				$("#rt").attr("disabled",false);
				
				var execData=new Array();
		  		var tcExecDetailsList=[];
		       	tcExecDetailsList=${Model.tcExecDetailsList};
      
  				var tcId=$("#tcid_"+t_id+"").attr("value");
  				var buildId=$("#buildid_"+t_id+"").attr("value");
  				var projectId=$("#projectid_"+t_id+"").attr("value");
  				var moduleId=$("#moduleid_"+t_id+"").attr("value");
  				var scenarioId=$("#scenarioid_"+t_id+"").attr("value");
  				var testerId=$("#userid_"+t_id+" option:selected").val();
  				var statusId=$("#tcStatusPut_"+t_id+" option:selected").val();
  				var notes="";
  				var completeStatus="2";
  				var typeMethod=$("#tcOp_"+t_id+"").text();
  				
  				/*for(var j=0;j<tcExecDetailsList.length;j++){
						var temp=tcExecDetailsList[j].tc_id;
						if(temp==t_id){
							typeMethod="update";
						}
						
   				}*/
   				
  				
  				execData.push({tcId, buildId, projectId, moduleId, scenarioId, testerId, statusId, notes, completeStatus, typeMethod});

		  //console.log(execData);
  		  var str=JSON.stringify(execData);
  		  var res=$.ajax({
  			 type:"POST",
  			 url:"insertTcExecSummaryMultiple",
  			 dataType:"json",
  			 data:{execData:str},
  			 success: function(data){
				 if(data==1){
					 toastr.options = {
							 "closeButton": true,
							  "debug": true,
							  "progressBar": true,
							  "preventDuplicates": true,
							  "positionClass": "toast-top-right",
							  "showDuration": "400",
							  "hideDuration": "1000",
							  "timeOut": "1500",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "slideDown",
							  "hideMethod": "slideUp"
				     };
				     toastr.success('',$("#userid_"+t_id+" option:selected").text()+" has been assigned");
				     $("#tcOp_"+t_id+"").text("update");
					 $("#tcStatuses_"+t_id+"").text($("#tcStatusPut_"+t_id+" option:selected").text());
						
				 }else{
					 toastr.options = {
							 "closeButton": true,
							  "debug": true,
							  "progressBar": true,
							  "preventDuplicates": true,
							  "positionClass": "toast-top-right",
							  "showDuration": "400",
							  "hideDuration": "1000",
							  "timeOut": "1500",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "slideDown",
							  "hideMethod": "slideUp"
				     };
				     toastr.error('',"Execution Error!");
				 }
			 }
  		  });
}
</script>

<script>
 //Test case execution on TC status change
  function ll(){
	 $('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden"); 
		//$('#tblTC').DataTable();
		//$('.dataTables_length').addClass('bs-select');
		
		 var foo = $('.footable').footable();
		 foo.trigger('footable_initialize'); //Reinitialize
		 foo.trigger('footable_redraw'); //Redraw the table
		 foo.trigger('footable_resize'); //Resize the table
		// alert("hi");
 }
 	function changeTcStatus(tcid){
 		

		 		$("#checkTc_"+tcid+"").prop("checked","checked");
		 		flag=1;
		 		$("#rt").attr("disabled",false);
		 		
		 		var execData=new Array();
				var tcExecDetailsList=[];
		   		tcExecDetailsList=${Model.tcExecDetailsList};
		
				var tcId=$("#tcid_"+tcid+"").attr("value");
				var buildId=$("#buildid_"+tcid+"").attr("value");
				var projectId=$("#projectid_"+tcid+"").attr("value");
				var moduleId=$("#moduleid_"+tcid+"").attr("value");
				var scenarioId=$("#scenarioid_"+tcid+"").attr("value");
				var testerId=$("#userid_"+tcid+" option:selected").val();
				var statusId=$("#tcStatusPut_"+tcid+" option:selected").val();
				var notes="";
				var completeStatus="2";
				var typeMethod=$("#tcOp_"+tcid+"").text();
	        	document.getElementById('tcStatusHidden').value=statusId;

				/*for(var j=0;j<tcExecDetailsList.length;j++){
					var temp=tcExecDetailsList[j].tc_id;
					if(temp==tcId){
						typeMethod="update";
					}
					
				}*/
				
				execData.push({tcId, buildId, projectId, moduleId, scenarioId, testerId, statusId, notes, completeStatus, typeMethod});
		
			//console.log(execData);
			var str=JSON.stringify(execData);
			var res=$.ajax({
			 type:"POST",
			 url:"insertTcExecSummaryMultiple",
			 dataType:"json",
			 data:{execData:str},
			 success: function(data,e){
				 if(data==1){
					 toastr.options = {
							 "closeButton": true,
							  "debug": true,
							  "progressBar": true,
							  "preventDuplicates": true,
							  "positionClass": "toast-top-right",
							  "showDuration": "400",
							  "hideDuration": "1000",
							  "timeOut": "1500",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "slideDown",
							  "hideMethod": "slideUp"
				     };
				     toastr.success('',"Test Case Status has been changed to "+$("#tcStatusPut_"+tcid+" option:selected").text());
				     $("#tcOp_"+tcid+"").text("update");
					 $("#tcStatuses_"+tcid+"").text($("#tcStatusPut_"+tcid+" option:selected").text());
					 
						 
					 //table = $("#tblTC").DataTable();
				 }else{
					 toastr.options = {
							 "closeButton": true,
							  "debug": true,
							  "progressBar": true,
							  "preventDuplicates": true,
							  "positionClass": "toast-top-right",
							  "showDuration": "400",
							  "hideDuration": "1000",
							  "timeOut": "1500",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "slideDown",
							  "hideMethod": "slideUp"
				     };
				     toastr.error('',"Execution Error!");
				 }
			 }
			});
 	}
 

 </script>
<script type="text/javascript">
    function goToNewPage()
    {
        var url = document.getElementById('action-select').value;
        if(url != 'none') {
            window.location = url;
        }
    }
</script>


<div class="modal fade" id="filterModal" tabindex="-1" role="dialog"
	aria-labelledby="filterModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">Select checkbox to show column</h3>
			</div>
			<div class="modal-body">
				<form>
					<div class="row">
						<div class="col-md-4 no-padding">
							<select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
								<option selected="">Column</option>
								<option value="1">Status</option>
								<option value="2">Tester</option>
								<option value="3">Module</option>
								<option value="4">Application</option>
							</select>
						</div>
						<div class="col-md-4 no-padding">
							<select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
								<option selected="">Contains</option>

							</select>
						</div>
						<div class="col-md-4 no-padding">
							<select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
								<option selected="">Select Value</option>
								<option value="1">Value</option>
								<option value="2">Value</option>
								<option value="3">Value</option>
							</select>
						</div>

					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Apply</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<script>
$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>x');
            }
        }
        init();
    });
});

jQuery(function($) {
	$.fn.select2.amd.require([
    'select2/selection/single',
    'select2/selection/placeholder',
    'select2/selection/allowClear',
    'select2/dropdown',
    'select2/dropdown/search',
    'select2/dropdown/attachBody',
    'select2/utils'
  ], function (SingleSelection, Placeholder, AllowClear, Dropdown, DropdownSearch, AttachBody, Utils) {

		var SelectionAdapter = Utils.Decorate(
      SingleSelection,
      Placeholder
    );
    
    SelectionAdapter = Utils.Decorate(
      SelectionAdapter,
      AllowClear
    );
          
    var DropdownAdapter = Utils.Decorate(
      Utils.Decorate(
        Dropdown,
        DropdownSearch
      ),
      AttachBody
    );
    
		var base_element = $('.select2-multiple2')
    $(base_element).select2({
    	placeholder: 'Select multiple items',
      selectionAdapter: SelectionAdapter,
      dropdownAdapter: DropdownAdapter,
      allowClear: true,
      templateResult: function (data) {

        if (!data.id) { return data.text; }

        var $res = $('<div></div>');

        $res.text(data.text);
        $res.addClass('wrap');

        return $res;
      },
      templateSelection: function (data) {
      	if (!data.id) { return data.text; }
        var selected = ($(base_element).val() || []).length;
        var total = $('option', $(base_element)).length;
        //return "Selected " + selected + " of " + total;
        return "You Selected ";
      }
    })
  
  });
  
});

</script>

<style>
.select2-results__option .wrap:before {
	font-family: fontAwesome;
	color: #999;
	content: "\f096";
	width: 25px;
	height: 25px;
	padding-right: 10px;
}

.select2-results__option[aria-selected=true] .wrap:before {
	content: "\f14a";
}

/* not required css */
.select2-multiple, .select2-multiple2 {
	width: 220%
}
</style>

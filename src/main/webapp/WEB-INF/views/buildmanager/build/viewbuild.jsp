<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- <div id="spinner"></div> -->

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Test Set</h2>
			<ol class="breadcrumb">
<!-- 			<li><a href="bmdashboard">Dashboard</a></li> -->
			<li><a href="builddetails">Test Set</a></li>
			<li class="active"><strong>Report</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
	<c:if test="${model.automationStatus == 1}">
		<div class="tabs-container">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#tab-1" id="manualBuildTabLink">Manual
						Test Set</a></li>
				<li class=""><a data-toggle="tab" href="#tab-2" id="automationBuildTabLink">Automation
						Test Set</a></li>
						
						<!-- Commenting Scriptless tab -->
						<!--<c:if test="${model.scriptlessStatus == 1}">
				<li class=""><a data-toggle="tab" href="#tab-3" id="scriptlessBuildTabLink">Script Less
						Builds</a></li></c:if>-->
			</ul>
			<div class="tab-content">
				<div id="tab-1" class="tab-pane active">
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-8">
								<div class="ibox">
									<div class="ibox-title">
										<h5>Test Sets</h5>
										<%-- <c:if test="${model.createBuildAccessStatus == 1}">
											<div class="ibox-tools">
												<button type="button" id=create_button1 class="btn btn-success btn-xs"
													onclick="location.href = 'createbuild?type=1';">Create
													new Test Set</button>
											</div>
										</c:if> --%> 
									</div>
									
									<div class="ibox-content">
										<div class="full-height-scroll">
											<div class="table-responsive" style="overflow-x: visible;">
												<table class="table table-striped table-hover"
													id="allBuildTable">
													<thead>
													   <tr>
													   <th>Release</th>
													   </tr>
													</thead>
													<tbody>
													<c:forEach var="releaseDetails" items="${model.releaseDetails}">
														<tr>
														<td><a onclick="viewBuilds(${releaseDetails.release_id})">${releaseDetails.release_name}</a></td>
														</tr>
														<div>
														<tr style="display: none;" id="${releaseDetails.release_id}">
														<td>
														<table class="table table-striped table-hover"
													>
														 <tr>
														 <th class="hidden">Test Set Id</th>
															<th>Test Set Name</th>
															<th>Test Set State</th>
															<th style="text-align: right;">Action</th>
															<th class="hidden">Learning</th>
														 </tr>
														<c:forEach var="data" items="${model.allBuildDetails}" varStatus="loopManual">
														    	<tr>
														     
															<c:if test="${releaseDetails.release_name==data.releaseName}">
													           <td class="hidden">${data.build_id}</td>
																<td class="lgTextAlignment" data-original-title="${data.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_name}</td>
																<td class="clie nt-status"><label
																	class=" label ${data.build_state}">
																	<c:if test="${data.build_state != 6}">${data.buildState}</c:if>
																	<c:if test="${data.build_state == 6}">Ready for Execution</c:if>
																	</label></td>
																<td class="project-actions">
																	<button class="btn btn-white btn-xs"
																		onclick="viewBuild(${data.build_id},'View','${data.build_name}')">Report</button>
																	<button class="btn btn-white btn-xs"
																		onclick="viewBuildEdit(${data.build_id},'Edit','${data.build_name}')">Edit</button>
																	<button class="btn btn-white btn-xs"
																		onclick="removeBuild(${data.build_id})">Delete</button>
																	<c:if test="${data.build_status == 1}">

																		<c:if
																			test="${(data.build_state == 3 || data.build_state == 4 ) && (accessList[0].removetestcases == 1) }">
																			<button class="btn btn-white btn-xs"
																				onclick="view(${data.build_id},'${data.build_name}')">Add/Remove
																				Test cases</button>
																		</c:if>
																		<c:if
																			test="${(model.userID == data.creatorID) || (accessList[0].buildproject == 1)}">
																			<c:if
																				test="${(data.build_state == 1 || data.build_state == 2 ||  data.build_state == 6)}">

																				<button class="btn btn-white btn-xs"
																					onclick="view(${data.build_id},'${data.build_name}')">Add/Remove
																					Test cases</button>
																			</c:if>
																		</c:if>
																		<!--<c:if
																			test="${(model.userID == data.creatorID) || (accessList[0].assignbuild == 1)}">
																			<c:if test="${data.build_state == 6}">
																				<button class="btn btn-white btn-xs"
																					onclick="assign(${data.build_id},'${data.build_name}')">Assign</button>
																			</c:if>
																		</c:if>-->
																		<c:if test="${data.executeFlag > 0}">
																			<button class="btn btn-white btn-xs"
																				onclick="executeBuild(${data.build_id},'${data.build_name}')">Execute</button>
																		</c:if>
																	</c:if>
																	<c:if test="${data.build_state == 5 }">
																		<button class="btn btn-white btn-xs viewLearningBtn" <%-- onclick="viewLearning('${data.build_exe_comments}')" --%>>Details</button>
																	</c:if>
																	
																</td>
																       
 														   </c:if> 
 														   														</tr>
 														   
														</c:forEach>
														</table>
														</td>
														 
														</tr>
														
														</div>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
							<c:if test="${fn:length(allBuildDetails) > 0}">
								<div class="ibox">
									<div class="ibox-content">
										<div class="tab-content" id="extraBuildDetails">
<%-- 											<c:forEach var="data1" items="${allBuildDetails}" varStatus="stat"> --%>
<%-- 												<div id="build_${data1.build_id}" class="tab-pane"> --%>
													<div class="row m-b-lg">
														<div class="col-lg-4 text-center">

															<div class="m-b-sm">
															<c:if test="${stat.first}">
																<img alt="image" class="img-circle getUserImageById1"
																	src="data:image/jpg;base64,${allBuildDetails[0].user_photo}"
																	style="width: 62px">
															</c:if>
															<c:if test="${!stat.first}">
																<img alt="image" class="img-circle getUserImageById1"
																	style="width: 62px">
															</c:if>
															</div>
														</div>
														<div class="col-lg-8">
															<h2
																style="vertical-align: middle; text-align: center; margin-top: 20px;">${data1.creator}</h2>
														</div>
													</div>
													<div class="">
														<div class="full-height-scroll">
															<strong>Test Set Information</strong>
															<ul class="list-group clear-list">
																<li class="list-group-item fist-item"><span
																	class="pull-right"> ${allBuildDetails[0].build_name} </span>Test Set
																	Name</li>
																<li class="list-group-item "><span
																	class="pull-right label statusLabels">
																		${allBuildDetails[0].buildStatus} </span>Test Set Status</li>
																<li class="list-group-item"><span
																	class="pull-right"> ${allBuildDetails[0].releaseName} </span>Test Set
																	Release</li>
																<li class="list-group-item"><span
																	class="pull-right"> ${allBuildDetails[0].build_createdate} </span>Test Set
																	Create Date</li>
																<li class="list-group-item">Test Set Assignee<c:forEach
																		var="assignee" items="${buildAssignee}">
																		<c:if test="${allBuildDetails[0].build_id == assignee.build_id}">
																			<span class="pull-right">
																				${assignee.first_name} ${assignee.last_name}</span>
																			<br>

																		</c:if>
																	</c:forEach></li>
															</ul>
															<strong>Test Set Description</strong>
															<p>${allBuildDetails[0].build_desc}</p>
														</div>
													</div>
<!-- 												</div> -->
<%-- 											</c:forEach> --%>
										</div>
									</div>
								</div>
								</c:if>
							</div>
						</div>
					</div>
				</div>
				<div id="tab-2" class="tab-pane">
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-8">
								<div class="ibox">
									<div class="ibox-title">
										<h5>Test Sets</h5>
										<%-- <c:if test="${model.createBuildAccessStatus == 1}">
											<div class="ibox-tools">
												<button type="button" class="btn btn-success btn-xs"
													onclick="location.href = 'createbuild?type=2';">Create
													new Test Set</button>
											</div>
										</c:if> --%>
									</div> 

                                     

									<div class="ibox-content">
										<div class="full-height-scroll">
											<div class="table-responsive" style="overflow-x: visible;">
												<table class="table table-striped table-hover"
													id="autoBuildTable">
													<thead>
													   <tr>
													   <th>Release</th>
													   </tr>
													</thead>
													<tbody>
													<c:forEach var="releaseDetails" items="${model.releaseDetails}">
														<tr>
														<td><a onclick="viewBuilds1(${releaseDetails.release_id})">${releaseDetails.release_name}</a></td>
														</tr>
														<div>
														<tr style="display: none;" id="${releaseDetails.release_id}--forIddifference">
														<td>
														<table class="table table-striped table-hover"
													>
														 <tr>
														 <th class="hidden">Test Set Id</th>
															<th>Test Set Name</th>
															<th>Test Set State</th>
															<th style="text-align: right;">Action</th>
															<th class="hidden">Learning</th>
														 </tr>
														<c:forEach var="data" items="${model.allAutomationBuilds}">
														    	<tr>
														     
															<c:if test="${releaseDetails.release_name == data.releaseName}">
															
															
													           <td class="hidden">${data.build_id}</td>
																<td class="lgTextAlignment" data-original-title="${data.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_name}</td>
																<td class="client-status"><label
																	class=" label ${data.build_state}">${data.buildState}</label></td>
                                               													<td class="project-actions">
																	<button class="btn btn-white btn-xs"
																		onclick="viewAutoBuild1(${data.build_id},'View','${data.build_name}')">Report</button>
																	<c:if
																			test="${(data.build_state == 1   ||  data.build_state == 2) }">
																	
																	<button class="btn btn-white btn-xs"
																		onclick="viewAutoBuildEdit(${data.build_id},'Edit','${data.build_name}')">Edit</button>
																		</c:if>
																	<c:if test="${data.build_status == 1}">
																		<c:if
																			test="${(data.build_state == 2)}">
																			<button class="btn btn-white btn-xs"
																				onclick="addRemoveAutoBuild(${data.build_id},'${data.build_name}')">Add/Remove
																				Test cases</button>
																		</c:if>
																		<c:if
																			test="${(model.userID == data.creatorID) || (accessList[0].buildproject == 1)}">
																			<c:if
																				test="${(data.build_state == 1 )}">
				
																				<button class="btn btn-white btn-xs"
																					onclick="addRemoveAutoBuild(${data.build_id},'${data.build_name}')">Add/Remove
																					Test cases</button>
																			</c:if>
																		</c:if>
																		
																		<c:if test="${data.build_state == 2}">
																			<button onclick="executeAutoBuild(${data.build_id},'${data.build_name}')" class="btn btn-white btn-xs">Execute</button>
																		</c:if>
																	</c:if>
																</td>
																       
 														   </c:if> 
 														   														</tr>
 														   
														</c:forEach>
														</table>
														</td>
														 
														</tr>
														
														</div>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</div>


								</div>
							</div>

							<div class="col-sm-4">
							<c:if test="${fn:length(allAutomationBuilds) > 0}">
								<div class="ibox">
									<div class="ibox-content">
										<div class="tab-content" id="extraBuildDetails">
<%-- 											<c:forEach var="data1" items="${allAutomationBuilds}" varStatus="stat"> --%>
<%-- 												<div id="build_${data1.build_id}" class="tab-pane"> --%>
													<div class="row m-b-lg">
														<div class="col-lg-4 text-center">

															<div class="m-b-sm">
															<c:if test="${stat.first}">
																<img alt="image" class="img-circle getUserImageById1"
																	src="data:image/jpg;base64,${allAutomationBuilds[0].user_photo}"
																	style="width: 62px">
															</c:if>
															<c:if test="${!stat.first}">
																<img alt="image" class="img-circle getUserImageById1"
																	style="width: 62px">
															</c:if>
															</div>
														</div>
														<div class="col-lg-8">
															<h2
																style="vertical-align: middle; text-align: center; margin-top: 20px;">${data1.creator}</h2>
														</div>
													</div>
													<div class="">
														<div class="full-height-scroll">
															<strong>Test Set Information</strong>
															<ul class="list-group clear-list">
																<li class="list-group-item fist-item"><span
																	class="pull-right"> ${allAutomationBuilds[0].build_name} </span>Test Set
																	Name</li>
																<li class="list-group-item "><span
																	class="pull-right label statusLabels">
																		${allAutomationBuilds[0].buildStatus} </span>Test Set Status</li>
																<li class="list-group-item"><span
																	class="pull-right"> ${allAutomationBuilds[0].releaseName} </span>Test Set
																	Release</li>
																<li class="list-group-item"><span
																	class="pull-right"> ${allAutomationBuilds[0].build_createdate} </span>Test Set
																	Create Date</li>
																<li class="list-group-item">Test Set Assignee<c:forEach
																		var="assignee" items="${buildAssignee}">
																		<c:if test="${allAutomationBuilds[0].build_id == assignee.build_id}">
																			<span class="pull-right">
																				${assignee.first_name} ${assignee.last_name}</span>
																			<br>

																		</c:if>
																	</c:forEach></li>
															</ul>
															<strong>Test Set Description</strong>
															<p>${allAutomationBuilds[0].build_desc}</p>
														</div>
													</div>
<!-- 												</div> -->
<%-- 											</c:forEach> --%>
										</div>
									</div>
								</div>
								</c:if>
							</div>

						</div>
					</div>
				</div>
				<c:if test="${model.scriptlessStatus == 1}">
				<div id="tab-3" class="tab-pane">
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-8">
								<div class="ibox">
									<div class="ibox-title">
										<h5>Builds</h5>
										<c:if test="${model.createBuildAccessStatus == 1}">
											<div class="ibox-tools">
												<button type="button" class="btn btn-success btn-xs"
													onclick="location.href = 'createbuild?type=3';">Create
													new Build</button>
											</div>
										</c:if>
									</div>
									<div class="ibox-content">
										<div class="full-height-scroll">
											<div class="table-responsive" style="overflow-x: visible;">
												<table class="table table-striped table-hover"
													id="scriptBuildTable">
													<thead>
														<tr>
															<th class="hidden">Build Id</th>
															<th>Build Name</th>
															<th>Build State</th>
															<th style="text-align: right;">Action</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="data" items="${model.allScriptlessBuilds}">
															<tr data-toggle="tab" href="#scriptBuild_${data.build_id}" onclick="getUserImageById3(${data.creatorID})"
																style="cursor: pointer">
																<td class="hidden">${data.build_id}</td>
																<td class="lgTextAlignment" data-original-title="${data.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_name}</td>
																<td class="client-status"><label
																	class=" label ${data.build_state}">${data.buildState}</label></td>
																<td class="project-actions">
																	<button class="btn btn-white btn-xs hidden"
																		onclick="viewAutoBuild(${data.build_id},'View','${data.build_name}')">Report</button>
																	<c:if
																			test="${(data.build_state == 1   ||  data.build_state == 2) }">
																	
																	<button class="btn btn-white btn-xs hidden"
																		onclick="viewAutoBuildEdit(${data.build_id},'Edit','${data.build_name}')">Edit</button>
																		</c:if>
																	<c:if test="${data.build_status == 1}">
																		<c:if
																			test="${(data.build_state == 2)}">
																			<button class="btn btn-white btn-xs"
																				onclick="addRemoveScriptBuild(${data.build_id},'${data.build_name}',${data.build_env})">Add/Remove
																				Test cases</button>
																		</c:if>
																		<c:if
																			test="${(model.userID == data.creatorID) || (accessList[0].buildproject == 1)}">
																			<c:if
																				test="${(data.build_state == 1 )}">
				
																				<button class="btn btn-white btn-xs"
																					onclick="addRemoveScriptBuild(${data.build_id},'${data.build_name}',${data.build_env})">Add/Remove
																					Test cases</button>
																			</c:if>
																		</c:if>
																		
																		<c:if test="${data.build_state == 3}">
												 							<button onclick="executeScriptBuild(${data.build_id},'${data.build_name}')" class="btn btn-white btn-xs">Execute</button>
																		</c:if>
																	</c:if>
																</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
							<c:if test="${fn:length(allScriptlessBuilds) > 0}">
								<div class="ibox">
									<div class="ibox-content">
										<div class="tab-content" id="extraBuildDetails">
											<c:forEach var="data" items="${allScriptlessBuilds}"  varStatus="loopScript">
												<div id="scriptBuild_${data.build_id}" class="tab-pane">
													<div class="row m-b-lg">
														<div class="col-lg-4 text-center">

															<div class="m-b-sm">
																<c:if test="${loopScript.first}">
															<img alt="image" class="img-circle getUserImageById3"
																	src="data:image/jpg;base64,${allScriptlessBuilds[0].user_photo}"
																	style="width: 62px">
															</c:if>
															<c:if test="${!loopScript.first}">
																<img alt="image" class="img-circle getUserImageById3"
																	style="width: 62px">
															</c:if>
															</div>
														</div>
														<div class="col-lg-8">
															<h2
																style="vertical-align: middle; text-align: center; margin-top: 20px;">${data.creator}</h2>
														</div>
													</div>
													<div class="">
														<div class="full-height-scroll">
															<strong>Build Information</strong>
															<ul class="list-group clear-list">
																<li class="list-group-item fist-item"><span
																	class="pull-right"> ${data.build_name} </span>Build
																	Name</li>
																<li class="list-group-item "><span
																	class="pull-right label statusLabels">
																		${data.buildStatus} </span>Build Status</li>
																<li class="list-group-item"><span
																	class="pull-right"> ${data.releaseName} </span>Build
																	Release</li>
																<li class="list-group-item"><span
																	class="pull-right"> ${data.build_createdate} </span>Build
																	Create Date</li>
															</ul>
															<strong>Build Description</strong>
															<p>${data.build_desc}</p>
														</div>
													</div>
												</div>
											</c:forEach>
										</div>
									</div>
								</div>
								</c:if>
							</div>
						</div>
					</div>
				</div></c:if>
			</div>
		</div>

	</c:if>
	<c:if test="${model.automationStatus != 1}">
		<div class="row">
			<div class="col-sm-8">
				<div class="ibox">
					<div class="ibox-title">
						<h5>Builds</h5>
						<c:if test="${model.createBuildAccessStatus == 1}">
							<div class="ibox-tools">
								<button type="button" class="btn btn-success btn-xs"
									onclick="location.href = 'createbuild';">Create new
									Build ${Model.automationStatus}</button>
							</div>
						</c:if>
					</div>
					<div class="ibox-content">
						<div class="full-height-scroll">
							<div class="table-responsive" style="overflow-x: visible;">
								<table class="table table-striped table-hover"
									id="allBuildTable">
									<thead>
										<tr>
											<th class="hidden">Build Id</th>
											<th>Build Name</th>
											<th>Build State</th>
											<th style="text-align: right;">Action</th>
											<th class="hidden">Learning</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="data" items="${model.allBuildDetails}">
											<tr data-toggle="tab" href="#build_${data.build_id}" onclick="getUserImageById1(${data.creatorID})"
												style="cursor: pointer">
												<td class="hidden">${data.build_id}</td>
												<td>${data.build_name}</td>
												<td class="client-status"><label
													class=" label ${data.build_state}">${data.buildState}</label></td>
												<td class="project-actions">
													<button class="btn btn-white btn-xs"
														onclick="viewBuild(${data.build_id},'View','${data.build_name}')">Report</button>
													<button class="btn btn-white btn-xs"
														onclick="viewBuildEdit(${data.build_id},'Edit','${data.build_name}')">Edit</button>
													<c:if test="${data.build_status == 1}">

														<c:if
															test="${(data.build_state == 3 || data.build_state == 4 ) && (accessList[0].removetestcases == 1) }">
															<button class="btn btn-white btn-xs"
																onclick="view(${data.build_id},'${data.build_name}')">Add/Remove
																Test cases</button>
														</c:if>
														<c:if
															test="${(model.userID == data.creatorID) || (accessList[0].buildproject == 1)}">
															<c:if
																test="${(data.build_state == 1 || data.build_state == 2 ||  data.build_state == 6)}">

																<button class="btn btn-white btn-xs"
																	onclick="view(${data.build_id},'${data.build_name}')">Add/Remove
																	Test cases</button>
															</c:if>
														</c:if>
														<c:if
															test="${(model.userID == data.creatorID) || (accessList[0].assignbuild == 1)}">
															<c:if test="${data.build_state == 6}">
																<button class="btn btn-white btn-xs"
																	onclick="assign(${data.build_id},'${data.build_name}')">Assign</button>
															</c:if>
														</c:if>
														<c:if test="${data.executeFlag > 0}">
															<button class="btn btn-white btn-xs"
																onclick="executeBuild(${data.build_id},'${data.build_name}')">Execute</button>
														</c:if>
													</c:if>
													<c:if test="${data.build_state == 5 }">
														<button class="btn btn-white btn-xs viewLearningBtn" <%-- onclick="viewLearning('${data.build_exe_comments}')" --%>>Details</button>
													</c:if>
												</td>
												<td class="hidden buildLearning">${data.build_exe_comments}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
			<c:if test="${fn:length(allBuildDetails) > 0}">
				<div class="ibox">
					<div class="ibox-content">
						<div class="tab-content" id="extraBuildDetails">
							<c:forEach var="data" items="${allBuildDetails}">
								<div id="build_${data.build_id}" class="tab-pane">
									<div class="row m-b-lg">
										<div class="col-lg-4 text-center">

											<div class="m-b-sm">
											<c:if test="${loop.first }">
												<img alt="image" class="img-circle getUserImageById1"
													src="data:image/jpg;base64,${allBuildDetails[0].user_photo}"
													style="width: 62px">
											</c:if>
											<c:if test="${!loop.first }">
												<img alt="image" class="img-circle getUserImageById1"
													style="width: 62px">
											</c:if>
											
											</div>
										</div>
										<div class="col-lg-8">
											<h2
												style="vertical-align: middle; text-align: center; margin-top: 20px;">${data.creator}</h2>
										</div>
									</div>
									<div class="">
										<div class="full-height-scroll">
											<strong>Build Information</strong>
											<ul class="list-group clear-list">
												<li class="list-group-item fist-item"><span
													class="pull-right"> ${data.build_name} </span>Build Name</li>
												<li class="list-group-item "><span
													class="pull-right label statusLabels">
														${data.buildStatus} </span>Build Status</li>
												<li class="list-group-item"><span class="pull-right">
														${data.releaseName} </span>Build Release</li>
												<li class="list-group-item"><span class="pull-right">
														${data.build_createdate} </span>Build Create Date</li>
												<li class="list-group-item">Build Assignee<c:forEach
														var="assignee" items="${buildAssignee}">
														<c:if test="${data.build_id == assignee.build_id}">
															<span class="pull-right"> ${assignee.first_name}
																${assignee.last_name}</span>
															<br>

														</c:if>
													</c:forEach></li>
											</ul>
											<strong>Build Description</strong>
											<p>${data.build_desc}</p>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
				</c:if>
			</div>
		</div>
	</c:if>
</div>
</div>
<input type="hidden" id="hiddenInput"
	value=<%=session.getAttribute("buildCreateStatus")%>>


<input type="hidden" id="newBuildName"
	value='<%=session.getAttribute("newBuildName")%>'>

<input type="hidden" id="newBuildTypeForTab"
	value=<%=session.getAttribute("newBuildTypeForTab")%>>

<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	localStorage.setItem('activePage', "0");
	localStorage.setItem('activeTc', "0");
	localStorage.setItem('SelectedProject', "0");
	localStorage.setItem('SelectedModule', "0");
	localStorage.setItem('SelectedScenario', "0");
	localStorage.setItem('AutoSelectedProject', "0");
	localStorage.setItem('AutoSelectedModule', "0");
	localStorage.setItem('AutoSelectedScenario', "0");
	
	
	 var buildType= "<%= request.getParameter("buildType") %>";
	 if(buildType != null){
		 if(buildType == "automation"){
			 $('#automationBuildTabLink').click();
		 }else  if(buildType == "scriptless"){
			 $('#scriptlessBuildTabLink').click();
		 }
	 }
	 
});
</script>
<script>
$(function() {
	
	var state = $('#hiddenInput').val();
	var newBuildName = $('#newBuildName').val();
	if(state == 1){
		var message =" Build is successfully created";
		var toasterMessage = newBuildName +  message;
		showToster(toasterMessage);
	}
	if(state==2)
		{
			var message =" Build is successfully updated";
			var toasterMessage = newBuildName +  message;
			showToster(toasterMessage);
		}
	$('#hiddenInput').val(3);
	
	var newBuildTypeForTab = $('#newBuildTypeForTab').val();

	if(newBuildTypeForTab == "automation"){
		//set the automation tab active
		$('#automationBuildTabLink').click();
	}
	if(newBuildTypeForTab == "scriptless"){
		//set the automation tab active
		$('#scriptlessBuildTabLink').click();
	}
	
	
	var manualBuild = $('#allBuildTable').DataTable({
		dom : '<"html5buttons"B>lTfgitp',
		buttons : [],
		"paging" : true,
		"lengthChange" : false,
		"searching" : true,
		"ordering" : false
	});
	
	var autoBuild = $('#autoBuildTable').DataTable({
		dom : '<"html5buttons"B>lTfgitp',
		buttons : [],
		"paging" : true,
		"lengthChange" : false,
		"searching" : true,
		"ordering" : false
	});
	
	var scriptBuild = $('#scriptBuildTable').DataTable({
		dom : '<"html5buttons"B>lTfgitp',
		buttons : [],
		"paging" : false,
		"lengthChange" : false,
		"searching" : true,
		"ordering" : false
	});
	
	
	//to show first build details
	var firstBuildId = $('#allBuildTable > tbody > tr').first().find('td:first').text();
	$('#build_'+firstBuildId).addClass('active');
	var autoBuildId = $('#autoBuildTable > tbody > tr').first().find('td:first').text();
	$('#autoBuild_'+autoBuildId).addClass('active');
	var scriptBuildId = $('#scriptBuildTable > tbody > tr').first().find('td:first').text();
	$('#scriptBuild_'+scriptBuildId).addClass('active');
	
	$('.statusLabels').each(function() {
		var status = $.trim($(this).text());
		if (status.toLowerCase() == 'active') {
			$(this).addClass('label-primary');
		} else {
			$(this).addClass('label-danger');
		}
	});
	
	var allManualRows =  manualBuild.$(".label", {"page": "all"});
	allManualRows.each(function(){
		if($(this).hasClass('1'))
			$(this).addClass('label-success');
		else if($(this).hasClass('2'))
			$(this).addClass('label-info');
		else if($(this).hasClass('3'))
			$(this).addClass('label-warning');
		else if($(this).hasClass('4'))
			$(this).addClass('label-danger');
		else if($(this).hasClass('5'))
			$(this).addClass('label-primary');
		else if($(this).hasClass('6'))
			$(this).addClass('label-warning');
	});
	
	var allAutoRows =  autoBuild.$(".label", {"page": "all"});
	allAutoRows.each(function(){
		if($(this).hasClass('1'))
			$(this).addClass('label-success');
		else if($(this).hasClass('2'))
			$(this).addClass('label-info');
		else if($(this).hasClass('3'))
			$(this).addClass('label-warning');
		else if($(this).hasClass('4'))
			$(this).addClass('label-danger');
		else if($(this).hasClass('5'))
			$(this).addClass('label-primary');
	});
	
	var allScriptRows =  scriptBuild.$(".label", {"page": "all"});
	allScriptRows.each(function(){
		if($(this).hasClass('1'))
			$(this).addClass('label-success');
		else if($(this).hasClass('2'))
			$(this).addClass('label-info');
		else if($(this).hasClass('3'))
			$(this).addClass('label-warning');
		else if($(this).hasClass('4'))
			$(this).addClass('label-danger');
		else if($(this).hasClass('5'))
			$(this).addClass('label-primary');
	});
	
	 var manual= <%= request.getParameter("manual") %>;
	 if(manual == null){
		 manual = 1;
	 }
	 
	 var auto= <%= request.getParameter("auto") %>;
	 if(auto == null){
		 auto = 1;
	 }
		 
	 var sc= <%= request.getParameter("sc") %>;
	 if(sc == null){
		 sc = 1;
	 }
	 
	 var manualBuildCount='${model.manualBuildCount}';

	 var pageSize='${model.pageSize}';
	 
	 var manualLastRec=((manual-1)*pageSize+parseInt(pageSize));

	 if(manualLastRec>manualBuildCount)
	 	manualLastRec=manualBuildCount;
	 var manualShowCount = ((manual-1)*pageSize+1);
	 if(manualShowCount <= manualLastRec){
		 $("#allBuildTable_info").html("Showing "+manualShowCount+" to "+manualLastRec+" of "+manualBuildCount+" entries ");// + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="manualPrevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="manualNextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
	 }else{
		 $("#allBuildTable_info").html("");
	 }
	 
	 if(manual == 1){
		 $("#manualPrevBtn").attr('disabled','disabled');
	 }
	 
	 if(manualLastRec == manualBuildCount){
		 $("#manualNextBtn").attr('disabled','disabled');
	 }
	 
	    $("#manualPrevBtn").click(function() {
	    	$("#barInMenu").removeClass("hidden");
	    	$("#wrapper").addClass("hidden");
	    	
	    	if(manual == 1)
	    		window.location.href = "viewbuild?manual=1&auto="+auto+"&sc="+sc;
	    	else
	    		window.location.href = "viewbuild?manual="+(manual - 1)+"&auto="+auto+"&sc="+sc;
		});
		
		$("#manualNextBtn").click(function(){
			$("#barInMenu").removeClass("hidden");
	    	$("#wrapper").addClass("hidden");
	    	
			window.location.href = "viewbuild?manual="+(manual + 1)+"&auto="+auto+"&sc="+sc;
		});
		
		/**
		* this is code for automation builds
		*
		*/
		
		var autobuildcount='${model.autobuildcount}';

		 var pageSize='${model.pageSize}';
		 
		 var autoLastRec=((auto-1)*pageSize+parseInt(pageSize));

		 if(autoLastRec>autobuildcount)
		 	autoLastRec=autobuildcount;
		 var autoShowCount = ((auto-1)*pageSize+1);
		 if(autoShowCount <= autoLastRec){
			 $("#autoBuildTable_info").html("Showing "+autoShowCount+" to "+autoLastRec+" of "+autobuildcount+" entries "); //+ '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="autoPrevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="autoNextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
		 }else{
			 $("#autoBuildTable_info").html("");
		 }
		 
		 if(auto == 1){
			 $("#autoPrevBtn").attr('disabled','disabled');
		 }
		 
		 if(autoLastRec == autobuildcount){
			 $("#autoNextBtn").attr('disabled','disabled');
		 }
		 
		    $("#autoPrevBtn").click(function() {
		    	$("#barInMenu").removeClass("hidden");
		    	$("#wrapper").addClass("hidden");
		    	
		    	if(auto == 1)
		    		window.location.href = "viewbuild?manual="+manual+"&auto=1&sc="+sc+"&buildType=automation";
		    	else
		    		window.location.href = "viewbuild?manual="+manual+"&auto="+(auto - 1)+"&sc="+sc+"&buildType=automation";
			});
			
			$("#autoNextBtn").click(function(){
				$("#barInMenu").removeClass("hidden");
		    	$("#wrapper").addClass("hidden");
		  		
				window.location.href = "viewbuild?manual="+manual+"&auto="+(auto + 1)+"&sc="+sc+"&buildType=automation";
			});
			
			/**
			* this code is for scriptles
			*/
			
			var scbuildcount='${model.scbuildcount}';

			 var pageSize='${model.pageSize}';
			 
			 var scLastRec=((sc-1)*pageSize+parseInt(pageSize));

			 if(scLastRec>scbuildcount)
			 	scLastRec=scbuildcount;
			 var scShowCount = ((sc-1)*pageSize+1);
			 if(scShowCount <= scLastRec){
				 $("#scriptBuildTable_info").html("Showing "+scShowCount+" to "+scLastRec+" of "+scbuildcount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="scPrevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="scNextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
			 }else{
				 $("#scriptBuildTable_info").html("");
			 }
			 
			 if(sc == 1){
				 $("#scPrevBtn").attr('disabled','disabled');
			 }
			 
			 if(scLastRec == scbuildcount){
				 $("#scNextBtn").attr('disabled','disabled');
			 }
			 
			    $("#scPrevBtn").click(function() {
			    	$("#barInMenu").removeClass("hidden");
			    	$("#wrapper").addClass("hidden");
			    	
			    	if(sc == 1)
			    		window.location.href = "viewbuild?manual="+manual+"&auto="+auto+"&sc=1"+"&buildType=scriptless";
			    	else
			    		window.location.href = "viewbuild?manual="+manual+"&auto="+auto+"&sc="+(sc - 1)+"&buildType=scriptless";
				});
				
				$("#scNextBtn").click(function(){
					$("#barInMenu").removeClass("hidden");
			    	$("#wrapper").addClass("hidden");
			    	
					window.location.href = "viewbuild?manual="+manual+"&auto="+auto+"&sc="+(sc + 1)+"&buildType=scriptless";
				});
});

function viewBuild(id,viewType,buildName){
	
	
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	 
	var posting = $.post('setexecutebuildid',{
		buildID : id,
		buildName : buildName
	});
	posting.done(function(data){
		window.location.href="viewreport";
	});
	
}


function viewBuildEdit(id,viewType,buildName){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setbuildid',{
		buildID : id,
		viewType : viewType,
		buildName:buildName
	});
	posting.done(function(data){
		window.location.href="editbuild";
	}); 

}

function viewAutoBuild(id,viewType,buildName){
	
	/* $('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setautobuildid',{
		buildID : id,
		viewType : viewType,
		buildName:buildName
	});
	posting.done(function(data){
		window.location.href="editbuild";
	}); */
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	 
	 var posting = $.post('setautoexecutebuildid',{
			buildID : id
	});
	posting.done(function(data){
		window.location.href="viewautomationreport";
	});
}

function viewAutoBuild1(id,viewType,buildName){
	
	/* $('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setautobuildid',{
		buildID : id,
		viewType : viewType,
		buildName:buildName
	});
	posting.done(function(data){
		window.location.href="editbuild";
	}); */
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	 
	 var posting = $.post('setautoexecutebuildid',{
			buildID : id
	});
	posting.done(function(data){
		window.location.href="viewautomationreport";
	});
}

function viewAutoBuildEdit(id,viewType,buildName){
	
	 $('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setautobuildid',{
		buildID : id,
		viewType : viewType,
		buildName:buildName
	});
	posting.done(function(data){
		window.location.href="editbuild";
	}); 
}

function executeBuild(id,buildName){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setexecutebuildid',{
		buildID : id,
		buildName:buildName
	});
	posting.done(function(data){
		window.location.href="executebuild";
	});
}

function view(buildId,buildName){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setBuildSession', {
		buildId : buildId,
		buildName:buildName
	});
	
	 posting.done(function(data) {
			window.location.href="buildproject"; 
	 });
}

function addRemoveAutoBuild(buildId,buildName){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setautobuildsession', {
		buildId : buildId,
		buildName:buildName
	});
	
	 posting.done(function(data) {
			window.location.href="buildexecute"; 
	 });
}

function addRemoveScriptBuild(buildId,buildName,envId){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setscriptbuildsession', {
		buildId : buildId,
		buildName:buildName,
		envId:envId
	});
	
	 posting.done(function(data) {
			window.location.href="buildaddtc"; 
	 });
}

function executeAutoBuild(buildId,buildName){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setautobuildsession', {
		buildId : buildId,
		buildName:buildName
	});
	
	 posting.done(function(data) {
			window.location.href="autoexecute"; 
	 });
}

function executeScriptBuild(buildId,buildName){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setscriptexecbuildsession', {
		buildId : buildId,
		buildName:buildName
	});
	
	 posting.done(function(data) {
			window.location.href="sfdcbuildexecute"; 
	 });
}

function assign(buildId,buildName){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setAssignBuildSession', {
		buildId : buildId,
		buildName:buildName
	});
	
	 posting.done(function(data) { 
			window.location.href="assignbuild"; 
	 });
}

function showToster(toasterMessage){
	 toastr.options = {
			 "closeButton": true,
			  "debug": true,
			  "progressBar": true,
			  "preventDuplicates": true,
			  "positionClass": "toast-top-right",
			  "showDuration": "400",
			  "hideDuration": "1000",
			  "timeOut": "9000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "slideDown",
			  "hideMethod": "slideUp"
     };
     toastr.success('',toasterMessage);
} 	

function getUserImageById1(userId)
{
	var image;
	var posting = $.post('getUserImageById', {
		userId : userId
	});	
	 posting.done(function(data) { 
		 $(".getUserImageById1").attr("src", "data:image/jpg;base64,"+data);
	 });
	
	
}

function getUserImageById2(userId)
{
	var image;
	var posting = $.post('getUserImageById', {
		userId : userId
	});	
	 posting.done(function(data) { 
		 $(".getUserImageById2").attr("src", "data:image/jpg;base64,"+data);
	 });
	
	
}

function getUserImageById3(userId)
{
	var image;
	var posting = $.post('getUserImageById', {
		userId : userId
	});	
	 posting.done(function(data) { 
		 $(".getUserImageById3").attr("src", "data:image/jpg;base64,"+data);
	 });
	
	
}


function viewLearning(learning){
	
	swal({
		title: "Details",
		text : learning,
		html: true,
		confirmButtonColor : "#1c84c6"
	});
}

function removeBuild(build_id){
	
	swal({
		title : "Are you sure?",
		text : "Do you want to remove build!",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "Yes, Remove it!",
		cancelButtonText : "No, cancel!",
		closeOnConfirm : true,
		closeOnCancel : true
	}, function(isConfirm) {

		if (isConfirm) {
			
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");
			
			var posting = $.post('viewbuild',{
				buildId : build_id
			});
			
			posting.done(function(data){
				window.location.href = "viewbuild";
			}); 	
		}
	});
	
}

$('.viewLearningBtn').click(function(){
	
	swal({
		title: "Details",
		text : $(this).parent('td').next().html(),
		html: true,
		confirmButtonColor : "#1c84c6"
	});
	
});
</script>

<%
	//set session variable to another value
	session.setAttribute("buildCreateStatus", 3);

	session.setAttribute("newBuildTypeForTab",99);
%>

<style>
.collapsible {

  color: white;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
}

.active, .collapsible:hover {
  background-color: ;
}

.content {
  padding: 0 18px;
  display: none;
  overflow: hidden;
  
}
</style>

<script>
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}
function viewBuilds(id){
	var curStyle=document.getElementById(id).style.display;
	if(curStyle=='none')
	{
		document.getElementById(id).style.display='block';
	}else{
		document.getElementById(id).style.display='none';

	}
}
function viewBuilds1(id){
	var curStyle=document.getElementById(id+'--forIddifference').style.display;
	if(curStyle=='none')
	{
		document.getElementById(id+'--forIddifference').style.display='block';
	}else{
		document.getElementById(id+'--forIddifference').style.display='none';

	}
}
</script>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="bar" id="barInMenu">
  <p>loading</p>
</div>

<div id="wrapper" class="hidden">
	<nav class="navbar-default navbar-static-side " role="navigation">
		<div class="sidebar-collapse">
			<ul class="nav metismenu" id="side-menu">			
				<li class="nav-header">
					<div class="dropdown profile-element">
						<span> <img alt="image" class="img-circle"
							style="width: 48px; height: 48px"
							src="data:image/jpg;base64,${Model.UserProfilePhoto }" />
						</span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
							class="clear"> <span class="block m-t-xs"> <strong
									class="font-bold">${Model.userName}</strong>
							</span>
						</span>
						</a> <span class="text-muted text-xs block">${Model.role}<b
							class=""></b></span>
					</div>
					<div class="logo-element" style="padding: 14px !important">
						<a href="#" onclick="gotoLinkInMenu('xedashboard')">
						<img alt="image" class=""
							src="<%=request.getContextPath()%>/resources/img/xenon.png" />
						</a>
					</div>
				</li>
				
				<li><a href="buildproject"><i class="fa fa-th-large"></i>
						<span class="nav-label">Applications</span></a></li>
				<c:forEach var="projectsList" items="${Model.projectsList}">
					<li class="projectNameClick"
						id="projectMenu_${projectsList.project_id }" name="${projectsList.project_id }"><a href="#" id="projectHref_${projectsList.project_id}" class="menuTooltip"
						data-original-title='${projectsList.project_name}' data-container='body' data-toggle='tooltip' data-placement='right'
						onclick="projectClick(${projectsList.project_id })"><i
							class="fa fa-th-large"></i> <span class="smMenuTextAlignment menuSpan" style="margin-right: 5px;">${projectsList.project_name}</span>
							<!--  <span id="projectCount_${projectsList.project_id }"
							class="label label-info ">0</span>-->
							<span class="fa arrow pull-right"></span></a>

						<ul class="nav nav-second-level collapse">
							<c:forEach var="modulesList" items="${Model.modulesList}"
								varStatus="moduleCounter">
								<c:if
									test="${projectsList.project_id==modulesList.project_id}">
									<li class=""
										id="moduleMenu_${modulesList.module_id}"><a href="#" id="moduleHref_${modulesList.module_id}" class="menuTooltip"
										data-original-title='${modulesList.module_name}' data-container='body' data-toggle='tooltip' data-placement='right'
										onclick="moduleClick(${modulesList.module_id})"><label class="smMenuTextAlignment menuSpan" style="margin-right: 5px;">${modulesList.module_name}</label>

										<!--  <span class="label label-success " id="moduleCount_${modulesList.module_id}">0</span>-->

										<%-- <span class="label label-success " id="moduleCount_${modulesList.module_id}">0</span> --%>

											<span class="fa arrow pull-right"></span></a>

										<ul class="nav nav-third-level " style="margin-left:10px;" id="moduleUl_${modulesList.module_id}">
											<c:forEach var="scenarioList" items="${Model.scenarioList}">
												<c:if
													test="${modulesList.module_id==scenarioList.module_id}">
													<li class=""
														id="scenarioMenu_${scenarioList.scenario_id}">
														<a href="#" class="menuTooltip" id="scenarioHref_${scenarioList.scenario_id}"
														data-original-title='${scenarioList.scenario_name}' data-container='body' data-toggle='tooltip' data-placement='right'
														onclick="scenarioClick(${scenarioList.scenario_id})"><label class="smMenuTextAlignment menuSpan" style="margin-right: 5px;">${scenarioList.scenario_name}</label>

														<!--  <span class="label label-warning"
															id="scenarioCount_${scenarioList.scenario_id}">0</span>--></a></li>

														<%-- <span class="label label-warning"
															id="scenarioCount_${scenarioList.scenario_id}">0</span> --%></a></li>

												</c:if>
											</c:forEach>


										</ul></li>
								</c:if>
							</c:forEach>
						</ul></li>
				</c:forEach>
				
				<!-- Commenting Document Library -->
				<!--  
				<li class="mainLi"><a href="documentlibrary"><i
						class="fa fa-files-o"></i> <span class="nav-label">Document
							Library</span></a></li>
				-->
			</ul>

		</div>
	</nav>
<script type="text/javascript">
$(function () {
	//$("#custLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	
	//tooltip
	$('[data-toggle="tooltip"]').tooltip(); 
});

	function gotoLinkInMenu(link)//link
	{
		$('body').addClass('white-bg');
		$("#barInMenu").removeClass('hidden');
		$("#wrapper").addClass("hidden");
		window.location.href = link; 
	}

</script>
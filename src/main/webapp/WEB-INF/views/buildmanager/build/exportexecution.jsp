<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div>
	<table
		class="table table-striped table-bordered table-hover dataTables-example"
		id="tblExport">
		<thead>
			<tr>
				
				<th style="text-align: center"><b>Id</b></th>
				<th style="text-align: center"><b>Title</b></th>
				<th style="text-align: center"><b>Summary</b></th>
				<th style="text-align: center"><b>Status<b></b></th>
				<th style="text-align: center"><b>Tester</b></th>
				<th style="text-align: center"><b>Bugs</b></th>
				<th style="text-align: center"><b>Application</b></th>
				<th style="text-align: center"><b>Module</b></th>
				<th style="text-align: center"><b>Scenario</b></th>
				<th style="text-align: center"><b>Attachments</b></th>
			</tr>

		</thead>
		<tbody>
			<c:forEach var="projectsList" items="${Model.projectsList}">
				<c:forEach var="modulesList" items="${Model.modulesList}">
					<c:forEach var="scenarioList" items="${Model.scenarioList}">
						<c:forEach var="testcasesList" items="${Model.testcasesList}"
							varStatus="testCase">

							<c:if test="${projectsList.project_id==modulesList.project_id}">
								<c:if test="${modulesList.module_id==scenarioList.module_id}">
									<c:if
										test="${scenarioList.scenario_id==testcasesList.scenario_id}">
										<tr
											id="scenario_${testcasesList.scenario_id}_testCase_${testcasesList.testcase_id}">
											
											<td>${testcasesList.test_prefix}</td>

											<td>${testcasesList.testcase_name}</td>

											<td>${testcasesList.summary}</td>

											<td><c:forEach var="statusUser" items="${Model.statusUser}">
													<c:if test="${statusUser.tc_id == testcasesList.testcase_id}">
														<span>${statusUser.description }</span>
													</c:if>
												</c:forEach>
											</td>
												
											<td>
												<c:forEach var="statusUser" items="${Model.statusUser}">
													<c:if test="${statusUser.tc_id == testcasesList.testcase_id}">
														${statusUser.first_name}&nbsp;${statusUser.last_name}
													</c:if>
												</c:forEach>
											</td>

											<td><ul style="padding: 0; list-style-position: inside;">
													<c:forEach var="step_bug" items="${Model.step_bug}">
														<c:if
															test="${step_bug.test_case_id == testcasesList.testcase_id}">
															<li>Bug # ${step_bug.bug_prefix}<br></li>
														</c:if>
													</c:forEach>
												</ul>
												</td>

											<td id="projectid_${testcasesList.testcase_id}"
												value="${projectsList.project_id}">${projectsList.project_name}</td>

											<td id="moduleid_${testcasesList.testcase_id}"
												value="${modulesList.module_id}">${modulesList.module_name}</td>

											<td id="scenarioid_${testcasesList.testcase_id}"
												value="${scenarioList.scenario_id}">${scenarioList.scenario_name}</td>

											<td>

												<div class="form-group col-lg-12">
													<span id="tcFileTable_${testcasesList.testcase_id}"
														class="pull-left"></span>
												</div> <c:forEach var="attachmentsList"
													items="${Model.attachmentsList}">
													<c:if
														test="${attachmentsList.testcase_id == testcasesList.testcase_id}">

														<span class="glyphicon glyphicon-download-alt"
															onclick="downloadFile(${attachmentsList.attach_id},'${attachmentsList.attachment_title}')"
															style="cursor: pointer">&nbsp;${attachmentsList.attachment_title}</span>

													</c:if>
												</c:forEach>
											</td>
										</tr>
									</c:if>
								</c:if>
							</c:if>

						</c:forEach>
					</c:forEach>
				</c:forEach>
			</c:forEach>
		</tbody>
	</table>

</div>

<script type="text/javascript">
$(window).load(function() {
	$('body').addClass("white-bg");
	
	var table=$("#tblExport").DataTable(
			{
				dom : '<"html5buttons"B>lT',
				buttons : [
						{
							extend : 'csv',
							title : 'Test Execution'
							
						},
						{
							extend : 'excel',
							title : 'Test Execution'
							
						},
						{
							extend : 'pdf',
							title : 'Test Execution'
						},

						{
							extend : 'print',
							customize : function(win) {
								$(win.document.body).addClass('white-bg');
								$(win.document.body).css('font-size',
										'10px');

								$(win.document.body).find('table')
										.addClass('compact').css(
												'font-size', 'inherit');
							}
						} ],
						"paging" : false,
						"lengthChange" : false
			
		});
});


</script>

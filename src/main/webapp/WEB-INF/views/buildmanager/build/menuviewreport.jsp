<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="bar" id="barInMenu">
  <p>loading</p>
</div>
<div id="wrapper" class="">
	<nav class="navbar-default navbar-static-side" role="navigation">
		<div class="sidebar-collapse">
			<ul class="nav metismenu" id="side-menu">
				<li class="nav-header">
                    <div class="dropdown profile-element text-center">
                        <a href="#" onclick="gotoLink('xedashboard')" >
                        <img src="<%=request.getContextPath()%>/resources/img/Jade-Logo.png" class="xenon-logo">
                        </a>
                    <!--    <img class="img-circle" style="width:48px; height:48px" src="resources/img/user.jpg">
                         <img alt="image" class="img-circle"
                            style="width: 48px; height: 48px"
                            src="data:image/jpg;base64,${Model.UserProfilePhoto }" />
                        </span> <a href="#" onclick="gotoLinkInMenu('profile')"> <span
                            class="clear"> <span class="block m-t-xs"> <strong
                                    class="font-bold">${Model.userName }</strong>
                            </span>
                        </span>
                        </a> <span class="text-muted text-xs block">${Model.role}<b
                            class=""></b></span>  -->
                    </div>
                    <div class="logo-element" style="padding: 14px !important">
                        <a href="#" onclick="gotoLinkInMenu('xedashboard')">
                        <img alt="image" class=""
                            src="<%=request.getContextPath()%>/resources/img/xenon.png" />
                        </a>
                    </div>
                </li>
				<li><a href="javascript:history.back()">
						<i class="fa fa-th-large"></i> 
						<span class="nav-label">${Model.selectedExecuteBuildName}</span>
					</a>
				</li>
							</ul>

		</div>
	</nav>
</div>	
<script type="text/javascript">
$(function () {
	//$("#custLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	
	//tooltip
	$('[data-toggle="tooltip"]').tooltip(); 
});

	function gotoLinkInMenu(link)//link
	{
		$('body').addClass('white-bg');
		$("#barInMenu").removeClass('hidden');
		$("#wrapper").addClass("hidden");
		window.location.href = link; 
	}
</script>
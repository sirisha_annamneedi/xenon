<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- <div id="spinner"></div> -->

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Create New Test Set</h2>
		<!-- <ol class="breadcrumb">
			<li><a href="viewbuild">Test Set</a></li>
			<li class="active"><strong>New Test Set</strong></li>
		</ol>-->
	</div>
</div>
<div class="row sub-topmenu">
		<div class="col-md-12">
			<div class="">
				<div class="col-md-6 pd-l-0">
					<ul>
						<li><a href="executebuild" id="homeMenu" class="active noclick"><i class="fa fa-home"></i> Home</a></li>
						<li><a href="buildproject" class="noclick"><i class="fa fa-plus"></i> Add/Remove Test Cases</a></li>
						<!-- <li><a href="javascript:void(0)" data-toggle="modal" data-target="#uploadTestCase"><i class="fa fa-list"></i>
				 		 Execute</a></li> -->
						<li><a href="viewreport" id="addParameter" class="noclick"><i class="fa fa-line-chart"></i>
							 Report</a></li>
						 
					</ul>
				</div>
				<!-- <button type="button" id="rt"
				class="btn btn-info pull-right" style="margin-top:5px" disabled>Submit Execution</button>-->
			</div>
		</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="tabs-container">
			<!-- <ul class="nav nav-tabs">
				<li id="bfnr1" class="active"><a data-toggle="tab" href="#tab-1">Test</a></li>
			</ul> -->
		<div class="tab-content">
			<div id="tab-1" class="tab-pane active">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox">
								<div class="ibox-content">
								<div id="example-basic">
								<h3>Select / Create Release</h3>
								        <fieldset>
								<br>
					        	<br>
					        	<div id="createdRelease">
					        		<label class="control-label col-sm-4">Created Release: </label>
									<div class="col-sm-8">
										<input type="text" id="createdReleaseTxt"
											placeholder="Release Name" class="form-control characters"
											name="releaseName" tabindex="1" disabled>
									</div>
					        	</div>
					        	<div id="releaseDropDown">
						        	<label class="col-sm-2 control-label">Choose Action :* </label>
									<div class="col-sm-6">
							        	<select id="releaseAction" onChange="releaseAction()">
							        		<option value="0">Select</option>
							        		<option value="1">Choose Release</option>
							        		<option value="2">Create Release</option>
							        	</select>
						        	</div>
					        	</div>
					        	<br>
					        	<br>
					        	
					        	<div id="chooseReleaseAction">
							        <label class="col-sm-2 control-label">Release:* </label>
									<div class="col-sm-6">
										<select onChange = "buildReleaseIdChange()" id="buildReleaseId" name="buildReleaseId" tabindex="1"
											style="width: 350px;">
											<option value="-1">Select</option>
											<c:forEach var="release"
												items="${Model.activeReleaseNames}">
												<option value="${release.release_id}">${release.release_name}</option>
											</c:forEach>
										</select>
									</div>
								</div>
									<div id="createReleaseAction">
										<form action="insertrelease" id="form"
											class="wizard-big wizard clearfix form-horizontal" method="POST">
											<div class="content clearfix">
												<div class="row">
													<label class="col-lg-4 text-right">* fields are mandatory</label>
												</div>
													<div class="row">
														<div class="col-lg-10">
															<c:if test="${Model.error == 1}">
																<div class="alert alert-danger">Release name already
																	exists, Please try another.</div>
															</c:if>
															<div class="form-group">
																<label class="control-label col-sm-2">Name:* </label>
																<div class="col-sm-10">
																	<input type="text" id="txtReleaseName"
																		placeholder="Release Name" class="form-control characters"
																		name="releaseName" tabindex="1" required>
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-2 control-label"> Description:</label>
																<div class="col-sm-10">
																
																	<textarea name="releaseDescription"
																		id="txtReleaseDescription" rows="5"
																		class="summernote" tabindex="2"> </textarea>
																</div>
															</div>
															<div class="form-group" id="dateRange">
																<label class="col-sm-2 control-label">Date:</label>
																<div class="col-sm-10">
																	<div class="input-daterange input-group" id="datepicker">
																		<input type="text" class="input-sm form-control todaysDate"
																			name="startDate" tabindex="3"> <span class="input-group-addon">to</span>
																		<input type="text" class="input-sm form-control todaysDate"
																			name="endDate" tabindex="4">
																	</div>
																</div>
															</div>
					
															<div class="form-group hidden">
																<label class="col-sm-2 control-label">Status: </label>
																<div class="col-sm-10">
																	<div class="radio radio-success radio-inline col-sm-2">
																		<input type="radio" id="inlineRadio1" value="1"
																			name="releaseStatus" checked="" tabindex="5"> <label
																			for="inlineRadio1"> Active </label>
																	</div>
																	<div class="radio radio-danger radio-inline col-sm-2">
																		<input type="radio" id="inlineRadio2" value="2"
																			name="releaseStatus"> <label for="inlineRadio2">
																			Inactive </label>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="hr-line-solid"></div>
											</div>
											<div class="actions clearfix">
												<div class="row">
													<div class="col-sm-4">
														
														<button onclick="sub()"
															class="btn btn-success pull-left ladda-button ladda-button-demo"
															style="margin-right: 15px;" type="button"
															data-style="slide-up" tabindex="7" id="btnSubmit">Submit</button>
													</div>
												</div>
											</div>
										</form>
									</div>
									</fieldset>
									<h3>Create Test Set</h3>									
									<fieldset>
											<form action="insertbuild" id="formNewBuild" method="POST"
												class="wizard-big wizard clearfix form-horizontal" style="overflow: visible !important;">
												<div class="content clearfix" style="overflow: visible !important;">
													
													<div class="row">
														<label class="col-lg-4 text-right">* fields are mandatory</label>
													</div>
														<div class="row">
															<div class="col-lg-10">
																<div class="form-group">
																
																<label class="control-label col-sm-2">Release:* </label>
																	<div class="col-sm-10">
																		<input type="text" id="selectedReleaseName" placeholder="Test Set Name"
																			class="form-control characters" name="buildReleaseName" disabled>
																		<input type="text" id="selectedReleaseId" placeholder="Test Set Name"
																			class="form-control characters hidden" name="buildReleaseId">
																	</div>
																
						
																	<!-- <label class="col-sm-2 control-label">:* </label>
																		<div class="col-sm-10">
																		<select id="selectedReleaseId" name="" style="width: 350px;" disabled>
																		</select>
																		</div> -->
																		
																	</div>
																<label class="alert alert-danger" style="display: none;" id="buildNameErr">Test Set name already
													exists, Please try another.</label>									
																<div class="form-group">
																	<label class="control-label col-sm-2">Name:* </label>
																	<div class="col-sm-10">
																		<input type="text" placeholder="Test Set Name" id="buildName"
																			class="form-control characters" name="buildName" tabindex="2" required>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label"> Description:</label>
																	<div class="col-sm-10">
																		<textarea name="buildDescription" 
																			rows="5" class="summernote"  tabindex="3"> </textarea>
																	</div>
																</div>
																<c:if test="${Model.automationStatus == 1}">
																<div class="form-group">
																	<label class="col-sm-2 control-label">Type:* </label>
																	<div class="col-sm-10">
						<%-- 												<select data-placeholder="Choose a Execution Type"
																			class="chosen-select" name="buildExecutionType" tabindex="1"
																			style="min-width: 300px;">
																			<c:forEach var="type"
																				items="${Model.exeTypes}">
																				<option value="${type.execution_type_id}">${type.description}</option>
																			</c:forEach>
																		</select> --%>
																		<div class="radio radio-primary radio-inline col-sm-2 ">
																			<input type="radio"  value="${Model.exeTypes[0].execution_type_id}"
																				name="buildExecutionType"  tabindex="3" id="check1">  <label
																				for="check1"> ${Model.exeTypes[0].description} </label>
																		</div>
																		<div class="radio radio-warning radio-inline col-sm-2">
																			<input type="radio" id="check2" value="${Model.exeTypes[1].execution_type_id}"
																				name="buildExecutionType"  tabindex="4" > <label
																				for="check2"> ${Model.exeTypes[1].description}  </label>
																		</div>
																		<%-- <c:if test="${Model.scriptlessStatus==1}">
																		<div class="radio radio-info radio-inline col-sm-4">
																			<input type="radio" id="check3" value="${Model.exeTypes[2].execution_type_id}"
																				name="buildExecutionType" tabindex="4"> <label
																				for="check3"> ${Model.exeTypes[2].description}  </label>
																		</div></c:if> --%>
																		
																	</div>
																</div>
																</c:if>
																
																<div class="form-group hidden" id="selectEnv">
						
																	<label class="col-sm-2 control-label">Environment:* </label>
																	<div class="col-sm-6">
																		<select data-placeholder="Choose a Release"
																			class="chosen-select" name="buildEnv" tabindex="1"
																			style="width: 350px;">
																			<c:forEach var="envList" items="${Model.envList}">
																				<option value="${envList.env_id}">${envList.env_name}</option>
																			</c:forEach>
																		</select>
																	</div>
																</div>
																
															<%-- 	<c:if test="${Model.automationStatus == 2}">
																<input type="text" class="hidden" value = "1">
																</c:if> --%>
																<div class="form-group hidden">
																	<label class="col-sm-2 control-label">Status:</label>
																	<div class="col-sm-10">
																		<div class="radio radio-success radio-inline col-sm-2">
																			<input type="radio" id="inlineRadio1" value="1"
																				name="buildStatus" checked="" tabindex="4"> <label
																				for="inlineRadio1"> Active </label>
																		</div>
																		<div class="radio radio-danger radio-inline col-sm-2">
																			<input type="radio" id="inlineRadio2" value="2"
																				name="buildStatus"> <label for="inlineRadio2">
																				Inactive </label>
																		</div>
																	</div>
																</div>
																	
															</div>
														</div>
														<div class="hr-line-solid"></div>
												</div>
												
											</form>
								        </fieldset>
									
									</div>
									
							
								<!-- end form -->
								</div>
								<!-- end ibox-content -->
				
							</div>
							<!-- end ibox -->
						</div>
						<!-- end col -->
					</div>
				</div>
			</div>
					
	<!-- end row -->
</div>
<!-- end wrapper -->
</div>
</div>
</div>



<script>
function sub(){
	var formValid = $("#form").valid();
	if(formValid){
		console.log("b");
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		 
		$('.summernote').each( function() {
			   $(this).val($(this).code());
			   })
		console.log("validated");
		$('#form').submit();
	}
}

$(function () { 
	  
	  $('.todaysDate').each(function(){
		  $(this).val(moment().format('MM/DD/YYYY'));
	  });
	  
	  $("#releaseForm").validate({
	        rules: {
	        	releaseName: {
	                required: true,
	                minlength: 1,
	                maxlength: 40
	            },
	            releaseDescription: {
	                minlength: 1,
	                maxlength: 200
	            }
	        }
		
	    });

	$('#dateRange .input-daterange').datepicker({
		keyboardNavigation: false,
      forceParse: false,
      autoclose: true
  });
	
});
 
$("#submitBtn").click(function() {
	console.log("a");
	
	var formValid = $("#form").valid();
	if(formValid){
		console.log("b");
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		 
		$('.summernote').each( function() {
			   $(this).val($(this).code());
			   })
		
		$('#form').submit();
	}
});

</script>
<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	var sessionValue=<%=request.getParameter("type")%>
	if(sessionValue!=null)
	{
		document.getElementById('check'+sessionValue).setAttribute("checked", "checked")
	}
	else
	{
		document.getElementById('check1').setAttribute("checked", "checked")
	}
	 
});

  $(function () {

	 //code to check if no release found
	  var releaseCount = ${Model.activeReleaseNames.size()};
	  var createReleaseAccess = ${Model.createReleaseAccess};
	  
	  if(createReleaseAccess == 0  && releaseCount == 0){
		  swal({
		        title: "You require minimum 1 release to create build",
		        text: "You dont't have access to create release. Please contact to administrator",
		        type: "warning",
		        showCancelButton: false,
		        confirmButtonColor: "#DD6B55",
		        confirmButtonText: "Ok",
		        closeOnConfirm: false
		    }, function () {
		    	window.location.href="bmdashboard";
		    });
	  }
	  
	  else if(releaseCount == 0){
		  swal(
				{
					title : "You need a minimum 1 release to create a build.",
						showCancelButton : true,
						confirmButtonColor : "#DD6B55",
						confirmButtonText : "Create Release",
						cancelButtonText : "Ok",
						closeOnConfirm : true,
						closeOnCancel : false
				}, function(isConfirm) {
					if (isConfirm) {
						window.location.href="createrelease";
					}
					else
						{
						window.location.href="bmdashboard";
						}
				}		
		  );
	  }
  });  
</script>

<script>
	var config = {
		'.chosen-select' : {},
		'.chosen-select-deselect' : {
			allow_single_deselect : true
		},
		'.chosen-select-no-single' : {
			disable_search_threshold : 10
		},
		'.chosen-select-no-results' : {
			no_results_text : 'Oops, nothing found!'
		},
		'.chosen-select-width' : {
			width : "95%"
		}
	}
	for ( var selector in config) {
		$(selector).chosen(config[selector]);
		$("ul.chosen-results").css('max-height','250px');
	}
	/* $("#createBuildCancelbtn").click(function() {
		window.location.href = "viewbuild";
	}); */
	$(document).ready(function() {
		$('.summernote').summernote();
		$('.note-editor').css('background','white');
		$('button[ data-event="codeview"]').remove();
		
		 $('div.note-insert').remove();
         $('div.note-table').remove();
         $('div.note-help').remove();
         $('div.note-style').remove();
         $('div.note-color').remove();
         $('button[ data-event="removeFormat"]').remove();
         $('button[ data-event="insertUnorderedList"]').remove();
         $('button[ data-event="fullscreen"]').remove();
         $('button[ data-original-title="Line Height"]').remove();
         $('button[ data-original-title="Font Family"]').remove();
         $('button[ data-original-title="Paragraph"]').remove();


		$("#form").validate({
			rules : {
				buildName : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				buildDescription : {
					minlength : 1,
					maxlength : 200
				}
			}
		});
		
		 		 
		 $('input[type=radio][name=buildExecutionType]').change(function() {
		        if (this.value == 3) {
		        	$("#selectEnv").removeClass("hidden");
		        }
		        else {
		        	$("#selectEnv").addClass("hidden");
		        }
		       
		    });
	});
	//var l = $('.ladda-button-demo').ladda();
	  
	
</script>

<!-- script for new build from changes : Anis -->
<script>
var urlReleaseId;
var urlReleaseName;

$(document).ready(function(){
	console.log("reading values from URL"); 
	var url_string = window.location.href;
	var url = new URL(url_string);
	urlReleaseId = url.searchParams.get("id");
	urlReleaseName = url.searchParams.get("name");
	
	if(urlReleaseId){ 
		console.log('with value '+urlReleaseId);
		$("#releaseDropDown").hide();
		$("#createdReleaseTxt").val(urlReleaseName);
		$("#selectedReleaseId").val(urlReleaseId);
        $("#selectedReleaseName").val(urlReleaseName);
	}
	else{
		console.log('withoud value '+urlReleaseId);
		$("#createdRelease").hide();
		$("ul").addClass("disabled")
	}
	
	
})

function releaseAction(){
	var releaseAction = $("#releaseAction").val();
	if(releaseAction == 1){
		$("#chooseReleaseAction").show();
		$("#createReleaseAction").hide();
		
		
	}
	else if(releaseAction == 2){
		$("#chooseReleaseAction").hide();
		$("#createReleaseAction").show();
		$("ul").addClass("disabled");		
		$("#btnSubmit").removeAttr("disabled");
	}
	else{
		$("#chooseReleaseAction").hide();
		$("#createReleaseAction").hide(); 
	}
}

$(document).ready(function() {
	
	$("#chooseReleaseAction").hide();
	$("#createReleaseAction").hide(); 
	
});

function buildReleaseIdChange(){
	console.log("calledyes");
	if($("#buildReleaseId").val() != '-1'){
		$("#btnSubmit").attr("disabled", true);
		$("ul").removeClass("disabled")
		
	}
	else{
		
		$("ul").addClass("disabled");
		
	}
		
}


$("#example-basic").steps({
    headerTag: "h3",
    bodyTag: "fieldset",
    transitionEffect: "slideLeft",
    autoFocus: true,
    
    onStepChanged : function(event, currentIndex, priorIndex) {
    	if(currentIndex == 1){
    		
    		var selectedValue = $("#buildReleaseId").val();
    		var selectedText = $("#buildReleaseId option:selected").html();
            console.log(selectedText);
            
            if(!urlReleaseId){
            	$("#selectedReleaseId").val(selectedValue);
            	$("#selectedReleaseName").val(selectedText);
            	
            }
            //$("#selectedReleaseId option").remove();
            //$("#selectedReleaseId").append($("<option>").attr('value',selectedValue).text(selectedText));
            
    		
    	}
    },
    onCanceled: function (event)
    {
    	window.location.reload();
    },
    onFinished: function (event){
    	console.log("Finishhh");
    	console.log("subbiolll");

    	var invalidate = false;
    	
    	if($('input[type=radio][name=buildExecutionType]:checked').val()==1)
    	{
    		console.log("Manual");
    		
    		var buildName=$("#buildName").val();
    		var relId = $('#selectedReleaseId').val();

    		$.ajax({
    		  type: 'POST',
    		  url: 'checkmanualbuildname',
    		  data: {
    			relId : relId,
    			buildName : buildName
    		  },
    		  async:false
    		}).done(function( data ) {
    		   if(data) {
    			$("#buildNameErr").css("display","");
    			$("#buildName").addClass("error");
    			invalidate = true;
    		   }
    		});
    	}
    	else if($('input[type=radio][name=buildExecutionType]:checked').val()==2)
    	{
    		console.log("automation")
    		var buildName=$("#buildName").val();
    		var relId = $('#selectedReleaseId').val();
    		$.ajax({
    			  type: 'POST',
    			  url: 'checkautobuildname',
    			  data: {
    				relId : relId,
    				buildName : buildName
    			  },
    			  async:false
    			}).done(function( data ) {
    			   if(data) {
    				$("#buildNameErr").css("display","");
    				$("#buildName").addClass("error");
    				invalidate = true;
    			   }
    			});
    	}
    	
    	if(invalidate){
    		
    	}
    	else{
    		console.log("build sub");
    		var valid = $("#formNewBuild").valid();
    		if(valid){
    			$(this).attr("disabled","true");
    			$('.summernote').each( function() {
    				$(this).val($(this).code());
    			});
    			$('#formNewBuild').submit();
    	 	}
    	} 
    	
    }
});
</script>

    
<style>
	a.noclick       {
  pointer-events: none;
  opacity:0.6;
}
</style>



<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!-- <div id="spinner"></div> -->
<link
	href="<%=request.getContextPath()%>/resources/css/xenon_custom.css"
	rel="stylesheet">

<div class="md-skin">
		<div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-lg-8">
				<h2>Build Application list -
					${Model.addremovetestcaseBuildName}</h2>
				<ol class="breadcrumb">
					<li><a href="bmdashboard">Dashboard</a></li>
					<li><a href="addremovetestcase">Add/Remove test cases</a></li>
					<li class="active"><strong>Application list</strong></li>
				</ol>
			</div>
			<!-- end col-lg-10 -->
		</div>
		<!-- end row -->
	</div>
	<!-- end wrapper -->
	<div class="row">
		<div class="col-lg-12">
			<div class="wrapper wrapper-content animated fadeInUp">
				<div class="ibox-content">

					<div class="content clearfix">


						<table class="footable table table-stripped toggle-arrow-tiny"
							id="tblProject" data-page-size="8">
							<thead>
								<tr id="tableRow">
									<th data-toggle="true">Application Name</th>
									<th class="hidden">Application Description</th>
									<th>Creation Date</th>
									<th data-hide="all"></th>

								</tr>
							</thead>
							<tbody>
								<c:forEach var="data" items="${Model.projectDetails}">
									<tr class="projectTr_${data.project_id}">

										<td class="projectId lgTextAlignment" name="${data.project_id}" data-original-title="${data.project_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.project_name}</td>
										<td class="hidden">${data.project_description}</td>
										<td>${data.project_createdate}</td>
										<td class="col-lg-12">
											<table
												class="table table-striped table-bordered table-hover dataTables-example">
												<thead>
													<tr>
														<th>Module Name</th>
														<th class="hidden">Module Description</th>
														<th>User</th>

													</tr>
												</thead>
												<tbody>
													<c:forEach var="module" items="${Model.moduleDetails}">
														<c:if
															test="${fn:indexOf(data.project_id,module.project_id)==0}">
															<tr class="gradeX">
																<td class="module_${data.project_id}"
																	name="${module.module_id}">${module.module_name}</td>
																<td class="hidden">${module.module_description}</td>

																<td><select id="userSelect_${module.module_id}"
																	class="select2_demo_1 form-control">
																		<option value="0">Select user</option>
																		<c:if test="${module.assignFlag==1 }">
																		<c:forEach var="userDetail"
																			items="${Model.userDetail}">

																			<c:forEach var="assigneeUserList"
																				items="${Model.assigneeUserList}">

																				<c:if
																					test="${userDetail.module_id==module.module_id}">

																					<c:if
																						test="${userDetail.module_id == assigneeUserList.module_id}">


																						<c:forEach var="singleUser"
																							items="${Model.singleUser}">
																							<c:if
																								test="${userDetail.user_id==singleUser.user_id}">
																								
																								<c:if test="${singleUser.user_id==assigneeUserList.user_id}">
																								
																								<option value="${singleUser.user_id}" selected>${singleUser.first_name}
																									${singleUser.last_name}</option>
																									
																								</c:if>
																								
																								<c:if test="${singleUser.user_id!=assigneeUserList.user_id}">
																								<option value="${singleUser.user_id}">${singleUser.first_name}
																									${singleUser.last_name}</option>
																								
																								</c:if>
																							
																							</c:if>
																							
																						</c:forEach>
																					</c:if>
																				</c:if>
																			</c:forEach>
																		</c:forEach>
																		</c:if>
																		<c:if test="${module.assignFlag==0}">
																		<c:forEach var="userDetail" items="${Model.userDetail}">
																		<c:if
																			test="${fn:indexOf(userDetail.module_id,module.module_id)==0}">
																			<c:forEach var="singleUser" items="${Model.singleUser}">
																				<c:if
																					test="${userDetail.user_id==singleUser.user_id}">
																					<option value="${singleUser.user_id}">${singleUser.first_name}
																						${singleUser.last_name}</option>
																				</c:if>
																			</c:forEach>

																		</c:if>
																	</c:forEach>
																		
																		</c:if>
																</select></td>

															</tr>
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</td>
									</tr>
								</c:forEach>

							</tbody>
							<tfoot>
								<tr>
									<td colspan="5">
										<ul class="pagination pull-right"></ul>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
					<div class="row">
						<div class="col-sm-1">
							<button class="btn btn-success" onclick="assignBuild()"
								type="submit">Assign</button>
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>

<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");

		var foo = $('.footable').footable();
		foo.trigger('footable_initialize'); //Reinitialize
		foo.trigger('footable_redraw'); //Redraw the table
		foo.trigger('footable_resize'); //Resize the table
	});

	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});
	
	var userAssignmentStatus=${Model.userAssignmentStatus};
	if(userAssignmentStatus==0)
		{
			swal({
				title : "Project/Module not found for assignment",
				//text : "Please assign project to the user to assign modules from build for execution",
				type : "warning",
				confirmButtonText : "OK",
				closeOnConfirm : true
			}, function(isConfirm) {
				if (isConfirm) {
					window.location.href = "viewbuild";
				}
			});
		}
</script>
<script>
	$(function() {
		$('#projectTable').DataTable({
			"paging" : true,
			"lengthChange" : true,
			"searching" : true,
			"ordering" : true,
			"info" : true,
			"autoWidth" : true
		});

		$('.projectId').each(
				function() {
					var project_id = $(this).attr("name");

					$(
							"#tblProject tbody tr.projectTr_" + project_id
									+ " td:first").trigger('click');
				});
	});
	var invalidUserFlag = 0;
	var invalidProjectId = 0;
	function assignBuild() {
		$('.projectId').each(
				function() {
					var project_id = $(this).attr("name");

					$('.module_' + project_id).each(
							function() {
								var module_id = $(this).attr("name");
								var user_id = $('#userSelect_' + module_id)
										.val();
								if (user_id == 0) {
									invalidUserFlag = 1;
									invalidProjectId = project_id;

									$('#userSelect_' + module_id).css("border",
											"1px solid red");

								} else {
									$('#userSelect_' + module_id).css("border",
											"1px solid #e5e6e7");

								}

							});

				});

		if (invalidUserFlag == 1) {
			swal({
				title : "Please select assignee for modules",
				//text : "Selected users assigned to build",
				type : "warning",
				confirmButtonText : "OK",
				closeOnConfirm : true
			}, function(isConfirm) {
				if (isConfirm) {
					invalidUserFlag = 0;
				}
			});
		} else {

			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");

			var posting = $.post('resetbuildassign');
			posting.done(function(data) {
				$('.projectId').each(function() {
					var project_id = $(this).attr("name");

					$('.module_' + project_id).each(function() {
						var module_id = $(this).attr("name");
						var user_id = $('#userSelect_' + module_id).val();

						var posting = $.post('setbuildassign', {
							projectID : project_id,
							moduleID : module_id,
							userID : user_id
						});

						posting.done(function(data) {
							swal({
								title : "Build Assignment Completed!",
								text : "Selected users assigned to build",
								type : "success",
								confirmButtonText : "OK",
								closeOnConfirm : true
							}, function(isConfirm) {
								if (isConfirm) {
									window.location.href = "viewbuild";
								}
							});

						});

					});

				});
			});
		}
	}
</script>
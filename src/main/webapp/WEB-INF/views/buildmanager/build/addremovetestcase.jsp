<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!-- <div id="spinner"></div> -->

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Amend build</h2>
		<ol class="breadcrumb">
			<li><a href="viewbuild">Test Set</a></li>
			<li class="active"><strong>Amend build</strong></li>
		</ol>
	</div> 
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<input type="text" class="form-control input-sm m-b-xs" id="filter"
						placeholder="Search">
					<table class="footable table table-stripped toggle-arrow-tiny"
						data-page-size="10" data-filter="#filter">
						<thead>
							<tr>

								<th data-toggle="true">Build Name</th>
								<th data-hide="">Release</th>
								<th data-hide="all">Description</th>
								<th data-hide="all">Application Wise Counts</th>
								<th data-hide="phone">Total Test case</th>
								<!-- <th data-hide="phone,tablet">Project</th> -->
								<th data-hide="phone">State</th>
								<th class="text-right" data-sort-ignore="true">Action</th>

							</tr>
						</thead>
						<tbody>
							<c:forEach var="data" items="${model.details}">
							<c:if test="${data.build_state !=4  && data.build_state !=3  && data.build_state !=6 }">
								<tr>
									<td class="lgTextAlignment" data-original-title="${data.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_name}</td>
									<td class="lgTextAlignment" data-original-title="${data.release_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.release_name}</td>
									<td>${data.build_desc}</td>

									<td><c:forEach var="tcCountDetails"
											items="${model.tcCountDetails}">
											<c:if
												test="${data.build_id == tcCountDetails.build_id}">
												</br>${tcCountDetails.project_name}:  ${tcCountDetails.proWiseCount}   
										</c:if>
										</c:forEach></td>

									<c:set var="count" value="0" scope="page" />
									<c:forEach var="tcCountDetails" items="${model.tcCountDetails}">
										<c:if
											test="${data.build_id == tcCountDetails.build_id}">
											<c:set var="count"
												value="${count + tcCountDetails.proWiseCount}" scope="page" />
										</c:if>
									</c:forEach>

									<td><c:out value="${count}" /></td>

									<%-- <td>${data.proTcCount}</td> --%>
									<td><label class=" label ${data.build_state}">${data.state}</label></td>
									<td class="text-right"><c:if
											test="${fn:indexOf(data.state,'Waiting For Assignee')==0}">
											<button class="btn btn-white btn-xs"
												onclick="assign(${data.build_id},'${data.build_name}')">Assign</button>
										</c:if>
										<button class="btn btn-white btn-xs"
											onclick="view(${data.build_id},'${data.build_name}')">Add/Remove Test Cases</button></td>
								</tr></c:if>
								
								<c:if test="${data.build_state ==4   ||  data.build_state ==3}">
									<c:if test="${model.removeStatus}">
								<tr>
									<td class="lgTextAlignment" data-original-title="${data.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_name}</td>
									<td class="lgTextAlignment" data-original-title="${data.release_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.release_name}</td>
									<td>${data.build_desc}</td>

									<td><c:forEach var="tcCountDetails"
											items="${model.tcCountDetails}">
											<c:if
												test="${data.build_id == tcCountDetails.build_id}">
												</br>${tcCountDetails.project_name}:  ${tcCountDetails.proWiseCount}   
										</c:if>
										</c:forEach></td>

									<c:set var="count" value="0" scope="page" />
									<c:forEach var="tcCountDetails" items="${model.tcCountDetails}">
										<c:if
											test="${data.build_id == tcCountDetails.build_id}">
											<c:set var="count"
												value="${count + tcCountDetails.proWiseCount}" scope="page" />
										</c:if>
									</c:forEach>

									<td><c:out value="${count}" /></td>

									<%-- <td>${data.proTcCount}</td> --%>
									<td><label class=" label ${data.build_state}">${data.state}</label></td>
									<td class="text-right"><c:if
											test="${fn:indexOf(data.state,'Waiting For Assignee')==0}">
											<button class="btn btn-white btn-xs"
												onclick="assign(${data.build_id},'${data.build_name}')">Assign</button>
										</c:if>
										<button class="btn btn-white btn-xs"
											onclick="view(${data.build_id},'${data.build_name}')">Add/Remove Test Cases</button></td>
								</tr></c:if></c:if>
							</c:forEach>

						</tbody>
						<tfoot>
							<tr>
								<td colspan="6">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	 var foo = $('.footable').footable();
	 foo.trigger('footable_initialize'); //Reinitialize
	 foo.trigger('footable_redraw'); //Redraw the table
	 foo.trigger('footable_resize'); //Resize the table
});

  $(function () {
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	});  
</script>
<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {
         
         $('.1').each(function(){
     		$(this).addClass('label-success');
     	});
     	$('.2').each(function(){
     		$(this).addClass('label-info');
     	});
     	$('.3').each(function(){
     		$(this).addClass('label-warning');
     	});
     	$('.4').each(function(){
     		$(this).addClass('label-danger');
     	});
     	$('.5').each(function(){
     		$(this).addClass('label-success');
     	});
     });
    
    function assign(buildId,buildName){
    	
    	$('body').addClass("white-bg");
    	$("#barInMenu").removeClass("hidden");
    	$("#wrapper").addClass("hidden");	
    	
    	var posting = $.post('setAssignBuildSession', {
			buildId : buildId,
			buildName:buildName
		});
    	
    	 posting.done(function(data) {
 			window.location.href="assignbuild"; 
    	 });
    }
    function view(buildId,buildName){
    
    	$('body').addClass("white-bg");
    	$("#barInMenu").removeClass("hidden");
    	$("#wrapper").addClass("hidden");	
    	
    	var posting = $.post('setBuildSession', {
			buildId : buildId,
			buildName:buildName
		});
    	
    	 posting.done(function(data) {
 			window.location.href="buildproject"; 
    	 });
    }
</script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Build Execution</h2>
		<ol class="breadcrumb">
			<li><a href="viewbuild">Test Set</a></li>
			<li class="active"><strong>Build Execution</strong></li>
		</ol>
	</div> 
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
	<c:if test="${Model.automationStatus == 1}">
		<div class="tabs-container">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#manualBuilds">Manual
						Builds</a></li>
				<li class=""><a data-toggle="tab" href="#automationBuilds">Automation
						Builds</a></li>
			</ul>
			<div class="tab-content">
				<div id="manualBuilds" class="tab-pane active">
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-8">
								<div class="ibox">
									<div class="ibox-content">
										<div class="full-height-scroll">
											<div class="table-responsive" style="overflow-x: visible;">
												<table class="table table-striped table-hover"
													id="allBuildTable">
													<thead>
														<tr>
															<th class="hidden">Build Id</th>
															<th style="width: 250px">Build Name</th>
															<th>Build State</th>
															<th></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="data" items="${allBuildDetails}">
															<tr data-toggle="tab" href="#${data.build_id}"
																style="cursor: pointer">
																<td class="hidden">${data.build_id}</td>
																<td class="lgTextAlignment" data-original-title="${data.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_name}</td>
																<td class="client-status"><label
																	class=" label ${data.build_state}">${data.buildState}</label></td>
																<td class="project-actions">
																	<button class="btn btn-white btn-xs"
																		onclick="executeBuild(${data.build_id},'${data.build_name}')">Execute</button>
																</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
							<c:if test="${fn:length(allBuildDetails) > 0}">
								<div class="ibox">
									<div class="ibox-content">
										<div class="tab-content" id="extraBuildDetails">
											<c:forEach var="data" items="${allBuildDetails}">
												<div id="${data.build_id}" class="tab-pane">
													<div class="row m-b-lg">
														<div class="col-lg-4 text-center">

															<div class="m-b-sm">
																<img alt="image" class="img-circle "
																	src="data:image/jpg;base64,${data.photo}"
																	style="width: 62px">
															</div>
														</div>
														<div class="col-lg-8">
															<h2
																style="vertical-align: middle; text-align: center; margin-top: 20px;">${data.creator}</h2>
														</div>
													</div>
													<div class="">
														<div class="full-height-scroll">
															<strong>Build Information</strong>
															<ul class="list-group clear-list">
																<li class="list-group-item fist-item"><span
																	class="pull-right"> ${data.build_name} </span>Build
																	Name</li>
																<li class="list-group-item "><span
																	class="pull-right label statusLabels">
																		${data.buildStatus} </span>Build Status</li>
																<li class="list-group-item"><span
																	class="pull-right"> ${data.releaseName} </span>Build
																	Release</li>
																<li class="list-group-item"><span
																	class="pull-right"> ${data.build_createdate} </span>Build
																	Create Date</li>
																<li class="list-group-item">Build Assignee <c:forEach
																		var="assignee" items="${buildAssignee}">
																		<c:if test="${data.build_id == assignee.build_id}">
																			<span class="pull-right">
																				${assignee.first_name} ${assignee.last_name}</span>
																			<br>
																		</c:if>
																	</c:forEach></li>
															</ul>
															<strong>Build Description</strong>
															<p>${data.build_desc}</p>
														</div>
													</div>
												</div>
											</c:forEach>
										</div>
									</div>
								</div>
								</c:if>
							</div>
						</div>
					</div>
				</div>
				<!-- Automation data -->
				<div id="automationBuilds" class="tab-pane">
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-8">
								<div class="ibox">
									<div class="ibox-content">
										<div class="full-height-scroll">
											<div class="table-responsive" style="overflow-x: visible;">
												<table class="table table-striped table-hover"
													id="autoBuildTable">
													<thead>
														<tr>
															<th class="hidden">Build Id</th>
															<th style="width: 150px">Build Name</th>
															<th>Build State</th>
															<th style="text-align: right;">Action</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="data" items="${Model.allAutomationBuilds}">
															<tr data-toggle="tab" href="#autoBuild_${data.build_id}"
																style="cursor: pointer">
																<td class="hidden">${data.build_id}</td>
																<td class="lgTextAlignment" data-original-title="${data.build_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.build_name}</td>
																<td class="client-status"><label
																	class=" label label-info">${data.buildState}</label></td>
																<td class="project-actions">
																	<button class="btn btn-white btn-xs"
																		onclick="executeAutomationBuild(${data.build_id},'${data.build_name}')">Execute</button>
																
																</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
							<c:if test="${fn:length(allAutomationBuilds) > 0}">
								<div class="ibox">
									<div class="ibox-content">
										<div class="tab-content" id="extraBuildDetails">
											<c:forEach var="data" items="${allAutomationBuilds}">
												<div id="autoBuild_${data.build_id}" class="tab-pane">
													<div class="row m-b-lg">
														<div class="col-lg-4 text-center">

															<div class="m-b-sm">
																<img alt="image" class="img-circle"
																	src="data:image/jpg;base64,${data.photo}"
																	style="width: 62px">
															</div>
														</div>
														<div class="col-lg-8">
															<h2
																style="vertical-align: middle; text-align: center; margin-top: 20px;">${data.creator}</h2>
														</div>
													</div>
													<div class="">
														<div class="full-height-scroll">
															<strong>Build Information</strong>
															<ul class="list-group clear-list">
																<li class="list-group-item fist-item"><span
																	class="pull-right"> ${data.build_name} </span>Build
																	Name</li>
																<li class="list-group-item "><span
																	class="pull-right label statusLabels">
																		${data.buildStatus} </span>Build Status</li>
																<li class="list-group-item"><span
																	class="pull-right"> ${data.releaseName} </span>Build
																	Release</li>
																<li class="list-group-item"><span
																	class="pull-right"> ${data.buildCreateDate} </span>Build
																	Create Date</li>
															</ul>
															<strong>Build Description</strong>
															<p>${data.build_desc}</p>
														</div>
													</div>
												</div>
											</c:forEach>
										</div>
									</div>
								</div>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${Model.automationStatus != 1}">
		<div class="row">
			<div class="col-sm-8">
				<div class="ibox">
					<div class="ibox-content">
						<div class="full-height-scroll">
							<div class="table-responsive" style="overflow-x: visible;">
								<table class="table table-striped table-hover"
									id="allBuildTable">
									<thead>
										<tr>
											<th class="hidden">Build Id</th>
											<th>Build Name</th>
											<th>Build State</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="data" items="${allBuildDetails}">
											<tr data-toggle="tab" href="#${data.build_id}"
												style="cursor: pointer">
												<td class="hidden">${data.build_id}</td>
												<td>${data.build_name}</td>
												<td class="client-status"><label
													class=" label ${data.build_state}">${data.buildState}</label></td>
												<td class="project-actions">
													<button class="btn btn-white btn-xs"
														onclick="executeBuild(${data.build_id},'${data.build_name}')">Execute</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
			<c:if test="${fn:length(allBuildDetails) > 0}">
				<div class="ibox">
					<div class="ibox-content">
						<div class="tab-content" id="extraBuildDetails">
							<c:forEach var="data" items="${allBuildDetails}">
								<div id="${data.build_id}" class="tab-pane">
									<div class="row m-b-lg">
										<div class="col-lg-4 text-center">

											<div class="m-b-sm">
												<img alt="image" class="img-circle "
													src="data:image/jpg;base64,${data.photo}"
													style="width: 62px">
											</div>
										</div>
										<div class="col-lg-8">
											<h2
												style="vertical-align: middle; text-align: center; margin-top: 20px;">${data.creator}</h2>
										</div>
									</div>
									<div class="">
										<div class="full-height-scroll">
											<strong>Build Information</strong>
											<ul class="list-group clear-list">
												<li class="list-group-item fist-item"><span
													class="pull-right"> ${data.build_name} </span>Build Name</li>
												<li class="list-group-item "><span
													class="pull-right label statusLabels">
														${data.buildStatus} </span>Build Status</li>
												<li class="list-group-item"><span class="pull-right">
														${data.releaseName} </span>Build Release</li>
												<li class="list-group-item"><span class="pull-right">
														${data.build_createdate} </span>Build Create Date</li>
												<li class="list-group-item">Build Assignee <c:forEach
														var="assignee" items="${buildAssignee}">
														<c:if test="${data.build_id == assignee.build_id}">
															<span class="pull-right"> ${assignee.first_name}
																${assignee.last_name}</span>
															<br>
														</c:if>
													</c:forEach></li>
											</ul>
											<strong>Build Description</strong>
											<p>${data.build_desc}</p>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
				</c:if>
			</div>
		</div>
	</c:if>
</div>
</div>
<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});

  $(function () {
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	  sessionStorage.setItem("ActiveProjectId",0);
	  sessionStorage.setItem("ActiveModuleId",0);
	  sessionStorage.setItem("ActiveScenarioId",0);
	});  
</script>
<script>
$(function() {
	var executeBuildTable = $('#allBuildTable').DataTable({
		dom : '<"html5buttons"B>lTfgitp',
		buttons : [],
		"paging" : false,
		"lengthChange" : false,
		"searching" : false,
		"ordering" : false
	});
	var autoBuild = $('#autoBuildTable').DataTable({
		dom : '<"html5buttons"B>lTfgitp',
		buttons : [],
		"paging" : false,
		"lengthChange" : false,
		"searching" : false,
		"ordering" : false
	});
	
	var scriptBuild = $('#scriptBuildTable').DataTable({
		dom : '<"html5buttons"B>lTfgitp',
		buttons : [],
		"paging" : false,
		"lengthChange" : false,
		"searching" : false,
		"ordering" : false
	});
	
	$('#extraBuildDetails > :first-child').addClass('active');
	$('.statusLabels').each(function() {
		var status = $.trim($(this).text());
		if (status.toLowerCase() == 'active') {
			$(this).addClass('label-success');
		} else {
			$(this).addClass('label-danger');
		}
	});
	
	var allExecuteRows =  executeBuildTable.$(".label", {"page": "all"});
	allExecuteRows.each(function(){
		if($(this).hasClass('1'))
			$(this).addClass('label-success');
		else if($(this).hasClass('2'))
			$(this).addClass('label-info');
		else if($(this).hasClass('3'))
			$(this).addClass('label-warning');
		else if($(this).hasClass('4'))
			$(this).addClass('label-danger');
		else if($(this).hasClass('5'))
			$(this).addClass('label-primary');
	});
	
	 var manual= <%= request.getParameter("manual") %>;
	 if(manual == null){
		 manual = 1;
	 }
	 
	 var auto= <%= request.getParameter("auto") %>;
	 if(auto == null){
		 auto = 1;
	 }
		 
	 var sc= <%= request.getParameter("sc") %>;
	 if(sc == null){
		 sc = 1;
	 }
	 
	 var manualBuildCount='${Model.manualBuildCount}';

	 var pageSize='${Model.pageSize}';
	 
	 var manualLastRec=((manual-1)*pageSize+parseInt(pageSize));

	 if(manualLastRec>manualBuildCount)
	 	manualLastRec=manualBuildCount;
	 var manualShowCount = ((manual-1)*pageSize+1);
	 if(manualShowCount <= manualLastRec){
		 $("#allBuildTable_info").html("Showing "+manualShowCount+" to "+manualLastRec+" of "+manualBuildCount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="manualPrevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="manualNextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
	 }else{
		 $("#allBuildTable_info").html("");
	 }
	 
	 if(manual == 1){
		 $("#manualPrevBtn").attr('disabled','disabled');
	 }
	 
	 if(manualLastRec == manualBuildCount){
		 $("#manualNextBtn").attr('disabled','disabled');
	 }
	 
	    $("#manualPrevBtn").click(function() {
	    	$("#barInMenu").removeClass("hidden");
	    	$("#wrapper").addClass("hidden");
	    	
	    	if(manual == 1)
	    		window.location.href = "executebuildlist?manual=1&auto="+auto+"&sc="+sc;
	    	else
	    		window.location.href = "executebuildlist?manual="+(manual - 1)+"&auto="+auto+"&sc="+sc;
		});
		
		$("#manualNextBtn").click(function(){
			$("#barInMenu").removeClass("hidden");
	    	$("#wrapper").addClass("hidden");
	    	
			window.location.href = "executebuildlist?manual="+(manual + 1)+"&auto="+auto+"&sc="+sc;
		});
		
		/**
		* this is code for automation builds
		*
		*/
		
		var autobuildcount='${Model.autobuildcount}';

		 var pageSize='${Model.pageSize}';
		 
		 var autoLastRec=((auto-1)*pageSize+parseInt(pageSize));

		 if(autoLastRec>autobuildcount)
		 	autoLastRec=autobuildcount;
		 var autoShowCount = ((auto-1)*pageSize+1);
		 if(autoShowCount <= autoLastRec){
			 $("#autoBuildTable_info").html("Showing "+autoShowCount+" to "+autoLastRec+" of "+autobuildcount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="autoPrevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="autoNextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
		 }else{
			 $("#autoBuildTable_info").html("");
		 }
		 
		 if(auto == 1){
			 $("#autoPrevBtn").attr('disabled','disabled');
		 }
		 
		 if(autoLastRec == autobuildcount){
			 $("#autoNextBtn").attr('disabled','disabled');
		 }
		 
		    $("#autoPrevBtn").click(function() {
		    	$("#barInMenu").removeClass("hidden");
		    	$("#wrapper").addClass("hidden");
		    	
		    	if(auto == 1)
		    		window.location.href = "executebuildlist?manual="+manual+"&auto=1&sc="+sc+"&buildType=automation";
		    	else
		    		window.location.href = "executebuildlist?manual="+manual+"&auto="+(auto - 1)+"&sc="+sc+"&buildType=automation";
			});
			
			$("#autoNextBtn").click(function(){
				$("#barInMenu").removeClass("hidden");
		    	$("#wrapper").addClass("hidden");
		  		
				window.location.href = "executebuildlist?manual="+manual+"&auto="+(auto + 1)+"&sc="+sc+"&buildType=automation";
			});
			
			/**
			* this code is for scriptles
			*/
			
			var scbuildcount='${Model.scbuildcount}';

			 var pageSize='${Model.pageSize}';
			 
			 var scLastRec=((sc-1)*pageSize+parseInt(pageSize));

			 if(scLastRec>scbuildcount)
			 	scLastRec=scbuildcount;
			 var scShowCount = ((sc-1)*pageSize+1);
			 if(scShowCount <= scLastRec){
				 $("#scriptBuildTable_info").html("Showing "+scShowCount+" to "+scLastRec+" of "+scbuildcount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="scPrevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="scNextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
			 }else{
				 $("#scriptBuildTable_info").html("");
			 }
			 
			 if(sc == 1){
				 $("#scPrevBtn").attr('disabled','disabled');
			 }
			 
			 if(scLastRec == scbuildcount){
				 $("#scNextBtn").attr('disabled','disabled');
			 }
			 
			    $("#scPrevBtn").click(function() {
			    	$("#barInMenu").removeClass("hidden");
			    	$("#wrapper").addClass("hidden");
			    	
			    	if(sc == 1)
			    		window.location.href = "executebuildlist?manual="+manual+"&auto="+auto+"&sc=1"+"&buildType=scriptless";
			    	else
			    		window.location.href = "executebuildlist?manual="+manual+"&auto="+auto+"&sc="+(sc - 1)+"&buildType=scriptless";
				});
				
				$("#scNextBtn").click(function(){
					$("#barInMenu").removeClass("hidden");
			    	$("#wrapper").addClass("hidden");
			    	
					window.location.href = "executebuildlist?manual="+manual+"&auto="+auto+"&sc="+(sc + 1)+"&buildType=scriptless";
				});
	
});

function executeBuild(id,name){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");
	
	var posting = $.post('setexecutebuildid',{
		buildID : id,
		buildName:name
	});
	posting.done(function(data){
		window.location.href="executebuild";
	});
}


</script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<input type="hidden"
	value="<%=session.getAttribute("selectedBuildViewType")%>"
	id="hiddenBuildViewType">

<div class="md-skin">
<div class="row wrapper page-heading white-bg border-bottom">
	<div class="col-lg-8">
		<h2><span class="breadcrumbText">View</span> build - ${model.buildDetails[0].build_name}</h2>
		<ol class="breadcrumb">
			<!-- <li><a href="bmdashboard">Dashboard</a></li> -->
			<li><a href="viewbuild">Test Set</a></li>
			<li class="active"><strong class="breadcrumbText">View</strong></li>
		</ol>
	</div>
</div>

<div class="row wrapper wrapper-content animated fadeInRight">
	<div class="col-lg-12">
		<div class="ibox">
			<div class="ibox-content">
				<form action="updatebuild"
					class="wizard-big wizard clearfix form-horizontal" method="POST"
					id="editBuildForm">
					<div class="content clearfix">
						<fieldset class="body current" id="editBuildFieldSet" disabled>
							<div class="row">
								<div class="col-lg-10">
									<div class="form-group hidden">
										<label class="control-labael cok-sm-2">Build Id: </label>
										<div class="col-sm-10">
											<input type="hidden"
												value="${model.buildDetails[0].build_id}" name="buildID">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Release:</label>
										<div class="col-sm-6">
											<select
												data-placeholder="${model.buildDetails[0].releaseName}"
												tabindex="2" style="width: 350px;" class="chosen-select"
												name="buildReleaseId" id="selectBuildReleaseId" disabled>
												<option></option>
												<c:forEach var="release" items="${model.activeReleaseNames}">
													<option value="${release.release_id}"
														${model.buildDetails[0].releaseName == release.release_name ? 'selected="selected"' : ''}>
														${release.release_name}</option>
												</c:forEach>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2">Name:*</label>
										<div class="col-sm-10">
											<input type="text" class="form-control characters"
												value="${model.buildDetails[0].build_name}" name="buildName" tabindex="1">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2">Description: </label>
										<div class="col-sm-10" id="editBuildTextArea">
											<textarea rows="5" class="summernote" tabindex="2"
												 name="buildDescription">${model.buildDetails[0].build_desc}</textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Status: </label>
										<div class="col-sm-10">
											<div class="radio radio-success radio-inline col-sm-2">
												<input type="radio" id="activeLabel" value="1"
													${model.buildDetails[0].buildStatus == 'Active' ? 'checked="true" tabindex="3"' : ""}
													name="buildStatus"> <label for="activeLabel">Active</label>
											</div>
											<div class="radio radio-danger radio-inline col-sm-2">
												<input type="radio" id="inactiveLabel" value="2"
													${model.buildDetails[0].buildStatus == 'Inactive' ? 'checked="true" tabindex="3"' : ""}
													name="buildStatus"> <label for="inactiveLabel">Inactive</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="hr-line-solid"></div>
						</fieldset>
					</div>
					<div class="actions clearfix">
						<div class="row">
							<div class="col-md-1">
								<button class="btn btn-success" type="button" id="editBuildBtn" tabindex="4">Edit</button>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
								<button class="btn btn-white pull-left hidden"
									style="margin-right: 15px;" type="button"
									id="editBuildCancelbtn" tabindex="6">Cancel</button>
								<button
									class="btn btn-success pull-left hidden ladda-button ladda-button-demo"
									style="margin-right: 15px;" type="submit"
									id="editBuildSubmitbtn" data-style="slide-up" tabindex="5">Submit</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">

	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
	
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});
</script>

<script>
	var config = {
		'.chosen-select' : {},
		'.chosen-select-deselect' : {
			allow_single_deselect : true
		},
		'.chosen-select-no-single' : {
			disable_search_threshold : 10
		},
		'.chosen-select-no-results' : {
			no_results_text : 'Oops, nothing found!'
		},
		'.chosen-select-width' : {
			width : "95%"
		}
	}
	for ( var selector in config) {
		$(selector).chosen(config[selector]);
	}
	$("#editBuildCancelbtn").click(function() {
		window.location.href="viewbuild";
	});
	
	$('#editBuildBtn').click(function() {
		$('#editBuildFieldSet').removeAttr('disabled');
		$('#editBuildTextArea').find('button').removeAttr('disabled');
		$('#selectBuildReleaseId').removeAttr('disabled');
		$('#editBuildCancelbtn').removeClass('hidden');
		$('#editBuildSubmitbtn').removeClass('hidden');
		$('#editBuildBtn').addClass('hidden');
		$('.note-toolbar').show();
		$('.note-editable').attr('contenteditable','true');
		
	});
	
	$(document).ready(function() {
	
		var viewType = $('#hiddenBuildViewType').val();
		if (viewType == "Edit") {
			$('#editBuildFieldSet').removeAttr('disabled');
			$('#editBuildTextArea').find('button').removeAttr('disabled');
			$('#editBuildCancelbtn').removeClass('hidden');
			$('#editBuildSubmitbtn').removeClass('hidden');
			$('#editBuildBtn').addClass('hidden');
			$('#selectBuildReleaseId').removeAttr('disabled');
			$('.chosen-container').removeAttr('chosen-disabled');
			$('#selectBuildReleaseId_chosen').removeAttr('disabled');
			$('.breadcrumbText').text('Edit');
			
			 $('.summernote').summernote('disable');
			    $('.note-toolbar').show();
				$('.note-editor').css('background','white');
				$('button[ data-event="codeview"]').remove();
				
				 $('div.note-insert').remove();
		         $('div.note-table').remove();
		         $('div.note-help').remove();
		         $('div.note-style').remove();
		         $('div.note-color').remove();
		         $('button[ data-event="removeFormat"]').remove();
		         $('button[ data-event="insertUnorderedList"]').remove();
		         $('button[ data-event="fullscreen"]').remove();
		         $('button[ data-original-title="Line Height"]').remove();
		         $('button[ data-original-title="Font Family"]').remove();
		         $('button[ data-original-title="Paragraph"]').remove(); 
		}
		else
		{ 
		    $('.summernote').summernote('disable');
		    $('.note-toolbar').hide();
			$('.note-editor').css('background','white');
			$('button[ data-event="codeview"]').remove();
			
			 $('div.note-insert').remove();
	         $('div.note-table').remove();
	         $('div.note-help').remove();
	         $('div.note-style').remove();
	         $('div.note-color').remove();
	         $('button[ data-event="removeFormat"]').remove();
	         $('button[ data-event="insertUnorderedList"]').remove();
	         $('button[ data-event="fullscreen"]').remove();
	         $('button[ data-original-title="Line Height"]').remove();
	         $('button[ data-original-title="Font Family"]').remove();
	         $('button[ data-original-title="Paragraph"]').remove();
		}
		  
	
		$("#editBuildForm").validate({
			rules : {
				buildName : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				buildDescription : {
					minlength : 1,
					maxlength : 200
				}
			}
		});
		
	});

	//var l = $('.ladda-button-demo').ladda();
	$("#editBuildSubmitbtn").click(function() {
		if($('#editBuildForm').valid()){
			
			$("#barInMenu > p").text('submitting');
				
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
				
			 $('.summernote').each( function() {
				   $(this).val($(this).code());
				   })
				   
			$('#editBuildForm').submit();

		}
	});
</script>
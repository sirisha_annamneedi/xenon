<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Release List</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li class="active"><strong>Releases</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>Releases</h5>
					<c:if test="${model.createReleaseAccessStat == 1}">
						<div class="ibox-tools">
							<button type="button"
													class="btn btn-success btn-xs" onclick="location.href = 'createrelease';">
								Add Release </button>
						</div>
					</c:if>
				</div>
				<div class="ibox-content">
					<table class="table table-stripped"
						id="allReleasetable">
						<thead>
							<tr>

								<th>Name</th>
								<!-- <th data-hide="all">Description</th> -->
								<th>Start Date</th>
								<th>End Date</th>
								<th>Status</th>
								<th style="text-align: right;">Action</th>

							</tr>
						</thead>
						<tbody>
							<c:forEach var="data" items="${model.releaseDetails}">
								<tr>

									<td name="${data.release_id}" class="mdTextAlignment" data-original-title="${data.release_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.release_name}</td>
									<%-- <td>${data.release_description}</td> --%>
									<td class="mdTextAlignment" data-original-title="${data.startDate}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.startDate}</td>
									<td class="mdTextAlignment" data-original-title="${data.endDate}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.endDate}</td>
									<td><span class="label releaseStatusLabel">${data.releaseStatus}</span>
									</td>
									<td class="text-right">
										<div class="btn-group">
											<button class="btn-white btn btn-xs"
												onclick="viewRelease(${data.release_id},'Edit')">Edit</button>
										</div>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<input type="hidden" id="hiddenInput"
	value=<%=session.getAttribute("releaseCreateStatus")%>>
	
<input type="hidden" id="newReleaseName"
	value='<%=session.getAttribute("newReleaseName")%>'>

<script type="text/javascript">

$(window).load(function() {
	 $('body').removeClass("white-bg");
	 $("#barInMenu").addClass("hidden");
	 $("#wrapper").removeClass("hidden");
	
	 /* var foo =  */$('#allReleasetable').dataTable();
	/*  foo.trigger('footable_initialize'); //Reinitialize
	 foo.trigger('footable_redraw'); //Redraw the table
	 foo.trigger('footable_resize'); //Resize the table  */
});

  $(function () {

	  var state = $('#hiddenInput').val();
		var newProjectName = $('#newReleaseName').val();
		if(state == 1){
			var message =" Release is successfully created";
			var toasterMessage = newProjectName +  message;
			showToster(toasterMessage);
		}
		else if(state == 2){
			var message =" Release is successfully Updated";
			var toasterMessage = newProjectName +  message;
			showToster(toasterMessage);
		}
		
		$('#hiddenInput').val('11');
	});  
  
  function showToster(toasterMessage){
		 toastr.options = {
				 "closeButton": true,
				  "debug": true,
				  "progressBar": true,
				  "preventDuplicates": true,
				  "positionClass": "toast-top-right",
				  "showDuration": "400",
				  "hideDuration": "1000",
				  "timeOut": "9000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "slideDown",
				  "hideMethod": "slideUp"
       };
       toastr.success('',toasterMessage);
	}  
</script>

<script>
$(document).ready(function() {



	$('.releaseStatusLabel').each(function(){
		var status = $(this).text();
		if(status.toLowerCase() == 'active')
			$(this).addClass('label-primary');
		else
			$(this).addClass('label-danger');
	});

});

function viewRelease(id,viewType){
	
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	
	var posting = $.post('setreleaseid',{
		releaseID : id,
		viewType : viewType
	});
	posting.done(function(data){
		location.href ="editrelease";
	});
}

</script>
<%
	//set session variable to another value
	session.setAttribute("releaseCreateStatus",3);
%>
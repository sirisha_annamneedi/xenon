<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<input type="hidden"
	value="<%=session.getAttribute("selectedReleaseViewType")%>"
	id="hiddenInputType">
<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Edit Release - ${releaseDetails[0].release_name}</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li><a href="viewrelease">Releases</a></li>
			<li class="active"><strong>Edit Release</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form action="updaterelease" id="form"
						class="wizard-big wizard clearfix form-horizontal" method="POST">
						<div class="content clearfix">

							<fieldset class="body current" disabled id="editReleaseFieldSet">
								<div class="row">

									<div class="col-lg-10">
										<div class="form-group hidden">
											<label class="control-label col-sm-2">Release ID:</label>
											<div class="col-sm-10">
												<input type="text" id="" placeholder="release ID"
													class="form-control" name="releaseID"
													value="${releaseDetails[0].release_id}">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-2">Name:* </label>
											<div class="col-sm-10">
												<input type="text" id="txtReleaseName"
													placeholder="Release Name" class="form-control characters"
													name="releaseName"
													value="${releaseDetails[0].release_name}">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label"> Description:</label>
											<div class="col-sm-10" id="editReleaseTextArea">
												<!-- <textarea class="form-control" style="max-width: 100%;"></textarea> -->
												<textarea name="releaseDescription"
													id="txtReleaseDescription"  rows="5"
													class="summernote"  tabindex="1"> ${releaseDetails[0].release_description} </textarea>
											</div>
										</div>
										
										<div class="form-group" id="dateRange">
											<label class="col-sm-2 control-label">Date:</label>
											<div class="col-sm-3">
											<input type="text" class="input-sm form-control"
														name="startDate"
														value='<fmt:formatDate pattern="MM/dd/yyyy" value="${releaseDetails[0].startDate}" />' 
														disabled>
														</div><div class="col-sm-7">
														<label class="col-sm-3 control-label">Date To:</label>
												<div class="input-daterange input-group" id="datepicker">
													<input type="text" class="input-sm form-control"
														name="endDate" 
														value='<fmt:formatDate pattern="MM/dd/yyyy"  value="${releaseDetails[0].endDate}" />' >
														
												</div>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">Status: </label>
											<div class="col-sm-10">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="inlineRadio1" value="1"
														name="releaseStatus" tabindex="2"
														${releaseDetails[0].releaseStatus == 'Active' ? 'checked="true"' : ""}>
													<label for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inlineRadio2" value="2"
														name="releaseStatus"
														${releaseDetails[0].releaseStatus == 'Inactive' ? 'checked="true"' : ""}>
													<label for="inlineRadio2"> Inactive </label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="hr-line-solid"></div>
							</fieldset>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-md-1">
									<button class="btn btn-success" type="button"
										id="editReleaseBtn" tabindex="3">Edit</button>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-white pull-left hidden"
										style="margin-right: 15px;" type="button"
										id="editReleaseCancelBtn" tabindex="3">Cancel</button>
									<button
										class="btn btn-success pull-left hidden ladda-button ladda-button-demo"
										style="margin-right: 15px;" type="button"
										id="editReleaseSubmitBtn" data-style="slide-up" tabindex="4">Submit</button>
								</div>
							</div>
						</div>
					</form>
					<!-- end form -->
				</div>
				<!-- end ibox-content -->

			</div>
			<!-- end ibox -->
		</div>
		<!-- end col -->
	</div>
	<!-- end row -->
</div>
<!-- end wrapper -->
</div>
<script type="text/javascript">

	$(window).load(function() {
		 $('body').removeClass("white-bg");
		  $("#barInMenu").addClass("hidden");
		  $("#wrapper").removeClass("hidden");
	});

	$(document).ready(function() {
		
		var viewType = $('#hiddenInputType').val();
		if (viewType == "Edit") {
			$('#editReleaseFieldSet').removeAttr("disabled");
			$('#editReleaseTextArea').find('button').removeAttr('disabled');
			$('#editReleaseCancelBtn').removeClass('hidden');
			$('#editReleaseSubmitBtn').removeClass('hidden');
			$('#editReleaseBtn').addClass('hidden');
			$('#txtReleaseName').attr("disabled", true);

		}
		
		
		$('.summernote').summernote();
		$('.note-editor').css('background','white');
		$('button[ data-event="codeview"]').remove();
		
		 $('div.note-insert').remove();
         $('div.note-table').remove();
         $('div.note-help').remove();
         $('div.note-style').remove();
         $('div.note-color').remove();
         $('button[ data-event="removeFormat"]').remove();
         $('button[ data-event="insertUnorderedList"]').remove();
         $('button[ data-event="fullscreen"]').remove();
         $('button[ data-original-title="Line Height"]').remove();
         $('button[ data-original-title="Font Family"]').remove();
         $('button[ data-original-title="Paragraph"]').remove();

		
		//release end date validation
		jQuery.validator.addMethod("selectnic", function(value, element){
			var  startDate = $('input[name="startDate"]').val();
			var endDate = $('input[name="endDate"]').val();
			if(new Date(endDate).getTime() < new Date(startDate).getTime()){
		        return false;  // FAIL validation 
		    } else {
		        return true;   // PASS validation 
		    };
		}, "Please select proper end date"); 
		
		$("#form").validate({
			rules : {
				releaseName : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				releaseDescription : {
					minlength : 1,
					maxlength : 200
				},
				endDate :{
					 selectnic: true 
					/* date:true,
					required : true,
					min:$('input[name="startDate"]').val() */
				}
			}
		});

		$('#dateRange .input-daterange').datepicker({
			keyboardNavigation : false,
			forceParse : false,
			autoclose : true
		});
		
});

$('#editReleaseCancelBtn').click(function() {
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	location.href = "viewrelease";
});

	$('#editReleaseBtn').click(function() {
		$('#editReleaseFieldSet').removeAttr("disabled");
		$('#editReleaseTextArea').find('button').removeAttr('disabled');
		$('#editReleaseCancelBtn').removeClass('hidden');
		$('#editReleaseSubmitBtn').removeClass('hidden');
		$('#editReleaseBtn').addClass('hidden');
	});
	
	//var l = $('.ladda-button-demo').ladda();
	$("#editReleaseSubmitBtn").click(function() {
		var formValid = $("#form").valid();
		if(formValid){
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
			 
			$('.summernote').each( function() {
				   $(this).val($(this).code());
				   })
			$('#txtReleaseName').attr("disabled", false);
			
			$('#form').submit();
		}
	});
	
</script>

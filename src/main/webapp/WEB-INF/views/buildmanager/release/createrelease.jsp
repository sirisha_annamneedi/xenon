<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Add Release</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li class="active"><strong>Add Release</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form action="insertrelease" id="form"
						class="wizard-big wizard clearfix form-horizontal" method="POST">
						<div class="content clearfix">
							<fieldset class="body current">
							<div class="row">
								<label class="col-lg-4 text-right">* fields are mandatory</label>
							</div>
								<div class="row">
									<div class="col-lg-10">
										<c:if test="${Model.error == 1}">
											<div class="alert alert-danger">Release name already
												exists, Please try another.</div>
										</c:if>
										<div class="form-group">
											<label class="control-label col-sm-2">Name:* </label>
											<div class="col-sm-10">
												<input type="text" id="txtReleaseName"
													placeholder="Release Name" class="form-control characters"
													name="releaseName" tabindex="1">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label"> Description:</label>
											<div class="col-sm-10">
											
												<textarea name="releaseDescription"
													id="txtReleaseDescription" rows="5"
													class="summernote" tabindex="2"> </textarea>
											</div>
										</div>
										<div class="form-group" id="dateRange">
											<label class="col-sm-2 control-label">Date:</label>
											<div class="col-sm-10">
												<div class="input-daterange input-group" id="datepicker">
													<input type="text" class="input-sm form-control todaysDate"
														name="startDate" tabindex="3"> <span class="input-group-addon">to</span>
													<input type="text" class="input-sm form-control todaysDate"
														name="endDate" tabindex="4">
												</div>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">Status: </label>
											<div class="col-sm-10">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="inlineRadio1" value="1"
														name="releaseStatus" checked="" tabindex="5"> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inlineRadio2" value="2"
														name="releaseStatus"> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="hr-line-solid"></div>
							</fieldset>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-white pull-left"
										style="margin-right: 15px;" type="button"
										id="createReleaseCancelBtn" tabindex="6">Cancel</button>
									<button
										class="btn btn-success pull-left ladda-button ladda-button-demo"
										style="margin-right: 15px;" type="button"
										data-style="slide-up" tabindex="7" id="submitBtn">Submit</button>
								</div>
							</div>
						</div>
					</form>
					<!-- end form -->
				</div>
				<!-- end ibox-content -->

			</div>
			<!-- end ibox -->
		</div>
		<!-- end col -->
	</div>
	<!-- end row -->
</div>
<!-- end wrapper -->
</div>
<script type="text/javascript">

    $(window).load(function() {
	  $('body').removeClass("white-bg");
	  $("#barInMenu").addClass("hidden");
	  $("#wrapper").removeClass("hidden");
	  
	  $('.summernote').summernote();
		$('.note-editor').css('background','white');
		$('button[ data-event="codeview"]').remove();
		
		 $('div.note-insert').remove();
         $('div.note-table').remove();
         $('div.note-help').remove();
         $('div.note-style').remove();
         $('div.note-color').remove();
         $('button[ data-event="removeFormat"]').remove();
         $('button[ data-event="insertUnorderedList"]').remove();
         $('button[ data-event="fullscreen"]').remove();
         $('button[ data-original-title="Line Height"]').remove();
         $('button[ data-original-title="Font Family"]').remove();
         $('button[ data-original-title="Paragraph"]').remove();
 
	  
	});
	
  $(function () {
	  
	  $('.todaysDate').each(function(){
		  $(this).val(moment().format('MM/DD/YYYY'));
	  });
	  
	  $("#form").validate({
	        rules: {
	        	releaseName: {
	                required: true,
	                minlength: 1,
	                maxlength: 40
	            },
	            releaseDescription: {
	                minlength: 1,
	                maxlength: 200
	            }
	        }
		
	    });

	$('#dateRange .input-daterange').datepicker({
		keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });
	
}); 
	
	$('#createReleaseCancelBtn').click(function(){
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		window.location.href="viewrelease";
	});
	
	var l = $('.ladda-button-demo').ladda();
	$("#submitBtn").click(function() {
		var formValid = $("#form").valid();
		if(formValid){
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");	
			 
			$('.summernote').each( function() {
				   $(this).val($(this).code());
				   })
			
			$('#form').submit();
		}
	});
	
</script>

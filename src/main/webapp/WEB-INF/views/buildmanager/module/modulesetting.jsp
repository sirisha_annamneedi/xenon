<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Module Settings</h2>
			<ol class="breadcrumb">
				<li><a href="bmdashboard">Dashboard</a></li>
				<li><a href="viewmodules">Modules</a></li>
				<li class="active"><strong>Module Setting</strong></li>
			</ol>
		</div>
	</div>

	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-lg-12">
				<div class="tabs-container">
					<ul class="nav nav-tabs">
						<li class="<c:if test="${model.settingTab == 1}">active </c:if>"><a data-toggle="tab" href="#detailsTab">
								Edit </a></li>
									<c:if test="${model.moduleDetails[0].module_active == 1}">
									<li class="<c:if test="${model.settingTab == 2}">active </c:if>"><a data-toggle="tab" href="#membersTab">
								Members </a></li>
									</c:if>
						
					</ul>
					<div class="tab-content">
						<div id="detailsTab" class="tab-pane <c:if test="${model.settingTab == 1}">active </c:if>">
							<div class="panel-body">
								<div class="ibox">
									<div class="ibox-content">
										<form action="updatemodule" id="form" method="POST"
											class="wizard-big wizard clearfix form-horizontal">
											<div class="content clearfix">
												<fieldset class="body current" id="fieldset">
													<div class="row">
														<div class="col-lg-10">

															<div class="form-group hidden">
																<label class="control-label col-sm-2">Module Id</label>
																<div class="col-sm-10">
																	<input type="text" placeholder="User Id"
																		class="form-control"
																		value="${model.moduleDetails[0].module_id}"
																		name="moduleId">
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-sm-2">Module
																	Name:* </label>
																<div class="col-sm-10">
																	<input type="text"
																		value="${model.moduleDetails[0].module_name}"
																		placeholder="Module Name"
																		class="form-control characters" name="moduleName">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-2 control-label">Module
																	Description: </label>
																<div class="col-sm-10" id="txtArea">
																	<!-- <textarea class="form-control " style="max-width: 100%;"></textarea> -->
																	<textarea name="moduleDescription" rows="5"
																		class="summernote">${model.moduleDetails[0].module_description}</textarea>
																</div>
															</div>

															<div class="form-group">
																<label class="col-sm-2 control-label">Module
																	Status: </label>
																<div class="col-sm-10">
																	<div class="radio radio-success radio-inline col-sm-2">
																		<input type="radio" value="1" name="moduleStatus"
																			${model.moduleDetails[0].module_active == 1 ? 'checked="true"' : ""}>
																		<label for="inlineRadio1"> Active </label>
																	</div>
																	<div class="radio radio-danger radio-inline col-sm-2">
																		<input type="radio" value="2" name="moduleStatus"
																			${model.moduleDetails[0].module_active == 2 ? 'checked="true"' : ""}>
																		<label for="inlineRadio2"> Inactive </label>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="hr-line-solid"></div>
												</fieldset>
											</div>
											<div class="actions clearfix">
												<div class="row">
													<div class="col-sm-5">

														<button class="btn btn-white pull-left"
															style="margin-right: 15px;" type="button" id="btnCancel">Cancel</button>
														<button
															class="btn btn-success pull-left ladda-button ladda-button-demo"
															type="submit" id="btnUpdate" data-style="slide-up">Update</button>

													</div>
												</div>
											</div>
										</form>
										<!-- end form -->
									</div>
									<!-- end ibox-content -->

								</div>
								<!-- end ibox -->
							</div>
						</div>
						<div id="membersTab" class="tab-pane <c:if test="${model.settingTab == 2}">active </c:if>">
							<div class="panel-body">
								<div class="ibox ">
									<div class="ibox-title">
										<strong>${model.moduleDetails[0].module_name} Members</strong>
										<button class="btn btn-success btn-sm pull-right"
											id="showUsersBtn">Add users to module</button>
									</div>
									<div class="ibox-content">
										<div class="panel panel-default hidden">
											<div class="panel-heading">Add users</div>
											<form action="assignorunassignmoduletouser"
												class="wizard-big wizard clearfix form-horizontal"
												id="addUsertoModule" method="POST"
												style="overflow: visible !important;">
												<div class="panel-body">
													<div class="content clearfix"
														style="overflow: visible !important;">
														<fieldset class="body current">
															<div class="form-group">
																<label class="col-lg-3 control-label">Select
																	Users*:</label>
																<div class="col-lg-7">
																	<select data-placeholder="Please Select Users"
																		class="chosen-select-add" aria-required="true"
																		multiple name="selectedUsers" id="selectMultiUser"
																		style="min-width: 100%; z-index: 9999;">
																		<c:forEach var="user"
																			items="${model.moduleUNAssignedUserDetails}">
																			<option value="${user.user_id}">${user.first_name} ${user.last_name}</option>
																		</c:forEach>
																	</select>
																	<div class="hidden" id="multiSelect">
																		<span class="error" style="font-weight: bold;color: #8a1f11;">This
																			field is required.</span>
																	</div>
																</div>
															</div>

														</fieldset>
													</div>

												</div>
												<div class="panel-footer">
													<button class="btn btn-white" type="button" id="cancelAddUsersBtn">Cancel</button>
													<button class="btn btn-success" type="button" id="addUserModule">Submit</button>

												</div>
											</form>
										</div>
										<div class="table-responsive">
											<table class="table table-striped table-hover"
												id="membersTable">
												<thead>
													<tr>
														<th>Photo</th>
														<th>User</th>
														<th>Email</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data"
														items="${model.moduleAssignedUserDetails}">
														<tr class="assignModuleRow">
															<td class="client-avatar"><img alt="image"
																class="img-circle"
																src="data:image/jpg;base64,${data.photo}"></td>
															<td>${data.first_name}&nbsp;${data.last_name}</td>
															<td>${data.email_id}</td>

															<td>
																<button class="btn-white btn btn-xs"
																	onclick="removeUser(${data.user_id},${model.moduleIdForAssign},'member')"
																	name="${data.user_id}">Remove</button>
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});

	$(function() {
		$('#membersTable').DataTable();
	});

	$('#showUsersBtn').click(function() {
		$('.panel').removeClass('hidden');
		$(".chosen-select-add").chosen();
	});

	$('#addUsersBtn').click(function() {
		location.reload();
	});

	$('#cancelAddUsersBtn').click(function() {
		$('.panel').addClass('hidden');
	});
</script>


<script type="text/javascript">
	$("#btnEdit").click(function() {
		$('#fieldset').removeAttr("disabled");
		$("#txtArea").find('button').removeAttr("disabled");
		$('#btnCancel').show();
		$('#btnUpdate').show();
		$(this).hide();
	});

	$("#btnCancel").click(function() {
		location.href = "viewmodules";
	});

	$(document).ready(function() {

		$('.summernote').summernote();
		$('.note-editor').css('background', 'white');
		$('button[ data-event="codeview"]').remove();

		 $('div.note-insert').remove();
         $('div.note-table').remove();
         $('div.note-help').remove();
         $('div.note-style').remove();
         $('div.note-color').remove();
         $('button[ data-event="removeFormat"]').remove();
         $('button[ data-event="insertUnorderedList"]').remove();
         $('button[ data-event="fullscreen"]').remove();
         $('button[ data-original-title="Line Height"]').remove();
         $('button[ data-original-title="Font Family"]').remove();
         $('button[ data-original-title="Paragraph"]').remove();


		$("#form").validate({
			rules : {
				moduleName : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				moduleDescription : {
					minlength : 1,
					maxlength : 200
				}
			}
		});
	});
	
	function removeUser(user_id,module_id){
		
		swal({
			title : "Are you sure?",
			text : "Do you want to remove user from module!",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "Yes, Remove it!",
			cancelButtonText : "No, cancel!",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function(isConfirm) {

			if (isConfirm) {
				
				$('body').addClass("white-bg");
				$("#barInMenu").removeClass("hidden");
				$("#wrapper").addClass("hidden");
				
				var posting = $.post('removemoduleuser',{
					userId : user_id,
					moduleId : module_id
				});
				
				posting.done(function(data){
					window.location.href = "modulesetting";
				}); 	
			}
		});
		
	}
	
	$("#addUserModule").click(function()
			{
		
		var values = $('#selectMultiUser').val();
	    
		
			 if($("#addUsertoModule").valid())
				{
				 if(values!=null)
					 {
					    $('body').addClass("white-bg");
						$("#barInMenu").removeClass("hidden");
						$("#wrapper").addClass("hidden");
						
					 $("#addUsertoModule").submit();
					 }
				 else
					 {
					 $("#multiSelect").removeClass("hidden");
					 } 
				}
			});
	
	$("#selectMultiUser").change(function() {
		$("#multiSelect").addClass("hidden");
	});
	

	$("#btnUpdate").click(function() {
		var formValid = $("#form").valid();
		if (formValid) {

			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");

			$('.summernote').each(function() {
				$(this).val($(this).code());
			})

			$('#form').submit();
		}
	});
</script>

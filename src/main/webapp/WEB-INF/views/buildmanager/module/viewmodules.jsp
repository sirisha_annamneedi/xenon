<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Modules</h2>
			<ol class="breadcrumb">
				<li><a href="bmdashboard">Dashboard</a></li>
				<li class="active"><strong>Modules</strong></li>
			</ol>
		</div>
	</div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Modules</h5>
						<c:if test="${model.createModuleAccessStatus == 1}">
							<div class="ibox-tools">
								<button type="button" class="btn btn-success btn-xs"
									onclick="location.href = 'createmodule';">Add Module</button>
							</div>
						</c:if>
					</div>

					<%-- <div class="content clearfix ibox-content">
						<fieldset class="body current">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<label class="col-sm-2 control-label">Application*: </label>
										<div class="col-sm-4">
											<select data-placeholder="Choose a Application"
												class="chosen-select" id="buildApplication"
												name="buildApplication" tabindex="1" style="width: 350px;">

												<option value="0">All</option>
												<c:forEach var="projects" items="${model.project}">
													<option value="${projects.project_id}">${projects.project_name}</option>
												</c:forEach>
											</select>
										</div>

									</div>

								</div>
							</div>
						</fieldset>
					</div> --%>
					<div class="ibox-content">
						<table class="table table-hover" id="viewModulesTable">
							<thead>
								<tr>
									<td style="width :41%"><input type="text" class="form-control" style="width :100%" /></td>
									<td style="width :41%"><input type="text" class="form-control" style="width :100%" /></td>
									<td style="width :10%"></td>
									<td class="text-right" style="width :10%"></td>
								</tr>
								
								<tr>
									<th>Module Name</th>
									<th>Application Name</th>
									<th>Status</th>
									<th class="text-right">Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="modules" items="${model.module}">
									<tr class="row_${modules.project_id} addHidden">
										<td class="xlTextAlignment"
											data-original-title="${modules.module_name}"
											data-container="body" data-toggle="tooltip"
											data-placement="right" class="issue-info">${modules.module_name}</td>
										<td class="xlTextAlignment"
											data-original-title="${modules.project_name}"
											data-container="body" data-toggle="tooltip"
											data-placement="right" class="issue-info">${modules.project_name}</td>
										<c:if test="${modules.module_active == 1}">
											<td><span class="label label-primary">Active</span></td>
										</c:if>
										<c:if test="${modules.module_active == 2}">
											<td><span class="label label-danger">Inactive</span></td>
										</c:if>
										<td class="text-right">
											<div class="btn-group">
												<button class="btn-white btn btn-xs"
													onclick="settingModule(${modules.module_id},${modules.project_id},'${modules.module_name}')"
													name="${modules.module_id}">Setting</button>
													<c:if test="${model.roleId == 2}">
												<button class="btn-white btn btn-xs"
													onclick="removeModule(${modules.module_id})"
													name="${modules.module_id}">Delete</button>
													</c:if>
											</div>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-Level Scripts -->

<input type="hidden" id="hiddenInput"
	value='<%=session.getAttribute("moduleCreateStatus")%>'>

<input type="hidden" id="newModuleName"
	value='<%=session.getAttribute("newModuleName")%>'>

<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});
  $(function () {
	  
	   var state = $('#hiddenInput').val();
		var newBuildName = $('#newModuleName').val();
		
		if(state == 1){
			var message =" Module is successfully created";
			var toasterMessage = newBuildName +  message;
			showToster(toasterMessage);
		}
		if(state==2)
			{
				var message =" Module is successfully updated";
				var toasterMessage = newBuildName +  message;
				showToster(toasterMessage);
			}

		$('#hiddenInput').val('11');
	
  	var today = moment().format('MMMM Do YYYY, h:mm:ss a');
				var table = $('#viewModulesTable').DataTable(
						{
							dom : '<"html5buttons"B>lTfgitp',
							buttons : [
									{
										extend : 'csv',
										title : 'View All Modules - ' + today,
										exportOptions: {
						                    columns: ':visible'
						                }
									},
									{
										extend : 'excel',
										title : 'View All Modules - ' + today,
										exportOptions: {
						                    columns: ':visible'
						                }
									},
									{
										extend : 'pdf',
										title : 'View All Modules - ' + today,
										exportOptions: {
						                    columns: ':visible'
						                }
									},

									{
										extend : 'print',
										exportOptions: {
						                    columns: ':visible'
						                },
										customize : function(win) {
											$(win.document.body).addClass(
													'white-bg');
											$(win.document.body).css(
													'font-size', '10px');

											$(win.document.body).find('table')
													.addClass('compact').css(
															'font-size',
															'inherit');
										},
										title : 'View All Modules - ' + today
									} ]/* ,
							"aaSorting": [[0, "desc"]] */
						});
				
				// Apply the filter
			    $("#viewModulesTable thead input").on( 'keyup change', function () {
			    	table
			            .columns( $(this).parent().index()+':visible' )
			            .search( this.value )
			            .draw();
			    } );
  });  

</script>
<script>
	$(document).ready(function() {
		var config = {
				'.chosen-select' : {},
				'.chosen-select-deselect' : {
					allow_single_deselect : true
				},
				'.chosen-select-no-single' : {
					disable_search_threshold : 10
				},
				'.chosen-select-no-results' : {
					no_results_text : 'Oops, nothing found!'
				},
				'.chosen-select-width' : {
					width : "95%"
				}
			}
		for ( var selector in config) {
			$(selector).chosen(config[selector]);
			$("ul.chosen-results").css('max-height','250px');
		}
		
		
	});

	
	$("#buildApplication").change(function() {
		
		var projId = $(this).find(":selected").val();
		$(".addHidden").addClass( "hidden" );
		
		if(projId == 0){
			$(".addHidden").removeClass("hidden");
		}else{
			
			$(".row_"+projId).each(function() {
				  $( this ).removeClass( "hidden" );
			});  
		}
		
	});
	
	function settingModule(id,projectID,moduleName){
		$('body').addClass("white-bg");
		$("#barInMenu").removeClass("hidden");
		$("#wrapper").addClass("hidden");	
		
		var posting = $.post('setmoduleidforassign',{
			moduleID : id,
			projectID : projectID,
			moduleName: moduleName
		});
		
		posting.done(function(data){
			location.href  = "modulesetting";
		}); 
	}
	
	
	function showToster(toasterMessage){
		 toastr.options = {
				 "closeButton": true,
				  "debug": true,
				  "progressBar": true,
				  "preventDuplicates": true,
				  "positionClass": "toast-top-right",
				  "showDuration": "400",
				  "hideDuration": "1000",
				  "timeOut": "9000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "slideDown",
				  "hideMethod": "slideUp"
	     };
	     toastr.success('',toasterMessage);
	} 
	
	function removeModule(module_id){
		
		swal({
			title : "Are you sure?",
			text : "Do you want to remove module!",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "Yes, Remove it!",
			cancelButtonText : "No, cancel!",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function(isConfirm) {

			if (isConfirm) {
				
				$('body').addClass("white-bg");
				$("#barInMenu").removeClass("hidden");
				$("#wrapper").addClass("hidden");
				
				var posting = $.post('removemodule',{
					moduleId : module_id
				});
				
				posting.done(function(data){
					window.location.href = "viewmodules";
				}); 	
			}
		});
		
	}
	<%//set session variable to another value
			session.setAttribute("moduleCreateStatus", 3);%>
</script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Create New Module</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li class="active"><strong>New Module</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<form action="insertmodule" method="POST" id="form"
						class="wizard-big wizard clearfix form-horizontal">
						<div class="content clearfix">
							<fieldset class="body current">
							<div class="row">
								<label class="col-lg-4 text-right">* fields are mandatory</label>
							</div>
								<div class="row">
									<div class="col-lg-10">
										<div class="form-group">
											<c:if test="${Model.error == 1}">
												<div class="alert alert-danger">Module name already
													exists, Please try another.</div>
											</c:if>
											<label class="col-sm-2 control-label">Application:*</label>
											<div class="col-sm-6">
												<select data-placeholder="Choose a application"
													class="chosen-select" style="width: 350px;"
													name="projectId">
													<c:forEach var="project" items="${Model.projects}">
														<option value="${project.project_id}"
														${project.project_id == Model.selectedProject ? 'selected="selected"' : ''}
														>${project.project_name}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-2">Name:*</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Module Name"
													class="form-control characters" name="moduleName" tabindex="1">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label"> Description:</label>
											<div class="col-sm-10">
												<!-- <textarea class="form-control" style="max-width: 100%;"></textarea> -->
												<textarea name="moduleDescription" 
													rows="5" class="summernote"  tabindex="2"> </textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Status:</label>
											<div class="col-sm-10">
												<div class="radio radio-success radio-inline col-sm-2 ">
													<input type="radio" id="inlineRadio1" value="1"
														name="moduleStatus" checked=""  tabindex="3"> <label
														for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inlineRadio2" value="2"
														name="moduleStatus"  tabindex="4"> <label for="inlineRadio2">
														Inactive </label>
												</div>
											</div>
										</div>
									</div>
								</div>
							<div class="hr-line-solid"></div>
							</fieldset>
						</div>
						<div class="actions clearfix">
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-white pull-left" id="createModuleCancel"
										style="margin-right: 15px;" type="button"  tabindex="5">Cancel</button>
									<button
										class="btn btn-success pull-left ladda-button ladda-button-demo"
										style="margin-right: 15px;" type="button"
										data-style="slide-up"  tabindex="6" id="submitBtn">Submit</button>
								</div>
							</div>
						</div>
					</form>
					<!-- end form -->
				</div>
				<!-- end ibox-content -->

			</div>
			<!-- end ibox -->
		</div>
		<!-- end col -->
	</div>
	<!-- end row -->
</div>
<!-- end wrapper -->
</div>
<script type="text/javascript">

$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	
	$('.summernote').summernote();
	$('.note-editor').css('background','white');
	$('button[ data-event="codeview"]').remove();
	
	 $('div.note-insert').remove();
     $('div.note-table').remove();
     $('div.note-help').remove();
     $('div.note-style').remove();
     $('div.note-color').remove();
     $('button[ data-event="removeFormat"]').remove();
     $('button[ data-event="insertUnorderedList"]').remove();
     $('button[ data-event="fullscreen"]').remove();
     $('button[ data-original-title="Line Height"]').remove();
     $('button[ data-original-title="Font Family"]').remove();
     $('button[ data-original-title="Paragraph"]').remove();

	
});

  $(function () {
	  
    //to apply choosen
	$(".chosen-select").chosen();
	$("ul.chosen-results").css('max-height','250px'); 
	 
    var projectCount = ${Model.projects.size()};
	  if(projectCount == 0){
		  swal(
				{
					title : "You need a minimum 1 application to create a module",
					text : "Please contact administrator."
				}, function(isConfirm) {
					if (isConfirm) {
						window.location.href="bmdashboard";
					}
				}		
		  );
	  }
	  
	  $("#form").validate({
			rules : {
				moduleName : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				moduleDescription : {
					minlength : 1,
					maxlength : 200
				}
			}
		});
	  
	});  

	/* var config = {
		'.chosen-select' : {},
		'.chosen-select-deselect' : {
			allow_single_deselect : true
		},
		'.chosen-select-no-single' : {
			disable_search_threshold : 10
		},
		'.chosen-select-no-results' : {
			no_results_text : 'Oops, nothing found!'
		},
		'.chosen-select-width' : {
			width : "95%"
		}
	}
	for ( var selector in config) {
		$(selector).chosen(config[selector]);
	} */
	
	$("#btnCancel").click(function() {
		window.location.href="createmodule";
	});
	
/* 	$(document).ready(function() {

		$("#form").validate({
			rules : {
				moduleName : {
					required : true,
					minlength : 1,
					maxlength : 40
				},
				moduleDescription : {
					minlength : 1,
					maxlength : 200
				}
			}
		});
	});
 */	
	$("span[class='switchery']").html="Active";
	var elem = document.querySelector('.js-switch');
	var switchery = new Switchery(elem, {
		color : '#1AB394'
	});

	var elem_2 = document.querySelector('.js-switch_2');
	var switchery_2 = new Switchery(elem_2, {
		color : '#ED5565'
	});
	
	$("#createModuleCancel").click(function(){
		window.location.href="viewmodules";
	});
	//var l = $('.ladda-button-demo').ladda();
	$("#submitBtn").click(function() {
		var formValid = $("#form").valid();
		if(formValid){
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");
			
			$('.summernote').each( function() {
				   $(this).val($(this).code());
				   });
			
			$('#form').submit();
		}
	});
</script>

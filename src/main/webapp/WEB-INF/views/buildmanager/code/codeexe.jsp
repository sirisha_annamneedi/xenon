<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<%=request.getContextPath()%>/resources/css/eclipse.css"
	rel="stylesheet">

<style>
.CodeMirror {
	min-height: 500px;
	height: auto;
	overflow: hidden;
}

.CodeMirror-scroll {
	overflow: hidden !important;
}
</style>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Add Code For Execution</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Home</a></li>
			<li class="active"><strong>Code Editor</strong></li>
		</ol>
	</div>
	<div class="col-lg-2"></div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">

		<div class="col-lg-8">
			<div class="ibox ">
				<div class="ibox-title">
					<h5>Code Editor</h5>
				</div>
				<div class="ibox-content">
					<div class="alert alert-danger" id="errorDiv"
						style="display: none;">Build name already exists, Please try
						another.</div>

					<div class="form-group">
						<label class="control-label col-sm-3">Build Name:</label>
						<div class="col-sm-7">
							<input type="text" placeholder="Build Name" class="form-control"
								name="buildName" id="buildName">
						</div>


					</div>
					<br> <br>

					<div class="form-group">
						<label class="control-label col-sm-3">Select Browser:</label>
						<div class="col-sm-7">
							<select id="exeBrowser" name="stepStatus"
								class="form-control m-b" style="margin-bottom: 0px;">

								<option value="FIREFOX">Firefox</option>
								<option value="CHROME">Chrome</option>
								<option value="IE">IE</option>

							</select>
						</div>




					</div>

					<br> <br>
					<div class="row">
						<div class="col-sm-3"></div>
						<div class="col-sm-7">
							<textarea id="userCode" style="width: 100%; overflow-x: hidden">
					
					</textarea>
						</div>
					</div>


				</div>

			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<button class="btn btn-white pull-left" style="margin-right: 15px;"
				button"="" id="createProCancelBtn">Cancel</button>
			<button class="btn btn-info" id="executeCode" type="button">Execute</button>
		</div>
	</div>
</div>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/chosen/chosen.jquery.js"></script>
<script>
$(window).load(function() {
	$("#barInMenu").fadeOut();
});
	$(document)
			.ready(
					function() {

						var userEditor = CodeMirror.fromTextArea(document
								.getElementById("userCode"), {
							lineNumbers : true,
							matchBrackets : true,
							styleActiveLine : true,
							theme : "eclipse"
						});

						$('#executeCode')
								.click(
										function() {

											$.ajax({
														type : "POST",
														async : true,
														url : "inserttrialbuild",
														data : {
															buildName : $("#buildName").val()
														},
														success : function(data) {
															 if (data == 'SERVER NOT RUNNING')
								                    		  {
									                    		  swal("Alert!", "Server VM Is Not Running At This Moment, Please Contact Admin.", "error");
									                    		  $('.confirm').click(function() {
										                                location.reload(true);
										                            });
								          	    			
								                    		  }
								                    	  else if (data == 'CLIENT VM NOT RUNNING')
								                    		  {
									                    		  swal("Alert!", "Client VM Is Not Running At This Moment, Please Contact Admin.", "error");
									                    		  $('.confirm').click(function() {
										                                location.reload(true);
										                            });
								                    		  }
								                    	  else if (data == 'OK') {

																$(this).prop('disabled',true);
																  swal("Added!", "Code Has Been Submitted For Execution.", "success");
																  
																$('.confirm').click(function() {
																					location.reload(true);
																				});

																var userCode = userEditor.getValue();

																$.ajax({
																			type : "POST",
																			crossDomain : true,
																			async : true,
																			url : "createfile",
																			data : {
																				codeString : encodeURI(userCode),
																				buildName : $("#buildName").val(),
																				exebrowser : $("#exeBrowser").val()
																			},

																			success : function(data) {
																				console.log("SUCCESS: ",data);

																			},
																			error : function(e) {
																				console.log("ERROR: ",e);

																			},
																			done : function(e) {
																				console.log("DONE");
																			}
																		});

															} else {
																$("#errorDiv").fadeIn();
															}
														}
													});

										});

					});
	
</script>

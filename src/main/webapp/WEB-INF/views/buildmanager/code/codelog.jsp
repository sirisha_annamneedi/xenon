<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style>
.CodeMirror {
	min-height: 500px;
	height: auto;
	overflow: hidden;
}

.CodeMirror-scroll {
	overflow: hidden !important;
}
</style>

<link href="<%=request.getContextPath()%>/resources/css/eclipse.css"
	rel="stylesheet">

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Execution Log</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Home</a></li>
			<li class="active"><strong>Execution Log</strong></li>
		</ol>
	</div>
	<div class="col-lg-2"></div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">

		<div class="col-lg-12">
			<div class="ibox ">
				<div class="ibox-title">
					<h5>Real-Time Automation Log Analysis</h5>
					<div class="ibox-tools">
						<button type="button" class="btn btn-success btn-xs"
							onclick="location.href = 'getlogfile?logpath=${model.logPath}'">Download Log</button>
					</div>
				</div>

				<div class="ibox-content">
					<textarea id="code2" style="overflow-x: hidden; display: none">
							
								${model.Logline}
							
					</textarea>
<div class="client-detail" style="max-height: 100%;">
						<div class="full-height-scroll">
					<textarea id="code1" style="overflow-x: hidden;"></textarea>
</div>
				</div>
			</div>

		</div>
	</div>

</div>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/chosen/chosen.jquery.js"></script>
	
<script>

$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	var userEditor = CodeMirror.fromTextArea(document
			.getElementById("code1"), {
		lineNumbers : true,
		matchBrackets : true,
		styleActiveLine : true,
		theme : "eclipse"
	});

	var userEdi2 = JSON.parse($('#code2').val());

	$.each(userEdi2, function(key, value) {
		$.each(value, function(key, value) {
			if (key == "log") {
				userEditor.setValue(userEditor.getValue() + "\n"
						+ value);
			}
		});
	});
});
			
</script>

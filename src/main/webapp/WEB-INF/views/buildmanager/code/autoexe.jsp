<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Build Execution</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Home</a></li>
			<li class="active"><strong>Build Execution</strong></li>
		</ol>
	</div>
	<div class="col-lg-2"></div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">

<div class="row">

		<div class="col-lg-6">
			<div class="ibox ">

				<div class="ibox-content">
				<form id="auto_exe">
					<div class="alert alert-danger" id="errorDiv"
						style="display: none;">Build name already exists, Please try
						another.</div>

					<div class="form-group">
						<label class="control-label col-sm-3">Build Name*:</label>
						<div class="col-sm-7">
							<input type="text" placeholder="Build Name" class="form-control"
								name="buildName" id="buildName">
						</div>

					</div>
					</form>
					<br> <br>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Add Test Cases For Execution</h5>
				</div>

				<div class="ibox-content">
				
				 <div class="alert alert-danger" id="errorDiv"
						style="display: none;">Build name already exists, Please try
						another.</div>

					<div class="table-responsive">
						<table
							class="table table-striped table-bordered table-hover dataTables-example"
							id="autoexe_table" name="">
							<thead>
								<tr>
									<td style="display: none">Module Id</td>
									<td class="check"><input id="autoExeCheck" type="checkbox"></td>
									<td><b>Test Case</b></td>
									<td><b>Description</b></td>

								</tr>
							</thead>
							<tbody>
								<c:forEach var="testRow" items="${Model.autoTest}">

									<tr id="">
										<td style="display: none">${testRow.id}</td>
										<td class="check"><input class="clsCheckbox" name=""
											type="checkbox"></td>
										<td>${testRow.test_description}</td>
										<td>${testRow.test_script}</td>

									</tr>

								</c:forEach>

								

							</tbody>
						</table>
					</div>


				</div>
			</div>

		</div>
	</div>
	<div class="row">

		<div class="col-lg-6">
			<div class="ibox ">

				<div class="ibox-content">
					

					<div class="form-group">
						<label class="control-label col-sm-3">Select Browser:</label>
						<div class="col-sm-7">
							<select id="exeBrowser" name="stepStatus"
								class="form-control m-b" style="margin-bottom: 0px;">

<!-- 								<option value="FIREFOX">Firefox</option> -->
								<option value="CHROME">Chrome</option>
<!-- 								<option value="IE">IE</option> -->

							</select>
						</div>
					</div>
					<br> <br>

					<div class="row">
						<div class="col-sm-8">
							<button class="btn btn-white pull-left"
								style="margin-right: 15px;" button"="" id="createProCancelBtn">Cancel</button>
							<button class="btn btn-info" id="executeCode" type="button">Execute</button>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>

</div>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/chosen/chosen.jquery.js"></script>
<script>
$(window).load(function() {
	$("#barInMenu").fadeOut();
});
$(document).ready(
	    function() {

	        var autoExetable = $("#autoexe_table").dataTable();

	        $('#executeCode').click(
	            function() {
	            	
	            	$("#barInMenu").fadeIn();
	    			$("#wrapper").addClass("hidden");

	                var jsonArray = [];

	                $('input:checked', autoExetable.fnGetNodes()).each(
	                    function() {

	                        var jsonObject = {
	                            'testDescription': $(this)
	                                .closest('tr').find(
	                                    'td:eq(2)').text(),
	                            'testScript': $(this)
	                                .closest('tr').find(
	                                    'td:eq(3)').text()
	                        };
	                        jsonArray.push(jsonObject);

	                    });

	                var jsonObject = {
	                    'build_name': $('#buildName').val(),
	                    'browser': $('#exeBrowser').val()
	                };

	                $.ajax({
	                    type: "POST",
	                    async: true,
	                    url: "insertautobuild",
	                    data: {
	                        buildName: $("#buildName").val()
	                    },
	                    success: function(data) {
	                    	
	                    	  if (data == 'SERVER NOT RUNNING')
	                    		  {
		                    		  swal("Alert!", "Server VM Is Not Running At This Moment, Please Contact Admin.", "error");
		                    		  $('.confirm').click(function() {
			                                location.reload(true);
			                            });
	          	    			
	                    		  }
	                    	  else if (data == 'CLIENT VM NOT RUNNING')
	                    		  {
		                    		  swal("Alert!", "Client VM Is Not Running At This Moment, Please Contact Admin.", "error");
		                    		  $('.confirm').click(function() {
			                                location.reload(true);
			                            });
	                    		  }
	                    	  else if (data == 'OK') {
	                    		  
	                            $(this).prop('disabled', true);
	                            
	                            swal("Added!", "Code Has Been Submitted For Execution.", "success");
	                            
	                            $('.confirm').click(function() {
	                                location.reload(true);
	                            });
									
	                           
	                            $.ajax({
	                                dataType: "json",
	                                type: "GET",
	                                crossDomain: true,
	                                async: true,
	                                contentType: 'application/json',
	                                url: "automationexecution?autoExejson=" + JSON.stringify(jsonArray) + "&browserJson=" + JSON.stringify(jsonObject),
	                                success: function(data) {
	                                   
	                                    if (data == 'FAILED')
	                                    	{
	                                    	 swal("Alert!", "Internal Server Error.Execution Process Interrupted.", "warning");
	                                    	 $('.confirm').click(function() {
	         	                                location.reload(true);
	         	                            });
	                                    	}
	                                }
	                            });
	                            

	                        } else {
	                        	$("#barInMenu").fadeOut();
	        	    			$("#wrapper").removeClass("hidden");
	                            $("#errorDiv").fadeIn();
	                        }
	                    }

	                });



	            });

	        $('#autoExeCheck').click(
	            function() {

	                if ($(this).is(':checked')) {
	                    $(".clsCheckbox", autoExetable.fnGetNodes())
	                        .each(function() {
	                            $(this).prop("checked", true);
	                        });
	                } else {
	                    $(".clsCheckbox", autoExetable.fnGetNodes())
	                        .each(function() {
	                            $(this).prop("checked", false);
	                        })
	                }
	            });
	    });
	    
</script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Jenkins</h2>
		<ol class="breadcrumb">
			<li><a href="bmdashboard">Dashboard</a></li>
			<li class="active"><strong>View Servers</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Jenkins Servers</h5>
					<div class="ibox-tools">
					<c:if test="${model.isAdminUser == 'true'}">
						<button type="button" class="btn btn-success btn-xs"
							onclick="location.href = 'jenkinsinstances';">Add New
							Instance</button>
					</c:if>
					</div>
				</div>


				<div class="ibox-content">
					<table class="table table-hover" id="viewModulesTable">
						<thead>
							<tr>
								<th>Server Name</th>
								<th>Server URL</th>
								<!-- <th>API Token</th>
								<th>Username</th> -->
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="row" items="${model.allRows}">
								<tr class="row_${row.jenkin_name} addHidden">
									<td class="xlTextAlignment"
										data-original-title="${row.jenkin_name}" data-container="body"
										data-toggle="tooltip" data-placement="right"
										class="issue-info">${row.jenkin_name}</td>

									<td class="xlTextAlignment"
										data-original-title="${row.jenkin_url}" data-container="body"
										data-toggle="tooltip" data-placement="right"
										class="issue-info">${row.jenkin_url}</td>


									<td><button type="button"
											onclick="setinstaceId(${row.jenkin_id})"
											class="btn btn-info btn-xs">View Jobs</button> 
											<c:if
											test="${row.update_status == '1'}">
											<button type="button" class="btn btn-primary btn-xs"
												onclick="editServer(${row.jenkin_id})">Edit</button>

										</c:if></td>

								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page-Level Scripts -->

<input type="hidden" id="hiddenInput"
	value='<%=session.getAttribute("moduleCreateStatus")%>'>

<input type="hidden" id="newModuleName"
	value='<%=session.getAttribute("newModuleName")%>'>

<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
	
function setinstaceId(id){
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
		var posting = $.post('setinstanceid',{
			instanceId : id
		});
		posting.done(function(data){
			window.location.href="jenkinsreports";
		});
	}
	
function editServer(id){
	$('body').addClass("white-bg");
	$("#barInMenu").removeClass("hidden");
	$("#wrapper").addClass("hidden");	
	var posting = $.post('setinstanceid',{
		instanceId : id
	});
	posting.done(function(data){
		window.location.href="jenkinserversetting";
	});
}
</script>
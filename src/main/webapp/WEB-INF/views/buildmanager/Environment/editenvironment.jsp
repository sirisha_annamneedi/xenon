<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Create New Environment</h2>
			<ol class="breadcrumb">
				<li><a href="viewbuild">Test Set</a></li>
				<li><a href="viewenvironment">Environment</a></li>
				<li class="active"><strong>Update Environment</strong></li>
			</ol>
		</div>
	</div>

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content">
						<form action="updateenvdetails" id="updateEnvForm"
							class="wizard-big wizard clearfix form-horizontal" method="POST">
							<div class="content clearfix">
								<fieldset class="body current">
									<div class="row">
										<label class="col-lg-4 pull-right text-right">* fields
											are mandatory</label>
									</div>
									<div class="row">
										<div class="col-lg-10">


											<div class="form-group">
												<label class="control-label col-sm-2">Name:* </label>
												<div class="col-sm-10">
													<input type="text" placeholder="Environment Name"
														class="form-control" name="envName" tabindex="1" value="${envDetails[0].env_name}">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-2">Configuration
													Parameter:* </label>
												<div class="col-sm-10">
													<input type="text"
														placeholder="Configuration Parameter Name"
														class="form-control " name="configParameterName" value="${envDetails[0].config_parameter}"
														tabindex="2">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label"> Description:</label>
												<div class="col-sm-10">
													<textarea rows="5" class="form-control  summernote" 
														name="envDescription" tabindex="3"> ${envDetails[0].description}</textarea>
												</div>
											</div>
											<div class="form-group hidden">
												<div class="col-sm-10">
													<input type="text" placeholder="EnvironmentId "
														class="form-control" name="envId" tabindex="1" value="${envDetails[0].env_id}">
												</div>
											</div>
											<div class="form-group">
											<label class="col-sm-2 control-label">Status: </label>
											<div class="col-sm-10">
												<div class="radio radio-success radio-inline col-sm-2">
													<input type="radio" id="inlineRadio1" value="1"
														name="envStatus" tabindex="2"
														${envDetails[0].status == '1' ? 'checked="true"' : ""}>
													<label for="inlineRadio1"> Active </label>
												</div>
												<div class="radio radio-danger radio-inline col-sm-2">
													<input type="radio" id="inlineRadio2" value="2"
														name="envStatus"
														${envDetails[0].status == '2' ? 'checked="true"' : ""}>
													<label for="inlineRadio2"> Inactive </label>
												</div>
											</div>
										</div>

										</div>
									</div>
									<div class="hr-line-solid"></div>
								</fieldset>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-4">
										<button class="btn btn-white pull-left"
											style="margin-right: 15px;" type="button"
											id="cancelBtn" tabindex="7">Cancel</button>
										<button class="pull-left btn btn-success"
											data-style="slide-up" type="button" tabindex="8"
											id="submitBtn">Submit</button>
									</div>
								</div>

							</div>
						</form>
						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<!-- end row -->
	</div>
	<!-- end wrapper -->
</div>
<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});
</script>


<script>
	$(document).ready(function() {

		$('.summernote').summernote();
		$('.note-editor').css('background', 'white');
		$('button[ data-event="codeview"]').remove();

		$('div.note-insert').remove();
		$('div.note-table').remove();
		$('div.note-help').remove();
		$('div.note-style').remove();
		$('div.note-color').remove();
		$('button[ data-event="removeFormat"]').remove();
		$('button[ data-event="insertUnorderedList"]').remove();
		$('button[ data-event="fullscreen"]').remove();
		$('button[ data-original-title="Line Height"]').remove();
		$('button[ data-original-title="Font Family"]').remove();
		$('button[ data-original-title="Paragraph"]').remove();

		$("#updateEnvForm").validate({
			rules : {
				envName : {
					required : true,
					minlength : 1,
					maxlength : 40
				},

				envDescription : {
					minlength : 1,
					maxlength : 200
				}
			}
		});

	});

	$('#cancelBtn').click(function() {
		window.location.href = "viewenvironment";
	});

	$("#submitBtn").click(function() {
		if ($("#updateEnvForm").valid()) {
			$("#updateEnvForm").submit();
		}
	});
</script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Add Environment</h2>
			<ol class="breadcrumb">
				<li><a href="viewbuild">Test Set</a></li>
				<li class="active"><strong>Environment</strong></li>
			</ol>
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>Environment Details</h5>
						<div class="ibox-tools">
							<button type="button" class="btn btn-success btn-xs"
								onclick="location.href = 'addenvironment';">Add
								Environment</button>
						</div>
					</div>
					<div class="ibox-content">
						<table class="table table-stripped" id="table">
							<thead>
								<tr>
									<th>Status</th>
									<th>Environment Name</th>
									<th>Configuration Parameter</th>
									<th>Description</th>
									<th>Settings</th>

								</tr>
							</thead>
							<tbody>
								<c:forEach var="data" items="${model.envDetails}">
									<tr>
										<td><span class="label EnvStatus">${data.envStatus}</span></td>
										<td style="text-transform: capitalize;">${data.env_name}</td>
										<td style="text-transform: capitalize;">${data.config_parameter}</td>
										<td style="text-transform: capitalize;">${data.description}</td>
										<td><button class="btn btn-white btn-xs"
												onclick="setEnvId(${data.env_id});">Settings</button>
											<button class="btn btn-success btn-xs"
												onclick="editEnv(${data.env_id});">Edit</button>
												<button class="btn btn-danger btn-xs"
												onclick="deleteEnv(${data.env_id});">Delete</button>
								    </td>
									</tr>
								</c:forEach>
							</tbody>

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");

		$(".chosen-select").chosen().trigger('chosen:updated');

	});

function setEnvId(id){
		
		var posting = $.post('setenvironmentid',{
			envId : id
		});
		posting.done(function(data){
			window.location.href="envsettings";
		});
	}
	
function editEnv(id){
	
	var posting = $.post('setenvironmentid',{
		envId : id
	});
	posting.done(function(data){
		window.location.href="editenvironment";
	});
}

function deleteEnv(id){
	var posting = $.post('deleteenvironment',{
		envId : id
	});
	posting.done(function(data){
		if(data=201)
		window.location.href="viewenvironment";
		else
		window.location.href="500";
		
	});
}

$(document).ready(function() {



	$('.EnvStatus').each(function(){
		var status = $(this).text();
		if(status.toLowerCase() == 'active')
			$(this).addClass('label-primary');
		else
			$(this).addClass('label-danger');
	});

});
	
</script>
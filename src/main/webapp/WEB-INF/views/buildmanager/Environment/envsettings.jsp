<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Create New Environment</h2>
			<ol class="breadcrumb">
				<li><a href="viewbuild">Test Set</a></li>
				<li><a href="viewenvironment">Environment</a></li>
				<li class="active"><strong>Environment Settings</strong></li>
			</ol>
		</div>
	</div>

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-10">
				<div class="ibox">
					<div class="ibox-content">
						<form action="insertenvvariable" id="varibleForm"
							enctype="multipart/form-data" class=" clearfix form-horizontal"
							method="POST">
							<div class="content clearfix">
								<fieldset class="body current">

									<div class="row">
										<div class="col-lg-10">
											<table class="table" id="variableTable">
												<thead>
													<th>Variable Name</th>
													<th>Parameter Name</th>
													<th>Parameter Value</th>
													
													<th></th>

												</thead>
												<tbody>
													<c:forEach var="data" items="${Model.variableList}">
														<tr class="data-wrapper tableTr">
															<td class=" column1" style="text-transform: capitalize;">${data.parameter_name}</td>
															<td class=" column3" style="text-transform: capitalize;">${data.parameter_val}</td>
															<td class=" column2" style="text-transform: capitalize;">${data.config_val}</td>
															
															<td class=" column4 hidden"
																style="text-transform: capitalize;">${data.variable_id}</td>
															<td class="button-group">
																<button class=" btn-success btn btn-xs editVarible"
																	type="button">Edit</button>
																<button class=" btn-danger btn btn-xs " type="button"
																	onClick="deleteRow(${data.variable_id});">Delete</button>

															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>

										</div>
									</div>

								</fieldset>
							</div>
						</form>
						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<!-- end row -->
		<div class="row">
			<div class="col-lg-10">
				<div class="ibox">
					<div class="ibox-content">
						<form action="insertenvvariable" id="envForm"
							enctype="multipart/form-data" class=" clearfix form-horizontal"
							method="POST">
							<div class="content clearfix">
								<fieldset class="body current">

									<div class="row">
										<div class="col-lg-9">
											<a href="javascript:void(0);"
												class=" add-btn btn btn-success btn-xs addRow pull-right"
												onclick="addRow(this)">Add Variable</a>
											<table class="table" id="envTable">
												<thead>
													<th>Variable Name</th>
													<th>Parameter Name</th>
													<th>Parameter Value</th>
													
													<th></th>

												</thead>
												<tbody>

													<tr class="data-wrapper tableTr">
														<td class="column column1"
															style="text-transform: capitalize;"><input
															class="form-control inputtext" type="text"> <label
															class="fieldError text-danger pull-bottom hidden">This
																field is required.</label></td>
														<td class="column column3"
															style="text-transform: capitalize;"><input
															class="form-control inputtext" type="text"> <label
															class="fieldError text-danger pull-bottom hidden">This
															field is required.</label></td>
														<td class="column column2"
															style="text-transform: capitalize;"><input
															class="form-control inputtext" type="text"> <label
															class="fieldError text-danger pull-bottom hidden">This
																field is required.</label></td>
														
														<td class="button-group"><a
															class="glyphicon glyphicon-remove btn-danger btn-xs"
															type="button" onClick="removeRow(this);"
															style="height: 23.5px;"></a> <!--  button class="glyphicon glyphicon-minus btn-success btn-sm"type="button" onClick="removeRow(this);"></button-->
														</td>
													</tr>

												</tbody>
											</table>

										</div>
									</div>

								</fieldset>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-4">
										<button class="btn btn-white pull-left"
											style="margin-right: 15px;" type="button"
											id="createEnvCancelBtn" tabindex="7">Cancel</button>
										<button class="pull-left btn btn-success"
											data-style="slide-up" type="button" tabindex="8"
											id="submitBtn">Submit</button>
									</div>
								</div>

							</div>
							<input type="text" id="jsonArray" name="jsonArray" class="hidden">
						</form>
						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<!-- end wrapper -->
	</div>

	<div class="modal inmodal" id="updateVariableModal" tabindex="-1"
		role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-lg">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<h6 class="modal-title pull-left">Update Variable</h6>
					<button type="button" class="close pull-right" data-dismiss="modal">
						<span aria-hidden="false">�</span><span class="sr-only">Close</span>
					</button>

				</div>

				<div class="modal-body">
					<form action="updateenvvariable"
						class="wizard-big wizard clearfix form-horizontal"
						enctype="multipart/form-data" method="POST" id="updateForm">
						<div class="row">
							<div class="col-lg-9">
								<div class="form-group">
									<label class="control-label col-sm-2">Variable Name:* </label>
									<div class="col-sm-10">
										<input type="text" class="form-control"
											name="updateVariableName" id="updateVariableName">
									</div>
									<label class='error hidden' style="margin-left: 20%;">This
										field is required</label>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2">Configuration
										Value:* </label>
									<div class="col-sm-10">
										<input type="text" class="form-control"
											name="updateConfigValue" id="updateConfigValue">
									</div>
									<label class='error hidden' style="margin-left: 20%;">This
										field is required</label>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2">Variable Value:*
									</label>
									<div class="col-sm-10">
										<input type="text" class="form-control"
											name="updateVariableValue" id="updateVariableValue">
									</div>
									<label class='error hidden' style="margin-left: 20%;">This
										field is required</label>
								</div>
								<div class="form-group hidden">
									<label class="control-label col-sm-2">Variable Value:*
									</label>
									<div class="col-sm-10">
										<input type="text" class="form-control"
											name="updateVariableId" id="updateVariableId">
									</div>

								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white pull-left"
						data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success pull-left"
						id="updateBtn">Submit</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
var clone = $("#envTable tr.data-wrapper:first").clone(true);
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
});

$(function(){
	
	var table = $('#variableTable').DataTable({
		dom : '<"html5buttons"B>lTfgitp',
		buttons : [],
		"paging" : false,
		"lengthChange" : false,
		"searching" : false,
		"ordering" : false
	});
});
	$('#createEnvCancelBtn').click(function() {
		window.location.href="viewenvironment";
	});
	
	
	function addRow(btnClass){
		var ParentRow = $("#envTable tr.data-wrapper").last();
		clone.find('.column').find('.inputtext').val('');
		clone.clone(true).insertAfter(ParentRow);
		
	}
	
	function removeRow(selectedRow) {
		var rowCount = $('#envTable tbody  tr').length;
		rowIndex = $(selectedRow).parent('.button-group').parent('.tableTr')
				.index();
		
		if (rowIndex != 0) {

			$(selectedRow).closest('tr').remove();
		}
	}
	 
	 
	 
	 
	 $("#submitBtn").click(function(){
		 
		 
		 var JsonArray='{ "array" : [';
		 var data='';
		 var status=true;
		 
		 
		 $('#envTable tbody tr.tableTr').each(
					function(row, tr) {
						$(tr).find('td.column1').find('.fieldError')
						.addClass("hidden");
						$(tr).find('td.column2').find('.fieldError')
						.addClass("hidden");
						$(tr).find('td.column3').find('.fieldError')
						.addClass("hidden");
						var textValue1 = $(tr).find('td.column1').find(
								'.inputtext').val();
						var textValue2 = $(tr).find('td.column2').find(
						'.inputtext').val();
						var textValue3 = $(tr).find('td.column3').find(
						'.inputtext').val();
						if (textValue1=='') {
							status = false;
							$(tr).find('td.column1').find('.fieldError')
									.removeClass("hidden");
						} 
						
						if (textValue2=='') {
							status = false;
							$(tr).find('td.column2').find('.fieldError')
									.removeClass("hidden");
						} 
						
						if (textValue3=='') {
							status = false;
							$(tr).find('td.column3').find('.fieldError')
									.removeClass("hidden");
						} 
						
					});
		 
		if(status==true){
		 $('#envTable tbody tr.data-wrapper').each(
					function(row, tr) {
						var column1 = $(tr).find('td.column1').find('.inputtext').val();
						
						var column2 = $(tr).find('td.column2').find('.inputtext').val();
						var column3 = $(tr).find('td.column3').find('.inputtext').val();
						data='{"variableName":"'+column1+'","configValue":"'+column2+'","variableValue":"'+column3+'"},'
						JsonArray+=data;
						 data='';
					});
		 
		 JsonArray+=']}';
		 
		
		 var variableData = (JsonArray.substr(0,JsonArray.length - 3))+ (JsonArray.substr(JsonArray.length - 2,JsonArray.length - 1));
		 $("#jsonArray").val(variableData);
		 if ($("#envForm").valid()) {

				$('body').addClass("white-bg");
				$("#barInMenu").removeClass("hidden");
				$("#wrapper").addClass("hidden");
		
				$("#envForm").submit();
			}
		 }
	});
	 
	 function deleteRow(variableId){
		 $.ajax({
				url: "deletevariable",
				data: {
					variableId:variableId
				},
				type: "POST",
			})
			.done(function(data){
				if(data == "500"){
					location.href  = "500";
				}else if(data == "201"){
					location.reload();
				}else{
					location.href  = "404";
				}
			});
	 }
	$(".editVarible").click(function (){
		 var variableName=$(this).closest( "tr").children('td:eq(0)').html();
		 var configValue=$(this).closest( "tr").children('td:eq(1)').html();
		 var variableValue=$(this).closest( "tr").children('td:eq(2)').html();
		 var variableId=$(this).closest( "tr").children('td:eq(3)').html();
		 $("#updateVariableModal").modal('show');
		 $("#updateVariableName").val(variableName);
		 $("#updateConfigValue").val(configValue);
		 $("#updateVariableValue").val(variableValue);
		 $("#updateVariableId").val(variableId);
		 
	  })
	
	  
	  $("#updateBtn").click(function(){
		  
		  $("#updateForm").validate({
			  rules:{
				  updateVariableName:{required:true },
				  updateConfigValue:{required:true },
				  updateVariableValue:{required:true }
			  }
				 
		  });
		  
		  
		  if($("#updateForm").valid()){
			  $('body').addClass("white-bg");
			  $("#barInMenu").removeClass("hidden");
			  $("#wrapper").addClass("hidden");
			  $("#updateForm").submit();
		  }
	  })
</script>
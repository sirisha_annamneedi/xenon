<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
		<h2>Release Wise Bugs</h2>
		<ol class="breadcrumb">
			<li><a href="btdashboard">Dashboard</a></li>
			<li class="active"><strong>Release Wise Bugs</strong></li>
		</ol>
		<!-- end breadcrumb -->
	</div>

	<!-- end col-lg-10 -->
</div>
<!-- end wrapper -->

<div class="wrapper wrapper-content  animated fadeInRight">

	<div class="row">
		<div class="col-lg-12">
			
					<div>
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#critical" aria-controls="critical"
								role="tab" data-toggle="tab">Critical Bugs</a></li>
							<li role="presentation"><a href="#blocker"
								aria-controls="blocker" role="tab" data-toggle="tab">Blocker Bugs</a>
							</li>
							<li role="presentation"><a href="#minor"
								aria-controls="minor" role="tab" data-toggle="tab">Minor Bugs</a>
							</li>
							<li role="presentation"><a href="#medium"
								aria-controls="medium" role="tab" data-toggle="tab">Medium Bugs</a>
							</li>
							<li role="presentation"><a href="#major"
								aria-controls="major" role="tab" data-toggle="tab">Major Bugs</a>
							</li>
							
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="critical">
							<div class="ibox">
								<div class="ibox-content">
									<div class="table-responsive">
										<table
											class="table table-hover issue-tracker dataTables-example"
											id="viewBugTable">
											<thead>
												<h2>
													<b>Critical Bugs</b>
												</h2>
												<tr>
													<th class="hidden">Bug ID</th>
													<th>Bug ID</th>
													<th>Status</th>
													<th style="width: 150px">Title</th>
													<th>Priority</th>
													<th>Module</th>
													<th>Reported By</th>
													<th>Assigned To</th>
													<th>Created Date</th>
													<th>Updated Date</th>

												</tr>
											</thead>
											<tbody>
												<c:forEach var="data" items="${Model.allBugDetailsPriority}">
													<c:if
														test="${Model.filterText == data.issue_tracker_release_id}">

														<c:if test="${data.bug_priority == 'Critical'}">
														<tr style="cursor: pointer;">
															<td class="hidden">${data.bug_id}</td>
															<td>${data.bug_prefix}</td>
															<td><span class="label td_bugStatus"
																style="color: #FFFFFF">${data.bugStatus}</span></td>
															<td class="lgTextAlignment"
																data-original-title="${data.bug_title}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.bug_title}</td>
															<td>${data.bug_priority}</td>
															<td class="textAlignment"
																data-original-title="${data.module_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.module_name}</td>
															<td class="textAlignment"
																data-original-title="${data.reporterFN} ${data.reporterLN}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.reporterFN}
																${data.reporterLN}</td>
															<td class="textAlignment"
																data-original-title="${data.assigneeFN} ${data.assigneeLN}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.assigneeFN}
																${data.assigneeLN}</td>
															<td class="textAlignment"
																data-original-title="${data.create_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.create_date}</td>
															<td class="textAlignment"
																data-original-title="${data.update_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.update_date}</td>

														</tr>

													</c:if>

													 </c:if>
												</c:forEach>

											</tbody>
										</table>
									</div>
								</div>
								</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="blocker">
								<div class="ibox">
								<div class="ibox-content">
									<div class="table-responsive">
										<table
											class="table table-hover issue-tracker dataTables-example"
											id="viewBugTable">
											<thead>
												<h2>
													<b>Blocker Bugs</b>
												</h2>
												<tr>
													<th class="hidden">Bug ID</th>
													<th>Bug ID</th>
													<th>Status</th>
													<th style="width: 150px">Title</th>
													<th>Priority</th>
													<th>Module</th>
													<th>Reported By</th>
													<th>Assigned To</th>
													<th>Created Date</th>
													<th>Updated Date</th>

												</tr>
											</thead>
											<tbody>
												<c:forEach var="data" items="${Model.allBugDetailsPriority}">
													<c:if
														test="${Model.filterText == data.issue_tracker_release_id}">

														<c:if test="${data.bug_priority == 'Blocker'}">
														<tr style="cursor: pointer;">
															<td class="hidden">${data.bug_id}</td>
															<td>${data.bug_prefix}</td>
															<td><span class="label td_bugStatus"
																style="color: #FFFFFF">${data.bugStatus}</span></td>
															<td class="lgTextAlignment"
																data-original-title="${data.bug_title}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.bug_title}</td>
															<td>${data.bug_priority}</td>
															<td class="textAlignment"
																data-original-title="${data.module_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.module_name}</td>
															<td class="textAlignment"
																data-original-title="${data.reporterFN} ${data.reporterLN}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.reporterFN}
																${data.reporterLN}</td>
															<td class="textAlignment"
																data-original-title="${data.assigneeFN} ${data.assigneeLN}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.assigneeFN}
																${data.assigneeLN}</td>
															<td class="textAlignment"
																data-original-title="${data.create_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.create_date}</td>
															<td class="textAlignment"
																data-original-title="${data.update_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.update_date}</td>

														</tr>

													</c:if>

													</c:if>
												</c:forEach>

											</tbody>
										</table>
									</div>
								</div>
								</div>
								</div>

							<div role="tabpanel" class="tab-pane" id="minor">
							<div class="ibox">
								<div class="ibox-content">
									<div class="table-responsive">
										<table
											class="table table-hover issue-tracker dataTables-example"
											id="viewBugTable">
											<thead>
												<h2>
													<b>Minor Bugs</b>
												</h2>
												<tr>
													<th class="hidden">Bug ID</th>
													<th>Bug ID</th>
													<th>Status</th>
													<th style="width: 150px">Title</th>
													<th>Priority</th>
													<th>Module</th>
													<th>Reported By</th>
													<th>Assigned To</th>
													<th>Created Date</th>
													<th>Updated Date</th>

												</tr>
											</thead>
											<tbody>
												<c:forEach var="data" items="${Model.allBugDetailsPriority}">
													<c:if
														test="${Model.filterText == data.issue_tracker_release_id}">

														<c:if test="${data.bug_priority == 'Minor'}">
														<tr style="cursor: pointer;">
															<td class="hidden">${data.bug_id}</td>
															<td>${data.bug_prefix}</td>
															<td><span class="label td_bugStatus"
																style="color: #FFFFFF">${data.bugStatus}</span></td>
															<td class="lgTextAlignment"
																data-original-title="${data.bug_title}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.bug_title}</td>
															<td>${data.bug_priority}</td>
															<td class="textAlignment"
																data-original-title="${data.module_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.module_name}</td>
															<td class="textAlignment"
																data-original-title="${data.reporterFN} ${data.reporterLN}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.reporterFN}
																${data.reporterLN}</td>
															<td class="textAlignment"
																data-original-title="${data.assigneeFN} ${data.assigneeLN}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.assigneeFN}
																${data.assigneeLN}</td>
															<td class="textAlignment"
																data-original-title="${data.create_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.create_date}</td>
															<td class="textAlignment"
																data-original-title="${data.update_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.update_date}</td>

														</tr>

													</c:if>

													</c:if>
												</c:forEach>

											</tbody>
										</table>
									</div>
								</div>
								</div>
								</div>
								
								<div role="tabpanel" class="tab-pane" id="medium">
								<div class="ibox">
								<div class="ibox-content">
									<div class="table-responsive">
										<table
											class="table table-hover issue-tracker dataTables-example"
											id="viewBugTable">
											<thead>
												<h2>
													<b>Medium Bugs</b>
												</h2>
												<tr>
													<th class="hidden">Bug ID</th>
													<th>Bug ID</th>
													<th>Status</th>
													<th style="width: 150px">Title</th>
													<th>Priority</th>
													<th>Module</th>
													<th>Reported By</th>
													<th>Assigned To</th>
													<th>Created Date</th>
													<th>Updated Date</th>

												</tr>
											</thead>
											<tbody>
												<c:forEach var="data" items="${Model.allBugDetailsPriority}">
													<c:if
														test="${Model.filterText == data.issue_tracker_release_id}">

														<c:if test="${data.bug_priority == 'Medium'}">
														<tr style="cursor: pointer;">
															<td class="hidden">${data.bug_id}</td>
															<td>${data.bug_prefix}</td>
															<td><span class="label td_bugStatus"
																style="color: #FFFFFF">${data.bugStatus}</span></td>
															<td class="lgTextAlignment"
																data-original-title="${data.bug_title}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.bug_title}</td>
															<td>${data.bug_priority}</td>
															<td class="textAlignment"
																data-original-title="${data.module_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.module_name}</td>
															<td class="textAlignment"
																data-original-title="${data.reporterFN} ${data.reporterLN}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.reporterFN}
																${data.reporterLN}</td>
															<td class="textAlignment"
																data-original-title="${data.assigneeFN} ${data.assigneeLN}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.assigneeFN}
																${data.assigneeLN}</td>
															<td class="textAlignment"
																data-original-title="${data.create_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.create_date}</td>
															<td class="textAlignment"
																data-original-title="${data.update_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.update_date}</td>

														</tr>

													</c:if>

													</c:if> 
												</c:forEach>

											</tbody>
										</table>
									</div>
								</div>
								</div>
								</div>
								
								<div role="tabpanel" class="tab-pane" id="major">
								<div class="ibox">
								<div class="ibox-content">
									<div class="table-responsive">
										<table
											class="table table-hover issue-tracker dataTables-example"
											id="viewBugTable">
											<thead>
												<h2>
													<b>Major Bugs</b>
												</h2>
												<tr>
													<th class="hidden">Bug ID</th>
													<th>Bug ID</th>
													<th>Status</th>
													<th style="width: 150px">Title</th>
													<th>Priority</th>
													<th>Module</th>
													<th>Reported By</th>
													<th>Assigned To</th>
													<th>Created Date</th>
													<th>Updated Date</th>

												</tr>
											</thead>
											<tbody>
												<c:forEach var="data" items="${Model.allBugDetailsPriority}">
													<c:if
														test="${Model.filterText == data.issue_tracker_release_id}">

														<c:if test="${data.bug_priority == 'Major'}">
														<tr style="cursor: pointer;">
															<td class="hidden">${data.bug_id}</td>
															<td>${data.bug_prefix}</td>
															<td><span class="label td_bugStatus"
																style="color: #FFFFFF">${data.bugStatus}</span></td>
															<td class="lgTextAlignment"
																data-original-title="${data.bug_title}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.bug_title}</td>
															<td>${data.bug_priority}</td>
															<td class="textAlignment"
																data-original-title="${data.module_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.module_name}</td>
															<td class="textAlignment"
																data-original-title="${data.reporterFN} ${data.reporterLN}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.reporterFN}
																${data.reporterLN}</td>
															<td class="textAlignment"
																data-original-title="${data.assigneeFN} ${data.assigneeLN}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.assigneeFN}
																${data.assigneeLN}</td>
															<td class="textAlignment"
																data-original-title="${data.create_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.create_date}</td>
															<td class="textAlignment"
																data-original-title="${data.update_date}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.update_date}</td>

														</tr>

													</c:if>

													</c:if>
												</c:forEach>

											</tbody>
										</table>
									</div>
								</div>
								</div>
								</div>
							
						</div>
					</div>
					
				</div>

			</div>
		</div>
</div>


<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	
});

	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});

	$(document).ready(
			function() {
				 $('[data-toggle="tooltip"]').tooltip();   
				var today = moment().format('MMMM Do YYYY, h:mm:ss a');
				var table = $('.dataTables-example').DataTable(
						{
							dom : '<"html5buttons"B>lTfgitp',
							buttons : [
									{
										extend : 'csv',
										title : 'Release wise Bugs - ' + today,
										exportOptions: {
						                    columns: ':visible'
						                }
									},
									{
										extend : 'excel',
										title : 'Release wise Bugs - ' + today,
										exportOptions: {
						                    columns: ':visible'
						                }
									},
									{
										extend : 'pdf',
										title : 'Release wise Bugs - ' + today,
										exportOptions: {
						                    columns: ':visible'
						                }
									},

									{
										extend : 'print',
										exportOptions: {
						                    columns: ':visible'
						                },
										customize : function(win) {
											$(win.document.body).addClass(
													'white-bg');
											$(win.document.body).css(
													'font-size', '10px');

											$(win.document.body).find('table')
													.addClass('compact').css(
															'font-size',
															'inherit');
										},
										title : 'Release wise Bugs - ' + today
									} ],
							"aaSorting": [[0, "desc"]],
							"paging" : true,
							"lengthChange" : false,
							"searching" : true,
							"ordering" : false
						});
				
				// Apply the filter
			    $("#viewBugTable thead input").on( 'keyup change', function () {
			    	table
			            .columns( $(this).parent().index()+':visible' )
			            .search( this.value )
			            .draw();
			    	
			    	setSpanColor();
			    } );
				
				setSpanColor();
				$('#viewBugTable_paginate').on('click', function() {
					setSpanColor();
				});
				$('#viewBugTable_length select').on('change', function() {
					setSpanColor();
				});
				$('#viewBugTable_filter input').on('keyup change', function() {
					setSpanColor();
				});
				$('#viewBugTable thead tr').on('click', function() {
					setSpanColor();
				});

			});

	var setSpanColor = function() {
		$('.td_bugStatus').each(function() {
            var stat=$(this).text();
           
            if(stat=='New')
            {
            	$(this).css("background-color","${Model.New}");
            }
            else if(stat=='Assigned')
            {
            	$(this).css("background-color","${Model.Assigned}");
            }
            else if(stat=='Dev in Progress')
            {
            	$(this).css("background-color","${Model.Fixed}");
            }
            else if(stat=='Ready for QA')
            {
            	$(this).css("background-color","${Model.Verified}");
            }
            else if(stat=='Closed')
            {
            	$(this).css("background-color","rgb(0, 153, 0)");
            }
            else if(stat=='Reject')
            {
            	$(this).css("background-color","${Model.Reject}");
            }
           
           
      });
	}
	$("#viewBugTable tbody tr").click(function() {
		var bugId = $(this).children('td').first().html();
		$.post("setbugidforsummary", {
			bugID : bugId
		}, function(response) {
			if (response == true) {
				window.open('releasebugsummary', '_blank');
			} else {
				window.location.href = "500";
			}

		});
	});

	
</script>
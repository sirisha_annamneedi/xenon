<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Reports</h2>
			<ol class="breadcrumb">
				<li><a href="btdashboard">Dashboard</a></li>
				<li class="active"><strong>Reports</strong></li>
			</ol>
		</div>
	</div>
	<div class="wrapper wrapper-content ">
		<div class="row">
			<div class="col-lg-9">


				<div class="ibox">
					<div class="scroll_content" style="max-height: 300px;">
						<div class="ibox-content" style="min-height: 300px">

							<form id="tableForm" action="savecustomquery" method="post"
								enctype="multipart/form-data" class="">
								<div class="row form-group">
									<table class="table "
										style="padding: 5px; vertical-align: middle;" id="queryTable">
										<thead>
											<th style="width: 5%"></th>
											<th style="width: 10%"></th>
											<th style="width: 10%"></th>
											<th style="width: 30%"></th>
											<th style="width: 10%"></th>

										</thead>
										<tbody>

											<tr class="data-wrapper tableRow">

												<td class="childOp"><label class="operatorValue hidden"
													style="font-size: 11px;"></label></td>
												<td class="searchParameter"
													style="max-width: 20px; max-width: 20px">
													<div class="form-group">
														<select data-placeholder="choose a value"
															id="selectCondition_1" onchange="getSelectedvalue(this)"
															class="chosen-select selectCondition form-control"
															name="parameter">
															<option value="1" class="option">Release</option>
															<option value="2" class="option">Build</option>
															<option value="3" class="option">Status</option>
															<option value="4" class="option">Priority</option>
															<option value="5" class="option">Severity</option>

														</select> <label class="error hidden text-danger"
															style="position: fixed;">Please select unique
															parameter</label>
													</div>
												</td>
												<td class="Status " style="max-width: 20px; max-width: 20px">
													<div class="form-group">
														<select data-placeholder="choose a value"
															class="chosen-select  form-control selectCondition2"
															name="QueryOption" id="selectCondition2_1"
															onchange="getConditionValue(this);">
															<option value="is" class="other">IS</option>
															<option value="isnot" class="other">IS NOT</option>
															<option value="islessthan" class="date hidden">IS
																LESS THAN</option>
															<option value="isgreaterthan" class="date hidden">IS
																GREATER THAN</option>
															<option value="isequal" class="date hidden">IS
																EQUALS</option>
														</select>
													</div>
												</td>
												<td class="selectedResult ">
													<div class="form-group ">
														<div class="content1 ErrorStatus form-horizontal"
															id="div1_1">
															<div class="searchDiv input-group m-b">
																<input
																	class="releaseText form-control inputText selectedOption"
																	value="" style="" disabled> <span
																	class="input-group-addon success"><i
																	class="popup glyphicon glyphicon-search"
																	onclick="showPop(this)"></i></span>
															</div>
															<div>
																<label class="fieldError text-danger pull-bottom hidden">This
																	field is required.</label>
															</div>

														</div>

														<!-- <div class="content2 ErrorStatus hidden " id="div2_1">
														<input  class="text form-control" disabled/>
														
														<label class="fieldError text-danger hidden">This field is required.</label>
													</div> -->
														<div class="content2 ErrorStatus hidden " id="div2_1">
															<div class="col-lg-4 pull-left">
																<select data-placeholder="choose a value "
																	class="chosen-select buildType " name="buildType"
																	onchange=selectBuildType(this); id="dropdown2"
																	style="max-width: 159px">
																	<c:forEach var="buildExeType"
																		items="${Model.buildExeType}">
																		<option value="${buildExeType.execution_type_id}">${buildExeType.description}</option>
																	</c:forEach>
																</select>
															</div>
															<div class="searchDiv input-group m-b">

																<input class="buildText form-control inputText" value=""
																	style="" disabled> <span
																	class="input-group-addon success"><i
																	class="buildpopup glyphicon glyphicon-search"
																	onclick="buildPopUp(this)"></i></span>
															</div>
															<div>
																<label class="fieldError text-danger pull-bottom hidden">This
																	field is required.</label>
															</div>

														</div>
														<div class="content3 ErrorStatus hidden " id="div3_1">
															<select data-placeholder="choose a value"
																class="chosen-select requiredField " name="Status"
																id="dropdown4" style="" multiple>
																<c:forEach var="status" items="${Model.statusList}">
																	<option value="${status.status_id}">${status.bug_status}</option>
																</c:forEach>
															</select> <label class="fieldError text-danger  hidden">This
																field is required.</label>
														</div>
														<div class="content4 ErrorStatus hidden " id="div4_1">
															<select data-placeholder="choose a value"
																class="chosen-select requiredField " name="Priority"
																id="dropdown5" style="" multiple>
																<c:forEach var="priority" items="${Model.priorityList}">
																	<option value="${priority.priority_id}">${priority.bug_priority}</option>
																</c:forEach>
															</select> <label class="fieldError text-danger  hidden">This
																field is required.</label>
														</div>
														<div class="content5 ErrorStatus hidden " id="div5_1">
															<select data-placeholder="choose a value"
																class="chosen-select requiredField " name="Severity"
																id="dropdown5" style="" multiple>
																<c:forEach var="severity" items="${Model.severityList}">
																	<option value="${severity.severity_id}">${severity.bug_severity}</option>
																</c:forEach>
															</select> <label class="fieldError text-danger  hidden">This
																field is required.</label>
														</div>

														<div class="content6 requiredField ErrorStatus hidden"
															id="div6_1">
															<div class="input-group m-b DateGroup">
																<input type="text" name="DateInput"
																	class=" inputDate form-control inputText" style=""
																	onmouseover="showCalender()" /> <label
																	class="fieldError text-danger pull-bottom hidden">This
																	field is required.</label>
															</div>
															<div>
																<label class="fieldError text-danger pull-bottom hidden">This
																	field is required.</label>
															</div>
														</div>
													</div>

												</td>
												<!-- <td>
											<a href="#"  class="popup" onclick="showPop(this)"><i class="glyphicon glyphicon-search"></i> </a>
											</td> -->
												<td class="button-group" style="padding-top: 1%;"><a
													href="javascript:void(0);"
													class=" add-btn btn btn-success btn-xs addRow"
													onclick="addRow(this)">AND</a> <a
													href="javascript:void(0);"
													class=" add-btn btn btn-success  btn-xs addRow"
													onclick="addRow(this)">OR</a> <a
													class="glyphicon glyphicon-remove btn-danger btn-xs"
													type="button" onClick="removeRow(this);"
													style="height: 21px;"></a> <!--  button class="glyphicon glyphicon-minus btn-success btn-sm"type="button" onClick="removeRow(this);"></button-->
												</td>



											</tr>
										</tbody>
									</table>
									<input name="jsonArray" id="jsonArray"
										class="form-group hidden">
								</div>
								<div>
									<input class="hidden form-group" id="queryName"
										name="queryName">
								</div>
								<div>
									<input class="hidden form-group" id="queryId" name="queryId">
								</div>

							</form>
							<form id="executeForm" action="executeCustomQuery" method="post"
								enctype="multipart/form-data" class="">
								<input class="hidden form-group" id="executequery"
									name="executequery">
							</form>

							<button class="btn btn-success" id="submitButton"
								onclick="saveQuery()">Save</button>

							<button class="btn btn-success hidden" onclick="saveQuery()"
								id="createNewBtn" type="button" style="margin-right: 15px;">Create
								New</button>
							<button class="btn btn-success hidden" id="updateQueryBtn"
								type="button">Update</button>
							<button class="btn btn-success hidden" id="editBtn"
								name="editBtn" onclick="editQuery()" type="button"
								style="margin-right: 15px;">Edit</button>
							<button class="btn btn-success" id="executeButton"
								onclick="saveOrExecuteQuery()">Execute</button>
							<a class="btn btn-success" href="customizesearch"
								style="margin-left: 1.5%;">Reset</a>

						</div>
					</div>

				</div>

			</div>


			<div class="col-lg-3 ">
				<div class="ibox">
					<div class="full-height-scroll" style="max-height: 250px">
						<div class="ibox-title">
							<h5>Queries</h5>

						</div>
						<div class="ibox-content" style="min-height: 250px">

							<ul class="list-group clear-list">
								<c:forEach var="UserSavedQueries"
									items="${Model.UserSavedQueries}">
									<li class="list-group-item"
										id="viewQueryLi_${UserSavedQueries.idxebt}"><a
										onclick="displayQueryData('${UserSavedQueries.queryjson}','${UserSavedQueries.queryname}','${UserSavedQueries.idxebt}');">${UserSavedQueries.queryName}</a>
										<button class="btn btn-danger pull-right"
											style="padding-left: 2px; padding-bottom: 4px; padding-right: 6px; padding-top: 3px;">
											<i class="fa fa-trash pull-right"
												onclick="deleteQuery(${UserSavedQueries.idxebt})"
												style="margin-top: 2%; cursor: pointer;"></i>
										</button></li>


								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="releaseModal" class="modal">

			<!-- Modal content -->
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">

						<h3>Please select release</h3>
					</div>
					<div class="modal-body ">
						<div class="full-height-scroll">
							<div class="table-responsive " style="overflow-x: hidden">
								<table id="releaseTable"
									style="display: block; height: 250px; overflow-y: scroll;"
									class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th class="hidden"></th>
											<th>Check Release</th>
											<th>Release Name</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${Model.releaseList}" var="releases">
											<tr class="releaseTR">
												<td class="checkboxTD" style="width: 20%;"><input
													type="checkbox" class="checkbox-success releaseId"
													release="${releases.release_id}"></td>
												<td class="releaseTD hidden" style="width: 20%;"><input
													type="text" value="${releases.release_name}"
													class=" releaseName hidden" /></td>
												<td style="width: 20%;"><p class="releaseVal">${releases.release_name}</p></td>

											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="modal-footer">

						<button class="closePopup btn-success pull-bottom btn"
							style="margin-right: 10px">Cancel</button>
						<button class="windowClose btn-success pull-bottom btn">OK</button>
					</div>

				</div>
			</div>

		</div>

		<div id="buildModal" class="modal" style="width: fixed;">

			<!-- Modal content -->
			<div class="modal-dialog" style="width: fixed;">
				<div class="modal-content">
					<div class="modal-header">

						<div class="modal-body">
							<h3>Please select Build</h3>

							<div class="manualbuildTable hidden">
								<div class="table-responsive " style="overflow-x: hidden">
									<table id="manualbuildTable"
										class="table table-striped table-bordered table-hover"
										style="display: block; height: 250px; overflow-y: scroll;">
										<thead>
											<tr>
												<th>Check Build</th>
												<th>Build Name</th>
												<th>Release Name</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${Model.buildList}" var="manual">
												<tr class="manualBuildRow releaseId_${manual.release_id}">
													<td class="checkboxTD" style="width: 20%;">
														<div class="manual">
															<input type="checkbox"
																class="checkbox-success manualBuild"
																manual="${manual.build_id}">
														</div>

													</td>

													<td class="buildTD" style="width: 20%;">
														<p class="manualBuildVal" buildName="${manual.build_name}">${manual.build_name}</p>
													</td>
													<td class="" style="width: 20%;">
														<p class="release_Id">Release ${manual.release_id}</p>
													</td>

												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>

							<div class="autobuildTable hidden">
								<div class="table-responsive " style="overflow-x: hidden">
									<table id="autobuildTable"
										class="table table-striped table-bordered table-hover"
										style="display: block; height: 250px; overflow-y: scroll;">

										<thead>
											<tr>
												<th>Check Build</th>
												<th>Build Name</th>
												<th>Release Name</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${Model.autoBuildList}" var="autoBuild">

												<tr
													class="autoBuildRow  releaseId_${autoBuild.release_id} autoBuildRow1">
													<td class="checkboxTD" style="width: 20%;">
														<div class="autoBuild ">
															<input type="checkbox"
																class="checkbox-success autoBuildcheck"
																auto="${autoBuild.build_id}">
														</div>

													</td>

													<td class="buildTD"><p class="autoBuildVal "
															buildName="${autoBuild.build_name}" style="width: 20%;">${autoBuild.build_name}</p></td>
													<td class="" style="width: 20%;">
														<p class="release_Id">Release ${autoBuild.release_id}</p>
													</td>

												</tr>

											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button class="closePopup btn-success pull-bottom btn"
								style="margin-right: 10px">Cancel</button>
							<button class="closeBuildWindow btn-success pull-bottom btn">OK</button>

						</div>

					</div>
				</div>

			</div>
		</div>

		<div class="row">

			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Results</div>
					<div class="panel-body">
						<div class="tabs-container">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#tab-1"
									aria-expanded="true"> Bugs</a></li>
								<li class=""><a data-toggle="tab" href="#tab-2"
									aria-expanded="false">Pie Charts</a></li>
								<li class="hidden"><a data-toggle="tab" href="#tab-3"
									aria-expanded="false">Bar Chart</a></li>
							</ul>
							<div class="tab-content">
								<div id="tab-1" class="tab-pane active">
									<div class="panel-body">

										<div class="ibox">
											<div class="ibox-content" id="tableDataDiv">
												<div class="table-responsive">
													<table class="table table-hover " id="viewBugTable">
														<thead class="">
															<tr>
																<th class="">Bug ID</th>
																<th>Bug Status</th>
																<th>Bug Priority</th>
																<th>Severity</th>
																<th>Assigned To</th>
																<th>Category</th>
																<th>Severity</th>
																<th>Updated By</th>
																<th>Created Date</th>

															</tr>
														</thead>
														<tbody>
															<c:forEach var="data" items="${Model.getAllBugs}">
																<tr>

																	<td class="">${data.bug_prefix}</td>
																	<td style="text-transform: capitalize;">${data.bugStatus}</td>
																	<td style="text-transform: capitalize;">${data.bugPriority}</td>
																	<td style="text-transform: capitalize;">${data.bugSeverity}</td>
																	<td style="text-transform: capitalize;">${data.assigneeName}</td>
																	<td style="text-transform: capitalize;">${data.category_name}</td>
																	<td style="text-transform: capitalize;">${data.bugSeverity}</td>
																	<td style="text-transform: capitalize;">${data.UpdaterName}</td>
																	<td class="textAlignment"
																		data-original-title="${data.update_date}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.update_date}</td>


																</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</div>

										</div>

									</div>
									<!-- end tab -1 -->
								</div>
								<div id="tab-2" class="tab-pane">
									<div class="panel-body">
										<div class="ibox">
											<div class="">
												<div class="row">

													<div class="col-lg-6" id="pieChartDiv">
														<div class="panel panel-default">
															<div class="panel-heading" style="text-align: center;">Status
															</div>
															<div class="panel-body">
																<canvas class="" id="StatusPiechart"></canvas>
															</div>
														</div>
													</div>
													<div class="col-lg-6" id="">
														<div class="panel panel-default">
															<div class="panel-heading" style="text-align: center;">Priority
															</div>
															<div class="panel-body">
																<canvas class="" id="priorityPiechart"></canvas>
															</div>
														</div>

													</div>
												</div>

												<div class="row">
													<div class="col-lg-3"></div>
													<div class="col-lg-6" id="">
														<div class="panel panel-default center">
															<div class="panel-heading" style="text-align: center;">Severity
															</div>
															<div class="panel-body">
																<canvas class="" id="severityPiechart"></canvas>
															</div>
														</div>
													</div>


												</div>
											</div>
										</div>
									</div>
								</div>
								<div id="tab-3" class="tab-pane hidden">
									<div class="panel-body">

										<div class="ibox">
											<div class="">
												<div class="panel panel-default">
													<div class="panel-heading">Status Bar Chart</div>
													<div class="panel-body">
														<canvas id="StatusPieChart" height="100px"></canvas>
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>


						</div>
					</div>
				</div>
			</div>

		</div>

	</div>
</div>


<script type="text/javascript">
	var i = 1, rowCount = 1, BuildType = 1, releaseIDList, rowNumber, releaseStatus = "is", releaseValue = 2;
	;
	var popupWindow = null;
	var popupIsShown = false;
	var clone = $("table tr.data-wrapper:first").clone(true);
	$('tr.data-wrapper:first select').chosen();
	var modal = document.getElementById('releaseModal');
	var buildModal = document.getElementById('buildModal');
	var span = document.getElementsByClassName("close")[0];

	$(document).ready(function () { 
		 	 
		 	// Add slimscroll to element 
		 	$('.scroll_content').slimscroll({ 
		 	/* height: '250px'  */
		 	}) 
		 	 
		 	}); 
	$(window).load(
			function() {

				$('body').removeClass("white-bg");
				$("#barInMenu").addClass("hidden");
				$("#wrapper").removeClass("hidden");
				$(".chosen-select").chosen().trigger('chosen:updated');
				$('tr.data-wrapper:first select').chosen();
				$(".chosen-select option:first").attr('selected', 'selected');
				$('.Status').find('.chosen-select').chosen().trigger(
						'chosen:updated');
				$('.buildType').find(".chosen-select option:first").attr(
						'selected', 'selected');

				var tableValue = "${Model.getAllBugs}";

				if (tableValue != '') {

					$('#tableDataDiv').removeClass("hidden");
				}
				var json="${Model.JsonData}";
				if(json.length>0){
					showTableData(json);
				$('#editBtn').addClass('hidden');
				$('#updateQueryBtn').addClass('hidden');
				$('#createNewBtn').addClass('hidden');
				}

			});
	$(function() {

		var releaseTable = $('#releaseTable').DataTable({

			buttons : [],
			"paging" : true, 
			"lengthChange" : true,
			"searching" : true,
			"ordering" : true
		});
		var manualBuildTable = $('#manualbuildTable').DataTable({

			buttons : [],
			"paging" : true,
			"lengthChange" : true,
			"ordering" : true
		});
		var autoBuildTable = $('#autobuildTable').DataTable({

			buttons : [],
			"paging" : true,
			"lengthChange" : true,
			"searching" : true,
			"ordering" : true
		});
		var autoBuildTable = $('#viewBugTable').DataTable({

			buttons : [],
			"paging" : true,
			"lengthChange" : true,
			"searching" : true,
			"ordering" : true
		});

	});

	function addRow(currentClass) {

		rowCount++;
		var result = $(currentClass).text();

		var optionValue = $(currentClass).parent('.button-group').parent(
				'.tableRow').find('.selectCondition').val();
		var currentRow = $(currentClass).parent('.button-group').parent(
				'.tableRow');
		$(".chosen-select").chosen().trigger('chosen:updated');
		var ParentRow = $("table tr.data-wrapper").last();

		clone.find('.selectCondition')
				.attr('id', "selectCondition_" + rowCount);
		clone.find('.selectCondition2').attr('id',
				"selectCondition2_" + rowCount);

		var operatorText = $(currentClass).text();
		clone.find('.dropdown1').attr('id', "dropdown1_" + rowCount);
		clone.find('.dropdown2').attr('id', "dropdown2_" + rowCount);
		clone.find('.dropdown3').attr('id', "dropdown3_" + rowCount);
		clone.find('.dropdown4').attr('id', "dropdown4_" + rowCount);
		clone.find('.dropdown5').attr('id', "dropdown5_" + rowCount);
		clone.find('.dropdown6').attr('id', "dropdown6_" + rowCount);
		clone.find('.content1').attr('id', "div1_" + rowCount);
		clone.find('.content2').attr('id', "div2_" + rowCount);
		clone.find('.content3').attr('id', "div3_" + rowCount);
		clone.find('.content4').attr('id', "div4_" + rowCount);
		clone.find('.content5').attr('id', "div5_" + rowCount);
		clone.find('.content6').attr('id', "div6_" + rowCount);

		clone.find('.selectCondition').val(5);
		clone.find('.operatorValue').removeClass('hidden');
		$(".chosen-select").chosen().trigger('chosen:updated');
		clone.find('.operatorValue').text(operatorText);

		clone.find('.ParentOperator').text('');

		clone.clone(true).insertAfter(currentRow);
		//$('tr.data-wrapper:last select').chosen();
		//$('tr.data-wrapper:first select').chosen();
		//$(".chosen-select option:first").attr('selected', 'selected');
		$(".chosen-select").chosen().trigger('chosen:updated');
		$('.operatorSelected').removeClass('hidden');

		buildSelection();
		$(".chosen-select").chosen().trigger('chosen:updated');

	}

	function getSelectedvalue(selectedOption) {
		$('.Status').find('.chosen-select').chosen().trigger('chosen:updated');
		var selectedId = $(selectedOption).attr("id");
		var lastChar = selectedId.slice(-1);
		var selectedvalue = $(selectedOption).val();
		var showDiv, length;
		var StatusSelected = "#selectCondition2_" + lastChar;
		$('.chosen-select').chosen().trigger('chosen:updated');
		if (selectedvalue == 6) {
			$(StatusSelected).find('.date').removeClass("hidden");
			$(StatusSelected).find('.other').addClass("hidden");
		} else {
			$(StatusSelected).find('.date').addClass("hidden");
			$(StatusSelected).find('.other').removeClass("hidden");
		}
		$('.Status').find('.chosen-select').chosen().trigger('chosen:updated');
		length = $("#selectCondition_1 option").length;

		for (i = 1; i <= length; i++) {

			if (i == selectedvalue) {
				showDiv = "#div" + i + "_" + lastChar;
				$(showDiv).removeClass("hidden");

				if (i == 2 || i == 1 || i == 6) {
					$(showDiv).find('.inputText').addClass("selectedOption");
				} else
					$(showDiv).find('.chosen-select')
							.addClass("selectedOption");
			} else {
				showDiv = "#div" + i + "_" + lastChar;
				$(showDiv).addClass("hidden");

				$(showDiv).find('.inputText').removeClass("selectedOption");
				$(showDiv).find('.chosen-select').removeClass("selectedOption");

			}
		}
		i = 1;

		$(".chosen-select").chosen().trigger('chosen:updated');
	}

	function saveOrExecuteQuery(query) {
		var arrayIndex = 0, status = true;
		$('.chosen-select').trigger("chosen:updated");
		var searchParameter = [], Status = [], selectedResult = [], operatorArray = [], parentOperator = [], mainArray = [];
		var jsonObjectAND = '{ "AND1" : [';
		var jsonObjectOR = '{ "OR1" : [';
		var tempDataHolder = '', OpeartorLine = '', result;
		$('#queryTable tbody tr.tableRow').each(
				function(row, tr) {
					fieldValue = $(tr).find('td.selectedResult').find(
							'.selectedOption').val();
					if (fieldValue == '' || fieldValue == null) {
						status = false;
						$(tr).find('td.selectedResult').find('.fieldError')
								.removeClass("hidden");
					} else {
						$(tr).find('td.selectedResult').find('.fieldError')
								.addClass("hidden");
					}
				});

		if (status == true) {
			$('#queryTable tbody tr.tableRow')
					.each(
							function(row, tr) {
								if (tempDataHolder == '') {
									tempDataHolder = 'AND';
									andCount = 1;
								}
								if (tempDataHolder == 'AND') {
									var operatorValue = $(tr).find(
											'.operatorValue').text();
									var selectedParameter = $(tr).find(
											'td.searchParameter').find(
											'.option:selected').text();
									var status = $(tr).find('td.Status').find(
											'.chosen-select').val();
									result = $(tr).find('td.selectedResult')
											.find('.selectedOption').attr(
													'actualValue');
									if (result == null) {
										result = $(tr)
												.find('td.selectedResult')
												.find('.selectedOption').val();

									}
									var buildTypeVal = $(tr).find(
											'td.selectedResult').find(
											'.content2').find('.buildType')
											.val();
									if (buildTypeVal == null) {
										buildTypeVal = 0;
									}
									OpeartorLine = '{"operator" : "'+ operatorValue+ '","parameter" : "'+ selectedParameter+ '", "condition" :"' + status+ '", "selectedData" :"' + result+ '", "buildType" :"'+ buildTypeVal + '"},';
								}

								jsonObjectAND += OpeartorLine;
								OpeartorLine = '';
							});
			jsonObjectAND += ']}';

			tempDataHolder = '';

			var finalStringAND = (jsonObjectAND.substr(0,
					jsonObjectAND.length - 3))
					+ (jsonObjectAND.substr(jsonObjectAND.length - 2,
							jsonObjectAND.length - 1));
			
			var jsonData = JSON.stringify(finalStringAND);
			;
			$('#jsonArray').val(jsonData);
			$('#executequery').val(jsonData);
			if (query == true) {
				if ($("#tableForm").valid()) {

					$('body').addClass("white-bg");
					$("#barInMenu").removeClass("hidden");
					$("#wrapper").addClass("hidden");

					$("#tableForm").submit();
				}
			} else {
				if ($("#executeForm").valid()) {

					$('body').addClass("white-bg");
					$("#barInMenu").removeClass("hidden");
					$("#wrapper").addClass("hidden");

					$("#executeForm").submit();
				}

			}
		}
	}

	function getBuildType(BuildTypeId) {

		BuildType = $(BuildTypeId).val();
		buildSelection();

	}

	function buildSelection() {
		if (releaseIDList != null && BuildType != null) {
			for (var i = 0; i < releaseIDList.length; i++) {
				$(".releaseId_" + releaseIDList[i]).each(function() {
					var showValue = ".releaseId_" + releaseIDList[i];
					$(showValue).removeClass("hidden");
					$(showValue).trigger("chosen:updated");
				});
			}
			if (BuildType != null) {
				var buildTypeLen = BuildType.length;

				if (buildTypeLen = 1) {
					if (BuildType == 1) {
						$('.buildType_2').addClass("hidden");
					} else if (BuildType == 2) {
						$('.buildType_1').addClass("hidden");
					} else {

					}
				} else {
				}
				$(".buildType_1").trigger("chosen:updated");
				$(".buildType_2").trigger("chosen:updated");
			}
			$('.chosen-select').trigger("chosen:updated");
		}
	}

	function showCalender() {
		$('.inputDate').datepicker({
			keyboardNavigation : false,
			forceParse : false,
			autoclose : true
		})
		;
	}

	function removeRow(selectedRow) {
		var rowCount = $('#queryTable tbody  tr').length;
		rowIndex = $(selectedRow).parent('.button-group').parent('.tableRow')
				.index();

		if (rowIndex != 0) {

			$(selectedRow).closest('tr').remove();
		}
	}

	function showPop(currentClass) {

		rowNumber = $(currentClass).closest('tr').index();
		modal.style.display = "block";

		$('#releaseModal').css('height', "100%");
		$('.modal-body').css('height', "50%");
		$('#releaseModal').css('top', 0);
		$('input[type=checkbox]').attr('checked', false);

	}

	$('.closePopup').click(function() {
		buildModal.style.display = "none";
		modal.style.display = "none";
	});

	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";

		}
		if (event.target == buildModal) {
			buildModal.style.display = "none";

		}
	}

	$('.windowClose').click(
			function() {

				var releaseName = '', releaseId = [], release = [];
				modal.style.display = "none";

				$('#releaseTable tbody  tr').each(
						function(row, tr) {
							if ($(tr).find('td.checkboxTD').find(".releaseId")
									.is(':checked')) {
								release.push($(tr).find('td.releaseTD').find(
										".releaseName").val());
								releaseId.push($(tr).find('td.checkboxTD')
										.find(".releaseId").attr('release'));

							}

						});

				releaseName = releaseName.substr(0, releaseName.length - 1);

				releaseIDList = releaseId;
				releaseValue = 1;

				$('#queryTable tbody  tr').eq(rowNumber).find(
						'td.selectedResult').find('.form-group').find(
						'.content1').find(".releaseText").val(release);
				$('#queryTable tbody  tr').eq(rowNumber).find(
						'td.selectedResult').find('.form-group').find(
						'.content1').find(".releaseText").attr('actualValue',
						releaseId);

				$('.autoBuildRow').addClass("hidden");
				$('.manualBuildRow').addClass("hidden");

			});

	$('.closeBuildWindow').click(
			function() {

				var buildId = [], buildName = [];
				var buildTypeValue = $('#queryTable tbody  tr').eq(rowNumber)
						.find('td.selectedResult').find('.form-group').find(
								'.content2').find(".buildType").val();

				if (buildTypeValue == 1) {
					$('#manualbuildTable tbody  tr').each(
							function(row, tr) {
								if ($(tr).find('td.checkboxTD').find(
										".manualBuild").is(':checked')) {
									buildId.push($(tr).find('td.checkboxTD')
											.find(".manualBuild")
											.attr('manual'));
									buildName.push($(tr).find('td.buildTD')
											.find(".manualBuildVal").attr(
													'buildName'));
								}

							});
					$('#queryTable tbody  tr').eq(rowNumber).find(
							'td.selectedResult').find('.form-group').find(
							'.content2').find(".buildText").val(buildName);
					$('#queryTable tbody  tr').eq(rowNumber).find(
							'td.selectedResult').find('.form-group').find(
							'.content2').find(".buildText").attr('actualValue',
							buildId);
				} else if (buildTypeValue == 2) {
					$('#autobuildTable tbody  tr').each(
							function(row, tr) {
								if ($(tr).find('td.checkboxTD').find(
										".autoBuildcheck").is(':checked')) {
									buildId.push($(tr).find('td.checkboxTD')
											.find(".autoBuildcheck").attr(
													'auto'));
									buildName.push($(tr).find('td.buildTD')
											.find(".autoBuildVal").attr(
													'buildName'));
								}

							});
					$('#queryTable tbody  tr').eq(rowNumber).find(
							'td.selectedResult').find('.form-group').find(
							'.content2').find(".buildText").val(buildName);
					$('#queryTable tbody  tr').eq(rowNumber).find(
							'td.selectedResult').find('.form-group').find(
							'.content2').find(".buildText").attr('actualValue',
							buildId);
				}
				buildModal.style.display = "none";

			});

	function buildPopUp(currentClass) {
		var dataTable = $('#manualbuildTable').DataTable();
		dataTable.destroy();
		var dataTable1 = $('#autobuildTable').DataTable();
		dataTable1.destroy();
		$('input[type=checkbox]').attr('checked', false);
		rowNumber = $(currentClass).closest('tr').index();
		BuildType = $(currentClass).closest('tr').find('.selectedResult').find(
				'.content2').find('.buildType').val();
		buildModal.style.display = "block";
		if (releaseValue == 1 && releaseStatus == 'is') {
			if (releaseIDList != null) {
				$('.manualBuildRow').addClass("hidden");
				$('.autobuildRow').addClass("hidden");
				for (var i = 0; i < releaseIDList.length; i++) {
					$(".releaseId_" + releaseIDList[i]).each(function() {
						var showValue = ".releaseId_" + releaseIDList[i];
						$(showValue).removeClass("hidden");

					});
				}

				if (BuildType == 1) {
					$('.manualbuildTable').removeClass("hidden");
					$('.autobuildTable').addClass("hidden");
					$('.autobuildRow').addClass("hidden");

				} else if (BuildType == 2) {
					$('.manualbuildTable').addClass("hidden");
					$('.autobuildTable').removeClass("hidden");
					$('.manualBuildRow').addClass("hidden");
				} else {
					$('.manualbuildTable').removeClass("hidden");
					$('.autobuildTable').removeClass("hidden");

				}

			}
		} else if (releaseValue == 1 && releaseStatus == 'isnot') {
			if (releaseIDList != null) {
				$('.manualBuildRow').removeClass("hidden");
				$('.manualbuildTable').removeClass("hidden");

				$('.autobuildTable').removeClass("hidden");
				$(".autoBuildRow1").each(function() {

					$('.autoBuildRow1').removeClass("hidden");

				});
				$('.autoBuildRow1').removeClass("hidden");

				for (var i = 0; i < releaseIDList.length; i++) {
					$(".releaseId_" + releaseIDList[i]).each(function() {
						var showValue = ".releaseId_" + releaseIDList[i];
						$(showValue).addClass("hidden");
					});
				}
				if (BuildType == 1) {
					$('.manualbuildTable').removeClass("hidden");
					$('.autobuildTable').addClass("hidden");
					$('.autobuildRow').addClass("hidden");
				} else if (BuildType == 2) {
					$('.autobuildTable').removeClass("hidden");
					$('.manualbuildTable').addClass("hidden");

					$('.manualBuildRow').addClass("hidden");
				} else {
					$('.manualbuildTable').addClass("hidden");
					$('.autobuildTable').addClass("hidden");
				}

			}
		} else {
			if (BuildType == 1) {
				$('.manualbuildTable').removeClass("hidden");
				$('.manualBuildRow').removeClass("hidden");
				$('.autobuildTable').addClass("hidden");
				$('.autoBuildRow1').addClass("hidden");
			} else if (BuildType == 2) {
				$('.manualbuildTable').addClass("hidden");
				$('.manualBuildRow').addClass("hidden");
				$('.autobuildTable').removeClass("hidden");
				$('.autoBuildRow1').removeClass("hidden");
			}
		}
		var manualBuildTable = $('#manualbuildTable').DataTable({

			buttons : [],
			"paging" : true,
			"lengthChange" : true,
			"ordering" : true,
			
		});
		var autoBuildTable = $('#autobuildTable').DataTable({

			buttons : [],
			"paging" : true,
			"lengthChange" : true,
			"ordering" : true,
			
		});
		 
	}

	function selectBuildType(currentClass) {

		var buildTypeValue = $(currentClass).val();
		$(".chosen-select").chosen().trigger('chosen:updated');
		BuildType = buildTypeValue;
		$('.autoBuildRow').addClass("hidden");
		$('.manualBuildRow').addClass("hidden");
		$('.manualbuildTable').addClass("hidden");
		$('.autobuildTable').addClass("hidden");

	}

	function getConditionValue(selectedclass) {
		releaseStatus = $(selectedclass).val()
		var parameterValue = $(selectedclass).closest('tr').find(
				'.searchParameter').find('.chosen-select').val();
		if (parameterValue == 1) {
			releaseValue = 1;
		}
		$('.autoBuildRow').addClass("hidden");
		$('.manualBuildRow').addClass("hidden");
		$('.manualbuildTable').addClass("hidden");
		$('.autobuildTable').addClass("hidden");

	}
	function saveQuery() {
		swal({
			title : "Save your query",
			text : "Enter the name of query",
			type : "input",
			showCancelButton : true,
			closeOnConfirm : false,
			animation : "slide-from-left",
			inputPlaceholder : "The name of query"
		},

		function(inputValue) {
			if (inputValue === false)
				return false;
			if (inputValue === "") {
				swal.showInputError("Please enter name of the query!");
				return false
			}
			$('#queryName').val(inputValue);
			var saveFlag = true;
			saveOrExecuteQuery(saveFlag);
		});

	}

	
	function editQuery() {
		$('.chosen-select').each(function()  {
			$(this).attr("disabled", false).trigger('chosen:updated');
		});
		$('.inputDate').each(function() {
			$(this).attr("disabled",false);
		});
		$('.popup').each(function() {
			$(this).attr("onclick", "showPop(this)");
		});
		
		$('.buildpopup').each(function() {
			$(this).attr("onclick", "buildPopUp(this)");
		});
		
		$('#editBtn').addClass('hidden');
		$('#executeButton').addClass('hidden');
		$('#resetBtn').addClass('hidden');
		$('#createNewBtn').removeClass('hidden');
		$('#updateQueryBtn').removeClass('hidden');
	}
	function displayQueryData(savedQuery,queryName,queryId) {
		releaseIDList=[];
		
		if(queryName!=null){
			$('#submitButton').addClass('hidden');
			$('#editBtn').removeClass('hidden');
			$('#updateQueryBtn').addClass('hidden');
			$('#createNewBtn').addClass('hidden');
			$('#executeButton').removeClass('hidden');
			$('#resetBtn').removeClass('hidden');
		}
		$('#queryId').val(queryId);
		$('#queryName').val(queryName);
		query = savedQuery.replace("(", " ");
		var valueArray = query.split(" ");
		var opertarArray = [], parameterArray = [], conditionArray = [], resultArray = [], buildType = []
		var j = 0, releaseName = [], releaseId = [], manualbuildName = [], manualbuildId = [], autobuildNameArray = [], autobuildId = [], statusValue;
		$('.ErrorStatus').addClass("hidden");
		$('.selectCondition').removeAttr('selected');
		$('#releaseTable tbody  tr.releaseTR').each(
				function(row, tr) {

					releaseName.push($(tr).find('td.releaseTD').find(
							'.releaseName').val());
					releaseId.push($(tr).find('td.checkboxTD').find(
							'.releaseId').attr('release'));
				});

		var dataTable = $('#manualbuildTable').DataTable();
			dataTable.destroy();
			$('#manualbuildTable tbody  tr.manualBuildRow').each(
					function(row, tr) {

						manualbuildName.push($(tr).find('td.buildTD').find(
								".manualBuildVal").attr('buildName'));
						manualbuildId.push($(tr).find('td.checkboxTD').find(
								".manualBuild").attr('manual'));
					});
			
			var manualBuildTable = $('#manualbuildTable').DataTable({

				buttons : [],
				"paging" : true,
				"lengthChange" : true,
				"ordering" : true,
				
			});
			var dataTable1 = $('#autobuildTable').DataTable();
			dataTable1.destroy();
		$('#autobuildTable tbody  tr.autoBuildRow1').each(
				function(row, tr) {
					autobuildNameArray.push($(tr).find('td.buildTD').find(
							".autoBuildVal").attr('buildName'));
					autobuildId.push($(tr).find('td.checkboxTD').find(
							".autoBuildcheck").attr('auto'));
				});
		var autoBuildTable = $('#autobuildTable').DataTable({

			buttons : [],
			"paging" : true,
			"lengthChange" : true,
			"ordering" : true,
			
		});
		for (var i = 0; i <= valueArray.length; i++) {

			if (i == 0) {
				opertarArray[j] = "";
			} else {
				opertarArray[j] = valueArray[i];
				i++;
			}
			parameterArray[j] = valueArray[i];
			i++;
			conditionArray[j] = valueArray[i];
			i++;
			resultArray[j] = valueArray[i];
			i++;
			buildType[j] = valueArray[i];
			j++;
		}

		var arrayCount = conditionArray.length;

		$('#queryTable tr.data-wrapper').not(
				$('#queryTable tr.data-wrapper:first')).remove();
		for (var count = 0; count < arrayCount - 2; count++) {
			rowCount++;
			$(".chosen-select").chosen().trigger('chosen:updated');
			var ParentRow = $("#queryTable tr.data-wrapper").last();

			clone.find('.selectCondition').attr('id',
					"selectCondition_" + rowCount);
			clone.find('.selectCondition2').attr('id',
					"selectCondition2_" + rowCount);
			clone.find('.dropdown1').attr('id', "dropdown1_" + rowCount);
			clone.find('.dropdown2').attr('id', "dropdown2_" + rowCount);
			clone.find('.dropdown3').attr('id', "dropdown3_" + rowCount);
			clone.find('.dropdown4').attr('id', "dropdown4_" + rowCount);
			clone.find('.dropdown5').attr('id', "dropdown5_" + rowCount);
			clone.find('.dropdown6').attr('id', "dropdown6_" + rowCount);
			clone.find('.content1').attr('id', "div1_" + rowCount);
			clone.find('.content2').attr('id', "div2_" + rowCount);
			clone.find('.content3').attr('id', "div3_" + rowCount);
			clone.find('.content4').attr('id', "div4_" + rowCount);
			clone.find('.content5').attr('id', "div5_" + rowCount);
			clone.find('.content6').attr('id', "div6_" + rowCount);

			clone.find('.operatorValue').removeClass('hidden');
			$(".chosen-select").chosen().trigger('chosen:updated');

			clone.find('.ParentOperator').text('');

			clone.clone(true).insertAfter(ParentRow);
			$('tr.data-wrapper:last select').chosen();
			$('tr.data-wrapper:first select').chosen();
			$(".chosen-select option:first").attr('selected', 'selected');
			$(".chosen-select").chosen().trigger('chosen:updated');
			$('.operatorSelected').removeClass('hidden');

			buildSelection();
			$(".chosen-select").chosen().trigger('chosen:updated');
		}
		var arrayNumber = 0;
		$('#queryTable tbody  tr.tableRow')
				.each(
						function(row, tr) {

							var dropdownValue = parameterArray[arrayNumber];

							if (dropdownValue == "Build") {
								$(tr).find('td.searchParameter').find(
										'.selectCondition').find('option')
										.removeAttr("selected");
								$(tr).find('td.searchParameter').find(
										'.selectCondition').chosen().trigger(
										'chosen:updated');
								var buildName = [];
								$(tr).find('td.searchParameter').find('.selectCondition').val(2).change();
								var buildValue = resultArray[arrayNumber];
								var buildID=buildValue.split(",");
								if (buildType[arrayNumber] == 1) {
									
									for (var i = 0; i < buildID.length; i++) {
									var buildIdArray=manualbuildId;
										for (var j = 0; j < buildIdArray.length; j++) {
											if (buildIdArray[j] == buildID[i]) {
												buildName.push(manualbuildName[j]);
											}
										}
										buildIdArray=[];
									}
								} else if (buildType[arrayNumber] == 2) {
									
									for (var i = 0; i < buildID.length; i++) {
										
										for (var j = 0; j < autobuildId.length; j++) {
											if (autobuildId[j] == buildID[i]) {

												buildName
														.push(autobuildNameArray[j]);
											}
										}
									}
									
								}
								autobuildName=[];
								var buildTypeVal = ".buildType option[value='"
										+ buildType[arrayNumber] + "']";
								$(tr).find('.selectedResult').find('.content2')
										.find(buildTypeVal).attr("selected",
												"selected");
								$(tr).find('.selectedResult')
										.find(".buildText").val(buildName);
								$(tr).find('.selectedResult')
										.find(".buildText").attr('actualvalue',
												buildID);
								$(tr).find('.selectedResult')
										.find(".buildText").addClass(
												"selectedOption");
								statusValue = ".selectCondition2 option[value='"
										+ conditionArray[arrayNumber] + "']";
								$(tr).find('.selectedResult').find('.content1')
										.addClass("hidden");
								$(tr).find('.selectedResult').find('.content2')
										.removeClass("hidden");
								$(tr).find('.Status').find(statusValue).attr(
										"selected", "selected");
								$(tr).find('.selectedResult').find(
										".releaseText").removeClass(
										"selectedOption");

							} else if (dropdownValue == "Release") {

								$(tr).find('td.searchParameter').find(
										'.selectCondition').find('option')
										.removeAttr("selected");
								
								
								var relName = [], releaseData = [];
								releaseValue = 1;

								$(tr).find('td.searchParameter').find('.selectCondition').val(1).change();
								$(tr).find('td.searchParameter').find(
										".form-group").find(".chosen-select")
										.chosen().trigger('chosen:updated');
								var releaseval = resultArray[arrayNumber];

								for (var i = 0; i < releaseval.length; i++) {
									if (releaseval[i] != ",") {
										releaseData.push(releaseval[i]);

									}
								}
								for (var i = 0; i < releaseData.length; i++) {

									for (var j = 0; j < releaseId.length; j++)
										if (releaseData[i] == releaseId[j]) {
											relName.push(releaseName[i]);
										}
								}
								releaseIDList = releaseData;
								$(tr).find('.selectedResult').find(
										'.releaseText').val(relName);
								$(tr).find('.selectedResult').find(
										".releaseText").attr('actualValue',
										releaseData);
								$(tr).find('.selectedResult').find(
										".releaseText").addClass(
										"selectedOption");
								statusValue = ".selectCondition2 option[value='"
										+ conditionArray[arrayNumber] + "']";
								$(tr).find('.Status').find(statusValue).attr(
										"selected", "selected");
								$(tr).find('.selectedResult').find('.content1')
										.removeClass("hidden")

								$(tr).find('td.searchParameter').find(
										'.selectCondition').chosen().trigger(
										'chosen:updated');

							} else if (dropdownValue == "Status") {

								$(tr).find('td.searchParameter').find('.selectCondition').find('option').removeAttr("selected");
								$(tr).find('td.searchParameter').find(
										'.selectCondition').chosen().trigger(
										'chosen:updated');
								var statusValue = resultArray[arrayNumber], status = ".selectCondition option[value='"
										+ 3 + "']";
								;

								$(tr).find('td.searchParameter').find('.selectCondition').val(3).change();
								for (var i = 0; i < statusValue.length; i++) {
									if (statusValue[i] != ",") {
										var statusDropdown = ".chosen-select option[value='"
												+ statusValue[i] + "']";
										$(tr).find('td.selectedResult').find(
												'.content3').find(
												statusDropdown).attr(
												"selected", "selected");
									}
								}
								statusValue = ".selectCondition2 option[value='"
										+ conditionArray[arrayNumber] + "']";
								$(tr).find('.Status').find(statusValue).attr(
										"selected", "selected");
								$(tr).find('.selectedResult').find('.content1')
										.addClass("hidden");
								$(tr).find('.selectedResult').find('.content3')
										.removeClass("hidden")
								$(tr).find('.selectedResult').find('.content3')
										.find(".requiredField").addClass(
												"selectedOption");
								$(tr).find('.selectedResult').find(
										".releaseText").removeClass(
										"selectedOption");
							} else if (dropdownValue == "Priority") {

								$(tr).find('td.searchParameter').find(
										'.selectCondition').find('option')
										.removeAttr("selected");
								$(tr).find('td.searchParameter').find(
										'.selectCondition').chosen().trigger(
										'chosen:updated');
								var priorityValue = resultArray[arrayNumber], priority = ".selectCondition option[value='"
										+ 4 + "']";
								;

								$(tr).find('td.searchParameter').find('.selectCondition').val(4).change();
								for (var i = 0; i < priorityValue.length; i++) {
									if (priorityValue[i] != ",") {

										var priorityDropdown = ".chosen-select option[value='"
												+ priorityValue[i] + "']";
										$(tr).find('td.selectedResult').find(
												'.content4').find(
												priorityDropdown).attr(
												"selected", "selected");
									}
								}
								statusValue = ".selectCondition2 option[value='"
										+ conditionArray[arrayNumber] + "']";
								$(tr).find('.Status').find(statusValue).attr(
										"selected", "selected");
								$(tr).find('.selectedResult').find('.content1')
										.addClass("hidden");
								$(tr).find('.selectedResult').find('.content4')
										.removeClass("hidden");
								$(tr).find('.selectedResult').find('.content4')
										.find(".chosen-select").addClass(
												"selectedOption");
								$(tr).find('.selectedResult').find(
										".releaseText").removeClass(
										"selectedOption");
								$(".chosen-select").chosen().trigger(
										'chosen:updated');
							} else if (dropdownValue == "Severity") {

								$(tr).find('td.searchParameter').find(
										'.selectCondition').find('option')
										.removeAttr("selected");
								$(tr).find('td.searchParameter').find(
										'.selectCondition').chosen().trigger(
										'chosen:updated');
								var severityValue = resultArray[arrayNumber], severity = ".selectCondition option[value='"
										+ 5 + "']";
								;

								$(tr).find('td.searchParameter').find('.selectCondition').val(5).change();
								for (var i = 0; i < severityValue.length; i++) {
									if (severityValue[i] != ",") {

										var severityDropdown = ".chosen-select option[value='"
												+ severityValue[i] + "']";
										$(tr).find('td.selectedResult').find(
												'.content5').find(
												severityDropdown).attr(
												"selected", "selected");
									}
								}
								statusValue = ".selectCondition2 option[value='"
										+ conditionArray[arrayNumber] + "']";
								$(tr).find('.Status').find(statusValue).attr(
										"selected", "selected");
								$(tr).find('.selectedResult').find('.content1')
										.addClass("hidden");
								$(tr).find('.selectedResult').find('.content5')
										.removeClass("hidden");
								$(tr).find('.selectedResult').find('.content5')
										.find(".chosen-select").addClass(
												"selectedOption");
								$(tr).find('.selectedResult').find(
										".releaseText").removeClass(
										"selectedOption");
							} else if (dropdownValue == "Date") {

								$(tr).find('td.searchParameter').find(
										'.selectCondition').find('option')
										.removeAttr("selected");
								$(tr).find('td.searchParameter').find(
										'.selectCondition').chosen().trigger(
										'chosen:updated');
								var dateValue = resultArray[arrayNumber], date = ".selectCondition option[value='"
										+ 6 + "']";
								;
								$(tr).find('td.selectedResult').find(
										'.content6').find('.input-group').find(
										'.inputText').val(dateValue);
								$(tr).find('td.searchParameter').find('.selectCondition').val(6).change();

								$(tr).find('.selectedResult').find('.content1')
										.addClass("hidden");
								$(tr).find('.selectedResult').find('.content6')
										.removeClass("hidden");
								statusValue = ".selectCondition2 option[value='"
										+ conditionArray[arrayNumber] + "']";
								$(tr).find('.Status').find(statusValue).attr(
										"selected", "selected");
								$(tr).find('.selectedResult').find(
										".releaseText").removeClass(
										"selectedOption");
								$(tr).find('.selectedResult').find('.content6')
										.find('.DateGroup').find('.inputDate')
										.addClass("selectedOption");

							}

							$(".chosen-select").chosen().trigger(
									'chosen:updated');
							$(tr).find('.childOp').find(".operatorValue").text(
									opertarArray[arrayNumber]);
							arrayNumber++;

						});
		if(queryName!=null){
		$('.chosen-select').each(function() {
			$(this).attr("disabled", "true").trigger('chosen:updated');
		});
		$('.inputDate').each(function() {
			$(this).attr("disabled", "true");
		});
		$('.popup').each(function() {
			$(this).attr("onclick", "return false;");
		});
		$('.buildpopup').each(function() {
			$(this).attr("onclick", "return false;");
		});
		}
	}
	
	$('#updateQueryBtn').click(function(){
		
		saveOrExecuteQuery();
		if ($("#tableForm").valid()) {

			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");
			$('#tableForm').attr("action","updatecustomquery");
			$("#tableForm").submit();
		}
	});
	
	function showTableData(json){
		displayQueryData(json);
		$('#submitButton').removeClass('hidden');
		$('#editBtn').addClass('hidden');
		$('#updateQueryBtn').addClass('hidden');
		$('#createNewBtn').addClass('hidden');
		$('#executeButton').removeClass('hidden');
	}
	function deleteQuery(id){
		
		 swal({
		        title: "Are you sure?",
		        text: "You will not be able to recover this query!",
		        type: "warning",
		        showCancelButton: true,
		        confirmButtonColor: "#DD6B55",
		        confirmButtonText: "Yes, delete it!",
		        closeOnConfirm: false
		    }, function () {
		    	$('body').addClass("white-bg");
				$("#barInMenu").removeClass("hidden");
				$("#wrapper").addClass("hidden");
		    	var posting = $.post('deleteCustomSearchQuery', {
					 queryId : id,
					});
					 posting.done(function(data) {
						 $('body').removeClass("white-bg");
							$("#barInMenu").addClass("hidden");
							$("#wrapper").removeClass("hidden");
							
						if(data=="success"){
							$("#viewQueryLi_"+id).remove();
							swal("Deleted!", "Query has been deleted.", "success");
						}else{
							swal("Failure!", "Query deletion unsuccessful.", "warning");
						}
					});		    	
		    });
	}
	
	function resetSelection(){
		$('.selectedOption').each(function() {
			$(this).val('').trigger('chosen:updated');
		});
		$('.popup').each(function() {
			$(this).attr("value", "");
		});  
		
	}
	
	$(function(){
		
	var statusLabels=[];
	var statusData=${Model.statusData};
	
	
	
		var doughnutData = {
				labels : [ "New", "Closed", "Assigned", "Dev in Progress","Ready for QA","Reject"],
				datasets : [ {
					data : [ statusData.New, statusData.Closed, statusData.Assigned, statusData.DevinProgress,statusData.ReadyforQA,statusData.Reject],
					backgroundColor : [ "#5d85d5", "#009900", "#ff8c1a","#b3b300","#804000","#595959" ],
					hoverBackgroundColor : [ "#5d85d5", "#009900", "#ff8c1a","#b3b300","#804000","#595959"]
				} ]
			};

			var doughnutOptions = {
				segmentShowStroke : true,
				segmentStrokeColor : "#fff",
				segmentStrokeWidth : 2,
				percentageInnerCutout : 45,
				animationSteps : 100,
				animationEasing : "easeOutBounce",
				animateRotate : true,
				animateScale : true,
				responsive : true,
				legend : {
					position : 'top'
				}
			};

			var priorityChartId = document.getElementById("StatusPiechart");
			var myDoughnutChart = new Chart(priorityChartId, {
				type : 'doughnut',
				data : doughnutData,
				options : doughnutOptions
			});
			
			var priorityLabels=[];
			var priorityData=${Model.priorityData};
			
			var doughnutData = {
					labels : [ "Immediate", "High", "Medium", "Low" ],
					datasets : [ {
						data : [ priorityData.Immediate,priorityData.High,priorityData.Medium,priorityData.Low],
						backgroundColor : [ "#144b7e", "#148fce", "#bbbbbb",
								"#14ceb0" ],
						hoverBackgroundColor : [ "#144b7e","#148fce", "#bbbbbb",
								"#14ceb0" ]
					} ]
				};

				var doughnutOptions = {
					segmentShowStroke : true,
					segmentStrokeColor : "#fff",
					segmentStrokeWidth : 2,
					percentageInnerCutout : 45, // This is 0 for Pie charts
					animationSteps : 100,
					animationEasing : "easeOutBounce",
					animateRotate : true,
					animateScale : true,
					responsive : true,
					legend : {
						position : 'top'
					}
				};

				var ctx = document.getElementById("priorityPiechart");
				var myDoughnutChart = new Chart(ctx, {
					type : 'doughnut',
					data : doughnutData,
					options : doughnutOptions
				});
				var severityLabels=[];
				var severityData=${Model.severityData};
				
				var doughnutData = {
						labels : [ "Critical", "Major", "Moderate", "Minor" ],
						datasets : [ {
							data : [severityData.Critical,severityData.Major, severityData.Moderate,severityData.Minor],
							backgroundColor : [ "#e59f32", "#564c4c", "#a6926a",
									"#bc8f2d" ],
							hoverBackgroundColor : [ "#e59f32", "#564c4c", "#a6926a",
									"#bc8f2d" ]
						} ]
					};

					var doughnutOptions = {
						segmentShowStroke : true,
						segmentStrokeColor : "#fff",
						segmentStrokeWidth : 2,
						percentageInnerCutout : 45, // This is 0 for Pie charts
						animationSteps : 100,
						animationEasing : "easeOutBounce",
						animateRotate : true,
						animateScale : true,
						responsive : true,
						legend : {
							position : 'top'
						}
					};

					var priorityChartId = document.getElementById("severityPiechart");
					var myDoughnutChart = new Chart(priorityChartId, {
						type : 'doughnut',
						data : doughnutData,
						options : doughnutOptions
					});
					
					var dataBar = [];
					 var dataset = {
						label : "Nitin Gupta",
						backgroundColor :"rgba(30, 192, 251, 0.51)",
						data : [1,2,3,4,5,6]
					}
					
					var barData = {
					        labels: ["New","Assigned","Dev in Progress","Ready for QA","Closed","Reject"],
					        datasets: dataBar
					    };
					 dataBar.push(dataset);
					    var barOptions = {
					        scaleBeginAtZero: true,
					        scaleShowGridLines: true,
					        scaleGridLineColor: "rgba(0,0,0,.05)",
					        scaleGridLineWidth: 1,
					        barShowStroke: true,
					        barStrokeWidth: 2,
					        barValueSpacing: 5,
					        barDatasetSpacing: 1,
					        responsive: true,
					        scales: {
				                yAxes: [{
				                        ticks: {
				                            min: 0,
				                            callback: function(value) {if (value % 1 === 0) {return value;}}
				                        }
				                }]
				            }
					    }
					    var ctx1 = document.getElementById("StatusPieChart");
					    var myNewChart1 = new Chart(ctx1, {
						    type: 'bar',
						    data: barData,
						    options: barOptions
						});
	});
</script>
<style>
.modal {
	display: none; /* Hidden by default */
	position: fixed; /* Stay in place */
	z-index: 1; /* Sit on top */
	padding-top: 100px; /* Location of the box */
	left: 0;
	top: 0;
	width: 100%; /* Full width */
	height: 100%; /* Full height */
	background-color: rgb(0, 0, 0); /* Fallback color */
	background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
	background-color: #fefefe;
	margin: auto;
	padding: 20px;
	border: 1px solid #888;
}

/* The Close Button */
.close {
	color: #aaaaaa;
	float: right;
	font-size: 28px;
	font-weight: bold;
}

.close:hover, .close:focus {
	color: #000;
	text-decoration: none;
	cursor: pointer;
}
</style>

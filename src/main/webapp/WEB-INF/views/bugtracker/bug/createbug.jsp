<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Create Bug</h2>
			<!-- <ol class="breadcrumb">
				<li><a href="javascript:void(0)" onclick="gotoLinkInMenu('viewallbugs')">Dashboard</a></li>
				<li class="active"><strong>Create Bug </strong></li>
			</ol> -->
			<!-- end breadcrumb -->
		</div>

		<!-- end col-lg-10 -->
	</div>
	<!-- end wrapper -->

	<div class="row sub-topmenu">
		<div class="col-md-12">
			<div class="">

				<div class="col-md-6 pd-l-0">
					<ul>
						<li><a href="javascript:void(0)"
							onclick="gotoLinkInMenu('viewallbugs')"><i class="fa fa-home"></i>
								Home</a></li>
						<li><a href="javascript:void(0)"
							onclick="gotoLinkInMenu('createbug')" class="active "><i
								class="fa fa-plus"></i> Create Bug</a></li>




					</ul>
				</div>

			</div>
		</div>
	</div>

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content">
						<form:form action="insertbug" class="" modelAttribute="uploadForm"
							enctype="multipart/form-data" method="POST" id="createBugForm">
							<div class="content clearfix">
								<fieldset class="body current">

									<div class="row">
										<div class="col-md-12">
											<label style="padding-bottom: 10px; float: right">*
												Fields are Mandatory</label>

										</div>
										<div class="form-group col-md-12">
											<label class="control-label col-lg-1">Summary*:</label>
											<div class="col-lg-11">
												<input type="text" placeholder="Summary"
													class="form-control" required="" aria-required="true"
													name="bugSummary" id="bugSummary">
											</div>
										</div>


										<div class="form-group col-md-6">
											<label class="col-lg-2 control-label">Module*: </label>
											<div class="col-lg-10">
												<select data-placeholder="Select" class="chosen-select"
													style="width: 350px;" tabindex="2" name="moduleId"
													id="selectedModuleID">
													<option value="0"></option>
													<c:forEach var="module" items="${Model.moduleList}">
														<option value="${module.module_id}">${module.module_name}</option>
													</c:forEach>

												</select> <label class="errorMessage hidden">This field is
													required.</label>
											</div>
										</div>
										<%-- <c:if test="${Model.tmStatus==2    ||   Model.tmStatus!=1 }"> --%>
										<div class="form-group hidden col-md-6">
											<label class="col-lg-2 control-label">Build type: </label>
											<div class="col-lg-10">
												<select data-placeholder="Select" class="chosen-select"
													style="width: 350px;" tabindex="2" name="buildTypeId"
													id="selectedBuildTypeID">
													<option value="3">None</option>
													<c:forEach var="buildExecType"
														items="${Model.buildExecType}">
														<option value="${buildExecType.execution_type_id}">${buildExecType.description}</option>
													</c:forEach>

												</select>
											</div>
										</div>
										<div class="form-group hidden col-md-6">
											<label class="col-lg-2 control-label">Build: </label>
											<div class="col-lg-10">
												<select data-placeholder="Select" class="chosen-select"
													style="width: 350px;" tabindex="2" name="selectedBuildID"
													id="selectedBuildID">
													<option value="1">None</option>

												</select>
											</div>
										</div>
										<%-- </c:if> --%>
										<div class="form-group col-md-6">
											<label class="col-lg-2 control-label">Category*: </label>
											<div class="col-lg-10">
												<select data-placeholder="Choose a Status"
													class="chosen-select" style="width: 350px;" tabindex="2"
													name="category">
													<!-- <option value="0"></option> -->
													<c:forEach var="categoryList" items="${Model.categoryList}">
														<option value="${categoryList.category_id}">${categoryList.category_name}</option>
													</c:forEach>
												</select> <label class="errorMessage hidden">This field is
													required.</label>
											</div>
										</div>
										<div class="form-group col-md-6">
											<c:set var="schemaName" value="${Model.schema}" />
											<label class="col-lg-2 control-label">Status*: </label>
											<div class="col-lg-10">
												<select data-placeholder="Choose a Status"
													class="chosen-select" style="width: 350px;" tabindex="2"
													name="statusId">
													<!-- <option value="0"></option> -->
													<%-- 													<c:forEach var="status" items="${Model.statusList}"> --%>
													<%-- 														<option value="${status.status_id}">${status.bug_status}</option> --%>
													<%-- 													</c:forEach> --%>
													<c:choose>
														<c:when test="${schemaName == 'XenonAnmol01172019020400'}">
															<option value="91">In Backlog</option>
														</c:when>
														<c:otherwise>
															<c:forEach var="status" items="${Model.statusList}">
																<option value="${status.status_id}">${status.bug_status}</option>
															</c:forEach>
														</c:otherwise>
													</c:choose>

												</select> <label class="errorMessage hidden">This field is
													required.</label>
											</div>
										</div>
										<!-- end form-group -->

										<div class="form-group col-md-6">
											<label class="col-lg-2 control-label">Priority*:</label>
											<div class="col-lg-10">
												<select data-placeholder="Select" class="chosen-select"
													style="width: 350px;" tabindex="2" name="priorityId">
													<!-- <option value="0"></option> -->
													<c:forEach var="priority" items="${Model.priorityList}">
														<option value="${priority.priority_id}">${priority.bug_priority}</option>
													</c:forEach>
												</select> <label class="errorMessage hidden">This field is
													required.</label>
											</div>
										</div>
										<div class="form-group col-md-6">
											<label class="col-lg-2 control-label">Severity*:</label>
											<div class="col-lg-10">
												<select data-placeholder="Select" class="chosen-select"
													style="width: 350px;" tabindex="2" name="severityId">
													<!-- <option value="0"></option> -->
													<c:forEach var="severity" items="${Model.severityList}">
														<option value="${severity.severity_id}">${severity.bug_severity}</option>
													</c:forEach>
												</select> <label class="errorMessage hidden">This field is
													required.</label>
											</div>
										</div>
										<!-- end form-group -->
										<div class="form-group col-md-6">
											<label class="col-lg-2 control-label">Assign To: </label>
											<div class="col-lg-10">

												<select data-placeholder="Select" class="chosen-select"
													style="width: 350px;" name="assignedUserID"
													id="assignedUserID">
													<option value="-1">None</option>
												</select> <label class="errorMessage hidden">This field is
													required.</label>
											</div>
										</div>
										<!--  end .form-group -->
										<div class="form-group col-md-12">
											<label class="col-lg-1 control-label">Description:</label>
											<div class="col-lg-11">
												<!-- <textarea class="form-control" style="max-width: 100%;"></textarea> -->
												<textarea placeholder="Description" name="bugDescription"
													class="form-control characters summernote"
													style="border: 1px solid #ccc;"></textarea>
												<!-- resize: none;  -->
											</div>
										</div>
										<div class="col-md-12">
											<label class="col-lg-1 control-label">File:</label>
											<table id="fileTable" class="pull-left">
											</table>
											<div class="form-group col-md-11" style="float: left;">
												<input id="addFile" type="button" class="btn btn-success"
													value="Attach File" /> <label class="text-primary"
													style="font-weight: 400;">Supported file types png,
													jpg, pdf, docx</label>
											</div>
										</div>

									</div>
									<!-- <div class="hr-line-solid"></div> -->
								</fieldset>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-md-4">
										<button class="btn btn-white pull-left"
											style="margin-right: 15px" type="button" id="btnCancel">Cancel</button>
										<button type="button" id="btnSubmit"
											class="btn btn-success pull-left ladda-button"
											data-style="slide-up">Submit</button>
									</div>
								</div>
							</div>
						</form:form>


						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<!-- end row -->
	</div>
</div>
<!-- end .md-skin -->
<input type="hidden" value="" id="userData">

<script type="text/javascript">
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");

		var config = {
			'.chosen-select' : {},
			'.chosen-select-deselect' : {
				allow_single_deselect : true
			},
			'.chosen-select-no-single' : {
				disable_search_threshold : 10
			},
			'.chosen-select-no-results' : {
				no_results_text : 'Oops, nothing found!'
			},
			'.chosen-select-width' : {
				width : "95%"
			}
		}
		for ( var selector in config) {
			$(selector).chosen(config[selector]);
			$("ul.chosen-results").css('max-height', '150px');
		}

	});

	$('#selectedBuildTypeID')
			.on(
					'change',
					function() {
						var optionsAsString1 = '';
						var typeId = $('#selectedBuildTypeID').val();
						$('select[name="selectedBuildID"]').append(
								"<option value='1'>None</option>");
						$('#selectedBuildID').find('option').remove();
						if (typeId.localeCompare('1') == 0) {
							var manualData = $
							{
								Model.manualBuildList
							}
							;
							for (var i = 0; i < manualData.length; i++)
								optionsAsString1 += "<option value='"+manualData[i].build_id + "'>"
										+ manualData[i].build_name
										+ "</option>";
							$('select[name="selectedBuildID"]').append(
									optionsAsString1);
						} else if (typeId.localeCompare('2') == 0) {
							var automationData = $
							{
								Model.automationBuildList
							}
							;
							for (var i = 0; i < automationData.length; i++)
								optionsAsString1 += "<option value='"+automationData[i].build_id + "'>"
										+ automationData[i].build_name
										+ "</option>";
							$('select[name="selectedBuildID"]').append(
									optionsAsString1);
						} else {
							$('select[name="selectedBuildID"]').append(
									optionsAsString1);
						}
						$('#selectedBuildID').trigger("chosen:updated");
					});

	$('#selectedModuleID')
			.on(
					'change',
					function() {
						$(this).parent('div').find('.errorMessage').addClass(
								'hidden');
						$(this).parent('div').find('.chosen-container')
								.removeClass('errorDisplay');
						$('#selectedModuleID').parent('div').find(
								'.chosen-container > a').css("background",
								"#ffffff");

						var id = $(this).val();
						$
								.post(
										"getmoduleusers",
										{
											moduleID : id
										},
										function(data, status) {

											//$('#assignedUserID' ).html('');
											//	$('#assignedUserID').trigger("chosen:updated");

											var arr = JSON.parse(JSON
													.stringify(data));
											$('select[name="assignedUserID"]')
													.append(
															"<option value='0'></option>");
											$
													.each(
															arr,
															function(index,
																	jsonObject) {
																if (jsonObject.user_id) {
																	optionsAsString = "<option value='USR"+jsonObject.user_id + "'>"
																			+ jsonObject.userName
																			+ "</option>";
																} else {
																	optionsAsString = "<option value='GRP"+jsonObject.bug_group_id + "'>"
																			+ jsonObject.bug_group_name
																			+ "</option>";
																}

																$(
																		'select[name="assignedUserID"]')
																		.append(
																				optionsAsString);
																$(
																		'#assignedUserID')
																		.trigger(
																				"chosen:updated");
															});
										});
					});

	$(document)
			.ready(
					function() {

						$('.summernote').summernote();
						$('.note-editor').css('background', 'white');
						$('button[ data-event="codeview"]').remove();
						$('.note-editor').css('border', '1px solid #CBD5DD');

						$('div.note-insert').remove();
						$('div.note-table').remove();
						$('div.note-help').remove();
						$('div.note-style').remove();
						$('div.note-color').remove();
						$('button[ data-event="removeFormat"]').remove();
						$('button[ data-event="insertUnorderedList"]').remove();
						$('button[ data-event="fullscreen"]').remove();
						$('button[ data-original-title="Line Height"]')
								.remove();
						$('button[ data-original-title="Font Family"]')
								.remove();
						$('button[ data-original-title="Paragraph"]').remove();

						$('#addFile')
								.click(
										function() {
											var fileIndex = $('#fileTable tr')
													.children().length;
											if (fileIndex > 0) {
												lastIndex = $(
														'#fileTable tr:last')
														.attr("id").length;
												fileIndex = parseInt($(
														'#fileTable tr:last')
														.attr("id").substring(
																9, lastIndex))
														+ parseInt(1);
											}
											$('#fileTable')
													.append(
															'<tr id="tableRow_'+ fileIndex +'"><td>'
																	+ '	<input type="file"  accept=".png,.jpg,.pdf,.docx" class="file hidden" id="file_'+ fileIndex +'" />'
																	+ '<label class="fileName" id="fileName_'+ fileIndex +'"></label>'
																	+ '<a id="close_'+ fileIndex +'" class="close hidden"  style="float: none">�</a>'
																	+ '<label class="error hidden" id="uploadSizeError_'+ fileIndex +'"></label>'
																	+ '<label class="error hidden" id="uploadTypeError_'+ fileIndex +'"></label>'
																	+ '</td></tr>');

											$("#file_" + fileIndex).click();
											$("#file_" + fileIndex)
													.change(
															function() {
																if (this.files[0] == null
																		|| this.files[0] == undefined) {
																	$(
																			"#fileName_"
																					+ fileIndex)
																			.html(
																					"");
																	$(
																			'#tableRow_'
																					+ fileIndex)
																			.remove();
																} else {
																	$(
																			"#fileName_"
																					+ fileIndex)
																			.html(
																					this.files[0].name);
																	$(
																			"#close_"
																					+ fileIndex)
																			.removeClass(
																					'hidden');
																	$('.close')
																			.each(
																					function() {
																						if ($(
																								this)
																								.hasClass(
																										'hidden')) {
																							lastIndex = $(
																									this)
																									.attr(
																											'id').length;
																							id = $(
																									this)
																									.attr(
																											'id')
																									.substring(
																											5,
																											lastIndex);
																							$(
																									'#tableRow'
																											+ id)
																									.remove();
																						}
																					});
																}
															});

											$(".close")
													.click(
															function() {
																lastIndex = $(
																		this)
																		.attr(
																				'id').length;
																id = $(this)
																		.attr(
																				'id')
																		.substring(
																				6,
																				lastIndex);
																$(
																		'#tableRow_'
																				+ id)
																		.remove();
															});

										});

					});
	$(function() {
		var size = $
		{
			Model.moduleList.size()
		}
		;
		if (size == 0) {
			swal({
				title : "You need a minimum 1 module to create a bug",
				text : "Please contact administrator."
			}, function(isConfirm) {

				if (isConfirm) {
					window.location.href = "btdashboard";
				}
			});
		}

		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});

	$(function() {
		$('#createBugForm').validate({

			rules : {
				bugSummary : {
					required : true,
					minlength : 1,
					maxlength : 250
				},
				bugDescription : {
					minlength : 1,
					maxlength : 4000
				}
			}

		});
	});
	$('#btnCancel').click(function() {
		window.location.href = 'btdashboard';
	});

	$("#bugSummary").change(function() {
		console.log(" bugsummary text ", $("#bugSummary").val());
	});

	$("#btnSubmit")
			.click(
					function() {
						var status = true;

						var formStat = $("#createBugForm").valid();
						//check for the validity of select
						var value = $('#selectedModuleID').val();

						if (value == 0) {
							status = false;
							$('#selectedModuleID').parent('div').find(
									'.errorMessage').removeClass('hidden');
							$('#selectedModuleID').parent('div').find(
									'.chosen-container').addClass(
									'errorDisplay');
							$('#selectedModuleID').parent('div').find(
									'.chosen-container > a').css("background",
									"rgb(251, 227, 228)");
						} else {
							$('#selectedModuleID').parent('div').find(
									'.errorMessage').addClass('hidden');
							$('#selectedModuleID').parent('div').find(
									'.chosen-container').removeClass(
									'errorDisplay');
							$('#selectedModuleID').parent('div').find(
									'.chosen-container > a').css("background",
									"#ffffff");
						}

						var counter = 0;
						var statusUpload = 1;
						$('.file')
								.each(
										function() {
											lastIndex = $(this).attr('id').length;
											id = $(this).attr('id').substring(
													5, lastIndex);
											var fsize = 0;
											var filetype = 1;
											if ($(this)[0].files[0]) {
												file = $(this)[0].files[0];
												fsize = file.size;
												var fileName = file.name;
												var fileNameExt = fileName
														.substr(fileName
																.lastIndexOf('.') + 1);
												if (!(fileNameExt.toLowerCase() == "jpg"
														|| fileNameExt
																.toLowerCase() == "png"
														|| fileNameExt
																.toLowerCase() == "docx" || fileNameExt
														.toLowerCase() == "pdf")) {
													filetype = 0;
													statusUpload = 0;
												}
												$(this).attr(
														"name",
														"files[" + counter
																+ "]");
												counter++;
											} else {
												$(this).attr('name', '');
											}
											if (filetype == 0) {
												statusUpload = 0;
												$("#uploadTypeError_" + id)
														.text(
																"Please upload file with one of type (png, jpg, pdf, docx)");
												$("#uploadTypeError_" + id)
														.removeClass('hidden');
												$("#uploadTypeError_" + id)
														.css('display', '');
											} else if (fsize > 1048576) {
												statusUpload = 0;
												$("#uploadSizeError_" + id)
														.text(
																"Please upload file with size less than 1MB");
												$("#uploadSizeError_" + id)
														.removeClass('hidden');
												$("#uploadSizeError_" + id)
														.css('display', '');
											}
										});

						//submit the form	
						if (formStat && status) {

							if (statusUpload == 1) {
								$('body').addClass("white-bg");
								$("#barInMenu").removeClass("hidden");
								$("#wrapper").addClass("hidden");

								$('.summernote').each(function() {
									$(this).val($(this).code());
								})
								$('#createBugForm').submit();
							}
						}

					});
</script>
<style>
.md-skin .ibox {
	box-shadow: none;
	margin-bottom: 50px;
}

.ibox-content {
	padding: 20px;
}

.md-skin .wrapper-content {
	padding: 0;
	margin: 10px 0;
}
</style>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Bug Summary</h2>
			<!-- <ol class="breadcrumb">
			<li><a href="btdashboard">Dashboard</a></li>
			<li><a href="viewallbugs">All Bugs</a></li>
			<li class="active"><strong>Bug Summary</strong></li>
		</ol> -->
			<!-- end breadcrumb -->

		</div>
		<!-- end col-lg-10 -->
	</div>
	<!-- end .row .wrapper -->

	<div class="row sub-topmenu">
		<div class="col-md-12">
			<div class="">

				<div class="col-md-6 pd-l-0">
					<ul>
						<li><a href="javascript:void(0)"
							onclick="gotoLinkInMenu('viewallbugs')" class=" "><i
								class="fa fa-home"></i> Home</a></li>
						<li><a href="javascript:void(0)"
							onclick="gotoLinkInMenu('createbug')"><i class="fa fa-plus"></i>
								Create Bug</a></li>


					</ul>
				</div>

			</div>
		</div>
	</div>

	<div class="row wrapper wrapper-content animated fadeInRight">
		<div class="col-lg-12 no-padding">
			<div class="ibox">
				<div class="ibox-content">

					<div class="ibox-content hidden" id="bugUpdateFormDiv">
						<div class="row">
							<div class="col-sm-7">
								<h2>${Model.bugDetails[0].bug_title}:
									${Model.bugDetails[0].bug_prefix}</h2>
							</div>
							<%-- <div class="col-sm-5">
							<button type="button" class="btn btn-white btn-xs pull-right"
								style="margin-right: 10px" onclick="removeBug(${Model.summaryDetails[0].bug_id})">Delete</button>
							<button type="button" class="btn btn-white btn-xs pull-right"
								style="margin-right: 10px" id="editBugBtn">Edit bug</button>
							<button type="button" class="btn btn-success btn-xs pull-right"
								style="margin-right: 10px" id="addCommentBtn">Add
								Comment</button>
						</div> --%>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-12 text-right">
								<p>* fields are mandatory</p>
							</div>
						</div>

						<br>

						<form:form action="updatebug" modelAttribute="uploadForm"
							enctype="multipart/form-data" method="POST" role="form"
							id="updateBugForm">
							<div class="content clearfix">
								<fieldset class="body current">
									<div class="row form-group">
										<div class="col-lg-6">
											<div class="form-group">
												<label class="control-lable col-lg-2">Bug ID:</label>
												<div class="col-md-8">
													<span><strong>${Model.bugDetails[0].bug_prefix}</strong></span>
												</div>

											</div>
										</div>




										<div class="col-lg-6">
											<div class="form-group">
												<!-- 										<label class="control-lable col-lg-2">Jira Id:</label> -->
												<div class="col-md-8">
													<!--  <span><strong>${Model.bugDetails[0].jira_id}</strong></span> -->

													<input type="text" class="hidden"
														value="${Model.bugDetails[0].jira_id}" name="jiraId">
												</div>

											</div>
										</div>


										<div class="col-lg-6">
											<div class="form-group">

												<input type="text" class="hidden"
													value="${Model.bugDetails[0].bug_title}" name="bugTitle">
												<%-- <input type="text" class="hidden" value="${Model.bugDetails[0].bug_desc}" name="bugDesc"> --%>
												<textarea placeholder="Description" name="bugDesc"
													class="form-control hidden" style="border: 1px solid #ccc;">${Model.bugDetails[0].bug_desc}</textarea>
												<input type="text" class="hidden"
													value="${Model.summaryDetails[0].bug_id}" name="bugId">
												<input type="text" class="hidden"
													value="${Model.summaryDetails[0].bug_status}"
													name="prevStatus" id="prevStatus"> <input
													type="text" class="hidden"
													value="${Model.summaryDetails[0].bug_priority}"
													name="prevPriority" id="prevPriority"> <input
													type="text" class="hidden"
													value="${Model.summaryDetails[0].bug_severity}"
													name="prevSeverity" id="prevSeverity"> <input
													type="text" class="hidden"
													value="${Model.summaryDetails[0].category}"
													name="prevCategory" id="prevCategory"> <input
													type="text" class="hidden"
													value="${Model.summaryDetails[0].assignee}"
													name="prevAssignee" id="prevAssignee"> <input
													type="text" class="hidden"
													value="${Model.summaryDetails[0].is_build_assigned}"
													name="prevBuildType" id="prevBuildType">
												<c:if test="${Model.summaryDetails[0].is_build_assigned==1}">
													<input type="text" class="hidden"
														value="${Model.summaryDetails[0].manual_build}"
														name="prevBuildId">
												</c:if>
												<c:if test="${Model.summaryDetails[0].is_build_assigned==2}">
													<input type="text" class="hidden"
														value="${Model.summaryDetails[0].automation_build}"
														name="prevBuildId">
												</c:if>
												<label class="control-lable col-lg-2">Status:</label>
												<div class="col-lg-8">

													<c:set var="statusId"
														value="${Model.summaryDetails[0].bug_status}" />

													<input type="text" class="hidden"
														value="${Model.summaryDetails[0].currentStatus}"
														name="currentBug">

													<%-- 												   <c:out value="${statusId}"/> --%>
													<select data-placeholder="Choose a Status"
														class="chosen-select " name="status" id="status">
														<%-- 												<c:forEach var="status" items="${Model.statusList}"> --%>
														<%-- 													<option value="${status.status_id}" --%>
														<%-- 														${status.status_id == Model.summaryDetails[0].bug_status ? 'selected="selected"' : ''}>${status.bug_status}</option> --%>
														<%-- 												</c:forEach> --%>

														<c:if test="${statusId == 1}">
															<option value="1">New</option>
															<option value="3">In Progress</option>
															<option value="4">Resolved</option>
															<option value="5">Closed</option>
															<option value="7">Open</option>
														</c:if>

														<c:if test="${statusId == 7}">
															<option value="7">Open</option>
															<option value="3">In Progress</option>
															<option value="4">Resolved</option>
															<option value="5">Closed</option>
														</c:if>

														<c:if test="${statusId == 3}">
															<option value="3">In Progress</option>
															<option value="4">Resolved</option>
															<option value="5">Closed</option>
															<option value="7">Open</option>

														</c:if>

														<c:if test="${statusId == 4}">
															<option value="4">Resolved</option>
															<option value="5">Closed</option>
															<option value="8">Reopened</option>
														</c:if>

														<c:if test="${statusId == 8}">
															<option value="8">Reopened</option>
															<option value="5">Closed</option>
															<option value="3">In Progress</option>

														</c:if>

														<c:if test="${statusId == 5}">
															<option value="5">Closed</option>

														</c:if>


													</select>
												</div>

											</div>
											<!-- end form-group -->
										</div>
									</div>
									<div class="row form-group">

										<div class="col-lg-6">
											<div class="form-group">
												<label class="control-lable col-lg-2">Priority:</label>
												<div class="col-lg-8">
													<select data-placeholder="Choose a Priority"
														class="chosen-select " name="priority" id="priority">
														<c:forEach var="priority" items="${Model.priorityList}">
															<option value="${priority.priority_id}"
																${priority.priority_id == Model.summaryDetails[0].bug_priority ? 'selected="selected"' : ''}>${priority.bug_priority}</option>
														</c:forEach>
													</select>

													<!-- 											<input type="text"  -->
													<!-- 												value="" id ="updatePriority" name="updatePriority"> -->

												</div>
											</div>
											<!-- end form-group -->
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label class="control-lable col-lg-2">Severity:</label>
												<div class="col-lg-8">
													<select data-placeholder="Choose a Severity"
														class="chosen-select" name="severity" id="severity">
														<c:forEach var="severity" items="${Model.severityList}">
															<option value="${severity.severity_id}"
																${severity.severity_id == Model.summaryDetails[0].bug_severity ? 'selected="selected"' : ''}>${severity.bug_severity}</option>
														</c:forEach>
													</select>
												</div>
											</div>
											<!-- end form-group -->
										</div>
									</div>

									<!-- end row -->
									<div class="row form-group">
										<div class="col-lg-6">
											<div class="form-group">
												<label class="control-lable col-lg-2">Assignee:</label>
												<div class="col-lg-8">


													<select data-placeholder="Choose a Assignee" id="assignee"
														class="chosen-select" name="assignee">
														<c:forEach var="user" items="${Model.userList}">
															<option value="USR${user.user_id}"
																${user.user_id == Model.summaryDetails[0].assignee ? 'selected="selected"' : ''}>${user.first_name} ${user.last_name}</option>
															<option value="GRP${user.bug_group_id}"
																${user.bug_group_name == Model.summaryDetails[0].bug_assignee_name ? 'selected="selected"' : ''}>${user.bug_group_name}</option>

														</c:forEach>
														<option value="-1"
															${'None' == Model.summaryDetails[0].bug_assignee_name ? 'selected="selected"' : ''}>None</option>
													</select> <input type="text" class="hidden" value=""
														id="assigneeName" name="assigneeName">
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label class="control-lable col-lg-2">Reporter:</label>
												<div class="col-lg-8">
													<span><b>${Model.bugDetails[0].bug_author_name}</b></span>
												</div>
											</div>
										</div>
									</div>


									<!-- end row -->
									<div class="row form-group">
										<div class="col-lg-6">
											<div class="form-group">
												<label class="control-lable col-lg-2">Project:</label>
												<div class="col-lg-8">
													<span><strong>${Model.bugDetails[0].project_name}</strong></span>

												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label class="control-lable col-lg-2">Module:</label>
												<div class="col-lg-8">
													<span><strong>${Model.bugDetails[0].module_name}</strong></span>


												</div>
											</div>
										</div>
									</div>

									<!-- end row -->
									<div class="row form-group">
										<div class="col-lg-6">
											<div class="form-group">
												<label class="control-lable col-lg-2">Category:</label>
												<div class="col-lg-8">
													<select data-placeholder="Choose a Category"
														class="chosen-select" name="categoryId" id="categoryId">
														<c:forEach var="categoryList"
															items="${Model.categoryList}">
															<option value="${categoryList.category_id}"
																${categoryList.category_id == Model.summaryDetails[0].category ? 'selected="selected"' : ''}>${categoryList.category_name}</option>
														</c:forEach>
													</select>
												</div>
											</div>
											<!-- end form-group -->


										</div>

									</div>




									<div class="row form-group">
										<c:if test="${Model.summaryDetails[0].is_build_assigned!=3}">
											<div class="col-lg-6 hidden">
												<div class="form-group">
													<label class="control-lable col-lg-2">Build:</label>
													<div class="col-lg-8">
														<select data-placeholder="Choose a Build"
															class="chosen-select" name="buildId" id="buildId">
															<c:choose>
																<c:when
																	test="${Model.summaryDetails[0].is_build_assigned==1}">
																	<c:forEach var="buildList" items="${Model.buildList}">
																		<option value="${buildList.build_id}"
																			${buildList.build_id == Model.summaryDetails[0].manual_build ? 'selected="selected"' : ''}>${buildList.build_name}</option>
																	</c:forEach>
																</c:when>
																<c:when
																	test="${Model.summaryDetails[0].is_build_assigned==2}">
																	<c:forEach var="buildList" items="${Model.buildList}">
																		<option value="${buildList.build_id}"
																			${buildList.build_id == Model.summaryDetails[0].automation_build ? 'selected="selected"' : ''}>${buildList.build_name}</option>
																	</c:forEach>
																</c:when>
															</c:choose>
														</select>
													</div>
												</div>
												<!-- end form-group -->
											</div>
										</c:if>
									</div>

									<div class="row form-group">
										<div class="form-group">
											<div class="col-lg-1">
												<label class="control-lable" style="padding-left: 15px;">Comment:*</label>
											</div>
											<div class="col-lg-7">
												<textarea name="comment" id="comment"
													class="form-control characters summernote"
													placeholder="Comment"></textarea>
												<!-- rows="5" style="max-width: 100%; max-height: 100px; margin-left: 6.4%" -->
												<!-- <label id="bugComment-error" class="error hidden">This field is required.</label> -->
												<label id="bugCommentError" class="error"
													style="display: none; margin-left: 11px"></label>
											</div>
										</div>

									</div>

									<div class="row">
										<label class="col-lg-1 control-lable"
											style="padding-left: 15px; text-align: right">File:*</label>
										<div class="col-lg-11 form-group" style="float: left">
											<input id="btnAddAttachment" type="button"
												class="btn btn-sm btn-success" style="margin-left: 10px;"
												value="Attach File" /> <label class="">Supported
												file types png, jpg, pdf, docx</label>
											<table id="tblAttachment">
											</table>
										</div>
										<!-- end form-group -->
									</div>
									<!-- end row -->
								</fieldset>
								<!-- end fieldset -->
							</div>
							<!-- end .content .clearfix -->
							<div class="actions clearfix hidden" id="updateActionDiv">
								<div class="row">
									<div class="col-md-4">
										<button class="btn btn-white pull-left"
											style="margin-right: 15px" type="button" id="btnCancel">Cancel</button>
										<button class="btn btn-success pull-left ladda-button"
											type="button" id="btnUpdateBug" data-style="slide-up">Submit</button>
									</div>
								</div>
							</div>
							<!-- end .actions .clearfix -->
						</form:form>
					</div>
					<!-- end #bugUpdateFormDiv -->
					<div class="ibox-content" id="bugDetailsDiv">
						<c:set var="schemaName" value="${Model.schema}" />
						<div class="row">
							<div class="col-sm-7">
								<h2>${Model.bugDetails[0].bug_title}:
									${Model.bugDetails[0].bug_prefix}</h2>
							</div>
							<div class="col-sm-5">


								<!-- 							<button type="button" class="btn btn-white btn-xs pull-right" -->
								<%-- 								style="margin-right: 10px" onclick="removeBug(${Model.summaryDetails[0].bug_id})">Delete</button> --%>

								<c:choose>
									<c:when test="${schemaName == 'XenonAnmol01172019020400'}">
										<button type="button" disabled
											class="btn btn-white btn-xs pull-right"
											style="margin-right: 10px" id="editBugBtn">Edit bug</button>
									</c:when>
									<c:otherwise>
										<button type="button" class="btn btn-white btn-xs pull-right"
											style="margin-right: 10px" id="editBugBtn">Edit bug</button>
									</c:otherwise>
								</c:choose>


								<button type="button" class="btn btn-success btn-xs pull-right"
									style="margin-right: 10px" id="addCommentBtn">Add
									Comment</button>
							</div>
						</div>
						<hr>
						<!-- end row -->
						<div class="row">
							<div class="col-lg-5">
								<div class="dl-horizontal">
									<dt class="horizonatl-lable">Bug ID:</dt>
									<dd>${Model.bugDetails[0].bug_prefix}</dd>
									<dt class="horizonatl-lable">Priority:</dt>
									<dd>${Model.summaryDetails[0].currentPriority}</dd>
									<dt class="horizonatl-lable">Assignee:</dt>

									<c:if test="${Model.bugDetails[0].assign_status==2}">
										<dd>None</dd>
									</c:if>

									<c:if test="${Model.bugDetails[0].assign_status==1}">
										<c:if test="${Model.bugDetails[0].group_status==1}">
											<dd>${Model.bugDetails[0].bug_groupName}</dd>
										</c:if>
										<c:if test="${Model.bugDetails[0].group_status==2}">
											<dd>${Model.summaryDetails[0].bug_assignee_name}</dd>
										</c:if>
									</c:if>

									<dt class="horizonatl-lable">Created Date:</dt>
									<dd>${Model.bugDetails[0].create_date}</dd>
									<dt class="horizonatl-lable">Project:</dt>
									<dd>${Model.bugDetails[0].project_name}</dd>
									<dt class="horizonatl-lable">Category:</dt>
									<dd>${Model.summaryDetails[0].currentCategory}</dd>
									<%--<dt class="horizonatl-lable">Build Type:</dt>
								 <c:if test="${Model.summaryDetails[0].is_build_assigned==1 }">
									<dd>Manual</dd>
								</c:if>
								<c:if test="${Model.summaryDetails[0].is_build_assigned==2 }">
									<dd>Automation</dd>
								</c:if>
								<c:if test="${Model.summaryDetails[0].is_build_assigned==3 }">
									<dd>None</dd>
								</c:if>
								<c:if test="${Model.summaryDetails[0].is_build_assigned!=3 }">
									<dt class="horizonatl-lable">Build Name:</dt>
									<c:if test="${Model.summaryDetails[0].is_build_assigned==1 }">
									<c:forEach var="buildList" items="${Model.buildList}">
									
									<c:if test="${buildList.build_id == Model.summaryDetails[0].manual_build}">
												<dd>	${buildList.build_name}</dd></c:if>
												</c:forEach>
									</c:if>
									<c:if test="${Model.summaryDetails[0].is_build_assigned==2 }">
									<c:forEach var="buildList" items="${Model.buildList}">
									
									<c:if test="${buildList.build_id == Model.summaryDetails[0].automation_build}">
												<dd>	${buildList.build_name}</dd></c:if>
												</c:forEach>
									</c:if>
									
									</c:if> --%>

								</div>
								<!-- end .dl-horizontal -->
							</div>

							<!-- end col -->
							<div class="">
								<div class="col-lg-7">
									<div class="dl-horizontal">
										<dt class="horizonatl-lable">Status:</dt>
										<dd>
											<span class="label td_bugStatus" style="color: #FFFFFF">${Model.summaryDetails[0].currentStatus}</span>
										</dd>
										<dt class="horizonatl-lable">Severity:</dt>
										<dd>${Model.summaryDetails[0].currentSeverity}</dd>
										<dt class="horizonatl-lable">Reporter:</dt>
										<dd>${Model.bugDetails[0].bug_author_name}</dd>
										<dt class="horizonatl-lable">Updated Date:</dt>
										<dd>${Model.summaryDetails[0].update_date}</dd>
										<dt class="horizonatl-lable">Module:</dt>
										<dd>${Model.bugDetails[0].module_name}</dd>


									</div>
								</div>
								<!-- end .dl-horizontal -->
							</div>

							<!-- end col -->
						</div>
						<hr>
					</div>
					<!-- end #bugDetailsDiv -->
					<!-- <hr> -->
					<div class="row">
						<div class="col-lg-12">
							<!-- <div class="ibox"> -->
							<div class="ibox-title">
								<h5>Bug Description</h5>
							</div>
							<div class="ibox-content">
								<div>
									<p>${Model.bugDetails[0].bug_desc}</p>
								</div>

								<div style="margin-top: 15px;">
									<ol>
										<c:forEach var="attachment" items="${Model.bugAttachDetails}"
											varStatus="loopCounter">
											<div class="row">
												<strong class="col-lg-6">${attachment.attach_title }</strong>
												<div class="btn-group col-lg-6">
													<button class="btn-white btn btn-xs"
														style="margin-right: 10px;"
														onclick="viewFile(${attachment.attach_id},'${attachment.attach_title}');">View
													</button>
													<button class="btn btn-white btn-xs"
														onclick="downloadFile(${attachment.attach_id},'${attachment.attach_title}');">Download</button>
												</div>
											</div>
										</c:forEach>
									</ol>
								</div>
								<div class="row hidden" style="margin-top: 15px;"
									id="addCommentDiv">
									<div class="chat-form col-sm-11">
										<form:form action="addcomment" modelAttribute="uploadForm"
											enctype="multipart/form-data" method="POST" role="form"
											id="commentForm">
											<div class="form-group ">
												<textarea class="form-control characters summernote"
													id="txtComment" name="comment" placeholder="Comment"
													required="required" rows="5"
													style="max-width: 100%; max-height: 100px"></textarea>

												<label id="addCommentError" class="error"
													style="display: none; margin-left: 11px"></label> <input
													type="text" class="hidden"
													value="${Model.summaryDetails[0].bug_id}" name="bugId">
												<input type="text" class="hidden"
													value="${Model.summaryDetails[0].bug_severity}"
													name="severity" id="severity"> <input type="text"
													class="hidden" value="${Model.summaryDetails[0].category}"
													name="category" id="category"> <input type="text"
													class="hidden"
													value="${Model.summaryDetails[0].bug_status}" name="status">
												<input type="text" class="hidden"
													value="${Model.summaryDetails[0].bug_priority}"
													name="priority"> <input type="text" class="hidden"
													value="${Model.summaryDetails[0].assignee}" name="assignee"
													id="prevAssignee">
											</div>
											<div class="form-group ">
												<button class="btn btn-sm btn-success ladda-button"
													type="button" id="btnAddComment" data-style="slide-up">
													<strong>Submit</strong>
												</button>
												<button class="btn btn-sm btn-white ladda-button"
													type="button" id="btnCancelComment" data-style="slide-up"
													style="margin-right: 10px">
													<strong>Cancel</strong>
												</button>


												<input id="addFile" type="button"
													class="btn btn-sm btn-success m-t-n-xs  pull-right"
													style="margin-right: 10px" value="Attach File" /> <label
													class="pull-right" style="margin-left: 10px;">Supported
													file types png, jpg, pdf, docx </label>
												<table class="pull-right" id="fileTable"
													style="margin-right: 10px;">
												</table>

											</div>
										</form:form>
									</div>
									<div class="col-sm-4"></div>
								</div>
							</div>

							<!-- </div> -->
						</div>
					</div>

					<!-- end row -->
					<div class="row m-t-sm">
						<div class="col-lg-12">
							<!-- <div class="ibox"> -->
							<div class="ibox-content">
								<div class="panel blank-panel">
									<div class="panel-heading">
										<div class="panel-options">
											<ul class="nav nav-tabs">
												<li class="active"><a href="#tab-1" data-toggle="tab"
													aria-expanded="true"> Comments </a></li>
												<li class=""><a href="#tab-2" data-toggle="tab"
													aria-expanded="false"> Activity </a></li>
											</ul>
										</div>
										<!-- end panel-options -->
									</div>

									<!-- end panel heading -->
									<div class="panel-body">
										<div class="tab-content">
											<div class="tab-pane active" id="tab-1">
												<div class="chat-activity-list">
													<div class="text-right">
														<div id="commentsNavDiv"></div>
													</div>
													<c:forEach var="comments" items="${Model.summaryDetails}"
														varStatus="loopCounter">
														<c:if test="${fn:length(comments.comment_desc) > 0}">
															<div class="chat-element">
																<a href="#" class="pull-left"> <img alt="image"
																	class="img-circle"
																	src="data:image/jpg;base64,${comments.updater_logo }">
																</a>
																<div class="media-body">
																	<c:if test="${comments.TimeDiff < 60 }">
																		<strong class="pull-right">${comments.TimeDiff}
																			min ago</strong>
																	</c:if>

																	<c:if
																		test="${comments.TimeDiff > 60 && comments.TimeDiff < 1440 }">
																		<fmt:parseNumber var="hour" integerOnly="true"
																			type="number" value="${comments.TimeDiff/60}" />
																		<strong class="pull-right">${hour} hr ago </strong>
																	</c:if>
																	<c:set var="date"
																		value="${fn:split(comments.update_date,' ')}" />
																	<c:if test="${comments.TimeDiff > 1440}">
																		<strong class="pull-right">${date[0]} </strong>
																	</c:if>
																	<strong>${comments.bug_updated_by}</strong>
																	<p class="m-b-xs">${comments.comment_desc}</p>
																	<c:if test="${comments.update_date == Model.nowDate}">
																		<small class="text-muted"> ${date[1]}
																			${date[2]} </small>
																	</c:if>
																	<c:if test="${comments.update_date != Model.nowDate}">
																		<c:choose>
																			<c:when
																				test="${comments.update_date == Model.prevDate}">
																				<small class="text-muted"> Yesterday -
																					${date[1]} ${date[2]} </small>
																			</c:when>
																			<c:otherwise>
																				<small class="text-muted">
																					${comments.update_date} </small>
																			</c:otherwise>
																		</c:choose>
																	</c:if>

																	<c:forEach var="attachment"
																		items="${Model.summaryAttachment}"
																		varStatus="loopsCounter">
																		<c:if
																			test="${attachment.comment_id == comments.bs_id}">
																			<div class="row">
																				<strong class="col-lg-2">${attachment.attach_title}</strong>
																				<div class="btn-group">
																					<button class="btn-white btn btn-xs"
																						onclick="viewAttachment(${attachment.attach_id},'${attachment.attach_title }');"
																						style="margin-right: 10px;">View</button>
																					<button class="btn btn-white btn-xs"
																						onclick="downloadAttachment(${attachment.attach_id},'${attachment.attach_title }');">Download</button>
																				</div>
																			</div>

																		</c:if>
																	</c:forEach>
																</div>
															</div>
														</c:if>
													</c:forEach>
												</div>
											</div>
											<!-- end #tab-1 -->
											<div class="tab-pane" id="tab-2">
												<table class="table table-striped">
													<thead>
														<tr>
															<th class="hidden">Bug ID</th>
															<th>Activity</th>
															<th>Comment</th>
															<th>Updated By</th>
															<th>Update Date</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="comments" items="${Model.summaryDetails}"
															varStatus="loopCounter">
															<c:if
																test="${!loopCounter.last && (comments.bug_status!= comments.prev_status || comments.bug_priority != comments.prev_priority || 
															comments.bug_severity != comments.prev_severity|| comments.assignee!= comments.prev_assignee ||  comments.category != comments.prev_category  ||		comments.manual_build!=comments.prev_manual_build   ||  comments.automation_build!=comments.prev_automation_build) }">
																<tr>
																	<td class="hidden">${comments.bug_id}</td>
																	<td><span> <c:if
																				test="${comments.bug_status!= comments.prev_status }">
																				<p>
																					Status changed from <strong>${comments.prevStatus}</strong>
																					to <strong>${comments.currentStatus}</strong>
																				</p>
																			</c:if> <c:if
																				test="${comments.bug_priority != comments.prev_priority }">
																				<p>
																					Priority changed from <strong>${comments.prevPriority}</strong>
																					to <strong>${comments.currentPriority}</strong>
																				</p>
																			</c:if> <c:if
																				test="${comments.bug_severity != comments.prev_severity }">
																				<p>
																					Severity changed from <strong>${comments.prevSeverity}</strong>
																					to <strong>${comments.currentSeverity}</strong>
																				</p>
																			</c:if> <c:if
																				test="${comments.category != comments.prev_category }">
																				<p>
																					Category changed from <strong>${comments.prevCategory}</strong>
																					to <strong>${comments.currentCategory}</strong>
																				</p>
																			</c:if> <c:if
																				test="${comments.assignee!= comments.prev_assignee}">
																				<p>
																					Assignee changed from <strong>${comments.prev_assignee_name}</strong>
																					to <strong>${comments.bug_assignee_name}</strong>
																				</p>
																			</c:if> <c:if
																				test="${comments.manual_build!= comments.prev_manual_build }">
																				<p>
																					Build changed from <strong> <c:forEach
																							var="buildList" items="${Model.buildList}">
																							<c:if
																								test="${buildList.build_id == comments.prev_manual_build}">
																					${buildList.build_name}
																					</c:if>
																						</c:forEach></strong> to <strong> <c:forEach var="buildList"
																							items="${Model.buildList}">
																							<c:if
																								test="${buildList.build_id == comments.manual_build}">
																					${buildList.build_name}
																					</c:if>
																						</c:forEach>
																					</strong>
																				</p>
																			</c:if> <c:if
																				test="${comments.automation_build!= comments.prev_automation_build }">
																				<p>
																					Build changed from <strong> <c:forEach
																							var="buildList" items="${Model.buildList}">
																							<c:if
																								test="${buildList.build_id == comments.prev_automation_build}">
																					${buildList.build_name}
																					</c:if>
																						</c:forEach></strong> to <strong> <c:forEach var="buildList"
																							items="${Model.buildList}">
																							<c:if
																								test="${buildList.build_id == comments.automation_build}">
																					${buildList.build_name}
																					</c:if>
																						</c:forEach>
																					</strong>
																				</p>
																			</c:if>
																	</span></td>
																	<td>${comments.comment_desc}</td>
																	<td>${comments.bug_updated_by}</td>
																	<td>${comments.update_date}</td>

																</tr>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
											<!-- end #tab-2 -->
										</div>
										<!-- end tab-content -->
									</div>
									<!-- end panel body -->
								</div>
								<!-- end panel -->
							</div>
							<!-- end col -->
							<!-- </div> -->
						</div>
					</div>
					<!-- </div>  -->
				</div>
				<!-- end ibox-content -->
			</div>
			<!-- end ibox -->
		</div>
	</div>
	<!-- end row -->
</div>
<style>
.horizonatl-lable {
	margin-bottom: 13px;
}
</style>
<input type="hidden" id="hiddenCreateBugStatus"
	value='<%=session.getAttribute("createBugStatus")%>'>
<input type="hidden" value="${Model.bugDetails[0].bug_prefix}"
	id="bugPrefix">

<script>
    
    $( "#assignee" ).change(function() {
    	var temp = $("#assignee option:selected").text();
        //alert(temp);
        
        $("#assigneeName").val(temp);
        
    	});
    
//     $( "#priority" ).change(function() {
//     	var temp1 = $("#priority option:selected").text();
        
//         $("#updatePriority").val(temp1);
        
//     	});
	
	</script>

<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	var page= <%=request.getParameter("page")%>;
	 if(page == null){
		 page = 1;
	 }
	 
	 var allCommentsCount='${Model.allCommentsCount}';
	 
	 var pageSize='${Model.pageSize}';
	 
	 var lastRec=((page-1)*pageSize+parseInt(pageSize));
	 
	 if(lastRec>allCommentsCount)
	 	lastRec=allCommentsCount;
	 var showCount = ((page-1)*pageSize+1);
	 if(showCount <= lastRec){
		 $("#commentsNavDiv").html("Showing "+showCount+" to "+lastRec+" of "+allCommentsCount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="prevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="nextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
	 }else{
		 $("#commentsNavDiv").html("");
	 }
	
	 if(page == 1){
		 $("#prevBtn").attr('disabled','disabled');
	 }
	 
	 if(lastRec == allCommentsCount){
		 $("#nextBtn").attr('disabled','disabled');
	 }
	 
	    $("#prevBtn").click(function() {
	    	$("#barInMenu").removeClass("hidden");
	    	$("#wrapper").addClass("hidden");
	    	
	    	if(page == 1)
	    		window.location.href = "bugsummary";
	    	else
	    		window.location.href = "bugsummary?page="+(page - 1);
		});
		
		$("#nextBtn").click(function(){
			$("#barInMenu").removeClass("hidden");
	    	$("#wrapper").addClass("hidden");
	    	
			window.location.href = "bugsummary?page="+(page + 1);
		});
});
$(document).ready(
		function() {
			
			$('.summernote').summernote();
			//$('.note-editor').css('background','white');
            $('button[ data-event="codeview"]').remove();
            $('.note-editor').css('margin-left','10px');
			//$('.note-editable').html('');
            $('div.note-insert').remove();
            $('div.note-table').remove();
            $('div.note-help').remove();
            $('div.note-style').remove();
            $('div.note-color').remove();
            $('button[ data-event="removeFormat"]').remove();
            $('button[ data-event="insertUnorderedList"]').remove();
            $('button[ data-event="fullscreen"]').remove();
            $('button[ data-original-title="Line Height"]').remove();
            $('button[ data-original-title="Font Family"]').remove();
            $('button[ data-original-title="Paragraph"]').remove();

			
			//toster message
			var state = $('#hiddenCreateBugStatus').val();
			$('#hiddenCreateBugStatus').val(11);
			var newBugID = $('#bugPrefix').val();
			if(state == 1){
				var message =" New Bug is successfully created with bug ID : ";
				var toasterMessage =message+newBugID;
				showToster(toasterMessage);
			}
			else if(state == 2){
				var message ="  Bug is successfully updated ";
				var toasterMessage =newBugID+message;
				showToster(toasterMessage);
			}
			
				$('#btnAddAttachment').click(function() {
					var fileIndex = $('#tblAttachment tr').children().length;
					if(fileIndex>0){
						lastIndex = $('#tblAttachment tr:last').attr("id").length;
						fileIndex = parseInt($('#tblAttachment tr:last').attr("id").substring(15,lastIndex)) + parseInt(1);
					}
					$('#tblAttachment').append(
							'<tr id="attachTableRow_'+ fileIndex +'"><td>'+
							'	<input type="file" accept=".png,.jpg,.pdf,.docx" class="attachFile hidden" id="attachFile_'+ fileIndex +'" />'+
							'<label class="attachFileName" style="font-weight: 400;" id="attachFileName_'+ fileIndex +'"></label>'+
							'<a id="attachClose_'+ fileIndex +'" class="attachClose hidden"  style="float: none">�</a>'+
							'<label class="error hidden" id="attachSizeError_'+ fileIndex +'"></label>'+
							'<label class="error hidden" id="attachTypeError_'+ fileIndex +'"></label>'+
							'</td></tr>');
					
					$("#attachFile_"+fileIndex).click();
					$("#attachFile_"+fileIndex).change(function(){
						if(this.files[0] ==null || this.files[0] == undefined)
							{
							$("#attachFileName_"+fileIndex).html("");
							$('#attachTableRow_'+ fileIndex ).remove();
							}
						else
							{
							$("#attachFileName_"+fileIndex).html(this.files[0].name);
							$("#attachClose_"+fileIndex).removeClass('hidden');
							$('.attachClose').each(function()
							{
								if($(this).hasClass('hidden'))
									{
									lastIndex = $(this).attr('id').length;
									id = $(this).attr('id').substring(12,lastIndex);
							        $('#attachTableRow_'+ id ).remove();
									}
							}); 
							} 
					});
					$(".attachClose").click(function(){
						lastIndex = $(this).attr('id').length;
						id =  $(this).attr('id').substring(12,lastIndex);
						$('#attachTableRow_'+ id ).remove();
				    });
					
				});
				
				$('#addFile').click(function() {
					var fileIndex = $('#fileTable tr').children().length;
					if(fileIndex>0){
						lastIndex = $('#fileTable tr:last').attr("id").length;
						fileIndex = parseInt($('#fileTable tr:last').attr("id").substring(9,lastIndex)) + parseInt(1);
					}
					$('#fileTable').append(
							'<tr id="tableRow_'+ fileIndex +'"><td>'+
							'	<input type="file"accept=".png,.jpg,.pdf,.docx"  class="file hidden" id="file_'+ fileIndex +'" />'+
							'<label class="fileName" style="font-weight:400;" id="fileName_'+ fileIndex +'"></label>'+
							'<a id="close_'+ fileIndex +'" class="close hidden"  style="float: none">�</a>'+
							'<label class="error hidden" id="uploadSizeError_'+ fileIndex +'"></label>'+
							'<label class="error hidden" id="uploadTypeError_'+ fileIndex +'"></label>'+
							'</td></tr>');
					
					$("#file_"+fileIndex).click();
					$("#file_"+fileIndex).change(function(){
						
					
						if(this.files[0] ==null || this.files[0] == undefined)
							{
							$("#fileName_"+fileIndex).html("");
							$('#tableRow_'+ fileIndex ).remove();
							}
						else
							{
							$("#fileName_"+fileIndex).html(this.files[0].name);
							$("#close_"+fileIndex).removeClass('hidden');
							$('.close').each(function()
							{
								if($(this).hasClass('hidden'))
									{
									lastIndex = $(this).attr('id').length;
									id = $(this).attr('id').substring(5,lastIndex);
							        $('#tableRow'+ id ).remove();
									}
							}); 
							} 
					});
						
					$(".close").click(function(){
						lastIndex = $(this).attr('id').length;
						id =  $(this).attr('id').substring(6,lastIndex);
						$('#tableRow_'+ id ).remove();
				    });
					
				});
				$('.td_bugStatus').each(function() {
		            var stat=$(this).text();
		            if(stat=='New')
		            {
		            	$(this).css("background-color","${Model.New}");
		            }
		            else if(stat=='Assigned')
		            {
		            	$(this).css("background-color","${Model.Assigned}");
		            }
		            else if(stat=='Dev in Progress')
		            {
		            	$(this).css("background-color","${Model.Fixed}");
		            }
		            else if(stat=='Ready for QA')
		            {
		            	$(this).css("background-color","${Model.Verified}");
		            }
		            else if(stat=='Closed')
		            {
		            	$(this).css("background-color","${Model.Closed}");
		            }
		            else if(stat=='Reject')
		            {
		            	$(this).css("background-color","${Model.Reject}");
		            }
		           
		      });
		});
	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});
	$('#editBugBtn').click(function() {
		$('#bugUpdateFormDiv').removeClass('hidden');
		$('#bugDetailsDiv').addClass('hidden');
		$('#updateActionDiv').removeClass('hidden');
		var config = {
			'.chosen-select' : {},
			'.chosen-select-deselect' : {
				allow_single_deselect : true
			},
			'.chosen-select-no-single' : {
				disable_search_threshold : 10
			},
			'.chosen-select-no-results' : {
				no_results_text : 'Oops, nothing found!'
			},
			'.chosen-select-width' : {
				width : "95%"
			}
		}
		for ( var selector in config) {
			$(selector).chosen(config[selector]);
		}
	});
	
	$('#btnCancelComment').click(function() {
		window.location.reload();
});
	$('#addCommentBtn').click(function() {
		 	$('#addCommentDiv').removeClass('hidden');
		 	$("#txtComment").attr("autofocus",true);
			 $('html, body').animate({
			        scrollTop: $("#addCommentDiv").offset().top
			    }, 1000);
			$(this).hide();
	});
	$('#btnCancel').click(function() {
		window.location.reload();
	});
	$
	
	$('#updateBugForm').validate({
		rules : {
			comment : {
				required : true,
				minlength : 1,
				maxlength : 4000,
				characters : true
			}
		}
	});
	
	$('#commentForm').validate({
		rules : {
			txtComment : {
				required : true,
				minlength : 1,
				maxlength : 4000
			}
		}
	});
	function downloadFile(attchmentId,title)
	{
		var posting = $.post('getbugattchment', {
			 attchmentId : attchmentId,
			});
			 posting.done(function(data) {
				
				  var fileNameExt = title.substr(title.lastIndexOf('.') + 1);
				 var a = window.document.createElement('a');
				 link =  "data:"+fileNameExt+";base64,"+data;
				 a.href=link;
				 a.download = title;
				 document.body.appendChild(a)
				 a.click();
				 document.body.removeChild(a)
				
			});
			
	}
	function viewFile(attchmentId,title)
	{
		var posting = $.post('getbugattchment', {
			 attchmentId : attchmentId,
			});
			 posting.done(function(data) {
				 let data1 = 'data:image/png;base64,'+data;
				 let w = window.open('about:blank');
				 let image = new Image();
				 image.src = data1;
				 setTimeout(function(){
				   w.document.write(image.outerHTML);
				 }, 0);
			});
			
	}
	function downloadAttachment(attchmentId,title)
	{
		
		var posting = $.post('getcommentattchment', {
			 attchmentId : attchmentId,
			});
			 posting.done(function(data) {
				 
				 
				 var fileNameExt = title.substr(title.lastIndexOf('.') + 1);
				 var a = window.document.createElement('a');
				 link =  "data:"+fileNameExt+";base64,"+data;
				 a.href=link;
				 a.download = title;
				 document.body.appendChild(a)
				 a.click();
				 document.body.removeChild(a)
			});
			
	}
	function viewAttachment(attchmentId,title)
	{
		
		var posting = $.post('getcommentattchment', {
			 attchmentId : attchmentId,
			});
			 posting.done(function(data) {
				 var fileNameExt = title.substr(title.lastIndexOf('.') + 1);
				 var a = window.document.createElement('a');
				 var dataType;
				 if(fileNameExt.toLowerCase() == "jpg" || fileNameExt.toLowerCase() == "png")
					   dataType = "image/"+fileNameExt;
					 else if(fileNameExt.toLowerCase() == "pdf")
						dataType = "application/"+fileNameExt;
					 else if(fileNameExt.toLowerCase() == "docx")
						 dataType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
				 link =  "data:"+dataType+";base64,"+data;
				 a.href=link;
				 a.target = "_blank";
				 document.body.appendChild(a)
				 a.click();
				 document.body.removeChild(a)
			});
			
	}
	
	 //var la = $('#btnAddComment').ladda();
	 $('#btnAddComment').click(function() {
		// la.ladda('start');
		var commentstr=$('.note-editable').text();
		$('.note-editable').text(commentstr.trim());
		var formValid = $("#commentForm").valid();
		
		 var statusUpload = 1;
		var counter = 0;
		$('.file').each(function(){
			
				lastIndex = $(this).attr('id').length;
				id =  $(this).attr('id').substring(5,lastIndex);
			  
			   var fsize = 0;
				var filetype = 1;
				if($(this)[0].files[0]){
					
					file = $(this)[0].files[0];
			       fsize = file.size;
			       var fileName = file.name;
		          var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
		          if(!(fileNameExt.toLowerCase() == "jpg" || fileNameExt.toLowerCase() == "png" || fileNameExt.toLowerCase() == "docx" || fileNameExt.toLowerCase() == "txt" || fileNameExt.toLowerCase() == "pdf"))
		        	  {
		        	  filetype = 0;
		        	  statusUpload = 0;
		        	  }
		          $(this).attr( "name","files["+ counter +"]");
					counter++;
				}
				else
					{
					$(this).attr('name','');
					}
				if(filetype == 0){
					statusUpload = 0;
					$("#uploadTypeError_"+id).text("Please upload file with one of type (png, jpg, pdf, docx)");
					$("#uploadTypeError_"+id).removeClass('hidden');
					$("#uploadTypeError_"+id).css('display','');
				}
				else if(fsize> 1048576)
					{
					$("#uploadSizeError_"+id).text("Please upload file with size less than 1MB");
					statusUpload = 0;
					$("#uploadSizeError_"+id).removeClass('hidden');
					$("#uploadSizeError_"+id).css('display','');
					}
				
		});
		
		var textValue = $('.note-editable').text();
		
		if(textValue.length == 0) {
			formValid = false;
			$('.note-editor').addClass('errorBorder');
			$('#addCommentError').text('This field is required.');
			$('#addCommentError').show();
		}else {
			$('.note-editor').removeClass('errorBorder');
			$('#addCommentError').hide();
		}
		
		 if(statusUpload == 1 && formValid ==true ){
			 $('body').addClass("white-bg");
			 $("#barInMenu").removeClass("hidden");
			 $("#wrapper").addClass("hidden");
			 $('.summernote').each( function() {
				 $(this).val($(this).code());
			 });
		     $("#commentForm").submit();
		}
	});
  
   //var l = $('#btnUpdateBug').ladda();
	$('#btnUpdateBug').click(function() {
		
		var commentstr=$('.note-editable').text();
		$('.note-editable').text(commentstr.trim());
		
		var formValid = $("#updateBugForm").valid();
		var statusUpload = 1;
		var counter = 0;
		$('.attachFile').each(function(){
			lastIndex = $(this).attr('id').length;
			id =  $(this).attr('id').substring(11,lastIndex);
			   var fsize = 0;
				var filetype = 1;
				if($(this)[0].files[0]){
					file = $(this)[0].files[0];
			       fsize = file.size;
			       var fileName = file.name;
		          var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
		          if(!(fileNameExt.toLowerCase() == "jpg" || fileNameExt.toLowerCase() == "png" || fileNameExt.toLowerCase() == "docx" || fileNameExt.toLowerCase() == "txt" || fileNameExt.toLowerCase() == "pdf"))
		        	  {
		        	  filetype = 0;
		        	  statusUpload = 0;
		        	  }
		          $(this).attr( "name","files["+ counter +"]");
					counter++;
				}
				else
					{
					$(this).attr('name','');
					}
				if(filetype == 0){
					statusUpload = 0;
					$("#attachTypeError_"+id).text("Please upload file with one of type (png, jpg, pdf, docx)");
					$("#attachTypeError_"+id).removeClass('hidden');
					$("#attachTypeError_"+id).css('display','');
				}
				else if(fsize> 1048576)
					{
					statusUpload = 0;
					$("#attachSizeError_"+id).text("Please upload file with size less than 1MB");
					$("#attachSizeError_"+id).removeClass('hidden');
					$("#attachSizeError_"+id).css('display','');
					}
		});
		
		var textValue = $('.note-editable').text();
		
		if(textValue.length == 0) {
			formValid = false;
			$('.note-editor').addClass('errorBorder');
			$('#bugCommentError').text('This field is required.');
			$('#bugCommentError').show();
		}else {
			$('.note-editor').removeClass('errorBorder');
			$('#bugCommentError').hide();
		}

		if(statusUpload == 1 && formValid == true){
			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");
			
			$('.summernote').each( function() {
				$(this).val($(this).code());
			});
			
			$("#updateBugForm").submit();
		}
	});
	
	//toster
	  function showToster(toasterMessage){
		 toastr.options = {
				 "closeButton": true,
				  "debug": true,
				  "progressBar": true,
				  "preventDuplicates": true,
				  "positionClass": "toast-top-right",
				  "showDuration": "400",
				  "hideDuration": "1000",
				  "timeOut": "9000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "slideDown",
				  "hideMethod": "slideUp"
          };
          toastr.success('',toasterMessage);
	} 
	
	  function removeBug(bug_id){
			
			swal({
				title : "Are you sure?",
				text : "Do you want to remove bug!",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "Yes, Remove it!",
				cancelButtonText : "No, cancel!",
				closeOnConfirm : true,
				closeOnCancel : true
			}, function(isConfirm) {

				if (isConfirm) {
					
					$('body').addClass("white-bg");
					$("#barInMenu").removeClass("hidden");
					$("#wrapper").addClass("hidden");
					
					var posting = $.post('removebug',{
						bugId : bug_id
					});
					
					posting.done(function(data){
						window.location.href = "viewallbugs";
					}); 	
				}
			});
			
		}
	
</script>

<%
	//set session variable to another value
	session.setAttribute("createBugStatus", 11);
%>
<style>
.md-skin .ibox {
	margin-bottom: 50px;
}

#bugUpdateFormDiv hr, #bugDetailsDiv hr {
	margin-top: 10px;
	margin-bottom: 20px;
}
</style>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Pie Chart Report</h2>
			<ol class="breadcrumb">
				<li><a href="bmdashboard">Dashboard</a></li>
				<li class="active"><strong>Pie Chart Report</strong></li>
			</ol>
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row m-b-lg">
			<div class="col-lg-9">
				<div class="row">
					
						<div class="ibox">
							<div class="ibox-content">
								<form action="" id="tableForm" method="post">
									<table class="table" id="queryTable"
										style="padding: 5px; vertical-align: middle;">
										<thead>
											<th style="width: 5%"></th>
											<th style="width: 5%"></th>
											<th style="width: 10%"></th>
											<th style="width: 15%"></th>
											<th style="width: 10%"></th>

										</thead>
										<tbody>
											<tr class="currentRow">
												<!-- 	<td class="ParentOp">
												<label class="ParentOperator" style="font-size: 14px;"></label>
											</td> -->
												<td class="childOp"><label class="operatorValue hidden"
													style="font-size: 11px;"></label></td>
												<td class="queryParameter ">
													<div class="">
														<label class="source font-weight-bold h4"
															style="vertical-align: middle;">Source</label>
													</div>
												</td>
												<td class="Condition">
													<div class="">
														<select data-placeholder="choose a value"
															class="chosen-select " name="SourceCondition">
															<option value="is" class="other">IS</option>
															<!-- <option value="isnot" class="other">IS NOT</option>
														<option value="in" class="other">IN</option>
														<option value="notin" class="other">NOT IN</option>
														<option value="any" class="other">ANY</option>
														<option value="contains" class="other">CONTAINS</option> -->

														</select>
													</div>
												</td>
												<td class="selectedResult">

													<div class="SourceData">
														<select data-placeholder="choose a value"
															class="chosen-select selectedOption" name="sourceResult"
															style="width: 100%;">
															<option value="status" class="other">status</option>
															<option value="priority" class="other">priority</option>
															<option value="severity" class="other">severity</option>

														</select> <label class="fieldError text-danger hidden">This
															field is required.</label>
													</div>
												</td>
												<td class="button-group"></td>
											</tr>
											<tr class="data-wrapper currentRow">
												<!-- <td class="ParentOp">
												<label class="ParentOperator" style="font-size: 14px;"></label>
											</td> -->
												<td class="childOp"><label class="operatorValue hidden"
													style="font-size: 11px;"></label></td>
												<td class="queryParameter ">
													<div class="">
														<select data-placeholder="choose a value"
															id="selectCondition_1" onchange="getSelectedvalue(this)"
															class="chosen-select selectCondition" name="parameter"
															style="width: 100%;">
															<option value="1" class="option">Release</option>
															<option value="2" class="option">Build</option>
															<option value="3" class="option">Project</option>

														</select> <label class="error hidden text-danger"
															style="position: fixed;">Please select unique
															parameter</label>
													</div>
												</td>
												<td class="Condition">
													<div class="">
														<select data-placeholder="choose a value"
															class="chosen-select  selectCondition2" name="Condition"
															id="selectCondition2_1"
															onchange="getConditionValue(this)">
															<option value="is" class="other">IS</option>
															<option value="isnot" class="other">IS NOT</option>
															<option value="in" class="other">IN</option>
															<option value="notin" class="other">NOT IN</option>
															<option value="any" class="other">ANY</option>
															<option value="contains" class="other">CONTAINS</option>

														</select>
													</div>
												</td>
												<td class="selectedResult">
													<div class="form-group">
														<div class="content1 ErrorStatus form-horizontal"
															id="div1_1">
															<div class="input-group m-b">
																<input
																	class="releaseText form-control inputText selectedOption"
																	style="" disabled> <span
																	class="input-group-addon success"><i
																	class="popup glyphicon glyphicon-search"
																	onclick="showPopup(this)"></i></span>
															</div>

															<div>
																<label class="fieldError text-danger pull-bottom hidden">This
																	field is required.</label>
															</div>
														</div>

														<div class="content2 ErrorStatus hidden " id="div2_1">
															<div class="col-lg-3 pull-left">
																<select data-placeholder="choose a value "
																	class="chosen-select buildType " name="buildType"
																	onchange=selectBuildType(this); id="dropdown2"
																	style="width: 100%;">
																	<c:forEach var="buildExeType"
																		items="${Model.buildExeType}">
																		<option value="${buildExeType.execution_type_id}">${buildExeType.description}</option>
																	</c:forEach>
																</select>
															</div>
															<div class="input-group m-b">

																<input class="buildText form-control inputText" value=""
																	style="" disabled> <span
																	class="input-group-addon success"><i
																	class=" glyphicon glyphicon-search buildPopup"
																	onclick="buildPopUp(this)"></i></span>
															</div>

															<div>
																<label class="fieldError text-danger pull-bottom hidden">This
																	field is required.</label>
															</div>
														</div>

														<div class="content3 ErrorStatus hidden" id="div3_1">
															<select data-placeholder="choose a value "
																class="chosen-select project" name="project"
																id="dropdown3" style="width: 100%;" multiple>
																<c:forEach var="projectList"
																	items="${Model.projectList}">
																	<option value="${projectList.project_id}">${projectList.project_name}</option>
																</c:forEach>
															</select> <label class="fieldError text-danger hidden">This
																field is required.</label>
														</div>
													</div>

												</td>
												<td class="button-group"><a href="javascript:void(0);"
													class=" add-btn btn btn-success btn-xs addRow"
													onclick="addRow(this)">AND</a> <a
													href="javascript:void(0);"
													class=" add-btn btn btn-success  btn-xs addRow"
													onclick="addRow(this)">OR</a> <a
													class="glyphicon glyphicon-minus btn-danger btn-xs"
													type="button" onClick="removeRow(this);"
													style="height: 23px;"></a> <!--  button class="glyphicon glyphicon-minus btn-success btn-sm"type="button" onClick="removeRow(this);"></button-->
												</td>



											</tr>


										</tbody>
									</table>
									<div>
										<input id="jsonArray" class="hidden" name="jsonArray">
									</div>
									<div>
										<input class="hidden form-group" id="queryName"
											name="queryName"> <input class="hidden form-group"
											id="queryId" name="queryId">
									</div>
								</form>
								<button class="btn btn-success" id="submitButton"
									onclick="saveQuery()">Save</button>
								<button class="btn btn-success hidden" onclick="saveQuery()"
									id="createNewBtn" type="button" style="margin-right: 15px;">Create
									New</button>
								<button class="btn btn-success hidden" id="updateQueryBtn"
									type="button">Update</button>
								<button class="btn btn-success hidden" id="editBtn"
									name="editBtn" onclick="editQuery()" type="button"
									style="margin-right: 15px;">Edit</button>
								<button class="btn btn-success" id="executeButton"
									onclick="saveOrExecuteQuery()">Execute</button>

							</div>
						</div>

					
				</div>
			</div>
			
			<div class="col-lg-3">
			
				<div class="ibox">
					<div class="ibox-content">
						<div class="row m-b-sm">
							<div class="col-lg-12">
								<h2>Queries</h2>
							</div>
						</div>
						<ul class="list-group clear-list">
							<c:forEach var="UserSavedQueries"
								items="${Model.UserSavedQueries}">
								<li class="list-group-item"><a
									onclick="displayQueryData('${UserSavedQueries.queryjson}','${UserSavedQueries.queryname}','${UserSavedQueries.idxebt}');">${UserSavedQueries.queryName}</a></li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</div>
			<div class="">
				<div class="col-lg-4  hidden" id="pieChartDiv">
					<canvas class="" id="doughnutChart" height="300px"></canvas>
				</div>
			</div>

		</div>

	</div>
</div>
<div id="releaseModal" class="modal">

	<!-- Modal content -->
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<h3>Please select release</h3>
			</div>
			<div class="modal-body ">
				<div class="full-height-scroll">
					<div class="table-responsive " style="overflow-x: hidden">
						<table id="releaseTable"
							style="display: block; height: 350px; overflow-y: scroll;"
							class="table table-striped table-bordered table-hover">
							<thead>
								<tr>

									<th class="hidden"></th>
									<th>Check Release</th>
									<th>Release Name</th>

								</tr>
							</thead>
							<tbody>
								<c:forEach items="${Model.releaseList}" var="releases">
									<tr class="releaseTR">
										<td class="checkboxTD" style="width: 20%;"><input type="checkbox"
											class="checkbox-success releaseId"
											release="${releases.release_id}"></td>
										<td class="releaseTD hidden" style="width: 20%;"><input type="text"
											value="${releases.release_name}" class=" releaseName hidden" /></td>
										<td style="width: 20%;"><p class="releaseVal">${releases.release_name}</p></td>

									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">

				<button class="closePopup btn-success pull-bottom btn"
					style="margin-right: 10px">Cancel</button>
				<button class="windowClose btn-success pull-bottom btn">OK</button>
			</div>

		</div>
	</div>

</div>
<div id="buildModal" class="modal" style="width: fixed;">

	<!-- Modal content -->
	<div class="modal-dialog" style="width: fixed;">
		<div class="modal-content">
			<div class="modal-header">

				<div class="modal-body">
					<h3>Please select Build</h3>
					<div class="manualbuildTable hidden">
						<div class="table-responsive " style="overflow-x: hidden">
							<table id="manualbuildTable"
								class="table table-striped table-bordered table-hover"
								style="display: block; height: 350px; overflow-y: scroll;">
								<thead>
									<tr>
										<th>Check Build</th>
										<th>Build Name</th>
										<th>Release Name</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${Model.buildList}" var="manual">
										<tr class="manualBuildRow releaseId_${manual.release_id}">
											<td class="checkboxTD" style="width: 20%;">
												<div class="manual">
													<input type="checkbox" class="checkbox-success manualBuild"
														manual="${manual.build_id}">
												</div>

											</td>

											<td class="buildTD" style="width: 20%;">
												<p class="manualBuildVal" buildName="${manual.build_name}">${manual.build_name}</p>
											</td>
											<td class="" style="width: 20%;">
												<p class="release_Id">Release ${manual.release_id}</p>
											</td>

										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="autobuildTable hidden">
						<div class="table-responsive " style="overflow-x: hidden">
							<table id="autobuildTable"
								class="table table-striped table-bordered table-hover"
								style="display: block; height: 350px; overflow-y: scroll;">

								<thead>
									<tr>
										<th>Check Build</th>
										<th>Build Name</th>
										<th>Release Name</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${Model.autoBuildList}" var="autoBuild">
										<tr
											class="autoBuildRow releaseId_${autoBuild.release_id} autoBuildRow1">
											<td class="checkboxTD">
												<div class="autoBuild ">
													<input type="checkbox"
														class="checkbox-success autoBuildcheck"
														auto="${autoBuild.build_id}">
												</div>

											</td>

											<td class="buildTD"><p class="autoBuildVal "
													buildName="${autoBuild.build_name}">${autoBuild.build_name}</p></td>
											<td class="">
												<p class="release_Id">Release ${autoBuild.release_id}</p>
											</td>

										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="closePopup btn-success pull-bottom btn"
						style="margin-right: 10px">Cancel</button>
					<button class="closeBuildWindow btn-success pull-bottom btn">OK</button>

				</div>

			</div>
		</div>

	</div>
</div>
<style>
/* #releaseTable tr td:nth-child(1) {
	width: 20%;
}

#releaseTable tr td:nth-child(2) {
	width: 20%;
}

#releaseTable tr td:nth-child(3) {
	width: 20%;
}

#manualbuildTable tr td:nth-child(1) {
	width: 20%;
}

#manualbuildTable tr td:nth-child(2) {
	width: 20%;
}

#manualbuildTable tr td:nth-child(3) {
	width: 20%;
}

#manualbuildTable tr td:nth-child(4) {
	width: 20%;
}

#autobuildTable tr td:nth-child(1) {
	width: 20%;
}

#autobuildTable tr td:nth-child(2) {
	width: 20%;
}

#autobuildTable tr td:nth-child(3) {
	width: 20%;
} */
</style>
<script>
	var i = 1, rowCount = 1, BuildType, releaseIDList, sourceType, releaseStatus = "is", releaseValue = 2;
	var clone = $("table tr.data-wrapper:first").clone(true);
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");

		$(".chosen-select").chosen().trigger('chosen:updated');
		var json = "${Model.JSonData}";

		if (json.length > 0) {

			displayQueryData(json);
		}

	});

	$(function() {

		var releaseTable = $('#releaseTable').DataTable({

			buttons : [],
			"paging" : true,
			"lengthChange" : true,
			"searching" : true,
			"ordering" : true
		});
		var manualBuildTable = $('#manualbuildTable').DataTable({

			buttons : [],
			"paging" : true,
			"lengthChange" : true,
			"searching" : true,
			"ordering" : true
		});
		var autoBuildTable = $('#autobuildTable').DataTable({

			buttons : [],
			"paging" : true,
			"lengthChange" : true,
			"searching" : true,
			"ordering" : true
		});

	});

	function addRow(currentClass) {
		rowCount++;
		var result = $(currentClass).text();
		//if(result=="AND"){
		var optionValue = $(currentClass).parent('.button-group').parent(
				'.currentRow').find('.selectCondition').val();
		//alert(optionValue);
		//$(currentClass).parent('.button-group').parent('.stepRow').find('.selectCondition').val(3);

		var currentRow = $(currentClass).parent('.button-group').parent(
				'.currentRow');
		$(".chosen-select").chosen().trigger('chosen:updated');
		var ParentRow = $("table tr.data-wrapper").last();

		clone.find('.selectCondition')
				.attr('id', "selectCondition_" + rowCount);
		clone.find('.selectCondition2').attr('id',
				"selectCondition2_" + rowCount);

		var operatorText = $(currentClass).text();
		clone.find('.dropdown1').attr('id', "dropdown1_" + rowCount);
		clone.find('.dropdown2').attr('id', "dropdown2_" + rowCount);
		clone.find('.dropdown3').attr('id', "dropdown3_" + rowCount);

		clone.find('.content1').attr('id', "div1_" + rowCount);
		clone.find('.content2').attr('id', "div2_" + rowCount);
		clone.find('.content3').attr('id', "div3_" + rowCount);

		//clone.find('.selectCondition').val(5);
		clone.find('.operatorValue').removeClass('hidden');
		$(".chosen-select").chosen().trigger('chosen:updated');
		clone.find('.operatorValue').text(operatorText);

		clone.find('.ParentOperator').text('');

		clone.clone(true).insertAfter(currentRow);
		$('tr.data-wrapper:last select').chosen();
		$('tr.data-wrapper:first select').chosen();
		$(".chosen-select option:first").attr('selected', 'selected');
		$(".chosen-select").chosen().trigger('chosen:updated');
		$('.operatorSelected').removeClass('hidden');

		$(".chosen-select").chosen().trigger('chosen:updated');

	}

	/* function getReleaseValue()
	{
		var releaseId=$(releaseValue).val();	
		releaseIDList=releaseId;
		$('.chosen-select').trigger("chosen:updated");
		$('.buildType').val('').trigger('chosen:updated');
		//$('.buildname').val('').trigger('chosen:updated');
		
		BuildType=null;
		//$('.buildType_1').addClass("hidden");
		//$('.buildType_2').addClass("hidden");
		$('.chosen-select').trigger("chosen:updated");
		//buildSelection();
	} */

	function getSelectedvalue(selectedOption) {
		//$('.Status').find('.chosen-select').chosen().trigger('chosen:updated');
		var selectedId = $(selectedOption).attr("id");
		var lastChar = selectedId.slice(-1);
		var selectedvalue = $(selectedOption).val();

		var StatusSelected = "#selectCondition2_" + lastChar;

		$('.Condition').find('.chosen-select').chosen().trigger(
				'chosen:updated');
		var length = $("#selectCondition_1 option").length;

		for (i = 1; i <= length; i++) {
			if (i == selectedvalue) {
				var showDiv = "#div" + i + "_" + lastChar;
				$(showDiv).removeClass("hidden");

				if (i == 2 || i == 1) {
					//alert(showDiv);
					$(showDiv).find('.inputText').addClass("selectedOption");
				} else
					$(showDiv).find('.chosen-select')
							.addClass("selectedOption");
			} else {
				var showDiv = "#div" + i + "_" + lastChar;
				$(showDiv).addClass("hidden");

				$(showDiv).find('.inputText').removeClass("selectedOption");
				$(showDiv).find('.chosen-select').removeClass("selectedOption");

			}
		}

		$(".chosen-select").chosen().trigger('chosen:updated');

	}

	function removeRow(selectedRow) {
		var rowCount = $('#queryTable tbody  tr').length;
		rowIndex = $(selectedRow).parent('.button-group').parent('.currentRow')
				.index();

		//	var currentRow=$(selectedRow).closest('tr').find('.ParentOperator').text();
		//var operatorValue=$(selectedRow).closest('tr').find('.operatorValue ').text();
		//var prevRow=$(selectedRow).closest('tr').prev().find('.ParentOperator').text();
		//var nextRow=$(selectedRow).closest('tr').next().find('.ParentOperator').text();

		if (rowIndex > 1) {

			/* if(prevRow!='' && operatorValue== ''){
				//alert("remove operator");
				$(selectedRow).closest('tr').next().find('.operatorValue ').text('');
			}
			
			if(prevRow!='' && nextRow!='')
				{
			//	alert("inside");
				$(selectedRow).closest('tr').prev().remove();
				$(selectedRow).closest('tr').remove();
				}
			else if(rowIndex==(rowCount-1) && operatorValue!='')
				{
				//alert("child of last row");
				
				//$(selectedRow).closest('tr').prev().remove();
				$(selectedRow).closest('tr').remove();
				}
			else if (rowIndex==(rowCount-1) && operatorValue=='')
			{
				//alert(" last row");
				
				$(selectedRow).closest('tr').prev().remove();
				$(selectedRow).closest('tr').remove();
			}
			else{ */
			$(selectedRow).closest('tr').remove();
			//}

		}
	}

	$(function() {

		var statusCount_new = "${Model.New}";
		var statusCount_assign = "${Model.Assigned}";
		var status_closed = "${Model.Closed}";
		var status_DevinProgress= "${Model.DevinProgress}";
		var status_ReadyforQA= "${Model.ReadyforQA}";
		var status_Reject= "${Model.Reject}";
		
		var source = "${Model.source}";
		var immediateBug = "${Model.Immediate}";
		var highBug = "${Model.High}";
		var mediumBug = "${Model.Medium}";
		var lowBug = "${Model.Low}";
		var criticalBug = "${Model.Critical}";
		var majorBug = "${Model.Major}";
		var moderateBug = "${Model.Moderate}";
		var minorBug = "${Model.Minor}";

		if (source == "priority") {
			////alert("IN");
			$('#pieChartDiv').removeClass("hidden");
			var doughnutData = {
				labels : [ "Immediate", "High", "Medium", "Low" ],
				datasets : [ {
					data : [ immediateBug, highBug, mediumBug, lowBug ],
					backgroundColor : [ "#5d85d5", "#ff8c1a", "#009900",
							"#b3b300" ],
					hoverBackgroundColor : [ "#5d85d5", "#ff8c1a", "#009900",
							"#b3b300" ]
				} ]
			};

			var doughnutOptions = {
				segmentShowStroke : true,
				segmentStrokeColor : "#fff",
				segmentStrokeWidth : 2,
				percentageInnerCutout : 45, // This is 0 for Pie charts
				animationSteps : 100,
				animationEasing : "easeOutBounce",
				animateRotate : true,
				animateScale : true,
				responsive : true,
				legend : {
					position : 'top'
				}
			};

			var ctx = document.getElementById("doughnutChart");
			var myDoughnutChart = new Chart(ctx, {
				type : 'doughnut',
				data : doughnutData,
				options : doughnutOptions
			});
		} else if (source == "status") {
			//	//alert("OUT"+statusCount_new);
			$('#pieChartDiv').removeClass("hidden");
			var doughnutData1 = {
				labels : [ "New", "Assigned", "Closed","Dev in Progress","Ready for QA","Reject" ],
				datasets : [ {
					data : [ statusCount_new, statusCount_assign, status_closed ,status_DevinProgress,status_ReadyforQA,status_Reject],
					backgroundColor : [ "#5d85d5", "#ff8c1a", "#009900","#b3b300","#804000","#595959" ],
					hoverBackgroundColor : [ "#5d85d5", "#ff8c1a", "#009900","#b3b300","#804000","#595959" ]
				} ]
			};

			var doughnutOptions1 = {
				segmentShowStroke : true,
				segmentStrokeColor : "#fff",
				segmentStrokeWidth : 2,
				percentageInnerCutout : 45, // This is 0 for Pie charts
				animationSteps : 100,
				animationEasing : "easeOutBounce",
				animateRotate : true,
				animateScale : true,
				responsive : true,
				legend : {
					position : 'top'
				}
			};

			var ctx = document.getElementById("doughnutChart");
			var myDoughnutChart = new Chart(ctx, {
				type : 'doughnut',
				data : doughnutData1,
				options : doughnutOptions1
			});
		} else if (source == "severity") {
			//	//alert("OUT"+statusCount_new);
			$('#pieChartDiv').removeClass("hidden");
			var doughnutData1 = {
				labels : [ "Critical", "Major", "Moderate", "Minor" ],
				datasets : [ {
					data : [ criticalBug, majorBug, moderateBug, minorBug ],
					backgroundColor : [ "#5d85d5", "#ff8c1a", "#009900",
							"#b3b300" ],
					hoverBackgroundColor : [ "#5d85d5", "#ff8c1a", "#009900",
							"#b3b300" ]
				} ]
			};

			var doughnutOptions1 = {
				segmentShowStroke : true,
				segmentStrokeColor : "#fff",
				segmentStrokeWidth : 2,
				percentageInnerCutout : 45, // This is 0 for Pie charts
				animationSteps : 100,
				animationEasing : "easeOutBounce",
				animateRotate : true,
				animateScale : true,
				responsive : true,
				legend : {
					position : 'top'
				}
			};

			var ctx = document.getElementById("doughnutChart");
			var myDoughnutChart = new Chart(ctx, {
				type : 'doughnut',
				data : doughnutData1,
				options : doughnutOptions1
			});
		}

		$(".dt-buttons").addClass("hidden");
		window.dispatchEvent(new Event('resize'));
	});

	/* $('#submitButton').click(function(){
		
		 //$('#doughnutChart').removeClass("hidden");
		 $('#submitForm').attr("action","getpiechartdata");
		 $('#submitForm').attr("method","POST");
		 $('#submitForm').submit();
	}); */

	function saveOrExecuteQuery(query) {
		var arrayIndex = 0, status = true;
		$('.chosen-select').trigger("chosen:updated");
		var searchParameter = [], Status = [], selectedResult = [], multiSelectedArray = [], operatorArray = [], parentOperator = [], mainArray = [];
		andCount = 0, orCount = 0;
		var jsonObjectAND = '{ "AND1" : [';
		var tempDataHolder = '', OpeartorLine = '', result;
		//	alert(jsonObject);
		$('.selectedOption').each(
				function() {
					var fieldValue = $(this).val();
					if (fieldValue == null || fieldValue == '') {
						status = false;
						$(this).parents('.ErrorStatus').find('.fieldError')
								.removeClass("hidden");
					} else {
						$(this).parents('.selectedResult').find('.fieldError')
								.addClass("hidden");
					}

				});

		//var flag = Validation();
		if (status == true) {
			$('#queryTable tbody tr.currentRow')
					.each(
							function(row, tr) {

								multiSelectedArray = [];
								// jsonObject=jsonObject+'{';

								//var parentOperator=$(tr).find('td.ParentOp').find('.ParentOperator').text();

								if (tempDataHolder == '') {
									tempDataHolder = 'AND';
									andCount = 1;
								}

								if (tempDataHolder == 'AND') {

									var operatorValue = $(tr).find(
											'.operatorValue').text();
									if ($(tr).index() == 1
											&& operatorValue == '') {

										operatorValue = 'AND';
									}
									var selectedParameter = $(tr).find(
											'td.queryParameter').find(
											'.option:selected').text();

									if (selectedParameter == '') {

										// alert(selectedParameter);
										selectedParameter = $(tr).find(
												'td.queryParameter').find(
												'.source').text();
									}

									var status = $(tr).find('td.Condition')
											.find('.chosen-select').val();
									result = $(tr).find('td.selectedResult')
											.find('.selectedOption').attr(
													'actualValue');

									if (result == null) {
										result = $(tr)
												.find('td.selectedResult')
												.find('.selectedOption').val();

									}
									// alert(OpeartorLine);
									var buildTypeVal = $(tr).find(
											'td.selectedResult').find(
											'.content2').find('.buildType')
											.val();
									//	alert("buildTypeVal"+buildTypeVal+" "+result);
									if (buildTypeVal == null) {
										buildTypeVal = 0;
									}
									OpeartorLine = '{"operator" : "'
											+ operatorValue
											+ '","parameter" : "'
											+ selectedParameter
											+ '", "condition" :"' + status
											+ '", "selectedData" :"' + result
											+ '", "buildType" :"'
											+ buildTypeVal + '"},';

								}

								jsonObjectAND += OpeartorLine;

								OpeartorLine = '';

							});
			jsonObjectAND += ']}';

			tempDataHolder = '';

			var finalStringAND = (jsonObjectAND.substr(0,
					jsonObjectAND.length - 3))
					+ (jsonObjectAND.substr(jsonObjectAND.length - 2,
							jsonObjectAND.length - 1));

			//finalStringOR=finalStringOR.substr(0,1)+finalStringOR.substr(14,finalStringOR.length);
			//alert(finalStringAND);
			var jsonData = JSON.stringify(finalStringAND);
			$('#jsonArray').val(jsonData);

			//$('#jsonData').val(jsonData);

			if (query == true) {
				if ($("#tableForm").valid()) {

					$('body').addClass("white-bg");
					$("#barInMenu").removeClass("hidden");
					$("#wrapper").addClass("hidden");
					$('#tableForm').attr("action", "savepiechartquery");
					$("#tableForm").submit();
				}
			} else {
				if ($("#tableForm").valid()) {

					$('body').addClass("white-bg");
					$("#barInMenu").removeClass("hidden");
					$("#wrapper").addClass("hidden");
					$('#tableForm')
							.attr("action", "executeCustomPieChartQuery");
					$("#tableForm").submit();
				}

			}
		}
	};

	var modal = document.getElementById('releaseModal');
	var buildModal = document.getElementById('buildModal');

	var span = document.getElementsByClassName("close")[0];

	function showPopup(currentClass) {
		//alert($(currentClass).closest('tr').index());
		rowNumber = $(currentClass).closest('tr').index();
		modal.style.display = "block";

	}

	/* span.onclick = function() {
	    modal.style.display = "none";
	    buildModal.style.display = "none";
	} */

	$('.closePopup').click(function() {
		buildModal.style.display = "none";
		modal.style.display = "none";
	});

	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
		if (event.target == buildModal) {
			buildModal.style.display = "none";
		}
	}

	$('.windowClose').click(
			function() {

				var releaseName = '', releaseId = [], release = [];
				modal.style.display = "none";
				//alert($('.releaseId').val());
				$('#releaseTable tbody  tr').each(
						function(row, tr) {
							//var relaseId=$(tr).find('td.relaseTD').find(".releaseId").is(':checked');

							//	alert(relaseId1);
							if ($(tr).find('td.checkboxTD').find(".releaseId")
									.is(':checked')) {
								//var relId=$(tr).find('td.checkboxTD').find(".releaseId").attr('release');
								//var relasename=$(tr).find('td.releaseTD').find(".releaseName").val();
								release.push($(tr).find('td.releaseTD').find(
										".releaseName").val());
								releaseId.push($(tr).find('td.checkboxTD')
										.find(".releaseId").attr('release'));

							}

						});

				//releaseId=releaseId.substr(0,releaseId.length-1);
				releaseName = releaseName.substr(0, releaseName.length - 1);
				// alert($('#editable tbody  tr').eq(rowNumber).find('td.selectedResult').find('.form-group').find('.content1').find(".releaseText").val());
				//alert("");
				releaseIDList = releaseId;
				releaseValue = 1;
				//alert(releaseIDList);
				$('#queryTable tbody  tr').eq(rowNumber).find(
						'td.selectedResult').find('.form-group').find(
						'.content1').find(".releaseText").val(release);
				$('#queryTable tbody  tr').eq(rowNumber).find(
						'td.selectedResult').find('.form-group').find(
						'.content1').find(".releaseText").attr('actualValue',
						releaseId);
				/* $('.manualBuildVal').addClass("hidden");
				$('.autoBuildVal').addClass("hidden");
				$('.manual').addClass("hidden");
				$('.autoBuild').addClass("hidden"); */
				$('.autoBuildRow').addClass("hidden");
				$('.manualBuildRow').addClass("hidden");

			});

	$('.closeBuildWindow').click(
			function() {

				var buildId = [], buildName = [];
				var buildTypeValue = $('#queryTable tbody  tr').eq(rowNumber)
						.find('td.selectedResult').find('.form-group').find(
								'.content2').find(".buildType").val();

				if (buildTypeValue == 1) {

					$('#manualbuildTable tbody  tr').each(
							function(row, tr) {
								if ($(tr).find('td.checkboxTD').find(
										".manualBuild").is(':checked')) {
									buildId.push($(tr).find('td.checkboxTD')
											.find(".manualBuild")
											.attr('manual'));
									buildName.push($(tr).find('td.buildTD')
											.find(".manualBuildVal").attr(
													'buildName'));
									// alert(buildId+buildName);

								}

							});
					$('#queryTable tbody  tr').eq(rowNumber).find(
							'td.selectedResult').find('.form-group').find(
							'.content2').find(".buildText").val(buildName);
					$('#queryTable tbody  tr').eq(rowNumber).find(
							'td.selectedResult').find('.form-group').find(
							'.content2').find(".buildText").attr('actualValue',
							buildId);
				} else if (buildTypeValue == 2) {
					$('#autobuildTable tbody  tr').each(
							function(row, tr) {
								//var relaseId=$(tr).find('td.relaseTD').find(".releaseId").is(':checked');

								//	alert(relaseId1);
								if ($(tr).find('td.checkboxTD').find(
										".autoBuildcheck").is(':checked')) {
									buildId.push($(tr).find('td.checkboxTD')
											.find(".autoBuildcheck").attr(
													'auto'));
									buildName.push($(tr).find('td.buildTD')
											.find(".autoBuildVal").attr(
													'buildName'));
									// alert(buildId+buildName);

								}

							});
					$('#queryTable tbody  tr').eq(rowNumber).find(
							'td.selectedResult').find('.form-group').find(
							'.content2').find(".buildText").val(buildName);
					$('#queryTable tbody  tr').eq(rowNumber).find(
							'td.selectedResult').find('.form-group').find(
							'.content2').find(".buildText").attr('buildId',
							buildId);
				}
				buildModal.style.display = "none";

			});

	function buildPopUp(currentClass) {

		$('input[type=checkbox]').attr('checked', false);
		rowNumber = $(currentClass).closest('tr').index();
		BuildType = $(currentClass).closest('tr').find('.selectedResult').find(
				'.content2').find('.buildType').val();
		buildModal.style.display = "block";
		//alert(releaseValue+releaseStatus)
		//alert(BuildType+releaseValue);
		//alert(releaseValue+releaseStatus+BuildType);
		if (releaseValue == 1 && releaseStatus == 'is') {
			//	alert("if 1");

			if (releaseIDList != null) {
				//alert("build");
				$('.manualBuildRow').addClass("hidden");
				$('.autobuildRow').addClass("hidden");
				//alert(BuildType);
				for (var i = 0; i < releaseIDList.length; i++) {
					$(".releaseId_" + releaseIDList[i]).each(function() {
						var showValue = ".releaseId_" + releaseIDList[i];
						//alert(releaseIDList[i]);

						$(showValue).removeClass("hidden");

					});
				}

				if (BuildType == 1) {
					//alert("1");
					$('.manualbuildTable').removeClass("hidden");
					$('.autobuildTable').addClass("hidden");
					$('.autobuildRow').addClass("hidden");

				} else if (BuildType == 2) {

					//alert("2");
					$('.manualbuildTable').addClass("hidden");
					$('.autobuildTable').removeClass("hidden");
					$('.manualBuildRow').addClass("hidden");
				} else {

					//alert("3");
					$('.manualbuildTable').removeClass("hidden");
					$('.autobuildTable').removeClass("hidden");

				}
				//$('.manualBuildRow').addClass("hidden");
				//$('.autobuildRow').addClass("hidden");

			}
		} else if (releaseValue == 1 && releaseStatus == 'isnot') {

			//alert("if 2");
			if (releaseIDList != null) {
				//alert("build1");
				// $('.autobuildRow').removeClass("hidden");
				$('.manualBuildRow').removeClass("hidden");
				$('.manualbuildTable').removeClass("hidden");

				$('.autobuildTable').removeClass("hidden");
				$(".autoBuildRow1").each(function() {

					$('.autoBuildRow1').removeClass("hidden");

				});
				$('.autoBuildRow1').removeClass("hidden");

				for (var i = 0; i < releaseIDList.length; i++) {
					$(".releaseId_" + releaseIDList[i]).each(function() {
						var showValue = ".releaseId_" + releaseIDList[i];
						//alert(releaseIDList[i]);

						$(showValue).addClass("hidden");

					});
				}
				//alert(BuildType);
				if (BuildType == 1) {
					$('.manualbuildTable').removeClass("hidden");
					$('.autobuildTable').addClass("hidden");
					$('.autobuildRow').addClass("hidden");
				} else if (BuildType == 2) {

					//alert("Inside");

					$('.autobuildTable').removeClass("hidden");
					$('.manualbuildTable').addClass("hidden");

					$('.manualBuildRow').addClass("hidden");
				} else {
					$('.manualbuildTable').addClass("hidden");
					$('.autobuildTable').addClass("hidden");
				}

			}
		} else {

			//alert("else");
			if (BuildType == 1) {
				$('.manualbuildTable').removeClass("hidden");
				$('.manualBuildRow').removeClass("hidden");
			} else if (BuildType == 2) {
				$('.autobuildTable').removeClass("hidden");
				$('.autoBuildRow1').removeClass("hidden");
			}
		}

	}

	function selectBuildType(currentClass) {

		var buildTypeValue = $(currentClass).val();
		$(".chosen-select").chosen().trigger('chosen:updated');
		BuildType = buildTypeValue;
		$('.autoBuildRow').addClass("hidden");
		$('.manualBuildRow').addClass("hidden");
		$('.manualbuildTable').addClass("hidden");
		$('.autobuildTable').addClass("hidden");
	}

	function getConditionValue(selectedclass) {
		releaseStatus = $(selectedclass).val();

		var parameterValue = $(selectedclass).closest('tr').find(
				'.queryParameter').find('.chosen-select').val();
		if (parameterValue == 1) {
			releaseValue = 1;
		}
		//alert(releaseValue);
		$('.autoBuildRow').addClass("hidden");
		$('.manualBuildRow').addClass("hidden");
		$('.manualbuildTable').addClass("hidden");
		$('.autobuildTable').addClass("hidden");
	}

	function buildSelection() {
		if (releaseIDList != null && BuildType != null) {
			for (var i = 0; i < releaseIDList.length; i++) {
				$(".releaseId_" + releaseIDList[i]).each(function() {
					var showValue = ".releaseId_" + releaseIDList[i];
					$(showValue).removeClass("hidden");
					$(showValue).trigger("chosen:updated");
				});
			}
			if (BuildType != null) {
				var buildTypeLen = BuildType.length;

				if (buildTypeLen = 1) {
					if (BuildType == 1) {
						$('.buildType_2').addClass("hidden");
					} else if (BuildType == 2) {
						$('.buildType_1').addClass("hidden");
					} else {

					}
				} else {
				}
				$(".buildType_1").trigger("chosen:updated");
				$(".buildType_2").trigger("chosen:updated");
			}
			$('.chosen-select').trigger("chosen:updated");
		}
	}

	function saveQuery() {
		swal({
			title : "Save your query",
			text : "Enter the name of query",
			type : "input",
			showCancelButton : true,
			closeOnConfirm : false,
			animation : "slide-from-left",
			inputPlaceholder : "The name of query"
		},

		function(inputValue) {
			if (inputValue === false)
				return false;
			if (inputValue === "") {
				swal.showInputError("Please enter name of the query!");
				return false
			}
			$('#queryName').val(inputValue);
			swal("Action Saved!", "Your query is: " + inputValue, "success");
			var saveFlag = true;
			saveOrExecuteQuery(saveFlag);
		});

	}

	function displayQueryData(savedQuery, queryName, queryId) {
		
		
		if(queryName!=null){
			$('#submitButton').addClass('hidden');
			$('#editBtn').removeClass('hidden');
			$('#updateQueryBtn').addClass('hidden');
			$('#createNewBtn').addClass('hidden');
			$('#executeButton').removeClass('hidden');
		}
		
		$(".chosen-select").chosen().trigger('chosen:updated');
		$('#queryId').val(queryId);
		$('#queryName').val(queryName);
		
		query = savedQuery.replace("(", " ");
		var valueArray = query.split(" ");

		var opertarArray = [], parameterArray = [], conditionArray = [], resultArray = [], buildType = []
		var j = 0, releaseName = [], releaseId = [], manualbuildName = [], manualbuildId = [], autobuildName = [], autobuildId = [], statusValue;
		$('.ErrorStatus').addClass("hidden");
		$('.selectCondition').removeAttr('selected');
		$('#releaseTable tbody  tr.releaseTR').each(
				function(row, tr) {

					releaseName.push($(tr).find('td.releaseTD').find(
							'.releaseName').val());
					releaseId.push($(tr).find('td.checkboxTD').find(
							'.releaseId').attr('release'));
				});

		$('#manualbuildTable tbody  tr.manualBuildRow').each(
				function(row, tr) {

					manualbuildName.push($(tr).find('td.buildTD').find(
							".manualBuildVal").attr('buildName'));
					manualbuildId.push($(tr).find('td.checkboxTD').find(
							".manualBuild").attr('manual'));
				});
		$('#autobuildTable tbody  tr.autoBuildRow1').each(
				function(row, tr) {

					autobuildName.push($(tr).find('td.buildTD').find(
							".autoBuildVal").attr('buildName'));
					autobuildId.push($(tr).find('td.checkboxTD').find(
							".autoBuildcheck").attr('auto'));
				});
		//alert(autobuildId+autobuildName);
		for (var i = 0; i <= valueArray.length; i++) {

			//alert(valueArray[i]);
			if (i == 0) {
				opertarArray[j] = "";
			} else {
				opertarArray[j] = valueArray[i];
				i++;
			}

			parameterArray[j] = valueArray[i];
			i++;
			conditionArray[j] = valueArray[i];
			i++;
			resultArray[j] = valueArray[i];
			i++;
			buildType[j] = valueArray[i];
			j++;
		}

		var arrayCount = conditionArray.length;
		//alert("opeartor"+opertarArray+"parameterArray"+parameterArray+"resultArray"+resultArray+"buildType"+buildType+"conditionArray"+conditionArray);

		$('#queryTable tbody  tr.currentRow').each(function(row, tr) {
			var index = $(tr).index();

			if (index > 1) {
				$(tr).remove();

			}

		});

		for (var count = 0; count < arrayCount - 3; count++) {
			rowCount++;
			//var result=$(currentClass).text();

			//var optionValue=$(currentClass).parent('.button-group').parent('.currentRow').find('.selectCondition').val();
			//var currentRow=$(currentClass).parent('.button-group').parent('.currentRow');
			$(".chosen-select").chosen().trigger('chosen:updated');
			var ParentRow = $("#queryTable tr.data-wrapper").last();

			clone.find('.selectCondition').attr('id',
					"selectCondition_" + rowCount);
			clone.find('.selectCondition2').attr('id',
					"selectCondition2_" + rowCount);

			// var operatorText=$(currentClass).text();
			clone.find('.dropdown1').attr('id', "dropdown1_" + rowCount);
			clone.find('.dropdown2').attr('id', "dropdown2_" + rowCount);
			clone.find('.dropdown3').attr('id', "dropdown3_" + rowCount);
			clone.find('.dropdown4').attr('id', "dropdown4_" + rowCount);
			clone.find('.dropdown5').attr('id', "dropdown5_" + rowCount);
			clone.find('.dropdown6').attr('id', "dropdown6_" + rowCount);
			clone.find('.content1').attr('id', "div1_" + rowCount);
			clone.find('.content2').attr('id', "div2_" + rowCount);
			clone.find('.content3').attr('id', "div3_" + rowCount);
			clone.find('.content4').attr('id', "div4_" + rowCount);
			clone.find('.content5').attr('id', "div5_" + rowCount);
			clone.find('.content6').attr('id', "div6_" + rowCount);

			clone.find('.operatorValue').removeClass('hidden');
			$(".chosen-select").chosen().trigger('chosen:updated');
			//	clone.find('.operatorValue').text(operatorText);

			clone.find('.ParentOperator').text('');

			clone.clone(true).insertAfter(ParentRow);
			//$('tr.data-wrapper:last select').chosen();
			//$('tr.data-wrapper:first select').chosen();
			//$(".chosen-select option:first").attr('selected', 'selected');
			$(".chosen-select").chosen().trigger('chosen:updated');
			$('.operatorSelected').removeClass('hidden');

			///buildSelection();
			$(".chosen-select").chosen().trigger('chosen:updated');
		}
		
		$('.selectCondition').each(function() {
			$('.selectCondition').find('option').removeAttr("selected");
			/* alert($('.selectCondition').find('option').val()); */
		});
		$('.selectCondition').chosen().trigger('chosen:updated');
		var arrayNumber = 0;
		$('#queryTable tbody  tr.currentRow')
				.each(
						function(row, tr) {

							var dropdownValue = parameterArray[arrayNumber];
							
							$(tr).find('td.queryParameter').find('.selectCondition').chosen().trigger('chosen:updated');
						
							if (dropdownValue == "Build") {
								$(tr).find('td.queryParameter').find('.selectCondition').chosen().trigger('chosen:updated');

								var buildName = [];
								buildData = [];
								var parameterValue = ".selectCondition option[value='2']", buildData = [];
								$(tr).find('td.queryParameter').find('.selectCondition').val(2).change();
								//$(tr).find('td.queryParameter').find('.selectCondition option[value=2]').attr("selected","selected");
								$(tr).find('td.queryParameter').find('.selectCondition').chosen().trigger('chosen:updated');
								/* alert($(tr).find('td.queryParameter')
										.find(parameterValue).val()); */
								var buildValue = resultArray[arrayNumber];

								for (var i = 0; i < buildValue.length; i++) {
									if (buildValue[i] != ",") {
										buildData.push(buildValue[i]);

									}
								}
								if (buildType[arrayNumber] == 1) {
									for (var i = 0; i < buildData.length; i++) {

										for (var j = 0; j < manualbuildId.length; j++) {
											if (manualbuildId[j] == buildData[i]) {

												buildName
														.push(manualbuildName[i]);
											}
										}
									}
								} else if (buildType[arrayNumber] == 2) {
									for (var i = 0; i < autobuildId.length; i++) {

										if (autobuildId[i] == buildData[i]) {

											//alert("releaseId[i]"+autobuildId[i]+"releaseData[i]"+buildData[i]+"releaseName[i]"+autobuildName[i]);
											buildName.push(autobuildName[i]);
										}
									}
								}

								var buildTypeVal = ".buildType option[value='"
										+ buildType[arrayNumber] + "']";
								$(tr).find('.selectedResult').find('.content2')
										.find(buildTypeVal).attr("selected",
												"selected");
								$(tr).find('.selectedResult')
										.find(".buildText").val(buildName);
								$(tr).find('.selectedResult')
										.find(".buildText").attr('actualvalue',
												buildData);
								$(tr).find('.selectedResult')
										.find(".buildText").addClass(
												"selectedOption");
								//alert("conditionArray[arrayNumber]"+conditionArray[arrayNumber]);
								statusValue = ".selectCondition2 option[value='"
										+ conditionArray[arrayNumber] + "']";
								$(tr).find('.selectedResult').find('.content1')
										.addClass("hidden");
								$(tr).find('.selectedResult').find('.content2')
										.removeClass("hidden");
								$(tr).find('.Status').find(statusValue).attr(
										"selected", "selected");
								$(tr).find('.selectedResult').find(
										".releaseText").removeClass(
										"selectedOption");

							} else if (dropdownValue == "Release") {

								
								/* $(tr).find('td.queryParameter').find(
										'.selectCondition').find('option')
										.removeAttr("selected"); */
								
								$('.selectCondition').chosen().trigger(
										'chosen:updated');
								var relName = [], releaseData = [];
								releaseValue = 1;

								var value = ".selectCondition option[value='1']";
								
								
								//$(tr).find('td.queryParameter').find(value).attr("selected","selected");
								$(tr).find('td.queryParameter').find('.selectCondition').val(1).change();

								
								$(tr).find('td.queryParameter').find(
										".form-group").find(".chosen-select")
										.chosen().trigger('chosen:updated');
								//alert($(tr).find('td.queryParameter').find('.chosen-select').val());
								var releaseval = resultArray[arrayNumber];

								for (var i = 0; i < releaseval.length; i++) {
									if (releaseval[i] != ",") {
										releaseData.push(releaseval[i]);

									}
								}
								//alert(releaseData);
								for (var i = 0; i < releaseData.length; i++) {

									for (var j = 0; j < releaseId.length; j++)
										if (releaseData[i] == releaseId[j]) {
											//alert(releaseData[i]+releaseId[j]);
											relName.push(releaseName[i]);
										}
								}
								releaseIDList = releaseData;
								$(tr).find('.selectedResult').find(
										'.releaseText').val(relName);
								$(tr).find('.selectedResult').find(
										".releaseText").attr('actualValue',
										releaseData);
								$(tr).find('.selectedResult').find(
										".releaseText").addClass(
										"selectedOption");
								//alert("conditionArray[arrayNumber]"+conditionArray[arrayNumber]);
								statusValue = ".selectCondition2 option[value='"
										+ conditionArray[arrayNumber] + "']";
								$(tr).find('.Status').find(statusValue).attr(
										"selected", "selected");
								$(tr).find('.selectedResult').find('.content1')
										.removeClass("hidden")

								$(tr).find('td.queryParameter').find(
										'.selectCondition').chosen().trigger(
										'chosen:updated');
								//alert($(tr).find('td.queryParameter').find('.chosen-select').val());

							} else if (arrayNumber == 0) {
								//alert("In source");
								/* $(tr).find('td.queryParameter').find(
										'.selectCondition').find('option')
										.removeAttr("selected"); */
								$(tr).find('td.queryParameter').find(
										'.selectCondition').chosen().trigger(
										'chosen:updated');
								var sourceData = resultArray[arrayNumber], status = ".selectCondition option[value='"
										+ 1 + "']";

								//$(tr).find('td.queryParameter').find(status)
								//		.attr("selected", "selected");

								//alert(statusValue[i]);
								var statusDropdown = ".chosen-select option[value='"+sourceData+"']";
								//alert(sourceData);
								$(tr).find('td.selectedResult').find(
										'.SourceData').find(statusDropdown)
										.attr("selected", "selected");

								sourceCodition = ".SourceCondition option[value='"
										+ conditionArray[arrayNumber] + "']";
								//alert("source condition"+sourceCodition);
								$(tr).find('td.Condition').find(sourceCodition)
										.attr("selected", "selected");
								$(tr).find('.selectedResult').find(
										'.SourceData').removeClass("hidden");
								/*$(tr).find('.selectedResult').find('.content3')
										.removeClass("hidden")*/
								/* $(tr).find('.selectedResult').find('.content3') 
										.find(".requiredField").addClass(
												"selectedOption"); */

							} else if (dropdownValue == "Project") {

								/* $(tr).find('td.queryParameter').find(
										'.selectCondition').find('option')
										.removeAttr("selected"); */
								$(tr).find('td.queryParameter').find(
										'.selectCondition').chosen().trigger(
										'chosen:updated');
								$(tr).find('td.queryParameter').find(
										'.selectCondition').val();
								var projectValue = resultArray[arrayNumber], project = ".selectCondition option[value='3']";
								$(tr).find('td.queryParameter').find('.selectCondition').val(3).change();
								$(".chosen-select").chosen().trigger(
								'chosen:updated');
								//alert($(tr).find('td.queryParameter').find('.selectCondition').val());
								//$(tr).find('td.queryParameter').find(project).attr("selected", "selected");
								for (var i = 0; i < projectValue.length; i++) {
									if (projectValue[i] != ",") {

										var projectDropdown = ".chosen-select option[value='"
												+ projectValue[i] + "']";
									
										$(".chosen-select").chosen().trigger(
												'chosen:updated');
										$(tr).find('td.selectedResult').find(
												'.content3').find(
												projectDropdown).attr(
												"selected", "selected");
									}
								}
								statusValue = ".selectCondition2 option[value='"
										+ conditionArray[arrayNumber] + "']";
								$(tr).find('.Condition').find(statusValue)
										.attr("selected", "selected");
								$(tr).find('.selectedResult').find('.content1')
										.addClass("hidden");
								$(tr).find('.selectedResult').find('.content3')
										.removeClass("hidden");
								$(tr).find('.selectedResult').find('.content3')
										.find(".chosen-select").addClass(
												"selectedOption");
								$(tr).find('.selectedResult').find(
										".releaseText").removeClass(
										"selectedOption");
								//	alert($(tr).find('td.queryParameter').find('.selectCondition').val());
								//alert($(tr).find('.Condition').find(statusValue).val());
							}

							$(".chosen-select").chosen().trigger(
									'chosen:updated');
							$(tr).find('.childOp').find(".operatorValue").text(
									opertarArray[arrayNumber]);
							arrayNumber++;

						});
		if(queryName!=null){
		$('.chosen-select').each(function() {
			$(this).attr("disabled", "true").trigger('chosen:updated');
		});
		
		$('.popup').each(function() {
			$(this).attr("onclick", "return false;");
		});
		
		$('.buildPopup').each(function() {
			$(this).attr("onclick", "return false;");
		});
		}
	}
	function editQuery() {
		$('.chosen-select').each(function() {
			$(this).attr("disabled", false).trigger('chosen:updated');
		});
		
		$('.popup').each(function() {
			$('.popup').attr("onclick", "showPopup(this)");
		});
		$('.buildPopup').each(function() {
			$(this).attr("onclick", "buildPopUp(this)");
		});

		$('#editBtn').addClass('hidden');
		$('#executeButton').addClass('hidden');
		$('#createNewBtn').removeClass('hidden');
		$('#updateQueryBtn').removeClass('hidden');
	}

	$('#updateQueryBtn').click(function() {

		saveOrExecuteQuery();
		if ($("#tableForm").valid()) {

			$('body').addClass("white-bg");
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");
			$('#tableForm').attr("action", "updatepiechartcustomquery");
			$("#tableForm").submit();
		}

	});
</script>
<style>
.modal {
	display: none; /* Hidden by default */
	position: fixed; /* Stay in place */
	z-index: 1; /* Sit on top */
	padding-top: 100px; /* Location of the box */
	left: 0;
	top: 0;
	width: 100%; /* Full width */
	height: 100%; /* Full height */
	overflow: auto; /* Enable scroll if needed */
	background-color: rgb(0, 0, 0); /* Fallback color */
	background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
	background-color: #fefefe;
	margin: auto;
	padding: 20px;
	border: 1px solid #888;
	width: 100%;
	height: 100%;
}

/* The Close Button */
.close {
	color: #aaaaaa;
	float: right;
	font-size: 28px;
	font-weight: bold;
}

.close:hover, .close:focus {
	color: #000;
	text-decoration: none;
	cursor: pointer;
}
</style>

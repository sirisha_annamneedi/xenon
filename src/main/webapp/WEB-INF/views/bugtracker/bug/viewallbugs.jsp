<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="md-skin">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-8">
<%-- 		<h2>${sessionScope.UserCurrentProjectName} / All Bugs</h2> --%>
		<h2>
			<span class="lgMenuTextAlignment" style="width: 90% !important;" data-toggle="tooltip">
				<ol class="breadcrumb">
					<li><strong>${sessionScope.UserCurrentProjectName}</strong></li>
					<li>All Bugs</li>
					
				</ol>
				</span>
			</h2>
		<!-- <ol class="breadcrumb">
			<li><a href="javascript:void(0)" onclick="gotoLinkInMenu('viewallbugs')">Dashboard</a></li>
			<li class="active"><strong>All Bugs</strong></li>
		</ol> -->
		<!-- end breadcrumb -->
	</div>

	<!-- end col-lg-10 -->
</div>
<!-- end wrapper -->

<div class="row sub-topmenu">
<div class="col-md-12">
<div class="">

<div class="col-md-6 pd-l-0">
<ul>
<li><a href="javascript:void(0)" onclick="gotoLinkInMenu('viewallbugs')" class="active "><i class="fa fa-home"></i> Home</a></li>
<li><a href="javascript:void(0)" onclick="gotoLinkInMenu('createbug')"><i class="fa fa-plus"></i> Create Bug</a></li>


</ul>
</div>

</div>
</div>
</div>


<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<div class="table-responsive" >
						<table class="table table-striped table-bordered table-hover dataTables-example dataTable no-footer issue-tracker" id="viewBugTable">
							<thead class="">
								<tr>
									<th class="hidden">Bug ID</th>
									<th>Bug ID</th>
									<th>Status</th>
									<th style="width: 150px">Title</th>
									<th>Priority</th>
									<th>Module</th>
									<th>Reported By</th>
									<th>Assigned To</th>
									<th>Created Date</th>
									<th>Updated Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="data" items="${Model.allBugDetails}">
									<tr style="cursor: pointer;">
										<td class="hidden">${data.bug_id}</td>
										<td>${data.bug_prefix}</td>
										<td><span class="label td_bugStatus" style="color: #FFFFFF">${data.bugStatus}</span></td>
										<td class="lgTextAlignment" data-original-title="${data.bug_title}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.bug_title}</td>
										<td>${data.bug_priority}</td>
										<td class="textAlignment" data-original-title="${data.module_name}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.module_name}</td>
										<td class="textAlignment" data-original-title="${data.reporterFN} ${data.reporterLN}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.reporterFN} ${data.reporterLN}</td>
										<td class="textAlignment" data-original-title="${data.assigneeFN} ${data.assigneeLN}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.assigneeFN} ${data.assigneeLN}</td>
										<td class="textAlignment" data-original-title="${data.create_date}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.create_date}</td>
										<td class="textAlignment" data-original-title="${data.update_date}" data-container="body" data-toggle="tooltip" data-placement="right" class="issue-info">${data.update_date}</td>
										<td>
											
											<button class="btn-white btn btn-xs edit-btn" id="editBugBtn">
												<i class="fa fa-edit"></i>
											</button>
											
											
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					
				</div>

			</div>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	 var page= <%= request.getParameter("page") %>;
	 if(page == null){
		 page = 1;
	 }
	 
	 var allBugCount='${Model.allBugCount}';
	 
	 var pageSize='${Model.pageSize}';
	 
	 var lastRec=((page-1)*pageSize+parseInt(pageSize));
	 
	 if(lastRec>allBugCount)
	 	lastRec=allBugCount;
	 var showCount = ((page-1)*pageSize+1);
	 if(showCount <= lastRec){
		// $("#viewBugTable_info").html("Showing "+showCount+" to "+lastRec+" of "+allBugCount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="prevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="nextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
		 $("#viewBugTable_info").html("Showing "+showCount+" to "+lastRec+" of "+allBugCount+" entries " + '<div class="btn-group"> </div>');
	 }else{
		 $("#viewBugTable_info").html("");
	 }
	
// 	 if(page == 1){
// 		 $("#prevBtn").attr('disabled','disabled');
// 	 }
	 
// 	 if(lastRec == allBugCount){
// 		 $("#nextBtn").attr('disabled','disabled');
// 	 }
	 
// 	    $("#prevBtn").click(function() {
// 	    	$("#barInMenu").removeClass("hidden");
// 	    	$("#wrapper").addClass("hidden");
	    	
// 	    	if(page == 1)
// 	    		window.location.href = "viewallbugs";
// 	    	else
// 	    		window.location.href = "viewallbugs?page="+(page - 1);
// 		});
		
// 		$("#nextBtn").click(function(){
// 			$("#barInMenu").removeClass("hidden");
// 	    	$("#wrapper").addClass("hidden");
	    	
// 			window.location.href = "viewallbugs?page="+(page + 1);
// 		});
});

	$(function() {
		$("#customerLogo")
				.attr(
						"src",
						"data:image/jpg;base64,"
								+ localStorage.getItem("customerLogo"));
	});

	$(document).ready(
			function() {
				 $('[data-toggle="tooltip"]').tooltip();   
				var today = moment().format('MMMM Do YYYY, h:mm:ss a');
				var table = $('.dataTables-example').DataTable(
						{
							dom : '<"html5buttons"B>lTfgitp',
							buttons : [
									{
										extend : 'csv',
										title : 'View All Bugs - ' + today,
										exportOptions: {
						                    columns: ':visible'
						                }
									},
									{
										extend : 'excel',
										title : 'View All Bugs - ' + today,
										exportOptions: {
						                    columns: ':visible'
						                }
									},
									{
										extend : 'pdf',
										title : 'View All Bugs - ' + today,
										exportOptions: {
						                    columns: ':visible'
						                }
									},

									{
										extend : 'print',
										exportOptions: {
						                    columns: ':visible'
						                },
										customize : function(win) {
											$(win.document.body).addClass(
													'white-bg');
											$(win.document.body).css(
													'font-size', '10px');

											$(win.document.body).find('table')
													.addClass('compact').css(
															'font-size',
															'inherit');
										},
										title : 'View All Bugs - ' + today
									} ],
							"aaSorting": [[0, "desc"]],
							"paging" : true,
							"lengthChange" : false,
							"searching" : true,
							"ordering" : true
						});
				
				// Apply the filter
			    $("#viewBugTable thead input").on( 'keyup change', function () {
			    	table
			            .columns( $(this).parent().index()+':visible' )
			            .search( this.value )
			            .draw();
			    	
			    	setSpanColor();
			    } );
				
				setSpanColor();
				$('#viewBugTable_paginate').on('click', function() {
					setSpanColor();
				});
				$('#viewBugTable_length select').on('change', function() {
					setSpanColor();
				});
				$('#viewBugTable_filter input').on('keyup change', function() {
					setSpanColor();
				});
				$('#viewBugTable thead tr').on('click', function() {
					setSpanColor();
				});

			});

	var setSpanColor = function() {
		$('.td_bugStatus').each(function() {
            var stat=$(this).text();
           
            if(stat=='New')
            {
            	$(this).css("background-color","${Model.New}");
            }
            else if(stat=='Assigned')
            {
            	$(this).css("background-color","${Model.Assigned}");
            }
            else if(stat=='Dev in Progress')
            {
            	$(this).css("background-color","${Model.Fixed}");
            }
            else if(stat=='Ready for QA')
            {
            	$(this).css("background-color","${Model.Verified}");
            }
            else if(stat=='Closed')
            {
            	$(this).css("background-color","${Model.Closed}");
            }
            else if(stat=='Reject')
            {
            	$(this).css("background-color","${Model.Reject}");
            }
           
           
      });
	}
	$("#viewBugTable tbody tr").click(function() {
		var bugId = $(this).children('td').first().html();
		$.post("setbugidforsummary", {
			bugID : bugId
		}, function(response) {
			if (response == true) {
				window.open('bugsummary', '_blank');
			} else {
				window.location.href = "500";
			}

		});
	});

	
</script>

<style>
.md-skin .ibox{
box-shadow:none;
}
.ibox-content{
padding:20px;
}
.md-skin .wrapper-content{
padding:0;
margin:10px 0;
}
table.table-bordered.dataTable th, table.table-bordered.dataTable td {
    border-left-width: 0;
    width: 100% !important;
    vertical-align: middle;
}
.table{
table-layout:fixed;
}
#viewBugTable_filter{
width: 50%;
float: right;
}
#viewBugTable_info{
padding-top:0;
}
</style>
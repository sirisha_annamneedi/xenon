<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>Bug Group</h2>
			<ol class="breadcrumb">
				<li><a href="btdashboard">Dashboard</a></li>
				<li class="active"><strong>Bug Group Setting</strong></li>
			</ol>
		</div>
		<!-- end col-lg-10 -->
	</div>
	<!-- end row -->
	<div class="wrapper wrapper-content animated fadeInUp row">
		<div class="row hidden" id="divEditBugGroup">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content">
						<div class="ibox-title">
							<h5>Edit Bug Group</h5>
						</div>
						<form action="updatebuggroup"
							class="wizard-big wizard clearfix form-horizontal"
							id="updateBugGroup" method="POST"
							style="overflow: visible !important;">
							<div class="content clearfix"
								style="overflow: visible !important;">

								<fieldset class="body current">
									<div class="row">
										<label class="col-lg-4 pull-right text-right">* fields
											are mandatory</label>
									</div>
									<input id="bugGroupId" name="bugGroupId" type="text"
										class="form-control hidden">
									<div class="form-group">
										<label class="col-lg-3 control-label">Bug Group Name*:</label>
										<div class="col-lg-7">
											<input id="bugGroupName" name="bugGroupName" type="text"
												class="form-control" required>
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Select Users*:</label>
										<div class="col-lg-7">
											<select data-placeholder="Please Select Users"
												class="chosen-select-edit" multiple id="selectUsers"
												name="users" style="min-width: 100%">
												<c:forEach var="user" items="${Model.UserDetails}">
													<option value="${user.user_id}">${user.user_name}</option>
												</c:forEach>
											</select>

											<div class="hidden" id="multiSelectUpdate">
												<span class="error"
													style="font-weight: bold; color: #8a1f11;">This
													field is required.</span>
											</div>

										</div>
									</div>

								</fieldset>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-4">
										<button class="btn btn-white pull-left" id="btnCancelUpdate"
											style="margin-right: 15px;" type="button">Cancel</button>
										<button class="btn btn-success pull-left ladda-button"
											style="margin-right: 15px;" type="button"
											data-style="slide-up" id="btnupdateBugGroup">Submit</button>
									</div>
								</div>
							</div>
						</form>
						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<div class="row hidden" id="divAddBugGroup">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content">
						<div class="ibox-title">
							<h5>Create New Bug Group</h5>
						</div>
						<form action="insertbuggroup"
							class="wizard-big wizard clearfix form-horizontal"
							id="insertMailGroup" method="POST"
							style="overflow: visible !important;">
							<div class="content clearfix"
								style="overflow: visible !important;">
								<fieldset class="body current">
									<div class="row">
										<label class="col-lg-4 pull-right text-right">* fields
											are mandatory</label>
									</div>
									<c:if test="${Model.insertError == 1}">
										<div class="alert alert-danger" id="insertErrorMsg">Bug
											group name already exists, Please try another.</div>
									</c:if>
									<div class="form-group">
										<label class="col-lg-3 control-label">Bug Group Name*:</label>
										<div class="col-lg-7">
											<input name="bugGroupName" type="text"
												class="form-control characters" required=""
												aria-required="true">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Select Users*:</label>
										<div class="col-lg-7">
											<select data-placeholder="Please Select Users"
												class="chosen-select-add" multiple name="users"
												id="createUsers" style="min-width: 100%; z-index: 9999;">
												<c:forEach var="user" items="${Model.UserDetails}">
													<option value="${user.user_id}">${user.user_name}</option>
												</c:forEach>
											</select>

											<div class="hidden" id="multiSelectCreate">
												<span class="error"
													style="font-weight: bold; color: #8a1f11;">This
													field is required.</span>
											</div>
										</div>
									</div>

								</fieldset>
							</div>
							<div class="actions clearfix">
								<div class="row">
									<div class="col-sm-4">
										<button class="btn btn-white pull-left"
											id="mailGroupCancelBtn" style="margin-right: 15px;"
											type="button">Cancel</button>
										<button class="btn btn-success pull-left ladda-button"
											style="margin-right: 15px;" type="button"
											data-style="slide-up" id="submitMailGroup">Submit</button>
									</div>
								</div>
							</div>
						</form>
						<!-- end form -->
					</div>
					<!-- end ibox-content -->

				</div>
				<!-- end ibox -->
			</div>
			<!-- end col -->
		</div>
		<!-- end wrapper -->

		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Bug Groups</h5>
						<div class="ibox-tools">
							<button type="button" class="btn btn-success btn-xs"
								id="btnCreateMailGroup">Create New Bug Group</button>
						</div>
					</div>
					<div class="ibox-content">
						<table class="footable table table-stripped toggle-arrow-tiny"
							data-page-size="8">
							<thead>
								<tr>
									<th data-toggle="true">Name</th>
									<th data-hide="all"></th>
									<th class="text-right" data-sort-ignore="true">Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="data" items="${Model.bugGroups}">
									<tr>
										<td class="project-title"><a href="#"
											style="pointer-events: none; cursor: default;">${data.bug_group_name}</a>
										</td>
										<td>
											<table class="table datatable userTable">
												<thead>
													<tr>
														<th class="hidden">Id</th>
														<th></th>
														<th>User Name</th>
														<th>Email Id</th>
														<th>Role</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="user" items="${Model.bugGroupDetails}">
														<c:if test="${user.group_id == data.bug_group_id}">
															<tr class="tr_mail_group_${data.bug_group_id}">
																<td class="hidden">${user.user}</td>
																<td><img alt="image" id="userPhotoId"
																	class="img-circle"
																	src="data:image/jpg;base64,${user.user_photo}"
																	style="width: 30px"></td>
																<td class="verticalMiddleAlign">${user.user_name}</td>
																<td class="verticalMiddleAlign">${user.email_id}</td>
																<td class="verticalMiddleAlign">${user.user_role}</td>
															</tr>
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</td>
										<td class="mail-group-actions pull-right">
											<button class="btn btn-white btn-sm btnEditMailGroup"
												onclick="editMailGroup(${data.bug_group_id},'${data.bug_group_name}')">
												Edit</button>
										</td>
									</tr>
								</c:forEach>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="5">
										<ul class="pagination pull-right"></ul>
									</td>
								</tr>
							</tfoot>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(window).load(function() {
	$('body').removeClass("white-bg");
	$("#barInMenu").addClass("hidden");
	$("#wrapper").removeClass("hidden");
	
	 var foo = $('.footable').footable();
	 foo.trigger('footable_initialize'); //Reinitialize
	 foo.trigger('footable_redraw'); //Redraw the table
	 foo.trigger('footable_resize'); //Resize the table
});
</script>
<script type="text/javascript">
  $(function () {
	  $("#customerLogo").attr("src","data:image/jpg;base64,"+localStorage.getItem("customerLogo"));
	
});
</script>
<script>
	$(function(){
		
$('.userTable').DataTable({
	"paging" : false,
	"lengthChange" : false,
	"searching" : false,
	"ordering" : false,
	"info" : false,
	"autoWidth" : true
});	
error = "${Model.insertError}";
if(error == 1)
	{
	$("#divAddBugGroup").removeClass('hidden');
	$(".chosen-select-add").chosen();
	//$("ul.chosen-results").css('max-height','150px');
	}
});
	$("#btnCreateMailGroup").click(function(){
		$("#insertErrorMsg").addClass('hidden');
			$("#divAddBugGroup").removeClass('hidden');
			$(".chosen-select-add").chosen();
			//$("ul.chosen-results").css('max-height','150px');
			$("#divEditBugGroup").addClass('hidden');
			
			$(this).hide();
			});
	$("#submitMailGroup").click(function()
			{
			 if($("#insertMailGroup").valid())
				{
				 if($("#createUsers").val()!=null)
					 {
					 $("#insertMailGroup").submit();
					 }
				 else
					 {
					 $("#multiSelectCreate").removeClass("hidden");
					 }
				}
			});
	$("#btnupdateBugGroup").click(function()
			{
			 if($("#updateBugGroup").valid())
				{
				 if($("#selectUsers").val()!=null)
					 {
					    $("#updateBugGroup").submit();
					 }
				 else
					 {
					   $("#multiSelectUpdate").removeClass("hidden");
					 }
				
				}
			});
	
	$("#mailGroupCancelBtn").click(function()
			{
		     $("#divAddBugGroup").addClass('hidden');
		     $('#btnCreateMailGroup').show();
		     
			});
	
	
	$("#selectUsers").change(function() {
		$("#multiSelectUpdate").addClass("hidden");
	});
	
	$("#createUsers").change(function() {
		$("#multiSelectCreate").addClass("hidden");
	});
	
	$("#btnCancelUpdate").click(function(){
		$("#divEditBugGroup").addClass('hidden');
		$('#btnCreateMailGroup').show();
			});
	
	function editMailGroup(bugGroupId,bugGroupName){
		localStorage.removeItem('bugGroupName');
		$("#bugGroupName").val("");
		$("#bugGroupId").val("");
		$('#selectUsers').val('').trigger('chosen:updated');
		$(".tr_mail_group_"+bugGroupId).each(function(){
			value = $(this).children('td').first().html();
			$("#selectUsers option[value='" + value + "']").prop("selected", true);
			$('#selectUsers').trigger("chosen:updated");

		});
		$("#bugGroupName").val(bugGroupName);
		$("#bugGroupId").val(bugGroupId);
		$("#divEditBugGroup").removeClass('hidden');
		$("#divAddBugGroup").addClass('hidden');
		$(".chosen-select-edit").chosen();
		 $('#btnCreateMailGroup').hide();
		
		//$("ul.chosen-results").css('max-height','150px');
	}
</script>
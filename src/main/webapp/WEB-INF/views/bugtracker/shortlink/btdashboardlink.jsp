<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
	<div class="wrapper wrapper-content">
		<c:if test="${Model.filterText=='immediate'}">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Immediate Priority Bugs</h5>
						</div>
						<div class="ibox-content">
							<div class="table-responsive" style="overflow-x: visible;">
								<table
									class="table table-striped table-bordered table-hover dataTables-example"
									id="immediateBugList" name="Immediate Priority Bugs">
									<thead>
										<tr>
											<th class="hidden">Bug ID</th>
											<th>Bug ID</th>
											<th style="width: 250px">Title</th>
											<th>Status</th>
											<th>Priority</th>
											<th>Severity</th>
											<th>Category</th>
											<th>Reported By</th>
											<th>Assigned To</th>
											<th>Module</th>
											<th class="hidden">Updated Date</th>
											<th class="hidden">Application ID</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="data" items="${Model.immediateList}">
											<tr style="cursor: pointer;" class="gradeX">
												<td class="hidden">${data.bug_id}</td>
												<td>${data.bug_prefix}</td>
												<td class="xlTextAlignment"
													data-original-title="${data.bug_title}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${data.bug_title}</td>
												<td><span class="label td_bugStatus"
													style="color: #FFFFFF">${data.bug_status}</span></td>
												<td>${data.bug_priority}</td>
												<td>${data.bug_severity}</td>
												<td>${data.category_name}</td>
												<td class="textAlignment"
													data-original-title="${data.reporter}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${data.reporter}</td>
												<td class="textAlignment"
													data-original-title="${data.assignee}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${data.assignee}</td>
												<td class="textAlignment"
													data-original-title="${data.module_name}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${data.module_name}</td>
												<td class="hidden">${data.update_date}</td>
												<td class="hidden">${data.project}</td>
											</tr>
										</c:forEach>
									</tbody>

								</table>
							</div>

						</div>
					</div>


				</div>
			</div>
		</c:if>

		<c:set var="string1" value='${Model.filterText}' />
		<c:set var="string2" value="${fn:substring(string1, 0, 6)}" />
		<c:set var="cat" value="${fn:substring(string1, 6, 8)}" />
		<c:set var="rid" value="${fn:substring(string1, 8, 20)}" />

		<c:if test="${string2=='closed'}">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox">
						<div class="ibox-title">
							<h5>Closed Bugs</h5>
						</div>

						<div class="ibox-content">

							<div class="table-responsive">
								<table
									class="table table-striped table-bordered table-hover dataTables-example"
									id="closedBugList" name="Closed Bugs">
									<thead>
										<tr>
											<th class="hidden">Bug ID</th>
											<th>Bug ID</th>
											<th style="width: 250px">Title</th>
											<th>Status</th>
											<th>Priority</th>
											<th>Severity</th>
											<th>Category</th>
											<th>Reported By</th>
											<th>Assigned To</th>
											<th>Module</th>
											<th class="hidden">Updated Date</th>
											<th class="hidden">Application ID</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="data" items="${Model.closedList}">
											<c:if test="${Model.filterText == 'closed'}">
												<tr style="cursor: pointer;" class="gradeX">
													<td class="hidden">${data.bug_id}</td>
													<td>${data.bug_prefix}</td>
													<td class="xlTextAlignment"
														data-original-title="${data.bug_title}"
														data-container="body" data-toggle="tooltip"
														data-placement="right" class="issue-info">${data.bug_title}</td>
													<td><span class="label td_bugStatus"
														style="color: #FFFFFF">${data.bug_status}</span></td>
													<td>${data.bug_priority}</td>
													<td>${data.bug_severity}</td>
													<td>${data.category_name}</td>
													<td class="textAlignment"
														data-original-title="${data.reporter}"
														data-container="body" data-toggle="tooltip"
														data-placement="right" class="issue-info">${data.reporter}</td>
													<td class="textAlignment"
														data-original-title="${data.assignee }"
														data-container="body" data-toggle="tooltip"
														data-placement="right" class="issue-info">${data.assignee}</td>
													<td class="textAlignment"
														data-original-title="${data.module_name}"
														data-container="body" data-toggle="tooltip"
														data-placement="right" class="issue-info">${data.module_name}</td>
													<td class="hidden">${data.update_date}</td>
													<td class="hidden">${data.project}</td>
												</tr>
											</c:if>
											<c:if test="${data.cat_id == cat}">
												<c:if test="${data.issue_tracker_release_id == rid}">
													<tr style="cursor: pointer;" class="gradeX">
														<td class="hidden">${data.bug_id}</td>
														<td>${data.bug_prefix}</td>
														<td class="xlTextAlignment"
															data-original-title="${data.bug_title}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${data.bug_title}</td>
														<td><span class="label td_bugStatus"
															style="color: #FFFFFF">${data.bug_status}</span></td>
														<td>${data.bug_priority}</td>
														<td>${data.bug_severity}</td>
														<td>${data.category_name}</td>
														<td class="textAlignment"
															data-original-title="${data.reporter}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${data.reporter}</td>
														<td class="textAlignment"
															data-original-title="${data.assignee }"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${data.assignee}</td>
														<td class="textAlignment"
															data-original-title="${data.module_name}"
															data-container="body" data-toggle="tooltip"
															data-placement="right" class="issue-info">${data.module_name}</td>
														<td class="hidden">${data.update_date}</td>
														<td class="hidden">${data.project}</td>
													</tr>
												</c:if>
											</c:if>
										</c:forEach>
									</tbody>

								</table>
							</div>

						</div>
					</div>
				</div>
			</div>
		</c:if>

		<!-- for root cause -->

		<c:set var="close_list" value='${Model.filterText}' />
		<c:set var="b_list" value="${fn:substring(close_list, 0, 6)}" />
		<c:set var="end_len" value="${fn:length(close_list)}" />
		<c:set var="bugID" value="${fn:substring(close_list,7, end_len)}" />
		<c:set var="bool_val" value="${fn:contains(bugID,',')}" />

		<c:if test="${b_list=='bugCat'}">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Closed Bugs</h5>
						</div>

						<div class="ibox-content">

							<div class="table-responsive">
								<table
									class="table table-striped table-bordered table-hover dataTables-example"
									id="closedBugList" name="Closed Bugs">
									<thead>
										<tr>
											<th class="hidden">Bug ID</th>
											<th>Bug ID</th>
											<th style="width: 250px">Title</th>
											<th>Status</th>
											<th>Priority</th>
											<th>Severity</th>
											<th>Category</th>
											<th>Reported By</th>
											<th>Assigned To</th>
											<th>Module</th>
											<th class="hidden">Updated Date</th>
											<th class="hidden">Application ID</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="data" items="${Model.closedList}">

											<c:if test="${bool_val == true}">
												<c:set var="arrayoflist" value="${fn:split(bugID,',')}" />
												<c:forEach var="i" begin="0"
													end="${fn:length(arrayoflist)-1}">
													<fmt:parseNumber var="bugId" integerOnly="true"
														type="number" value="${arrayoflist[i]}" />
													<c:if test="${data.bug_id == bugId }">
														<tr style="cursor: pointer;" class="gradeX">
															<td class="hidden">${data.bug_id}</td>
															<td>${data.bug_prefix}</td>
															<td class="xlTextAlignment"
																data-original-title="${data.bug_title}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.bug_title}</td>
															<td><span class="label td_bugStatus"
																style="color: #FFFFFF">${data.bug_status}</span></td>
															<td>${data.bug_priority}</td>
															<td>${data.bug_severity}</td>
															<td>${data.category_name}</td>
															<td class="textAlignment"
																data-original-title="${data.reporter}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.reporter}</td>
															<td class="textAlignment"
																data-original-title="${data.assignee }"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.assignee}</td>
															<td class="textAlignment"
																data-original-title="${data.module_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.module_name}</td>
															<td class="hidden">${data.update_date}</td>
															<td class="hidden">${data.project}</td>
														</tr>
													</c:if>
												</c:forEach>
											</c:if>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</c:if>


		<c:if test="${Model.filterText=='assigned'}">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox">
						<div class="ibox-title">
							<h5>Assigned To Me Bugs</h5>
						</div>
						<div class="ibox-content">

							<div class="table-responsive">
								<table
									class="table table-striped table-bordered table-hover dataTables-example"
									id="assignedToMeBugList" name="Assigned To Me Bugs">
									<thead>
										<tr>
											<th class="hidden">Bug ID</th>
											<th>Bug ID</th>
											<th style="width: 250px">Title</th>
											<th>Status</th>
											<th>Priority</th>
											<th>Severity</th>
											<th>Category</th>
											<th>Reported By</th>
											<th>Assigned To</th>
											<th>Module</th>
											<th class="hidden">Updated Date</th>
											<th class="hidden">Application ID</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="data" items="${Model.assignedList}">
											<tr style="cursor: pointer;" class="gradeX">
												<td class="hidden">${data.bug_id}</td>
												<td>${data.bug_prefix}</td>
												<td class="xlTextAlignment"
													data-original-title="${data.bug_title}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${data.bug_title}</td>
												<td><span class="label td_bugStatus"
													style="color: #FFFFFF">${data.bug_status}</span></td>
												<td>${data.bug_priority}</td>
												<td>${data.bug_severity}</td>
												<td>${data.category_name}</td>
												<td class="textAlignment"
													data-original-title="${data.reporter}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${data.reporter}</td>
												<td class="textAlignment"
													data-original-title="${data.assignee}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${data.assignee}</td>
												<td class="textAlignment"
													data-original-title="${data.module_name}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${data.module_name}</td>
												<td class="hidden">${data.update_date}</td>
												<td class="hidden">${data.project}</td>
											</tr>
										</c:forEach>
									</tbody>

								</table>
							</div>

						</div>
					</div>


				</div>
			</div>
		</c:if>

		<c:set var="open_list" value='${Model.filterText}' />
		<c:set var="bug_list" value="${fn:substring(open_list, 0, 8)}" />
		<c:set var="len" value="${fn:length(open_list)}" />
		<c:set var="bID" value="${fn:substring(open_list, 8, len)}" />
		<c:set var="boolVal" value="${fn:contains(bID,',')}" />


		<c:if test="${bug_list =='openList'}">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox">
						<div class="ibox-title">
							<h5>OpeN List</h5>
						</div>

						<div class="ibox-content">

							<div class="table-responsive">
								<table
									class="table table-striped table-bordered table-hover dataTables-example"
									id="closedBugList" name="Closed Bugs">
									<thead>
										<tr>
											<th class="hidden">Bug ID</th>
											<th>Bug ID</th>
											<th style="width: 250px">Title</th>
											<th>Status</th>
											<th>Priority</th>
											<th>Severity</th>
											<th>Category</th>
											<th>Reported By</th>
											<th>Assigned To</th>
											<th>Module</th>
											<th class="hidden">Updated Date</th>
											<th class="hidden">Application ID</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="data" items="${Model.allBugDetails}">

											<c:if test="${bool_val == true}">
												<c:set var="arrayoflist" value="${fn:split(bID,',')}" />
												<c:forEach var="i" begin="0"
													end="${fn:length(arrayoflist)-1}">
													<fmt:parseNumber var="bugId" integerOnly="true"
														type="number" value="${arrayoflist[i]}" />
													<c:if test="${data.bug_id == bugId }">
														<tr style="cursor: pointer;" class="gradeX">
															<td class="hidden">${data.bug_id}</td>
															<td>${data.bug_prefix}</td>
															<td class="xlTextAlignment"
																data-original-title="${data.bug_title}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.bug_title}</td>
															<td><span class="label td_bugStatus"
																style="color: #FFFFFF">${data.bug_status}</span></td>
															<td>${data.bug_priority}</td>
															<td>${data.bug_severity}</td>
															<td>${data.category_name}</td>
															<td class="textAlignment"
																data-original-title="${data.reporter}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.reporter}</td>
															<td class="textAlignment"
																data-original-title="${data.assignee }"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.assignee}</td>
															<td class="textAlignment"
																data-original-title="${data.module_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.module_name}</td>
															<td class="hidden">${data.update_date}</td>
															<td class="hidden">${data.project}</td>
														</tr>
													</c:if>
												</c:forEach>
											</c:if>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</c:if>

		<c:if test="${Model.filterText =='openBug'}">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Open Bug List</h5>
						</div>

						<div class="ibox-content">

							<div class="table-responsive">
								<table
									class="table table-striped table-bordered table-hover dataTables-example"
									id="openBugList" name="Closed Bugs">
									<thead>
										<tr>
											<th class="hidden">Bug ID</th>
											<th>Bug ID</th>
											<th style="width: 250px">Title</th>
											<th>Status</th>
											<th>Priority</th>
											<th>Severity</th>
											<th>Category</th>
											<th>Reported By</th>
											<th>Assigned To</th>
											<th>Module</th>
											<th>Created Date</th>
											<th>Updated Date</th>
											<th class="hidden">Application ID</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="data" items="${Model.allBugDetails}">
											<tr style="cursor: pointer;" class="gradeX">
												<td class="hidden">${data.bug_Id}</td>
												<td>${data.bug_prefix}</td>
												<td class="xlTextAlignment"
													data-original-title="${data.bug_title}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${data.bug_title}</td>
												<td><span class="label td_bugStatus"
													style="color: #FFFFFF">${data.bug_status}</span></td>
												<td>${data.bug_priority}</td>
												<td>${data.bug_severity}</td>
												<td>${data.category_name}</td>
												<td class="textAlignment"
													data-original-title="${data.reporter}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${data.reporter}</td>
												<td class="textAlignment"
													data-original-title="${data.assignee}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${data.assignee}</td>
												<td class="textAlignment"
													data-original-title="${data.module_name}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${data.module_name}</td>
												<td  class="textAlignment"
													data-original-title="${data.create_date}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info" id="cid">${data.create_date}</td>
												<td class="textAlignment"
													data-original-title="${data.updated_date}"
													data-container="body" data-toggle="tooltip"
													data-placement="right" class="issue-info">${data.updated_date}</td>
												<td class="hidden">${data.project}</td>
											</tr>

										</c:forEach>
									</tbody>

								</table>
							</div>

						</div>
					</div>


				</div>
			</div>
		</c:if>

		<c:set var="tmp" value='${Model.filterText}' />
		<c:set var="filtersearch" value="${fn:substring(tmp, 0, 7)}" />
		<c:set var="udate" value='${fn:substring(tmp, 7, 20)}' />

		<c:if test="${filtersearch =='allbugs'}">
			<div class="row">
				<div class="col-lg-12">

					<div>
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#open"
								aria-controls="open" role="tab" data-toggle="tab">Open Bugs</a></li>
							<li role="presentation"><a href="#close"
								aria-controls="close" role="tab" data-toggle="tab">Closed
									Bugs</a></li>
							<li role="presentation"><a href="#progress"
								aria-controls="progress" role="tab" data-toggle="tab">In
									Progress Bugs</a></li>
							<li role="presentation"><a href="#resolved"
								aria-controls="resolved" role="tab" data-toggle="tab">Resolved
									Bugs</a></li>
							<li role="presentation"><a href="#reopen"
								aria-controls="reopen" role="tab" data-toggle="tab">Reopened
									Bugs</a></li>

						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="open">
								<div class="ibox">
									<div class="ibox-content">
										<div class="table-responsive">
											<table
												class="table table-striped table-bordered table-hover dataTables-example"
												id="table" name="Closed Bugs">
												<thead>
													<h3>Open Bugs</h3>
													<tr>
														<th class="hidden">Bug ID</th>
														<th>Bug ID</th>
														<th style="width: 250px">Title</th>
														<th>Status</th>
														<th>Priority</th>
														<th>Severity</th>
														<th>Category</th>
														<th>Reported By</th>
														<th>Assigned To</th>
														<th>Module</th>
														<th class="hidden">Updated Date</th>
														<th class="hidden">Application ID</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data" items="${Model.UpdatedDateBugList}">

														<c:if test="${udate == data.weekday}">
															<c:if test="${data.bug_status == 'Open'}">
																<tr style="cursor: pointer;" class="gradeX">
																	<td class="hidden">${data.bug_Id}</td>
																	<td>${data.bug_prefix}</td>
																	<td class="xlTextAlignment"
																		data-original-title="${data.bug_title}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.bug_title}</td>
																	<td><span class="label td_bugStatus"
																		style="color: #FFFFFF">${data.bug_status}</span></td>
																	<td>${data.bug_priority}</td>
																	<td>${data.bug_severity}</td>
																	<td>${data.category_name}</td>
																	<td class="textAlignment"
																		data-original-title="${data.reporter}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.reporter}</td>
																	<td class="textAlignment"
																		data-original-title="${data.assignee}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.assignee}</td>
																	<td class="textAlignment"
																		data-original-title="${data.module_name}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.module_name}</td>
																	<td class="hidden">${data.update_date}</td>
																	<td class="hidden">${data.project}</td>
																</tr>
															</c:if>
														</c:if>
													</c:forEach>
												</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="close">
								<div class="ibox">
									<div class="ibox-content">
										<div class="table-responsive">
											<table
												class="table table-striped table-bordered table-hover dataTables-example"
												id="table" name="Closed Bugs">
												<thead>
													<h3>Closed Bugs</h3>
													<tr>
														<th class="hidden">Bug ID</th>
														<th>Bug ID</th>
														<th style="width: 250px">Title</th>
														<th>Status</th>
														<th>Priority</th>
														<th>Severity</th>
														<th>Category</th>
														<th>Reported By</th>
														<th>Assigned To</th>
														<th>Module</th>
														<th class="hidden">Updated Date</th>
														<th class="hidden">Application ID</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data" items="${Model.UpdatedDateBugList}">

														<c:if test="${udate == data.weekday}">
															<c:if test="${data.bug_status == 'Closed'}">
																<tr style="cursor: pointer;" class="gradeX">
																	<td class="hidden">${data.bug_Id}</td>
																	<td>${data.bug_prefix}</td>
																	<td class="xlTextAlignment"
																		data-original-title="${data.bug_title}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.bug_title}</td>
																	<td><span class="label td_bugStatus"
																		style="color: #FFFFFF">${data.bug_status}</span></td>
																	<td>${data.bug_priority}</td>
																	<td>${data.bug_severity}</td>
																	<td>${data.category_name}</td>
																	<td class="textAlignment"
																		data-original-title="${data.reporter}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.reporter}</td>
																	<td class="textAlignment"
																		data-original-title="${data.assignee}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.assignee}</td>
																	<td class="textAlignment"
																		data-original-title="${data.module_name}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.module_name}</td>
																	<td class="hidden">${data.update_date}</td>
																	<td class="hidden">${data.project}</td>
																</tr>
															</c:if>
														</c:if>
													</c:forEach>
												</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane" id="progress">
								<div class="ibox">
									<div class="ibox-content">
										<div class="table-responsive">
											<table
												class="table table-striped table-bordered table-hover dataTables-example"
												id="table" name="Closed Bugs">
												<thead>
													<h3>In Progress Bugs</h3>
													<tr>
														<th class="hidden">Bug ID</th>
														<th>Bug ID</th>
														<th style="width: 250px">Title</th>
														<th>Status</th>
														<th>Priority</th>
														<th>Severity</th>
														<th>Category</th>
														<th>Reported By</th>
														<th>Assigned To</th>
														<th>Module</th>
														<th class="hidden">Updated Date</th>
														<th class="hidden">Application ID</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data" items="${Model.UpdatedDateBugList}">

														<c:if test="${udate == data.weekday}">
															<c:if test="${data.bug_status == 'In Progress'}">
																<tr style="cursor: pointer;" class="gradeX">
																	<td class="hidden">${data.bug_Id}</td>
																	<td>${data.bug_prefix}</td>
																	<td class="xlTextAlignment"
																		data-original-title="${data.bug_title}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.bug_title}</td>
																	<td><span class="label td_bugStatus"
																		style="color: #FFFFFF">${data.bug_status}</span></td>
																	<td>${data.bug_priority}</td>
																	<td>${data.bug_severity}</td>
																	<td>${data.category_name}</td>
																	<td class="textAlignment"
																		data-original-title="${data.reporter}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.reporter}</td>
																	<td class="textAlignment"
																		data-original-title="${data.assignee}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.assignee}</td>
																	<td class="textAlignment"
																		data-original-title="${data.module_name}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.module_name}</td>
																	<td class="hidden">${data.update_date}</td>
																	<td class="hidden">${data.project}</td>
																</tr>
															</c:if>
														</c:if>
													</c:forEach>
												</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane" id="resolved">
								<div class="ibox">
									<div class="ibox-content">
										<div class="table-responsive">
											<table
												class="table table-striped table-bordered table-hover dataTables-example"
												id="table" name="Closed Bugs">
												<thead>
													<h3>Resolved Bugs</h3>
													<tr>
														<th class="hidden">Bug ID</th>
														<th>Bug ID</th>
														<th style="width: 250px">Title</th>
														<th>Status</th>
														<th>Priority</th>
														<th>Severity</th>
														<th>Category</th>
														<th>Reported By</th>
														<th>Assigned To</th>
														<th>Module</th>
														<th class="hidden">Updated Date</th>
														<th class="hidden">Application ID</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data" items="${Model.UpdatedDateBugList}">

														<c:if test="${udate == data.weekday}">
															<c:if test="${data.bug_status == 'Resolved'}">
																<tr style="cursor: pointer;" class="gradeX">
																	<td class="hidden">${data.bug_Id}</td>
																	<td>${data.bug_prefix}</td>
																	<td class="xlTextAlignment"
																		data-original-title="${data.bug_title}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.bug_title}</td>
																	<td><span class="label td_bugStatus"
																		style="color: #FFFFFF">${data.bug_status}</span></td>
																	<td>${data.bug_priority}</td>
																	<td>${data.bug_severity}</td>
																	<td>${data.category_name}</td>
																	<td class="textAlignment"
																		data-original-title="${data.reporter}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.reporter}</td>
																	<td class="textAlignment"
																		data-original-title="${data.assignee}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.assignee}</td>
																	<td class="textAlignment"
																		data-original-title="${data.module_name}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.module_name}</td>
																	<td class="hidden">${data.update_date}</td>
																	<td class="hidden">${data.project}</td>
																</tr>
															</c:if>
														</c:if>
													</c:forEach>
												</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane" id="reopen">
								<div class="ibox">
									<div class="ibox-content">
										<div class="table-responsive">
											<table
												class="table table-striped table-bordered table-hover dataTables-example"
												id="table" name="Closed Bugs">
												<thead>
													<h3>Reopened Bugs</h3>
													<tr>
														<th class="hidden">Bug ID</th>
														<th>Bug ID</th>
														<th style="width: 250px">Title</th>
														<th>Status</th>
														<th>Priority</th>
														<th>Severity</th>
														<th>Category</th>
														<th>Reported By</th>
														<th>Assigned To</th>
														<th>Module</th>
														<th class="hidden">Updated Date</th>
														<th class="hidden">Application ID</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="data" items="${Model.UpdatedDateBugList}">

														<c:if test="${udate == data.weekday}">
															<c:if test="${data.bug_status == 'Reopened'}">
																<tr style="cursor: pointer;" class="gradeX">
																	<td class="hidden">${data.bug_Id}</td>
																	<td>${data.bug_prefix}</td>
																	<td class="xlTextAlignment"
																		data-original-title="${data.bug_title}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.bug_title}</td>
																	<td><span class="label td_bugStatus"
																		style="color: #FFFFFF">${data.bug_status}</span></td>
																	<td>${data.bug_priority}</td>
																	<td>${data.bug_severity}</td>
																	<td>${data.category_name}</td>
																	<td class="textAlignment"
																		data-original-title="${data.reporter}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.reporter}</td>
																	<td class="textAlignment"
																		data-original-title="${data.assignee}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.assignee}</td>
																	<td class="textAlignment"
																		data-original-title="${data.module_name}"
																		data-container="body" data-toggle="tooltip"
																		data-placement="right" class="issue-info">${data.module_name}</td>
																	<td class="hidden">${data.update_date}</td>
																	<td class="hidden">${data.project}</td>
																</tr>
															</c:if>
														</c:if>
													</c:forEach>
												</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>

				</div>

			</div>
		</c:if>
		
		
		
		
		
		<!-- InProgressList -->
<c:set var="inprogress_list" value='${Model.filterText}' />
		<c:set var="b_list" value="${fn:substring(inprogress_list, 0, 14)}" />
		<c:set var="end_len" value="${fn:length(inprogress_list)}" />
		<c:set var="bugID" value="${fn:substring(inprogress_list,15, end_len)}" />
		<c:set var="bool_val" value="${fn:contains(bugID,',')}" />

		<c:if test="${b_list=='InProgressList'}">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Inprogress Bugs</h5>
						</div>
						<div class="ibox-content">

							<div class="table-responsive">
								<table
									class="table table-striped table-bordered table-hover dataTables-example"
									id="InProgressList" name="Inprogress Bugs">
									<thead>
										<tr>
											<th class="hidden">Bug ID</th>
											<th>Bug ID</th>
											<th style="width: 250px">Title</th>
											<th>Status</th>
											<th>Priority</th>
											<th>Severity</th>
											<th>Category</th>
											<th>Reported By</th>
											<th>Assigned To</th>
											<th>Module</th>
											<th class="hidden">Updated Date</th>
											<th class="hidden">Application ID</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="data" items="${Model.allIPL}">
											<c:if test="${bool_val == true}">
                                           
												<c:set var="arrayoflist" value="${fn:split(bugID,',')}" />
												<c:forEach var="i" begin="0"
													end="${fn:length(arrayoflist)-1}">
													<fmt:parseNumber var="bugId" integerOnly="true"
														type="number" value="${arrayoflist[i]}" />
													<c:if test="${data.bug_id == bugId }">
														<tr style="cursor: pointer;" class="gradeX">
															<td class="hidden">${data.bug_id}</td>
															<td>${data.bug_prefix}</td>
															<td class="xlTextAlignment"
																data-original-title="${data.bug_title}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.bug_title}</td>
															<td><span class="label td_bugStatus"
																style="color: #FFFFFF">${data.bug_status}</span></td>
															<td>${data.bug_priority}</td>
															<td>${data.bug_severity}</td>
															<td>${data.category_name}</td>
															<td class="textAlignment"
																data-original-title="${data.reporter}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.reporter}</td>
															<td class="textAlignment"
																data-original-title="${data.assignee }"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.assignee}</td>
															<td class="textAlignment"
																data-original-title="${data.module_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.module_name}</td>
															<td class="hidden">${data.update_date}</td>
															<td class="hidden">${data.project}</td>
														</tr>
													</c:if>
												</c:forEach>
											</c:if>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</c:if>

		<!-- End IPL -->


<!-- Resolved List -->

<c:set var="resolved_list" value='${Model.filterText}' />
		<c:set var="b_list" value="${fn:substring(resolved_list, 0, 12)}" />
		<c:set var="end_len" value="${fn:length(resolved_list)}" />
		<c:set var="bugID" value="${fn:substring(resolved_list,12, end_len)}" />
		<c:set var="bool_val" value="${fn:contains(bugID,',')}" />

		
		<c:if test="${b_list=='ResolvedList'}">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Resolved Bugs</h5>
						</div>
						<div class="ibox-content">

							<div class="table-responsive">
								<table
									class="table table-striped table-bordered table-hover dataTables-example"
									id="ResolvedList" name="Resolved Bugs">
									<thead>
										<tr>
											<th class="hidden">Bug ID</th>
											<th>Bug ID</th>
											<th style="width: 250px">Title</th>
											<th>Status</th>
											<th>Priority</th>
											<th>Severity</th>
											<th>Category</th>
											<th>Reported By</th>
											<th>Assigned To</th>
											<th>Module</th>
											<th class="hidden">Updated Date</th>
											<th class="hidden">Application ID</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="data" items="${Model.allIPL1}">
											<c:if test="${bool_val == true}">  
											
												<c:set var="arrayoflist" value="${fn:split(bugID,',')}" />
												<c:forEach var="i" begin="0"
													end="${fn:length(arrayoflist)-1}">
													<fmt:parseNumber var="bugId" integerOnly="true"
														type="number" value="${arrayoflist[i]}" />
													<c:if test="${data.bug_id == bugId }">
														<tr style="cursor: pointer;" class="gradeX">
															<td class="hidden">${data.bug_id}</td>
															<td>${data.bug_prefix}</td>
															<td class="xlTextAlignment"
																data-original-title="${data.bug_title}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.bug_title}</td>
															<td><span class="label td_bugStatus"
																style="color: #FFFFFF">${data.bug_status}</span></td>
															<td>${data.bug_priority}</td>
															<td>${data.bug_severity}</td>
															<td>${data.category_name}</td>
															<td class="textAlignment"
																data-original-title="${data.reporter}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.reporter}</td>
															<td class="textAlignment"
																data-original-title="${data.assignee }"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.assignee}</td>
															<td class="textAlignment"
																data-original-title="${data.module_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.module_name}</td>
															<td class="hidden">${data.update_date}</td>
															<td class="hidden">${data.project}</td>
														</tr>
													</c:if>
												</c:forEach>
											</c:if>  
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</c:if>

<!-- End IPL1 -->



<!-- Reopened List -->

<c:set var="reopened_list" value='${Model.filterText}' />
		<c:set var="b_list" value="${fn:substring(reopened_list, 0, 12)}" />
		<c:set var="end_len" value="${fn:length(reopened_list)}" />
		<c:set var="bugID" value="${fn:substring(reopened_list,12, end_len)}" />
		<c:set var="bool_val" value="${fn:contains(bugID,',')}" />

		
		<c:if test="${b_list=='ReopenedList'}">
		<h1>hello</h1>
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Reopened Bugs</h5>
						</div>
						<div class="ibox-content">

							<div class="table-responsive">
								<table
									class="table table-striped table-bordered table-hover dataTables-example"
									id="ReopenedList" name="Resolved Bugs">
									<thead>
										<tr>
											<th class="hidden">Bug ID</th>
											<th>Bug ID</th>
											<th style="width: 250px">Title</th>
											<th>Status</th>
											<th>Priority</th>
											<th>Severity</th>
											<th>Category</th>
											<th>Reported By</th>
											<th>Assigned To</th>
											<th>Module</th>
											<th class="hidden">Updated Date</th>
											<th class="hidden">Application ID</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="data" items="${Model.allIPL2}">
											<c:if test="${bool_val == true}">  
											
												<c:set var="arrayoflist" value="${fn:split(bugID,',')}" />
												<c:forEach var="i" begin="0"
													end="${fn:length(arrayoflist)-1}">
													<fmt:parseNumber var="bugId" integerOnly="true"
														type="number" value="${arrayoflist[i]}" />
													<c:if test="${data.bug_id == bugId }">
														<tr style="cursor: pointer;" class="gradeX">
															<td class="hidden">${data.bug_id}</td>
															<td>${data.bug_prefix}</td>
															<td class="xlTextAlignment"
																data-original-title="${data.bug_title}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.bug_title}</td>
															<td><span class="label td_bugStatus"
																style="color: #FFFFFF">${data.bug_status}</span></td>
															<td>${data.bug_priority}</td>
															<td>${data.bug_severity}</td>
															<td>${data.category_name}</td>
															<td class="textAlignment"
																data-original-title="${data.reporter}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.reporter}</td>
															<td class="textAlignment"
																data-original-title="${data.assignee }"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.assignee}</td>
															<td class="textAlignment"
																data-original-title="${data.module_name}"
																data-container="body" data-toggle="tooltip"
																data-placement="right" class="issue-info">${data.module_name}</td>
															<td class="hidden">${data.update_date}</td>
															<td class="hidden">${data.project}</td>
														</tr>
													</c:if>
												</c:forEach>
											</c:if>  
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</c:if>

<!-- End IPL2 -->
	</div>
</div> <!-- end .md-skin -->
<script type="text/javascript">
	$(window).load(function() {
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
		
		var page= <%= request.getParameter("page") %>;
		if(page == null){
		 page = 1;
		}
		
		
		var allBugCount='${Model.allBugsCount}';

		var pageSize='${Model.pageSize}';

		var lastRec=((page-1)*pageSize+parseInt(pageSize));
		
		if(lastRec>allBugCount)
			lastRec=allBugCount;
		
		var showCount = ((page-1)*pageSize+1);
		
		var filterText = '${Model.filterText}';
		
		if(filterText == "closed"){
			if(showCount <= lastRec){
				 $("#closedBugList_info").html("Showing "+showCount+" to "+lastRec+" of "+allBugCount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="prevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="nextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
			}else{
				 $("#closedBugList_info").html("");
			}
		}
		else if(filterText == "open1"){
			if(showCount <= lastRec){
				 $("#openBugList_info").html("Showing "+showCount+" to "+lastRec+" of "+allBugCount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="prevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="nextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
			}else{
				 $("#openBugList_info").html("");
			}
		}else if(filterText == "assigned"){
			if(showCount <= lastRec){
				 $("#assignedToMeBugList_info").html("Showing "+showCount+" to "+lastRec+" of "+allBugCount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="prevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="nextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
			}else{
				 $("#assignedToMeBugList_info").html("");
			}
		}else if(filterText == "immediate"){
			if(showCount <= lastRec){
				 $("#immediateBugList_info").html("Showing "+showCount+" to "+lastRec+" of "+allBugCount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="prevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="nextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
			}else{
				 $("#immediateBugList_info").html("");
			}
		}
		
		else if(filterText == "InProgressList")
		{
			if(showCount <= lastRec){
				 $("#viewBugTable_info").html("Showing "+showCount+" to "+lastRec+" of "+allBugCount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="prevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="nextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
			 }else{
				 $("#viewBugTable_info").html("");
			 }
		}
		
		else if(filterText == "ResolvedList")
		{
			if(showCount <= lastRec){
				 $("#viewBugTable_info").html("Showing "+showCount+" to "+lastRec+" of "+allBugCount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="prevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="nextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
			 }else{
				 $("#viewBugTable_info").html("");
			 }
		}
		
		else if(filterText == "allbug")
		{
			if(showCount <= lastRec){
				 $("#viewBugTable_info").html("Showing "+showCount+" to "+lastRec+" of "+allBugCount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="prevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="nextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
			 }else{
				 $("#viewBugTable_info").html("");
			 }
		}else if(filterText == "allbugs")
		{
			if(showCount <= lastRec){
				 $("#viewBugTable_info").html("Showing "+showCount+" to "+lastRec+" of "+allBugCount+" entries " + '<div class="btn-group">	<button type="button" class="btn btn-white btn-sm" id="prevBtn"><i class="fa fa-chevron-left"></i></button>	<button type="button" class="btn btn-white btn-sm" id="nextBtn"><i class="fa fa-chevron-right"></i> </button> </div>');
			 }else{
				 $("#viewBugTable_info").html("");
			 }
		}
		
		if(page == 1){
		 $("#prevBtn").attr('disabled','disabled');
		}

		if(lastRec == allBugCount){
			$("#nextBtn").attr('disabled','disabled');
		}

		$("#prevBtn").click(function() {
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");
			
			if(page == 1)
				window.location.href = "btdashboardlink";
			else
				window.location.href = "btdashboardlink?page="+(page - 1);
		});

		$("#nextBtn").click(function(){
			$("#barInMenu").removeClass("hidden");
			$("#wrapper").addClass("hidden");
			
			window.location.href = "btdashboardlink?page="+(page + 1);
		});
		
		
	});

	$(function() {
		var appFlag='${Model.appFlag}';
		if(appFlag=='true'){
			if ($(window).width() >= 768) {
				// is desktop
				$('.firstAppName').html(
						'All Applications <span class="caret"></span>');
			} else {
				// is phone or tablet
				$('.firstAppName').html('All<span class="caret"></span>');
			}
		}
		
	});
	
	$(document).ready(function() {
		setSpanColor();

	});
	var setSpanColor = function() {
		$('.td_bugStatus').each(function() {
			var stat = $(this).text();
			if (stat == 'New') {
				$(this).css("background-color", "${Model.New}");
			} else if (stat == 'Assigned') {
				$(this).css("background-color", "${Model.Assigned}");
			} else if (stat == 'Dev in Progress') {
				$(this).css("background-color", "${Model.Fixed}");
			} else if (stat == 'Ready for QA') {
				$(this).css("background-color", "${Model.Verified}");
			} else if (stat == 'Closed') {
				$(this).css("background-color", "${Model.Closed}");
			} else if (stat == 'Reject') {
				$(this).css("background-color", "${Model.Reject}");
			}

		});
	}
</script>

<script>
	$(document).ready(
			function() {
				
				var templateName=$(".dataTables-example").attr("name");
				
				var table = $('.dataTables-example').DataTable(
						{
							dom : '<"html5buttons"B>lTfgitp',
							buttons : [
									{
										extend : 'csv',
										title : templateName,
										exportOptions: {
						                    columns: ':visible'
						                }
									},
									{
										extend : 'excel',
										title : templateName,
										exportOptions: {
						                    columns: ':visible'
						                }
									},
									{
										extend : 'pdf',
										title : templateName,
										exportOptions: {
						                    columns: ':visible'
						                }
									},

									{
										extend : 'print',
										exportOptions: {
						                    columns: ':visible'
						                },
										customize : function(win) {
											$(win.document.body).addClass(
													'white-bg');
											$(win.document.body).css(
													'font-size', '10px');

											$(win.document.body).find('table')
													.addClass('compact').css(
															'font-size',
															'inherit');
										}
									} ],
									"paging" : true,
									"lengthChange" : false,
									"searching" : true,
									"ordering" : true
						});

				// Apply the filter
			    $("#openBugList thead input").on( 'keyup change', function () {
			    	table
			            .columns( $(this).parent().index()+':visible' )
			            .search( this.value )
			            .draw();
			    	
			    	setSpanColor();
			    } );
				
			 // Apply the filter
			    $("#immediateBugList thead input").on( 'keyup change', function () {
			    	table
			            .columns( $(this).parent().index()+':visible' )
			            .search( this.value )
			            .draw();
			    	
			    	setSpanColor();
			    } );
			 
			 // Apply the filter
			    $("#assignedToMeBugList thead input").on( 'keyup change', function () {
			    	table
			            .columns( $(this).parent().index()+':visible' )
			            .search( this.value )
			            .draw();
			    	
			    	setSpanColor();
			    } );
			 
			});


	$("#openBugList tbody tr").click(function() {
		var bugId = $(this).children('td').first().html();
		var projectId = $(this).children('td:eq(12)').html();
		console.log(projectId);
	 	$.post("setidfordashbugs", {
			bugId : bugId,
			projectId : projectId
		}, function(response) {
			if (response == true) {
				window.open('bugsummary', '_blank');
			} else {
				window.location.href = "500";
			}

		});
	});
	$("#immediateBugList tbody tr").click(function() {
		var bugId = $(this).children('td').first().html();
		var projectId = $(this).children('td:eq(11)').html();
		console.log(projectId);
	 	$.post("setidfordashbugs", {
			bugId : bugId,
			projectId : projectId
		}, function(response) {
			if (response == true) {
				window.open('bugsummary', '_blank');
			} else {
				window.location.href = "500";
			}

		});
	});
	$("#closedBugList tbody tr").click(function() {
		var bugId = $(this).children('td').first().html();
		var projectId = $(this).children('td:eq(11)').html();
		console.log(projectId);
	 	$.post("setidfordashbugs", {
			bugId : bugId,
			projectId : projectId
		}, function(response) {
			if (response == true) {
				window.open('bugsummary', '_blank');
			} else {
				window.location.href = "500";
			}
		});
	});$("#assignedToMeBugList tbody tr").click(function() {
		var bugId = $(this).children('td').first().html();
		var projectId = $(this).children('td:eq(11)').html();
		console.log(projectId);
	 	$.post("setidfordashbugs", {
			bugId : bugId,
			projectId : projectId
		}, function(response) {
			if (response == true) {
				window.open('bugsummary', '_blank');
			} else {
				window.location.href = "500";
			}

		});
	});
	
	$("#ReopenedList tbody tr").click(function() {
		var bugId = $(this).children('td').first().html();
		var projectId = $(this).children('td:eq(11)').html();
		console.log(projectId);
	 	$.post("setidfordashbugs", {
			bugId : bugId,
			projectId : projectId
		}, function(response) {
			if (response == true) {
				window.open('bugsummary', '_blank');
			} else {
				window.location.href = "500";
			}

		});
	});
	
	
	$("#ResolvedList tbody tr").click(function() {
		var bugId = $(this).children('td').first().html();
		var projectId = $(this).children('td:eq(11)').html();
		console.log(projectId);
	 	$.post("setidfordashbugs", {
			bugId : bugId,
			projectId : projectId
		}, function(response) {
			if (response == true) {
				window.open('bugsummary', '_blank');
			} else {
				window.location.href = "500";
			}

		});
	});
	
	
	$("#InProgressList tbody tr").click(function() {
		console.log(InProgressList);
		var bugId = $(this).children('td').first().html();
		var projectId = $(this).children('td:eq(11)').html();
		console.log(projectId);
	 	$.post("setidfordashbugs", {
			bugId : bugId,
			projectId : projectId
		}, function(response) {
			if (response == true) {
				window.open('bugsummary', '_blank');
			} else {
				window.location.href = "500";
			}

		});
	});
	

	
	
	
</script>
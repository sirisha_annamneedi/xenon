<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
	<div class="wrapper wrapper-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Test Performance Analysis (Thread Vs Elapse Time)</h5>

					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-6">
								<div class="ibox-content">
									<div>
										<canvas id="morris-area-chart" height="150"></canvas>

									</div>

								</div>

							</div>

						
							<div class="col-lg-6">
								<div class="ibox-content">
									<div>
		                                <canvas id="barChartDiv" height="150"></canvas>
		                            </div>


								</div>

							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end .md-skin -->


<script>
	$(window)
			.load(
					function() {

						$('body').removeClass("white-bg");
						$("#barInMenu").addClass("hidden");
						$("#wrapper").removeClass("hidden");

						var graphData = '${Model.ResponseGraph}';
						var graphXData = [];
						var graphGData = [];

						graphData = JSON.parse(graphData);
						
					 	graphXData= $.unique(graphData.map(function (d) {
						    return d.threadName;}));
						
						graphGData= $.unique(graphData.map(function (d) {
						    return d.label;})); 

						var responseDataSet = [];
						var responseBarDataSet = [];

						for (var j = 0; j < graphGData.length; j++) {
							var gLable = graphGData[j];
							var tempArray = [];

							for (var i = 0; i < graphData.length; i++) {

								if (gLable == graphData[i].label) {
									var elapse = graphData[i].elapsed;
									tempArray.push(elapse);
								}
							}
							 var bagColor=getRandomColor();
							responseDataSet.push({
								label : gLable,
								backgroundColor : 'rgba(26,179,148,0.0)',
								borderColor :bagColor ,
								data : tempArray
							});
							
							
							responseBarDataSet.push({
								label : gLable,
								backgroundColor : bagColor,
					            hoverBackgroundColor: getRandomColor(),
								borderColor : bagColor,
								data : tempArray
							});
						}


						var lineData = {
							labels : graphXData,
							datasets:responseDataSet
						};

						var lineOptions = {
							responsive : true
						};

						var ctx = document.getElementById("morris-area-chart")
								.getContext("2d");
						new Chart(ctx, {
							type : 'line',
							data : lineData,
							options : lineOptions
						});
						
						
						
						var barData1 = {
								labels : graphXData,
								datasets:responseBarDataSet
							};

					    var barOptions1 = {
					        scaleBeginAtZero: true,
					        scaleShowGridLines: true,
					        scaleGridLineWidth: 1,
					        barShowStroke: true,
					        barStrokeWidth: 2,
					        barValueSpacing: 5,
					        barDatasetSpacing: 1,
					        responsive: true
					    }


					    var ctx1 = document.getElementById("barChartDiv");
					    var myNewChart1 = new Chart(ctx1, {
						    type: 'bar',
						    data: barData1,
						    options: barOptions1
						});
					    

					});
	
	
	
	function getRandomColor() {
	    var letters = '0123456789ABCDEF'.split('');
	    var color = '#';
	    for (var i = 0; i < 6; i++ ) {
	        color += letters[Math.floor(Math.random() * 16)];
	    }
	    return color;
	}
	
</script>
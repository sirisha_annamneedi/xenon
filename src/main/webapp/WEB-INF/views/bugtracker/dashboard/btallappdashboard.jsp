<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="md-skin">
	<div class="wrapper wrapper-content">
		<div class="row">
			<div class="col-lg-3">
				<div class="ibox float-e-margins dashlink"
					onclick="dashLink('closed')">
					<div class="ibox-title">
						<h5 title="Closed bug count">Closed Bugs</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins text-success">${Model.bugCount.status_closed}</h1>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="ibox float-e-margins dashlink"
					onclick="dashLink('open')">
					<div class="ibox-title">
						<h5 title="Bugs which are not closed">Open Bugs</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins text-success">${Model.bugCount.status_new}</h1>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="ibox float-e-margins dashlink"
					onclick="dashLink('assigned')">
					<div class="ibox-title">
						<h5 title="Bugs Assigned to me">Assigned to Me Bugs</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins text-success">${Model.bugCount.assigned_to_me}</h1>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="ibox float-e-margins dashlink"
					onclick="dashLink('immediate')">
					<div class="ibox-title">
						<h5 title="Immediate Bugs">Immediate Priority Bugs</h5>
					</div>
					<div class="ibox-content" Style="color: red">
						<h1 class="no-margins">${Model.bugCount.priority}</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Recent Bug Analysis</h5>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-12">
								<div class="ibox-content" style="position: relative">
									<div id="morris-bar-chart"></div>

								</div>
								<div class="ibox-content text-center">
									<div class="" id="divBuilds">
										<div class="row">
											<button class="btn" width="12" height="12"
												style="display: inline-block; background-color: ${Model.New}; color: #FFFFFF"></button>
											<strong>Open</strong>

											<button class="btn" width="12" height="12"
												style="display: inline-block; background-color: ${Model.Assigned}; color: #FFFFFF"></button>
											<strong>In Progress</strong>

											<button class="btn" width="12" height="12"
												style="display: inline-block; background-color: ${Model.Fixed}; color: #FFFFFF"></button>
											<strong>Resolved</strong>
											<button class="btn" width="12" height="12"
												style="display: inline-block; background-color: ${Model.Closed}; color: #FFFFFF"></button>
											<strong>Closed</strong>
											<button class="btn" width="12" height="12"
												style="display: inline-block; background-color: ${Model.Reopened}; color: #FFFFFF"></button>
											<strong>Reopened</strong>
										</div>


									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>All Applications - Open Bugs</h5>
					</div>

					<div class="ibox-content">

						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover "
								id="openBugTable">
								<thead>
								
								<tr id="filterrow">
					                <td class="hidden">Bug ID</td>
									<td><input type="text" class="form-control" style="width :100%" /></td>
									<td><input type="text" class="form-control" style="width :100%" /></td>
									<td><input type="text" class="form-control" style="width :100%" /></td>
									<td><input type="text" class="form-control" style="width :100%" /></td>
									<td><input type="text" class="form-control" style="width :100%" /></td>
									<td><input type="text" class="form-control" style="width :100%" /></td>
									<td><input type="text" class="form-control" style="width :100%" /></td>
									<td><input type="text" class="form-control" style="width :100%" /></td>
									<td><input type="text" class="form-control" style="width :100%" /></td>
									<td class="hidden">Updated Date</td>
									<td class="hidden">Application ID</td>
					            </tr>
					            
									<tr>
										<th class="hidden">Bug ID</th>
										<th>Bug ID</th>
										<th>Status</th>
										<th style="width: 250px">Title</th>
										<th>Application</th>
										<th>Priority</th>
										<th>Severity</th>
										<th>Reported By</th>
										<th>Assigned To</th>
										<th>Created Date</th>
										<th class="hidden">Updated Date</th>
										<th class="hidden">Application Id</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="data" items="${Model.allBugDetails}">
										<tr style="cursor: pointer;" class="gradeX">
											<td class="hidden">${data.bug_id}</td>
											<td>${data.bug_prefix}</td>
											<td><span class="label td_bugStatus"
												style="color: #FFFFFF">${data.bugStatus}</span></td>
											<td class="lgTextAlignment"
												data-original-title="${data.bug_title}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${data.bug_title}</td>
											<td>${data.project_name}</td>
											<td>${data.bugPriority}</td>
											<td>${data.bugSeverity}</td>
											<td class="textAlignment"
												data-original-title="${data.reporterFN} ${data.reporterLN}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${data.reporterFN}
												${data.reporterLN}</td>
											<td class="textAlignment"
												data-original-title="${data.assigneeFN} ${data.assigneeLN}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${data.assigneeFN}
												${data.assigneeLN}</td>
											<td class="textAlignment"
												data-original-title="${data.create_date}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${data.create_date}</td>
											<td class="hidden">${data.update_date}</td>
											<td class="hidden">${data.project}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end .md-skin -->

<script>
	$(window).load(function() {
		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");
	});

	$(document).ready(function() {
		setSpanColor();
	});
	
	$(function() {

		var labels = [];
		var newData = [];
		var fixedData = [];
		var closedData = [];
		var barData = [];
		var graphData = '${Model.graphDataProject}';

		
		graphData = JSON.parse(graphData);

		for (i = 0; i < graphData.length; i++) {

			barData.push({
				y : graphData[i].weekday,
				a : graphData[i].New,
				b : graphData[i].DevinProgress,
				c : graphData[i].ReadyforQA,
				d : graphData[i].Closed,
				e : graphData[i].Reopened
			});
		}

		
		var colorNew = "${Model.New}";
		var colorFixed = "${Model.Fixed}";
		var colorClosed = "${Model.Closed}";
		var colorAssigned = "${Model.Assigned}";
		var colorReopened = "${Model.Reopened}";

		Morris.Bar({
			element : 'morris-bar-chart',
			data : barData,
			xkey : 'y',
			ykeys : [ 'a','b','c','d','e'],
			labels : ["Open","In Progress","Resolved","Closed","Reopened" ],
			hideHover : 'auto',
			resize : true,
			barColors : [ colorNew,colorAssigned, colorFixed, colorClosed ,colorReopened],
		}).on('click', function(i, row){
			  
			  var str1 = 'allbugs';
			  var res = str1.concat(row.y);
			  console.log(res);
			  dashLink(res);
		});

		window.dispatchEvent(new Event('resize'));
	});
	
	var setSpanColor = function() {
		$('.td_bugStatus').each(function() {
			var stat = $(this).text();
			if (stat == 'Open') {
				$(this).css("background-color", "${Model.New}");
			} else if (stat == 'In Progress') {
				$(this).css("background-color", "${Model.Assigned}");
			} else if (stat == 'Resolved') {
				$(this).css("background-color", "${Model.Fixed}");
			} else if (stat == 'Closed') {
				$(this).css("background-color", "${Model.Closed}");
			} else if (stat == 'Reopened') {
				$(this).css("background-color", "${Model.Reject}");
			}

		});
	}
	
	
	$(function() {
		//change selected project name in header
		if ($(window).width() >= 768) {
			// is desktop
			$('.firstAppName').html(
					'All Applications <span class="caret"></span>');
		} else {
			// is phone or tablet
			$('.firstAppName').html('All<span class="caret"></span>');
		}


		//data table for open bugs
		var table = $('#openBugTable').DataTable(
				{
					dom : '<"html5buttons"B>lTfgitp',
					buttons : [
							{
								extend : 'csv',
								title : 'All Applications Open Bugs',
								exportOptions : {
									columns : ':visible'
								}
							},
							{
								extend : 'excel',
								title : 'All Applications Open Bugs',
								exportOptions : {
									columns : ':visible'
								}
							},
							{
								extend : 'pdf',
								title : 'All Applications Open Bugs',
								exportOptions : {
									columns : ':visible'
								}
							},

							{
								extend : 'print',
								exportOptions : {
									columns : ':visible'
								},
								customize : function(win) {
									$(win.document.body).addClass('white-bg');
									$(win.document.body).css('font-size',
											'10px');

									$(win.document.body).find('table')
											.addClass('compact').css(
													'font-size', 'inherit');
								}
							} ],
					"aaSorting" : [ [ 0, "desc" ] ]

				});
		
		// Apply the filter
	    $("#openBugTable thead input").on( 'keyup change', function () {
	    	table
	            .columns( $(this).parent().index()+':visible' )
	            .search( this.value )
	            .draw();
	    	
	    	setSpanColor();
	    } );
		
	});
	$("#openBugTable tbody tr").click(function() {
		var bugId = $(this).children('td').first().html();
		var projectId = $(this).children('td:eq(11)').html();

		$.post("setidfordashbugs", {
			bugId : bugId,
			projectId : projectId
		}, function(response) {
			if (response == true) {
				window.open('bugsummary', '_blank');
			} else {
				window.location.href = "500";
			}

		});
	});
	function dashLink(link) {
		$('body').addClass('white-bg');
		$("#barInMenu").removeClass('hidden');
		$("#wrapper").addClass("hidden");

		var posting = $.post('setBtLinkSession', {
			filterText : link,
			dashboard : "all"
		});

		posting.done(function(data) {
			window.location.href = "btdashboardlink";
		});
	}
</script>
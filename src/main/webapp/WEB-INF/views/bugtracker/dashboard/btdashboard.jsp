

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>	 -->
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/plugins/c3/c3.min.css">
	<div class="wrapper wrapper-content ">
<div class="md-skin">
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-8">
			<h2>
				<ol class="breadcrumb">
					<li><strong>Bug Tracker Dashboard</strong></li>
					<li>${UserCurrentProjectName}</li>

				</ol>
			</h2>
		</div>

	</div>
	<div class="">
		<div class="graph-dashboard"></div>


		<div class="graph-dashboard">
			<div class="col-md-12">
				<div class="gd-title">Recent Test Set Wise Bug</div>
				<div class="row">
					<div class="col-md-6">
						<div class="testc-status mt-20">
							<canvas id="testWiseBug" height="300"></canvas>
							<table class="table table-bordered">
								<thead>
									<tr>
										<th scope="col">Status</th>
										<th scope="col">Immediate</th>
										<th scope="col">High</th>
										<th scope="col">Medium</th>
										<th scope="col">Low</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="data3" items="${Model.bugsWithPriority}">
										<tr>
											<td>${data3.bug_status}</td>
											<td>${data3.Immediate}</td>
											<td>${data3.High}</td>
											<td>${data3.Medium}</td>
											<td>${data3.Low}</td>
										</tr>

									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-6">
						<div class="mt-50">
							<div id="testWiseBugPie"></div>
						</div>

					</div>
				</div>
				<!--  ${Model.bugCount.status_closed}  -->
			</div>
		</div> 


		<div class="graph-dashboard">
			<div class="col-md-12">
				<div class="gd-title">Root Cause Analysis</div>
				<div class="row">
					<div class="col-md-6">
						<div class="testc-status mt-20">
							<canvas id="rootCauseChart" height="300"></canvas>
						</div>
					</div>
					<div class="col-md-6">
						<div class="mt-50">
						<%-- <canvas id="rootCausePie" height="300"></canvas>
 --%>
							<div id="rootCausePie"></div> 

						</div>

					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th scope="col">Category</th>
									<th scope="col">In Progress</th>
									<th scope="col">Resolved</th>
									<th scope="col">Closed</th>
									<th scope="col">Open</th>
									<th scope="col">Reopened</th>
									<th scope="col">Total</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="data5" items="${Model.userWiseBugs}">
									<tr>
										<td class="categoryName">${data5.category_name}</td>
										<td>${data5.inProgressCount}</td>
										<td>${data5.resolvedCount}</td>
										<td>${data5.ClosedCount}</td>
										<td>${data5.OpenCount}</td>
										<td>${data5.ReOpenCount}</td>
										<td class="totalCount">${data5.inProgressCount+data5.resolvedCount+data5.ClosedCount+data5.OpenCount+data5.ReOpenCount}</td>
									</tr>

								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>



			</div>
		</div>

		<!-- 	
		<div class="row">
			<div class="col-lg-10">
			</div>
			<div class="col-lg-1">
				<div class="ibox float-e-margins dashlink">
				</div>
			</div>
		</div>
		
		<br> -->
		<%-- <div class="row">
			<div class="col-lg-4">
				<div class="ibox float-e-margins dashlink"
					onclick="dashLink('closed')">
					<div class="ibox-title">
						<h5 title="Closed bug count">Closed Bugs</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins text-success">${Model.bugCount.status_closed}</h1>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="ibox float-e-margins dashlink"
					onclick="dashLink('openBug')">
					<div class="ibox-title">
						<h5 title="Bugs which are not closed">Open Bugs</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins text-success">${Model.bugCount.status_new}</h1>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="ibox float-e-margins dashlink"
					onclick="dashLink('assigned')">
					<div class="ibox-title">
						<h5 title="Bugs Assigned to me">Assigned to Me Bugs</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins text-success">${Model.bugCount.assigned_to_me}</h1>
					</div>
				</div>
			</div>
		</div> --%>


		<%-- <div class="row">
			<div class="col-md-6">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Recent Bug Analysis</h5>
					</div>
					<div class="ibox-content">
						<canvas id="baBarChart" height="200px"></canvas>
						
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Root Cause Analysis</h5>
					</div>
					<c:if test="${Model.status_closed != 0}">
					<div class="ibox-content">
						<canvas id="doughnutChart" height="200px"></canvas>
					</div></c:if>
					<c:if test="${Model.status_closed == 0}">
					<div class="ibox-content">
						<span class="text-warning">**No Closed bugs found!</span>
					</div></c:if>
					
				</div>
			</div>
		</div>	 --%>
		<!-- 		hide Priority wise Bug Status
 -->
		<%-- <div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Priority wise Bug Status</h5>
					</div>
					<div class="ibox-content">
						<div>
							<canvas id="barChartDiv" height="100"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div> --%>

		<div class="row">

			<div class="col-lg-12">
				<div class="ibox float-e-margins" style="background-color: white;">
					<div class="ibox-title">
						<h5>All Applications - Immediate Bugs</h5>
					</div>

					<div class="ibox-content">

						<div class="table-responsive">
							<table
								class="table table-striped table-bordered table-hover dataTables-example"
								id="immediateBugList">
								<thead>

									<tr id="filterrow">
										<td class="hidden">Bug ID</td>
										<td><input type="text" class="form-control"
											style="width: 90%" /></td>
										<td><input type="text" class="form-control"
											style="width: 100%" /></td>
										<td><input type="text" class="form-control"
											style="width: 100%" /></td>
										<td><input type="text" class="form-control"
											style="width: 100%" /></td>
										<td class="hidden"></td>
									</tr>
									<tr>
										<th class="hidden">Bug ID</th>
										<th style="width: 50px;">ID</th>
										<th>Application</th>
										<th>Title</th>
										<th>Creator</th>
										<th class="hidden">Application Id</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="data" items="${Model.bugTable}">
										<tr style="cursor: pointer;" class="gradeX">
											<td class="hidden">${data.bug_id}</td>
											<td style="width: 50px;">${data.bug_prefix}</td>
											<td class="textAlignment"
												data-original-title="${data.project_name}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${data.project_name}</td>
											<td class="textAlignment"
												data-original-title="${data.bug_title}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${data.bug_title}</td>
											<td class="textAlignment"
												data-original-title="${data.assigneeFN} ${data.assigneeLN}"
												data-container="body" data-toggle="tooltip"
												data-placement="right" class="issue-info">${data.reporterFN}&nbsp;${data.reporterLN}</td>
											<td class="hidden">${data.project}</td>
										</tr>
									</c:forEach>
								</tbody>

							</table>
						</div>

					</div>

				</div>
			</div>
			<p id="demo"></p>
			<script>
				var catLabel = [];
				var catBCount = [];
				var jira_name_release =[];
				var jira_bug_count =[];
				var jira_bug_status = [];
				var bstatus = [];
				var bid = [];
				var catID =[];
				var BugPrefix = [];
				var rID =[];
				rel_ID =[];
				
				var P_relId =[];
				var P_CurrentRelease = [];
				var P_bugPriId =[];
				var P_bugPri = [];
				var P_bugId = [];
				var P_BugCount =[];
				var P_PID = [];
				var P_currentRId = [];
				var P_bugState =[];
				var p_data1 = [];
				
			</script>
			<c:forEach var="dataLabel" items="${Model.CatwisebugCount}">
				<script>
					var temp = '${dataLabel.category_name}';
					var c = '${dataLabel.bCount}';
					var id = '${dataLabel.category_id}';
					var r = '${dataLabel.jira_release_version_id}';
					catLabel.push(temp);
					catBCount.push(c);
					catID.push(id);
					rID.push(r);
				</script>
			</c:forEach>

			<c:forEach var="dataL" items="${Model.JirareleaseBug}">
				<script>
				var temp = '${dataL.jira_release_version_name}';
				var bs = '${dataL.bugStatus}';
				var bc = '${dataL.bugCount}';
				jira_name_release.push(temp);
				jira_bug_count.push(bc);
				jira_bug_status.push(bs);	
				</script>
			</c:forEach>


			<c:forEach var="bug" items="${Model.BugList}">
				<script>
				var bs = '${bug.bugStatus}';
				var bc = '${bug.bug_id}';
				var r = '${bug.jira_release_version_id}';
				bid.push(bc);
				bstatus.push(bs);
				rel_ID.push(r);
				</script>
			</c:forEach>
			<c:forEach var="Pbug" items="${Model.BugCountPerPriority}">
				<script>
				var bs = '${Pbug.bug_priority}';
				var pre = '${Pbug.bugPCount}';
				P_bugPriId.push(bs);
				P_BugCount.push(pre);
				</script>
			</c:forEach>
			<c:forEach var="Pbug1" items="${Model.ReleaseIdWithPriority}">
				<script>
				var r = '${Pbug1.RID}';
				var p = '${Pbug1.PID}';
				var b = '${Pbug1.bug_id}';
				P_relId.push(r);
				P_PID.push(p);
				P_bugId.push(b);
				</script>
			</c:forEach>
			<c:forEach var="Pbug2" items="${Model.CurrentRelease}">
				<script>
				var rn = '${Pbug2.R_name}';
				var rv = '${Pbug2.R_vid}';
				P_CurrentRelease.push(rn);
				P_currentRId.push(rv);
				</script>
			</c:forEach>



			<c:forEach var="data" items="${Model.immediateList}">
				<script>					
		      var data1 ='${data.module_name}'									
	           P_data1.push(data1);
			</script>
			</c:forEach>




			<!-- <div class="col-lg-6">
				<div class="ibox float-e-margins">
					<div class="client-detail" style="max-height: 483px">
						<div class="full-height-scroll">
							<div class="ibox-title">
								<h5>Recent Activities</h5>
								<div class="ibox-tools">
									<span class="label label-warning-light pull-right">
										Recent &nbsp;${fn:length(Model.topActivities)}
										&nbsp;Activities</span>
								</div>
							</div>
							<div class="ibox-content">
								<div class="feed-activity-list">
									<c:forEach var="topActivities" items="${Model.topActivities}"
										varStatus="loop">
										<div class="feed-element">
											<a class="pull-left"> <img alt="image" class="img-circle"
												src="data:image/jpg;base64,${topActivities.user_image}">
											</a>
											<div class="media-body ">


												<c:set var="date"
													value="${fn:split(topActivities.activity_date,' ')}" />


												<c:choose>

													<c:when test="${topActivities.TimeDiff < 60 }">
														<strong class="pull-right">${topActivities.TimeDiff}
															min ago</strong>
													</c:when>

													<c:when
														test="${topActivities.TimeDiff >= 60 && topActivities.TimeDiff < 1440 }">
														<fmt:parseNumber var="hour" integerOnly="true"
															type="number" value="${topActivities.TimeDiff/60}" />
														<strong class="pull-right">${hour} hr ago </strong>
													</c:when>


													<c:when
														test="${topActivities.TimeDiff >= 1440 && topActivities.TimeDiff < 2880 }">
														<strong class="pull-right"> Yesterday </strong>
													</c:when>
													<c:otherwise>
														<strong class="pull-right">${topActivities.activity_date}
														</strong>
													</c:otherwise>
												</c:choose>

												<strong>${topActivities.first_name}&nbsp;${topActivities.last_name}
												</strong>${topActivities.activityState}.<br> <small
													class="text-muted">${topActivities.activity_date}</small>
												
												<div class="well">
													<div class="client-detail" style="max-height: 80px">
														<div class="full-height-scroll">
															${topActivities.activity_desc}
														</div>
													</div>
												</div>
												
													</div>
												</div>
									</c:forEach>

								</div>

							</div>
						</div>
					</div>
				</div>

			</div>-->
		</div>
	</div>
</div>
</div>
<!-- end .md-skin -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/d3/d3.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/c3/c3.min.js"></script>

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script>

	$(document).ready(
			function() {
				var table = $('.dataTables-example').DataTable(
						{
							dom : '<"html5buttons"B>lTfgitp',
							buttons : [
									{
										extend : 'csv',
										title : 'Open Bugs',
										exportOptions : {
											columns : ':visible'
										}
									},
									{
										extend : 'excel',
										title : 'Open Bugs',
										exportOptions : {
											columns : ':visible'
										}
									},
									{
										extend : 'pdf',
										title : 'Open Bugs',
										exportOptions : {
											columns : ':visible'
										}
									},

									{
										extend : 'print',
										exportOptions : {
											columns : ':visible'
										},
										customize : function(win) {
											$(win.document.body).addClass(
													'white-bg');
											$(win.document.body).css(
													'font-size', '10px');

											$(win.document.body).find('table')
													.addClass('compact').css(
															'font-size',
															'inherit');
										}
									} ],
							"aaSorting" : [ [ 0, "desc" ] ],
							"aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
							"iDisplayLength": 5,
							"pageLength": 5

						});

				// Apply the filter
			    $("#immediateBugList thead input").on( 'keyup change', function () {
			    	table
			            .columns( $(this).parent().index()+':visible' )
			            .search( this.value )
			            .draw();
			    	
			    	setSpanColor();
			    } );
			});

	$('#refreshBugsFromJiraButton').click(function() {
		
			$('#refreshBugsFromJiraID').submit();
		
	});
	$("#openBugList tbody tr").click(function() {
		var bugId = $(this).children('td').first().html();
		var projectId = $(this).children('td:eq(9)').html();
		$.post("setidfordashbugs", {
			bugId : bugId,
			projectId : projectId
		}, function(response) {
			if (response == true) {
				window.open('bugsummary', '_blank');
			} else {
				window.location.href = "500";
			}

		});
	});
	$("#immediateBugList tbody tr").click(function() {
		var bugId = $(this).children('td').first().html();
		var projectId = $(this).children('td:eq(5)').html();
		
		$.post("setidfordashbugs", {
			bugId : bugId,
			projectId : projectId
		}, function(response) {
			if (response == true) {
				//window.open('bugsummary', '_blank');
			} else {
				window.location.href = "500";
			}

		});
	});
	function dashLink(link) {

		$('body').addClass('white-bg');
		$("#barInMenu").removeClass('hidden');
		$("#wrapper").addClass("hidden");

		var posting = $.post('setBtLinkSession', {
			filterText : link,
			dashboard : "projectWise"
		});

		posting.done(function(data) {
			window.location.href = "btdashboardlink";
		});
	}
	
	function dashBugLink(link) {

		$('body').addClass('white-bg');
		$("#barInMenu").removeClass('hidden');
		$("#wrapper").addClass("hidden");

		var posting = $.post('setbtlinktoallbugs', {
			filterText : link,
			dashboard : "projectWise"
		});

		posting.done(function(data) {
			window.location.href = "bugprioritylist";
		});
	}
	
	$(window).load(function() {

		$('body').removeClass("white-bg");
		$("#barInMenu").addClass("hidden");
		$("#wrapper").removeClass("hidden");

	});
	
	var ChartColor = ["#1EC0FB","#2ECC71","#F1948A", "#85C1E9","#C39BD3","#AA85E9", "#2EC7CC","#9C2ECC","#BBFF33","#CC492E","#EA185E","#827017","#F76E85","#FFDD33","#FF7733"];

	$(document).ready(
		function() {

		var labels = [];
		var newData = [];
		var fixedData = [];
		var closedData = [];
		var barData = [];
		var graphData = '${Model.graphData}';
		var release_stat_Data = '${Model.JirareleaseBug}';
		graphData = JSON.parse(graphData);
	
	var baDataSet = [];
	var baLabels = [];
	var baNew = [];
	var baAssign = [];
	var baReadyForQa = [];
	var baClosed = [];
	var baReopened = [];
	var monthLabel = [];
	var weekday = [];
	var dt = [];


	var date=new Date();
	var i=5;
	var j;
	while(i>0){
		

	var diff = date.getDate() - date.getDay() + (date.getDay() === 0 ? -7 : 0);
	var mdate=new Date(date.setDate(diff));
	dt[i]= mdate.toISOString().slice(0,10);
	 i--;
	} 
	var gdLen = graphData.length;
	var newWeekdata = [];

	
	for(i=5;i>0;i--){
		weekday.push(dt[i]);
		var flg = 0;
		
		for(j=0;j<gdLen;j++){
			if(graphData[j]){

				if(graphData[j].weekday == dt[i]){
					newWeekdata.push(graphData[j].weekday);
					baNew.push( graphData[j].New);
					baClosed.push( graphData[j].Closed);
					baAssign.push(graphData[j].DevinProgress);
					baReadyForQa.push(graphData[j].ReadyforQA);
					baReopened.push(graphData[j].Reopened);
					flg = 1;
					break;
					
				} 
			}
		}
		if(flg == 0){
			newWeekdata.push("0");
			baNew.push(0);
			baClosed.push(0);
			baAssign.push(0);
			baReadyForQa.push(0);
			baReopened.push(0);
		}
			
	}
	
	var statusArray = ["Open","In Progress","Resolved","Closed","Reopened"];
	var bscount = [];
	
	var colorNew = "${Model.New}";
	var colorReadyForQA = "${Model.Fixed}";
	var colorFixed = "${Model.Fixed}";
	var colorClosed = "${Model.Closed}";
	var colorAssigned = "${Model.Assigned}";
	var colorReopened = "${Model.Reopened}";
	
	
	var baBarData = {
		    labels: weekday,//["","","","",""],
		    datasets: [
		        {
		            label: "Open",
		            backgroundColor: colorNew,
		            hoverBackgroundColor : colorNew,
		            data: baNew
		        },
		        {
		            label: "In Progress",
		            backgroundColor: colorAssigned,
		            hoverBackgroundColor : colorAssigned,
		            data: baAssign
		        },
		        {
		            label: "Resolved",
		            backgroundColor: colorReadyForQA,
		            hoverBackgroundColor : colorReadyForQA,
		            data: baReadyForQa
		        },
		        {
		            label: "Closed",
		            backgroundColor: colorClosed,
		            hoverBackgroundColor : colorClosed,
		            data: baClosed
		        },
		        {
		            label: "Reopened",
		            backgroundColor: colorReopened,
		            hoverBackgroundColor : colorReopened,
		            data: baReopened
		        }
		    ]
		};
	
	 var baBarOptions = {
	            responsive: true,
	            scales: {
	                yAxes: [{
	                        ticks: {
	                            min: 0,
	                            callback: function(value) {if (value % 1 === 0) {return value;}}
	                        }
	                }]
	            },
	            title: {
	      
	                display: true,
	                //text: weekday,
	                position: 'bottom',
	                fontStyle: 'normal'
	            }
	        };
	 
	 var chartx = document.getElementById("baBarChart");

	 var myBarChart = new Chart(chartx, {
			type: 'bar',
	  		data: baBarData	,
		   	options:baBarOptions
	  });
	 
	 
	 chartx.onclick = function(evt) {
		 
				var activePoint = myBarChart.getElementAtEvent(evt);
			   	var label = activePoint[0]._model.datasetLabel;
			   	var week = activePoint[0]._model.label;
			   	var value = [];
			   	var len = '${Model.BugList}';
			   	var k =[];
			   	var st = [];
			   	var R_ID =[];
			
 		        		for(var x = 0; x < bid.length; x++) 
		       			 {

 		        			if (label == bstatus[x])
 		        			{		 		        			    
 		        				value.push(bid[x]);
 		        			     st = label;
 			       			}
 		        		}
 		
		        var str2;
		        var str3 = value.toString();
		        var res;
		      
		        
		        if(label == 'Closed')
		        {
		        	str2 = 'bugCat,';
		        	res = str2.concat(str3);
		        	dashLink(res);
		        }
		        
 		        else if(label=='Open')
 		        {
 		        	str2 = 'openList,';
 		        	res = str2.concat(str3);
 		        	dashLink(res);
 		        }
  		        else if(label == 'In Progress'){
  		        	str2 = 'InProgressList,';
  		        	res = str2.concat(str3);
  		        	dashLink(res);
 	        	}
  		        else if(label == 'Resolved'){
  		        	str2 = 'ResolvedList,';
		        	res = str2.concat(str3);
 		        	dashLink(res);
  		        }
  		       else if(label == 'Reopened'){
 		        	str2 = 'ReopenedList,';
 		        	res = str2.concat(str3);
		        	dashLink(res);
 		         }
		        		    };
		    

			$(".dt-buttons").addClass("hidden");
			window.dispatchEvent(new Event('resize'));
		
		    
	}
	);
		    
		$(document).ready(
				  function() {

		var bugStatusList = '${Model.CatwisebugCount}';
		//var labelArray = ["Bug in code","Change in requirements","Code migration issue","Enhancement","Environment issue","Functional setup missing","Lack of clear requirements","Not an issue","Production issue","Technical limitation","Technical setup missing","User training issue","Not selected"];
		var labelArray = ["Code Issue","Environment Issue","DB issue","Data issue","UI issue","Invalid issue","Duplicate issue","Design issue","Requirement Gap","Performance Issue","Unknown","Other","Enhancement"];
		var chartBugData = [];
		var linkId = [];
		
		for(var i = 0; i < labelArray.length; i++)
		{
			for(var j = 0; j < catLabel.length; j++)
			{
				if(catLabel[j] == labelArray[i])
				{
					chartBugData[i] = catBCount[j];
					break;
				}
				else
				{
					chartBugData[i] = 0;
					
				}
			}

		}
		
		var statusCount_new = "${Model.statusCount_new}";
		var statusCount_assign = "${Model.statusCount_assign}";
		var status_closed = "${Model.status_closed}";

		var doughnutData = {
			    labels: labelArray,
			    datasets: [
			        {
			            data:chartBugData,
			            backgroundColor:ChartColor,
			           
			        }]
			};
		var doughnutOptions = {
				segmentShowStroke : true,
				segmentStrokeColor : "#fff",
				segmentStrokeWidth : 2,
				percentageInnerCutout : 45, // This is 0 for Pie charts
				animationSteps : 100,
				animationEasing : "easeOutBounce",
				animateRotate : true,
				animateScale : true,
				responsive : true,
				legend: {
					display : true,
					position : 'right'
		        },
		        pieceLabel: {
			         render: 'value' //show values
			      }
			};
		
		var ctx = document.getElementById("doughnutChart");
		var ctxnew = ctx.getContext("2d");
		var myDoughnutChart = new Chart(ctxnew, {
		    type: 'doughnut',
		    data: doughnutData,
		    options: doughnutOptions
		});
		
		ctx.onclick = function(evt) {
		      var activePoints = myDoughnutChart.getElementsAtEvent(evt);
		      if (activePoints[0]) {
		        var chartData = activePoints[0]['_chart'].config.data;
		        var idx = activePoints[0]['_index'];

		        var label = chartData.labels[idx];
		        var value = chartData.datasets[0].data[idx];
		        var categoryID = [];
		        var relID = [];
		        for(var x = 0; x < catLabel.length; x++)
		        {
		        	
		        	if (label == catLabel[x])
		        	{
		        		categoryID.push(catID[x]);
		        		relID.push(rID[x]);
		       		}
		        }
		        var str2 = 'closed';
		        var str3 = relID.toString();
		        var str4 = categoryID.toString();
		        var str5 = str4.concat(str3);
		        var res = str2.concat(str5);
		       dashLink(res);
		      }
		    };
		    
		}
		);
		
		
		
		
	
	$(function () {
		
		var barData1 = ${Model.ReleaseWiseJson}

		var barOptions1 = {
			scaleBeginAtZero : true,
			scaleShowGridLines : true,
			scaleGridLineColor : "rgba(0,0,0,.05)",
			scaleGridLineWidth : 1,
			barShowStroke : true,
			barStrokeWidth : 2,
			barValueSpacing : 5,
			barDatasetSpacing : 1,
			responsive : true,
			scales : {
				yAxes : [ {
					ticks : {
						min : 0,
						callback : function(value) {
							if (value % 1 === 0) {
								return value;
							}
						}
					}
				} ]
			}
		}
		var ctx1 = document.getElementById("barChartDiv");

 	var chart_config = {
			type : 'bar',
			data : barData1,
			options : barOptions1
		};
 	var myNewChart1 =new Chart(ctx1, chart_config);
		
 	ctx1.onclick = function(evt) {
		 
		var activePoints = myNewChart1.getElementsAtEvent(evt);
		var cdata = activePoints[0]['_chart'].config.data;
		var idx = activePoints[0]['_datasetIndex']; 
		var chart_label = activePoints[0]['_model'].datasetLabel;
		var label = activePoints[0]['_model'].label;
		var value = cdata.datasets[0].data[idx];

		var rid = [];
		for(var i =0; i<P_CurrentRelease.length; i++)
		{
			if(label == P_CurrentRelease[i])
			{
				rid.push(P_currentRId[i]);						
			}
		}
		rid = rid.toString();
		//console.log(rid);
		dashBugLink(rid); 

		};
	});
		
// root cause analysis

c3.generate({
    bindto: '#testWiseBugPie',
    data:{
        columns: [

       	    ['Immediate', ${Model.bugsWithPriority[0].Immediate+Model.bugsWithPriority[1].Immediate+Model.bugsWithPriority[2].Immediate+Model.bugsWithPriority[3].Immediate}],
            ['High', ${Model.bugsWithPriority[0].High+Model.bugsWithPriority[1].High+Model.bugsWithPriority[2].High+Model.bugsWithPriority[3].High}],
            ['Medium', ${Model.bugsWithPriority[0].Medium+Model.bugsWithPriority[1].Medium+Model.bugsWithPriority[2].Medium+Model.bugsWithPriority[3].Medium}],
            ['Low', ${Model.bugsWithPriority[0].Low+Model.bugsWithPriority[1].Low+Model.bugsWithPriority[2].Low+Model.bugsWithPriority[3].Low}], 
        ], 
        colors:{
       	    'Immediate': 'rgba(255, 99, 132, 0.9)',
            'High': 'rgba(54, 162, 235, 0.9)',
            'Medium': 'rgba(255, 206, 86, 0.9)',
            'Low': 'rgba(75, 192, 192, 0.9)'
        },
        type : 'pie'
    }
}); 
 var labelArray = [];
 var columnArray = [];
 var text=0;
 
$('.categoryName').each(function(){
	labelArray.push(this.innerHTML); 
});

 $('.totalCount').each(function(){
	columnArray.push(this.innerHTML); 
}); 
 
  for(var i=0;i<columnArray.length;i++){
		text+=columnArray[i];
 }  
  document.getElementById("demo").innerHTML = text;
  
var dataArray=[];
for(var i=0;i<labelArray.length;i++){
	dataArray.push([labelArray[i], columnArray[i]]);  
	
}
var jsObj = {};

 for (var  i = 0; i <= labelArray.length; i++) {
	if(labelArray[i]=='Code Issue'){
		jsObj[labelArray[i]]='rgba(255, 99, 132, 0.9)'; 
		
	}else if(labelArray[i]=='Data issue'){
		jsObj[labelArray[i]]='rgba(54, 162, 235, 0.9)'; 
		
	}else if(labelArray[i]=='DB issue'){
		jsObj[labelArray[i]]='rgba(255, 206, 86, 0.9)'; 
		
	}else if(labelArray[i]=='Environment Issue'){
		jsObj[labelArray[i]]='rgba(75, 192, 192, 0.9)'; 

	}else if(labelArray[i]=='Invalid issue'){
		jsObj[labelArray[i]]='rgba(153, 102, 255, 0.9)'; 

	}else if(labelArray[i]=='UI issue'){
		jsObj[labelArray[i]]='rgba(255, 159, 64, 0.9)'; 
	}	

} 
 
c3.generate({
    bindto: '#rootCausePie',
    data:{ 
    	 columns: dataArray,
    	  colors: jsObj,
          type : 'pie'
    }
    
});  

		
var ctx = document.getElementById("testWiseBug").getContext('2d');
var myChart = new Chart(ctx, {
	type: 'bar',
	data: {
		labels: ["Immediate","High","Medium","Low"],
		datasets: [{
		
		//cancel
			label: 'Cancel',
			backgroundColor: "rgba(128,128,128)",
			data: [${Model.bugsWithPriority[0].Immediate}, ${Model.bugsWithPriority[0].High},${Model.bugsWithPriority[0].Medium} , ${Model.bugsWithPriority[0].Low}],
		
		},
		{//In progress
			label: 'In progress',
			backgroundColor: "rgba(255,206,86,0.9)",
			data: [${Model.bugsWithPriority[1].Immediate}, ${Model.bugsWithPriority[1].High},${Model.bugsWithPriority[1].Medium} , ${Model.bugsWithPriority[1].Low}],
		
		}, {
		//new
			label: 'New',
			backgroundColor: "rgba(34,139,34)",
			data: [${Model.bugsWithPriority[2].Immediate}, ${Model.bugsWithPriority[2].High},${Model.bugsWithPriority[2].Medium} , ${Model.bugsWithPriority[2].Low}],
		
		}, {
			//open
	        label: 'Open',
			backgroundColor: "rgba(139,0,0)",
			data: [${Model.bugsWithPriority[3].Immediate}, ${Model.bugsWithPriority[3].High},${Model.bugsWithPriority[3].Medium} , ${Model.bugsWithPriority[3].Low}]
		
		}
		]
	},
options: {
    tooltips: {
      displayColors: true,
			mode: 'point',
     
    },
    scales: {
      xAxes: [{
    	  barThickness : 30, 
        stacked: true,
        gridLines: {
          display: false,
        }
      
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
		responsive: true,
		maintainAspectRatio: true,
		legend: { position: 'top' },
	}
});

var ctx = document.getElementById("rootCauseChart").getContext('2d');
var myChart = new Chart(ctx, {
	type: 'bar',
	data: {
		labels: labelArray,
		datasets: [{
			
			label: 'In Progress',
			backgroundColor: "rgba(255,206,86,0.9)",
			data: [${Model.userWiseBugs[0].inProgressCount},${Model.userWiseBugs[1].inProgressCount}, ${Model.userWiseBugs[2].inProgressCount}, ${Model.userWiseBugs[3].inProgressCount}, ${Model.userWiseBugs[4].inProgressCount}, ${Model.userWiseBugs[5].inProgressCount}],
		},{
			label: 'Resolved',
			backgroundColor: "rgba(34,139,34)",
			data: [${Model.userWiseBugs[0].resolvedCount},${Model.userWiseBugs[1].resolvedCount}, ${Model.userWiseBugs[2].resolvedCount}, ${Model.userWiseBugs[3].resolvedCount}, ${Model.userWiseBugs[4].resolvedCount}, ${Model.userWiseBugs[5].resolvedCount}],
		},{
			label: 'Closed',
			backgroundColor: "rgba(128,128,128)",
			data: [${Model.userWiseBugs[0].ClosedCount}, ${Model.userWiseBugs[1].ClosedCount}, ${Model.userWiseBugs[2].ClosedCount}, ${Model.userWiseBugs[3].ClosedCount}, ${Model.userWiseBugs[4].ClosedCount}, ${Model.userWiseBugs[5].ClosedCount}],
		}, {
			label: 'Open',
			backgroundColor: "rgba(139,0,0)",
			data: [${Model.userWiseBugs[0].OpenCount}, ${Model.userWiseBugs[1].OpenCount}, ${Model.userWiseBugs[2].OpenCount}, ${Model.userWiseBugs[3].OpenCount}, ${Model.userWiseBugs[4].OpenCount}, ${Model.userWiseBugs[5].OpenCount}],
		}, {
			label: 'Reopened',
			backgroundColor: "rgba(255,165,0)",
			data: [${Model.userWiseBugs[0].ReOpenCount}, ${Model.userWiseBugs[1].ReOpenCount}, ${Model.userWiseBugs[2].ReOpenCount}, ${Model.userWiseBugs[3].ReOpenCount}, ${Model.userWiseBugs[4].ReOpenCount}, ${Model.userWiseBugs[5].ReOpenCount}],
		}
		],
	},
options: {
    tooltips: {
      displayColors: true,
			mode: 'point',
     
    },
    scales: {
      xAxes: [{
    	  barThickness : 30,  
          stacked: true,
          gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
        beginAtZero: true,
        },
        type: 'linear',
      }]
    },
		responsive: true,
		maintainAspectRatio: true,
		legend: { position: 'top' },
	}
});

</script>
<style>
.wrapper-content {
    padding: 0px 0px 25px 0px;
}
</style>
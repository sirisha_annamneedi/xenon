<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Cache-control" content="private">
<link rel="icon" type="image/png"
	href="<%=request.getContextPath()%>/resources/img/xenon.png">
<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/font-awesome/css/font-awesome.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/iCheck/custom.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/steps/jquery.steps.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/chosen/chosen.css"
	rel="stylesheet">

<link
	href="<%=request.getContextPath()%>/resources/css/plugins/dataTables/datatables.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/bootstrap-markdown/bootstrap-markdown.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/dropzone/basic.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/dropzone/dropzone.css"
	rel="stylesheet">
<!-- FooTable -->
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/footable/footable.core.css"
	rel="stylesheet">

<link
	href="<%=request.getContextPath()%>/resources/css/plugins/sweetalert/sweetalert.css"
	rel="stylesheet">

<!-- Ladda style -->
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/ladda/ladda-themeless.min.css"
	rel="stylesheet">
<!-- Toastr style -->
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/toastr/toastr.min.css"
	rel="stylesheet">


<!-- SUMMERNOTE -->
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/summernote/summernote.css"
	rel="stylesheet">
<%-- <link
	href="<%=request.getContextPath()%>/resources/css/plugins/summernote/summernote-bs3.css"
	rel="stylesheet"> --%>

<!-- Morris -->
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/morris/morris-0.4.3.min.css"
	rel="stylesheet">


<link href="<%=request.getContextPath()%>/resources/css/animate.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/resources/css/style.css"
	rel="stylesheet">

<!-- xenon custom style sheet -->
<link
	href="<%=request.getContextPath()%>/resources/css/xenon_custom.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/plugins/datapicker/datepicker3.css"
	rel="stylesheet">
<title><tiles:insertAttribute name="title" ignore="true" /></title>

</head>

<!-- Mainly scripts -->
<script src="<%=request.getContextPath()%>/resources/js/jquery-2.1.1.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<%=request.getContextPath()%>/resources/js/inspinia.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/pace/pace.min.js"></script>

<!-- Steps -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/staps/jquery.steps.min.js"></script>

<!-- Jquery Validate -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/validate/jquery.validate.min.js"></script>

<!-- Chosen -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/chosen/chosen.jquery.js"></script>

<script
	src="<%=request.getContextPath()%>/resources/js/plugins/dataTables/datatables.min.js"></script>

<!-- Bootstrap markdown -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/bootstrap-markdown/bootstrap-markdown.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/bootstrap-markdown/markdown.js"></script>
<!-- DROPZONE -->

<!-- ChartJS-->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/chartJs/Chart.min.js"></script>

<!-- ChartJS-->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/chartJs/Chart.minV2.js"></script>

<script
	src="<%=request.getContextPath()%>/resources/js/plugins/dropzone/dropzone.js"></script>

<!-- Flot -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/flot/jquery.flot.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/flot/jquery.flot.spline.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/flot/jquery.flot.resize.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/flot/jquery.flot.pie.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/flot/jquery.flot.symbol.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/flot/jquery.flot.time.js"></script>

<!-- FooTable -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/footable/footable.all.min.js"></script>

<script
	src="<%=request.getContextPath()%>/resources/js/plugins/sweetalert/sweetalert.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/datapicker/moment.js"></script>

<!-- Ladda -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/ladda/spin.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/ladda/ladda.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/ladda/ladda.jquery.min.js"></script>
<!-- Toastr script -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/toastr/toastr.min.js"></script>
<!-- Morris -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/morris/raphael-2.1.0.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/morris/morris.js"></script>
<!-- SUMMERNOTE -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/summernote/summernote.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<!-- Date range picker -->
<script
	src="<%=request.getContextPath()%>/resources/js/plugins/daterangepicker/daterangepicker.js"></script>
<body class="">
	<!-- fixed-sidebar pace-done fixed-nav fixed-nav-basic md-skin -->
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="body" />
	<tiles:insertAttribute name="footer" />

</body>

</html>
function getScenarioList (moduleId,moduleName){
	 sessionStorage.setItem('selectedTestModuleId',moduleId);
	    sessionStorage.setItem('selectedTestModuleName',moduleName);
	 var posting = $.post('settestmoduleid', {
		   moduleId : moduleId,
		   moduleName : moduleName,
		   viewType : "edit"
		});
		 posting.done(function(data) {
			window.location.href="scenariolist"; 
		});
 }
function getTestCaseList (scenarioId,scenarioName,moduleId,moduleName){
	sessionStorage.setItem('selectedTestModuleId',moduleId);
    sessionStorage.setItem('selectedTestModuleName',moduleName);
    sessionStorage.setItem('selectedScenarioId',scenarioId);
    sessionStorage.setItem('selectedScenarioName',scenarioName);
	 var posting = $.post('setscenarioid', {
		 scenarioId : scenarioId,
		 scenarioName : scenarioName, 
		 moduleId : moduleId,
		 moduleName : moduleName
		});
		 posting.done(function(data) {
			window.location.href="testcase"; 
		});
		
}

function setTestCaseList (scenarioId,scenarioName,moduleId,moduleName){
	sessionStorage.setItem('selectedTestModuleId',moduleId);
    sessionStorage.setItem('selectedTestModuleName',moduleName);
    sessionStorage.setItem('selectedScenarioId',scenarioId);
    sessionStorage.setItem('selectedScenarioName',scenarioName);
	 var posting = $.post('setscenarioid', {
		 scenarioId : scenarioId,
		 scenarioName : scenarioName, 
		 moduleId : moduleId,
		 moduleName : moduleName
		});
		 posting.done(function(data) {
			window.location.href="apitest"; 
		});
		
}



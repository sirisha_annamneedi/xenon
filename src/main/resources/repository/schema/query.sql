-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: jadesales03292016183243
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `xe_active`
--

DROP TABLE IF EXISTS `xe_active`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_active` (
  `active_id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`active_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_activities`
--

DROP TABLE IF EXISTS `xe_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_activities` (
  `xetm_activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_desc` varchar(100) DEFAULT NULL,
  `activity_format` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`xetm_activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_automation_build`
--

DROP TABLE IF EXISTS `xe_automation_build`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_automation_build` (
  `build_id` int(11) NOT NULL AUTO_INCREMENT,
  `build_name` varchar(400) DEFAULT NULL,
  `build_desc` varchar(400) DEFAULT NULL,
  `build_status` int(11) DEFAULT NULL,
  `release_id` int(11) DEFAULT NULL,
  `build_state` int(11) DEFAULT NULL,
  `build_createdate` datetime DEFAULT NULL,
  `build_enddate` datetime DEFAULT NULL,
  `build_creator` int(11) DEFAULT NULL,
  `build_logpath` varchar(200) DEFAULT NULL,
  `build_updatedate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`build_id`),
  KEY `status_FK_idx` (`build_status`),
  KEY `release_FK_idx` (`release_id`),
  KEY `executed_FK_idx` (`build_state`),
  KEY `buildCreator_FK _idx` (`build_creator`),
  CONSTRAINT `buildCreator_FK ` FOREIGN KEY (`build_creator`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `buildState_FK` FOREIGN KEY (`build_state`) REFERENCES `xebm_build_state` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `buildStatus_FK` FOREIGN KEY (`build_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `release_FK` FOREIGN KEY (`release_id`) REFERENCES `xe_release` (`release_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_automation_step_execution_bug`
--

DROP TABLE IF EXISTS `xe_automation_step_execution_bug`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_automation_step_execution_bug` (
  `exe_step_bug_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_step_id` int(11) NOT NULL,
  `bug_id` int(11) NOT NULL,
  `step_exe_id` int(11) NOT NULL,
  `test_case_id` int(11) DEFAULT NULL,
  `build_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`exe_step_bug_id`),
  KEY `testcaseFK_idx` (`test_case_id`),
  KEY `buildidFK_idx` (`build_id`),
  KEY `stepidFK_idx` (`step_exe_id`),
  KEY `autobugid_idx` (`bug_id`),
  KEY `autotestcaseFK_idx` (`test_case_id`),
  KEY `autobuildidFK_idx` (`build_id`),
  KEY `autostepidFK_idx` (`step_exe_id`),
  CONSTRAINT `autobugid` FOREIGN KEY (`bug_id`) REFERENCES `xebt_bug` (`bug_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `autobuildidFK` FOREIGN KEY (`build_id`) REFERENCES `xe_automation_build` (`build_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `autostepidFK` FOREIGN KEY (`step_exe_id`) REFERENCES `xebm_automation_steps_execution_summary` (`trial_step_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `autotestcaseFK` FOREIGN KEY (`test_case_id`) REFERENCES `xetm_testcase` (`testcase_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_bm_access`
--

DROP TABLE IF EXISTS `xe_bm_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_bm_access` (
  `bm_id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `createproject` int(11) DEFAULT NULL,
  `editproject` int(11) DEFAULT NULL,
  `viewproject` int(11) DEFAULT NULL,
  `assignproject` int(11) DEFAULT NULL,
  `createuser` int(11) DEFAULT NULL,
  `edituser` int(11) DEFAULT NULL,
  `viewuser` int(11) DEFAULT NULL,
  `createmodule` int(11) DEFAULT NULL,
  `editmodule` int(11) DEFAULT NULL,
  `viewmodule` int(11) DEFAULT NULL,
  `assignmodule` int(11) DEFAULT NULL,
  `bmdashboard` int(11) DEFAULT NULL,
  `createrelease` int(11) DEFAULT NULL,
  `viewrelease` int(11) DEFAULT NULL,
  `editrelease` int(11) DEFAULT NULL,
  `createbuild` int(11) DEFAULT NULL,
  `viewbuild` int(11) DEFAULT NULL,
  `editbuild` int(11) DEFAULT NULL,
  `addremovetestcase` int(11) DEFAULT NULL,
  `executebuild` int(11) DEFAULT NULL,
  `mail` int(11) DEFAULT NULL,
  `accountsettings` int(11) DEFAULT NULL,
  `removetestcases` int(11) DEFAULT NULL,
  `assignbuild` int(11) DEFAULT NULL,
  `viewallbuild` int(11) DEFAULT NULL,
  `buildproject` int(11) DEFAULT NULL,
  `assigntoall` int(11) DEFAULT NULL,
  `createvm` int(11) DEFAULT NULL,
  `viewvm` int(11) DEFAULT NULL,
  `automationtestcase` int(11) DEFAULT NULL,
  `autoexe_build` int(11) DEFAULT NULL,
  PRIMARY KEY (`bm_id`),
  KEY `roleFK_idx` (`role_id`),
  KEY `createProjectFK_idx` (`createproject`),
  KEY `editProjectFK_idx` (`editproject`),
  KEY `viewProjectFK_idx` (`viewproject`),
  KEY `assignProject_idx` (`assignproject`),
  KEY `createUserFK_idx` (`createuser`),
  KEY `editUserFK_idx` (`edituser`),
  KEY `viewUserFK_idx` (`viewuser`),
  KEY `createModuleFK_idx` (`createmodule`),
  KEY `editModuleFK_idx` (`editmodule`),
  KEY `viewModuleFK_idx` (`viewmodule`),
  KEY `assignModuleFK_idx` (`assignmodule`),
  KEY `bmDashboardFK_idx` (`bmdashboard`),
  KEY `createReleaseFK_idx` (`createrelease`),
  KEY `viewReleaseFK_idx` (`viewrelease`),
  KEY `editReleaseFK_idx` (`editrelease`),
  KEY `createBuildFK_idx` (`createbuild`),
  KEY `viewBuildFK_idx` (`viewbuild`),
  KEY `editBuildFK_idx` (`editbuild`),
  KEY `addRemoveTCFK_idx` (`addremovetestcase`),
  KEY `executeBuildFK_idx` (`executebuild`),
  KEY `mailFK_idx` (`mail`),
  KEY `AccSettingFK_idx` (`accountsettings`),
  KEY `removetestcasesFK_idx` (`removetestcases`),
  KEY `assignbuildFK_idx` (`assignbuild`),
  KEY `viewallbuildFK_idx1` (`viewallbuild`),
  KEY `buildprojectFK_idx` (`buildproject`),
  KEY `assigntoallFK_idx` (`assigntoall`),
  KEY `autoexebuildFK_idx` (`autoexe_build`),
  KEY `createvmFk_idx` (`createvm`),
  KEY `viewvmFK_idx` (`viewvm`),
  KEY `automationtestcaseFK_idx` (`automationtestcase`),
  CONSTRAINT `AccSettingFK` FOREIGN KEY (`accountsettings`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE SET NULL,
  CONSTRAINT `addRemoveTCFK` FOREIGN KEY (`addremovetestcase`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignbuildFK` FOREIGN KEY (`assignbuild`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `assignModuleFK` FOREIGN KEY (`assignmodule`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignProject` FOREIGN KEY (`assignproject`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assigntoallFK` FOREIGN KEY (`assigntoall`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `autoexebuildFK` FOREIGN KEY (`autoexe_build`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `automationtestcaseFK` FOREIGN KEY (`automationtestcase`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bmDashboardFK` FOREIGN KEY (`bmdashboard`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `buildprojectFK` FOREIGN KEY (`buildproject`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `createBuildFK` FOREIGN KEY (`createbuild`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `createModuleFK` FOREIGN KEY (`createmodule`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `createProjectFK` FOREIGN KEY (`createproject`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `createReleaseFK` FOREIGN KEY (`createrelease`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `createUserFK` FOREIGN KEY (`createuser`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `createvmFk` FOREIGN KEY (`createvm`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `editBuildFK` FOREIGN KEY (`editbuild`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `editModuleFK` FOREIGN KEY (`editmodule`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `editProjectFK` FOREIGN KEY (`editproject`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `editReleaseFK` FOREIGN KEY (`editrelease`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `editUserFK` FOREIGN KEY (`edituser`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `executeBuildFK` FOREIGN KEY (`executebuild`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mailFK` FOREIGN KEY (`mail`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `removetestcasesFK` FOREIGN KEY (`removetestcases`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `roleFK` FOREIGN KEY (`role_id`) REFERENCES `xe_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `viewallbuildFK` FOREIGN KEY (`viewallbuild`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `viewBuildFK` FOREIGN KEY (`viewbuild`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `viewModuleFK` FOREIGN KEY (`viewmodule`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `viewProjectFK` FOREIGN KEY (`viewproject`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `viewReleaseFK` FOREIGN KEY (`viewrelease`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `viewUserFK` FOREIGN KEY (`viewuser`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `viewvmFK` FOREIGN KEY (`viewvm`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_bt_access`
--

DROP TABLE IF EXISTS `xe_bt_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_bt_access` (
  `bt_id` int(11) NOT NULL,
  `roleID` int(11) DEFAULT NULL,
  `btdashboard` int(11) DEFAULT NULL,
  `createbug` int(11) DEFAULT NULL,
  `bugsummary` int(11) DEFAULT NULL,
  `viewallbugs` int(11) DEFAULT NULL,
  `bugsreportedbyme` int(11) DEFAULT NULL,
  `bugsassignedtome` int(11) DEFAULT NULL,
  `buggroup` int(11) DEFAULT '1',
  PRIMARY KEY (`bt_id`),
  KEY `btRoleFK_idx` (`roleID`),
  KEY `btDashFK_idx` (`btdashboard`),
  KEY `createBugFK_idx` (`createbug`),
  KEY `bugSummaryFK_idx` (`bugsummary`),
  KEY `viewAllBugFK_idx` (`viewallbugs`),
  KEY `reportedBugFK_idx` (`bugsreportedbyme`),
  KEY `assignedBugFK_idx` (`bugsassignedtome`),
  KEY `buggroupFK_idx` (`buggroup`),
  CONSTRAINT `assignedBugFK` FOREIGN KEY (`bugsassignedtome`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `btDashFK` FOREIGN KEY (`btdashboard`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `btRoleFK` FOREIGN KEY (`roleID`) REFERENCES `xe_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `buggroupFK` FOREIGN KEY (`buggroup`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bugSummaryFK` FOREIGN KEY (`bugsummary`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `createBugFK` FOREIGN KEY (`createbug`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `reportedBugFK` FOREIGN KEY (`bugsreportedbyme`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `viewAllBugFK` FOREIGN KEY (`viewallbugs`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_bug_customize_query`
--

DROP TABLE IF EXISTS `xe_bug_customize_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_bug_customize_query` (
  `query_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `query_text` text,
  `query_name` text,
  `query_variables` text,
  PRIMARY KEY (`query_id`),
  KEY `userFK_idx` (`user_id`),
  CONSTRAINT `userIdFk` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_build`
--

DROP TABLE IF EXISTS `xe_build`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_build` (
  `build_id` int(11) NOT NULL AUTO_INCREMENT,
  `build_name` varchar(400) DEFAULT NULL,
  `build_desc` varchar(400) DEFAULT NULL,
  `build_status` int(11) DEFAULT NULL,
  `release_id` int(11) DEFAULT NULL,
  `build_state` int(11) DEFAULT NULL,
  `build_createdate` datetime DEFAULT NULL,
  `build_enddate` datetime DEFAULT NULL,
  `build_creator` int(11) DEFAULT NULL,
  `build_exe_comments` longtext,
  `build_updatedate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`build_id`),
  KEY `status_idx` (`build_status`),
  KEY `release_idx` (`release_id`),
  KEY `executed_idx` (`build_state`),
  KEY `buildCreatorFK _idx` (`build_creator`),
  CONSTRAINT `buildCreatorFK ` FOREIGN KEY (`build_creator`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `buildState` FOREIGN KEY (`build_state`) REFERENCES `xebm_build_state` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `buildStatus` FOREIGN KEY (`build_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `release` FOREIGN KEY (`release_id`) REFERENCES `xe_release` (`release_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='create build table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_build_execution_type`
--

DROP TABLE IF EXISTS `xe_build_execution_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_build_execution_type` (
  `execution_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`execution_type_id`),
  KEY `status_FK_idx` (`status`),
  CONSTRAINT `status_FK` FOREIGN KEY (`status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_current_project`
--

DROP TABLE IF EXISTS `xe_current_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_current_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iser_cp_idx` (`user_id`),
  KEY `pro_cp_idx` (`project_id`),
  CONSTRAINT `pro_cp` FOREIGN KEY (`project_id`) REFERENCES `xe_project` (`project_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_cp` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_doc_library`
--

DROP TABLE IF EXISTS `xe_doc_library`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_doc_library` (
  `xe_doc_library_id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_title` varchar(200) DEFAULT NULL,
  `doc_type` varchar(45) DEFAULT NULL,
  `doc_data` longblob,
  `upload_time` datetime DEFAULT NULL,
  `uploaded_by` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`xe_doc_library_id`),
  KEY `projectIdDocFk_idx` (`project_id`),
  KEY `uploadedByFK_idx` (`uploaded_by`),
  CONSTRAINT `projectIdDocFk` FOREIGN KEY (`project_id`) REFERENCES `xe_project` (`project_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `uploadedByFK` FOREIGN KEY (`uploaded_by`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_email_details`
--

DROP TABLE IF EXISTS `xe_email_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_email_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `smtp_host` varchar(100) DEFAULT NULL,
  `smtp_port` int(20) DEFAULT NULL,
  `smtp_user_id` varchar(100) DEFAULT NULL,
  `smtp_password` varchar(100) DEFAULT NULL,
  `is_enabled` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `isEnabledFK_idx` (`is_enabled`),
  CONSTRAINT `isEnabledFK` FOREIGN KEY (`is_enabled`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_mail_group`
--

DROP TABLE IF EXISTS `xe_mail_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_mail_group` (
  `mail_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `mail_group_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`mail_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_mail_group_details`
--

DROP TABLE IF EXISTS `xe_mail_group_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_mail_group_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `mail_group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mail_group_idFK_idx` (`mail_group_id`),
  KEY `user_idFK_idx` (`user_id`),
  CONSTRAINT `mail_group_idFK` FOREIGN KEY (`mail_group_id`) REFERENCES `xe_mail_group` (`mail_group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_idFK` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_mail_notifications`
--

DROP TABLE IF EXISTS `xe_mail_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_mail_notifications` (
  `mail_notification_id` int(11) NOT NULL,
  `create_user` int(11) DEFAULT NULL,
  `assign_project` int(11) DEFAULT NULL,
  `assign_module` int(11) DEFAULT NULL,
  `assign_build` int(11) DEFAULT NULL,
  `build_exec_complete` int(11) DEFAULT NULL,
  `create_bug` int(11) DEFAULT NULL,
  `update_bug` int(11) DEFAULT NULL,
  PRIMARY KEY (`mail_notification_id`),
  KEY `createUserFK_idx` (`create_user`),
  KEY `assignProjectFK_idx` (`assign_project`),
  KEY `buildExecCompleteFK_idx` (`build_exec_complete`),
  KEY `assignModuleFK_idx` (`assign_module`),
  KEY `assignBuildFK_idx` (`assign_build`),
  KEY `createBugFK_idx` (`create_bug`),
  KEY `updateBugFK_idx` (`update_bug`),
  KEY `createUserFkVal_idx` (`create_user`),
  KEY `assignProjectFkVal_idx` (`assign_project`),
  KEY `buildExecCompleteFkVal_idx` (`build_exec_complete`),
  KEY `createBugFkVal_idx` (`create_bug`),
  KEY `updateBugFkVal_idx` (`update_bug`),
  KEY `assignModuleFkVal_idx` (`assign_module`),
  KEY `assignBuildFkVal_idx` (`assign_build`),
  CONSTRAINT `assignBuildFkVal` FOREIGN KEY (`assign_build`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignModuleFkVal` FOREIGN KEY (`assign_module`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignProjectFkVal` FOREIGN KEY (`assign_project`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `buildExecCompleteFkVal` FOREIGN KEY (`build_exec_complete`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `createBugFkVal` FOREIGN KEY (`create_bug`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `createUserFkVal` FOREIGN KEY (`create_user`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `updateBugFkVal` FOREIGN KEY (`update_bug`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_module`
--

DROP TABLE IF EXISTS `xe_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(45) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `module_description` longtext,
  `module_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`module_id`),
  KEY `module_active_idx` (`module_active`),
  KEY `xe_project_module` (`project_id`),
  CONSTRAINT `module_active` FOREIGN KEY (`module_active`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `xe_project_module` FOREIGN KEY (`project_id`) REFERENCES `xe_project` (`project_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_notification_users`
--

DROP TABLE IF EXISTS `xe_notification_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_notification_users` (
  `notification_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `nf_notification_id` int(11) DEFAULT NULL,
  `nf_user_id` int(11) DEFAULT NULL,
  `nf_read_status` int(11) DEFAULT '2',
  `nf_read_count_flag` int(11) DEFAULT '2',
  PRIMARY KEY (`notification_user_id`),
  KEY `notfUersFK_idx` (`nf_user_id`),
  KEY `notfFk_idx` (`nf_notification_id`),
  KEY `reafFlagNf_idx` (`nf_read_count_flag`),
  CONSTRAINT `notfFk` FOREIGN KEY (`nf_notification_id`) REFERENCES `xe_user_notifications` (`notification_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `notfUersFK` FOREIGN KEY (`nf_user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `readFlagNf` FOREIGN KEY (`nf_read_count_flag`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_notifications`
--

DROP TABLE IF EXISTS `xe_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_notifications` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_for` varchar(1000) DEFAULT NULL,
  `module` varchar(45) DEFAULT NULL,
  `notification_icon` varchar(500) DEFAULT NULL,
  `notification_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_project`
--

DROP TABLE IF EXISTS `xe_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(45) DEFAULT NULL,
  `project_description` longtext,
  `project_active` int(11) DEFAULT NULL,
  `project_createdate` datetime NOT NULL,
  `tm_prefix` varchar(45) DEFAULT NULL,
  `bt_prefix` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`project_id`),
  KEY `activeID_idx` (`project_active`),
  CONSTRAINT `prj_active` FOREIGN KEY (`project_active`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='xe_systems';

--
-- Table structure for table `xe_recent_activities`
--

DROP TABLE IF EXISTS `xe_recent_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_recent_activities` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `activity` int(11) DEFAULT NULL,
  `activity_desc` varchar(2000) DEFAULT NULL,
  `module` varchar(45) DEFAULT NULL,
  `activity_date` datetime DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `activity_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`activity_id`),
  KEY `activityUserFk_idx` (`user`),
  KEY `sctivityStatusFk_idx` (`activity_status`),
  KEY `activityFk_idx` (`activity`),
  CONSTRAINT `activityFk` FOREIGN KEY (`activity`) REFERENCES `xe_activities` (`xetm_activity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `activityUserFk` FOREIGN KEY (`user`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sctivityStatusFk` FOREIGN KEY (`activity_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_release`
--

DROP TABLE IF EXISTS `xe_release`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_release` (
  `release_id` int(11) NOT NULL AUTO_INCREMENT,
  `release_name` varchar(45) DEFAULT NULL,
  `release_description` longtext,
  `release_start_date` datetime DEFAULT NULL,
  `release_end_date` datetime DEFAULT NULL,
  `release_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`release_id`),
  KEY `release_active_flag_idx` (`release_active`),
  CONSTRAINT `release_active_flag` FOREIGN KEY (`release_active`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_role`
--

DROP TABLE IF EXISTS `xe_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_type` varchar(100) DEFAULT NULL,
  `role_status` int(11) NOT NULL,
  PRIMARY KEY (`role_id`),
  KEY `status_idx` (`role_status`),
  CONSTRAINT `role_status` FOREIGN KEY (`role_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_step_execution_bug`
--

DROP TABLE IF EXISTS `xe_step_execution_bug`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_step_execution_bug` (
  `exe_step_bug_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_step_id` int(11) NOT NULL,
  `bug_id` int(11) NOT NULL,
  `step_exe_id` int(11) NOT NULL,
  `test_case_id` int(11) DEFAULT NULL,
  `build_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`exe_step_bug_id`),
  KEY `exe_bug_id_fk_idx` (`bug_id`),
  KEY `step_id_fk_idx` (`test_step_id`),
  KEY `step_exe_id_fk_idx` (`step_exe_id`),
  KEY `exe_test_case_id_fk_idx` (`test_case_id`),
  KEY `exe_build_id_fk_idx` (`build_id`),
  CONSTRAINT `exe_bug_id_fk` FOREIGN KEY (`bug_id`) REFERENCES `xebt_bug` (`bug_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `exe_build_id_fk` FOREIGN KEY (`build_id`) REFERENCES `xe_build` (`build_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `exe_test_case_id_fk` FOREIGN KEY (`test_case_id`) REFERENCES `xetm_testcase` (`testcase_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `step_exe_summ_id_fk` FOREIGN KEY (`step_exe_id`) REFERENCES `xe_steps_execution_summary` (`stex_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_steps_execution_summary`
--

DROP TABLE IF EXISTS `xe_steps_execution_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_steps_execution_summary` (
  `stex_id` int(11) NOT NULL AUTO_INCREMENT,
  `testex_id` int(11) DEFAULT NULL,
  `step_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `comments` varchar(1000) DEFAULT NULL,
  `screenshot` longblob,
  `bug_raised` int(11) NOT NULL,
  PRIMARY KEY (`stex_id`),
  KEY `test_exec_id_idx` (`testex_id`),
  KEY `ststus_exec_idx` (`status`),
  CONSTRAINT `ststus_exec` FOREIGN KEY (`status`) REFERENCES `xetm_exec_status` (`exst_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `test_exec_id` FOREIGN KEY (`testex_id`) REFERENCES `xe_testcase_execution_summary` (`exec_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_support_access`
--

DROP TABLE IF EXISTS `xe_support_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_support_access` (
  `support_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `create_subcomponent` int(11) DEFAULT '2',
  `create_article` int(11) DEFAULT '2',
  `create_faq` int(11) DEFAULT '2',
  PRIMARY KEY (`support_id`),
  KEY `createSubcomponentFK_idx` (`create_subcomponent`),
  KEY `createArticle_idx` (`create_article`),
  KEY `createFaq_idx` (`create_faq`),
  KEY `roleIdFK_idx` (`role_id`),
  CONSTRAINT `createArticle` FOREIGN KEY (`create_article`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `createFaq` FOREIGN KEY (`create_faq`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `createSubCompoFK` FOREIGN KEY (`create_subcomponent`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `supportroleFk1` FOREIGN KEY (`role_id`) REFERENCES `xe_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_testcase_execution_attachments`
--

DROP TABLE IF EXISTS `xe_testcase_execution_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_testcase_execution_attachments` (
  `attach_id` int(11) NOT NULL AUTO_INCREMENT,
  `testcase_id` int(11) DEFAULT NULL,
  `build_id` int(11) DEFAULT NULL,
  `attachment` mediumblob,
  `attachment_title` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`attach_id`),
  KEY `attach_testcase_fk_idx` (`testcase_id`),
  KEY `attach_build_id_fk_idx` (`build_id`),
  CONSTRAINT `attach_build_id_fk` FOREIGN KEY (`build_id`) REFERENCES `xe_testcase_execution_summary` (`build_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `attach_testcase_fk` FOREIGN KEY (`testcase_id`) REFERENCES `xe_testcase_execution_summary` (`tc_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_testcase_execution_summary`
--

DROP TABLE IF EXISTS `xe_testcase_execution_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_testcase_execution_summary` (
  `exec_id` int(11) NOT NULL AUTO_INCREMENT,
  `tc_id` int(11) DEFAULT NULL,
  `build_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `mudule_id` int(11) DEFAULT NULL,
  `scenario_id` int(11) DEFAULT NULL,
  `tester_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `notes` varchar(400) DEFAULT NULL,
  `complete_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`exec_id`),
  KEY `testcase_idx` (`tc_id`),
  KEY `build_idx` (`build_id`),
  KEY `project_idx` (`project_id`),
  KEY `module_idx` (`mudule_id`),
  KEY `scenario_idx` (`scenario_id`),
  KEY `user_idx` (`tester_id`),
  CONSTRAINT `build_exec` FOREIGN KEY (`build_id`) REFERENCES `xe_build` (`build_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `module_exec` FOREIGN KEY (`mudule_id`) REFERENCES `xe_module` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `project_exec` FOREIGN KEY (`project_id`) REFERENCES `xe_project` (`project_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `scenario_exec` FOREIGN KEY (`scenario_id`) REFERENCES `xetm_scenario` (`scenario_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `testcase_exec` FOREIGN KEY (`tc_id`) REFERENCES `xetm_testcase` (`testcase_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_exec` FOREIGN KEY (`tester_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_timezone_details`
--

DROP TABLE IF EXISTS `xe_timezone_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_timezone_details` (
  `xe_timezone_id` int(11) NOT NULL,
  `timezone_name` varchar(100) DEFAULT NULL,
  `timezone_id` varchar(100) DEFAULT NULL,
  `timezone_staus` int(11) DEFAULT NULL,
  PRIMARY KEY (`xe_timezone_id`),
  KEY `tzStatus_idx` (`timezone_staus`),
  CONSTRAINT `tzStatus` FOREIGN KEY (`timezone_staus`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_tm_access`
--

DROP TABLE IF EXISTS `xe_tm_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_tm_access` (
  `tm_id` int(11) NOT NULL,
  `roleID` int(11) DEFAULT NULL,
  `tmdashboard` int(11) DEFAULT NULL,
  `testspecification` int(11) DEFAULT NULL,
  `scenariolist` int(11) DEFAULT NULL,
  `testcase` int(11) DEFAULT NULL,
  `teststeps` int(11) DEFAULT NULL,
  PRIMARY KEY (`tm_id`),
  KEY `roleFK_idx` (`roleID`),
  KEY `tmDashFK_idx` (`tmdashboard`),
  KEY `testSpecFK_idx` (`testspecification`),
  KEY `scenListFK_idx` (`scenariolist`),
  KEY `testCaseFK_idx` (`testcase`),
  KEY `testStepsFK_idx` (`teststeps`),
  CONSTRAINT `roleIdFK` FOREIGN KEY (`roleID`) REFERENCES `xe_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `scenListFK` FOREIGN KEY (`scenariolist`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `testCaseFK` FOREIGN KEY (`testcase`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `testSpecFK` FOREIGN KEY (`testspecification`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `testStepsFK` FOREIGN KEY (`teststeps`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tmDashFK` FOREIGN KEY (`tmdashboard`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Test Manager access Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_user_details`
--

DROP TABLE IF EXISTS `xe_user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_user_details` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(60) DEFAULT NULL,
  `user_password` varchar(45) NOT NULL,
  `email_id` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `tm_status` int(11) DEFAULT NULL,
  `bt_status` int(11) DEFAULT NULL,
  `user_status` int(11) DEFAULT NULL,
  `jenkin_status` int(11) DEFAULT '2',
  `git_status` int(11) DEFAULT '2',
  `ldap_status` int(11) DEFAULT '2',
  `user_photo` mediumblob,
  `location` varchar(245) DEFAULT NULL,
  `about_me` text,
  `user_timezone` int(11) DEFAULT '2',
  `pass_flag` int(11) DEFAULT '2',
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `role_id_idx` (`role_id`),
  KEY `user_status_idx` (`user_status`),
  KEY `tm_status_idx` (`tm_status`),
  KEY `bm_status_idx` (`bt_status`),
  KEY `pass_flag` (`pass_flag`),
  KEY `timezoneFK_idx` (`user_timezone`),
  KEY `gitStateFk_idx` (`git_status`),
  KEY `jenkinStateFK_idx` (`jenkin_status`),
  CONSTRAINT `bt_status` FOREIGN KEY (`bt_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `gitstatefk` FOREIGN KEY (`jenkin_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jenkinstatefk` FOREIGN KEY (`git_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pass_flag` FOREIGN KEY (`pass_flag`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `xe_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `timezoneFK` FOREIGN KEY (`user_timezone`) REFERENCES `xe_timezone_details` (`xe_timezone_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tm_status` FOREIGN KEY (`tm_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_status` FOREIGN KEY (`user_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



--
-- Table structure for table `xe_user_module`
--

DROP TABLE IF EXISTS `xe_user_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_user_module` (
  `user_module_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_module_id`),
  KEY `moduleID_idx` (`module_id`),
  KEY `userID_idx` (`user_id`),
  KEY `projectID_idx` (`project_id`),
  CONSTRAINT `moduleID` FOREIGN KEY (`module_id`) REFERENCES `xe_module` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `projectID` FOREIGN KEY (`project_id`) REFERENCES `xe_project` (`project_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `userID` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_user_notifications`
--

DROP TABLE IF EXISTS `xe_user_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_user_notifications` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `activity` int(11) DEFAULT NULL,
  `notification_description` varchar(1000) DEFAULT NULL,
  `notification_date` datetime DEFAULT NULL,
  `notification_url` varchar(200) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `read_status` int(11) DEFAULT '2',
  `notification_status` int(11) DEFAULT '1',
  `read_count_flag` int(11) DEFAULT '2',
  PRIMARY KEY (`notification_id`),
  KEY `statusIdFK_idx` (`notification_status`),
  KEY `nfUserIdFK_idx` (`user_id`),
  KEY `nfReadStatusFK_idx` (`read_status`),
  KEY `nfCreator_idx` (`created_by`),
  KEY `nfActivityFK_idx` (`activity`),
  KEY `rdFgNf_idx` (`read_count_flag`),
  CONSTRAINT `nfActivityFK` FOREIGN KEY (`activity`) REFERENCES `xe_notifications` (`notification_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nfCreator` FOREIGN KEY (`created_by`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nfReadStatusFK` FOREIGN KEY (`read_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nfUserIdFK` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rdFgNf` FOREIGN KEY (`read_count_flag`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `statusIdFK` FOREIGN KEY (`notification_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `xe_user_project`
--

DROP TABLE IF EXISTS `xe_user_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_user_project` (
  `user_project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_project_id`),
  KEY `projetct_idx` (`project_id`),
  KEY `user_idx` (`user`),
  CONSTRAINT `projetct` FOREIGN KEY (`project_id`) REFERENCES `xe_project` (`project_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user` FOREIGN KEY (`user`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `xe_utilization`
--

DROP TABLE IF EXISTS `xe_utilization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_utilization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `userName` varchar(45) DEFAULT NULL,
  `bandwidth_usageMB` double DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userIdFK_idx` (`userId`),
  CONSTRAINT `userId_FK` FOREIGN KEY (`userId`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebm_assign_build`
--

DROP TABLE IF EXISTS `xebm_assign_build`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebm_assign_build` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `buildforeigh_idx` (`build_id`),
  KEY `projectforegn_idx` (`project_id`),
  KEY `moduleforegn_idx` (`module_id`),
  KEY `userforign_idx` (`user_id`),
  CONSTRAINT `buildforeigh` FOREIGN KEY (`build_id`) REFERENCES `xe_build` (`build_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `moduleforegn` FOREIGN KEY (`module_id`) REFERENCES `xe_module` (`module_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `projectforegn` FOREIGN KEY (`project_id`) REFERENCES `xe_project` (`project_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userforign` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebm_auto_build`
--

DROP TABLE IF EXISTS `xebm_auto_build`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebm_auto_build` (
  `build_id` int(11) NOT NULL AUTO_INCREMENT,
  `build_name` varchar(100) DEFAULT NULL,
  `build_createdate` datetime DEFAULT NULL,
  `build_creator` int(11) DEFAULT NULL,
  `build_logfile` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`build_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebm_autoexe`
--

DROP TABLE IF EXISTS `xebm_autoexe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebm_autoexe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_description` longtext,
  `test_script` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebm_automation_build_testcase`
--

DROP TABLE IF EXISTS `xebm_automation_build_testcase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebm_automation_build_testcase` (
  `tc_id` int(11) NOT NULL AUTO_INCREMENT,
  `build_id` int(11) DEFAULT NULL,
  `testcase_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `scenario_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `datasheet_title` varchar(100) DEFAULT NULL,
  `datasheet` longblob,
  PRIMARY KEY (`tc_id`),
  KEY `buildFK_idx` (`build_id`),
  KEY `projectFK_idx` (`project_id`),
  KEY `moduleFK_idx` (`module_id`),
  KEY `scenarioFK_idx` (`scenario_id`),
  KEY `tcFK_idx` (`testcase_id`),
  CONSTRAINT `autobuildFK` FOREIGN KEY (`build_id`) REFERENCES `xe_automation_build` (`build_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `automoduleFK` FOREIGN KEY (`module_id`) REFERENCES `xe_module` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `autoprojectFK` FOREIGN KEY (`project_id`) REFERENCES `xe_project` (`project_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `autoscenarioFK` FOREIGN KEY (`scenario_id`) REFERENCES `xetm_scenario` (`scenario_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `autotcFK` FOREIGN KEY (`testcase_id`) REFERENCES `xetm_testcase` (`testcase_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebm_automation_execution_summary`
--

DROP TABLE IF EXISTS `xebm_automation_execution_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebm_automation_execution_summary` (
  `exe_id` int(11) NOT NULL AUTO_INCREMENT,
  `build` int(11) DEFAULT NULL,
  `project` int(11) DEFAULT NULL,
  `module` int(11) DEFAULT NULL,
  `scenario` int(11) DEFAULT NULL,
  `testcase` int(11) DEFAULT NULL,
  `testcase_status` int(11) DEFAULT NULL,
  `tc_start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `tc_end_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`exe_id`),
  KEY `exe_build_id_idx` (`build`),
  KEY `exe_project_id_idx` (`project`),
  KEY `exe_module_id_idx` (`module`),
  KEY `exe_scenario_id_idx` (`scenario`),
  KEY `exe_testcase_id_idx` (`testcase`),
  KEY `exe_testcase_status_idx` (`testcase_status`),
  CONSTRAINT `exec_build_id` FOREIGN KEY (`build`) REFERENCES `xe_automation_build` (`build_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `exec_module_id` FOREIGN KEY (`module`) REFERENCES `xe_module` (`module_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `exec_project_id` FOREIGN KEY (`project`) REFERENCES `xe_project` (`project_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `exec_scenario_id` FOREIGN KEY (`scenario`) REFERENCES `xetm_scenario` (`scenario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `exec_testcase_id` FOREIGN KEY (`testcase`) REFERENCES `xetm_testcase` (`testcase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `exec_testcase_status` FOREIGN KEY (`testcase_status`) REFERENCES `xetm_exec_status` (`exst_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebm_automation_steps_execution_summary`
--

DROP TABLE IF EXISTS `xebm_automation_steps_execution_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebm_automation_steps_execution_summary` (
  `trial_step_id` int(11) NOT NULL AUTO_INCREMENT,
  `testcase_id` int(11) DEFAULT NULL,
  `step_id` int(11) DEFAULT NULL,
  `step_details` longtext,
  `build_id` int(11) DEFAULT NULL,
  `step_value` longtext,
  `step_status` int(11) DEFAULT NULL,
  `screenshot` mediumblob,
  `screenshot_title` longtext,
  `actual_result` longtext,
  `is_bug_raised` int(11) DEFAULT NULL,
  `dataset_id` int(11) DEFAULT 0,
  PRIMARY KEY (`trial_step_id`),
  KEY `trial_testCaseFK_idx` (`testcase_id`),
  KEY `trial_stepStatusFK_idx` (`step_status`),
  KEY `buildidFK_idx` (`build_id`),
  KEY `bugraisedFk_idx` (`is_bug_raised`),
  CONSTRAINT `bugraisedFk` FOREIGN KEY (`is_bug_raised`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `buildidFK` FOREIGN KEY (`build_id`) REFERENCES `xe_automation_build` (`build_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `trial_stepStatusFK` FOREIGN KEY (`step_status`) REFERENCES `xetm_exec_status` (`exst_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trial_testCaseFK` FOREIGN KEY (`testcase_id`) REFERENCES `xetm_testcase` (`testcase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebm_build_exec_details`
--

DROP TABLE IF EXISTS `xebm_build_exec_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebm_build_exec_details` (
  `build_exec_id` int(11) NOT NULL AUTO_INCREMENT,
  `build_id` int(11) DEFAULT NULL,
  `vm_id` int(11) DEFAULT NULL,
  `browser` varchar(45) DEFAULT NULL,
  `mail_group_id` int(11) DEFAULT NULL,
  `env_id` int(11) DEFAULT '2',
  `step_wise_screenshot` int(11) DEFAULT NULL,
  `report_bug` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `start_time` varchar(45) DEFAULT NULL,
  `end_time` varchar(45) DEFAULT NULL,
  `complete_status` int(11) DEFAULT '2',
  `exec_json` longtext,
  PRIMARY KEY (`build_exec_id`),
  KEY `autoBuildFk1_idx` (`build_id`),
  KEY `vmIdFk_idx` (`vm_id`),
  KEY `stepWiseScreenshot_idx` (`step_wise_screenshot`),
  KEY `reportBug_idx` (`report_bug`),
  KEY `userIdFkExec_idx` (`user_id`),
  KEY `completeStatusFk_idx` (`complete_status`),
  KEY `envFkval_idx` (`env_id`),
  CONSTRAINT `autoBuildFk1` FOREIGN KEY (`build_id`) REFERENCES `xe_automation_build` (`build_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `completeStatusFk` FOREIGN KEY (`complete_status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `envFkVal` FOREIGN KEY (`env_id`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `reportBug` FOREIGN KEY (`report_bug`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `stepWiseScreenshot` FOREIGN KEY (`step_wise_screenshot`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userIdFkExec` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `vmBuildFK` FOREIGN KEY (`vm_id`) REFERENCES `xebm_vm_details` (`xe_vm_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `xebm_build_state`
--

DROP TABLE IF EXISTS `xebm_build_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebm_build_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status_idx` (`status`),
  CONSTRAINT `status_active` FOREIGN KEY (`status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `xebm_buildtestcase`
--

DROP TABLE IF EXISTS `xebm_buildtestcase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebm_buildtestcase` (
  `tc_id` int(11) NOT NULL AUTO_INCREMENT,
  `build_id` int(11) DEFAULT NULL,
  `testcase_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `scenario_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`tc_id`),
  KEY `buildFK_idx` (`build_id`),
  KEY `projectFK_idx` (`project_id`),
  KEY `moduleFK_idx` (`module_id`),
  KEY `scenarioFK_idx` (`scenario_id`),
  KEY `tcFK_idx` (`testcase_id`),
  CONSTRAINT `buildFK` FOREIGN KEY (`build_id`) REFERENCES `xe_build` (`build_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `moduleFK` FOREIGN KEY (`module_id`) REFERENCES `xe_module` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `projectFK` FOREIGN KEY (`project_id`) REFERENCES `xe_project` (`project_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `scenarioFK` FOREIGN KEY (`scenario_id`) REFERENCES `xetm_scenario` (`scenario_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tcFK` FOREIGN KEY (`testcase_id`) REFERENCES `xetm_testcase` (`testcase_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebm_customize_query`
--

DROP TABLE IF EXISTS `xebm_customize_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebm_customize_query` (
  `query_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `query_text` text,
  `query_name` text,
  `query_variables` text,
  PRIMARY KEY (`query_id`),
  KEY `userFK_idx` (`user_id`),
  CONSTRAINT `userFK` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `xebm_vm_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebm_vm_details` (
  `xe_vm_id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(100) DEFAULT NULL,
  `vm_username` varchar(100) DEFAULT NULL,
  `vm_password` varchar(100) DEFAULT NULL,
  `ip_address` varchar(100) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `project_location` longtext,
  `cust_vm_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `build_id` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `vm_state` int(11) DEFAULT NULL,
  PRIMARY KEY (`xe_vm_id`),
  KEY `vm_status_idx` (`status`),
  KEY `vm_stateFK_idx` (`vm_state`),
  KEY `build_idFK_idx` (`build_id`),
  KEY `user_idFK_idx` (`userId`),
  CONSTRAINT `build_idFK` FOREIGN KEY (`build_id`) REFERENCES `xe_automation_build` (`build_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userIdVMFK` FOREIGN KEY (`userId`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `vm_stateFK` FOREIGN KEY (`vm_state`) REFERENCES `xe_vm_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `vm_status` FOREIGN KEY (`status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Table structure for table `xebm_vm_exec_details`
--

DROP TABLE IF EXISTS `xebm_vm_exec_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebm_vm_exec_details` (
  `vm_exec_id` int(11) NOT NULL AUTO_INCREMENT,
  `vm_id` int(11) DEFAULT NULL,
  `build_exec_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`vm_exec_id`),
  KEY `vmExecutionFK_idx` (`vm_id`),
  KEY `buildExecFk_idx` (`build_exec_id`),
  CONSTRAINT `buildExecFk` FOREIGN KEY (`build_exec_id`) REFERENCES `xebm_build_exec_details` (`build_exec_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `vmExecutionFK` FOREIGN KEY (`vm_id`) REFERENCES `xebm_vm_details` (`xe_vm_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebt_bug`
--

DROP TABLE IF EXISTS `xebt_bug`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebt_bug` (
  `bug_id` int(11) NOT NULL AUTO_INCREMENT,
  `bug_title` varchar(250) DEFAULT NULL,
  `bug_desc` longtext,
  `project` int(11) DEFAULT NULL,
  `module` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `bug_prefix` varchar(45) DEFAULT NULL,
  `assign_status` int(11) DEFAULT '2',
  `group_status` int(11) DEFAULT '2',
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`bug_id`),
  KEY `project_idx` (`project`),
  KEY `module_idx` (`module`),
  KEY `user_idx` (`created_by`),
  KEY `bug_project_idx` (`project`),
  KEY `bug_module_idx` (`module`),
  KEY `bug_user_idx` (`created_by`),
  KEY `assign_statusFK_idx` (`assign_status`),
  KEY `group_status_idx` (`group_status`),
  CONSTRAINT `assign_statusFK` FOREIGN KEY (`assign_status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bug_module` FOREIGN KEY (`module`) REFERENCES `xe_module` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bug_project` FOREIGN KEY (`project`) REFERENCES `xe_project` (`project_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bug_user` FOREIGN KEY (`created_by`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `group_status` FOREIGN KEY (`group_status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='				';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebt_bug_attachment`
--

DROP TABLE IF EXISTS `xebt_bug_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebt_bug_attachment` (
  `attach_id` int(11) NOT NULL AUTO_INCREMENT,
  `bug_id` int(11) DEFAULT NULL,
  `attach_path` mediumblob,
  `attach_title` varchar(450) DEFAULT NULL,
  PRIMARY KEY (`attach_id`),
  KEY `attach_bug_id_idx` (`bug_id`),
  CONSTRAINT `attach_bug_id` FOREIGN KEY (`bug_id`) REFERENCES `xebt_bug` (`bug_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebt_bug_group`
--

DROP TABLE IF EXISTS `xebt_bug_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebt_bug_group` (
  `bug_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `bug_group_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`bug_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebt_bug_group_details`
--

DROP TABLE IF EXISTS `xebt_bug_group_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebt_bug_group_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_idFK_idx` (`user`),
  KEY `groupFK_idx` (`group_id`),
  CONSTRAINT `buguserFK` FOREIGN KEY (`user`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `gbugroupFK` FOREIGN KEY (`group_id`) REFERENCES `xebt_bug_group` (`bug_group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebt_bugcomment_attachment`
--

DROP TABLE IF EXISTS `xebt_bugcomment_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebt_bugcomment_attachment` (
  `attach_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) DEFAULT NULL,
  `attach_path` mediumblob,
  `attach_title` varchar(450) DEFAULT NULL,
  PRIMARY KEY (`attach_id`),
  KEY `attach_comment_id_idx` (`comment_id`),
  CONSTRAINT `attach_comment_id` FOREIGN KEY (`comment_id`) REFERENCES `xebt_bugsummary` (`bs_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebt_bugsummary`
--

DROP TABLE IF EXISTS `xebt_bugsummary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebt_bugsummary` (
  `bs_id` int(11) NOT NULL AUTO_INCREMENT,
  `bug_id` int(11) DEFAULT NULL,
  `bug_status` int(11) DEFAULT NULL,
  `bug_priority` int(11) DEFAULT NULL,
  `assignee` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT '5',
  `update_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `prev_status` int(11) DEFAULT NULL,
  `prev_priority` int(11) DEFAULT NULL,
  `prev_assignee` int(11) DEFAULT NULL,
  `comment_desc` longtext,
  `bug_severity` int(11) DEFAULT NULL,
  `prev_severity` int(11) DEFAULT NULL,
  `group_assign` int(11) DEFAULT '2',
  `prev_category` int(11) DEFAULT '5',
  `manual_build` int(11) DEFAULT NULL,
  `prev_manual_build` int(11) DEFAULT NULL,
  `automation_build` int(11) DEFAULT NULL,
  `prev_automation_build` int(11) DEFAULT NULL,
  `is_build_assigned` int(11) DEFAULT '3',
  PRIMARY KEY (`bs_id`),
  KEY `summary_bug_id_idx` (`bug_id`),
  KEY `summary_bug_status_idx` (`bug_status`),
  KEY `summary_bug_priority_idx` (`bug_priority`),
  KEY `summary_bug_assigne_idx` (`assignee`),
  KEY `summary_updated_by_idx` (`updated_by`),
  KEY `summary_prev_status_idx` (`prev_status`),
  KEY `summary_prev_priority_idx` (`prev_priority`),
  KEY `summary_prev_assigne_idx` (`prev_assignee`),
  KEY `summary_bug_severity_idx` (`bug_severity`),
  KEY `summary_prev_severity_idx` (`prev_severity`),
  KEY `summary_group_assign_idx` (`group_assign`),
  CONSTRAINT `summary_bug_assignee` FOREIGN KEY (`assignee`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `summary_bug_id` FOREIGN KEY (`bug_id`) REFERENCES `xebt_bug` (`bug_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `summary_bug_priority` FOREIGN KEY (`bug_priority`) REFERENCES `xebt_priority` (`priority_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `summary_bug_severity` FOREIGN KEY (`bug_severity`) REFERENCES `xebt_severity` (`severity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `summary_bug_status` FOREIGN KEY (`bug_status`) REFERENCES `xebt_status` (`status_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `summary_group_assign` FOREIGN KEY (`group_assign`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `summary_prev_assignee` FOREIGN KEY (`prev_assignee`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `summary_prev_priority` FOREIGN KEY (`prev_priority`) REFERENCES `xebt_priority` (`priority_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `summary_prev_severity` FOREIGN KEY (`prev_severity`) REFERENCES `xebt_severity` (`severity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `summary_prev_status` FOREIGN KEY (`prev_status`) REFERENCES `xebt_status` (`status_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `summary_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebt_category`
--

DROP TABLE IF EXISTS `xebt_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebt_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) DEFAULT NULL,
  `category_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  KEY `catStatus_idx` (`category_status`),
  CONSTRAINT `catStatus` FOREIGN KEY (`category_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebt_priority`
--

DROP TABLE IF EXISTS `xebt_priority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebt_priority` (
  `priority_id` int(11) NOT NULL AUTO_INCREMENT,
  `bug_priority` varchar(45) DEFAULT NULL,
  `priority_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`priority_id`),
  KEY `prioStatusFK_idx` (`priority_status`),
  CONSTRAINT `prioStatusFK` FOREIGN KEY (`priority_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebt_severity`
--

DROP TABLE IF EXISTS `xebt_severity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebt_severity` (
  `severity_id` int(11) NOT NULL AUTO_INCREMENT,
  `bug_severity` varchar(45) DEFAULT NULL,
  `severity_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`severity_id`),
  KEY `severityStatusFK_idx` (`severity_status`),
  CONSTRAINT `severityStatusFK` FOREIGN KEY (`severity_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xebt_status`
--

DROP TABLE IF EXISTS `xebt_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebt_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `bug_status` varchar(45) DEFAULT NULL,
  `bug_state` int(11) DEFAULT NULL,
  PRIMARY KEY (`status_id`),
  KEY `bugStateFK_idx` (`bug_state`),
  CONSTRAINT `bugStateFK` FOREIGN KEY (`bug_state`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xetm_comments_likes`
--

DROP TABLE IF EXISTS `xetm_comments_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xetm_comments_likes` (
  `xetm_comments_likes_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) DEFAULT NULL,
  `liked_by_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`xetm_comments_likes_id`),
  KEY `comment_idFK_idx` (`comment_id`),
  KEY `likeUserFk_idx` (`liked_by_user`),
  CONSTRAINT `comment_idFK` FOREIGN KEY (`comment_id`) REFERENCES `xetm_testcase_comments` (`xetm_testcase_comments_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `likeUserFk` FOREIGN KEY (`liked_by_user`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xetm_exec_status`
--

DROP TABLE IF EXISTS `xetm_exec_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xetm_exec_status` (
  `exst_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`exst_id`),
  KEY `status_exe_idx` (`status`),
  CONSTRAINT `status_exe` FOREIGN KEY (`status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xetm_execution_type`
--

DROP TABLE IF EXISTS `xetm_execution_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xetm_execution_type` (
  `execution_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`execution_type_id`),
  KEY `status_idx` (`status`),
  CONSTRAINT `exec_status` FOREIGN KEY (`status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xetm_scenario`
--

DROP TABLE IF EXISTS `xetm_scenario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xetm_scenario` (
  `scenario_id` int(11) NOT NULL AUTO_INCREMENT,
  `scenario_name` longtext NOT NULL,
  `scenario_description` longtext,
  `module_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`scenario_id`),
  KEY `module_idx` (`module_id`),
  KEY `module_status_idx` (`status`),
  CONSTRAINT `module` FOREIGN KEY (`module_id`) REFERENCES `xe_module` (`module_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `module_status` FOREIGN KEY (`status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xetm_testcase`
--

DROP TABLE IF EXISTS `xetm_testcase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xetm_testcase` (
  `testcase_id` int(11) NOT NULL AUTO_INCREMENT,
  `testcase_name` longtext NOT NULL,
  `scenario_id` int(11) DEFAULT NULL,
  `summary` longtext,
  `precondition` longtext,
  `status` int(11) DEFAULT NULL,
  `execution_type` int(11) DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `test_prefix` varchar(45) DEFAULT NULL,
  `projectid` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `excel_file_present_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`testcase_id`),
  KEY `execution_type_idx` (`execution_type`),
  KEY `testcase_author_idx` (`author`),
  KEY `testcase_updated_by_idx` (`updated_by`),
  KEY `status_id_idx` (`status`),
  KEY `project_tc_idx` (`projectid`),
  KEY `scenario_id_fk_idx` (`scenario_id`),
  KEY `module_id_fk_idx` (`module_id`),
  KEY `excel_file_present_status_fk_idx` (`excel_file_present_status`),
  CONSTRAINT `excel_file_present_status_fk` FOREIGN KEY (`excel_file_present_status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `execution_type` FOREIGN KEY (`execution_type`) REFERENCES `xetm_execution_type` (`execution_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `module_id_fk` FOREIGN KEY (`module_id`) REFERENCES `xe_module` (`module_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_tc` FOREIGN KEY (`projectid`) REFERENCES `xe_project` (`project_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scenario_id_fk` FOREIGN KEY (`scenario_id`) REFERENCES `xetm_scenario` (`scenario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `status_id` FOREIGN KEY (`status`) REFERENCES `xetm_testcase_status` (`tc_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `testcase_author` FOREIGN KEY (`author`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `testcase_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xetm_testcase_comments`
--

DROP TABLE IF EXISTS `xetm_testcase_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xetm_testcase_comments` (
  `xetm_testcase_comments_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_description` longtext,
  `commnetor` int(11) DEFAULT NULL,
  `comment_time` datetime DEFAULT NULL,
  `testcase_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`xetm_testcase_comments_id`),
  KEY `commentorFK_idx` (`commnetor`),
  KEY `testcaseCommnetFK_idx` (`testcase_id`),
  CONSTRAINT `commentorFK` FOREIGN KEY (`commnetor`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `testcaseCommentFK` FOREIGN KEY (`testcase_id`) REFERENCES `xetm_testcase` (`testcase_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xetm_testcase_status`
--

DROP TABLE IF EXISTS `xetm_testcase_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xetm_testcase_status` (
  `tc_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  `testcase_state` int(11) DEFAULT NULL,
  PRIMARY KEY (`tc_status_id`),
  KEY `tcStateFK_idx` (`testcase_state`),
  CONSTRAINT `tcStateFK` FOREIGN KEY (`testcase_state`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xetm_testcase_steps`
--

DROP TABLE IF EXISTS `xetm_testcase_steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xetm_testcase_steps` (
  `step_id` int(11) NOT NULL AUTO_INCREMENT,
  `testcase_id` int(11) DEFAULT NULL,
  `action` longtext,
  `result` longtext,
  `test_step_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`step_id`),
  KEY `testcase_id_idx` (`testcase_id`),
  CONSTRAINT `testcase_id` FOREIGN KEY (`testcase_id`) REFERENCES `xetm_testcase` (`testcase_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xetm_testcase_summary`
--

DROP TABLE IF EXISTS `xetm_testcase_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xetm_testcase_summary` (
  `test_summary_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_case_id` int(11) DEFAULT NULL,
  `activity_desc` varchar(2000) DEFAULT NULL,
  `activity_date` datetime DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  PRIMARY KEY (`test_summary_id`),
  KEY `user_FK_idx` (`user`),
  KEY `test_case_id_FK_idx` (`test_case_id`),
  CONSTRAINT `test_case_id_FK` FOREIGN KEY (`test_case_id`) REFERENCES `xetm_testcase` (`testcase_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_FK` FOREIGN KEY (`user`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `xe_vm_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `VMFstatus_idx` (`status`),
  CONSTRAINT `VMFstatus` FOREIGN KEY (`status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `xesfdc_automation_build` (
  `build_id` int(11) NOT NULL AUTO_INCREMENT,
  `build_name` varchar(400) DEFAULT NULL,
  `build_desc` varchar(400) DEFAULT NULL,
  `build_status` int(11) DEFAULT NULL,
  `release_id` int(11) DEFAULT NULL,
  `build_state` int(11) DEFAULT NULL,
  `build_createdate` datetime DEFAULT NULL,
  `build_enddate` datetime DEFAULT NULL,
  `build_creator` int(11) DEFAULT NULL,
  `build_env` int(11) DEFAULT NULL,
  `build_logpath` varchar(200) DEFAULT NULL,
  `build_updatedate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`build_id`),
  KEY `status_FK_idx` (`build_status`),
  KEY `release_FK_idx` (`release_id`),
  KEY `executed_FK_idx` (`build_state`),
  KEY `buildCreator_FK _idx` (`build_creator`),
  KEY `buildEnvFk_idx` (`build_env`),
  CONSTRAINT `buildEnvFk` FOREIGN KEY (`build_env`) REFERENCES `xesfdc_env` (`env_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sfdc_buildCreator_FK ` FOREIGN KEY (`build_creator`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sfdc_buildState_FK` FOREIGN KEY (`build_state`) REFERENCES `xebm_build_state` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sfdc_buildStatus_FK` FOREIGN KEY (`build_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sfdc_release_FK` FOREIGN KEY (`release_id`) REFERENCES `xe_release` (`release_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `xesfdc_automation_build_testcase` (
  `build_tc_id` int(11) NOT NULL AUTO_INCREMENT,
  `sfdc_build_id` int(11) DEFAULT NULL,
  `sfdc_testcase_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`build_tc_id`),
  KEY `sfdc_tc_fk_idx` (`sfdc_testcase_id`),
  KEY `sfdc_build_fk_idx` (`sfdc_build_id`),
  CONSTRAINT `sfdc_build_fk` FOREIGN KEY (`sfdc_build_id`) REFERENCES `xesfdc_automation_build` (`build_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `sfdc_tc_fk` FOREIGN KEY (`sfdc_testcase_id`) REFERENCES `xesfdc_tc` (`sfdc_tc_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `xesfdc_build_exec_details` (
  `build_exec_id` int(11) NOT NULL AUTO_INCREMENT,
  `build_id` int(11) DEFAULT NULL,
  `vm_id` int(11) DEFAULT NULL,
  `browser` varchar(45) DEFAULT NULL,
  `mail_group_id` int(11) DEFAULT NULL,
  `step_wise_screenshot` int(11) DEFAULT NULL,
  `report_bug` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `start_time` varchar(45) DEFAULT NULL,
  `end_time` varchar(45) DEFAULT NULL,
  `complete_status` int(11) DEFAULT '2',
  PRIMARY KEY (`build_exec_id`),
  KEY `scriptBuildFk1_idx` (`build_id`),
  KEY `vmIdFk_script_idx` (`vm_id`),
  KEY `mailGrpFkScript_idx` (`mail_group_id`),
  KEY `scriptstepWiseScreenshot_idx` (`step_wise_screenshot`),
  KEY `userIdscriptFkExec_idx` (`user_id`),
  KEY `completeStatusscriptFk_idx` (`complete_status`),
  CONSTRAINT `mailGrpFkScript` FOREIGN KEY (`mail_group_id`) REFERENCES `xe_mail_group` (`mail_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `scriptBuildFk1` FOREIGN KEY (`build_id`) REFERENCES `xesfdc_automation_build` (`build_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `userIdscriptFkExec` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `xesfdc_build_tc_step_val` (
  `build_tc_step_val_id` int(11) NOT NULL AUTO_INCREMENT,
  `build_tc_id` int(11) DEFAULT NULL,
  `tc_step_id` int(11) DEFAULT NULL,
  `step_val` longtext,
  `build_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`build_tc_step_val_id`),
  KEY `tcstepidfk_idx` (`tc_step_id`),
  KEY `buildtcidfk_idx` (`build_tc_id`),
  KEY `sfdcbuildtcidfk_idx` (`build_id`),
  CONSTRAINT `buildtcidfk` FOREIGN KEY (`build_tc_id`) REFERENCES `xesfdc_tc` (`sfdc_tc_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sfdcbuildtcidfk` FOREIGN KEY (`build_id`) REFERENCES `xesfdc_automation_build` (`build_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tcstepidfk` FOREIGN KEY (`tc_step_id`) REFERENCES `xesfdc_test_steps` (`sfdc_tc_steps`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

CREATE TABLE `xesfdc_env` (
  `env_id` int(11) NOT NULL AUTO_INCREMENT,
  `env_name` longtext,
  `env_desc` longtext,
  `login_url` longtext,
  `env_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`env_id`),
  KEY `active_statsu_idx` (`env_status`),
  CONSTRAINT `active_statsu` FOREIGN KEY (`env_status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `xesfdc_func` (
  `functionality_id` int(11) NOT NULL AUTO_INCREMENT,
  `functionality` varchar(45) DEFAULT NULL,
  `func_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`functionality_id`),
  KEY `funcstatusfk_idx` (`func_status`),
  CONSTRAINT `funcstatusfk` FOREIGN KEY (`func_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `xesfdc_locator_type` (
  `loc_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(45) DEFAULT NULL,
  `type_desc` varchar(45) DEFAULT NULL,
  `type_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`loc_type_id`),
  KEY `ty_status_idx` (`type_status`),
  CONSTRAINT `ty_status` FOREIGN KEY (`type_status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `xesfdc_pages` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `pagename` varchar(100) DEFAULT NULL,
  `env_id` int(11) DEFAULT NULL,
  `xpath` longtext,
  `page_status` int(11) DEFAULT NULL,
  `is_scanned` int(11) DEFAULT NULL,
  PRIMARY KEY (`page_id`),
  KEY `pgstatusfk_idx` (`page_status`),
  KEY `pgEnvIdFk_idx` (`env_id`),
  KEY `isScannedFk_idx` (`is_scanned`),
  CONSTRAINT `isScannedFk` FOREIGN KEY (`is_scanned`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `pgEnvIdFk` FOREIGN KEY (`env_id`) REFERENCES `xesfdc_env` (`env_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `pgstatusfk` FOREIGN KEY (`page_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `xesfdc_repository` (
  `repo_id` int(11) NOT NULL AUTO_INCREMENT,
  `scanner_id` int(11) DEFAULT NULL,
  `label` longtext,
  `type` int(11) DEFAULT NULL,
  `xpath` longtext,
  `required` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`repo_id`),
  KEY `type_id_idx` (`type`),
  KEY `scannerFk_idx` (`scanner_id`),
  CONSTRAINT `scannerFkrepo` FOREIGN KEY (`scanner_id`) REFERENCES `xesfdc_scanner` (`scanner_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `type_id` FOREIGN KEY (`type`) REFERENCES `xesfdc_locator_type` (`loc_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `xesfdc_scanner` (
  `scanner_id` int(11) NOT NULL AUTO_INCREMENT,
  `env` int(11) DEFAULT NULL,
  `pagename_id` int(11) DEFAULT NULL,
  `functionality_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`scanner_id`),
  KEY `pageidfk_idx` (`pagename_id`),
  KEY `funcidfk_idx` (`functionality_id`),
  KEY `scanuserfk_idx` (`user_id`),
  KEY `envfk_idx` (`env`),
  CONSTRAINT `envfk` FOREIGN KEY (`env`) REFERENCES `xesfdc_env` (`env_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `funcidfk` FOREIGN KEY (`functionality_id`) REFERENCES `xesfdc_func` (`functionality_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pageidfk` FOREIGN KEY (`pagename_id`) REFERENCES `xesfdc_pages` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `scanuserfk` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `xesfdc_tc` (
  `sfdc_tc_id` int(11) NOT NULL AUTO_INCREMENT,
  `testcase_name` longtext,
  `testcase_desc` longtext,
  `sfdc_page_id` int(11) DEFAULT NULL,
  `sfdc_functionality_id` int(11) DEFAULT NULL,
  `sfdc_env` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`sfdc_tc_id`),
  KEY `envfk_idx` (`sfdc_env`),
  KEY `funcfk_idx` (`sfdc_functionality_id`),
  KEY `pgidfk_idx` (`sfdc_page_id`),
  KEY `useridfk1_idx` (`user_id`),
  CONSTRAINT `envidfk1` FOREIGN KEY (`sfdc_env`) REFERENCES `xesfdc_env` (`env_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `funcidfk1` FOREIGN KEY (`sfdc_functionality_id`) REFERENCES `xesfdc_func` (`functionality_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pageidfk1` FOREIGN KEY (`sfdc_page_id`) REFERENCES `xesfdc_pages` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `useridfk1` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `xesfdc_test_steps` (
  `sfdc_tc_steps` int(11) NOT NULL AUTO_INCREMENT,
  `sfdc_testcase` int(11) DEFAULT NULL,
  `repo_id` int(11) DEFAULT NULL,
  `step_desc` longtext,
  `step_no` int(11) NOT NULL,
  PRIMARY KEY (`sfdc_tc_steps`),
  KEY `testcase_idx` (`sfdc_testcase`),
  KEY `repository_idFk_idx` (`repo_id`),
  CONSTRAINT `repository_idFk` FOREIGN KEY (`repo_id`) REFERENCES `xesfdc_repository` (`repo_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `testcase` FOREIGN KEY (`sfdc_testcase`) REFERENCES `xesfdc_tc` (`sfdc_tc_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `xesfdc_vm_details` (
  `xe_vm_id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(100) DEFAULT NULL,
  `vm_username` varchar(100) DEFAULT NULL,
  `vm_password` varchar(100) DEFAULT NULL,
  `ip_address` varchar(100) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `project_location` longtext,
  `cust_vm_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `build_id` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `vm_state` int(11) DEFAULT NULL,
  PRIMARY KEY (`xe_vm_id`),
  KEY `scrvm_status_idx` (`status`),
  KEY `scrvm_stateFK_idx` (`vm_state`),
  KEY `scrbuild_idFK_idx` (`build_id`),
  KEY `scruser_idFK_idx` (`userId`),
  CONSTRAINT `scrbuild_idFK` FOREIGN KEY (`build_id`) REFERENCES `xesfdc_automation_build` (`build_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scruserIdVMFK` FOREIGN KEY (`userId`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scrvm_stateFK` FOREIGN KEY (`vm_state`) REFERENCES `xe_vm_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scrvm_status` FOREIGN KEY (`status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `xesfdc_vm_exec_details` (
  `vm_exec_id` int(11) NOT NULL AUTO_INCREMENT,
  `vm_id` int(11) DEFAULT NULL,
  `build_exec_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`vm_exec_id`),
  KEY `scrvmExecutionFK_idx` (`vm_id`),
  KEY `scrbuildExecFk_idx` (`build_exec_id`),
  CONSTRAINT `scrbuildExecFk` FOREIGN KEY (`build_exec_id`) REFERENCES `xesfdc_build_exec_details` (`build_exec_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `scrvmExecutionFK` FOREIGN KEY (`vm_id`) REFERENCES `xesfdc_vm_details` (`xe_vm_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `xesfdc_automation_execution_summary`
--
DROP TABLE IF EXISTS `xesfdc_automation_execution_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xesfdc_automation_execution_summary` (
  `exe_id` int(11) NOT NULL AUTO_INCREMENT,
  `build_id` int(11) DEFAULT NULL,
  `env_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `tc_id` int(11) DEFAULT NULL,
  `tc_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`exe_id`),
  KEY `scBuildIdFk_idx` (`build_id`),
  KEY `scEnvIdFk_idx` (`env_id`),
  KEY `scPageIdFk_idx` (`page_id`),
  KEY `scTcIdFk_idx` (`tc_id`),
  CONSTRAINT `scBuildIdFk` FOREIGN KEY (`build_id`) REFERENCES `xesfdc_automation_build` (`build_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scEnvIdFk` FOREIGN KEY (`env_id`) REFERENCES `xesfdc_env` (`env_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scPageIdFk` FOREIGN KEY (`page_id`) REFERENCES `xesfdc_pages` (`page_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scTcIdFk` FOREIGN KEY (`tc_id`) REFERENCES `xesfdc_tc` (`sfdc_tc_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xesfdc_automation_steps_execution_summary`
--
DROP TABLE IF EXISTS `xesfdc_automation_steps_execution_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xesfdc_automation_steps_execution_summary` (
  `step_exe_id` int(11) NOT NULL AUTO_INCREMENT,
  `build_id` int(11) DEFAULT NULL,
  `env_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `tc_id` int(11) DEFAULT NULL,
  `step_id` int(11) DEFAULT NULL,
  `step_value` longtext,
  `actual_result` longtext,
  `is_bug_raised` int(11) DEFAULT NULL,
  `step_status` int(11) DEFAULT NULL,
  `step_details` longtext,
  `screenshot` blob,
  `screenshot_title` text,
  PRIMARY KEY (`step_exe_id`),
  KEY `scStepsBuildfk_idx` (`build_id`),
  KEY `scStepsEnvfk_idx` (`env_id`),
  KEY `scStepsPagefk_idx` (`page_id`),
  KEY `scStepsTcfk_idx` (`tc_id`),
  KEY `scStepsStepfk_idx` (`step_id`),
  CONSTRAINT `scStepsBuildfk` FOREIGN KEY (`build_id`) REFERENCES `xesfdc_automation_build` (`build_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scStepsEnvfk` FOREIGN KEY (`env_id`) REFERENCES `xesfdc_env` (`env_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scStepsPagefk` FOREIGN KEY (`page_id`) REFERENCES `xesfdc_pages` (`page_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scStepsStepfk` FOREIGN KEY (`step_id`) REFERENCES `xesfdc_test_steps` (`sfdc_tc_steps`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scStepsTcfk` FOREIGN KEY (`tc_id`) REFERENCES `xesfdc_tc` (`sfdc_tc_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xesfdc_automation_step_execution_bug`
--
DROP TABLE IF EXISTS `xesfdc_automation_step_execution_bug`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xesfdc_automation_step_execution_bug` (
  `exe_bug_id` int(11) NOT NULL AUTO_INCREMENT,
  `bug_id` int(11) DEFAULT NULL,
  `step_id` int(11) DEFAULT NULL,
  `step_exe_id` int(11) DEFAULT NULL,
  `test_case_id` int(11) DEFAULT NULL,
  `build_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`exe_bug_id`),
  KEY `scStepBug_stepIdFk_idx` (`step_id`),
  KEY `scStepBug_bugIfFk_idx` (`bug_id`),
  KEY `scStepBug_tcIdFk_idx` (`test_case_id`),
  KEY `scStepBug_BuildIdFk_idx` (`build_id`),
  CONSTRAINT `scStepBug_stepIdFk` FOREIGN KEY (`step_id`) REFERENCES `xesfdc_test_steps` (`sfdc_tc_steps`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scStepBug_bugIfFk` FOREIGN KEY (`bug_id`) REFERENCES `xebt_bug` (`bug_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scStepBug_stepExeIdFk` FOREIGN KEY (`exe_bug_id`) REFERENCES `xesfdc_automation_steps_execution_summary` (`step_exe_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scStepBug_tcIdFk` FOREIGN KEY (`test_case_id`) REFERENCES `xesfdc_tc` (`sfdc_tc_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scStepBug_BuildIdFk` FOREIGN KEY (`build_id`) REFERENCES `xesfdc_automation_build` (`build_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `xebt_customizesearch`;

CREATE TABLE `xebt_customizesearch` (
  `idxebt` int(11) NOT NULL AUTO_INCREMENT,
  `queryname` longtext,
  `query` longtext,
  `queryjson` longtext,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`idxebt`),
  KEY `fk_userID_idx` (`user_id`),
  CONSTRAINT `fk_userID` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `xebt_piechartsearch`;

CREATE TABLE `xebt_piechartsearch` (
  `idxebt` int(11) NOT NULL AUTO_INCREMENT,
  `queryname` longtext,
  `query` longtext,
  `queryjson` longtext,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`idxebt`),
  KEY `fk_userID_idx` (`user_id`),
  CONSTRAINT `fk_userIDpie` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `xead_jenkins_build_details`; 
DROP TABLE IF EXISTS `xead_jenkin_jobs`; 
DROP TABLE IF EXISTS `xead_jenkins_build_status`; 
DROP TABLE IF EXISTS `xead_jenkins_details`;

CREATE TABLE `xead_jenkins_details` (
  `jenkin_id` INT NOT NULL AUTO_INCREMENT,
  `jenkin_url` LONGTEXT NULL,
  `jenkin_token` LONGTEXT NULL,
  `jenkin_username` LONGTEXT NULL,
  `jenkin_name` LONGTEXT NULL,
  `jenkin_instance_status` INT REFERENCES `xe_active`.`active_id`,
  PRIMARY KEY (`jenkin_id`));
  

  

CREATE TABLE `xead_jenkins_build_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `build_status` VARCHAR(45) NULL,
  `status` INT NULL,
   PRIMARY KEY (`id`),
   INDEX `statusBstate_idx` (`status` ASC),
   CONSTRAINT `statusBstate`
   FOREIGN KEY (`status`)
   REFERENCES `xe_active` (`active_id`)
   ON DELETE CASCADE
   ON UPDATE CASCADE
 );
 
 
CREATE TABLE `xead_jenkin_jobs` (
  `jenkin_jobId` int(11) NOT NULL AUTO_INCREMENT,
  `instance` int(11) DEFAULT NULL,
  `jenkin_job` longtext,
  `job_name` longtext,
  `job_description` longtext,
  `job_status` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `build_status` int(11) DEFAULT NULL,
  `build_trigger_token` longtext,
   PRIMARY KEY (`jenkin_jobId`),
   KEY `user_idx` (`user_id`),
   KEY `instanceFK_idx` (`instance`),
   KEY `build_status_idx` (`build_status`),
   CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
   CONSTRAINT `instance` FOREIGN KEY (`instance`) REFERENCES `xead_jenkins_details` (`jenkin_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
   CONSTRAINT `build_status` FOREIGN KEY (`build_status`) REFERENCES `xead_jenkins_build_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;


 CREATE TABLE `xead_jenkins_build_details` (
  `details_id` int(11) NOT NULL AUTO_INCREMENT,
  `build_id` int(11) DEFAULT NULL,
  `build_name` varchar(1000) DEFAULT NULL,
  `total_count` int(11) DEFAULT NULL,
  `fail_count` int(11) DEFAULT NULL,
  `skip_count` int(11) DEFAULT NULL,
  `duration` varchar(45) DEFAULT NULL,
  `estimated_duration` varchar(45) DEFAULT NULL,
  `result` varchar(45) DEFAULT NULL,
  `timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  `url` varchar(2000) DEFAULT NULL,
  `jenkin_jobId` int(11) DEFAULT NULL,
  PRIMARY KEY (`details_id`),
  KEY `job_id_idx` (`jenkin_jobId`),
  CONSTRAINT `jenkin_jobId` FOREIGN KEY (`jenkin_jobId`) REFERENCES `xead_jenkin_jobs` (`jenkin_jobId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `xead_user_jobs`;

CREATE TABLE `xead_user_jobs` (
  `userjob_id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NULL REFERENCES `xe_user_details` (`user_id`),
  `job_id` INT NULL REFERENCES `xead_jenkin_jobs` (`jenkin_jobId`),
  `update_status` INT NULL,
  `execute_status` INT NULL,
  PRIMARY KEY (`userjob_id`)
  );
  
 DROP TABLE IF EXISTS `xead_user_jenkins_instance`;
 
 CREATE TABLE `xead_user_jenkins_instance` (
  `user_instance_id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NULL,
  `instance_id` INT NULL,
  `update_status` INT NULL,
  `view_status` INT NULL,
  PRIMARY KEY (`user_instance_id`),
  INDEX `update_status_key_idx` (`view_status` ASC),
  INDEX `instance_id_key_idx` (`update_status` ASC),
  CONSTRAINT `update_status_key`
    FOREIGN KEY (`update_status`)
    REFERENCES `xe_active` (`active_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `view_status_key`
    FOREIGN KEY (`view_status`)
    REFERENCES `xe_active` (`active_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
 DROP TABLE IF EXISTS `xead_user_git_instance`;
 
 CREATE TABLE `xead_user_git_instance` (
  `user_instance_id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NULL,
  `instance_id` INT NULL,
  `update_status` INT NULL,
  `view_status` INT NULL,
  PRIMARY KEY (`user_instance_id`),
  INDEX `update_status_key_index` (`view_status` ASC),
  INDEX `instance_id_key_index` (`update_status` ASC),
  CONSTRAINT `update_status_key1`
    FOREIGN KEY (`update_status`)
    REFERENCES `xe_active` (`active_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `view_status_key1`
    FOREIGN KEY (`view_status`)
    REFERENCES `xe_active` (`active_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


 DROP TABLE IF EXISTS `xetm_testcase_datasheet`;
 
  CREATE TABLE `xetm_testcase_datasheet` (
  `datasheet_id` INT NOT NULL AUTO_INCREMENT,
  `testcase_id` INT NULL,
  `user_id` INT NULL,
  `datasheet_file` BLOB NULL,
  `datasheet_filename` VARCHAR(200) NULL,
  PRIMARY KEY (`datasheet_id`),
  INDEX `testcase_id_idx` (`testcase_id` ASC),
  INDEX `user_id_idx` (`user_id` ASC),
  CONSTRAINT `ds_testcase_id`
    FOREIGN KEY (`testcase_id`)
    REFERENCES `xetm_testcase` (`testcase_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ds_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `xe_user_details` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


DROP TABLE IF EXISTS `xebm_environment_details`;
CREATE TABLE `xebm_environment_details` (
  `env_id` int(11) NOT NULL AUTO_INCREMENT,
  `env_name` varchar(50) DEFAULT NULL,
  `description` varchar(45) NOT NULL,
  `config_parameter` varchar(45) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`env_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

 DROP TABLE IF EXISTS `xebm_env_variables`;
 CREATE TABLE `xebm_env_variables` (
 `variable_id` int(11) NOT NULL AUTO_INCREMENT,
 `env_id` int(11) NOT NULL,
 `parameter_name` varchar(45) NOT NULL,
 `config_val` varchar(45) NOT NULL,
 `parameter_val` varchar(45) NOT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `env_id_idx` (`env_id`),
  CONSTRAINT `env_id` FOREIGN KEY (`env_id`) REFERENCES `xebm_environment_details` (`env_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `xead_git_project_details`;
CREATE TABLE `xead_git_project_details` (
  `git_instance_id` int(11) NOT NULL AUTO_INCREMENT,
  `git_instance_name` varchar(100) DEFAULT NULL,
  `git_server_url` varchar(200) DEFAULT NULL,
  `api_token` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `created_by_user` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`git_instance_id`),
  KEY `user_id_key_idx` (`created_by_user`),
  KEY `status_key_idx` (`status`),
  CONSTRAINT `user_id_key_fk` FOREIGN KEY (`created_by_user`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `status_key_fk` FOREIGN KEY (`status`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `xebm_build_exec_env`;
CREATE TABLE `xebm_build_exec_env` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build_exec_id` int(11) DEFAULT NULL,
  `env_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `buildExecFk_idx` (`build_exec_id`),
  KEY `envidfkVal_idx` (`env_id`),
  CONSTRAINT `buildExecFkval` FOREIGN KEY (`build_exec_id`) REFERENCES `xebm_build_exec_details` (`build_exec_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `envidfkVal` FOREIGN KEY (`env_id`) REFERENCES `xebm_environment_details` (`env_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `xead_git_repositories`;
CREATE TABLE `xead_git_repositories` (
  `repo_id` int(11) NOT NULL AUTO_INCREMENT,
  `repo_name` varchar(45) NOT NULL,
  `branch_name` varchar(45) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`repo_id`),
  KEY `user_idx` (`user_id`),
  KEY `projectFK_idx` (`project_id`),
  CONSTRAINT `project_id` FOREIGN KEY (`project_id`) REFERENCES `xead_git_project_details` (`git_instance_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_idx` FOREIGN KEY (`user_id`) REFERENCES `xe_user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `xetm_tc_dataset` (
  `dataset_id` int(11) NOT NULL AUTO_INCREMENT,
  `dataset_name` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `tc_exe_id` int(11) DEFAULT NULL,
  `dataset_no` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`dataset_id`),
  KEY `testcase_fk_idx` (`tc_exe_id`),
  CONSTRAINT `tcExeId` FOREIGN KEY (`tc_exe_id`) REFERENCES `xebm_automation_execution_summary` (`exe_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `xe_components` and `xe_scenario_components`
--
DROP TABLE IF EXISTS `xe_components`;
DROP TABLE IF EXISTS `xe_scenario_components`;
CREATE TABLE `xe_components` (
  `component_id` int(11) NOT NULL AUTO_INCREMENT,
  `component_name` varchar(45) NOT NULL,
  `component_description` varchar(45) NOT NULL,
  `component_active` int(11) NOT NULL, 
  PRIMARY KEY (`component_id`),
  KEY `component_active_key_idx` (`component_active`),
  CONSTRAINT `component_active_key_fk` FOREIGN KEY (`component_active`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `xe_scenario_components` (
  `scenario_component_id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `scenario_id` int(11) NOT NULL,
  PRIMARY KEY (`scenario_component_id`),
  KEY `component_idx` (`component_id`),
  KEY `scenario_idx` (`scenario_id`),
  CONSTRAINT `xe_components_ibfk` FOREIGN KEY (`component_id`) REFERENCES `xe_components` (`component_id`) ON UPDATE CASCADE  ON DELETE CASCADE,
  CONSTRAINT `xetm_scenario_ibfk` FOREIGN KEY (`scenario_id`) REFERENCES `xetm_scenario` (`scenario_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

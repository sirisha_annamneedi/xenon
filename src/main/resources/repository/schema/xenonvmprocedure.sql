CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewVM`(
hostNameTemp varchar(100),
ipAddress varchar(100),
portNumber INT(11),
projectLocation varchar(1000),
ieStatus INT(11),
chromeStatus INT(11),
firefoxStatus INT(11),
safariStatus INT(11),
concurrentExecStatus INT(11),
concurrentExecs INT(11),
statusVal INT(11),
remoteUsername VARCHAR(100),
remotePassword VARCHAR(100),
cust_id INT(11),
repoStatus INT(11)
) RETURNS int(11)
BEGIN
DECLARE count,insertStatus INT DEFAULT 0;
SET insertStatus=-1;
    
	SELECT count(*) INTO count FROM xevm_customervm_details where hostname=hostNameTemp;
          
        IF count=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF count=0 THEN 
        INSERT INTO `xevm_customervm_details` (`cust_vm_id`,`hostname`, `ip_address`,`port`, `project_location`, `ie_status`, `chrome_status`, `firefox_status`, `safari_status`,`concurrent_exec_status`,`concurrent_execs`, `status`,`vm_username`,`vm_password`,`customer_id`,`git_status`) 
        VALUES (0,hostNameTemp, ipAddress,portNumber, projectLocation,ieStatus, chromeStatus, firefoxStatus, safariStatus,concurrentExecStatus,concurrentExecs, statusVal,remoteUsername,remotePassword,cust_id,repoStatus);
              
        SET insertStatus=1;
                               
        END IF;
RETURN insertStatus;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `allocateCustomerVM`(IN vmId INT(11),
IN userId INT(11), IN buildId INT(11), IN customerId VARCHAR(45))
BEGIN
DECLARE schemaName VARCHAR(250);
SELECT customer_schema INTO schemaName FROM `xenon-core`.xe_customer_details where customer_id=customerId;

SELECT * FROM `xenonvm`.`xevm_customervm_details` WHERE cust_vm_id = vmId and customer_id = customerId;

SET @queryToExec=CONCAT('INSERT INTO `',schemaName,'`.`xebm_vm_details`(`xe_vm_id`,`hostname`,`vm_username`,`vm_password`,
`ip_address`,`port`,`project_location`,`cust_vm_id`,`status`,`build_id`, `userId`, 
`vm_state`) SELECT 0,
    `xevm_customervm_details`.`hostname`,
    `xevm_customervm_details`.`vm_username`,
    `xevm_customervm_details`.`vm_password`,
    `xevm_customervm_details`.`ip_address`,
    `xevm_customervm_details`.`port`,
    `xevm_customervm_details`.`project_location`,
     `xevm_customervm_details`.`cust_vm_id`,
    `xevm_customervm_details`.`status`,',buildId,',',userId,',1
FROM `xenonvm`.`xevm_customervm_details` WHERE cust_vm_id = ',vmId,' and customer_id = ',customerId,'');

SELECT @queryToExec;
PREPARE stmt FROM @queryToExec;
  EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

END ;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `allocateSfdcCustomerVM`(IN vmId INT(11),
IN userId INT(11), IN buildId INT(11), IN customerId VARCHAR(45))
BEGIN
DECLARE schemaName VARCHAR(250);
SELECT customer_schema INTO schemaName FROM `xenon-core`.xe_customer_details where customer_id=customerId;

SELECT * FROM `xenonvm`.`xevm_customervm_details` WHERE cust_vm_id = vmId and customer_id = customerId;

SET @queryToExec=CONCAT('INSERT INTO `',schemaName,'`.`xesfdc_vm_details`(`xe_vm_id`,`hostname`,`vm_username`,`vm_password`,
`ip_address`,`port`,`project_location`,`cust_vm_id`,`status`,`build_id`, `userId`, 
`vm_state`) SELECT 0,
    `xevm_customervm_details`.`hostname`,
    `xevm_customervm_details`.`vm_username`,
    `xevm_customervm_details`.`vm_password`,
    `xevm_customervm_details`.`ip_address`,
    `xevm_customervm_details`.`port`,
    `xevm_customervm_details`.`project_location`,
     `xevm_customervm_details`.`cust_vm_id`,
    `xevm_customervm_details`.`status`,',buildId,',',userId,',1
FROM `xenonvm`.`xevm_customervm_details` WHERE cust_vm_id = ',vmId,' and customer_id = ',customerId,'');

SELECT @queryToExec;
PREPARE stmt FROM @queryToExec;
  EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `allocateCloudeVM`(IN vmId INT(11),
IN userId INT(11), IN buildId INT(11), IN customerId VARCHAR(45),IN hookState INT(11))
BEGIN
DECLARE schemaName VARCHAR(250);

SELECT customer_schema INTO schemaName FROM `xenon-core`.xe_customer_details where customer_id=customerId;

SELECT * FROM `xenonvm`.`xevm_cloudevm_details` WHERE cloude_vm_id = vmId;

SELECT * FROM `xenonvm`.xevm_webhooks where hook = hookState;

SET @queryToExec=CONCAT('INSERT INTO `',schemaName,'`.`xebm_vm_details`(`xe_vm_id`,`hostname`,`vm_username`,`vm_password`,
`ip_address`,`port`,`project_location`,`cust_vm_id`,`status`,`build_id`, `userId`, 
`vm_state`) SELECT 0,
    `xevm_cloudevm_details`.`hostname`,
    `xevm_cloudevm_details`.`vm_username`,
    `xevm_cloudevm_details`.`vm_password`,
    `xevm_cloudevm_details`.`ip_address`,
    `xevm_cloudevm_details`.`port`,
    `xevm_cloudevm_details`.`project_location`,
     `xevm_cloudevm_details`.`cloude_vm_id`,
    `xevm_cloudevm_details`.`status`,',buildId,',',userId,',1
FROM `xenonvm`.`xevm_cloudevm_details` WHERE cloude_vm_id = ',vmId,'');

SELECT @queryToExec;
PREPARE stmt FROM @queryToExec;
  EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCloudeVMDetails`()
BEGIN

DECLARE vmId INT(11);

DECLARE concurrentExecs INT(11);
DECLARE currentExecs INT(11);
DECLARE concurrentExecsStatus INT(11);
SELECT * FROM `xenonvm`.xevm_cloudevm_details where is_free = 1 and status=1 LIMIT 1;
SELECT cloude_vm_id INTO vmId FROM `xenonvm`.xevm_cloudevm_details where is_free = 1 and status=1 LIMIT 1;

SELECT concurrent_exec_status INTO concurrentExecsStatus FROM `xenonvm`.xevm_cloudevm_details where is_free = 1 and status=1 and `cloude_vm_id`=vmId LIMIT 1;

IF concurrentExecsStatus=1 THEN
SELECT concurrent_execs INTO concurrentExecs FROM `xenonvm`.xevm_cloudevm_details where is_free = 1 and `cloude_vm_id`=vmId and status=1 and concurrent_exec_status=1 LIMIT 1;
SELECT current_execs INTO currentExecs FROM `xenonvm`.xevm_cloudevm_details where is_free = 1 and `cloude_vm_id`=vmId and status=1 LIMIT 1;

IF currentExecs<concurrentExecs THEN
UPDATE `xenonvm`.xevm_cloudevm_details SET `current_execs`=current_execs+1 WHERE `cloude_vm_id`=vmId and status=1;
END IF;
SELECT current_execs INTO currentExecs FROM `xenonvm`.xevm_cloudevm_details where is_free = 1 and `cloude_vm_id`=vmId  and status=1 LIMIT 1;

IF currentExecs>=concurrentExecs THEN
UPDATE `xenonvm`.xevm_cloudevm_details SET `is_free`='2' WHERE `cloude_vm_id`=vmId and status=1;
END IF;

END IF;
IF concurrentExecsStatus=2 THEN
UPDATE `xenonvm`.xevm_cloudevm_details SET `is_free`='2' WHERE `cloude_vm_id`=vmId and status=1;
END IF;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateCloudeDetails`(IN vmId INT(11),IN hookState INT(11),
IN userID INT(11), IN buildID INT(11), IN schemaName VARCHAR(45))
BEGIN

SELECT * FROM `xenonvm`.xevm_cloudevm_details where cloude_vm_id = vmId;

SELECT * FROM `xenonvm`.xevm_webhooks where hook = hookState;

DELETE FROM `xenonvm`.`xevm_build_exec_details` WHERE build_id=buildID and customer_schema=schemaName and user_id=userID;

SET @queryToExec= CONCAT('Update `',schemaName,'`.`xebm_vm_details` SET vm_state=4 where build_id=',buildID,' and userId=',userID,';');

SELECT @queryToExec;

PREPARE stmt FROM @queryToExec;
  EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCustomerVMDetails`(IN customerId INT)
BEGIN
DECLARE vmId INT(11);
DECLARE concurrentExecs INT(11);
DECLARE currentExecs INT(11);
DECLARE concurrentExecsStatus INT(11);
SELECT * FROM `xenonvm`.xevm_customervm_details where is_free = 1 and status=1 and customer_id = customerId LIMIT 1;
SELECT cust_vm_id INTO vmId FROM `xenonvm`.xevm_customervm_details where is_free = 1 and status=1 and customer_id = customerId LIMIT 1;
SELECT concurrent_exec_status INTO concurrentExecsStatus FROM `xenonvm`.xevm_customervm_details where is_free = 1 and status=1 and customer_id = customerId LIMIT 1;

IF concurrentExecsStatus=1 THEN
SELECT concurrent_execs INTO concurrentExecs FROM `xenonvm`.xevm_customervm_details where is_free = 1 and status=1 and concurrent_exec_status=1 and customer_id = customerId LIMIT 1;
SELECT current_execs INTO currentExecs FROM `xenonvm`.xevm_customervm_details where is_free = 1 and status=1 and customer_id = customerId LIMIT 1;

IF currentExecs<concurrentExecs THEN
UPDATE `xenonvm`.xevm_customervm_details SET `current_execs`=current_execs+1 WHERE `cust_vm_id`=vmId and customer_id = customerId and status=1;
END IF;
SELECT current_execs INTO currentExecs FROM `xenonvm`.xevm_customervm_details where is_free = 1 and status=1 and customer_id = customerId LIMIT 1;

IF currentExecs>=concurrentExecs THEN
UPDATE `xenonvm`.xevm_customervm_details SET `is_free`='2' WHERE `cust_vm_id`=vmId and customer_id = customerId and status=1;
END IF;

END IF;
IF concurrentExecsStatus=2 THEN
UPDATE `xenonvm`.xevm_customervm_details SET `is_free`='2' WHERE `cust_vm_id`=vmId and customer_id = customerId and status=1;
END IF;

END;;

CREATE DEFINER=`xenon`@`localhost` FUNCTION `insertCloudVMDetails`(
hostNameTemp varchar(100),
ipAddress varchar(100),
portNumber INT(11),
projectLocation varchar(1000),
ieStatus INT(11),
chromeStatus INT(11),
firefoxStatus INT(11),
safariStatus INT(11),
concurrentExecStatus INT(11),
concurrentExecs INT(11),
statusVal INT(11),
remoteUsername VARCHAR(100),
remotePassword VARCHAR(100),
gitStatus INT(11)
) RETURNS int(11)
BEGIN
DECLARE count,insertStatus INT DEFAULT 0;
SET insertStatus=-1;
    
	SELECT count(*) INTO count FROM xevm_cloudevm_details where hostname=hostNameTemp;
          
        IF count=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF count=0 THEN 
        INSERT INTO `xevm_cloudevm_details` (`cloude_vm_id`,`hostname`, `ip_address`,`port`, `project_location`, `ie_status`, `chrome_status`, `firefox_status`, `safari_status`,`concurrent_exec_status`,`concurrent_execs`, `status`,`vm_username`,`vm_password`,`git_status`) 
        VALUES (0,hostNameTemp, ipAddress,portNumber, projectLocation,ieStatus, chromeStatus, firefoxStatus, safariStatus,concurrentExecStatus,concurrentExecs, statusVal,remoteUsername,remotePassword,gitStatus);
              
        SET insertStatus=1;
                               
        END IF;
RETURN insertStatus;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `getAllocatedCloudVMDetails`(IN vmId INT(11),IN userId INT(11),IN buildId INT(11),IN schemaName Varchar(100))
BEGIN

SET @queryToExec=CONCAT(' SELECT *
FROM `',schemaName,'`.`xebm_vm_details` WHERE cust_vm_id = ',vmId,' and build_id=',buildId,' and user_id=',userId,'');

SELECT @queryToExec;
PREPARE stmt FROM @queryToExec;
  EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SELECT * FROM xenonvm.xevm_webhooks;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `deallocateCloudeVM`(IN vmId INT(11),
IN userId INT(11), IN buildId INT(11), IN schemaName VARCHAR(200),IN hookState INT(11))
BEGIN

DECLARE concurrentExecStatus INT(11);

UPDATE `xenonvm`.`xevm_cloudevm_details` SET `is_free`=2 WHERE `cloude_vm_id`=vmId;

SELECT concurrent_exec_status INTO concurrentExecStatus FROM xenonvm.xevm_cloudevm_details where cloude_vm_id=vmId;

IF concurrentExecStatus=1 THEN
UPDATE `xenonvm`.`xevm_cloudevm_details` SET `current_execs`=current_execs-1,`is_free`=1 WHERE `cloude_vm_id`=vmId;
END IF;

IF concurrentExecStatus=2 THEN
UPDATE `xenonvm`.`xevm_cloudevm_details` SET `is_free`=1 WHERE `cloude_vm_id`=vmId;
END IF;

SELECT * FROM `xenonvm`.`xevm_cloudevm_details` WHERE cloude_vm_id = vmId;

SELECT * FROM `xenonvm`.xevm_webhooks where hook = hookState;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `deallocateCustomerVM`(IN vmId INT(11),IN userId INT(11),IN buildId INT(11),IN schemaName VARCHAR(1000))
BEGIN

DECLARE concurrentExecStatus INT(11);
DECLARE concurrentExec INT(11);

SELECT concurrent_exec_status INTO concurrentExecStatus FROM xenonvm.xevm_customervm_details where cust_vm_id=vmId;

IF concurrentExecStatus=2 THEN
UPDATE `xenonvm`.`xevm_customervm_details` SET `is_free`=1 , `current_execs`=0 WHERE `cust_vm_id`=vmId;
END IF;

IF concurrentExecStatus=1 THEN

SELECT current_execs INTO concurrentExec FROM xenonvm.xevm_customervm_details where cust_vm_id=vmId;

IF concurrentExec>0 THEN
UPDATE `xenonvm`.`xevm_customervm_details` SET `is_free`=1, `current_execs`=current_execs-1 WHERE `cust_vm_id`=vmId;
END IF;
END IF;

SET @queryToExec=CONCAT('UPDATE `',schemaName,'`.`xebm_vm_details` SET `vm_state`=4 WHERE `build_id`=',buildId,' and userId=',userId,' ORDER BY xe_vm_id desc LIMIT 1','');

SELECT @queryToExec;
PREPARE stmt FROM @queryToExec;
  EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

END;;
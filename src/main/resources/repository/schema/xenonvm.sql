-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: xenonvm
-- ------------------------------------------------------
-- Server version	5.6.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `xevm_active`
--

DROP TABLE IF EXISTS `xevm_active`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xevm_active` (
  `active_id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`active_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xevm_build_exec_details`
--

DROP TABLE IF EXISTS `xevm_build_exec_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xevm_build_exec_details` (
  `exec_id` int(11) NOT NULL AUTO_INCREMENT,
  `exec_json` longtext,
  `vm_ip` varchar(45) DEFAULT NULL,
  `vm_id` int(11) DEFAULT NULL,
  `build_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_schema` varchar(100) DEFAULT NULL,
  `vm_port` int(11) DEFAULT NULL,
  PRIMARY KEY (`exec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xevm_cloudevm_details`
--

DROP TABLE IF EXISTS `xevm_cloudevm_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xevm_cloudevm_details` (
  `cloude_vm_id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(100) DEFAULT NULL,
  `vm_username` varchar(100) DEFAULT NULL,
  `vm_password` varchar(100) DEFAULT NULL,
  `ip_address` varchar(100) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `project_location` varchar(1000) DEFAULT NULL,
  `ie_status` int(11) DEFAULT NULL,
  `chrome_status` int(11) DEFAULT NULL,
  `firefox_status` int(11) DEFAULT NULL,
  `safari_status` int(11) DEFAULT NULL,
  `concurrent_exec_status` int(11) DEFAULT '2',
  `concurrent_execs` int(11) DEFAULT '1',
  `is_free` int(11) DEFAULT '1',
  `status` int(11) DEFAULT NULL,
  `current_execs` int(11) DEFAULT '0',
  `git_status` int(11) DEFAULT 2,
  PRIMARY KEY (`cloude_vm_id`),
  KEY `is_freeFK_idx` (`is_free`),
  KEY `ie_statusFK_idx` (`ie_status`),
  KEY `chrome_statusFK_idx` (`chrome_status`),
  KEY `firefox_statusFK_idx` (`firefox_status`),
  KEY `safari_statusFK_idx` (`safari_status`),
  KEY `con_FK_idx` (`concurrent_exec_status`),
  KEY `cloude_vm_status_idx` (`status`),
  CONSTRAINT `chrome_statusFK` FOREIGN KEY (`chrome_status`) REFERENCES `xevm_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cloude_vm_status` FOREIGN KEY (`status`) REFERENCES `xevm_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `con_FK` FOREIGN KEY (`concurrent_exec_status`) REFERENCES `xevm_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `firefox_statusFK` FOREIGN KEY (`firefox_status`) REFERENCES `xevm_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ie_statusFK` FOREIGN KEY (`ie_status`) REFERENCES `xevm_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `is_freeFK` FOREIGN KEY (`is_free`) REFERENCES `xevm_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `safari_statusFK` FOREIGN KEY (`safari_status`) REFERENCES `xevm_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xevm_customervm_details`
--

DROP TABLE IF EXISTS `xevm_customervm_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xevm_customervm_details` (
  `cust_vm_id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(100) DEFAULT NULL,
  `vm_username` varchar(100) DEFAULT NULL,
  `vm_password` varchar(100) DEFAULT NULL,
  `ip_address` varchar(100) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `project_location` varchar(1000) DEFAULT NULL,
  `ie_status` int(11) DEFAULT NULL,
  `chrome_status` int(11) DEFAULT NULL,
  `firefox_status` int(11) DEFAULT NULL,
  `safari_status` int(11) DEFAULT NULL,
  `concurrent_exec_status` int(11) DEFAULT '2',
  `concurrent_execs` int(11) DEFAULT '1',
  `customer_id` int(11) DEFAULT NULL,
  `is_free` int(11) DEFAULT '1',
  `status` int(11) DEFAULT NULL,
  `current_execs` int(11) DEFAULT '0',
  `git_status` int(11) DEFAULT 2,
  PRIMARY KEY (`cust_vm_id`),
  KEY `vm_status_idx` (`is_free`),
  KEY `ie_status_idx` (`ie_status`),
  KEY `chrome_status_idx` (`chrome_status`),
  KEY `firefox_status_idx` (`firefox_status`),
  KEY `safari_status_idx` (`safari_status`),
  KEY `concurrent_fk_idx` (`concurrent_exec_status`),
  KEY `cust_vm_statusFK_idx` (`status`),
  CONSTRAINT `chrome_status` FOREIGN KEY (`chrome_status`) REFERENCES `xevm_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `concurrent_fk` FOREIGN KEY (`concurrent_exec_status`) REFERENCES `xevm_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cust_vm_statusFK` FOREIGN KEY (`status`) REFERENCES `xevm_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `firefox_status` FOREIGN KEY (`firefox_status`) REFERENCES `xevm_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ie_status` FOREIGN KEY (`ie_status`) REFERENCES `xevm_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `safari_status` FOREIGN KEY (`safari_status`) REFERENCES `xevm_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `vm_status` FOREIGN KEY (`is_free`) REFERENCES `xevm_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-29 16:24:09


DROP TABLE IF EXISTS `xevm_webhooks`;

CREATE TABLE `xevm_webhooks` (
  `idwb` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `webhook` longtext,
  `hook` int(11) DEFAULT NULL,
  PRIMARY KEY (`idwb`),
  KEY `hookname_idx` (`hook`),
  CONSTRAINT `hookname` FOREIGN KEY (`hook`) REFERENCES `xevm_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `xenonvm`.`xevm_active`(`active_id`,`status`) VALUES (1,'Active'),(2,'Inactive'),(3,'Remove');
INSERT INTO `xenonvm`.`xevm_webhooks` (`name`, `webhook`, `hook`) VALUES ('startVM', 'https://s5events.azure-automation.net/webhooks?token=WumNJV050Aa6ichzLuBGWuBCvm9rOBQsf2WjqFD52jA%3d', '1');
INSERT INTO `xenonvm`.`xevm_webhooks` (`name`, `webhook`, `hook`) VALUES ('stopVM', 'https://s5events.azure-automation.net/webhooks?token=vNQ%2bFMLJ0SMjb77tZF%2bIkH8IyqoggIGjsK6N05EWN4I%3d', '2');
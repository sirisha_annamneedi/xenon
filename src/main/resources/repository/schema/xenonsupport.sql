-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: xenonsupport
-- ------------------------------------------------------
-- Server version	5.6.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `xes_article`
--

DROP TABLE IF EXISTS `xes_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xes_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `art_name` varchar(100) DEFAULT NULL,
  `art_desc` longtext,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subcomponent_id` int(11) DEFAULT NULL,
  `article_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subcomponentIdFK_idx` (`subcomponent_id`),
  CONSTRAINT `subcomponentIdFK` FOREIGN KEY (`subcomponent_id`) REFERENCES `xes_subcomponents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xes_article`
--

LOCK TABLES `xes_article` WRITE;
/*!40000 ALTER TABLE `xes_article` DISABLE KEYS */;
/*!40000 ALTER TABLE `xes_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xes_component`
--

DROP TABLE IF EXISTS `xes_component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xes_component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cmp_name` varchar(100) DEFAULT NULL,
  `cmp_desc` varchar(1500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xes_component`
--

LOCK TABLES `xes_component` WRITE;
/*!40000 ALTER TABLE `xes_component` DISABLE KEYS */;
INSERT INTO `xes_component` VALUES (1,'Administration','Administration'),(2,'Test Plan Manager','Test Plan Manager'),(3,'Test Manager','Test Manager'),(4,'Bug Tracker','Bug Tracker');
/*!40000 ALTER TABLE `xes_component` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xes_subcomponents`
--

DROP TABLE IF EXISTS `xes_subcomponents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xes_subcomponents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subcmp_name` varchar(100) DEFAULT NULL,
  `subcmp_desc` longtext,
  `component_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `componentIdFK_idx` (`component_id`),
  CONSTRAINT `componentIdFK` FOREIGN KEY (`component_id`) REFERENCES `xes_component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `xes_faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xes_faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `description` longtext,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `faq_creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
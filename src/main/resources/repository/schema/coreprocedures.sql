CREATE DEFINER=`root`@`localhost` PROCEDURE `updateAutomationStatus`(IN automation_status INT(11),IN scriptless_status INT(11),IN customer_schema VARCHAR(500))
BEGIN

SET @queryToExec1= CONCAT('UPDATE `',customer_schema,'`.xetm_execution_type SET status=',automation_status,' WHERE execution_type_id=2');
SET @queryToExec2= CONCAT('UPDATE `',customer_schema,'`.xetm_execution_type SET status=',automation_status,' WHERE execution_type_id=3');
SET @queryToExec3= CONCAT('UPDATE `',customer_schema,'`.xe_build_execution_type SET status=',automation_status,' WHERE execution_type_id=2');
SET @queryToExec4= CONCAT('UPDATE `',customer_schema,'`.xe_build_execution_type SET status=',scriptless_status,' WHERE execution_type_id=3');

PREPARE stmt1 FROM @queryToExec1;
  EXECUTE stmt1; 
DEALLOCATE PREPARE stmt1;

PREPARE stmt2 FROM @queryToExec2;
  EXECUTE stmt2; 
DEALLOCATE PREPARE stmt2;

PREPARE stmt3 FROM @queryToExec3;
  EXECUTE stmt3; 
DEALLOCATE PREPARE stmt3;

PREPARE stmt4 FROM @queryToExec4;
  EXECUTE stmt4; 
DEALLOCATE PREPARE stmt4;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateAutomationStatusByCustId`(IN customerId INT(11))
BEGIN

DECLARE automationStatus INT(11);
DECLARE scriptlessStatus INT(11);
DECLARE customerSchema VARCHAR(45);
SELECT automation_status INTO automationStatus FROM `xenon-core`.xe_customer_details where customer_id=customerId; 
SELECT scriptless_status INTO scriptlessStatus FROM `xenon-core`.xe_customer_details where customer_id=customerId; 
SELECT customer_schema INTO customerSchema FROM `xenon-core`.xe_customer_details where customer_id=customerId; 

SET @queryToExec1= CONCAT('UPDATE `',customerSchema,'`.xetm_execution_type SET status=',automationStatus,' WHERE execution_type_id=2');
SET @queryToExec2= CONCAT('UPDATE `',customerSchema,'`.xetm_execution_type SET status=',automationStatus,' WHERE execution_type_id=3');
SET @queryToExec3= CONCAT('UPDATE `',customerSchema,'`.xe_build_execution_type SET status=',automationStatus,' WHERE execution_type_id=2');
SET @queryToExec4= CONCAT('UPDATE `',customerSchema,'`.xe_build_execution_type SET status=',scriptlessStatus,' WHERE execution_type_id=3');

PREPARE stmt1 FROM @queryToExec1;
  EXECUTE stmt1; 
DEALLOCATE PREPARE stmt1;

PREPARE stmt2 FROM @queryToExec2;
  EXECUTE stmt2; 
DEALLOCATE PREPARE stmt2;

PREPARE stmt3 FROM @queryToExec3;
  EXECUTE stmt3; 
DEALLOCATE PREPARE stmt3;

PREPARE stmt4 FROM @queryToExec4;
  EXECUTE stmt4; 
DEALLOCATE PREPARE stmt4;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertLdapDetails`(
ldapUrl varchar(100) ,
rootDn varchar(500) ,
userSearchBase varchar(500) ,
userSearchFilter varchar(500) ,
userDn varchar(500) ,
userPassword varchar(500) ,
displayNameAttribute varchar(45) ,
emailAddrAttribute varchar(45) ,
statusVal int(11) ,
emailDomainName varchar(100) ,
customerId int(11),
userMail VARCHAR(100),
userName VARCHAR(100))
BEGIN

DELETE FROM `xenon-core`.`xe_ldap_settings` WHERE `customer_id`=customerId ;

INSERT INTO `xenon-core`.`xe_ldap_settings` VALUES(0,ldapUrl,rootDn,userSearchBase,userSearchFilter,userDn,userMail,userPassword,displayNameAttribute,emailAddrAttribute,statusVal,emailDomainName,customerId);

UPDATE `xenon-core`.`xe_customer_details` SET `ldap_status`='1' WHERE `customer_id`=customerId;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCustomerLdapDetails`()
BEGIN

SELECT * FROM `xenon-core`.xe_customer_details C,`xenon-core`.xe_ldap_settings L where C.on_premise=1 and C.customer_status=1 and ldap_status=1 and C.customer_id=L.customer_id ;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `getJenkinsInstanceDetails`(IN jobName longtext,IN apiToken longtext)
BEGIN

DECLARE customerSchema longtext;
DECLARE instanceId INT(11);
SELECT customer_schema INTO customerSchema FROM xe_customer_details where customer_id = (SELECT cust_id FROM xead_jenkins_api_token_details where token=apiToken); 

SELECT instance_id INTO instanceId FROM `xenon-core`.xead_jenkins_api_token_details where token=apiToken;

SET @queryToExec= CONCAT('SELECT * FROM `',customerSchema,'`.xead_jenkins_details where jenkin_id=',instanceId);

PREPARE stmt FROM @queryToExec;
  EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET @queryToExec= CONCAT('SELECT * FROM `',customerSchema,'`.xead_jenkin_jobs where instance=',instanceId,' and jenkin_job=\'',jobName,'\'');

PREPARE stmt FROM @queryToExec;
  EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SELECT customer_schema FROM xe_customer_details where customer_id = (SELECT cust_id FROM xead_jenkins_api_token_details where token=apiToken); 

END;;

CREATE DEFINER=`xenon`@`%` PROCEDURE `insertTokenProcedure`(IN instance_id1 int,IN cust_id int,IN user_id int,IN token1 longtext,IN windows_script longtext,IN powershell_script longtext)
BEGIN
DECLARE instanceCount INT(11);
select count(*) into instanceCount from xead_jenkins_api_token_details where instance_id = instance_id1;
if instanceCount = 0 then
insert into xead_jenkins_api_token_details (instance_id,cust_id,user_id,token,windows_script,powershell_script) values (instance_id1,cust_id,user_id,token1,windows_script,powershell_script);
else
update xead_jenkins_api_token_details set token = token1, windows_script= windows_script, powershell_script=powershell_script where instance_id = instance_id1;
END IF;
END;;
/**
 * **********************************************************************************
 * **********************************************************************************
 * 
 * TRIGGER
 * 
 * **********************************************************************************
 * **********************************************************************************
 */
CREATE DEFINER=`xenon`@`localhost`TRIGGER `xe_project_AFTER_INSERT` AFTER INSERT ON `xe_project` FOR EACH ROW
BEGIN 
DECLARE project_id varchar(50);
  INSERT INTO xe_user_project( project_id, user)
  SELECT NEW.project_id,xe_user.user_id 
  FROM xe_user_details AS xe_user 
  WHERE xe_user.role_id=2;
END;;
CREATE DEFINER=`xenon`@`localhost` TRIGGER `xe_project_AFTER_UPDATE` AFTER UPDATE ON `xe_project` FOR EACH ROW
BEGIN
DECLARE project_id int;
DECLARE project_active int;
	UPDATE xe_module SET xe_module.module_active = NEW.project_active WHERE xe_module.project_id = NEW.project_id;
END;;
CREATE DEFINER=`xenon`@`localhost`TRIGGER `xe_user_project_AFTER_INSERT` AFTER INSERT ON `xe_user_project` FOR EACH ROW
BEGIN
DECLARE project_id int;
DECLARE user int;
  INSERT INTO xe_user_module(module_id,user_id,project_id) SELECT xe_module.module_id,NEW.user,NEW.project_id
  FROM xe_module WHERE xe_module.project_id=NEW.project_id;
END ;;
CREATE DEFINER=`root`@`localhost` TRIGGER `xebm_build_exec_details_AFTER_UPDATE` AFTER UPDATE ON `xebm_build_exec_details` FOR EACH ROW
BEGIN
IF(NEW.complete_status = 1) THEN
    DELETE FROM xebm_vm_exec_details WHERE build_exec_id=NEW.build_exec_id;
END IF;
END;;

CREATE DEFINER=`xenon`@`localhost` TRIGGER `xe_user_project_AFTER_DELETE` AFTER DELETE ON `xe_user_project` FOR EACH ROW
BEGIN
DECLARE project_id int;
DECLARE user int;
	DELETE FROM xe_user_module WHERE xe_user_module.project_id=OLD.project_id and xe_user_module.user_id=OLD.user; 
	
    DELETE FROM xe_current_project WHERE xe_current_project.project_id =OLD.project_id and user_id NOT IN 
    (SELECT user FROM xe_user_project where xe_user_project.project_id in (OLD.project_id));
END;;

CREATE DEFINER=`root`@`localhost` TRIGGER `xe_user_details_AFTER_INSERT` AFTER INSERT ON `xe_user_details` FOR EACH ROW
BEGIN
DECLARE user_id INT;
DECLARE email_id VARCHAR(45);

  INSERT INTO `xe_utilization` (`userId`,`userName`) 
  VALUES(NEW.user_id,NEW.email_id);

END;;

CREATE DEFINER=`root`@`localhost` TRIGGER `xe_build_before_update` BEFORE UPDATE ON `xe_build` FOR EACH ROW
BEGIN 
SET NEW.`build_updatedate`= now();
END;;

CREATE DEFINER=`root`@`localhost` TRIGGER `xe_automation_build_before_update` BEFORE UPDATE ON `xe_automation_build` FOR EACH ROW
BEGIN 
SET NEW.`build_updatedate`= now();
END;;

CREATE DEFINER=`root`@`localhost` TRIGGER `xesfdc_automation_build_before_update` BEFORE UPDATE ON `xesfdc_automation_build` FOR EACH ROW
BEGIN 
SET NEW.`build_updatedate`= now();
END;;

CREATE DEFINER=`root`@`localhost` TRIGGER `xebm_automation_build_testcase_AFTER_INSERT` AFTER INSERT ON `xebm_automation_build_testcase` FOR EACH ROW
BEGIN
Update xe_automation_build SET `build_updatedate`= now() where `build_id`=NEW.build_id;
END;;

CREATE DEFINER=`root`@`localhost` TRIGGER `xebm_automation_build_testcase_BEFORE_DELETE` BEFORE DELETE ON `xebm_automation_build_testcase` FOR EACH ROW
BEGIN
Update xe_automation_build SET `build_updatedate`= now() where `build_id`=OLD.build_id;
END;;

CREATE DEFINER=`root`@`localhost` TRIGGER `xebm_buildtestcase_BEFORE_DELETE` BEFORE DELETE ON `xebm_buildtestcase` FOR EACH ROW
BEGIN
Update xe_build SET `build_updatedate`= now() where `build_id`=old.build_id;
END;;
/**
 * ****************************************************************************************************
 * ****************************************************************************************************
 * 
 * FUNCTIONS
 * 
 * ****************************************************************************************************
 * ****************************************************************************************************
 */

CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewAutoBuild`(
buildName varchar(100),
buildCreator INT(11)
) RETURNS int(11)
BEGIN
DECLARE count,insertStatus INT DEFAULT 0;
SET insertStatus=-1;
    
	SELECT count(*) INTO count FROM xebm_auto_build where build_name=buildName;
          
        IF count=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF count=0 THEN 
        INSERT INTO `xebm_auto_build` (`build_name`,`build_createdate`,`build_creator`) VALUES 
        (buildName,NOW(),buildCreator);
                
        SET insertStatus=1;
                               
        END IF;
RETURN insertStatus;
END ;;
CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewBugStatus`(
custTypeId INT(11)
) RETURNS int(11)
BEGIN

DECLARE maxBugCount,bugCount INT DEFAULT 0;

	SELECT bugs_count INTO maxBugCount FROM `xenon-core`.xe_licence_details where type_id=custTypeId;
    
    SELECT count(*) INTO bugCount FROM xebt_bug;
    
	IF bugCount<maxBugCount THEN
          RETURN 1;
	END IF;
RETURN 0;
END ;;
CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewBuild`(
buildName varchar(400),
buildDescription LONGTEXT,
buildActive INT(11),
releaseId INT(11),
buildCreator INT(11),
currentDate varchar(200)
) RETURNS int(11)
BEGIN
DECLARE buildCreateDate DATETIME;
DECLARE count,insertStatus INT DEFAULT 0;
SET insertStatus=-1;
    
	SELECT count(*) INTO count FROM xe_build where build_name=buildName and release_id=releaseId;
          
        IF count=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF count=0 THEN 
        INSERT INTO `xe_build` (`build_name`,  `build_desc`, `build_status`,`release_id`,`build_state`,`build_createdate`,`build_creator`) VALUES 
        (buildName,buildDescription,buildActive,releaseId,1,currentDate,buildCreator);
                
        SET insertStatus=1;
                               
        END IF;
RETURN insertStatus;
END;;
CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewBuildStatus`(
custTypeId INT(11)
) RETURNS int(11)
BEGIN

DECLARE maxBuildCount,buildCount INT DEFAULT 0;

	SELECT builds_count INTO maxBuildCount FROM `xenon-core`.xe_licence_details where type_id=custTypeId;
    
    SELECT count(*) INTO buildCount FROM xe_build;
    
	IF buildCount<maxBuildCount THEN
          RETURN 1;
	END IF;
RETURN 0;
END ;;
CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewModule`(
moduleName varchar(45),
moduleDescription LONGTEXT,
moduleActive INT(11),
projectId INT(11)
) RETURNS int(11)
BEGIN
DECLARE count,insertStatus,modID INT DEFAULT 0;
SET insertStatus=-1;
    
    SELECT count(*) INTO count FROM xe_module where module_name=moduleName and project_id=projectId;
          
        IF count=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF count=0 THEN 
       
		INSERT INTO `xe_module` (`module_name`, `project_id`, `module_description`, `module_active`) VALUES (moduleName,projectId,moduleDescription,moduleActive);
		SELECT module_id INTO modID from xe_module where module_name =moduleName and module_description=moduleDescription and project_id=projectId and module_active =moduleActive;
		SET insertStatus=modID;
        
		END IF;
RETURN insertStatus;
END ;;
CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewModuleStatus`(
custTypeId INT(11)
) RETURNS int(11)
BEGIN

DECLARE maxModuleCount,moduleCount INT DEFAULT 0;

	SELECT modules_count INTO maxModuleCount FROM `xenon-core`.xe_licence_details where type_id=custTypeId;
    
    SELECT count(*) INTO moduleCount FROM xe_module;
    
	IF moduleCount<maxModuleCount THEN
          RETURN 1;
	END IF;
RETURN 0;
END ;;
CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewProject`(
projectName varchar(45),
projectDescription LONGTEXT,
projectActive INT(11),
tmPrefix varchar(45),
btPrefix varchar(45),
currentDate varchar(100)
) RETURNS int(11)
BEGIN
DECLARE projectCreateDate DATETIME;
DECLARE prjCount,tmPrefixCount,btPrefixCount,insertStatus INT DEFAULT 0;
SET insertStatus=-1;
    
	
    
    SELECT count(*) INTO prjCount FROM xe_project where project_name=projectName;
    
    SELECT count(*) INTO tmPrefixCount FROM xe_project where tm_prefix=tmPrefix;
    
    SELECT count(*) INTO btPrefixCount FROM xe_project where bt_prefix=btPrefix;
        
         IF tmPrefixCount=1 THEN 
             SET insertStatus=2;
       
		END IF;
        
         IF btPrefixCount=1 THEN 
             SET insertStatus=3;
       
		END IF;
        
        
        IF prjCount=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF prjCount=0  and tmPrefixCount=0  and btPrefixCount=0 THEN 
        INSERT INTO `xe_project` (`project_name`, `project_description`, `project_active`, `project_createdate`,`tm_prefix`,`bt_prefix`) VALUES ( projectName, projectDescription, projectActive, currentDate,tmPrefix,btPrefix);
        
        SET insertStatus=1;
                               
        END IF;
        
RETURN insertStatus;
END;;
CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewProjectStatus`(
custTypeId INT(11)
) RETURNS int(11)
BEGIN

DECLARE maxProjectCount,projectCount INT DEFAULT 0;

	SELECT projects_count INTO maxProjectCount FROM `xenon-core`.xe_licence_details where type_id=custTypeId;
    
    SELECT count(*) INTO projectCount FROM xe_project;
    
	IF projectCount<maxProjectCount THEN
          RETURN 1;
	END IF;
RETURN 0;
END ;;
CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewRelease`(
releaseName varchar(45),
releaseDescription LONGTEXT,
releaseStartDate DATETIME,
releaseEndDate DATETIME,
releaseActive INT(11)
) RETURNS int(11)
BEGIN
DECLARE count,insertStatus INT DEFAULT 0;
SET insertStatus=-1;
    
    SELECT count(*) INTO count FROM xe_release where release_name=releaseName;
          
        IF count=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF count=0 THEN 
        INSERT INTO `xe_release` (`release_name`, `release_description`, `release_start_date`, `release_end_date`, `release_active`) VALUES (releaseName, releaseDescription, releaseStartDate, releaseEndDate, releaseActive);
                
        SET insertStatus=1;
                               
        END IF;
RETURN insertStatus;
END ;;
CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewScenarioStatus`(
custTypeId INT(11)
) RETURNS int(11)
BEGIN

DECLARE maxScenarioCount,scenarioCount INT DEFAULT 0;

	SELECT scenario_count INTO maxScenarioCount FROM `xenon-core`.xe_licence_details where type_id=custTypeId;
    
    SELECT count(*) INTO scenarioCount FROM xetm_scenario;
    
	IF scenarioCount<maxScenarioCount THEN
          RETURN 1;
	END IF;
RETURN 0;
END ;;
CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewTcStepStatus`(
custTypeId INT(11),
tcId INT(11)
) RETURNS int(11)
BEGIN

DECLARE maxTcStepCount,tcStepCount INT DEFAULT 0;

	SELECT steps_count INTO maxTcStepCount FROM `xenon-core`.xe_licence_details where type_id=custTypeId;
    
    SELECT count(*) INTO tcStepCount FROM xetm_testcase_steps where testcase_id=tcId;
    
	IF tcStepCount<maxTcStepCount THEN
          RETURN 1;
	END IF;
RETURN 0;
END ;;
CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewTestcaseStatus`(
custTypeId INT(11)
) RETURNS int(11)
BEGIN

DECLARE maxTestcaseCount,testcaseCount INT DEFAULT 0;

	SELECT testcases_count INTO maxTestcaseCount FROM `xenon-core`.xe_licence_details where type_id=custTypeId;
    
    SELECT count(*) INTO testcaseCount FROM xetm_testcase;
    
	IF testcaseCount<maxTestcaseCount THEN
          RETURN 1;
	END IF;
RETURN 0;
END ;;
CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewTrialBuild`(
buildName varchar(100),
buildCreator INT(11)
) RETURNS int(11)
BEGIN
DECLARE count,insertStatus INT DEFAULT 0;
SET insertStatus=-1;
    
	SELECT count(*) INTO count FROM xebm_trial_build where build_name=buildName;
          
        IF count=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF count=0 THEN 
        INSERT INTO `xebm_trial_build` (`build_name`,`build_createdate`,`build_creator`) VALUES 
        (buildName,NOW(),buildCreator);
                
        SET insertStatus=1;
                               
        END IF;
RETURN insertStatus;
END ;;
CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewUserStatus`(
custTypeId INT(11)
) RETURNS int(11)
BEGIN

DECLARE maxUserCount,userCount INT DEFAULT 0;

	SELECT users_count INTO maxUserCount FROM `xenon-core`.xe_licence_details where type_id=custTypeId;
    
    SELECT count(*) INTO userCount FROM xe_user_details;
    
	IF userCount<maxUserCount THEN
          RETURN 1;
	END IF;
RETURN 0;
END ;;

CREATE DEFINER=`xenon`@`localhost` FUNCTION `createAutomationBuild`(
buildName varchar(400),
buildDescription varchar(400),
buildActive INT(11),
releaseId INT(11),
buildCreator INT(11),
currentDate varchar(200)
) RETURNS int(11)
BEGIN
DECLARE buildCreateDate DATETIME;
DECLARE count,insertStatus INT DEFAULT 0;
SET insertStatus=-1;
    
	SELECT count(*) INTO count FROM xe_automation_build where build_name=buildName and release_id=releaseId;
          
        IF count=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF count=0 THEN 
        INSERT INTO `xe_automation_build` (`build_name`,  `build_desc`, `build_status`,`release_id`,`build_state`,`build_createdate`,`build_creator`) VALUES 
        (buildName,buildDescription,buildActive,releaseId,1,currentDate,buildCreator);
                
        SET insertStatus=1;
                               
        END IF;
RETURN insertStatus;
END;;

CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewMailGroup`(
mailGroupName varchar(100)
) RETURNS int(11)
BEGIN
DECLARE count,insertStatus,mailGroupId INT DEFAULT 0;
SET insertStatus=-1;
    
	SELECT count(*) INTO count FROM xe_mail_group where mail_group_name=mailGroupName;
          
        IF count=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF count=0 THEN 
        INSERT INTO `xe_mail_group` (`mail_group_name`) VALUES 
        (mailGroupName);
        SELECT mail_group_id INTO mailGroupId from xe_mail_group where mail_group_name=mailGroupName;
        SET insertStatus=mailGroupId;
                               
        END IF;
RETURN insertStatus;
END;;

CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewVM`(
hostNameTemp varchar(100),
ipAddress varchar(100),
portNumber INT(11),
projectLocation varchar(1000),
ieStatus INT(11),
chromeStatus INT(11),
firefoxStatus INT(11),
safariStatus INT(11),
concurrentExecStatus INT(11),
concurrentExecs INT(11),
statusVal INT(11)
) RETURNS int(11)
BEGIN
DECLARE count,insertStatus INT DEFAULT 0;
SET insertStatus=-1;
    
	SELECT count(*) INTO count FROM xebm_vm_details where hostname=hostNameTemp;
          
        IF count=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF count=0 THEN 
        INSERT INTO `xebm_vm_details` (`hostname`, `ip_address`,`port`, `project_location`, `ie_status`, `chrome_status`, `firefox_status`, `safari_status`,`concurrent_exec_status`,`concurrent_execs`, `status`) 
        VALUES (hostNameTemp, ipAddress,portNumber, projectLocation,ieStatus, chromeStatus, firefoxStatus, safariStatus,concurrentExecStatus,concurrentExecs, statusVal);
              
        SET insertStatus=1;
                               
        END IF;
RETURN insertStatus;
END;;

CREATE DEFINER=`xenon`@`localhost` FUNCTION `insertBuildExecDetails`(
buildId INT,
vmId INT,
browserTemp INT,
mailGroupId INT,
stepWiseScreenshot INT,
reportBug INT,
userId INT,
envId INT,
envArray varchar(100)
) RETURNS int(11)
BEGIN
DECLARE buildExecId INT DEFAULT 0 ;
DECLARE currentTime DATETIME;
DECLARE envIdVal INT DEFAULT 0;
DECLARE strLen INT DEFAULT 0;
DECLARE SubStrLen INT DEFAULT 0;
SELECT NOW() INTO currentTime;
INSERT INTO `xebm_build_exec_details` (`build_id`, `vm_id`, `browser`, `mail_group_id`, `step_wise_screenshot`, 
`report_bug`, `user_id`, `start_time`,`env_id`) 
VALUES (buildId, vmId, browserTemp, mailGroupId, stepWiseScreenshot, reportBug, userId, currentTime,envId);

SELECT build_exec_id INTO buildExecId from xebm_build_exec_details where `build_id`=buildId and `vm_id`=vmId and  
`browser` =browserTemp  and `mail_group_id` = mailGroupId and  `step_wise_screenshot`=  stepWiseScreenshot and 
`report_bug`=reportBug and  `user_id` = userId and `start_time`=currentTime and `env_id`=envId;

INSERT INTO `xebm_vm_exec_details` (`vm_id`, `build_exec_id`) VALUES ( vmId, buildExecId);

UPDATE `xe_automation_build` SET `build_state`='4' WHERE `build_id`=buildId;


IF envArray IS NULL THEN
    SET envArray = '';
  END IF;
IF envArray !='' THEN 
  do_this:
  LOOP
    SET strLen = CHAR_LENGTH(envArray);
    
    SET envIdVal = SUBSTRING_INDEX(envArray, ',', 1);
		INSERT INTO xebm_build_exec_env (build_exec_id,env_id) VALUES (buildExecId,envIdVal);
		 SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(envArray, ',', 1)) + 2;
    SET envArray = MID(envArray, SubStrLen, strLen);
    IF envArray = '' THEN
      LEAVE do_this;
    END IF;
  END LOOP do_this;
END IF;

RETURN 1;
END;;

CREATE DEFINER=`xenon`@`localhost` FUNCTION `createBugOnExecution`(tcId int, notes varchar(450), tcStatus int, buildId int,
									 stepId int, stepStatus int, stepComment varchar(450),
                            testCaseInsertMethod int, stepInsertMethod int, userId int, bugId int,screenshot blob) RETURNS int(11)
BEGIN
  DECLARE project,module,scenario,exeId,stexId INT DEFAULT 0;

SELECT projectid,module_id,scenario_id INTO project,module,scenario  from xetm_testcase where testcase_id = tcId;

if (testCaseInsertMethod = 2) then
        Update xe_testcase_execution_summary SET  `status_id`=tcStatus, `notes`=notes,`complete_status`=2 where `tc_id`=tcId and `build_id`=buildId;
    else 
       INSERT INTO xe_testcase_execution_summary ( `tc_id`, `build_id`, `project_id`, `mudule_id`, `scenario_id`, `tester_id`, `status_id`, `notes`,`complete_status`) 
       VALUES (tcId,buildId,project,module,scenario,userId,tcStatus,notes,2);

    end if;

   SELECT exec_id into exeId FROM xe_testcase_execution_summary where build_id=buildId and tc_id=tcId;

if (stepInsertMethod = 2) then
       Update xe_steps_execution_summary SET `status`=stepStatus, `comments`=stepComment,`bug_raised`=1 where `testex_id`=exeId and `step_id`=stepId;
       SELECT stex_id into stexId FROM xe_steps_execution_summary where testex_id=exeId and step_id =stepId;
    else 
       INSERT INTO xe_steps_execution_summary ( `testex_id`, `step_id`, `status`, `comments`,`screenshot`,`bug_raised`)
        VALUES (exeId, stepId,stepStatus,stepComment,screenshot, 1);
        SELECT stex_id into stexId FROM xe_steps_execution_summary where testex_id=exeId and step_id =stepId;
    end if;
INSERT INTO xe_step_execution_bug ( `build_id`,`step_exe_id`, `test_step_id`, `bug_id`,`test_case_id`) VALUES (buildId,stexId,stepId,bugId,tcId);

if(exeId>0 && stexId>0) then 
RETURN 1;
else
RETURN 0;
end if;
END;;

CREATE DEFINER=`xenon`@`localhost` FUNCTION `addUpdateBugActivity`(bugId int,userId int,bugComment varchar(1000)) RETURNS int(11)
BEGIN

    DECLARE currentStatus,prevStatus,currentPriority,currentAssignerFName,currentAssignerLName,prevPriority,prevAssignerFName,prevAssignerLName,currentSeverity,prevSeverity,bugPrefix,currentCategory,prevCategory VARCHAR(450);
  DECLARE activityDescription longtext;
  DECLARE bugTitle longtext;
  DECLARE createdBy INT(11);
  DECLARE notificationId INT(11);
  DECLARE currentTime VARCHAR(45);
  DECLARE done INT DEFAULT FALSE;
  DECLARE nfUser INT;     
  DECLARE cur1 CURSOR FOR SELECT distinct updated_by FROM xebt_bugsummary where bug_id=bugId;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  
  SELECT NOW() INTO currentTime;

  Select bug_prefix INTO bugPrefix from xebt_bug where  bug_id = bugId;

  Select distinct xs.bug_status,xps.bug_status,xp.bug_priority,xud.first_name,xud.last_name,  
			xpp.bug_priority,xpud.first_name,xpud.last_name, severity.bug_severity,p_severity.bug_severity,
			category.category_name as currentCategory,prevCategory.category_name as prevCategory
            INTO currentStatus,prevStatus,currentPriority,currentAssignerFName,currentAssignerLName,prevPriority,prevAssignerFName,prevAssignerLName,
			currentSeverity,prevSeverity, currentCategory, prevCategory
            from xebt_bugsummary xb,xebt_status xs,xebt_status xps ,xebt_priority xp,xebt_priority xpp,xebt_severity severity ,xebt_severity  p_severity ,
		    xe_user_details xud,xe_user_details xpud,xe_user_details xu,
			xebt_category as category, xebt_category as prevCategory 
			where xb.bug_status=xs.status_id and xb.bug_priority=xp.priority_id and xb.assignee = xud.user_id 
			and xb.prev_status=xps.status_id and xb.prev_priority=xpp.priority_id and xb.bug_severity=severity.severity_id and  xb.prev_severity=p_severity.severity_id and  xb.prev_assignee = xpud.user_id and xb.updated_by = xu.user_id 
			and xb.category = category.category_id
			and xb.prev_category = prevCategory.category_id
			and xb.bug_id = bugId GROUP BY xb.bs_id DESC limit 1;
            	     
    SET activityDescription = concat(bugPrefix,"#",bugPrefix,"#",prevStatus,"#",currentStatus,"#",prevAssignerFName," ",prevAssignerLName ,"#",currentAssignerFName ," ",currentAssignerLName,"#",prevPriority,"#",currentPriority,"#",prevSeverity,"#",currentSeverity,
      "#",bugComment,"#",prevCategory,"#",currentCategory);

   INSERT INTO xe_recent_activities (activity, activity_desc,module,activity_date,user,activity_status) VALUES (7,activityDescription,"BT",NOW(),userId,1);
    
    Select bug_title INTO bugTitle from xebt_bug where  bug_id = bugId;
	Select created_by INTO createdBy from xebt_bug where  bug_id = bugId;
	INSERT INTO `xe_user_notifications` (`activity`, `notification_description`, `notification_date`,`notification_url`, `user_id`, `created_by`, `read_status`, `notification_status`)
	VALUES (8,CONCAT("Updated bug '",bugTitle,"'"),currentTime,CONCAT('bugsummarybyprefix?btPrefix=',bugPrefix), createdBy,userId,2,1);

	SELECT notification_id INTO notificationId from `xe_user_notifications` where
	`activity` = 8 and `notification_description`=CONCAT("Updated bug '",bugTitle,"'") and
	`notification_url`=CONCAT('bugsummarybyprefix?btPrefix=',bugPrefix) and 
	`user_id` = createdBy  and notification_date=currentTime and
	`created_by` = userId  and  `read_status`= 2 and  `notification_status` = 1 LIMIT 1;

	open cur1;
    read_loop: LOOP
			FETCH cur1 INTO nfUser;
				IF done THEN
					LEAVE read_loop;
				END IF;                
	INSERT INTO `xe_notification_users` (`nf_notification_id`, `nf_user_id`) VALUES (notificationId,nfUser);
	END LOOP;
	CLOSE cur1;
    
    CALL updateBugAssigneeNotifications(notificationId,bugId);

Return 1;
END;;

CREATE DEFINER=`xenon`@`localhost` FUNCTION `addInsertBugActivity`(bugId int,userId int,bugSummary varchar(10000)) RETURNS int(11)
BEGIN

  DECLARE currentStatus,currentPriority,currentSeverity,currentAssignerFName,currentAssignerLName,updatorFirstName,updatorLastName,bugPrefix,currentCategory VARCHAR(450);
  DECLARE activityDescription longtext;

  Select bug_prefix INTO bugPrefix from xebt_bug where  bug_id = bugId;

	Select distinct xs.bug_status,xp.bug_priority,severity.bug_severity,xud.first_name,xud.last_name,xu.first_name,xu.last_name,category.category_name 
	INTO currentStatus,currentPriority,currentSeverity,currentAssignerFName,currentAssignerLName,updatorFirstName,updatorLastName,currentCategory
	from xebt_bugsummary xb,xebt_status xs,xebt_priority xp,xebt_severity severity,
	xe_user_details xud,xe_user_details xu,xebt_category as category
	where xb.bug_status=xs.status_id and xb.bug_priority=xp.priority_id and xb.bug_severity=severity.severity_id and xb.assignee = xud.user_id and xb.updated_by = xu.user_id 
	and xb.category = category.category_id
	and xb.bug_id = bugId GROUP BY xb.bs_id DESC limit 1;
            	     
    SET activityDescription = concat(bugPrefix,"#",bugPrefix,"#",bugSummary,"#",updatorFirstName ," ",updatorLastName,"#",currentAssignerFName ," ",currentAssignerLName,
                                   "#",currentStatus,"#",currentPriority,"#",currentSeverity,"#",currentCategory);

   INSERT INTO xe_recent_activities (activity, activity_desc,module,activity_date,user,activity_status) VALUES (6,activityDescription,"BT",NOW(),userId,1);
    
Return 1;
END;;

CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewBugGroup`(
bugGroupName varchar(100)
) RETURNS int(11)
BEGIN
DECLARE count,insertStatus,bugGroupId INT DEFAULT 0;
SET insertStatus=-1;
    
	SELECT count(*) INTO count FROM xebt_bug_group where bug_group_name=bugGroupName;
          
        IF count=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF count=0 THEN 
        INSERT INTO `xebt_bug_group` (`bug_group_name`) VALUES 
        (bugGroupName);
        SELECT bug_group_id INTO bugGroupId from xebt_bug_group where bug_group_name=bugGroupName;
        SET insertStatus=bugGroupId;
                               
        END IF;
RETURN insertStatus;
END;;

CREATE DEFINER=`xenon`@`localhost` FUNCTION `createScriptlessBuild`(
buildName varchar(400),
buildDescription varchar(400),
buildActive INT(11),
releaseId INT(11),
buildCreator INT(11),
buildEnv INT(11),
currentDate varchar(200)
) RETURNS int(11)
BEGIN
DECLARE buildCreateDate DATETIME;
DECLARE count,insertStatus INT DEFAULT 0;
SET insertStatus=-1;
    
	SELECT count(*) INTO count FROM xesfdc_automation_build where build_name=buildName and release_id=releaseId;
          
        IF count=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF count=0 THEN 
        INSERT INTO `xesfdc_automation_build` (`build_name`,  `build_desc`, `build_status`,`release_id`,`build_state`,`build_createdate`,`build_creator`,`build_env`) VALUES 
        (buildName,buildDescription,buildActive,releaseId,1,currentDate,buildCreator,buildEnv);
                
        SET insertStatus=1;
                               
        END IF;
RETURN insertStatus;
END;;
CREATE DEFINER=`xenon`@`localhost` FUNCTION `insertSfdcBuildExecDetails`(
buildId INT,
vmId INT,
browserTemp INT,
mailGroupId INT,
stepWiseScreenshot INT,
reportBug INT,
userId INT
) RETURNS int(11)
BEGIN
DECLARE buildExecId INT DEFAULT 0 ;
DECLARE currentTime DATETIME;

SELECT NOW() INTO currentTime;
INSERT INTO `xesfdc_build_exec_details` (`build_id`, `vm_id`, `browser`, `mail_group_id`, `step_wise_screenshot`, 
`report_bug`, `user_id`, `start_time`) 
VALUES (buildId, vmId, browserTemp, mailGroupId, stepWiseScreenshot, reportBug, userId, currentTime);

SELECT build_exec_id INTO buildExecId from xesfdc_build_exec_details where `build_id`=buildId and `vm_id`=vmId and  
`browser` =browserTemp  and `mail_group_id` = mailGroupId and  `step_wise_screenshot`=  stepWiseScreenshot and 
`report_bug`=reportBug and  `user_id` = userId and `start_time`=currentTime;

INSERT INTO `xesfdc_vm_exec_details` (`vm_id`, `build_exec_id`) VALUES ( vmId, buildExecId);

UPDATE `xesfdc_automation_build` SET `build_state`='4' WHERE `build_id`=buildId;

RETURN 1;
END;;

CREATE DEFINER=`xenon`@`localhost` FUNCTION `sfdcCreateNewEnv`(
envName varchar(45),
envDescription LONGTEXT,
loginUrl LONGTEXT,
envActive INT(11)
) RETURNS int(11)
BEGIN
DECLARE envCount,insertStatus INT DEFAULT 0;
SET insertStatus=-1;
    
	SELECT Count(*) INTO envCount FROM xesfdc_env where env_name=envName;  
   
        IF envCount=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF envCount=0  THEN 
        INSERT INTO `xesfdc_env` (`env_name`, `env_desc`, `login_url`, `env_status`) VALUES (envName, envDescription, loginUrl, envActive);
        
        SET insertStatus=1;
                               
        END IF;
        
RETURN insertStatus;
END;;


CREATE DEFINER=`xenon`@`localhost` FUNCTION `sfdcCreateNewAction`(
actionName varchar(45),
actionActive INT(11)
) RETURNS int(11)
BEGIN
DECLARE actionCount,insertStatus INT DEFAULT 0;
SET insertStatus=-1;
    
	SELECT Count(*) INTO actionCount FROM xesfdc_func where functionality=actionName;  
   
        IF actionCount=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF actionCount=0  THEN 
        INSERT INTO `xesfdc_func` (`functionality`, `func_status`) VALUES (actionName,actionActive);
        
        SET insertStatus=1;
                               
        END IF;
        
RETURN insertStatus;
END;;
/**
 * ****************************************************************************************************
 * ****************************************************************************************************
 * 
 * PROCEDURE
 * 
 * ****************************************************************************************************
 * ****************************************************************************************************
 */

CREATE DEFINER=`xenon`@`%` PROCEDURE `setPrefixes`( IN projectId int,IN moduleId int, IN testcase_name longtext, IN scenario_id int, IN summary longtext, IN precondition longtext, IN testStatus int, IN execution_type int, IN execution_time int, IN author int, IN updated_by int,IN created_date longtext , IN datasheet_status int)
BEGIN  
	if exists( SELECT tc.test_prefix FROM xetm_testcase as tc, xe_project as pro 
	where tc.projectid =projectId and pro.project_id =tc.projectid ORDER BY tc.testcase_id desc limit 1 ) 
	then INSERT INTO xetm_testcase (`testcase_name`,`scenario_id`,`summary`,`precondition`,`status`,`execution_type`,`execution_time`,`author`,`updated_by`,`test_prefix`,`projectid`,`created_date`,`module_id`,`excel_file_present_status`) 
	VALUES ( testcase_name, scenario_id, summary, precondition, testStatus, execution_type, execution_time, author, updated_by, (SELECT CONCAT_WS('-',SUBSTRING_INDEX(tc.test_prefix,'-',1),(SUBSTRING_INDEX(tc.test_prefix,'-',-1)+1)) 
	FROM xetm_testcase as tc ,xe_project as pro where tc.projectid =projectid and pro.project_id =tc.projectid ORDER BY tc.testcase_id desc LIMIT 1), projectId,created_date,moduleId,datasheet_status ); else INSERT INTO xetm_testcase (`testcase_name`,`scenario_id`,`summary`,`precondition`,`status`,`execution_type`,`execution_time`,`author`,`updated_by`,`test_prefix`,`projectid`,`created_date`,`module_id`,`excel_file_present_status`) 
	VALUES (testcase_name, scenario_id, summary, precondition, testStatus, execution_type, execution_time, author, updated_by, (SELECT CONCAT_WS('-',tm_prefix,1) FROM xe_project as pro where pro.project_id=projectid), projectId,created_date,moduleId,datasheet_status ); 
	end if; 
END;;

CREATE DEFINER=`xenon`@`%` PROCEDURE `setBTPrefixes`( IN projectId int, IN bug_title longtext,IN bug_desc longtext,IN module_id int,IN created_date datetime,IN created_by INT,IN assignbug_status INT,IN bugGroupStatus INT,IN bugGroupId INT,IN jira_id varchar(15),IN updated_date datetime,IN jira_resolution varchar(300),IN fix_version varchar(100))
BEGIN 
if exists 
(SELECT bt.bug_prefix FROM xebt_bug as bt, xe_project as pro where bt.project =projectId and pro.project_id = bt.project ORDER BY bt.bug_id desc limit 1 ) 

then 

INSERT INTO xebt_bug (bug_title, bug_desc, project, module, create_date, created_by, bug_prefix,assign_status,group_status,group_id,jira_id,updated_date,jira_resolution,fix_version) 

VALUES (bug_title, bug_desc, projectId, module_id, created_date, created_by, (SELECT CONCAT_WS('-',SUBSTRING_INDEX(bt.bug_prefix,'-',1),(SUBSTRING_INDEX(bt.bug_prefix,'-',-1)+1)) 
	FROM 
	xebt_bug as bt ,xe_project as pro where bt.project =projectid and pro.project_id =bt.project ORDER BY bt.bug_id desc LIMIT 1),assignbug_status,bugGroupStatus,bugGroupId,jira_id,updated_date,jira_resolution,fix_version); 
else 

	INSERT INTO xebt_bug (bug_title, bug_desc, project, module, create_date, created_by, bug_prefix,assign_status,group_status,group_id,jira_id,updated_date,jira_resolution,fix_version) VALUES (bug_title, bug_desc, projectId, module_id, created_date, created_by, (SELECT CONCAT_WS('-',bt_prefix,1) FROM xe_project as pro where pro.project_id=projectid),assignbug_status,bugGroupStatus,bugGroupId,jira_id,updated_date,jira_resolution,fix_version);
	end if; 
	
	END;;
CREATE DEFINER=`xenon`@`localhost` PROCEDURE `createbug`(IN projectId int,IN userId int)
BEGIN
	  SELECT * FROM xebt_status where bug_state = 1;
	  SELECT * FROM xebt_priority WHERE priority_status = 1;
	  SELECT * FROM xe_module where module_active = 1 and project_id =projectId and module_id IN(select module_id from xe_user_module where user_id = userId);
	  SELECT userPro.project_id,userPro.user as user_id,CONCAT(user.first_name,' ',user.last_name) as userName FROM xe_user_project as userPro LEFT JOIN xe_user_details as user ON user.user_id = userPro.user where userPro.project_id = projectId and user.user_status = 1;
	  SELECT * FROM xebt_severity WHERE severity_status = 1;
      SELECT * FROM xebt_category where category_status=1;
      SELECT * FROM xe_build_execution_type where status=1;
      SELECT * FROM xe_build where build_status=1;
      SELECT * FROM xe_automation_build where build_status=1;
END ;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `bugsummary`(IN projectId int,IN bugId int, IN startValue int, IN offsetValue int,OUT commentcount int)
BEGIN
DECLARE buildType INT(11);

                              SELECT * FROM xebt_status where bug_state = 1;
                              SELECT * FROM xebt_priority WHERE priority_status = 1;
                              
                              SELECT distinct xb.bug_id,xb.group_status,(SELECT bug_group_name FROM xebt_bug_group WHERE bug_group_id=xb.group_id) 
                              AS bug_groupName,xb.group_id,xb.assign_status,
                              xb.bug_prefix,xb.bug_title,xb.bug_desc,xb.project,xp.project_name,@moduleId :=xb.module module,xm.module_name,xb.create_date,xb.created_by,
                                                                           CONCAT(xud.first_name, ' ',xud.last_name) bug_author_name,xud.user_photo 
                                                                           from xebt_bug xb,xe_project xp,xe_module xm ,xe_user_details xud 
                                                                           where xb.project=xp.project_id and xb.module=xm.module_id and xb.created_by= xud.user_id and xb.bug_id = bugId;
                                                                           
                              SELECT user_id,first_name,last_name from xe_user_details userDetails where user_id  in (SELECT module.user_id FROM xe_user_module as module WHERE module_id = @moduleId) and user_id in (SELECT project.user FROM xe_user_project as project WHERE project_id = projectId) and user_id IN ( SELECT user_id FROM xe_user_details where user_status=1) and bt_status=1;
                              
                              SELECT distinct (xb.bs_id),xb.group_assign,xb.bug_id,xb.bug_status,xs.bug_status currentStatus,xb.prev_status,xb.prev_severity,xb.bug_severity,xb.category,xb.prev_category,xps.bug_status prevStatus,
                                             xb.bug_priority,xp.bug_priority currentPriority,xb.bug_severity,severity.bug_severity currentSeverity,curCategory.category_name currentCategory,xb.assignee,CONCAT(xud.first_name, ' ',xud.last_name) bug_assignee_name, 
                                              xb.prev_priority ,xpp.bug_priority prevPriority,xb.prev_priority ,prev_severity.bug_severity prevSeverity,previousCategory.category_name prevCategory,xb.prev_assignee,CONCAT(xpud.first_name, ' ',xpud.last_name) prev_assignee_name, 
                                              xb.update_date,DATE_FORMAT(xb.update_date , '%d-%m-%Y %h:%i %p') as date,xb.comment_desc,xb.updated_by,
                                             TIMESTAMPDIFF(MINUTE,xb.update_date,CURRENT_TIME()) AS TimeDiff,CONCAT(xu.first_name, ' ',xu.last_name) bug_updated_by,xu.user_photo 
                                              from xebt_bugsummary xb,xebt_status xs,xebt_status xps ,xebt_priority xp,xebt_priority xpp,xebt_severity severity,xebt_severity prev_severity,xebt_category curCategory,xebt_category previousCategory,
										      xe_user_details xud,xe_user_details xpud,xe_user_details xu  
                                              where xb.bug_status=xs.status_id and xb.bug_priority=xp.priority_id and xb.bug_severity=severity.severity_id and xb.category=curCategory.category_id and xb.assignee = xud.user_id 
                                              and xb.prev_status=xps.status_id and xb.prev_priority=xpp.priority_id and xb.prev_severity=prev_severity.severity_id 
                                              and xb.prev_category=previousCategory.category_id                                                
                                              and xb.prev_assignee = xpud.user_id and xb.updated_by = xu.user_id 
                                              and xb.bug_id = bugId GROUP BY xb.update_date DESC LIMIT startValue,offsetValue;
                                              
                                              
                              SELECT attach_id,attach_title FROM xebt_bug_attachment where bug_id = bugId;
                              SELECT * FROM xebt_severity WHERE severity_status = 1;
							  SELECT * FROM xebt_category where category_status=1;
                              
                              
							  SELECT module FROM xebt_bug WHERE xebt_bug.bug_id=bugId;
                                
							  SELECT * FROM xebt_bug_group;
                              
                              SELECT is_build_assigned INTO buildType FROM xebt_bugsummary where bug_id = bugId GROUP BY bs_id DESC LIMIT 1;
                             
                              IF buildType=1 THEN 
								SELECT * FROM xe_build where build_status=1;
								END IF;
							  IF buildType=2 THEN 
								SELECT * FROM xe_automation_build where build_status=1;
                                END IF;
                                
                               
SELECT count(*) INTO commentcount FROM xebt_bugsummary xb
WHERE xb.bug_id = bugId;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getLogin`(IN userName VARCHAR(45),IN upassword VARCHAR(45))
BEGIN
SELECT ud.user_id,ud.user_password,ud.email_id,ud.first_name,ud.last_name,@roleId := ud.role_id as role_id,ud.tm_status,ud.bt_status,ud.user_status,ud.user_photo,
ud.location,ud.about_me,ud.pass_flag,ro.role_status,ro.role_type,tz.timezone_id 
	FROM xe_user_details as ud ,xe_role as ro , xe_timezone_details as tz
    where ud.user_password =SHA1(upassword) and ud.user_name=userName and ro.role_id = ud.role_id and ud.user_status =1 and ud.user_timezone=tz.xe_timezone_id;	
	SELECT * from xe_bm_access where role_id = @roleId;
	
    SELECT * from xe_tm_access where roleID = @roleId;
    
    SELECT * from xe_bt_access where roleID = @roleId;
    
    SELECT * FROM xe_support_access  where role_id = @roleId;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `build_project`(IN in_userID int,IN build_auto int)
BEGIN
SELECT * FROM 
	(SELECT project_id, project_name, project_description, project_active, prj_mdl.module_id, module_name, module_description, scenario_id, scenario_name, scenario_description  FROM 
			(SELECT prj.project_id, project_name, project_description, project_active, module_id, module_name, module_description FROM 
				(SELECT * FROM xe_project WHERE project_id in (SELECT project_id FROM xe_user_project WHERE user=in_userID) AND project_active=1) AS prj
				LEFT OUTER JOIN 
				(SELECT * FROM xe_module WHERE module_id IN (SELECT module_id FROM xe_user_module WHERE user_id=in_userID) AND module_active=1) AS mdl ON mdl.project_id=prj.project_id) AS prj_mdl
					LEFT OUTER JOIN 
					(SELECT * FROM xetm_scenario WHERE status=1) AS sce ON sce.module_id=prj_mdl.module_id) AS prj_mdl_sce
						LEFT OUTER JOIN
                        (SELECT * FROM 
							(SELECT test_case.testcase_id, testcase_name,execution_type, scenario_id as sce_id, projectid,test_complete,module_id as mdl_id FROM xetm_testcase AS test_case 
								LEFT OUTER JOIN 
								(SELECT 'test_complete',testcase_id FROM xebm_buildtestcase WHERE build_id=build_auto)AS build_test ON build_test.testcase_id=test_case.testcase_id) AS test_build_status
									LEFT OUTER JOIN 
									(SELECT 'test_executed',tc_id FROM xe_testcase_execution_summary WHERE build_id=build_auto group by tc_id) AS test_exe ON test_exe.tc_id=test_build_status.testcase_id) AS test_case 
									     ON prj_mdl_sce.scenario_id=test_case.sce_id and test_case.execution_type IN(1,3);
END;;
CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getReportByBuildId`(IN buildId INT,IN userId INT)
BEGIN

                SELECT * FROM  xe_project where project_id IN 
                (SELECT project_id FROM  xe_module where module_id IN 
                (SELECT module_id FROM  xetm_scenario where scenario_id IN 
                (SELECT scenario_id FROM  xetm_testcase where testcase_id IN 
                (SELECT testcase_id FROM  xebm_buildtestcase where build_id=buildId)))) 
                and project_id IN (SELECT project_id FROM xe_user_project where user =userId) 
                and project_active=1 and project_id IN 
                (SELECT project_id FROM xebm_assign_build where user_id =userId);
        
                SELECT * FROM xe_module where module_id IN 
                (SELECT module_id FROM xetm_scenario where scenario_id IN 
                (SELECT scenario_id FROM xetm_testcase where testcase_id IN 
                (SELECT testcase_id FROM xebm_buildtestcase where build_id=buildId))) 
                and module_id IN 
                (SELECT module_id FROM xebm_assign_build where user_id =userId);
    
    
                SELECT * FROM xetm_scenario where scenario_id IN 
                (SELECT scenario_id FROM xetm_testcase where testcase_id IN 
                (SELECT testcase_id FROM xebm_buildtestcase where build_id=buildId));
   
                SELECT * FROM xetm_testcase where testcase_id IN 
                (SELECT testcase_id FROM xebm_buildtestcase where build_id=buildId);
    
    SELECT S.exe_step_bug_id,S.test_step_id,S.bug_id,S.step_exe_id,S.test_case_id,S.build_id,B.bug_prefix FROM xe_step_execution_bug S,xebt_bug B where build_id=buildId and B.bug_id=S.bug_id;
    
    SELECT * FROM xetm_testcase_steps;
    
    SELECT * FROM xetm_exec_status where status=1;
    
    SELECT distinct TS.exec_id,TS.tc_id,TS.build_id,TS.project_id,TS.mudule_id, TS.scenario_id,TS.tester_id,
                TS.status_id,TS.notes,ES.description,TS.complete_status 
                FROM xe_testcase_execution_summary TS 
                INNER JOIN xebm_buildtestcase BT on BT.testcase_id=TS.tc_id 
                INNER JOIN xetm_exec_status ES on TS.status_id=ES.exst_id 
                where TS.build_id=buildId;
        
                SELECT * FROM xe_steps_execution_summary where testex_id IN  
                (SELECT exec_id FROM xe_testcase_execution_summary where build_id=buildId);
    
    SELECT count(*) TotalTcCount,project_id FROM xebm_buildtestcase where build_id=buildId GROUP BY project_id;
        
    SELECT count(*) ExecutedTcCount,SUM(case when status_id = 1 then 1 else 0 end) as PassCount,
    SUM(case when status_id = 2 then 1 else 0 end) as FailCount,SUM(case when status_id = 3 then 1 else 0 end) as SkipCount,
    SUM(case when status_id = 4 then 1 else 0 end) as BlockCount,project_id 
                FROM xe_testcase_execution_summary where build_id=buildId and complete_status=1 GROUP BY project_id;    
    
    SELECT count(*) TcCount,module_id FROM xebm_buildtestcase where build_id=buildId GROUP BY module_id;
    
    SELECT count(*) TcCount,SUM(case when status_id = 1 then 1 else 0 end) as PassCount,
    SUM(case when status_id = 2 then 1 else 0 end) as FailCount,
    SUM(case when status_id = 3 then 1 else 0 end) as SkipCount,
    SUM(case when status_id = 4 then 1 else 0 end) as BlockCount,mudule_id
    FROM xe_testcase_execution_summary where build_id=buildId and complete_status=1 GROUP BY mudule_id;
    
    SELECT count(*) TcCount,scenario_id FROM xebm_buildtestcase where build_id=buildId  GROUP BY scenario_id; 
    
    SELECT count(*) TcCount,SUM(case when status_id = 1 then 1 else 0 end) as PassCount,
    SUM(case when status_id = 2 then 1 else 0 end) as FailCount,
    SUM(case when status_id = 3 then 1 else 0 end) as SkipCount,
    SUM(case when status_id = 4 then 1 else 0 end) as BlockCount,scenario_id
    FROM xe_testcase_execution_summary where build_id=buildId and complete_status=1 GROUP BY scenario_id;
    
    SELECT attach_id,testcase_id,attachment_title 
	FROM xe_testcase_execution_attachments
	WHERE testcase_id in (select tc_id FROM xe_testcase_execution_summary WHERE build_id = buildId)
	and build_id = buildId;

END;;
CREATE DEFINER=`xenon`@`localhost` PROCEDURE `testspecification`(IN projectId int,IN userId int)
BEGIN
  SELECT project_id,project_name,project_description FROM xe_project where project_id=projectId;
  SELECT module_id,module_name,module_description FROM xe_module WHERE module_id IN (SELECT module_id FROM xe_user_module where project_id=projectId and user_id = userId) and module_active = 1;
  SELECT * FROM xetm_scenario where module_id IN (SELECT module_id FROM xe_user_module where project_id=projectId and user_id = userId) and module_id IN (select module_id from xe_module where module_active = 1);
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `testcase`(IN userId int,IN projectId int,IN moduleId int,IN scenarioId int, IN startValue int, IN offsetValue int,OUT count int)
BEGIN
  
SELECT scenario_id, scenario_name, module_id, scenario_description FROM xetm_scenario where module_id = moduleId and status = 1;
 
SELECT distinct xt.testcase_id,xt.testcase_name,xt.summary,xt.precondition,xt.execution_type,xet.description exe_type,xt.execution_time,xts.tc_status_id,xts.status,xt.test_prefix,CONCAT(user.first_name,' ',user.last_name) authorFullName, user.about_me authorAboutMe, user.user_photo 
FROM xetm_testcase xt,xetm_execution_type xet,xetm_testcase_status xts,xe_user_details as user, xetm_testcase_summary as tcSummary
where xt.execution_type = xet.execution_type_id and xt.status = xts.tc_status_id and user.user_id = xt.author and xet.status = 1 and scenario_id=scenarioId and module_id=moduleId
and xt.testcase_id = tcSummary.test_case_id and tcSummary.test_summary_id in (select max(test_summary_id) from xetm_testcase_summary group by test_case_id)
ORDER BY tcSummary.activity_date DESC LIMIT startValue, offsetValue;

SELECT module_id,module_name FROM xe_module WHERE module_id IN (SELECT module_id FROM xe_user_module where project_id=projectId and user_id = userId) and module_active = 1;

SET @currcount = NULL, @currvalue = NULL; 
select * from (SELECT
    test_summary_id,test_case_id,activity_desc, activity_date,TIMESTAMPDIFF(MINUTE,activity_date,CURRENT_TIME()) AS TimeDiff,xetm_testcase_summary.user,(select concat(first_name,' ',last_name)  from xe_user_details where xe_user_details.user_id=xetm_testcase_summary.user) AS user_name,
    @currcount := IF(@currvalue = test_case_id, @currcount + 1, 1) AS count,
    @currvalue := test_case_id 
FROM xetm_testcase_summary 
ORDER BY test_case_id,test_summary_id desc) as summaryTable where summaryTable.count<=5 and  summaryTable.test_case_id IN(select testcase_id from xetm_testcase where scenario_id = scenarioId);

SELECT count(*) INTO count
FROM xetm_testcase xt,xetm_execution_type xet, xetm_testcase_summary as tcSummary
where xt.execution_type = xet.execution_type_id 
and xet.status = 1 and scenario_id=scenarioId and module_id=moduleId
and xt.testcase_id = tcSummary.test_case_id and tcSummary.test_summary_id in (select max(test_summary_id) from xetm_testcase_summary group by test_case_id);
	
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `testcasesteps`(IN userId int,IN projectId int,IN moduleId int,IN scenarioId int,IN testCaseId int)
BEGIN
SELECT * FROM xetm_scenario where module_id=moduleId and status = 1;
SELECT distinct xt.testcase_id,xt.excel_file_present_status,xt.testcase_name,xt.summary,xt.precondition,xt.execution_type,xet.description exe_type,xt.execution_time,xts.tc_status_id,xts.status,xt.test_prefix,CONCAT(user.first_name,' ',user.last_name) authorFullName, user.about_me authorAboutMe, user.user_photo FROM xetm_testcase xt,xetm_execution_type xet,xetm_testcase_status xts,xe_user_details as user where xt.execution_type = xet.execution_type_id and xt.status = xts.tc_status_id and user.user_id = xt.author and xet.status = 1 and scenario_id=scenarioId and module_id=moduleId;
SELECT * FROM xetm_testcase_steps where testcase_id=testCaseId;
SELECT * FROM xetm_execution_type WHERE status=1;              
SELECT * FROM xetm_testcase_status WHERE testcase_state = 1;
SELECT tc.xetm_testcase_comments_id,tc.comment_description,tc.commnetor,DATE_FORMAT(tc.comment_time , '%d-%m-%Y %h:%i %p') as comment_time,tc.testcase_id, CONCAT(xud.first_name, ' ',xud.last_name) as user_name,xud.user_photo,TIMESTAMPDIFF(MINUTE,tc.comment_time,CURRENT_TIME()) AS TimeDiff ,tc.comment_time cmtTime FROM xetm_testcase_comments as tc, xe_user_details xud where tc.testcase_id = testCaseId and xud.user_id=tc.commnetor ORDER BY xetm_testcase_comments_id DESC;
SELECT * FROM xetm_comments_likes where liked_by_user=userId;
SELECT count(*) likeCount,comment_id FROM xetm_comments_likes where comment_id IN (SELECT xetm_testcase_comments_id FROM xetm_testcase_comments where testcase_id=testCaseId) GROUP BY comment_id;
SELECT module_id,module_name,module_description FROM xe_module WHERE module_id IN (SELECT module_id FROM xe_user_module where project_id=projectId and user_id = userId) and module_active = 1; 
SELECT test_summary_id,test_case_id,activity_desc,DATE_FORMAT(activity_date , '%d-%m-%Y %h:%i %p') as activity_date,DATE_FORMAT(activity_date , '%d-%b-%Y') as activityDate,TIMESTAMPDIFF(MINUTE,activity_date,CURRENT_TIME()) AS TimeDiff,user,concat(first_name,' ',last_name) as user_name,user_photo
  FROM xetm_testcase_summary ts,xe_user_details ud where ts.user= ud.user_id and test_case_id = testCaseId 
  ORDER BY test_summary_id desc;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getCreateUserData`(IN userId INT)
BEGIN
                                select * from xe_project where project_id IN
        (SELECT project_id FROM xe_user_project where user=userId) 
                                and project_Active = 1;

                                select * from xe_role where role_status = 1;
                            SELECT * FROM xe_email_details;
END;;
CREATE DEFINER=`xenon`@`localhost` PROCEDURE `insertUser`(IN userPassword VARCHAR(45),IN emailId VARCHAR(45),IN firstName VARCHAR(45),IN lastName VARCHAR(45),
IN roleId INT,IN tmStatus INT,IN btStatus INT,IN userStatus INT,IN userPhoto mediumblob,IN schemaName VARCHAR(45), IN createDate varchar(200),IN userName VARCHAR(100),
IN jenkinStatus INT,IN gitStatus INT)
BEGIN

insert into xe_user_details (user_name,user_password, email_id, first_name, last_name, role_id, tm_status, bt_status, user_status,user_timezone,user_photo, create_date, update_date,jenkin_status,git_status) 
values (userName,SHA1(userPassword),emailId,firstName,lastName,roleId,tmStatus,btStatus,userStatus,2,userPhoto, createDate, createDate,jenkinStatus,gitStatus);

select @custId:=cust.customer_id as customer_id from `xenon-core`.xe_customer_details as cust where cust.customer_schema =schemaName;

insert into `xenon-core`.xe_user(user_name,user_email,customer_id)values (userName,emailId,@custId);

select user_id from xe_user_details where email_id = emailId and user_name=userName;

select email.smtp_host,email.smtp_port,email.smtp_user_id,email.smtp_password  
from xe_email_details as email , xe_mail_notifications as mailnotify
where email.is_enabled=1 and mailnotify.create_user =1;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `dropTestCase`(IN testArray LONGTEXT,IN buildId int)
BEGIN
  DECLARE strLen    INT DEFAULT 0;
  DECLARE SubStrLen INT DEFAULT 0;
  DECLARE testCaseId INT DEFAULT 0;


  IF testArray IS NULL THEN
    SET testArray = '';
  END IF;

do_this:
  LOOP
    SET strLen = CHAR_LENGTH(testArray);
    SET testCaseId = SUBSTRING_INDEX(testArray, ',', 1);
		DELETE FROM xebm_buildtestcase WHERE testcase_id=testCaseId and build_id=buildId;
    SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(testArray, ',', 1)) + 2;
    SET testArray = MID(testArray, SubStrLen, strLen);
    IF testArray = '' THEN
      LEAVE do_this;
    END IF;
  END LOOP do_this;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `addTestCase`(IN testArray VARCHAR(255),IN buildId int,IN userId int)
BEGIN
  DECLARE strLen    INT DEFAULT 0;
  DECLARE SubStrLen INT DEFAULT 0;
  DECLARE testCaseId INT DEFAULT 0;
  DECLARE projectId INT DEFAULT 0;
  DECLARE moduleId INT DEFAULT 0;
  DECLARE sceId INT DEFAULT 0;
  DECLARE buildName TEXT DEFAULT null;
  DECLARE userName TEXT DEFAULT null;
   DECLARE activity_desc TEXT DEFAULT null;


  IF testArray IS NULL THEN
    SET testArray = '';
  END IF;

do_this:
  LOOP
    SET strLen = CHAR_LENGTH(testArray);
    SET testCaseId = SUBSTRING_INDEX(testArray, ',', 1);
		 SELECT @projectId:=M.project_id,@moduleId:=S.module_id,@sceId:=T.scenario_id FROM xe_module M,xetm_scenario S,xetm_testcase T where T.testcase_id=testCaseId and T.scenario_id=S.scenario_id and M.module_id=S.module_id;
         
		 DELETE FROM xebm_buildtestcase where xebm_buildtestcase.testcase_id=testCaseId and xebm_buildtestcase.build_id=buildId;
         
		 INSERT INTO xebm_buildtestcase(`build_id`,`testcase_id`,`project_id`,`scenario_id`,`module_id`) VALUES(buildId,testCaseId,@projectId,@sceId,@moduleId);
         
		 UPDATE xe_build SET build_state=2 WHERE build_id=buildId;
    SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(testArray, ',', 1)) + 2;
    SET testArray = MID(testArray, SubStrLen, strLen);
    IF testArray = '' THEN
      LEAVE do_this;
    END IF;
  END LOOP do_this;

  
          SELECT @buildName:=build_name,@userName:=user_name FROM (SELECT build_name FROM xe_build where build_id=buildId) AS build 
          JOIN (SELECT CONCAT(first_name,' ', last_name) AS user_name FROM xe_user_details WHERE user_id=userId) AS user;
          
		  SET activity_desc=CONCAT('Test cases are added to build: <br/><strong> Build name: </strong> ',@buildName,'<br/> <strong> Updated By: </strong> ',@userName);
          
          INSERT INTO xe_recent_activities (activity, activity_desc,module,activity_date,user,activity_status) 
          VALUES (10,activity_desc,'BM',NOW(),userId,1);
  
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getBuildDashboard`(IN userId INT(11))
BEGIN
DECLARE done INT DEFAULT 0;
DECLARE buildId INT DEFAULT 0;
DECLARE cur CURSOR FOR SELECT build_id FROM xe_build where build_status=1 order by build_id desc limit 5;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;


SELECT count(*) TotalTcCount , build_id FROM xebm_buildtestcase GROUP BY build_id DESC LIMIT 10;
SELECT count(*) AS ExecutedTcCount,build_id FROM xe_testcase_execution_summary where complete_status=1 GROUP BY build_id DESC LIMIT 10;
SELECT count(build_id) AS manualbuilds,DATE_FORMAT(build_createdate, '%d-%m-%Y') AS dateOnly FROM xe_build WHERE YEARWEEK(`build_createdate`, 1) = YEARWEEK(CURDATE(), 1) GROUP BY dateOnly DESC ;
SELECT count(build_id) AS automationbuilds,DATE_FORMAT(build_createdate, '%d-%m-%Y') AS dateOnly FROM xe_automation_build WHERE YEARWEEK(`build_createdate`, 1) = YEARWEEK(CURDATE(), 1) GROUP BY dateOnly DESC ;

SELECT (SELECT COUNT(*) FROM xe_build WHERE build_status=1 AND release_id IN(SELECT release_id FROM xe_release WHERE release_active=1)) as total_build,(SELECT COUNT(*) FROM xe_release WHERE release_active=1)  as total_release, (SELECT SUM(TIMESTAMPDIFF(second,start_time,end_time)) FROM xebm_build_exec_details)  as ExecTimeSec,(select ifnull(sum(bug_count), 0) total from (SELECT count(*) bug_count FROM xe_step_execution_bug where bug_id IN (SELECT bug_id FROM xebt_bugsummary where bs_id IN (SELECT max(bs_id) FROM xebt_bugsummary GROUP BY bug_id) and bug_status IN (1,2,3,4))  and build_id in  (SELECT build_id FROM xe_build where build_status=1 AND release_id IN(SELECT release_id FROM xe_release WHERE release_active=1)) group by build_id) src) as total_bug,(SELECT (SELECT COUNT(*) AS TotalTc FROM xebm_buildtestcase where build_id IN (SELECT build_id FROM xe_build where build_status=1) and project_id IN (SELECT DISTINCT(project_id) FROM xe_user_project WHERE user=userId)) -(SELECT count(*) AS ExecTc FROM xe_testcase_execution_summary where complete_status=1 and build_id IN (SELECT build_id FROM xe_build where build_status=1) and project_id IN (SELECT DISTINCT(project_id) FROM xe_user_project WHERE user=userId))) AS total_test;

SELECT build_id,build_name,build_createdate FROM xe_build where build_status=1 order by build_id desc limit 5;
                OPEN cur;
                read_loop: LOOP
                                FETCH cur INTO buildId; 
        
                                IF done THEN
                                                LEAVE read_loop;  
                                END IF;
        
       select Count(*) as tc_count,build_id,(SELECT description FROM xetm_exec_status WHERE exst_id=status_id)
                                as tc_status FROM xe_testcase_execution_summary where build_id=buildId AND status_id<>5 group by status_id
                                UNION ALL
                                (SELECT (count(*)-(select ifnull(sum(tc_count), 0) from (select Count(*) as tc_count,build_id,
                                (SELECT description FROM xetm_exec_status WHERE exst_id=status_id) as tc_status FROM xe_testcase_execution_summary where build_id=buildId
                                group by status_id) src where tc_status<>'Not run')) as not_run,build_id,'Not run' FROM xebm_buildtestcase where build_id=buildId);
 
                END LOOP;
                CLOSE cur; 
                


END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `viewbuild`(IN userId int, IN automationStatus int, IN scriptlessStatus int, IN manualStartValue int, IN autoStartValue int, IN scStartValue int, IN pageSize int, OUT manualbuildcount INT, OUT autobuildcount int, OUT scbuildcount int)
BEGIN

SELECT @vieWAccess:=viewallbuild,buildproject,assignbuild,removetestcases  FROM xe_bm_access  where role_id IN(select role_id from xe_user_details where user_id = userId);

SELECT xab.build_id,xud.user_id,xud.first_name,xud.last_name FROM xebm_assign_build xab,xe_user_details xud where xab.user_id = xud.user_id and build_id IN(SELECT build_id from xe_build where (build_state=3 || build_state = 4 || build_state = 5 ) and (@vieWAccess = 1  or build_creator=userId or (SELECT count(build_id) FROM xebm_assign_build where build_id=xe_build.build_id and user_id = userId)));

IF @vieWAccess = 1 THEN
	SELECT build_id,build_name,build_desc,a1.status as buildStatus,build_status,r1.release_name as releaseName,
	build_createdate,state.desc as buildState, build_state,
	Concat(user.first_name ,' ', user.last_name) as creator , build.build_exe_comments,
	build.build_creator as creatorID,user.about_me,user.user_photo,(SELECT count(build_id) FROM xebm_assign_build where build_id=build.build_id and user_id = userId) as assignStatus 
	FROM xe_build as build INNER JOIN xe_active as a1 ON a1.active_id = build.build_status 
	INNER JOIN xe_release as r1 on r1.release_id = build.release_id
	inner join xebm_build_state as state on state.id=build.build_state
	inner join xe_user_details as user on user.user_id = build.build_creator where r1.release_active=1 and (build.build_creator=userId or (select count(project_id) from xe_user_project where user= userId and project_id in (select project_id from xe_user_project where user= build.build_creator)))  
	ORDER BY build.build_updatedate DESC LIMIT manualStartValue, pageSize;

	
	SELECT count(*) INTO manualbuildcount FROM xe_build as build 
	INNER JOIN xe_release as r1 on r1.release_id = build.release_id inner join xe_user_details as user on user.user_id = build.build_creator 
	where r1.release_active=1 and (build.build_creator= userId or (select count(project_id) from xe_user_project where user= userId 
	and project_id in (select project_id from xe_user_project where user= build.build_creator))) ;
ELSE
	SELECT build_id,build_name,build_desc,a1.status as buildStatus,build_status,r1.release_name as releaseName,
	build_createdate,state.desc as buildState, build_state,
	Concat(user.first_name ,' ', user.last_name) as creator , build.build_exe_comments,
	build.build_creator as creatorID,user.about_me,user.user_photo,(SELECT count(build_id) FROM xebm_assign_build where build_id=build.build_id and user_id = userId) as assignStatus 
	FROM xe_build as build INNER JOIN xe_active as a1 ON a1.active_id = build.build_status 
	INNER JOIN xe_release as r1 on r1.release_id = build.release_id
	inner join xebm_build_state as state on state.id=build.build_state
	inner join xe_user_details as user on user.user_id = build.build_creator where r1.release_active=1 and build.build_creator=userId or(build.build_state IN(3,4) and (SELECT count(build_id) FROM xebm_assign_build where build_id=build.build_id and user_id = userId))  
	ORDER BY build.build_updatedate DESC LIMIT manualStartValue, pageSize;
	
	SELECT count(*) INTO manualbuildcount FROM xe_build as build 
	INNER JOIN xe_release as r1 on r1.release_id = build.release_id
	where r1.release_active=1 and build.build_creator= userId 
	or(build.build_state IN(3,4) and (SELECT count(build_id) FROM xebm_assign_build where build_id=build.build_id and user_id = userId));

END IF; 

IF automationStatus = 1 THEN
	SELECT build_id,build_name,build_desc,a1.status as buildStatus,build_status,r1.release_name as releaseName,
	build_createdate,state.desc as buildState, build_state,
	Concat(user.first_name ,' ', user.last_name) as creator ,
	build.build_creator as creatorID,user.about_me,user.user_photo
	FROM xe_automation_build as build INNER JOIN xe_active as a1 ON a1.active_id = build.build_status 
	INNER JOIN xe_release as r1 on r1.release_id = build.release_id
	inner join xebm_build_state as state on state.id=build.build_state
	inner join xe_user_details as user on user.user_id = build.build_creator where r1.release_active=1 and build.build_creator=userId 
	ORDER BY build.build_updatedate DESC LIMIT autoStartValue, pageSize;

	SELECT count(*) INTO autobuildcount	FROM xe_automation_build as build 
	INNER JOIN xe_release as r1 on r1.release_id = build.release_id
	where r1.release_active=1 and build.build_creator=userId;
END IF; 

IF scriptlessStatus = 1 THEN	
	SELECT build_id,build_name,build_desc,a1.status as buildStatus,build_status,r1.release_name as releaseName,
	build_createdate,state.desc as buildState, build_state, build_env,
	Concat(user.first_name ,' ', user.last_name) as creator ,
	build.build_creator as creatorID,user.about_me,user.user_photo
	FROM xesfdc_automation_build as build INNER JOIN xe_active as a1 ON a1.active_id = build.build_status 
	INNER JOIN xe_release as r1 on r1.release_id = build.release_id
	inner join xebm_build_state as state on state.id=build.build_state
	inner join xe_user_details as user on user.user_id = build.build_creator where r1.release_active=1 and build.build_creator=userId 
	ORDER BY build.build_updatedate DESC LIMIT scStartValue, pageSize;

	SELECT count(*) INTO scbuildcount FROM xesfdc_automation_build as build 
	INNER JOIN xe_release as r1 on r1.release_id = build.release_id
	where r1.release_active=1 and build.build_creator=userId;
END IF;    
 
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getProjectData`()
BEGIN
	SELECT project_id,project_name,project_description,active.status as projectStatus,project_createdate FROM xe_project as project 
	INNER JOIN xe_active as active on active.active_id=project.project_active;

	SELECT * from xe_module;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getEditProjectData`(IN projectId INT)
BEGIN
SELECT project_id,project_name,project_description,bt_prefix,tm_prefix,active.status as projectStatus,project_createdate 
FROM xe_project as project 
INNER JOIN xe_active as active on active.active_id=project.project_active 
WHERE project_id = projectId;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `updateProjectData`(IN projectDescription LONGTEXT,IN projectActive INT,IN projectId INT)
BEGIN
UPDATE xe_project SET project_description = projectDescription, project_active =projectActive  WHERE project_id = projectId;
END;;
CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getAssignProjectData`(IN projectId INT)
BEGIN

SELECT * FROM xe_user_details as ud where user_id not in 
(SELECT user FROM xe_user_project where project_id in (projectId)) and ud.user_status = 1;

SELECT * FROM xe_user_details as ud where user_id in 
(SELECT user FROM xe_user_project where project_id in (projectId)) and ud.user_status = 1;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `assignOrUnassignProjectToUser`(IN projectId INT)
BEGIN

		SELECT * FROM xe_user_details as ud where user_id in 
		(SELECT user FROM xe_user_project where project_id in (projectId)) and ud.user_status = 1;

		SELECT * FROM xe_email_details;

		SELECT * FROM xe_mail_notifications;

		SELECT * FROM xe_user_details as ud where user_id not in 
		(SELECT user FROM xe_user_project where project_id in (projectId)) and ud.user_status = 1;

END ;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getEditUserData`(IN userId INT)
BEGIN
		select * from xe_user_details where user_id =userId;
        
        select * from xe_role where role_status = 1;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getViewUserData`( IN startValue int, IN offsetValue int,OUT count int)
BEGIN
       SELECT user_id,first_name,last_name,email_id,role.role_type,userDetails.role_id as roleID ,  
       active.status userStatus,tmStatus.status tmStatus, btStatus.status btStatus,user_photo,about_me  
       FROM xe_user_details userDetails  
       INNER JOIN xe_role role ON role.role_id=userDetails.role_id 
       INNER join xe_active active ON active.active_id = userDetails.user_status 
       INNER JOIN xe_active tmStatus ON tmStatus.active_id=userDetails.tm_status 
       INNER JOIN xe_active btStatus ON btStatus.active_id=userDetails.bt_status 
       Where userDetails.user_status<>3 order by userdetails.user_id DESC LIMIT startValue, offsetValue;
       
       select count(*) INTO count  from  xe_user_details userDetails  
       INNER JOIN xe_role role ON role.role_id=userDetails.role_id 
       Where user_status<>3;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `updateUserData`(IN userId VARCHAR(45),IN firstName VARCHAR(45),IN lastName VARCHAR(45),IN roleId INT,IN tmStatus INT,IN btStatus INT,IN userStatus INT,IN updateDate VARCHAR(200),IN jenkinStatus INT,IN gitStatus INT)
BEGIN
UPDATE xe_user_details  SET `first_name`=firstName, `last_name`=lastName,`role_id`=roleId, `tm_status`=tmStatus, `bt_status`=btStatus, `user_status`=userStatus,`update_date`=updateDate, `jenkin_status`=jenkinStatus, `git_status`=gitStatus  WHERE `user_id`=userId;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getUserAssignedProjects`(IN userId INT)
BEGIN

SELECT project_id,project_name,project_active FROM xe_project where project_id in ( SELECT project_id FROM xe_user_project where user=userId);

SELECT project_id,project_name,project_active FROM xe_project where project_active=1 and project_id not in ( SELECT project_id FROM xe_user_project where user=userId);


SELECT module_id,module_name,module.project_id,module_description,module_active,pro.project_name FROM 
xe_module as module  INNER JOIN 
xe_project pro on pro.project_id = module.project_id  WHERE module.project_id in 
(SELECT up.project_id FROM xe_user_project as up where user = userId) and module_id in 
(SELECT module_id FROM xe_user_module where user_id = userId);


SELECT module_id,module_name,module.project_id,module_description,module_active,pro.project_name FROM 
xe_module as module  INNER JOIN 
xe_project pro on pro.project_id = module.project_id  WHERE module.project_id in 
(SELECT up.project_id FROM xe_user_project as up where user = userId) and module_id not in 
(SELECT module_id FROM xe_user_module where user_id = userId);

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `editbuild`(IN buildId int,IN buildType int)
BEGIN
IF buildType= 1 THEN
SELECT build_id,build_name,build_desc,a1.status as buildStatus, build.build_creator, r1.release_name as releaseName FROM xe_build as build
			   INNER JOIN xe_active as a1 ON a1.active_id = build.build_status INNER JOIN xe_release as r1 on r1.release_id = build.release_id WHERE build_id= buildId;
ELSE
SELECT build_id,build_name,build_desc,a1.status as buildStatus, build.build_creator, r1.release_name as releaseName FROM xe_automation_build as build
			   INNER JOIN xe_active as a1 ON a1.active_id = build.build_status INNER JOIN xe_release as r1 on r1.release_id = build.release_id WHERE build_id= buildId;
  END IF; 
SELECT release_id,release_name FROM xe_release WHERE release_active=1;

END;;
CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getAccountSettingsData`(IN customerId INT)
BEGIN

DECLARE schemaName varchar(45);

SELECT * from `xenon-core`.xe_customer_type;

SELECT customer_id,customer_name,customer_address,customer_logo,customer_type,automation_status,created_date,start_date,end_date,tm_status,bt_status,
customer_status,DATE_FORMAT(created_date , '%d-%m-%Y %h:%i %p') as createdDate,
DATE_FORMAT(start_date , '%d-%m-%Y %h:%i %p') as startDate,DATE_FORMAT(end_date , '%d-%m-%Y %h:%i %p') as endDate ,customer_schema,issue_tracker,issue_tracker_type,issue_tracker_host,licen.max_cloud_space
FROM `xenon-core`.xe_customer_details as cd,`xenon-core`.xe_licence_details as licen where customer_id=customerId and licen.type_id = cd.customer_type;

SELECT customer_schema INTO schemaName FROM `xenon-core`.xe_customer_details where customer_id=customerId LIMIT 1;

SET @queryToExec= CONCAT('SELECT * FROM `',schemaName,'`.xe_user_details where role_id =2;');

SELECT * from `xenon-core`.xe_issue_tracker_type;
PREPARE stmt FROM @queryToExec;
  EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SELECT table_schema,round((sum(data_length + index_length) / 1024 / 1024), 2) 'Used_Space'
FROM information_schema.TABLES WHERE table_schema=schemaName GROUP BY table_schema ;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getMailSettings`()
BEGIN

SELECT * FROM xe_email_details;

SELECT * FROM xe_mail_notifications;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getExecuteBuildList`(IN userId INT, IN manualStartValue int, IN autoStartValue int, IN pageSize int, OUT manualbuildcount INT, OUT autobuildcount int, OUT scbuildcount int)
BEGIN
	
DECLARE buildId INT;
DECLARE finished INTEGER DEFAULT 0;
    
DECLARE buildIdList CURSOR FOR SELECT build_id FROM xe_build as build INNER JOIN xe_active as a1 ON a1.active_id = build.build_status 
INNER JOIN xe_release as r1 on r1.release_id = build.release_id 
inner join xebm_build_state as state on state.id=build.build_state  
inner join xe_user_details as user on user.user_id = build.build_creator 
where (state.id=3 OR state.id=4) and build_id IN
(select build_id From xebm_assign_build where user_id =userId) and r1.release_active=1 ORDER BY build.build_updatedate DESC LIMIT manualStartValue, pageSize;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;

SELECT build_id,build_name,build_desc,build_updatedate,a1.status as buildStatus,
r1.release_name as releaseName,build_createdate,state.desc as buildState,build_state,
Concat(user.first_name ,' ', user.last_name) as creator,user.user_photo FROM xe_build as build 
INNER JOIN xe_active as a1 ON a1.active_id = build.build_status 
INNER JOIN xe_release as r1 on r1.release_id = build.release_id 
inner join xebm_build_state as state on state.id=build.build_state 
inner join xe_user_details as user on user.user_id = build.build_creator 
where (state.id=3 OR state.id=4) and build_id 
IN(select build_id From xebm_assign_build where user_id =userId) 
and build.build_status = 1  and r1.release_active=1 
ORDER BY build.build_updatedate DESC LIMIT manualStartValue, pageSize;

SELECT count(*) INTO manualbuildcount FROM xe_build as build 
INNER JOIN xe_active as a1 ON a1.active_id = build.build_status 
INNER JOIN xe_release as r1 on r1.release_id = build.release_id 
inner join xebm_build_state as state on state.id=build.build_state 
inner join xe_user_details as user on user.user_id = build.build_creator 
where (state.id=3 OR state.id=4) and build_id 
IN(select build_id From xebm_assign_build where user_id =userId) 
and build.build_status = 1  and r1.release_active=1 ;

SELECT build_id,build_name,build_desc,a1.status as buildStatus,build_status,r1.release_name as releaseName,
build_createdate,state.desc as buildState, build_state,
Concat(user.first_name ,' ', user.last_name) as creator ,
build.build_creator as creatorID,user.about_me,user.user_photo,build_createdate
FROM xe_automation_build as build INNER JOIN xe_active as a1 ON a1.active_id = build.build_status 
INNER JOIN xe_release as r1 on r1.release_id = build.release_id
inner join xebm_build_state as state on state.id=build.build_state
inner join xe_user_details as user on user.user_id = build.build_creator 
where r1.release_active=1 and build.build_state = 2 and build.build_creator=userId 
ORDER BY build.build_updatedate DESC LIMIT autoStartValue, pageSize;

SELECT count(*) INTO autobuildcount
FROM xe_automation_build as build INNER JOIN xe_active as a1 ON a1.active_id = build.build_status 
INNER JOIN xe_release as r1 on r1.release_id = build.release_id
inner join xebm_build_state as state on state.id=build.build_state
inner join xe_user_details as user on user.user_id = build.build_creator 
where r1.release_active=1 and build.build_state = 2 and build.build_creator=userId ;

OPEN buildIdList;
	startLoop: LOOP
		FETCH buildIdList INTO buildId;
		IF finished = 1 THEN
		LEAVE startLoop;
		END IF;
			SELECT xab.build_id,xud.user_id,xud.first_name,xud.last_name FROM xebm_assign_build xab,xe_user_details xud 
			where xab.user_id = xud.user_id and build_id = buildId group by user_id;
		END LOOP startLoop;


CLOSE buildIdList;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getDocumentLibraryData`(IN projectIdToUpload INT, IN userId INT)
BEGIN

DECLARE projectId INT DEFAULT 0;
DECLARE projectName VARCHAR(45);
SELECT project_id INTO projectId FROM xe_project as project INNER JOIN xe_active as active on active.active_id=project.project_active 
WHERE project.project_id in (SELECT project_id FROM xe_user_project WHERE user = userId) LIMIT 1;
SELECT project_name INTO projectName FROM xe_project as project INNER JOIN xe_active as active on active.active_id=project.project_active
WHERE project.project_id in (SELECT project_id FROM xe_user_project WHERE user = userId) LIMIT 1;

SELECT project_id,project_name
FROM xe_project as project
WHERE project.project_active = 1 and
project.project_id in (SELECT project_id FROM xe_user_project WHERE user = userId);
    
	IF projectIdToUpload <> 0 THEN
		SET projectId:=projectIdToUpload;
        SELECT project_name INTO projectName FROM xe_project as project INNER JOIN xe_active as active on active.active_id=project.project_active and project_id=projectIdToUpload LIMIT 1;
	END IF;
    
    SELECT projectId,projectName;
    
    
    IF projectId <> 0 THEN
		SELECT xd.xe_doc_library_id,xd.doc_title,xd.doc_type,xd.upload_time,
        CONCAT(xu.first_name, ' ',xu.last_name) uploaded_by,xu.user_photo ,xd.project_id 
		FROM xe_doc_library xd,xe_user_details xu 
        where xd.project_id=projectId and xd.uploaded_by = xu.user_id;
	END IF;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getAssignModuleData`(IN projectId INT,IN moduleId INT)
BEGIN

SELECT userPro.user as user_id,user.first_name,user.last_name,user.email_id,user.user_photo 
FROM xe_user_project as userPro inner join xe_user_details as user on user.user_id = userPro.user 
WHERE userPro.project_id = projectId and user.user_status!=3 and  userPro.user in 
(SELECT module.user_id FROM xe_user_module as module WHERE module_id = moduleId);

SELECT userPro.user as user_id,user.first_name,user.last_name,user.email_id,user.user_photo 
FROM xe_user_project as userPro inner join xe_user_details as user on user.user_id = userPro.user 
WHERE userPro.project_id = projectId and user.user_status!=3 and  userPro.user not in 
(SELECT module.user_id FROM xe_user_module as module WHERE module_id = moduleId);

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `tmDashboard`(IN currentUser int)
BEGIN
SELECT * FROM (SELECT project_id,project_name, 
(SELECT count(*) from xetm_testcase WHERE xetm_testcase.projectid=xe_project.project_id  and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1)) as test_count, 
 (SELECT count(*) from xe_module WHERE xe_module.project_id=xe_project.project_id and xe_module.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xe_module.module_id) and xe_module.module_active = 1) as module_count, 
 (SELECT count(*) from xetm_scenario WHERE module_id IN (select module_id from xe_module where xe_module.project_id=xe_project.project_id) 
     and xetm_scenario.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_scenario.module_id)
     and xetm_scenario.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_scenario.module_id and xe_module.module_active = 1))as sce_count from xe_project where project_active=1 
     and project_id IN(select project_id from xe_user_project where xe_user_project.user = currentUser
     and xe_user_project.project_id =  project_id)) AS C ;
     
SELECT YEAR(created_date) AS year,MONTH(created_date) as month,CONCAT(SUBSTRING(MONTHNAME(created_date), 1, 3),' ',YEAR(created_date))  AS monthyear,projectid as project,
COUNT(DISTINCT testcase_id) as count 
FROM xetm_testcase where YEAR(created_date)=YEAR(CURDATE()) and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1)
GROUP BY project,year, month ORDER BY month; 
 
SELECT project_id,project_name, project_createdate
FROM xe_project where project_active=1 
     and project_id IN(select project_id from xe_user_project where xe_user_project.user = currentUser
     and xe_user_project.project_id =  project_id);  
     
SELECT * FROM xe_module where xe_module.project_id IN (select project_id from xe_user_project where xe_user_project.user = currentUser
     and xe_user_project.project_id =  xe_module.project_id ) and  xe_module.project_id 
    IN (select project_id from xe_project where xe_project.project_active = 1
     and xe_project.project_id =  xe_module.project_id ) and xe_module.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xe_module.module_id) and xe_module.module_active = 1;
     
SELECT * FROM xetm_scenario  where module_id IN (select module_id from xe_module where project_id IN (select project_id from xe_project where xe_project.project_id = project_id
     and xe_project.project_active = 1)  and project_id 
     IN (select project_id from xe_user_project where xe_user_project.user = currentUser
     and xe_user_project.project_id =  project_id )) and xetm_scenario.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_scenario.module_id)
     and xetm_scenario.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_scenario.module_id and xe_module.module_active = 1);
     
SELECT testcase_id,testcase_name,(select status from xetm_testcase_status where tc_status_id =xetm_testcase.status)as status,precondition,created_date,test_prefix,scenario_id  FROM xetm_testcase where xetm_testcase.projectid IN (select project_id from xe_user_project where xe_user_project.user = currentUser
     and xe_user_project.project_id =  xetm_testcase.projectid ) and  xetm_testcase.projectid 
     IN (select project_id from xe_project where xe_project.project_active = 1
     and xe_project.project_id =  xetm_testcase.projectid ) and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1);

SELECT testcase_id,testcase_name,(select status from xetm_testcase_status where tc_status_id =xetm_testcase.status)as status,precondition,created_date,test_prefix  FROM xetm_testcase where xetm_testcase.projectid IN (select project_id from xe_user_project where xe_user_project.user = currentUser
     and xe_user_project.project_id =  xetm_testcase.projectid ) and  xetm_testcase.projectid 
     IN (select project_id from xe_project where xe_project.project_active = 1
     and xe_project.project_id =  xetm_testcase.projectid ) and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1)
     and xetm_testcase.status=1;
	 
SELECT testcase_id,testcase_name,(select status from xetm_testcase_status where tc_status_id =xetm_testcase.status)as status,precondition,created_date,test_prefix  FROM xetm_testcase where xetm_testcase.projectid IN (select project_id from xe_user_project where xe_user_project.user = currentUser
     and xe_user_project.project_id =  xetm_testcase.projectid ) and  xetm_testcase.projectid 
     IN (select project_id from xe_project where xe_project.project_active = 1
     and xe_project.project_id =  xetm_testcase.projectid ) and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1)
     and xetm_testcase.status=2;
     
SELECT testcase_id,testcase_name,(select status from xetm_testcase_status where tc_status_id =xetm_testcase.status)as status,precondition,created_date,test_prefix  FROM xetm_testcase where xetm_testcase.projectid IN (select project_id from xe_user_project where xe_user_project.user = currentUser
     and xe_user_project.project_id =  xetm_testcase.projectid ) and  xetm_testcase.projectid 
     IN (select project_id from xe_project where xe_project.project_active = 1
     and xe_project.project_id =  xetm_testcase.projectid ) and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1)
     and xetm_testcase.status=3;
     
SELECT testcase_id,testcase_name,(select status from xetm_testcase_status where tc_status_id =xetm_testcase.status)as status,precondition,created_date,test_prefix  FROM xetm_testcase where xetm_testcase.projectid IN (select project_id from xe_user_project where xe_user_project.user = currentUser
     and xe_user_project.project_id =  xetm_testcase.projectid ) and  xetm_testcase.projectid 
     IN (select project_id from xe_project where xe_project.project_active = 1
     and xe_project.project_id =  xetm_testcase.projectid ) and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1)
     and xetm_testcase.status=7;
     
SELECT testcase_id,testcase_name,(select status from xetm_testcase_status where tc_status_id =xetm_testcase.status)as status,precondition,created_date,test_prefix  FROM xetm_testcase where xetm_testcase.projectid IN (select project_id from xe_user_project where xe_user_project.user = currentUser
     and xe_user_project.project_id =  xetm_testcase.projectid ) and  xetm_testcase.projectid 
     IN (select project_id from xe_project where xe_project.project_active = 1
     and xe_project.project_id =  xetm_testcase.projectid ) and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1)
     and xetm_testcase.status=8;     
     
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `buildManagerData`(IN userId INT(11))
BEGIN
	
 SELECT * FROM xe_release WHERE release_active=1;
 
 SELECT build_id, build_name, build_desc, build_status, build_createdate ,(SELECT concat(first_name,' ',last_name) as creator FROM xe_user_details WHERE user_id=build_creator) as creator FROM xe_build WHERE build_status=1 AND release_id IN(SELECT release_id FROM xe_release WHERE release_active=1);
 
 SELECT bug_id, bug_title, bug_desc, create_date,bug_prefix,
(select build_name from xe_build where build_id IN(SELECT build_id FROM xe_step_execution_bug where xe_step_execution_bug.bug_id=xebt_bug.bug_id))as build_name,
 (SELECT concat(first_name,' ',last_name) as creator FROM xe_user_details WHERE user_id=created_by) as creator FROM xebt_bug WHERE bug_id IN
 (SELECT bug_id FROM xe_step_execution_bug where build_id in 
 (SELECT build_id FROM xe_build where build_status=1 AND release_id IN(SELECT release_id FROM xe_release WHERE release_active=1)) and bug_id in 
 (SELECT bug_id FROM xebt_bugsummary where bs_id IN (SELECT max(bs_id) FROM xebt_bugsummary GROUP BY bug_id) and bug_status IN (1,2,3,4))  
 group by build_id) and bug_id IN (SELECT distinct bug_id FROM xebt_bugsummary where bug_status IN (1,2,3,4));

SELECT * FROM xetm_testcase AS A
RIGHT OUTER JOIN 
(SELECT build_id, tc_id FROM xe_testcase_execution_summary WHERE complete_status=2 and build_id in (SELECT build_id FROM xe_build where build_state IN (3,4) AND release_id IN(SELECT release_id FROM xe_release WHERE release_active=1))) AS B ON A.testcase_id=B.tc_id
LEFT OUTER JOIN 
(SELECT build_name,build_id FROM xe_build ) AS C ON C.build_id=B.build_id;

SELECT T.test_prefix,B.build_name,T.testcase_name,T.created_date,BT.build_id,BT.project_id,BT.module_id,BT.scenario_id,T.testcase_id,T.summary,T.precondition FROM xetm_testcase T, xe_build B,xebm_buildtestcase BT where T.testcase_id=BT.testcase_id and B.build_id=BT.build_id and B.build_status=1 and BT.project_id IN (SELECT DISTINCT(project_id) FROM xe_user_project WHERE user=userId) ORDER BY build_id;

SELECT * FROM xe_testcase_execution_summary where build_id IN (SELECT build_id FROM xe_build where build_status=1) and complete_status=1;

select xe_build.build_id , xe_release.release_id from xe_build, xe_release where xe_build.release_id = xe_release.release_id and xe_build.release_id IN (select xe_release.release_id from xe_release);

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `tcBugAnalysis`(IN userId int)
BEGIN
	SELECT uPro.project_id,pro.project_name FROM xe_user_project as uPro,xe_project as pro WHERE pro.project_id = uPro.project_id and pro.project_active = 1 and uPro.user = userId ;
	SELECT uMod.module_id,module.module_name,uMod.project_id FROM xe_user_module as uMod,xe_module as module  where module.module_id=uMod.module_id and module.module_active = 1 and uMod.user_id = userId;
	SELECT status_id,bug_status FROM xebt_status where bug_state = 1;
	SELECT priority_id,bug_priority FROM xebt_priority WHERE priority_status = 1;
    SELECT severity_id,bug_severity FROM xebt_severity WHERE severity_status = 1;
	SELECT query_id,query_name,query_variables FROM xebm_customize_query WHERE user_id = userId ORDER BY query_id DESC;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `tmProjectDashboard`(IN currentProject int,IN currentUser int)
BEGIN
SELECT * FROM (SELECT project_id,project_name, 
(SELECT count(*) from xetm_testcase WHERE xetm_testcase.projectid=xe_project.project_id  and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1)) as test_count, 
 (SELECT count(*) from xe_module WHERE xe_module.project_id=xe_project.project_id and xe_module.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xe_module.module_id) and xe_module.module_active = 1) as module_count, 
 (SELECT count(*) from xetm_scenario WHERE module_id IN (select module_id from xe_module where xe_module.project_id=xe_project.project_id) 
     and xetm_scenario.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_scenario.module_id)
     and xetm_scenario.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_scenario.module_id and xe_module.module_active = 1))as sce_count from xe_project where project_active=1 
     and project_id IN(select project_id from xe_user_project where xe_user_project.user = currentUser
     and xe_user_project.project_id =  project_id)) AS C ;
SELECT YEAR(created_date) AS year, MONTH(created_date) AS month,projectid as project,
COUNT(DISTINCT testcase_id) as count 
FROM xetm_testcase where YEAR(created_date)=YEAR(CURDATE()) and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1)
GROUP BY project,year, month;  
SELECT project_id,project_name,project_description,project_createdate
FROM xe_project where project_active=1 
     and project_id IN(select project_id from xe_user_project where xe_user_project.user = currentUser
     and xe_user_project.project_id =  project_id);
SELECT * FROM xe_module where project_id = currentProject and xe_module.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xe_module.module_id) and xe_module.module_active = 1;
SELECT * FROM xetm_scenario where module_id IN(select module_id from xe_module where project_id = currentProject) and xetm_scenario.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_scenario.module_id)
     and xetm_scenario.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_scenario.module_id and xe_module.module_active = 1);
SELECT testcase_id,testcase_name,(select status from xetm_testcase_status where tc_status_id =xetm_testcase.status)as status,precondition,created_date,test_prefix,scenario_id,execution_type FROM xetm_testcase where projectid = currentProject and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1);

  select * from (select summary.bs_id,summary.bug_id,bugDetails.bug_prefix,summary.bug_status,bugStatus.bug_status bugStatus,executeBugs.test_case_id,testCase.test_prefix,testCase.testcase_name, executeBugs.build_id,build.build_name
  from xe_step_execution_bug executeBugs,xebt_bugsummary summary,xetm_testcase testCase,xe_build build,xebt_status bugStatus,xebt_bug bugDetails
   where executeBugs.bug_id= summary.bug_id and testCase.testcase_id = executeBugs.test_case_id and build.build_id = executeBugs.build_id 
   and summary.bug_status = bugStatus.status_id 
   and summary.bug_id = bugDetails.bug_id
   and summary.bs_id in (SELECT max(bs_id) FROM xebt_bugsummary as sm WHERE sm.bug_id = bugDetails.bug_id)
   and summary.bug_status not in (5)
   and executeBugs.bug_id=bugDetails.bug_id  and testCase.projectid = currentProject 
   and bugDetails.module 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser
     and xe_user_module.module_id = bugDetails.module)
     and bugDetails.module 
     IN(select module_id from xe_module where xe_module.module_id = bugDetails.module and xe_module.module_active = 1)
    order by bs_id desc) as BugDetails group by bug_id;
    
SELECT count(*) module_wise_bug_count, bug.module as module_id, mo.module_name
FROM xebt_bug as bug, xe_module as mo, xebt_bugsummary as  summary 
WHERE bug.project = currentProject and bug.bug_id in(SELECT bug_id FROM xe_step_execution_bug)
and summary.bug_id = bug.bug_id
and summary.bs_id in (SELECT max(bs_id) FROM xebt_bugsummary as sm WHERE sm.bug_id = bug.bug_id)
and summary.bug_status not in (5)
and bug.module = mo.module_id
and mo.module_active = 1
and bug.module in (select module_id from xe_user_module as uMod where uMod.user_id = currentUser and bug.module = uMod.module_id)
group by module; 

SELECT module.module_id, module.module_name 
FROM xe_module as module
WHERE module.project_id = currentProject and module.module_active = 1
and module.module_id in (select module_id from xe_user_module as uMod where uMod.user_id = currentUser)
ORDER BY module.module_id ASC;

SELECT tc.module_id, module.module_name, count(tc.module_id) as tcCount, MONTH(tc.created_date) AS month
FROM xetm_testcase as tc, xe_module as module
WHERE tc.projectid = currentProject and tc.module_id = module.module_id
and tc.module_id in (select module_id from xe_user_module as uMod where uMod.user_id = currentUser and uMod.module_id = tc.module_id)
and module.module_active = 1
GROUP BY tc.module_id, month
ORDER BY month;

SELECT testcase_id,testcase_name,(select status from xetm_testcase_status where tc_status_id =xetm_testcase.status)as status,precondition,created_date,test_prefix  FROM xetm_testcase where xetm_testcase.projectid IN (select project_id from xe_user_project where xe_user_project.user = currentUser and xe_user_project.project_id = currentProject
     and xe_user_project.project_id =  xetm_testcase.projectid ) and  xetm_testcase.projectid 
     IN (select project_id from xe_project where xe_project.project_active = 1
     and xe_project.project_id =  xetm_testcase.projectid ) and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser and xe_user_module.project_id = currentProject
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1)
     and xetm_testcase.status=1;
     
SELECT testcase_id,testcase_name,(select status from xetm_testcase_status where tc_status_id =xetm_testcase.status)as status,precondition,created_date,test_prefix  FROM xetm_testcase where xetm_testcase.projectid IN (select project_id from xe_user_project where xe_user_project.user = currentUser and xe_user_project.project_id = currentProject
     and xe_user_project.project_id =  xetm_testcase.projectid ) and  xetm_testcase.projectid 
     IN (select project_id from xe_project where xe_project.project_active = 1
     and xe_project.project_id =  xetm_testcase.projectid ) and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser and xe_user_module.project_id = currentProject
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1)
     and xetm_testcase.status=2;
     
SELECT testcase_id,testcase_name,(select status from xetm_testcase_status where tc_status_id =xetm_testcase.status)as status,precondition,created_date,test_prefix  FROM xetm_testcase where xetm_testcase.projectid IN (select project_id from xe_user_project where xe_user_project.user = currentUser and xe_user_project.project_id = currentProject
     and xe_user_project.project_id =  xetm_testcase.projectid ) and  xetm_testcase.projectid 
     IN (select project_id from xe_project where xe_project.project_active = 1
     and xe_project.project_id =  xetm_testcase.projectid ) and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser and xe_user_module.project_id = currentProject
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1)
     and xetm_testcase.status=3;
     
SELECT testcase_id,testcase_name,(select status from xetm_testcase_status where tc_status_id =xetm_testcase.status)as status,precondition,created_date,test_prefix  FROM xetm_testcase where xetm_testcase.projectid IN (select project_id from xe_user_project where xe_user_project.user = currentUser and xe_user_project.project_id = currentProject
     and xe_user_project.project_id =  xetm_testcase.projectid ) and  xetm_testcase.projectid 
     IN (select project_id from xe_project where xe_project.project_active = 1
     and xe_project.project_id =  xetm_testcase.projectid ) and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser and xe_user_module.project_id = currentProject
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1)
     and xetm_testcase.status=7;
     
SELECT testcase_id,testcase_name,(select status from xetm_testcase_status where tc_status_id =xetm_testcase.status)as status,precondition,created_date,test_prefix  FROM xetm_testcase where xetm_testcase.projectid IN (select project_id from xe_user_project where xe_user_project.user = currentUser and xe_user_project.project_id = currentProject
     and xe_user_project.project_id =  xetm_testcase.projectid ) and  xetm_testcase.projectid 
     IN (select project_id from xe_project where xe_project.project_active = 1
     and xe_project.project_id =  xetm_testcase.projectid ) and xetm_testcase.module_id 
     IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser and xe_user_module.project_id = currentProject
     and xe_user_module.module_id = xetm_testcase.module_id)
     and xetm_testcase.module_id 
     IN(select module_id from xe_module where xe_module.module_id = xetm_testcase.module_id and xe_module.module_active = 1)
     and xetm_testcase.status=8;

SELECT module_id, module_name FROM xe_module
	WHERE xe_module.module_id IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser and xe_user_module.module_id = xe_module.module_id) 
	and project_id = currentProject and module_active =1 ORDER BY module_id DESC LIMIT 5; 

SELECT count( *) as countOfScenario, module_id FROM xetm_scenario
	WHERE xetm_scenario.module_id IN (select module_id from xe_user_module where xe_user_module.user_id = currentUser and xe_user_module.module_id = xetm_scenario.module_id)
	and xetm_scenario.module_id IN (select module_id from xe_module where xe_module.module_id = xetm_scenario.module_id and xe_module.module_active = 1 and project_id = currentProject)
	group by module_id ORDER BY module_id DESC LIMIT 5; 
	
SELECT component_id, component_name FROM xe_components WHERE component_active = 1;
	
SELECT COUNT(*) AS scenario_count, component_id FROM xe_scenario_components 
WHERE component_id IN (SELECT component_id FROM xe_components WHERE component_active = 1)
and scenario_id IN (SELECT scenario_id FROM xetm_scenario WHERE status = 1 and module_id IN (SELECT module_id FROM xe_module
	WHERE xe_module.module_id IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser and xe_user_module.module_id = xe_module.module_id) 
	and project_id = currentProject and module_active =1)) 
GROUP BY component_id;

SELECT scenario_id, component_id FROM xe_scenario_components 
WHERE component_id IN (SELECT component_id FROM xe_components WHERE component_active = 1)
and scenario_id IN (SELECT scenario_id FROM xetm_scenario WHERE status = 1 and module_id IN (SELECT module_id FROM xe_module
	WHERE xe_module.module_id IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser and xe_user_module.module_id = xe_module.module_id) 
	and project_id = currentProject and module_active =1));  

SELECT count(*) as test_case_count , execution_type  FROM xetm_testcase where projectid = currentProject
 and module_id IN (SELECT module_id FROM xe_module
	WHERE xe_module.module_id IN(select module_id from xe_user_module where xe_user_module.user_id = currentUser and xe_user_module.module_id = xe_module.module_id) 
	and project_id = currentProject and module_active =1) group by execution_type;	

END;;
    
CREATE DEFINER=`xenon`@`localhost` PROCEDURE `bugAnalysis`(IN userId int)
BEGIN
SELECT release_id,release_name FROM xe_release WHERE release_active = 1;
	SELECT build_id,build_name,release_id FROM xe_build WHERE build_status = 1;
	SELECT status_id,bug_status FROM xebt_status where bug_state = 1;
	SELECT priority_id,bug_priority FROM xebt_priority WHERE priority_status = 1;
    SELECT severity_id,bug_severity FROM xebt_severity WHERE severity_status = 1;
	SELECT query_id,query_name,query_variables FROM xe_bug_customize_query WHERE user_id = userId ORDER BY query_id DESC;
	SELECT execution_type_id,description FROM xe_build_execution_type WHERE status=1;
	SELECT build_id,build_name,release_id FROM xe_automation_build WHERE build_status = 1;
END;;
CREATE DEFINER=`xenon`@`localhost` PROCEDURE `createbuild`()
BEGIN
 SELECT release_id,release_name FROM `xe_release` WHERE release_active=1;
 SELECT * FROM `xe_build_execution_type` WHERE status=1;
 SELECT * FROM xesfdc_env where env_status=1;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `mailgroup`()
BEGIN
 SELECT * from xe_mail_group;
 SELECT distinct mgd.user_id,mgd.mail_group_id,ud.email_id,Concat(ud.first_name,' ',ud.last_name)user_name,(select role_type from xe_role where role_id= ud.role_id)user_role,ud.role_id,ud.user_photo FROM xe_mail_group_details mgd,xe_mail_group mg, xe_user_details ud where mgd.user_id = ud.user_id ;
 SELECT user_id,Concat(first_name,' ',last_name)user_name from xe_user_details where user_status = 1;
END;;


CREATE DEFINER=`xenon`@`localhost` PROCEDURE `executeAutomationBuild`(IN custId INT,IN buildIdTemp INT,IN userIdTemp INT,IN projectIdTemp INT)
BEGIN
DECLARE AddedTcCount INT;
DECLARE onPremiseStatus INT;
SELECT count(*) AS TcCount INTO AddedTcCount FROM xebm_automation_build_testcase where build_id=buildIdTemp;

SELECT count(*) AS TcCount FROM xebm_automation_build_testcase where build_id=buildIdTemp;

SELECT * FROM xe_project WHERE project_id in (SELECT project_id FROM xe_user_project WHERE user=userIdTemp);

IF projectIdTemp>0  ||  AddedTcCount!=0 THEN
SELECT distinct project_id INTO projectIdTemp FROM xebm_automation_build_testcase where build_id=buildIdTemp;

	SELECT * FROM 
	(SELECT project_id, project_name, project_description, project_active, prj_mdl.module_id, module_name, module_description, scenario_id, scenario_name, scenario_description  FROM 
			(SELECT prj.project_id, project_name, project_description, project_active, module_id, module_name, module_description FROM 
				(SELECT * FROM xe_project WHERE project_id in (SELECT project_id FROM xe_user_project WHERE user=userIdTemp) AND project_id IN (projectIdTemp) AND project_active=1) AS prj
				LEFT OUTER JOIN 
				(SELECT * FROM xe_module WHERE module_id IN (SELECT module_id FROM xe_user_module WHERE user_id=userIdTemp) AND module_active=1) AS mdl ON mdl.project_id=prj.project_id) AS prj_mdl
					LEFT OUTER JOIN 
					(SELECT * FROM xetm_scenario WHERE status=1) AS sce ON sce.module_id=prj_mdl.module_id) AS prj_mdl_sce
						LEFT OUTER JOIN
                        (SELECT * FROM 
                        (SELECT * FROM 
							(SELECT test_case.testcase_id, testcase_name, execution_type,scenario_id as sce_id, projectid,test_complete,module_id as mdl_id,excel_file_present_status FROM xetm_testcase AS test_case 
								LEFT OUTER JOIN 
								(SELECT 'test_complete',testcase_id FROM xebm_automation_build_testcase WHERE build_id=buildIdTemp)AS build_test ON build_test.testcase_id=test_case.testcase_id) AS test_build_status
                                
                                LEFT OUTER JOIN 
								(SELECT tc_id AS testDsId,testcase_id AS tcDsId,datasheet_title AS datasheetTitle  FROM xebm_automation_build_testcase WHERE build_id=buildIdTemp)AS build_test_ds ON build_test_ds.tcDsId=test_build_status.testcase_id) AS test_build_status_ds
                                
                                
									LEFT OUTER JOIN 
									(SELECT 'test_executed',tc_id FROM xe_testcase_execution_summary WHERE build_id=buildIdTemp group by tc_id) AS test_exe ON test_exe.tc_id=test_build_status_ds.testcase_id) AS test_case 
									 ON prj_mdl_sce.scenario_id=test_case.sce_id and test_case.execution_type IN(2,3);
                             
	SELECT * FROM xe_mail_group;
	SELECT on_premise INTO onPremiseStatus FROM `xenon-core`.xe_customer_details where customer_id=custId;
    IF onPremiseStatus=1 THEN
    	SELECT * FROM `xenonvm`.xevm_customervm_details where is_free = 1 and status=1 and customer_id = custId;
    END IF;
    IF onPremiseStatus=2 THEN
		SELECT * FROM `xenonvm`.xevm_cloudevm_details where is_free = 1 and status=1 LIMIT 1;
	END IF;
	IF onPremiseStatus=3 THEN
		SELECT * FROM `xenonvm`.xevm_customervm_details where is_free = 1 and status=1 and customer_id = custId;
        SELECT * FROM `xenonvm`.xevm_cloudevm_details where is_free = 1 and status=1 LIMIT 1;
	END IF;
    SELECT count(*) AS execCount,cust_vm_id FROM xebm_vm_details where build_id NOT IN (SELECT build_id FROM xe_automation_build where build_state=5) GROUP BY cust_vm_id;
    
    SELECT * FROM xebm_environment_details;

	SELECT * from xead_git_project_details xgpd,xead_git_repositories xgp,xead_user_git_instance xugi WHERE xgpd.git_instance_id=xugi.instance_id AND xgpd.git_instance_id=xgp.project_id and xugi.user_id =userIdTemp;
END IF;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `addAutomationTestCase`(IN testArray VARCHAR(255),IN buildId int,IN userId int)
BEGIN
  DECLARE strLen    INT DEFAULT 0;
  DECLARE SubStrLen INT DEFAULT 0;
  DECLARE testCaseId INT DEFAULT 0;
  DECLARE projectId INT DEFAULT 0;
  DECLARE moduleId INT DEFAULT 0;
  DECLARE sceId INT DEFAULT 0;
  DECLARE buildName TEXT DEFAULT null;
  DECLARE userName TEXT DEFAULT null;
  DECLARE activity_desc TEXT DEFAULT null;
  DECLARE TC_COUNT INT;

  IF testArray IS NULL THEN
    SET testArray = '';
  END IF;

do_this:
  LOOP
    SET strLen = CHAR_LENGTH(testArray);
    SET testCaseId = SUBSTRING_INDEX(testArray, ',', 1);
		 SELECT @projectId:=M.project_id,@moduleId:=S.module_id,@sceId:=T.scenario_id FROM xe_module M,xetm_scenario S,xetm_testcase T where T.testcase_id=testCaseId and T.scenario_id=S.scenario_id and M.module_id=S.module_id;
         
		 SELECT count(*) INTO TC_COUNT FROM xebm_automation_build_testcase where xebm_automation_build_testcase.testcase_id=testCaseId and xebm_automation_build_testcase.build_id=buildId;

		IF TC_COUNT=0 THEN

		INSERT INTO xebm_automation_build_testcase(`build_id`,`testcase_id`,`project_id`,`scenario_id`,`module_id`) VALUES(buildId,testCaseId,@projectId,@sceId,@moduleId);
			
		END IF;	
		 UPDATE xe_automation_build SET build_state=2 WHERE build_id=buildId;
    SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(testArray, ',', 1)) + 2;
    SET testArray = MID(testArray, SubStrLen, strLen);
    IF testArray = '' THEN
      LEAVE do_this;
    END IF;
  END LOOP do_this;

  
          SELECT @buildName:=build_name,@userName:=user_name FROM (SELECT build_name FROM xe_automation_build where build_id=buildId) AS build 
          JOIN (SELECT CONCAT(first_name,' ', last_name) AS user_name FROM xe_user_details WHERE user_id=userId) AS user;
          
		  SET activity_desc=CONCAT('Test cases are added to build: <br/><strong> Build name: </strong> ',@buildName,'<br/> <strong> Updated By: </strong> ',@userName);
          
          INSERT INTO xe_recent_activities (activity, activity_desc,module,activity_date,user,activity_status) 
          VALUES (10,activity_desc,'BM',NOW(),userId,1);
  
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `dropAutomationTestCase`(IN testArray VARCHAR(255),IN buildId int)
BEGIN
  DECLARE strLen    INT DEFAULT 0;
  DECLARE SubStrLen INT DEFAULT 0;
  DECLARE testCaseId INT DEFAULT 0;


  IF testArray IS NULL THEN
    SET testArray = '';
  END IF;

do_this:
  LOOP
    SET strLen = CHAR_LENGTH(testArray);
    SET testCaseId = SUBSTRING_INDEX(testArray, ',', 1);
		DELETE FROM xebm_automation_build_testcase WHERE testcase_id=testCaseId and build_id=buildId;
    SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(testArray, ',', 1)) + 2;
    SET testArray = MID(testArray, SubStrLen, strLen);
    IF testArray = '' THEN
      LEAVE do_this;
    END IF;
  END LOOP do_this;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `admindashboard`(IN startValue int, IN offsetValue int,OUT count int,IN schemaName VARCHAR(45),IN typeId INT,IN custId INT)
BEGIN

SELECT user_id,concat(first_name,' ',last_name)user_name,email_id,user_status,role_id,(select role_type From xe_role where role_id = xe_user_details.role_id)user_role FROM xe_user_details where user_status!=3 order by create_date desc limit startValue,offsetValue;

SELECT count(*) as proCount FROM xe_project;

Select * from (
(SELECT COALESCE(count(*),0) as vm_count FROM `xenonvm`.xevm_customervm_details where customer_id= custId) as vm_count,
(SELECT round((sum(data_length + index_length) / 1024 / 1024), 2) 'Used_Space' 
 FROM information_schema.TABLES WHERE table_schema=schemaName GROUP BY table_schema) as used_space,
 (SELECT max_cloud_space from `xenon-core`.xe_licence_details where type_id = typeId) as max_cloud_space);
 
 select count(*) INTO count from xe_user_details where user_status!=3 ;
 END;;
 
CREATE DEFINER=`root`@`localhost` PROCEDURE `automationSummary`(IN buildId INT,IN userId INT)
BEGIN

                SELECT count(*) TotalTcCount,project_id FROM xebm_automation_build_testcase where build_id=buildId GROUP BY project_id;

                SELECT project,project.project_name,project.project_description,project.project_createdate,project.project_active,count(*) ExecutedTcCount,
                SUM(case when testcase_status = 1 then 1 else 0 end) as PassCount,
    SUM(case when testcase_status = 2 then 1 else 0 end) as FailCount,SUM(case when testcase_status = 3 then 1 else 0 end) as SkipCount,
    SUM(case when testcase_status = 4 then 1 else 0 end) as BlockCount 
                FROM xebm_automation_execution_summary as trialSummary,xe_project as project
                where trialSummary.build=buildId and trialSummary.project=project.project_id
                and trialSummary.project in (SELECT project_id FROM xe_user_project where user = userId)
                GROUP BY trialSummary.project;

                SELECT count(*) TotalTcCount,module_id FROM xebm_automation_build_testcase where build_id=buildId GROUP BY module_id;

                SELECT project,module,module.module_name,count(*) ExecutedTcCount,SUM(case when testcase_status = 1 then 1 else 0 end) as PassCount,
    SUM(case when testcase_status = 2 then 1 else 0 end) as FailCount,SUM(case when testcase_status = 3 then 1 else 0 end) as SkipCount,
    SUM(case when testcase_status = 4 then 1 else 0 end) as BlockCount 
                FROM xebm_automation_execution_summary as trialSummary,xe_module as module
                where trialSummary.build=buildId and trialSummary.module=module.module_id
                and trialSummary.module in (SELECT module_id FROM xe_user_module where user_id = userId)
                GROUP BY trialSummary.module; 

                SELECT count(*) TotalTcCount,scenario_id FROM xebm_automation_build_testcase where build_id=buildId  GROUP BY scenario_id;

                SELECT project,module,scenario,scenario.scenario_name,count(*) ExecutedTcCount,SUM(case when testcase_status = 1 then 1 else 0 end) as PassCount,
    SUM(case when testcase_status = 2 then 1 else 0 end) as FailCount,SUM(case when testcase_status = 3 then 1 else 0 end) as SkipCount,
    SUM(case when testcase_status = 4 then 1 else 0 end) as BlockCount 
                FROM xebm_automation_execution_summary as trialSummary,xetm_scenario as scenario
                where trialSummary.build=buildId and trialSummary.scenario=scenario.scenario_id
                and trialSummary.module in (SELECT module_id FROM xe_user_module where user_id = userId)
                GROUP BY trialSummary.scenario;
                
                SELECT testcase_id,testcase_name,summary,scenario_id,trialSummary.testcase_status,statusTable.description,trialSummary.tc_start_time,trialSummary.tc_end_time,excel_file_present_status
                FROM xetm_testcase as tc,xebm_automation_execution_summary as trialSummary,xetm_exec_status as statusTable
                where trialSummary.build=buildId
                and testcase_id IN (SELECT testcase_id FROM xebm_automation_build_testcase where build_id=buildId)
                and module_id IN(SELECT module_id FROM xe_user_module where user_id = userId)
                and trialSummary.testcase = tc.testcase_id
                and statusTable.exst_id =trialSummary.testcase_status;

select build_id,trial_step_id,testcase_id,step_id,step_details,step_value,step_status,screenshot,screenshot_title,actual_result,is_bug_raised,statusTable.description
from xebm_automation_steps_execution_summary as trialSummary,xetm_exec_status as statusTable
where build_id = buildId and statusTable.exst_id =trialSummary.step_status;

SELECT step_exe_bug.test_step_id, step_exe_bug.test_case_id, step_exe_bug.build_id,xe_bug.bug_id,xe_bug.bug_prefix FROM xebt_bug xe_bug
JOIN 
(SELECT * FROM xe_automation_step_execution_bug step_bug) AS step_exe_bug ON  xe_bug.bug_id=step_exe_bug.bug_id;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getAutomationBuildsDash`()
BEGIN
DECLARE done INT DEFAULT 0;
DECLARE buildId INT DEFAULT 0;
DECLARE cur CURSOR FOR SELECT build_id FROM xe_automation_build where build_status=1 order by build_id desc limit 5;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

SELECT count(*) TotalTcCount , build_id FROM xebm_automation_build_testcase GROUP BY build_id DESC LIMIT 5;
SELECT count(*) AS ExecutedTcCount,build as build_id FROM xebm_automation_execution_summary GROUP BY build DESC LIMIT 5;

SELECT build_id,build_name,build_createdate FROM xe_automation_build where build_status=1 order by build_id desc limit 5;

	OPEN cur;
	read_loop: LOOP
		FETCH cur INTO buildId; 
        
		IF done THEN
			LEAVE read_loop;  
		END IF;
        
	       select Count(*) as tc_count,build as build_id,(SELECT description FROM xetm_exec_status WHERE exst_id=testcase_status)
			as tc_status FROM xebm_automation_execution_summary where build=buildId AND testcase_status<>5 group by testcase_status
			UNION ALL
			(SELECT (count(*)-(select ifnull(sum(tc_count), 0) from (select Count(*) as tc_count,build,
			(SELECT description FROM xetm_exec_status WHERE exst_id=testcase_status) as tc_status FROM xebm_automation_execution_summary where build=buildId
			 group by testcase_status) src where tc_status<>'Not run')) as not_run,build_id,'Not run' FROM xebm_automation_build_testcase where build_id=buildId);
     
	END LOOP;
	CLOSE cur; 

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertExecutionSteps`(IN testCaseStatus int,IN buildId int,IN testCaseId longtext,
                                            IN stepIdArray longtext,IN stepDetailsArray longtext,
                                            IN stepInputTypeArray longtext,IN stepValueArray longtext,
                                            IN stepStatusArray longtext,IN attachmentPathArray longtext,IN titleArray longtext,
                                            IN bugRaiseArray longtext,IN datasetNo INT(11))
BEGIN

  DECLARE strLenstepId   INT DEFAULT 0;
  DECLARE strLenstepDetails   INT DEFAULT 0;
  DECLARE strLenstepInputType   INT DEFAULT 0;
  DECLARE strLenstepValue   INT DEFAULT 0;
  DECLARE strLenstepStatus   INT DEFAULT 0;
  DECLARE strLenattachmentPath   INT DEFAULT 0;
  DECLARE strLentitle   INT DEFAULT 0;
  DECLARE strLenBug   INT DEFAULT 0;
       
  DECLARE SubStrLen INT DEFAULT 0;
  
  DECLARE stepId INT DEFAULT 0;
  DECLARE stepDetails LONGTEXT DEFAULT 0;
  DECLARE stepInputType LONGTEXT DEFAULT 0;
  DECLARE stepValue LONGTEXT DEFAULT 0;
  DECLARE stepStatus LONGTEXT DEFAULT 0;
  DECLARE attachmentPath LONGTEXT DEFAULT 0;
  DECLARE title LONGTEXT DEFAULT 0;
  DECLARE bugRaise LONGTEXT DEFAULT 0;
  
  DECLARE projectId INT DEFAULT 0;
  DECLARE stepExeId INT DEFAULT 0;
  DECLARE raiseBugId INT DEFAULT 0;
  DECLARE bugTestName LONGTEXT DEFAULT 0;
  DECLARE bugBuildName LONGTEXT DEFAULT 0;
  DECLARE buildExecutor INT DEFAULT 0;
  DECLARE datasetIdTemp INT DEFAULT 0;
   DECLARE datasetIdTemp2 INT DEFAULT 0;
  DECLARE moduleId INT DEFAULT 0;
  DECLARE sceId INT DEFAULT 0;
  DECLARE testStatus INT DEFAULT 0;
  DECLARE exeIdTemp INT DEFAULT 0;
  DECLARE done INT DEFAULT 0;
  DECLARE testCasePass LONGTEXT DEFAULT 0;
   DECLARE testCasePassId INT DEFAULT 0;
  DECLARE testCaseString LONGTEXT DEFAULT 0;
  DECLARE failCaseString LONGTEXT DEFAULT 0;
 
	DECLARE testCaseIdTemp INT default 0;
    DECLARE tempCounter INT default 0;
 
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
  SET testCaseString='';
  
  IF stepIdArray IS NULL THEN
    SET stepIdArray = '';
  END IF;


		SELECT @projectId:=M.project_id,@moduleId:=S.module_id,@sceId:=T.scenario_id,@testCaseIdTemp:=T.testcase_id FROM xe_module M,xetm_scenario S,xetm_testcase T where T.test_prefix=testCaseId and T.scenario_id=S.scenario_id and M.module_id=S.module_id;
    
		Update xebm_automation_execution_summary SET testcase_status=testCaseStatus, tc_end_time=now() where build=buildId 
         and testcase=@testCaseIdTemp ;
		                          
		SELECT @buildExecutor:=user_id FROM xebm_build_exec_details WHERE xebm_build_exec_details.build_id=buildId;
          
  IF datasetNo!=0 THEN
	SELECT exe_id INTO exeIdTemp FROM xebm_automation_execution_summary  where testcase_status=testCaseStatus and build=buildId and testcase=@testCaseIdTemp;
    SELECT exeIdTemp;
    SELECT dataset_id INTO datasetIdTemp FROM xetm_tc_dataset where tc_exe_id=exeIdTemp and dataset_no=datasetNo;
    SELECT datasetIdTemp;
    IF datasetIdTemp=0  THEN
    INSERT INTO `xetm_tc_dataset` (`dataset_id`, `dataset_name`, `status`, `tc_exe_id`,`dataset_no`) VALUES (0,CONCAT('Dataset ',datasetNo,''),testCaseStatus,exeIdTemp,datasetNo);
    END IF;
    	SET datasetNo = (SELECT dataset_id FROM xetm_tc_dataset where tc_exe_id=exeIdTemp and dataset_no=datasetNo);
  END IF;
        
 do_this:
  LOOP
    SET strLenstepId = CHAR_LENGTH(stepIdArray);
    SET strLenstepDetails = CHAR_LENGTH(stepDetailsArray);
    SET strLenstepInputType = CHAR_LENGTH(stepInputTypeArray);
    SET strLenstepValue = CHAR_LENGTH(stepValueArray);
    SET strLenstepStatus = CHAR_LENGTH(stepStatusArray);
    SET strLenattachmentPath = CHAR_LENGTH(attachmentPathArray);
    SET strLentitle = CHAR_LENGTH(titleArray);
    SET strLenBug = CHAR_LENGTH(bugRaiseArray);
                
                SET stepId = SUBSTRING_INDEX(stepIdArray,'|', 1);
                SET stepDetails = SUBSTRING_INDEX(stepDetailsArray,'|', 1);
                SET stepInputType = SUBSTRING_INDEX(stepInputTypeArray,'|', 1);
                SET stepValue = SUBSTRING_INDEX(stepValueArray,'|', 1);
                SET stepStatus = SUBSTRING_INDEX(stepStatusArray,'|', 1);
                SET attachmentPath = SUBSTRING_INDEX(attachmentPathArray,'|', 1);
                SET title = SUBSTRING_INDEX(titleArray,'|', 1);
				SET bugRaise = SUBSTRING_INDEX(bugRaiseArray,'|', 1);
                
                                
         SELECT @testCase:=T.testcase_id,@bugTestName:=testcase_name FROM xetm_testcase T where T.test_prefix=testCaseId;
         
         SELECT @bugBuildName:=build_name FROM xe_automation_build WHERE xe_automation_build.build_id=buildId limit 1;
         
       
         INSERT INTO xebm_automation_steps_execution_summary (testcase_id, step_id, step_details, build_id, step_value, step_status, screenshot, screenshot_title,actual_result,is_bug_raised,dataset_id)
         VALUES (@testCase,stepId,stepDetails,buildId,stepValue,stepStatus,LOAD_FILE(attachmentPath),title,stepInputType,bugRaise,datasetNo);
         
         SELECT @stepExeId:=trial_step_id FROM xebm_automation_steps_execution_summary AS xebm_exe 
         WHERE xebm_exe.testcase_id=@testCase AND xebm_exe.build_id=buildId AND xebm_exe.step_id=stepId AND xebm_exe.is_bug_raised=bugRaise AND xebm_exe.step_status=stepStatus AND dataset_id=datasetNo limit 1;
         
         
                
         IF bugRaise=2 THEN 
             BEGIN
              
              DECLARE cur CURSOR FOR  SELECT CONCAT('Step :',step_id,'. "Detail :" ',step_details,'  "Value :" ',step_value,'  "Actual Result :" ',actual_result,'<br>') 
					    AS step_detail,testcase_id FROM xebm_automation_steps_execution_summary exe_sum  WHERE exe_sum.build_id=buildId AND exe_sum.is_bug_raised=1 AND exe_sum.testcase_id=@testCase group by exe_sum.step_id;
              
               OPEN cur;
                   read_loop: LOOP
                                FETCH cur INTO testCasePass,testCasePassId; 
                                IF done THEN
                                     LEAVE read_loop;  
                                END IF;

                                  SET testCaseString = CONCAT(testCaseString,' ',testCasePass);
                             
                END LOOP;
                CLOSE cur;
                
			   SET testCaseString = CONCAT('Pass Steps :','<br>',testCaseString);
              
                
			   SELECT @failCaseString:=step_detail FROM (SELECT CONCAT('Step :',step_id,'. "Detail :" ',step_details,'  "Value :" ',step_value,'  "Actual Result :" ',actual_result,'<br>') 
					    AS step_detail FROM xebm_automation_steps_execution_summary exe_sum  WHERE exe_sum.build_id=buildId  AND exe_sum.is_bug_raised=2 LiMIT 1) test;
                        
               SET testCaseString = CONCAT(testCaseString,'<br>Fail Steps :','<br>',@failCaseString,'<br>');
               
               
                CALL `setBTPrefixes`(@projectId,Concat('Build :',@bugBuildName,' - ',@bugTestName),SUBSTRING(Concat('Steps :','<br>',testCaseString), 1, 999),@moduleId,NOW(),@buildExecutor,1,2,0,"",NOW(),"","");
                
                SELECT @raiseBugId:=B.bug_id FROM xebt_bug B WHERE B.project=@projectId AND B.module=@moduleId AND B.created_by=@buildExecutor order by bug_id DESC limit 1;
                
                INSERT INTO `xebt_bug_attachment`(`bug_id`,`attach_path`,`attach_title`)
                VALUES (@raiseBugId,LOAD_FILE(attachmentPath),CONCAT(title,'.png'));
                
                UPDATE `xebt_bugsummary` SET `group_assign` = 2 WHERE `bug_id` = @raiseBugId;
                
                INSERT INTO `xebt_bugsummary`(`bug_id`,`bug_status`,`bug_priority`,`assignee`,`update_date`,`updated_by`,`prev_status`,`prev_priority`,`prev_assignee`,`comment_desc`,`bug_severity`,`prev_severity`,`group_assign`)
                VALUES (@raiseBugId,1,1,@buildExecutor,NOW(),@buildExecutor,1,1,@buildExecutor,'',1,1,1);

				INSERT INTO `xe_automation_step_execution_bug` (`test_step_id`,`bug_id`,`step_exe_id`,`test_case_id`,`build_id`)
				VALUES (stepId,@raiseBugId,@stepExeId,@testCase,buildId);
                
              END; 
         END IF;
         
                SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(stepIdArray, '|', 1)) + 2;
    SET stepIdArray = MID(stepIdArray, SubStrLen, strLenstepId);
                
                SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(stepDetailsArray, '|', 1)) + 2;
    SET stepDetailsArray = MID(stepDetailsArray, SubStrLen, strLenstepDetails);
                
                SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(stepInputTypeArray, '|', 1)) + 2;
    SET stepInputTypeArray = MID(stepInputTypeArray, SubStrLen, strLenstepInputType);
                
                SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(stepValueArray, '|', 1)) + 2;
    SET stepValueArray = MID(stepValueArray, SubStrLen, strLenstepValue);
                
                SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(stepStatusArray, '|', 1)) + 2;
    SET stepStatusArray = MID(stepStatusArray, SubStrLen, strLenstepStatus);
                
                SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(attachmentPathArray, '|', 1)) + 2;
    SET attachmentPathArray = MID(attachmentPathArray, SubStrLen, strLenattachmentPath);
                
                SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(titleArray, '|', 1)) + 2;
    SET titleArray = MID(titleArray, SubStrLen, strLentitle);
    
      SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(bugRaiseArray, '|', 1)) + 2;
    SET bugRaiseArray = MID(bugRaiseArray, SubStrLen, strLenBug);
                
                IF stepIdArray = '' THEN
      LEAVE do_this;
    END IF;
  END LOOP do_this;
  
   IF datasetNo>1 THEN
	Update xebm_automation_execution_summary SET  testcase_status=(SELECT max(status) FROM xetm_tc_dataset where exe_id=exeIdTemp);
   END IF;
      
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `buildExecutionDetails`(IN buildId INT(11))
BEGIN

DECLARE mailGrpId INT(11);

SELECT count(*) TotalTcCount,module_id FROM xebm_automation_build_testcase where build_id=buildId GROUP BY module_id;

SELECT project,module,module.module_name,count(*) ExecutedTcCount,SUM(case when testcase_status = 1 then 1 else 0 end) as PassCount,
SUM(case when testcase_status = 2 then 1 else 0 end) as FailCount,SUM(case when testcase_status = 3 then 1 else 0 end) as SkipCount,
SUM(case when testcase_status = 4 then 1 else 0 end) as BlockCount 
FROM xebm_automation_execution_summary as trialSummary,xe_module as module
where trialSummary.build=buildId and trialSummary.module=module.module_id                
GROUP BY trialSummary.module; 
                
SELECT BE.build_id,V.hostname,AB.build_name,MG.mail_group_name,U.first_name, U.last_name,BE.step_wise_screenshot,BE.report_bug,BE.browser
FROM xebm_build_exec_details BE,xebm_vm_details V,xe_mail_group MG, xe_automation_build AB,xe_user_details U 
where BE.mail_group_id = MG.mail_group_id and BE.vm_id=V.xe_vm_id and BE.build_id= AB.build_id and U.user_id=BE.user_id and BE.build_id =buildId;

SELECT * FROM xe_email_details;

SELECT * FROM xe_mail_notifications;

SELECT mail_group_id INTO mailGrpId FROM xebm_build_exec_details where build_id=buildId;

select * from xe_user_details where user_id IN (SELECT user_id FROM xe_mail_group_details where mail_group_id=mailGrpId);

END;;
CREATE DEFINER=`xenon`@`localhost` PROCEDURE `buildExecutionDetailsMail`(IN buildId INT(11))
BEGIN

DECLARE mailGrpId INT(11);
            
SELECT BE.build_id,V.hostname,AB.build_name,MG.mail_group_name,U.first_name, U.last_name,BE.step_wise_screenshot,BE.report_bug,BE.browser
FROM xebm_build_exec_details BE,xebm_vm_details V,xe_mail_group MG, xe_automation_build AB,xe_user_details U 
where BE.mail_group_id = MG.mail_group_id and BE.vm_id=V.xe_vm_id and BE.build_id= AB.build_id and U.user_id=BE.user_id and BE.build_id =buildId;

SELECT * FROM xe_email_details;

SELECT * FROM xe_mail_notifications;

SELECT mail_group_id INTO mailGrpId FROM xebm_build_exec_details where build_id=buildId;

select * from xe_user_details where user_id IN (SELECT user_id FROM xe_mail_group_details where mail_group_id=mailGrpId);

END;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateAutomationBuildStatus`(IN buildId INT, IN buildStamp VARCHAR(200), IN buildStatus INT(11))
BEGIN

Update xebm_build_exec_details SET `end_time`=now()  , `complete_status`=1 WHERE build_id=buildId;

UPDATE `xe_automation_build` SET `build_state`=buildStatus WHERE `build_id`=buildId;

UPDATE `xebm_vm_details` SET `vm_state`='4' WHERE `build_id`=buildId;

DELETE FROM `xenon-core`.xebm_execution where build_stamp=buildStamp and build_id=buildId;

END;;
CREATE DEFINER=`xenon`@`localhost` PROCEDURE `completemanualbuild`(IN buildId INT(11))
BEGIN

SELECT build_id,build_name,build_desc,a1.status as buildStatus, @user := build.build_creator as build_creator, r1.release_name as releaseName 
             FROM xe_build as build
			 INNER JOIN xe_active as a1 ON a1.active_id = build.build_status
			 INNER JOIN xe_release as r1 on r1.release_id = build.release_id
		     WHERE build_id= buildId;

SELECT * FROM xe_email_details;

SELECT * FROM xe_mail_notifications;

select * from xe_user_details where user_id IN (@user);

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `mailoncreatebug`(IN bugId int)
BEGIN

DECLARE groupId int default 0;

 SELECT * FROM xe_email_details;
 SELECT create_bug FROM xe_mail_notifications;
 
		  select * from (Select distinct  xbs.bs_id ,xb.bug_id,xb.bug_prefix,project.project_name,xm.module_name,
			CONCAT(user.first_name, ' ',user.last_name) bug_author_name,user.email_id bug_author_email_id,
             xs.bug_status currentStatus,xp.bug_priority currentPriority,xud.email_id assignee_email_id
			from xebt_bug xb,xebt_bugsummary xbs,xe_project project,xe_module xm ,xe_user_details user,
             xebt_status xs,xebt_priority xp,
			 xe_user_details xud
			where xb.project=project.project_id and xb.module=xm.module_id and xb.created_by= user.user_id and 
            xbs.bug_status=xs.status_id and xbs.bug_priority=xp.priority_id and xbs.assignee = xud.user_id and
           xbs.bug_id=xb.bug_id and xb.bug_id = bugId order by xbs.bs_id desc)as bugDetails GROUP BY bug_id;
           
		 SELECT group_id into groupId FROM xebt_bug where bug_id=bugId;
         
         SELECT (SELECT email_id FROM xe_user_details WHERE user_id=user) as user_emailId
		 FROM xebt_bug_group_details where group_id=groupId;
		 SELECT bug_group_name FROM xebt_bug_group WHERE bug_group_id=groupId;
         
END;;
CREATE DEFINER=`xenon`@`localhost` PROCEDURE `mailonupdatebug`(IN bugId int,IN bugGroupId int)
BEGIN
 SELECT * FROM xe_email_details;
 SELECT * FROM xe_mail_notifications;
  select * from (Select distinct  xbs.bs_id ,xb.bug_id,xb.bug_prefix,xb.bug_title,xb.bug_desc,project.project_name,xm.module_name,
			CONCAT(user.first_name, ' ',user.last_name) bug_author_name,user.email_id bug_author_email_id,DATE_FORMAT(xb.create_date , '%d-%b-%Y') as create_date,
             xs.bug_status currentStatus,xps.bug_status prevStatus,xp.bug_priority currentPriority,xud.email_id assignee_email_id,
            xpp.bug_priority prevPriority,xpud.email_id prev_assignee_email_id,
             xu.email_id bug_updated_by_email_id,xb.group_id,
             (SELECT bug_group_name  FROM xebt_bug_group WHERE bug_group_id=xb.group_id) as prevGroupName
			from xebt_bug xb,xebt_bugsummary xbs,xe_project project,xe_module xm ,xe_user_details user,xe_user_details xu,
             xebt_status xs,xebt_status xps ,xebt_priority xp,xebt_priority xpp,
			 xe_user_details xud,xe_user_details xpud
			where xb.project=project.project_id and xb.module=xm.module_id and xb.created_by= user.user_id and xbs.updated_by= xu.user_id and
            xbs.bug_status=xs.status_id and xbs.bug_priority=xp.priority_id and xbs.assignee = xud.user_id 
			and xbs.prev_status=xps.status_id and xbs.prev_priority=xpp.priority_id and xbs.prev_assignee = xpud.user_id and
           xbs.bug_id=xb.bug_id and xb.bug_id = bugId order by xbs.bs_id desc)as bugDetails GROUP BY bug_id;

         
         SELECT (SELECT email_id FROM xe_user_details WHERE user_id=user) as user_emailId
		 FROM xebt_bug_group_details where group_id=bugGroupId;
         
		 SELECT bug_group_name FROM xebt_bug_group WHERE bug_group_id=bugGroupId;
END;;
CREATE DEFINER=`xenon`@`localhost` PROCEDURE `manualBuildExecutionDetails`(IN buildId INT(11))
BEGIN

SELECT count(*) TotalTcCount,module_id FROM xebm_buildtestcase where build_id=buildId GROUP BY module_id;

SELECT trialSummary.project_id,project.project_name,trialSummary.mudule_id,module.module_name,count(*) ExecutedTcCount,SUM(case when status_id = 1 then 1 else 0 end) as PassCount,
SUM(case when status_id = 2 then 1 else 0 end) as FailCount,SUM(case when status_id = 3 then 1 else 0 end) as SkipCount,
SUM(case when status_id = 4 then 1 else 0 end) as BlockCount 
FROM xe_testcase_execution_summary as trialSummary,xe_module as module, xe_project as project
where trialSummary.build_id=buildId and trialSummary.mudule_id=module.module_id and trialSummary.project_id=project.project_id                
GROUP BY trialSummary.mudule_id; 
                
SELECT * FROM xe_email_details;

SELECT * FROM xe_mail_notifications;

SELECT build_id,build_name,build_desc,a1.status as buildStatus, @user := build.build_creator as build_creator, r1.release_name as releaseName 
             FROM xe_build as build
			 INNER JOIN xe_active as a1 ON a1.active_id = build.build_status
			 INNER JOIN xe_release as r1 on r1.release_id = build.release_id
		     WHERE build_id= buildId;

select * from xe_user_details where user_id IN (@user);

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertTestcaseSummary`(IN tcId INT(11),IN buildId INT(11), IN methodType INT(11),
IN testerId INT(11),IN statusId INT(11),
IN notesVal VARCHAR(400), IN completeStatus INT(11), IN userName VARCHAR(100),IN userId INT(11))
BEGIN

DECLARE TotalCount , ExecCount,projectId, muduleId,scenarioId INT(11); 
DECLARE buildStatus INT(11);

SELECT build_status INTO buildStatus FROM xe_build where build_id=buildId; 
SELECT build_status FROM xe_build where build_id=buildId;

IF buildStatus = 1 THEN 

SELECT M.project_id INTO projectId FROM xe_module M,xetm_scenario S,xetm_testcase T where T.testcase_id=tcId and T.scenario_id=S.scenario_id and M.module_id=S.module_id;
SELECT S.module_id INTO muduleId FROM xe_module M,xetm_scenario S,xetm_testcase T where T.testcase_id=tcId and T.scenario_id=S.scenario_id and M.module_id=S.module_id;
SELECT T.scenario_id INTO scenarioId  FROM xe_module M,xetm_scenario S,xetm_testcase T where T.testcase_id=tcId and T.scenario_id=S.scenario_id and M.module_id=S.module_id;

IF methodType=1 THEN 

Update xe_testcase_execution_summary SET  `status_id`=statusId, `notes`=notesVal,`complete_status`=completeStatus where `tc_id`=tcId and `build_id`=buildId;

END IF;

IF methodType=2 THEN 

INSERT INTO xe_testcase_execution_summary ( tc_id, build_id, project_id, mudule_id, scenario_id,tester_id,status_id, notes,complete_status) 
VALUES ( tcId,buildId,projectId,muduleId,scenarioId,testerId,statusId,notesVal, completeStatus);

END IF;

UPDATE `xe_build` SET build_state=4 WHERE build_id=buildId;

SELECT count(*) TotalTcCount FROM xebm_buildtestcase where  build_id=buildId;
SELECT count(*) ExecutedTcCount FROM xe_testcase_execution_summary where  build_id=buildId and complete_status=1;

SELECT count(*) TotalTcCount INTO TotalCount FROM xebm_buildtestcase where  build_id=buildId;

SELECT count(*) ExecutedTcCount INTO ExecCount FROM xe_testcase_execution_summary where  build_id=buildId and complete_status=1;

END IF;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `getHeaderData`(IN userId INT)
BEGIN

SELECT count(*) TotalTcCount , build_id FROM xebm_buildtestcase GROUP BY build_id;
SELECT count(*) AS ExecutedTcCount,build_id FROM xe_testcase_execution_summary where complete_status=1 GROUP BY build_id;

SELECT distinct a.notification_id,a.activity,a.notification_description,a.read_status,
DATE_FORMAT(a.notification_date , '%d-%m-%Y %h:%i:%s %p') as notification_date,a.notification_date as actDate,
TIMESTAMPDIFF(MINUTE,a.notification_date,CURRENT_TIME()) AS TimeDiff,
u.first_name,u.last_name,nfu.nf_user_id as nfUserId,a.user_id,u.user_photo,nf.notification_for,nf.notification_icon,a.notification_url ,userId as requiredId,a.created_by,nfu.nf_read_status,nfu.nf_read_count_flag,a.read_count_flag
FROM xe_user_notifications a
INNER JOIN xe_user_details u ON u.user_id=a.created_by 
INNER JOIN xe_notifications nf ON nf.notification_id=a.activity
LEFT JOIN xe_notification_users nfu ON nfu.nf_notification_id=a.notification_id
and a.notification_status=1 and nfu.nf_user_id=userId  and a.created_by!=userId and nf.notification_status=1 ORDER BY a.notification_id DESC;

SELECT count(distinct a.notification_id) as unreadCount
FROM xe_user_notifications a
INNER JOIN xe_user_details u ON u.user_id=a.created_by 
INNER JOIN xe_notifications nf ON nf.notification_id=a.activity
LEFT JOIN xe_notification_users nfu ON nfu.nf_notification_id=a.notification_id
and a.notification_status=1 and nfu.nf_user_id=userId  and a.created_by!=userId and nf.notification_status=1 ORDER BY a.notification_id DESC;

SELECT B.build_id,B.build_name,B.build_desc,B.build_status,B.release_id,B.build_state,B.build_createdate,B.build_createdate as creDate , TIMESTAMPDIFF(MINUTE,B.build_createdate,CURRENT_TIME()) AS TimeDiff,B.build_enddate,B.build_creator ,XU.user_photo, BS.desc,XU.first_name,XU.last_name  FROM xe_build B INNER JOIN xebm_build_state BS ON BS.id=B.build_state INNER JOIN xe_user_details XU  ON  B.build_creator=XU.user_id   ORDER BY B.build_id DESC LIMIT 5;

SELECT project_id,project_name FROM xe_project
where project_id IN (SELECT project_id FROM xe_user_project where user = userId) and project_active=1;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `assignModuleToExecute`(
IN buildId INT(11),
IN projectId INT(11),
IN moduleId INT(11),
IN userId INT(11), 
IN updater INT(11))
BEGIN

DECLARE moduleName VARCHAR(45);
DECLARE buildName VARCHAR(400);
DECLARE notificationId INT(11);
DECLARE currentTime VARCHAR(45);

INSERT INTO `xebm_assign_build` (`build_id`,`project_id`,`module_id`,`user_id`) VALUES(buildId,projectId,moduleId,userId);

SELECT build_id,module_id FROM xebm_assign_build where build_id = buildId group by module_id;

SELECT build_id,module_id FROM xebm_buildtestcase where build_id = buildId and module_id IN (SELECT module_id FROM xe_module where module_active=1 and project_id IN (SELECT project_id FROM xe_project where project_active=1)) group by module_id;

SELECT module_name INTO moduleName FROM xe_module where module_id=moduleId;

SELECT build_name INTO buildName FROM xe_build where build_id=buildId;

SELECT NOW() INTO currentTime;

INSERT INTO `xe_user_notifications` (`activity`, `notification_description`, `notification_date`,`notification_url`, `user_id`, `created_by`, `read_status`, `notification_status`)
VALUES (6,CONCAT("Assigned module '",moduleName,"' for execution from build '",buildName,"'"),currentTime,'viewbuild', userId,updater,2,1);

SELECT notification_id INTO notificationId from `xe_user_notifications` where
`activity` = 6 and `notification_description`=CONCAT("Assigned module '",moduleName,"' for execution from build '",buildName,"'") and
`notification_url`='viewbuild' and 
 `user_id` = userId  and notification_date=currentTime and
`created_by` = updater  and  `read_status`= 2 and  `notification_status` = 1 LIMIT 1;

INSERT INTO `xe_notification_users` (`nf_notification_id`, `nf_user_id`) VALUES (notificationId,userId);

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertTestcaseComment`(
IN commentDescription VARCHAR(2000),
IN commentorId INT(11),
IN testcaseId INT(11),
IN currentTime varchar(200))
BEGIN

DECLARE createdBy INT(11);
DECLARE testcaseName VARCHAR(2000);
DECLARE testPrefix VARCHAR(45);
DECLARE notificationId INT(11);

DECLARE counter INT DEFAULT 0;
DECLARE done INT DEFAULT FALSE;
DECLARE nfUser INT;     
DECLARE cur1 CURSOR FOR SELECT DISTINCT commnetor FROM xetm_testcase_comments where testcase_id=testcaseId;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;



INSERT INTO `xetm_testcase_comments` ( `comment_description`, `commnetor`,`comment_time`, `testcase_id`) 
VALUES (commentDescription,commentorId,currentTime,testcaseId);

SELECT author INTO createdBy FROM xetm_testcase where testcase_id=testcaseId;
SELECT testcase_name INTO testcaseName FROM xetm_testcase where testcase_id=testcaseId;
SELECT test_prefix INTO testPrefix FROM xetm_testcase where testcase_id=testcaseId;

INSERT INTO `xe_user_notifications` (`activity`, `notification_description`, `notification_date`,`notification_url`, `user_id`, `created_by`, `read_status`, `notification_status`)
VALUES (4,CONCAT("Commented on test case '",testcaseName,"'"),currentTime,CONCAT('testcasebyprefix?tcPrefix=',testPrefix), createdBy,commentorId,2,1);

SELECT notification_id INTO notificationId from `xe_user_notifications` where
`activity` = 4 and `notification_description`=CONCAT("Commented on test case '",testcaseName,"'") and
`notification_url`=CONCAT('testcasebyprefix?tcPrefix=',testPrefix) and 
 `user_id` = createdBy and notification_date=currentTime and
`created_by` = commentorId  and  `read_status`= 2 and  `notification_status` = 1 LIMIT 1;
 
open cur1;
    read_loop: LOOP
			FETCH cur1 INTO nfUser;
				IF done THEN
					LEAVE read_loop;
				END IF;
                SELECT nfUser;
	INSERT INTO `xe_notification_users` (`nf_notification_id`, `nf_user_id`) VALUES (notificationId,nfUser);
	END LOOP;
CLOSE cur1;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertTestcaseCommentLike`(IN commentId INT(11),IN likedByUser INT(11))
BEGIN

DECLARE createdBy INT(11);
DECLARE testcaseName VARCHAR(2000);
DECLARE testPrefix VARCHAR(45);
DECLARE notificationId INT(11);
DECLARE currentTime VARCHAR(45);

INSERT INTO `xetm_comments_likes` ( `comment_id`, `liked_by_user`) VALUES ( commentId,likedByUser);

SELECT testcase_name INTO testcaseName FROM xetm_testcase where 
testcase_id IN (SELECT testcase_id FROM xetm_testcase_comments where xetm_testcase_comments_id=commentId);

SELECT commnetor INTO createdBy FROM xetm_testcase_comments where xetm_testcase_comments_id=commentId;

SELECT test_prefix INTO testPrefix FROM xetm_testcase where 
testcase_id IN (SELECT testcase_id FROM xetm_testcase_comments where xetm_testcase_comments_id=commentId);

IF createdBy!=likedByUser THEN
SELECT NOW() INTO currentTime;

INSERT INTO `xe_user_notifications` (`activity`, `notification_description`, `notification_date`,`notification_url`, `user_id`, `created_by`, `read_status`, `notification_status`)
VALUES (5,CONCAT("Liked comment on test case '",testcaseName,"'"),currentTime,CONCAT('testcasebyprefix?tcPrefix=',testPrefix), createdBy,likedByUser,2,1);

SELECT notification_id INTO notificationId from `xe_user_notifications` where
`activity` = 5 and `notification_description`=CONCAT("Liked comment on test case '",testcaseName,"'") and
`notification_url`=CONCAT('testcasebyprefix?tcPrefix=',testPrefix) and 
 `user_id` = createdBy and notification_date=currentTime and
`created_by` = likedByUser  and  `read_status`= 2 and  `notification_status` = 1 LIMIT 1;

INSERT INTO `xe_notification_users` (`nf_notification_id`, `nf_user_id`) VALUES (notificationId,createdBy);

END IF;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateTestcase`(IN tcTestcaseName varchar(2000),IN tcSummary varchar(2000),
IN tcPrecondition varchar(2000), IN tcStatus INT(11),IN tcExecutionType INT(11),
IN tcExecutionTime INT(11), IN tcUpdatedBy INT(11),IN tcTestcaseId INT(11),IN datasheetStatus INT(11))
BEGIN

DECLARE createdBy INT(11);
DECLARE testcaseName VARCHAR(2000);
DECLARE testPrefix VARCHAR(45);
DECLARE notificationId INT(11);
DECLARE currentTime VARCHAR(45);

UPDATE xetm_testcase  SET `testcase_name`=tcTestcaseName,`summary`=tcSummary,`precondition`=tcPrecondition,
`status`=tcStatus,`execution_type`=tcExecutionType,
`execution_time`=tcExecutionTime,`updated_by`=tcUpdatedBy ,`excel_file_present_status` = datasheetStatus
where `testcase_id`=tcTestcaseId;

SELECT testcase_name INTO testcaseName FROM xetm_testcase where testcase_id=tcTestcaseId;

SELECT author INTO createdBy FROM xetm_testcase where testcase_id=tcTestcaseId;

SELECT test_prefix INTO testPrefix FROM xetm_testcase where testcase_id=tcTestcaseId;

SELECT NOW() INTO currentTime;

INSERT INTO `xe_user_notifications` (`activity`, `notification_description`,  `notification_date`,`notification_url`, `user_id`, `created_by`, `read_status`, `notification_status`)
VALUES (7,CONCAT("Updated test case '",testcaseName,"'"),currentTime,CONCAT('testcasebyprefix?tcPrefix=',testPrefix), createdBy,tcUpdatedBy,2,1);

SELECT notification_id INTO notificationId from `xe_user_notifications` where
`activity` = 7 and `notification_description`=CONCAT("Updated test case '",testcaseName,"'") and
`notification_url`=CONCAT('testcasebyprefix?tcPrefix=',testPrefix) and 
 `user_id` = createdBy and notification_date=currentTime and
`created_by` = tcUpdatedBy  and  `read_status`= 2 and  `notification_status` = 1 LIMIT 1;

INSERT INTO `xe_notification_users` (`nf_notification_id`, `nf_user_id`) VALUES (notificationId,createdBy);

END ;;


CREATE DEFINER=`xenon`@`localhost` PROCEDURE `recentManualBuilds`()
BEGIN

SELECT build_id,build_name,build_createdate,build_state,buildState.desc
FROM xe_build as build
INNER JOIN xebm_build_state as buildState ON buildState.id = build.build_state
ORDER BY build_id DESC LIMIT 5;

SELECT count(*) AS TotalTcCount,build_id FROM xebm_buildtestcase GROUP BY build_id DESC LIMIT 5;

SELECT count(*) AS ExecutedTcCount,build_id FROM xe_testcase_execution_summary
where complete_status=1 GROUP BY build_id DESC LIMIT 5;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `recentAutomationBuilds`()
BEGIN

SELECT build_id,build_name,build_createdate,build_state,buildState.desc
FROM xe_automation_build as build
INNER JOIN xebm_build_state as buildState ON buildState.id = build.build_state
ORDER BY build_id DESC LIMIT 5;

SELECT count(*) AS TotalTcCount , build_id FROM xebm_automation_build_testcase GROUP BY build_id DESC LIMIT 5; 

SELECT count(*) AS ExecutedTcCount,build as build_id FROM xebm_automation_execution_summary GROUP BY build DESC LIMIT 5;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertBuildLearning`(IN buildId INT(11),IN buildName VARCHAR(400),
IN buildLearning VARCHAR(400),IN userID INT(11),IN userName VARCHAR(400))

BEGIN

UPDATE `xe_build` SET build_state=5,build_exe_comments = buildLearning
WHERE build_id=buildId;

INSERT INTO xe_recent_activities (activity, activity_desc,module,activity_date,user,activity_status)
VALUES (11,CONCAT(buildName,'#',userName),'BM',NOW(),userID,1);

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCloneBuildData`()
BEGIN

SELECT * FROM xe_release where release_active=1;

SELECT * FROM xe_build where build_status=1 and build_state IN (4, 5);

SELECT * FROM xe_automation_build where build_status=1 and build_state IN (4, 5);

SELECT * FROM `xe_build_execution_type` WHERE status=1;

END;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertCloneBuild`(IN cloneBuildId INT(11),
IN buildName varchar(400),
IN buildDescription varchar(400),
IN buildType INT(11),
IN releaseId INT(11),
IN buildCreator INT(11),
IN buildStatus INT(11),
IN currenDate varchar(200))
BEGIN
DECLARE newBuildId INT(11);
DECLARE buildState INT(11);

IF buildType=1 then
	
    SELECT build_state INTO buildState FROM `xe_build` WHERE build_id=cloneBuildId;
    
	INSERT INTO `xe_build` (`build_name`,  `build_desc`, `build_status`,`release_id`,`build_state`,`build_createdate`,`build_creator`) VALUES 
        (buildName,buildDescription,buildStatus,releaseId,2,currenDate,buildCreator);

	SELECT build_id INTO newBuildId FROM `xe_build` WHERE build_name=buildName and release_id=releaseId limit 1;
	
	INSERT INTO xebm_buildtestcase (build_id,testcase_id,project_id,scenario_id,module_id) SELECT newBuildId,testcase_id,project_id,scenario_id,module_id FROM xebm_buildtestcase where build_id=cloneBuildId;
    
END IF;

IF buildType=2 then
	
	SELECT build_state INTO buildState FROM `xe_automation_build` WHERE build_id=cloneBuildId;
    
	INSERT INTO `xe_automation_build` (`build_name`,  `build_desc`, `build_status`,`release_id`,`build_state`,`build_createdate`,`build_creator`) VALUES 
        (buildName,buildDescription,buildStatus,releaseId,2,currenDate,buildCreator);

	SELECT build_id INTO newBuildId FROM `xe_automation_build` WHERE build_name=buildName and release_id=releaseId;
    
    INSERT INTO xebm_automation_build_testcase (build_id,testcase_id,project_id,scenario_id,module_id) SELECT newBuildId,testcase_id,project_id,scenario_id,module_id FROM xebm_automation_build_testcase where build_id=cloneBuildId;
END IF;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getapplicationstoassign`(IN userId INT,IN currentUser INT)
BEGIN
SELECT distinct uPro.project_id,pro.project_name,pro.project_active ,uMod.module_id,uMod.user_id ,module.module_name,
module.module_active,(select count(project_id) from xe_user_module where user_id=userId and module_id = module.module_id) as assign_count
FROM xe_user_project as uPro  
LEFT JOIN xe_user_module as uMod ON uMod.project_id = uPro.project_id and user_id = currentUser
LEFT JOIN xe_project as pro ON pro.project_id = uPro.project_id 
LEFT JOIN xe_module as module ON module.module_id = uMod.module_id 
WHERE pro.project_id = uPro.project_id  and uPro.user = currentUser;
END ;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `bugGroup`()
BEGIN
 SELECT * from xebt_bug_group;
 SELECT distinct mgd.user,mgd.group_id,ud.email_id,Concat(ud.first_name,' ',ud.last_name)user_name,(select role_type from xe_role where role_id= ud.role_id)user_role,ud.role_id,ud.user_photo FROM xebt_bug_group_details mgd,xebt_bug_group mg, xe_user_details ud where mgd.user = ud.user_id ;
 SELECT user_id,Concat(first_name,' ',last_name)user_name from xe_user_details where user_status = 1;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUtilization`(IN bandUsage DOUBLE,IN userName VARCHAR(45))
BEGIN
   
UPDATE `xe_utilization` SET `bandwidth_usageMB`= `bandwidth_usageMB` + bandUsage WHERE xe_utilization.userName=userName;

UPDATE `xenon-core`.xe_customer_details  SET bandwidth_usageMB = bandwidth_usageMB + bandUsage 
WHERE `customer_schema`= (SELECT DATABASE());
   
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `mailOnSettingsTest`(IN userId INT)
BEGIN

SELECT email_id,first_name,last_name FROM xe_user_details where user_id = userId;

SELECT * FROM xe_email_details;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `addCommentOnBugActivity`(
IN bugId INT,
IN activityDesc VARCHAR(2000),
IN actUser INT(11),
IN nfUserId INT(11),
IN createdBy INT(11))
BEGIN
DECLARE bugPrefix VARCHAR(45);
DECLARE bugTitle VARCHAR(250);
DECLARE notificationId INT(11);
DECLARE currentTime VARCHAR(45);
DECLARE counter INT DEFAULT 0;
DECLARE done INT DEFAULT FALSE;
DECLARE nfUser INT;     
DECLARE cur1 CURSOR FOR SELECT DISTINCT updated_by FROM xebt_bugsummary where bug_id=bugId;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

SELECT NOW() INTO currentTime;
SELECT bug_prefix INTO bugPrefix FROM xebt_bug where bug_id=bugId;
SELECT bug_title INTO bugTitle FROM xebt_bug where bug_id=bugId;


INSERT INTO xe_recent_activities (activity, activity_desc,module,activity_date,user,activity_status) 
VALUES (8,activityDesc,"BT",currentTime,actUser,1);

INSERT INTO `xe_user_notifications` 
(`notification_id`, `activity`, `notification_description`, `notification_url`, 
`notification_date`, `user_id`, `created_by`, `read_status`, `notification_status`) 
VALUES (0,3, CONCAT("Commented on bug '",bugTitle,"'"),CONCAT('bugsummarybyprefix?btPrefix=',bugPrefix),currentTime, nfUserId,createdBy,2, 1);

SELECT notification_id INTO notificationId from `xe_user_notifications` where
`activity` = 3 and `notification_description`=CONCAT("Commented on bug '",bugTitle,"'") and
`notification_url`=CONCAT('bugsummarybyprefix?btPrefix=',bugPrefix) and notification_date=currentTime and
`user_id` = nfUserId and notification_date=currentTime and
`created_by` = createdBy  and  `read_status`= 2 and  `notification_status` = 1 LIMIT 1;

open cur1;
    read_loop: LOOP
			FETCH cur1 INTO nfUser;
				IF done THEN
					LEAVE read_loop;
				END IF;
                SELECT nfUser;
	INSERT INTO `xe_notification_users` (`nf_notification_id`, `nf_user_id`) VALUES (notificationId,nfUser);
	END LOOP;
CLOSE cur1;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateBugUserNotification`(
IN groupStatus INT,
IN bugId INT,
IN createdBy INT)
BEGIN
DECLARE bugPrefix VARCHAR(45);
DECLARE bugTitle VARCHAR(250);
DECLARE tempUser INT(11);
DECLARE tempPrevUser INT(11);
DECLARE bugGrpId INT(11);
DECLARE notificationId INT(11);
DECLARE currentTime VARCHAR(45);
SELECT NOW() INTO currentTime;
SELECT bug_prefix INTO bugPrefix FROM xebt_bug where bug_id=bugId;
SELECT bug_title INTO bugTitle FROM xebt_bug where bug_id=bugId;

IF groupStatus=1 THEN 	

	SELECT group_id INTO bugGrpId FROM xebt_bug where bug_id=bugId;
    
    SELECT group_id FROM xebt_bug where bug_id=bugId;

	CALL updateBugUserNotificationProc(bugGrpId,bugId,createdBy);

END IF;

IF groupStatus=2 THEN

	SELECT assignee INTO tempUser FROM xebt_bugsummary where bug_id=bugId ORDER BY bs_id DESC LIMIT 1;
    
    SELECT prev_assignee INTO tempPrevUser FROM xebt_bugsummary where bug_id=bugId ORDER BY bs_id DESC LIMIT 1;
    
    IF tempUser!=tempPrevUser THEN 
 	
	INSERT INTO `xe_user_notifications` 
	(`notification_id`, `activity`, `notification_description`, `notification_url`, `notification_date`, `user_id`, `created_by`, `read_status`, `notification_status`) 
    VALUES (0,9,CONCAT("Assigned bug '",bugPrefix,"' to you"),CONCAT('bugsummarybyprefix?btPrefix=',bugPrefix),currentTime,tempUser,createdBy,2, 1);

	END IF;
END IF;

END;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateBugUserNotificationProc`(
IN groupId INT,
IN bugId INT,
IN createdBy INT)
BEGIN
DECLARE bugPrefix VARCHAR(45);
DECLARE bugTitle VARCHAR(250);
DECLARE tempUser INT(11);
DECLARE groupStatus INT(11);
DECLARE groupName VARCHAR(45);
DECLARE notificationId INT(11);
DECLARE currentTime VARCHAR(45);
DECLARE done INT DEFAULT FALSE;
DECLARE nfUser INT;     
DECLARE cur1 CURSOR FOR SELECT distinct user FROM xebt_bug_group_details where group_id=groupId;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

SELECT NOW() INTO currentTime;
SELECT bug_prefix INTO bugPrefix FROM xebt_bug where bug_id=bugId;
SELECT bug_title INTO bugTitle FROM xebt_bug where bug_id=bugId;
SELECT group_status INTO groupStatus FROM xebt_bug where bug_id=bugId;
SELECT bug_group_name INTO groupName FROM xebt_bug_group WHERE bug_group_id=groupId;

INSERT INTO `xe_user_notifications` 
(`notification_id`, `activity`, `notification_description`, `notification_url`, 
`notification_date`, `user_id`, `created_by`, `read_status`, `notification_status`) 
VALUES (0,9,CONCAT("Assigned bug '",bugPrefix,"' to group '",groupName,"'"),CONCAT('bugsummarybyprefix?btPrefix=',bugPrefix),currentTime, createdBy,createdBy,2, 1);

SELECT notification_id INTO notificationId from `xe_user_notifications` where
`activity` = 9 and `notification_description`=CONCAT("Assigned bug '",bugPrefix,"' to group '",groupName,"'") and
`notification_url`=CONCAT('bugsummarybyprefix?btPrefix=',bugPrefix) and notification_date=currentTime and
`user_id` = createdBy and notification_date=currentTime and
`created_by` = createdBy  and  `read_status`= 2 and  `notification_status` = 1 LIMIT 1;

open cur1;
    read_loop: LOOP
			FETCH cur1 INTO nfUser;
				IF done THEN
					LEAVE read_loop;
				END IF;
                SELECT nfUser;
	INSERT INTO `xe_notification_users` (`nf_notification_id`, `nf_user_id`) VALUES (notificationId,nfUser);
	END LOOP;
CLOSE cur1;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertBugUserNotification`(
IN assignStatus INT,
IN groupStatus INT,
IN groupId INT,
IN bugId INT,
IN createdBy INT)
BEGIN
DECLARE bugPrefix VARCHAR(45);
DECLARE bugTitle VARCHAR(250);
DECLARE tempUser INT(11);
DECLARE groupName VARCHAR(45);
DECLARE notificationId INT(11);
DECLARE currentTime VARCHAR(45);
DECLARE counter INT DEFAULT 0;
DECLARE done INT DEFAULT FALSE;
DECLARE nfUser INT;     
DECLARE cur1 CURSOR FOR SELECT distinct user FROM xebt_bug_group_details where group_id=groupId;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

SELECT NOW() INTO currentTime;
SELECT bug_prefix INTO bugPrefix FROM xebt_bug where bug_id=bugId;
SELECT bug_title INTO bugTitle FROM xebt_bug where bug_id=bugId;
SELECT bug_group_name INTO groupName FROM xebt_bug_group WHERE bug_group_id=groupId;

IF assignStatus=1 and groupStatus=1 THEN 

INSERT INTO `xe_user_notifications` 
(`notification_id`, `activity`, `notification_description`, `notification_url`, 
`notification_date`, `user_id`, `created_by`, `read_status`, `notification_status`) 
VALUES (0,9,CONCAT("Assigned bug '",bugPrefix,"' to group '",groupName,"'"),CONCAT('bugsummarybyprefix?btPrefix=',bugPrefix),currentTime, createdBy,createdBy,2, 1);

SELECT notification_id INTO notificationId from `xe_user_notifications` where
`activity` = 9 and `notification_description`=CONCAT("Assigned bug '",bugPrefix,"' to group '",groupName,"'") and
`notification_url`=CONCAT('bugsummarybyprefix?btPrefix=',bugPrefix) and notification_date=currentTime and
`user_id` = createdBy and notification_date=currentTime and
`created_by` = createdBy  and  `read_status`= 2 and  `notification_status` = 1 LIMIT 1;

open cur1;
    read_loop: LOOP
			FETCH cur1 INTO nfUser;
				IF done THEN
					LEAVE read_loop;
				END IF;
                SELECT nfUser;
	INSERT INTO `xe_notification_users` (`nf_notification_id`, `nf_user_id`) VALUES (notificationId,nfUser);
	END LOOP;
CLOSE cur1;

END IF;

IF assignStatus=1 and groupStatus=2 THEN

	SELECT assignee INTO tempUser FROM xebt_bugsummary where bug_id=bugId LIMIT 1;
 	
	INSERT INTO `xe_user_notifications` 
	(`notification_id`, `activity`, `notification_description`, `notification_url`, `notification_date`, `user_id`, `created_by`, `read_status`, `notification_status`) 
    VALUES (0,9,CONCAT("Assigned bug '",bugPrefix,"' to you"),CONCAT('bugsummarybyprefix?btPrefix=',bugPrefix),currentTime,tempUser,createdBy,2, 1);

END IF;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateBugAssigneeNotifications`(IN notificationId INT,IN bugId INT)
BEGIN
  DECLARE done INT DEFAULT FALSE;
  DECLARE nfUser INT;     
  DECLARE cur1 CURSOR FOR SELECT distinct assignee FROM xebt_bugsummary where bug_id=bugId and assignee NOT IN (SELECT distinct updated_by FROM xebt_bugsummary where bug_id=bugId)  ;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  
  open cur1;
    read_loop: LOOP
			FETCH cur1 INTO nfUser;
				IF done THEN
					LEAVE read_loop;
				END IF;                
	INSERT INTO `xe_notification_users` (`nf_notification_id`, `nf_user_id`) VALUES (notificationId,nfUser);
	END LOOP;
	CLOSE cur1;
END;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateNotificationReadCountFlag`(IN userId INT)
BEGIN
Update xe_user_notifications SET read_count_flag=1 where read_count_flag=2 and user_id=userId;

Update xe_notification_users SET nf_read_count_flag=1 where nf_read_count_flag=2  and nf_user_id=userId;
END;;
CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getBuildReportByBuildId`(IN buildId INT,IN userId INT)
BEGIN

                SELECT * FROM  xe_project where project_id IN 
                (SELECT project_id FROM  xe_module where module_id IN 
                (SELECT module_id FROM  xetm_scenario where scenario_id IN 
                (SELECT scenario_id FROM  xetm_testcase where testcase_id IN 
                (SELECT testcase_id FROM  xebm_buildtestcase where build_id=buildId)))) 
                and project_id IN (SELECT project_id FROM xe_user_project where user =userId) 
                and project_active=1;
        
                SELECT * FROM xe_module where module_id IN 
                (SELECT module_id FROM xetm_scenario where scenario_id IN 
                (SELECT scenario_id FROM xetm_testcase where testcase_id IN 
                (SELECT testcase_id FROM xebm_buildtestcase where build_id=buildId)));
    
    
                SELECT * FROM xetm_scenario where scenario_id IN 
                (SELECT scenario_id FROM xetm_testcase where testcase_id IN 
                (SELECT testcase_id FROM xebm_buildtestcase where build_id=buildId));
   
                SELECT * FROM xetm_testcase where testcase_id IN 
                (SELECT testcase_id FROM xebm_buildtestcase where build_id=buildId);
    
    SELECT S.exe_step_bug_id,S.test_step_id,S.bug_id,S.step_exe_id,S.test_case_id,S.build_id,B.bug_prefix FROM xe_step_execution_bug S,xebt_bug B where build_id=buildId and B.bug_id=S.bug_id;
    
    SELECT * FROM xetm_testcase_steps;
    
    SELECT * FROM xetm_exec_status where status=1;
    
    SELECT distinct TS.exec_id,TS.tc_id,TS.build_id,TS.project_id,TS.mudule_id, TS.scenario_id,TS.tester_id,
                TS.status_id,TS.notes,ES.description,TS.complete_status 
                FROM xe_testcase_execution_summary TS 
                INNER JOIN xebm_buildtestcase BT on BT.testcase_id=TS.tc_id 
                INNER JOIN xetm_exec_status ES on TS.status_id=ES.exst_id 
                where TS.build_id=buildId;
        
                SELECT * FROM xe_steps_execution_summary where testex_id IN  
                (SELECT exec_id FROM xe_testcase_execution_summary where build_id=buildId);
    
    SELECT count(*) TotalTcCount,project_id FROM xebm_buildtestcase where build_id=buildId GROUP BY project_id;
        
    SELECT count(*) ExecutedTcCount,SUM(case when status_id = 1 then 1 else 0 end) as PassCount,
    SUM(case when status_id = 2 then 1 else 0 end) as FailCount,SUM(case when status_id = 3 then 1 else 0 end) as SkipCount,
    SUM(case when status_id = 4 then 1 else 0 end) as BlockCount,project_id 
                FROM xe_testcase_execution_summary where build_id=buildId and complete_status=1 GROUP BY project_id;    
    
    SELECT count(*) TcCount,module_id FROM xebm_buildtestcase where build_id=buildId GROUP BY module_id;
    
    SELECT count(*) TcCount,SUM(case when status_id = 1 then 1 else 0 end) as PassCount,
    SUM(case when status_id = 2 then 1 else 0 end) as FailCount,
    SUM(case when status_id = 3 then 1 else 0 end) as SkipCount,
    SUM(case when status_id = 4 then 1 else 0 end) as BlockCount,mudule_id
    FROM xe_testcase_execution_summary where build_id=buildId and complete_status=1 GROUP BY mudule_id;
    
    SELECT count(*) TcCount,scenario_id FROM xebm_buildtestcase where build_id=buildId  GROUP BY scenario_id; 
    
    SELECT count(*) TcCount,SUM(case when status_id = 1 then 1 else 0 end) as PassCount,
    SUM(case when status_id = 2 then 1 else 0 end) as FailCount,
    SUM(case when status_id = 3 then 1 else 0 end) as SkipCount,
    SUM(case when status_id = 4 then 1 else 0 end) as BlockCount,scenario_id
    FROM xe_testcase_execution_summary where build_id=buildId and complete_status=1 GROUP BY scenario_id;
    
    SELECT attach_id,testcase_id,attachment_title 
	FROM xe_testcase_execution_attachments
	WHERE testcase_id in (select tc_id FROM xe_testcase_execution_summary WHERE build_id = buildId)
	and build_id = buildId;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `applicationSettings`(IN applicationId int)
BEGIN

	SELECT project_id,project_name,project_description,bt_prefix,tm_prefix,active.status as projectStatus,project_createdate 
	FROM xe_project as project 
	INNER JOIN xe_active as active on active.active_id=project.project_active 
	WHERE project_id = applicationId;

	 SELECT user_id, user_photo, first_name, last_name, email_id
	 FROM xe_user_details as ud where user_id in 
	 (SELECT user FROM xe_user_project where project_id in (applicationId)) and ud.user_status = 1;

	 SELECT user_id, first_name, last_name, email_id
	 FROM xe_user_details as ud where user_id not in 
	 (SELECT user FROM xe_user_project where project_id in (applicationId)) and ud.user_status = 1;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `updateApplication`(IN projectDescription LONGTEXT,IN projectActive INT,IN projectId INT)
BEGIN
	
	IF projectActive = 2 THEN
		DELETE FROM xe_current_project 
		WHERE project_id = projectId and 
		user_id IN (SELECT user FROM xe_user_project where project_id in (projectId));
	END IF;
	
	UPDATE xe_project 
	SET project_description = projectDescription, project_active = projectActive 
	WHERE project_id = projectId;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `getBuildVmDetails`(IN buildId INT(11),IN userId INT(11))
BEGIN
    SELECT * FROM xebm_vm_details where build_id = buildId and userId=userId ORDER BY xe_vm_id DESC LIMIT 1;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `getBugDashboard`(IN projectId INT(11),IN userId INT(11))
BEGIN
SELECT Count(*) bugCount,created_by,bug_status FROM (SELECT * FROM xebt_bugsummary WHERE bs_id IN (SELECT max(bs_id) FROM xebt_bugsummary GROUP BY bug_id ORDER BY bs_id DESC)) AS A
JOIN (SELECT DISTINCT (XB.created_by),U.first_name,U.last_name,bug_id,project FROM xebt_bug XB,xe_user_details U where U.user_id=XB.created_by) AS B ON A.bug_id=B.bug_id AND B.project=projectId GROUP BY created_by,bug_status ;

SELECT DISTINCT (XB.created_by),U.first_name,U.last_name FROM xebt_bug XB,xe_user_details U where U.user_id=XB.created_by and U.user_id in (SELECT user FROM xe_user_project WHERE project_id = projectId);

SELECT * FROM xebt_status where bug_state=1;

SELECT DISTINCT(module_id) FROM xe_user_module where user_id=userId;

SELECT distinct(update_date),summary.bug_id, 
DATE(DATE_ADD(update_date, INTERVAL(1-DAYOFWEEK(update_date)) DAY)) 
weekday,stat.bug_status bugStatus FROM xebt_bugsummary summary,xebt_bug bug ,
 xe_module xemodule,xebt_status as stat where bug.bug_id = summary.bug_id and 
 summary.bug_status = stat.status_id and bug.project=projectId and bug.module = xemodule.module_id 
 and xemodule.module_active = 1 and bug.module IN ( select module_id from xe_user_module 
 where user_id=userId ) group by bug_id,bugStatus order by bs_id desc;

select * from (select * from (select xbs.bug_id,xbs.group_assign,
xb.group_status,xb.group_id,xb.assign_status,xb.bug_title,xb.bug_desc,
xb.project,@moduleId:=xb.module as module,xb.create_date,xb.created_by,
xb.bug_prefix,xbs.bug_status,stat.bug_status as bugStatus,priority.bug_priority 
as bugPriority,xbs.bug_priority,severity.bug_severity as bugSeverity,xbs.bug_severity,
xbs.assignee,xbs.update_date,xbs.updated_by,category.category_name, xp.project_name,
xp.project_description,xm.module_name,xm.module_description,xum.user_id,xud.user_photo,
xud.first_name as assigneeFN ,xud.last_name as assigneeLN,xu.first_name as reporterFN ,
xu.last_name as reporterLN,(select count(user_id) from xe_user_module  where @moduleId = xe_user_module.module_id 
and xe_user_module.user_id=userId) as user_count from xebt_bug xb,xebt_bugsummary xbs,xe_project xp,xe_module xm ,
 xe_user_module xum,xe_user_details xu,xe_user_details xud,xebt_status as stat,xebt_priority as priority ,
 xebt_severity as severity, xebt_category as category where xb.module IN (SELECT module_id FROM xe_user_module 
 xum WHERE xum.user_id=userId) AND xb.project IN (SELECT project_id FROM xe_user_project xup WHERE xup.user=userId) 
 AND xb.bug_id=xbs.bug_id and xb.project=xp.project_id and xb.module = xm.module_id and xum.module_id = xm.module_id and 
 xu.user_id = xb.created_by and xud.user_id = xbs.assignee and xbs.bug_status = stat.status_id and 
 priority.priority_id = xbs.bug_priority and xbs.category = category.category_id and severity.severity_id = xbs.bug_severity and xp.project_active = 1 
and xm.module_active = 1 order by bs_id desc ) as table1 group by bug_id)as table2 order by project;

SELECT cat.category_id,cat.category_name,count(bg.bug_id) as bCount
FROM xebt_bugsummary AS bgs 
	INNER JOIN xebt_bug bg 
	ON bgs.bug_id = bg.bug_id
	INNER JOIN xebt_category cat 
	ON bgs.category = cat.category_id
    WHERE bgs.bug_status=5 and bg.project = projectId  GROUP BY cat.category_id; 

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `sfdcAddStepPageData`(IN scannerId INT(11),IN testcaseId INT(11), IN envId INT(11))
BEGIN

SELECT r.repo_id,r.scanner_id,r.label,r.type,r.xpath,l.type_name,r.required FROM xesfdc_repository r,xesfdc_locator_type l where scanner_id=scannerId and l.loc_type_id=r.type;

SELECT * FROM xesfdc_locator_type where type_status=1;

SELECT ts.sfdc_tc_steps,ts.sfdc_testcase,ts.repo_id,ts.step_desc,ts.step_no,r.repo_id,r.scanner_id,r.label,r.type,r.xpath,l.type_name FROM xesfdc_test_steps ts,xesfdc_repository r,xesfdc_locator_type l where ts.sfdc_testcase=testcaseId  and scanner_id=scannerId and l.loc_type_id=r.type and r.repo_id=ts.repo_id;

SELECT tc.sfdc_tc_id ,tc.testcase_name, tc.sfdc_page_id, pages.pagename, tc.sfdc_functionality_id, tc.sfdc_env 
FROM xesfdc_tc as tc, xesfdc_pages as pages 
WHERE tc.sfdc_page_id = pages.page_id and tc.sfdc_env = envId
Order by tc.sfdc_page_id ;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `sfdcGetScanPageData`()
BEGIN
SELECT * FROM xesfdc_env where env_status=1;

SELECT * FROM xesfdc_func where func_status=1;

SELECT * FROM xesfdc_pages where page_status=1 and is_scanned = 2;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `sfdcLocatorsPageData`(IN scannerId INT(11))
BEGIN
SELECT * FROM xesfdc_env where env_status=1;

SELECT * FROM xesfdc_func where func_status=1;

SELECT * FROM xesfdc_pages where page_status=1;

SELECT r.repo_id,r.scanner_id,r.label,r.type,r.xpath,l.type_name FROM xesfdc_repository r,xesfdc_locator_type l where scanner_id=scannerId and l.loc_type_id=r.type;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `sfdcAddRemoveTcStepsPageData`(IN testcaseId INT(11),IN envId INT(11),IN buildId INT(11))
BEGIN
SELECT step.sfdc_tc_steps,step.sfdc_testcase,step.repo_id,step.step_desc,step.step_no,repo.label, repo.type
,repo.scanner_id, repo.repo_id, loc.type_name 
 FROM xesfdc_test_steps step, xesfdc_repository repo,
 xesfdc_locator_type loc where  repo.repo_id=step.repo_id and loc.loc_type_id=repo.type and step.sfdc_testcase = testcaseId;
 
SELECT tc.sfdc_tc_id ,tc.testcase_name, tc.sfdc_page_id,tc.sfdc_functionality_id,tc.sfdc_env, pages.pagename  FROM xesfdc_tc as tc, xesfdc_pages as pages  WHERE tc.sfdc_env = envId and tc.sfdc_page_id = pages.page_id  Order by tc.sfdc_page_id ;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `sfdcBuildAddRemoveTc`(IN userId INT(11),IN envId INT(11), IN buildId INT(11))
BEGIN
SELECT tc.sfdc_tc_id ,tc.testcase_name, tc.sfdc_page_id,tc.sfdc_functionality_id,tc.sfdc_env, pages.pagename  FROM xesfdc_tc as tc, xesfdc_pages as pages  WHERE tc.sfdc_env = envId and tc.user_id = userId and  tc.sfdc_page_id = pages.page_id  Order by tc.sfdc_page_id ;

SELECT sfdc_testcase_id FROM xesfdc_automation_build_testcase where sfdc_build_id=buildId;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `sfdcExecuteBuildData`(IN custId INT,IN buildIdTemp INT)
BEGIN
SELECT count(*) AS TcCount FROM xesfdc_automation_build_testcase where sfdc_build_id=buildIdTemp;

SELECT * FROM `xenonvm`.xevm_customervm_details where is_free = 1 and status=1 and customer_id = custId LIMIT 1;
  
SELECT * FROM xe_mail_group;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `SfdcScAutoReportData`(IN buildId INT(11))
BEGIN

SELECT exeSummary.env_id, exeSummary.page_id, pages.pagename, exeSummary.tc_id, testCase.testcase_name, exeSummary.tc_status 
FROM xesfdc_automation_execution_summary as exeSummary, xesfdc_pages as pages, xesfdc_tc as testCase
WHERE exeSummary.build_id = buildId
and exeSummary.page_id = pages.page_id and exeSummary.tc_id = testCase.sfdc_tc_id;

SELECT summary.page_id, pages.pagename ,Count(*) as totalTcCount,
SUM(case when summary.tc_status = 1 then 1 else 0 end) as PassCount,
SUM(case when summary.tc_status = 2 then 1 else 0 end) as FailCount,
SUM(case when summary.tc_status = 3 then 1 else 0 end) as SkipCount,
SUM(case when summary.tc_status = 4 then 1 else 0 end) as BlockCount,
SUM(case when summary.tc_status = 5 then 1 else 0 end) as NotRunCount
FROM xesfdc_automation_execution_summary as summary, xesfdc_pages as pages
WHERE summary.build_id = buildId and 
summary.page_id = pages.page_id
GROUP BY summary.page_id;

SELECT stepSummary.step_exe_id, stepSummary.tc_id, stepSummary.step_id, stepSummary.step_value, stepSummary.actual_result,
stepSummary.is_bug_raised, stepSummary.step_status, exeStatus.description,stepSummary.screenshot_title,stepSummary.step_details
FROM xesfdc_automation_steps_execution_summary as stepSummary, 
xetm_exec_status as exeStatus
WHERE stepSummary.build_id = buildId and exeStatus.exst_id = stepSummary.step_status;

SELECT testCase.sfdc_page_id as page_id, steps.tc_id, testCase.testcase_name, count(*) as totalStepsCount,
SUM(case when steps.step_status = 1 then 1 else 0 end) as PassCount,
SUM(case when steps.step_status = 2 then 1 else 0 end) as FailCount,
SUM(case when steps.step_status = 3 then 1 else 0 end) as SkipCount,
SUM(case when steps.step_status = 4 then 1 else 0 end) as BlockCount,
SUM(case when steps.step_status = 5 then 1 else 0 end) as NotRunCount,
summary.tc_status, exeStatus.description
FROM xesfdc_automation_steps_execution_summary as steps,
xesfdc_automation_execution_summary as summary,
xesfdc_tc as testCase, xetm_exec_status as exeStatus
WHERE steps.build_id = buildId and summary.tc_id = steps.tc_id and summary.tc_id = testCase.sfdc_tc_id
and exeStatus.exst_id = summary.tc_status
GROUP BY steps.tc_id;

SELECT stepSummary.tc_id, stepSummary.step_id, bug.bug_prefix
FROM xesfdc_automation_steps_execution_summary as stepSummary, 
xetm_exec_status as exeStatus, xesfdc_automation_step_execution_bug as stepBug,
xebt_bug as bug
WHERE stepSummary.build_id = buildId and exeStatus.exst_id = stepSummary.step_status and
stepBug.step_exe_id = stepSummary.step_exe_id and 
stepBug.bug_id = bug.bug_id;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `getVmIdByBuildId`(IN  buildId INT(11),IN schemaName VARCHAR(250))
BEGIN
SELECT * FROM xebm_vm_details where build_id = buildId ORDER BY xe_vm_id DESC LIMIT 1;
SELECT on_premise FROM `xenon-core`.xe_customer_details WHERE customer_schema=schemaName;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `sfdcMenuData`()
BEGIN
SELECT env_id, env_name FROM xesfdc_env where env_status = 1 ;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `getModuleSummary`(IN moduleId int)
BEGIN
SELECT uMod.user_id ,CONCAT(user.first_name,' ',user.last_name) as 
userName FROM xe_user_module as uMod  LEFT JOIN xe_user_details as user on user.user_id = uMod.user_id  
WHERE uMod.module_id = moduleId and user.user_status = 1 and user.bt_status=1;
SELECT * FROM xebt_bug_group;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `getInsertBugsummary`(IN bug_id int)
BEGIN
Select distinct xb.bug_id,xb.bug_prefix,xb.bug_title,xb.bug_desc,xb.project,xp.project_name,xb.module,xm.module_name,DATE_FORMAT(xb.create_date , '%d-%b-%Y') as create_date,xb.created_by,CONCAT(xud.first_name, ' ',xud.last_name) bug_author_name,xud.user_photo from xebt_bug xb,xe_project xp,xe_module xm ,xe_user_details xud where xb.project=xp.project_id and xb.module=xm.module_id and xb.created_by= xud.user_id and xb.bug_id = bug_id;
Select distinct (xb.bs_id),xb.bug_id,xb.bug_status,xs.bug_status currentStatus,xb.prev_status,xps.bug_status prevStatus,xb.bug_priority,xp.bug_priority currentPriority,xb.assignee,CONCAT(xud.first_name, ' ',xud.last_name) bug_assignee_name,  xb.prev_priority ,xpp.bug_priority prevPriority,xb.prev_assignee,CONCAT(xpud.first_name, ' ',xpud.last_name) prev_assignee_name,  DATE_FORMAT(xb.update_date , '%d-%b-%Y') as update_date,DATE_FORMAT(xb.update_date , '%d-%m-%Y %h:%i %p') as date,xb.comment_desc,xb.updated_by,TIMESTAMPDIFF(MINUTE,xb.update_date,CURRENT_TIME()) AS TimeDiff,CONCAT(xu.first_name, ' ',xu.last_name) bug_updated_by,xu.user_photo, category.category_name  from xebt_bugsummary xb,xebt_status xs,xebt_status xps ,xebt_priority xp,xebt_priority xpp,xe_user_details xud,xe_user_details xpud,xe_user_details xu, xebt_category as category  where xb.bug_status=xs.status_id and xb.bug_priority=xp.priority_id and xb.assignee = xud.user_id and xb.prev_status=xps.status_id and xb.prev_priority=xpp.priority_id and xb.prev_assignee = xpud.user_id and xb.updated_by = xu.user_id and xb.category = category.category_id and xb.bug_id = bug_id GROUP BY xb.bs_id DESC;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertSfdcTc`(IN tcName LONGTEXT, IN tcDesc LONGTEXT, IN pagenameId INT, IN functionalityId INT, IN envId INT, IN userId INT)
BEGIN
	INSERT INTO `xesfdc_tc` (`sfdc_tc_id`, `testcase_name`, `testcase_desc`, `sfdc_page_id`, `sfdc_functionality_id`, `sfdc_env`,`user_id`) VALUES (0,tcName,tcDesc,pagenameId,functionalityId,envId,userId);
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `sfdcInsertExecutionSteps`(IN testCaseStatus int,IN buildId int,IN testCaseId VARCHAR(1500),
                                            IN stepIdArray VARCHAR(1500),IN stepDetailsArray VARCHAR(1500),
                                            IN stepInputTypeArray VARCHAR(1500),IN stepValueArray VARCHAR(1500),
                                            IN stepStatusArray VARCHAR(1500),IN attachmentPathArray VARCHAR(1500),IN titleArray VARCHAR(1500),
                                            IN bugRaiseArray VARCHAR(1500))
BEGIN

 DECLARE strLenstepId   INT DEFAULT 0;
  DECLARE strLenstepDetails   INT DEFAULT 0;
  DECLARE strLenstepInputType   INT DEFAULT 0;
  DECLARE strLenstepValue   INT DEFAULT 0;
  DECLARE strLenstepStatus   INT DEFAULT 0;
  DECLARE strLenattachmentPath   INT DEFAULT 0;
  DECLARE strLentitle   INT DEFAULT 0;
  DECLARE strLenBug   INT DEFAULT 0;
       
  DECLARE SubStrLen INT DEFAULT 0;
  
  DECLARE stepId INT DEFAULT 0;
  DECLARE stepDetails VARCHAR(1500) DEFAULT 0;
  DECLARE stepInputType VARCHAR(1500) DEFAULT 0;
  DECLARE stepValue VARCHAR(1500) DEFAULT 0;
  DECLARE stepStatus VARCHAR(1500) DEFAULT 0;
  DECLARE attachmentPath VARCHAR(1500) DEFAULT 0;
  DECLARE title VARCHAR(1500) DEFAULT 0;
  DECLARE bugRaise VARCHAR(1500) DEFAULT 0;
  
  DECLARE projectId INT DEFAULT 0;
  DECLARE testCase INT DEFAULT 0;
  
  DECLARE stepExeId INT DEFAULT 0;
  DECLARE raiseBugId INT DEFAULT 0;
  DECLARE bugTestName VARCHAR(1500) DEFAULT 0;
  DECLARE bugBuildName VARCHAR(1500) DEFAULT 0;
  DECLARE buildExecutor INT DEFAULT 0;

  DECLARE moduleId INT DEFAULT 0;
  DECLARE sceId INT DEFAULT 0;
  DECLARE testStatus INT DEFAULT 0;
  
  DECLARE done INT DEFAULT 0;
  DECLARE testCasePass VARCHAR(1500) DEFAULT 0;
   DECLARE testCasePassId INT DEFAULT 0;
  DECLARE testCaseString VARCHAR(2000) DEFAULT 0;
  DECLARE failCaseString VARCHAR(2000) DEFAULT 0;

  DECLARE sfdcPageId INT DEFAULT 0;
  DECLARE sfdcFunctionalityId INT DEFAULT 0;
  DECLARE sfdcEnv INT DEFAULT 0;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
  SET testCaseString='';

  IF stepIdArray IS NULL THEN
    SET stepIdArray = '';
  END IF;

       SELECT @sfdcPageId:=sfdc_page_id,@sfdcFunctionalityId:=sfdc_functionality_id,@sfdcEnv:=sfdc_env FROM xesfdc_tc where sfdc_tc_id=testCaseId;
     
        DELETE  FROM xesfdc_automation_execution_summary 
        WHERE build_id=buildId AND env_id=@sfdcEnv AND page_id=@sfdcPageId AND xesfdc_automation_execution_summary.tc_id=testCaseId ;
       
            INSERT INTO xesfdc_automation_execution_summary (build_id,env_id,page_id,tc_id,tc_status)
                                                VALUES (buildId,@sfdcEnv,@sfdcPageId,testCaseId,testCaseStatus); 
                                
            SELECT @buildExecutor:=user_id FROM xesfdc_build_exec_details WHERE xesfdc_build_exec_details.build_id=buildId;
          
  
        
 do_this:
  LOOP
    SET strLenstepId = CHAR_LENGTH(stepIdArray);
    SET strLenstepDetails = CHAR_LENGTH(stepDetailsArray);
    SET strLenstepInputType = CHAR_LENGTH(stepInputTypeArray);
    SET strLenstepValue = CHAR_LENGTH(stepValueArray);
    SET strLenstepStatus = CHAR_LENGTH(stepStatusArray);
    SET strLenattachmentPath = CHAR_LENGTH(attachmentPathArray);
    SET strLentitle = CHAR_LENGTH(titleArray);
    SET strLenBug = CHAR_LENGTH(bugRaiseArray);
                
                SET stepId = SUBSTRING_INDEX(stepIdArray,'|', 1);
                SET stepDetails = SUBSTRING_INDEX(stepDetailsArray,'|', 1);
                SET stepInputType = SUBSTRING_INDEX(stepInputTypeArray,'|', 1);
                SET stepValue = SUBSTRING_INDEX(stepValueArray,'|', 1);
                SET stepStatus = SUBSTRING_INDEX(stepStatusArray,'|', 1);
                SET attachmentPath = SUBSTRING_INDEX(attachmentPathArray,'|', 1);
                SET title = SUBSTRING_INDEX(titleArray,'|', 1);
                        SET bugRaise = SUBSTRING_INDEX(bugRaiseArray,'|', 1);
                
                                
         SELECT @testCase:=T.sfdc_tc_id,@bugTestName:=testcase_name FROM xesfdc_tc T where T.sfdc_tc_id=testCaseId;
         
         SELECT @bugBuildName:=build_name FROM xesfdc_automation_build WHERE xesfdc_automation_build.build_id=buildId limit 1;
                
         INSERT INTO xesfdc_automation_steps_execution_summary (tc_id, step_id, step_details, build_id, step_value, step_status, screenshot, screenshot_title,actual_result,is_bug_raised, env_id, page_id)
         VALUES (@testCase,stepId,stepDetails,buildId,stepValue,stepStatus,LOAD_FILE(attachmentPath),title,stepInputType,bugRaise, @sfdcEnv, @sfdcPageId);
         
         SELECT @stepExeId:=step_exe_id FROM xesfdc_automation_steps_execution_summary AS xebm_exe 
         WHERE xebm_exe.tc_id=@testCase AND xebm_exe.build_id=buildId AND xebm_exe.step_id=stepId AND xebm_exe.is_bug_raised=bugRaise AND xebm_exe.step_status=stepStatus limit 1;
         
                SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(stepIdArray, '|', 1)) + 2;
    SET stepIdArray = MID(stepIdArray, SubStrLen, strLenstepId);
                
                SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(stepDetailsArray, '|', 1)) + 2;
    SET stepDetailsArray = MID(stepDetailsArray, SubStrLen, strLenstepDetails);
                
                SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(stepInputTypeArray, '|', 1)) + 2;
    SET stepInputTypeArray = MID(stepInputTypeArray, SubStrLen, strLenstepInputType);
                
                SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(stepValueArray, '|', 1)) + 2;
    SET stepValueArray = MID(stepValueArray, SubStrLen, strLenstepValue);
                
                SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(stepStatusArray, '|', 1)) + 2;
    SET stepStatusArray = MID(stepStatusArray, SubStrLen, strLenstepStatus);
                
                SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(attachmentPathArray, '|', 1)) + 2;
    SET attachmentPathArray = MID(attachmentPathArray, SubStrLen, strLenattachmentPath);
                
                SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(titleArray, '|', 1)) + 2;
    SET titleArray = MID(titleArray, SubStrLen, strLentitle);
    
      SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(bugRaiseArray, '|', 1)) + 2;
    SET bugRaiseArray = MID(bugRaiseArray, SubStrLen, strLenBug);
                
                IF stepIdArray = '' THEN
      LEAVE do_this;
    END IF;
END LOOP do_this;
      
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `viewallbugs`(IN userId int, IN projectId int, IN startValue int, IN offsetValue int,OUT count int)
BEGIN

SELECT summary.bug_id,bug.group_status,bug.group_id,bug.assign_status,bug.bug_title,bug.bug_prefix,summary.update_date,
bug.create_date,stat.bug_status as bugStatus,priority.bug_priority,severity.bug_severity as bugSeverity,
assignee.first_name as assigneeFN ,assignee.last_name as assigneeLN,reporter.first_name as reporterFN ,
reporter.last_name as reporterLN, category.category_name, bug.module, moduleTable.module_name
FROM xebt_bugsummary summary,xebt_bug bug ,xe_module as xemodule,xebt_status as stat,xebt_priority as priority,
xebt_severity as severity,xe_user_details as assignee , xe_user_details as reporter, xebt_category as category, xe_module as moduleTable
where bug.project= projectId and bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and severity.severity_id= summary.bug_severity
and priority.priority_id= summary.bug_priority and assignee.user_id = summary.assignee and bug.created_by = reporter.user_id
and bug.module = xemodule.module_id and xemodule.module_active = 1 and bug.module IN (select module_id from xe_user_module where user_id= userId) and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and summary.category = category.category_id and moduleTable.module_id = bug.module ORDER BY summary.update_date DESC LIMIT startValue, offsetValue;

SELECT count(*) INTO count FROM xebt_bug as bug, xe_module as xemodule
WHERE bug.project= projectId and bug.module = xemodule.module_id and xemodule.module_active = 1 
and bug.module IN (select module_id from xe_user_module where user_id = userId);

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `viewassignedbugs`(IN userId int, IN projectId int, IN startValue int, IN offsetValue int, OUT count int)
BEGIN

SELECT * FROM  (SELECT summary.bug_id,summary.assignee,summary.bug_status,bug.bug_prefix,bug.bug_title,summary.update_date,bug.create_date,stat.bug_status as bugStatus,priority.bug_priority,severity.bug_severity as bugSeverity,assignee.first_name as assigneeFN ,assignee.last_name as assigneeLN,reporter.first_name as reporterFN ,reporter.last_name as reporterLN, category.category_name, bug.module, moduleTable.module_name
FROM xebt_bugsummary summary,xebt_bug bug ,xe_module as xemodule ,xebt_status as stat,xebt_severity as severity,xebt_priority as priority,xe_user_details as assignee , xe_user_details as reporter, xebt_category as category, xe_module as moduleTable
where bug.assign_status=1 and summary.group_assign=1 and bug.project= projectId and bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority and severity.severity_id= summary.bug_severity and assignee.user_id = summary.assignee and bug.created_by = reporter.user_id 
and summary.category = category.category_id
and bug.module = xemodule.module_id and xemodule.module_active = 1 and bug.module IN (select module_id from xe_user_module where user_id= userId) and summary.bs_id in (select bs_id from xebt_bugsummary group by bug_id) and moduleTable.module_id = bug.module ) as table2 WHERE assignee= userId and bug_status not in (5,6) 
ORDER BY update_date DESC LIMIT startValue, offsetValue;

SELECT count(*) INTO count FROM (SELECT summary.assignee, summary.bug_status
FROM xebt_bugsummary summary,xebt_bug bug ,xe_module as xemodule
where bug.assign_status=1 and summary.group_assign=1 and bug.project= projectId and bug.bug_id = summary.bug_id 
and bug.module = xemodule.module_id and xemodule.module_active = 1 and bug.module IN (select module_id from xe_user_module where user_id= userId) 
and summary.bs_id in (select bs_id from xebt_bugsummary group by bug_id) )
as table2 WHERE assignee = userId and bug_status not in (5,6) ;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `viewreportedbugs`(IN userId int, IN projectId int, IN startValue int, IN offsetValue int,OUT count int)
BEGIN

SELECT summary.bug_id,bug.group_status,bug.group_id,bug.assign_status,bug.bug_title,bug.bug_prefix,summary.update_date,bug.create_date,stat.bug_status as bugStatus,priority.bug_priority,severity.bug_severity as bugSeverity,assignee.first_name as assigneeFN ,assignee.last_name as assigneeLN,reporter.first_name as reporterFN ,reporter.last_name as reporterLN, category.category_name, bug.module, moduleTable.module_name 
FROM xebt_bugsummary summary,xebt_bug bug ,xe_module as xemodule ,xebt_status as stat,xebt_severity as severity,xebt_priority as priority,xe_user_details as assignee , xe_user_details as reporter, xebt_category as category , xe_module as moduleTable
where bug.project= projectId and bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority and severity.severity_id= summary.bug_severity and assignee.user_id = summary.assignee and bug.created_by = reporter.user_id and bug.created_by = userId 
and bug.module = xemodule.module_id and xemodule.module_active = 1 and bug.module IN (select module_id from xe_user_module where user_id = userId) and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and summary.category = category.category_id and moduleTable.module_id = bug.module ORDER BY summary.update_date DESC LIMIT startValue, offsetValue;

SELECT count(*) INTO count FROM xebt_bugsummary summary,xebt_bug bug ,xe_module as xemodule
where bug.project= projectId and bug.bug_id = summary.bug_id and bug.created_by = userId 
and bug.module = xemodule.module_id and xemodule.module_active = 1 
and bug.module IN (select module_id from xe_user_module where user_id = userId) 
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id) ;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `createtestcase`(IN userId int,IN projectId int,IN moduleId int,IN scenarioId int)
BEGIN

SELECT module_id,module_name FROM xe_module 
WHERE module_id IN (SELECT module_id FROM xe_user_module where project_id=projectId and user_id = userId) 
and module_active = 1;

SELECT scenario_id, scenario_name, module_id, scenario_description FROM xetm_scenario 
where module_id = moduleId and status = 1;

SELECT execution_type_id, description FROM xetm_execution_type WHERE status=1;

SELECT tc_status_id, status FROM xetm_testcase_status WHERE testcase_state = 1;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertpagescandetails`(IN envId int,IN pageId int,IN functionalityId int,IN userId int, OUT scannerid int)
BEGIN

INSERT INTO `xesfdc_scanner` (`scanner_id`, `env`, `pagename_id`, `functionality_id`, `user_id`) 
VALUES (0, envId, pageId, functionalityId, userId);

Select max(scanner_id) INTO scannerid FROM `xesfdc_scanner` where env = envId and pagename_id = pageId and functionality_id = functionalityId;

UPDATE `xesfdc_pages` SET `is_scanned`='1' WHERE `page_id`=pageId and `env_id`=envId;

END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `sfdcCreateTCData`()
BEGIN
SELECT env_id, env_name FROM xesfdc_env where env_status=1;

SELECT functionality_id, functionality FROM xesfdc_func where func_status=1;

SELECT page_id, pagename FROM xesfdc_pages where page_status=1;
END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getBtDashLinkClosedBugs`(IN userId int, IN projectId int, IN startValue int, IN offsetValue int,OUT count int)
BEGIN

SELECT bug.bug_title, bug.bug_prefix, summary.bug_id, stat.bug_status, priority.bug_priority, severity.bug_severity,
category.category_name, CONCAT(assignee.first_name, ' ', assignee.last_name) as assignee ,
moduleTable.module_name, CONCAT(reporter.first_name, ' ' ,reporter.last_name) as reporter,
bug.assign_status, bug.project
FROM xebt_bugsummary as summary, xebt_status as stat, xebt_priority as priority, xebt_severity as severity,
xebt_category as category, xe_user_details as assignee, xebt_bug bug, xe_module as moduleTable, xe_user_details as reporter
WHERE summary.bug_status IN (5) 
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority
and severity.severity_id= summary.bug_severity and summary.category = category.category_id
and bug.bug_id = summary.bug_id and assignee.user_id = summary.assignee
and moduleTable.module_id = bug.module
and bug.module IN (select module_id from xe_user_module where user_id = userId and project_id = projectId)
and bug.created_by = reporter.user_id and bug.project = projectId
ORDER BY summary.update_date DESC LIMIT startValue, offsetValue;

SELECT count(*) INTO count
FROM xebt_bugsummary as summary, xebt_bug bug
WHERE summary.bug_status IN (5) and bug.bug_id = summary.bug_id
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and bug.module IN (select module_id from xe_user_module where user_id = userId and project_id = projectId)
and bug.project = projectId;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getBtDashLinkOpenBugs`(IN userId int, IN projectId int, IN startValue int, IN offsetValue int,OUT count int)
BEGIN

SELECT bug.bug_title, bug.bug_prefix, summary.bug_id, stat.bug_status, priority.bug_priority, severity.bug_severity,
category.category_name, CONCAT(assignee.first_name, ' ', assignee.last_name) as assignee ,
moduleTable.module_name, CONCAT(reporter.first_name, ' ' ,reporter.last_name) as reporter,
bug.assign_status, bug.project
FROM xebt_bugsummary as summary, xebt_status as stat, xebt_priority as priority, xebt_severity as severity,
xebt_category as category, xe_user_details as assignee, xebt_bug bug, xe_module as moduleTable, xe_user_details as reporter
WHERE summary.bug_status NOT IN (5) 
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority
and severity.severity_id= summary.bug_severity and summary.category = category.category_id
and bug.bug_id = summary.bug_id and assignee.user_id = summary.assignee
and moduleTable.module_id = bug.module
and bug.module IN (select module_id from xe_user_module where user_id = userId and project_id = projectId)
and bug.created_by = reporter.user_id and bug.project = projectId
ORDER BY summary.update_date DESC LIMIT startValue, offsetValue;

SELECT count(*) INTO count
FROM xebt_bugsummary as summary, xebt_bug bug
WHERE summary.bug_status NOT IN (5) and bug.bug_id = summary.bug_id
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and bug.module IN (select module_id from xe_user_module where user_id = userId and project_id = projectId)
and bug.project = projectId;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getBtDashLinkAssignedBugs`(IN userId int, IN projectId int, IN startValue int, IN offsetValue int,OUT count int)
BEGIN

SELECT bug.bug_title, bug.bug_prefix, summary.bug_id, stat.bug_status, priority.bug_priority, severity.bug_severity,
category.category_name, CONCAT(assignee.first_name, ' ', assignee.last_name) as assignee ,
moduleTable.module_name, CONCAT(reporter.first_name, ' ' ,reporter.last_name) as reporter,
bug.assign_status, bug.project
FROM xebt_bugsummary as summary, xebt_status as stat, xebt_priority as priority, xebt_severity as severity,
xebt_category as category, xe_user_details as assignee, xebt_bug bug, xe_module as moduleTable, xe_user_details as reporter
WHERE summary.bug_status NOT IN (5, 6) 
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority
and severity.severity_id= summary.bug_severity and summary.category = category.category_id
and bug.bug_id = summary.bug_id and assignee.user_id = summary.assignee
and moduleTable.module_id = bug.module
and bug.module IN (select module_id from xe_user_module where user_id = userId and project_id = projectId)
and bug.created_by = reporter.user_id and bug.project = projectId
and bug.assign_status = 1 and summary.assignee = userId
ORDER BY summary.update_date DESC LIMIT startValue, offsetValue;

SELECT count(*) INTO count
FROM xebt_bugsummary as summary, xebt_bug bug
WHERE summary.bug_status NOT IN (5, 6) and bug.bug_id = summary.bug_id
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and bug.module IN (select module_id from xe_user_module where user_id = userId and project_id = projectId)
and bug.project = projectId
and bug.assign_status = 1 and summary.assignee = userId;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getBtDashLinkImmediateBugs`(IN userId int, IN projectId int, IN startValue int, IN offsetValue int,OUT count int)
BEGIN

SELECT bug.bug_title, bug.bug_prefix, summary.bug_id, stat.bug_status, priority.bug_priority, severity.bug_severity,
category.category_name, CONCAT(assignee.first_name, ' ', assignee.last_name) as assignee ,
moduleTable.module_name, CONCAT(reporter.first_name, ' ' ,reporter.last_name) as reporter,
bug.assign_status, bug.project
FROM xebt_bugsummary as summary, xebt_status as stat, xebt_priority as priority, xebt_severity as severity,
xebt_category as category, xe_user_details as assignee, xebt_bug bug, xe_module as moduleTable, xe_user_details as reporter
WHERE summary.bug_status NOT IN (5) 
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority
and severity.severity_id= summary.bug_severity and summary.category = category.category_id
and bug.bug_id = summary.bug_id and assignee.user_id = summary.assignee
and moduleTable.module_id = bug.module
and bug.module IN (select module_id from xe_user_module where user_id = userId and project_id = projectId)
and bug.created_by = reporter.user_id and bug.project = projectId and summary.bug_priority IN (1) 
ORDER BY summary.update_date DESC LIMIT startValue, offsetValue;

SELECT count(*) INTO count
FROM xebt_bugsummary as summary, xebt_bug bug
WHERE summary.bug_status NOT IN (5) and bug.bug_id = summary.bug_id
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and bug.module IN (select module_id from xe_user_module where user_id = userId and project_id = projectId)
and bug.project = projectId and summary.bug_priority IN (1) ;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getBtDashLinkAllClosedBugs`(IN userId int, IN startValue int, IN offsetValue int,OUT count int)
BEGIN

SELECT bug.bug_title, bug.bug_prefix, summary.bug_id, stat.bug_status, priority.bug_priority, severity.bug_severity,
category.category_name, CONCAT(assignee.first_name, ' ', assignee.last_name) as assignee ,
moduleTable.module_name, CONCAT(reporter.first_name, ' ' ,reporter.last_name) as reporter,
bug.assign_status, bug.project
FROM xebt_bugsummary as summary, xebt_status as stat, xebt_priority as priority, xebt_severity as severity,
xebt_category as category, xe_user_details as assignee, xebt_bug bug, xe_module as moduleTable, xe_user_details as reporter
WHERE summary.bug_status IN (5) 
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority
and severity.severity_id= summary.bug_severity and summary.category = category.category_id
and bug.bug_id = summary.bug_id and assignee.user_id = summary.assignee
and moduleTable.module_id = bug.module
and bug.module IN (select module_id from xe_user_module where user_id = userId)
and bug.created_by = reporter.user_id 
ORDER BY summary.update_date DESC LIMIT startValue, offsetValue;

SELECT count(*) INTO count
FROM xebt_bugsummary as summary, xebt_bug bug
WHERE summary.bug_status IN (5) and bug.bug_id = summary.bug_id
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and bug.module IN (select module_id from xe_user_module where user_id = userId);

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getBtDashLinkAllOpenBugs`(IN userId int, IN startValue int, IN offsetValue int,OUT count int)
BEGIN

SELECT bug.bug_title, bug.bug_prefix, summary.bug_id, stat.bug_status, priority.bug_priority, severity.bug_severity,
category.category_name, CONCAT(assignee.first_name, ' ', assignee.last_name) as assignee ,
moduleTable.module_name, CONCAT(reporter.first_name, ' ' ,reporter.last_name) as reporter,
bug.assign_status, bug.project
FROM xebt_bugsummary as summary, xebt_status as stat, xebt_priority as priority, xebt_severity as severity,
xebt_category as category, xe_user_details as assignee, xebt_bug bug, xe_module as moduleTable, xe_user_details as reporter
WHERE summary.bug_status NOT IN (5) 
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority
and severity.severity_id= summary.bug_severity and summary.category = category.category_id
and bug.bug_id = summary.bug_id and assignee.user_id = summary.assignee
and moduleTable.module_id = bug.module
and bug.module IN (select module_id from xe_user_module where user_id = userId)
and bug.created_by = reporter.user_id
ORDER BY summary.update_date DESC LIMIT startValue, offsetValue;

SELECT count(*) INTO count
FROM xebt_bugsummary as summary, xebt_bug bug
WHERE summary.bug_status NOT IN (5) and bug.bug_id = summary.bug_id
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and bug.module IN (select module_id from xe_user_module where user_id = userId);

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getBtDashLinkAllAssignedBugs`(IN userId int, IN startValue int, IN offsetValue int,OUT count int)
BEGIN

SELECT bug.bug_title, bug.bug_prefix, summary.bug_id, stat.bug_status, priority.bug_priority, severity.bug_severity,
category.category_name, CONCAT(assignee.first_name, ' ', assignee.last_name) as assignee ,
moduleTable.module_name, CONCAT(reporter.first_name, ' ' ,reporter.last_name) as reporter,
bug.assign_status, bug.project
FROM xebt_bugsummary as summary, xebt_status as stat, xebt_priority as priority, xebt_severity as severity,
xebt_category as category, xe_user_details as assignee, xebt_bug bug, xe_module as moduleTable, xe_user_details as reporter
WHERE summary.bug_status NOT IN (5, 6) 
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority
and severity.severity_id= summary.bug_severity and summary.category = category.category_id
and bug.bug_id = summary.bug_id and assignee.user_id = summary.assignee
and moduleTable.module_id = bug.module
and bug.module IN (select module_id from xe_user_module where user_id = userId)
and bug.created_by = reporter.user_id 
and bug.assign_status = 1 and summary.assignee = userId
ORDER BY summary.update_date DESC LIMIT startValue, offsetValue;

SELECT count(*) INTO count
FROM xebt_bugsummary as summary, xebt_bug bug
WHERE summary.bug_status NOT IN (5, 6) and bug.bug_id = summary.bug_id
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and bug.module IN (select module_id from xe_user_module where user_id = userId)
and bug.assign_status = 1 and summary.assignee = userId;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `getBtDashLinkAllImmediateBugs`(IN userId int, IN startValue int, IN offsetValue int,OUT count int)
BEGIN

SELECT bug.bug_title, bug.bug_prefix, summary.bug_id, stat.bug_status, priority.bug_priority, severity.bug_severity,
category.category_name, CONCAT(assignee.first_name, ' ', assignee.last_name) as assignee ,
moduleTable.module_name, CONCAT(reporter.first_name, ' ' ,reporter.last_name) as reporter,
bug.assign_status, bug.project
FROM xebt_bugsummary as summary, xebt_status as stat, xebt_priority as priority, xebt_severity as severity,
xebt_category as category, xe_user_details as assignee, xebt_bug bug, xe_module as moduleTable, xe_user_details as reporter
WHERE summary.bug_status NOT IN (5) 
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority
and severity.severity_id= summary.bug_severity and summary.category = category.category_id
and bug.bug_id = summary.bug_id and assignee.user_id = summary.assignee
and moduleTable.module_id = bug.module
and bug.module IN (select module_id from xe_user_module where user_id = userId)
and bug.created_by = reporter.user_id and summary.bug_priority IN (1) 
ORDER BY summary.update_date DESC LIMIT startValue, offsetValue;

SELECT count(*) INTO count
FROM xebt_bugsummary as summary, xebt_bug bug
WHERE summary.bug_status NOT IN (5) and bug.bug_id = summary.bug_id
and summary.bs_id in (select max(bs_id) from xebt_bugsummary group by bug_id)
and bug.module IN (select module_id from xe_user_module where user_id = userId)
and summary.bug_priority IN (1) ;

END;;

CREATE DEFINER=`xenon`@`localhost` PROCEDURE `buildReportData`(IN automationStatus int, IN scriptlessStatus int, IN manualStartValue int, IN autoStartValue int, IN scStartValue int, IN pageSize int, OUT manualbuildcount INT, OUT autobuildcount int, OUT scbuildcount int)
BEGIN

	SELECT build_id,build_name,build_desc,a1.status as buildStatus, 
	r1.release_name as releaseName,build_createdate,state.desc as buildState,build_state,Concat(user.first_name ,' ', user.last_name) as creator,user.user_photo,user.about_me
	FROM xe_build as build
	INNER JOIN xe_active as a1 ON a1.active_id = build.build_status
	INNER JOIN xe_release as r1 on r1.release_id = build.release_id
	inner join xebm_build_state as state on state.id=build.build_state 
	inner join xe_user_details as user on user.user_id = build.build_creator where (state.id=4 or state.id=5) and r1.release_active = 1
	ORDER BY build.build_id DESC LIMIT manualStartValue, pageSize;
	
	SELECT count(*) INTO manualbuildcount FROM xe_build as build
	INNER JOIN xe_release as r1 on r1.release_id = build.release_id
	inner join xebm_build_state as state on state.id=build.build_state 
	where (state.id=4 or state.id=5) and r1.release_active = 1;

IF automationStatus = 1 THEN
	SELECT build.build_id,build_name,build_desc,build_logpath,a1.status as buildStatus, 
	r1.release_name as releaseName,build_createdate,state.desc as buildState,
    build_state,Concat(user.first_name ,' ', user.last_name) as creator,user.user_photo,user.about_me,
    TIMESTAMPDIFF(second,B1.start_time,b1.end_time) AS ExecTimeSec
	FROM xe_automation_build as build
	LEFT JOIN xebm_build_exec_details as b1 on b1.build_id = build.build_id
	INNER JOIN xe_active as a1 ON a1.active_id = build.build_status
	INNER JOIN xe_release as r1 on r1.release_id = build.release_id
	inner join xebm_build_state as state on state.id=build.build_state 
	inner join xe_user_details as user on user.user_id = build.build_creator 
   
	where (state.id=4 or state.id=5) and r1.release_active = 1 ORDER BY build.build_id DESC LIMIT autoStartValue, pageSize;

	SELECT count(*) INTO autobuildcount FROM xe_automation_build as build
	INNER JOIN xe_release as r1 on r1.release_id = build.release_id
	inner join xebm_build_state as state on state.id=build.build_state 
	where (state.id=4 or state.id=5) and r1.release_active = 1;
END IF; 

IF scriptlessStatus = 1 THEN	
	SELECT build_id,build_name,build_desc,build_logpath,a1.status as buildStatus, 
	r1.release_name as releaseName,build_createdate,state.desc as buildState,build_state,Concat(user.first_name ,' ', user.last_name) as creator,user.user_photo,user.about_me
	FROM xesfdc_automation_build as build
	INNER JOIN xe_active as a1 ON a1.active_id = build.build_status
	INNER JOIN xe_release as r1 on r1.release_id = build.release_id
	inner join xebm_build_state as state on state.id=build.build_state 
	inner join xe_user_details as user on user.user_id = build.build_creator where (state.id=4 or state.id=5) and r1.release_active = 1
	ORDER BY build.build_id DESC LIMIT scStartValue, pageSize;
	
	
	SELECT count(*) INTO scbuildcount FROM xesfdc_automation_build as build
	INNER JOIN xe_release as r1 on r1.release_id = build.release_id
	inner join xebm_build_state as state on state.id=build.build_state 
	where (state.id=4 or state.id=5) and r1.release_active = 1;

END IF;    
 
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertTestCaseDetails`(IN buildId int,IN testCaseId VARCHAR(1500))
BEGIN
 	DECLARE testCaseStatus INT DEFAULT 5;
 	SELECT @projectId:=M.project_id,@moduleId:=S.module_id,@sceId:=T.scenario_id,@testCase:=T.testcase_id FROM xe_module M,xetm_scenario S,xetm_testcase T where T.test_prefix=testCaseId and T.scenario_id=S.scenario_id and M.module_id=S.module_id;
    
     
        DELETE  FROM xebm_automation_execution_summary 
        WHERE build=buildId AND project=@projectId AND module=@moduleId AND scenario=@sceId AND xebm_automation_execution_summary.testcase=@testCase ;
       
		INSERT INTO xebm_automation_execution_summary (build,project,module,scenario,testcase,testcase_status,tc_start_time)
								VALUES (buildId,@projectId,@moduleId,@sceId,@testCase,testCaseStatus,now()); 
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `checkBTAccess`(IN roleAccess VARCHAR(45),IN roleId VARCHAR(45))
BEGIN
	 SET @s=CONCAT('Select ',roleAccess,' from  xe_bt_access Where ',roleAccess,'= \'1\' and roleID=',roleId,'');
	PREPARE stmt1 FROM @s;
	EXECUTE stmt1;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `checkBMAccess`(IN roleAccess VARCHAR(45),IN roleId VARCHAR(45))
BEGIN
	 SET @s=CONCAT('Select ',roleAccess,' from  xe_bm_access Where ',roleAccess,'= \'1\' and role_id=',roleId,'');
	PREPARE stmt1 FROM @s;
	EXECUTE stmt1;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `checkTMAccess`(IN roleAccess VARCHAR(45),IN roleId VARCHAR(45))
BEGIN
	SET @s=CONCAT('Select ',roleAccess,' from  xe_tm_access Where ',roleAccess,'= \'1\' and roleID=',roleId,'');
	PREPARE stmt1 FROM @s;
	EXECUTE stmt1;
END;;	


CREATE DEFINER=`root`@`localhost` PROCEDURE `getLoginUserDetails`(IN userName VARCHAR(45))
BEGIN
SELECT ud.user_id,ud.user_password,ud.email_id,ud.first_name,ud.last_name,@roleId := ud.role_id as role_id,ud.tm_status,ud.bt_status,ud.ldap_status,ud.user_status,ud.user_photo,
ud.location,ud.about_me,ud.pass_flag,ro.role_status,ro.role_type,tz.timezone_id 
	FROM xe_user_details as ud ,xe_role as ro , xe_timezone_details as tz
    where  ud.user_name=userName and ro.role_id = ud.role_id and ud.user_status =1 and ud.user_timezone=tz.xe_timezone_id;	
	SELECT * from xe_bm_access where role_id = @roleId;
	
    SELECT * from xe_tm_access where roleID = @roleId;
    
    SELECT * from xe_bt_access where roleID = @roleId;
    
    SELECT * FROM xe_support_access  where role_id = @roleId;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `mapXenonUserToLdapUser`(In xenonUser VARCHAR(200),IN ldapUser VARCHAR(200),IN custSchema VARCHAR(200))
BEGIN

UPDATE `xenon-core`.`xe_user` SET `user_name`=ldapUser WHERE `user_name`=xenonUser;

SET @queryToExec= CONCAT('UPDATE `',custSchema,'`.xe_user_details SET user_name="',ldapUser,'",ldap_status=1,user_status=1 WHERE user_name="',xenonUser,'"');

PREPARE stmt FROM @queryToExec;
  EXECUTE stmt; 
DEALLOCATE PREPARE stmt;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateLdapAdminUserDetails`(
userName VARCHAR(100))
BEGIN
DECLARE userMail VARCHAR(100);

SELECT email_id INTO userMail FROM xe_user_details where role_id=2;

UPDATE xe_user_details SET ldap_status=2;

UPDATE xe_user_details SET `user_name`=userName, ldap_status=1  WHERE `email_id`=userMail;

UPDATE `xenon-core`.xe_user SET `user_name`=userName  WHERE `user_email`=userMail;


END;;

CREATE DEFINER=`xenon`@`localhost` FUNCTION `insertCustomSearchQuery`(
queryName LONGTEXT,
queryString LONGTEXT,
queryJson LONGTEXT,
userId INT(11)
) RETURNS int(11)
BEGIN
DECLARE insertStatus INT;
SET insertStatus=-1;
    
	INSERT INTO `xebt_customizesearch` (`queryname`,`query`,`queryjson`,`user_id`) VALUES (queryName,queryString,queryJson,userId);
	SET insertStatus=1;

RETURN insertStatus;
END;;

CREATE DEFINER=`xenon`@`localhost` FUNCTION `insertPiechartCustomSearchQuery`(
queryName LONGTEXT,
queryString LONGTEXT,
queryJson LONGTEXT,
userId INT(11)
) RETURNS int(11)
BEGIN
DECLARE insertStatus INT;
SET insertStatus=-1;
    
              INSERT INTO `xebt_piechartsearch` (`queryname`,`query`,`queryjson`,`user_id`) VALUES (queryName,queryString,queryJson,userId);
              SET insertStatus=1;

RETURN insertStatus;
END;;

CREATE PROCEDURE `insert_token_procedure` (IN instance_id1 int,IN cust_id int,IN user_id int,IN token1 longtext,IN windows_script longtext,IN powershell_script longtext)
BEGIN
DECLARE instanceCount INT(11);
select count(*) into instanceCount from xead_jenkins_api_token_details where instance_id = instance_id1;
if instanceCount = 0 then
insert into xead_jenkins_api_token_details (instance_id,cust_id,user_id,token,windows_script,powershell_script) values (instance_id1,cust_id,user_id,token1,windows_script,powershell_script);
else
update xead_jenkins_api_token_details set token = token1 where instance_id = instance_id1;
END IF;
END;;

CREATE PROCEDURE `jenkinInstanceSettings`(IN instanceId int)
BEGIN
	 SELECT ud.user_id, user_photo, first_name, last_name, email_id,uj.update_status,uj.view_status
	 FROM xe_user_details as ud,xead_user_jenkins_instance as uj where ud.user_id = uj.user_id and uj.instance_id = instanceId
	 and ud.user_status = 1;

	 SELECT user_id, user_photo, first_name, last_name, email_id
	 FROM xe_user_details as ud where user_id not in 
	 (SELECT user_id FROM xead_user_jenkins_instance where instance_id in (instanceId)) and ud.user_status = 1;
	
	SELECT user_id, user_photo, first_name, last_name, email_id
	 FROM xe_user_details as ud where user_id in 
	 (SELECT user_id FROM xead_user_jenkins_instance where update_status =1 and view_status !=1 and instance_id in (instanceId)) and ud.user_status = 1;

	SELECT user_id, user_photo, first_name, last_name, email_id
	 FROM xe_user_details as ud where user_id in 
	 (SELECT user_id FROM xead_user_jenkins_instance where view_status =1 and update_status !=1 and instance_id in (instanceId)) and ud.user_status = 1;

	SELECT user_id, user_photo, first_name, last_name, email_id
	 FROM xe_user_details as ud where user_id in 
	 (SELECT user_id FROM xead_user_jenkins_instance where view_status !=1 and update_status !=1 and instance_id in (instanceId)) and ud.user_status = 1;

	SELECT * from xead_jenkins_details where jenkin_id = instanceId;
END;;

CREATE PROCEDURE `gitInstanceSettings`(IN instanceId int)
BEGIN
	 SELECT ud.user_id, user_photo, first_name, last_name, email_id,uj.update_status,uj.view_status
	 FROM xe_user_details as ud,xead_user_git_instance as uj where ud.user_id = uj.user_id and uj.instance_id = instanceId
	 and ud.user_status = 1;

	 SELECT user_id, user_photo, first_name, last_name, email_id
	 FROM xe_user_details as ud where user_id not in 
	 (SELECT user_id FROM xead_user_git_instance where instance_id in (instanceId)) and ud.user_status = 1;
	
	SELECT user_id, user_photo, first_name, last_name, email_id
	 FROM xe_user_details as ud where user_id in 
	 (SELECT user_id FROM xead_user_git_instance where update_status =1 and view_status !=1 and instance_id in (instanceId)) and ud.user_status = 1;

	SELECT user_id, user_photo, first_name, last_name, email_id
	 FROM xe_user_details as ud where user_id in 
	 (SELECT user_id FROM xead_user_git_instance where view_status =1 and update_status !=1 and instance_id in (instanceId)) and ud.user_status = 1;

	SELECT user_id, user_photo, first_name, last_name, email_id
	 FROM xe_user_details as ud where user_id in 
	 (SELECT user_id FROM xead_user_git_instance where view_status !=1 and update_status !=1 and instance_id in (instanceId)) and ud.user_status = 1;

	SELECT * from xead_git_project_details where git_instance_id = instanceId;
END;;

CREATE PROCEDURE `jenkinJobSettings`(IN jobId int)
BEGIN
	 SELECT ud.user_id, user_photo, first_name, last_name, email_id,uj.update_status,uj.execute_status
	 FROM xe_user_details as ud,xead_user_jobs as uj where ud.user_id = uj.user_id and uj.job_id = jobId
	 and ud.user_status = 1;

	 SELECT user_id, user_photo, first_name, last_name, email_id
	 FROM xe_user_details as ud where user_id not in 
	 (SELECT user_id FROM xead_user_jobs where job_id in (jobId)) and ud.user_status = 1;
	
	SELECT user_id, user_photo, first_name, last_name, email_id
	 FROM xe_user_details as ud where user_id in 
	 (SELECT user_id FROM xead_user_jobs where update_status =1 and execute_status !=1 and job_id in (jobId)) and ud.user_status = 1;

	SELECT user_id, user_photo, first_name, last_name, email_id
	 FROM xe_user_details as ud where user_id in 
	 (SELECT user_id FROM xead_user_jobs where execute_status =1 and update_status !=1 and job_id in (jobId)) and ud.user_status = 1;

	SELECT user_id, user_photo, first_name, last_name, email_id
	 FROM xe_user_details as ud where user_id in 
	 (SELECT user_id FROM xead_user_jobs where execute_status !=1 and update_status !=1 and job_id in (jobId)) and ud.user_status = 1;
END;;

CREATE TRIGGER default_instance_permission_insertion_trigger
AFTER INSERT
   ON xead_jenkins_details FOR EACH ROW
BEGIN
	declare instance_id1 int;
	declare admin_user_id int;

	select user_id into admin_user_id from xe_user_details where role_id= '2';
	select max(jenkin_id) into instance_id1 from xead_jenkins_details;
	insert into xead_user_jenkins_instance(user_id,instance_id,update_status,view_status) values(admin_user_id,instance_id1,'1','2');	
END;;


CREATE TRIGGER deafult_git_instance_permission_insertion_trigger
AFTER INSERT
   ON xead_git_project_details FOR EACH ROW
BEGIN
	declare instance_id1 int;
	declare admin_user_id int;

	select user_id into admin_user_id from xe_user_details where role_id= '2';
	select max(git_instance_id) into instance_id1 from xead_git_project_details;
	insert into xead_user_git_instance(user_id,instance_id,update_status,view_status) values(admin_user_id,instance_id1,'1','2');	
END;;

CREATE TRIGGER default_job_permission_insertion_trigger
AFTER INSERT
   ON xead_jenkin_jobs FOR EACH ROW
BEGIN
	declare user_id1 int;
	declare job_id1 int;
	declare admin_user_id int;

	select user_id into admin_user_id from xe_user_details where role_id= '2';
	select xjj.jenkin_jobId, xjj.user_id into job_id1, user_id1 from xead_jenkin_jobs as xjj where jenkin_jobId in (select max(jenkin_jobId) from xead_jenkin_jobs);
	if user_id1=admin_user_id then
		insert into xead_user_jobs(user_id,job_id,update_status,execute_status) values(user_id1,job_id1,'1','1');
	else
		insert into xead_user_jobs(user_id,job_id,update_status,execute_status) values(user_id1,job_id1,'2','1');
		insert into xead_user_jobs(user_id,job_id,update_status,execute_status) values(admin_user_id,job_id1,'1','1');
	end if;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `dataset`(IN userId INT,IN testCaseId VARCHAR(10),IN buildID INT)
BEGIN
	 SELECT project,project.project_name,project.project_description,project.project_createdate,project.project_active,count(*) ExecutedTcCount,
                SUM(case when testcase_status = 1 then 1 else 0 end) as PassCount,
    SUM(case when testcase_status = 2 then 1 else 0 end) as FailCount,SUM(case when testcase_status = 3 then 1 else 0 end) as SkipCount,
    SUM(case when testcase_status = 4 then 1 else 0 end) as BlockCount 
                FROM xebm_automation_execution_summary as trialSummary,xe_project as project
                where trialSummary.build=buildID and trialSummary.project=project.project_id
                and trialSummary.project in (SELECT project_id FROM xe_user_project where user = userId)
                GROUP BY trialSummary.project;

SELECT *,testCaseId AS testcase_id  FROM xetm_tc_dataset xtd,xetm_exec_status xes WHERE xes.exst_id=xtd.status AND tc_exe_id IN (SELECT exe_id FROM xebm_automation_execution_summary where build=buildId);

select build_id,trial_step_id,testcase_id,step_id,step_details,step_value,step_status,screenshot,screenshot_title,actual_result,is_bug_raised,statusTable.description,dataset_id
from xebm_automation_steps_execution_summary as trialSummary,xetm_exec_status as statusTable
where build_id = buildId and statusTable.exst_id =trialSummary.step_status and testcase_id=testCaseId;

SELECT step_exe_bug.test_step_id, step_exe_bug.test_case_id, step_exe_bug.build_id,xe_bug.bug_id,xe_bug.bug_prefix FROM xebt_bug xe_bug
JOIN 
(SELECT * FROM xe_automation_step_execution_bug step_bug) AS step_exe_bug ON  xe_bug.bug_id=step_exe_bug.bug_id;

SELECT project,module,module.module_name,count(*) ExecutedTcCount,SUM(case when testcase_status = 1 then 1 else 0 end) as PassCount,
    SUM(case when testcase_status = 2 then 1 else 0 end) as FailCount,SUM(case when testcase_status = 3 then 1 else 0 end) as SkipCount,
    SUM(case when testcase_status = 4 then 1 else 0 end) as BlockCount 
                FROM xebm_automation_execution_summary as trialSummary,xe_module as module
                where trialSummary.build=buildId and trialSummary.module=module.module_id
                and trialSummary.module in (SELECT module_id FROM xe_user_module where user_id = userId)
                GROUP BY trialSummary.module; 
                
 SELECT project,module,scenario,scenario.scenario_name,count(*) ExecutedTcCount,SUM(case when testcase_status = 1 then 1 else 0 end) as PassCount,
    SUM(case when testcase_status = 2 then 1 else 0 end) as FailCount,SUM(case when testcase_status = 3 then 1 else 0 end) as SkipCount,
    SUM(case when testcase_status = 4 then 1 else 0 end) as BlockCount 
                FROM xebm_automation_execution_summary as trialSummary,xetm_scenario as scenario
                where trialSummary.build=buildId and trialSummary.scenario=scenario.scenario_id
                and trialSummary.module in (SELECT module_id FROM xe_user_module where user_id = userId)
                GROUP BY trialSummary.scenario;

END;;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: xenon-core
-- ------------------------------------------------------
-- Server version	5.6.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `xe_active`
--

DROP TABLE IF EXISTS `xe_active`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_active` (
  `active_id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`active_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_customer_details`
--

DROP TABLE IF EXISTS `xe_customer_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_customer_details` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(500) DEFAULT NULL,
  `customer_address` varchar(1000) DEFAULT NULL,
  `customer_logo` longblob,
  `customer_type` varchar(500) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `tm_status` int(11) DEFAULT NULL,
  `bt_status` int(11) DEFAULT NULL,
  `scriptless_status` int(11) DEFAULT '2',
  `on_premise` int(11) DEFAULT '2',
  `ldap_status` int(11) DEFAULT '2',
  `jenkin_status` int(11) DEFAULT '2',
  `git_status` int(11) DEFAULT '2',
  `customer_status` int(11) DEFAULT NULL,
  `customer_schema` varchar(45) DEFAULT NULL,
  `automation_status` int(11) DEFAULT '2',
  `upgrade_schema` int(11) DEFAULT '1',
  PRIMARY KEY (`customer_id`),
  KEY `on_premiseFK_idx` (`on_premise`),
  KEY `scriptlessFKCust_idx` (`scriptless_status`),
  KEY `ldapStatusFk_idx` (`ldap_status`),
  KEY `gitfkcust_idx` (`git_status`),
  KEY `jenkinFkcust_idx` (`jenkin_status`),
  CONSTRAINT `gitfkcust` FOREIGN KEY (`git_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jenkinFkcust` FOREIGN KEY (`jenkin_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ldapStatusFk` FOREIGN KEY (`ldap_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `on_premiseFK` FOREIGN KEY (`on_premise`) REFERENCES `xe_active` (`active_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `scriptlessFKCust` FOREIGN KEY (`scriptless_status`) REFERENCES `xe_active` (`active_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_customer_type`
--

DROP TABLE IF EXISTS `xe_customer_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_customer_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_type` varchar(45) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_status_idx` (`status`),
  CONSTRAINT `type_status` FOREIGN KEY (`status`) REFERENCES `xe_active` (`active_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_issue_tracker_type`
--

DROP TABLE IF EXISTS `xe_issue_tracker_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_issue_tracker_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_tracker_type` varchar(45) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tracker_type_status_idx` (`status`),
  CONSTRAINT `tracker_type_status_idx` FOREIGN KEY (`status`) REFERENCES `xe_active` (`active_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_licence_details`
--

DROP TABLE IF EXISTS `xe_licence_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_licence_details` (
  `id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `no_of_days` int(11) DEFAULT NULL,
  `users_count` int(11) DEFAULT NULL,
  `projects_count` int(11) DEFAULT NULL,
  `modules_count` int(11) DEFAULT NULL,
  `scenario_count` int(11) DEFAULT NULL,
  `testcases_count` int(11) DEFAULT NULL,
  `steps_count` int(11) DEFAULT NULL,
  `builds_count` int(11) DEFAULT NULL,
  `bugs_count` int(11) DEFAULT NULL,
  `comments_count` int(11) DEFAULT NULL,
  `max_cloud_space` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_idx` (`type_id`),
  CONSTRAINT `type` FOREIGN KEY (`type_id`) REFERENCES `xe_customer_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xe_user`
--

DROP TABLE IF EXISTS `xe_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(60) DEFAULT NULL,
  `user_email` varchar(45) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `custoner_idx` (`customer_id`),
  CONSTRAINT `customer_id` FOREIGN KEY (`customer_id`) REFERENCES `xe_customer_details` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: xenon-core
-- ------------------------------------------------------
-- Server version	5.6.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `xe_active`
--

LOCK TABLES `xe_active` WRITE;
/*!40000 ALTER TABLE `xe_active` DISABLE KEYS */;
INSERT INTO `xe_active` VALUES (1,'Active'),(2,'Inactive');
/*!40000 ALTER TABLE `xe_active` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `xe_customer_details`
--

LOCK TABLES `xe_customer_details` WRITE;
/*!40000 ALTER TABLE `xe_customer_details` DISABLE KEYS */;
INSERT INTO `xe_customer_details` VALUES (1,'Xenon','USA','','1','2015-01-01 00:00:00','2015-01-01 00:00:00','2050-01-01 00:00:00',2,2,2,2,2,2,2,1,'xenon',1,0);
UNLOCK TABLES;


LOCK TABLES `xe_customer_type` WRITE;
/*!40000 ALTER TABLE `xe_customer_type` DISABLE KEYS */;
INSERT INTO `xe_customer_type` VALUES (1,'Trial',1),(2,'Silver',1),(3,'Gold',1),(4,'Platnium',1);
/*!40000 ALTER TABLE `xe_customer_type` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `xe_licence_details` WRITE;
/*!40000 ALTER TABLE `xe_licence_details` DISABLE KEYS */;
INSERT INTO `xe_licence_details` VALUES (1,1,7,10,10,10,10,50,500,30,50,200,25),(2,2,50,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,25),(3,3,100,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,25),(4,4,30,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,25);
/*!40000 ALTER TABLE `xe_licence_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `xe_user`
--

LOCK TABLES `xe_user` WRITE;
/*!40000 ALTER TABLE `xe_user` DISABLE KEYS */;
INSERT INTO `xe_user` VALUES (1,'xenon.support@jadeglobal.com','xenon.support@jadeglobal.com',1),(2,'admin@jadeglobal.com','admin@jadeglobal.com',1);
/*!40000 ALTER TABLE `xe_user` ENABLE KEYS */;
UNLOCK TABLES;
Update xe_customer_details SET customer_logo=LOAD_FILE('C:\\repository\\logo\\xenon.png') where customer_id=1;


DROP TABLE IF EXISTS `xebm_execution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xebm_execution` (
  `build_no` int(11) NOT NULL AUTO_INCREMENT,
  `build_stamp` varchar(200) DEFAULT NULL,
  `schema_name` varchar(200) DEFAULT NULL,
  `build_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`build_no`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


CREATE TABLE `xesfdc_execution` (
  `build_no` int(11) NOT NULL AUTO_INCREMENT,
  `build_stamp` varchar(200) DEFAULT NULL,
  `schema_name` varchar(200) DEFAULT NULL,
  `build_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`build_no`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `xe_ldap_settings`;

CREATE TABLE `xe_ldap_settings` (
  `ldap_id` int(11) NOT NULL AUTO_INCREMENT,
  `ldap_url` varchar(100) DEFAULT NULL,
  `root_dn` varchar(500) DEFAULT NULL,
  `user_search_base` varchar(500) DEFAULT NULL,
  `user_search_filter` varchar(500) DEFAULT NULL,
  `user_dn` varchar(500) DEFAULT NULL,
  `user_mail` varchar(100) DEFAULT NULL,
  `user_password` varchar(500) DEFAULT NULL,
  `display_name_attribute` varchar(45) DEFAULT NULL,
  `email_addr_attribute` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `email_domain_name` varchar(100) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ldap_id`),
  KEY `custFk_idx` (`customer_id`),
  CONSTRAINT `custFk` FOREIGN KEY (`customer_id`) REFERENCES `xe_customer_details` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `xead_jenkins_api_token_details`;

CREATE TABLE `xead_jenkins_api_token_details` (
  `token_id` INT NOT NULL AUTO_INCREMENT,
  `instance_id` INT NOT NULL,
  `cust_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `token` LONGTEXT NULL,
  `windows_script` LONGTEXT NULL,
  `powershell_script` LONGTEXT NULL,
  PRIMARY KEY (`token_id`));
CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewArticle`(
articleName varchar(45),
articleDescription LONGTEXT,
customerId INT,
userId INT,
subcomponent_id INT
) RETURNS int(11)
BEGIN
DECLARE count,insertStatus INT DEFAULT 0;
SET insertStatus=-1;
    
    SELECT count(*) INTO count FROM xes_article where art_name=articleName;
          
        IF count=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF count=0 THEN 
        INSERT INTO `xenonsupport`.`xes_article` 
        ( `art_name`, `art_desc`, `created_date`, `updated_date`, `customer_id`, `user_id`, `subcomponent_id`, `article_status`) 
        VALUES (articleName, articleDescription, NOW(), NOW(), customerId, userId, subcomponent_id, 1);
        SET insertStatus=1;
                               
        END IF;
RETURN insertStatus;
END ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getComponentData`(	IN componentId INT)
BEGIN

SELECT * FROM xenonsupport.xes_subcomponents where component_id=componentId;

SELECT id,art_name,art_desc,created_date,updated_date,customer_id,user_id,subcomponent_id,article_status,
TIMESTAMPDIFF(MINUTE,created_date,CURRENT_TIME()) AS TimeDiff FROM xenonsupport.xes_article ORDER BY subcomponent_id,updated_date DESC;

END;;

CREATE DEFINER=`xenon`@`localhost` FUNCTION `createNewSubcomponent`(
subcomponentName varchar(45),
subcomponentDescription LONGTEXT,
componentId INT
) RETURNS int(11)
BEGIN
DECLARE count,insertStatus INT DEFAULT 0;
SET insertStatus=-1;
    
    SELECT count(*) INTO count FROM xes_subcomponents where subcmp_name=subcomponentName;
          
        IF count=1 THEN 
             SET insertStatus=0;
       
		END IF;
         
        IF count=0 THEN 
        INSERT INTO `xenonsupport`.`xes_subcomponents` (`subcmp_name`, `subcmp_desc`, `component_id`)
        VALUES ( subcomponentName, subcomponentDescription, componentId);
                
        SET insertStatus=1;
                               
        END IF;
RETURN insertStatus;
END;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `getHelpData`()
BEGIN

SELECT id,cmp_name,cmp_desc FROM xenonsupport.xes_component;

SELECT faq_id,title,description,create_date,update_date,faq_creator
From xenonsupport.xes_faq ORDER BY faq_id DESC LIMIT 5;

END;;
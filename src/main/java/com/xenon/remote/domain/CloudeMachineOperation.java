package com.xenon.remote.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xenon.remote.dao.CommonAdapterImpl;
import com.xenon.remote.dao.RemoteOperation;

public class CloudeMachineOperation extends CommonAdapterImpl implements RemoteOperation {
	private static final Logger logger = LoggerFactory.getLogger(CloudeMachineOperation.class);
	private static CloudeMachineOperation INSTANCE;

	private CloudeMachineOperation() {
	}

	@Override
	public String getRemoteMachineStatus(String remoteURL) throws RemoteMachineException {
		String statusResponse = null;
		if (isValid(remoteURL)) {
			statusResponse = super.getRemoteMachineStatus(remoteURL);
		} else {
			statusResponse = HTTP_STATUS_INVALID_URL;
			logger.error("Resource URL not valid :" + remoteURL);
		}
		return statusResponse;
	}

	@Override
	public String postRequest(String remoteURL, String remoteParameter) throws RemoteMachineException {
		String statusResponse = null;
		if (isValid(remoteURL)) {
			statusResponse = super.postRequest(remoteURL, remoteParameter);
		} else {
			statusResponse = HTTP_STATUS_INVALID_URL;
			logger.error("Resource URL not valid :" + remoteURL);
		}
		return statusResponse;
	}

	@Override
	public String getRequest(String remoteURL, String remoteParameter) throws RemoteMachineException {
		String statusResponse = null;
		if (isValid(remoteURL)) {
			statusResponse = super.getRequest(remoteURL, remoteParameter);
		} else {
			statusResponse = HTTP_STATUS_INVALID_URL;
			logger.error("Resource URL not valid :" + remoteURL);
		}
		return statusResponse;
	}

	@Override
	public String handleRemoteStatus(RemoteOperation remoteOperation,String xenonServerWS) throws RemoteMachineException {
		String responseStatus = null;
		responseStatus = super.handleRemoteStatus(remoteOperation,xenonServerWS);
		return responseStatus;
	}

	@Override
	public String getTimeStamp() {
		return super.getTimeStamp();
	}

	@Override
	public boolean createDirectory(String dirName) throws RemoteMachineException {
		return super.createDirectory(dirName);
	}

	@Override
	public boolean createFile(String filePath) throws RemoteMachineException {
		return super.createFile(filePath);
	}


	/**
	 * 
	 * @return
	 */
	public static CloudeMachineOperation getInstance() {
		if (INSTANCE == null) {
			synchronized (CloudeMachineOperation.class) {
				if (INSTANCE == null) {
					INSTANCE = new CloudeMachineOperation();
				}
			}
		}
		return INSTANCE;
	}
	
	@Override
	public boolean createDirectory(String dirName,boolean flag) throws RemoteMachineException {
		return super.createDirectory(dirName,flag);
	}
	
	@Override
	public boolean createFile(String filePath,boolean flag) throws RemoteMachineException {
		return super.createFile(filePath,flag);
	}

}

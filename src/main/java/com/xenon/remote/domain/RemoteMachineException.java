package com.xenon.remote.domain;

public class RemoteMachineException extends Exception {

	private static final long serialVersionUID = 4664456874499611218L;

	private String errorMessage;;

	public RemoteMachineException(String exception, String errorMessage) {
		super(exception);
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return this.errorMessage;
	}

}
package com.xenon.remote.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import com.xenon.remote.dao.CommonAdapterImpl;
import com.xenon.remote.dao.RemoteOperation;

@Async
public class CustomerMachineOperation extends CommonAdapterImpl implements RemoteOperation {
	private static final Logger logger = LoggerFactory.getLogger(CustomerMachineOperation.class);
	private static CustomerMachineOperation INSTANCE;

	private CustomerMachineOperation() {
	}

	@Async
	@Override
	public String getRemoteMachineStatus(String remoteURL) throws RemoteMachineException {
		String statusResponse = null;
		if (isValid(remoteURL)) {
			statusResponse = super.getRemoteMachineStatus(remoteURL);
		} else {
			statusResponse = HTTP_STATUS_INVALID_URL;
			logger.error("Resource URL not valid :" + remoteURL);
		}
		return statusResponse;
	}

	@Override
	public String postRequest(String remoteURL, String remoteParameter) throws RemoteMachineException {
		String statusResponse = null;
		if (isValid(remoteURL)) {
			statusResponse = super.postRequest(remoteURL, remoteParameter);
		} else {
			statusResponse = HTTP_STATUS_INVALID_URL;
			logger.error("Resource URL not valid :" + remoteURL);
		}
		return statusResponse;
	}

	@Override
	public String getRequest(String remoteURL, String remoteParameter) throws RemoteMachineException {
		String statusResponse = null;
		if (isValid(remoteURL)) {
			statusResponse = super.getRequest(remoteURL, remoteParameter);
		} else {
			statusResponse = HTTP_STATUS_INVALID_URL;
			logger.error("Resource URL not valid :" + remoteURL);
		}
		return statusResponse;
	}

	@Override
	public String handleRemoteStatus(RemoteOperation remoteOperation,String xenonServerWS) throws RemoteMachineException {
		String responseStatus = null;
		responseStatus = super.handleRemoteStatus(remoteOperation,xenonServerWS);
		return responseStatus;
	}

	@Override
	public String getTimeStamp() {
		return super.getTimeStamp();
	}

	@Override
	public boolean createDirectory(String dirName) throws RemoteMachineException {
		return super.createDirectory(dirName);
	}

	@Override
	public boolean createFile(String filePath) throws RemoteMachineException {
		return super.createFile(filePath);
	}

	public static CustomerMachineOperation getInstance() {
		if (INSTANCE == null) {
			synchronized (CustomerMachineOperation.class) {
				if (INSTANCE == null) {
					INSTANCE = new CustomerMachineOperation();
				}
			}
		}
		return INSTANCE;
	}
}

package com.xenon.remote.dao;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Async;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.xenon.controller.utils.Constants;
import com.xenon.remote.domain.RemoteMachineException;

@Async
public abstract class CommonAdapterImpl implements RemoteOperation {
	private static final Logger logger = LoggerFactory.getLogger(CommonAdapterImpl.class);

	@Async
	@Override
	public String getRemoteMachineStatus(String remoteURL) throws RemoteMachineException {

		String statusResonse = null;
		logger.info("Connecting to remote machine :" + remoteURL);
		try {
			URL siteURL = new URL(remoteURL);
			HttpURLConnection connection = (HttpURLConnection) siteURL.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();

			int statusCode = connection.getResponseCode();
			statusResonse = CommonAdapterImpl.resolveStatus(statusCode);

		} catch (Exception e) {
			statusResonse = HTTP_STATUS_EXCEPTION;
			logger.error("Exception :" + e);
			e.printStackTrace();
			throw new RemoteMachineException(e.getMessage(), HTTP_STATUS_EXCEPTION);
		}
		logger.info("Returning server status :" + statusResonse);
		return statusResonse;
	}

	@Override
	public String postRequest(String remoteURL, String remoteParameter) throws RemoteMachineException {
		String statusResonse = null;
		logger.info("Connecting to remote machine :" + remoteURL);
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(remoteURL + URLEncoder.encode(remoteParameter, "UTF-8"));
			ClientResponse remoteResponse = webResource.accept("application/json").post(ClientResponse.class);

			statusResonse = CommonAdapterImpl.resolveStatus(remoteResponse.getStatus());
			logger.info("Remote machine response " + remoteResponse);
			if (statusResonse.equals(HTTP_STATUS_OK)) {
				statusResonse = remoteResponse.getEntity(String.class);
			}

		} catch (Exception e) {
			statusResonse = HTTP_STATUS_EXCEPTION;
			logger.error("Exception :" + e);
			e.printStackTrace();
			throw new RemoteMachineException(e.getMessage(), HTTP_STATUS_EXCEPTION);
		}
		logger.info("Returning server status :" + statusResonse);
		return statusResonse;
	}

	@Override
	public String getRequest(String remoteURL, String remoteParameter) throws RemoteMachineException {
		String statusResonse = null;
		logger.info("Connecting to remote machine :" + remoteURL);
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(remoteURL + URLEncoder.encode(remoteParameter, "UTF-8"));
			ClientResponse remoteResponse = webResource.accept("application/json").get(ClientResponse.class);

			statusResonse = CommonAdapterImpl.resolveStatus(remoteResponse.getStatus());
			logger.info("Remote machine response " + remoteResponse);
			if (statusResonse.equals(HTTP_STATUS_OK)) {
				statusResonse = remoteResponse.getEntity(String.class);
			}

		} catch (Exception e) {
			statusResonse = HTTP_STATUS_EXCEPTION;
			logger.error("Exception :" + e);
			e.printStackTrace();
			throw new RemoteMachineException(e.getMessage(), HTTP_STATUS_EXCEPTION);
		}
		logger.info("Returning server status :" + statusResonse);
		return statusResonse;
	}

	@Override
	public String handleRemoteStatus(RemoteOperation remoteOperation, String xenonServerWS)
			throws RemoteMachineException {
		logger.info("Validating server URL :" + xenonServerWS);
		String responseStatus = null;

		responseStatus = remoteOperation.getRemoteMachineStatus(xenonServerWS);
		if (responseStatus.equals(HTTP_STATUS_OK)) {
			responseStatus = Constants.XENON_STATUS_SUCCESS;
		} else {
			logger.info("Server machine not running");
			responseStatus = Constants.XENON_SERVER_INACTIVE;
		}
		return responseStatus;
	}

	/**
	 * 
	 * @param statusCode
	 * @return
	 */
	public static String resolveStatus(int statusCode) {
		switch (statusCode) {
		case 200:
			return HTTP_STATUS_OK;
		case 400:
			return HTTP_STATUS_BAD_REQUEST;
		case 404:
			return HTTP_STATUS_NOT_FOUND;
		case 405:
			return HTTP_STATUS_METHOD_NOT_ALLOWED;
		default:
			return HTTP_STATUS_UNHANDLED;
		}
	}

	/**
	 * 
	 * @param str
	 * @return
	 */
	protected boolean isValid(String str) {
		return (str != null && str.trim().length() > 0);
	}

	/**
	 * @author navnath.damale This method is used to get time stamp
	 * @return
	 */
	protected String getTimeStamp() {
		logger.info("Gettting time stamp ");
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyhh:mm:ss:S");
		String formattedDate = sdf.format(date);
		logger.info("Returning time stamp ");
		return formattedDate.replaceAll(":", "");
	}

	/**
	 * 
	 * @param dirName
	 * @return
	 * @throws RemoteMachineException
	 */
	protected boolean createDirectory(String dirName) throws RemoteMachineException {
		boolean result = false;
		logger.info("Creating directory :" + dirName);
		Resource resource = new ClassPathResource(dirName);
		File dir = null;
		try {
			dir = resource.getFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		if (!dir.exists()) {
			try {
				dir.mkdir();
				result = true;
			} catch (SecurityException e) {
				e.printStackTrace();
				logger.error("Exception :" + e);
				throw new RemoteMachineException(e.getMessage(), Constants.DIRECTORY_STATUS_NOT_CREATED);
			}
			if (result) {
				logger.info("Directory created :" + dirName);
			}
		} else {
			result = true;
		}

		return result;
	}

	/**
	 * 
	 * @param filePath
	 * @return
	 * @throws RemoteMachineException
	 */
	protected boolean createFile(String filePath) throws RemoteMachineException {
		logger.info("Creaing file :" + filePath);
		boolean flag = false;
		try {
			Resource resource = new ClassPathResource(filePath);
			File file = resource.getFile();
			if (file.createNewFile()) {
				logger.info("File is created :" + filePath);
				flag = true;
			} else {
				logger.info("File already exists :" + filePath);
				flag = true;
			}

		} catch (IOException e) {
			flag = false;
			e.printStackTrace();
			logger.error("Exception :" + e);
			throw new RemoteMachineException(e.getMessage(), Constants.FILE_STATUS_NOT_CREATED);
		}

		return flag;
	}

	public boolean createDirectory(String dirName, boolean flag) throws RemoteMachineException {
		boolean result = false;
		logger.info("Creating directory :" + dirName);
		File dir = new File(dirName);

		if (!dir.exists()) {
			try {
				dir.mkdir();
				result = true;
			} catch (SecurityException e) {
				e.printStackTrace();
				logger.error("Exception :" + e);
				throw new RemoteMachineException(e.getMessage(), Constants.DIRECTORY_STATUS_NOT_CREATED);
			}
			if (result) {
				logger.info("Directory created :" + dirName);
			}
		} else {
			result = true;
		}

		return result;
	}
	
	public boolean createFile(String filePath,boolean test) throws RemoteMachineException {
		logger.info("Creaing file :" + filePath);
		boolean flag = false;
		try {
			File file = new File(filePath);
			if (file.createNewFile()) {
				logger.info("File is created :" + filePath);
				flag = true;
			} else {
				logger.info("File already exists :" + filePath);
				flag = true;
			}

		} catch (IOException e) {
			flag = false;
			e.printStackTrace();
			logger.error("Exception :" + e);
			throw new RemoteMachineException(e.getMessage(), Constants.FILE_STATUS_NOT_CREATED);
		}

		return flag;
	}


}

package com.xenon.remote.dao;

import com.xenon.remote.domain.RemoteMachineException;

/**
 * 
 * @author navnath.damale
 *
 */
public interface RemoteOperation {
	//HTTP status constants
	public static final String HTTP_STATUS_OK="200";
	public static final String HTTP_STATUS_BAD_REQUEST="400";
	public static final String HTTP_STATUS_NOT_FOUND="404";
	public static final String HTTP_STATUS_METHOD_NOT_ALLOWED="405";
	public static final String HTTP_STATUS_UNHANDLED="NOT FOUND";
	public static final String HTTP_STATUS_EXCEPTION="INTERNAL SERVER ERROR";
	public static final String HTTP_STATUS_INVALID_URL="RESOURCE URL NOT VALID";

	
	/**
	 * 
	 * @param remoteURL
	 * @return
	 * @throws RemoteMachineException 
	 */
	public String getRemoteMachineStatus(String remoteURL) throws RemoteMachineException;
	/**
	 * 
	 * @param remoteURL
	 * @param remoteParameter
	 * @return
	 * @throws RemoteMachineException 
	 */
	public String postRequest(String remoteURL,String remoteParameter) throws RemoteMachineException;
	/**
	 * 
	 * @param remoteURL
	 * @param remoteParameter
	 * @return
	 * @throws RemoteMachineException 
	 */
	public String getRequest(String remoteURL,String remoteParameter) throws RemoteMachineException;
	
	/**
	 * 
	 * @param remoteOperation
	 * @return
	 * @throws RemoteMachineException
	 */
	public String handleRemoteStatus(RemoteOperation remoteOperation,String xenonServerWS) throws RemoteMachineException;
}

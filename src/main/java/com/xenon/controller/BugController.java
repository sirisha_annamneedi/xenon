package com.xenon.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.core.util.Base64;
import com.xenon.api.administration.ProjectService;
import com.xenon.api.bugtracker.BugService;
import com.xenon.api.bugtracker.CustomSearchService;
import com.xenon.api.common.CommonService;
import com.xenon.api.common.JiraIntegrationService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.bugtracker.dao.BugDAO;
import com.xenon.bugtracker.dao.BugGroupDAO;
import com.xenon.bugtracker.dao.BugGroupDAOImpl;
import com.xenon.bugtracker.dao.BugSummaryDAO;
import com.xenon.buildmanager.dao.ActivitiesDAO; // DAO class to perform JDBC operations for all activities 
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.ModuleDAO; // DAO class to perform JDBC operations related to module
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.dao.ReportDAO;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.util.service.FileUploadForm; // Class used to upload multiple files
import com.xenon.util.service.JiraRESTClient;

import net.rcarz.jiraclient.Issue;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.JiraException;
import net.rcarz.jiraclient.Version;

/**
 * 
 * @author suresh.adling
 * @description Controller includes requests to do create,update operations on a
 *              bug
 * 
 */

@SuppressWarnings({ "unchecked", "unused" })
@Component
@Controller
@EnableScheduling
public class BugController {

	@Resource(name = "colorProperties")
	private Properties colorProperties;

	@Resource(name = "configurationProperties")
	private Properties configurationProperties;

	@Autowired
	BugService bugService;

	@Autowired
	ProjectService projectService;

	@Autowired
	JiraIntegrationService jiraIntegrationService;

	@Autowired
	BugGroupDAO bugGroupDAO;

	@Autowired
	BugSummaryDAO bugSummaryService;

	@Autowired
	ModuleDAO moduleDao;

	@Autowired
	ActivitiesDAO activitiesDAO;

	@Autowired
	BugGroupDAOImpl bugGroupDAOImpl;

	@Autowired
	LoginService loginService;

	@Autowired
	CommonService commonService;

	@Autowired
	UtilsService utilsService;

	@Autowired
	RoleAccessService roleAccessService;

	@Autowired
	BugDAO bugDAO;

	@Autowired
	UserDAO userDAO;

	@Autowired
	ReportDAO reportService;

	@Autowired
	ProjectDAO projectDAO;

	@Autowired
	CustomSearchService customSearchService;

	@Autowired
	BuildDAO buildDAO;

	private List<Map<String, Object>> bugResult = new ArrayList<Map<String, Object>>();
	private String JsonString = null;
	private Map<String, Object> statusResult = new HashMap<String, Object>();
	private Map<String, Object> priorityResult = new HashMap<String, Object>();
	private Map<String, Object> severityResult = new HashMap<String, Object>();

	private static HttpSession sessionForCronJob = null;
	private static final Logger logger = LoggerFactory.getLogger(BugController.class);

	// Initializing variables for Cron Job
	private String cust_schema = "";
	private String role_id = "";
	private String user_id = "";
	private String user_current_proj_id = "";
	private String customer_id = "";

	/**
	 * Refresh bug dashboard using Jira
	 * 
	 */
	@RequestMapping(value = "/refreshBugsFromJira", method = RequestMethod.POST)
	@Consumes(MediaType.APPLICATION_JSON)
	public ModelAndView refreshBugsFromJira(HttpSession session) throws Exception {
		sessionForCronJob = session;
		logger.info("Refreshing bugs from Jira in Xenon");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("createbug", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {

			// Assign value to variables for cron job (In case session gets
			// invalidated)
			cust_schema = session.getAttribute("cust_schema").toString();
			role_id = session.getAttribute("roleId").toString();
			user_id = sessionForCronJob.getAttribute("userId").toString();
			user_current_proj_id = sessionForCronJob.getAttribute("UserCurrentProjectId").toString();
			customer_id = sessionForCronJob.getAttribute("customerId").toString();

			int projectID = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
			logger.info("Xenon Project Selected with id as : " + projectID);
			List<Map<String, Object>> project = projectService.getSingleProject(projectID,
					session.getAttribute("cust_schema").toString());
			String jiraProject = project.get(0).get("issue_tracker_project_key").toString();
			String moduleId = "";
			List<Map<String, Object>> modules = moduleDao.getModulesByProject(projectID,
					session.getAttribute("cust_schema").toString());
			if (modules.size() > 0) {
				moduleId = modules.get(0).get("module_id").toString();
			} else {
				moduleId = "1";
			}
			logger.info("Jira Project  : " + jiraProject);
			JiraClient jira = jiraIntegrationService.jiraConnect(session);

			/* Search for issues */
			Issue.SearchResult sr = null;
			List<Map<String, Object>> lastUpdateDate = bugDAO.getJiraLastUpdate(jiraProject,
					session.getAttribute("cust_schema").toString());
			if (lastUpdateDate.size() > 0) {
				DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				Date date = inputFormat.parse(lastUpdateDate.get(0).get("jira_bug_tracker_last_update").toString());
				SimpleDateFormat formatterJiraUpdateDateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm");
				String jiraLastUpdateQueryDate = formatterJiraUpdateDateFormat.format(date);
				logger.info("last synch call happened in Xenon from Jira is :" + jiraLastUpdateQueryDate);

				sr = jira.searchIssues("project = " + jiraProject + " AND issuetype=Bug and (createdDate>='"
						+ jiraLastUpdateQueryDate + "' or updatedDate >='" + jiraLastUpdateQueryDate
						+ "') ORDER BY priority DESC, updated DESC", 1000);
				logger.info("project = " + jiraProject + " AND issuetype=Bug and (createdDate>='"
						+ jiraLastUpdateQueryDate + "' or updatedDate >='" + jiraLastUpdateQueryDate
						+ "') ORDER BY priority DESC, updated DESC");

			} else {
				sr = jira.searchIssues(
						"project = " + jiraProject + " AND issuetype=Bug ORDER BY priority DESC, updated DESC", 1000);
				logger.info("Jira JQL search Query :" + "project = " + jiraProject
						+ " AND issuetype=Bug ORDER BY priority DESC, updated DESC");
			}

			logger.info("Total Issues identified  : " + sr.total);
			for (Issue i : sr.issues) {
				logger.info("Issue Key  : " + i);
				logger.info("Issue Priority  : " + i.getPriority());
				logger.info("Issue Reporter Email  : " + i.getReporter().getEmail());
				logger.info("Issue Status  : " + i.getStatus().getName());
				logger.info("Issue Key  : " + i);

				Object cfvalueRootCause = i.getField("customfield_10360");
				String rootCause = "Not selected";
				if (!cfvalueRootCause.equals(null)) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonInString = mapper.writeValueAsString(cfvalueRootCause);

					JSONObject jsonObject = new JSONObject(jsonInString);

					rootCause = jsonObject.get("value").toString();

				}

				String createJiraIssueDate = i.getField("created").toString();
				String updateJiraIssueDate = i.getField("updated").toString();
				Hashtable<String, String> jiraStatusList = bugDAO
						.getStatusHashList(session.getAttribute("cust_schema").toString());
				Hashtable<String, String> jiraPriorityList = bugDAO
						.getPriorityHashList(session.getAttribute("cust_schema").toString());
				Hashtable<String, String> jiraCategoryList = bugDAO
						.getCategoryHashList(session.getAttribute("cust_schema").toString());
				List<Map<String, Object>> reporterDetails = userDAO.getUserDetailsByEmailId(i.getReporter().getEmail());
				String reporterUserId = Integer.toString(userDAO.getUserIDbyEmail("yogi.garg@riverbed.com",
						session.getAttribute("cust_schema").toString()));
				try {
					reporterUserId = reporterDetails.get(0).get("user_id").toString();
					if (reporterDetails.isEmpty()) {
						logger.info("Not able to find reporter in Xenon database hence defaulting to user id 1");
					}
				} catch (Exception e) {
					logger.info("Not able to find reporter in Xenon database hence defaulting to user id 1");
				}

				String assigneeUserId = Integer.toString(userDAO.getUserIDbyEmail("yogi.garg@riverbed.com",
						session.getAttribute("cust_schema").toString()));
				try {
					List<Map<String, Object>> assigneeDetails = userDAO
							.getUserDetailsByEmailId(i.getAssignee().getEmail());

					assigneeUserId = reporterDetails.get(0).get("user_id").toString();
					if (assigneeDetails.isEmpty()) {
						logger.info("Not able to find assignee in Xenon database hence defaulting to user id 3");
					}
				} catch (Exception e) {
					logger.info("Not able to find assignee in Xenon database hence defaulting to user id 3");
				}
				String jiraStatus = jiraStatusList.get(i.getStatus().getName());
				String jiraPriority = jiraPriorityList.get(i.getPriority().getName());

				String jiraReleaseId = "0";
				if (i.getVersions().size() > 0)
					jiraReleaseId = i.getVersions().get(0).getId();
				String jiraRootCauseId = jiraCategoryList.get(rootCause);

				FileUploadForm uploadForm = new FileUploadForm();
				String createdDate = createJiraIssueDate;
				String fixVersion = "";
				String resoultionDescription = "";
				if (!(i.getResolution() == null)) {
					resoultionDescription = i.getResolution().getDescription();
				}
				if (i.getFixVersions().size() > 0) {
					fixVersion = i.getFixVersions().get(0).getName();
				}

				List<Map<String, Object>> existingBugIdObject = bugService.getBugIdMappedWithJiraId(i.toString(),
						session.getAttribute("cust_schema").toString());

				if (existingBugIdObject.size() > 0) {
					String dbBugId = existingBugIdObject.get(0).get("bug_id").toString();
					int bugId = bugService.updateBug(dbBugId, jiraStatus, jiraPriority, assigneeUserId, "1", "1", "2",
							"2", jiraRootCauseId, "1", assigneeUserId, "1", "1", "1", "", reporterUserId,
							session.getAttribute("cust_schema").toString(), uploadForm,
							configurationProperties.getProperty("XENON_REPOSITORY_PATH"), i.getSummary(),
							i.getDescription(), updateJiraIssueDate, resoultionDescription, fixVersion, jiraReleaseId);
					logger.info("Updated Bug Id  : " + bugId);
				} else {
					List<Map<String, Object>> dataForInsertBug = bugService.insertBug(Integer.toString(projectID),
							moduleId, i.getSummary(), jiraStatus, jiraPriority, "1", jiraRootCauseId, "1", "1",
							reporterUserId, i.getDescription(), session.getAttribute("cust_schema").toString(),
							assigneeUserId, uploadForm, configurationProperties.getProperty("XENON_REPOSITORY_PATH"),
							i.toString(), updateJiraIssueDate, resoultionDescription, fixVersion, createdDate,
							jiraReleaseId);
					int bugId = (Integer) dataForInsertBug.get(0).get("bug_id");
					logger.info("Newly Created Bug Id  : " + bugId);
				}

			}

			Map<Long, String> treeMap = new TreeMap<Long, String>();
			// int days = d.getDays();
			List<Version> fetchCurrentReleaseVersions = jira.getProject(jiraProject).getVersions();
			for (int ver = 0; ver < fetchCurrentReleaseVersions.size(); ver++) {
				String jiraReleaseName = fetchCurrentReleaseVersions.get(ver).getName();
				String jiraReleaseId = fetchCurrentReleaseVersions.get(ver).getId();
				String releaseDescription = fetchCurrentReleaseVersions.get(ver).getDescription();
				String releaseStatus = "2";
				if (fetchCurrentReleaseVersions.get(ver).isReleased()) {
					releaseStatus = "1";
				}
				String releaseDate = fetchCurrentReleaseVersions.get(ver).getReleaseDate();
				if (jiraIntegrationService.getJiraRelease(jiraReleaseId, session.getAttribute("cust_schema").toString())
						.size() > 0) {
					bugService.updateJiraRelease(jiraReleaseId, jiraReleaseName, jiraProject, releaseDescription,
							releaseStatus, releaseDate, session.getAttribute("cust_schema").toString());
				} else {
					bugService.insertJiraRelease(jiraReleaseName, jiraProject, releaseDescription, releaseStatus,
							releaseDate, jiraReleaseId, session.getAttribute("cust_schema").toString());
				}

				if (!fetchCurrentReleaseVersions.get(ver).isReleased()) {

					LocalDate startDate = LocalDate.now();
					SimpleDateFormat formatterJiraUpdateDateFormat = new SimpleDateFormat("yyyy-MM-dd");
					Days d = null;
					try {
						Date jiraReleaseDate = formatterJiraUpdateDateFormat
								.parse(fetchCurrentReleaseVersions.get(ver).getReleaseDate());
						d = Days.daysBetween(new LocalDate(startDate), new LocalDate(jiraReleaseDate));
						if (d.getDays() > 0) {
							treeMap.put((long) d.getDays(), fetchCurrentReleaseVersions.get(ver).getId());
						}
					} catch (Exception e) {
						logger.info("Jira release date has not been set for " + fetchCurrentReleaseVersions.get(ver));

					}

				}
			}
			String currentReleaseId = "0";
			try {

				currentReleaseId = (treeMap.values().toArray()[0]).toString();
			} catch (Exception e) {
				logger.info("Current release is not defined for project  : " + jiraProject + " exception :" + e);
			}
			bugService.insertUpdateJira("bugTracker", jiraProject, currentReleaseId,
					session.getAttribute("cust_schema").toString());

			/* Print the issue key. */
			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if (appCount == 0) {
				return new ModelAndView("redirect:/xedashboard");
			}
		}
		return new ModelAndView("redirect:/btdashboard");

	}

	/**
	 * @author suresh.adling
	 * @method createBug GET Request
	 * @output get create bug view
	 * @param Model
	 *            model,HttpSession session
	 * @description This method reads following list by using session stored
	 *              values(userId,projectId) and redirects user to create bug view
	 *              with the model
	 * @return Model (statusList,priorityList,moduleList) & View (onSuccess -
	 *         createbug ,onFailure - logout/trialerror/500)
	 */

	@RequestMapping(value = "/createbug", method = RequestMethod.GET)
	public ModelAndView createBug(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to create bug page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("createbug", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			int flag = bugService.createNewBugStatus(session.getAttribute("cust_schema").toString(),
					Integer.parseInt(session.getAttribute("cust_type_id").toString()));
			if (flag == 1) {
				/*
				 * model = common.getHeaderValues(model, session); if
				 * (model.containsAttribute("noProject")) { model.asMap().clear(); return new
				 * ModelAndView("redirect:/xedashboard"); }
				 */

				int appCount = commonService.getLoginUserAssignedAppCount(session);
				if (appCount == 0) {
					return new ModelAndView("redirect:/xedashboard");
				}

				String schema = session.getAttribute("cust_schema").toString();
				int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
				session.setAttribute("selectedBugProjectId", projectId);
				int userId = Integer.parseInt(session.getAttribute("userId").toString());
				logger.info(" Session is active, userId is " + userId + " and projectId is " + projectId
						+ " Customer Schema " + schema);
				Map<String, Object> data = bugService.getDataForCreateBug(projectId, userId, schema);

				model.addAttribute("statusList", data.get("#result-set-1"));
				model.addAttribute("priorityList", data.get("#result-set-2"));
				model.addAttribute("moduleList", data.get("#result-set-3"));
				model.addAttribute("userList", data.get("#result-set-4"));
				model.addAttribute("bugGroup", data.get("#result-set-5"));
				model.addAttribute("severityList", data.get("#result-set-5"));
				model.addAttribute("categoryList", data.get("#result-set-6"));
				model.addAttribute("buildExecType", data.get("#result-set-7"));
				model.addAttribute("tmStatus", session.getAttribute("tmStatus"));
				model.addAttribute("manualBuildList",
						utilsService.convertListOfMapToJson((List<Map<String, Object>>) data.get("#result-set-8")));
				model.addAttribute("automationBuildList",
						utilsService.convertListOfMapToJson((List<Map<String, Object>>) data.get("#result-set-9")));
				model.addAttribute("automationStatus",
						Integer.parseInt(session.getAttribute("automationStatus").toString()));
				model.addAttribute("UserProfilePhoto",
						loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
				model.addAttribute("userName", session.getAttribute("userFullName"));
				model.addAttribute("role", session.getAttribute("role"));
				model.addAttribute("schema", schema);
				logger.info(" User is redirected to create bug  page ");
				return new ModelAndView("createbug", "Model", model);
			} else {
				logger.error("User has exhausted the limit and redirected to trialerror page ");
				return new ModelAndView("redirect:/trialerror");
			}
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	@RequestMapping(value = "/syncToJira", method = RequestMethod.GET)
	public void getTestData(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		logger.info("Ajax Post request to syncToJira");
		try {
			String schema = session.getAttribute("cust_schema").toString();
			String userId = session.getAttribute("userId").toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author suresh.adling
	 * @method insertBug POST Request
	 * @output raise bug in a particular module
	 * @param Request
	 *            Parameter (moduleId,statusId,summary,priorityId,bugDescription,
	 *            attachments) Method Parameter (HttpSession session)
	 * @description This method gets moduleId,bug details,bug attachment from user
	 *              then inserts a row in bug details in 'xebt_bug', summary details
	 *              'xebt_bugsummary' and attachments in 'xebt_bug_attachment' and
	 *              redirects user to bug summary page
	 * @return Model & View (onSuccess - bugsummary ,onFailure - logout/500)
	 */

	@RequestMapping(value = "/insertbug", method = RequestMethod.POST)
	@Consumes(MediaType.APPLICATION_JSON)
	public ModelAndView insertBug(@RequestParam("moduleId") String moduleId, @RequestParam("statusId") String statusId,
			@RequestParam("bugSummary") String summary, @RequestParam("priorityId") String priorityId,
			@RequestParam("severityId") String severityId, @RequestParam("assignedUserID") String assignedUserId,
			@RequestParam("category") String categoryId, @RequestParam("bugDescription") String bugDescription,
			@RequestParam(value = "buildTypeId", defaultValue = "3") String buildTypeId,
			@RequestParam(value = "selectedBuildID", defaultValue = "0") String selectedBuildID,
			@ModelAttribute("uploadForm") FileUploadForm uploadForm, HttpSession session) throws Exception {
		logger.info("Post method to insert bug method");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("createbug", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			String[] inputStrings = { summary };
			/* It removes special Character from summary */
			// JSONObject plainText =
			// utilsService.convertToPlainText(inputStrings);
			// summary = plainText.get(summary).toString();

			String schema = session.getAttribute("cust_schema").toString();
			// System.out.println(schema);
			String projectId = session.getAttribute("UserCurrentProjectId").toString();
			String userId = session.getAttribute("userId").toString();

			logger.info("Session is active, userId is " + userId + " and projectId is " + projectId
					+ " Customer Schema " + schema);
			String dtJira = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
			;
			// check jira status and insert bug to jira start (Abhay Thakur)
			String jiraID = "";
			try {
				String issue_tracker = "0";
				String issue_tracker_type = "0";
				String issue_tracker_host = "";
				try {
					issue_tracker = session.getAttribute("issue_tracker").toString();
					if (issue_tracker != null && issue_tracker.equals("1")) {
						issue_tracker_type = session.getAttribute("issue_tracker_type").toString();
						issue_tracker_host = session.getAttribute("issue_tracker_host").toString();
						List<Map<String, Object>> projList = projectDAO
								.getProjectDetailsById(Integer.parseInt(projectId), schema);
						String issue_tracker_project_key = projList.get(0).get("issue_tracker_project_key").toString();
						List<Map<String, Object>> jiraDetails = projectDAO.getJiraDetails(schema);
						if (issue_tracker_project_key != null && jiraDetails != null) {
							String auth = new String(
									Base64.encode(jiraDetails.get(0).get("jira_integ_username").toString() + ":"
											+ jiraDetails.get(0).get("jira_integ_password").toString()));
							JSONObject issuetype = new JSONObject();
							issuetype.put("name", "Bug");
							JSONObject project = new JSONObject();
							project.put("key", issue_tracker_project_key);
							JSONObject priority = new JSONObject();
							priority.put("name", "High");
							JSONObject fields = new JSONObject();
							fields.put("summary", summary);
							fields.put("project", project);
							fields.put("issuetype", issuetype);
							JSONObject request = new JSONObject();
							request.put("fields", fields);

							try {
								String issue = JiraRESTClient.invokePostMethod(auth,
										issue_tracker_host + "/rest/api/2/issue", request.toString());
								JSONObject issueObj = new JSONObject(issue);
								jiraID = issueObj.getString("key");
								System.out.println(jiraID);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					}
				} catch (Exception e) {
					e.printStackTrace();
					issue_tracker = "0";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			// check jira status and insert bug to jira end (Abhay Thakur)
			List<Map<String, Object>> dataForInsertBug = bugService.insertBug(projectId, moduleId, summary, statusId,
					priorityId, severityId, categoryId, buildTypeId, selectedBuildID, userId, bugDescription, schema,
					assignedUserId, uploadForm, configurationProperties.getProperty("XENON_REPOSITORY_PATH"), jiraID,
					dtJira, "", "", dtJira, "");
			int bugId = (Integer) dataForInsertBug.get(0).get("bug_id");
			if (bugId != 0) {

				session.setAttribute("selectedBugId", bugId);
				logger.info("User is redirected to bug summary page ");
				// setting session variable for the toaster
				session.setAttribute("createBugStatus", 1);
				return new ModelAndView("redirect:/bugsummary");
			} else {
				logger.error("Something went wrong,redirected to error page");
				return new ModelAndView("redirect:/500");
			}
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	@RequestMapping(value = "/createBugToJiraByBuildId/{buildId}", method = RequestMethod.GET)
	@Consumes(MediaType.APPLICATION_JSON)
	public @ResponseBody String createBugToJiraByBuildId(@PathVariable Integer buildId, HttpSession session)
			throws Exception {
		logger.info("Post method to insert bug to jira");
		List<Map<String, Object>> bugList =null;
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return "Session has expired.";
		} else if (bugDAO.checkBTRoleAccess("createbug", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {

			String schema = session.getAttribute("cust_schema").toString();
			String projectId = session.getAttribute("UserCurrentProjectId").toString();
			String userId = session.getAttribute("userId").toString();

			logger.info("Session is active, userId is " + userId + " and projectId is " + projectId
					+ " Customer Schema " + schema);
			String dtJira = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
			;
			String jiraId = "";
			Integer isUpdate = 0;
			try {
				String issue_tracker = "0";
				String issue_tracker_type = "0";
				String issue_tracker_host = "";
				try {
					issue_tracker = session.getAttribute("issue_tracker").toString();
					if (issue_tracker != null && issue_tracker.equals("1")) {
						issue_tracker_type = session.getAttribute("issue_tracker_type").toString();
						issue_tracker_host = session.getAttribute("issue_tracker_host").toString();
						List<Map<String, Object>> projList = projectDAO
								.getProjectDetailsById(Integer.parseInt(projectId), schema);
						String issue_tracker_project_key = projList.get(0).get("issue_tracker_project_key").toString();
						List<Map<String, Object>> jiraDetails = projectDAO.getJiraDetails(schema);
						if (issue_tracker_project_key != null && jiraDetails != null) {
							String auth = new String(
									Base64.encode(jiraDetails.get(0).get("jira_integ_username").toString() + ":"
											+ jiraDetails.get(0).get("jira_integ_password").toString()));
						 bugList = buildDAO.getBugDetailsByBuildId(userId, buildId,
									projectId, schema);
							if (bugList != null && bugList.size() > 0) {
								for (int i = 0; i < bugList.size(); i++) {
									JSONObject issuetype = new JSONObject();
									issuetype.put("name", "Bug");
									JSONObject project = new JSONObject();
									project.put("key", issue_tracker_project_key);
									JSONObject priority = new JSONObject();
									priority.put("name", "High");
									JSONObject fields = new JSONObject();
									fields.put("summary", bugList.get(i).get("bug_title"));
									fields.put("project", project);
									fields.put("issuetype", issuetype);
									JSONObject request = new JSONObject();
									request.put("fields", fields);
									try {
										String issue = JiraRESTClient.invokePostMethod(auth,
												issue_tracker_host + "/rest/api/2/issue", request.toString());
										JSONObject issueObj = new JSONObject(issue);
										jiraId = issueObj.getString("key");
										logger.info("After send request to jira id :" + jiraId);
										isUpdate = bugDAO.updateJiraIdByBigId(bugList.get(i).get("bug_id").toString(),
												jiraId, userId, schema);
										if (isUpdate > 0) {
											logger.info("jira id upated successfully and map with bug id :"
													+ bugList.get(i).get("bug_id").toString());
										}
									} catch (Exception e) {
										e.printStackTrace();
										return "Something went wrong.  While connectiong to jira";

									}
								}
							}else {
								return "No bugs for pushed to jira.";
							}
						}

					}
				} catch (Exception e) {
					e.printStackTrace();
					return "Something went wrong.";
				}
			} catch (Exception e) {
				e.printStackTrace();
				return "Something went wrong.";
			}
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return "Something went wrong.";
		}
		return bugList.size()+" , Bugs created on jira ";
		}

	/**
	 * @author suresh.adling
	 * @method bugSummary GET Request
	 * @output get bug summary view
	 * @param Model
	 *            model,HttpSession session
	 * @description This method gets bug id from session,reads following lists by
	 *              calling different methods and returns bug summary with model
	 * @return Model (statusList,priorityList,userList,bugDetails,bugDetails,
	 *         bugAttachDetails,summaryDetails,summaryAttachment,
	 *         UserProfilePhoto,userName,role) View (onSuccess - bugsummary
	 *         ,onFailure - logout/500)
	 */
	@RequestMapping(value = "/bugsummary", method = RequestMethod.GET)
	public ModelAndView bugSummary(@RequestParam(value = "page", defaultValue = "1") int page, Model model,
			HttpSession session) throws Exception {
		List<Map<String, Object>> attachment = new ArrayList<Map<String, Object>>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-Y");
		Date date = new Date();

		logger.info("Redirecting to  bug summary page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("bugsummary", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			/*
			 * model = common.getHeaderValues(model, session); if
			 * (model.containsAttribute("noProject")) { model.asMap().clear(); return new
			 * ModelAndView("redirect:/xedashboard"); }
			 */
			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if (appCount == 0) {
				return new ModelAndView("redirect:/xedashboard");
			}

			int pageSize = 5;
			model.addAttribute("pageSize", pageSize);
			int startValue = (page - 1) * pageSize;

			String schema = session.getAttribute("cust_schema").toString();
			int bugId = Integer.parseInt(session.getAttribute("selectedBugId").toString());
			logger.warn("Session is active, bugId is " + bugId + " Customer Schema " + schema);

			int projectId = Integer.parseInt(session.getAttribute("selectedBugProjectId").toString());
			Map<String, Object> data = bugSummaryService.getDataForBugSummary(projectId, bugId, startValue, pageSize,
					schema);
			List<Map<String, Object>> summaryDetails = (List<Map<String, Object>>) data.get("#result-set-5");
			for (int i = 0; i < summaryDetails.size(); i++) {
				int summaryId = Integer.parseInt(summaryDetails.get(i).get("bs_id").toString());
				List<Map<String, Object>> commentAttachments = bugService.getCommentAttachmentDeatils(summaryId,
						schema);
				for (int j = 0; j < commentAttachments.size(); j++) {
					attachment.add(commentAttachments.get(j));
				}
				String logo = loginService.getImage((byte[]) summaryDetails.get(i).get("user_photo"));
				summaryDetails.get(i).put("updater_logo", logo);
				summaryDetails.get(i).put("update_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(summaryDetails.get(i).get("update_date").toString())));

			}
			List<Map<String, Object>> bugDetails = (List<Map<String, Object>>) data.get("#result-set-3");
			for (int i = 0; i < bugDetails.size(); i++) {
				bugDetails.get(i).put("create_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(bugDetails.get(i).get("create_date").toString())));

				if (bugDetails.get(i).get("bug_id").toString().equals(summaryDetails.get(i).get("bug_id").toString())) {
					if (bugDetails.get(i).get("assign_status").toString().equals("2")) {
						summaryDetails.get(i).put("bug_assignee_name", "None");
						summaryDetails.get(i).put("prev_assignee_name", "None");
					} else if (bugDetails.get(i).get("group_status").toString().equals("1")) {
						int bugGroupId = Integer.parseInt(bugDetails.get(i).get("group_id").toString());
						List<Map<String, Object>> bugGroup = bugGroupDAO.getGroupbyId(bugGroupId, schema);
						summaryDetails.get(i).put("bug_assignee_name", bugGroup.get(0).get("bug_group_name"));
					}
				}
			}
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);

			// List<Map<String, Object>>
			// moduleId=modeuleService.getBugModule(bugId, schema);

			List<Map<String, Object>> moduleId = (List<Map<String, Object>>) data.get("#result-set-9");

			List<Map<String, Object>> groupList = (List<Map<String, Object>>) data.get("#result-set-10");
			List<Map<String, Object>> userList = (List<Map<String, Object>>) data.get("#result-set-4");
			boolean userBugAccess = false;
			for (int count = 0; count < userList.size(); count++) {
				if (Integer.parseInt(userList.get(count).get("user_id").toString()) == Integer
						.parseInt(session.getAttribute("userId").toString())) {
					userBugAccess = true;
					break;
				}
				if (userBugAccess)
					break;
			}
			if (!userBugAccess) {
				logger.error("Something went wrong, user is redirected to 401 error page");
				return new ModelAndView("401");
			}
			for (int i = 0; i < groupList.size(); i++) {
				int groupId = Integer.parseInt(groupList.get(i).get("bug_group_id").toString());
				List<Map<String, Object>> groupUser = bugGroupDAOImpl.getUserbyGroupId(groupId, schema);
				List<Map<String, Object>> moduleUser = moduleDao
						.moduleUserDetails(Integer.parseInt(moduleId.get(0).get("module").toString()), schema);

				moduleUser.retainAll(groupUser);
				if (moduleUser.size() > 0) {
					userList.add(groupList.get(i));
				}
			}
			model.addAttribute("schema", schema);
			model.addAttribute("buildList", data.get("#result-set-11"));
			model.addAttribute("statusList", data.get("#result-set-1"));
			model.addAttribute("priorityList", data.get("#result-set-2"));
			model.addAttribute("bugDetails", bugDetails);
			model.addAttribute("userList", userList);
			model.addAttribute("summaryDetails", summaryDetails);
			model.addAttribute("bugAttachDetails", data.get("#result-set-6"));
			model.addAttribute("severityList", data.get("#result-set-7"));
			model.addAttribute("categoryList", data.get("#result-set-8"));
			model.addAttribute("summaryAttachment", attachment);
			model.addAttribute("New", colorProperties.getProperty("bug.New"));
			model.addAttribute("Fixed", colorProperties.getProperty("bug.Fixed"));
			model.addAttribute("Closed", colorProperties.getProperty("bug.Closed"));
			model.addAttribute("Assigned", colorProperties.getProperty("bug.Assigned"));
			model.addAttribute("Verified", colorProperties.getProperty("bug.Verified"));
			model.addAttribute("Reject", colorProperties.getProperty("bug.Reject"));
			model.addAttribute("nowDate", dateFormat.format(date));
			model.addAttribute("prevDate", dateFormat.format(cal.getTime()));
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			int allCommentsCount = (Integer) data.get("commentcount");
			model.addAttribute("allCommentsCount", (allCommentsCount - 1));
			logger.info("User is redirected to bug summary page ");
			return new ModelAndView("bugsummary", "Model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	/**
	 * @author suresh.adling
	 * @method updateBug POST Request
	 * @output insert a row for comment in 'xebt_bugsummary' from update bug form on
	 *         the bug summary page
	 * @param Request
	 *            Parameter(bugId,,statusId,priorityId,assigneeId,prevPriorityId
	 *            ,prevStatus,prevAssignee,comment,attachments) Method
	 *            Parameter(HttpSession session)
	 * @description This method accepts bug details,comments,attachments then
	 *              inserts a new row into 'xebt_bugsummary',redirects user to
	 *              bugsummary
	 * @return Model & View (onSuccess - bugsummary ,onFailure - logout/500)
	 */
	@RequestMapping(value = "/updatebug", method = RequestMethod.POST)
	public ModelAndView updateBug(@RequestParam("bugTitle") String bugTitle, @RequestParam("bugDesc") String bugDesc,
			@RequestParam("bugId") String bugId, @RequestParam("status") String statusId,
			@RequestParam("jiraId") String jiraId, @RequestParam("currentBug") String currentBug,
			@RequestParam("priority") String priorityId, @RequestParam("assignee") String assigneeId,
			@RequestParam("assigneeName") String assigneeName, @RequestParam("prevPriority") String prevPriorityId,
			@RequestParam("prevStatus") String prevStatusId, @RequestParam("severity") String severityId,
			@RequestParam("prevSeverity") String prevSeverityId,
			@RequestParam(value = "buildId", defaultValue = "0") String buildId,
			@RequestParam(value = "prevBuildId", defaultValue = "0") String prevBuildId,
			@RequestParam(value = "prevBuildType", defaultValue = "3") String prevBuildType,
			@RequestParam("categoryId") String categoryId, @RequestParam("prevCategory") String prevCategoryId,
			@RequestParam("prevAssignee") String prevAssigneeId, @RequestParam("comment") String comment,
			@ModelAttribute("uploadForm") FileUploadForm uploadForm, HttpSession session) throws Exception {

		session.setAttribute("selectedBugId", bugId);

		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("bugsummary", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);

			logger.info("Post method to insert bug method");
			String schema = session.getAttribute("cust_schema").toString();
			String userId = session.getAttribute("userId").toString();
			String inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(new Date());
			;

			String projectId = session.getAttribute("UserCurrentProjectId").toString();

			logger.info("Session is active, userId is " + userId + " and projectId is " + projectId
					+ " Customer Schema " + schema);
			String dtJira = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
			;
			String jiraID = "";
			try {
				String issue_tracker = "0";
				String issue_tracker_type = "0";
				String issue_tracker_host = "";
				try {
					issue_tracker = session.getAttribute("issue_tracker").toString();
					if (issue_tracker != null && issue_tracker.equals("1")) {
						issue_tracker_type = session.getAttribute("issue_tracker_type").toString();
						issue_tracker_host = session.getAttribute("issue_tracker_host").toString();
						List<Map<String, Object>> projList = projectDAO
								.getProjectDetailsById(Integer.parseInt(projectId), schema);
						String issue_tracker_project_key = projList.get(0).get("issue_tracker_project_key").toString();
						List<Map<String, Object>> jiraDetails = projectDAO.getJiraDetails(schema);

						if (issue_tracker_project_key != null && jiraDetails != null) {
							String auth = new String(
									Base64.encode(jiraDetails.get(0).get("jira_integ_username").toString() + ":"
											+ jiraDetails.get(0).get("jira_integ_password").toString()));

							JSONObject transition = new JSONObject();

							String editAssigneeData = " {\"fields\":{\"assignee\":{\"name\":\"" + assigneeName
									+ "\"}}}";
							// String
							// editPriorityData="{\"update\":{\"priority\":[{\"set\":{\"name\":\""+updatePriority+"\"}}}";
							// System.out.println(currentBug);
							String Open = "Open";
							String inProgress = "In Progress";
							String Reopened = "Reopened";
							String Resolved = "Resolved";

							if (currentBug.equals(Open)) {
								switch (statusId) {
								case "3":
									transition.put("id", "11");
									break;

								case "4":
									transition.put("id", "101");
									break;

								case "5":
									transition.put("id", "71");
									break;

								default:
									break;
								}
							}

							else if (currentBug.equals(inProgress)) {
								switch (statusId) {
								case "4":
									transition.put("id", "41");
									break;

								case "5":
									transition.put("id", "21");
									break;

								case "7":
									transition.put("id", "91");
									break;

								default:
									break;
								}
							}

							else if (currentBug.equals(Resolved)) {
								switch (statusId) {
								case "5":
									transition.put("id", "31");
									break;

								case "8":
									transition.put("id", "81");
									break;

								default:
									break;
								}
							}

							else if (currentBug.equals(Reopened)) {
								switch (statusId) {
								case "3":
									transition.put("id", "51");
									break;

								case "5":
									transition.put("id", "61");
									break;

								default:
									break;
								}
							}

							JSONObject request = new JSONObject();
							request.put("transition", transition);
							try {
								JiraRESTClient.updatePostMethod(auth,
										issue_tracker_host + "/rest/api/2/issue/" + jiraId + "/transitions",
										request.toString());

								JiraRESTClient.updatePutMethod1(auth,
										issue_tracker_host + "/rest/api/2/issue/" + jiraId, editAssigneeData);

								// JiraRESTClient.updatePutMethod2(auth,
								// issue_tracker_host +
								// "/rest/api/2/issue/"+jiraId,
								// editAssigneeData);

							}

							catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					issue_tracker = "0";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			int summaryId = bugService.updateBug(bugId, statusId, priorityId, assigneeId, prevStatusId, prevPriorityId,
					severityId, prevSeverityId, categoryId, prevCategoryId, prevAssigneeId, buildId, prevBuildId,
					prevBuildType, comment, userId, schema, uploadForm,
					configurationProperties.getProperty("XENON_REPOSITORY_PATH"), bugTitle, bugDesc, inputFormat, "",
					"", "");
			if (summaryId != 0) {
				logger.info("User is redirected to bug summary page ");
				session.setAttribute("createBugStatus", 2);
				return new ModelAndView("redirect:/bugsummary");
			} else {
				logger.error("Something went wrong,redirected to error page");
				return new ModelAndView("redirect:/500");
			}
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	/**
	 * @author suresh.adling
	 * @method addComment POST Request
	 * @output insert a row for comment in 'xebt_bugsummary'
	 * @param Request
	 *            Parameter(bugId,,statusId,priorityId,assigneeId,prevPriorityId
	 *            ,prevStatus,prevAssignee,comment,attachments) Method
	 *            Parameter(HttpSession session)
	 * @description This method accepts comments,attachments and unchanged bug
	 *              details,inserts comments and attachments,redirects user to
	 *              bugsummary
	 * @return Model & View (onSuccess - bugsummary ,onFailure - logout/500)
	 */
	@RequestMapping(value = "/addcomment", method = RequestMethod.POST)
	public ModelAndView addComment(@RequestParam("bugId") String bugId, @RequestParam("status") String statusId,
			@RequestParam("priority") String priorityId, @RequestParam("assignee") String assigneeId,
			@RequestParam("severity") String severityId, @RequestParam("category") String categoryId,
			@RequestParam(value = "prevBuildId", defaultValue = "0") String prevBuildId,
			@RequestParam(value = "prevBuildType", defaultValue = "3") String prevBuildType,
			@RequestParam("comment") String comment, @ModelAttribute("uploadForm") FileUploadForm uploadForm,
			HttpSession session) throws Exception {
		logger.info("Post method to insert bug method");

		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		String userId = session.getAttribute("userId").toString();

		int summaryId = bugService.updateBug(bugId, statusId, priorityId, assigneeId, statusId, priorityId, severityId,
				severityId, categoryId, categoryId, assigneeId, prevBuildId, prevBuildId, prevBuildType, comment,
				userId, schema, uploadForm, configurationProperties.getProperty("XENON_REPOSITORY_PATH"), "", "", "",
				"", "", "");
		if (summaryId != 0) {
			session.setAttribute("selectedBugId", bugId);
			logger.info("User is redirected to bug summary page ");
			return new ModelAndView("redirect:/bugsummary");
		} else {
			logger.error("Something went wrong,redirected to error page");
			return new ModelAndView("redirect:/500");
		}

	}

	/**
	 * @author suresh.adling
	 * @method getBugSummaryByPrefix GET Request
	 * @output get bug details by unique bug prefix
	 * @param Request
	 *            Parameter(btPrefix) Method Parameter(Model model,HttpSession
	 *            session)
	 * @description This method accepts bug prefix,gets bug details and redirects
	 *              user to show bug summary
	 * @return Model (statusList,priorityList,userList,bugDetails,bugDetails,
	 *         bugAttachDetails,summaryDetails,summaryAttachment,
	 *         UserProfilePhoto,userName,role) View (onSuccess - bugsummary
	 *         ,onFailure - logout/500)
	 */
	@RequestMapping(value = "/bugsummarybyprefix", method = RequestMethod.GET)
	public ModelAndView getBugSummaryByPrefix(@RequestParam("btPrefix") String btPrefix,
			@RequestParam(value = "page", defaultValue = "1") int page, Model model, HttpSession session)
			throws Exception {
		List<Map<String, Object>> attachment = new ArrayList<Map<String, Object>>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-Y");
		Date date = new Date();
		utilsService.getLoggerUser(session);
		logger.info("Get method to get bug details by prefix");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("bugsummary", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			model = commonService.getHeaderValues(model, session);
			/*
			 * if (model.containsAttribute("noProject")) { model.asMap().clear(); return new
			 * ModelAndView("redirect:/xedashboard"); }
			 */
			int pageSize = 5;
			model.addAttribute("pageSize", pageSize);
			int startValue = (page - 1) * pageSize;

			String schema = session.getAttribute("cust_schema").toString();
			List<Map<String, Object>> bug = bugService.getBugDetailsByPrefix(btPrefix, schema);
			int projectId = Integer.parseInt(bug.get(0).get("project").toString());
			int bugId = Integer.parseInt(bug.get(0).get("bug_id").toString());
			session.setAttribute("selectedBugProjectId", projectId);
			Map<String, Object> data = bugSummaryService.getDataForBugSummary(projectId, bugId, startValue, pageSize,
					schema);
			List<Map<String, Object>> summaryDetails = (List<Map<String, Object>>) data.get("#result-set-5");
			for (int i = 0; i < summaryDetails.size(); i++) {
				int summaryId = Integer.parseInt(summaryDetails.get(i).get("bs_id").toString());
				List<Map<String, Object>> commentAttachments = bugService.getCommentAttachmentDeatils(summaryId,
						schema);
				for (int j = 0; j < commentAttachments.size(); j++) {
					attachment.add(commentAttachments.get(j));
				}
				String logo = loginService.getImage((byte[]) summaryDetails.get(i).get("user_photo"));
				summaryDetails.get(i).put("updater_logo", logo);

				summaryDetails.get(i).put("update_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(summaryDetails.get(i).get("update_date").toString())));
			}
			List<Map<String, Object>> bugDetails = (List<Map<String, Object>>) data.get("#result-set-3");
			for (int i = 0; i < bugDetails.size(); i++) {
				bugDetails.get(i).put("create_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(bugDetails.get(i).get("create_date").toString())));
			}
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			List<Map<String, Object>> userList = (List<Map<String, Object>>) data.get("#result-set-4");
			boolean userBugAccess = false;
			for (int count = 0; count < userList.size(); count++) {
				if (Integer.parseInt(userList.get(count).get("user_id").toString()) == Integer
						.parseInt(session.getAttribute("userId").toString())) {
					userBugAccess = true;
					break;
				}
				if (userBugAccess)
					break;
			}
			if (!userBugAccess) {
				logger.error("Something went wrong, user is redirected to 401 error page");
				return new ModelAndView("401");
			}
			model.addAttribute("statusList", data.get("#result-set-1"));
			model.addAttribute("priorityList", data.get("#result-set-2"));
			model.addAttribute("bugDetails", bugDetails);
			model.addAttribute("userList", data.get("#result-set-4"));
			model.addAttribute("summaryDetails", summaryDetails);
			model.addAttribute("bugAttachDetails", data.get("#result-set-6"));
			model.addAttribute("severityList", data.get("#result-set-7"));
			model.addAttribute("categoryList", data.get("#result-set-8"));
			model.addAttribute("summaryAttachment", attachment);
			model.addAttribute("New", colorProperties.getProperty("bug.New"));
			model.addAttribute("Fixed", colorProperties.getProperty("bug.Fixed"));
			model.addAttribute("Closed", colorProperties.getProperty("bug.Closed"));
			model.addAttribute("Assigned", colorProperties.getProperty("bug.Assigned"));
			model.addAttribute("Verified", colorProperties.getProperty("bug.Verified"));
			model.addAttribute("Reject", colorProperties.getProperty("bug.Reject"));
			model.addAttribute("nowDate", dateFormat.format(date));
			model.addAttribute("prevDate", dateFormat.format(cal.getTime()));
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			int allCommentsCount = (Integer) data.get("commentcount");
			model.addAttribute("allCommentsCount", (allCommentsCount - 1));

			logger.info("User is redirected to bug summary page ");
			return new ModelAndView("bugsummary", "Model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	/**
	 * @author suresh.adling
	 * @method getBugAttchment POST Request
	 * @output download bug attachments
	 * @param Request
	 *            Parameter(attchmentId) Method Parameter(HttpSession session)
	 * @description This method accepts attachment id,gets byte array from
	 *              'xebt_bug_attachment' table and return converted encodeBase64
	 *              String
	 * @return encodeBase64 String
	 */
	@RequestMapping(value = "/getbugattchment", method = RequestMethod.POST)
	public @ResponseBody String getBugAttchment(@RequestParam("attchmentId") String attchmentId, HttpSession session)
			throws Exception {
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		List<Map<String, Object>> bugAttachment = bugService.getBugAttachmentById(Integer.parseInt(attchmentId),
				schema);
		return utilsService.getFileData((byte[]) bugAttachment.get(0).get("attach_path"));
	}

	/**
	 * @author suresh.adling
	 * @method getCommentAttchment POST Request
	 * @output download bug comments attachments
	 * @param Request
	 *            Parameter(attchmentId) Method Parameter(HttpSession session)
	 * @description This method accepts attachment id,gets byte array from
	 *              'xebt_bugcomment_attachment' table and return converted
	 *              encodeBase64 String
	 * @return encodeBase64 String
	 */
	@RequestMapping(value = "/getcommentattchment", method = RequestMethod.POST)
	public @ResponseBody String getCommentAttchment(@RequestParam("attchmentId") String attchmentId,
			HttpSession session) throws Exception {
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		List<Map<String, Object>> commentAttachment = bugService.getCommentAttachmentById(Integer.parseInt(attchmentId),
				schema);
		return utilsService.getFileData((byte[]) commentAttachment.get(0).get("attach_path"));
	}

	/**
	 * @author suresh.adling
	 * @method getModuleUsers POST Request
	 * @output get list of users assigned to input module id
	 * @param Request
	 *            Parameter(moduleId) Method Parameter(HttpSession session)
	 * @description This method accepts module id, return list of user assigned to
	 *              that module
	 * 
	 * @return List Map (userList)
	 */

	@RequestMapping(value = "/getmoduleusers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody List<Map<String, Object>> getModuleUsers(@RequestParam("moduleID") String moduleId,
			Model model, HttpSession session) throws Exception {

		int module = Integer.parseInt(moduleId);
		String schema = session.getAttribute("cust_schema").toString();

		Map<String, Object> data = moduleDao.getDataForModuleSummary(module, schema);
		List<Map<String, Object>> userList = (List<Map<String, Object>>) data.get("#result-set-1");
		List<Map<String, Object>> groupList = (List<Map<String, Object>>) data.get("#result-set-2");

		for (int i = 0; i < groupList.size(); i++) {
			int groupId = Integer.parseInt(groupList.get(i).get("bug_group_id").toString());
			List<Map<String, Object>> moduleUserList = moduleDao.moduleUserDetails(module, schema);
			List<Map<String, Object>> groupUser = bugGroupDAOImpl.getUserbyGroupId(groupId, schema);
			moduleUserList.retainAll(groupUser);

			if (moduleUserList.size() > 0) {
				userList.add(groupList.get(i));
			}
		}

		return userList;
	}

	/**
	 * @author suresh.adling
	 * @method bugSummary GET Request
	 * @output get bug summary view
	 * @param Model
	 *            model,HttpSession session
	 * @description This method gets bug id from session,reads following lists by
	 *              calling different methods and returns bug summary with model
	 * @return Model (statusList,priorityList,userList,bugDetails,bugDetails,
	 *         bugAttachDetails,summaryDetails,summaryAttachment,
	 *         UserProfilePhoto,userName,role) View (onSuccess - bugsummary
	 *         ,onFailure - logout/500)
	 */
	@RequestMapping(value = "/bugsummarycomments", method = RequestMethod.GET)
	public @ResponseBody List<Map<String, Object>> bugSummaryComments(
			@RequestParam(value = "page", defaultValue = "1") int page, Model model, HttpSession session)
			throws Exception {
		List<Map<String, Object>> attachment = new ArrayList<Map<String, Object>>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-Y");
		Date date = new Date();

		logger.info("Redirecting to  bug summary page and checking for active session");

		utilsService.getLoggerUser(session);

		int pageSize = 5;
		model.addAttribute("pageSize", pageSize);
		int startValue = (page - 1) * pageSize;

		String schema = session.getAttribute("cust_schema").toString();
		int bugId = Integer.parseInt(session.getAttribute("selectedBugId").toString());
		logger.warn("Session is active, bugId is " + bugId + " Customer Schema " + schema);

		int projectId = Integer.parseInt(session.getAttribute("selectedBugProjectId").toString());
		Map<String, Object> data = bugSummaryService.getDataForBugSummary(projectId, bugId, startValue, pageSize,
				schema);
		List<Map<String, Object>> summaryDetails = (List<Map<String, Object>>) data.get("#result-set-5");
		for (int i = 0; i < summaryDetails.size(); i++) {
			int summaryId = Integer.parseInt(summaryDetails.get(i).get("bs_id").toString());
			List<Map<String, Object>> commentAttachments = bugService.getCommentAttachmentDeatils(summaryId, schema);
			for (int j = 0; j < commentAttachments.size(); j++) {
				attachment.add(commentAttachments.get(j));
			}
			String logo = loginService.getImage((byte[]) summaryDetails.get(i).get("user_photo"));
			summaryDetails.get(i).put("updater_logo", logo);
			summaryDetails.get(i).put("update_date",
					utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
							utilsService.getDate(summaryDetails.get(i).get("update_date").toString())));

		}
		List<Map<String, Object>> bugDetails = (List<Map<String, Object>>) data.get("#result-set-3");
		for (int i = 0; i < bugDetails.size(); i++) {
			bugDetails.get(i).put("create_date",
					utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
							utilsService.getDate(bugDetails.get(i).get("create_date").toString())));

			if (bugDetails.get(i).get("bug_id").toString().equals(summaryDetails.get(i).get("bug_id").toString())) {
				if (bugDetails.get(i).get("assign_status").toString().equals("2")) {
					summaryDetails.get(i).put("bug_assignee_name", "None");
					summaryDetails.get(i).put("prev_assignee_name", "None");
				} else if (bugDetails.get(i).get("group_status").toString().equals("1")) {
					int bugGroupId = Integer.parseInt(bugDetails.get(i).get("group_id").toString());
					List<Map<String, Object>> bugGroup = bugGroupDAO.getGroupbyId(bugGroupId, schema);
					summaryDetails.get(i).put("bug_assignee_name", bugGroup.get(0).get("bug_group_name"));
				}
			}
		}
		return summaryDetails;

	}

	/**
	 * @author suresh.adling
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :" + ex);
		return new ModelAndView("redirect:/500");
	}

	@RequestMapping(value = "/customizesearch")
	public ModelAndView bugAnalysis(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Bug Analysis page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userId + " Customer Schema " + schema);
			List<Map<String, Object>> getAllBugs;

			Map<String, Object> data = reportService.getBugAnalysisData(userId, schema);
			List<Map<String, Object>> releaseList = (List<Map<String, Object>>) data.get("#result-set-1");
			model.addAttribute("releaseList", releaseList);
			List<Map<String, Object>> buildList = (List<Map<String, Object>>) data.get("#result-set-2");
			model.addAttribute("buildList", buildList);
			List<Map<String, Object>> statusList = (List<Map<String, Object>>) data.get("#result-set-3");
			model.addAttribute("statusList", statusList);
			List<Map<String, Object>> priorityList = (List<Map<String, Object>>) data.get("#result-set-4");
			model.addAttribute("priorityList", priorityList);
			List<Map<String, Object>> severityList = (List<Map<String, Object>>) data.get("#result-set-5");
			model.addAttribute("severityList", severityList);
			List<Map<String, Object>> queryData = (List<Map<String, Object>>) data.get("#result-set-6");
			model.addAttribute("queryData", queryData);
			List<Map<String, Object>> buildExeType = (List<Map<String, Object>>) data.get("#result-set-7");
			model.addAttribute("buildExeType", buildExeType);
			List<Map<String, Object>> autoBuildList = (List<Map<String, Object>>) data.get("#result-set-8");
			model.addAttribute("autoBuildList", autoBuildList);

			try {
				model.addAttribute("JsonData", JsonString);

				for (int i = 0; i < bugResult.size(); i++) {
					bugResult.get(i).put("update_date",
							utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
									utilsService.getDate(bugResult.get(i).get("update_date").toString())));
				}

				model.addAttribute("getAllBugs", bugResult);

				Map<String, Object> statusData = statusResult;
				Map<String, Object> priorityData = priorityResult;
				Map<String, Object> severityData = severityResult;
				model.addAttribute("statusData", statusData.toString().replaceAll("=", ":"));
				model.addAttribute("priorityData", priorityData.toString().replaceAll("=", ":"));
				model.addAttribute("severityData", severityData.toString().replaceAll("=", ":"));
			} catch (Exception e) {

			}
			JsonString = null;
			bugResult = new ArrayList<Map<String, Object>>();
			statusResult = new HashMap<String, Object>();
			priorityResult = new HashMap<String, Object>();
			severityResult = new HashMap<String, Object>();

			model.addAttribute("UserSavedQueries", customSearchService.getUserSavedCustomQueries(userId, schema));

			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			logger.info(" User is redirected to custom search page ");
			return new ModelAndView("customizesearch", "Model", model);
		}
	}

	@RequestMapping(value = "/savecustomquery", produces = MediaType.APPLICATION_JSON, method = RequestMethod.POST)
	public ModelAndView saveCustomQuery(Model model, HttpSession session, HttpServletRequest req,
			HttpServletResponse res, @RequestParam("jsonArray") String jsonANDData,
			@RequestParam("queryName") String queryName) throws Exception {

		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("bugsummary", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			String schema = session.getAttribute("cust_schema").toString();

			String userId = session.getAttribute("userId").toString();
			int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
			try {
				Map<String, Object> returnData = customSearchService.getCustomQueryData(userId, schema, jsonANDData,
						queryName, projectId);
				Map<String, String> reponseData = customSearchService.insertCustomSearchQuery(schema, queryName,
						returnData.get("customQuery").toString(), returnData.get("jsonObject").toString(),
						Integer.parseInt(userId));
				if (Integer.parseInt(reponseData.get("responseCode")) == 201
						&& reponseData.get("status").equalsIgnoreCase("Query inserted successfully.")) {
					logger.info("Query inserted successfully. Redirected to View customize search page ");
					return new ModelAndView("redirect:/customizesearch");
				} else {
					logger.info("Query insertion unsuccessful. Redirected to error page ");
					return new ModelAndView("redirect:/500");
				}

			} catch (Exception ex) {
				ex.printStackTrace();
				return new ModelAndView("redirect:/500");
			}

		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	@RequestMapping(value = "/executeCustomQuery", produces = MediaType.APPLICATION_JSON, method = RequestMethod.POST)
	public ModelAndView executeCustomQuery(Model model, HttpSession session, HttpServletRequest req,
			HttpServletResponse res, @RequestParam("executequery") String jsonANDData) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("bugsummary", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			try {
				bugResult = null;
				statusResult = null;
				priorityResult = null;
				severityResult = null;
				JsonString = null;
				String schema = session.getAttribute("cust_schema").toString();
				int userID = Integer.parseInt(session.getAttribute("userId").toString());
				int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
				Map<String, Object> returnModel = customSearchService.executeCustomQuery(jsonANDData, schema, userID,
						projectId);
				List<Map<String, Object>> bugData = (List<Map<String, Object>>) returnModel.get("getAllBugs");
				Map<String, Object> statusData = (Map<String, Object>) returnModel.get("statusQueryResult");
				Map<String, Object> priorityData = (Map<String, Object>) returnModel.get("priorityQueryResult");
				Map<String, Object> severityData = (Map<String, Object>) returnModel.get("severityQueryResult");
				String JsonData = returnModel.get("JSonData").toString();
				bugResult = bugData;
				statusResult = statusData;
				priorityResult = priorityData;
				severityResult = severityData;
				JsonString = JsonData;
				logger.info(" User is redirected to custom search page ");
				return new ModelAndView("redirect:/customizesearch");

			} catch (Exception ex) {
				ex.printStackTrace();
				JsonString = null;
				bugResult = new ArrayList<Map<String, Object>>();
				statusResult = new HashMap<String, Object>();
				priorityResult = new HashMap<String, Object>();
				severityResult = new HashMap<String, Object>();
				return new ModelAndView("redirect:/500");
			}
		} else {
			logger.info(" User is redirected to 500 page ");
			return new ModelAndView("redirect:/500");
		}

	}

	@RequestMapping(value = "/savepiechartquery", produces = MediaType.APPLICATION_JSON, method = RequestMethod.POST)
	public ModelAndView savePiechartQuery(Model model, HttpSession session, HttpServletRequest req,
			HttpServletResponse res, @RequestParam("jsonArray") String jsonANDData,
			@RequestParam("queryName") String queryName) throws Exception {

		String schema = session.getAttribute("cust_schema").toString();
		try {
			String userId = session.getAttribute("userId").toString();
			Map<String, Object> returnData = customSearchService.savePieChartQuery(userId, schema, jsonANDData,
					queryName);
			Map<String, String> reponseData = customSearchService.insertPiechartCustomSearchQuery(schema, queryName,
					returnData.get("pieChartQueryQuery").toString(), returnData.get("jsonObject").toString(),
					Integer.parseInt(userId));
			if (Integer.parseInt(reponseData.get("responseCode")) == 201
					&& reponseData.get("status").equalsIgnoreCase("Query inserted successfully.")) {
				logger.info("Query inserted successfully. Redirected to View customize search page ");
				return new ModelAndView("redirect:/piechartbugreport");
			} else {
				logger.info("Query insertion unsuccessful. Redirected to error page ");
				return new ModelAndView("redirect:/500");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		logger.info(" User is redirected to custom search page ");
		return new ModelAndView("redirect:/piechartbugreport");
	}

	@RequestMapping(value = "/executeCustomPieChartQuery", produces = MediaType.APPLICATION_JSON, method = RequestMethod.POST)
	public ModelAndView executeCustomPieChartQuery(Model model, HttpSession session, HttpServletRequest req,
			HttpServletResponse res, @RequestParam("jsonArray") String jsonANDData,
			@RequestParam("queryName") String queryName) throws Exception {

		try {
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());

			Map<String, Object> returnModel = customSearchService.executeCustomPieChartQuery(jsonANDData, schema);
			// List<Map<String, Object>> bugData = (List<Map<String, Object>>)
			// returnModel.get("getAllBugs");

			// String JsonData = returnModel.get("JSonData").toString();

			// session.setAttribute("getAllBugs", bugData);
			// session.setAttribute("JsonData", JsonData);
			session.setAttribute("returnModel", returnModel);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		logger.info(" User is redirected to custom search page ");
		return new ModelAndView("redirect:/piechartbugreport");

	}

	@RequestMapping(value = "/updatecustomquery", produces = MediaType.APPLICATION_JSON, method = RequestMethod.POST)
	public ModelAndView updateCustomQuery(Model model, HttpSession session, HttpServletRequest req,
			HttpServletResponse res, @RequestParam("jsonArray") String jsonANDData,
			@RequestParam("queryName") String queryName, @RequestParam("queryId") String queryId) throws Exception {

		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("bugsummary", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {

			String schema = session.getAttribute("cust_schema").toString();

			String userId = session.getAttribute("userId").toString();
			int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
			try {
				Map<String, Object> returnData = customSearchService.getCustomQueryData(userId, schema, jsonANDData,
						queryName, projectId);

				Map<String, String> reponseData = customSearchService.updateCustomSearchQuery(schema, queryName,
						returnData.get("customQuery").toString(), returnData.get("jsonObject").toString(),
						Integer.parseInt(userId), Integer.parseInt(queryId));
				if (Integer.parseInt(reponseData.get("responseCode")) == 201
						&& reponseData.get("status").equalsIgnoreCase("Query inserted successfully.")) {
					logger.info("Query inserted successfully. Redirected to View customize search page ");
					return new ModelAndView("redirect:/customizesearch");
				} else {

					logger.info("Query insertion unsuccessful. Redirected to error page ");
					return new ModelAndView("redirect:/500");
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.info(" User is redirected to 500 page ");
				return new ModelAndView("redirect:/500");
			}
		} else {
			logger.info(" User is redirected to 500 page ");
			return new ModelAndView("redirect:/500");
		}
		/*
		 * logger.info(" User is redirected to custom search page "); return new
		 * ModelAndView("redirect:/customizesearch");
		 */

	}

	@RequestMapping(value = "/updatepiechartcustomquery", produces = MediaType.APPLICATION_JSON, method = RequestMethod.POST)
	public ModelAndView updatePirChartCustomQuery(Model model, HttpSession session, HttpServletRequest req,
			HttpServletResponse res, @RequestParam("jsonArray") String jsonANDData,
			@RequestParam("queryName") String queryName, @RequestParam("queryId") String queryId) throws Exception {

		String schema = session.getAttribute("cust_schema").toString();
		String userId = session.getAttribute("userId").toString();
		try {
			Map<String, Object> returnData = customSearchService.getCustomQueryPieChartData(userId, schema, jsonANDData,
					queryName);

			Map<String, String> reponseData = customSearchService.updatePieChartQuery(schema, queryName,
					returnData.get("customQuery").toString(), returnData.get("jsonObject").toString(),
					Integer.parseInt(userId), Integer.parseInt(queryId));
			if (Integer.parseInt(reponseData.get("responseCode")) == 201
					&& reponseData.get("status").equalsIgnoreCase("Query inserted successfully.")) {
				logger.info("Query inserted successfully. Redirected to View customize search page ");
				return new ModelAndView("redirect:/piechartbugreport");
			} else {
				logger.info("Query insertion unsuccessful. Redirected to error page ");
				return new ModelAndView("redirect:/500");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		logger.info(" User is redirected to pie chart page ");
		return new ModelAndView("redirect:/piechartbugreport");

	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request is used to delete custom search query
	 * 
	 * @param queryId
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteCustomSearchQuery", method = RequestMethod.POST)
	public @ResponseBody String deleteCustomSearchQuery(@RequestParam("queryId") String queryId, HttpSession session)
			throws Exception {
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();

		Map<String, String> reponseData = customSearchService.deleteCustomSearchQuery(Integer.parseInt(queryId),
				schema);
		if (Integer.parseInt(reponseData.get("responseCode")) == 201) {
			logger.info("Query deleted successfully. Redirected to View customize search page ");
			return "success";
		} else {
			logger.info("Query deletion unsuccessful. Redirected to error page ");
			return "failure";
		}
	}

	@RequestMapping(value = "/removebug", method = RequestMethod.POST)
	public @ResponseBody boolean removeBug(Model model, HttpSession session, @RequestParam("bugId") int bugId) {
		logger.info("Ajax Post request to delete bug");
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		int flag = bugService.deleteBug(bugId, schema);
		if (flag == 200) {
			return true;
		} else {
			return false;
		}
	}

	@Scheduled(cron = "0 05 2 * * ?")
	public void scheduleJiraSync() {
		System.out.println("Initiating Jira Sync using Cron Job");
		if (user_id == null) {
			logger.error("Session has expired,user is redirected to login page");

		} else if (bugDAO.checkBTRoleAccess("createbug", role_id, cust_schema)) {
			int projectID = Integer.parseInt(user_current_proj_id);
			logger.info("Xenon Project Selected with id as : " + projectID);
			List<Map<String, Object>> project = projectService.getSingleProject(projectID, cust_schema);
			String jiraProject = project.get(0).get("issue_tracker_project_key").toString();
			String moduleId = "";
			List<Map<String, Object>> modules = moduleDao.getModulesByProject(projectID, cust_schema);
			if (modules.size() > 0) {
				moduleId = modules.get(0).get("module_id").toString();
			} else {
				moduleId = "1";
			}
			try {
				if (jiraIntegrationService.searchJiraIssueByUpdatedDate(jiraProject, cust_schema, customer_id,
						projectID, moduleId)) {
					jiraIntegrationService.fetchAndUpdateReleaseVersion(customer_id, cust_schema, jiraProject);

				}
			} catch (IOException e) {
				logger.error("Error ocurred while scheduling Jira cron job: " + e.getMessage());
			} catch (ParseException e) {
				logger.error("Error ocurred while scheduling Jira cron job: " + e.getMessage());
			} catch (JiraException e) {
				logger.error("Error ocurred while scheduling Jira cron job: " + e.getMessage());
			}
		}

	}
}

package com.xenon.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.QueryParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.administration.domain.JenkinsDetails;
import com.xenon.api.administration.JenkinsBuildService;
import com.xenon.api.administration.JenkinsDetailsService;
import com.xenon.api.administration.JenkinsService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;

@Controller
public class JenkinsDetailsController {

	private static final Logger logger = LoggerFactory.getLogger(JenkinsDetailsController.class);

	@Autowired
	UtilsService utilsService;

	@Autowired
	LoginService loginService;

	@Autowired
	JenkinsDetailsService jenkinsDetailsService;

	@Autowired
	JenkinsService jenkinsService;

	@Autowired
	BuildDAO buildDAO;

	@Autowired
	JenkinsBuildService jenkinsBuildService;
	
	@Resource(name = "configurationProperties")
	private Properties configurationProperties;

	@RequestMapping(value = "/removeUsersJobPermission", method = RequestMethod.POST)
	public ModelAndView removeUsersJobPermission(@RequestParam("userId") String userId,@RequestParam("jobId") String jobId, Model model, HttpSession session) {
		
		String coreSchema = session.getAttribute("cust_schema").toString();
		jenkinsDetailsService.removeUsersJobPermission(userId,jobId,coreSchema);
		model.addAttribute("jobId",jobId);		
		return new ModelAndView("redirect:/jenkinsjobsettings","model",model);
	}
	
	@RequestMapping(value = "/removeUsersInstancePermission", method = RequestMethod.POST)
	public ModelAndView removeUsersInstancePermission(@RequestParam("userId") String userId,@RequestParam("instanceId") String instanceId, Model model, HttpSession session) {
		
		String coreSchema = session.getAttribute("cust_schema").toString();
		jenkinsDetailsService.removeUsersInstancePermission(userId,instanceId,coreSchema);
		return new ModelAndView("redirect:/jenkinserversetting","model",model);
	}
	
	
	@RequestMapping(value = "/revokeUsersJobPermission", method = RequestMethod.POST)
	public ModelAndView revokeUsersJobPermission(@RequestParam("userId") String userId,@RequestParam("jobId") String jobId, Model model, HttpSession session) {
		
		String coreSchema = session.getAttribute("cust_schema").toString();
		jenkinsDetailsService.revokeUsersJobPermission(userId,jobId,coreSchema);
		model.addAttribute("jobId",jobId);		
		return new ModelAndView("redirect:/jenkinsjobsettings","model",model);
	}
	
	@RequestMapping(value = "/revokeUsersInstancePermission", method = RequestMethod.POST)
	public ModelAndView revokeUsersInstancePermission(@RequestParam("userId") String userId,@RequestParam("instanceId") String instanceId, Model model, HttpSession session) {
		
		String coreSchema = session.getAttribute("cust_schema").toString();
		
		jenkinsDetailsService.revokeUsersInstancePermission(userId,instanceId,coreSchema);
		
		return new ModelAndView("redirect:/jenkinserversetting","model",model);
	}
	
	
	@RequestMapping(value = "/updateUsersJobPermission", method = RequestMethod.POST)
	public ModelAndView updateUsersJobPermission(@RequestParam(value ="editPermissionUsers",required=false) String editPermissionUsers,
			@RequestParam(value = "executePermissionUsers", required=false) String executePermissionUsers,
			@RequestParam("jobId") String jobId, Model model, HttpSession session) {
		String coreSchema = session.getAttribute("cust_schema").toString();	
		if(editPermissionUsers!=null || executePermissionUsers!=null) {
			jenkinsDetailsService.updateUsersJobPermission(editPermissionUsers,executePermissionUsers,jobId,coreSchema);
		}
		model.addAttribute("jobId",jobId);		
		return new ModelAndView("redirect:/jenkinsjobsettings","model",model);
	}
	
	@RequestMapping(value = "/updateUsersInstancePermission", method = RequestMethod.POST)
	public ModelAndView updateUsersInstancePermission(@RequestParam(value ="editPermissionUsers",required=false) String editPermissionUsers,
			@RequestParam("instanceId") String instanceId, Model model, HttpSession session) {
		String coreSchema = session.getAttribute("cust_schema").toString();	
		if(editPermissionUsers!=null) {
			jenkinsDetailsService.updateUsersInstancePermission(editPermissionUsers,instanceId,coreSchema);
		}
		return new ModelAndView("redirect:/jenkinserversetting","model",model);
	}
	
	@RequestMapping(value = "/addPermissionToMember", method = RequestMethod.POST)
	public ModelAndView addPermissionToMember(@RequestParam("users") String users,@RequestParam("jobId") String jobId, Model model, HttpSession session) {
		String coreSchema = session.getAttribute("cust_schema").toString();
		String[] allUserIds = new String[0];		
		if(users!=null && !users.trim().equals("")) {
			if(users.contains(",")) {
				allUserIds=users.split(",");
			}
			else {
				allUserIds = new String[1];
				allUserIds[0] = users;
			}
		}
		jenkinsDetailsService.assignJobPermissionToUsers(allUserIds,jobId,coreSchema);
		model.addAttribute("jobId",jobId);		
		return new ModelAndView("redirect:/jenkinsjobsettings","model",model);
	}
	
	@RequestMapping(value = "/addInstancePermissionToMember", method = RequestMethod.POST)
	public ModelAndView addInstancePermissionToMember(@RequestParam("users") String users,@RequestParam("instanceId") String instanceId, Model model, HttpSession session) {
		String coreSchema = session.getAttribute("cust_schema").toString();
		String[] allUserIds = new String[0];		
		if(users!=null && !users.trim().equals("")) {
			if(users.contains(",")) {
				allUserIds=users.split(",");
			}
			else {
				allUserIds = new String[1];
				allUserIds[0] = users;
			}
		}
		jenkinsDetailsService.assignInstancePermissionToUsers(allUserIds,instanceId,coreSchema);
		
		return new ModelAndView("redirect:/jenkinserversetting","model",model);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/jenkinsjobsettings", method = RequestMethod.GET)
	public ModelAndView getJenkinsSettings(@RequestParam("jobId") String jobId, Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (jenkinsDetailsService.checkJobAccess("update_status",session.getAttribute("userId").toString(),jobId,
				session.getAttribute("cust_schema").toString())) {
		String coreSchema = session.getAttribute("cust_schema").toString();
		Map<String, Object> memberData =jenkinsDetailsService.getJobPermissionWiseMembers(jobId,coreSchema);
		
		List<Map<String, Object>> permissionAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-1");
		for (int i = 0; i < permissionAssignedMembers.size(); i++) {
			String photo = loginService.getImage((byte[]) permissionAssignedMembers.get(i).get("user_photo"));
			permissionAssignedMembers.get(i).put("photo", photo);
		}
		model.addAttribute("permissionAssignedMembers", permissionAssignedMembers);
		
		List<Map<String, Object>> permissionNonAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-2");
		model.addAttribute("permissionNonAssignedMembers", permissionNonAssignedMembers);
		
		List<Map<String, Object>> updatePermissionAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-3");
		model.addAttribute("updatePermissionAssignedMembers", updatePermissionAssignedMembers);
		
		List<Map<String, Object>> executePermissionAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-4");
		model.addAttribute("executePermissionAssignedMembers", executePermissionAssignedMembers);
		
		List<Map<String, Object>> noPermissionAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-5");
		model.addAttribute("noPermissionAssignedMembers", noPermissionAssignedMembers);
		
		model.addAttribute("jobId",jobId);
		
		JenkinsDetails jenkinsDetails =jenkinsDetailsService.getJenkinsJobDetails(Integer.parseInt(""+jobId), coreSchema);
		model.addAttribute("jenkinsDetails",jenkinsDetails);
		
		Map<String, Object> instanceInformation = jenkinsDetailsService.getInstanceInformationById(jenkinsDetails.getInstanceId(),coreSchema);
		model.addAttribute("jenkinsInstanceName",instanceInformation.get("jenkin_name"));
		
		return new ModelAndView("jenkinsjobsettings","model",model); 
		}else{
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	



	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/createjenkinsjob")
	public ModelAndView createJenkinsJob(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			Map<String, Object> instanceData = jenkinsDetailsService.createJenkinsJob(schema, userID);
			List<Map<String, Object>> jobList = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> instanceList = (List<Map<String, Object>>) instanceData.get("instanceData");

			/*
			 * for(int cnt=0;cnt<instanceList.size();cnt++){
			 * 
			 * System.out.println(instanceList.size());
			 * System.out.println(instanceMap.size());
			 */
			for (int i = 0; i < instanceList.size(); i++) {
				Map<String, Object> instanceMap = instanceList.get(i);
				String jenkinUrl = instanceMap.get("jenkin_url").toString();
				String jenkinId = instanceMap.get("jenkin_id").toString();
				String jenkinUserName = instanceMap.get("jenkin_username").toString();
				String jenkinToken = instanceMap.get("jenkin_token").toString();
				Map<String, Object> returnData = jenkinsService.readJenkinsAllJobs(jenkinUrl, jenkinUserName,
						jenkinToken, jenkinId);
				String responseCode = returnData.get("responseCode").toString();
				if (responseCode.equals("200")) {
					List<Map<String, Object>> status = (List<Map<String, Object>>) returnData.get("status");
					jobList.addAll(status);
				}

				
			}
			Iterator it = instanceData.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				model.addAttribute((String) pair.getKey(), pair.getValue());
				it.remove();
			}

			model.addAttribute("jobList", jobList);
			return new ModelAndView("createjenkinsjob", "Model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}

	}

	@RequestMapping(value = "/jenkinsinstances", method = RequestMethod.GET)
	public ModelAndView createJenkinsInstance(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			return new ModelAndView("jenkinsinstances");
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}

	}

	@RequestMapping(value = "/insertjenkinsinstance", method = RequestMethod.POST)
	public ModelAndView insertJenkinsInstance(@RequestParam("serverName") String serverName,
			@RequestParam("username") String username, @RequestParam("jenkinsUrl") String jenkinsUrl,
			@RequestParam("apiToken") String apiToken, @RequestParam("instanceStatus") String instanceStatus,
			Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {

			String currentSchema = session.getAttribute("cust_schema").toString();

			jenkinsDetailsService.insertJenkinsInstance(serverName, username, jenkinsUrl, apiToken, instanceStatus,
					currentSchema);

			return new ModelAndView("redirect:/viewjenkinsurls");
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	@RequestMapping(value = "/apitokenjenkins", method = RequestMethod.GET)
	public ModelAndView showApiToken(@QueryParam("serverName") String serverName, Model model, HttpSession session,
			HttpServletRequest request) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		}

		else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			try {
				model.addAttribute("serverName", serverName);
				String currentSchema = session.getAttribute("cust_schema").toString();
				int serverInstanceId = jenkinsDetailsService.getServerIdByServerName(serverName, currentSchema);
				List<Map<String, Object>> tokenDetails = jenkinsDetailsService.getApiTokenDetails(serverInstanceId);
				if (tokenDetails != null && !tokenDetails.isEmpty()) {
					model.addAttribute("tokenDetails", tokenDetails.get(0));
				}

				return new ModelAndView("apitokenjenkins", "model", model);
			} catch (Exception e) {
				e.printStackTrace();
				return new ModelAndView("500");
			}
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}

	}

	@RequestMapping(value = "/generateapitoken", method = RequestMethod.GET)
	public ModelAndView generateApiToken(@QueryParam("serverName") String serverName, Model model, HttpSession session,
			HttpServletRequest request) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		}

		else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			try {
				model.addAttribute("serverName", serverName);
				
				String currentSchema = session.getAttribute("cust_schema").toString();
				int serverInstanceId = jenkinsDetailsService.getServerIdByServerName(serverName, currentSchema);
				int custId = Integer.parseInt("" + session.getAttribute("customerId"));
				int userId = Integer.parseInt("" + session.getAttribute("userId"));

				String token = utilsService.generateRandomString(32);
				String windowsScript = configurationProperties.getProperty("HOST_NAME")+"/jenkinprebuildaction?apiToken="+token+"&jobName=${JOB_NAME}&buildNo=${BUILD_NUMBER}";
				String powershellScript = configurationProperties.getProperty("HOST_NAME")+"/jenkinpostbuildaction?apiToken="+token+"&jobName=${JOB_NAME}&buildNo=${BUILD_NUMBER}";

				jenkinsDetailsService.insertNewApiToken(serverInstanceId, custId, userId, token, windowsScript,
						powershellScript);

				return new ModelAndView("redirect:/apitokenjenkins");
			} catch (Exception e) {
				e.printStackTrace();
				return new ModelAndView("500");
			}
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}

	}

	@RequestMapping(value = "/viewjenkinsurls", method = RequestMethod.GET)
	public ModelAndView viewJenkinsUrls(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {

			String currentSchema = session.getAttribute("cust_schema").toString();
			List<Map<String, Object>> allRows = jenkinsDetailsService.getAllInstances(currentSchema,session.getAttribute("userId").toString());
			model.addAttribute("allRows", allRows);
			session.setAttribute("menuLiText", "Plugins");
			if(session.getAttribute("roleId").toString().trim().equalsIgnoreCase("2")) {
				model.addAttribute("isAdminUser", "true");
			}else {
				model.addAttribute("isAdminUser", "false");
			}
			return new ModelAndView("viewjenkinsurls", "model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	@RequestMapping(value = "/updatejenkinsjob")
	public ModelAndView updateJenkinsJob(Model model, HttpSession session,
			@RequestParam("jobId") String jobId,
			@RequestParam("jobname") String jobname, @RequestParam("jobDesciption") String jobDesciption,
			@RequestParam("jobStatus") String jobStatus, @RequestParam("buildtoken") String buildToken) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			String schema = session.getAttribute("cust_schema").toString();
			int flag = jenkinsDetailsService.updateJenkinsJob(Integer.parseInt(jobId), jobname,
					jobDesciption, Integer.parseInt(jobStatus), buildToken, schema);
			
			model.addAttribute("jobId",jobId);
			if (flag == 201) {
				logger.info("Job is updated successfully");
				return new ModelAndView("redirect:/jenkinsjobsettings");
			} else {
				logger.error("Something went wrong, User redirected to error page");
				return new ModelAndView("redirect:/500");
			}

		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	@RequestMapping(value = "/insertjenkinsjob")
	public ModelAndView insertJenkinsJob(Model model, HttpSession session,
			@RequestParam("instanceid") String instanceId, @RequestParam("jenkinsJobName") String jenkinsJobName,
			@RequestParam("jobname") String jobname, @RequestParam("jobDesciption") String jobDesciption,
			@RequestParam("jobStatus") String jobStatus, @RequestParam("buildtoken") String buildToken) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			int buildStatus = 4;
			int flag = jenkinsDetailsService.insertJenkinsJob(Integer.parseInt(instanceId), jenkinsJobName, jobname,
					jobDesciption, Integer.parseInt(jobStatus), buildStatus, buildToken, schema, userID);
			/*int instanceId=jenkinsDetailsService.getServerIdByServerName(instanceName,schema);*/
			session.setAttribute("currentInstanceId", instanceId);
			if (flag == 201) {
				logger.info("Job is added successfully");
				return new ModelAndView("redirect:/jenkinsreports");
			} else {
				logger.error("Something went wrong, User redirected to error page");
				return new ModelAndView("redirect:/500");
			}

		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request reads jenkins reports
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/jenkinsreports")
	public ModelAndView jenkinsreports(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Bug Analysis page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Continuous Integration");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			String instanceID=session.getAttribute("currentInstanceId").toString();
			Map<String, Object> returnData = jenkinsDetailsService.jenkinsReport(schema, userID,instanceID);
			List<Map<String, Object>> jobList = (List<Map<String, Object>>) returnData.get("JobData");
			
			for (Map<String, Object> jobInformation : jobList) {
			  int jobId = Integer.parseInt(""+jobInformation.get("jenkin_jobId"));
			  int isUpdateEnabled = jenkinsDetailsService.checkIsUpdateEnabled(jobId,userID,schema);
			  int isExecuteEnabled = jenkinsDetailsService.checkIsExecuteEnabled(jobId,userID,schema);
			  jobInformation.put("isUpdateEnabled", isUpdateEnabled);
			  jobInformation.put("isExecuteEnabled", isExecuteEnabled);
			}
			
			if(session.getAttribute("roleId").toString().trim().equalsIgnoreCase("2")) {
				model.addAttribute("isAdminUser", "true");
			}else {
				model.addAttribute("isAdminUser", "false");
			}
			model.addAttribute("JobData", jobList);
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			logger.info(" User is redirected to Bug Analysis page ");
			return new ModelAndView("jenkinsreports", "Model", model);
		}
	}

	
	@RequestMapping(value = "/setviewjobid", method = RequestMethod.POST)
	public @ResponseBody void setviewjobid(Model model, HttpSession session, @RequestParam("jobId") String jobId) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("viewJobIdBuilds", jobId);
	}
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request reads builds history
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/buildhistory")
	public ModelAndView buildhistory(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to build Analysis page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Continuous Integration");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			int jobId=Integer.parseInt(session.getAttribute("viewJobIdBuilds").toString());
			Map<String,Object> buildData=jenkinsBuildService.getBuildHistory(jobId,schema);
			List<Map<String,Object>> buildList=(List<Map<String, Object>>) buildData.get("buildData");
			/*for(int cnt=0;cnt<buildList.size();cnt++) {
				buildList.get(cnt).put("timestamp", utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), buildList.get(cnt).get("timestamp").toString()));
			}*/
			
			model.addAttribute("buildData",buildList);
			model.addAttribute("buildDataJson",utilsService.convertListOfMapToJson(buildList));

			logger.info(" User is redirected to build Analysis page ");
			return new ModelAndView("buildhistory", "Model", model);
		}
	}

	@RequestMapping(value = "/verifyjenkinsapitoken", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> verifyjenkinsapitoken(Model model, HttpSession session,
			@RequestParam("requesturl") String requesturl, @RequestParam("username") String username,
			@RequestParam("apiToken") String passwordOrToken) throws Exception {
		utilsService.getLoggerUser(session);
		return jenkinsService.verifyRequestUrl(requesturl, username, passwordOrToken);
	}

	@RequestMapping(value = "/readjenkinsjobs", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> readjenkinsjobs(Model model, HttpSession session,
			@RequestParam("requesturl") String requesturl, @RequestParam("username") String username,
			@RequestParam("apiToken") String passwordOrToken, @RequestParam("instanceId") String instanceId)
			throws Exception {
		utilsService.getLoggerUser(session);
		return jenkinsService.readJenkinsAllJobs(requesturl, username, passwordOrToken, instanceId);
	}

	@RequestMapping(value = "/jenkinprebuildaction", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> jenkinprebuildaction(Model model, HttpSession session,
			@RequestParam("apiToken") String apiToken, @RequestParam("buildNo") String buildNo,
			@RequestParam("jobName") String jobName) throws Exception {
		// IN PROGRESS
		utilsService.getLoggerUser(session);
		Map<String, Object> response = jenkinsBuildService.insertJenkinsBuildDetails(jobName, apiToken, buildNo);
		return response;
	}
	//Added by Swapnil W - to fetch report from jenkins server
	@RequestMapping(value = "/jenkinfetchreportaction", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> jenkinfetchreportaction(Model model, HttpSession session,
			@RequestParam("apiToken") String apiToken,
			@RequestParam("jobName") String jobName) throws Exception {
		// IN PROGRESS
		utilsService.getLoggerUser(session);
		//Build No not required, hence passing 1
		Map<String, Object> response = jenkinsBuildService.insertJenkinsBuildDetails(jobName, apiToken, "1");
		return response;
	}
	
	@RequestMapping(value = "/jenkinpostbuildaction", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> jenkinpostbuildaction(Model model, HttpSession session,
			@RequestParam("apiToken") String apiToken, @RequestParam("buildNo") String buildNo,
			@RequestParam("jobName") String jobName) throws Exception {
		utilsService.getLoggerUser(session);
		// IN PROGRESS
		Map<String, Object> response = jenkinsBuildService.updateJenkinsBuildDetails(jobName, apiToken, buildNo);
		return response;
	}

	@RequestMapping(value = "/triggerbuild", method = RequestMethod.POST)
	public @ResponseBody int triggerbuild(Model model, HttpSession session,
			@RequestParam("JenkinsJobId") String JenkinsJobId) throws Exception {
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		Map<String,Object> returnData=jenkinsBuildService.getBuildTriggerData(schema, Integer.parseInt(JenkinsJobId));
		List<Map<String,Object>> getbuildData=(List<Map<String, Object>>) returnData.get("buildTrigger");
		if(getbuildData.size()>0){
		 
				Map<String, Object> data = jenkinsService.triggerBuild(getbuildData.get(0).get("jenkin_url").toString(), getbuildData.get(0).get("jenkin_username").toString(), getbuildData.get(0).get("jenkin_token").toString(), getbuildData.get(0).get("jenkin_job").toString(), getbuildData.get(0).get("build_trigger_token").toString());
				return Integer.parseInt(data.get("responseCode").toString());
		}
		else
		{
			return 500;
		}
	}
	
	@RequestMapping(value = "/jenkinservers", method = RequestMethod.GET)
	public ModelAndView jenkinservers(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {

			String currentSchema = session.getAttribute("cust_schema").toString();
			List<Map<String, Object>> allRows = jenkinsDetailsService.getAllInstances(currentSchema,session.getAttribute("userId").toString());
			model.addAttribute("allRows", allRows);

			if(session.getAttribute("roleId").toString().trim().equalsIgnoreCase("2")) {
				model.addAttribute("isAdminUser", "true");
			}else {
				model.addAttribute("isAdminUser", "false");
			}
			
			return new ModelAndView("jenkinservers", "model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}

	}

	@RequestMapping(value = "/setinstanceid", method = RequestMethod.POST)
	public @ResponseBody void setInstanceid(Model model, HttpSession session,
			@RequestParam("instanceId") String instanceId) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("currentInstanceId", instanceId);
	}
	
	
	
	@RequestMapping(value = "/jenkinserversetting", method = RequestMethod.GET)
	public ModelAndView serverSetting(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
		utilsService.getLoggerUser(session);
		String serverId=session.getAttribute("currentInstanceId").toString();
		String schema = session.getAttribute("cust_schema").toString();
		
		Map<String, Object> memberData =jenkinsDetailsService.getInstancePermissionWiseMembers(serverId,schema);
		
		List<Map<String, Object>> permissionAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-1");
		for (int i = 0; i < permissionAssignedMembers.size(); i++) {
			String photo = loginService.getImage((byte[]) permissionAssignedMembers.get(i).get("user_photo"));
			permissionAssignedMembers.get(i).put("photo", photo);
		}
		model.addAttribute("permissionAssignedMembers", permissionAssignedMembers);
		
		List<Map<String, Object>> permissionNonAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-2");
		model.addAttribute("permissionNonAssignedMembers", permissionNonAssignedMembers);
		
		List<Map<String, Object>> updatePermissionAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-3");
		model.addAttribute("updatePermissionAssignedMembers", updatePermissionAssignedMembers);
		
		List<Map<String, Object>> executePermissionAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-4");
		model.addAttribute("executePermissionAssignedMembers", executePermissionAssignedMembers);
		
		List<Map<String, Object>> noPermissionAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-5");
		model.addAttribute("noPermissionAssignedMembers", noPermissionAssignedMembers);
		
		List<Map<String, Object>> serverDetails = (List<Map<String, Object>>) memberData.get("#result-set-6");
		model.addAttribute("ServerDetails",serverDetails);

		return new ModelAndView("jenkinserversetting", "model", model);
		}
		else{
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	
	
	@RequestMapping(value = "/updatejenkinserver", method = RequestMethod.POST)
	public ModelAndView updateServerDetails(Model model, HttpSession session,@RequestParam("serverId") String serverId,@RequestParam("serverName") String serverName,@RequestParam("username") String username,@RequestParam("jenkinsUrl") String jenkinsUrl,@RequestParam("apiToken") String apiToken,@RequestParam("instanceStatus") String serverStatus) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		Map<String,Object> serverDetails=jenkinsDetailsService.updateServerDetails(Integer.parseInt(serverId),serverName,username,jenkinsUrl,apiToken,Integer.parseInt(serverStatus),schema);
		int responseCode=Integer.parseInt(serverDetails.get("responseCode").toString());
		if(responseCode==201){
			
			return new ModelAndView("redirect:/viewjenkinsurls");
		}
		else{
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
		}else{
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
		
		
	}
}

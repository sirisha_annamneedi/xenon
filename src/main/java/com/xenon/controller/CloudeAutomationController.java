package com.xenon.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.ServerOperationService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.AutomationBuildDAO;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.BuildExecDetailsDAO;
import com.xenon.buildmanager.dao.VMDetailsDAO;
import com.xenon.buildmanager.domain.BuildExecDetails;
import com.xenon.controller.utils.Constants;
import com.xenon.remote.domain.CloudeMachineOperation;
import com.xenon.remote.domain.RemoteMachineException;

@Component
@Controller
public class CloudeAutomationController {
	@Resource(name = "configurationProperties")
	private Properties configurationProperties;
	
	private static final Logger logger = LoggerFactory.getLogger(BugController.class);
	private CloudeMachineOperation cloudeMachineOperation;

	@Autowired
	RoleAccessService roleAccessService;
	
	@Autowired
	BuildExecDetailsDAO buildExecDetailsDAO;

	@Autowired
	AutomationBuildDAO automationBuildDAO;
	
	@Autowired
	VMDetailsDAO vmdetails;
	
	@Autowired
	BuildDAO buildDao;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	ServerOperationService serverOperationService;

	@RequestMapping(value = "/startoncloudeexecution", method = RequestMethod.POST)
	public @ResponseBody String startOnCloudeExecution(@RequestParam("exebrowser") final String exebrowser,@RequestParam("selectBrowser") final String browser,
			@RequestParam(value="selectEnv[]",defaultValue="0") final String env,
			@RequestParam("selectMailGroup") final int mailGroupId,
			@RequestParam("stepwiseScreenshotStatus") final int stepWiseScreenshot,
			@RequestParam("reportBugStatus") final int reportBug, final HttpSession session) throws Exception {

		utilsService.getLoggerUser(session);
		logger.info("In on cloude customer execution method");
		cloudeMachineOperation = CloudeMachineOperation.getInstance();
		final String schema = session.getAttribute("cust_schema").toString();
		final int userId = Integer.parseInt(session.getAttribute("userId").toString());
		final int buildId = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());

		buildDao.updateAutomationBuildStatus(9, buildId, schema);

		// int customerId =
		// Integer.parseInt(session.getAttribute("customerId").toString());
		int customerId = Integer.parseInt(session.getAttribute("customerId").toString());// setting/getting all properties with xenon customer
							// context
		//buildDao.updateAutomationBuildStatus(4, buildId, schema);
		String responseStatus=null;
		String serverWSStatus;
		try {
			if (buildDao.checkBMRoleAccess("createbuild",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
				serverWSStatus=cloudeMachineOperation.getRemoteMachineStatus(configurationProperties.getProperty("XENON_SERVER_WS"));
				if(serverWSStatus.equals("200")){
					responseStatus = cloudeMachineOperation.handleRemoteStatus(cloudeMachineOperation,configurationProperties.getProperty("XENON_SERVER_WS"));
					
					if (responseStatus.equals(Constants.XENON_STATUS_SUCCESS)) {
						String freeVMList = cloudeMachineOperation
								.getRequest(configurationProperties.getProperty("XENON_SERVER_WS") + "/getcloudefreevm?customerId=", "" + customerId);

						JSONArray freeVMListArray = new JSONArray(freeVMList);
						if (freeVMListArray.length() > 0) {
							JSONObject freeVMObject = freeVMListArray.getJSONObject(0);
							final String freeVMId = freeVMObject.get("cloude_vm_id").toString();

							JSONObject postParameter = new JSONObject();
							postParameter.put("vmId", freeVMId);
							postParameter.put("userId", userId);
							postParameter.put("buildId", buildId);
							postParameter.put("custId", customerId);
							String allocatedVM = cloudeMachineOperation.postRequest(
									configurationProperties.getProperty("XENON_SERVER_WS") + "/allocatecloudevm?postjson=", postParameter.toString());
						
							JSONArray allocatedVMArray = new JSONArray(allocatedVM);
							if (allocatedVMArray.length() > 0) {

								List<Map<String, Object>> getVMDetails = buildExecDetailsDAO.getVMDetails(schema, userId,
										buildId);
								final String REMOTE_URL = getVMDetails.get(0).get("inetaddress").toString() + "/XenonClientWS";
								final int allocateVMId = Integer.parseInt(getVMDetails.get(0).get("xe_vm_id").toString());
								
								
								
								Runnable startVm=new Runnable() {
									
									@Override
									public void run() {
										String responceClient = null;
										serverOperationService.pollWSRequest(1000000, REMOTE_URL, schema, userId, buildId, vmdetails, ""+allocateVMId);
										try {
											responceClient = cloudeMachineOperation.getRemoteMachineStatus(REMOTE_URL);
										} catch (RemoteMachineException e) {
										}finally{
											try {
												cloudeMachineOperation.postRequest(
														configurationProperties.getProperty("XENON_SERVER_WS") + "/deallocatecloudevm?allocatedvmid="+freeVMId+"&schema="+schema+"&buildId="+buildId+"&userId="+userId,"");
											} catch (RemoteMachineException e) {
												e.printStackTrace();
											}
										
										}
										if (responceClient.equals("200")) {
											int envStatus=2;
											if(env.equalsIgnoreCase("0")){
												envStatus=2;
											}else{
												envStatus=1;
											}
											BuildExecDetails buildExecDetails = buildExecDetailsDAO.setBuildExecDetailsValues(
													buildId, browser, mailGroupId, stepWiseScreenshot, reportBug, userId,
													allocateVMId,envStatus);
											int flag;
											try {
												if(envStatus==1)
													flag = buildExecDetailsDAO.insertBuildExecDetails(buildExecDetails,env, schema);
													else
														flag = buildExecDetailsDAO.insertBuildExecDetails(buildExecDetails,"", schema);
											
											if (flag == 1) {
												try {
													executeOnCloudeBuild(exebrowser, session);
												} catch (Exception e) {
												}
											} else {
												buildExecDetailsDAO.changeVMStatus(freeVMId, 1);
											}
											} catch (SQLException e) {
											}
										} else {
											logger.info("Remote machine not running");
											buildExecDetailsDAO.changeVMStatus(freeVMId, 1);
											responceClient = Constants.XENON_CLIENT_INACTIVE;
										}
									}
								};
								
								Thread startVmThread=new Thread(startVm);
								startVmThread.start();
								//session.setAttribute("newBuildTypeForTab", "automation");
								return Constants.XENON_CLIENT_STARTED;
								
								/*responseStatus = cloudeMachineOperation.getRemoteMachineStatus(REMOTE_URL);
								if (responseStatus.equals("200")) {
									session.setAttribute("newBuildTypeForTab", "automation");
									BuildExecDetails buildExecDetails = buildExecDetailsDAO.setBuildExecDetailsValues(
											buildId, browser, mailGroupId, stepWiseScreenshot, reportBug, userId,
											allocateVMId);
									int flag = buildExecDetailsDAO.insertBuildExecDetails(buildExecDetails, schema);
									if (flag == 1) {
										
										 * buildDAO.sendMailOnSatrtOfAutoBuildExe(
										 * Integer.parseInt(session.getAttribute(
										 * "automationExecBuildId").toString()),
										 * schema);
										 
										return Constants.XENON_CLIENT_STARTED;
									} else {
										buildExecDetailsDAO.changeVMStatus(freeVMId, 1);
										return Constants.XENON_EXCEPTION;
									}
								} else {
									logger.info("Remote machine not running");
									buildExecDetailsDAO.changeVMStatus(freeVMId, 1);
									responseStatus = Constants.XENON_CLIENT_INACTIVE;
								}*/
							} else {
								logger.error("Allocate cloud VM issue");
								return Constants.XENON_EXCEPTION;
							}

						} else {
							logger.info("FREE VM NOT FOUND");
							return Constants.XENON_VM_NOT_FOUND;
						}
					} else {
						logger.error("VM Server WS not running");
						logger.error("Server WS not running");
						return responseStatus; // server or vm not running
					}
					
				}
				else{
					buildDao.updateAutomationBuildStatus(2, buildId, schema);
					logger.error("Server WS not running");
					return Constants.XENON_SERVER_INACTIVE;
				}
			} else {
				logger.error("User do not having access for create build");
				return Constants.XENON_EXCEPTION;
			}
		} catch (Exception e) {
			logger.error("Something went wrong = " + e.getMessage());
			e.printStackTrace();
			return Constants.XENON_EXCEPTION;

		}
	}

	@RequestMapping(value = "/executeoncloudebuild", method = RequestMethod.POST)
	public @ResponseBody String executeOnCloudeBuild(@RequestParam("exebrowser") String exebrowser, HttpSession session)
			throws Exception {
		utilsService.getLoggerUser(session);
		logger.info("In buildautomation execution method");
		cloudeMachineOperation = CloudeMachineOperation.getInstance();
		int userId = Integer.parseInt(session.getAttribute("userId").toString());
		int buildId = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());
		String schema = session.getAttribute("cust_schema").toString();
		String responseStatus = null;
		//buildDao.updateAutomationBuildStatus(4, buildId, schema);
		List<Map<String, Object>> autoBuildList = automationBuildDAO.getAutoBuildDetail(schema, buildId);
		String userName = autoBuildList.get(0).get("user_name").toString();
		String dirName = schema + userName + cloudeMachineOperation.getTimeStamp();

		if (cloudeMachineOperation.createDirectory(configurationProperties.getProperty("AUTOMATION_REPOSITORY_PATH")+"repository/upload/tmpFiles/" + dirName,true)) {
			if (cloudeMachineOperation.createFile(configurationProperties.getProperty("AUTOMATION_REPOSITORY_PATH")+"repository/upload/logFiles/" + dirName + ".txt",true)) {
				int flag = automationBuildDAO.updateBuildLogfile(buildId, dirName, schema);
				if (flag == 1) {
					flag = automationBuildDAO.insertExecution(dirName, buildId, schema);
					if (flag == 1) {
						String buildName = autoBuildList.get(0).get("build_name").toString();
						String projectLocation = autoBuildList.get(0).get("project_location").toString();
						String inetAddress = autoBuildList.get(0).get("inetaddress").toString();
						String REMOTE_URL = inetAddress + "/XenonClientWS";
						String stepWiseScreenShot = utilsService
								.getScreenShotStatusFromId(autoBuildList.get(0).get("step_wise_screenshot").toString());
						String reportBug = utilsService
								.getRerportBugStatusFromId(autoBuildList.get(0).get("report_bug").toString());

						List<Map<String, Object>> autoTestList = automationBuildDAO.getAutomationTestCases(schema,
								buildId);
						JSONArray testArray = new JSONArray(autoTestList);
						String ouathPass = schema + userName;

						JSONObject autoPostParameter = new JSONObject();
						autoPostParameter.put("DIRENAME", dirName);
						autoPostParameter.put("BUILDNAME", buildName);
						autoPostParameter.put("USER_NAME", userName);
						autoPostParameter.put("SCHEMA", schema);
						autoPostParameter.put("USER", userId);
						autoPostParameter.put("BUILD_ID", buildId);
						autoPostParameter.put("EXE_BROWSER", exebrowser);
						autoPostParameter.put("OAUTH_PASS", ouathPass);
						autoPostParameter.put("USER_ID", session.getAttribute("userEmailId").toString());
						autoPostParameter.put("CLIENT_NAME", session.getAttribute("customerName").toString());
						autoPostParameter.put("SCREENSHOT", stepWiseScreenShot);
						autoPostParameter.put("RAISED_BUG", reportBug);
						autoPostParameter.put("AUTO_EXE_JSON", testArray.toString());
						autoPostParameter.put("CLIENT_VM_URL", REMOTE_URL);
						autoPostParameter.put("SERVER_VM_URL",configurationProperties.getProperty("XENON_SERVER_WS"));
						autoPostParameter.put("PROJECT_PATH", projectLocation);

						int customerId = Integer.parseInt(session.getAttribute("customerId").toString());
						List<Map<String, Object>> getVMDetails = buildExecDetailsDAO.getVMDetails(schema, userId,
								buildId);
						String vmIPAddress = getVMDetails.get(0).get("ip_address").toString();
						String port = getVMDetails.get(0).get("port").toString();
						int vmId = Integer.parseInt(getVMDetails.get(0).get("xe_vm_id").toString());
						flag = automationBuildDAO.insertbuildExecDetails(autoPostParameter.toString(), vmIPAddress,port,
								vmId, buildId, userId, customerId, schema);
						responseStatus = cloudeMachineOperation.postRequest(
								configurationProperties.getProperty("XENON_SERVER_WS") + "/executeoncloudbuild?postjson=", autoPostParameter.toString());
						if (flag != 1) {
							logger.info("Something went wrong,Build details not inserted");
							return Constants.XENON_EXCEPTION;
						}
					} else {
						logger.info("Something went wrong,Build details not inserted");
						return Constants.XENON_EXCEPTION;
					}

				} else {
					logger.info("Something went wrong, Build Status Not changed");
					return Constants.XENON_EXCEPTION;
				}

			} else {
				logger.info("Something went wrong, Log File Not Created");
				return Constants.FILE_STATUS_NOT_CREATED;
			}
		} else {
			logger.info("Something went wrong,Execution Directory Not Created");
			return Constants.DIRECTORY_STATUS_NOT_CREATED;
		}
		return responseStatus;
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :" + ex);
		return new ModelAndView("redirect:/500");
	}

}

package com.xenon.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.common.UtilsService;
import com.xenon.common.dao.UtilizationDAO;
import com.xenon.common.domain.Utilization;



/**
 * 
 * This class handles all pre and post requests 
 *
 */
public class XenonInterceptor implements HandlerInterceptor {

	@Autowired
	UtilizationDAO utilizationService;
	
	@Autowired
	UtilsService utilsService;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		long startTime = System.currentTimeMillis();
		request.setAttribute("startTime", startTime);

		HttpSession session = request.getSession(false);
		String resourceURL= request.getRequestURI();
		String loginPage = request.getServletPath();


		if(resourceURL.contains("password") || loginPage.contains("login.jsp")|| loginPage.contains("authenticateUserRequest")||resourceURL.contains("logout")||resourceURL.contains("resources"))
		{

		}
		else
		{
			if(utilsService.checkSessionContainsAttribute("userId", session))
			{
				if (session.getAttribute("userId") != null) 
				{
					if(Integer.parseInt(session.getAttribute("passFlag").toString())== 2)
					{
						session.setAttribute("userId",null);
					}
				}
			}

		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		//		//System.out.println("Post-handle");
		//		//System.out.println("\n\nRequest URL::" + request.getRequestURL().toString()
		//				+ " Sent to Handler :: Current Time=" + System.currentTimeMillis());
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

		  // getBandwidthUsage(request);

	}

	@SuppressWarnings("deprecation")
	public synchronized void getBandwidthUsage(HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		File file =new File(request.getRealPath(request.getServletPath()));
		String schema = session.getAttribute("cust_schema").toString();
		long startTime = (Long) request.getAttribute("startTime");
		if(session.getAttribute("userEmailId") != null)
		{
			String userName=session.getAttribute("userEmailId").toString();
			double dataTransferKB=0;
			if(file.exists()){
				double bytes = file.length();
				dataTransferKB = (bytes / 1024);
			    //System.out.println("URL :: "+request.getRequestURL().toString()+"  Time Taken ::"+(System.currentTimeMillis() - startTime)+" Data transfer in MB : " + dataTransferKB /1024);
			    
			    	    Utilization usage=new Utilization();
						usage.setBandwidthUsage(dataTransferKB/1024);
						usage.setUserName(userName);
						utilizationService.updateUtilization(usage, schema);
			    
			} else{
				
				//System.out.println("Resource does not exists!");
			}
		}
	}
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response,HttpSession session) {
		utilsService.sendMailonExecption(ex,session);
		return new ModelAndView("redirect:/500");
	}

	
}
package com.xenon.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.administration.ProjectService;
import com.xenon.api.buildmanager.BuildService;
import com.xenon.api.buildmanager.ReleaseService;
import com.xenon.api.buildmanager.TrialExecutionService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.api.testmanager.TestCaseService;
import com.xenon.bugtracker.dao.BugDAO;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.ExecuteBuildDAO;
import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.dao.ReleaseDAO;
import com.xenon.buildmanager.dao.ReportDAO;
import com.xenon.common.pojos.GenerateReportRequest;
import com.xenon.common.pojos.ReportBuildDetails;
import com.xenon.common.pojos.TestCaseData;
import com.xenon.common.pojos.TestCaseDataDetails;
import com.xenon.controller.utils.Constants;
import com.xenon.testmanager.dao.ScenarioDAO;
import com.xenon.testmanager.dao.TestcaseDAO;
import com.xenon.testmanager.dao.TestcaseStepDAO;
import com.xenon.util.service.FileUploadForm;

/**
 * 
 * @author prafulla.pol Purpose : The ReportSummary controller is used for
 *         generating reports.
 *
 */
@SuppressWarnings("unchecked")
@Component
@RestController
public class ReportSummaryController {
	@Resource(name = "configurationProperties")
	private Properties configurationProperties;

	@Resource(name = "colorProperties")
	private Properties colorProperties;
	@Autowired
	ProjectDAO projectDao;

	@Autowired
	ExecuteBuildDAO executeBuildDAO;

	@Autowired
	ModuleDAO moduleDao;

	@Autowired
	ScenarioDAO scenarioDao;

	@Autowired
	TestcaseDAO testcaseDao;

	@Autowired
	ReleaseDAO releaseDAO;

	@Autowired
	BuildDAO buildDAO;

	@Autowired
	TestcaseStepDAO testcaseStepDao;

	@Autowired
	TrialExecutionService trialExecutionService;

	@Autowired
	BugDAO bugService;

	@Autowired
	ReportDAO reportService;

	@Autowired
	LoginService loginService;

	@Autowired
	UtilsService utilsService;
	
	@Autowired
	ReleaseService releaseService;
	
	@Autowired
	BuildService buildService;
	
	@Autowired
	TestCaseService testCaseService;
	
	@Autowired
	ModuleDAO moduleDAO;
	
	@Autowired
	ProjectService projectService;

	private static final Logger logger = LoggerFactory.getLogger(ReportSummaryController.class);
	private HashMap<Integer, HashMap<String, String>> stepData = new HashMap<Integer, HashMap<String, String>>();
	/**
	 * @author abhay.thakur
	 * @method redwoodReport POST Request
	 */
	@RequestMapping(value = "insertredwoodexesteps", method = RequestMethod.POST)
	public ResponseEntity<String> insertautoexesteps(@RequestBody String resultJson) {
		try {
			JSONObject jsonObject = new JSONObject(resultJson);
			String redwoodBuildId= jsonObject.get("testset").toString();
			String redwoodTestCaseNo= jsonObject.get("testCaseNo").toString();
			int testCaseStatus=5,actionResult=5,bugRaise=1;String action,actions="";
			String schemaName="",screenshot=jsonObject.get("screenshot").toString();
			String screenshotTitle=jsonObject.get("screenshotTitle").toString();
			
			//byte[] screenshot=screenshotStr.getBytes(); 
			
			if(jsonObject.get("result").toString().equals("Passed")){
				testCaseStatus=1;
			}else if(jsonObject.get("result").toString().equals("Failed")){
				testCaseStatus=2;
			}else{
				testCaseStatus=5;
			}
			JSONArray actionAry = (JSONArray) jsonObject.get("actions");
			JSONObject clientJson = jsonObject.getJSONObject("clientDeatils");
			schemaName=clientJson.get("schemaName").toString();
			for (int i = 0; i < actionAry.length(); i++) {
				action="";
				JSONObject jsonObjAction = actionAry.getJSONObject(i);
				if(jsonObjAction.get("result").toString().equals("Passed")){
					actionResult=1;
					bugRaise=1;
				}else if(jsonObjAction.get("result").toString().equals("Failed")){
					actionResult=2;
					bugRaise=2;
				}else{
					actionResult=5;
					bugRaise=1;
				}
				action=i+1+","+jsonObjAction.get("name").toString()+","+jsonObjAction.get("value").toString()+","+actionResult+","+bugRaise;
				if(actions!="") {
					actions+="|"+action;
				}else {
					actions=action;
				}

			}
			try {
				buildDAO.insertRedwoodExecutionSteps(actions, redwoodBuildId, redwoodTestCaseNo, screenshot,screenshotTitle, testCaseStatus, schemaName);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return new ResponseEntity<String>("Success", HttpStatus.OK);
		} catch (JSONException e) {
			e.printStackTrace();
			return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/modulereport")
	public ModelAndView modulereport(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			return new ModelAndView("modulereport");
		}
	}

	/**
	 * @author prafulla.pol
	 * @method buildDetails GET Request
	 * @param Model
	 *            model
	 * @param HttpSession
	 *            session
	 * @return BuildDetails which are eligible for reporting
	 * @throws Exception
	 * @description buildDetails method returns Build Details which are eligible
	 *              for reporting based on the users access to the automation
	 *              build
	 */
	@RequestMapping(value = "/builddetails")
	public ModelAndView buildDetails(@RequestParam(value = "manual", defaultValue = "1") int manual,
			@RequestParam(value = "auto", defaultValue = "1") int auto,
			@RequestParam(value = "sc", defaultValue = "1") int sc,
			@RequestParam(value = "buildType", defaultValue = "manual") String buildType, Model model,
			HttpSession session) throws Exception {
		logger.info("Redirecting to Builds page in report and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			if (utilsService.checkSessionContainsAttribute("remoteMachineStatus", session)) {
				model.addAttribute("remoteMachineStatus", session.getAttribute("remoteMachineStatus"));
			} else {
				model.addAttribute("remoteMachineStatus", 0);
			}

			int pageSize = 10;
			model.addAttribute("pageSize", pageSize);

			int manualStartValue = (manual - 1) * pageSize;
			int autoStartValue = (auto - 1) * pageSize;
			int scStartValue = (sc - 1) * pageSize;
			int autoStatus = Integer.parseInt(session.getAttribute("automationStatus").toString());
			int scriptlessStatus = Integer.parseInt(session.getAttribute("custScriptlessStatus").toString());

			Map<String, Object> data = buildDAO.getBuildReportData(autoStatus, scriptlessStatus, manualStartValue,
					autoStartValue, scStartValue, pageSize, schema);

			// List<Map<String, Object>> allBuildDetails =
			// buildDAO.getAllExecutedBuildDetails(schema);
			List<Map<String, Object>> allBuildDetails = (List<Map<String, Object>>) data.get("#result-set-1");
			for (int i = 0; i < allBuildDetails.size(); i++) {
				String photo = loginService.getImage((byte[]) allBuildDetails.get(i).get("user_photo"));
				allBuildDetails.get(i).put("photo", photo);
				allBuildDetails.get(i).put("build_createdate",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(allBuildDetails.get(i).get("build_createdate").toString())));
			}
			model.addAttribute("allBuildDetails", allBuildDetails);
			int manualBuildCount = (Integer) data.get("manualbuildcount");
			model.addAttribute("manualBuildCount", manualBuildCount);

			if (autoStatus == 1) {
				List<Map<String, Object>> automationBuildDetails = (List<Map<String, Object>>) data
						.get("#result-set-2");
				for (int i = 0; i < automationBuildDetails.size(); i++) {
					String photo = loginService.getImage((byte[]) automationBuildDetails.get(i).get("user_photo"));
					automationBuildDetails.get(i).put("photo", photo);
					automationBuildDetails.get(i).put("build_createdate", utilsService.getDateTimeOfTimezone(
							session.getAttribute("userTimezone").toString(),
							utilsService.getDate(automationBuildDetails.get(i).get("build_createdate").toString())));
				}
				model.addAttribute("autoBuildDetails", automationBuildDetails);
				int autobuildcount = (Integer) data.get("autobuildcount");
				model.addAttribute("autobuildcount", autobuildcount);
			}

			if (scriptlessStatus == 1) {
				List<Map<String, Object>> scriptBuildDetails = (List<Map<String, Object>>) data.get("#result-set-3");

				for (int i = 0; i < scriptBuildDetails.size(); i++) {
					String photo = loginService.getImage((byte[]) scriptBuildDetails.get(i).get("user_photo"));
					scriptBuildDetails.get(i).put("photo", photo);
					scriptBuildDetails.get(i).put("build_createdate", utilsService.getDateTimeOfTimezone(
							session.getAttribute("userTimezone").toString(),
							utilsService.getDate(scriptBuildDetails.get(i).get("build_createdate").toString())));
				}
				model.addAttribute("scriptBuildDetails", scriptBuildDetails);
				int scbuildcount = (Integer) data.get("scbuildcount");
				model.addAttribute("scbuildcount", scbuildcount);
			}

			model.addAttribute("automationStatus", autoStatus);
			model.addAttribute("scriptlessStatus",
					Integer.parseInt(session.getAttribute("custScriptlessStatus").toString()));
			logger.info(" User is redirected to Builds page in report ");

			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			return new ModelAndView("builddetails", "Model", model);
		}
	}

	@RequestMapping(value = "stepwisedetails")
	public ModelAndView stepWiseDetails(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			String buildId = session.getAttribute("automationBuildId").toString();
			String schema = session.getAttribute("cust_schema").toString();
			List<Map<String, Object>> excuteBuildDetails = executeBuildDAO
					.getExecBuildDetails(Integer.parseInt(buildId), schema);
			model.addAttribute("ExecuteBuild", excuteBuildDetails);
			return new ModelAndView("stepwisedetails", "Model", model);
		}
	}

	@RequestMapping(value = "inserttrialexesteps", method = RequestMethod.POST)
	public @ResponseBody String insertTrialExeSteps(@RequestParam("build") String build,
			@RequestParam("project") String project, @RequestParam("module") String module,
			@RequestParam("scenario") String scenario, @RequestParam("testcase") String testcase,
			@RequestParam("testStep") String testStep, @RequestParam("stepDetails") String stepDetails,
			@RequestParam("stepInputType") String stepInputType, @RequestParam("stepValue") String stepValue,
			@RequestParam("stepStatus") String stepStatus, @RequestParam("screenshotTitle") String screenshotTitle,
			HttpSession session) throws Exception {
		String schema = "xenon";
		String filepath = configurationProperties.getProperty("XENON_REPOSITORY_PATH") + "repository" + File.separator
				+ "upload" + File.separator + "tmpFiles" + File.separator + screenshotTitle + ".png";
		utilsService.getLoggerUser(session);
		trialExecutionService.insertExecutionSteps(build, project, module, scenario, testcase, testStep, stepDetails,
				stepInputType, stepValue, stepStatus, filepath, screenshotTitle, schema);
		return "sucess";
	}

	/**
	 * @author prafulla.pol
	 * @method ProjectWiseReport GET Request
	 * @param Model
	 *            model
	 * @param HttpSession
	 *            session
	 * @return build report
	 * @throws Exception
	 * @description ProjectWiseReport method gets the build ID from the session
	 *              and return the report for that build id
	 */
	@RequestMapping(value = "/buildreport")
	public ModelAndView ProjectWiseReport(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Build Report page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			int buildId = 0, releaseId = 0;
			String buildName = "";
			// int buildId =
			// Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userId + " Customer Schema " + schema);

			if (session.getAttribute("selectedExecuteBuildId") == null) {
				List<Map<String, Object>> lastBuildDetails = buildDAO.getlastBuildDetails(userId, schema);
				for (int i = 0; i < lastBuildDetails.size(); i++) {
					buildId = (Integer) lastBuildDetails.get(i).get("build_id");
					buildName = (String) lastBuildDetails.get(i).get("build_name");
					releaseId = (Integer) lastBuildDetails.get(i).get("release_id");
				}
				session.setAttribute("selectedExecuteBuildId", buildId);
				session.setAttribute("selectedExecuteReleaseId", releaseId);
				session.setAttribute("BuildId", buildId);
				session.setAttribute("selectedExecuteBuildName", buildName);
			} else {
				buildId = Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());
				session.setAttribute("selectedExecuteBuildId", buildId);
				session.setAttribute("BuildId", buildId);
			}

			List<Map<String, Object>> manBuilds = reportService.getManualBuilds(schema);
			model.addAttribute("manualBuilds", manBuilds);

			Map<String, Object> buildReportData = buildDAO.getBuildReportDataByBuildId(buildId, userId, schema);

			List<Map<String, Object>> projectDetailsByBuildId = projectDao.selectProjectDetailsByBuildId(
					(List<Map<String, Object>>) buildReportData.get("#result-set-1"),
					(List<Map<String, Object>>) buildReportData.get("#result-set-10"),
					(List<Map<String, Object>>) buildReportData.get("#result-set-11"));

			for (int i = 0; i < projectDetailsByBuildId.size(); i++) {
				projectDetailsByBuildId.get(i).put("project_createdate", utilsService.getDateTimeOfTimezone(
						session.getAttribute("userTimezone").toString(),
						utilsService.getDate(projectDetailsByBuildId.get(i).get("project_createdate").toString())));
			}
			model.addAttribute("projectsList", projectDetailsByBuildId);

			List<Map<String, Object>> moduleDetailsByBuildId = moduleDao.selectModuleDetailsByBuildId(
					(List<Map<String, Object>>) buildReportData.get("#result-set-2"),
					(List<Map<String, Object>>) buildReportData.get("#result-set-12"),
					(List<Map<String, Object>>) buildReportData.get("#result-set-13"));
			model.addAttribute("modulesList", moduleDetailsByBuildId);

			List<Map<String, Object>> scenarioDetailsByBuildId = scenarioDao.selectScenarioDetailsByBuildId(
					(List<Map<String, Object>>) buildReportData.get("#result-set-3"),
					(List<Map<String, Object>>) buildReportData.get("#result-set-14"),
					(List<Map<String, Object>>) buildReportData.get("#result-set-15"));
			model.addAttribute("scenarioList", scenarioDetailsByBuildId);

			List<Map<String, Object>> testcaseDetailsByBuildId = (List<Map<String, Object>>) buildReportData
					.get("#result-set-4");
			model.addAttribute("testcasesList", testcaseDetailsByBuildId);

			List<Map<String, Object>> executeBugs = (List<Map<String, Object>>) buildReportData.get("#result-set-5");

			List<Map<String, Object>> testcaseStepsList = (List<Map<String, Object>>) buildReportData
					.get("#result-set-6");

			model.addAttribute("testcaseStepsList", testcaseStepsList);
			model.addAttribute("step_bug", executeBugs);

			List<Map<String, Object>> statusList = (List<Map<String, Object>>) buildReportData.get("#result-set-7");
			model.addAttribute("statusList", statusList);

			List<Map<String, Object>> tcExecDetailsList = (List<Map<String, Object>>) buildReportData
					.get("#result-set-8");

			model.addAttribute("tcExecDetailsList", utilsService.convertListOfMapToJson(tcExecDetailsList));
			model.addAttribute("tcExecDetailsList1", tcExecDetailsList);

			List<Map<String, Object>> tcStepStatus = (List<Map<String, Object>>) buildReportData.get("#result-set-9");
			model.addAttribute("tcStepStatus", utilsService.convertListOfMapToJson(tcStepStatus));

			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			model.addAttribute("selectedExecuteBuildName", session.getAttribute("selectedExecuteBuildName").toString());
			model.addAttribute("Pass", colorProperties.getProperty("testCase.Pass"));
			model.addAttribute("Fail", colorProperties.getProperty("testCase.Fail"));
			model.addAttribute("Skip", colorProperties.getProperty("testCase.Skip"));
			model.addAttribute("Block", colorProperties.getProperty("testCase.blocked"));
			model.addAttribute("NotRun", colorProperties.getProperty("testCase.NotRun"));

			// get the attachments
			List<Map<String, Object>> attachmentsList = (List<Map<String, Object>>) buildReportData
					.get("#result-set-16");
			model.addAttribute("attachmentsList", attachmentsList);

			List<Map<String, Object>> testerDeatilsBybuildId = (List<Map<String, Object>>) buildReportData
					.get("#result-set-17");
			model.addAttribute("testerDeatils", testerDeatilsBybuildId);

			/*
			 * List<Map<String, Object>> BuildtestCase = (List<Map<String,
			 * Object>>) buildReportData.get("#result-set-17");
			 * model.addAttribute("BuildtestCase", BuildtestCase);
			 */

			logger.info(" User is redirected to Build Report page ");
			return new ModelAndView("buildreport", "Model", model);

		}
	}

	@RequestMapping(value = "/buildreportmanual")
	public ModelAndView buildReportManual(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Build Report page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			// System.out.println("ReportSummaryController.buildReportManual()");
			// System.out.println(model.asMap().get("paramMap"));
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			int buildId = 0, releaseId = 0;
			String buildName = "";
			// int buildId =
			// Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int projectID = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userId + " Customer Schema " + schema);

			if (session.getAttribute("selectedExecuteBuildId") == null) {
				List<Map<String, Object>> lastBuildDetails = buildDAO.getlastBuildDetails(userId, schema);
				for (int i = 0; i < lastBuildDetails.size(); i++) {
					buildId = (Integer) lastBuildDetails.get(i).get("build_id");
					buildName = (String) lastBuildDetails.get(i).get("build_name");
					releaseId = (Integer) lastBuildDetails.get(i).get("release_id");
				}
				session.setAttribute("selectedExecuteBuildId", buildId);
				session.setAttribute("selectedExecuteReleaseId", releaseId);
				session.setAttribute("BuildId", buildId);
				session.setAttribute("selectedExecuteBuildName", buildName);
			} else {
				buildId = Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());
				session.setAttribute("selectedExecuteBuildId", buildId);
				session.setAttribute("BuildId", buildId);
			}
			   List<Map<String, Object>> getQueryforBugTracker = reportService.getFilteredvaluesforBugTracker(userId,
	                    schema);
	            model.addAttribute("getQueryforBugTracker", getQueryforBugTracker);
	           
	           
	            List<Map<String, Object>> getQueryforTEMTracker = reportService.getQueryforTEMTracker(userId,
	                    schema);
	            model.addAttribute("getQueryforTEMTracker", getQueryforTEMTracker);
			List<Map<String, Object>> manBuilds = reportService.getManualBuilds(schema);
			model.addAttribute("manualBuilds", manBuilds);

			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			model.addAttribute("selectedExecuteBuildName", session.getAttribute("selectedExecuteBuildName").toString());
			model.addAttribute("Pass", colorProperties.getProperty("testCase.Pass"));
			model.addAttribute("Fail", colorProperties.getProperty("testCase.Fail"));
			model.addAttribute("Skip", colorProperties.getProperty("testCase.Skip"));
			model.addAttribute("Block", colorProperties.getProperty("testCase.blocked"));
			model.addAttribute("NotRun", colorProperties.getProperty("testCase.NotRun"));

			Map<String, Object> buildReportFilteringData = reportService.getBuildReportManualFilteringData(schema);
			model.addAttribute("releasesJSON", utilsService
					.convertListOfMapToJson((List<Map<String, Object>>) buildReportFilteringData.get("#result-set-1")));
			model.addAttribute("testSetsJSON", utilsService
					.convertListOfMapToJson((List<Map<String, Object>>) buildReportFilteringData.get("#result-set-2")));
			model.addAttribute("modulesJSON", utilsService
					.convertListOfMapToJson((List<Map<String, Object>>) buildReportFilteringData.get("#result-set-3")));
			model.addAttribute("scenariosJSON", utilsService
					.convertListOfMapToJson((List<Map<String, Object>>) buildReportFilteringData.get("#result-set-4")));
			model.addAttribute("statusesJSON", utilsService
					.convertListOfMapToJson((List<Map<String, Object>>) buildReportFilteringData.get("#result-set-5")));
			model.addAttribute("testersJSON", utilsService
					.convertListOfMapToJson((List<Map<String, Object>>) buildReportFilteringData.get("#result-set-6")));
			model.addAttribute("tcExecSummaryJSON",
					(buildReportFilteringData.get("#result-set-7") != null) ? utilsService.convertListOfMapToJson(
							(List<Map<String, Object>>) buildReportFilteringData.get("#result-set-7")) : "{}");
			List<Map<String, Object>> ExecutionManualQueryFilter = reportService.ExecutionManualQueryFilter(userId,projectID ,
                    schema);
            model.addAttribute("ExecutionManualQueryFilter", ExecutionManualQueryFilter);
            
            List<Map<String, Object>> SummaryExecutionManualQueryFilter = reportService.SummaryExecutionManualQueryFilter(userId,projectID ,
                    schema);
            model.addAttribute("SummaryExecutionManualQueryFilter", SummaryExecutionManualQueryFilter);
           
            model.addAttribute("ExecutionManualQueryFilter1", utilsService.convertListOfMapToJson(ExecutionManualQueryFilter));
			logger.info(" User is redirected to Build Report page ");
			return new ModelAndView("buildreport", "Model", model);

		}
	}

	@RequestMapping(value = "/filterBuildReportManual")
	public @ResponseBody List<Map<String, Object>> filterBuildReportManual(
			@RequestParam MultiValueMap<String, String> paramMap, HttpServletRequest request, Model model,
			HttpSession session) throws Exception {
		logger.info("Redirecting to Build Report page and checking for active session");
		List<Map<String, Object>> maps = new ArrayList<Map<String, Object>>();
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return maps;
		} else {
			String schema = session.getAttribute("cust_schema").toString();
			maps = reportService.buildReportManual(schema, paramMap);
			return maps;
		}
	}

	@RequestMapping(value = "/viewreport")
	public ModelAndView getDashBoardReport(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Build Report page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			int buildId = Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userId + " Customer Schema " + schema);

			Map<String, Object> buildReportData = buildDAO.getBuildReportDataByBuildId(buildId, userId, schema);

			List<Map<String, Object>> projectDetailsByBuildId = projectDao.selectProjectDetailsByBuildId(
					(List<Map<String, Object>>) buildReportData.get("#result-set-1"),
					(List<Map<String, Object>>) buildReportData.get("#result-set-10"),
					(List<Map<String, Object>>) buildReportData.get("#result-set-11"));

			for (int i = 0; i < projectDetailsByBuildId.size(); i++) {
				projectDetailsByBuildId.get(i).put("project_createdate", utilsService.getDateTimeOfTimezone(
						session.getAttribute("userTimezone").toString(),
						utilsService.getDate(projectDetailsByBuildId.get(i).get("project_createdate").toString())));
			}
			model.addAttribute("projectsList", projectDetailsByBuildId);

			List<Map<String, Object>> moduleDetailsByBuildId = moduleDao.selectModuleDetailsByBuildId(
					(List<Map<String, Object>>) buildReportData.get("#result-set-2"),
					(List<Map<String, Object>>) buildReportData.get("#result-set-12"),
					(List<Map<String, Object>>) buildReportData.get("#result-set-13"));
			model.addAttribute("modulesList", moduleDetailsByBuildId);
			List<Map<String, Object>> scenarioDetailsByBuildId = scenarioDao.selectScenarioDetailsByBuildId(
					(List<Map<String, Object>>) buildReportData.get("#result-set-3"),
					(List<Map<String, Object>>) buildReportData.get("#result-set-14"),
					(List<Map<String, Object>>) buildReportData.get("#result-set-15"));
			model.addAttribute("scenarioList", scenarioDetailsByBuildId);

			List<Map<String, Object>> testcaseDetailsByBuildId = (List<Map<String, Object>>) buildReportData
					.get("#result-set-4");
			model.addAttribute("testcasesList", testcaseDetailsByBuildId);

			List<Map<String, Object>> executeBugs = (List<Map<String, Object>>) buildReportData.get("#result-set-5");

			List<Map<String, Object>> testcaseStepsList = (List<Map<String, Object>>) buildReportData
					.get("#result-set-6");

			model.addAttribute("testcaseStepsList", utilsService.convertListOfMapToJson(testcaseStepsList));
			model.addAttribute("step_bug", executeBugs);

			List<Map<String, Object>> statusList = (List<Map<String, Object>>) buildReportData.get("#result-set-7");
			model.addAttribute("statusList", statusList);

			List<Map<String, Object>> tcExecDetailsList = (List<Map<String, Object>>) buildReportData
					.get("#result-set-8");

			model.addAttribute("tcExecDetailsList", utilsService.convertListOfMapToJson(tcExecDetailsList));
			model.addAttribute("tcExecDetailsList1", tcExecDetailsList);

			List<Map<String, Object>> tcStepStatus = (List<Map<String, Object>>) buildReportData.get("#result-set-9");
			model.addAttribute("tcStepStatus", utilsService.convertListOfMapToJson(tcStepStatus));

			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			model.addAttribute("selectedExecuteBuildName", session.getAttribute("selectedExecuteBuildName").toString());
			model.addAttribute("Pass", colorProperties.getProperty("testCase.Pass"));
			model.addAttribute("Fail", colorProperties.getProperty("testCase.Fail"));
			model.addAttribute("Skip", colorProperties.getProperty("testCase.Skip"));
			model.addAttribute("Block", colorProperties.getProperty("testCase.blocked"));
			model.addAttribute("NotRun", colorProperties.getProperty("testCase.NotRun"));

			// get the attachments
			List<Map<String, Object>> attachmentsList = (List<Map<String, Object>>) buildReportData
					.get("#result-set-16");
			model.addAttribute("attachmentsList", attachmentsList);

			List<Map<String, Object>> testerDeatilsBybuildId = (List<Map<String, Object>>) buildReportData
					.get("#result-set-17");
			model.addAttribute("testerDeatils", testerDeatilsBybuildId);
			model.addAttribute("testerDeatils1", utilsService.convertListOfMapToJson(testerDeatilsBybuildId));

			/*
			 * List<Map<String, Object>> BuildtestCase = (List<Map<String,
			 * Object>>) buildReportData.get("#result-set-17");
			 * model.addAttribute("BuildtestCase", BuildtestCase);
			 */

			logger.info(" User is redirected to Build Report page ");
			return new ModelAndView("viewreport", "Model", model);

		}
	}

	@RequestMapping(value = "/testcasebuild")
	public ModelAndView BuildTest(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Build Report page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			int buildId = Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userId + " Customer Schema " + schema);
			String filterText = session.getAttribute("filterTextRS").toString();
			Map<String, Object> buildReportData = buildDAO.getBuildReportDataByBuildId(buildId, userId, schema);

			List<Map<String, Object>> testcaseDetailsByBuildId = (List<Map<String, Object>>) buildReportData
					.get("#result-set-4");
			model.addAttribute("testcasesList", testcaseDetailsByBuildId);

			List<Map<String, Object>> testcaseStepsList = (List<Map<String, Object>>) buildReportData
					.get("#result-set-6");
			model.addAttribute("testcaseStepsList", testcaseStepsList);

			List<Map<String, Object>> BuildtestCase = (List<Map<String, Object>>) buildReportData.get("#result-set-17");
			model.addAttribute("BuildtestCase", BuildtestCase);
			model.addAttribute("filterText", filterText);

			logger.info(" User is redirected to Build Report page ");
			return new ModelAndView("testcasebuild", "Model", model);

		}
	}

	@RequestMapping(value = "/testCasedata")
	public ModelAndView TestCaseInfo(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Build Report page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			int buildId = Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userId + " Customer Schema " + schema);
			String filterText = session.getAttribute("filterTextRS").toString();

			Map<String, Object> buildReportData = buildDAO.getBuildReportDataByBuildId(buildId, userId, schema);

			List<Map<String, Object>> testcaseDetailsByBuildId = (List<Map<String, Object>>) buildReportData
					.get("#result-set-4");
			model.addAttribute("testcasesList", testcaseDetailsByBuildId);

			List<Map<String, Object>> testcaseStepsList = (List<Map<String, Object>>) buildReportData
					.get("#result-set-6");
			model.addAttribute("testcaseStepsList", testcaseStepsList);

			List<Map<String, Object>> statusList = (List<Map<String, Object>>) buildReportData.get("#result-set-7");
			model.addAttribute("statusList", statusList);

			List<Map<String, Object>> executeBugs = (List<Map<String, Object>>) buildReportData.get("#result-set-5");
			model.addAttribute("step_bug", executeBugs);

			List<Map<String, Object>> attachmentsList = (List<Map<String, Object>>) buildReportData
					.get("#result-set-16");
			model.addAttribute("attachmentsList", attachmentsList);

			List<Map<String, Object>> BuildtestCase = (List<Map<String, Object>>) buildReportData.get("#result-set-17");
			model.addAttribute("BuildtestCase", BuildtestCase);

			model.addAttribute("filterText", filterText);

			logger.info(" User is redirected to Build Report page ");
			return new ModelAndView("testCasedata", "Model", model);

		}
	}

	@RequestMapping(value = "/getbuildscreenshot", method = RequestMethod.POST)
	public @ResponseBody String getbuildscreenshot(@RequestParam("attchmentId") String attchmentId, HttpSession session)
			throws Exception {
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		List<Map<String, Object>> commentAttachment = executeBuildDAO.getScreenshotById(Integer.parseInt(attchmentId),
				schema);
        if(session.getAttribute("automationExecBuildType")!=null  && session.getAttribute("automationExecBuildType").toString().equals("5")){
    		byte[] bytes=(byte[]) commentAttachment.get(0).get("screenshot");
    		String s = new String(bytes);
    		//System.out.println(s);
        	return s; 
        }
		return utilsService.getFileData((byte[]) commentAttachment.get(0).get("screenshot")); 
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		return new ModelAndView("redirect:/500");
	}

	@RequestMapping(value = "/setAutomationBuildId", method = RequestMethod.POST)
	public ModelAndView setAutomationBuildId(Model model, HttpSession session, @RequestParam("buildId") String buildID)
			throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("automationBuildId", buildID);
		return stepWiseDetails(model, session);
	}

	/**
	 * @author prafulla.pol
	 * @method tcWiseReport GET Request
	 * @param Model
	 *            model,HttpSession session
	 * @return Model (projectList,moduleList,statusList,priorityList,queryData)
	 *         & View (tcbuganalysis)
	 * @description The tcWiseReport method is used for getting 'Test case
	 *              analysis' which is based on the filters on Projects and
	 *              Modules.
	 */
	@RequestMapping(value = "/tcbuganalysis")
	public ModelAndView tcWiseReport(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Test Case Analysis page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);

			// Get the user assigned projects and module details with active
			// priorities and
			// status
			Map<String, Object> data = reportService.getTCBugAnalysisData(userID, schema);
			List<Map<String, Object>> projectList = (List<Map<String, Object>>) data.get("#result-set-1");
			model.addAttribute("projectList", projectList);
			List<Map<String, Object>> moduleList = (List<Map<String, Object>>) data.get("#result-set-2");
			model.addAttribute("moduleList", moduleList);
			List<Map<String, Object>> statusList = (List<Map<String, Object>>) data.get("#result-set-3");
			model.addAttribute("statusList", statusList);
			List<Map<String, Object>> priorityList = (List<Map<String, Object>>) data.get("#result-set-4");
			model.addAttribute("priorityList", priorityList);
			List<Map<String, Object>> severityList = (List<Map<String, Object>>) data.get("#result-set-5");
			model.addAttribute("severityList", severityList);

			// get the customized query data
			List<Map<String, Object>> queryData = (List<Map<String, Object>>) data.get("#result-set-6");
			model.addAttribute("queryData", queryData);

			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			logger.info(" User is redirected to Test Case Analysis page ");
			return new ModelAndView("tcbuganalysis", "Model", model);
		}
	}

	/**
	 * @author prafulla.pol
	 * @method tcBugReport a ajax POST Request
	 * @param model
	 *            is Model type
	 * @param session
	 *            is HttpSession type
	 * @param proState
	 *            it is project status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param project
	 *            it is list of project ID's selected by the user
	 * @param modState
	 *            it is module status for dynamic query whether 'is' or 'is not'
	 * @param module
	 *            it is list of module ID's selected by the user
	 * @param statusState
	 *            it is status's status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param status
	 *            it is list of status ID's selected by the user
	 * @param priorityState
	 *            it is priority status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param priority
	 *            it is list of priority ID's selected by the user
	 * @return filtered data as per user's request in form of List<Map<String,
	 *         Object>>
	 * @throws Exception
	 * @description The tcBugReport method accept user filters from 'Test Case
	 *              Analysis' page and returns the filtered data.
	 */

	@RequestMapping(value = "/tcbugreport", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody List<Map<String, Object>> tcBugReport(Model model, HttpSession session,
			@RequestParam(value = "projectState", defaultValue = "0") String proState,
			@RequestParam(value = "project", defaultValue = "0") String project,
			@RequestParam(value = "moduleState", defaultValue = "0") String modState,
			@RequestParam(value = "module", defaultValue = "0") String module,
			@RequestParam(value = "statusState", defaultValue = "0") String statusState,
			@RequestParam(value = "status", defaultValue = "0") String status,
			@RequestParam(value = "priorityState", defaultValue = "0") String priorityState,
			@RequestParam(value = "priority", defaultValue = "0") String priority,
			@RequestParam(value = "severityState", defaultValue = "0") String severityState,
			@RequestParam(value = "severity", defaultValue = "0") String severity) throws Exception {
		logger.info("Redirecting to Ajax post request for getting the filtered data for Test Case Analysis");

		utilsService.getLoggerUser(session);
		session.setAttribute("menuLiText", "Real-Time Analytics");
		String schema = session.getAttribute("cust_schema").toString();
		int userID = Integer.parseInt(session.getAttribute("userId").toString());
		logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
		List<Map<String, Object>> filteredData = trialExecutionService.tcBugReport(proState, modState, statusState,
				priorityState, severityState, project, module, status, priority, severity, userID, schema,
				reportService);
		logger.info(" Returning the data to ajax request for Test Case Analysis");
		return filteredData;
	}

	/**
	 * 
	 * @author prafulla.pol
	 * @method saveTcQuery a POST Request
	 * @param model
	 *            is Model type
	 * @param session
	 *            is HttpSession type
	 * @param projectState
	 *            it is project status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param project
	 *            it is list of project ID's selected by the user
	 * @param moduleState
	 *            it is module status for dynamic query whether 'is' or 'is not'
	 * @param module
	 *            it is list of module ID's selected by the user
	 * @param statusState
	 *            it is status's status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param status
	 *            it is list of status ID's selected by the user
	 * @param priorityState
	 *            it is priority status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param priority
	 *            it is list of priority ID's selected by the user
	 * @param queryName
	 *            it is the name of the query to be saved
	 * @return Model () & View (onSuccess - tcbuganalysis, onFailure -
	 *         logout/500)
	 * @throws Exception
	 * @description The saveTcQuery method accept user filters from 'Test Case
	 *              Analysis' page and converts these filters into dynamic query
	 *              and saves the query.
	 */
	@RequestMapping(value = "/savetcquery", method = RequestMethod.POST)
	public ModelAndView saveTcQuery(Model model, HttpSession session,
			@RequestParam(value = "projectState", defaultValue = "0") String projectState,
			@RequestParam(value = "project", defaultValue = "0") String project,
			@RequestParam(value = "moduleState", defaultValue = "0") String moduleState,
			@RequestParam(value = "module", defaultValue = "0") String module,
			@RequestParam(value = "statusState", defaultValue = "0") String statusState,
			@RequestParam(value = "status", defaultValue = "0") String status,
			@RequestParam(value = "priorityState", defaultValue = "0") String priorityState,
			@RequestParam(value = "priority", defaultValue = "0") String priority,
			@RequestParam(value = "severityState", defaultValue = "0") String severityState,
			@RequestParam(value = "severity", defaultValue = "0") String severity,
			@RequestParam(value = "queryName", required = false) String queryName) throws Exception {
		logger.info("Post method to save the user query for Test Case Analysis");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);

			int flag = trialExecutionService.saveTcQuery(projectState, moduleState, statusState, priorityState,
					severityState, project, module, status, priority, severity, reportService, userID, schema,
					queryName);
			if (flag != 0) {
				logger.info("User is redirected to Test Case Analysis page ");
				return new ModelAndView("redirect:/tcbuganalysis");
			} else {
				logger.warn("Something went wrong,user is redirected to error page");
				return new ModelAndView("redirect:/500");
			}
		}
	}

	/**
	 * 
	 * @author prafulla.pol
	 * @method updateTcQuery a POST Request
	 * @param model
	 *            is Model type
	 * @param session
	 *            is HttpSession type
	 * @param projectState
	 *            it is project status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param project
	 *            it is list of project ID's selected by the user
	 * @param moduleState
	 *            it is module status for dynamic query whether 'is' or 'is not'
	 * @param module
	 *            it is list of module ID's selected by the user
	 * @param statusState
	 *            it is status's status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param status
	 *            it is list of status ID's selected by the user
	 * @param priorityState
	 *            it is priority status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param priority
	 *            it is list of priority ID's selected by the user
	 * @param queryName
	 *            it is the name of the query to be saved
	 * @param queryId
	 *            it is the id of query which is to be updated
	 * @return Model () & View (onSuccess - tcbuganalysis, onFailure -
	 *         logout/500)
	 * @throws Exception
	 * @description The updateTcQuery method accept user filters from 'Test Case
	 *              Analysis' page and converts these filters into dynamic query
	 *              and updates the query with respected query id.
	 */
	@RequestMapping(value = "/updatetcquery", method = RequestMethod.POST)
	public ModelAndView updateTcQuery(Model model, HttpSession session,
			@RequestParam(value = "projectState", defaultValue = "0") String projectState,
			@RequestParam(value = "project", defaultValue = "0") String project,
			@RequestParam(value = "moduleState", defaultValue = "0") String moduleState,
			@RequestParam(value = "module", defaultValue = "0") String module,
			@RequestParam(value = "statusState", defaultValue = "0") String statusState,
			@RequestParam(value = "status", defaultValue = "0") String status,
			@RequestParam(value = "priorityState", defaultValue = "0") String priorityState,
			@RequestParam(value = "priority", defaultValue = "0") String priority,
			@RequestParam(value = "severityState", defaultValue = "0") String severityState,
			@RequestParam(value = "severity", defaultValue = "0") String severity,
			@RequestParam(value = "save", defaultValue = "0") String save,
			@RequestParam(value = "execute", defaultValue = "0") String execute,
			@RequestParam(value = "queryName", required = false) String queryName,
			@RequestParam("queryID") String queryId) throws Exception {
		logger.info("Post method to update the user query for Test Case Analysis");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);

			int flag = trialExecutionService.updateTcQuery(queryId, projectState, moduleState, statusState,
					priorityState, severityState, project, module, status, priority, severity, reportService, userID,
					schema);
			if (flag != 0) {
				logger.info("User is redirected to Test Case Analysis page ");
				return new ModelAndView("redirect:/tcbuganalysis");
			} else {
				logger.warn("Something went wrong,user is redirected to error page");
				return new ModelAndView("redirect:/500");
			}
		}
	}

	/**
	 * 
	 * @param Model
	 *            model
	 * @param HttpSession
	 *            session
	 * @return Model (releaseList,buildList,statusList,priorityList,queryData) &
	 *         View (bugAnalysis)
	 * @throws Exception
	 * @description The bugAnalysis method is used for getting 'Bug Analysis'
	 *              which is based on the filters on Projects and Modules.
	 */
	@RequestMapping(value = "/buganalysis")
	public ModelAndView bugAnalysis(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Bug Analysis page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);

			Map<String, Object> data = reportService.getBugAnalysisData(userID, schema);
			List<Map<String, Object>> releaseList = (List<Map<String, Object>>) data.get("#result-set-1");
			model.addAttribute("releaseList", releaseList);
			List<Map<String, Object>> buildList = (List<Map<String, Object>>) data.get("#result-set-2");
			model.addAttribute("buildList", buildList);
			List<Map<String, Object>> statusList = (List<Map<String, Object>>) data.get("#result-set-3");
			model.addAttribute("statusList", statusList);
			List<Map<String, Object>> priorityList = (List<Map<String, Object>>) data.get("#result-set-4");
			model.addAttribute("priorityList", priorityList);
			List<Map<String, Object>> severityList = (List<Map<String, Object>>) data.get("#result-set-5");
			model.addAttribute("severityList", severityList);

			List<Map<String, Object>> getQueryforBugTracker = reportService.getFilteredvaluesforBugTracker(userID,
					schema);
			model.addAttribute("getQueryforBugTracker", getQueryforBugTracker);

			// get the customized query data
			List<Map<String, Object>> queryData = (List<Map<String, Object>>) data.get("#result-set-6");
			model.addAttribute("queryData", queryData);
			List<Map<String, Object>> buildExeType = (List<Map<String, Object>>) data.get("#result-set-7");
			model.addAttribute("buildExeType", buildExeType);
			List<Map<String, Object>> autoBuildList = (List<Map<String, Object>>) data.get("#result-set-8");
			model.addAttribute("autoBuildList", autoBuildList);

			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			int buildId = 0, releaseId = 0;
			String buildName = "";
			int buildStatus = 0;
			if (session.getAttribute("automationExecBuildId") == null) {
				List<Map<String, Object>> lastAutoBuildDetails = buildDAO.getlastAutoBuildDetails(userID, schema);
				for (int i = 0; i < lastAutoBuildDetails.size(); i++) {
					buildId = (Integer) lastAutoBuildDetails.get(i).get("build_id");
					buildName = (String) lastAutoBuildDetails.get(i).get("build_name");
					buildStatus = (Integer) lastAutoBuildDetails.get(i).get("build_status");
					releaseId = (Integer) lastAutoBuildDetails.get(i).get("release_id");
				}
				session.setAttribute("automationExecBuildId", buildId);
				session.setAttribute("automationExecReleaseId", releaseId);
				session.setAttribute("selectedAutoReportBuildId", buildId);
				session.setAttribute("BuildId", buildId);
				session.setAttribute("automationExecBuildName", buildName);
				session.setAttribute("UsertestRemoveStatus", buildStatus);
				session.setAttribute("selectedExecuteBuildName", buildName);
			}
			model.addAttribute("BugColorNew", colorProperties.getProperty("bug.New"));
			model.addAttribute("BugColorFixed", colorProperties.getProperty("bug.Fixed"));
			model.addAttribute("BugColorClosed", colorProperties.getProperty("bug.Closed"));
			model.addAttribute("BugColorAssigned", colorProperties.getProperty("bug.Assigned"));
			model.addAttribute("BugColorInProgress", colorProperties.getProperty("bug.InProgress"));
			model.addAttribute("BugColorResolved", colorProperties.getProperty("bug.Resolved"));
			model.addAttribute("BugColorReopened", colorProperties.getProperty("bug.Reopened"));

			logger.info(" User is redirected to Bug Analysis page ");
			return new ModelAndView("buganalysis", "Model", model);
		}
	}

	/**
	 * @author prafulla.pol
	 * @method saveBugQuery a POST Request
	 * @param Model
	 *            model
	 * @param HttpSession
	 *            session
	 * @param releaseState
	 *            it is release status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param release
	 *            it is list of release ID's selected by the user
	 * @param buildState
	 *            it is build status for dynamic query whether 'is' or 'is not'
	 * @param build
	 *            it is list of build ID's selected by the user
	 * @param statusState
	 *            it is status's status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param status
	 *            it is list of status ID's selected by the user
	 * @param priorityState
	 *            it is priority status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param priority
	 *            it is list of priority ID's selected by the user
	 * @param queryName
	 *            it is the name of the query to be saved
	 * @return Model () & View (onSuccess - buganalysis, onFailure - logout/500)
	 * @throws Exception
	 * @description The saveBugQuery method accept user filters from 'Bug
	 *              Analysis' page and converts these filters into dynamic query
	 *              and saves the query.
	 */
	@RequestMapping(value = "/savebugquery", method = RequestMethod.POST)
	public ModelAndView saveBugQuery(Model model, HttpSession session,
			@RequestParam(value = "releaseState", defaultValue = "0") String releaseState,
			@RequestParam(value = "release", defaultValue = "0") String release,
			@RequestParam(value = "buildState", defaultValue = "0") String buildState,
			@RequestParam(value = "build", defaultValue = "0") String build,
			@RequestParam(value = "statusState", defaultValue = "0") String statusState,
			@RequestParam(value = "status", defaultValue = "0") String status,
			@RequestParam(value = "priorityState", defaultValue = "0") String priorityState,
			@RequestParam(value = "priority", defaultValue = "0") String priority,
			@RequestParam(value = "severityState", defaultValue = "0") String severityState,
			@RequestParam(value = "severity", defaultValue = "0") String severity,
			@RequestParam(value = "queryName", required = false) String queryName,
			@RequestParam(value = "buildTypeStatus", defaultValue = "0") String buildTypeState,
			@RequestParam(value = "buildTypeValues", defaultValue = "0") String buildType) throws Exception {
		logger.info("Post method to save the user query for Bug Analysis");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			// saving the query
			int automationAccess = Integer.parseInt(session.getAttribute("automationStatus").toString());
			int flag = trialExecutionService.saveBugQuery(releaseState, buildState, statusState, priorityState,
					severityState, buildTypeState, release, build, status, priority, severity, buildType,
					automationAccess, schema, userID, reportService, queryName);
			if (flag != 0) {
				logger.info("User is redirected to Bug Analysis page ");
				return new ModelAndView("redirect:/buganalysis");
			} else {
				logger.warn("Something went wrong,user is redirected to error page");
				return new ModelAndView("redirect:/500");
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method saveBugQuery a POST Request
	 * @param Model
	 *            model
	 * @param HttpSession
	 *            session
	 * @param releaseState
	 *            it is release status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param release
	 *            it is list of release ID's selected by the user
	 * @param buildState
	 *            it is build status for dynamic query whether 'is' or 'is not'
	 * @param build
	 *            it is list of build ID's selected by the user
	 * @param statusState
	 *            it is status's status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param status
	 *            it is list of status ID's selected by the user
	 * @param priorityState
	 *            it is priority status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param priority
	 *            it is list of priority ID's selected by the user
	 * @param queryName
	 *            it is the name of the query to be saved
	 * @param queryId
	 *            it is the id of the query which is to be updated
	 * @return Model () & View (onSuccess - buganalysis, onFailure - logout/500)
	 * @throws Exception
	 * @description The updateBugQuery method accept user filters from 'Bug
	 *              Analysis' page and converts these filters into dynamic query
	 *              and updates the query with respected query id.
	 */
	@RequestMapping(value = "/updatebugquery", method = RequestMethod.POST)
	public ModelAndView updateBugQuery(Model model, HttpSession session,
			@RequestParam(value = "releaseState", defaultValue = "0") String releaseState,
			@RequestParam(value = "release", defaultValue = "0") String release,
			@RequestParam(value = "buildState", defaultValue = "0") String buildState,
			@RequestParam(value = "build", defaultValue = "0") String build,
			@RequestParam(value = "statusState", defaultValue = "0") String statusState,
			@RequestParam(value = "status", defaultValue = "0") String status,
			@RequestParam(value = "priorityState", defaultValue = "0") String priorityState,
			@RequestParam(value = "priority", defaultValue = "0") String priority,
			@RequestParam(value = "severityState", defaultValue = "0") String severityState,
			@RequestParam(value = "severity", defaultValue = "0") String severity,
			@RequestParam(value = "save", defaultValue = "0") String save,
			@RequestParam(value = "execute", defaultValue = "0") String execute,
			@RequestParam(value = "queryName", required = false) String queryName,
			@RequestParam("queryID") String queryId,
			@RequestParam(value = "buildTypeStatus", defaultValue = "0") String buildTypeState,
			@RequestParam(value = "buildTypeValues", defaultValue = "0") String buildType) throws Exception {
		logger.info("Post method to update the user query for Bug Analysis");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			int automationAccess = Integer.parseInt(session.getAttribute("automationStatus").toString());
			int flag = trialExecutionService.updateBugQuery(queryId, releaseState, buildState, statusState,
					priorityState, severityState, buildTypeState, release, build, status, priority, severity, buildType,
					reportService, automationAccess, userID, schema);
			if (flag != 0) {
				logger.info("User is redirected to Bug Analysis page ");
				return new ModelAndView("redirect:/buganalysis");
			} else {
				logger.warn("Something went wrong,user is redirected to error page");
				return new ModelAndView("redirect:/500");
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method bugReport a ajax POST Request
	 * @param Model
	 *            model
	 * @param HttpSession
	 *            session
	 * @param releaseState
	 *            it is release status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param release
	 * @param buildState
	 *            it is build status for dynamic query whether 'is' or 'is not'
	 * @param build
	 * @param statusState
	 *            it is status's status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param status
	 * @param priorityState
	 *            it is priority status for dynamic query whether 'is' or 'is
	 *            not'
	 * @param priority
	 * @return filtered data as per user's request in form of List<Map<String,
	 *         Object>>
	 * @throws Exception
	 * @description The bugReport method accept user filters from 'bug Analysis'
	 *              page and returns the filtered data.
	 */
	@RequestMapping(value = "/bugreport", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody List<Map<String, Object>> bugReport(Model model, HttpSession session,
			@RequestParam(value = "releaseState", defaultValue = "0") String releaseState,
			@RequestParam(value = "release", defaultValue = "0") String release,
			@RequestParam(value = "buildState", defaultValue = "0") String buildState,
			@RequestParam(value = "build", defaultValue = "0") String build,
			@RequestParam(value = "statusState", defaultValue = "0") String statusState,
			@RequestParam(value = "status", defaultValue = "0") String status,
			@RequestParam(value = "priorityState", defaultValue = "0") String priorityState,
			@RequestParam(value = "priority", defaultValue = "0") String priority,
			@RequestParam(value = "severityState", defaultValue = "0") String severityState,
			@RequestParam(value = "severity", defaultValue = "0") String severity,
			@RequestParam(value = "buildTypeState", defaultValue = "0") String buildTypeState,
			@RequestParam(value = "buildType", defaultValue = "0") String buildType) throws Exception {
		logger.info("Redirecting to Ajax post request for getting the filtered data for Bug Analysis");
		utilsService.getLoggerUser(session);
		session.setAttribute("menuLiText", "Real-Time Analytics");
		String schema = session.getAttribute("cust_schema").toString();
		int userID = Integer.parseInt(session.getAttribute("userId").toString());
		logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
		int automationAccess = Integer.parseInt(session.getAttribute("automationStatus").toString());
		List<Map<String, Object>> returnList = trialExecutionService.bugReport(releaseState, buildState, statusState,
				priorityState, severityState, buildTypeState, release, build, status, priority, severity,
				automationAccess, buildType, reportService, userID, schema);
		return returnList;
	}

	@RequestMapping(value = "/insertFilterQueryForBugTracker")
	public @ResponseBody ModelAndView FilteringforBugTracker(@RequestParam String inputValue,
			@RequestParam String commonData, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			reportService.saveFilterValues(userId, inputValue, commonData, schema);
			return new ModelAndView("redirect:/buganalysis");
		}

	}

    @RequestMapping(value = "/insertFilterQueryForTEAutomation")
    public @ResponseBody ModelAndView insertFilterQueryForBugTracker1(@RequestParam String inputValue,
            @RequestParam String commonData, HttpSession session) throws Exception {
        if (session.getAttribute("userId") == null) {
            logger.error("Session has expired,user is redirected to login page");
            return new ModelAndView("redirect:/");
        } else {
            String schema = session.getAttribute("cust_schema").toString();
            int userId = Integer.parseInt(session.getAttribute("userId").toString());
            reportService.saveFilterValues2(userId, inputValue, commonData, schema);
            return new ModelAndView("redirect:/buildreportautomation");
            
        }
        


    }
    
    @RequestMapping(value = "/insertFilterQueryForTEManual")
    public @ResponseBody ModelAndView insertFilterQueryForBugTracker(@RequestParam String inputValue,
            @RequestParam String commonData, HttpSession session) throws Exception {
        if (session.getAttribute("userId") == null) {
            logger.error("Session has expired,user is redirected to login page");
            return new ModelAndView("redirect:/");
        } else {
            String schema = session.getAttribute("cust_schema").toString();
            int userId = Integer.parseInt(session.getAttribute("userId").toString());
            reportService.saveFilterValues1(userId, inputValue, commonData, schema);
            return new ModelAndView("redirect:/buildreportmanual");
        }
        


    }

	@RequestMapping(value = "/automationsummary")
	public ModelAndView automationSummary(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Automation Summary page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			// int buildId =
			// Integer.parseInt(session.getAttribute("selectedAutoReportBuildId").toString());

			int buildId = 0, releaseId = 0, buildState = 0;
			String buildName = "";
			int buildStatus = 0;
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			
			Long relCount = 0L;
			if (session.getAttribute("automationExecBuildId") == null) {
				List<Map<String, Object>> lastAutoBuildDetails = buildDAO.getlastAutoBuildDetails(userId, schema);
				for (int i = 0; i < lastAutoBuildDetails.size(); i++) {
					buildId = (Integer) lastAutoBuildDetails.get(i).get("build_id");
					buildName = (String) lastAutoBuildDetails.get(i).get("build_name");
					buildStatus = (Integer) lastAutoBuildDetails.get(i).get("build_status");
					releaseId = (Integer) lastAutoBuildDetails.get(i).get("release_id");
				}
				session.setAttribute("automationExecBuildId", buildId);
				session.setAttribute("automationExecReleaseId", releaseId);
				session.setAttribute("selectedAutoReportBuildId", buildId);
				session.setAttribute("BuildId", buildId);
				session.setAttribute("automationExecBuildName", buildName);
				session.setAttribute("UsertestRemoveStatus", buildStatus);
			} else {
				buildId = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());
			}

			model.addAttribute("Pass", colorProperties.getProperty("testCase.Pass"));
			model.addAttribute("Fail", colorProperties.getProperty("testCase.Fail"));
			model.addAttribute("Skip", colorProperties.getProperty("testCase.Skip"));
			model.addAttribute("Block", colorProperties.getProperty("testCase.blocked"));
			model.addAttribute("NotRun", colorProperties.getProperty("testCase.NotRun"));
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			List<Map<String, Object>> autoBuildState = buildDAO.getAutoBuildState(buildId, schema);
			// System.out.println("autoBuildState:"+autoBuildState);
			for (int i = 0; i < autoBuildState.size(); i++) {
				buildState = (Integer) autoBuildState.get(i).get("build_state");
			}
			session.setAttribute("autoBuildState", buildState);
			model.addAttribute("state", autoBuildState);

			List<Map<String, Object>> autoBuilds = reportService.getAtomationBuilds(schema);
			model.addAttribute("automationBuilds", autoBuilds);

			Map<String, Object> data = reportService.getAutomationSummaryData(buildId, userID, schema);
			// System.out.println("autosummery map data:"+data);
			// System.out.println("buildId:"+buildId+"
			// UserID:"+userID+"schema:"+schema);
			List<Map<String, Object>> projectWiseCount = (List<Map<String, Object>>) data.get("#result-set-1");
			model.addAttribute("projectWiseCount", projectWiseCount);
			List<Map<String, Object>> projectList = (List<Map<String, Object>>) data.get("#result-set-2");
			model.addAttribute("projectList", projectList);
			// System.out.println("pro List A :"+projectList);
			List<Map<String, Object>> moduleWiseCount = (List<Map<String, Object>>) data.get("#result-set-3");
			model.addAttribute("moduleWiseCount", moduleWiseCount);
			List<Map<String, Object>> moduleList = (List<Map<String, Object>>) data.get("#result-set-4");
			model.addAttribute("moduleList", moduleList);
			// System.out.println("module list A :"+moduleList);

			List<Map<String, Object>> scenarioWiseCount = (List<Map<String, Object>>) data.get("#result-set-5");
			model.addAttribute("scenarioWiseCount", scenarioWiseCount);
			List<Map<String, Object>> scenarioList = (List<Map<String, Object>>) data.get("#result-set-6");
			model.addAttribute("scenarioList", scenarioList);
			// System.out.println("scenario list A:"+scenarioList);

			List<Map<String, Object>> testCaseList = (List<Map<String, Object>>) data.get("#result-set-7");

			for (int cnt = 0; cnt < testCaseList.size(); cnt++) {
				testCaseList.get(cnt).put("tcStartTime",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(testCaseList.get(cnt).get("tc_start_time").toString())));
				testCaseList.get(cnt).put("tcEndTime",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(testCaseList.get(cnt).get("tc_end_time").toString())));
			}

			model.addAttribute("testCaseList", testCaseList);

			List<Map<String, Object>> testStepsList = (List<Map<String, Object>>) data.get("#result-set-8");
			model.addAttribute("testStepsList", testStepsList);

			List<Map<String, Object>> testStepsBugList = (List<Map<String, Object>>) data.get("#result-set-9");
			model.addAttribute("testStepsBugList", testStepsBugList);

			logger.info(" User is redirected to Automation Summary page ");
			return new ModelAndView("automationsummary", "Model", model);
		}
	}

	@RequestMapping(value = "/buildreportautomation")
	public ModelAndView buildReportAutomation(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Automation Summary page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			// int buildId =
			// Integer.parseInt(session.getAttribute("selectedAutoReportBuildId").toString());

			int buildId = 0, releaseId = 0, buildState = 0;
			String buildName = "";
			int buildStatus = 0;
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			
		    int projectID = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());

			Long relCount = 0L;
			if (session.getAttribute("automationExecBuildId") == null) {
				List<Map<String, Object>> lastAutoBuildDetails = buildDAO.getlastAutoBuildDetails(userId, schema);
				for (int i = 0; i < lastAutoBuildDetails.size(); i++) {
					buildId = (Integer) lastAutoBuildDetails.get(i).get("build_id");
					buildName = (String) lastAutoBuildDetails.get(i).get("build_name");
					buildStatus = (Integer) lastAutoBuildDetails.get(i).get("build_status");
					releaseId = (Integer) lastAutoBuildDetails.get(i).get("release_id");
				}
				session.setAttribute("automationExecBuildId", buildId);
				session.setAttribute("automationExecReleaseId", releaseId);
				session.setAttribute("selectedAutoReportBuildId", buildId);
				session.setAttribute("BuildId", buildId);
				session.setAttribute("automationExecBuildName", buildName);
				session.setAttribute("UsertestRemoveStatus", buildStatus);
			} else {
				buildId = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());
			}

			model.addAttribute("Pass", colorProperties.getProperty("testCase.Pass"));
			model.addAttribute("Fail", colorProperties.getProperty("testCase.Fail"));
			model.addAttribute("Skip", colorProperties.getProperty("testCase.Skip"));
			model.addAttribute("Block", colorProperties.getProperty("testCase.blocked"));
			model.addAttribute("NotRun", colorProperties.getProperty("testCase.NotRun"));
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			List<Map<String, Object>> autoBuildState = buildDAO.getAutoBuildState(buildId, schema);
			// System.out.println("autoBuildState:"+autoBuildState);
			for (int i = 0; i < autoBuildState.size(); i++) {
				buildState = (Integer) autoBuildState.get(i).get("build_state");
			}
			session.setAttribute("autoBuildState", buildState);
			model.addAttribute("state", autoBuildState);

			List<Map<String, Object>> autoBuilds = reportService.getAtomationBuilds(schema);
			model.addAttribute("automationBuilds", autoBuilds);

			Map<String, Object> buildReportFilteringData = reportService.getBuildReportAutomationFilteringData(schema);
			model.addAttribute("releasesJSON", utilsService
					.convertListOfMapToJson((List<Map<String, Object>>) buildReportFilteringData.get("#result-set-1")));
			model.addAttribute("testSetsJSON", utilsService
					.convertListOfMapToJson((List<Map<String, Object>>) buildReportFilteringData.get("#result-set-2")));
			model.addAttribute("modulesJSON", utilsService
					.convertListOfMapToJson((List<Map<String, Object>>) buildReportFilteringData.get("#result-set-3")));
			model.addAttribute("scenariosJSON", utilsService
					.convertListOfMapToJson((List<Map<String, Object>>) buildReportFilteringData.get("#result-set-4")));
			model.addAttribute("statusesJSON", utilsService
					.convertListOfMapToJson((List<Map<String, Object>>) buildReportFilteringData.get("#result-set-5")));
			model.addAttribute("xebmAutoExecSummaryJSON", utilsService
					.convertListOfMapToJson((List<Map<String, Object>>) buildReportFilteringData.get("#result-set-6")));
			List<Map<String, Object>> ExecutionAytomationQueryFilter = reportService.ExecutionAytomationQueryFilter(userId,projectID,
                    schema);
			
			
			 List<Map<String, Object>> SummaryExecutionAutomationQueryFilter = reportService.SummaryExecutionAutomationQueryFilter(userId,projectID ,
	                    schema);
	            model.addAttribute("SummaryExecutionAutomationQueryFilter", SummaryExecutionAutomationQueryFilter);
	            
	           // System.out.println(SummaryExecutionAutomationQueryFilter);
			
			
            model.addAttribute("ExecutionAytomationQueryFilter", ExecutionAytomationQueryFilter);
           
            model.addAttribute("ExecutionAytomationQueryFilter1", utilsService.convertListOfMapToJson(ExecutionAytomationQueryFilter));
           
            //System.out.println("ExecutionManualQueryFilter"+ExecutionManualQueryFilter);
           
            List<Map<String, Object>> getQueryforTEATracker = reportService.getQueryforTEATracker(userId,
                    schema);
            model.addAttribute("getQueryforTEATracker", getQueryforTEATracker);
			logger.info(" User is redirected to Automation Summary page ");
			return new ModelAndView("automationsummary", "Model", model);
		}
	}

	@RequestMapping(value = "/filterBuildReportAutomation")
	public @ResponseBody List<Map<String, Object>> filterBuildReportAutomation(
			@RequestParam MultiValueMap<String, String> paramMap, HttpServletRequest request, Model model,
			HttpSession session) throws Exception {
		logger.info("Redirecting to Build Report page and checking for active session");
		List<Map<String, Object>> maps = new ArrayList<Map<String, Object>>();
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return maps;
		} else {
			String schema = session.getAttribute("cust_schema").toString();
			maps = reportService.buildReportAutomation(schema, paramMap);
			return maps;
		}
	}
	
	
	@RequestMapping(value = "saveDetailsForReportGeneration", method = RequestMethod.POST)
	public String saveDetailsForReportGeneration(@RequestBody GenerateReportRequest request, @RequestParam("schemaName") String schema, HttpSession session) throws Exception {
		System.out.println("started....");
		
			String roleId= "2", result= "Fail", redwoodId =null, testPrefix=null;
			int userId = 1, releaseId = 0, projectId = 0, buildId = 0, datasetId=0, buildStatus = 1, scenarioId =0, moduleId =0, testCaseId=0; 
			ArrayList<String> testPrefixDbList = null;
			ArrayList<Integer> testCaseIdList = null;
			//Insert Project Details
			if (null != request.getProjectName() && !request.getProjectName().isEmpty()) {
				String projectName = request.getProjectName();
					List<Map<String, Object>> projects = projectDao.getAllProjects(schema);
					List<String> projectNamesList = new ArrayList<>();
					if (null != projects && !projects.isEmpty()) {
						for (int i = 0; i < projects.size(); i++) {
							if (projectName.equalsIgnoreCase(projects.get(i).get("project_name").toString())) {
								projectNamesList.add(projectName);
								projectId = (int) projects.get(i).get("project_id");
								break;
							}
						}
						if (projectNamesList.isEmpty()) {
							return "Given project name doesn't exist in db";
						}
					} else {
						return "No project present in db for given schema";
					}
			}
			else {
				return "project Name is null or empty in the request";
			}
			
			//to find moduleId, scenarioId for each testcase based on projectId and testPrefix
			
			if(null != request.getTestPrefixList() && !request.getTestPrefixList().isEmpty() && projectId != 0) {
				/*
				 //to pass below as parameter to fetch testcaseDbList later if so multiple scnearios are there.
				String[] testPrefixArr = new String[request.getTestPrefixList().size()];
				testPrefixArr = request.getTestPrefixList().toArray(testPrefixArr);
				int j=0;
				StringBuilder sb1 = new StringBuilder();
				for (String i : testPrefixArr) {
					j++;
					if (testPrefixArr.length == j)
						sb1.append(i);
					else
						sb1.append(i + ",");
				}
				*/
				List<Map<String, Object>> testCaseDbList = testcaseDao.getTestcaseDetailsByProjectIdAndPrefixList(request.getTestPrefixList(), projectId, schema);
				if(null != testCaseDbList && !testCaseDbList.isEmpty()) {
					for(int i = 0; i < testCaseDbList.size(); i++) {
						 scenarioId = (int) testCaseDbList.get(i).get("scenario_id");
						 moduleId = (int) testCaseDbList.get(i).get("module_id");
						 testPrefix = testCaseDbList.get(i).get("test_prefix").toString();
						  testCaseId = (int) testCaseDbList.get(i).get("testcase_id");
					}
					
					if(null != testCaseDbList && !testCaseDbList.isEmpty()) {
						testPrefixDbList = new ArrayList<>();
						testCaseIdList = new ArrayList<>();
						for(Map<String, Object> s : testCaseDbList) {
							String dbTestPrefix = s.get("test_prefix").toString();
							int tcId = (int) s.get("testcase_id");
							testPrefixDbList.add(dbTestPrefix);
							testCaseIdList.add(tcId);
						}
					}
					
					if(testPrefixDbList.size() == request.getTestPrefixList().size()) {
						System.out.println("both in request & db the testPrefix counts are equal");
						
					}
					else {
						Collections.sort(request.getTestPrefixList());
						Collections.sort(testPrefixDbList);
						testPrefixDbList.removeAll(request.getTestPrefixList());
						System.out.println(testPrefixDbList+ "Not present in db"); //need to send in response
					}										
				}
				else {
					return "No data found for in db for given test prefixes";
				}
			}
			else {
				return "Test case list is null or empty";
			}
			
			//fetch only releaseId for build purpose.
			
			if (null != request.getReleaseName() && !request.getReleaseName().isEmpty()) {
				String releaseName = request.getReleaseName();
				List<Map<String, Object>> releaseDetailsDbList = releaseDAO.getAllReleaseDetails(schema);
				List<String> existingReleaseNames = new ArrayList<>();
				
				if(null != releaseDetailsDbList && !releaseDetailsDbList.isEmpty()) {
				
				for (int i = 0; i < releaseDetailsDbList.size(); i++) {
					if (releaseName.equalsIgnoreCase(releaseDetailsDbList.get(i).get("release_name").toString())) {
						existingReleaseNames.add(releaseName);
						releaseId = (int) releaseDetailsDbList.get(i).get("release_id");
						break;
					}
				}
				 if (existingReleaseNames.isEmpty()) {
						return "given release name doesn't exist in db";
					} 
			  }
				else {
					return "Release details are empty for given schema";
				}
				
			}
			else {
				return "release details are null or empty in request";
			}
			
			// build insertion testSet
			
			if (null != request.getReportBuildDetails() && releaseId != 0) {
				//ReportBuildDetails buildDetails = json.getJSONObject("reportBuildDetails");
				ReportBuildDetails buildDetails = request.getReportBuildDetails();
				
				List<Map<String, Object>> buildDetailsDbList = buildDAO.getAllAutoBuildsByReleaseId(releaseId, schema);
						String buildName = buildDetails.getBuildName();
						String buildDescription = buildDetails.getBuildDesc();
						String currentDate = buildDetails.getCurrentDate();
						String buildExecutionType = String.valueOf(buildDetails.getBuildExecType());
						boolean buildNameFound = false;
						if(null != buildDetailsDbList && !buildDetailsDbList.isEmpty()) {
							 buildNameFound = buildDetailsDbList.stream()
										.filter(bName -> !bName.get("build_name").toString().isEmpty())
										.anyMatch(bName -> bName.get("build_name").toString().equalsIgnoreCase(buildName));
						}
						else {
							return "There are no builds in db for given schema";
				        }
						if (buildNameFound) {
							return "given buildName in request already exists for same given release pls give another";
						} else {
							int flag = buildService.insertBuild(buildName, buildDescription, buildStatus,
									buildExecutionType, 1, Integer.parseInt(buildExecutionType), null, schema,
									releaseId, userId, currentDate, roleId, redwoodId);
							
							/*fetch the buildId for above to use for inserting data in xebm_automation_steps_execution_summary,
							xebm_automation_execution_summary, xebm_build_exec_details, xebm_automation_build_testcase */
							
							if(flag != 0) {
								if(null != testPrefixDbList && !testPrefixDbList.isEmpty() && "2".equals(buildExecutionType)) {
									buildId = buildDAO.getBuildIdForAutoBuild(buildName, schema);
									if(buildId != 0) {
									// to update xebm_automation_build_testcase for automation
									int j = 0;
									StringBuilder sb2 = new StringBuilder();
									for (int i : testCaseIdList) {
										j++;
										if (testCaseIdList.size() == j)
											sb2.append(i);
										else
											sb2.append(i + ",");
									}
									int flag1 = buildDAO.addAutomationTestCase(sb2.toString(), buildId, userId, schema);
									
									//to update xebm_automation_execution_summary
									int flag2 = buildDAO.insertcstarttime(buildId, testPrefix, schema);
									}
									else {
										return "buildId is 0 for automationBuild";
									}
								}else if(null != testPrefixDbList && !testPrefixDbList.isEmpty() && "1".equals(buildExecutionType)) {
									 buildId = buildDAO.getBuildIdForManualBuild(buildName, schema);
									 //To do, need to update even for manual builds...
								}
								else {
									return "buildExecutionType must be either 1 or 2";
								}
									
								if(null != request.getTestCaseDataList() && !request.getTestCaseDataList().isEmpty()) {
									HashMap<String, HashMap<Integer, HashMap<String,String>>> h1 = new LinkedHashMap<>();
									for(TestCaseData tcData : request.getTestCaseDataList()) {
										
										String prefixId = tcData.getTestPrefixId();
										List<TestCaseDataDetails> tcDataDetailsList = tcData.getTcDataDetails();
										if(null != prefixId && null != tcDataDetailsList) {
											int count = 1;
											HashMap<Integer, HashMap<String,String>> h2 = new LinkedHashMap<>();
											for(TestCaseDataDetails tcDataDetails : tcDataDetailsList) {
											
											HashMap<String, String> h3 = new LinkedHashMap<>();
											
											h3.put("iterationCount", tcDataDetails.getIterationCount());
											h3.put("takeScreenshot", tcDataDetails.getTakeScreenshot());
											h3.put("screenshotLink", tcDataDetails.getScreenshotLink());
											h3.put("executedTimestamp", tcDataDetails.getExecutedTimestamp());
											h3.put("stepDescription", tcDataDetails.getStepDescription());
											h3.put("raiseBug", tcDataDetails.getRaiseBug());
											h3.put("buildId", tcDataDetails.getBuildId());
											h3.put("value", tcDataDetails.getValue());
											h3.put("actualResult", tcDataDetails.getActualResult());
											h3.put("status", tcDataDetails.getStatus());
											h3.put("dirName", tcDataDetails.getDirName());
											h3.put("screenshotTitle", tcDataDetails.getScreenshotTitle());
											
										
											h2.put(count, h3);
											count++;
											}
											h1.put(prefixId, h2);
										}
										
									}
									result = updateTestCasedata(h1, "1", 0);
									if(null != result && result.equalsIgnoreCase("Success")) {
										return "Success";
									}
									else {
										return "failed..";
									}
									
								}else {
									return "test case data is null or empty in request";
								}
							}
						}
				
			}
			else {
				return "Build details are null or empty";
			}
			return result;
		}
		
	//to be present at testng side
	public String addTestCaseData(HashMap<String, HashMap<Integer, HashMap<String, String>>> hashMap,
			Integer datasetId, int buildId) {
		FileUploadForm fileUploadForm = new FileUploadForm();
		String testCaseStatus = "";
		boolean isFail = true;
		Set<String> testCases = hashMap.keySet();
		
		//iterating over each test case
		for (String testCase : testCases) {
			stepData = hashMap.get(testCase);
			Set<Integer> steps = stepData.keySet();
			
			//iterating over test steps
			for (Integer step : steps) {
				HashMap<String, String> stepValues = stepData.get(step);
				stepValues.replace("buildId", String.valueOf(buildId));
				if (stepValues.get("takeScreenshot").equalsIgnoreCase("yes")) {
					fileUploadForm.sendFile(stepValues.get("screenshotLink"), stepValues.get("dirName"));
				} else if (stepValues.get("takeScreenshot").equalsIgnoreCase("no")
						&& stepValues.get("status").equalsIgnoreCase("FAIL")) {
					fileUploadForm.sendFile(stepValues.get("screenshotLink"), stepValues.get("dirName"));
				}

				if (stepValues.get("raiseBug").equalsIgnoreCase("yes")) {
					fileUploadForm.raiseBug(testCase, step, stepValues.get("stepDescription"), stepValues.get("actualResult"),
							stepValues.get("screenshotLink"), stepValues.get("buildId"), stepValues.get("dirName"));
				}

				// Need add Switch Case in future
				if (isFail) {
					if (stepValues.get("status").equalsIgnoreCase("1")) {
						testCaseStatus = "1";
					} else if (stepValues.get("status").equalsIgnoreCase("2")) {
						testCaseStatus = "2";
						isFail = false;
					} else if (stepValues.get("status").equalsIgnoreCase("3")) {
						testCaseStatus = "3";
					}
				}
			}
		}
		String result = updateTestCasedata(hashMap, testCaseStatus, datasetId);
		return result;
	}

	//at testNg side only
	@SuppressWarnings({ "deprecation", "unused" })
	private String updateTestCasedata(HashMap<String, HashMap<Integer, HashMap<String, String>>> testCaseData,
			String testCaseStatus, Integer datasetId) {
		String result = null;
		JSONObject testStepJson = new JSONObject();
		try {
			testStepJson.put("Map", testCaseData);
			testStepJson.put("TestStatus", testCaseStatus);
			testStepJson.put("datasetId", datasetId);
			//Client client = Client.create();
			/*WebResource webResource = client.resource("insertautoexestepsForReport?testStepJson="
					+ URLEncoder.encode(testStepJson.toString()));
			ClientResponse response = webResource.accept("application/json").post(ClientResponse.class);*/
			String response = insertautoexestepsForReport(testStepJson.toString());

			if("Success".equalsIgnoreCase(response)) {
				return "Success";
			}else {
				return "Failed";
			}

		} catch (Exception e) {
			result = "FAILED";
			e.printStackTrace();

		}
		return result;
	}
	
	@SuppressWarnings({ "unused" })
	@RequestMapping(value = "insertautoexestepsForReport", method = RequestMethod.POST)
	public @ResponseBody String insertautoexestepsForReport(@RequestParam("testStepJson") String testStepJson) throws Exception {

		
		int datasetId = 0 ;
		JSONObject json = new JSONObject(testStepJson);
		System.out.println("---------------------------------------FUC"+json);
		String testPrefix = "";
		String testCaseStatus = json.get("TestStatus").toString();
		try{
		if(json.get("datasetId") != null)
		datasetId = Integer.parseInt(json.get("datasetId").toString());
		}catch(Exception e){
			System.out.println("dataset id is not present in JSON hence setting it to 0");
		}
		Set<String> keySet = json.getJSONObject("Map").keySet();
		Iterator<String> jsonIterator = keySet.iterator();
		while (jsonIterator.hasNext()) {
			testPrefix = jsonIterator.next().toString();
			int jsonLenght = json.getJSONObject("Map").getJSONObject(testPrefix).length();
			String build_id = null;
			String buildStamp = null;
			String file_path = "";
			StringBuilder testSteps = new StringBuilder();
			StringBuilder stepDetails = new StringBuilder();
			StringBuilder stepValue = new StringBuilder();
			StringBuilder stepStatus = new StringBuilder();
			StringBuilder actualResult = new StringBuilder();
			StringBuilder filepath = new StringBuilder();
			StringBuilder screenshotTitle = new StringBuilder();
			StringBuilder bugRaise = new StringBuilder();
			StringBuilder timestampSB = new StringBuilder();
			StringBuilder iterationCountSB = new StringBuilder();

			Set<String> stepKeySet = json.getJSONObject("Map").getJSONObject(testPrefix).keySet();
			
			List<String> stepKeyList = new ArrayList<String>(stepKeySet);
			Collections.sort(stepKeyList, new Comparator<String>() {
			    @Override
			    public int compare(String s1, String s2) {
			        return Integer.valueOf(s1).compareTo(Integer.valueOf(s2));
			    }
			});
			Iterator<String> jsonStepIterator = stepKeyList.iterator();

			try {
				while (jsonStepIterator.hasNext()) {

					JSONObject stepJson = new JSONObject();
					String stepIdValue = jsonStepIterator.next().toString();
					stepJson = json.getJSONObject("Map").getJSONObject(testPrefix).getJSONObject(stepIdValue + "");
					file_path = Constants.XENON_REPORT_API_REPOSITORY_PATH+"/repository/upload/tmpFiles/" + stepJson.get("dirName") + "/"
							+ stepJson.get("screenshotTitle") + ".png";

					buildStamp = stepJson.get("dirName").toString();
					String stepStatusId = stepJson.get("status").toString();
					String bugStatus = stepStatusId.equalsIgnoreCase("1") ? "1" : "2";

					if (!jsonStepIterator.hasNext()) {
						bugRaise.append(bugStatus);
						testSteps.append(stepIdValue);
						stepDetails.append(stepJson.get("stepDescription").toString().replace("|", ":"));
						stepValue.append(stepJson.get("value").toString().replace("|", ":"));
						stepStatus.append(stepJson.get("status"));
						actualResult.append(stepJson.get("actualResult").toString().replace("|", ":"));
						screenshotTitle.append(stepJson.get("screenshotTitle").toString().replace("|", ":"));
						filepath.append(file_path.toString().replace("|", ":"));
						build_id = stepJson.get("buildId").toString();
						timestampSB.append(stepJson.get("executedTimestamp").toString().replace("|", ":"));
						iterationCountSB.append(stepJson.get("iterationCount").toString().replace("|", ":"));

					} else {
						bugRaise.append(bugStatus + "|");
						testSteps.append(stepIdValue + "|");
						stepDetails.append(stepJson.get("stepDescription").toString().replace("|", ":") + "|");
						stepValue.append(stepJson.get("value").toString().replace("|", ":") + "|");
						stepStatus.append(stepJson.get("status") + "|");
						filepath.append(file_path.toString().replace("|", ":") + "|");
						actualResult.append(stepJson.get("actualResult").toString().replace("|", ":") + "|");
						screenshotTitle.append(stepJson.get("screenshotTitle").toString().replace("|", ":") + "|");
						timestampSB.append(stepJson.get("executedTimestamp").toString().replace("|", ":") + "|");
						iterationCountSB.append(stepJson.get("iterationCount").toString().replace("|", ":") + "|");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String createdDate = dateFormat.format(date);

			/*List<Map<String, Object>> buildDetail = buildDAO.getBuildDetailsByTimestampAndId(Integer.parseInt(build_id),
					buildStamp);
			String schema_name = buildDetail.get(0).get("schema_name").toString();*/
			String schema_name = "XenonAbhay12062019041316";
			buildDAO.insertExecutionStepsArray(testCaseStatus, build_id, testPrefix, testSteps.toString(),
					stepDetails.toString(), actualResult.toString(), stepValue.toString(), stepStatus.toString(),
					filepath.toString(), screenshotTitle.toString(), bugRaise.toString(), schema_name,datasetId, 
					iterationCountSB.toString(), timestampSB.toString());
		}

		return "Success";
	}
	
	@RequestMapping(value = "/viewautomationreport")
	public ModelAndView viewautomationReport(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Automation Summary page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			int buildId = Integer.parseInt(session.getAttribute("selectedAutoReportBuildId").toString());
			// System.out.println("buildId:"+buildId);
			model.addAttribute("Pass", colorProperties.getProperty("testCase.Pass"));
			model.addAttribute("Fail", colorProperties.getProperty("testCase.Fail"));
			model.addAttribute("Skip", colorProperties.getProperty("testCase.Skip"));
			model.addAttribute("Block", colorProperties.getProperty("testCase.blocked"));
			model.addAttribute("NotRun", colorProperties.getProperty("testCase.NotRun"));
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			// System.out.println("viewautomationreport :" + model);

			Map<String, Object> data = reportService.getAutomationSummaryData(buildId, userID, schema);
			// System.out.println("autosummery map data:"+data);
			// System.out.println("buildId:"+buildId+"
			// UserID:"+userID+"schema:"+schema);
			List<Map<String, Object>> projectWiseCount = (List<Map<String, Object>>) data.get("#result-set-1");
			model.addAttribute("projectWiseCount", projectWiseCount);
			List<Map<String, Object>> projectList = (List<Map<String, Object>>) data.get("#result-set-2");
			model.addAttribute("projectList", projectList);
			// System.out.println("pro List A :"+projectList);
			List<Map<String, Object>> moduleWiseCount = (List<Map<String, Object>>) data.get("#result-set-3");
			model.addAttribute("moduleWiseCount", moduleWiseCount);
			List<Map<String, Object>> moduleList = (List<Map<String, Object>>) data.get("#result-set-4");
			model.addAttribute("moduleList", moduleList);
			// System.out.println("module list A :" + moduleList);

			List<Map<String, Object>> scenarioWiseCount = (List<Map<String, Object>>) data.get("#result-set-5");
			model.addAttribute("scenarioWiseCount", scenarioWiseCount);
			List<Map<String, Object>> scenarioList = (List<Map<String, Object>>) data.get("#result-set-6");
			model.addAttribute("scenarioList", scenarioList);
			// System.out.println("scenario list A:" + scenarioList);

			List<Map<String, Object>> testCaseList = (List<Map<String, Object>>) data.get("#result-set-7");

			for (int cnt = 0; cnt < testCaseList.size(); cnt++) {
				testCaseList.get(cnt).put("tcStartTime",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(testCaseList.get(cnt).get("tc_start_time").toString())));
				testCaseList.get(cnt).put("tcEndTime",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(testCaseList.get(cnt).get("tc_end_time").toString())));
			}
			// System.out.println(session.getAttribute("automationExecBuildName"));
			model.addAttribute("testCaseList", testCaseList);

            model.addAttribute("testCaseListJson", utilsService.convertListOfMapToJson(testCaseList));
			List<Map<String, Object>> testStepsList = (List<Map<String, Object>>) data.get("#result-set-8");
			model.addAttribute("testStepsList", testStepsList);
            model.addAttribute("testStepsListJson", utilsService.convertListOfMapToJson(testStepsList));			// System.out.println("testCaseList A:" + testCaseList);
			model.addAttribute("selectedExecuteBuildName", session.getAttribute("automationExecBuildName").toString());
			model.addAttribute("automationExecBuildType", session.getAttribute("automationExecBuildType").toString());
			List<Map<String, Object>> testStepsBugList = (List<Map<String, Object>>) data.get("#result-set-9");
			model.addAttribute("testStepsBugList", testStepsBugList);
			logger.info(" User is redirected to Automation Summary page ");
			return new ModelAndView("viewautomationreport", "Model", model);
		}
	}

	@RequestMapping(value = "/automationtestcase")
	public ModelAndView automationtestcase(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Automation Summary page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			int buildId = Integer.parseInt(session.getAttribute("selectedAutoReportBuildId").toString());

			model.addAttribute("Pass", colorProperties.getProperty("testCase.Pass"));
			model.addAttribute("Fail", colorProperties.getProperty("testCase.Fail"));
			model.addAttribute("Skip", colorProperties.getProperty("testCase.Skip"));
			model.addAttribute("Block", colorProperties.getProperty("testCase.blocked"));
			model.addAttribute("NotRun", colorProperties.getProperty("testCase.NotRun"));
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			String filterText = session.getAttribute("filterTextRS").toString();

			Map<String, Object> data = reportService.getAutomationSummaryData(buildId, userID, schema);

			List<Map<String, Object>> testCaseList = (List<Map<String, Object>>) data.get("#result-set-7");

			for (int cnt = 0; cnt < testCaseList.size(); cnt++) {
				testCaseList.get(cnt).put("tcStartTime",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(testCaseList.get(cnt).get("tc_start_time").toString())));
				testCaseList.get(cnt).put("tcEndTime",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(testCaseList.get(cnt).get("tc_end_time").toString())));
			}

			model.addAttribute("testCaseList", testCaseList);

			List<Map<String, Object>> testStepsList = (List<Map<String, Object>>) data.get("#result-set-8");
			model.addAttribute("testStepsList", testStepsList);

			List<Map<String, Object>> testStepsBugList = (List<Map<String, Object>>) data.get("#result-set-9");
			model.addAttribute("testStepsBugList", testStepsBugList);

			model.addAttribute("filterText", filterText);

			logger.info(" User is redirected to Automation Summary page ");
			return new ModelAndView("automationtestcase", "Model", model);
		}
	}

	@RequestMapping(value = "/automationtestcasereport")
	public ModelAndView automationtestcasereport(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Automation Summary page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Real-Time Analytics");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			int buildId = Integer.parseInt(session.getAttribute("selectedAutoReportBuildId").toString());

			model.addAttribute("Pass", colorProperties.getProperty("testCase.Pass"));
			model.addAttribute("Fail", colorProperties.getProperty("testCase.Fail"));
			model.addAttribute("Skip", colorProperties.getProperty("testCase.Skip"));
			model.addAttribute("Block", colorProperties.getProperty("testCase.blocked"));
			model.addAttribute("NotRun", colorProperties.getProperty("testCase.NotRun"));
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			String filterText = session.getAttribute("filterTextRS").toString();

			Map<String, Object> data = reportService.getAutomationSummaryData(buildId, userID, schema);

			List<Map<String, Object>> testCaseList = (List<Map<String, Object>>) data.get("#result-set-7");

			for (int cnt = 0; cnt < testCaseList.size(); cnt++) {
				testCaseList.get(cnt).put("tcStartTime",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(testCaseList.get(cnt).get("tc_start_time").toString())));
				testCaseList.get(cnt).put("tcEndTime",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(testCaseList.get(cnt).get("tc_end_time").toString())));
			}

			model.addAttribute("testCaseList", testCaseList);

			List<Map<String, Object>> testStepsList = (List<Map<String, Object>>) data.get("#result-set-8");
			model.addAttribute("testStepsList", testStepsList);

			List<Map<String, Object>> testStepsBugList = (List<Map<String, Object>>) data.get("#result-set-9");
			model.addAttribute("testStepsBugList", testStepsBugList);

			model.addAttribute("filterText", filterText);

			logger.info(" User is redirected to Automation Summary page ");
			return new ModelAndView("automationtestcasereport", "Model", model);
		}
	}

	@RequestMapping(value = "/getlogfile")
	public void getlogfile(@RequestParam("logpath") String logpath, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws IOException {
		if (session.getAttribute("userId") == null) {
			return;
		} else {

			final int BUFFER_SIZE = 4096;

			// get absolute path of the application
			ServletContext context = request.getServletContext();
			String appPath = context.getRealPath("");
			// System.out.println("appPath = " + appPath);

			// construct the complete absolute path of the file
			String fullPath = logpath;
			File downloadFile = new File(fullPath);
			FileInputStream inputStream = new FileInputStream(downloadFile);

			// get MIME type of the file
			String mimeType = context.getMimeType(fullPath);
			if (mimeType == null) {
				// set to binary type if MIME mapping not found
				mimeType = "application/octet-stream";
			}
			// System.out.println("MIME type: " + mimeType);

			// set content attributes for the response
			response.setContentType(mimeType);
			response.setContentLength((int) downloadFile.length());

			// set headers for the response
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
			response.setHeader(headerKey, headerValue);

			// get output stream of the response
			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[BUFFER_SIZE];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

			inputStream.close();
			outStream.close();
		}

	}

	@RequestMapping(value = "/setautoexecutebuildid", method = RequestMethod.POST)
	public @ResponseBody int setexecutebuildid(Model model, HttpSession session,
			@RequestParam("buildID") String buildID) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("selectedAutoReportBuildId", buildID);
		return 1;
	}

	@RequestMapping(value = "/viewdataset", method = RequestMethod.GET)
	public ModelAndView viewDatasheet(HttpSession session, Model model, HttpServletRequest request) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);

			String testcaseid = session.getAttribute("selectedTcId").toString();
			String schema = session.getAttribute("cust_schema").toString();
			String userId = session.getAttribute("userId").toString();
			session.setAttribute("menuLiText", session.getAttribute("selectedProject"));
			session.setAttribute("menuLiText1", session.getAttribute("selectedModule"));
			int buildId = Integer.parseInt(session.getAttribute("selectedAutoReportBuildId").toString());
			Map<String, Object> dateSetResult = reportService.getDatasetData(testcaseid, Integer.parseInt(userId),
					buildId, schema);
			List<Map<String, Object>> testStepsList = (List<Map<String, Object>>) dateSetResult.get("#result-set-3");
			model.addAttribute("testStepsList", testStepsList);

			List<Map<String, Object>> testStepsBugList = (List<Map<String, Object>>) dateSetResult.get("#result-set-4");
			model.addAttribute("testStepsBugList", testStepsBugList);
			List<Map<String, Object>> projectList = (List<Map<String, Object>>) dateSetResult.get("#result-set-1");
			List<Map<String, Object>> datasetList = (List<Map<String, Object>>) dateSetResult.get("#result-set-2");
			List<Map<String, Object>> moduleList = (List<Map<String, Object>>) dateSetResult.get("#result-set-5");
			List<Map<String, Object>> scenarioList = (List<Map<String, Object>>) dateSetResult.get("#result-set-6");
			model.addAttribute("projectList", projectList);
			model.addAttribute("datasetList", datasetList);
			model.addAttribute("moduleList", moduleList);
			model.addAttribute("scenarioList", scenarioList);
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			return new ModelAndView("viewdataset", "Model", model);
		}
	}

	@RequestMapping(value = "/settcid", method = RequestMethod.POST)
	public @ResponseBody int setTestCaseId(@RequestParam("testcaseId") String testcaseId,
			@RequestParam("selectedProject") String selectedProject,
			@RequestParam("selectedModule") String selectedModule, Model model, HttpSession session) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("selectedTcId", testcaseId);
		session.setAttribute("selectedProject", selectedProject);
		session.setAttribute("selectedModule", selectedModule);
		return 201;
	}

	@RequestMapping(value = "/setdatasettcid", method = RequestMethod.POST)
	public @ResponseBody int setdatasettcid(@RequestParam("datasetId") String datasetId, Model model,
			HttpSession session) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("selectedDatasetId", datasetId);
		return 201;
	}

	/**
	 * 
	 * @param Model
	 *            model
	 * @param HttpSession
	 *            session
	 * @return Model (extSysReleases) & View (extSysReports)
	 * @throws Exception
	 * @description The extSysReports method is used for getting 'External
	 *              System Reports' which is based on data fetched from the
	 *              External System sync.
	 */
	@RequestMapping(value = "/extSysReportsQTP")
	public ModelAndView extSysReportsQTP(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Bug Analysis page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Third Party Automation Reports");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			List<Map<String, Object>> data = reportService.getAllExtSysReleases(schema);
			model.addAttribute("ExtSysReleases", data);
			List<Map<String, Object>> tcCount = reportService.getExtSysTCCount(schema);
			model.addAttribute("ExtSysTCCount", tcCount);
			logger.info(" User is redirected to Bug Analysis page ");
			return new ModelAndView("extSysReportsQTP", "Model", model);
		}
	}

	/**
	 * 
	 * @param Model
	 *            model
	 * @param HttpSession
	 *            session
	 * @return Model (extSysReleases) & View (extSysReports)
	 * @throws Exception
	 * @description The extSysReports method is used for getting 'External
	 *              System Reports' which is based on data fetched from the
	 *              External System sync.
	 */
	@RequestMapping(value = "/extSysReportsSelenium")
	public ModelAndView extSysReportsSelenium(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Bug Analysis page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Third Party Automation Reports");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			List<Map<String, Object>> data = reportService.getAllExtSysReleases(schema);
			model.addAttribute("ExtSysReleases", data);
			List<Map<String, Object>> tcCount = reportService.getExtSysTCCount(schema);
			model.addAttribute("ExtSysTCCount", tcCount);
			logger.info(" User is redirected to Bug Analysis page ");
			return new ModelAndView("extSysReportsSelenium", "Model", model);
		}
	}

	@RequestMapping(value = "/extSysReportsBuilds")
	public ModelAndView extSysReportsBuilds(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Bug Analysis page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Third Party Automation Reports");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			String filterText = session.getAttribute("filterTextRS").toString();
			String extSysName = session.getAttribute("extSysNameRS").toString();
			List<Map<String, Object>> data = reportService.getAllExtSysBuilds(schema);
			model.addAttribute("ExtSysBuilds", data);
			List<Map<String, Object>> tcCount = reportService.getReleaseTCCount(filterText, schema);
			model.addAttribute("ExtSysReleaseTCCount", tcCount);
			model.addAttribute("filterText", filterText);
			model.addAttribute("extSysName", extSysName);
			logger.info(" User is redirected to Bug Analysis page ");
			return new ModelAndView("extSysReportsBuilds", "Model", model);
		}
	}

	@RequestMapping(value = "/extSysReportsTC")
	public ModelAndView extSysReportsTC(Model model, HttpSession session) throws Exception {

		logger.info("Redirecting to Bug Analysis page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Third Party Automation Reports");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			String filterText = session.getAttribute("filterTextRS").toString();
			String extSysName = session.getAttribute("extSysNameRS").toString();
			List<Map<String, Object>> data = reportService.getAllExtSysTestcases(schema);
			model.addAttribute("ExtSysTestcases", data);
			List<Map<String, Object>> tcCount = reportService.getBuildTCCount(filterText, schema);
			model.addAttribute("ExtSysBuildTCCount", tcCount);
			model.addAttribute("filterText", filterText);
			model.addAttribute("extSysName", extSysName);
			logger.info(" User is redirected to Bug Analysis page ");
			return new ModelAndView("extSysReportsTC", "Model", model);
		}
	}

	@RequestMapping(value = "/extSysReportsTCsteps")
	public ModelAndView extSysReportsTCsteps(Model model, HttpSession session) throws Exception {

		logger.info("Redirecting to Bug Analysis page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Third Party Automation Reports");
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			List<Map<String, Object>> data = reportService.getAllExtSysTeststeps(schema);
			model.addAttribute("ExtSysTeststeps", data);
			String filterText = session.getAttribute("filterTextRS").toString();
			model.addAttribute("filterText", filterText);
			String extSysName = session.getAttribute("extSysNameRS").toString();
			model.addAttribute("extSysName", extSysName);
			logger.info(" User is redirected to Bug Analysis page ");
			return new ModelAndView("extSysReportsTCsteps", "Model", model);
		}
	}

	/**
	 * @author sushnt.arora
	 * @param model
	 * @param session
	 * @param filterText
	 * @param extSysName
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setExtSysSession")
	public ModelAndView setExtSysSession(Model model, HttpSession session,
			@RequestParam("filterText") String filterText, @RequestParam("extSysName") String extSysName)
			throws Exception {
		logger.info("Setting selecte choice for redirection to test manager data page");
		logger.info("checking for active session");

		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirecting to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("filterTextRS", filterText);
			session.setAttribute("extSysNameRS", extSysName);
			return new ModelAndView("redirect:/");
		}
	}

	@RequestMapping(value = "/setTestIdParameter")
	public ModelAndView setParam(Model model, HttpSession session, @RequestParam("filterText") String filterText)
			throws Exception {
		logger.info("Setting selecte choice for redirection to test manager data page");
		logger.info("checking for active session");

		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirecting to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("filterTextRS", filterText);
			return new ModelAndView("redirect:/");
		}
	}

	@RequestMapping(value = "/setSessionController", method = RequestMethod.POST)
	public @ResponseBody int setSessionController(Model model, HttpSession session,
			@RequestParam("autoBuildID") String autoBuildID, @RequestParam("autoBuildName") String autoBuildName)
			throws Exception {
		logger.info("Setting selecte choice for redirection to test manager data page");
		logger.info("checking for active session");

		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirecting to login page");
			return 0;
		} else {
			utilsService.getLoggerUser(session);
			int buildId = Integer.parseInt(autoBuildID);
			session.setAttribute("automationExecBuildId", buildId);
			session.setAttribute("automationExecBuildName", autoBuildName);
			return 1;
		}
	}

	@RequestMapping(value = "/setSessionControllerManual", method = RequestMethod.POST)
	public @ResponseBody int setSessionControllerManual(Model model, HttpSession session,
			@RequestParam("manBuildID") String manBuildID, @RequestParam("manBuildName") String manBuildName)
			throws Exception {
		logger.info("Setting selecte choice for redirection to test manager data page");
		logger.info("checking for active session");

		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirecting to login page");
			return 0;
		} else {
			utilsService.getLoggerUser(session);
			int buildId = Integer.parseInt(manBuildID);
			session.setAttribute("selectedExecuteBuildId", buildId);
			session.setAttribute("selectedExecuteBuildName", manBuildName);
			return 1;
		}
	}

}

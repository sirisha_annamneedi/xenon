package com.xenon.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.NotificationDAO;
import com.xenon.help.dao.ArticleDAO;
import com.xenon.help.dao.FaqDAO;
import com.xenon.help.dao.HelpDAO;
import com.xenon.help.dao.SubcomponentDAO;
import com.xenon.help.domain.ArticleDetails;
import com.xenon.help.domain.FaqDetails;
import com.xenon.help.domain.SubcomponentDetails;

/**
 * 
 * @author prafulla.pol
 * This controller is used for help
 *
 */
@SuppressWarnings({ "unchecked"})
@Controller
public class HelpController {

	private static final Logger logger = LoggerFactory.getLogger(HelpController.class);

	@Autowired
	RoleAccessService roleAccessService;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	NotificationDAO notificationDAO;
	
	@Autowired
	BuildDAO buildDAO;
	
	@Autowired
	FaqDAO FaqDAO;
	
	@Autowired
	ArticleDAO articleDAO;
	
	@Autowired
	SubcomponentDAO subcomponentDAO;
	
	@Autowired
	HelpDAO helpDAO;
	
	/**
	 * @author prafulla.pol
	 * This method is used to show help page
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/help")
	public ModelAndView help(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
			
			if (roleAccessService.checkSupportAccess("create_faq", session))
				model.addAttribute("createFaq", 1);
			else
				model.addAttribute("createFaq", 2);
			utilsService.getLoggerUser(session);
			Map<String, Object> data = helpDAO.getHelpData();
			model.addAttribute("componentData", data.get("#result-set-1"));
			
			List<Map<String, Object>> faqData = (List<Map<String, Object>>) data.get("#result-set-2");
			for (int i = 0; i < faqData.size(); i++) {
				faqData.get(i)
						.put("create_date",
							  utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
							  utilsService.getDate(faqData.get(i).get("create_date").toString())));
			}
			
			model.addAttribute("faqData",faqData);
			
			return new ModelAndView("help","Model",model);
		}
	}
	
	@RequestMapping(value = "/helpmenu")
	public ModelAndView helpmenu(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
			utilsService.getLoggerUser(session);
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			
			List<Map<String,Object>> helpMenu = FaqDAO.getHelpMenu();
			model.addAttribute("helpMenu",helpMenu);
			
			return new ModelAndView("helpmenu","Model",model);
		}
	}
	
	
	@RequestMapping(value = "/helpdeskmenu")
	public ModelAndView helpdeskmenu(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
			utilsService.getLoggerUser(session);
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			
			Map<String, Object> data = subcomponentDAO.getComponentData(Integer.parseInt(session.getAttribute("componentId").toString()));
			model.addAttribute("componentName",session.getAttribute("componentName"));
			model.addAttribute("subcomponentList", data.get("#result-set-1"));
			model.addAttribute("articleList", data.get("#result-set-2"));
			
			return new ModelAndView("helpdeskmenu","Model",model);
		}
	}
	
	
	@RequestMapping(value = "/search")
	public ModelAndView search(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
			utilsService.getLoggerUser(session);
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			return new ModelAndView("search","Model",model);
		}
	}
	

	
	@RequestMapping(value = "/helpheader")
	public ModelAndView bmheader(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active,  Customer Schema " + schema);
			Map<String, Object> buildDashboard =notificationDAO.getNotifications(Integer.parseInt(session.getAttribute("userId").toString()), schema); //buildDAO.headerData(schema);
			List<Map<String, Object>> allBuildsTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-1");
			List<Map<String, Object>> allBuildsExecutedTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-2");
			
			List<Map<String, Object>> allBuilds = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-5");
			
			List<Map<String, Object>> topBuilds = buildDAO.getBmDashboardTopBuildsNf(allBuilds,allBuildsTestcasesCount,
					allBuildsExecutedTestcasesCount, schema);
			for (int i = 0; i < topBuilds.size(); i++) {
				topBuilds.get(i).put("user_image",
						utilsService.getFileData((byte[]) topBuilds.get(i).get("user_photo")));
				topBuilds.get(i).put("build_createdate",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(topBuilds.get(i).get("build_createdate").toString())));
			}
			
			List<Map<String, Object>> notifications =  (List<Map<String, Object>>) buildDashboard
					.get("#result-set-3");
			for (int i = 0; i < notifications.size(); i++) {
				notifications.get(i).put("user_image", utilsService.getFileData((byte[]) notifications.get(i).get("user_photo")));
				String dateInString = notifications.get(i).get("notification_date").toString();
				notifications.get(i).put("notification_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), dateInString));
			}
			List<Map<String, Object>> unreadCount =  (List<Map<String, Object>>) buildDashboard
					.get("#result-set-4");
			model.addAttribute("unreadCount", Integer.parseInt(unreadCount.get(0).get("unreadCount").toString()));
			model.addAttribute("nowDate", utilsService.getNowDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("prevDate",
					utilsService.getPrevDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("notifications", notifications);
			model.addAttribute("topBuilds", topBuilds);
			return new ModelAndView("helpheader", "Model", model);
		}
	}
	
	/**
	 * @author prafulla.pol
	 * This method is used to show create faq page
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addfaq")
	public ModelAndView addFaq(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
			utilsService.getLoggerUser(session);
			
			if (roleAccessService.checkSupportAccess("create_faq", session)) {
				utilsService.getLoggerUser(session);
				model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
				model.addAttribute("userName", session.getAttribute("userFullName"));
				model.addAttribute("role", session.getAttribute("role"));
				return new ModelAndView("addfaq","Model",model);
			} else{
				logger.warn("User has no access to create subcomponent . Redirected to 404 page");
				return new ModelAndView("redirect:/500");
			}
			
		}
	}
	
	/**
	 * @author prafulla.pol
	 * This method is used to insert the faq details into the database
	 * @param questionTitle
	 * @param questionDescription
	 * @param session
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertfaq", method = RequestMethod.POST)
	public ModelAndView insertModule(@RequestParam("questionTitle") String questionTitle,
			@RequestParam("questionDescription") String questionDescription, 
			HttpSession session, Model model) throws Exception {
		logger.info("Post method to insert FAQ. Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			
			if (roleAccessService.checkSupportAccess("create_faq", session)) {
				utilsService.getLoggerUser(session);
				int userID = Integer.parseInt(session.getAttribute("userId").toString());
				String schema = session.getAttribute("cust_schema").toString();
				logger.info(" Session is active, userId is " + userID+ " Customer Schema " + schema);
				
				String queTitle = questionTitle.trim();
				
				FaqDetails faq = new FaqDetails();
				faq.setTitle(queTitle);
				faq.setDescription(questionDescription);
				faq.setFaqCreator(userID);
				
				//int status = moduleApi.insertModule(moduleName, moduleDescription, projectId, moduleStatus,schema);
				int status = FaqDAO.insertFaq(faq);
				if (status != 0) {
					logger.info("New FAQ is Created. Redirected to View FAQ page ");
					return new ModelAndView("redirect:/viewfaq");
				} else {
					logger.warn("Error occured while inserting the FAQ");
					return new ModelAndView("redirect:/500");
				}
			} else{
				logger.warn("User has no access to create subcomponent . Redirected to 404 page");
				return new ModelAndView("redirect:/500");
			}
		}
	}
	
	@RequestMapping(value = "/helpdesk")
	public ModelAndView helpdesk(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
			utilsService.getLoggerUser(session);
		
			Map<String, Object> data = subcomponentDAO.getComponentData(Integer.parseInt(session.getAttribute("componentId").toString()));
			model.addAttribute("componentName",session.getAttribute("componentName"));
			
			
			List<Map<String, Object>> subcomponentList = (List<Map<String, Object>>) data.get("#result-set-1");
			List<Map<String, Object>> articleList = (List<Map<String, Object>>) data.get("#result-set-2");
			
			for(int i=0;i<subcomponentList.size();i++)
			{
				int subComponentId=Integer.parseInt(subcomponentList.get(i).get("id").toString());
				int articleCount=0;
				for(int j=0;j<articleList.size();j++)
				{
					
					int tempId=Integer.parseInt(articleList.get(j).get("subcomponent_id").toString());
					if(subComponentId==tempId)
					{
						articleCount++;
						if(articleCount==1)
							subcomponentList.get(i).put("lastUpdateDate",utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), 
									utilsService.getDate(articleList.get(j).get("updated_date").toString())));
					}
				}
				subcomponentList.get(i).put("articleCount", articleCount);
			}
			
			for(int j=0;j<articleList.size();j++)
			{
				
				articleList.get(j).put("createdDate",utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), 
							utilsService.getDate(articleList.get(j).get("created_date").toString())));
				articleList.get(j).put("updatedDate",utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), 
						utilsService.getDate(articleList.get(j).get("updated_date").toString())));
						
			}
			model.addAttribute("subcomponentList",subcomponentList);
			model.addAttribute("articleList", data.get("#result-set-2"));

			if(roleAccessService.checkSupportAccess("create_subcomponent", session))
			model.addAttribute("createSubcomponent",  1);
			else
				model.addAttribute("createSubcomponent",  2);
			if(roleAccessService.checkSupportAccess("create_article", session))
			model.addAttribute("createArticle", 1);
			else
				model.addAttribute("createArticle", 2);
			
			
			model.addAttribute("componentName",session.getAttribute("componentName"));
			return new ModelAndView("helpdesk","Model",model);
		}
	}
	

	@RequestMapping(value = "/subcomponent")
	public ModelAndView subcomponent(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
			utilsService.getLoggerUser(session);
			List<Map<String, Object>> subcomponent = subcomponentDAO.getSubComponentbyiId(Integer.parseInt(session.getAttribute("subcomponentId").toString()),"");
			List<Map<String, Object>> articleList = articleDAO.getArticleData(Integer.parseInt(session.getAttribute("subcomponentId").toString()),"xenonsupport");
			model.addAttribute("componentName",session.getAttribute("componentName"));
			model.addAttribute("subcomponentList", subcomponent);
			
			for(int j=0;j<articleList.size();j++)
			{
				
				articleList.get(j).put("createdDate",utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), 
							utilsService.getDate(articleList.get(j).get("created_date").toString())));
				articleList.get(j).put("updatedDate",utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), 
						utilsService.getDate(articleList.get(j).get("updated_date").toString())));
						
			}
			
			model.addAttribute("articleList",articleList);
		
			return new ModelAndView("subcomponent","Model",model);
		}
	}
	
	
	@RequestMapping(value = "/addarticle")
	public ModelAndView addarticle(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
			if (roleAccessService.checkSupportAccess("create_article", session)) {
				utilsService.getLoggerUser(session);
			
				if (session.getAttribute("duplicateArticleError") == null) {
					model.addAttribute("articleCreateStatus", -1);
					session.setAttribute("duplicateArticleError",0);
				}
				model.addAttribute("componentName", session.getAttribute("componentName"));
				model.addAttribute("subcomponentName", session.getAttribute("subcomponentName"));
				
				return new ModelAndView("addarticle","Model",model);
			} else{
				logger.warn("User has no access to create subcomponent . Redirected to 404 page");
				return new ModelAndView("redirect:/500");
			}
			
		}
	}
	
	@RequestMapping(value = "/article")
	public ModelAndView viewarticle(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
				utilsService.getLoggerUser(session);
				
				String schema = "xenonsupport";
				List<Map<String, Object>> articleList = articleDAO.getArticle(Integer.parseInt(session.getAttribute("articleId").toString()),schema);
			
				if (roleAccessService.checkSupportAccess("create_article", session))
					model.addAttribute("createArticle", 1);
				else
					model.addAttribute("createArticle", 2);
				
				for(int j=0;j<articleList.size();j++)
				{
					
					articleList.get(j).put("createdDate",utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), 
								utilsService.getDate(articleList.get(j).get("created_date").toString())));
					articleList.get(j).put("updatedDate",utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), 
							utilsService.getDate(articleList.get(j).get("updated_date").toString())));
							
				}
				model.addAttribute("Article",articleList);
				
				model.addAttribute("componentName",session.getAttribute("componentName"));
				model.addAttribute("subcomponentName", session.getAttribute("subcomponentName"));
				return new ModelAndView("article","Model",model);
		}
	}

	@RequestMapping(value = "/updatearticle",method = RequestMethod.POST)
	public ModelAndView updatearticle(Model model, HttpSession session,
			@RequestParam("articleDescription") String articleDescription,
			@RequestParam("articleId") String articleId) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
			
			if (roleAccessService.checkSupportAccess("create_article", session)) {
				utilsService.getLoggerUser(session);
				String schema = "xenonsupport";
				int flag = articleDAO.updateArticle(Integer.parseInt(articleId), articleDescription, schema);
				if(flag!=0)
				{
					return new ModelAndView("redirect:/article");
				}
				return new ModelAndView("redirect:/500");
			} else{
				logger.warn("User has no access to create subcomponent . Redirected to 404 page");
				return new ModelAndView("redirect:/500");
			}
			
		}
	}
	
	@RequestMapping(value = "/insertarticle", method = RequestMethod.POST)
	public @ResponseBody  String insertarticle(Model model, HttpSession session, @RequestParam("articleName") String articleName,
			@RequestParam("articleDescription") String articleDescription
			)throws Exception {
		logger.info("Post method to insert article and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return "-1";
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			int customerId = Integer.parseInt(session.getAttribute("customerId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID);
			logger.info("Cheking for User's access to create article");
		
				logger.info("User has access to create article");
				
				ArticleDetails articleDetails = new ArticleDetails();
				articleDetails.setArtName(articleName);
				articleDetails.setArtDesc(articleDescription);
				articleDetails.setCustomerId(customerId);
				articleDetails.setSubcomponentId(Integer.parseInt(session.getAttribute("subcomponentId").toString()));
				articleDetails.setUserId(userID);
				int flag = articleDAO.createNewArticle(articleDetails, schema);
				if (flag == 1) {
					session.setAttribute("articleCreateStatus", 1);
					session.setAttribute("newArticleName", articleName.trim());
					logger.info("Article is created. User is redirected to view article ");
					return "1";
				} else {
					session.setAttribute("articleCreateStatus", 0);
					session.setAttribute("duplicateArticleError", 1);
					model.addAttribute("duplicateArticleError", 1);
					logger.info("Error : Duplicate article name . User is redirected to create article ");
					return "0";
				}
		}
	}
	
	@RequestMapping(value = "/addsubcomponent")
	public ModelAndView addsubcomponent(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
			if (roleAccessService.checkSupportAccess("create_subcomponent", session)) {
				utilsService.getLoggerUser(session);
				
				if (session.getAttribute("duplicateSubcomponentError") == null) {
					model.addAttribute("subcomponentCreateStatus", -1);
					session.setAttribute("duplicateSubcomponentError",0);
				}
				
				model.addAttribute("componentName",session.getAttribute("componentName"));
				return new ModelAndView("addsubcomponent","Model",model);

			} else{
				logger.warn("User has no access to create subcomponent . Redirected to 404 page");
				return new ModelAndView("redirect:/500");
			}
		}
	}
	
	@RequestMapping(value = "/insertsubcomponent", method = RequestMethod.POST)
	public @ResponseBody  String insertsubcomponent(Model model, HttpSession session, @RequestParam("subcomponentName") String subcomponentName,
			@RequestParam("subcomponentDescription") String subcomponentDescription,@RequestParam("componentId") String componentId
			)
					throws Exception {
		logger.info("Post method to insert subcomponent and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return "-1";
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID);
			logger.info("Cheking for User's access to create subcomponent");
			if (roleAccessService.checkSupportAccess("create_subcomponent", session)) {
				logger.info("User has access to create subcomponent");
				SubcomponentDetails subcomponentDetails = new SubcomponentDetails();
				subcomponentDetails.setSubcmpName(subcomponentName);
				subcomponentDetails.setSubcmpDesc(subcomponentDescription);
				subcomponentDetails.setComponentId(Integer.parseInt(session.getAttribute("componentId").toString()));
				int flag = subcomponentDAO.createNewSubcomponent(subcomponentDetails, schema);
				if (flag == 1) {
					session.setAttribute("subcomponentCreateStatus", 1);
					session.setAttribute("newSubcomponentName", subcomponentName.trim());
					logger.info("Subcomponent is created. User is redirected to view subcomponent ");
					return "1";
				} else {
					session.setAttribute("subcomponentCreateStatus", 0);
					session.setAttribute("duplicateSubcomponentError", 1);
					model.addAttribute("duplicateSubcomponentError", 1);
					logger.info("Error : Duplicate subcomponent name . User is redirected to create subcomponent ");
					return "0";
				}
			} else{
				logger.warn("User has no access to create subcomponent . Redirected to 404 page");
				return "-1";
			}
		}
	}
	
	@RequestMapping(value = "/setArticleId", method = RequestMethod.POST)
	public ModelAndView setArticleId(Model model, HttpSession session, @RequestParam("articleId") String articleId) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("articleId", articleId);
		return helpdesk(model, session);
	}
	
	@RequestMapping(value = "/setSubcomponentId", method = RequestMethod.POST)
	public ModelAndView setSubcomponentId(Model model, HttpSession session, @RequestParam("subcomponentId") String subcomponentId) throws Exception {
		utilsService.getLoggerUser(session);
		List<Map<String, Object>> data = subcomponentDAO.getSubComponentbyiId(Integer.parseInt(subcomponentId),"");
		session.setAttribute("subcomponentId", subcomponentId);
		session.setAttribute("subcomponentName", data.get(0).get("subcmp_name"));
		return helpdesk(model, session);
	}
	

	/**
	 * @author prafulla.pol
	 * This method is used to view all the FAQ's
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/viewfaq")
	public ModelAndView viewFaq(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
			utilsService.getLoggerUser(session);
			List<Map<String,Object>> faqData = FaqDAO.viewFaq();
			for (int i = 0; i < faqData.size(); i++) {
				faqData.get(i)
						.put("create_date",
							  utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
							  utilsService.getDate(faqData.get(i).get("create_date").toString())));
			}
			model.addAttribute("faqData",faqData);
			
			if (roleAccessService.checkSupportAccess("create_faq", session))
				model.addAttribute("create_faq", 1);
			else
				model.addAttribute("create_faq", 2);
			
			return new ModelAndView("viewfaq","Model",model);
		}
	}
	
	/**
	 * @author prafulla.pol
	 * This method is used to set the component id to the session
	 * @param model
	 * @param session
	 * @param componentId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setcomponentid", method = RequestMethod.POST)
	public @ResponseBody  int setComponentId(Model model, HttpSession session,
			@RequestParam("componentId") String componentId)throws Exception {
		logger.info("Post method to set component ID into the session");
		utilsService.getLoggerUser(session);
		List<Map<String, Object>> data = subcomponentDAO.getComponentbyiId(Integer.parseInt(componentId),"");
		session.setAttribute("componentId", componentId);
		session.setAttribute("componentName", data.get(0).get("cmp_name"));
		return 1;		
	}
	
	/**
	 * @author prafulla.pol
	 * This method is used to get the single FAQ details and show on the faqdetails page
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/faqdetails")
	public ModelAndView faqDetails(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
			utilsService.getLoggerUser(session);
			
			int faqId = Integer.parseInt(session.getAttribute("faqId").toString());
			//get the faq details
			List<Map<String,Object>> details = FaqDAO.viewSingleFaq(faqId);
			
			for (int i = 0; i < details.size(); i++) {
				details.get(i)
						.put("create_date",
							  utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
							  utilsService.getDate(details.get(i).get("create_date").toString())));
			}
			model.addAttribute("details",details);
			
			if (roleAccessService.checkSupportAccess("create_faq", session))
				model.addAttribute("create_faq", 1);
			else
				model.addAttribute("create_faq", 2);
			
			return new ModelAndView("faqdetails","Model",model);
		}
	}
	
	/**
	 * @author prafulla.pol
	 * This method is used to set the set the faq id into the session
	 * @param model
	 * @param session
	 * @param faqId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setfaqid", method = RequestMethod.POST)
	public @ResponseBody int setFaqId(Model model, HttpSession session,
			@RequestParam("faqId") String faqId)throws Exception {
		logger.info("Post method to set component ID into the session");
		utilsService.getLoggerUser(session);
		session.setAttribute("faqId", faqId);
		return 1;		
	}
	
	/**
	 * @author prafulla.pol
	 * update the faq details
	 * @param model
	 * @param session
	 * @param articleDescription
	 * @param articleId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatefaq",method = RequestMethod.POST)
	public ModelAndView updateFaq(Model model, HttpSession session,
			@RequestParam("faqDescription") String faqDescription,
			@RequestParam("faqId") int faqId) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
			utilsService.getLoggerUser(session);
			String schema = "xenonsupport";

			int flag = FaqDAO.updateFaq(faqId, faqDescription, schema);

			if(flag!=0)
			{
				return new ModelAndView("redirect:/faqdetails");
			}else
				return new ModelAndView("redirect:/500");
		}
	}
	
	/**
	 * @author navnath.damale
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :" + ex);
		return new ModelAndView("redirect:/500");
	}

}

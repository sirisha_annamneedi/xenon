package com.xenon.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jcraft.jsch.Session;
import com.xenon.api.buildmanager.SettingService;
import com.xenon.api.common.JiraIntegrationService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.VMDetailsDAO;
import com.xenon.buildmanager.domain.MailDetails;
import com.xenon.common.dao.MailDAO;
import com.xenon.core.dao.CustomerDAO;;

/**
 * This controller includes requests to create,update operations of a customer settings
 * 
 * @author bhagyashri.ajmera
 *
 */
@SuppressWarnings({ })
@Controller
public class SettingsController {

	private static final Logger logger = LoggerFactory.getLogger(SettingsController.class);

	@Autowired
	RoleAccessService roleAccessService;
	
	@Autowired
	SettingService settingService;

	@Autowired
	LoginService loginService;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	VMDetailsDAO vmDetailsDAO;
	
	@Autowired
	CustomerDAO custDetailsDAO;
	
	@Autowired
	JiraIntegrationService jiraService;
	
	@Autowired
	MailDAO mailDAO;
	
	@Autowired
	BuildDAO buildDAO;
	
	/**
	 * Description : This request gives the view VM details
	 * vm page
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/vmdetails")
	public ModelAndView vmdetails(Model model, HttpSession session) throws Exception {
		logger.info("Navigating to create customer page");
		if (session.getAttribute("userId") == null) {
			logger.info("User not logged in or session expired");
			logger.info("Navigating to login page");
			return new ModelAndView("redirect:/logout");
		}
		if (!(buildDAO.checkBMRoleAccess("viewvm",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString()) && Integer.parseInt(session.getAttribute("automationStatus").toString())==1)) {
			return new ModelAndView("redirect:/500");
		}
		else {
			if(!utilsService.checkSessionContainsAttribute("duplicateVmNameError", session))
			{
				session.setAttribute("duplicateVmNameError", 0);
			}
			boolean createVMStatus = buildDAO.checkBMRoleAccess("createvm",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString());
			if(createVMStatus)
				model.addAttribute("createvm",1);
			else
				model.addAttribute("createvm",0);	
			List<Map<String, Object>> vmList=vmDetailsDAO.getAllVmDetails(session.getAttribute("customerId").toString());
			model.addAttribute("vmList", vmList);
			utilsService.getLoggerUser(session);
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			session.setAttribute("menuLiText", "Settings");
			return new ModelAndView("vmdetails", "Model", model);
		}
	}
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request sets selected VM id in to session
	 * 
	 * @param vmId
	 * @param viewType
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setselectedvmid")
	public ModelAndView setselectedvmid(@RequestParam("vmId") int vmId,
			@RequestParam("viewType") int viewType,  Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("selectedVmId", vmId);
			session.setAttribute("viewType", viewType);
			return new ModelAndView("redirect:/");
		}
	}
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request edits VM details
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/editvmdetails")
	public ModelAndView editvmdetails(Model model, HttpSession session) throws Exception {
		logger.info("Navigating to create customer page");
		if (session.getAttribute("userId") == null) {
			logger.info("User not logged in or session expired");
			logger.info("Navigating to login page");
			return new ModelAndView("redirect:/logout");
		}
		if (!buildDAO.checkBMRoleAccess("createvm",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			return new ModelAndView("redirect:/500");
		}
		else {
			utilsService.getLoggerUser(session);
			if(utilsService.checkSessionContainsAttribute("selectedVmId", session))
			{
				boolean createVMStatus = buildDAO.checkBMRoleAccess("createvm",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString());
				if(createVMStatus)
					model.addAttribute("createvm",1);
				else
					model.addAttribute("createvm",0);	
				logger.info("Reading VM details info");
				List<Map<String, Object>> vmDetails=vmDetailsDAO.getVmDetailsById(Integer.parseInt(session.getAttribute("selectedVmId").toString()),session.getAttribute("customerId").toString());
				logger.info("Add attributes to model");
				model.addAttribute("vmDetails", vmDetails);
				model.addAttribute("viewType",Integer.parseInt(session.getAttribute("viewType").toString()));
				model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
				model.addAttribute("userName", session.getAttribute("userFullName"));
				model.addAttribute("role", session.getAttribute("role"));
				session.setAttribute("menuLiText", "Settings");
				return new ModelAndView("editvmdetails", "Model", model);
			}
			else
			{
				return new ModelAndView("redirect:/500");
			}
		}
	}
	
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request inserts VM details to database
	 * 
	 * @param model
	 * @param session
	 * @param hostname
	 * @param ipAddress
	 * @param projectLocation
	 * @param ieStatus
	 * @param chromeStatus
	 * @param firefoxStatus
	 * @param portNumber
	 * @param safariStatus
	 * @param vmStatus
	 * @param concExecStatus
	 * @param concurrentExecNumber
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/insertvmdetails", method = RequestMethod.POST)
	public ModelAndView insertvmdetails(Model model, HttpSession session, @RequestParam("hostname") String hostname,
			@RequestParam("ipAddress") String ipAddress, @RequestParam("projectLocation") String projectLocation,
			@RequestParam("ieStatus") String ieStatus, @RequestParam("chromeStatus") String chromeStatus,
			@RequestParam("firefoxStatus") String firefoxStatus,
			@RequestParam("portNumber") String portNumber,
			@RequestParam(value = "remoteUsername",defaultValue = "") String remoteUsername ,
			@RequestParam(value = "remotePassword",defaultValue = "") String remotePassword,
			@RequestParam("safariStatus") String safariStatus, @RequestParam("vmStatus") String vmStatus,
			@RequestParam("concExecStatus") String concExecStatus,@RequestParam(value="concurrentExecNumber",defaultValue="1") String concurrentExecNumber,
			@RequestParam("repositoryStatus") String repositoryStatus) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			
			int flag = settingService.insertvmdetails(hostname, ipAddress,Integer.parseInt(portNumber),remoteUsername,remotePassword,projectLocation,Integer.parseInt(concExecStatus),Integer.parseInt(concurrentExecNumber), Integer.parseInt(ieStatus), Integer.parseInt(chromeStatus), Integer.parseInt(firefoxStatus),Integer.parseInt( safariStatus), Integer.parseInt(vmStatus),session.getAttribute("customerId").toString(),session.getAttribute("cust_schema").toString(),session.getAttribute("roleId").toString(),Integer.parseInt(repositoryStatus));

			if (flag == 201) {
				logger.info("VM details inserted successfully");
				session.setAttribute("duplicateVmNameError", 0);
				// setting session variable for the toster
				session.setAttribute("vmCreateStatus", 1);
				session.setAttribute("newVmName", hostname);
				return new ModelAndView("redirect:/vmdetails");
			} else if (flag == 208) {
				session.setAttribute("duplicateVmNameError", -1);
				return new ModelAndView("redirect:/vmdetails");
			} else {
				logger.error("VM details insertion unsuccessful");
				session.setAttribute("duplicateVmNameError", -1);
				return new ModelAndView("redirect:/"+flag);
			}
		}
		
		
	}
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request updates VM details to database
	 * 
	 * @param model
	 * @param session
	 * @param hostname
	 * @param ipAddress
	 * @param projectLocation
	 * @param ieStatus
	 * @param chromeStatus
	 * @param firefoxStatus
	 * @param portNumber
	 * @param safariStatus
	 * @param vmStatus
	 * @param concExecStatus
	 * @param concurrentExecNumber
	 * @return
	 */

	@RequestMapping(value = "/updatevmdetails", method = RequestMethod.POST)
	public ModelAndView updatevmdetails(Model model, HttpSession session, @RequestParam("hostname") String hostname,
			@RequestParam("ipAddress") String ipAddress, @RequestParam("projectLocation") String projectLocation,
			@RequestParam("ieStatus") String ieStatus, @RequestParam("chromeStatus") String chromeStatus,
			@RequestParam("firefoxStatus") String firefoxStatus,
			@RequestParam("portNumber") String portNumber,
			@RequestParam(value = "remoteUsername",defaultValue = "") String remoteUsername ,
			@RequestParam(value = "remotePassword",defaultValue = "") String remotePassword,
			@RequestParam("safariStatus") String safariStatus,
			@RequestParam("vmStatus") String vmStatus,
			@RequestParam("vmStatusFree") String vmStatusFree,
			@RequestParam("concExecStatus") String concExecStatus,
			@RequestParam(value="concurrentExecNumber",defaultValue="1") String concurrentExecNumber,
			@RequestParam("repositoryStatus") String repositoryStatus) {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			
			int flag  = settingService.updatevmdetails(Integer.parseInt(vmStatusFree),hostname, ipAddress,Integer.parseInt(portNumber),remoteUsername,remotePassword, projectLocation,Integer.parseInt(concExecStatus),Integer.parseInt(concurrentExecNumber), Integer.parseInt(ieStatus), Integer.parseInt(chromeStatus), Integer.parseInt(firefoxStatus),Integer.parseInt( safariStatus), Integer.parseInt(vmStatus),session.getAttribute("customerId").toString(),Integer.parseInt(session.getAttribute("selectedVmId").toString()),session.getAttribute("cust_schema").toString(),session.getAttribute("roleId").toString(),Integer.parseInt(repositoryStatus));
				if (flag == 201) {
					logger.info("VM details updated successfully");
					session.setAttribute("vmupdateStatus",1);
					session.setAttribute("updateVmName",hostname);
					return new ModelAndView("redirect:/editvmdetails");
			} else
				return new ModelAndView("redirect:/"+flag);
		}
	}

	
	/**
	 * @author 
	 * @method get view for account setting
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/accountsettings")
	public ModelAndView accountsettings(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("accountsettings",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Settings");
			int custId = Integer.parseInt(session.getAttribute("customerId").toString());

			Map<String, Object> custData = custDetailsDAO.getAccountSettingsData(custId,
					session.getAttribute("cust_schema").toString());

			model.addAttribute("custTypes", (List<Map<String, Object>>) custData.get("#result-set-1"));
			List<Map<String, Object>> custDetails = (List<Map<String, Object>>) custData.get("#result-set-2");
			for (int i = 0; i < custDetails.size(); i++) {
				custDetails.get(i).put("created_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(custDetails.get(i).get("created_date").toString())));
				custDetails.get(i).put("start_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(custDetails.get(i).get("start_date").toString())));
				custDetails.get(i).put("end_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(custDetails.get(i).get("end_date").toString())));
			}

			model.addAttribute("CustDetails", custDetails);
			model.addAttribute("customerLogo", loginService.getImage((byte[]) custDetails.get(0).get("customer_logo")));
			List<Map<String, Object>> custAdminDetails = (List<Map<String, Object>>) custData.get("#result-set-4");
			model.addAttribute("custAdminDetails", custAdminDetails);
			List<Map<String, Object>> cloudSpace = (List<Map<String, Object>>) custData.get("#result-set-5");
			model.addAttribute("cloudSpace", cloudSpace);

			return new ModelAndView("accountsettings", "Model", model);
		} else
			return new ModelAndView("500");

	}
	
	/**
	 * @author 
	 * @method get view for third party issue tracker setting
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/thirdpartyissuetracker")
	public ModelAndView thirdpartyissuetracker(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("accountsettings",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Settings");
			int custId = Integer.parseInt(session.getAttribute("customerId").toString());

			Map<String, Object> custData = custDetailsDAO.getAccountSettingsData(custId,
					session.getAttribute("cust_schema").toString());

			
			List<Map<String, Object>> custDetails = (List<Map<String, Object>>) custData.get("#result-set-2");
			for (int i = 0; i < custDetails.size(); i++) {
				
				if((custDetails.get(i).get("issue_tracker").toString()=="1")){
					custDetails.get(i).put("connected_status", "Connected");
				}else{
					custDetails.get(i).put("connected_status", "Not Connected");
				}
			}

			model.addAttribute("CustDetails", custDetails);
			
			List<Map<String, Object>> issueTrackerType = (List<Map<String, Object>>) custData.get("#result-set-3");
			model.addAttribute("issueTrackerType", issueTrackerType);
			List<Map<String, Object>> jiraDetails = jiraService.getJiraDetails(session.getAttribute("cust_schema").toString());
			model.addAttribute("JiraDetails", jiraDetails);
			return new ModelAndView("thirdpartyissuetracker", "Model", model);
		} else
			return new ModelAndView("500");

	}
	
	/**
	 * Description : This request handles the exceptions in the class
	 * 
	 * @param ex
	 * @param response
	 * @param session
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		return new ModelAndView("redirect:/500");
	}
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request inserts cloud VM details to database
	 * 
	 * @param model
	 * @param session
	 * @param hostname
	 * @param ipAddress
	 * @param projectLocation
	 * @param ieStatus
	 * @param chromeStatus
	 * @param firefoxStatus
	 * @param portNumber
	 * @param safariStatus
	 * @param vmStatus
	 * @param concExecStatus
	 * @param concurrentExecNumber
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/insertcloudvmdetails", method = RequestMethod.POST)
	public ModelAndView insertcloudvmdetails(Model model, HttpSession session, @RequestParam("hostname") String hostname,
			@RequestParam("ipAddress") String ipAddress, @RequestParam("projectLocation") String projectLocation,
			@RequestParam("ieStatus") String ieStatus, @RequestParam("chromeStatus") String chromeStatus,
			@RequestParam("firefoxStatus") String firefoxStatus,
			@RequestParam("portNumber") String portNumber,
			@RequestParam(value = "remoteUsername",defaultValue = "") String remoteUsername ,
			@RequestParam(value = "remotePassword",defaultValue = "") String remotePassword,
			@RequestParam("safariStatus") String safariStatus, @RequestParam("vmStatus") String vmStatus,
			@RequestParam("concExecStatus") String concExecStatus,@RequestParam(value="concurrentExecNumber",defaultValue="1") String concurrentExecNumber,
			@RequestParam("repositoryStatus") String repositoryStatus) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			
			int flag = settingService.insertCloudVmDetails(hostname, ipAddress,Integer.parseInt(portNumber),remoteUsername,remotePassword,projectLocation,Integer.parseInt(concExecStatus),Integer.parseInt(concurrentExecNumber), Integer.parseInt(ieStatus), Integer.parseInt(chromeStatus), Integer.parseInt(firefoxStatus),Integer.parseInt( safariStatus), Integer.parseInt(vmStatus),session.getAttribute("customerId").toString(),session.getAttribute("cust_schema").toString(),session.getAttribute("roleId").toString(),Integer.parseInt(repositoryStatus));

			if (flag == 201) {
				logger.info("VM details inserted successfully");
				session.setAttribute("duplicateVmNameError", 0);
				// setting session variable for the toster
				session.setAttribute("vmCreateStatus", 1);
				session.setAttribute("newVmName", hostname);
				return new ModelAndView("redirect:/cloudvmdetails");
			} else if (flag == 208) {
				session.setAttribute("duplicateVmNameError", -1);
				return new ModelAndView("redirect:/cloudvmdetails");
			} else {
				logger.error("VM details insertion unsuccessful");
				session.setAttribute("duplicateVmNameError", -1);
				return new ModelAndView("redirect:/"+flag);
			}
		}
	}
	
	
	/**
	 * Description : This request gives the view cloud VM details vm page
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/cloudvmdetails")
	public ModelAndView cloudvmdetails(Model model, HttpSession session) throws Exception {
		logger.info("Navigating to create customer page");
		if (session.getAttribute("userId") == null) {
			logger.info("User not logged in or session expired");
			logger.info("Navigating to login page");
			return new ModelAndView("redirect:/logout");
		}
		if (!(buildDAO.checkBMRoleAccess("viewvm",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())
				&& Integer.parseInt(session.getAttribute("automationStatus").toString()) == 1)) {
			return new ModelAndView("redirect:/500");
		} else {
			if (!utilsService.checkSessionContainsAttribute("duplicateVmNameError", session)) {
				session.setAttribute("duplicateVmNameError", 0);
			}
			boolean createVMStatus = buildDAO.checkBMRoleAccess("createvm",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString());
			if (createVMStatus)
				model.addAttribute("createvm", 1);
			else
				model.addAttribute("createvm", 0);
			List<Map<String, Object>> vmList = vmDetailsDAO.getAllCloudVmDetails();
			model.addAttribute("vmList", vmList);
			utilsService.getLoggerUser(session);
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			session.setAttribute("menuLiText", "Settings");
			return new ModelAndView("cloudvmdetails", "Model", model);
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request sets selected cloud VM id in to session
	 * 
	 * @param vmId
	 * @param viewType
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setselectedcloudvmid")
	public ModelAndView setselectedcloudvmid(@RequestParam("vmId") int vmId, @RequestParam("viewType") int viewType,
			Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("selectedCloudVmId", vmId);
			session.setAttribute("viewVMType", viewType);
			return new ModelAndView("redirect:/");
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request edits cloud VM details
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/editcloudvmdetails")
	public ModelAndView editcloudvmdetails(Model model, HttpSession session) throws Exception {
		logger.info("Navigating to create customer page");
		if (session.getAttribute("userId") == null) {
			logger.info("User not logged in or session expired");
			logger.info("Navigating to login page");
			return new ModelAndView("redirect:/logout");
		}
		if (!buildDAO.checkBMRoleAccess("createvm",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			return new ModelAndView("redirect:/500");
		} else {
			utilsService.getLoggerUser(session);
			if (utilsService.checkSessionContainsAttribute("selectedCloudVmId", session)) {
				boolean createVMStatus =buildDAO.checkBMRoleAccess("createvm",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString());
				if (createVMStatus)
					model.addAttribute("createvm", 1);
				else
					model.addAttribute("createvm", 0);
				logger.info("Reading VM details info");
				List<Map<String, Object>> vmDetails = vmDetailsDAO.getCloudVmDetailsById(
						Integer.parseInt(session.getAttribute("selectedCloudVmId").toString()));
				logger.info("Add attributes to model");
				model.addAttribute("vmDetails", vmDetails);
				model.addAttribute("viewType", Integer.parseInt(session.getAttribute("viewVMType").toString()));
				model.addAttribute("UserProfilePhoto",
						loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
				model.addAttribute("userName", session.getAttribute("userFullName"));
				model.addAttribute("role", session.getAttribute("role"));
				session.setAttribute("menuLiText", "Settings");
				return new ModelAndView("editcloudvmdetails", "Model", model);
			} else {
				return new ModelAndView("redirect:/500");
			}
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request updates cloud VM details to database
	 * 
	 * @param model
	 * @param session
	 * @param hostname
	 * @param ipAddress
	 * @param projectLocation
	 * @param ieStatus
	 * @param chromeStatus
	 * @param firefoxStatus
	 * @param portNumber
	 * @param safariStatus
	 * @param vmStatus
	 * @param concExecStatus
	 * @param concurrentExecNumber
	 * @return
	 */
	
	@RequestMapping(value = "/updatecloudvmdetails", method = RequestMethod.POST)
	public ModelAndView updatecloudvmdetails(Model model, HttpSession session,
			@RequestParam("hostname") String hostname, @RequestParam("ipAddress") String ipAddress,
			@RequestParam("projectLocation") String projectLocation, @RequestParam("ieStatus") String ieStatus,
			@RequestParam("chromeStatus") String chromeStatus, @RequestParam("firefoxStatus") String firefoxStatus,
			@RequestParam("portNumber") String portNumber,
			@RequestParam(value = "remoteUsername",defaultValue = "") String remoteUsername ,
			@RequestParam(value = "remotePassword",defaultValue = "") String remotePassword,
			@RequestParam("safariStatus") String safariStatus,
			@RequestParam("vmStatus") String vmStatus, @RequestParam("vmStatusFree") String vmStatusFree,
			@RequestParam("concExecStatus") String concExecStatus,
			@RequestParam(value = "concurrentExecNumber", defaultValue = "1") String concurrentExecNumber,
			@RequestParam("repositoryStatus") String repositoryStatus) {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);

			int flag = settingService.updateCloudvmdetails(Integer.parseInt(vmStatusFree), hostname, ipAddress, Integer.parseInt(portNumber), remoteUsername,
					remotePassword, projectLocation, Integer.parseInt(concExecStatus),
					Integer.parseInt(concurrentExecNumber), Integer.parseInt(ieStatus), Integer.parseInt(chromeStatus),
					Integer.parseInt(firefoxStatus), Integer.parseInt(safariStatus), Integer.parseInt(vmStatus),
					Integer.parseInt(session.getAttribute("selectedCloudVmId").toString()),session.getAttribute("cust_schema").toString(),session.getAttribute("roleId").toString(),Integer.parseInt(repositoryStatus));
			if (flag == 201) {
				logger.info("VM details updated successfully");
				session.setAttribute("vmupdateStatus", 1);
				session.setAttribute("updateVmName", hostname);
				return new ModelAndView("redirect:/cloudvmdetails");
			} else
				return new ModelAndView("redirect:/" + flag);
		}
	}
	
	
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This returns view for mail page
	 * 
	 * @param model
	 * @param session
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/mail")
	public ModelAndView mail(Model model, HttpSession session) {

		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/");
		} else if (buildDAO.checkBMRoleAccess("mail",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Settings");
			String schema = session.getAttribute("cust_schema").toString();
			logger.info("Reading mail settings data");
			Map<String, Object> buildDashboard = mailDAO.getMailSettings(schema);
			List<Map<String, Object>> smtpDetails = (List<Map<String, Object>>) buildDashboard.get("#result-set-1");
			List<Map<String, Object>> mailNotificationDetails = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-2");
			if (smtpDetails.size() > 0) {
				session.setAttribute("mailDetailsStatus", 2);
				model.addAttribute("smtpDetails", smtpDetails);
				model.addAttribute("mailNotificationDetails", mailNotificationDetails);
				return new ModelAndView("mail", "Model", model);
			} else {
				logger.info("Returning to mail view");
				session.setAttribute("mailDetailsStatus", 1);
				return new ModelAndView("mail");
			}
		} else
			return new ModelAndView("500");
	}


	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request inserts mail details to database
	 * 
	 * @param model
	 * @param session
	 * @param smtpHost
	 * @param smtpPort
	 * @param smtpUserID
	 * @param mailStatus
	 * @param smtpPassword
	 * @return
	 */
	@RequestMapping(value = "/insertsmtpdetails", method = RequestMethod.POST)
	public ModelAndView insertSMTPDetails(Model model, HttpSession session,
			@RequestParam(value = "smtpHost") String smtpHost,
			@RequestParam(value = "smtpPort", defaultValue = "0") String smtpPort,
			@RequestParam(value = "smtpUserID") String smtpUserID, @RequestParam("mailStatus") String mailStatus,
			@RequestParam(value = "smtpPassword") String smtpPassword) {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/");
		} else if (buildDAO.checkBMRoleAccess("mail",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			logger.info("Setting mail details domain");
			MailDetails mailDetails =new MailDetails();
			mailDetails.setIsEnabled(Integer.parseInt(mailStatus));
			mailDetails.setSmtpHost(smtpHost);
			mailDetails.setSmtpPassword(smtpPassword);
			mailDetails.setSmtpPort(Integer.parseInt(smtpPort));
			mailDetails.setSmtpUserId(smtpUserID);
			logger.info("Inserting SMTP details to database");
			int flag = mailDAO.insertSMTPDetails(mailDetails, schema);
			if (flag == 1)
			{
				logger.info("Mail details inserted successfully");
				return new ModelAndView("redirect:/mail");
			}
			else
			{
				logger.error("mail details insertion unsuccessful");
				return new ModelAndView("500");
			}
		} else
			return new ModelAndView("500");
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *  This request updates mail details into the database
	 * 
	 * @param model
	 * @param session
	 * @param smtpID
	 * @param smtpHost
	 * @param smtpPort
	 * @param smtpUserID
	 * @param mailStatus
	 * @param smtpPassword
	 * @return
	 */
	@RequestMapping(value = "/updatesmtpdetails", method = RequestMethod.POST)
	public ModelAndView updateSmtpDetails(Model model, HttpSession session, @RequestParam("smtpID") String smtpID,
			@RequestParam("smtpHost") String smtpHost,
			@RequestParam(value = "smtpPort", defaultValue = "0") String smtpPort,
			@RequestParam("smtpUserID") String smtpUserID, @RequestParam("mailStatus") String mailStatus,
			@RequestParam("smtpPassword") String smtpPassword) {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/");
		} else if (buildDAO.checkBMRoleAccess("mail",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			/*String schema = session.getAttribute("cust_schema").toString();
			int id = Integer.parseInt(smtpID);
			logger.info("Setting mail details domain");
			MailDetails mailDetails=new MailDetails();
			mailDetails.setIsEnabled(Integer.parseInt(mailStatus));
			mailDetails.setSmtpHost(smtpHost);
			mailDetails.setSmtpPassword(smtpPassword);
			mailDetails.setSmtpPort(Integer.parseInt(smtpPort));
			mailDetails.setSmtpUserId(smtpUserID);
			mailDetails.setId(id);
			int flag = mailDAO.updateSmtpDetails(mailDetails, schema);*/
			String schema = session.getAttribute("cust_schema").toString();
			int flag=settingService.updatesmtpdetails(schema,smtpID,mailStatus,smtpHost,smtpPort,smtpPassword,smtpUserID);
			if (flag == 1){
				logger.info("Mail details updated successfully");
				return new ModelAndView("redirect:/mail");
			}
			else{
				logger.error("mail details updation unsuccessful");
				return new ModelAndView("500");
			}
		} else
			return new ModelAndView("500");
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request updates mail notification for BM
	 * 
	 * @param model
	 * @param session
	 * @param mailNotificationId
	 * @param mailStatusCreateUser
	 * @param mailStatusAssignProject
	 * @param mailStatusAssignModule
	 * @param mailStatusAssignBuild
	 * @param mailStatusBuildExecComplete
	 * @return
	 */
	@RequestMapping(value = "/updateMailNotificationBm", method = RequestMethod.POST)
	public ModelAndView updateMailNotificationBm(Model model, HttpSession session,
			@RequestParam("mailNotificationId") String mailNotificationId,
			@RequestParam("mailStatusCreateUser") String mailStatusCreateUser,
			@RequestParam("mailStatusAssignProject") String mailStatusAssignProject,
			@RequestParam("mailStatusAssignModule") String mailStatusAssignModule,
			@RequestParam("mailStatusAssignBuild") String mailStatusAssignBuild,
			@RequestParam("mailStatusBuildExecComplete") String mailStatusBuildExecComplete) {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/");
		} else if (buildDAO.checkBMRoleAccess("mail",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			
			String schema = session.getAttribute("cust_schema").toString();
			int flag=settingService.updateMailNotificationBm(schema,mailNotificationId,mailStatusCreateUser,mailStatusAssignProject,mailStatusAssignModule,mailStatusAssignBuild,mailStatusBuildExecComplete);
			if (flag == 1)
			{
				logger.info("Mail notification details updated successfully");
				return new ModelAndView("redirect:/mail");
			}
			else
			{
				logger.info("Mail notification details updation unsuccessful");
				return new ModelAndView("500");
			}
		} else
			return new ModelAndView("500");
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request updates mail notification for BT
	 * 
	 * @param model
	 * @param session
	 * @param mailNotificationId
	 * @param mailStatusCreateBug
	 * @param mailStatusUpdateBug
	 * @return
	 */
	@RequestMapping(value = "/updateMailNotificationBt", method = RequestMethod.POST)
	public ModelAndView updateMailNotificationBt(Model model, HttpSession session,
			@RequestParam("mailNotificationId") String mailNotificationId,
			@RequestParam("mailStatusCreateBug") String mailStatusCreateBug,
			@RequestParam("mailStatusUpdateBug") String mailStatusUpdateBug) {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/");
		} else if (buildDAO.checkBMRoleAccess("mail",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			int flag=settingService.updateMailNotificationBt(schema,mailNotificationId,mailStatusCreateBug,mailStatusUpdateBug);
			if (flag == 1)
			{
				logger.info("Mail notification details updated successfully");
				return new ModelAndView("redirect:/mail");
			}
			else
			{
				logger.info("Mail notification details updation unsuccessful");
				return new ModelAndView("500");
			}
		} else
			return new ModelAndView("500");
	}
	
	/**
	 * @author prafulla.pol
	 * This method is used to check the smtp details 
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/testemailsettings", method = RequestMethod.GET)
	public @ResponseBody boolean testEmailSettings(Model model, HttpSession session){
		logger.info("Ajax Get request to check the email settings");
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		int userId = Integer.parseInt(session.getAttribute("userId").toString());
		
		//used to get the email settings data and login user E-mail details
		Map<String,Object> emailDetails = mailDAO.testEmailSettings(userId,schema);
		
		//used to send the test mail
		String status = mailDAO.sendTestMail(userId, emailDetails);
		
		if(status.equalsIgnoreCase("success")) {
			logger.info("Mail settings are valid. Returning status : "+status);
			return true;
		} else {
			logger.info("Mail settings are not valid. Returning status : "+status);
			return false;
		}		
	}
	
	@RequestMapping(value="/soapapihttpsettings")
	public ModelAndView soapapihttpsettings(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Settings");
			return new ModelAndView("soapapihttpsettings","Model",model);
		}
	}
	
	@RequestMapping(value="/soapapiproxysettings")
	public ModelAndView soapapiproxysettings(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Settings");
			return new ModelAndView("soapapiproxysettings","Model",model);
		}
	}
	
	@RequestMapping(value="/soapapisslsettings")
	public ModelAndView soapapisslsettings(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Settings");
			return new ModelAndView("soapapisslsettings","Model",model);
		}
	}
	
	@RequestMapping(value="/soapapiwsdlsettings")
	public ModelAndView soapapiwsdlsettings(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Settings");
			return new ModelAndView("soapapiwsdlsettings","Model",model);
		}
	}
}

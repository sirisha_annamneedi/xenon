package com.xenon.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import com.xenon.api.common.CommonService;
import com.xenon.api.common.JiraIntegrationService;
import com.xenon.api.common.UtilsService;
import com.xenon.api.testmanager.ScenarioService;
import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.testmanager.dao.ScenarioDAO;

import net.rcarz.jiraclient.Component;
import net.rcarz.jiraclient.Issue;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.Version;

/**
 * 
 * @author suresh.adling
 * @description Controller class with requests to do insert,update operations on
 *              scenario
 * 
 */
@Controller
public class ScenarioController {

	@Autowired
	ScenarioDAO scenarioDAO;

	@Autowired
	ProjectDAO projectDAO;

	@Autowired
	UtilsService utilsService;

	@Autowired
	CommonService commonService;

	@Autowired
	ScenarioService scenarioService;

	@Autowired
	ActivitiesDAO activitiesDAO;

	@Autowired
	JiraIntegrationService jiraIntegrationService;

	private static final Logger logger = LoggerFactory.getLogger(TestCaseController.class);

	/**
	 * 
	 * @author pawan.kumar
	 * @method insertBulkScenario POST Request
	 * @output addition of multiple scenarios
	 * @description This method accepts Excel Sheet and create scenario for
	 *              respective project-module
	 * @param model
	 * @param session
	 * @param moduleId
	 * @param moduleName
	 * @param scenarioName
	 * @param scenarioDescription
	 * @param status
	 * @return Model & View (onSuccess - testspecification ,onFailure -
	 *         logout/500)
	 * @throws Exception
	 */

	@RequestMapping(value = "/bulkinsertscenarios", method = RequestMethod.POST)
	public ModelAndView insertBulkScenarios(Model model, HttpSession session,
			@RequestParam(value = "uploadDatasheet", required = false) MultipartFile file,
			@RequestParam("moduleId") String moduleId, @RequestParam("moduleName") String moduleName,
			@RequestParam("status") String status) throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String project = session.getAttribute("UserCurrentProjectId").toString();
			String schema = session.getAttribute("cust_schema").toString();
			String projectName = session.getAttribute("UserCurrentProjectName").toString();
			logger.info("Session is active, userId is " + userId + " and " + " Customer Schema " + schema);
			int flag = scenarioService.bulkInsertScenario(project, moduleId, moduleName, projectName, file, userId,
					schema, status);
			if (flag == 1) {
				return new ModelAndView("redirect:/testspecification");
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return new ModelAndView("500");
			}
		}
	}

	/**
	 * 
	 * @author suresh.adling
	 * @method insertScenario POST Request
	 * @output addition of a scenario
	 * @description This method accepts required parameter and create scenario
	 *              for respective project-module
	 * @param model
	 * @param session
	 * @param moduleId
	 * @param moduleName
	 * @param scenarioName
	 * @param scenarioDescription
	 * @param status
	 * @return Model & View (onSuccess - testspecification ,onFailure -
	 *         logout/500)
	 * @throws Exception
	 */

	@RequestMapping(value = "/insertscenario", method = RequestMethod.POST, params = { "moduleId", "moduleName",
			"scenarioName", "scenarioDescription", "status" })
	public ModelAndView insertScenario(Model model, HttpSession session, @RequestParam("moduleId") String moduleId,
			@RequestParam("moduleName") String moduleName, @RequestParam("scenarioName") String scenarioName,
			@RequestParam("scenarioDescription") String scenarioDescription, @RequestParam("status") String status)
			throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			int flag = 0;
			utilsService.getLoggerUser(session);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			String projectName = session.getAttribute("UserCurrentProjectName").toString();
			logger.info("Session is active, userId is " + userId + " and " + " Customer Schema " + schema);
			flag = scenarioService.insertScenario(moduleId, scenarioName, scenarioDescription, status, projectName,
					moduleName, userId, session.getAttribute("cust_schema").toString());
			if (flag == 1) {
				session.setAttribute("insertedScenarioName", scenarioName);
				session.setAttribute("insertedScenarioStatus", 1);
				return new ModelAndView("redirect://testcase");
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return new ModelAndView("500");
			}
		}
	}

	@RequestMapping(value = "/insertscenario", method = RequestMethod.POST, params = { "moduleId", "moduleName",
			"scenarioName", "jiraStory", "jiraStoryAlt", "scenarioDescription", "status" })
	public ModelAndView insertScenario(Model model, HttpSession session, @RequestParam("moduleId") String moduleId,
			@RequestParam("moduleName") String moduleName, @RequestParam("scenarioName") String scenarioName,
			@RequestParam("jiraStory") String jiraStory, @RequestParam("jiraStoryAlt") String jiraStoryAlt,
			@RequestParam("scenarioDescription") String scenarioDescription, @RequestParam("status") String status)
			throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			int flag = 0;
			String jiraStoryId = null;
			String jiraStroryName = null;
			List<Component> componentList = null;
			List<Version> versionList = null;
			JiraClient jira = jiraIntegrationService.jiraConnect(session);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			String projectName = session.getAttribute("UserCurrentProjectName").toString();
			if (jiraStory.equals("default") && !jiraStoryAlt.equals("")) {
				if (jiraStoryAlt.contains(" ")) {
					jiraStoryId = jiraStoryAlt.split(" ", 2)[0];
					jiraStroryName = jiraStoryAlt.split(" ", 2)[1];
				} else {
					jiraStoryId = jiraStoryAlt.split(" ", 2)[0];
					jiraStroryName = null;
				}
			} else if (jiraStory.equals("default") && jiraStoryAlt.equals("")) {
				jiraStoryId = null;
				jiraStroryName = null;
			} else {
				jiraStoryId = jiraStory.split(" ", 2)[0];
				jiraStroryName = jiraStory.split(" ", 2)[1];
			}
			logger.info("Session is active, userId is " + userId + " and " + " Customer Schema " + schema);
			try {
				if (!(jiraStoryId == null)) {
					Issue issue = jira.getIssue(jiraStoryId);
					componentList = issue.getComponents();
					versionList = issue.getFixVersions();
				}
			} catch (Exception e) {
				session.setAttribute("insertedJiraId", jiraStoryId);
				session.setAttribute("insertedScenarioStatus", 3);
				return new ModelAndView("redirect:/testspecification");
			}
			flag = scenarioService.insertScenario(moduleId, scenarioName, jiraStoryId, jiraStroryName, versionList,
					scenarioDescription, status, projectName, moduleName, userId,
					session.getAttribute("cust_schema").toString());
			if (flag == 1) {
				if (!(componentList == null)) {
					scenarioService.insertComponent(componentList, scenarioName, moduleId, userId, schema);
				}
				session.setAttribute("insertedScenarioName", scenarioName);
				session.setAttribute("insertedScenarioStatus", 1);
				return new ModelAndView("redirect:/testspecification");
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return new ModelAndView("500");
			}
		}
	}

	/**
	 * 
	 * @author suresh.adling
	 * @method updateScenario POST Request
	 * @output modify scenario details
	 * @description This method accepts required parameter and and update
	 *              scenario details
	 * @param model
	 * @param session
	 * @param moduleName
	 * @param scenarioName
	 * @param scenarioDescription
	 * @param scenarioId
	 * @return Model & View (onSuccess - testspecification ,onFailure -
	 *         logout/500)
	 * @throws Exception
	 */

	@RequestMapping(value = "/updatescenario", method = RequestMethod.POST)
	public ModelAndView updateScenario(Model model, HttpSession session, @RequestParam("moduleName") String moduleName,
			@RequestParam("scenarioName") String scenarioName,
			@RequestParam("scenarioDescription") String scenarioDescription,
			@RequestParam("scenarioId") String scenarioId) throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			String projectName = session.getAttribute("UserCurrentProjectName").toString();
			logger.info("Session is active, userId is " + userId + " and " + " Customer Schema " + schema);
			int flag = scenarioService.updateScenario(scenarioName, scenarioDescription, scenarioId, projectName,
					moduleName, userId, session.getAttribute("cust_schema").toString());
			if (flag == 1) {
				session.setAttribute("insertedScenarioName", scenarioName);
				session.setAttribute("insertedScenarioStatus", 2);
				return new ModelAndView("redirect:/testcase");
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return new ModelAndView("500");
			}
		}
	}

	@RequestMapping(value = "/deletescenario", method = RequestMethod.POST)
	public ModelAndView updateScenarioStatus(Model model, HttpSession session,
			@RequestParam("scenarioStatus") Integer scenarioStatus, @RequestParam("scenarioId") Integer scenarioId)
			throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			// String projectName =
			// session.getAttribute("UserCurrentProjectName").toString();
			logger.info("Session is active, userId is " + userId + " and " + " Customer Schema " + schema);
			int flag = scenarioService.updateScenarioStatus(scenarioStatus, scenarioId, userId, schema);
			if (flag == 200 || flag == 404) {
				session.setAttribute("selectedTestModuleId", "0");
				return new ModelAndView("redirect:/testcase");
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return new ModelAndView("500");
			}
		}
	}

	// Anmol Chadha
	@RequestMapping(value = "/restoreScenario")
	public @ResponseBody ModelAndView restoreScenario(@RequestParam int moduleId, @RequestParam int scenarioId,
			HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");

		} else {
			utilsService.getLoggerUser(session);

			String schema = session.getAttribute("cust_schema").toString();

			scenarioDAO.restoreScenarioForTrash(moduleId, scenarioId, schema);

		}
		return new ModelAndView("redirect:/viewtmtrash");

	}

	@RequestMapping(value = "/updatescenario", method = RequestMethod.POST, params = { "moduleName", "moduleId",
			"scenarioName", "scenarioDescription", "scenarioId", "jiraStory" })
	public ModelAndView updateScenario(Model model, HttpSession session, @RequestParam("moduleName") String moduleName,
			@RequestParam("moduleId") String moduleId, @RequestParam("scenarioName") String scenarioName,
			@RequestParam("scenarioDescription") String scenarioDescription,
			@RequestParam("scenarioId") String scenarioId, @RequestParam("jiraStory") String jiraStory)
			throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			String jiraStoryId = null;
			String jiraStoryName = null;
			List<Component> componentList = null;
			JiraClient jira = jiraIntegrationService.jiraConnect(session);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			String projectName = session.getAttribute("UserCurrentProjectName").toString();
			jiraStory = jiraStory.trim();
			if (!jiraStory.equals("")) {
				if (jiraStory.contains(" ")) {
					jiraStoryId = jiraStory.split(" ", 2)[0];
					jiraStoryName = jiraStory.split(" ", 2)[1];
				} else {
					jiraStoryId = jiraStory.split(" ", 2)[0];
					jiraStoryName = null;
				}
			} else {
				jiraStoryId = null;
				jiraStoryName = null;
			}
			try {
				if (!(jiraStoryId == null)) {
					Issue issue = jira.getIssue(jiraStoryId);
					componentList = issue.getComponents();
					if (jiraStoryName == null)
						jiraStoryName = issue.getSummary();
				}
			} catch (Exception e) {
				session.setAttribute("insertedJiraId", jiraStoryId);
				session.setAttribute("insertedScenarioStatus", 3);
				return new ModelAndView("redirect:/testspecification");
			}
			logger.info("Session is active, userId is " + userId + " and " + " Customer Schema " + schema);
			int flag = scenarioService.updateScenario(scenarioName, jiraStoryId, jiraStoryName, scenarioDescription,
					scenarioId, projectName, moduleName, userId, session.getAttribute("cust_schema").toString());
			if (flag == 1) {
				if (!(componentList == null)) {
					scenarioService.insertComponent(componentList, scenarioName, moduleId, userId, schema);
				}
				session.setAttribute("insertedScenarioName", scenarioName);
				session.setAttribute("insertedScenarioStatus", 2);
				return new ModelAndView("redirect:/testspecification");
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return new ModelAndView("500");
			}
		}
	}

	/**
	 * @author suresh.adling
	 * @method setScenarioId POST Request
	 * @output Set session attribute scenario id of selected scenario
	 * @description Post method to set scenario id in session to do edit,update
	 *              operations on scenario
	 * @param scenarioId
	 * @param scenarioName
	 * @param moduleId
	 * @param moduleName
	 * @param model
	 * @param session
	 * @return boolean value
	 * @throws Exception
	 */
	@RequestMapping(value = "/setscenarioid", method = RequestMethod.POST)
	public @ResponseBody boolean setScenarioId(@RequestParam("scenarioId") String scenarioId,
			@RequestParam("scenarioName") String scenarioName, @RequestParam("moduleId") String moduleId,
			@RequestParam("moduleName") String moduleName, Model model, HttpSession session) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("selectedScenarioId", scenarioId);
		session.setAttribute("selectedScenarioName", scenarioName);
		session.setAttribute("selectedTestModuleId", moduleId);
		session.setAttribute("selectedTestModuleName", moduleName);
		return true;
	}

	/**
	 * @author suresh.adling
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :" + ex);
		return new ModelAndView("redirect:/500");
	}

	@RequestMapping(value = "/removescenario", method = RequestMethod.POST)
	public @ResponseBody boolean removeScenario(Model model, HttpSession session,
			@RequestParam("scenarioId") int scenarioId) {
		logger.info("Ajax Post request to delete scenario");
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		int flag = scenarioService.deleteScenario(scenarioId, schema);
		if (flag == 200) {
			return true;
		} else {
			return false;
		}
	}
}
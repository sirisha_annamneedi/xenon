package com.xenon.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.administration.ProjectService;
import com.xenon.api.common.BrowserInformationService;
import com.xenon.api.common.CommonService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;

/**
 * Handles requests for the application home page.
 */
@Controller
public class XenonController {

	@Autowired
	BuildDAO buildDAO;

	@Autowired
	RoleAccessService roleAccessService;

	@Autowired
	LoginService loginService;

	@Autowired
	ProjectService projectService;

	@Autowired
	CommonService commonService;

	@Autowired
	UtilsService utilsService;

	@Autowired
	BrowserInformationService browserInformationService;

	private static final Logger logger = LoggerFactory.getLogger(XenonController.class); // logger

	/**
	 * @author bhagyashri.ajmera This request returns view for super admin
	 *         dashboard
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/spadmindashboard")
	public ModelAndView superadminDashboardpage(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/");
		}
		if (Integer.parseInt(session.getAttribute("roleId").toString()) != 1)
			return new ModelAndView("redirect:/500");

		else {
			utilsService.getLoggerUser(session);
			logger.info("Adding attributes to model");
			model.addAttribute("customerLogo", session.getAttribute("customerLogo"));
			model.addAttribute("userPhoto", session.getAttribute("userPhoto"));
			model.addAttribute("tmStatus", session.getAttribute("tmStatus"));
			model.addAttribute("btStatus", session.getAttribute("btStatus"));
			model.addAttribute("coreStatus", 1);
			model.addAttribute("adminStatus", 1);
			List<Map<String, Object>> projectList = new ArrayList<Map<String, Object>>();
			logger.info("reading project details");
			projectList = projectService.getcurrentAssignedProject(
					Integer.parseInt(session.getAttribute("userId").toString()),
					session.getAttribute("cust_schema").toString());

			if (projectList.size() == 0) {
				projectList = projectService.getProjectAssignedUser(
						Integer.parseInt(session.getAttribute("userId").toString()),
						session.getAttribute("cust_schema").toString());
				if (projectList.size() != 0) {
					projectList.get(0).put("AssignProject", "True");
				} else {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("AssignProject", "False");

					projectList.add(map);

				}

			} else {
				session.setAttribute("UserCurrentProjectName", projectList.get(0).get("project_name").toString());
				session.setAttribute("UserCurrentProjectId", projectList.get(0).get("project_id").toString());
				// projectList.get(0).put("AssignProject", "True");
			}

			model.addAttribute("projectList", projectList);
			return new ModelAndView("superadmindashboard", "Model", model);

		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request returns view for xenon dashboard
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/xedashboard")
	public ModelAndView xedashboard(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/");
		} else {

			if (Integer.parseInt(session.getAttribute("roleId").toString()) == 1) {
				logger.info("Return superadmin dashboard");
				return new ModelAndView("redirect:/spadmindashboard");
			} else {
				utilsService.getLoggerUser(session);
				logger.info("Add attributes to model");
				model.addAttribute("customerLogo", session.getAttribute("customerLogo"));
				model.addAttribute("userPhoto", session.getAttribute("userPhoto"));
				model.addAttribute("tmStatus", session.getAttribute("tmStatus"));
				model.addAttribute("btStatus", session.getAttribute("btStatus"));
				model.addAttribute("coreStatus", 2);
				if (Integer.parseInt(session.getAttribute("roleId").toString()) == 2) {
					model.addAttribute("adminStatus", 1);
				} else {
					model.addAttribute("adminStatus", 2);
				}
				logger.info("read current project details");
				List<Map<String, Object>> projectList = new ArrayList<Map<String, Object>>();

				List<Map<String, Object>> projectListSession = projectService.getProjectAssignedUser(
						Integer.parseInt(session.getAttribute("userId").toString()),
						session.getAttribute("cust_schema").toString());
				session.setAttribute("projectListSession", projectListSession);
				session.setAttribute("projectListSessionJSON", utilsService.convertListOfMapToJson(projectListSession));
				int buildId = 0, releaseId = 0;
				String buildName = "";
				int buildStatus = 0;
				int userId = Integer.parseInt(session.getAttribute("userId").toString());
				if (session.getAttribute("automationExecBuildId") == null) {
					List<Map<String, Object>> lastAutoBuildDetails = buildDAO.getlastAutoBuildDetails(userId,
							session.getAttribute("cust_schema").toString());
					for (int i = 0; i < lastAutoBuildDetails.size(); i++) {
						buildId = (Integer) lastAutoBuildDetails.get(i).get("build_id");
						buildName = (String) lastAutoBuildDetails.get(i).get("build_name");
						buildStatus = (Integer) lastAutoBuildDetails.get(i).get("build_status");
						releaseId = (Integer) lastAutoBuildDetails.get(i).get("release_id");
					}
					session.setAttribute("automationExecBuildId", buildId);
					session.setAttribute("automationExecReleaseId", releaseId);
					session.setAttribute("selectedAutoReportBuildId", buildId);
					session.setAttribute("BuildId", buildId);
					session.setAttribute("automationExecBuildName", buildName);
					session.setAttribute("UsertestRemoveStatus", buildStatus);
					session.setAttribute("selectedExecuteBuildName", buildName);
				} else {
					buildId = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());
				}

				projectList = projectService.getcurrentAssignedProject(
						Integer.parseInt(session.getAttribute("userId").toString()),
						session.getAttribute("cust_schema").toString());

				if (projectList.size() == 0) {
					projectList = projectListSession;
					if (projectList.size() != 0) {
						projectList.get(0).put("AssignProject", "True");
					} else {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("AssignProject", "False");

						projectList.add(map);

					}

				} else {
					session.setAttribute("UserCurrentProjectName", projectList.get(0).get("project_name").toString());
					session.setAttribute("UserCurrentProjectId", projectList.get(0).get("project_id").toString());
					// projectList.get(0).put("AssignProject", "True");
				}

				model.addAttribute("projectList", projectList);
				model.addAttribute("scriptlessStatus",
						Integer.parseInt(session.getAttribute("custScriptlessStatus").toString()));
				model.addAttribute("userRole", Integer.parseInt(session.getAttribute("roleId").toString()));
				logger.info("Return to xenon dashboard");
				return new ModelAndView("xedashboard", "Model", model);
			}

		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method authenticates user by user name and password
	 * 
	 * @param userName
	 * @param password
	 * @param session
	 * @param request
	 * @param device
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/authenticateUserRequest")
	public @ResponseBody ModelAndView authenticateUserRequest(@RequestParam("userName") String userName, Model model,
			@RequestParam("password") String password, HttpSession session, HttpServletRequest request, Device device)
			throws Exception {

		browserInformationService.getUserRequestDetails(request, device, session);

		String result = loginService.authenticateUser(userName.toLowerCase(), password, session);
		if (!result.isEmpty()) {
			MDC.remove("user");
			/*
			 * String challenge =
			 * request.getParameter("recaptcha_challenge_field"); String
			 * response = request.getParameter("recaptcha_response_field");
			 * String remoteAddr = request.getRemoteAddr();
			 * 
			 * ReCaptchaResponse reCaptchaResponse =
			 * reCaptchaService.checkAnswer(remoteAddr, challenge, response);
			 * if(reCaptchaResponse.isValid()) {
			 */
			session.setAttribute("userFileName", commonService.getFileName(session));
			MDC.put("user", session.getAttribute("userFileName").toString());
			MDC.put("details", commonService.getLogDetails(session));
			logger.info("Log message at INFO level from TestModel constructor");
			logger.info("Message at INFO level from TestService.service()");
			logger.warn("Message at WARN level from TestService.service()");
			return new ModelAndView("redirect:/" + result);
			/*
			 * } else { session.invalidate(); model.addAttribute("message",
			 * "Invalid Captcha, Please try again"); return new
			 * ModelAndView("login","Model",model); }
			 */
		} else {
			return new ModelAndView("redirect:/");
		}
	}

	@RequestMapping(value = { "/", "/login" }, method = RequestMethod.GET)
	public String show() {
		return "login";
	}

	/**
	 * 
	 * This inserts current project of user
	 * 
	 * @param model
	 * @param session
	 * @param prjId
	 * @param prjName
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertCurrentProject")
	public ModelAndView insertCurrentProject(Model model, HttpSession session, @RequestParam("projectId") String prjId,
			@RequestParam("projectName") String prjName) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			String SchemeName = session.getAttribute("cust_schema").toString();
			int projectId = Integer.parseInt(prjId);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());

			int flag = projectService.setCurrentProject(userId, projectId, SchemeName);

			if (flag == 201) {
				logger.info("Project inserted successfully");
				session.setAttribute("UserCurrentProjectName", prjName);
				session.setAttribute("UserCurrentProjectId", prjId);
			}

			return xedashboard(model, session);
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request handles logout request
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/logout")
	public ModelAndView logout(Model model, HttpSession session) throws Exception {
		// Utils.getLoggerUser(session);
		try {
			if (Integer.parseInt(session.getAttribute("logoutFlag").toString()) == -1)
				model.addAttribute("LogoutMessage", "Invalid username or password");
			else if (Integer.parseInt(session.getAttribute("logoutFlag").toString()) == 0)
				model.addAttribute("LogoutMessage", "Invalid user");
			else
				session.setAttribute("userId", null);
				model.addAttribute("LogoutMessage", "Logged out successfully");
			logger.info("Invalidating session");
			session.invalidate();
			logger.info("Returning login page");
			return new ModelAndView("Logout", "Model", model);
		} catch (Exception e) {
			return new ModelAndView("redirect:/");
		}

	}

	/**
	 * This request handles all exceptions in this controller
	 * 
	 * @param ex
	 * @param response
	 * @param session
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		return new ModelAndView("redirect:/500");
	}

	public String getImage(byte[] image) throws UnsupportedEncodingException {
		logger.info("Reading image as string");
		if (image == null)
			return "";
		else {
			byte[] encodeBase64 = Base64.encodeBase64(image);
			return new String(encodeBase64, "UTF-8");
		}
	}

}

package com.xenon.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xenon.api.common.UtilsService;

@RestController
@RequestMapping("/service")
public class XenonRestController {
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(XenonRestController.class);
	
	@Autowired
	UtilsService utilsService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getGreeting(@RequestParam("userName") String userName, @RequestParam("password") String password,
			HttpSession session) {
		utilsService.getLoggerUser(session);
		String result = "Hello " + userName + "---" + password;
		return result;
	}
}

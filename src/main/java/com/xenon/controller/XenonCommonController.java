package com.xenon.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.common.dao.UserDetailsDAO;
/**
 * 
 * @author suresh.adling
 * @description Handles requests for the errors,common features
 * 
 */
@Controller
public class XenonCommonController {
	
	private static final Logger logger = LoggerFactory.getLogger(XenonCommonController.class);
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	UserDetailsDAO userDao;
	
	/**
	 * @author suresh.adling
	 * @description get request for 404 (page not found)
	 * @param model
	 * @param session
	 * @return view- 404 
	 * @throws Exception
	 */
	@RequestMapping(value = "/404")
	public ModelAndView error404(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			logger.info(" User is redirected to 404 error  page ");
		return new ModelAndView("404");
		}
	}
	
	/**
	 * @author navnath.damale
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/405")
	public ModelAndView error405(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			logger.info(" User is redirected to 405 error  page ");
		return new ModelAndView("405");
		}
	}
	/**
	 * @author suresh.adling
	 * @description get request for 404 (internal server error,something went wrong) 
	 * @param model
	 * @param session
	 * @return view - 500
	 * @throws Exception
	 */
	@RequestMapping(value = "/500")
	public ModelAndView error500(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			logger.info(" User is redirected to 500 error  page ");
		return new ModelAndView("500");
		}
	}
	
	/**
	 * @author suresh.adling
	 * @description get request for trialerror (when user has exhausted the limit) 
	 * @param model
	 * @param session
	 * @return view - trialerror
	 * @throws Exception
	 */
	@RequestMapping(value = "/trialerror")
	public ModelAndView trialerror(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			logger.info(" User is redirected trialerror  page ");
		return new ModelAndView("trialerror");
		}
	}
	/**
	 * @author suresh.adling
	 * @method reset password after log in first time  
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/password")
	public ModelAndView password(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			String userId=session.getAttribute("userId").toString();
			model.addAttribute("userId", userId);
			logger.info(" User is redirected to password  page ");
			return new ModelAndView("password","Model",model);
		}
	}
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives user image by user id
	 * 
	 * @param model
	 * @param session
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getUserImageById")
	public @ResponseBody Object getUserImageById(Model model, HttpSession session, @RequestParam("userId") String userId) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return "";
		} else {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			List<Map<String, Object>> userImage=userDao.getUserImageById(Integer.parseInt(userId), schema);
			String image = loginService.getImage((byte[]) userImage.get(0).get("user_photo"));
			return image;
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives memory utilization details
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/memory")
	public @ResponseBody String memory(Model model, HttpSession session) throws Exception {
		Runtime r = Runtime.getRuntime();
		JSONObject obj = new JSONObject();
		obj.put("TotalMemory", r.totalMemory());
		obj.put("FreeMemory", r.freeMemory());
		obj.put("MaxMemory", r.maxMemory());
		return obj.toString();
	}
	
	/** Common Ajax Method To Set Session Attributes From JavaScript To Server Session
	 * 
	 * Map Key   :- Session Attribute Name Nothing but Parameter Name
	 * Map Value :- Session Value Name Nothing but Parameter Value
	 * 
	 * @author shantaram.tupe
	 * 23-Oct-2019:5:01:44 PM
	 * @param sessionAttributesMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setSessionAttributes")
	public @ResponseBody Boolean setSessionAttributes(@RequestParam Map<String, Object> sessionAttributesMap, HttpSession session) throws Exception {
		try {
			for ( Map.Entry<String, Object> entry : sessionAttributesMap.entrySet() )
				session.setAttribute( entry.getKey(), entry.getValue() );
			return true;
		} catch( Exception exception ) {
			return false;
		}
	}
	
	/**
	 * @author suresh.adling
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :"+ex);
		return new ModelAndView("redirect:/500");
	}
}

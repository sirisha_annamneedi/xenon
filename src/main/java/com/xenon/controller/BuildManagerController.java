package com.xenon.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.buildmanager.BuildDashboardService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.NotificationDAO;
import com.xenon.buildmanager.dao.ReleaseDAO;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.core.dao.CustomerDAO;
import com.xenon.core.dao.CustomerTypeDAO;

/**
 * 
 * @author suresh.adling
 * @class Build Manager Controller
 * @description Controller includes requests to do view for build manager, build
 *              manager link data,menu,setting session variable and account
 *              setting
 */

@SuppressWarnings("unchecked")
@Component
@Controller
public class BuildManagerController {

	@Resource(name = "colorProperties")
	private Properties colorProperties;
	@Autowired
	CustomerTypeDAO custTypeDao;

	@Autowired
	CustomerDAO custDetailsDAO;

	@Autowired
	UserDAO userDao;

	@Autowired
	BuildDAO buildDAO;

	@Autowired
	ActivitiesDAO activitiesDAO;

	@Autowired
	NotificationDAO notificationDAO;

	@Autowired
	LoginService loginService;

	@Autowired
	UtilsService utilsService;

	@Autowired
	ReleaseDAO releaseDAO;

	@Autowired
	BuildDashboardService buildDashboardService;

	@Autowired
	RoleAccessService roleAccessService;

	private static final Logger logger = LoggerFactory.getLogger(BuildManagerController.class);

	/**
	 * @author suresh.adling
	 * @method bmdashboard GET Request
	 * @output get bmdashboard view
	 * @param Model
	 *            model,HttpSession session
	 * @description This method redirects user to build manager view with the
	 *              model
	 * @return Model (Count,GraphData,topBuilds,activities) & View (onSuccess -
	 *         buildmanager ,onFailure - logout/resource not found/404)
	 */

	@SuppressWarnings({ "rawtypes" })
	@RequestMapping(value = "/bmdashboard")
	public ModelAndView bmdashboard(Model model, HttpSession session) throws Exception {

		logger.info("Redirecting to bmdashboard page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {

			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");

			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String userTimeZone = session.getAttribute("userTimezone").toString();
			int autoStatus = Integer.parseInt(session.getAttribute("automationStatus").toString());
			Object testcaseBuildName = session.getAttribute("addremovetestcaseBuildName");
			Map<String, Object> returnModel = buildDashboardService.getDashboardData(schema, userId, userTimeZone,
					autoStatus, testcaseBuildName, buildDAO, activitiesDAO, colorProperties);

			logger.info(" User is redirected buildmanager  page ");

			Iterator it = returnModel.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				model.addAttribute((String) pair.getKey(), pair.getValue());
				it.remove();
			}
			List<Map<String, Object>> releaseDetails = releaseDAO
					.getAllReleaseDetails(session.getAttribute("cust_schema").toString());
			List<Map<String, Object>> releaseTCCount = releaseDAO
					.getReleaseTCCount(session.getAttribute("cust_schema").toString());
			List<Map<String, Object>> totalReleaseTestData = buildDAO.getTotalReleaseTestData(userId, schema);
			model.addAttribute("lastRelease", totalReleaseTestData.get(0).get("release_id"));
			// List<Map<String, Object>> releaseTestDataManual
			// =buildDAO.getReleaseTestDataCountManual(userId, schema);
			// List<Map<String, Object>> releaseTestDataAutomation
			// =buildDAO.getReleaseTestDataCountAutomation(userId, schema);
			// List<Map<String, Object>> totalData=
			// releaseTestDataManual.add(arg0)
			model.addAttribute("releaseDetails", releaseDetails);
			model.addAttribute("releaseTCCount", releaseTCCount);
			model.addAttribute("releaseTestDataCountManual", buildDAO.getReleaseTestDataCountManual(userId, schema));
			model.addAttribute("releaseTestDataCountAutomation",
					buildDAO.getReleaseTestDataCountAutomation(userId, schema));
			model.addAttribute("totalReleaseTestData", totalReleaseTestData);
			model.addAttribute("totalReleaseTestData1", utilsService.convertListOfMapToJson(totalReleaseTestData));
			return new ModelAndView("buildmanager", "Model", model);
		} else {
			logger.error("User looking for resources is not available or prohibited");
			return new ModelAndView("redirect:/404");
		}

	}

	/**
	 * @author suresh.adling
	 * @method buildmanagerlink GET Request
	 * @output get buildmanagerlink view
	 * @param Model
	 *            model,HttpSession session
	 * @description This method redirects user to build manager link data view
	 *              with the model
	 * @return Model (Count,GraphData,topBuilds,activities) & View (onSuccess -
	 *         buildmanager ,onFailure - logout/resource not found/404)
	 */

	@RequestMapping(value = "/buildmanagerlink")
	public ModelAndView buildmanagerlink(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to bmdashboardlink page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {

			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");
			String schema = session.getAttribute("cust_schema").toString();
			String filterText = session.getAttribute("filterTextBM").toString();
			logger.info(" Session is active,  Customer Schema " + schema);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			Map<String, Object> buildDashboard = buildDAO.getBuildDashboardLinkData(userId, schema);

			List<Map<String, Object>> activeRelease = (List<Map<String, Object>>) buildDashboard.get("#result-set-1");
			List<Map<String, Object>> activeBuild = (List<Map<String, Object>>) buildDashboard.get("#result-set-2");
			List<Map<String, Object>> activeBug = (List<Map<String, Object>>) buildDashboard.get("#result-set-3");
			List<Map<String, Object>> testCases = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> testCasesData1 = (List<Map<String, Object>>) buildDashboard.get("#result-set-5");
			List<Map<String, Object>> testCasesData2 = (List<Map<String, Object>>) buildDashboard.get("#result-set-6");

			List<Map<String, Object>> BRid = (List<Map<String, Object>>) buildDashboard.get("#result-set-7");

			for (int i = 0; i < activeRelease.size(); i++) {
				activeRelease.get(i).put("release_start_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(activeRelease.get(i).get("release_start_date").toString())));
			}
			for (int i = 0; i < activeBuild.size(); i++) {
				activeBuild.get(i).put("build_createdate",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(activeBuild.get(i).get("build_createdate").toString())));
			}
			for (int i = 0; i < activeBug.size(); i++) {
				activeBug.get(i).put("create_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(activeBug.get(i).get("create_date").toString())));
			}

			for (int count1 = 0; count1 < testCasesData1.size(); count1++) {
				boolean foundflag = false;
				for (int count2 = 0; count2 < testCasesData2.size(); count2++) {
					int buildIdVal = Integer.parseInt(testCasesData1.get(count1).get("build_id").toString());
					int projectIdVal = Integer.parseInt(testCasesData1.get(count1).get("project_id").toString());
					int moduleIdVal = Integer.parseInt(testCasesData1.get(count1).get("module_id").toString());
					int scenarioIdVal = Integer.parseInt(testCasesData1.get(count1).get("scenario_id").toString());
					int tcIdVal = Integer.parseInt(testCasesData1.get(count1).get("testcase_id").toString());
					if (Integer.parseInt(testCasesData2.get(count2).get("build_id").toString()) == buildIdVal
							&& Integer.parseInt(testCasesData2.get(count2).get("project_id").toString()) == projectIdVal
							&& Integer.parseInt(testCasesData2.get(count2).get("mudule_id").toString()) == moduleIdVal
							&& Integer
									.parseInt(testCasesData2.get(count2).get("scenario_id").toString()) == scenarioIdVal
							&& Integer.parseInt(testCasesData2.get(count2).get("tc_id").toString()) == tcIdVal) {
						foundflag = true;
						break;
					}
				}
				if (!foundflag) {
					testCases.add(testCasesData1.get(count1));
				}
			}

			for (int i = 0; i < testCases.size(); i++) {
				testCases.get(i).put("created_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(testCases.get(i).get("created_date").toString())));
			}
			model.addAttribute("activeRelease", activeRelease);
			model.addAttribute("activeBuild", activeBuild);
			model.addAttribute("activeBug", activeBug);
			model.addAttribute("testCases", testCases);
			model.addAttribute("filterText", filterText);
			model.addAttribute("topActivities", activitiesDAO.getTopActivities(schema));
			model.addAttribute("Pass", colorProperties.getProperty("testCase.Pass"));
			model.addAttribute("Fail", colorProperties.getProperty("testCase.Fail"));
			model.addAttribute("Skip", colorProperties.getProperty("testCase.Skip"));
			model.addAttribute("NotRun", colorProperties.getProperty("testCase.NotRun"));

			model.addAttribute("BRid", BRid);

			logger.info(" User is redirected buildmanagerlink  page ");
			return new ModelAndView("buildmanagerlink", "Model", model);
		} else {
			logger.warn("User looking for resources is not available or prohibited");
			return new ModelAndView("redirect:/404");
		}

	}

	/**
	 * @author navnath.damale
	 * @param model
	 * @param session
	 * @param filterText
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setBMLinkSession")
	public ModelAndView setBMLinkSession(Model model, HttpSession session,
			@RequestParam("filterText") String filterText) throws Exception {
		logger.info("Setting selecte choice for redirection to Build manager data page");
		logger.info("checking for active session");

		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirecting to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("filterTextBM", filterText);
			return new ModelAndView("redirect:/");
		}
	}

	/**
	 * @author navnath.damale
	 * @method bmmenu GET Request
	 * @output get bmmenu view
	 * @param Model
	 *            model,HttpSession session
	 * @description This method checks all user access for the build manager
	 *              components.
	 * 
	 * @return Model (model) & View (onSuccess - bmmenu )
	 */
	@RequestMapping(value = "/bmmenu")
	public ModelAndView bmmenu(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			boolean createStatus = buildDAO.checkBMRoleAccess("createmodule", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString());
			boolean viewStatus = buildDAO.checkBMRoleAccess("viewmodule", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString());
			if (createStatus || viewStatus) {
				model.addAttribute("moduleAccess", 1);
				if (createStatus) {
					model.addAttribute("createModule", 1);
				}
				if (viewStatus) {
					model.addAttribute("viewModules", 1);
				}
			}

			createStatus = buildDAO.checkBMRoleAccess("createrelease", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString());
			viewStatus = buildDAO.checkBMRoleAccess("viewrelease", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString());
			if (createStatus || viewStatus) {
				model.addAttribute("releaseAccess", 1);
				if (createStatus) {
					model.addAttribute("createRelease", 1);
				}
				if (viewStatus) {
					model.addAttribute("viewRelease", 1);
				}
			}

			createStatus = buildDAO.checkBMRoleAccess("mail", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString());
			viewStatus = buildDAO.checkBMRoleAccess("accountsettings", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString());
			boolean viewVMStatus = buildDAO.checkBMRoleAccess("viewvm", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString());

			if (createStatus || viewStatus || viewVMStatus) {
				model.addAttribute("mail", 1);
				if (createStatus) {
					model.addAttribute("mail", 1);
				}
				if (viewStatus) {
					model.addAttribute("accountsettings", 1);
				}

				if (viewVMStatus) {
					model.addAttribute("viewvm", 1);
				}
			}

			createStatus = roleAccessService.checkBMAccess("createbuild", session);
			viewStatus = roleAccessService.checkBMAccess("viewbuild", session);
			boolean addRemoveStatus = roleAccessService.checkBMAccess("addremovetestcase", session);
			if (createStatus || viewStatus || addRemoveStatus) {
				model.addAttribute("buildAccess", 1);
				if (createStatus) {
					model.addAttribute("createbuild", 1);
				}
				if (viewStatus) {
					model.addAttribute("viewbuild", 1);
				}
				if (addRemoveStatus) {
					model.addAttribute("addRemoveStatus", 1);
				}
			}

			viewStatus = buildDAO.checkBMRoleAccess("autoexe_build", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString());
			if (viewStatus) {
				model.addAttribute("autoexe_build", 1);
			}
			model.addAttribute("automationStatus",
					Integer.parseInt(session.getAttribute("automationStatus").toString()));
			model.addAttribute("customerOnPremiseStatus",
					Integer.parseInt(session.getAttribute("customerOnPremiseStatus").toString()));
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			return new ModelAndView("bmmenu", "model", model);
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request is used to read header data
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/bmheader")
	public ModelAndView bmheader(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active,  Customer Schema " + schema);
			Map<String, Object> buildDashboard = notificationDAO
					.getNotifications(Integer.parseInt(session.getAttribute("userId").toString()), schema); // buildDAO.headerData(schema);
			List<Map<String, Object>> allBuildsTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-1");
			List<Map<String, Object>> allBuildsExecutedTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-2");

			List<Map<String, Object>> allBuilds = (List<Map<String, Object>>) buildDashboard.get("#result-set-5");

			List<Map<String, Object>> topBuilds = buildDAO.getBmDashboardTopBuildsNf(allBuilds, allBuildsTestcasesCount,
					allBuildsExecutedTestcasesCount, schema);
			for (int i = 0; i < topBuilds.size(); i++) {
				topBuilds.get(i).put("user_image",
						utilsService.getFileData((byte[]) topBuilds.get(i).get("user_photo")));
				topBuilds.get(i).put("build_createdate",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(topBuilds.get(i).get("build_createdate").toString())));
			}

			List<Map<String, Object>> notifications = (List<Map<String, Object>>) buildDashboard.get("#result-set-3");
			for (int i = 0; i < notifications.size(); i++) {
				notifications.get(i).put("user_image",
						utilsService.getFileData((byte[]) notifications.get(i).get("user_photo")));
				String dateInString = notifications.get(i).get("notification_date").toString();
				notifications.get(i).put("notification_date", utilsService
						.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), dateInString));
			}
			List<Map<String, Object>> unreadCount = (List<Map<String, Object>>) buildDashboard.get("#result-set-4");
			model.addAttribute("unreadCount", Integer.parseInt(unreadCount.get(0).get("unreadCount").toString()));
			model.addAttribute("nowDate",
					utilsService.getNowDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("prevDate",
					utilsService.getPrevDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("notifications", notifications);
			model.addAttribute("topBuilds", topBuilds);
			return new ModelAndView("bmheader", "Model", model);
		}
	}

	/**
	 * @author prafulla.pol This method is used to get recent manual builds
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/recentmanualbuilds", method = RequestMethod.GET)
	public @ResponseBody List<Map<String, Object>> recentManualBuilds(Model model, HttpSession session)
			throws Exception {
		String schema = session.getAttribute("cust_schema").toString();

		Map<String, Object> data = buildDAO.recentManualBuilds(schema);

		List<Map<String, Object>> builds = (List<Map<String, Object>>) data.get("#result-set-1");
		List<Map<String, Object>> totalTestCasesInBuilds = (List<Map<String, Object>>) data.get("#result-set-2");
		List<Map<String, Object>> executedTestCasesInBuilds = (List<Map<String, Object>>) data.get("#result-set-3");

		// add the builds execution percentage
		List<Map<String, Object>> manualBuildData = buildDAO.getTopRecentBuilds(builds, totalTestCasesInBuilds,
				executedTestCasesInBuilds);

		// convert the date
		for (int i = 0; i < manualBuildData.size(); i++) {
			manualBuildData.get(i).put("build_createdate",
					utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
							utilsService.getDate(manualBuildData.get(i).get("build_createdate").toString())));
		}

		return manualBuildData;
	}

	/**
	 * @author prafulla.pol This method is used to get recent manual builds
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/recentautobuilds", method = RequestMethod.GET)
	public @ResponseBody List<Map<String, Object>> recentAutomationBuilds(Model model, HttpSession session)
			throws Exception {
		String schema = session.getAttribute("cust_schema").toString();

		Map<String, Object> data = buildDAO.recentAutomationBuilds(schema);

		List<Map<String, Object>> builds = (List<Map<String, Object>>) data.get("#result-set-1");
		List<Map<String, Object>> totalTestCasesInBuilds = (List<Map<String, Object>>) data.get("#result-set-2");
		List<Map<String, Object>> executedTestCasesInBuilds = (List<Map<String, Object>>) data.get("#result-set-3");

		// add the builds execution percentage
		List<Map<String, Object>> automationBuildData = buildDAO.getTopRecentBuilds(builds, totalTestCasesInBuilds,
				executedTestCasesInBuilds);

		// convert the date
		for (int i = 0; i < automationBuildData.size(); i++) {
			automationBuildData.get(i).put("build_createdate",
					utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
							utilsService.getDate(automationBuildData.get(i).get("build_createdate").toString())));
		}

		return automationBuildData;
	}

	/**
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.getLoggerUser(session);
		utilsService.sendMailonExecption(ex, session);
		return new ModelAndView("redirect:/500");
	}
}

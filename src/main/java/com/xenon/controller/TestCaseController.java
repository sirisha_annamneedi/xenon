package com.xenon.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.common.CommonService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.api.testmanager.TestCaseService;
import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.buildmanager.dao.NotificationDAO;
import com.xenon.testmanager.dao.TestSpecificationDAO;
import com.xenon.testmanager.dao.TestcaseCommentsDAO;
import com.xenon.testmanager.dao.TestcaseDAO;
import com.xenon.testmanager.domain.TcCommentLike;

/**
 * 
 * @author suresh.adling
 * @description Controller class with requests to do insert,update operations on
 *              a test case
 * 
 */
@Controller
public class TestCaseController {

	@Autowired
	TestcaseCommentsDAO testcaseCommentsDAO;

	@Autowired
	TestCaseService testCaseService;

	@Autowired
	ActivitiesDAO activitiesDAO;

	@Autowired
	NotificationDAO notificationDAO;

	@Autowired
	TestcaseDAO testcaseDAO;

	@Autowired
	LoginService loginService;

	@Autowired
	CommonService commonService;

	@Autowired
	UtilsService utilsService;

	@Autowired
	TestSpecificationDAO testSpecificationDao;

	private static final Logger logger = LoggerFactory.getLogger(TestCaseController.class);

	/**
	 * @author suresh.adling
	 * @method insertTestCase POST Request
	 * @output addition of a test case
	 * @description This method accepts required parameter and create test case
	 *              for respective project-module-scenario
	 * @param model
	 * @param session
	 * @param moduleId
	 * @param scenarioId
	 * @param scenarioName
	 * @param testcaseName
	 * @param testcaseSummary
	 * @param testcasePrecondition
	 * @param executionType
	 * @param executionTime
	 * @param statusId
	 * @return Model & View (onSuccess - testcase ,onFailure - logout/500)
	 * @throws Exception
	 */
	@RequestMapping(value = "/inserttestcase", method = RequestMethod.POST)
	public ModelAndView insertTestCase(Model model, HttpSession session,
			@RequestParam(value = "uploadDatasheet", required = false) MultipartFile file,
			@RequestParam("moduleId") String moduleId, @RequestParam("scenarioId") String scenarioId,
			@RequestParam("scenarioName") String scenarioName, @RequestParam("testcaseName") String testcaseName,
			@RequestParam("testcaseSummary") String testcaseSummary,
			@RequestParam(required = false) String redwoodTestCaseId,
			@RequestParam("activeModuleName") String activeModuleName,
			@RequestParam("testcasePrecondition") String testcasePrecondition,
			@RequestParam("executionType") String executionType, @RequestParam("executionTime") String executionTime,
			@RequestParam("statusId") String statusId, @RequestParam("datasheetStatus") String datasheetStatus)
			throws Exception {
		// System.out.println("moduleId:"+moduleId+"\n"+"scenarioId:"+scenarioId+"\n"+"testcaseName:"+testcaseName+"\n"
		// +"testcaseSummary:"+testcaseSummary+"\n"+"testcasePrecondition:"+testcasePrecondition+"\n"
		// +"executionType:"+executionType+"\n"+"executionTime:"+executionTime+"\n");
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			String[] inputStrings = { testcaseName };
			JSONObject plainText = utilsService.convertToPlainText(inputStrings);
			testcaseName = plainText.get(testcaseName).toString();

			int authorId = Integer.parseInt(session.getAttribute("userId").toString());
			String project = session.getAttribute("UserCurrentProjectId").toString();
			String schema = session.getAttribute("cust_schema").toString();
			logger.info("Session is active, userId is " + authorId + " and projectId is " + project
					+ " Customer Schema " + schema);
			String projectName = session.getAttribute("UserCurrentProjectName").toString();
			String moduleName = activeModuleName;
			if ((executionType != null && executionType.trim().equals("1")) || file == null) {
				datasheetStatus = "2";
				file = null;
			}
			int testCaseId = testCaseService.insertTestcase(project, moduleId, scenarioId, testcaseName,
					testcaseSummary, testcasePrecondition, executionType, executionTime, statusId, authorId,
					scenarioName, moduleName, projectName, schema, Integer.parseInt("" + datasheetStatus),
					redwoodTestCaseId);

			if (datasheetStatus != null && datasheetStatus.equals("1") && file != null) {
				String fileName = file.getOriginalFilename();
				byte[] bytes = new byte[0];
				if (!file.isEmpty()) {
					bytes = file.getBytes();
				}
				testSpecificationDao.uploadTestcaseDatasheet(Integer.parseInt("" + testCaseId), authorId, bytes,
						fileName, schema);
			}
			logger.info("Test case is inserted,return test case Id " + testCaseId);
			if (testCaseId != 0) {
				session.setAttribute("TcCreateStatus", 1);
				session.setAttribute("newTcName", testcaseName);
				logger.info(" User is redirected to testcase page ");
				return new ModelAndView("redirect:/testcase");
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return new ModelAndView("500");
			}
		}
	}

	/**
	 * @author shantaram.tupe
	 * @description 11-Mar-2020 12:25:35 PM
	 * @param session
	 * @param redwoodTestCaseId
	 * @return
	 * @throws Exception
	 *             Boolean
	 */
	@RequestMapping(value = "/isRedwoodTestCaseIdMapped")
	public @ResponseBody Boolean isRedwoodTestCaseIdMapped(HttpSession session, @RequestParam String redwoodTestCaseId)
			throws Exception {
		return this.testCaseService.isRedwoodTestCaseIdAlreadyMapped(redwoodTestCaseId,
				session.getAttribute("cust_schema").toString());
	}

	/**
	 * @author sushant.arora
	 * @method uploadTestCase POST Request
	 * @output addition of multiple test cases from a xsls
	 * @description This method accepts required parameter and create test case
	 *              for respective project-module-scenario
	 * @param model
	 * @param session
	 * @param moduleId
	 * @param scenarioId
	 * @param scenarioName
	 * @return Model & View (onSuccess - testcase ,onFailure - logout/500)
	 * @throws Exception
	 */

	@RequestMapping(value = "/bulkinserttestcase", method = RequestMethod.POST)
	public ModelAndView bulkInsertTestCase(Model model, HttpSession session,
			@RequestParam(value = "uploadDatasheet", required = false) MultipartFile file,
			@RequestParam("moduleId") String moduleId, @RequestParam("scenarioId") String scenarioId,
			@RequestParam("scenarioName") String scenarioName) throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);

			int authorId = Integer.parseInt(session.getAttribute("userId").toString());
			String project = session.getAttribute("UserCurrentProjectId").toString();
			String schema = session.getAttribute("cust_schema").toString();
			logger.info("Session is active, userId is " + authorId + " and projectId is " + project
					+ " Customer Schema " + schema);
			String projectName = session.getAttribute("UserCurrentProjectName").toString();
			String moduleName = session.getAttribute("selectedTestModuleName").toString();
			int flag = testCaseService.bulkInsertTestCase(project, moduleId, scenarioId, authorId, scenarioName,
					moduleName, projectName, file, schema);
			logger.info("Test case uploaded,return to list");
			if (flag == 1) {
				logger.info(" User is redirected to testcase page ");
				return new ModelAndView("redirect:/testcase");
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return new ModelAndView("500");
			}
		}
	}

	/**
	 * 
	 * @author suresh.adling
	 * @method updateTestCase POST Request
	 * @output Modify test case details
	 * @description This method accepts required parameter and update test case
	 *              details
	 * @param model
	 * @param session
	 * @param testcaseId
	 * @param testcaseName
	 * @param testcaseSummary
	 * @param testcasePrecondition
	 * @param executionType
	 * @param executionTime
	 * @param statusId
	 * @return Model & View (onSuccess - testcase ,onFailure - logout/500)
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatetestcase", method = RequestMethod.POST)
	public ModelAndView updateTestCase(Model model, HttpSession session, @RequestParam("testcaseId") String testcaseId,
			@RequestParam("testcaseName") String testcaseName, @RequestParam("testcaseSummary") String testcaseSummary,
			@RequestParam("testcasePrecondition") String testcasePrecondition,
			@RequestParam("executionType") String executionType, @RequestParam("executionTime") String executionTime,
			@RequestParam("statusId") String statusId, @RequestParam(required = false) String redwoodTestCaseId,
			@RequestParam(value = "uploadDatasheet", required = false) MultipartFile file,
			@RequestParam("datasheetStatus") String datasheetStatus) throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int updaterId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			if (file == null || file.getOriginalFilename().trim().length() == 0 || file.getSize() == 0) {
				List<Map<String, Object>> datasheetFileInformation = testSpecificationDao
						.getDatasheetByTcId(Integer.parseInt("" + testcaseId), schema);
				if (!executionType.trim().equals("1") && (datasheetStatus.equals("1")
						&& datasheetFileInformation != null && !datasheetFileInformation.isEmpty()
						&& datasheetFileInformation.get(0) != null && !datasheetFileInformation.get(0).isEmpty())) {
					datasheetStatus = "1";
				} else {
					datasheetStatus = "2";
				}
			}
			int flag = testCaseService.updateTestcase(testcaseId, testcaseName, testcaseSummary, testcasePrecondition,
					executionType, executionTime, statusId, updaterId, schema, datasheetStatus, redwoodTestCaseId);

			if ((datasheetStatus != null && !datasheetStatus.equals("1"))) {
				testSpecificationDao.deleteExistingTestcaseDatasheet(Integer.parseInt("" + testcaseId), schema);
			}
			if (datasheetStatus != null && datasheetStatus.equals("1") && file != null
					&& file.getOriginalFilename().trim().length() != 0 || file.getSize() != 0) {
				String fileName = file.getOriginalFilename();
				byte[] bytes = new byte[0];
				if (!file.isEmpty()) {
					bytes = file.getBytes();
				}
				testSpecificationDao.uploadTestcaseDatasheet(Integer.parseInt("" + testcaseId), updaterId, bytes,
						fileName, schema);
			}

			if (flag == 1) {
				logger.info("Test case is successfully updated");
				session.setAttribute("TcCreateStatus", 2);
				session.setAttribute("newTcName", testcaseName);
				logger.info(" User is redirected to teststeps page ");
				return new ModelAndView("redirect:/testcase");
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return new ModelAndView("500");
			}
		}
	}

	/**
	 * 
	 * @author abhay.thakur
	 * @method deleteTestCase POST Request
	 * @output Modify test case details
	 * @param model
	 * @param session
	 * @param testcaseId
	 * @return Model & View (onSuccess - testcase ,onFailure - logout/500)
	 * @throws Exception
	 */
	@RequestMapping(value = "/deletetestcase", method = RequestMethod.POST)
	public ModelAndView deleteTestCase(Model model, HttpSession session,
			@RequestParam("deleteTestcaseId") Integer testcaseId) throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();

			testcaseDAO.deleteTestcase(testcaseId, schema);

			// if (flag == 1) {
			logger.info("Test case is successfully deleted.");
			session.setAttribute("TcCreateStatus", 4);
			logger.info(" User is redirected to teststeps page ");
			return new ModelAndView("redirect:/testcase");
			// } else {
			// logger.error("Something went wrong, user is redirected to 500
			// error page");
			// return new ModelAndView("500");
			// }
		}
	}

	/**
	 * 
	 * @author suresh.adling
	 * @method setTestCaseId POST Request
	 * @output Set session attribute test case id of selected test case
	 * @description Post method to set test case id in session to do edit,update
	 *              operations on test case
	 * @param testcaseId
	 * @param viewType
	 * @param model
	 * @param session
	 * @return boolean value
	 * @throws Exception
	 */
	@RequestMapping(value = "/settestcaseid", method = RequestMethod.POST)
	public @ResponseBody boolean setTestCaseId(@RequestParam("testcaseId") String testcaseId,
			@RequestParam("viewType") String viewType, Model model, HttpSession session) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("selectedTestcaseId", testcaseId);
		session.setAttribute("testcaseViewType", viewType);
		return true;
	}

	/**
	 * 
	 * @author suresh.adling
	 * @method insertStep POST Request
	 * @output add multiple steps for a selected test case
	 * @description Post method to insert multiple steps accepting step details
	 *              (action,result)
	 * @param model
	 * @param session
	 * @param testcaseId
	 * @param testcaseName
	 * @param testStep
	 * @param action
	 * @param result
	 * @return Model & View (onSuccess - teststeps ,onFailure - logout/500)
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertstep", method = RequestMethod.POST)
	public String insertStep(Model model, HttpSession session, @RequestParam("testcaseId") String testcaseId,
			@RequestParam("testcaseName") String testcaseName, @RequestParam("testStep[]") String testStep[],
			@RequestParam("action[]") String action[], @RequestParam("result[]") String result[]) throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return "redirect:/logout";
		} else {
			utilsService.getLoggerUser(session);
			String userId = session.getAttribute("userId").toString();
			String schema = session.getAttribute("cust_schema").toString();
			String projectName = session.getAttribute("UserCurrentProjectName").toString();
			String moduleName = session.getAttribute("selectedTestModuleName").toString();
			String scenarioName = session.getAttribute("selectedScenarioName").toString();
			int flag = testCaseService.insertSteps(testcaseId, testStep, action, result, testcaseName, scenarioName,
					moduleName, projectName, Integer.parseInt(userId), schema);
			logger.info("Steps are added successfully ");
			if (flag == 1) {
				session.setAttribute("TcStepCreateStatus", 1);
				logger.info(" User is redirected to teststeps page ");
				return "redirect:/teststeps";
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return "redirect:/500";
			}
		}
	}

	/**
	 * 
	 * @author abhay.thakur
	 * @method insertTestData POST Request
	 * @param model
	 * @param session
	 * @param testcaseId
	 * @param testdata
	 * @return Model & View (onSuccess - teststeps ,onFailure - logout/500)
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertdata", method = RequestMethod.POST)
	public ResponseEntity<String> insertData(Model model, HttpSession session,
			@RequestParam("testcaseId") String testcaseId, @RequestParam("testData") String testData) throws Exception {
		logger.info("Checking for active session");
		// if (session.getAttribute("userId") == null) {
		// logger.error("Session has expired,user is redirected to login page");
		// return "redirect:/logout";
		// } else {
		String[] testcaseIds = null;
		String[] testDatas = null;
		int flag = 0;
		utilsService.getLoggerUser(session);
		String userId = session.getAttribute("userId").toString();
		String schema = session.getAttribute("cust_schema").toString();
		testcaseIds = testcaseId.substring(0, testcaseId.length() - 1).split("\\|");
		
		testDatas = testData.substring(0, testData.length() - 1).split("\\|");

		for (int i = 0; i < testcaseIds.length; i++) {
			String tData = "";
			if (testDatas[i].endsWith(",")) {
				tData = testDatas[i].substring(0, testDatas[i].length() - 1);
			} else {
				tData = testDatas[i];
			}
			List<Map<String, Object>> data = testcaseDAO.getTestCaseData(testcaseIds[i], schema);
			if (data.size() > 0) {
				flag = testCaseService.updateData(testcaseIds[i], tData, Integer.parseInt(userId), schema);
			} else {
				flag = testCaseService.insertData(testcaseIds[i], tData, Integer.parseInt(userId), schema);

			}

		}
		logger.info("Data are added successfully ");
		if (flag == 1) {
			return new ResponseEntity<String>("Datas are added successfully.", HttpStatus.OK);
		} else {
			return new ResponseEntity<String>("Error while inserting data.", HttpStatus.OK);

		}
		// }
	}

	/**
	 * 
	 * @author abhay.thakur
	 * @method UpdateTestData POST Request
	 * @param model
	 * @param session
	 * @param testcaseId
	 * @param testcaseName
	 * @param testdata
	 * @return Model & View (onSuccess - teststeps ,onFailure - logout/500)
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateData", method = RequestMethod.POST)
	public String updateData(Model model, HttpSession session, @RequestParam("testcaseId") String testcaseId,
			@RequestParam("testData") String testData) throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return "redirect:/logout";
		} else {
			utilsService.getLoggerUser(session);
			String userId = session.getAttribute("userId").toString();
			String schema = session.getAttribute("cust_schema").toString();
			int flag = testCaseService.updateData(testcaseId, testData, Integer.parseInt(userId), schema);
			logger.info("Data are updated successfully ");
			if (flag == 1) {
				session.setAttribute("TcDataCreateStatus", 1);
				logger.info(" User is redirected to TcData page ");
				return "redirect:/teststeps";
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return "redirect:/500";
			}
		}
	}

	/**
	 * @author suresh.adling
	 * @method updateStep POST Request
	 * @output update step details by using step id
	 * @description Post method to update step details (action,result)
	 * @param model
	 * @param session
	 * @param stepId
	 * @param testStepId
	 * @param action
	 * @param result
	 * @return Model & View (onSuccess - teststeps ,onFailure - logout/500)
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatestep", method = RequestMethod.POST)
	public String updateStep(Model model, HttpSession session, @RequestParam("updateStepId") String stepId,
			@RequestParam("updateTestStepId") String testStepId, @RequestParam("updateStepAction") String action,
			@RequestParam("updateStepResult") String result) throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return "redirect:/logout";
		} else {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			int testCaseId = Integer.parseInt(session.getAttribute("selectedTestcaseId").toString());
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int flag = testCaseService.updateSteps(stepId, testStepId, action, result, testCaseId, userId, schema);
			logger.info("Step details are upadted ");
			if (flag == 1) {
				session.setAttribute("TcStepCreateStatus", 2);
				logger.info(" User is redirected to teststeps page ");
				return "redirect:/teststeps";
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return "redirect:/500";
			}
		}
	}

	/**
	 * 
	 * @description insert a comment for test case
	 * @param commentDescription
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/inserttccomment", method = RequestMethod.POST)
	public ModelAndView insertTcComment(@RequestParam("commentDescription") String commentDescription, Model model,
			HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int testCaseId = Integer.parseInt(session.getAttribute("selectedTestcaseId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			testCaseService.insertTestcaseComment(commentDescription, userId, testCaseId, schema,
					utilsService.getCurrentDateTime());
			return new ModelAndView("redirect:/teststeps");
		}
	}

	/**
	 * 
	 * @description Like a comment on test case
	 * @param commentId
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/inserttccommentlike", method = RequestMethod.POST)
	public ModelAndView insertTcCommentLike(@RequestParam("commentId") String commentId, Model model,
			HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int likedByUser = Integer.parseInt(session.getAttribute("userId").toString());
			TcCommentLike tcCommentLike = new TcCommentLike();
			tcCommentLike.setCommentId(Integer.parseInt(commentId));
			tcCommentLike.setLikedByUser(likedByUser);
			String schema = session.getAttribute("cust_schema").toString();
			Map<String, Object> tcCommentlikeMap = testcaseCommentsDAO.insertTestcaseCommentLike(tcCommentLike, schema);
			int flag = Integer.parseInt(tcCommentlikeMap.get("#update-count-1").toString());
			if (flag == 1) {
				return new ModelAndView("redirect:/teststeps");
			} else {
				return new ModelAndView("500");
			}
		}
	}

	/**
	 *
	 * @description remove like for comment on test case
	 * @param commentId
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/removetccommentlike", method = RequestMethod.POST)
	public ModelAndView removeTcCommentLike(@RequestParam("commentId") String commentId, Model model,
			HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int likedByUser = Integer.parseInt(session.getAttribute("userId").toString());
			TcCommentLike tcCommentLike = new TcCommentLike();
			tcCommentLike.setCommentId(Integer.parseInt(commentId));
			tcCommentLike.setLikedByUser(likedByUser);
			String schema = session.getAttribute("cust_schema").toString();
			int flag = testcaseCommentsDAO.removeCommentLike(schema, tcCommentLike);
			if (flag == 1) {
				return new ModelAndView("redirect:/teststeps");
			} else {
				return new ModelAndView("500");
			}
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/createtestcase", method = RequestMethod.GET)
	public ModelAndView createTestCase(Model model, HttpSession session) throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {

			utilsService.getLoggerUser(session);

			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if (appCount == 0) {
				return new ModelAndView("redirect:/xedashboard");
			}

			// get the session values
			int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			int moduleId = Integer.parseInt(session.getAttribute("selectedTestModuleId").toString());
			String moduleName = session.getAttribute("selectedTestModuleName").toString();
			int scenarioId = Integer.parseInt(session.getAttribute("selectedScenarioId").toString());
			String scenarioName = session.getAttribute("selectedScenarioName").toString();

			model.addAttribute("scenarioId", scenarioId);
			model.addAttribute("scenarioName", scenarioName);
			model.addAttribute("moduleId", moduleId);
			model.addAttribute("moduleName", moduleName);

			Map<String, Object> data = testcaseDAO.getCreateTestCaseData(userId, projectId, moduleId, scenarioId,
					schema);

			// For the left side menu module list
			List<Map<String, Object>> moduleList = (List<Map<String, Object>>) data.get("#result-set-1");
			model.addAttribute("moduleList", moduleList);

			// For the left menu scenario list
			List<Map<String, Object>> scenarioList = (List<Map<String, Object>>) data.get("#result-set-2");
			model.addAttribute("scenarioList", utilsService.convertListOfMapToJson(scenarioList));

			// Logged in User Info
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			// Test case execution types
			List<Map<String, Object>> ExecutionTypes = (List<Map<String, Object>>) data.get("#result-set-3");
			model.addAttribute("executionTypes", ExecutionTypes);

			// Test case status list
			model.addAttribute("statusList", data.get("#result-set-4"));

			return new ModelAndView("createtestcase", "Model", model);
		}

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/uploadtestcase", method = RequestMethod.GET)
	public ModelAndView uploadTestCase(Model model, HttpSession session) throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {

			utilsService.getLoggerUser(session);

			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if (appCount == 0) {
				return new ModelAndView("redirect:/xedashboard");
			}

			// get the session values
			int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			int moduleId = Integer.parseInt(session.getAttribute("selectedTestModuleId").toString());
			String moduleName = session.getAttribute("selectedTestModuleName").toString();
			int scenarioId = Integer.parseInt(session.getAttribute("selectedScenarioId").toString());
			String scenarioName = session.getAttribute("selectedScenarioName").toString();

			model.addAttribute("scenarioId", scenarioId);
			model.addAttribute("scenarioName", scenarioName);
			model.addAttribute("moduleId", moduleId);
			model.addAttribute("moduleName", moduleName);

			Map<String, Object> data = testcaseDAO.getCreateTestCaseData(userId, projectId, moduleId, scenarioId,
					schema);

			// For the left side menu module list
			List<Map<String, Object>> moduleList = (List<Map<String, Object>>) data.get("#result-set-1");
			model.addAttribute("moduleList", moduleList);

			// For the left menu scenario list
			List<Map<String, Object>> scenarioList = (List<Map<String, Object>>) data.get("#result-set-2");
			model.addAttribute("scenarioList", utilsService.convertListOfMapToJson(scenarioList));

			// Logged in User Info
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			// Test case execution types
			List<Map<String, Object>> ExecutionTypes = (List<Map<String, Object>>) data.get("#result-set-3");
			model.addAttribute("executionTypes", ExecutionTypes);

			// Test case status list
			model.addAttribute("statusList", data.get("#result-set-4"));

			return new ModelAndView("uploadtestcase", "Model", model);
		}

	}

	/*
	 * @RequestMapping(value = "/getTestData/{buildId}", method =
	 * RequestMethod.GET) public ResponseEntity<String>
	 * getTestData(@PathVariable Integer buildId, HttpSession session) {
	 * logger.info("Ajax Post request to get test data"); String schema =
	 * session.getAttribute("cust_schema").toString(); List<Map<String, Object>>
	 * data = testcaseDAO.getTestCaseDataByBuildId(buildId, schema); //
	 * [{test_prefix=TATC-1, data=PC1,PC2}, {test_prefix=TATC-2, data=PC3,PC4}]
	 * String str[] = null; JSONObject jsonObject = new JSONObject(); JSONArray
	 * jarray = null; for (int i = 0; i < data.size(); i++) { if
	 * (data.get(i).get("data").toString().contains(",")) { str =
	 * data.get(i).get("data").toString().split(","); jarray = new JSONArray();
	 * for (int j = 0; j < str.length; j++) { jarray.put(str[j]); } } else {
	 * jarray.put(str); }
	 * jsonObject.put(data.get(i).get("test_prefix").toString(), jarray);
	 * 
	 * } return new ResponseEntity<String>(jsonObject.toString(),
	 * HttpStatus.OK); }
	 */

	@RequestMapping(value = "/getTestData/{buildId}", method = RequestMethod.GET)
	public void getTestData(@PathVariable Integer buildId, HttpSession session, HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("Ajax Post request to get test data");
		String schema = session.getAttribute("cust_schema").toString();
		String buildName = session.getAttribute("automationExecBuildName").toString();
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet spreadsheet[] = null;
		XSSFRow row;
		String filePath = "";
		List<Map<String, Object>> data = testcaseDAO.getTestCaseDataByBuildId(buildId, schema);
		// [{test_prefix=TATC-1, data=PC1,PC2}, {test_prefix=TATC-2,
		// data=PC3,PC4}]
		String str[] = null;
		JSONObject jsonObject = new JSONObject();
		JSONArray jarray = null;
		spreadsheet = new XSSFSheet[data.size()];
		for (int i = 0; i < data.size(); i++) {
			if (data.get(i).get("data").toString().contains(",")) {
				str = data.get(i).get("data").toString().split(",");
				jarray = new JSONArray();
				for (int j = 0; j < str.length; j++) {
					if (str[j].toString().equals("-")) {
						jarray.put("");
					} else {
						jarray.put(str[j]);
					}
				}
			} else {
				jarray = new JSONArray();
				if (data.get(i).get("data").toString().equals("-")) {
					jarray.put("");
				} else {
					jarray.put(data.get(i).get("data").toString());
				}
			}
			jsonObject.put(data.get(i).get("test_prefix").toString(), jarray);
			spreadsheet[i] = workbook.createSheet(data.get(i).get("test_prefix").toString());
			row = spreadsheet[i].createRow(0);

			for (int j = 0; j < jarray.length(); j++) {
				Cell cell = row.createCell(j);
				if (jarray.getString(j) != "") {
					cell.setCellValue(jarray.getString(j));
				}
			}

		}
		FileOutputStream out = null;
		try {
			String dirPath = "F:/" + buildName + ".xlsx";
			out = new FileOutputStream(new File(dirPath));
			workbook.write(out);
			// Thread.sleep(5000);

			ServletContext context = request.getServletContext();

			String fullPath = dirPath;
			File downloadFile = new File(fullPath);
			FileInputStream inputStream = new FileInputStream(downloadFile);

			// get MIME type of the file
			String mimeType = context.getMimeType(fullPath);
			if (mimeType == null) {
				// set to binary type if MIME mapping not found
				mimeType = "application/octet-stream";
			}
			// System.out.println("MIME type: " + mimeType);

			// set content attributes for the response
			response.setContentType(mimeType);
			response.setContentLength((int) downloadFile.length());

			// set headers for the response
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
			response.setHeader(headerKey, headerValue);

			// get output stream of the response
			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[4096];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

			inputStream.close();
			outStream.close();
			File file = new File("F:/" + buildName + ".xlsx");
			file.delete();

		} catch (Exception e) { 
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/checkTestData/{buildId}", method = RequestMethod.GET)
	public ResponseEntity<String> checkTestData(@PathVariable Integer buildId, HttpSession session) {
		String schema = session.getAttribute("cust_schema").toString();
		String buildName = session.getAttribute("automationExecBuildName").toString();
		List<Map<String, Object>> data = testcaseDAO.getTestCaseDataByBuildId(buildId, schema);
		if (data.isEmpty()) {
			return new ResponseEntity<String>("DataError", HttpStatus.OK);
		} else {
			return new ResponseEntity<String>("DataSuccess", HttpStatus.OK);
		}
	}

	/**
	 * @author suresh.adling
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :" + ex);
		return new ModelAndView("redirect:/500");
	}
}

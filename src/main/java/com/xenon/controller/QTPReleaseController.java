package com.xenon.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xenon.api.common.LoginService;
import com.xenon.api.testmanager.QTPRelaseService;
import com.xenon.testmanager.domain.ExtSysReleaseDetail;

@RestController
@RequestMapping("/addReleaseDetails")
public class QTPReleaseController {

	@Autowired
	LoginService loginService;
	
	@Autowired
	private QTPRelaseService qtpReleaseService; 
	
	private static final Logger logger = LoggerFactory.getLogger(QTPReleaseController.class);
	
	@RequestMapping(method = RequestMethod.POST,path = "/qtp")
	public  ResponseEntity<?> insertQTPReleaseDetails(@RequestBody ExtSysReleaseDetail releaseDetails,HttpSession session) {
		
		String result = loginService.authenticateUser(releaseDetails.getUsername().toLowerCase(), releaseDetails.getPassword(), session);
		String body = null;
		 if (!result.isEmpty()) {
			 releaseDetails.setExtSys("QTP");
			 logger.info("Request received for adding QTP release details into Xenon");
				int response = qtpReleaseService.insertExtSysReleaseDetails(releaseDetails,session.getAttribute("cust_schema").toString());
				logger.info("QTP release details added into Xenon ");
				
				if(response == 1) {
					HttpHeaders headers = new HttpHeaders();
					headers.set("status", "Release details saved successfully into Xenon!");
					body = "Release details saved successfully into Xenon!";
				}else {
					HttpHeaders headers = new HttpHeaders();
					headers.set("status", "Error occured while adding release details into Xenon. Please check request JSON!");
					body = "Error occured while adding release details into Xenon. Please check request JSON!";
				}
		 }else {
				HttpHeaders headers = new HttpHeaders();
				headers.set("status", "Authentication Failed! Please check Username and Password!");
				body = "Authentication Failed! Please check Username and Password!";
				return new ResponseEntity<String>(body, HttpStatus.UNAUTHORIZED);
		 }
		 return new ResponseEntity<String>(body, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST,path = "/selenium")
	public  ResponseEntity<?> insertSelReleaseDetails(@RequestBody ExtSysReleaseDetail releaseDetails,
			@RequestParam("userName") String userName, @RequestParam("password") String password, HttpSession session) {
		
		String result = loginService.authenticateUser(userName.toLowerCase(), password, session);
		String body = null;
		 if (!result.isEmpty()) {
			 releaseDetails.setExtSys("Selenium");
			 logger.info("Request received for adding Selenium release details into Xenon");
				int response = qtpReleaseService.insertExtSysReleaseDetails(releaseDetails,session.getAttribute("cust_schema").toString());
				logger.info("Selenium release details added into Xenon");
				
				if(response == 1) {
					HttpHeaders headers = new HttpHeaders();
					headers.set("status", "Release details saved successfully into Xenon!");
					body = "Release details saved successfully into Xenon!";
				}else {
					HttpHeaders headers = new HttpHeaders();
					headers.set("status", "Error occured while adding release details into Xenon. Please check request JSON!");
					body = "Error occured while adding release details into Xenon. Please check request JSON!";
				}
		 }
		 return new ResponseEntity<String>(body, HttpStatus.OK);
	}
}

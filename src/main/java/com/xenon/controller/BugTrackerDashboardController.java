package com.xenon.controller;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.administration.ProjectService;
import com.xenon.api.bugtracker.BugTrackerDashboardService;
import com.xenon.api.common.CommonService;
import com.xenon.api.common.JiraIntegrationService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.bugtracker.dao.BugDAO; // DAO class to perform JDBC operations for Bug Tracker Module
import com.xenon.bugtracker.dao.BugGroupDAO;
import com.xenon.buildmanager.dao.ActivitiesDAO; // DAO class to perform JDBC operations for all activities 
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.NotificationDAO;
import com.xenon.buildmanager.dao.ProjectDAO; // DAO class to perform JDBC operations for applications

/**
 * 
 * @author suresh.adling
 * @description Controller containing bug tracker dashboard views
 * 
 */
@Component
@Controller
public class BugTrackerDashboardController {

	@Resource(name = "colorProperties")
	private Properties colorProperties;
	@Autowired
	ProjectDAO projectDAO;
	
	@Autowired
	BugGroupDAO bugGroupDAO;

	@Autowired
	BugDAO bugDAO;

	@Autowired
	ActivitiesDAO activitiesDAO;
	
	@Autowired
	NotificationDAO notificationDAO;
	
	@Autowired
	BuildDAO buildDAO;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	UtilsService utilsService;

	@Autowired
	ProjectService projectService;
	
	@Autowired
	BugTrackerDashboardService bugTrackerDashboardService;
	
	@Autowired
	JiraIntegrationService jiraIntegrationService;
	
	@Autowired
	CommonService commonService;

	@Autowired
	RoleAccessService roleAccessService;
	
	private static final Logger logger = LoggerFactory.getLogger(BugTrackerDashboardController.class);
	

	/**
	 * @author suresh.adling
	 * @method viewBTDashboard GET Request
	 * @output get bug dashboard view
	 * @param Model
	 *            model,HttpSession session
	 * @description This method reads following list by using session stored
	 *              values(userId,projectId) and redirects user to btdashboard
	 *              view with the model
	 * @return Model (OpenBugList,bugStat,getBugtable) & View (onSuccess -
	 *         btdashboard ,onFailure - logout/trialerror/500)
	 */
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping(value = "/btdashboard")
	public ModelAndView viewBTDashboard(@RequestParam(value="page",defaultValue="1") int page,Model model, HttpSession session) throws Exception {
	

		logger.info("Redirecting to bug Tracker Dashboard and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("btdashboard",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			/*model = common.getHeaderValues(model, session);
			if (model.containsAttribute("noProject")) {
				model.asMap().clear();
				return new ModelAndView("redirect:/xedashboard");
			}*/
			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if(appCount == 0){
				return new ModelAndView("redirect:/xedashboard");
			}
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
			String userTimeZone=session.getAttribute("userTimezone").toString();
			Object userprofilePhoto=loginService.getImage((byte[]) session.getAttribute("userProfilePhoto"));
			String fullName=session.getAttribute("userFullName").toString();
			String role=session.getAttribute("role").toString();
			//--------------------check Jira status-----------------------------------//
			
			Map<String,Object> returnModel=bugTrackerDashboardService.getDashboardData(schema,userId,projectId,userTimeZone,userprofilePhoto,fullName,role,page);
			
			boolean jiraStatus = jiraIntegrationService.isJiraConnected(session.getAttribute("customerId").toString());
			if(jiraStatus){
				returnModel.put("jiraStatus", "y");
			}else{
				returnModel.put("jiraStatus", "n");
			}
			
			Iterator it = returnModel.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        model.addAttribute((String) pair.getKey(),pair.getValue());
		       // System.out.println((String) pair.getKey()+" * "+pair.getValue());
		        it.remove();
		    }
		   // System.out.println("model");
		   // System.out.println(model);
			logger.warn("User is redirected to bug dashboard Page with model : " + model);
			return new ModelAndView("btdashboard", "Model", model);
		}
		logger.error("Something went wrong, user is redirected to 500 error page");
		return new ModelAndView("500");
	}

	/**
	 * @author suresh.adling
	 * @method viewAllBTDAshboard GET Request
	 * @output get bug dashboard view for all applications
	 * @param Model
	 *            model,HttpSession session
	 * @description Method used to create view which populates statistics for
	 *              all applications
	 * @return Model (statusList,priorityList,moduleList) & View (onSuccess -
	 *         createbug ,onFailure - logout/trialerror/500)
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/btallappdashboard")
	public ModelAndView viewAllBTDAshboard(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to BT all application dashboard page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			if (bugDAO.checkBTRoleAccess("btdashboard",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
				utilsService.getLoggerUser(session);
				/*model = common.getHeaderValues(model, session);
				if (model.containsAttribute("noProject")) {
					model.asMap().clear();
					return new ModelAndView("redirect:/xedashboard");
				}*/
				int appCount = commonService.getLoginUserAssignedAppCount(session);
				if(appCount == 0){
					return new ModelAndView("redirect:/xedashboard");
				}
				
				int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
				String schema = session.getAttribute("cust_schema").toString();
				int userId = Integer.parseInt(session.getAttribute("userId").toString());
				logger.info("Session is active, userId is " + userId + " and projectId is " + projectId
						+ " Customer Schema " + schema);
				String userTimeZone=session.getAttribute("userTimezone").toString();
				Object userProfilePhoto= session.getAttribute("userProfilePhoto");
				String userFullName=session.getAttribute("userFullName").toString();
				String role=session.getAttribute("role").toString();
				Map<String,Object> returnData=bugTrackerDashboardService.viewAllBTDAshboard(userId,schema,userTimeZone,userProfilePhoto,userFullName,role);
				Iterator it = returnData.entrySet().iterator();
				while (it.hasNext()) {
			        Map.Entry pair = (Map.Entry)it.next();
			        model.addAttribute((String) pair.getKey(),pair.getValue());
			        it.remove();
			    }	
				logger.info("User is redirected to BT all application dashboard page");
				return new ModelAndView("btallappdashboard", "Model", model);
			}
		}
		logger.error("Something went wrong, user is redirected to 500 error page");
		return new ModelAndView("500");
	}

	/**
	 * @author suresh.adling
	 * @method getBTDashboardLinks GET Request
	 * @output get view to list out bugs from Bug Tracker Dashboard
	 * @param Model
	 *            model,HttpSession session
	 * @description
	 * @return Model (statusList,priorityList,moduleList) & View (onSuccess -
	 *         btdashboardlink ,onFailure - logout/trialerror/500)
	 */
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/btdashboardlink")
	public ModelAndView getBTDashboardLinks(@RequestParam(value="page",defaultValue="1") int page,
			Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to bug Tracker Dashboard and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("btdashboard",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			
			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if(appCount == 0){
				return new ModelAndView("redirect:/xedashboard");
			}
		
			int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info("Session is active, userId is " + userId + " and projectId is " + projectId
					+ " Customer Schema " + schema);
			
			//this variable indicates which bugs are to be shown
			String filterText = session.getAttribute("filterText").toString();
			logger.info("here is fileter text :"+filterText);
			int pageSize= 10;
			model.addAttribute("pageSize",pageSize);
			int startValue = (page-1)*pageSize;
			
			System.out.println("start value "+startValue);
			
			String dashboardType = session.getAttribute("btDashboardType").toString();
			Map<String, Object> data;
			if(dashboardType.equalsIgnoreCase("projectWise")){
				//It is project wise link
				
				if(filterText.contains("closed")){
					//Closed bugs are selected
					logger.info("project :"+projectId+" user :"+userId+" schema :"+schema);
					data = bugDAO.getBtDashLinkClosedBugs(projectId, userId, startValue, pageSize,schema);
					System.out.println(data);
					List<Map<String, Object>> closedList = (List<Map<String, Object>>) data.get("#result-set-1");
					System.out.println("data "+closedList);
					if(closedList != null){
						for (int j = 0; j < closedList.size(); j++){
							if(closedList.get(j).get("assign_status").toString().equals("2")){
								closedList.get(j).put("assignee", "None");
							}
						}
					}
					model.addAttribute("closedList", closedList);
					int allBugsCount = (Integer) data.get("count");
					model.addAttribute("allBugsCount",allBugsCount);
					
				}else if(filterText.contains("openBug")){
					//Open bugs are selected
					data = bugDAO.getBtDashLinkOpenBugs(projectId, userId, startValue, pageSize, schema);
					
					List<Map<String, Object>> allOpenBugList = (List<Map<String, Object>>) data.get("#result-set-1");
					if(allOpenBugList != null){
						for (int j = 0; j < allOpenBugList.size(); j++){
							//Formatted create and update dates 
							String createdDate=allOpenBugList.get(j).get("create_date").toString().substring(0, 10);
							allOpenBugList.get(j).put("create_date", createdDate);
							String updatedDate=allOpenBugList.get(j).get("updated_date").toString().substring(0, 10);
							allOpenBugList.get(j).put("updated_date", updatedDate);
							
							if(allOpenBugList.get(j).get("assign_status").toString().equals("2")){
								allOpenBugList.get(j).put("assignee", "None");
							}
						}
					}
					model.addAttribute("allBugDetails", allOpenBugList);
					int allBugsCount = (Integer) data.get("count");
					model.addAttribute("allBugsCount",allBugsCount);
					
				}else if(filterText.equalsIgnoreCase("assigned")){
					//Assigned to me bugs are selected
					data = bugDAO.getBtDashLinkAssignedBugs(projectId, userId, startValue, pageSize, schema);
					
					List<Map<String, Object>> assignedList = (List<Map<String, Object>>) data.get("#result-set-1");
					if(assignedList != null){
						for (int j = 0; j < assignedList.size(); j++){
							if(assignedList.get(j).get("assign_status").toString().equals("2")){
								assignedList.get(j).put("assignee", "None");
							}
						}
					}
					model.addAttribute("assignedList", assignedList);
					int allBugsCount = (Integer) data.get("count");
					model.addAttribute("allBugsCount",allBugsCount);
				}else if(filterText.equalsIgnoreCase("immediate")){
					//immediate priority bugs are selected
					data = bugDAO.getBtDashLinkImmediateBugs(projectId, userId, startValue, pageSize, schema);
					
					List<Map<String, Object>> immediateList = (List<Map<String, Object>>) data.get("#result-set-1");
					if(immediateList != null){
						for (int j = 0; j < immediateList.size(); j++){
							if(immediateList.get(j).get("assign_status").toString().equals("2")){
								immediateList.get(j).put("assignee", "None");
							}
						}
					}
					model.addAttribute("immediateList", immediateList);
					int allBugsCount = (Integer) data.get("count");
					model.addAttribute("allBugsCount",allBugsCount);
				}
			else if(filterText.contains("bugCat")){
				//Closed bugs are selected
				logger.info("project :"+projectId+" user :"+userId+" schema :"+schema);
				data = bugDAO.getBtDashLinkClosedBugs(projectId, userId, startValue, pageSize,schema);
				System.out.println(data);
				List<Map<String, Object>> closedBugList = (List<Map<String, Object>>) data.get("#result-set-1");
				System.out.println("data "+closedBugList);
				if(closedBugList != null){
					for (int j = 0; j < closedBugList.size(); j++){
						if(closedBugList.get(j).get("assign_status").toString().equals("2")){
							closedBugList.get(j).put("assignee", "None");
						}
					}
				}
				model.addAttribute("closedList", closedBugList);
				int allBugsCount = (Integer) data.get("count");
				model.addAttribute("allBugsCount",allBugsCount);
				
			}
			else if(filterText.contains("openList")){
				//Open bugs are selected
				data = bugDAO.getBtDashLinkOpenBugs(projectId, userId, startValue, pageSize, schema);
				
				List<Map<String, Object>> allOpenBug = (List<Map<String, Object>>) data.get("#result-set-1");
				if(allOpenBug != null){
					for (int j = 0; j < allOpenBug.size(); j++){
						if(allOpenBug.get(j).get("assign_status").toString().equals("2")){
							allOpenBug.get(j).put("assignee", "None");
						}
					}
				}
				model.addAttribute("allBugDetails", allOpenBug);
				int allBugsCount = (Integer) data.get("count");
				model.addAttribute("allBugsCount",allBugsCount);
				
			}
				
				//mycode
				
			else if(filterText.contains("InProgressList")){
				//Inprogress bugs are selected
				//System.out.println(projectId +" "+ userId+" "+startValue+" "+pageSize+" "+schema);
				data = bugDAO.getBtDashLinkInprogressBugs(projectId, userId, startValue, pageSize, schema);
				
				List<Map<String, Object>> allIPL = (List<Map<String, Object>>) data.get("#result-set-1");
				if(allIPL != null){
					for (int j = 0; j < allIPL.size(); j++){
						if(allIPL.get(j).get("assign_status").toString().equals("2")){
							allIPL.get(j).put("assignee", "None");
						}
					}
				}
				model.addAttribute("allIPL", allIPL);
				int allIPLCount = (Integer) data.get("count");
				model.addAttribute("allBugsCount",allIPLCount);	
			}
			
			else if(filterText.contains("ResolvedList")){
				//Resolved bugs are selected
				
				System.out.println(projectId +" "+ userId+" "+startValue+" "+pageSize+" "+schema);
				data = bugDAO.getBtDashLinkResolvedBugs(projectId, userId, startValue, pageSize, schema);
				
				List<Map<String, Object>> allIPL1 = (List<Map<String, Object>>) data.get("#result-set-1");
				if(allIPL1 != null){
					for (int j = 0; j < allIPL1.size(); j++){
						if(allIPL1.get(j).get("assign_status").toString().equals("2")){
							allIPL1.get(j).put("assignee", "None");
						}
					}
				}
				model.addAttribute("allIPL1", allIPL1);
				int allIPL1Count = (Integer) data.get("count");
				model.addAttribute("allBugsCount",allIPL1Count);	
				
			}
				
			else if(filterText.contains("ReopenedList")){
				//Reopened bugs are selected
				
				System.out.println(projectId +" "+ userId+" "+startValue+" "+pageSize+" "+schema);
				data = bugDAO.getBtDashLinkReopenedBugs(projectId, userId, startValue, pageSize, schema);
				
				List<Map<String, Object>> allIPL2 = (List<Map<String, Object>>) data.get("#result-set-1");
				if(allIPL2 != null){
					for (int j = 0; j < allIPL2.size(); j++){
						if(allIPL2.get(j).get("assign_status").toString().equals("2")){
							allIPL2.get(j).put("assignee", "None");
						}
					}
				}
				model.addAttribute("allIPL2", allIPL2);
				int allIPL2Count = (Integer) data.get("count");
				model.addAttribute("allBugsCount",allIPL2Count);	
				
			}
				
		
			
				
				
			}else{
				//This is for all application 
				if(filterText.equalsIgnoreCase("closed")){
					//Closed bugs are selected
					data = bugDAO.getBtDashLinkAllClosedBugs(userId, startValue, pageSize, schema);
					
					List<Map<String, Object>> closedList = (List<Map<String, Object>>) data.get("#result-set-1");
					if(closedList != null){
						for (int j = 0; j < closedList.size(); j++){
							if(closedList.get(j).get("assign_status").toString().equals("2")){
								closedList.get(j).put("assignee", "None");
							}
						}
					}
					model.addAttribute("closedList", closedList);
					int allBugsCount = (Integer) data.get("count");
					model.addAttribute("allBugsCount",allBugsCount);
					
				}else if(filterText.equalsIgnoreCase("openBug")){
					//Open bugs are selected
					data = bugDAO.getBtDashLinkAllOpenBugs(userId, startValue, pageSize, schema);
					
					List<Map<String, Object>> allOpenBugList = (List<Map<String, Object>>) data.get("#result-set-1");
					if(allOpenBugList != null){
						for (int j = 0; j < allOpenBugList.size(); j++){
							//Formatted create and update dates 
							String createdDate=allOpenBugList.get(j).get("create_date").toString().substring(0, 10);
							allOpenBugList.get(j).put("create_date", createdDate);
							String updatedDate=allOpenBugList.get(j).get("updated_date").toString().substring(0, 10);
							allOpenBugList.get(j).put("updated_date", updatedDate);
							
							if(allOpenBugList.get(j).get("assign_status").toString().equals("2")){
								allOpenBugList.get(j).put("assignee", "None");
							}
						}
					}
					model.addAttribute("allBugDetails", allOpenBugList);
					int allBugsCount = (Integer) data.get("count");
					model.addAttribute("allBugsCount",allBugsCount);
					
				}else if(filterText.equalsIgnoreCase("assigned")){
					//Assigned to me bugs are selected
					data = bugDAO.getBtDashLinkAllAssignedBugs(userId, startValue, pageSize, schema);
					
					List<Map<String, Object>> assignedList = (List<Map<String, Object>>) data.get("#result-set-1");
					if(assignedList != null){
						for (int j = 0; j < assignedList.size(); j++){
							if(assignedList.get(j).get("assign_status").toString().equals("2")){
								assignedList.get(j).put("assignee", "None");
							}
						}
					}
					model.addAttribute("assignedList", assignedList);
					int allBugsCount = (Integer) data.get("count");
					model.addAttribute("allBugsCount",allBugsCount);
					
				}else if(filterText.equalsIgnoreCase("immediate")){
					//immediate priority bugs are selected
					data = bugDAO.getBtDashLinkAllImmediateBugs(userId, startValue, pageSize, schema);
					
					List<Map<String, Object>> immediateList = (List<Map<String, Object>>) data.get("#result-set-1");
					if(immediateList != null){
						for (int j = 0; j < immediateList.size(); j++){
							if(immediateList.get(j).get("assign_status").toString().equals("2")){
								immediateList.get(j).put("assignee", "None");
							}
						}
					}
					model.addAttribute("immediateList", immediateList);
					int allBugsCount = (Integer) data.get("count");
					model.addAttribute("allBugsCount",allBugsCount);
					
				}else if(filterText.contains("allbugs")){
					//immediate priority bugs are selected
					data = bugDAO.getBtDashLinkAllClosedBugs(userId, startValue, pageSize, schema);
					
					List<Map<String, Object>> UpdatedDateBugList = (List<Map<String, Object>>) data.get("#result-set-2");
							

					model.addAttribute("UpdatedDateBugList", UpdatedDateBugList);
				
					if(UpdatedDateBugList != null){
						for (int j = 0; j < UpdatedDateBugList.size(); j++){
							if(UpdatedDateBugList.get(j).get("assign_status").toString().equals("2")){
								UpdatedDateBugList.get(j).put("assignee", "None");
							}
						}
					}
					int allBugsCount = (Integer) data.get("count");
					model.addAttribute("allBugsCount",allBugsCount);
				}
			}

			String appFlag= "false";
			if(session.getAttribute("headerAllApplication")!=null){
				
				appFlag = session.getAttribute("headerAllApplication").toString();
			}
			
			model.addAttribute("appFlag",appFlag);
			
		
			
			model.addAttribute("filterText", filterText);
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			model.addAttribute("New", colorProperties.getProperty("bug.New"));
			model.addAttribute("Fixed", colorProperties.getProperty("bug.Fixed"));
			model.addAttribute("Closed", colorProperties.getProperty("bug.Closed"));
			model.addAttribute("Assigned", colorProperties.getProperty("bug.Assigned"));
			model.addAttribute("Verified", colorProperties.getProperty("bug.Verified"));
			model.addAttribute("Reject", colorProperties.getProperty("bug.Reject"));
			logger.warn("User is redirected to bug dashboard Page with model : " + model);
			return new ModelAndView("btdashboardlink", "Model", model);
		}
		logger.error("User does not have access to btdashboardlink service, user is redirected to 500 error page");
		return new ModelAndView("500");
	}
	
	

	/**
	 * @author suresh.adling
	 * @method setBtLinkSession GET Request
	 * @param model
	 * @param session
	 * @param filterText
	 * @param dashboard
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setBtLinkSession")
	public ModelAndView setBtLinkSession(Model model, HttpSession session,
			@RequestParam("filterText") String filterText, @RequestParam("dashboard") String dashboard)
					throws Exception {
		logger.info("Setting selecte choice for redirection to bug tracker data page");
		logger.info("checking for active session");

		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirecting to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("filterText", filterText);
			session.setAttribute("btDashboardType", dashboard);
			return new ModelAndView("redirect:/");
		}
	}

	/**
	 * @author suresh.adling
	 * @method setSessionforCurrentProject POST Request - Set current Project
	 *         for bug tracker
	 * @param model
	 * @param session
	 * @param prjId
	 * @param prjName
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setCurrentProjectBT")
	public ModelAndView setSessionforCurrentProject(Model model, HttpSession session,
			@RequestParam("projectId") String prjId, @RequestParam("projectName") String prjName) throws Exception {
		logger.info("Setting slected project as current project");
		logger.info("checking for active session");

		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirecting to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("UserCurrentProjectName", prjName);
			session.setAttribute("UserCurrentProjectId", prjId);
			session.setAttribute("headerAllApplication", false);
			session.setAttribute("btDashboardType", false);

			String SchemeName = session.getAttribute("cust_schema").toString();
			int projectId = Integer.parseInt(prjId);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info("Session is active, userId is " + userId + " and projectId is " + projectId
					+ " Customer Schema " + SchemeName);

			projectService.updateCurrentProject(userId, projectId, SchemeName);
			logger.info("Project is selected as a curent project");
			
			return viewBTDashboard(1,model, session);
		}
	}
	
	
	/**
	 * @author suresh.adling
	 * @method setidfordashbugs POST Request
	 * @output sets session attribute bug id for selected bug
	 * @param Request
	 *            Parameter(moduleId) Method Parameter(HttpSession session)
	 * @description This method accepts bug id,sets bug id and returns true
	 * 
	 * @return boolean output
	 */
	@RequestMapping(value = "/setidfordashbugs", method = RequestMethod.POST)
	public @ResponseBody boolean setBugIdForSummary(Model model, HttpSession session,
			@RequestParam("bugId") String bugId,@RequestParam("projectId") String projectId) {
		utilsService.getLoggerUser(session);
		if (session.getAttribute("userId") == null) {
			return false;
		} else {
			session.setAttribute("selectedBugId", bugId);
			session.setAttribute("selectedBugProjectId", projectId);
			return true;
		}
	}
	
	
	
	
	@RequestMapping(value = "/setidforallapp", method = RequestMethod.POST)
	public @ResponseBody boolean setidforallapp(Model model, HttpSession session) {
		utilsService.getLoggerUser(session);
		if (session.getAttribute("userId") == null) {
			return false;
		} else {
			session.setAttribute("headerAllApplication", true);
			return true;
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request is used to read header data
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/btheader")
	public ModelAndView btheader(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active,  Customer Schema " + schema);
			Map<String, Object> buildDashboard =notificationDAO.getNotifications(Integer.parseInt(session.getAttribute("userId").toString()), schema); //buildDAO.headerData(schema);
			List<Map<String, Object>> allBuildsTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-1");
			List<Map<String, Object>> allBuildsExecutedTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-2");
			
			List<Map<String, Object>> allBuilds = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-5");
			
			List<Map<String, Object>> topBuilds = buildDAO.getBmDashboardTopBuildsNf(allBuilds,allBuildsTestcasesCount,
					allBuildsExecutedTestcasesCount, schema);
			for (int i = 0; i < topBuilds.size(); i++) {
				topBuilds.get(i).put("user_image",
						utilsService.getFileData((byte[]) topBuilds.get(i).get("user_photo")));
				topBuilds.get(i).put("build_createdate",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(topBuilds.get(i).get("build_createdate").toString())));
			}
			
			List<Map<String, Object>> notifications =  (List<Map<String, Object>>) buildDashboard
					.get("#result-set-3");
			for (int i = 0; i < notifications.size(); i++) {
				notifications.get(i).put("user_image", utilsService.getFileData((byte[]) notifications.get(i).get("user_photo")));
				String dateInString = notifications.get(i).get("notification_date").toString();
				notifications.get(i).put("notification_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), dateInString));
			}
			List<Map<String, Object>> unreadCount =  (List<Map<String, Object>>) buildDashboard
					.get("#result-set-4");
			model.addAttribute("unreadCount", Integer.parseInt(unreadCount.get(0).get("unreadCount").toString()));
			model.addAttribute("nowDate", utilsService.getNowDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("prevDate",
					utilsService.getPrevDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("notifications", notifications);
			model.addAttribute("topBuilds", topBuilds);
			/*model = common.getHeaderValues(model, session);
			if (model.containsAttribute("noProject")) {
				model.asMap().clear();
				return new ModelAndView("redirect:/xedashboard");
			}*/
			List<Map<String, Object>> projectList = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-6");
			projectList = commonService.getHeaderValues(session, projectList);
			model.addAttribute("projectList", projectList);
			
			return new ModelAndView("btheader", "Model", model);
		}
	}
	
	@RequestMapping(value = "/setbtlinksessionappwise")
	public ModelAndView setBtLinkSessionAppWise(Model model, HttpSession session,
			@RequestParam("filterText") String filterText, @RequestParam("dashboard") String dashboard,
			@RequestParam("appID") int appID)
			throws Exception {
		logger.info("Setting selecte choice for redirection to bug tracker data page");
		logger.info("checking for active session");

		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirecting to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("filterText", filterText);
			session.setAttribute("btDashboardType", dashboard);
			return new ModelAndView("redirect:/");
		}
	}
	
	/**
	 * @author suresh.adling
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :" + ex);
		return new ModelAndView("redirect:/500");
	}
}

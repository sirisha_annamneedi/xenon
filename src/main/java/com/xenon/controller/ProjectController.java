package com.xenon.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jadelabs.custmoze.query.release.RedwoodManagement;
import com.xenon.api.administration.ProjectService;
import com.xenon.api.buildmanager.UserService;
import com.xenon.api.common.JiraIntegrationService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.buildmanager.dao.NotificationDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.buildmanager.domain.UserNotification;
import com.xenon.buildmanager.domain.UserProjectDetails;

/**
 * 
 * @author Abhay.Thakur
 * @purpose Project Controller is used for creating,editing,updating an
 *          application. Also it is used for assigning or un-assigning an
 *          application to users.
 *
 */
@SuppressWarnings("unchecked")
@Controller
public class ProjectController {

	@Autowired
	RoleAccessService roleAccessService;

	private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);

	@Autowired
	ProjectService projectService;

	@Autowired
	LoginService loginService;

	@Autowired
	UtilsService utilsService;

	@Autowired
	ProjectDAO projectDAO;
	@Autowired
	UserDAO userDao;

	@Autowired
	ModuleDAO moduleDao;

	@Autowired
	NotificationDAO notificationDAO;

	@Autowired
	UserService userService;

	@Autowired
	BuildDAO buildDAO;

	@Autowired
	JiraIntegrationService jiraIntegrationService;

	/**
	 * @author prafulla.pol
	 * @method createProject a GET Request
	 * @param Model
	 *            model
	 * @param HttpSession
	 *            session
	 * @return Model (model) & View (onSuccess - createproject , onFailure -
	 *         logout/trialerror/404)
	 * @throws Exception
	 * @Description This method returns Create New Application page for creating
	 *              an application. It also checks for limit of the trial user.
	 */
	@RequestMapping(value = "/createproject")
	public ModelAndView createProject(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Create New Application page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to create an application");
			boolean jiraStatus = jiraIntegrationService.isJiraConnected(session.getAttribute("customerId").toString());
			if (jiraStatus) {
				model.addAttribute("jiraStatus", "y");
			} else {
				model.addAttribute("jiraStatus", "n");
			}

			if (buildDAO.checkBMRoleAccess("createproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to create application");
				session.setAttribute("menuLiText", "Applications");
				if (session.getAttribute("duplicateProjectError") != null) {
					model.addAttribute("error", session.getAttribute("duplicateProjectError").toString());
					session.removeAttribute("duplicateProjectError");
				}

				int flag = projectDAO.createNewProjectStatus(schema,
						Integer.parseInt(session.getAttribute("cust_type_id").toString()));
				if (flag == 1) {
					logger.info(" User is redirected to Create New Application page ");
					return new ModelAndView("createproject", "Model", model);
				} else {
					logger.info(
							" User is Exceeded the Trial limit to create an application . Redirected to Trial Error page ");
					return new ModelAndView("redirect:/trialerror");
				}
			} else {
				logger.warn("User has no access to create an application . Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method setProjectID a POST Request
	 * @param selectedProjectID
	 *            Project ID from view project page which is clicked
	 * @param viewType
	 *            View Type from view project page which is clicked either
	 *            'view' or 'edit'
	 * @param HttpSession
	 *            session
	 * @param Model
	 *            model
	 * @return
	 * @throws Exception
	 * @Description this method accepts project id and view type and stores it
	 *              into the session.
	 */
	@RequestMapping(value = "/setprojectid", method = RequestMethod.POST)
	public ModelAndView setProjectID(@RequestParam("projectID") String selectedProjectID,
			@RequestParam("viewType") String viewType, HttpSession session, Model model) throws Exception {
		logger.info("Post method to save the Application id and view type into session.Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			session.setAttribute("selectedProjectID", selectedProjectID);
			session.setAttribute("selectedProjectviewType", viewType);
			logger.info(" User is redirected ");
			return new ModelAndView("redirect:/");
		}
	}

	/**
	 * @author prafulla.pol
	 * @method editProject a GET Request
	 * @param Model
	 *            model
	 * @param HttpSession
	 *            session
	 * @return Model (projectDetails) & View (onSuccess - editproject ,
	 *         onFailure - logout/404)
	 * @throws Exception
	 * @Description This method gets the project ID from the session and returns
	 *              the user edit application page with project data for that
	 *              ID.
	 */
	@RequestMapping(value = "/editproject", method = RequestMethod.GET)
	public ModelAndView editProject(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Edit Applications page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to Edit Application");
			if (buildDAO.checkBMRoleAccess("editproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to Edit an application");
				session.setAttribute("menuLiText", "Applications");

				int projectID = Integer.parseInt(session.getAttribute("selectedProjectID").toString());
				Map<String, Object> editProjectData = projectDAO.getEditProjectData(projectID, schema);

				logger.info(" User is redirected to Edit Application page ");
				return new ModelAndView("editproject", "projectDetails",
						(List<Map<String, Object>>) editProjectData.get("#result-set-1"));
			} else {
				logger.warn("User has no access to Edit an application . Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method viewProject a GET Request
	 * @param Model
	 *            model
	 * @param HttpSession
	 *            session
	 * @return Model (createProAccessStatus,projectDetails,moduleList) & View
	 *         (onSuccess - viewproject , onFailure - logout/404)
	 * @throws Exception
	 * @Description This method checks user access to access view applications
	 *              page. If user has access then it gets the data from database
	 *              and redirects to view application page.
	 */
	@RequestMapping(value = "/viewproject")
	public ModelAndView viewProject(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to view Applications page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to view Applications");
			if (buildDAO.checkBMRoleAccess("viewproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to View applications");
				session.setAttribute("menuLiText", "Applications");

				boolean createStatus = buildDAO.checkBMRoleAccess("createproject",
						session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString());
				if (createStatus) {
					model.addAttribute("createProAccessStatus", 1);
				}
				boolean accessStatus = buildDAO.checkBMRoleAccess("createmodule",
						session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString());
				if (accessStatus) {
					model.addAttribute("createModuleAccessStatus", 1);
				}

				Map<String, Object> projectData = projectDAO.getProjectData(schema);

				List<Map<String, Object>> projectDetails = (List<Map<String, Object>>) projectData.get("#result-set-1");
				for (int i = 0; i < projectDetails.size(); i++) {
					projectDetails.get(i).put("project_createdate",
							utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
									utilsService.getDate(projectDetails.get(i).get("project_createdate").toString())));
				}
				model.addAttribute("projectDetails", projectDetails);

				List<Map<String, Object>> moduleList = (List<Map<String, Object>>) projectData.get("#result-set-2");
				model.addAttribute("Modules", moduleList);

				logger.info(" User is redirected to view Applications page ");
				return new ModelAndView("viewproject", "model", model);
			} else {
				logger.warn("User has no access to View applications. Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method insertProject a POST Request
	 * @param projectName
	 *            A name of an application to be created
	 * @param bugPrefix
	 *            Bug prefix for an application
	 * @param tcPrefix
	 *            Test Case prefix for an application
	 * @param projectDescription
	 *            A Description of an application to be created
	 * @param proStatus
	 *            An Application status whether 'Active' or 'Inactive'
	 * @param Model
	 *            model
	 * @param HttpSession
	 *            session
	 * @return Model () & View (onSuccess - viewproject , onFailure -
	 *         logout/createproject/500)
	 * @throws Exception
	 * @Description This method accepts the create application form parameters
	 *              and inserts an application into the database. Based on the
	 *              insertion status user is redirected to respected pages.
	 */
	@RequestMapping(value = "/insertproject", method = RequestMethod.POST, params = { "projectName", "jiraprojectName",
			"bugPrefix", "tcPrefix", "projectDescription", "projectStatus" })
	public ModelAndView insertProject(@RequestParam("projectName") String projectName,
			@RequestParam("jiraprojectName") String jiraProjectName, @RequestParam("bugPrefix") String bugPrefix,
			@RequestParam("tcPrefix") String tcPrefix, @RequestParam("projectDescription") String projectDescription,
			@RequestParam("projectStatus") String proStatus, Model model, HttpSession session) throws Exception {

		logger.info("Post method to insert an application. Checking for active session");

		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			Map<String, String> reponseData = new HashMap<String, String>();

			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to create an application");

			if (buildDAO.checkBMRoleAccess("createproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to create application");
				int flag = -1;

				reponseData = projectService.insertProject(projectName, jiraProjectName, projectDescription, bugPrefix,
						tcPrefix, proStatus, schema, utilsService.getCurrentDateTime());

				if (Integer.parseInt(reponseData.get("responseCode")) == 201
						&& reponseData.get("status").equalsIgnoreCase("Project Created successfully.")) {
					flag = 1;
				} else if (Integer.parseInt(reponseData.get("responseCode")) == 208
						&& reponseData.get("status").equalsIgnoreCase("Duplicate Project details : Test Name.")) {
					flag = 2;
				} else if (Integer.parseInt(reponseData.get("responseCode")) == 208
						&& reponseData.get("status").equalsIgnoreCase("Duplicate Project details : Bug Prifix.")) {
					flag = 3;
				} else if (Integer.parseInt(reponseData.get("responseCode")) == 208
						&& reponseData.get("status").equalsIgnoreCase("Duplicate Project details : Project Name.")) {
					flag = 0;
				}

				if (flag == 1) {
					session.setAttribute("newProjectName", projectName);
					session.setAttribute("proCreateStatus", 1);
					logger.info("New application is Created. Redirected to View Application page ");
					return new ModelAndView("redirect:/viewproject");
				} else if (flag == 2) {
					session.setAttribute("duplicateProjectError", 2);
					logger.warn("Error : Test case prefix name already exists. Redirected to create Application page ");
					return new ModelAndView("redirect:/createproject");
				} else if (flag == 3) {
					session.setAttribute("duplicateProjectError", 3);
					logger.warn("Error : Bug prefix name already exists. Redirected to create Application page ");
					return new ModelAndView("redirect:/createproject");
				} else if (flag == 0) {
					session.setAttribute("duplicateProjectError", 1);
					logger.warn("Error : Application name already exists. Redirected to create Application page ");
					return new ModelAndView("redirect:/createproject");
				} else {
					session.setAttribute("duplicateProjectError", -1);
					logger.warn("Error : Application insertion failed. Redirected to create Application page ");
					return new ModelAndView("redirect:/createproject");
				}

			} else {

				return new ModelAndView("redirect:/500");
			}
		}
	}

	@RequestMapping(value = "/insertproject", method = RequestMethod.POST, params = { "projectName", "bugPrefix",
			"tcPrefix", "projectDescription", "projectStatus" })
	public ModelAndView insertProject(@RequestParam("projectName") String projectName,
			@RequestParam("bugPrefix") String bugPrefix, @RequestParam("tcPrefix") String tcPrefix,
			@RequestParam("projectDescription") String projectDescription,
			@RequestParam("projectStatus") String proStatus, Model model, HttpSession session) throws Exception {

		logger.info("Post method to insert an application. Checking for active session");

		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			Map<String, String> reponseData = new HashMap<String, String>();

			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to create an application");

			if (buildDAO.checkBMRoleAccess("createproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to create application");
				int flag = -1;
				reponseData = projectService.insertProject(projectName, projectDescription, bugPrefix, tcPrefix,
						proStatus, schema, utilsService.getCurrentDateTime());
				logger.info("responce data is" + "code is" + reponseData.get("responseCode"));
				logger.info("responce status is" + "status is" + reponseData.get("status"));
				if (Integer.parseInt(reponseData.get("responseCode")) == 201
						&& reponseData.get("status").equalsIgnoreCase("Project Created successfully.")) {
					logger.info("project created successfully" + "flag is" + flag);
					flag = 1;
					if (session.getAttribute("redwood_url") != null) {
						try {
							String redWoodUrl = session.getAttribute("redwood_url").toString();
							RedwoodManagement redwoodManagement = new RedwoodManagement();
							redwoodManagement.createProject(redWoodUrl, projectName);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} else if (Integer.parseInt(reponseData.get("responseCode")) == 208
						&& reponseData.get("status").equalsIgnoreCase("Duplicate Project details : Test Name.")) {
					flag = 2;
				} else if (Integer.parseInt(reponseData.get("responseCode")) == 208
						&& reponseData.get("status").equalsIgnoreCase("Duplicate Project details : Bug Prifix.")) {
					flag = 3;
				} else if (Integer.parseInt(reponseData.get("responseCode")) == 208
						&& reponseData.get("status").equalsIgnoreCase("Duplicate Project details : Project Name.")) {
					logger.info("Duplicate project name" + "flag is" + flag);
					flag = 0;
				}

				if (flag == 1) {
					session.setAttribute("newProjectName", projectName);
					session.setAttribute("proCreateStatus", 1);
					Map<String, Object> projectDetailMap = new HashMap<String, Object>();
					projectDetailMap.put("project_id", reponseData.get("projectId"));
					projectDetailMap.put("project_name", projectName);
					((List<Map<String, Object>>) session.getAttribute("projectListSession")).add(projectDetailMap);
					logger.info("New application is Created. Redirected to View Application page ");
					return new ModelAndView("redirect:/viewproject");
				} else if (flag == 2) {
					session.setAttribute("duplicateProjectError", 2);
					logger.warn("Error : Test case prefix name already exists. Redirected to create Application page ");
					return new ModelAndView("redirect:/createproject");
				} else if (flag == 3) {
					session.setAttribute("duplicateProjectError", 3);
					logger.warn("Error : Bug prefix name already exists. Redirected to create Application page ");
					return new ModelAndView("redirect:/createproject");
				} else if (flag == 0) {
					session.setAttribute("duplicateProjectError", 1);
					logger.warn("Error : Application name already exists. Redirected to create Application page ");
					// return new ModelAndView("redirect:/viewproject");
					return new ModelAndView("redirect:/createproject");
				} else {
					session.setAttribute("duplicateProjectError", -1);
					logger.warn("Error : Application insertion failed. Redirected to create Application page ");
					return new ModelAndView("redirect:/createproject");
				}

			} else {

				return new ModelAndView("redirect:/500");
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method assignProject GET Request
	 * @param model
	 * @param session
	 * @return Model
	 *         (projectNameForAssign,unAssignedUsersList,assignedUsersList) &
	 *         View (onSuccess - assignproject , onFailure - logout/404)
	 * @throws UnsupportedEncodingException
	 * @Description this method gets the application ID from the session. Based
	 *              on that application ID , Application assigned and unassigned
	 *              users list is retrieved. Along with this data user is
	 *              redirected to Assign application page
	 */
	@RequestMapping(value = "/assignproject")
	public ModelAndView assignProject(Model model, HttpSession session) throws UnsupportedEncodingException {
		logger.info("Redirecting to assign Application page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to assign applications");
			if (buildDAO.checkBMRoleAccess("assignproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to assign applications");
				session.setAttribute("menuLiText", "Applications");

				int projectId = Integer.parseInt(session.getAttribute("projectIDForAssign").toString());

				Map<String, Object> projectData = projectDAO.getAssignProjectData(projectId, schema);

				List<Map<String, Object>> unAssignedUsersList = (List<Map<String, Object>>) projectData
						.get("#result-set-1");
				for (int i = 0; i < unAssignedUsersList.size(); i++) {
					String photo = loginService.getImage((byte[]) unAssignedUsersList.get(i).get("user_photo"));
					unAssignedUsersList.get(i).put("photo", photo);
				}

				List<Map<String, Object>> assignedUsersList = (List<Map<String, Object>>) projectData
						.get("#result-set-2");
				for (int i = 0; i < assignedUsersList.size(); i++) {
					String photo = loginService.getImage((byte[]) assignedUsersList.get(i).get("user_photo"));
					assignedUsersList.get(i).put("photo", photo);
				}
				model.addAttribute("projectNameForAssign", session.getAttribute("projectNameForAssign").toString());
				model.addAttribute("unAssignedUsersList", unAssignedUsersList);
				model.addAttribute("assignedUsersList", assignedUsersList);

				logger.info(" User is redirected to Assign Application page ");
				return new ModelAndView("assignproject", "model", model);
			} else {
				logger.warn("User has no access to assign application . Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method setAssignProjectID GET Request
	 * @param model
	 * @param session
	 * @param projectID
	 * @param projectName
	 * @return
	 * @throws Exception
	 * @Description This method saves the application ID for assigning
	 *              application
	 */
	@RequestMapping(value = "/setassignprojectid", method = RequestMethod.POST)
	public ModelAndView setAssignProjectID(Model model, HttpSession session,
			@RequestParam("projectID") String projectID, @RequestParam("projectName") String projectName)
			throws Exception {
		logger.info("Post method to save the Application id and name into session.Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			session.setAttribute("projectIDForAssign", projectID);
			session.setAttribute("projectNameForAssign", projectName);
			logger.info("User is redirected");
			return new ModelAndView("redirect:/");
		}
	}

	/**
	 * 
	 * @author prafulla.pol
	 * @method assignProjectToUser a POST Request
	 * @param model
	 * @param session
	 * @param selectedUsers
	 * @return true
	 * @throws Exception
	 * @Description This method accepts the user ID's as parameter from assign
	 *              application page. Assigns the application to selected users
	 *              or unassigns based on the selection. And send mail on
	 *              assignment.
	 */
	@RequestMapping(value = "/assignorunassignprojecttouser", method = RequestMethod.POST)
	public @ResponseBody String assignProjectToUser(Model model, HttpSession session,
			@RequestParam("selectedUsers") String selectedUsers) throws Exception {
		logger.info("Ajax Post request to assign or unassign Application to users");
		utilsService.getLoggerUser(session);
		int projectID = Integer.parseInt(session.getAttribute("projectIDForAssign").toString());
		String schema = session.getAttribute("cust_schema").toString();
		int user = Integer.parseInt(session.getAttribute("userId").toString());
		logger.info(" Session is active, userId is " + user + " Customer Schema " + schema);

		ArrayList<Integer> assignedUserIdArrayUI = new ArrayList<Integer>();

		Map<String, Object> projectData = projectDAO.assignOrUnassignProjectToUser(projectID, schema);

		List<Map<String, Object>> assignedUsersList = (List<Map<String, Object>>) projectData.get("#result-set-1");
		ArrayList<Integer> assignedUserIdArray = new ArrayList<Integer>();
		for (int i = 0; i < assignedUsersList.size(); i++) {
			assignedUserIdArray.add(Integer.parseInt(assignedUsersList.get(i).get("user_id").toString()));
		}
		List<Map<String, Object>> unassignedUsersList = (List<Map<String, Object>>) projectData.get("#result-set-4");
		ArrayList<Integer> unassignedUserIdArray = new ArrayList<Integer>();
		for (int i = 0; i < unassignedUsersList.size(); i++) {
			unassignedUserIdArray.add(Integer.parseInt(unassignedUsersList.get(i).get("user_id").toString()));
		}
		List<UserProjectDetails> userProjectDetailsList = new ArrayList<UserProjectDetails>();
		List<UserNotification> userNotificationsList = new ArrayList<UserNotification>();
		logger.info("Insert new assigned records");
		int userID;

		if (selectedUsers.length() > 0) {
			String[] tokens = selectedUsers.split(",");
			for (String token : tokens) {
				userID = Integer.parseInt(token);
				assignedUserIdArrayUI.add(userID);
			}
		}
		unassignedUserIdArray.retainAll(assignedUserIdArrayUI);
		logger.info("Assigned list  = " + unassignedUserIdArray);

		assignedUserIdArray.removeAll(assignedUserIdArrayUI);
		logger.info("Un assigned list  = " + assignedUserIdArray);
		if (unassignedUserIdArray.size() > 0) {
			for (int i = 0; i < unassignedUserIdArray.size(); i++) {
				int userIdVal = unassignedUserIdArray.get(i);
				UserProjectDetails userProjectDetails = new UserProjectDetails();
				userProjectDetails.setUserId(userIdVal);
				userProjectDetails.setProjectId(projectID);
				userProjectDetails.setUserProjectId(0);
				userProjectDetailsList.add(userProjectDetails);
				userNotificationsList.add(notificationDAO.setUserNotificationValues(1,
						"Assigned project '" + session.getAttribute("projectNameForAssign").toString() + "'",
						"viewproject", userIdVal, Integer.parseInt(session.getAttribute("userId").toString()), 2, 1));

				/*
				 * projectDAO.sendMailOnAssignProject((List<Map<String,
				 * Object>>) projectData.get("#result-set-2"), (List<Map<String,
				 * Object>>) projectData.get("#result-set-3"), projectID,
				 * userIdVal, schema);
				 */
			}
			logger.info("Inserting assigned projects");
			projectDAO.insertAssignedProjectRecords(userProjectDetailsList, schema);
			logger.info("Inserting notifications assign project");
			notificationDAO.insertNotificationRecords(userNotificationsList, schema);
		}
		if (assignedUserIdArray.size() > 0) {
			logger.info("Deleting un assigned records");
			projectDAO.deleteUnassignedProjectRecords(assignedUserIdArray, projectID, schema);

			for (int j = 0; j < assignedUserIdArray.size(); j++) {
				int userIdVal = assignedUserIdArray.get(j);
				UserNotification userNotification = notificationDAO.setUserNotificationValues(2,
						"Un-assigned project '" + session.getAttribute("projectNameForAssign").toString() + "'",
						"viewproject", userIdVal, Integer.parseInt(session.getAttribute("userId").toString()), 2, 1);
				userNotificationsList.add(userNotification);
			}
			logger.info("Inserting notifications un assign project");
			notificationDAO.insertNotificationRecords(userNotificationsList, schema);
		}

		logger.info("Returning to ajax post request");
		return "1";
	}

	@RequestMapping(value = "/setapplicationid", method = RequestMethod.POST)
	public @ResponseBody String setApplicationId(Model model, HttpSession session,
			@RequestParam("applicationID") int applicationID) throws Exception {

		logger.info("Ajax Post request to set application ID into the session");
		utilsService.getLoggerUser(session);
		session.setAttribute("sessionApplicationID", applicationID);
		session.setAttribute("actionForTab", "edit");
		logger.info("Returning from ajax post request");

		return "true";
	}

	@RequestMapping(value = "/setmoduleapplicationid", method = RequestMethod.POST)
	public @ResponseBody String setModuleApplicationId(Model model, HttpSession session,
			@RequestParam("applicationID") int applicationID) throws Exception {

		logger.info("Ajax Post request to set application ID into the session");
		utilsService.getLoggerUser(session);
		session.setAttribute("moduleSessionApplicationID", applicationID);
		logger.info("Returning from ajax post request");

		return "true";
	}

	/**
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/applicationsettings")
	public ModelAndView applicationSettings(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Application Settings page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();

			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to view application");
			if (buildDAO.checkBMRoleAccess("viewproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to view applications");

				session.setAttribute("menuLiText", "Applications");

				logger.info(" User is redirected to Application Settings page ");

				int applicationID = Integer.parseInt(session.getAttribute("sessionApplicationID").toString());

				Map<String, Object> appData = projectDAO.getApplicationSettingsData(applicationID, schema);
				List<Map<String, Object>> applicationData = (List<Map<String, Object>>) appData.get("#result-set-1");
				model.addAttribute("applicationData", applicationData);

				List<Map<String, Object>> assignedUsersList = (List<Map<String, Object>>) appData.get("#result-set-2");
				for (int i = 0; i < assignedUsersList.size(); i++) {
					String photo = loginService.getImage((byte[]) assignedUsersList.get(i).get("user_photo"));
					assignedUsersList.get(i).put("photo", photo);
				}
				model.addAttribute("assignedUsersList", assignedUsersList);
				boolean jiraStatus = jiraIntegrationService
						.isJiraConnected(session.getAttribute("customerId").toString());
				if (jiraStatus) {
					model.addAttribute("jiraStatus", "y");
				} else {
					model.addAttribute("jiraStatus", "n");
				}
				List<Map<String, Object>> unassignedUsersList = (List<Map<String, Object>>) appData
						.get("#result-set-3");
				model.addAttribute("unassignedUsersList", unassignedUsersList);

				logger.info(" User is redirected to Application Settings page ");
				return new ModelAndView("applicationsettings", "model", model);
			} else {
				logger.warn("User has no access to view applications . Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}
		}
	}

	@RequestMapping(value = "/updateapplication", method = RequestMethod.POST)
	public ModelAndView updateApplication(@RequestParam("projectId") int projectId,
			@RequestParam("projectName") String projectName,
			@RequestParam("projectDescription") String projectDescription,
			@RequestParam("projectStatus") int projectStatus, Model model, HttpSession session) {
		logger.info("Post method to update application.Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();

			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to Edit an application");

			if (buildDAO.checkBMRoleAccess("editproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to Edit application");

				int flag = projectService.updateProject(projectId, projectDescription, projectStatus, schema);

				if (flag == 1) {
					session.setAttribute("newProjectName", projectName);
					session.setAttribute("proCreateStatus", 2);
					session.setAttribute("updatedAppName", projectName);
					session.setAttribute("actionForTab", "edit");
					logger.info(" User is redirected to Applications settings page ");
					return new ModelAndView("redirect:/viewproject");
				} else {
					logger.warn("Error : Error occured while updating application");
					return new ModelAndView("redirect:/500");
				}

			} else {
				logger.warn("User has no access to Edit application. Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}
		}
	}

	@RequestMapping(value = "/unassignapptouser", method = RequestMethod.POST)
	public @ResponseBody String unAssignUser(Model model, HttpSession session, @RequestParam("appID") int app,
			@RequestParam("userID") int user, @RequestParam("appName") String appName) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return "1";
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();

			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to assign an application");

			if (buildDAO.checkBMRoleAccess("assignproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to assign application");

				int status = userService.unAssignProjectsToUser(app, appName, user, schema, userID);
				if (status == 200) {
					logger.info(" User is redirected to Application Settings page ");
					session.setAttribute("actionForTab", "members");
					// for the document library
					session.removeAttribute("uploadProjectId");
					session.removeAttribute("uploadProjectName");
					return "200";
				} else {
					return "500";
				}
			} else {
				logger.warn("User has no access to assign application. Redirected to 404 page");
				return "404";
			}
		}
	}

	@RequestMapping(value = "/assignapptousers", method = RequestMethod.POST)
	public ModelAndView assignAppToUsers(@RequestParam("appId") int appId, @RequestParam("appName") String appName,
			@RequestParam("users") String users, Model model, HttpSession session) throws Exception {
		logger.info("Post method to assign application to users. Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);

			logger.info("Cheking for User's access to assign application");
			if (buildDAO.checkBMRoleAccess("assignproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to assign application");

				projectService.assignProjectToUsers(appId, appName, users, userID, schema);

				session.setAttribute("actionForTab", "members");
				logger.info(" User is redirected to Applications settings page ");
				return new ModelAndView("redirect:/applicationsettings");

			} else {
				logger.warn("User has no access to Edit application. Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}
		}
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		return new ModelAndView("redirect:/500");
	}

}

package com.xenon.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.common.UtilsService;

/**
 * 
 * @author suresh.adling
 * @description Handles requests for the application file upload requests
 * 
 */

@Controller
public class FileUploadController {

	private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);
	
	@Resource(name = "configurationProperties")
	private Properties configurationProperties;
	
	@Autowired
	UtilsService utilsService;
	

	/**
	 * @author suresh.adling
	 * @method Upload single file using Spring Controller
	 * @param file
	 * @return
	 */
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public @ResponseBody String uploadFileHandler(@RequestParam("file") MultipartFile file) {
		logger.info("Redirecting to uploadFile page ");
		logger.info("Checking for empty file ");
		String name = "";
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				name = file.getOriginalFilename().split("\\.")[0] + "." + file.getOriginalFilename().split("\\.")[1];
				// Creating the directory to store file
				String rootPath = configurationProperties.getProperty("XENON_REPOSITORY_PATH")+"repository"+File.separator+"upload";
				org.springframework.core.io.Resource resource = new ClassPathResource(rootPath + File.separator + "tmpFiles");
				File dir = resource.getFile();
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath() + File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location=" + serverFile.getAbsolutePath());
				logger.info("You successfully uploaded file=" + name);
				return "You successfully uploaded file=" + name;
			} catch (Exception e) {
				logger.error("You failed to upload " + name + " => " + e.getMessage());
				return "You failed to upload " + name + " => " + e.getMessage();

			}
		} else {
			logger.error("You failed to upload " + name + " because the file was empty.");
			return "You failed to upload " + name + " because the file was empty.";
		}
	}

	/**
	 * @author suresh.adling
	 * @method Upload multiple file using Spring Controller
	 * @param names
	 * @param files
	 * @return
	 */
	@RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST)
	public @ResponseBody String uploadMultipleFileHandler(@RequestParam("name") String[] names,
			@RequestParam("file") MultipartFile[] files) {
		logger.info("Redirecting to upload Muliple Files page ");
		logger.info("Checking for empty file ");
		if (files.length != names.length)
			return "Mandatory information missing";

		String message = "";
		for (int i = 0; i < files.length; i++) {
			MultipartFile file = files[i];
			String name = names[i];
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath() + File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location=" + serverFile.getAbsolutePath());
				logger.info(message + "You successfully uploaded file=" + name + "<br />");
				message = message + "You successfully uploaded file=" + name + "<br />";
			} catch (Exception e) {
				logger.error("You failed to upload " + name + " => " + e.getMessage());
				return "You failed to upload " + name + " => " + e.getMessage();
			}
		}
		return message;
	}
	/**
	 * @author suresh.adling
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :"+ex);
		return new ModelAndView("redirect:/500");
	}
}
package com.xenon.controller;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.administration.ProjectService;
import com.xenon.api.common.CommonService;
import com.xenon.api.common.JiraIntegrationService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.api.testmanager.TestCaseService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.testmanager.dao.ScenarioDAO;
import com.xenon.testmanager.dao.TestSpecificationDAO;
import com.xenon.testmanager.dao.TestcaseCommentsDAO;
import com.xenon.testmanager.dao.TestcaseDAO;
import com.xenon.testmanager.dao.TmDashboardDAO;

import net.rcarz.jiraclient.Issue;
import net.rcarz.jiraclient.JiraClient;

/**
 * 
 * @author suresh.adling
 * @description Controller class representing views for test case management
 * 
 */

@SuppressWarnings({ "unchecked" })
@Controller
public class TestSpecificationController {

	@Autowired
	TestSpecificationDAO testSpecificationDao;

	@Autowired
	ModuleDAO moduleDao;

	@Autowired
	ScenarioDAO scenarioDAO;

	@Autowired
	TestcaseDAO testCaseDao;

	@Autowired
	TestcaseCommentsDAO testcaseCommentsDAO;

	@Autowired
	ProjectDAO projectDao;

	@Autowired
	LoginService loginService;

	@Autowired
	UtilsService utilsService;

	@Autowired
	CommonService commonService;

	@Autowired
	RoleAccessService roleAccessService;

	@Autowired
	BuildDAO buildDAO;

	@Autowired
	TmDashboardDAO tmDashDAO;

	@Autowired
	TestCaseService testCaseService;

	@Autowired
	JiraIntegrationService jiraIntegrationService;

	@Autowired
	ProjectService projectService;

	private static final Logger logger = LoggerFactory.getLogger(TestCaseController.class);

	public static Issue.SearchResult sr = null;

	/**
	 * @author suresh.adling
	 * @method getModuleList GET Request
	 * @output get test specification view
	 * @description test specification view gives list of modules for selected
	 *              project and test case counts for each
	 * @param model
	 * @param session
	 * @return Model & View (onSuccess - testspecification ,onFailure -
	 *         logout/500)
	 * @throws Exception
	 */
	@RequestMapping(value = "/testspecification", method = RequestMethod.GET)
	public ModelAndView getModuleList(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to test specification page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (tmDashDAO.checkTMRoleAccess("testspecification", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			/*
			 * model = common.getHeaderValues(model, session); if
			 * (model.containsAttribute("noProject")) { model.asMap().clear();
			 * return new ModelAndView("redirect:/xedashboard"); }
			 */
			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if (appCount == 0) {
				return new ModelAndView("redirect:/xedashboard");
			}

			String project = session.getAttribute("UserCurrentProjectId").toString();
			int projectId = Integer.parseInt(project);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			boolean jiraStatus = jiraIntegrationService.isJiraConnected(session.getAttribute("customerId").toString());
			if (jiraStatus) {
				int projectID = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
				System.out.println(projectID);
				List<Map<String, Object>> projectt = projectService.getSingleProject(projectID,
						session.getAttribute("cust_schema").toString());
				String jiraProject = projectt.get(0).get("issue_tracker_project_key").toString();
				logger.info("Jira Project  : " + jiraProject);
				JiraClient jira = jiraIntegrationService.jiraConnect(session);
				sr = jira.searchIssues(
						"project = " + jiraProject
								+ " AND issuetype=Story AND sprint in openSprints() ORDER BY priority DESC, updated DESC",
						20);
				Map<String, String> jiraMap = new HashMap<String, String>();
				for (Issue i : sr.issues) {
					logger.info("Issue Key  : " + i.getKey());
					logger.info("Issue Summary  : " + i.getSummary());
					jiraMap.put(i.getKey(), i.getSummary());
				}
				Set<String> jiraSet = new HashSet<String>();
				for (Map.Entry<String, String> entry : jiraMap.entrySet()) {
					String key = entry.getKey();
					String value = entry.getValue();
					jiraSet.add(String.join(" ", key, value));
				}
				model.addAttribute("jiraId", jiraSet);
				model.addAttribute("jiraStatus", "y");
			} else {
				model.addAttribute("jiraStatus", "n");
			}
			logger.info("Session is active, userId is " + userId + " and projectId is " + project + " Customer Schema "
					+ schema);
			Map<String, Object> data = testSpecificationDao.getTestSpecification(projectId, userId, schema);
			List<Map<String, Object>> moduleLsit = (List<Map<String, Object>>) data.get("#result-set-2");
			List<Map<String, Object>> scenarioList = (List<Map<String, Object>>) data.get("#result-set-3");

			if (buildDAO.checkBMRoleAccess("createmodule", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				model.addAttribute("createmodule", 1);
			}
			if (buildDAO.checkBMRoleAccess("editmodule", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				model.addAttribute("editmodule", 1);
			}
			if (buildDAO.checkBMRoleAccess("createproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				model.addAttribute("createproject", 1);
			}
			int flag = scenarioDAO.createNewScenarioStatus(session.getAttribute("cust_schema").toString(),
					Integer.parseInt(session.getAttribute("cust_type_id").toString()));

			model.addAttribute("scenarioError", "0");
			model.addAttribute("createNewScenarioStatus", flag);
			model.addAttribute("projectDetails", data.get("#result-set-1"));
			model.addAttribute("moduleList", moduleLsit);
			model.addAttribute("scenarioList", utilsService.convertListOfMapToJson(scenarioList));
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			logger.info(" User is redirected to testspecification page ");
			return new ModelAndView("testspecification", "Model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	@RequestMapping(value = "/insertFilterQuery")
	public @ResponseBody ModelAndView Filtering(@RequestParam String inputValue, @RequestParam String commonData,
			HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			testSpecificationDao.saveFilterValues(inputValue, commonData,userId,schema);
			return new ModelAndView("redirect:/tmdashboardlink");
		}

	}

	/**
	 * @author suresh.adling
	 * @method getTestCaseList GET Request
	 * @output get test case view
	 * @description test case view gives list of test cases for selected
	 *              scenario
	 * @param model
	 * @param session
	 * @return Model & View (onSuccess - testcase ,onFailure - logout/500)
	 * @throws Exception
	 */
	@RequestMapping(value = "/testcase", method = RequestMethod.GET)
	public ModelAndView getTestCaseList(@RequestParam(value = "page", defaultValue = "1") int page, Model model,
			HttpSession session) throws Exception {
		List<Map<String, Object>> scenarioDetails = new ArrayList<Map<String, Object>>();
		logger.info("Redirecting to test case page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (tmDashDAO.checkTMRoleAccess("testcase", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);

			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if (appCount == 0) {
				return new ModelAndView("redirect:/xedashboard");
			}
			try {
				String project = session.getAttribute("UserCurrentProjectId").toString();
				int projectId = Integer.parseInt(project);
				int userId = Integer.parseInt(session.getAttribute("userId").toString());
				String schema = session.getAttribute("cust_schema").toString();
				int moduleId = 0;
				String moduleName = "NA";
				try {
					moduleId = Integer.parseInt(session.getAttribute("selectedTestModuleId").toString());
					moduleName = session.getAttribute("selectedTestModuleName").toString();
					if (moduleId == 0) {
						List<Map<String, Object>> data = testSpecificationDao.getLastModule(projectId, userId, schema);
						if (data != null && data.size() != 0) {
							moduleId = (Integer) data.get(0).get("module_id");
							moduleName = data.get(0).get("module_name").toString();
							session.setAttribute("selectedTestModuleName", moduleName);
							session.setAttribute("selectedTestModuleId", moduleId);
						}
					}
				} catch (Exception e) {
					List<Map<String, Object>> data = testSpecificationDao.getLastModule(projectId, userId, schema);
					if (data != null && data.size() != 0) {
						moduleId = (Integer) data.get(0).get("module_id");
						moduleName = data.get(0).get("module_name").toString();
						session.setAttribute("selectedTestModuleName", moduleName);
						session.setAttribute("selectedTestModuleId", moduleId);
					}

				}
				int scenarioId = 0;
				String scenario_name = null;
				try {
					scenarioId = Integer.parseInt(session.getAttribute("selectedScenarioId").toString());
					if (scenarioId == 0) {
						List<Map<String, Object>> data = testSpecificationDao.getLastScenario(moduleId, schema);
						if (data != null && data.size() != 0) {
							scenarioId = (Integer) data.get(0).get("scenario_id");
							scenario_name = (String) data.get(0).get("scenario_name");
							session.setAttribute("selectedScenarioName", scenario_name);
							session.setAttribute("selectedScenarioId", scenarioId);
						}
					}
				} catch (Exception e) {
					List<Map<String, Object>> data = testSpecificationDao.getLastScenario(moduleId, schema);
					if (data != null && data.size() != 0) {
						scenarioId = (Integer) data.get(0).get("scenario_id");
						scenario_name = (String) data.get(0).get("scenario_name");
						session.setAttribute("selectedScenarioName", scenario_name);
						session.setAttribute("selectedScenarioId", scenarioId);
					}
				}
				int pageSize = 1000;
				int startValue = (page - 1) * pageSize;
				
				List<Map<String, Object>> testCaseList1 = new ArrayList<Map<String, Object>>();

				logger.info("Session is active, userId is " + userId + " , projectId is " + project
						+ " Customer Schema " + schema);
				/*if (projectId != 0 && moduleId != 0 && scenarioId != 0) { */
				if (projectId != 0 && moduleId != 0 ) { 
					Map<String, Object> data = testSpecificationDao.getTestCaseData(userId, projectId, moduleId,
							scenarioId, schema, startValue, pageSize);

					// For the left menu scenario list
					List<Map<String, Object>> scenarioList = (List<Map<String, Object>>) data.get("#result-set-1");
					for (int i = 0; i < scenarioList.size(); i++) {
						if (Integer.parseInt(scenarioList.get(i).get("scenario_id").toString()) == scenarioId) {
							scenarioDetails.add(scenarioList.get(i));
							break;
						}
					}
					model.addAttribute("scenarioList", utilsService.convertListOfMapToJson(scenarioList));

					// For the test case table
					List<Map<String, Object>> testCaseList = (List<Map<String, Object>>) data.get("#result-set-2");
					
					for (int i = 0; i < testCaseList.size(); i++) {
						if (!testCaseList.get(i).get("execution_type").toString().equals("4")) {

							testCaseList1.add(testCaseList.get(i));
						    
						         
					       }
                         
						String logo = loginService.getImage((byte[]) testCaseList.get(i).get("user_photo"));
						testCaseList.get(i).put("user_photo", logo);

					}
					model.addAttribute("testCaseList", testCaseList1);
					

					String userFulName = session.getAttribute("userFirstname").toString() + " "
							+ session.getAttribute("userLastname").toString();

					// For the left side menu module list
					List<Map<String, Object>> moduleList = (List<Map<String, Object>>) data.get("#result-set-3");
					model.addAttribute("moduleList", moduleList);

					// For time line activity
					List<Map<String, Object>> testCaseSummary = (List<Map<String, Object>>) data.get("#result-set-4");
					for (int i = 0; i < testCaseSummary.size(); i++) {
						testCaseSummary.get(i).put("activity_date",
								utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
										utilsService.getDate(testCaseSummary.get(i).get("activity_date").toString())));
					}

					// For test data list
					List<Map<String, Object>> testdatas = (List<Map<String, Object>>) data.get("#result-set-5");
					model.addAttribute("testdatas", testdatas);

					List<Map<String, Object>> executionTypes = (List<Map<String, Object>>) data.get("#result-set-6");
					model.addAttribute("executionTypes", executionTypes);
					List<Map<String, Object>> statusList = (List<Map<String, Object>>) data.get("#result-set-7");
					model.addAttribute("statusList", statusList);
					// System.out.println("testdatas:"+testdatas);
					int allTcCount = (Integer) data.get("count");

					model.addAttribute("allTcCount", allTcCount);
					model.addAttribute("pageSize", pageSize);
					model.addAttribute("scenarioDetails", scenarioDetails);
					model.addAttribute("testCaseSummary", testCaseSummary);
					model.addAttribute("moduleId", moduleId);
					model.addAttribute("moduleName", moduleName);
					model.addAttribute("userFullName", userFulName);
					model.addAttribute("nowDate",
							utilsService.getNowDateOfTimezone(session.getAttribute("userTimezone").toString()));
					model.addAttribute("prevDate",
							utilsService.getPrevDateOfTimezone(session.getAttribute("userTimezone").toString()));
					model.addAttribute("UserProfilePhoto",
							loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
					model.addAttribute("userName", session.getAttribute("userFullName"));
					model.addAttribute("role", session.getAttribute("role"));
					logger.info(" User is redirected to testspecification page ");
					return new ModelAndView("testcase", "Model", model);
				} else {
					return new ModelAndView("testcase", "Model", model);
				}
			} catch (Exception ex) {
				return new ModelAndView("testcase", "Model", model);

			}
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	@RequestMapping(value = "/testcaseApi", method = RequestMethod.GET)
	public ModelAndView getTestCaseListApi(@RequestParam(value = "page", defaultValue = "1") int page, Model model,
			HttpSession session) throws Exception {
		List<Map<String, Object>> scenarioDetails = new ArrayList<Map<String, Object>>();
		logger.info("Redirecting to test case page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (tmDashDAO.checkTMRoleAccess("testcase", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);

			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if (appCount == 0) {
				return new ModelAndView("redirect:/xedashboard");
			}
			try {
				String project = session.getAttribute("UserCurrentProjectId").toString();
				int projectId = Integer.parseInt(project);
				int userId = Integer.parseInt(session.getAttribute("userId").toString());
				String schema = session.getAttribute("cust_schema").toString();
				int moduleId = 0;
				String moduleName = "NA";
				try {
					moduleId = Integer.parseInt(session.getAttribute("selectedTestModuleId").toString());
					moduleName = session.getAttribute("selectedTestModuleName").toString();
					if (moduleId == 0) {
						List<Map<String, Object>> data = testSpecificationDao.getLastModule(projectId, userId, schema);
						if (data != null && data.size() != 0) {
							moduleId = (Integer) data.get(0).get("module_id");
							moduleName = data.get(0).get("module_name").toString();
							session.setAttribute("selectedTestModuleName", moduleName);
							session.setAttribute("selectedTestModuleId", moduleId);
						}
					}
				} catch (Exception e) {
					List<Map<String, Object>> data = testSpecificationDao.getLastModule(projectId, userId, schema);
					if (data != null && data.size() != 0) {
						moduleId = (Integer) data.get(0).get("module_id");
						moduleName = data.get(0).get("module_name").toString();
						session.setAttribute("selectedTestModuleName", moduleName);
						session.setAttribute("selectedTestModuleId", moduleId);
					}

				}
				int scenarioId = 0;
				String scenario_name = null;
				try {
					scenarioId = Integer.parseInt(session.getAttribute("selectedScenarioId").toString());
					if (scenarioId == 0) {
						List<Map<String, Object>> data = testSpecificationDao.getLastScenario(moduleId, schema);
						if (data != null && data.size() != 0) {
							scenarioId = (Integer) data.get(0).get("scenario_id");
							scenario_name = (String) data.get(0).get("scenario_name");
							session.setAttribute("selectedScenarioName", scenario_name);
							session.setAttribute("selectedScenarioId", scenarioId);
						}
					}
				} catch (Exception e) {
					List<Map<String, Object>> data = testSpecificationDao.getLastScenario(moduleId, schema);
					if (data != null && data.size() != 0) {
						scenarioId = (Integer) data.get(0).get("scenario_id");
						scenario_name = (String) data.get(0).get("scenario_name");
						session.setAttribute("selectedScenarioName", scenario_name);
						session.setAttribute("selectedScenarioId", scenarioId);
					}
				}
				int pageSize = 1000;
				int startValue = (page - 1) * pageSize;
				
				List<Map<String, Object>> testCaseList1 = new ArrayList<Map<String, Object>>();

				logger.info("Session is active, userId is " + userId + " , projectId is " + project
						+ " Customer Schema " + schema);
				if (projectId != 0 && moduleId != 0 && scenarioId != 0) {
					Map<String, Object> data = testSpecificationDao.getTestCaseData(userId, projectId, moduleId,
							scenarioId, schema, startValue, pageSize);

					// For the left menu scenario list
					List<Map<String, Object>> scenarioList = (List<Map<String, Object>>) data.get("#result-set-1");
					for (int i = 0; i < scenarioList.size(); i++) {
						if (Integer.parseInt(scenarioList.get(i).get("scenario_id").toString()) == scenarioId) {
							scenarioDetails.add(scenarioList.get(i));
							break;
						}
					}
					model.addAttribute("scenarioList", utilsService.convertListOfMapToJson(scenarioList));

					// For the test case table
					List<Map<String, Object>> testCaseList = (List<Map<String, Object>>) data.get("#result-set-2");
					//int a = 0;
					for (int i = 0; i < testCaseList.size(); i++) {
						if (testCaseList.get(i).get("execution_type").toString().equals("4")) {

							testCaseList1.add(testCaseList.get(i));
						    //break;
						         
					       }
                         
						String logo = loginService.getImage((byte[]) testCaseList.get(i).get("user_photo"));
						testCaseList.get(i).put("user_photo", logo);

					}
					model.addAttribute("testCaseList", testCaseList1);

					String userFulName = session.getAttribute("userFirstname").toString() + " "
							+ session.getAttribute("userLastname").toString();

					// For the left side menu module list
					List<Map<String, Object>> moduleList = (List<Map<String, Object>>) data.get("#result-set-3");
					model.addAttribute("moduleList", moduleList);

					// For time line activity
					List<Map<String, Object>> testCaseSummary = (List<Map<String, Object>>) data.get("#result-set-4");
					for (int i = 0; i < testCaseSummary.size(); i++) {
						testCaseSummary.get(i).put("activity_date",
								utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
										utilsService.getDate(testCaseSummary.get(i).get("activity_date").toString())));
					}

					// For test data list
					List<Map<String, Object>> testdatas = (List<Map<String, Object>>) data.get("#result-set-5");
					model.addAttribute("testdatas", testdatas);

					List<Map<String, Object>> executionTypes = (List<Map<String, Object>>) data.get("#result-set-6");
					model.addAttribute("executionTypes", executionTypes);
					List<Map<String, Object>> statusList = (List<Map<String, Object>>) data.get("#result-set-7");
					model.addAttribute("statusList", statusList);
					// System.out.println("testdatas:"+testdatas);
					int allTcCount = (Integer) data.get("count");

					model.addAttribute("allTcCount", allTcCount);
					model.addAttribute("pageSize", pageSize);
					model.addAttribute("scenarioDetails", scenarioDetails);
					model.addAttribute("testCaseSummary", testCaseSummary);
					model.addAttribute("moduleId", moduleId);
					model.addAttribute("moduleName", moduleName);
					model.addAttribute("userFullName", userFulName);
					model.addAttribute("nowDate",
							utilsService.getNowDateOfTimezone(session.getAttribute("userTimezone").toString()));
					model.addAttribute("prevDate",
							utilsService.getPrevDateOfTimezone(session.getAttribute("userTimezone").toString()));
					model.addAttribute("UserProfilePhoto",
							loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
					model.addAttribute("userName", session.getAttribute("userFullName"));
					model.addAttribute("role", session.getAttribute("role"));
					logger.info(" User is redirected to testspecification page ");
					return new ModelAndView("testcase", "Model", model);
				} else {
					return new ModelAndView("testcase", "Model", model);
				}
			} catch (Exception ex) {
				return new ModelAndView("testcase", "Model", model);

			}
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	// Anmol Chadha
	@RequestMapping(value = "/viewtmtrash", method = RequestMethod.GET)
	public ModelAndView viewBuildTrash(HttpSession session, Model model, HttpServletRequest request) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			logger.info("Uploading document");
			String schema = session.getAttribute("cust_schema").toString();
			String project = session.getAttribute("UserCurrentProjectId").toString();
			int projectId = Integer.parseInt(project);
			List<Map<String, Object>> dataModuleTrash = testSpecificationDao.getModuleTrash(schema, projectId);
			model.addAttribute("dataModuleTrash", dataModuleTrash);
			List<Map<String, Object>> dataScenarioTrash = testSpecificationDao.getScenarioTrash(schema, projectId);
			model.addAttribute("dataScenarioTrash", dataScenarioTrash);

			List<Map<String, Object>> dataTestcaseTrash = testSpecificationDao.getTestCaseTrash(schema, projectId);
			model.addAttribute("dataTestcaseTrash", dataTestcaseTrash);

			return new ModelAndView("viewtmtrash", "Model", model);
		}
	}

	// Anmol Chadha
	@RequestMapping(value = "/restoreTestcase")
	public @ResponseBody ModelAndView restoreTestcase(@RequestParam int testcaseId, HttpSession session)
			throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");

		} else {
			utilsService.getLoggerUser(session);

			String schema = session.getAttribute("cust_schema").toString();

			testSpecificationDao.restoreTestcaseForTrash(testcaseId, schema);

		}
		return new ModelAndView("redirect:/viewtmtrash");

	}

	@RequestMapping(value = "/uploaddatasheet", method = RequestMethod.POST)
	public ModelAndView uploadDatasheet(@RequestParam("dataUploadTestcaseId") String testcaseId,
			@RequestParam(value = "uploadDatasheet", required = false) MultipartFile file, HttpSession session,
			Model model, HttpServletRequest request) throws Exception {
		int userId = Integer.parseInt("" + session.getAttribute("userId"));
		String fileName = file.getOriginalFilename();
		byte[] bytes = new byte[0];
		if (!file.isEmpty())
			bytes = file.getBytes();
		String schema = session.getAttribute("cust_schema").toString();

		testSpecificationDao.uploadTestcaseDatasheet(Integer.parseInt("" + testcaseId), userId, bytes, fileName,
				schema);

		return new ModelAndView("redirect:/teststeps");
	}

	/**
	 * @author suresh.adling
	 * @method getTestStepList GET Request
	 * @output get test case steps view
	 * @description test test case steps view gives list of steps for current
	 *              and gives feature to edit test case details
	 * @param model
	 * @param session
	 * @return Model & View (onSuccess - teststeps ,onFailure - logout/500)
	 * @throws Exception
	 */
	@RequestMapping(value = "/teststeps", method = RequestMethod.GET)
	public ModelAndView getTestStepList(Model model, HttpSession session) throws Exception {
		List<Map<String, Object>> testcaseDetails = new ArrayList<Map<String, Object>>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-Y");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);

		logger.info("Redirecting to teststeps page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (tmDashDAO.checkTMRoleAccess("teststeps", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			/*
			 * model = common.getHeaderValues(model, session); if
			 * (model.containsAttribute("noProject")) { model.asMap().clear();
			 * return new ModelAndView("redirect:/xedashboard"); }
			 */
			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if (appCount == 0) {
				return new ModelAndView("redirect:/xedashboard");
			}

			String project = session.getAttribute("UserCurrentProjectId").toString();
			int projectId = Integer.parseInt(project);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			int moduleId = Integer.parseInt(session.getAttribute("selectedTestModuleId").toString());
			int scenarioId = Integer.parseInt(session.getAttribute("selectedScenarioId").toString());
			int testcaseId = Integer.parseInt(session.getAttribute("selectedTestcaseId").toString());
			String moduleName = session.getAttribute("selectedTestModuleName").toString();
			String viewType = session.getAttribute("testcaseViewType").toString();
			Map<String, Object> data = testSpecificationDao.getTestCaseStepData(userId, projectId, moduleId, scenarioId,
					testcaseId, schema);
			List<Map<String, Object>> scenarioList = (List<Map<String, Object>>) data.get("#result-set-1");
			List<Map<String, Object>> testCaseList = (List<Map<String, Object>>) data.get("#result-set-2");
			for (int i = 0; i < testCaseList.size(); i++) {
				if (Integer.parseInt(testCaseList.get(i).get("testcase_id").toString()) == testcaseId) {
					testcaseDetails.add(testCaseList.get(i));
					break;
				}
			}
			List<Map<String, Object>> testCaseSummary = (List<Map<String, Object>>) data.get("#result-set-10");
			for (int i = 0; i < testCaseSummary.size(); i++) {
				String photo = loginService.getImage((byte[]) testCaseSummary.get(i).get("user_photo"));
				testCaseSummary.get(i).put("user_photo", photo);
			}
			boolean automationFlag = buildDAO.checkBMRoleAccess("automationtestcase",
					session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString());
			List<Map<String, Object>> ExecutionTypes = (List<Map<String, Object>>) data.get("#result-set-4");
			for (int i = 0; i < ExecutionTypes.size(); i++) {

				int typeId = Integer.parseInt(ExecutionTypes.get(i).get("execution_type_id").toString());
				if ((!automationFlag)
						&& (typeId != Integer.parseInt(testcaseDetails.get(0).get("execution_type").toString()))) {
					ExecutionTypes.remove(i);
					i--;
				}
			}
			List<Map<String, Object>> testComments = (List<Map<String, Object>>) data.get("#result-set-6");
			List<Map<String, Object>> tcCommentLike = (List<Map<String, Object>>) data.get("#result-set-7");
			List<Map<String, Object>> tcCommentLikeCounts = (List<Map<String, Object>>) data.get("#result-set-8");

			for (int i = 0; i < testComments.size(); i++) {
				String logo = loginService.getImage((byte[]) testComments.get(i).get("user_photo"));
				testComments.get(i).put("commenterImage", logo);
				testComments.get(i).put("likeStatus", "2");
				for (int j = 0; j < tcCommentLike.size(); j++) {
					if (Integer.parseInt(testComments.get(i).get("xetm_testcase_comments_id").toString()) == Integer
							.parseInt(tcCommentLike.get(j).get("comment_id").toString())
							&& Integer.parseInt(tcCommentLike.get(j).get("liked_by_user").toString()) == userId) {
						testComments.get(i).put("likeStatus", "1");
						break;
					}

				}
				for (int j = 0; j < tcCommentLikeCounts.size(); j++) {
					testComments.get(i).put("likeCount", "0");
					if (Integer.parseInt(testComments.get(i).get("xetm_testcase_comments_id").toString()) == Integer
							.parseInt(tcCommentLikeCounts.get(j).get("comment_id").toString())) {
						testComments.get(i).put("likeCount",
								Integer.parseInt(tcCommentLikeCounts.get(j).get("likeCount").toString()));
						break;
					}
				}

				/*
				 * for(int k=0;k<testComments.size();k++) {
				 * testComments.get(k).put("comment_time",
				 * utils.getDateTimeOfTimezone(session.getAttribute(
				 * "userTimezone").toString(),
				 * utils.getDate(testComments.get(k).get("comment_time").
				 * toString()))); }
				 */
			}

			if (viewType.equals("View")) {
				model.addAttribute("recentFlag", 1);
			} else if (viewType.equals("Edit")) {
				model.addAttribute("recentFlag", 0);
			}
			List<Map<String, Object>> datasheetFileInformation = new ArrayList<Map<String, Object>>();
			if (testcaseDetails.get(0).get("excel_file_present_status").toString().equals("1")) {
				int tcId = Integer.parseInt("" + testcaseDetails.get(0).get("testcase_id").toString());
				datasheetFileInformation = testSpecificationDao.getDatasheetByTcId(tcId, schema);
				if (datasheetFileInformation != null && !datasheetFileInformation.isEmpty()
						&& datasheetFileInformation.get(0).containsKey("datasheet_filename")) {
					model.addAttribute("datasheetFileName",
							datasheetFileInformation.get(0).get("datasheet_filename").toString());
				}
			}
			model.addAttribute("moduleList", data.get("#result-set-9"));
			model.addAttribute("scenarioList", utilsService.convertListOfMapToJson(scenarioList));
			model.addAttribute("testCaseList", testCaseList);
			model.addAttribute("testcaseDetails", testcaseDetails);
			model.addAttribute("stepList", data.get("#result-set-3"));
			model.addAttribute("executionTypes", ExecutionTypes);
			model.addAttribute("statusList", data.get("#result-set-5"));
			model.addAttribute("testCaseSummary", testCaseSummary);
			model.addAttribute("moduleId", moduleId);
			model.addAttribute("moduleName", moduleName);
			model.addAttribute("scenarioId", scenarioId);
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			model.addAttribute("nowDate", dateFormat.format(date));
			model.addAttribute("prevDate", dateFormat.format(cal.getTime()));
			model.addAttribute("testComments", testComments);
			logger.info(" User is redirected to teststeps page ");
			return new ModelAndView("teststeps", "Model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	/**
	 * 
	 * @param tcPrefix
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/testcasebyprefix", method = RequestMethod.GET)
	public ModelAndView testcasebyprefix(@RequestParam("tcPrefix") String tcPrefix, Model model, HttpSession session)
			throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {

			utilsService.getLoggerUser(session);
			/*
			 * model = common.getHeaderValues(model, session); if
			 * (model.containsAttribute("noProject")) { model.asMap().clear();
			 * return new ModelAndView("redirect:/xedashboard"); }
			 */
			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if (appCount == 0) {
				return new ModelAndView("redirect:/xedashboard");
			}

			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());

			// boolean prjFlag = false;
			// boolean moduleFlag = false;
			List<Map<String, Object>> tcDetails = testCaseDao.getProjectModuleScenarioIdByTcPrefix(tcPrefix, schema);
			int projectId = Integer.parseInt(tcDetails.get(0).get("project_id").toString());
			// List<Map<String, Object>> userPrjDetails =
			// projectDao.getProjectAssignedUser(userId, schema);
			//
			// for (int i = 0; i < userPrjDetails.size(); i++) {
			// int idCheck =
			// Integer.parseInt(userPrjDetails.get(i).get("project_id").toString());
			// if (idCheck == projectId) {
			// prjFlag = true;
			// break;
			// }
			//
			// }
			int moduleId = Integer.parseInt(tcDetails.get(0).get("module_id").toString());
			String moduleName = "";
			// if (prjFlag == true) {
			// List<Map<String, Object>> userModuleDetails =
			// moduleDao.getAllModules(userId,
			// schema);
			//
			// for (int i = 0; i < userModuleDetails.size(); i++) {
			// int idCheck =
			// Integer.parseInt(userModuleDetails.get(i).get("module_id").toString());
			// if (idCheck == moduleId) {
			// moduleName =
			// userModuleDetails.get(i).get("module_name").toString();
			// moduleFlag = true;
			// break;
			// }
			//
			// }
			// }
			// if (prjFlag == true && moduleFlag == true) {

			int scenarioId = Integer.parseInt(tcDetails.get(0).get("scenario_id").toString());
			int testcaseId = Integer.parseInt(tcDetails.get(0).get("testcase_id").toString());
			session.setAttribute("selectedTestModuleId", moduleId);
			session.setAttribute("selectedTestModuleName", moduleName);
			session.setAttribute("selectedScenarioId", scenarioId);
			session.setAttribute("selectedTestcaseId", testcaseId);
			session.setAttribute("testcaseViewType", "Edit");
			List<Map<String, Object>> testcaseDetails = new ArrayList<Map<String, Object>>();
			Map<String, Object> data = testSpecificationDao.getTestCaseStepData(userId, projectId, moduleId, scenarioId,
					testcaseId, schema);
			List<Map<String, Object>> scenarioList = (List<Map<String, Object>>) data.get("#result-set-1");
			List<Map<String, Object>> testCaseList = (List<Map<String, Object>>) data.get("#result-set-2");
			for (int i = 0; i < testCaseList.size(); i++) {
				if (Integer.parseInt(testCaseList.get(i).get("testcase_id").toString()) == testcaseId) {
					testcaseDetails.add(testCaseList.get(i));
					break;
				}
			}
			List<Map<String, Object>> testCaseSummary = (List<Map<String, Object>>) data.get("#result-set-10");
			for (int i = 0; i < testCaseSummary.size(); i++) {
				String photo = loginService.getImage((byte[]) testCaseSummary.get(i).get("user_photo"));
				testCaseSummary.get(i).put("user_photo", photo);
			}
			model.addAttribute("moduleList", data.get("#result-set-9"));
			model.addAttribute("scenarioList", utilsService.convertListOfMapToJson(scenarioList));
			model.addAttribute("testCaseList", testCaseList);
			model.addAttribute("testcaseDetails", testcaseDetails);
			model.addAttribute("stepList", data.get("#result-set-3"));
			model.addAttribute("executionTypes", data.get("#result-set-4"));
			model.addAttribute("statusList", data.get("#result-set-5"));
			model.addAttribute("testCaseSummary", testCaseSummary);
			model.addAttribute("moduleId", moduleId);
			model.addAttribute("moduleName", moduleName);
			model.addAttribute("scenarioId", scenarioId);
			model.addAttribute("recentFlag", 0);

			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-Y");
			Date date = new Date();
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			model.addAttribute("nowDate", dateFormat.format(date));
			model.addAttribute("prevDate", dateFormat.format(cal.getTime()));
			List<Map<String, Object>> testComments = (List<Map<String, Object>>) data.get("#result-set-6");
			List<Map<String, Object>> tcCommentLike = (List<Map<String, Object>>) data.get("#result-set-7");
			List<Map<String, Object>> tcCommentLikeCounts = (List<Map<String, Object>>) data.get("#result-set-8");
			for (int i = 0; i < testComments.size(); i++) {
				String logo = loginService.getImage((byte[]) testComments.get(i).get("user_photo"));
				testComments.get(i).put("commenterImage", logo);
				testComments.get(i).put("likeStatus", "2");
				for (int j = 0; j < tcCommentLike.size(); j++) {
					if (Integer.parseInt(testComments.get(i).get("xetm_testcase_comments_id").toString()) == Integer
							.parseInt(tcCommentLike.get(j).get("comment_id").toString())
							&& Integer.parseInt(tcCommentLike.get(j).get("liked_by_user").toString()) == userId) {
						testComments.get(i).put("likeStatus", "1");
						break;
					}

				}
				for (int j = 0; j < tcCommentLikeCounts.size(); j++) {
					testComments.get(i).put("likeCount", "0");
					if (Integer.parseInt(testComments.get(i).get("xetm_testcase_comments_id").toString()) == Integer
							.parseInt(tcCommentLikeCounts.get(j).get("comment_id").toString())) {
						testComments.get(i).put("likeCount",
								Integer.parseInt(tcCommentLikeCounts.get(j).get("likeCount").toString()));
						break;
					}
				}
			}
			model.addAttribute("testComments", testComments);
			return new ModelAndView("teststeps", "Model", model);
			// } else
			// return new ModelAndView("redirect:/404");
		}

	}

	/**
	 * @author suresh.adling
	 * @method setModuleId POST Request
	 * @output Set session attribute selectedTestModuleId of selected module id
	 *         in test case management
	 * @description Post method to selectedTestModuleId in session to
	 *              add,edit,update details for scenario,test case,test case
	 *              steps
	 * @param moduleId
	 * @param moduleName
	 * @param viewType
	 * @param model
	 * @param session
	 * @return boolean value
	 * @throws Exception
	 */
	@RequestMapping(value = "/settestmoduleid", method = RequestMethod.POST)
	public @ResponseBody int setModuleId(@RequestParam("moduleId") String moduleId,
			@RequestParam("moduleName") String moduleName, @RequestParam("viewType") String viewType, Model model,
			HttpSession session) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("selectedTestModuleId", moduleId);
		session.setAttribute("selectedModuleId", moduleId);
		session.setAttribute("selectedTestModuleName", moduleName);
		session.setAttribute("moduleViewType", viewType);
		return 1;
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request gives datasheet by testcase id
	 * 
	 * @param testDsId
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getDocumentByTcDsId", method = RequestMethod.POST)
	public @ResponseBody String getDocumentByTcDsId(@RequestParam("testDsId") int testcaseId, HttpSession session)
			throws Exception {
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		logger.info("Reading document by document id");
		List<Map<String, Object>> data = testCaseService.getDatasheetByTcId(testcaseId, schema);
		if (data.size() > 0) {
			data.get(0).put("status", true);
			data.get(0).put("datasheetFile", utilsService.getFileData((byte[]) data.get(0).get("datasheet_file")));
			ArrayList<String> response = utilsService.convertListOfMapToJson(data);
			return response.toString();
		} else {
			data = new ArrayList<Map<String, Object>>();
			Map<String, Object> status = new HashMap<String, Object>();
			status.put("status", false);
			data.add(status);
			return utilsService.convertListOfMapToJson(data).toString();
		}

	}

	@RequestMapping(value = "/getDoc", method = RequestMethod.POST)
	public @ResponseBody String getDocumentByTcDsId1(HttpSession session) throws Exception {
		String filename = "Test2.xlsx";
		// Object data=[B@1d89aec2;
		byte[] b1 = "[B@1d89aec2".getBytes();
		Blob blob = new javax.sql.rowset.serial.SerialBlob(b1);// (
																// (byte[])
																// data.get(0).get("datasheet_file"));
		InputStream is = blob.getBinaryStream();
		FileOutputStream fos = new FileOutputStream("C:\\repository\\upload\\my" + "\\" + filename);

		int b = 0;
		while ((b = is.read()) != -1) {
			fos.write(b);
		}
		return filename;

	}

	/**
	 * @author suresh.adling
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :" + ex);
		return new ModelAndView("redirect:/500");
	}
}

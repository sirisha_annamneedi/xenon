package com.xenon.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.NotificationDAO;

/**
 * 
 * @author prafulla.pol
 * @Description XenonCore class is used to redirect user to code dashboard based on the user role
 *
 */
@Controller
public class XenonCoreController {
	
	@Autowired
	NotificationDAO notificationDAO;
	
	@Autowired
	BuildDAO buildDAO;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	UtilsService utilsService;
	
	private static final Logger logger = LoggerFactory.getLogger(XenonCoreController.class);
	
	/**
	 * @author prafulla.pol
	 * @method coredashboard a GET Request
	 * @param model
	 * @param session
	 * @return Model (model) &
	 *  	   View (onSuccess - coredashboard ,
	 *               onFailure - logout/500)
	 * @throws Exception
	 */
	@RequestMapping(value = "/coredashboard")
	public ModelAndView coredashboard(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to core dashboard and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		}
		else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID+ " Customer Schema " + schema);
			
			logger.info("Checking for users role");
			if (Integer.parseInt(session.getAttribute("roleId").toString()) != 1){
				logger.warn("User's role is not having permission to access core dashboard. User is redirected to 500");
				return new ModelAndView("redirect:/500");
			}
			else{
				logger.warn("User's role is having permission to access core dashboard.");
				model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
				model.addAttribute("userName", session.getAttribute("userFullName"));
				model.addAttribute("role", session.getAttribute("role"));
				logger.info(" User is redirected to core dashboard");
				return new ModelAndView("coredashboard","Model",model);
			}
		}
	}
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request is used to read header data
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/coreheader")
	public ModelAndView coreheader(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active,  Customer Schema " + schema);
			Map<String, Object> buildDashboard =notificationDAO.getNotifications(Integer.parseInt(session.getAttribute("userId").toString()), schema); //buildDAO.headerData(schema);
			List<Map<String, Object>> allBuildsTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-1");
			List<Map<String, Object>> allBuildsExecutedTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-2");
			
			List<Map<String, Object>> allBuilds = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-5");
			
			List<Map<String, Object>> topBuilds = buildDAO.getBmDashboardTopBuildsNf(allBuilds,allBuildsTestcasesCount,
					allBuildsExecutedTestcasesCount, schema);
			for (int i = 0; i < topBuilds.size(); i++) {
				topBuilds.get(i).put("user_image",
						utilsService.getFileData((byte[]) topBuilds.get(i).get("user_photo")));
				topBuilds.get(i).put("build_createdate",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(topBuilds.get(i).get("build_createdate").toString())));
			}
			
			List<Map<String, Object>> notifications =  (List<Map<String, Object>>) buildDashboard
					.get("#result-set-3");
			for (int i = 0; i < notifications.size(); i++) {
				notifications.get(i).put("user_image", utilsService.getFileData((byte[]) notifications.get(i).get("user_photo")));
				String dateInString = notifications.get(i).get("notification_date").toString();
				notifications.get(i).put("notification_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), dateInString));
			}
			List<Map<String, Object>> unreadCount =  (List<Map<String, Object>>) buildDashboard
					.get("#result-set-4");
			model.addAttribute("unreadCount", Integer.parseInt(unreadCount.get(0).get("unreadCount").toString()));
			model.addAttribute("nowDate", utilsService.getNowDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("prevDate",
					utilsService.getPrevDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("notifications", notifications);
			model.addAttribute("topBuilds", topBuilds);
			return new ModelAndView("coreheader", "Model", model);
		}
	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		return new ModelAndView("redirect:/500");
	}
}

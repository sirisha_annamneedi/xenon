package com.xenon.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
/**
 * 
 * @author navnath.damale
 * @description handles controller process error
 */
@ControllerAdvice
public class ExceptionHandlerController {
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public ModelAndView handleError405(HttpServletRequest request, Exception e)   {
		return new ModelAndView("redirect:/405");  
	}
}
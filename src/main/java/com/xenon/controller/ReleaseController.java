package com.xenon.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.buildmanager.ReleaseService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.ReleaseDAO;

/**
 * 
 * @author prafulla.pol
 * @Description Release class is used for creating, editing and updating
 *              release.
 *
 */
@Controller
public class ReleaseController {

	private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);

	@Autowired
	RoleAccessService roleAccessService;
	
	@Autowired
	ReleaseService releaseService;
	
	@Autowired
	UtilsService utilsService;

	@Autowired
	ReleaseDAO releaseDAO;

	@Autowired
	BuildDAO buildDAO;
	/**
	 * @author prafulla.pol
	 * @method createRelease a GET Request
	 * @param model
	 * @param session
	 * @return Model (model) & View (onSuccess - createrelease , onFailure -
	 *         logout/404)
	 * @throws Exception
	 */
	@RequestMapping(value = "/createrelease")
	public ModelAndView createRelease(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Create Release and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to create release");

			if (buildDAO.checkBMRoleAccess("createrelease",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to create release");
				session.setAttribute("menuLiText", "Release");
				if (session.getAttribute("duplicateReleaseError") != null) {
					model.addAttribute("error", 1);
					session.removeAttribute("duplicateReleaseError");
				}
				logger.info(" User is redirected to Create release ");
				return new ModelAndView("createrelease", "Model", model);
			} else {
				logger.warn("User has no access to create release . Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method insertRelease a POST Request
	 * @param model
	 * @param session
	 * @param releaseName
	 * @param releaseDescription
	 * @param sDate
	 * @param eDate
	 * @param releaseStatus
	 * @return Model (model) & View (onSuccess - viewrelease , onFailure -
	 *         logout/404/createrelease)
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/insertrelease", method = RequestMethod.POST)
	public ModelAndView insertRelease(Model model, HttpSession session, @RequestParam("releaseName") String releaseName,
			@RequestParam("releaseDescription") String releaseDescription, @RequestParam("startDate") String sDate,
			@RequestParam("endDate") String eDate, @RequestParam("releaseStatus") String releaseStatus, @RequestParam(required=false) String source)
			throws Exception {
		logger.info("Post method to insert release and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to create release");
			int flag = releaseService.insertRelease( releaseName,releaseDescription, releaseStatus, sDate, eDate, schema,session.getAttribute("roleId").toString());
			if (flag>0) {
				session.setAttribute("releaseCreateStatus", 1);
				session.setAttribute("newReleaseName", releaseName.trim());
				logger.info("Release is created. User is redirected to view release ");
				//return new ModelAndView("redirect:/viewrelease?id="+flag);
				if( source != null && "LeftNavMenuManual".equalsIgnoreCase(source) )
					return new ModelAndView("redirect:/executebuild");
				else if( source != null && "LeftNavMenuAutomation".equalsIgnoreCase(source) )
					return new ModelAndView("redirect:/autoexecute");
				return new ModelAndView("redirect:/createbuild?id="+flag+"&name="+releaseName.trim());
			} else if (flag == -208) {
				session.setAttribute("duplicateReleaseError", 1); 
				logger.info("Error : Duplicate relese name . User is redirected to create release ");
				return new ModelAndView("redirect:/createrelease");
			} else {
				logger.warn("User has no access to create release . Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method viewRelease a GET Request
	 * @param model
	 * @param session
	 * @return Model (createReleaseAccessStat,releaseDetails) & View (onSuccess
	 *         - viewrelease , onFailure - logout/404)
	 * @throws ParseException
	 */
	@RequestMapping(value = "/viewrelease", method = RequestMethod.GET)
	public ModelAndView viewRelease(Model model, HttpSession session) throws ParseException {
		logger.info("Redirecting to view release and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to view release");
			if (buildDAO.checkBMRoleAccess("viewrelease",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to view release");

				boolean createStatus = buildDAO.checkBMRoleAccess("createrelease",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString());
				session.setAttribute("menuLiText", "Release");
				if (createStatus) {
					model.addAttribute("createReleaseAccessStat", 1);
				}
				List<Map<String, Object>> releaseDetails = releaseDAO
						.getAllReleaseDetails(session.getAttribute("cust_schema").toString());

				for (int i = 0; i < releaseDetails.size(); i++) {
					releaseDetails.get(i).put("startDate",
							utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
									utilsService.getDate(releaseDetails.get(i).get("startDate").toString())));
					releaseDetails.get(i).put("endDate",
							utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
									utilsService.getDate(releaseDetails.get(i).get("endDate").toString())));
				}
				model.addAttribute("releaseDetails", releaseDetails);

				logger.info(" User is redirected to view release ");
				return new ModelAndView("viewrelease", "model", model);
			} else {
				logger.warn("User has no access to view release . Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method setReleaseId a POST Request
	 * @param releaseID
	 * @param viewType
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/setreleaseid", method = RequestMethod.POST)
	public ModelAndView setReleaseId(@RequestParam("releaseID") String releaseID,
			@RequestParam("viewType") String viewType, Model model, HttpSession session) {
		logger.info("Post method to save the release id and view type into session.Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			session.setAttribute("selectedReleaseID", releaseID);
			session.setAttribute("selectedReleaseViewType", viewType);
			logger.info(" User is redirected ");
			return new ModelAndView("redirect:/");
		}
	}

	/**
	 * @author prafulla.pol
	 * @method editRelease a GET Request
	 * @param model
	 * @param session
	 * @return Model (releaseDetails) & View (onSuccess - editrelease ,
	 *         onFailure - logout/404)
	 * @throws Exception
	 */
	@RequestMapping(value = "/editrelease")
	public ModelAndView editRelease(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to edit release and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to edit release");
			if (buildDAO.checkBMRoleAccess("editrelease",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to edit release");
				session.setAttribute("menuLiText", "Release");
				int releaseId = Integer.parseInt(session.getAttribute("selectedReleaseID").toString());
				List<Map<String, Object>> releaseDetails = releaseDAO.getReleaseDetails(releaseId, schema);
				logger.info(" User is redirected to edit release");
				return new ModelAndView("editrelease", "releaseDetails", releaseDetails);
			} else {
				logger.warn("User has no access to edit release . Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}
		}
	}

	@RequestMapping(value = "/deleterelease")
	public ModelAndView deleteRelease( @RequestParam Integer releaseId, @RequestParam String source,
			Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to edit release and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to edit release");
			int flag = this.releaseService.deleteRelease(releaseId, schema, session.getAttribute("roleId").toString());
			if( flag == 204 ) {
				if( source != null && "LeftNavMenuManual".equalsIgnoreCase(source) )
					return new ModelAndView("redirect:/executebuild");
				else if( source != null && "LeftNavMenuAutomation".equalsIgnoreCase(source) )
					return new ModelAndView("redirect:/autoexecute");
			}
			return new ModelAndView("redirect:/"+flag);
		}
	}
	
	/**
	 * @author prafulla.pol
	 * @method updateRelease a POST Request
	 * @param model
	 * @param session
	 * @param releaseID
	 * @param releaseDescription
	 * @param eDate
	 * @param releaseStatus
	 * @return Model () & View (onSuccess - viewrelease , onFailure -
	 *         logout/404/500)
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/updaterelease", method = RequestMethod.POST)
	public ModelAndView updateRelease(Model model, HttpSession session, @RequestParam("releaseID") String releaseID,
			@RequestParam("releaseDescription") String releaseDescription, @RequestParam("endDate") String eDate,
			@RequestParam("releaseStatus") String releaseStatus, @RequestParam("releaseName") String releaseName,
			@RequestParam(required=false) String source ) throws Exception {
		logger.info("Post method to update release details.Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to edit release");

			int flag = releaseService.updateRelease(releaseID,releaseName,releaseDescription,releaseStatus,eDate,schema,session.getAttribute("roleId").toString());
			if (flag == 201) {
				session.setAttribute("newReleaseName", releaseName);
				session.setAttribute("releaseCreateStatus", 2);
				if( source != null && "LeftNavMenuManual".equalsIgnoreCase(source) )
					return new ModelAndView("redirect:/executebuild");
				else if( source != null && "LeftNavMenuAutomation".equalsIgnoreCase(source) )
					return new ModelAndView("redirect:/autoexecute");
				logger.info("User is redirected to view release");
				return new ModelAndView("redirect:/viewrelease");
			} else {
				logger.warn("User has no access to edit release . Redirected to 404 page");
				return new ModelAndView("redirect:/" + flag);
			}
		}
	}
	
	@RequestMapping(value = "/updatereleasename", method = RequestMethod.POST)
	public ModelAndView updateReleaseName(Model model, HttpSession session, @RequestParam("releaseId") Integer releaseId,
			@RequestParam("releaseName") String releaseName, @RequestParam(required=false) String source ) throws Exception {
		logger.info("Post method to update release details.Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to edit release");

			int flag = releaseService.updateReleaseName(releaseName,releaseId,schema,session.getAttribute("roleId").toString());
			if (flag == 201) {
				if( source != null && "LeftNavMenuManual".equalsIgnoreCase(source) )
					return new ModelAndView("redirect:/executebuild");
				else /*if( source != null && "LeftNavMenuAutomation".equalsIgnoreCase(source) )*/
					return new ModelAndView("redirect:/autoexecute");
			} else {
				logger.warn("User has no access to edit release . Redirected to 404 page");
				return new ModelAndView("redirect:/" + flag);
			}
		}
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		return new ModelAndView("redirect:/500");
	}
}

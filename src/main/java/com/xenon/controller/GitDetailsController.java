package com.xenon.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.administration.GitDetailsService;
import com.xenon.api.administration.GitRepositoryService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;

@Controller
public class GitDetailsController {

	@Autowired
	BuildDAO buildDAO;

	@Autowired
	GitDetailsService gitDetailsService;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	GitRepositoryService gitRepositoryService;

	private static final Logger logger = LoggerFactory.getLogger(GitDetailsController.class);

	@RequestMapping(value = "/viewgitprojects", method = RequestMethod.GET)
	public ModelAndView viewGitInstances(Model model, HttpSession session) {

		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			if (session.getAttribute("roleId").toString().trim().equalsIgnoreCase("2")) {
				model.addAttribute("isAdminUser", "true");
			} else {
				model.addAttribute("isAdminUser", "false");
			}
			String schema = session.getAttribute("cust_schema").toString();
			List<Map<String, Object>> allRows = gitDetailsService.getAllInstances(session.getAttribute("userId").toString(), schema);
			model.addAttribute("allRows", allRows);

			return new ModelAndView("viewgitprojects", "model", model);

		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}

	}

	@RequestMapping(value = "/addgitproject", method = RequestMethod.GET)
	public ModelAndView createGitInstance(Model model, HttpSession session) {

		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {

			return new ModelAndView("addgitproject", "Model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}

	}

	@RequestMapping(value = "/insertgitproject")
	public ModelAndView insertGitInstance(Model model, HttpSession session, @RequestParam("gitProjectName") String gitProjectName,
			@RequestParam("gitUrl") String gitUrl, @RequestParam("Username") String userName,
			@RequestParam("apiToken") String apiToken, @RequestParam("gitStatus") String gitStatus) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());

			int flag = gitDetailsService.insertGitProject(gitProjectName, gitUrl, userName, apiToken, gitStatus, userID,
					schema);
			if (flag == 201) {
				logger.info("Git Instance is added successfully");
				return new ModelAndView("redirect:/viewgitprojects");
			} else {
				logger.error("Something went wrong, User redirected to error page");
				return new ModelAndView("redirect:/500");
			}

		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/gitprojectsetting", method = RequestMethod.GET)
	public ModelAndView serverSetting(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			
			utilsService.getLoggerUser(session);
			String serverId=session.getAttribute("currentGitProjectId").toString();
			String schema = session.getAttribute("cust_schema").toString();
			
			Map<String, Object> memberData =gitDetailsService.getInstancePermissionWiseMembers(serverId,schema);
			
			List<Map<String, Object>> permissionAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-1");
			for (int i = 0; i < permissionAssignedMembers.size(); i++) {
				String photo = loginService.getImage((byte[]) permissionAssignedMembers.get(i).get("user_photo"));
				permissionAssignedMembers.get(i).put("photo", photo);
			}
			model.addAttribute("permissionAssignedMembers", permissionAssignedMembers);
			
			List<Map<String, Object>> permissionNonAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-2");
			model.addAttribute("permissionNonAssignedMembers", permissionNonAssignedMembers);
			
			List<Map<String, Object>> updatePermissionAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-3");
			model.addAttribute("updatePermissionAssignedMembers", updatePermissionAssignedMembers);
			
			List<Map<String, Object>> executePermissionAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-4");
			model.addAttribute("executePermissionAssignedMembers", executePermissionAssignedMembers);
			
			List<Map<String, Object>> noPermissionAssignedMembers = (List<Map<String, Object>>) memberData.get("#result-set-5");
			model.addAttribute("noPermissionAssignedMembers", noPermissionAssignedMembers);
			
			List<Map<String, Object>> serverDetails = (List<Map<String, Object>>) memberData.get("#result-set-6");
			model.addAttribute("ServerDetails",serverDetails);

			return new ModelAndView("gitprojectsetting", "model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	@RequestMapping(value = "/editgitrepository", method = RequestMethod.GET)
	public ModelAndView editGitRepository(Model model, HttpSession session) {

		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString()) && session.getAttribute("currentRepositoryId")!=null) {
			String userId = session.getAttribute("userId").toString();
			String repositoryId = session.getAttribute("currentRepositoryId").toString();
			String schema = session.getAttribute("cust_schema").toString();
			List<Map<String, Object>> returnData = gitDetailsService.getAllInstances(userId, schema);
			Map<String, Object> repository = gitRepositoryService.getRepositoryById(repositoryId,schema);
			model.addAttribute("repositoryInformation", repository);
			model.addAttribute("instanceDetails", returnData);
			String projectId = session.getAttribute("projectId").toString();
			model.addAttribute("projectId", projectId);
			return new ModelAndView("editgitrepository", "model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}

	}
	
	@RequestMapping(value="/editgitrepository",method= RequestMethod.POST)
	public ModelAndView editGitRepository(HttpSession session,Model model,@RequestParam("instanceList") String instanceId,@RequestParam("branchName") String branchName
			,@RequestParam("repositoryName") String repositoryName
			,@RequestParam("repositoryId") String repositoryId){
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			String schema = session.getAttribute("cust_schema").toString();
			
			int flag = gitRepositoryService.updateGitRepository(instanceId,branchName,repositoryName,repositoryId,schema);
			if (flag == 201) {
				logger.info("Git Instance is added successfully");
				return new ModelAndView("redirect:/viewgitprojects");
			} else {
				logger.error("Something went wrong, User redirected to error page");
				return new ModelAndView("redirect:/500");
			}
			
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	
	@RequestMapping(value = "/addGitInstancePermissionToMember", method = RequestMethod.POST)
	public ModelAndView addInstancePermissionToMember(@RequestParam("users") String users,@RequestParam("instanceId") String instanceId, Model model, HttpSession session) {
		String coreSchema = session.getAttribute("cust_schema").toString();
		String[] allUserIds = new String[0];		
		if(users!=null && !users.trim().equals("")) {
			if(users.contains(",")) {
				allUserIds=users.split(",");
			}
			else {
				allUserIds = new String[1];
				allUserIds[0] = users;
			}
		}
		gitDetailsService.assignInstancePermissionToUsers(allUserIds,instanceId,coreSchema);
		
		return new ModelAndView("redirect:/gitprojectsetting","model",model);
	}
	
	@RequestMapping(value = "/updateUsersGitInstancePermission", method = RequestMethod.POST)
	public ModelAndView updateUsersInstancePermission(@RequestParam(value ="editPermissionUsers",required=false) String editPermissionUsers,
			@RequestParam("instanceId") String instanceId, Model model, HttpSession session) {
		String coreSchema = session.getAttribute("cust_schema").toString();	
		if(editPermissionUsers!=null) {
			gitDetailsService.updateUsersInstancePermission(editPermissionUsers,instanceId,coreSchema);
		}
		return new ModelAndView("redirect:/gitprojectsetting","model",model);
	}
	
	@RequestMapping(value = "/revokeUsersGitInstancePermission", method = RequestMethod.POST)
	public ModelAndView revokeUsersInstancePermission(@RequestParam("userId") String userId,@RequestParam("instanceId") String instanceId, Model model, HttpSession session) {
		
		String coreSchema = session.getAttribute("cust_schema").toString();
		
		gitDetailsService.revokeUsersInstancePermission(userId,instanceId,coreSchema);
		
		return new ModelAndView("redirect:/gitprojectsetting","model",model);
	}
	
	@RequestMapping(value = "/removeUsersGitInstancePermission", method = RequestMethod.POST)
	public ModelAndView removeUsersInstancePermission(@RequestParam("userId") String userId,@RequestParam("instanceId") String instanceId, Model model, HttpSession session) {
		
		String coreSchema = session.getAttribute("cust_schema").toString();
		gitDetailsService.removeUsersInstancePermission(userId,instanceId,coreSchema);
		return new ModelAndView("redirect:/gitprojectsetting","model",model);
	}
	
	@RequestMapping(value = "/updategitproject")
	public ModelAndView updateGitInstance(Model model, HttpSession session,
			@RequestParam("serverName") String serverName,
			@RequestParam("username") String username, @RequestParam("gitUrl") String gitUrl,
			@RequestParam("apiToken") String apiToken, @RequestParam("instanceStatus") String instanceStatus,@RequestParam("serverId") String serverId) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			String schema = session.getAttribute("cust_schema").toString();
			int flag=gitDetailsService.updateGitProject(serverId, serverName, username, gitUrl, apiToken, instanceStatus,schema);
			if (flag == 201) {
				logger.info("Job is updated successfully");
				return new ModelAndView("redirect:/gitprojectsetting");
			} else {
				logger.error("Something went wrong, User redirected to error page");
				return new ModelAndView("redirect:/500");
			}

		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	@RequestMapping(value = "/setgitinstanceid", method = RequestMethod.POST)
	public @ResponseBody void setInstanceid(Model model, HttpSession session,
			@RequestParam("instanceId") String instanceId, @RequestParam("updatePermission") String updatePermission) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("currentGitProjectId", instanceId);
		session.setAttribute("updatePermission", updatePermission);
	}
	
	@RequestMapping(value = "/setgitprojectid", method = RequestMethod.POST)
	public @ResponseBody void setGitProjectId(Model model, HttpSession session,
			@RequestParam("projectId") String projectId) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("projectId", projectId);
	}
	
	
	@RequestMapping(value = "/testGitConnection", method = RequestMethod.POST)
	public @ResponseBody int testGitConnection(Model model, HttpSession session,
			@RequestParam("gitUrl") String gitUrl,@RequestParam("username") String username,@RequestParam("apiToken") String apiToken) throws Exception {
		utilsService.getLoggerUser(session);
		int flag=gitDetailsService.testGitConnection(gitUrl, username, apiToken);
		return flag;
	}
	
	@RequestMapping(value = "/creategitrepository", method = RequestMethod.GET)
	public ModelAndView createGitRepository(Model model, HttpSession session) {

		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			String userId = session.getAttribute("userId").toString();
			String schema = session.getAttribute("cust_schema").toString();
			List<Map<String, Object>> returnData = gitDetailsService.getAllInstances(userId, schema);
			String projectId = session.getAttribute("projectId").toString();
			model.addAttribute("projectId", projectId);
			model.addAttribute("instanceDetails", returnData);
			return new ModelAndView("creategitrepository", "model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}

	}
	
	@RequestMapping(value="/insertgitrepository",method= RequestMethod.POST)
	public ModelAndView insertgitRepository(HttpSession session,Model model,@RequestParam("instanceList") String instanceId,@RequestParam("branchName") String branchName
			,@RequestParam("repositoryName") String repositoryName){
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			String userId = session.getAttribute("userId").toString();
			String schema = session.getAttribute("cust_schema").toString();
			
			int flag = gitDetailsService.insertGitRepository(instanceId,branchName,repositoryName,userId,schema);//Instance(gitName, gitUrl, userName, apiToken, gitStatus, userID,schema);
			if (flag == 201) {
				logger.info("Git Instance is added successfully");
				return new ModelAndView("redirect:/viewgitprojects");
			} else {
				logger.error("Something went wrong, User redirected to error page");
				return new ModelAndView("redirect:/500");
			}
			
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	
	
	
	
	@RequestMapping(value="/getprojectcheckout",method= RequestMethod.POST)
	public @ResponseBody int getProjectCheckOut(HttpSession session,Model model,@RequestParam("instanceId") String instanceId,@RequestParam("branchName") String branchName){
			String userId = session.getAttribute("userId").toString();
			String schema = session.getAttribute("cust_schema").toString();
			List<Map<String, Object>> returnData = gitDetailsService.getInstanceDetailsById(Integer.parseInt(instanceId),userId, schema);
			int flag=gitDetailsService.getCheckOut(returnData.get(0).get("git_server_url").toString(),returnData.get(0).get("username").toString(),returnData.get(0).get("api_token").toString(),branchName);
			return flag;
	}
	
	@RequestMapping(value = "/viewgitrepository", method = RequestMethod.GET)
	public ModelAndView viewGitRepository(Model model, HttpSession session) {

		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString()) && session.getAttribute("currentGitProjectId") != null && session.getAttribute("updatePermission") != null) {
			if (session.getAttribute("roleId").toString().trim().equalsIgnoreCase("2")) {
				model.addAttribute("isAdminUser", "true");
			} else {
				model.addAttribute("isAdminUser", "false");
			}
			String schema = session.getAttribute("cust_schema").toString();
			String userId = session.getAttribute("userId").toString();
			String projectId=session.getAttribute("currentGitProjectId").toString();
			
			List<Map<String, Object>> repoDetails = gitRepositoryService.getAllrepositories(userId, projectId, schema);
			model.addAttribute("updatePermission",session.getAttribute("updatePermission"));
			model.addAttribute("repoDetails",repoDetails);
			model.addAttribute("projectId",projectId);
			
			

			return new ModelAndView("viewgitrepository", "model", model);

		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	@RequestMapping(value = "/setrepositoryid", method = RequestMethod.POST)
	public @ResponseBody void setRepositoryId(Model model, HttpSession session,
			@RequestParam("repositoryId") String repoId,
			@RequestParam("projectId") String projectId) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("currentRepositoryId", repoId);
		session.setAttribute("projectId", projectId);
	}
	
}

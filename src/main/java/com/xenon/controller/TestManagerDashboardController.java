package com.xenon.controller;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.administration.ProjectService;
import com.xenon.api.common.CommonService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.api.testmanager.TestManagerDashboardService;
import com.xenon.buildmanager.dao.ActivitiesDAO; // DAO class to perform JDBC operations for all activities 
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.NotificationDAO;
import com.xenon.buildmanager.dao.ProjectDAO; // DAO class to perform JDBC operations for applications
import com.xenon.testmanager.dao.TmDashboardDAO; // DAO class to perform JDBC operations for test manager dashboard stuff

/**
 * 
 * @author suresh.adling
 * @description Controller class Test Manager Dashboard and dashboard links
 * 
 */
@SuppressWarnings("unchecked")
@Component
@Controller
public class TestManagerDashboardController {

	@Resource(name = "colorProperties")
	private Properties colorProperties;
	
	@Autowired
	ProjectDAO projectDAO;
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	TestManagerDashboardService testManagerDashboardService;
	
	@Autowired
	RoleAccessService roleAccessService;
	
	private static final Logger logger = LoggerFactory.getLogger(TestManagerDashboardController.class);
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	TmDashboardDAO tmDashDAO;
	@Autowired
	ActivitiesDAO activitiesDAO;

	@Autowired
	NotificationDAO notificationDAO;
	
	@Autowired
	BuildDAO buildDAO;
	
	/**
	 * @author suresh.adling
	 * @method getTMDashboard GET Request
	 * @output get test manager dashboard view
	 * @param Model
	 *            model,HttpSession session
	 * @description This method reads following list by using session stored
	 *              values(userId,projectId) and redirects user to tmdashboard
	 *              view with the model
	 * @return Model (OpenBugList,bugStat,getBugtable) & View (onSuccess -
	 *         tmdashboard ,onFailure - logout/500)
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/tmdashboard")
	public ModelAndView getTMDashboard(Model model, HttpSession session) throws Exception {

		logger.info("Redirecting to Test Manager Tracker Dashboard and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirecting to login page");
			return new ModelAndView("redirect:/");
		} else if (tmDashDAO.checkTMRoleAccess("tmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {

			utilsService.getLoggerUser(session);

			String schema = session.getAttribute("cust_schema").toString();
			if (!utilsService.checkSessionContainsAttribute("UserCurrentProjectId", session)) {
				return new ModelAndView("redirect:/xedashboard");
			} else {
				int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
				int userId = Integer.parseInt(session.getAttribute("userId").toString());
				String userTimeZone = session.getAttribute("userTimezone").toString();
				Object setAllProjectTM = session.getAttribute("setAllProjectTM");
				String userFullName = session.getAttribute("userFullName").toString();
				String role = session.getAttribute("role").toString();
				String userProfilePhoto = loginService.getImage((byte[]) session.getAttribute("userProfilePhoto"));
				Map<String, Object> returnModel = testManagerDashboardService.getDashboardData(schema, projectId,
						userId, userTimeZone, setAllProjectTM, userFullName, userProfilePhoto, role, tmDashDAO,
						activitiesDAO, colorProperties);
				session.setAttribute("setAllProjectTM", returnModel.get("setAllProjectTM"));
				Iterator it = returnModel.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry) it.next();
					String key = (String) pair.getKey();
					Object value = pair.getValue();
					if (key != "setAllProjectTM") {
						model.addAttribute(key, value);
						it.remove();
					}
				}
				
				
				logger.info("User is redirected to test manager dashboard Page with model : " + model);
				return new ModelAndView("tmdashboard", "Model", model);
			}
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	/**
	 * @author navnath.damale
	 * @param model
	 * @param session
	 * @param inputValue 
	 * @param commonData 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tmdashboardlink")
	public ModelAndView tmdashboardlink(Model model, HttpSession session, Object inputValue, Object commonData) throws Exception {
		logger.info("Redirecting to Test Manager Tracker Dashboard  links and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirecting to login page");
			return new ModelAndView("redirect:/");
		}
		else if (tmDashDAO.checkTMRoleAccess("tmdashboard",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {

			utilsService.getLoggerUser(session);
			/*model = common.getHeaderValues(model, session);
			if (model.containsAttribute("noProject")) {
				model.asMap().clear();
				return new ModelAndView("redirect:/xedashboard");
			}*/
			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if(appCount == 0){
				return new ModelAndView("redirect:/xedashboard");
			}
			
//			String filterText = session.getAttribute("filterTextTM").toString();
//			String dashboardType = session.getAttribute("tmDashboardType").toString();
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			Map<String, Object> resultSet = tmDashDAO.getTMDashboardData(userId, schema);
			List<Map<String, Object>> project = (List<Map<String, Object>>) resultSet.get("#result-set-3");
			for (int i = 0; i < project.size(); i++)
				project.get(i).put("project_createdate",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(project.get(i).get("project_createdate").toString())));
			int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
			/*		if (dashboardType.equals("all")) {
				resultSet = tmDashDAO.getTMDashboardData(userId, schema);
				
				List<Map<String, Object>> projectList = (List<Map<String, Object>>)  resultSet.get("#result-set-3");
						for (int cnt = 0; cnt < projectList.size(); cnt++)
							projectList.get(cnt).put("project_createdate",
									utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
											utilsService.getDate(projectList.get(cnt).get("project_createdate").toString())));
				model.addAttribute("totalApp",projectList );
				model.addAttribute("totalMod", resultSet.get("#result-set-4"));
				model.addAttribute("totalSce", resultSet.get("#result-set-5"));
				List<Map<String, Object>> totalTestCases = (List<Map<String, Object>>) resultSet.get("#result-set-6");
				for (int i = 0; i < totalTestCases.size(); i++)
					totalTestCases.get(i).put("created_date",
							utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
									utilsService.getDate(totalTestCases.get(i).get("created_date").toString())));
				model.addAttribute("dashboardType",dashboardType);
				model.addAttribute("totalTest", totalTestCases);
			} else {*/
				resultSet = tmDashDAO.getTmProjectDashboard(projectId, userId, schema);
				model.addAttribute("totalMod", resultSet.get("#result-set-4"));
				model.addAttribute("totalSce", resultSet.get("#result-set-5"));
				
				List<Map<String, Object>> scenrioList = (List<Map<String, Object>>)resultSet.get("#result-set-5");

				model.addAttribute("scenrioList", utilsService.convertListOfMapToJson(scenrioList));
				List<Map<String, Object>> totalTestCases = (List<Map<String, Object>>) resultSet.get("#result-set-6");
				List<Map<String, Object>> totalTestCountByStatus = (List<Map<String, Object>>) resultSet.get("#result-set-22");
				
				List<Map<String, Object>> getfilteredValues = (List<Map<String, Object>>) resultSet.get("#result-set-24");
				model.addAttribute("getfilteredValues", getfilteredValues);
				
				//System.out.println("getfilteredValues"+getfilteredValues);

				for (int i = 0; i < totalTestCases.size(); i++)
					totalTestCases.get(i).put("created_date",
							utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
									utilsService.getDate(totalTestCases.get(i).get("created_date").toString())));
//				model.addAttribute("dashboardType",dashboardType);
				model.addAttribute("totalTest", totalTestCases);
				model.addAttribute("totalTestJson",utilsService.convertListOfMapToJson(totalTestCases));
				model.addAttribute("totalTestCountByStatus", totalTestCountByStatus);
				model.addAttribute("totalTestCountByStatus1",utilsService.convertListOfMapToJson(totalTestCountByStatus));
				model.addAttribute("UserCurrentProjectName", session.getAttribute("UserCurrentProjectName").toString());
				model.addAttribute("allBugDetails", resultSet.get("#result-set-7"));
			//}
			
			String appFlag= "false";
			if(session.getAttribute("setAllProjectTM")!=null){
				
				appFlag = session.getAttribute("setAllProjectTM").toString();
			}
			
			model.addAttribute("appFlag",appFlag);
			
//			model.addAttribute("filterText", filterText);
			model.addAttribute("New", colorProperties.getProperty("bug.New"));
			model.addAttribute("Fixed", colorProperties.getProperty("bug.Fixed"));
			model.addAttribute("Closed", colorProperties.getProperty("bug.Closed"));
			model.addAttribute("Assigned", colorProperties.getProperty("bug.Assigned"));
			model.addAttribute("Verified", colorProperties.getProperty("bug.Verified"));
			model.addAttribute("Reject", colorProperties.getProperty("bug.Reject"));
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			logger.info("User is redirected to test manager dashboard links Page with model : " + model);
			return new ModelAndView("tmdashboardlink", "Model", model);
		} else{
			logger.error("Something went wrong, user is redirected to 500 error page" );
			return new ModelAndView("500");
		}
	}
	/**
	 * @author navnath.damale
	 * @param model
	 * @param session
	 * @param filterText
	 * @param dashboard
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setTmLinkSession")
	public ModelAndView setTmLinkSession(Model model, HttpSession session,
			@RequestParam("filterText") String filterText, @RequestParam("dashboard") String dashboard)
					throws Exception {
		logger.info("Setting selecte choice for redirection to test manager data page");
		logger.info("checking for active session");

		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirecting to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("filterTextTM", filterText);
			session.setAttribute("tmDashboardType", dashboard);
			return new ModelAndView("redirect:/");
		}
	}
	/**
	 * @author navnath.damale
	 * @param model
	 * @param session
	 * @param prjId
	 * @param prjName
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/setCurrentProjectTM")
	public ModelAndView setSessionforCurrentProject(Model model, HttpSession session,
			@RequestParam("projectId") String prjId, @RequestParam("projectName") String prjName) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("UserCurrentProjectName", prjName);
			session.setAttribute("UserCurrentProjectId", prjId);

			session.setAttribute("setAllProjectTM", false);
			session.setAttribute("tmDashboardType", false);
			
			session.removeAttribute("selectedTestModuleId");
			session.removeAttribute("selectedScenarioId");
			
			String SchemeName = session.getAttribute("cust_schema").toString();
			int projectId = Integer.parseInt(prjId);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
						
			int flag = projectService.updateCurrentProject(userId, projectId, SchemeName);

			return getTMDashboard(model, session);
		}
	}
	
	
	
	@RequestMapping(value = "/setAllProjectTM", method = RequestMethod.POST)
	public @ResponseBody boolean setAllProjectTM(Model model, HttpSession session) {
		utilsService.getLoggerUser(session);
		if (session.getAttribute("userId") == null) {
			return false;
		} else {
			session.setAttribute("setAllProjectTM", true);
			return true;
		}
	}

	/**
	 * @author suresh.adling
	 * @method getTMAllAppDashboard GET Request
	 * @output get test manager dashboard view for all applications
	 * @param Model
	 *            model,HttpSession session
	 * @description This method reads following list by using session stored
	 *              values(userId) and redirects user to tmallappdashboard view
	 *              with the model
	 * @return Model (OpenBugList,bugStat,getBugtable) & View (onSuccess -
	 *         tmallappdashboard ,onFailure - logout/500)
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/tmallappdashboard")
	public ModelAndView getTMAllAppDashboard(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Test Manager Tracker All Dashboard and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirecting to login page");
			return new ModelAndView("redirect:/");
		} else if (tmDashDAO.checkTMRoleAccess("tmdashboard",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {

			utilsService.getLoggerUser(session);
			/*model = common.getHeaderValues(model, session);
			if (model.containsAttribute("noProject")) {
				model.asMap().clear();
				return new ModelAndView("redirect:/xedashboard");
			}*/
			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if(appCount == 0){
				return new ModelAndView("redirect:/xedashboard");
			}
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info("Session is active, userId is " + userId + " Customer Schema " + schema);
			String userTimeZone=session.getAttribute("userTimezone").toString();
			Object userprofilePotho =session.getAttribute("userProfilePhoto");
			String userFullName=session.getAttribute("userFullName").toString();
			String role=session.getAttribute("role").toString();
			Map<String,Object> returnData=testManagerDashboardService.getTMAllAppDashboard(tmDashDAO,userId,schema,colorProperties,activitiesDAO,userTimeZone,userprofilePotho,userFullName,role);
			Iterator it = returnData.entrySet().iterator();
			 while (it.hasNext()) {
			        Map.Entry pair = (Map.Entry)it.next();
			        String key=(String) pair.getKey();
			        Object value=pair.getValue();
			    	model.addAttribute(key,value);
			        it.remove();
			   	   }
			logger.info("User is redirected to test manager all dashboard Page with model : " + model);
			return new ModelAndView("tmallappdashboard", "Model", model);
		} else{
			logger.error("Something went wrong, user is redirected to 500 error page" );
			return new ModelAndView("500");
		}
	}
	
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request is used to read header data
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tmheader")
	public ModelAndView tmheader(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active,  Customer Schema " + schema);
			Map<String, Object> buildDashboard =notificationDAO.getNotifications(Integer.parseInt(session.getAttribute("userId").toString()), schema); //buildDAO.headerData(schema);
			List<Map<String, Object>> allBuildsTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-1");
			List<Map<String, Object>> allBuildsExecutedTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-2");
			
			List<Map<String, Object>> allBuilds = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-5");
			
			List<Map<String, Object>> topBuilds = buildDAO.getBmDashboardTopBuildsNf(allBuilds,allBuildsTestcasesCount,
					allBuildsExecutedTestcasesCount, schema);
			for (int i = 0; i < topBuilds.size(); i++) {
				topBuilds.get(i).put("user_image",
						utilsService.getFileData((byte[]) topBuilds.get(i).get("user_photo")));
				topBuilds.get(i).put("build_createdate",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(topBuilds.get(i).get("build_createdate").toString())));
			}
			
			List<Map<String, Object>> notifications =  (List<Map<String, Object>>) buildDashboard
					.get("#result-set-3");
			for (int i = 0; i < notifications.size(); i++) {
				notifications.get(i).put("user_image", utilsService.getFileData((byte[]) notifications.get(i).get("user_photo")));
				String dateInString = notifications.get(i).get("notification_date").toString();
				notifications.get(i).put("notification_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), dateInString));
			}
			List<Map<String, Object>> unreadCount =  (List<Map<String, Object>>) buildDashboard
					.get("#result-set-4");
			/*model = common.getHeaderValues(model, session);
			if (model.containsAttribute("noProject")) {
				model.asMap().clear();
				return new ModelAndView("redirect:/xedashboard");
			}*/
			List<Map<String, Object>> projectList = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-6");
			projectList = commonService.getHeaderValues(session, projectList);
			model.addAttribute("projectList", projectList);
			
			model.addAttribute("unreadCount", Integer.parseInt(unreadCount.get(0).get("unreadCount").toString()));
			model.addAttribute("nowDate", utilsService.getNowDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("prevDate",
					utilsService.getPrevDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("notifications", notifications);
			model.addAttribute("topBuilds", topBuilds);
			return new ModelAndView("tmheader", "Model", model);
		}
	}
	

	/**
	 * @author suresh.adling
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :" + ex);
		return new ModelAndView("redirect:/500");
	}
	
	
	
	

	
}

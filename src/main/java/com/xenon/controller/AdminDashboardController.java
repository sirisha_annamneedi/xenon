package com.xenon.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.administration.DashboardService;
import com.xenon.api.buildmanager.BuildService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.bugtracker.dao.BugDAO;
import com.xenon.buildmanager.dao.ActivitiesDAO; // DAO class to perform JDBC operations for all activities 
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.NotificationDAO;
import com.xenon.common.dao.AdminDashboardDAO; // DAO class to perform JDBC operations for getting data for admin dasboard

/**
 * 
 * @author suresh.adling
 * @description Controller class for Admin Dashboard and admin menu
 * 
 * 
 */
@SuppressWarnings({ "unchecked" })
@Controller
public class AdminDashboardController {

	@Autowired
	ActivitiesDAO activitiesDAO;

	@Autowired
	AdminDashboardDAO adminDao;

	@Autowired
	NotificationDAO notificationDAO;

	@Autowired
	BuildService buildService;

	@Autowired
	UtilsService utilsService;

	@Autowired
	LoginService loginService;

	@Autowired
	RoleAccessService roleAccessService;

	@Autowired
	BuildDAO buildDAO;

	@Autowired
	BugDAO bugDAO;

	private static final Logger logger = LoggerFactory.getLogger(BuildManagerController.class);

	@Autowired
	DashboardService dashboardService;

	/**
	 * @author suresh.adling
	 * @method getAdmindashboard GET Request
	 * @output get Admin Dashboard view
	 * @param model
	 * @param session
	 * @description This method reads following list by using session stored
	 *              values(custTypeId) and redirects user to admindashboard view
	 *              with the model
	 * @return Model (Users,Projects) & View (onSuccess - admindashboard
	 *         ,onFailure - logout/trialerror/500)
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/admindashboard")
	public ModelAndView getAdmindashboard(@RequestParam(value = "page", defaultValue = "1") int page, Model model,
			HttpSession session) throws Exception {

		logger.info("Redirecting to bmdashboard page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {

			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");
			String schema = session.getAttribute("cust_schema").toString();
			int custTypeId = Integer.parseInt(session.getAttribute("cust_type_id").toString());
			String customerId = session.getAttribute("customerId").toString();
			String timeZone = session.getAttribute("userTimezone").toString();
			Map<String, Object> returnModel = dashboardService.getDashboardData(schema, page, adminDao, activitiesDAO,
					custTypeId, customerId, timeZone);
			Iterator it = returnModel.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				model.addAttribute((String) pair.getKey(), pair.getValue());
				it.remove();
			}
			logger.info(" User is redirected buildmanager  page ");
			return new ModelAndView("admindashboard", "Model", model);
		} else {
			logger.error("User looking for resources is not available or prohibited");
			return new ModelAndView("redirect:/404");
		}

	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/dashandrepo")
	public ModelAndView dashandrepo(@RequestParam(value = "page", defaultValue = "1") int page, Model model,
			HttpSession session) throws Exception {

		logger.info("Redirecting to bmdashboard page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {

			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");

			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());

			Map<String, Object> data = bugDAO.getCommonDashboardData(projectId, userId, schema);
			List<Map<String, Object>> openBugs = (List<Map<String, Object>>) data.get("#result-set-1");
		
			List<Map<String, Object>> userBugs = (List<Map<String, Object>>) data.get("#result-set-2");
			model.addAttribute("userBugsData", utilsService.convertListOfMapToJson(userBugs));
            
			List<Map<String, Object>> rootGraph = (List<Map<String, Object>>) data.get("#result-set-3");
			model.addAttribute("rootGraphData", utilsService.convertListOfMapToJson(rootGraph));

			List<Map<String, Object>> rootTable = (List<Map<String, Object>>) data.get("#result-set-4");
			
			List<Map<String, Object>> buildTc = (List<Map<String, Object>>) data.get("#result-set-5");
			
			//List<Map<String, Object>> cancelBugs = (List<Map<String, Object>>) data.get("#result-set-6");
			
			List<Map<String, Object>> cancelCount = (List<Map<String, Object>>) data.get("#result-set-6");
			model.addAttribute("cancelCountData", utilsService.convertListOfMapToJson(cancelCount != null ? cancelCount : new ArrayList<Map<String, Object>>()));
	

			model.addAttribute("cancelCount", cancelCount);
			model.addAttribute("openBugs", openBugs);
			model.addAttribute("userBugs", userBugs);
			model.addAttribute("rootGraph", rootGraph);
			model.addAttribute("rootTable", rootTable);
			model.addAttribute("buildTc", buildTc);
			model.addAttribute("buildTc1", utilsService.convertListOfMapToJson(buildTc));

			logger.info(" User is redirected dashandrepo  page ");
			return new ModelAndView("dashandrepo", "Model", model);
		} else {
			logger.error("User looking for resources is not available or prohibited");
			return new ModelAndView("redirect:/404");
		}

	}

	/**
	 * @author suresh.adling
	 * @method getAdminMenu GET Request
	 * @output get Admin Menu view
	 * @param model
	 * @param session
	 * @description Method creates the left menu for administration module as
	 *              per resource access to the user
	 * @return Model (userName,UserProfilePhoto,role) & View (onSuccess -
	 *         adminmenu ,onFailure - logout/trialerror/500)
	 * @throws Exception
	 */
	@RequestMapping(value = "/adminmenu")
	public ModelAndView getAdminMenu(Model model, HttpSession session) throws Exception {
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			boolean createStatus = buildDAO.checkBMRoleAccess("createproject",
					session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString());
			boolean viewStatus = buildDAO.checkBMRoleAccess("viewproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString());
			if (createStatus || viewStatus) {
				model.addAttribute("projectAccess", 1);
				if (createStatus) {
					model.addAttribute("createProject", 1);
				}
				if (viewStatus) {
					model.addAttribute("viewProjects", 1);
				}
			}
			createStatus = buildDAO.checkBMRoleAccess("createuser", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString());
			viewStatus = buildDAO.checkBMRoleAccess("viewuser", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString());
			if (createStatus || viewStatus) {
				model.addAttribute("userAccess", 1);
				if (createStatus) {
					model.addAttribute("createUser", 1);
				}
				if (viewStatus) {
					model.addAttribute("viewUser", 1);
				}
			}
			createStatus = buildDAO.checkBMRoleAccess("mail", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString());
			viewStatus = buildDAO.checkBMRoleAccess("accountsettings", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString());
			boolean viewVMStatus = buildDAO.checkBMRoleAccess("viewvm", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString());
			if (createStatus || viewStatus || viewVMStatus) {
				model.addAttribute("mail", 1);
				if (createStatus) {
					model.addAttribute("mail", 1);
				}
				if (viewStatus) {
					model.addAttribute("accountsettings", 1);
				}

				if (viewVMStatus) {
					model.addAttribute("viewvm", 1);
				}
			}
			model.addAttribute("customerLDAPStatus", session.getAttribute("customerLDAPStatus"));
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			return new ModelAndView("adminmenu", "Model", model);
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request is used to read header data
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/adminheader")
	public ModelAndView adminheader(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active,  Customer Schema " + schema);
			Map<String, Object> buildDashboard = notificationDAO
					.getNotifications(Integer.parseInt(session.getAttribute("userId").toString()), schema); // buildDAO.headerData(schema);
			List<Map<String, Object>> allBuildsTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-1");
			List<Map<String, Object>> allBuildsExecutedTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-2");

			List<Map<String, Object>> allBuilds = (List<Map<String, Object>>) buildDashboard.get("#result-set-5");

			List<Map<String, Object>> topBuilds = buildService.getBmDashboardTopBuildsNf(allBuilds,
					allBuildsTestcasesCount, allBuildsExecutedTestcasesCount, schema);
			for (int i = 0; i < topBuilds.size(); i++) {
				topBuilds.get(i).put("user_image",
						utilsService.getFileData((byte[]) topBuilds.get(i).get("user_photo")));
				topBuilds.get(i).put("build_createdate",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(topBuilds.get(i).get("build_createdate").toString())));
			}

			List<Map<String, Object>> notifications = (List<Map<String, Object>>) buildDashboard.get("#result-set-3");
			for (int i = 0; i < notifications.size(); i++) {
				notifications.get(i).put("user_image",
						utilsService.getFileData((byte[]) notifications.get(i).get("user_photo")));
				String dateInString = notifications.get(i).get("notification_date").toString();
				notifications.get(i).put("notification_date", utilsService
						.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), dateInString));
			}
			List<Map<String, Object>> unreadCount = (List<Map<String, Object>>) buildDashboard.get("#result-set-4");
			model.addAttribute("unreadCount", Integer.parseInt(unreadCount.get(0).get("unreadCount").toString()));
			model.addAttribute("nowDate",
					utilsService.getNowDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("prevDate",
					utilsService.getPrevDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("notifications", notifications);
			model.addAttribute("topBuilds", topBuilds);
			return new ModelAndView("adminheader", "Model", model);
		}
	}

	@RequestMapping(value = "/dashboardheader")
	public ModelAndView dashboardheader(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active,  Customer Schema " + schema);
			Map<String, Object> buildDashboard = notificationDAO
					.getNotifications(Integer.parseInt(session.getAttribute("userId").toString()), schema); // buildDAO.headerData(schema);
			List<Map<String, Object>> allBuildsTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-1");
			List<Map<String, Object>> allBuildsExecutedTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-2");

			List<Map<String, Object>> allBuilds = (List<Map<String, Object>>) buildDashboard.get("#result-set-5");

			List<Map<String, Object>> topBuilds = buildService.getBmDashboardTopBuildsNf(allBuilds,
					allBuildsTestcasesCount, allBuildsExecutedTestcasesCount, schema);
			for (int i = 0; i < topBuilds.size(); i++) {
				topBuilds.get(i).put("user_image",
						utilsService.getFileData((byte[]) topBuilds.get(i).get("user_photo")));
				topBuilds.get(i).put("build_createdate",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(topBuilds.get(i).get("build_createdate").toString())));
			}

			List<Map<String, Object>> notifications = (List<Map<String, Object>>) buildDashboard.get("#result-set-3");
			for (int i = 0; i < notifications.size(); i++) {
				notifications.get(i).put("user_image",
						utilsService.getFileData((byte[]) notifications.get(i).get("user_photo")));
				String dateInString = notifications.get(i).get("notification_date").toString();
				notifications.get(i).put("notification_date", utilsService
						.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), dateInString));
			}
			List<Map<String, Object>> unreadCount = (List<Map<String, Object>>) buildDashboard.get("#result-set-4");
			model.addAttribute("unreadCount", Integer.parseInt(unreadCount.get(0).get("unreadCount").toString()));
			model.addAttribute("nowDate",
					utilsService.getNowDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("prevDate",
					utilsService.getPrevDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("notifications", notifications);
			model.addAttribute("topBuilds", topBuilds);
			return new ModelAndView("dashboardheader", "Model", model);
		}
	}

	/**
	 * @author suresh.adling
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :" + ex);
		return new ModelAndView("redirect:/500");
	}
}

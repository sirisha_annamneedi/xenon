package com.xenon.controller;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jadelabs.custmoze.query.release.RedwoodManagement;
import com.xenon.api.buildmanager.UserService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.buildmanager.dao.NotificationDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.buildmanager.dao.UserProfileDAO;
import com.xenon.buildmanager.domain.UserDetails;
import com.xenon.common.dao.OauthUserDAO;
import com.xenon.core.dao.CustomerDAO;

/**
 * 
 * @author suresh.adling
 * @description Handles requests for the create,edit,update users
 * 
 */
@SuppressWarnings("unchecked")
@Controller
public class UserController {

	@Resource(name = "configurationProperties")
	private Properties configurationProperties;

	@Autowired
	NotificationDAO notificationDAO;

	@Autowired
	ModuleDAO modeuleService;

	@Autowired
	UserDAO userDao;

	@Autowired
	ProjectDAO projectDao;

	@Autowired
	UserProfileDAO userProfileService;

	@Autowired
	OauthUserDAO oauthUserDAO;

	@Autowired
	CustomerDAO customerDao;

	@Autowired
	UserService userService;

	@Autowired
	UtilsService utilsService;

	@Autowired
	RoleAccessService roleAccessService;

	@Autowired
	BuildDAO buildDAO;

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	/**
	 * @author suresh.adling
	 * @description createUser GET Request
	 * @param userModel
	 * @param session
	 * @return View (onSuccess - createuser ,onFailure - trialerror/logout/500)
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/createuser", method = RequestMethod.GET)
	public ModelAndView createUser(Model userModel, HttpSession session) throws Exception {
		logger.info("Redirecting to create user page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (buildDAO.checkBMRoleAccess("createuser", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			int custTypeId = Integer.parseInt(session.getAttribute("cust_type_id").toString());
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String btStatus = session.getAttribute("btStatus").toString();
			String tmStatus = session.getAttribute("tmStatus").toString();
			int jenkinStatus = Integer.parseInt(session.getAttribute("jenkinStatus").toString());
			int gitStatus = Integer.parseInt(session.getAttribute("gitStatus").toString());

			Map<String, Object> returnModel = userService.createUser(schema, custTypeId, userId, btStatus, tmStatus,
					jenkinStatus, gitStatus);
			int flag = (Integer) returnModel.get("flag");
			if (flag == 1) {
				Iterator it = returnModel.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry) it.next();
					String key = (String) pair.getKey();
					Object value = pair.getValue();
					if (pair.getKey() == "flag") {
					} else {
						userModel.addAttribute(key, value);
						it.remove();
					}
				}
				session.setAttribute("emailSetting", returnModel.get("emailSetting"));
				session.setAttribute("menuLiText", "Users");
				logger.info(" User is redirected to create user  page ");
				return new ModelAndView("createuser", "userModel", userModel);
			} else {
				logger.error("User has exhausted the limit and redirected to trialerror page ");
				return new ModelAndView("redirect:/trialerror");
			}
		} else
			logger.error("User does not have access to create user request, user is redirected to 500 error page");
		return new ModelAndView("500");
	}

	/**
	 * @author suresh.adling
	 * @description setUserId POST Request
	 * @param userID
	 * @param userFullName
	 * @param session
	 * @param model
	 * @return ModelAndView
	 * @throws Exception
	 */
	@RequestMapping(value = "/setuserID", method = RequestMethod.POST)
	public ModelAndView setUserId(@RequestParam("userID") String userId, @RequestParam("userMailID") String userMailId,
			@RequestParam(value = "userFullName", required = false) String userFullName, HttpSession session,
			Model model) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("selectedUserID", userId);
			session.setAttribute("selectedUserMailID", userMailId);
			session.setAttribute("selectedUserFullName", userFullName);
			session.setAttribute("actionForTab", "edit");
			return new ModelAndView("redirect:/");
		}
	}

	/**
	 * @author suresh.adling
	 * @description editUser GET Request
	 * @param session
	 * @param userModel
	 * @return ModelAndView - edituser
	 * @throws Exception
	 */
	@RequestMapping(value = "/edituser", method = RequestMethod.GET)
	public ModelAndView editUser(HttpSession session, Model userModel) throws Exception {
		logger.info("Redirecting to edit user page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			if (buildDAO.checkBMRoleAccess("edituser", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				utilsService.getLoggerUser(session);
				int userId = Integer.parseInt(session.getAttribute("selectedUserID").toString());

				Map<String, Object> editUserData = userDao.getEditUserData(userId,
						session.getAttribute("cust_schema").toString());

				List<Map<String, Object>> userDetails = (List<Map<String, Object>>) editUserData.get("#result-set-1");
				List<Map<String, Object>> userRoles = (List<Map<String, Object>>) editUserData.get("#result-set-2");
				userModel.addAttribute("userDetails", userDetails);
				userModel.addAttribute("userRoles", userRoles);
				userModel.addAttribute("recentFlag", 0);
				userModel.addAttribute("role", session.getAttribute("role").toString());
				userModel.addAttribute("bugTStatus", Integer.parseInt(session.getAttribute("btStatus").toString()));
				userModel.addAttribute("testMStatus", Integer.parseInt(session.getAttribute("tmStatus").toString()));
				session.setAttribute("menuLiText", "Users");
				logger.info(" User is redirected to edit user  page ");
				return new ModelAndView("edituser", "userModel", userModel);
			} else
				logger.error("User does not have access to edit user request, user is redirected to 500 error page");
			return new ModelAndView("redirect:/500");
		}
	}

	/**
	 * @author abhay.thakur
	 * @description userSettings GET Request
	 * @param session
	 * @param userModel
	 * @return ModelAndView - edituser
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/usersettings", method = RequestMethod.GET)
	public ModelAndView userSetting(HttpSession session, Model userModel) throws Exception {
		logger.info("Redirecting to edit user page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			if (buildDAO.checkBMRoleAccess("edituser", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				utilsService.getLoggerUser(session);
				String schema = session.getAttribute("cust_schema").toString();
				int userId = Integer.parseInt(session.getAttribute("selectedUserID").toString());
				String role = session.getAttribute("role").toString();
				int btStatus = Integer.parseInt(session.getAttribute("btStatus").toString());
				int tmStatus = Integer.parseInt(session.getAttribute("tmStatus").toString());
				String actionForTab = session.getAttribute("actionForTab").toString();
				String userFullName = session.getAttribute("selectedUserFullName").toString();

				int jenkinStatus = Integer.parseInt(session.getAttribute("jenkinStatus").toString());
				int gitStatus = Integer.parseInt(session.getAttribute("gitStatus").toString());
				int redwoodStatus = Integer.parseInt(session.getAttribute("redwoodStatus").toString());
				int apiStatus = Integer.parseInt(session.getAttribute("apiStatus").toString());
				Map<String, Object> returnData = userService.userSetting(schema, userId, role, btStatus, tmStatus,
						actionForTab, userFullName, jenkinStatus, gitStatus, redwoodStatus, apiStatus);

				Iterator it = returnData.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry) it.next();
					userModel.addAttribute((String) pair.getKey(), pair.getValue());
					it.remove();
				}
				session.setAttribute("menuLiText", "Users");

				logger.info(" User is redirected to user setting page ");
				return new ModelAndView("usersettings", "userModel", userModel);
			} else
				logger.error("User does not have access to edit user request, user is redirected to 500 error page");
			return new ModelAndView("redirect:/500");
		}
	}

	/**
	 * @author suresh.adling
	 * @description viewUser GET Request - view list of users
	 * @param model
	 * @param session
	 * @return ModelAndView - viewuser
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/viewuser")
	public ModelAndView viewUser(@RequestParam(value = "page", defaultValue = "1") int page, Model model,
			HttpSession session) throws Exception {
		logger.info("Redirecting to view user page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			if (buildDAO.checkBMRoleAccess("viewuser", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				utilsService.getLoggerUser(session);
				boolean createStatus = buildDAO.checkBMRoleAccess("createuser",
						session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString());
				String schema = session.getAttribute("cust_schema").toString();
				Map<String, Object> returndata = userService.viewUser(page, createStatus, schema);

				Iterator it = returndata.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry) it.next();
					String key = (String) pair.getKey();
					Object value = pair.getValue();
					model.addAttribute(key, value);
					it.remove();
				}
				session.setAttribute("menuLiText", "Users");
				logger.info(" User is redirected to view user  page ");
				return new ModelAndView("viewuser", "model", model);
			}
			logger.error("User does not have access to view user request, user is redirected to 500 error page");
			return new ModelAndView("redirect:/500");
		}
	}

	/**
	 * @author suresh.adling
	 * @description getUserList GET Request - view list of users
	 * @param model
	 * @param session
	 * @return ModelAndView - viewuser
	 * @throws Exception
	 */
	/*
	 * @RequestMapping(value = "/users") public ModelAndView getUserList(Model
	 * model, HttpSession session) throws Exception { logger.
	 * info("Redirecting to view user page and checking for active session"); if
	 * (session.getAttribute("userId") == null) {
	 * logger.error("Session has expired,users is redirected to login page"); return
	 * new ModelAndView("redirect:/"); } else { if (access.checkBMAccess("viewuser",
	 * session)) { Utils.getLoggerUser(session); boolean assignProjectStatus =
	 * access.checkBMAccess("assignproject", session); if (assignProjectStatus) {
	 * model.addAttribute("assignProjectStatus", 1); } Map<String, Object>
	 * viewUserData = userService
	 * .getViewUserData(session.getAttribute("cust_schema").toString());
	 * List<Map<String, Object>> listUser = (List<Map<String, Object>>)
	 * viewUserData.get("#result-set-1"); for (int i = 0; i < listUser.size(); i++)
	 * { String logo = login.getImage((byte[]) listUser.get(i).get("user_photo"));
	 * listUser.get(i).put("user_photo", logo); }
	 * model.addAttribute("allUserDetails", listUser);
	 * session.setAttribute("menuLiText", "Users");
	 * logger.info(" User is redirected to view user  page "); return new
	 * ModelAndView("users", "model", model); } logger.
	 * error("User does not have access to users request, user is redirected to 500 error page"
	 * ); return new ModelAndView("redirect:/500"); } }
	 */

	/**
	 * @author abhay.thakur
	 * @description insertUser POST Request - accepts user details and add a user
	 * @param emailId
	 * @param firstName
	 * @param lastName
	 * @param passWord
	 * @param userRole
	 * @param tmStatus
	 * @param btStatus
	 * @param userStatus
	 * @param projects
	 * @param session
	 * @param userModel
	 * @return ModelAndView - viewuser
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertuser", method = RequestMethod.POST)
	public ModelAndView insertUser(@RequestParam("emailId") String emailId, @RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName, @RequestParam("usernameLogin") String userLoginName,
			@RequestParam(value = "password", defaultValue = "admin@123") String passWord,
			@RequestParam(value = "userRole", defaultValue = "0") String userRole,
			@RequestParam(value = "testManagerStatus") String tmStatus,
			@RequestParam(value = "bugTrackerStatus") String btStatus,
			@RequestParam(value = "userStatus") String userStatus,
			@RequestParam(value = "jenkinStatus", defaultValue = "2") int jenkinStatus,
			@RequestParam(value = "gitStatus", defaultValue = "2") int gitStatus,
			@RequestParam(value = "redwoodStatus", defaultValue = "0") int redwoodStatus,
			@RequestParam(value = "apiStatus", defaultValue = "0") int apiStatus,
			@RequestParam(value = "projects", defaultValue = "No Projects") String projects, HttpSession session,
			Model userModel) throws Exception {
		logger.info("In post request to insert user");
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			String CurrentDate = utilsService.getCurrentDateTime();
			int flag = userService.insertUser(emailId, firstName, lastName, passWord, userRole, tmStatus, btStatus,
					userStatus, schema, session.getAttribute("customerName").toString(), projects, CurrentDate,
					configurationProperties.getProperty("XENON_REPOSITORY_PATH"), userLoginName, jenkinStatus,
					gitStatus,redwoodStatus,apiStatus);

			if (flag == 201) {
				// setting session variables for toaster
				if (session.getAttribute("redwood_url") != null) {
					try {
						String redWoodUrl = session.getAttribute("redwood_url").toString();
						RedwoodManagement redwoodManagement = new RedwoodManagement();
						redwoodManagement.createUser(redWoodUrl, firstName, lastName, emailId);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				String tosterUserFullName = firstName + " " + lastName;
				session.setAttribute("tosterUserFullName", tosterUserFullName);
				session.setAttribute("tosterStatusForUser", 1);
				logger.info(" User is redirected to create user  page ");
				return new ModelAndView("redirect:/viewuser");
			} else if (flag == 403) {
				logger.error("User does not have access to create user request, user is redirected to 500 error page");
				return new ModelAndView("redirect:/" + flag);
			} else
				logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("redirect:/" + flag);
		}

	}

	/**
	 * @author suresh.adling
	 * @description checkEmailId POST Request - check for duplicate email id
	 * @param emailId
	 * @param session
	 * @return boolean value If email is already present in database then it will
	 *         return false.
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/checkuser")
	public @ResponseBody String checkEmailId(@RequestParam("emailId") String emailId,
			@RequestParam("userName") String userName, HttpSession session) {
		logger.info("In post request to check user for duplicate email id");
		logger.info("Checking for active session");
		utilsService.getLoggerUser(session);
		String emailFlag = userService.checkUser(emailId);
		boolean usernameFlag = userService.checkUserName(userName);
		JSONObject json = new JSONObject();
		json.put("emailFlag", emailFlag);
		json.put("usernameFlag", usernameFlag);
		return json.toString();

	}

	/**
	 * @author suresh.adling
	 * @description removeUser POST Request - remove user by user id
	 * @param userId
	 * @param session
	 * @return boolean value
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/removeuser")
	public @ResponseBody String removeUser(@RequestParam("userID") int userId, HttpSession session) {
		logger.info("In post request to remove user");
		logger.info("Checking for active session");
		utilsService.getLoggerUser(session);
		return userService.removeUser(userId, session.getAttribute("cust_schema").toString());
	}

	/**
	 * @author abhay.thakur
	 * @description updateUser POST Request - accept and update user details
	 * @param userId
	 * @param firstName
	 * @param lastName
	 * @param userRole
	 * @param tmStatus
	 * @param btStatus
	 * @param userStatus
	 * @param session
	 * @return ModelAndView - edituser
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateuser", method = RequestMethod.POST)
	public ModelAndView updateUser(@RequestParam("userId") int userId, @RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName, @RequestParam("userRole") String userRole,
			@RequestParam(value = "testManagerStatus") String tmStatus,
			@RequestParam(value = "bugTrackerStatus") String btStatus,
			@RequestParam(value = "userStatus") String userStatus,
			@RequestParam(value = "jenkinStatus", defaultValue = "2") int jenkinStatus,
			@RequestParam(value = "gitStatus", defaultValue = "2") int gitStatus,
			@RequestParam(value = "redwoodStatus", defaultValue = "0") int redwoodStatus,
			@RequestParam(value = "apiStatus", defaultValue = "0") int apiStatus, HttpSession session)
			throws Exception {
		logger.info("In post request to insert user");
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			if (buildDAO.checkBMRoleAccess("edituser", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				utilsService.getLoggerUser(session);
				boolean flag = userService.updateUser(userId, firstName, lastName, userRole, tmStatus, btStatus,
						userStatus, session.getAttribute("cust_schema").toString(), utilsService.getCurrentDateTime(),
						jenkinStatus, gitStatus,redwoodStatus,apiStatus);
				int loginUserID = Integer.parseInt(session.getAttribute("userId").toString());
				if (loginUserID == userId) {
					session.setAttribute("tmStatus", tmStatus);
					session.setAttribute("btStatus", btStatus);
				}
				if (flag == true) {
					String tosterUserFullName = firstName + " " + lastName;
					session.setAttribute("tosterUserFullName", tosterUserFullName);
					session.setAttribute("tosterStatusForUser", 2);
					session.setAttribute("actionForTab", "edit");
					logger.info(" User is redirected to view user  page ");
					return new ModelAndView("redirect:/viewuser");
				} else {
					logger.info(" User is redirected to edit user  page ");
					return new ModelAndView("redirect:/usersettings");
				}
			} else
				logger.error("User does not have access to edit user request, user is redirected to 500 error page");
			return new ModelAndView("redirect:/500");
		}
	}

	/**
	 * @author suresh.adling
	 * @description forgotPassword GET Request
	 * @param model
	 * @param session
	 * @return ModelAndView - forgotpassword
	 * @throws Exception
	 */
	@RequestMapping(value = "/forgotpassword")
	public ModelAndView forgotPassword(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to forgotpassword page and checking for active session");
		logger.info(" User is redirected to forgotpassword  page ");
		model.addAttribute("SendMail", "-1");
		return new ModelAndView("forgotpassword", "Model", model);
	}

	/**
	 * @author suresh.adling
	 * @description set forgot password POST Request (Generate random password and
	 *              update it )
	 * @param emailId
	 * @param model
	 * @param session
	 * @return ModelAndView - forgotpassword
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/setforgotpassword")
	public ModelAndView setforgotpassword(@RequestParam("emailId") String emailId, Model model, HttpSession session)
			throws Exception {
		logger.info("In post request to insert user");
		logger.info("Checking for active session");
		boolean flag = userDao.checkUserEmailIdExist(emailId);
		boolean updateStatus;
		if (flag) {
			int custId = customerDao.getCustomerIdByEmailId(emailId);
			List<Map<String, Object>> custDetails = customerDao.getCustomerDetailsById(custId);
			List<Map<String, Object>> userDetails = userDao.getUserDetailsByEmailId(emailId);
			String newPassword = utilsService.generateRandomString(8);

			UserDetails user = new UserDetails();
			user.setEmailId(emailId);
			user.setUserId(Integer.parseInt(userDetails.get(0).get("user_id").toString()));
			user.setPassword(newPassword);
			updateStatus = userProfileService.updatePassword(user,
					custDetails.get(0).get("customer_schema").toString());
			if (updateStatus == true) {
				userDao.sendMailOnForgotPassword(custDetails, user,
						custDetails.get(0).get("customer_schema").toString());
				flag = true;
			} else
				flag = false;
		} else {
			session.setAttribute("SendMail", "2");
			logger.info(" User is redirected to forgotpassword  page ");
			return new ModelAndView("redirect:/forgotpassword");
		}
		if (flag) {
			logger.info(" User is redirected to forgotpassword  page ");
			session.setAttribute("SendMail", "1");
			return new ModelAndView("redirect:/forgotpassword");
		}
		logger.info(" User is redirected to forgotpassword  page ");
		session.setAttribute("SendMail", "0");
		return new ModelAndView("redirect:/forgotpassword");
	}

	/**
	 * @author suresh.adling
	 * @description set forgot password GET Request
	 * @param model
	 * @param session
	 * @return ModelAndView - forgotpassword
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/setforgotpassword")
	public ModelAndView setforgotpassword(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to setforgotpassword  page and checking for active session");
		logger.info(" User is redirected to setforgotpassword   page ");
		return new ModelAndView("redirect:/forgotpassword");
	}

	/**
	 * @author suresh.adling
	 * @description userAssignedProject GET Request - view list of assigned projects
	 * @param model
	 * @param session
	 * @return ModelAndView - userassignedproject
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/userassignedproject")
	public ModelAndView userAssignedProject(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to user assigned project page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("selectedUserID").toString());
			String schema = session.getAttribute("cust_schema").toString();

			Map<String, Object> projects = userDao.getUserAssignedProjects(userID, schema);

			List<Map<String, Object>> details = (List<Map<String, Object>>) projects.get("#result-set-1");
			model.addAttribute("details", details);
			model.addAttribute("userFullName", session.getAttribute("selectedUserFullName"));
			model.addAttribute("userID", userID);
			logger.info(" User is redirected to userassignedproject  page ");
			return new ModelAndView("userassignedproject", "userModel", model);
		}
	}

	/**
	 * @author suresh.adling
	 * @description assignApplications GET Request - view list of projects to assign
	 * @param model
	 * @param session
	 * @return ModelAndView - assignapplications
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/assignapplications")
	public ModelAndView assignApplications(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to user assign applications page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("selectedUserID").toString());
			String schema = session.getAttribute("cust_schema").toString();

			Map<String, Object> projects = userDao.getApplicationsToAssign(userID,
					Integer.parseInt(session.getAttribute("userId").toString()), schema);

			List<Map<String, Object>> details = (List<Map<String, Object>>) projects.get("#result-set-1");
			model.addAttribute("details", details);
			model.addAttribute("userFullName", session.getAttribute("selectedUserFullName"));
			model.addAttribute("userID", userID);
			logger.info(" User is redirected to assignapplications  page ");
			return new ModelAndView("assignapplications", "userModel", model);
		}
	}

	/**
	 * @author navnath.damale
	 * @param appIds
	 * @param appName
	 * @param userId
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/assignusertoproject", method = RequestMethod.POST)
	public ModelAndView assignUsertoProject(@RequestParam("appId") String appIds, Model model, HttpSession session)
			throws Exception {
		logger.info("Post method to assign application to users. Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);

			logger.info("Cheking for User's access to assign application");
			if (buildDAO.checkBMRoleAccess("assignproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to assign application");

				int userId = Integer.parseInt(session.getAttribute("selectedUserID").toString());
				String userMailId = session.getAttribute("selectedUserMailID").toString();

				userService.assignProjectsToUser(userId, userMailId, appIds, schema);
				/*
				 * String userId=session.getAttribute("selectedUserID").toString(); String
				 * userMailId =session.getAttribute("selectedUserMailID").toString();
				 * 
				 * userAPI.assignusertoproject(userId,userMailId,appIds,schema);
				 */
				session.setAttribute("actionForTab", "members");
				logger.info(" User is redirected to Applications settings page ");
				return new ModelAndView("redirect:/usersettings");

			} else {
				logger.warn("User has no access to Edit application. Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method assignProjectToUser a POST Request
	 * @param model
	 * @param session
	 * @param selectedUsers
	 * @return
	 */
	@RequestMapping(value = "/assignmoduletouser", method = RequestMethod.POST)
	public @ResponseBody ModelAndView assignModuletoUser(Model model, HttpSession session,
			@RequestParam("projectID") int projectID,
			@RequestParam(value = "selectedModules", defaultValue = "No Modules") String selectedModules) {
		logger.info("Ajax Post request to assign or  module to users");
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		int user = Integer.parseInt(session.getAttribute("userId").toString());
		logger.info(" Session is active, userId is " + user + " Customer Schema " + schema);

		int userId = Integer.parseInt(session.getAttribute("selectedUserID").toString());

		userService.assignModuleToUser(userId, projectID, selectedModules, schema);

		session.setAttribute("actionForTab", "module");
		logger.info("Returning to ajax post request");

		return new ModelAndView("redirect:/usersettings");
	}

	/**
	 * @author suresh.adling
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :" + ex);
		return new ModelAndView("redirect:/500");
	}

	@RequestMapping(method = RequestMethod.POST, value = "/resetpassword")
	public ModelAndView resetPassword(@RequestParam("userId") String userId, @RequestParam("password") String password,
			HttpSession session) {
		String schema = session.getAttribute("cust_schema").toString();
		userService.updatePasswordByUserId(userId, password, schema);
		logger.info("Password reseted");
		// System.out.println("Password reseted");
		return new ModelAndView("redirect:/viewuser");
	}
}

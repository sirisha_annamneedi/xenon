package com.xenon.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.common.CommonService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.NotificationDAO;

/**
 * This controller includes requests of a Notifications
 * 
 * @author bhagyashri.ajmera
 *
 */
@SuppressWarnings({ "unused" })
@Controller
public class NotificationsController {

	private static final Logger logger = LoggerFactory.getLogger(NotificationsController.class);
	
	@Autowired
	RoleAccessService roleAccessService;
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	NotificationDAO notificationDAO;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	BuildDAO buildDAO;
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request is used to view all notifications
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/viewnotifications")
	public ModelAndView viewnotifications(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active,  Customer Schema " + schema);
			Map<String, Object> buildDashboard =notificationDAO.getNotifications(Integer.parseInt(session.getAttribute("userId").toString()), schema); //buildDAO.headerData(schema);
			List<Map<String, Object>> allBuildsTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-1");
			List<Map<String, Object>> allBuildsExecutedTestcasesCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-2");
			
			List<Map<String, Object>> topBuilds = buildDAO.getBmDashboardTopBuilds(allBuildsTestcasesCount,
					allBuildsExecutedTestcasesCount, schema);
			for (int i = 0; i < topBuilds.size(); i++) {
				topBuilds.get(i).put("build_createdate",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(topBuilds.get(i).get("build_createdate").toString())));
			}
			
			List<Map<String, Object>> notifications =  (List<Map<String, Object>>) buildDashboard
					.get("#result-set-3");
			for (int i = 0; i < notifications.size(); i++) {
				String dateInString = notifications.get(i).get("notification_date").toString();
				notifications.get(i).put("notification_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), dateInString));
			}
			model.addAttribute("nowDate", utilsService.getNowDateOfTimezone(session.getAttribute("userTimezone").toString()));
			model.addAttribute("prevDate",
					utilsService.getPrevDateOfTimezone(session.getAttribute("userTimezone").toString()));
			
			model.addAttribute("notifications", notifications);
			model.addAttribute("topBuilds", topBuilds);
			logger.info("Returning view notifications");
			return new ModelAndView("viewnotifications", "Model", model);
		}
		
	}
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request is used to mark notification as read
	 * 
	 * @param notificationId
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/marknotificationasread")
	public ModelAndView markNotificationAsRead(@RequestParam("notificationId") int notificationId,Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		notificationDAO.markNotificationAsRead(notificationId,Integer.parseInt(session.getAttribute("userId").toString()), schema);
		logger.info("Notifications marked as read");
		return new ModelAndView("viewnotifications");
		}
	}
	
	@RequestMapping(value = "/updateNotificationReadCountFlag")
	public ModelAndView updateNotificationReadCountFlag(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		notificationDAO.updateNotificationReadCountFlag(Integer.parseInt(session.getAttribute("userId").toString()), schema);
		logger.info("Notifications marked as read");
		return new ModelAndView("viewnotifications");
		}
	}
	
	
	
	/**
	 * Description : This request handles the exceptions in the class
	 * 
	 * @param ex
	 * @param response
	 * @param session
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		return new ModelAndView("redirect:/500");
	}
}

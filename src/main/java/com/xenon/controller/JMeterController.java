package com.xenon.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.jmeter.dao.JMeterDAO;
/**
 * 
 * @author navnath.damale
 *
 */

@Controller
public class JMeterController {

	@Autowired
	JMeterDAO jMeterService;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	UtilsService utilsService;

	private static final Logger logger = LoggerFactory.getLogger(JMeterController.class);
	
	@RequestMapping(value = "/jmetergraph")
	public ModelAndView viewJMeter(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			List<Map<String, Object>> responseGraph=jMeterService.getResponseGraph(userId, schema);
			
			/*Set<Object> x_lable=new HashSet<Object>();
			Set<Object> graph_lable=new HashSet<Object>();
			for(int i=0;i<responseGraph.size();i++)
			{
				x_lable.add(responseGraph.get(i).get("threadName"));
				graph_lable.add(responseGraph.get(i).get("label"));
			}*/
			
			JSONArray jArray=new JSONArray(responseGraph);
			
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("ResponseGraph", jArray);
		/*	model.addAttribute("ResponseXlable", x_lable);
			model.addAttribute("ResponseGlable", graph_lable);*/
			return new ModelAndView("jmetergraph", "Model", model);
		}
	}

	/**
	 * 
	 * @param ex
	 * @param response
	 * @param session
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :"+ex);
		return new ModelAndView("redirect:/500");
	}
}

package com.xenon.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.TimezoneDAO;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.buildmanager.dao.UserProfileDAO;
import com.xenon.buildmanager.domain.UserDetails;
/**
 * 
 * @author prafulla.pol
 * @Description Profile class provide method to get profile details.
 * 				It also contain methods to update profile details and password.
 *
 */
@Controller
public class ProfileController {
	
	@Resource(name = "configurationProperties")
	private Properties configurationProperties;

	@Autowired
	UserProfileDAO userProfileService;

	@Autowired
	UserDAO userService;

	@Autowired
	TimezoneDAO timezoneDAO;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	UtilsService utilsService;
	
	private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);
	
	/**
	 * @author prafulla.pol
	 * @method profile a GET Request
	 * @param userProfileModel
	 * @param session
	 * @return  Model (timezoneDetails,userDetails) &
	 *  	   View (onSuccess - profile ,
	 *               onFailure - logout)
	 * @throws Exception
	 */
	@RequestMapping(value = "/profile")
	public ModelAndView profile(Model userProfileModel, HttpSession session) throws Exception {
		logger.info("Redirecting to profile and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Settings");
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userId+ " Customer Schema " + schema);
			List<Map<String, Object>> userDetails = userService.getUserDetails(userId,schema);
			List<Map<String, Object>> timezoneDetails =timezoneDAO.getTimezones(schema);
			userDetails.get(0).put("photo", loginService.getImage((byte[]) userDetails.get(0).get("user_photo")));
			userProfileModel.addAttribute("timezoneDetails", timezoneDetails);
			userProfileModel.addAttribute("userDetails", userDetails);
			logger.info(" User is redirected to profile");
			return new ModelAndView("profile", "userProfileModel", userProfileModel);
		}
	}

	/**
	 * @author prafulla.pol
	 * @method updateProfile a POST Request
	 * @param firstName
	 * @param lastName
	 * @param location
	 * @param aboutMe
	 * @param timezone
	 * @param file
	 * @param session
	 * @param profileModel
	 * @return Model (userDetails) &
	 *  	   View (onSuccess - profile ,
	 *               onFailure - logout/500)
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateprofile", method = RequestMethod.POST)
	public ModelAndView updateProfile(@RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName, @RequestParam("location") String location,
			@RequestParam("aboutMe") String aboutMe,
			@RequestParam("timezone") String timezone,
			@RequestParam(value = "uploadImage", required = false) MultipartFile file, HttpSession session,
			Model profileModel) throws Exception {
		logger.info("Post method to update user profile details.Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userId+ " Customer Schema " + schema);
			
			boolean flag = false;
			UserDetails user = new UserDetails();
			user.setUserId(userId);
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setLocation(location);
			String about_me = aboutMe.trim();
			user.setAboutMe(about_me);
			user.setTimezone(Integer.parseInt(timezone));
			if (!file.isEmpty()) {
				byte[] bytes = file.getBytes();
				InputStream in = new ByteArrayInputStream(bytes);
				BufferedImage bImageFromConvert = ImageIO.read(in);
				String parentPath = configurationProperties.getProperty("XENON_REPOSITORY_PATH") + "repository";
				org.springframework.core.io.Resource resource = new ClassPathResource(parentPath);
				File profileImage = new File(resource.getFile().getAbsolutePath()+File.separator+"upload"+File.separator + file.getOriginalFilename());
				ImageIO.write(bImageFromConvert, "png", profileImage);
				user.setUserPhotoName(profileImage.getAbsolutePath());
				flag = userProfileService.updateUserWithProfilePhoto(user,schema);
				profileImage.delete();

			} else {
				flag = userProfileService.updateUser(user,schema);
			}

			List<Map<String, Object>> userDetails = userService.getUserDetails(userId,schema);
			profileModel.addAttribute("userDetails", userDetails);

			if (flag) {
				session.setAttribute("userTimezone",timezoneDAO.getTimezonesDetailsById(Integer.parseInt(timezone), session.getAttribute("cust_schema").toString()).get(0).get("timezone_id"));
				logger.info(" User is redirected to profile ");
				return new ModelAndView("redirect:/profile", "userProfileModel", profileModel);
			} else{
				logger.warn("Error : Error occured while updating user details . Redirected to 500");
				return new ModelAndView("redirect:/500");
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method updatePassword a POST Request
	 * @param password
	 * @param newPassword
	 * @param session
	 * @param profileModel
	 * @return integer values based on the password update status
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatepassword", method = RequestMethod.POST)
	public @ResponseBody int updatePassword(@RequestParam("password") String password,
			@RequestParam("newPassword") String newPassword, HttpSession session, Model profileModel) throws Exception {
		logger.info("Post method to update user password");
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		int userId = Integer.parseInt(session.getAttribute("userId").toString());
		logger.info(" Session is active, userId is " + userId+ " Customer Schema " + schema);
		
		if (utilsService.getPassword(password)
				.equals(userService.getUpdatePasswordUserDetails(userId, schema).get(0).get("user_password"))) {
			UserDetails user = new UserDetails();
			user.setUserId(userId);
			user.setPassword(newPassword);
			boolean flag = userProfileService.updatePassword(user, schema);
			logger.info(" Returning update status to ajax post request");
			if (flag) {
				session.setAttribute("passUpdateStatus", 1);
				return 1;
			} else
				return 3;
		} else {
			session.setAttribute("passUpdateStatus", 2);
			return 2;
		}
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		return new ModelAndView("redirect:/500");
	}
}

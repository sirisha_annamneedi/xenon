package com.xenon.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jadelabs.custmoze.query.release.RedwoodManagement;
import com.xenon.api.administration.GitDetailsService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.api.testmanager.TestCaseService;
import com.xenon.buildmanager.dao.AutomationBuildDAO;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.BuildExecDetailsDAO;
import com.xenon.buildmanager.domain.BuildExecDetails;
import com.xenon.controller.utils.Constants;
import com.xenon.remote.domain.CustomerMachineOperation;
import com.xenon.remote.domain.RemoteMachineException;

@Component
@Controller
public class CustomerAutomationController {
	
	@Resource(name = "configurationProperties")
	private Properties configurationProperties;
	
	private static final Logger logger = LoggerFactory.getLogger(BugController.class);
	private CustomerMachineOperation customerMachineOperation;

	@Autowired
	RoleAccessService roleAccessService;
	
	@Autowired
	BuildExecDetailsDAO buildExecDetailsDAO;

	@Autowired
	AutomationBuildDAO automationBuildDAO;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	BuildDAO buildDAO;
	
	@Autowired
	TestCaseService testCaseService;
	
	@Autowired
	GitDetailsService gitDetailsService;
	
	@Async
	@RequestMapping(value = "/startonpremiseexecution", method = RequestMethod.POST)
	public @ResponseBody String startOnPremiseExecution(@RequestParam("selectVM") String selectVM,@RequestParam("exebrowser") final String exebrowser,@RequestParam("selectBrowser") String browser,
			@RequestParam(value="selectEnv[]",defaultValue="0") final String env,
			@RequestParam(value="selectMailGroup",defaultValue="0") int mailGroupId,
			@RequestParam("stepwiseScreenshotStatus") int stepWiseScreenshot,
			@RequestParam("selectVMExecCount") String selectVMExecCount,
			@RequestParam(value="selectRepo",defaultValue="0") int selectRepo,
			@RequestParam("reportBugStatus") int reportBug,
			@RequestParam("selectAuthentication") String selectAuthentication,
			@RequestParam("basicUsername") String basicUsername,
			@RequestParam("basicPassword") String basicPassword,final HttpSession session) throws Exception {

		utilsService.getLoggerUser(session);
		logger.info("In on premise customer execution method");
		customerMachineOperation = CustomerMachineOperation.getInstance();
		String schema = session.getAttribute("cust_schema").toString();
		int userId = Integer.parseInt(session.getAttribute("userId").toString());
		int buildId = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());
		int customerId = Integer.parseInt(session.getAttribute("customerId").toString());
		List<Map<String, Object>> execVmDetails = buildExecDetailsDAO.getCustomerVMDetailsbyId(customerId, Integer.parseInt(selectVM));
		if(Integer.parseInt(execVmDetails.get(0).get("current_execs").toString())==Integer.parseInt(selectVMExecCount)){
			logger.info("Getting instance for CustomerMachineOperation class");
			String responseStatus = null;
			try {
				if (buildDAO.checkBMRoleAccess("createbuild",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
					try{
						responseStatus = customerMachineOperation.handleRemoteStatus(customerMachineOperation,configurationProperties.getProperty("XENON_SERVER_WS"));
					}catch (Exception e) {
						responseStatus=Constants.XENON_SERVER_INACTIVE;
					}
					if (responseStatus.equals(Constants.XENON_STATUS_SUCCESS)) {
						String freeVMList = customerMachineOperation
								.postRequest(configurationProperties.getProperty("XENON_SERVER_WS")+ "/getcustomerfreevm?customerId="+customerId+"&vmId="+selectVM, "");

						
						HttpHeaders headers = new HttpHeaders();
                        headers.add("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE);
                        headers.add("Accept", MediaType.APPLICATION_JSON.toString());

                        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<String, String>();
                        requestBody.add("customerId", String.valueOf(customerId));
                        requestBody.add("vmId", selectVM);
                        HttpEntity<MultiValueMap<String, String>> formEntity = new HttpEntity<MultiValueMap<String,String>>(requestBody, headers);
                        
                        
                        //RestTemplate restTemplate = new RestTemplate();
                        //String freeVMList = restTemplate.exchange(configurationProperties.getProperty("XENON_SERVER_WS")+ "/getcustomerfreevm", HttpMethod.POST, formEntity, String.class).getBody();
                        /*coding adding end */

						JSONArray freeVMListArray = new JSONArray(freeVMList);
						if (freeVMListArray.length() > 0) {
							JSONObject freeVMObject = freeVMListArray.getJSONObject(0);
							String freeVMId = freeVMObject.get("cust_vm_id").toString();
							//adding cust_vm_id to session to reuse in /autoexecute API.
							session.setAttribute("cust_vm_id", freeVMId);
							JSONObject postParameter = new JSONObject();
							postParameter.put("vmId", freeVMId);
							postParameter.put("userId", userId);
							postParameter.put("buildId", buildId);
							postParameter.put("custId", customerId);
							String allocatedVM = customerMachineOperation.postRequest(
									configurationProperties.getProperty("XENON_SERVER_WS") + "/allocatecustomervm?postjson=", postParameter.toString());
							JSONArray allocatedVMArray = new JSONArray(allocatedVM);
							if (allocatedVMArray.length() > 0) {

								List<Map<String, Object>> getVMDetails = buildExecDetailsDAO.getVMDetails(schema, userId,
										buildId);
								int allocateVMId = Integer.parseInt(getVMDetails.get(0).get("xe_vm_id").toString());
								String REMOTE_URL = getVMDetails.get(0).get("inetaddress").toString() + "/XenonClientWS";
								
								try {
								responseStatus = customerMachineOperation.getRemoteMachineStatus(REMOTE_URL);
								}catch(Exception e){
									customerMachineOperation.postRequest(
											configurationProperties.getProperty("XENON_SERVER_WS") + "/deallocatecustomervm?allocatedvmid="+freeVMId+"&schema="+schema+"&buildId="+buildId+"&userId="+userId,"");
								}
								if (responseStatus.equals("200")) {
									session.setAttribute("newBuildTypeForTab", "automation");
									int envStatus=2;
									if(env.equalsIgnoreCase("0")){
										envStatus=2;
									}else{
										envStatus=1;
									}
									BuildExecDetails buildExecDetails = buildExecDetailsDAO.setBuildExecDetailsValues(
											buildId, browser, mailGroupId, stepWiseScreenshot, reportBug, userId,
											allocateVMId,envStatus);
									int flag;
										if(envStatus==1)
											flag = buildExecDetailsDAO.insertBuildExecDetails(buildExecDetails,env, schema);
											else
												flag = buildExecDetailsDAO.insertBuildExecDetails(buildExecDetails,"", schema);
									if (flag == 1) {
										
                                                   logger.info("In buildautomation execution method");
                                                   customerMachineOperation = CustomerMachineOperation.getInstance();
                                                  // int userId = Integer.parseInt(session.getAttribute("userId").toString());
                                                 //  int buildId = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());
                                                  // String schema = session.getAttribute("cust_schema").toString();
                                                   List<Map<String, Object>> autoBuildList = automationBuildDAO.getAutoBuildDetail(schema, buildId);
                                                   String userName = autoBuildList.get(0).get("user_name").toString();
                                                   String dirName = schema + userName + customerMachineOperation.getTimeStamp();

                                                   try {
													if (customerMachineOperation.createDirectory(configurationProperties.getProperty("AUTOMATION_REPOSITORY_PATH")+File.separator+"repository"+File.separator+"upload"+File.separator+"tmpFiles"+File.separator + dirName,true)) {
													         if (customerMachineOperation.createFile(configurationProperties.getProperty("AUTOMATION_REPOSITORY_PATH")+File.separator+"repository"+File.separator+"upload"+File.separator+"logFiles"+File.separator + dirName + ".txt",true)) {
													                flag = automationBuildDAO.updateBuildLogfile(buildId, dirName, schema);
													                if (flag == 1) {
													                      flag = automationBuildDAO.insertExecution(dirName, buildId, schema);
													                      if (flag == 1) {
													                             String buildName = autoBuildList.get(0).get("build_name").toString();
													                             String projectLocation = autoBuildList.get(0).get("project_location").toString();
													                             String inetAddress = autoBuildList.get(0).get("inetaddress").toString();
													                             String REMOTE_URL_CLIENT = inetAddress + "/XenonClientWS";
													                             String stepWiseScreenShot = utilsService
													                                   .getScreenShotStatusFromId(autoBuildList.get(0).get("step_wise_screenshot").toString());
													                             String reportBugStatus = utilsService
													                                   .getRerportBugStatusFromId(autoBuildList.get(0).get("report_bug").toString());
													                             JSONObject autoPostParameter = new JSONObject();
													                             if(selectRepo!=0){
													                            	 autoPostParameter.put("GIT_STATUS", 1);
													                            	 List<Map<String, Object>> gitProjectDetails=gitDetailsService.getProjectDetailsByRepositoryId(selectRepo, userId, schema);
													                            	 if(gitProjectDetails.size()>0){
													                            		 autoPostParameter.put("GIT_PROJECT_FOUND", 1);
													                            		 autoPostParameter.put("GIT_URL", gitProjectDetails.get(0).get("git_server_url"));
													                            		 autoPostParameter.put("GIT_USER", gitProjectDetails.get(0).get("username"));
													                            		 autoPostParameter.put("GIT_API_TOKEN", gitProjectDetails.get(0).get("api_token"));
													                            		 autoPostParameter.put("GIT_BRANCH_NAME", gitProjectDetails.get(0).get("branch_name"));
													                            		 
													                            	 }else{
													                            		 autoPostParameter.put("GIT_PROJECT_FOUND", 2);
													                            	 }
													                             }else{
													                            	 autoPostParameter.put("GIT_STATUS", 2);
													                             }
													                             
													                             autoPostParameter.put("ENV_STATUS", envStatus);
													                             autoPostParameter.put("ENV_ID", env);
													                             
													                             List<Map<String, Object>> autoTestList=null;
													                             
													                             if(session.getAttribute("automationExecBuildType").equals("2")) {
													                            	 autoTestList = automationBuildDAO.getAutomationTestCases(schema,
													                                          buildId);
													                             }
													                             if(session.getAttribute("automationExecBuildType").equals("4")){
													                            	 autoTestList = automationBuildDAO.getApiTestCases(schema,
													                                          buildId);
													                            	 for(Map<String, Object> map : autoTestList){
													                            		 
													                            		 buildDAO.insertcstarttime( buildId, map.get("test_prefix").toString(), schema);
													                            	 }  
													                            	 autoPostParameter.put("EXE_TYPE", "API");
													                            	 if(selectAuthentication.equals("Basic")) {
													                            		 autoPostParameter.put("API_AUTH_TYPE", selectAuthentication);
													                            		 autoPostParameter.put("API_BASIC_USERNAME", basicUsername);
													                            		 autoPostParameter.put("API_BASIC_PASSWORD", basicPassword);
													                            	 }else{
													                            		 autoPostParameter.put("API_AUTH_TYPE", "None");
													                            		 autoPostParameter.put("API_BASIC_USERNAME", "None");
													                            		 autoPostParameter.put("API_BASIC_PASSWORD", "None");   
													                            	 }
													                             }else {
													                            	 autoPostParameter.put("EXE_TYPE", "Automation");   
													                             }  
													                             JSONArray testArray = new JSONArray(autoTestList);
													                             String ouathPass = schema + userName;
													                             autoPostParameter.put("DIRENAME", dirName);
													                             autoPostParameter.put("BUILDNAME", buildName);
													                             autoPostParameter.put("USER_NAME", userName); 	//REMOVE
													                             autoPostParameter.put("SCHEMA", schema); 		//REMOVE
													                             autoPostParameter.put("USER", userId);
													                             autoPostParameter.put("BUILD_ID", buildId);
													                             autoPostParameter.put("EXE_BROWSER", exebrowser);
													                             autoPostParameter.put("OAUTH_PASS", ouathPass);
													                             autoPostParameter.put("USER_ID", session.getAttribute("userEmailId").toString());
													                             autoPostParameter.put("CLIENT_NAME", session.getAttribute("customerName").toString());		//REMOVE
													                             autoPostParameter.put("SCREENSHOT", stepWiseScreenShot);
													                             autoPostParameter.put("RAISED_BUG", reportBugStatus);
													                             autoPostParameter.put("AUTO_EXE_JSON", testArray.toString());
													                             autoPostParameter.put("CLIENT_VM_URL", REMOTE_URL_CLIENT);
													                             autoPostParameter.put("SERVER_VM_URL", configurationProperties.getProperty("XENON_SERVER_WS"));		//REMOVE
													                             autoPostParameter.put("PROJECT_PATH", projectLocation);
													                             buildExecDetails.setExecJson(autoPostParameter.toString());
													                             buildExecDetails.setCompleteStatus(1);
													                             int queueStatus =  buildExecDetailsDAO.updateBuildExecDetails(buildExecDetails, schema);
													                             if(queueStatus==1){
													                            	final String postjson=autoPostParameter.toString();
													                            	Runnable onPremise=new Runnable() {
																						
																						@Override
																						public void run() {
																							 try {
																								customerMachineOperation.postRequest(
																											configurationProperties.getProperty("XENON_SERVER_WS") + "/executeonpremisebuild?postjson=", postjson);
																							} catch (RemoteMachineException e) {
																								System.out
																										.println("Something went wrong = "+e.getMessage());
																							}
																							
																						}
																					};
													                            	 Thread runBuild=new Thread(onPremise);
													                            	 runBuild.start();
													                            	 return Constants.EXECUTION_ADDED_IN_QUEUE;
													                             }else{
													                            	 
													                            	 return Constants.EXECUTION_ADD_UNSUCCESSFUL;
													                             }
													    						 /*customerMachineOperation.postRequest(
													    								configurationProperties.getProperty("XENON_SERVER_WS") + "/executeonpremisebuild?postjson=", autoPostParameter.toString());*/
													                      } else {
													                             logger.info("Something went wrong,Build details not inserted");
													                      }

													                } else {
													                      logger.info("Something went wrong, Build Status Not changed");
													                }

													         } else {
													                logger.info("Something went wrong, Log File Not Created");
													         }
													   } else {
													         logger.info("Something went wrong,Execution Directory Not Created");
													   }
                                                   } catch (JSONException e) {
   													// TODO Auto-generated catch block
   													e.printStackTrace();
   													return Constants.XENON_EXCEPTION;
   												} catch (RemoteMachineException e) {
   													// TODO Auto-generated catch block
   													e.printStackTrace();
   													return Constants.XENON_EXCEPTION;
   												}

                                                      
                                            }
								} else {
									logger.info("Remote machine not running");
									buildExecDetailsDAO.changeVMStatus(freeVMId, 1);
									customerMachineOperation.postRequest(
											configurationProperties.getProperty("XENON_SERVER_WS") + "/deallocatecustomervm?allocatedvmid="+freeVMId+"&schema="+schema+"&buildId="+buildId+"&userId="+userId,"");
									responseStatus = Constants.XENON_CLIENT_INACTIVE;
								}
							} else {
								logger.error("Something went wrong");
								return Constants.XENON_EXCEPTION;
							}

						} else {
							logger.info("FREE VM NOT FOUND");
							return Constants.XENON_VM_NOT_FOUND;
						}
					} else {
						return responseStatus; //server or vm not running
					}
				} else {
					logger.error("User do not having access for create build");
					return Constants.XENON_EXCEPTION;
				}
			} catch (Exception e) {
				logger.error("Something went wrong = " + e.getMessage());
				e.printStackTrace();
				return Constants.XENON_EXCEPTION;

			}
			return responseStatus;
		}
		else{
			return Constants.EXECUTION_IN_PROGRESS;
		}
		
	}

	@RequestMapping(value = "/startsfdconpremiseexecution", method = RequestMethod.POST)
	public @ResponseBody String startsfdconpremiseexecution(@RequestParam("selectBrowser") String browser,
			@RequestParam(value="selectEnv[]",defaultValue="0") final String env,
			@RequestParam("selectMailGroup") int mailGroupId,
			@RequestParam("stepwiseScreenshotStatus") int stepWiseScreenshot,
			@RequestParam("reportBugStatus") int reportBug,
			HttpSession session) throws Exception {

		utilsService.getLoggerUser(session);
		logger.info("In on premise customer execution method");

		customerMachineOperation = CustomerMachineOperation.getInstance();
		String schema = session.getAttribute("cust_schema").toString();
		int userId = Integer.parseInt(session.getAttribute("userId").toString());
		int buildId = Integer.parseInt(session.getAttribute("scriptExecBuildId").toString());
		int customerId = Integer.parseInt(session.getAttribute("customerId").toString());

		logger.info("Getting instance for CustomerMachineOperation class");
		String responseStatus = null;
		try {
			if (buildDAO.checkBMRoleAccess("createbuild",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
				responseStatus = customerMachineOperation.handleRemoteStatus(customerMachineOperation,configurationProperties.getProperty("XENON_SERVER_WS"));
				if (responseStatus.equals(Constants.XENON_STATUS_SUCCESS)) {
					String freeVMList = customerMachineOperation
							.getRequest(configurationProperties.getProperty("XENON_SERVER_WS")+ "/getcustomerfreevm?customerId=", "" + customerId);

					JSONArray freeVMListArray = new JSONArray(freeVMList);
					if (freeVMListArray.length() > 0) {
						JSONObject freeVMObject = freeVMListArray.getJSONObject(0);
						String freeVMId = freeVMObject.get("cust_vm_id").toString();

						JSONObject postParameter = new JSONObject();
						postParameter.put("vmId", freeVMId);
						postParameter.put("userId", userId);
						postParameter.put("buildId", buildId);
						postParameter.put("custId", customerId);
						String allocatedVM = customerMachineOperation.postRequest(
								configurationProperties.getProperty("XENON_SERVER_WS") + "/allocatesfdccustomervm?postjson=", postParameter.toString());
						JSONArray allocatedVMArray = new JSONArray(allocatedVM);
						if (allocatedVMArray.length() > 0) {

							List<Map<String, Object>> getVMDetails = buildExecDetailsDAO.getVMSfdcDetails(schema, userId,
									buildId);
							String REMOTE_URL = getVMDetails.get(0).get("inetaddress").toString() + "/XenonClientWS";
							int allocateVMId = Integer.parseInt(getVMDetails.get(0).get("xe_vm_id").toString());

							responseStatus = customerMachineOperation.getRemoteMachineStatus(REMOTE_URL);
							if (responseStatus.equals("200")) {
								session.setAttribute("newBuildTypeForTab", "scriptless");
								int envStatus=2;
								if(env.equalsIgnoreCase("0")){
									envStatus=2;
								}else{
									envStatus=1;
								}
								BuildExecDetails buildExecDetails = buildExecDetailsDAO.setBuildExecDetailsValues(
										buildId, browser, mailGroupId, stepWiseScreenshot, reportBug, userId,
										allocateVMId,envStatus);
								int flag;
									if(envStatus==1)
										flag = buildExecDetailsDAO.insertBuildExecDetails(buildExecDetails,env, schema);
										else
											flag = buildExecDetailsDAO.insertBuildExecDetails(buildExecDetails,"", schema);
								if (flag == 1) {
									/*
									 * buildDAO.sendMailOnSatrtOfAutoBuildExe(
									 * Integer.parseInt(session.getAttribute(
									 * "automationExecBuildId").toString()),
									 * schema);
									 */
									return Constants.XENON_CLIENT_STARTED;
								} else {
									buildExecDetailsDAO.changeVMStatus(freeVMId, 1);
									return Constants.XENON_EXCEPTION;
								}
							} else {
								logger.info("Remote machine not running");
								buildExecDetailsDAO.changeVMStatus(freeVMId, 1);
								responseStatus = Constants.XENON_CLIENT_INACTIVE;
							}
						} else {
							logger.error("Something went wrong");
							return Constants.XENON_EXCEPTION;
						}

					} else {
						logger.info("FREE VM NOT FOUND");
						return Constants.XENON_VM_NOT_FOUND;
					}
				} else {
					return responseStatus; //server or vm not running
				}
			} else {
				logger.error("User do not having access for create build");
				return Constants.XENON_EXCEPTION;
			}
		} catch (Exception e) {
			logger.error("Something went wrong = " + e.getMessage());
			e.printStackTrace();
			return Constants.XENON_EXCEPTION;

		}
		return responseStatus;
	}

	@Async
	@RequestMapping(value = "/executeonpremisebuild", method = RequestMethod.POST)
	public @ResponseBody String executeOnPremiseBuild(@RequestParam("exebrowser") String exebrowser,
			@RequestParam("selectVMExecCount") String selectVMExecCount,
			HttpSession session) throws Exception {
		utilsService.getLoggerUser(session);
		logger.info("In buildautomation execution method");
		customerMachineOperation = CustomerMachineOperation.getInstance();
		int userId = Integer.parseInt(session.getAttribute("userId").toString());
		int buildId = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());
		String schema = session.getAttribute("cust_schema").toString();
		//String responseStatus = null;

		List<Map<String, Object>> autoBuildList = automationBuildDAO.getAutoBuildDetail(schema, buildId);
		String userName = autoBuildList.get(0).get("user_name").toString();
		String dirName = schema + userName + customerMachineOperation.getTimeStamp();

		if (customerMachineOperation.createDirectory(configurationProperties.getProperty("XENON_REPOSITORY_PATH")+"repository"+File.separator+"upload"+File.separator+"tmpFiles"+File.separator + dirName)) {
			if (customerMachineOperation.createFile(configurationProperties.getProperty("XENON_REPOSITORY_PATH")+"repository"+File.separator+"upload"+File.separator+"logFiles"+File.separator + dirName + ".txt")) {
				int flag = automationBuildDAO.updateBuildLogfile(buildId, dirName, schema);
				if (flag == 1) {
					flag = automationBuildDAO.insertExecution(dirName, buildId, schema);
					if (flag == 1) {
						String buildName = autoBuildList.get(0).get("build_name").toString();
						String projectLocation = autoBuildList.get(0).get("project_location").toString();
						String inetAddress = autoBuildList.get(0).get("inetaddress").toString();
						String REMOTE_URL = inetAddress + "/XenonClientWS";
						String stepWiseScreenShot = utilsService
								.getScreenShotStatusFromId(autoBuildList.get(0).get("step_wise_screenshot").toString());
						String reportBug = utilsService
								.getRerportBugStatusFromId(autoBuildList.get(0).get("report_bug").toString());

						List<Map<String, Object>> autoTestList = automationBuildDAO.getAutomationTestCases(schema,
								buildId);
						JSONArray testArray = new JSONArray(autoTestList);
						String ouathPass = schema + userName;

						JSONObject autoPostParameter = new JSONObject();
						autoPostParameter.put("DIRENAME", dirName);
						autoPostParameter.put("BUILDNAME", buildName);
						autoPostParameter.put("USER_NAME", userName);
						autoPostParameter.put("SCHEMA", schema);
						autoPostParameter.put("USER", userId);
						autoPostParameter.put("BUILD_ID", buildId);
						autoPostParameter.put("EXE_BROWSER", exebrowser);
						autoPostParameter.put("OAUTH_PASS", ouathPass);
						autoPostParameter.put("USER_ID", session.getAttribute("userEmailId").toString());
						autoPostParameter.put("CLIENT_NAME", session.getAttribute("customerName").toString());
						autoPostParameter.put("SCREENSHOT", stepWiseScreenShot);
						autoPostParameter.put("RAISED_BUG", reportBug);
						autoPostParameter.put("AUTO_EXE_JSON", testArray.toString());
						autoPostParameter.put("CLIENT_VM_URL", REMOTE_URL);
						autoPostParameter.put("SERVER_VM_URL", configurationProperties.getProperty("XENON_SERVER_WS"));
						autoPostParameter.put("PROJECT_PATH", projectLocation);

						 customerMachineOperation.postRequest(
								configurationProperties.getProperty("XENON_SERVER_WS") + "/executeonpremisebuild?postjson=", autoPostParameter.toString());
					} else {
						logger.info("Something went wrong,Build details not inserted");
						return Constants.XENON_EXCEPTION;
					}

				} else {
					logger.info("Something went wrong, Build Status Not changed");
					return Constants.XENON_EXCEPTION;
				}

			} else {
				logger.info("Something went wrong, Log File Not Created");
				return Constants.FILE_STATUS_NOT_CREATED;
			}
		} else {
			logger.info("Something went wrong,Execution Directory Not Created");
			return Constants.DIRECTORY_STATUS_NOT_CREATED;
		}
		//return responseStatus;
		return "Ok";
	}
	
	@RequestMapping(value = "/executesfdconpremisebuild", method = RequestMethod.POST)
	public @ResponseBody String executesfdconpremisebuild(@RequestParam("exebrowser") String exebrowser,
			@RequestParam("loginUserName") String loginUserName,
			@RequestParam("loginUserPass") String loginUserPass,
			HttpSession session) throws Exception {
		utilsService.getLoggerUser(session);
		logger.info("In buildautomation execution method");
		customerMachineOperation = CustomerMachineOperation.getInstance();
		int userId = Integer.parseInt(session.getAttribute("userId").toString());
		int buildId = Integer.parseInt(session.getAttribute("scriptExecBuildId").toString());
		String schema = session.getAttribute("cust_schema").toString();
		String responseStatus = null;

		List<Map<String, Object>> autoBuildList = automationBuildDAO.getScriptBuildDetail(schema, buildId);
		String userName = autoBuildList.get(0).get("user_name").toString();
		String dirName = schema + userName + customerMachineOperation.getTimeStamp();

		if (customerMachineOperation.createDirectory(configurationProperties.getProperty("XENON_REPOSITORY_PATH")+"repository"+File.separator+"upload"+File.separator+"tmpFiles"+File.separator + dirName)) {
			if (customerMachineOperation.createFile(configurationProperties.getProperty("XENON_REPOSITORY_PATH")+"repository"+File.separator+"upload"+File.separator+"logFiles"+File.separator + dirName + ".txt")) {
				int flag = automationBuildDAO.updateScriptBuildLogfile(buildId, dirName, schema);
				if (flag == 1) {
					flag = automationBuildDAO.insertScriptBuildExecution(dirName, buildId, schema);
					if (flag == 1) {
						String buildName = autoBuildList.get(0).get("build_name").toString();
						String loginUrl = autoBuildList.get(0).get("loginUrl").toString();
						String projectLocation = autoBuildList.get(0).get("project_location").toString();
						String inetAddress = autoBuildList.get(0).get("inetaddress").toString();
						String REMOTE_URL = inetAddress + "/XenonClientWS";
						String stepWiseScreenShot = utilsService
								.getScreenShotStatusFromId(autoBuildList.get(0).get("step_wise_screenshot").toString());
						String reportBug = utilsService
								.getRerportBugStatusFromId(autoBuildList.get(0).get("report_bug").toString());

						List<Map<String, Object>> autoTestList = automationBuildDAO.getScriptlessTestCases(schema,
								buildId);
						JSONArray testArray = new JSONArray(autoTestList);
						String ouathPass = schema + userName;

						JSONObject autoPostParameter = new JSONObject();
						autoPostParameter.put("DIRENAME", dirName);
						autoPostParameter.put("BUILDNAME", buildName);
						autoPostParameter.put("USER_NAME", userName);
						autoPostParameter.put("SCHEMA", schema);
						autoPostParameter.put("USER", userId);
						autoPostParameter.put("BUILD_ID", buildId);
						autoPostParameter.put("EXE_BROWSER", exebrowser);
						autoPostParameter.put("OAUTH_PASS", ouathPass);
						autoPostParameter.put("USER_ID", session.getAttribute("userEmailId").toString());
						autoPostParameter.put("CLIENT_NAME", session.getAttribute("customerName").toString());
						autoPostParameter.put("SCREENSHOT", stepWiseScreenShot);
						autoPostParameter.put("RAISED_BUG", reportBug);
						autoPostParameter.put("AUTO_EXE_JSON", testArray.toString());
						autoPostParameter.put("CLIENT_VM_URL", REMOTE_URL);
						autoPostParameter.put("SERVER_VM_URL", configurationProperties.getProperty("XENON_SERVER_WS"));
						autoPostParameter.put("PROJECT_PATH", projectLocation);
						autoPostParameter.put("SFDC_LOGIN_URL", loginUrl);
						autoPostParameter.put("SFDC_LOGIN_USERNAME", loginUserName);
						autoPostParameter.put("SFDC_LOGIN_PASSWORD", loginUserPass);
						autoPostParameter.put("SCHEMA_NAME", schema);
						
						responseStatus = customerMachineOperation.postRequest(
								configurationProperties.getProperty("XENON_SERVER_WS") + "/executeonpremisesfdcbuild?postjson=", autoPostParameter.toString());
					} else {
						logger.info("Something went wrong,Build details not inserted");
						return Constants.XENON_EXCEPTION;
					}

				} else {
					logger.info("Something went wrong, Build Status Not changed");
					return Constants.XENON_EXCEPTION;
				}

			} else {
				logger.info("Something went wrong, Log File Not Created");
				return Constants.FILE_STATUS_NOT_CREATED;
			}
		} else {
			logger.info("Something went wrong,Execution Directory Not Created");
			return Constants.DIRECTORY_STATUS_NOT_CREATED;
		}
		return responseStatus;
	}


	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :" + ex);
		return new ModelAndView("redirect:/500");
	}
	
	@Async
	@RequestMapping(value = "/executeRHQTesSet", method = RequestMethod.POST)
	public @ResponseBody String executeRHQTesSet( @RequestParam(name="exebrowser",defaultValue="Chrome") final String exebrowser, 
			@RequestParam(name="selectBrowser",defaultValue="2") String browser, 
			@RequestParam(defaultValue="1") int reportBug,
			@RequestParam(defaultValue="0") int mailGroupId,
			@RequestParam(defaultValue="1") int stepWiseScreenshot,/*1*/
			@RequestParam(value="selectEnv[]",defaultValue="0") final String env,
			@RequestParam("selectVM") String selectVM,
			@RequestParam("selectVMExecCount") String selectVMExecCount,
			@RequestParam(defaultValue="127.0.0.1:1") final String machine, final HttpSession session )
			throws Exception {
		
//		if( Integer.parseInt( session.getAttribute("roleId").toString() ) != 2 )
//			user = session.getAttribute("userEmailId").toString();
		int userId = Integer.parseInt(session.getAttribute("userId").toString());
		final String projectName = session.getAttribute("UserCurrentProjectName").toString();
		final String buildName = session.getAttribute("automationExecBuildName").toString();
		final Integer buildId = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());
		final String schema = session.getAttribute("cust_schema").toString();
		/*String redwoodBuildId = session.getAttribute("redwoodBuildId").toString();*/
		String buildStamp = schema+session.getAttribute("userFirstname").toString()+"_"+session.getAttribute("userLastname").toString()+System.currentTimeMillis();
		session.setAttribute("redwoodBuildStamp", buildStamp);
		
		customerMachineOperation = CustomerMachineOperation.getInstance();
		int customerId = Integer.parseInt(session.getAttribute("customerId").toString());
		List<Map<String, Object>> execVmDetails = buildExecDetailsDAO.getCustomerVMDetailsbyId(customerId, Integer.parseInt(selectVM));
		if(Integer.parseInt(execVmDetails.get(0).get("current_execs").toString())==Integer.parseInt(selectVMExecCount)){
			logger.info("Getting instance for CustomerMachineOperation class");
			String responseStatus = null;
			try {
				if (buildDAO.checkBMRoleAccess("createbuild",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
					try{
						responseStatus = customerMachineOperation.handleRemoteStatus(customerMachineOperation,configurationProperties.getProperty("XENON_SERVER_WS"));
					}catch (Exception e) {
						responseStatus=Constants.XENON_SERVER_INACTIVE;
					}
					if (responseStatus.equals(Constants.XENON_STATUS_SUCCESS)) {
						String freeVMList = customerMachineOperation
								.postRequest(configurationProperties.getProperty("XENON_SERVER_WS")+ "/getcustomerfreevm?customerId="+customerId+"&vmId="+selectVM, "");

//						HttpHeaders headers = new HttpHeaders();
//                        headers.add("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE);
//                        headers.add("Accept", MediaType.APPLICATION_JSON.toString());

//                        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<String, String>();
//                        requestBody.add("customerId", String.valueOf(customerId));
//                        requestBody.add("vmId", selectVM);
//                        HttpEntity<MultiValueMap<String, String>> formEntity = new HttpEntity<MultiValueMap<String,String>>(requestBody, headers);
                        
                        
                        //RestTemplate restTemplate = new RestTemplate();
                        //String freeVMList = restTemplate.exchange(configurationProperties.getProperty("XENON_SERVER_WS")+ "/getcustomerfreevm", HttpMethod.POST, formEntity, String.class).getBody();
                        /*coding adding end */

						JSONArray freeVMListArray = new JSONArray(freeVMList);
						if (freeVMListArray.length() > 0) {
							JSONObject freeVMObject = freeVMListArray.getJSONObject(0);
							String freeVMId = freeVMObject.get("cust_vm_id").toString();

							JSONObject postParameter = new JSONObject();
							postParameter.put("vmId", freeVMId);
							postParameter.put("userId", userId);
							postParameter.put("buildId", buildId);
							postParameter.put("custId", customerId);
							String allocatedVM = customerMachineOperation.postRequest(
									configurationProperties.getProperty("XENON_SERVER_WS") + "/allocatecustomervm?postjson=", postParameter.toString());
							JSONArray allocatedVMArray = new JSONArray(allocatedVM);
							if (allocatedVMArray.length() > 0) {

								List<Map<String, Object>> getVMDetails = buildExecDetailsDAO.getVMDetails(schema, userId,
										buildId);
								int allocateVMId = Integer.parseInt(getVMDetails.get(0).get("xe_vm_id").toString());
								String REMOTE_URL = getVMDetails.get(0).get("inetaddress").toString() + "/XenonClientWS";
								
								/*try {
								responseStatus = customerMachineOperation.getRemoteMachineStatus(REMOTE_URL);
								}catch(Exception e){
									customerMachineOperation.postRequest(
											configurationProperties.getProperty("XENON_SERVER_WS") + "/deallocatecustomervm?allocatedvmid="+freeVMId+"&schema="+schema+"&buildId="+buildId+"&userId="+userId,"");
								}*/
//								if (responseStatus.equals("200")) {
									session.setAttribute("newBuildTypeForTab", "automation");
									int envStatus=2;
									if(env.equalsIgnoreCase("0")){
										envStatus=2;
									}else{
										envStatus=1;
									}
									BuildExecDetails buildExecDetails = buildExecDetailsDAO.setBuildExecDetailsValues(
											buildId, browser, mailGroupId, stepWiseScreenshot, reportBug, userId,
											allocateVMId,envStatus);
									int flag;
										if(envStatus==1)
											flag = buildExecDetailsDAO.insertBuildExecDetails(buildExecDetails,env, schema);
											else
												flag = buildExecDetailsDAO.insertBuildExecDetails(buildExecDetails,"", schema);
									if (flag == 1) {
										
                                               logger.info("In buildautomation execution method");
                                               customerMachineOperation = CustomerMachineOperation.getInstance();
                                              // int userId = Integer.parseInt(session.getAttribute("userId").toString());
                                             //  int buildId = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());
                                              // String schema = session.getAttribute("cust_schema").toString();
                                               List<Map<String, Object>> autoBuildList = automationBuildDAO.getAutoBuildDetail(schema, buildId);
                                               String userName = autoBuildList.get(0).get("user_name").toString();
                                       		   String dirName = schema + userName + customerMachineOperation.getTimeStamp();
                                               try {
									                if (flag == 1) {
									                      flag = automationBuildDAO.insertExecution(dirName, buildId, schema);
									                      if (flag == 1) {
									                    	  	 /*List<Map<String, Object>> autoTestList=new ArrayList<>();
									                    	  	 autoTestList = automationBuildDAO.getAutomationTestCases(schema, buildId);*/
		
									                    	  	List<String> testPrefixes = automationBuildDAO.getTestPrefixes(buildId, schema);
									                    		for (String testPrefix : testPrefixes)
									                    			buildDAO.insertcstarttime( buildId, testPrefix, schema);
									                    	  	 
									                             buildExecDetails.setCompleteStatus(1);
									                             int queueStatus =  buildExecDetailsDAO.updateBuildExecDetails(buildExecDetails, schema);
									                             if(queueStatus==1){
									                            	/*Runnable onPremise=new Runnable() {
																		private BuildDAO buildDao = buildDAO;
																		@Override
																		public void run() {*/
																			 try {
																				final String user = ( Integer.parseInt( session.getAttribute("roleId").toString() ) == 2 ) ?  "admin" : session.getAttribute("userEmailId").toString();
																				RedwoodManagement redwoodManagement = new RedwoodManagement();
																				buildDAO.updateAutomationBuildStatus(4, buildId, schema);
																				redwoodManagement.runExecutionByCli2(session.getAttribute("redwood_url").toString(), projectName,
																							user, buildName, exebrowser, "exec-"+buildName+"-"+new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format( new Date() ), machine );
																			} catch (Exception e) {
																				System.out
																						.println("Something went wrong = "+e.getMessage());
																				buildExecDetailsDAO.changeVMStatus(freeVMId, 1);
																				customerMachineOperation.postRequest(
																						configurationProperties.getProperty("XENON_SERVER_WS") + "/deallocatecustomervm?allocatedvmid="+freeVMId+"&schema="+schema+"&buildId="+buildId+"&userId="+userId,"");
																				responseStatus = Constants.XENON_CLIENT_INACTIVE;

																			}
																		/*}
																	};*/
									                            	 /*Thread runBuild=new Thread(onPremise);
									                            	 runBuild.start();
									                            	 runBuild.join();*/
																	 buildExecDetailsDAO.changeVMStatus(freeVMId, 1);
																	 customerMachineOperation.postRequest(
																				configurationProperties.getProperty("XENON_SERVER_WS") + "/deallocatecustomervm?allocatedvmid="+freeVMId+"&schema="+schema+"&buildId="+buildId+"&userId="+userId,"");

									                            	 return Constants.EXECUTION_ADDED_IN_QUEUE;
									                             }else{
									                            	 
									                            	 return Constants.EXECUTION_ADD_UNSUCCESSFUL;
									                             }
									    						 /*customerMachineOperation.postRequest(
									    								configurationProperties.getProperty("XENON_SERVER_WS") + "/executeonpremisebuild?postjson=", autoPostParameter.toString());*/
									                      } else {
									                             logger.info("Something went wrong,Build details not inserted");
									                      }

									                } else {
									                      logger.info("Something went wrong, Build Status Not changed");
									                }
                                               } catch (JSONException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												return Constants.XENON_EXCEPTION;
											} catch (Exception e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												return Constants.XENON_EXCEPTION;
											}
                                      }
//								} else {
//									logger.info("Remote machine not running");
//									buildExecDetailsDAO.changeVMStatus(freeVMId, 1);
//									customerMachineOperation.postRequest(
//											configurationProperties.getProperty("XENON_SERVER_WS") + "/deallocatecustomervm?allocatedvmid="+freeVMId+"&schema="+schema+"&buildId="+buildId+"&userId="+userId,"");
//									responseStatus = Constants.XENON_CLIENT_INACTIVE;
//								} 
							} else {
								logger.error("Something went wrong");
								return Constants.XENON_EXCEPTION;
							}

						} else {
							logger.info("FREE VM NOT FOUND");
							return Constants.XENON_VM_NOT_FOUND;
						}
					} else {
						return responseStatus; //server or vm not running
					}
				} else {
					logger.error("User do not having access for create build");
					return Constants.XENON_EXCEPTION;
				}
			} catch (Exception e) {
				logger.error("Something went wrong = " + e.getMessage());
				e.printStackTrace();
				return Constants.XENON_EXCEPTION;

			}
			return responseStatus;
		}
		else{
			return Constants.EXECUTION_IN_PROGRESS;
		}
		
//			redwoodBuildIdResponse = redwoodManagement.saveOrUpdateTestSet(session.getAttribute("redwood_url").toString(), redwoodBuildId, 
//			session.getAttribute("automationExecBuildName").toString(), session.getAttribute("UserCurrentProjectName").OtoString(), redwoodTestCases );
//		return "";
	}
	
    public String insertcstarttime(String testPrefix, Integer buildId, String buildStamp, String schemaName)
            throws Exception {
//    	List<Map<String, Object>> buildDetail = buildDAO.getBuildDetailsByTimestampAndId(Integer.parseInt(buildId), buildStamp);
        int status = buildDAO.insertcstarttime( buildId, testPrefix, schemaName);
        if(status==1){
            return "Success";
        }else{
            return "Failure";
        }
    }
	
	@RequestMapping(value = "/getDocumentByTcPrefix", method = RequestMethod.POST)
	public @ResponseBody String getDocumentByTcDsId(@RequestParam("tcId") int testcaseId, HttpSession session)
			throws Exception {
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		logger.info("Reading document by document id");
		List<Map<String, Object>> data = testCaseService.getDatasheetByTcId(testcaseId, schema);
		if (data.size() > 0) {
			data.get(0).put("status", true);

			String filename = data.get(0).get("datasheet_filename").toString();
			Blob blob = new javax.sql.rowset.serial.SerialBlob((byte[]) data.get(0).get("datasheet_file"));// (
																											// (byte[])
																											// data.get(0).get("datasheet_file"));
			InputStream is = blob.getBinaryStream();
			FileOutputStream fos = new FileOutputStream("C:\\repository\\upload\\my" + "\\" + filename);

			int b = 0;
			while ((b = is.read()) != -1) {
				fos.write(b);
			}
			
			data.get(0).put("datasheetFile", utilsService.getFileData((byte[]) data.get(0).get("datasheet_file")));
			ArrayList<String> response = utilsService.convertListOfMapToJson(data);
			return response.toString();
		} else {
			data = new ArrayList<Map<String, Object>>();
			Map<String, Object> status = new HashMap<String, Object>();
			status.put("status", false);
			data.add(status);
			return utilsService.convertListOfMapToJson(data).toString();
		}

	}

}


package com.xenon.controller;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.buildmanager.UserService;
import com.xenon.api.common.JiraIntegrationService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.api.core.CustomerService;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.buildmanager.dao.UserProfileDAO;
import com.xenon.common.dao.OauthUserDAO;
import com.xenon.common.domain.OauthClient;
import com.xenon.common.domain.OauthUser;
import com.xenon.core.dao.CustomerDAO;
import com.xenon.core.dao.CustomerTypeDAO;
import com.xenon.util.service.OauthTokenGenerator;

/**
 * This controller includes requests to create,update operations of a customer
 * 
 * @author bhagyashri.ajmera
 *
 */
@SuppressWarnings({ "unused" })
@Controller
public class CustomerController {
	@Resource(name = "configurationProperties")
	private Properties configurationProperties;
	
	@Autowired
	CustomerTypeDAO custTypeDao;

	@Autowired
	CustomerDAO custDetailsDAO;

	@Autowired
	UserDAO userDao;
	
	@Autowired
	OauthUserDAO oauthUserDAO; 

	@Autowired
	UserProfileDAO userProfileService;
	
	@Resource(name = "dbProperties")
	private Properties dbProperties;
	
	@Autowired
	UserService userService;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	JiraIntegrationService jiraIntegrationService;
	
	private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

	/**
	 * Description : This request gives the create customer view of create
	 * customer page
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/createcustomer")
	public ModelAndView newcustomer(Model model, HttpSession session) throws Exception {
		logger.info("Navigating to create customer page");
		if (session.getAttribute("userId") == null) {
			logger.info("User not logged in or session expired");
			logger.info("Navigating to login page");
			return new ModelAndView("redirect:/logout");
		}
		if (Integer.parseInt(session.getAttribute("roleId").toString()) != 1)
			return new ModelAndView("redirect:/500");
		else {
			utilsService.getLoggerUser(session);
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			logger.info("Reading required customer details");
			model.addAttribute("custTypes", custTypeDao.getAllCustomerTypes());
			logger.info("Navigating to create customer page");
			return new ModelAndView("createcustomer", "Model", model);
		}
	}

	/**
	 * Description : This request provides the view for view customer list
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/viewcustomer")
	public ModelAndView viewcustomer(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.info("User not logged in or session expired");
			logger.info("Navigating to login page");
			return new ModelAndView("redirect:/logout");
		}

		if (Integer.parseInt(session.getAttribute("roleId").toString()) != 1)
			return new ModelAndView("redirect:/500");
		else {
			utilsService.getLoggerUser(session);
			logger.info("Navigating to view customer page");

			List<Map<String, Object>> listCust = custDetailsDAO.getAllCustomers();
			for (int i = 0; i < listCust.size(); i++) {
				String logo = loginService.getImage((byte[]) listCust.get(i).get("customer_logo"));
				listCust.get(i).put("logo", logo);

				String customer_schema = listCust.get(i).get("customer_schema").toString();

				List<Map<String, Object>> cloudSpace = custDetailsDAO.getCloudeSpace(customer_schema);
				List<Map<String, Object>> userCount = custDetailsDAO.getUserCount(customer_schema);
				List<Map<String, Object>> projectCount = custDetailsDAO.getProjectCount(customer_schema);

				listCust.get(i).put("cloudeSpace", cloudSpace.get(0).get("Used_Space"));
				listCust.get(i).put("userCount", userCount.get(0).get("user_count"));
				listCust.get(i).put("projectCount", projectCount.get(0).get("project_count"));

			}
			logger.info("Customer list read successfully");
			model.addAttribute("CustomerList", listCust);

			List<Map<String, Object>> listAdmins = custDetailsDAO.getAllCustomerAdmins(listCust);
			for (int i = 0; i < listAdmins.size(); i++) {
				String logo = loginService.getImage((byte[]) listAdmins.get(i).get("user_photo"));
				listAdmins.get(i).put("logo", logo);
			}
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			logger.info("Customer admin list read successfully");
			model.addAttribute("CustomerAdminList", listAdmins);
			logger.info("Redirecting to view customer page");
			return new ModelAndView("viewcustomer", "Model", model);
		}
	}

	@RequestMapping(value = "/getjiraresultviaajax", method = RequestMethod.POST)
	  public @ResponseBody String getJiraResultViaAjax(@RequestParam(required=true) String url,@RequestParam(required=true) String username,@RequestParam(required=true) String password,
			  Model model  ) {
		
		boolean jiraKickStart = jiraIntegrationService.testJiraConnection(url, username, password);
	
	if(jiraKickStart){
		return "Successfully connected with Jira";
	}else{
		return "Not able to connect with Jira";
	}
				
		
	}
	

	
	/**
	 * Description : This request gives the view of particular/Selected customer
	 * details
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws Exception
	 */

	@RequestMapping(value = "/viewcustomerdetails")
	public ModelAndView viewcustomerdetails(Model model, HttpSession session) throws UnsupportedEncodingException {
		if (session.getAttribute("userId") == null) {
			logger.info("User not logged in or session expired");
			logger.info("Navigating to login page");
			return new ModelAndView("redirect:/logout");
		}
		if (Integer.parseInt(session.getAttribute("roleId").toString()) != 1)
			return new ModelAndView("redirect:/500");
		else {
			utilsService.getLoggerUser(session);
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			logger.info("Reading customer id to show details");
			int custId = Integer.parseInt(session.getAttribute("selectedCustId").toString());
			logger.info("Found customer id to show details");
			model.addAttribute("custTypes", custTypeDao.getAllCustomerTypes());
			List<Map<String, Object>> custDetails = custDetailsDAO.getCustomerDetailsById(custId);
			logger.info("Reading details of customer");
			model.addAttribute("CustDetails", custDetails);

			List<Map<String, Object>> custAdminDetails = userDao
					.getAdminUserDetails(custDetails.get(0).get("customer_schema").toString());
			logger.info("Reading details of customer admins");
			model.addAttribute("custAdminDetails", custAdminDetails);
			logger.info("Navigating to view customer details page");
			return new ModelAndView("viewcustomerdetails", "Model", model);
		}
	}

	/**
	 * 
	 * Description : This request inserts a new customer with respective
	 * customer details
	 * 
	 * @param custName
	 * @param custAddress
	 * @param custStatus
	 * @param bugTrackerStatus
	 * @param testManagerStatus
	 * @param custType
	 * @param firstName
	 * @param lastName
	 * @param emailId
	 * @param session
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/insertCustomer", method = RequestMethod.POST)
	public ModelAndView insertCustomer(@RequestParam("custName") String custName,
			@RequestParam("custAddress") String custAddress,
			@RequestParam(value = "custStatus", defaultValue = "2") int custStatus,
			@RequestParam(value = "bugTrackerStatus", defaultValue = "2") int bugTrackerStatus,
			@RequestParam(value = "testManagerStatus", defaultValue = "2") int testManagerStatus,
			@RequestParam(value = "automationStatus", defaultValue = "2") int automationStatus,
			@RequestParam(value = "onPremiseStatus", defaultValue = "2") int onPremiseStatus,
			@RequestParam(value = "scriptlessStatus", defaultValue = "2") int scriptlessStatus,
			
			@RequestParam(value = "pluginStatus", defaultValue = "2") int pluginStatus,
			@RequestParam(value = "pluginJenkinStatus", defaultValue = "2") int pluginJenkinStatus,
			@RequestParam(value = "pluginGitStatus", defaultValue = "2") int pluginGitStatus,
			@RequestParam(value = "redwoodStatus", defaultValue = "0") int redwoodStatus,
			@RequestParam(value = "apiStatus", defaultValue = "0") int apiStatus,
			@RequestParam(value = "custType", defaultValue = "1") int custType,
			@RequestParam(value = "redwoodUrl", defaultValue = "") String redwoodUrl,
			@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName,
			@RequestParam("usernameLogin") String usernameLogin, @RequestParam("emailId") String emailId, HttpSession session) throws Exception {

		if (session.getAttribute("userId") == null) {
			logger.info("User not logged in or session expired");
			logger.info("Navigating to login page");
			return new ModelAndView("redirect:/logout");
		}
		if (Integer.parseInt(session.getAttribute("roleId").toString()) != 1)
			return new ModelAndView("redirect:/500");
		else {
			utilsService.getLoggerUser(session);
			logger.info("Creating customer schema");
			String[] textArray = { custName, custAddress };
			JSONObject resultJson = utilsService.convertToPlainTextForCustomer(textArray);

			custName = resultJson.get(custName).toString();
			custAddress = resultJson.get(custAddress).toString();

			String newSchema = customerService.createCustomerSchema(custName,configurationProperties.getProperty("XENON_REPOSITORY_PATH"),dbProperties.getProperty("jdbc.driver"),dbProperties.getProperty("jdbc.url"),dbProperties.getProperty("jdbc.username"),dbProperties.getProperty("jdbc.password"));
			logger.info("Customer schema created successfully");

			int flag = customerService.insertCustomer(custName, custAddress, custType, bugTrackerStatus,
					testManagerStatus, automationStatus,onPremiseStatus,scriptlessStatus,custStatus, newSchema,configurationProperties.getProperty("XENON_REPOSITORY_PATH"),pluginJenkinStatus,pluginGitStatus,redwoodStatus,apiStatus,redwoodUrl);

			if (flag != 0) {
				logger.info("Customer inserted successfully");
				logger.info("Inserting customer admin details");
				session.setAttribute("selectedCustId", flag);
				
				OauthClient oauthClient=new OauthClient();
				oauthClient.setClientId(custName);
				oauthUserDAO.insertOauthClient(oauthClient);
				
				userService.insertAdminUser(emailId, firstName, lastName, "2", "" + testManagerStatus + "",
						"" + bugTrackerStatus + "", "" + custStatus + "", newSchema,configurationProperties.getProperty("XENON_REPOSITORY_PATH"),usernameLogin,pluginJenkinStatus,pluginGitStatus);
				
				String passToken= newSchema+firstName+"_"+lastName;
				OauthUser oauthUser=new OauthUser();
				oauthUser.setUserName(emailId);
				oauthUser.setPassword(OauthTokenGenerator.getPasstoken(passToken));
				oauthUserDAO.insertOauthUser(oauthUser);
				
				
				logger.info("Navigating to view customer details page");
				return new ModelAndView("redirect:/viewcustomerdetails");
			}
			logger.info("Something went wrong");
			return new ModelAndView("redirect:/500");
		}
	}

	/**
	 * 
	 * Description : This request edits customer details with respective
	 * customer details
	 * 
	 * @param custSchema
	 * @param custName
	 * @param custAddress
	 * @param custStatus
	 * @param bugTrackerStatus
	 * @param testManagerStatus
	 * @param custType
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/editcustomer")
	public String editcustomer(@RequestParam("custSchema") String custSchema, @RequestParam("custName") String custName,
			@RequestParam("custAddress") String custAddress,
			@RequestParam(value = "custStatusVerify", defaultValue = "2") int custStatus,
			@RequestParam(value = "bugTrackerStatusVerify", defaultValue = "2") int bugTrackerStatus,
			@RequestParam(value = "testManagerStatusVerify", defaultValue = "2") int testManagerStatus,
			@RequestParam(value = "automationStatusVerify", defaultValue = "2") int automationStatus,
			@RequestParam(value = "scriptlessStatusVerify", defaultValue = "2") int scriptlessStatus,
			@RequestParam(value = "pluginJenkinStatus", defaultValue = "2") int pluginJenkinStatus,
			@RequestParam(value = "pluginGitStatus", defaultValue = "2") int pluginGitStatus,
			@RequestParam(value = "redwoodStatus", defaultValue = "0") int redwoodStatus,
			@RequestParam(value = "apiStatus", defaultValue = "0") int apiStatus,
			@RequestParam(value = "redwoodUrl", defaultValue = "") String redwoodUrl,
			@RequestParam(value = "custTypeVerify", defaultValue = "1") int custType, Model model, HttpSession session)
					throws Exception {

		if (session.getAttribute("userId") == null) {
			logger.info("User not logged in or session expired");
			logger.info("Navigating to login page");
			return "redirect:/logout";
		}
		if (Integer.parseInt(session.getAttribute("roleId").toString()) != 1)
			return "redirect:/500";
		else {
			utilsService.getLoggerUser(session);
			int custId = Integer.parseInt(session.getAttribute("selectedCustId").toString());
			int flag = customerService.editcustomer(custName, custAddress, custStatus, bugTrackerStatus,
					testManagerStatus,scriptlessStatus,automationStatus, custType, custId, pluginJenkinStatus, pluginGitStatus,redwoodStatus,apiStatus,redwoodUrl);
			if (flag == 1) {
				logger.info(
						"Edited Cust Details is :\n\n Customer Name = {} \n Customer Address : {} \n Customer Status : {} \n Bug tracker status : {} \n Test manager status : {} \n Automation status : {}  \n Customer Type : {}",
						custName, custAddress, custStatus, bugTrackerStatus, testManagerStatus,automationStatus, custType);
				if (logger.isDebugEnabled())
					logger.debug(
							"Edited Cust Details is :\n\n Customer Name = {} \n Customer Address : {} \n Customer Status : {} \n Bug tracker status : {} \n Test manager status : {} \n Automation status : {} \n Customer Type : {}",
							custName, custAddress, custStatus, bugTrackerStatus, testManagerStatus,automationStatus, custType);
				int check = userDao.updateAllUsersStatus(testManagerStatus, bugTrackerStatus, custSchema);
				return "redirect:/viewcustomerdetails";
			} else {
				return "redirect:/500";
			}
		}

	}

	/**
	 * Description : This request sets the customer id in to the session
	 * 
	 * @param custId
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setCustomerId", method = RequestMethod.POST)
	public ModelAndView setCustomerId(@RequestParam("custId") int custId, Model model, HttpSession session)
			throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			logger.info("setting customer id into the session");
			session.setAttribute("selectedCustId", custId);
			return new ModelAndView("redirect:/");
		}
	}

	/**
	 * Description : This request edits customer admin details with respective
	 * customer details
	 * 
	 * @param firstName
	 * @param lastName
	 * @param file
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/editcustomeradmin")
	public ModelAndView editcustomeradmin(@RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName,
			@RequestParam(value = "uploadCustLogo", required = false) MultipartFile file, Model model,
			HttpSession session) throws Exception {

		if (session.getAttribute("userId") == null) {
			logger.info("User not logged in or session expired");
			logger.info("Navigating to login page");
			return new ModelAndView("redirect:/logout");
		}
		if (Integer.parseInt(session.getAttribute("roleId").toString()) != 2)
			return new ModelAndView("redirect:/500");
		else {
			utilsService.getLoggerUser(session);
			logger.info("Editing customer details");
			
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int customerId=Integer.parseInt(session.getAttribute("customerId").toString());
			String schema=session.getAttribute("cust_schema").toString();
			Map<String,Object> returnModel=customerService.editcustomeradmin(schema,userId,customerId,firstName,lastName,file,configurationProperties,custDetailsDAO,userProfileService);
			Boolean flag=(Boolean) returnModel.get("flag");
			if(flag){
				Iterator it = returnModel.entrySet().iterator();
			    while (it.hasNext()) {
			        Map.Entry pair = (Map.Entry)it.next();
			        String key=(String) pair.getKey();
			        Object value=pair.getValue();
			        if(key.equals("flag")||key.equals("customerLogo")){}
			        else{
					model.addAttribute(key,value);
			        it.remove();
			        }
			        session.setAttribute("customerLogo",returnModel.get("customerLogo"));
			    }
				logger.info("Redirecting to account settings");
				return new ModelAndView("redirect:/accountsettings", "Model", model);
			} else {
				logger.info("Something went wrong");
				return new ModelAndView("redirect:/500");
			}
		}

	}

	/**
	 * Description : This request edits customer admin details with respective
	 * customer details
	 * 
	 * @param firstName
	 * @param lastName
	 * @param file
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/editThirdPartyIssueTracker")
	public ModelAndView editthirdpartyissuetracker(@RequestParam("custTrackerType") int issueTrackerType,
			@RequestParam("custJiraHostURL") String issueTrackerHost,@RequestParam("issueTracker") int issueTracker,
			@RequestParam("custJiraIntegPass")String integPass,
			@RequestParam("custJiraIntegUser") String integUser,
			@RequestParam(value="jiraId", defaultValue = "0") int jiraId,
			Model model,
			HttpSession session) throws Exception {

		if (session.getAttribute("userId") == null) {
			logger.info("User not logged in or session expired");
			logger.info("Navigating to login page");
			return new ModelAndView("redirect:/logout");
		}
		if (Integer.parseInt(session.getAttribute("roleId").toString()) != 2)
			return new ModelAndView("redirect:/500");
		else {
			utilsService.getLoggerUser(session);
			logger.info("Editing Third party issue tracker details");
			
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int customerId=Integer.parseInt(session.getAttribute("customerId").toString());
			String schema=session.getAttribute("cust_schema").toString();
			Map<String,Object> returnModel=customerService.editIssueTracker(schema,userId,customerId,issueTracker,issueTrackerType,issueTrackerHost,integUser,integPass,jiraId,configurationProperties,custDetailsDAO,userProfileService,session);
			int flag=Integer.parseInt(returnModel.get("flag").toString());
			if(flag==1){
				Iterator it = returnModel.entrySet().iterator();
			    while (it.hasNext()) {
			        Map.Entry pair = (Map.Entry)it.next();
			        String key=(String) pair.getKey();
			        Object value=pair.getValue();
			        if(key.equals("flag")){}
			        else{
					model.addAttribute(key,value);
			        it.remove();
			        }
			        
			    }
				logger.info("Redirecting to third party issue tracker settings");
				return new ModelAndView("redirect:/thirdpartyissuetracker", "Model", model);
			} else {
				logger.info("Something went wrong");
				return new ModelAndView("redirect:/500");
			}
		}

	}
	/**
	 * Description : This request handles the exceptions in the class
	 * 
	 * @param ex
	 * @param response
	 * @param session
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		return new ModelAndView("redirect:/500");
	}
}

package com.xenon.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.administration.ProjectService;
import com.xenon.api.buildmanager.ModuleService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.buildmanager.domain.ModuleDetails;

/**
 * 
 * @author prafulla.pol
 * @Description Module class contain methods to create,edit,update module. Also
 *              it provides methods to assign or unassigns module to user.
 */
@SuppressWarnings("unchecked")
@Controller
public class ModuleController {

	@Autowired
	ModuleDAO moduleDAO;

	@Autowired
	LoginService loginService;

	@Autowired
	UtilsService utilsService;

	@Autowired
	ModuleService moduleService;

	@Autowired
	RoleAccessService roleAccessService;

	@Autowired
	BuildDAO buildDAO;

	@Autowired
	ProjectService projectService;

	private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);

	/**
	 * @author prafulla.pol
	 * @method createModule a GET Request
	 * @param model
	 * @param session
	 * @return Model (model) & View (onSuccess - createmodule , onFailure -
	 *         logout/trialerror/404)
	 * @throws Exception
	 */
	@RequestMapping(value = "/createmodule", method = RequestMethod.GET)
	public ModelAndView createModule(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to Create New Module page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			logger.info(" Session is active, userId is " + userId + " Customer Schema " + schema);
			logger.info("Cheking for User's access to create module");
			if (buildDAO.checkBMRoleAccess("createmodule", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to create module");
				session.setAttribute("menuLiText", "Modules");

				if (session.getAttribute("duplicateModuleError") != null) {
					model.addAttribute("error", session.getAttribute("duplicateModuleError").toString());
					session.removeAttribute("duplicateModuleError");
				}

				int flag = moduleDAO.createNewModuleStatus(schema,
						Integer.parseInt(session.getAttribute("cust_type_id").toString()));

				if (flag == 1) {

					if (session.getAttribute("moduleSessionApplicationID") != null) {
						model.addAttribute("selectedProject",
								session.getAttribute("moduleSessionApplicationID").toString());
					}

					model.addAttribute("projects", moduleDAO.getActiveProjects(userId, schema));
					logger.info(" User is redirected to Create New Module page ");
					return new ModelAndView("createmodule", "Model", model);
				} else {
					logger.info(" User is Exceeded the Trial limit to create module . Redirected to Trial Error page ");
					return new ModelAndView("redirect:/trialerror");
				}
			} else {
				logger.warn("User has no access to create module . Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}

		}
	}

	/**
	 * @author prafulla.pol
	 * @method updateModule a POST Request
	 * @param moduleId
	 * @param moduleName
	 * @param moduleDescription
	 * @param moduleStatus
	 * @param model
	 * @param session
	 * @return Model () & View (onSuccess - viewmodules , onFailure -
	 *         logout/500/404)
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatemodule", method = RequestMethod.POST)
	public ModelAndView updateModule(@RequestParam("moduleId") String moduleId,
			@RequestParam("moduleName") String moduleName, @RequestParam("moduleDescription") String moduleDescription,
			@RequestParam("moduleStatus") String moduleStatus, @RequestParam(required = false) String source,
			Model model, HttpSession session) throws Exception {
		logger.info("Post method to update module .Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to Edit module");
			int flag = moduleService.updateModule((List<String>) session.getAttribute("bm_access"), moduleId,
					moduleName, moduleDescription, moduleStatus, schema, session.getAttribute("roleId").toString());
			if (flag == 200) {
				if (source != null && "LeftNavMenu".equalsIgnoreCase(source)) {
					logger.info(" User is redirected to TestCase");
					return new ModelAndView("redirect:/testcase");
				}
				session.setAttribute("moduleCreateStatus", 2);
				session.setAttribute("newModuleName", moduleName.trim());
				logger.info(" User is redirected to view Modules");
				return new ModelAndView("redirect:/viewmodules");
			} else {
				logger.warn("User has no access to Edit module. Redirected to 404 page");
				return new ModelAndView("redirect:/" + flag);
			}
		}

	}

	@RequestMapping(value = "/updateModuleStatus", method = RequestMethod.POST)
	public ModelAndView updateModuleStatus(@RequestParam("moduleId") Integer moduleId,
			@RequestParam("moduleStatus") Integer moduleStatus, @RequestParam(required = false) String source,
			Model model, HttpSession session) throws Exception {
		logger.info("Post method to update module .Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to Edit module");
			int flag = moduleService.updateModuleStatus((List<String>) session.getAttribute("bm_access"), moduleId,
					moduleStatus, schema, session.getAttribute("roleId").toString());
			if (flag == 200) {
				/*
				 * if( source != null && "LeftNavMenu".equalsIgnoreCase(source)
				 * ) { logger.info(" User is redirected to TestCase"); return
				 * new ModelAndView("redirect:/testcase"); }
				 */
				logger.info(" User is redirected to test manager home page");
				if (moduleStatus == 1) {
					// for restore module by anmol
					return new ModelAndView("redirect:/viewtmtrash");
				} else {
					// for delete module
					session.setAttribute("selectedTestModuleId", "0");
					return new ModelAndView("redirect:/testcase");
				}
			} else {
				logger.warn("User has no access to Edit module. Redirected to 404 page");
				return new ModelAndView("redirect:/" + flag);
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method viewModules a GET Request
	 * @param model
	 * @param session
	 * @return Model (createModuleAccessStatus,module) & View (onSuccess -
	 *         viewmodules , onFailure - logout/404)
	 * @throws Exception
	 */
	@RequestMapping(value = "/viewmodules")
	public ModelAndView viewModules(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to view Modules page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to view Modules");
			if (buildDAO.checkBMRoleAccess("viewmodule", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to View Modules");
				session.setAttribute("menuLiText", "Modules");

				boolean accessStatus = buildDAO.checkBMRoleAccess("createmodule",
						session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString());
				if (accessStatus) {
					model.addAttribute("createModuleAccessStatus", 1);
				}
				List<Map<String, Object>> projects = moduleDAO.getActiveProjects(userID, schema);
				List<Map<String, Object>> module = moduleDAO.getAllModules(userID, schema);
				model.addAttribute("module", module);
				model.addAttribute("project", projects);
				model.addAttribute("roleId", session.getAttribute("roleId").toString());

				logger.info(" User is redirected to view Modules ");
				return new ModelAndView("viewmodules", "model", model);
			} else {
				logger.warn("User has no access to View Modules. Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}
		}
	}

	/**
	 * @author navnath.damale
	 * @method modulesetting a GET Request
	 * @param model
	 * @param session
	 * @return Model (modulesetting,module) & View (onSuccess - viewmodules ,
	 *         onFailure - logout/404)
	 * @throws Exception
	 */
	@RequestMapping(value = "/modulesetting")
	public ModelAndView modulesetting(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to view Modules page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			logger.info("Cheking for User's access to Edit module");

			if (buildDAO.checkBMRoleAccess("editmodule", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())
					&& buildDAO.checkBMRoleAccess("assignmodule", session.getAttribute("roleId").toString(),
							session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to Edit module");
				logger.info("User has access to assign applications");

				int moduleId = Integer.parseInt(session.getAttribute("moduleIDForAssign").toString());
				int projectID = Integer.parseInt(session.getAttribute("moduleProjectIDForAssign").toString());

				session.setAttribute("menuLiText", "Modules");

				List<Map<String, Object>> moduleDetails = moduleDAO.getModuleDetails(moduleId, schema);
				model.addAttribute("moduleDetails", moduleDetails);

				Map<String, Object> moduleData = moduleDAO.getAssignModuleData(projectID, moduleId, schema);

				List<Map<String, Object>> moduleAssignedUserDetails = (List<Map<String, Object>>) moduleData
						.get("#result-set-1");
				for (int i = 0; i < moduleAssignedUserDetails.size(); i++) {
					String photo = loginService.getImage((byte[]) moduleAssignedUserDetails.get(i).get("user_photo"));
					moduleAssignedUserDetails.get(i).put("photo", photo);
				}

				List<Map<String, Object>> moduleUnAssignedUserDetails = (List<Map<String, Object>>) moduleData
						.get("#result-set-2");
				for (int i = 0; i < moduleUnAssignedUserDetails.size(); i++) {
					String photo = loginService.getImage((byte[]) moduleUnAssignedUserDetails.get(i).get("user_photo"));
					moduleUnAssignedUserDetails.get(i).put("photo", photo);
				}
				model.addAttribute("moduleIdForAssign", moduleId);
				model.addAttribute("moduleAssignedUserDetails", moduleAssignedUserDetails);
				model.addAttribute("moduleUNAssignedUserDetails", moduleUnAssignedUserDetails);

				String moduleTab = session.getAttribute("settingTab").toString();
				model.addAttribute("settingTab", moduleTab);

				logger.info(" User is redirected to Edit module page ");
				return new ModelAndView("modulesetting", "model", model);
			} else {
				logger.warn("User has no access to Edit/Assign module . Redirected to 404 page");
				return new ModelAndView("redirect:/404");
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method insertModule a POST Request
	 * @param moduleName
	 * @param moduleDescription
	 * @param projectId
	 * @param moduleStatus
	 * @param session
	 * @param model
	 * @return Model () & View (onSuccess - viewmodules , onFailure -
	 *         logout/createmodule/404)
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertmodule", method = RequestMethod.POST)
	public ModelAndView insertModule(@RequestParam("moduleName") String moduleName,
			@RequestParam("moduleDescription") String moduleDescription, @RequestParam("projectId") int projectId,
			@RequestParam("moduleStatus") String moduleStatus, @RequestParam(required = false) String source,
			HttpSession session, Model model) throws Exception {
		logger.info("Post method to insert module. Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			try {
				utilsService.getLoggerUser(session);
				int userID = Integer.parseInt(session.getAttribute("userId").toString());
				String schema = session.getAttribute("cust_schema").toString();
				logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
				logger.info("Cheking for User's access to create module");
				int flag = moduleService.insertModule((List<String>) session.getAttribute("bm_access"), moduleName,
						moduleDescription, projectId, moduleStatus, schema, session.getAttribute("roleId").toString());

				if (source != null && "LeftNavMenu".equalsIgnoreCase(source)) {
					try {
						List<Map<String, Object>> maps = projectService.getProjectAssignedUsers(projectId, schema);
						String defaultUsers = "";
						List<String> userIds = new ArrayList<String>();
						for (Map<String, Object> map : maps)
							userIds.add(String.valueOf(map.get("user_id")));
						defaultUsers = String.join(",", userIds);

						ModuleDetails moduleDetails = new ModuleDetails();
						moduleDetails.setModuleName(moduleName);
						moduleDetails.setModuleDescription(moduleDescription);
						moduleDetails.setModuleStatus(Integer.valueOf(moduleStatus));
						moduleDetails.setProjectId(projectId);

						int moduleId = moduleDAO.getModuleId(moduleDetails, schema);
						int flagAssign = moduleService.assignOrUnassignModules(moduleId, projectId, defaultUsers,
								schema, moduleName);

						if (flagAssign == 200) {
							session.setAttribute("settingTab", 2);
							// logger.info("Returning to ajax post request");
							return new ModelAndView("redirect:/testcase");
						} else {
							return new ModelAndView("redirect:/" + flagAssign);
						}
					} catch (Exception e) {
						logger.info(e.getMessage());
						return new ModelAndView("redirect:/500");
					}
				}
				if (flag == 200) {
					session.setAttribute("moduleCreateStatus", 1);
					session.setAttribute("newModuleName", moduleName.trim());
					logger.info("New Module is Created. Redirected to View modules page ");
					return new ModelAndView("redirect:/viewmodules");
				} else if (flag == 208) {
					session.setAttribute("duplicateModuleError", 1);
					logger.warn("Error : Module name already exists . Redirected to create module");
					return new ModelAndView("redirect:/createmodule");
				} else {
					logger.warn("User has no access to create an application . Redirected to 404 page");
					return new ModelAndView("redirect:/" + flag);
				}
			} catch (Exception e) {
				logger.info(e.getMessage());
				return new ModelAndView("redirect:/500");
			}
		}
	}

	/**
	 * @author prafulla.pol
	 * @method setModuleId a POST Request
	 * @param moduleId
	 * @param viewType
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setmoduleid", method = RequestMethod.POST)
	public ModelAndView setModuleId(@RequestParam("moduleId") String moduleId,
			@RequestParam("viewType") String viewType, Model model, HttpSession session) throws Exception {
		logger.info("Post method to save the module id and view type into session.Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			session.setAttribute("selectedModuleId", moduleId);
			session.setAttribute("moduleViewType", viewType);
			logger.info(" User is redirected ");
			return new ModelAndView("redirect:/");
		}

	}

	/**
	 * @author prafulla.pol
	 * @method setModuleIdForAssign a GET Request
	 * @param moduleID
	 * @param moduleName
	 * @param projectID
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setmoduleidforassign", method = RequestMethod.POST)
	public ModelAndView setModuleIdForAssign(@RequestParam("moduleID") String moduleID,
			@RequestParam("moduleName") String moduleName, @RequestParam("projectID") String projectID, Model model,
			HttpSession session) throws Exception {
		logger.info(
				"Post method to save the module id ,module's project ID and module name into session.Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info(" Session is active, userId is " + userID + " Customer Schema " + schema);
			session.setAttribute("moduleIDForAssign", moduleID);
			session.setAttribute("moduleProjectIDForAssign", projectID);
			session.setAttribute("moduleNameForAssign", moduleName);
			session.setAttribute("settingTab", 1);
			logger.info("User is redirected");
			return new ModelAndView("redirect:/");
		}
	}

	/**
	 * @author prafulla.pol
	 * @method assignProjectToUser a POST Request
	 * @param model
	 * @param session
	 * @param selectedUsers
	 * @return
	 */
	@RequestMapping(value = "/assignorunassignmoduletouser", method = RequestMethod.POST)
	public @ResponseBody ModelAndView assignProjectToUser(Model model, HttpSession session,
			@RequestParam(value = "selectedUsers", defaultValue = "No Users") String selectedUsers) {
		logger.info("Ajax Post request to assign or unassign module to users");
		utilsService.getLoggerUser(session);
		int projectID = Integer.parseInt(session.getAttribute("moduleProjectIDForAssign").toString());
		int moduleID = Integer.parseInt(session.getAttribute("moduleIDForAssign").toString());
		String moduleName = session.getAttribute("moduleNameForAssign").toString();
		String schema = session.getAttribute("cust_schema").toString();
		int user = Integer.parseInt(session.getAttribute("userId").toString());
		logger.info(" Session is active, userId is " + user + " Customer Schema " + schema);
		int flag = moduleService.assignOrUnassignModules(moduleID, projectID, selectedUsers, schema, moduleName);
		if (flag == 200) {
			session.setAttribute("settingTab", 2);
			logger.info("Returning to ajax post request");
			return new ModelAndView("redirect:/modulesetting");
		} else {
			return new ModelAndView("redirect:/" + flag);
		}
	}

	/**
	 * @author prafulla.pol
	 * @method removemoduleuser a POST Request
	 * @param model
	 * @param session
	 * @param selectedUsers
	 * @return
	 */
	@RequestMapping(value = "/removemoduleuser", method = RequestMethod.POST)
	public @ResponseBody boolean removemoduleuser(Model model, HttpSession session, @RequestParam("userId") int userId,
			@RequestParam("moduleId") int moduleId) {
		logger.info("Ajax Post request to assign or unassign module to users");
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		int user = Integer.parseInt(session.getAttribute("userId").toString());
		logger.info(" Session is active, userId is " + user + " Customer Schema " + schema);
		int flag = moduleService.unassignModule(userId, moduleId, schema);
		if (flag == 200) {
			session.setAttribute("settingTab", 2);
			session.setAttribute("actionForTab", "module");
			return true;
		} else {
			return false;
		}
	}

	@RequestMapping(value = "/removemodule", method = RequestMethod.POST)
	public @ResponseBody boolean removeModule(Model model, HttpSession session,
			@RequestParam("moduleId") int moduleId) {
		logger.info("Ajax Post request to delete module");
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		int flag = moduleService.deleteModule(moduleId, schema);
		if (flag == 200) {
			return true;
		} else {
			return false;
		}
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.getLoggerUser(session);
		utilsService.sendMailonExecption(ex, session);
		return new ModelAndView("redirect:/500");
	}

	// Anmol Chadha
	@RequestMapping(value = "/restoreModule")
	public @ResponseBody ModelAndView restoreModule(@RequestParam int moduleId, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");

		} else {
			utilsService.getLoggerUser(session);

			String schema = session.getAttribute("cust_schema").toString();

			moduleDAO.restoreModuleForTrash(moduleId, schema);

		}
		return new ModelAndView("redirect:/viewtmtrash");

	}

}

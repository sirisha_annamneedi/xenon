package com.xenon.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.buildmanager.EnvVariableService;
import com.xenon.api.buildmanager.EnvironmentService;
import com.xenon.api.buildmanager.impl.EnvVariableServiceImpl;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;

@Controller
public class EnvironmentDetailsController {

	@Autowired
	BuildDAO buildDAO;

	@Autowired
	EnvironmentService environmentService;

	@Autowired
	UtilsService utilsService;

	@Autowired
	EnvVariableService envVariableService;

	private static final Logger logger = LoggerFactory.getLogger(JenkinsDetailsController.class);

	@RequestMapping(value = ("/addenvironment"), method = RequestMethod.GET)
	public ModelAndView createEnvironment(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			return new ModelAndView("addenvironment", "model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	@RequestMapping(value = ("/insertenvironment"), method = RequestMethod.POST)
	public ModelAndView insertEnvironment(Model model, HttpSession session, @RequestParam("envName") String envName,
			@RequestParam("envDescription") String envDescription,
			@RequestParam("configParameterName") String configParameterName,@RequestParam("envStatus") String envStatus ) {

		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			String schema = session.getAttribute("cust_schema").toString();
			session.setAttribute("menuLiText", "Settings");
			int flag = environmentService.insertEnvDetails(envName, envDescription, configParameterName,Integer.parseInt(envStatus),schema);
			if (flag == 201) {
				return new ModelAndView("redirect:/viewenvironment");
			} else {
				return new ModelAndView("500");
			}

		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = ("/viewenvironment"), method = RequestMethod.GET)
	public ModelAndView viewEnvironment(Model model, HttpSession session) {

		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			session.setAttribute("menuLiText", "Settings");
			String schema = session.getAttribute("cust_schema").toString();
			Map<String, Object> modelData = environmentService.getEnvDetails(schema);
			List<Map<String, Object>> envList = (List<Map<String, Object>>) modelData.get("envDetails");
			model.addAttribute("envDetails", envList);

			return new ModelAndView("viewenvironment", "model", model);

		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	@RequestMapping(value = ("/envsettings"), method = RequestMethod.GET)
	public ModelAndView addEnvVariable(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			String envId = session.getAttribute("currentEnvId").toString();
			String schema = session.getAttribute("cust_schema").toString();
			Map<String, Object> modelData = envVariableService.getVariableDetails(envId, schema);
			model.addAttribute("variableList", modelData.get("variableList"));
			return new ModelAndView("envsettings", "Model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	@RequestMapping(value = "/setenvironmentid", method = RequestMethod.POST)
	public @ResponseBody void setEnvId(Model model, HttpSession session, @RequestParam("envId") String envId)
			throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("currentEnvId", envId);
	}

	@RequestMapping(value = ("/insertenvvariable"), method = RequestMethod.POST)
	public ModelAndView insertEnvVariable(Model model, HttpSession session,
			@RequestParam("jsonArray") String jsonArray) {

		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			String schema = session.getAttribute("cust_schema").toString();
			int responsecode = 1;
			session.setAttribute("menuLiText", "Settings");
			String envId = session.getAttribute("currentEnvId").toString();
			JSONObject jsnobject = new JSONObject(jsonArray);

			String arrayName = "array";
			JSONArray array = jsnobject.getJSONArray(arrayName);

			for (int j = 0; j < array.length(); j++) {
				JSONObject jsonObj = array.getJSONObject(j);
				int flag = envVariableService.insertEnvVariableDetails(jsonObj.getString("variableName"),
						jsonObj.getString("configValue"), jsonObj.getString("variableValue"), envId, schema);
				if (flag != 201) {
					responsecode = 0;
					break;
				}
			}
			if (responsecode == 1) {
				return new ModelAndView("redirect:/envsettings");
			} else {
				return new ModelAndView("500");
			}
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	@RequestMapping(value = "/deletevariable", method = RequestMethod.POST)
	public @ResponseBody int deleteVariable(HttpSession session, Model model,
			@RequestParam("variableId") String variableId) {

		String schema = session.getAttribute("cust_schema").toString();
		session.setAttribute("menuLiText", "Settings");
		int flag = envVariableService.deleteVariable(schema, variableId);
		return flag;
	}

	@RequestMapping(value = "/updateenvvariable", method = RequestMethod.POST)
	public ModelAndView updateVariableDetails(HttpSession session, Model model,
			@RequestParam("updateVariableName") String updateVariableName,
			@RequestParam("updateConfigValue") String updateConfigValue,
			@RequestParam("updateVariableValue") String updateVariableValue,
			@RequestParam("updateVariableId") String updateVariableId) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			String schema = session.getAttribute("cust_schema").toString();
			int flag = envVariableService.updateVariableDetails(updateVariableName, updateConfigValue,
					updateVariableValue, updateVariableId, schema);
			if (flag == 201) {
				return new ModelAndView("redirect:/envsettings");
			} else {
				return new ModelAndView("500");
			}

		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	@RequestMapping(value = ("/editenvironment"), method = RequestMethod.GET)
	public ModelAndView editEnvironment(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			String envId=session.getAttribute("currentEnvId").toString();
			String schema=session.getAttribute("cust_schema").toString();
			Map<String,Object> returnData=environmentService.getEnvDetailsById(envId, schema);
			model.addAttribute("envDetails",returnData.get("envDetails"));
			return new ModelAndView("editenvironment","Model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	@RequestMapping(value = ("/updateenvdetails"), method = RequestMethod.POST)
	public ModelAndView updateEnvironment(Model model, HttpSession session,@RequestParam("envId") String  envId,@RequestParam("envName") String envName,@RequestParam("configParameterName") String configParameterName,@RequestParam("envDescription") String envDescription,@RequestParam("envStatus") String envStatus) {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (buildDAO.checkBMRoleAccess("bmdashboard", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			
			String schema=session.getAttribute("cust_schema").toString();
			int flag=environmentService.updateEnvDetails(envId,envName,configParameterName,envDescription,Integer.parseInt(envStatus),schema);
			if (flag == 201) {
				return new ModelAndView("redirect:/viewenvironment");
			} else {
				return new ModelAndView("500");
			}

			
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	@RequestMapping(value = "/deleteenvironment", method = RequestMethod.POST)
	public @ResponseBody int deleteEnv(HttpSession session, Model model,
			@RequestParam("envId") String envId) {

		String schema = session.getAttribute("cust_schema").toString();
		session.setAttribute("menuLiText", "Settings");
		int flag= environmentService.deleteEnvironment(envId,schema);
		return flag;
	}
	
}

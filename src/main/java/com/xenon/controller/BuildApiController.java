package com.xenon.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.bugtracker.BugService;
import com.xenon.api.buildmanager.BuildService;
import com.xenon.api.common.CommonService;
import com.xenon.api.common.LogMonitorService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.ServerOperationService;
import com.xenon.api.common.UtilsService;
import com.xenon.apitest.dao.ApiTestDAO;
import com.xenon.apitest.service.ApiTestService;
import com.xenon.bugtracker.dao.BugDAO;
import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.buildmanager.dao.AutomationBuildDAO;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.BuildExecDetailsDAO;
import com.xenon.buildmanager.dao.ExecuteBuildDAO;
import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.buildmanager.dao.NotificationDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.dao.ReleaseDAO;
import com.xenon.testmanager.dao.ScenarioDAO;
import com.xenon.testmanager.dao.TestSpecificationDAO;
import com.xenon.testmanager.dao.TestcaseDAO;
import com.xenon.testmanager.dao.TestcaseStepDAO;
import com.xenon.testmanager.dao.TmDashboardDAO;

@SuppressWarnings({ "unchecked" })
@Controller
public class BuildApiController {

	@Resource(name = "colorProperties")
	private Properties colorProperties;

	@Resource(name = "configurationProperties")
	private Properties configurationProperties;

	@Autowired
	ProjectDAO projectDao;

	@Autowired
	ModuleDAO moduleDao;

	@Autowired
	ScenarioDAO scenarioDao;

	@Autowired
	TestcaseDAO testcaseDao;

	@Autowired
	ReleaseDAO releaseDAO;

	@Autowired
	BuildDAO buildDAO;

	@Autowired
	BugDAO bugDao;

	@Autowired
	AutomationBuildDAO buildDAO1;

	@Autowired
	ExecuteBuildDAO exebuild;

	@Autowired
	ActivitiesDAO activitiesDAO;

	@Autowired
	TestcaseStepDAO testcaseStepDao;

	@Autowired
	BuildExecDetailsDAO buildExecDetailsDAO;

	@Autowired
	LoginService loginService;

	@Autowired
	BuildService buildService;

	@Autowired
	BugService bugService;

	@Autowired
	UtilsService utilsService;

	@Autowired
	LogMonitorService logMonitorService;

	@Autowired
	ServerOperationService serverOperationService;

	@Autowired
	RoleAccessService roleAccessService;

	@Autowired
	NotificationDAO notificationDAO;
	private static final Logger logger = LoggerFactory.getLogger(BuildController.class);

	@Autowired
	ApiTestService apiTestService;

	@Autowired
	TestSpecificationDAO testSpecificationDao;

	@Autowired
	ApiTestDAO apiTestDao;

	@Autowired
	CommonService commonService;

	@Autowired
	TmDashboardDAO tmDashDAO;

	/*
	 * @author Shailesh.khot
	 * 
	 * API test controller
	 */
	@RequestMapping(value = "/apitest")
	public ModelAndView apitest(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			List<Map<String, Object>> scenarioDetails = new ArrayList<Map<String, Object>>();
			try {
				String project = session.getAttribute("UserCurrentProjectId").toString();
				int projectId = Integer.parseInt(project);
				int userId = Integer.parseInt(session.getAttribute("userId").toString());
				String schema = session.getAttribute("cust_schema").toString();
				int moduleId = 0;
				String moduleName = "NA";
				try {
					moduleId = Integer.parseInt(session.getAttribute("selectedTestModuleId").toString());
					moduleName = session.getAttribute("selectedTestModuleName").toString();
					if (moduleId == 0) {
						List<Map<String, Object>> data = testSpecificationDao.getLastModule(projectId, userId, schema);
						if (data != null && data.size() != 0) {
							moduleId = (Integer) data.get(0).get("module_id");
							moduleName = data.get(0).get("module_name").toString();
							session.setAttribute("selectedTestModuleName", moduleName);
							session.setAttribute("selectedTestModuleId", moduleId);
						}
					}
				} catch (Exception e) {
					List<Map<String, Object>> data = testSpecificationDao.getLastModule(projectId, userId, schema);
					if (data != null && data.size() != 0) {
						moduleId = (Integer) data.get(0).get("module_id");
						moduleName = data.get(0).get("module_name").toString();
						session.setAttribute("selectedTestModuleName", moduleName);
						session.setAttribute("selectedTestModuleId", moduleId);
					}

				}
				int scenarioId = 0;
				String scenario_name = null;
				try {
					scenarioId = Integer.parseInt(session.getAttribute("selectedScenarioId").toString());
					if (scenarioId == 0) {
						List<Map<String, Object>> data = testSpecificationDao.getLastScenario(moduleId, schema);
						if (data != null && data.size() != 0) {
							scenarioId = (Integer) data.get(0).get("scenario_id");
							scenario_name = (String) data.get(0).get("scenario_name");
							session.setAttribute("selectedScenarioName", scenario_name);
							session.setAttribute("selectedScenarioId", scenarioId);
						}
					}
				} catch (Exception e) {
					List<Map<String, Object>> data = testSpecificationDao.getLastScenario(moduleId, schema);
					if (data != null && data.size() != 0) {
						scenarioId = (Integer) data.get(0).get("scenario_id");
						scenario_name = (String) data.get(0).get("scenario_name");
						session.setAttribute("selectedScenarioName", scenario_name);
						session.setAttribute("selectedScenarioId", scenarioId);
					}
				}

				Map<String, Object> data = apiTestDao.getApiTestCaseData(userId, projectId, moduleId, scenarioId,
						schema);

				// For the left menu scenario list
				List<Map<String, Object>> scenarioList = (List<Map<String, Object>>) data.get("#result-set-1");
				for (int i = 0; i < scenarioList.size(); i++) {
					if (Integer.parseInt(scenarioList.get(i).get("scenario_id").toString()) == scenarioId) {
						scenarioDetails.add(scenarioList.get(i));
						break;
					}
				}
				model.addAttribute("scenarioList", utilsService.convertListOfMapToJson(scenarioList));
				model.addAttribute("scenarioDetails", scenarioDetails);

				String userFulName = session.getAttribute("userFirstname").toString() + " "
						+ session.getAttribute("userLastname").toString();

				// For the left side menu module list
				List<Map<String, Object>> moduleList = (List<Map<String, Object>>) data.get("#result-set-2");
				model.addAttribute("moduleList", moduleList);

				model.addAttribute("moduleId", moduleId);
				model.addAttribute("moduleName", moduleName);
				model.addAttribute("userFullName", userFulName);
				model.addAttribute("nowDate",
						utilsService.getNowDateOfTimezone(session.getAttribute("userTimezone").toString()));
				model.addAttribute("prevDate",
						utilsService.getPrevDateOfTimezone(session.getAttribute("userTimezone").toString()));
				model.addAttribute("UserProfilePhoto",
						loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
				model.addAttribute("userName", session.getAttribute("userFullName"));
				model.addAttribute("role", session.getAttribute("role"));
				logger.info(" User is redirected to testspecification page ");
				return new ModelAndView("apitest", "Model", model);
			} catch (Exception ex) {
				return new ModelAndView("apitest", "Model", model);

			}

		}
	}

	/*
	 * @author Shailesh.Khot This method is only for test API Testing module. It
	 * has no other use.
	 */
	@RequestMapping(value = "/demotest", method = RequestMethod.GET)
	public ResponseEntity<String> demotest(@RequestParam String id, @RequestParam String name, Model model) throws Exception {
		// System.out.println("coming");
		/*
		 * ObjectNode json = new ObjectMapper().createObjectNode(); ObjectMapper
		 * jsonMapper = new ObjectMapper();
		 * 
		 * json.put("id", id); json.put("name", name);
		 * 
		 * 
		 * XmlMapper xmlMapper = new XmlMapper();
		 * System.out.println(xmlMapper.writeValueAsString(json.iterator().));
		 */
		JSONObject obj = new JSONObject(
				"{\"name\":\"John\",\"age\":30,\"cars\":{\"car1\":\"Ford\",\"car2\":\"BMW\",\"car3\":\"Fiat\"}}");
		/*
		 * obj.put("id", id); obj.put("name", name);
		 */

		// converting json to xml
		String xml_data = XML.toString(obj);

		// System.out.println(xml_data);
		// System.out.println(xmlMapper.writeValueAsString(foo));
		// System.out.println("BuildApiController.demotest()");
		// System.out.println(json);
		return new ResponseEntity<String>(xml_data, HttpStatus.OK);

	}

	/*
	 * @RequestMapping(path={"/testapi"},method=RequestMethod.POST)
	 * public @ResponseBody Map<String,Object> test(@RequestParam String urltext
	 * ,@RequestParam String method1, @RequestParam String
	 * bodytext, @RequestParam String paramstr, Model model) {
	 * 
	 * Map<String,Object> respData= new HashedMap<String,Object>();
	 * 
	 * final CloseableHttpClient httpClient = HttpClients.createDefault();
	 * //System.out.println("method:"+method1); String allurltext=urltext;
	 * //System.out.println(paramstr.length());
	 * //System.out.println("paramstr:"+paramstr); if(paramstr.length()>2) {
	 * StringBuffer paramListString=new StringBuffer("?"); JSONArray arr=new
	 * JSONArray(paramstr); for (int i = 0; i < arr.length(); i++) {
	 * paramListString.append(arr.getJSONObject(i).getString("key").toString());
	 * paramListString.append("=");
	 * paramListString.append(arr.getJSONObject(i).getString("value").toString()
	 * ); paramListString.append("&"); } String
	 * queryParameter=paramListString.toString();
	 * if(queryParameter.toString().endsWith("&")) {
	 * queryParameter=queryParameter.substring(0,queryParameter.length() - 1); }
	 * //System.out.println("parameter String:"+queryParameter);
	 * allurltext=urltext+queryParameter; respData.put("parameterList",
	 * queryParameter); }else { respData.put("parameterList",""); }
	 * 
	 * if(method1.equals("GET")) {
	 * 
	 * HttpGet request = new HttpGet(allurltext);
	 * 
	 * // add request headers //request.addHeader("Authorization",
	 * "Bearer yRveT7lqFkeYH2AlLRx4Td13Cthp4lxBnnUl");
	 * request.addHeader(HttpHeaders.USER_AGENT, "Googlebot");
	 * 
	 * try (CloseableHttpResponse response = httpClient.execute(request)) {
	 * 
	 * // Get HttpResponse Status String statusCode =
	 * String.valueOf(response.getStatusLine().getStatusCode()); String
	 * statusMsg = String.valueOf(response.getStatusLine().getReasonPhrase());
	 * respData.put("statusCode", statusCode); respData.put("statusMsg",
	 * statusMsg); respData.put("urlText", urltext); respData.put("method",
	 * method1);
	 * 
	 * //Get HttpResponse Headers org.apache.http.Header
	 * allHeaders[]=response.getAllHeaders(); for(int
	 * i=0;i<allHeaders.length;i++) {
	 * //System.out.println(allHeaders[i].toString());
	 * respData.put(allHeaders[i].getName(),allHeaders[i].getValue()); }
	 * 
	 * //Get HttpResponse resultData HttpEntity entity=null; try { entity =
	 * response.getEntity(); } catch (Exception e) { // TODO Auto-generated
	 * catch block e.printStackTrace(); } if (entity != null) { // return it as
	 * a String String result = EntityUtils.toString(entity);
	 * respData.put("responseData", result);
	 * //System.out.println("daata:"+result); //return result; }else { String
	 * res=String.valueOf(response.getStatusLine().getStatusCode()); // return
	 * res; } return respData;
	 * 
	 * 
	 * } catch (ClientProtocolException e) { e.printStackTrace(); } catch
	 * (IOException e1) { // TODO Auto-generated catch block
	 * e1.printStackTrace(); }
	 * 
	 * }else if(method1.equals("POST")) {
	 * 
	 * try { HttpPost request= new HttpPost(allurltext); //String json =
	 * "{"id":1,"name":"John"}"; //String
	 * json="{userId: 10,id: 200,title: ipsam aperiam voluptates qui,completed: false}"
	 * ; //String json=""; //System.out.println("bodytext:"+bodytext);
	 * StringEntity entity1 = new StringEntity(bodytext);
	 * request.setEntity(entity1); request.setHeader("Accept",
	 * "application/json"); request.setHeader("Content-type",
	 * "application/json"); try { CloseableHttpResponse response =
	 * httpClient.execute(request);
	 * //System.out.println("Executed:"+response.toString());
	 * 
	 * // Get HttpResponse Status String statusCode =
	 * String.valueOf(response.getStatusLine().getStatusCode()); String
	 * statusMsg = String.valueOf(response.getStatusLine().getReasonPhrase());
	 * respData.put("statusCode", statusCode); respData.put("statusMsg",
	 * statusMsg); respData.put("urlText", urltext); respData.put("method",
	 * method1); //System.out.println("Data:"+respData.toString()); //Get
	 * HttpResponse Headers org.apache.http.Header
	 * allHeaders[]=response.getAllHeaders(); for(int
	 * i=0;i<allHeaders.length;i++) {
	 * respData.put(allHeaders[i].getName(),allHeaders[i].getValue()); }
	 * 
	 * //Get HttpResponse resultData HttpEntity entity=null; try { entity =
	 * response.getEntity(); } catch (Exception e) { // TODO Auto-generated
	 * catch block e.printStackTrace(); } if (entity != null) { // return it as
	 * a String String result = EntityUtils.toString(entity);
	 * respData.put("responseData", result); //return result; }else { String
	 * res=String.valueOf(response.getStatusLine().getStatusCode()); // return
	 * res; //System.out.println("Response Entity: "+entity); } return respData;
	 * } catch (ClientProtocolException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (IOException e) { // TODO Auto-generated
	 * catch block e.printStackTrace(); }
	 * 
	 * 
	 * } catch (UnsupportedEncodingException e) { // TODO Auto-generated catch
	 * block e.printStackTrace(); }
	 * 
	 * } //return "error"; return respData; }
	 */

	@RequestMapping(path = { "/testapi" }, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> test(@RequestParam String urltext, @RequestParam String method1,
			@RequestParam String bodytext, @RequestParam String paramstr, Model model) {

		Map<String, Object> respData = new HashedMap<String, Object>();
		String allurltext = urltext;
		String queryParameter = "";
		if (paramstr.length() > 2) {
			StringBuffer paramListString = new StringBuffer("?");
			JSONArray arr = new JSONArray(paramstr);
			for (int i = 0; i < arr.length(); i++) {
				paramListString.append(arr.getJSONObject(i).getString("key").toString());
				paramListString.append("=");
				paramListString.append(arr.getJSONObject(i).getString("value").toString());
				paramListString.append("&");
			}
			queryParameter = paramListString.toString();
			if (queryParameter.toString().endsWith("&")) {
				queryParameter = queryParameter.substring(0, queryParameter.length() - 1);
			}
			allurltext = urltext + queryParameter;
		}

		switch (method1) {
		case "GET":
			try {
				CloseableHttpResponse response = apiTestService.methodGet(allurltext);
				respData = apiTestService.getResponseResult(response, allurltext, method1);
				respData.put("parameterList", queryParameter);

			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;

		case "POST":
			try {
				CloseableHttpResponse response = apiTestService.methodPost(allurltext, bodytext);
				respData = apiTestService.getResponseResult(response, allurltext, method1);
				respData.put("parameterList", queryParameter);

			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;
		}

		return respData;

	}

	@RequestMapping(path = { "/saveapitestcases" }, method = RequestMethod.POST)
	public @ResponseBody String saveapitestcases(@RequestParam String tcdata, HttpSession session, Model model) {

		String schema = session.getAttribute("cust_schema").toString();
		int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
		int userId = Integer.parseInt(session.getAttribute("userId").toString());
		int moduleId = Integer.parseInt(session.getAttribute("selectedTestModuleId").toString());
		int scenarioId = Integer.parseInt(session.getAttribute("selectedScenarioId").toString());
		int status = 1;
		int executionType = 4;
		int executionTime = 0;
		int author = userId;
		int updatedBy = userId;
		int isDeleted = 0;
		String header = "";
		String result = "";
		int authenticationTypeId = 0;
		String bodyParam = "";
		//System.out.println(tcdata);
		// int methodId;
		byte[] bytes=null;

		try {
			JSONArray arr = new JSONArray(tcdata);
			for (int i = 0; i < arr.length(); i++) {
				String testcaseName = arr.getJSONObject(i).getString("tcname").toString();
				if (arr.getJSONObject(i).getString("method").contains("GET")) {
					bodyParam = arr.getJSONObject(i).getString("parameter").toString();
				} else {
					bodyParam = arr.getJSONObject(i).getString("bodytext").toString();
				}
				String url = arr.getJSONObject(i).getString("urltext").toString();
				String methodName = arr.getJSONObject(i).getString("method").toString();
				if (methodName.equalsIgnoreCase("GET")) {
					executionTime = 1;
				} else {
					executionTime = 2;
				}
				int contentTypeId = Integer.parseInt(arr.getJSONObject(i).getString("contentTypeId").toString());
				// int contentTypeId=1;
				int flag = apiTestService.insertApiTestCase(testcaseName, scenarioId, bodyParam, url, status,
						executionType, executionTime, author, updatedBy, methodName, projectId, moduleId, contentTypeId,
						isDeleted, header, result, authenticationTypeId,bytes, schema);
				//System.out.println("flag:" + flag);
			} // end for
		} catch (Exception e) {
			e.printStackTrace();
		}
		// System.out.println("schema:"+schema+",projectId:"+projectId+",userId:"+userId+",moduleId:"+moduleId+",scenarioId:"+scenarioId);

		return "Success";

	}

	@RequestMapping(value = "/apitestcase", method = RequestMethod.GET)
	public ModelAndView getApiTestCaseList(@RequestParam(value = "page", defaultValue = "1") int page, Model model,
			HttpSession session) throws Exception {
		List<Map<String, Object>> scenarioDetails = new ArrayList<Map<String, Object>>();
		logger.info("Redirecting to test case page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (tmDashDAO.checkTMRoleAccess("testcase", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);

			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if (appCount == 0) {
				return new ModelAndView("redirect:/xedashboard");
			}
			try {
				String project = session.getAttribute("UserCurrentProjectId").toString();
				int projectId = Integer.parseInt(project);
				int userId = Integer.parseInt(session.getAttribute("userId").toString());
				String schema = session.getAttribute("cust_schema").toString();
				int moduleId = 0;
				String moduleName = "NA";
				try {
					moduleId = Integer.parseInt(session.getAttribute("selectedTestModuleId").toString());
					moduleName = session.getAttribute("selectedTestModuleName").toString();
					if (moduleId == 0) {
						List<Map<String, Object>> data = testSpecificationDao.getLastModule(projectId, userId, schema);
						if (data != null && data.size() != 0) {
							moduleId = (Integer) data.get(0).get("module_id");
							moduleName = data.get(0).get("module_name").toString();
							session.setAttribute("selectedTestModuleName", moduleName);
							session.setAttribute("selectedTestModuleId", moduleId);
						}
					}
				} catch (Exception e) {
					List<Map<String, Object>> data = testSpecificationDao.getLastModule(projectId, userId, schema);
					if (data != null && data.size() != 0) {
						moduleId = (Integer) data.get(0).get("module_id");
						moduleName = data.get(0).get("module_name").toString();
						session.setAttribute("selectedTestModuleName", moduleName);
						session.setAttribute("selectedTestModuleId", moduleId);
					}

				}
				int scenarioId = 0;
				String scenario_name = null;
				try {
					scenarioId = Integer.parseInt(session.getAttribute("selectedScenarioId").toString());
					if (scenarioId == 0) {
						List<Map<String, Object>> data = testSpecificationDao.getLastScenario(moduleId, schema);
						if (data != null && data.size() != 0) {
							scenarioId = (Integer) data.get(0).get("scenario_id");
							scenario_name = (String) data.get(0).get("scenario_name");
							session.setAttribute("selectedScenarioName", scenario_name);
						}
					}
				} catch (Exception e) {
					List<Map<String, Object>> data = testSpecificationDao.getLastScenario(moduleId, schema);
					if (data != null && data.size() != 0) {
						scenarioId = (Integer) data.get(0).get("scenario_id");
						scenario_name = (String) data.get(0).get("scenario_name");
						session.setAttribute("selectedScenarioName", scenario_name);
					}
				}
				int pageSize = 10;
				int startValue = (page - 1) * pageSize;

				logger.info("Session is active, userId is " + userId + " , projectId is " + project
						+ " Customer Schema " + schema);

				Map<String, Object> data = testSpecificationDao.getTestCaseData(userId, projectId, moduleId, scenarioId,
						schema, startValue, pageSize);

				// For the left menu scenario list
				List<Map<String, Object>> scenarioList = (List<Map<String, Object>>) data.get("#result-set-1");
				for (int i = 0; i < scenarioList.size(); i++) {
					if (Integer.parseInt(scenarioList.get(i).get("scenario_id").toString()) == scenarioId) {
						scenarioDetails.add(scenarioList.get(i));
						break;
					}
				}
				model.addAttribute("scenarioList", utilsService.convertListOfMapToJson(scenarioList));

				// For the test case table
				List<Map<String, Object>> testCaseList = (List<Map<String, Object>>) data.get("#result-set-2");
				for (int i = 0; i < testCaseList.size(); i++) {

					String logo = loginService.getImage((byte[]) testCaseList.get(i).get("user_photo"));
					testCaseList.get(i).put("user_photo", logo);

				}
				model.addAttribute("testCaseList", testCaseList);

				String userFulName = session.getAttribute("userFirstname").toString() + " "
						+ session.getAttribute("userLastname").toString();

				// For the left side menu module list
				List<Map<String, Object>> moduleList = (List<Map<String, Object>>) data.get("#result-set-3");
				model.addAttribute("moduleList", moduleList);

				// For time line activity
				List<Map<String, Object>> testCaseSummary = (List<Map<String, Object>>) data.get("#result-set-4");
				for (int i = 0; i < testCaseSummary.size(); i++) {
					testCaseSummary.get(i).put("activity_date",
							utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
									utilsService.getDate(testCaseSummary.get(i).get("activity_date").toString())));
				}

				// For test data list
				List<Map<String, Object>> testdatas = (List<Map<String, Object>>) data.get("#result-set-5");
				model.addAttribute("testdatas", testdatas);

				List<Map<String, Object>> executionTypes = (List<Map<String, Object>>) data.get("#result-set-6");
				model.addAttribute("executionTypes", executionTypes);
				List<Map<String, Object>> statusList = (List<Map<String, Object>>) data.get("#result-set-7");
				model.addAttribute("statusList", statusList);
				// System.out.println("testdatas:"+testdatas);
				int allTcCount = (Integer) data.get("count");

				model.addAttribute("allTcCount", allTcCount);
				model.addAttribute("pageSize", pageSize);
				model.addAttribute("scenarioDetails", scenarioDetails);
				model.addAttribute("testCaseSummary", testCaseSummary);
				model.addAttribute("moduleId", moduleId);
				model.addAttribute("moduleName", moduleName);
				model.addAttribute("userFullName", userFulName);
				model.addAttribute("nowDate",
						utilsService.getNowDateOfTimezone(session.getAttribute("userTimezone").toString()));
				model.addAttribute("prevDate",
						utilsService.getPrevDateOfTimezone(session.getAttribute("userTimezone").toString()));
				model.addAttribute("UserProfilePhoto",
						loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
				model.addAttribute("userName", session.getAttribute("userFullName"));
				model.addAttribute("role", session.getAttribute("role"));
				logger.info(" User is redirected to testspecification page ");
				return new ModelAndView("apitestcase", "Model", model);
			} catch (Exception ex) {
				return new ModelAndView("apitestcase", "Model", model);

			}
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	@RequestMapping(value = "/getLoadedFileData", method = RequestMethod.POST)
	@Consumes(MediaType.APPLICATION_JSON)
	public @ResponseBody String getLoadedFileData(HttpSession session, @RequestParam("uploadForm") CommonsMultipartFile uploadForm) {
		byte[] bytes = uploadForm.getBytes();
		try {
			String str=uploadForm.getInputStream().toString();
			//System.out.println("Data:"+str);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return utilsService.getFileData(bytes);
	}
	
	
	
	@RequestMapping(path = { "/performValidation" }, method = RequestMethod.POST)
	public @ResponseBody String performValidation(@RequestParam String responseJson,@RequestParam String validationArray,HttpSession session, Model model) {
		
		JSONObject responseArray = new JSONObject(responseJson);
		JSONArray fileDataArray = new JSONArray(validationArray);
		
		JSONArray resultArray=new JSONArray();
		//JSONObject resultObject=null;
		String responseValue,fileDataValue;
		
		for (int i = 0; i < fileDataArray.length(); i++) {
				String key=fileDataArray.getJSONObject(i).getString("key").toString();
				int level=fileDataArray.getJSONObject(i).getInt("level");
				if(responseArray.has(key) && responseArray.getInt("level")==level) {
					
					responseValue=responseArray.getString(key).toString();
					fileDataValue=fileDataArray.getJSONObject(i).get("value").toString();
					JSONObject resultObject=new JSONObject();
					resultObject.put("key", key);
					resultObject.put("responseValue", responseValue);
					
					if(fileDataArray.getJSONObject(i).get("isNull").toString().equals("true"))
					{
						if(responseValue==null) {
							resultObject.put("isNullResult", "Pass");
						}else {
							resultObject.put("isNullResult", "Fail");
						}
					}
					
					if(fileDataArray.getJSONObject(i).get("isEmpty").toString().equals("true"))
					{
						if(responseValue.isEmpty()) {
							resultObject.put("isEmptyResult", "Pass");
						}else {
							resultObject.put("isEmptyResult", "Fail");
						}
					}
					
					if(fileDataArray.getJSONObject(i).get("isNotNull").toString().equals("true"))
					{
						if(responseValue != null) {
							resultObject.put("isNotNullResult", "Pass");
						}else {
							resultObject.put("isNotNullResult", "Fail");
						}
					}
					
					if(fileDataArray.getJSONObject(i).get("isNotEmpty").toString().equals("true"))
					{
						if(responseValue.isEmpty()==false) {
							resultObject.put("isNotEmptyResult", "Pass");
						}else {
							resultObject.put("isNotEmptyResult", "Fail");
						}
					}
					
					if(fileDataArray.getJSONObject(i).has("containsString")) {
						if(responseValue.contains(fileDataArray.getJSONObject(i).get("containsString").toString())) {
							resultObject.put("containsString", "Pass");
						}else {
							resultObject.put("containsString", "Fail");
						}
					}
					resultArray.put(resultObject);
			}
			
		}
		
		return resultArray.toString();
	}
	
	
	@RequestMapping(path = { "/saveapitestcasesWithAssertion" }, method = RequestMethod.POST)
	public @ResponseBody String saveapitestcasesWithAssertion(@RequestParam String tcdata, @RequestParam String validationArray, HttpSession session, Model model) {

		String schema = session.getAttribute("cust_schema").toString();
		int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
		int userId = Integer.parseInt(session.getAttribute("userId").toString());
		int moduleId = Integer.parseInt(session.getAttribute("selectedTestModuleId").toString());
		int scenarioId = Integer.parseInt(session.getAttribute("selectedScenarioId").toString());
		int status = 1;
		int executionType = 4;
		int executionTime = 0;
		int author = userId;
		int updatedBy = userId;
		int isDeleted = 0;
		String header = "";
		String result = "";
		int authenticationTypeId = 0;
		String bodyParam = "";
		//System.out.println(tcdata);
		
		//System.out.println("validationArray:"+validationArray);
		byte[] bytes=validationArray.getBytes();
		
		// int methodId;

		try {
			JSONArray arr = new JSONArray(tcdata);
			for (int i = 0; i < arr.length(); i++) {
				String testcaseName = arr.getJSONObject(i).getString("tcname").toString();
				if (arr.getJSONObject(i).getString("method").contains("GET")) {
					bodyParam = arr.getJSONObject(i).getString("parameter").toString();
				} else {
					bodyParam = arr.getJSONObject(i).getString("bodytext").toString();
				}
				String url = arr.getJSONObject(i).getString("urltext").toString();
				String methodName = arr.getJSONObject(i).getString("method").toString();
				if (methodName.equalsIgnoreCase("GET")) {
					executionTime = 1;
				} else {
					executionTime = 2;
				}
				int contentTypeId = Integer.parseInt(arr.getJSONObject(i).getString("contentTypeId").toString());
				// int contentTypeId=1;
				int flag = apiTestService.insertApiTestCase(testcaseName, scenarioId, bodyParam, url, status,
						executionType, executionTime, author, updatedBy, methodName, projectId, moduleId, contentTypeId,
						isDeleted, header, result, authenticationTypeId,bytes, schema);
				//System.out.println("flag:" + flag);
			} // end for
		} catch (Exception e) {
			e.printStackTrace();
		}
		// System.out.println("schema:"+schema+",projectId:"+projectId+",userId:"+userId+",moduleId:"+moduleId+",scenarioId:"+scenarioId);

		return "Success";

	}
}

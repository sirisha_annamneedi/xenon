package com.xenon.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.DocumentLibraryDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.buildmanager.domain.DocumentLibraryDetails;

/**
 * 
 * @author bhagyashri.ajmera
 * 
 * This controller handles all document library related operations 
 *
 */
@SuppressWarnings("unchecked")
@Controller
public class DocumentLibraryController {

	private static final Logger logger = LoggerFactory.getLogger(DocumentLibraryController.class);
	
	@Autowired
	DocumentLibraryDAO docLibraryService;

	@Autowired
	UserDAO userService;

	@Autowired
	ProjectDAO projectDAO;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	UtilsService utilsService;


	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This is view for document library page
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/documentlibrary")
	public ModelAndView documentlibrary(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			Map<String, Object> docData = null;
			if (utilsService.checkSessionContainsAttribute("uploadProjectId", session)) {
				docData = docLibraryService.getDocumentLibraryData(
						Integer.parseInt(session.getAttribute("uploadProjectId").toString()), userId, schema);
			} else {
				logger.info("Reading document library data");
				docData = docLibraryService.getDocumentLibraryData(0, userId, schema);
			}
			List<Map<String, Object>> projectDetails = (List<Map<String, Object>>) docData.get("#result-set-1");
			model.addAttribute("projectDetails", projectDetails);
			session.setAttribute("menuLiText", "Settings");
			if (projectDetails.size() > 0) {
				List<Map<String, Object>> projectDetail = (List<Map<String, Object>>) docData.get("#result-set-2");
				int projectId = Integer.parseInt(projectDetail.get(0).get("projectId").toString());
				String projectName = projectDetail.get(0).get("projectName").toString();
				session.setAttribute("uploadProjectId", projectId);
				session.setAttribute("uploadProjectName", projectName);
				List<Map<String, Object>> listFiles = (List<Map<String, Object>>) docData.get("#result-set-3");
				for(int i=0;i<listFiles.size();i++)
					listFiles.get(i).put("upload_time", utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(), utilsService.getDate(listFiles.get(i).get("upload_time").toString())));
				logger.info("Add attributes to model");
				model.addAttribute("listFiles", listFiles);
			} else {
				logger.info("No project found");
				model.addAttribute("NoProjectFound", 1);
			}
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			return new ModelAndView("documentlibrary", "Model", model);
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request uploads document for project
	 * 
	 * @param file
	 * @param session
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/documentlibraryupload", method = RequestMethod.POST)
	public ModelAndView documentlibraryupload(@RequestParam(value = "uploadImage", required = false) MultipartFile file,
			HttpSession session, Model model, HttpServletRequest request) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			logger.info("Uploading document");
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int flag = 0;
			DocumentLibraryDetails docDetails = new DocumentLibraryDetails();

			String schema = session.getAttribute("cust_schema").toString();
			String fileName = file.getOriginalFilename();
			String fileType = "";
			logger.info("verifying file type");
			if (fileName.endsWith(".pdf"))
				fileType = "application/pdf";
			else if (fileName.endsWith(".doc") || fileName.endsWith(".docx"))
				fileType = "application/msword";
			else if (fileName.endsWith(".png") || fileName.contains(".jpg") || fileName.contains(".jpeg"))
				fileType = "image/jpg";

			byte[] bytes = new byte[0];
			if (!file.isEmpty())
				bytes = file.getBytes();

			docDetails.setDocData(bytes);
			docDetails.setDocTitle(file.getOriginalFilename());
			docDetails.setDocType(fileType);
			docDetails.setUploadedBy(userId);
			docDetails.setProjectId(Integer.parseInt(session.getAttribute("uploadProjectId").toString()));
			logger.info("Uploading document to database");
			flag = docLibraryService.uploadDocument(docDetails, schema);
			if (flag > 0) {
				logger.info("Document uploaded successfully");
				return new ModelAndView("redirect:/documentlibrary");
			} else
			{
				logger.info("Document upload failed");
				return new ModelAndView("redirect:/500");
			}
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request gives document by id
	 * 
	 * @param doclibraryId
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getDocumentById", method = RequestMethod.POST)
	public @ResponseBody String getDocumentById(@RequestParam("doclibraryId") String doclibraryId, HttpSession session)
			throws Exception {
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		logger.info("Reading document by document id");
		List<Map<String, Object>> document = docLibraryService.getDocumentById(Integer.parseInt(doclibraryId), schema);
		return utilsService.getFileData((byte[]) document.get(0).get("doc_data"));
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request sets project id in session to upload
	 * 
	 * @param model
	 * @param session
	 * @param projectId
	 * @param projectName
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setuploadprojectid", method = RequestMethod.POST)
	public ModelAndView setuploadprojectid(Model model, HttpSession session,
			@RequestParam("projectId") String projectId, @RequestParam("projectName") String projectName)
					throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else {
		utilsService.getLoggerUser(session);
		logger.info("Setting values to session");
		session.setAttribute("uploadProjectId", projectId);
		session.setAttribute("uploadProjectName", projectName);
		return new ModelAndView("redirect:/");
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This request handles all exceptions for this controller
	 * 
	 * @param ex
	 * @param response
	 * @param session
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.getLoggerUser(session);
		utilsService.sendMailonExecption(ex, session);
		return new ModelAndView("redirect:/500");
	}
}

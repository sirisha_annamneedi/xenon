package com.xenon.controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.xenon.api.administration.LDAPService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class LDAPController {

	@Autowired
	RoleAccessService roleAccessService;

	@Autowired
	LDAPService ldapService;

	@Autowired
	UtilsService utilsService;

	private static final Logger logger = LoggerFactory.getLogger(LDAPController.class);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * @param page
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/accesscontrol")
	public ModelAndView accesscontrol(@RequestParam(value = "page", defaultValue = "1") int page, Model model,
			HttpSession session) throws Exception {

		logger.info("Redirecting to bmdashboard page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (roleAccessService.checkBMAccess("accountsettings", session)) {

			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");
			Map<String, Object> response = ldapService
					.getCustomerLdapDetails(Integer.parseInt(session.getAttribute("customerId").toString()));
			int status = Integer.parseInt(response.get("responseCode").toString());
			if (status == 201) {
				model.addAttribute("customerLDAPStatus", session.getAttribute("customerLDAPStatus"));
				model.addAttribute("ldapDetails", response.get("ldapDetails"));
				logger.info(" User is redirected access control  page ");
				return new ModelAndView("accesscontrol", "Model", model);
			} else {
				return new ModelAndView("redirect:/500");
			}

		} else {
			logger.error("User looking for resources is not available or prohibited");
			return new ModelAndView("redirect:/404");
		}

	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * @param page
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/ldaptoown")
	public ModelAndView ldaptoown(@RequestParam(value = "page", defaultValue = "1") int page, Model model,
			HttpSession session) throws Exception {

		logger.info("Redirecting to bmdashboard page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (roleAccessService.checkBMAccess("accountsettings", session)) {

			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");

			logger.info(" User is redirected access control  page ");
			return new ModelAndView("ldaptoown", "Model", model);
		} else {
			logger.error("User looking for resources is not available or prohibited");
			return new ModelAndView("redirect:/404");
		}

	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * @param page
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	@RequestMapping(value = "/owntoldap")
	public ModelAndView owntoldap(@RequestParam(value = "page", defaultValue = "1") int page, Model model,
			HttpSession session) throws Exception {

		logger.info("Redirecting to bmdashboard page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (roleAccessService.checkBMAccess("accountsettings", session)) {

			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "DashBoard");

			String schema = session.getAttribute("cust_schema").toString();
			int custTypeId = Integer.parseInt(session.getAttribute("cust_type_id").toString());
			String customerId = session.getAttribute("customerId").toString();
			String timeZone = session.getAttribute("userTimezone").toString();
			Map<String, Object> response = ldapService.getLdapAndOwnUserList(Integer.parseInt(customerId), schema);
			int status = Integer.parseInt(response.get("responseCode").toString());
			if (status == 201) {
				List<Map<String, Object>> users = (List<Map<String, Object>>) response.get("userDetailsList");
				model.addAttribute("requireToMap",users.size());
				model.addAttribute("userDetailsList", response.get("userDetailsList"));
				model.addAttribute("ldapUserDetails", response.get("ldapUserDetails"));
				logger.info("LDAP details read successfully");
				return new ModelAndView("owntoldap", "Model", model);
			} else {
				logger.error("LDAP details read unsuccessful");
				return new ModelAndView("redirect:/500");
			}
		} else {
			logger.error("User looking for resources is not available or prohibited");
			return new ModelAndView("redirect:/404");
		}
	}

	@RequestMapping(value = "/insertldapdetails", method = RequestMethod.POST)
	public ModelAndView insertldapdetails(Model model, HttpSession session,
			@RequestParam(value = "accessControlChecked", defaultValue = "1") String accessControlChecked,
			@RequestParam(value = "ldapUrl") String ldapURL, @RequestParam(value = "ldapRootDN") String rootDN,
			@RequestParam(value = "ldapSearchBase", defaultValue = "") String searchBase,
			@RequestParam(value = "userSearchFilter", defaultValue = "mail={0}") String searchFilter,
			@RequestParam(value = "ldapUserDN", defaultValue = "") String userDistinguishedName,
			@RequestParam(value = "ldapUserPass", defaultValue = "") String userPassword,
			@RequestParam(value = "ldapUserMail", defaultValue = "") String ldapUserMail,
			@RequestParam(value = "displayNameAttr", defaultValue = "displayName") String displayNameAttr,
			@RequestParam(value = "emailAddrAttr", defaultValue = "mail") String emailAddrAttr,
			@RequestParam(value = "emailDomainName", defaultValue = "") String emailDomainName) {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/");
		} else if (roleAccessService.checkBMAccess("accountsettings", session)) {
			utilsService.getLoggerUser(session);
			logger.info("Setting mail details domain");
			if(accessControlChecked.equalsIgnoreCase(session.getAttribute("customerLDAPStatus").toString()) &&  accessControlChecked.equalsIgnoreCase("2")){
				return new ModelAndView("redirect:/admindashboard");
			}
			else if (accessControlChecked.equalsIgnoreCase("1")) {
				String schema = session.getAttribute("cust_schema").toString();
				Map<String, String> response = ldapService.insertLDAPDetails(ldapURL, rootDN, searchBase, searchFilter,
						userDistinguishedName, userPassword, displayNameAttr, emailAddrAttr, emailDomainName,
						Integer.parseInt(session.getAttribute("customerId").toString()),ldapUserMail,schema);
				int status = Integer.parseInt(response.get("responseCode").toString());
				if (status == 201) {
					session.setAttribute("customerLDAPStatus", accessControlChecked);
					logger.info("LDAP details inserted successfully");
					return new ModelAndView("redirect:/owntoldap");
				} else {
					logger.error("LDAP details insertion unsuccessful");
					return new ModelAndView("500");
				}
			} else {
				String schema = session.getAttribute("cust_schema").toString();
				Map<String, String> response = ldapService.updateCstomerLdapStatus(
						Integer.parseInt(session.getAttribute("customerId").toString()), schema);
				int status = Integer.parseInt(response.get("responseCode").toString());
				if (status == 201) {
					session.setAttribute("customerLDAPStatus", accessControlChecked);
					logger.info("LDAP status updated successfully");
					return new ModelAndView("redirect:/admindashboard");
				} else {
					logger.error("LDAP status update unsuccessful");
					return new ModelAndView("500");
				}
			}

		} else
			return new ModelAndView("500");
	}

	@RequestMapping(value = "/testldapdetails", method = RequestMethod.POST)
	public @ResponseBody int testldapdetails(Model model, HttpSession session,
			@RequestParam(value = "ldapUrl") String ldapURL, @RequestParam(value = "ldapRootDN") String rootDN,
			@RequestParam(value = "ldapSearchBase", defaultValue = "") String searchBase,
			@RequestParam(value = "userSearchFilter", defaultValue = "mail={0}") String searchFilter,
			@RequestParam(value = "ldapUserMail", defaultValue = "") String ldapUserMail,
			@RequestParam(value = "ldapUserDN") String userDistinguishedName,
			@RequestParam(value = "ldapUserPass") String userPassword) {
		int flag = 404;
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
		} else if (roleAccessService.checkBMAccess("accountsettings", session)) {
			utilsService.getLoggerUser(session);
			logger.info("Setting mail details domain");
			Map<String, String> response = ldapService.testLdapDetails(ldapURL, rootDN, searchBase, searchFilter,
					userDistinguishedName, userPassword,ldapUserMail);
			int status = Integer.parseInt(response.get("responseCode").toString());
			if (status == 201) {
				logger.info("LDAP details tested successfully");
				flag = status;
			}else if(status==204){
				logger.error("LDAP details test unsuccessful");
				flag = status;
			}else {
				logger.error("LDAP details test unsuccessful");
				flag = status;
			}
		}
		return flag;
	}

	@RequestMapping(value = "/mapuserslist", method = RequestMethod.POST)
	public @ResponseBody boolean mapuserslist(Model model, HttpSession session,
			@RequestParam(value = "xenonUserList[]") String[] xenonUser,
			@RequestParam(value = "ldapUserList[]") String[] ldapUser) {
		boolean flag = false;
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return flag;
		} else if (roleAccessService.checkBMAccess("accountsettings", session)) {
			utilsService.getLoggerUser(session);
			logger.info("Setting mail details domain");
			String schema = session.getAttribute("cust_schema").toString();
			Map<String, String> response = ldapService.mapXenonUserToLdapUser(xenonUser, ldapUser, schema);
			int status = Integer.parseInt(response.get("responseCode").toString());
			if (status == 201) {
				logger.info("LDAP details inserted successfully");
				flag = true;
			} else {
				logger.error("LDAP details insertion unsuccessful");
				flag = false;
			}
		}
		return flag;
	}

	@RequestMapping(value = "/testmapset", method = RequestMethod.POST)
	public @ResponseBody boolean testmapset(Model model, HttpSession session,
			@RequestParam(value = "ldapUserList") String ldapUserList) {

		//System.out.println("test");

		return false;

	}

	/**
	 * This request handles all exceptions in this controller
	 * 
	 * @param ex
	 * @param response
	 * @param session
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		return new ModelAndView("redirect:/500");
	}

}

package com.xenon.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.jadelabs.custmoze.query.release.RedwoodManagement;
import com.xenon.api.bugtracker.BugService;
import com.xenon.api.buildmanager.BuildService;
import com.xenon.api.common.LogMonitorService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.ServerOperationService;
import com.xenon.api.common.UtilsService;
import com.xenon.bugtracker.dao.BugDAO;
import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.buildmanager.dao.AutomationBuildDAO;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.BuildExecDetailsDAO;
import com.xenon.buildmanager.dao.ExecuteBuildDAO;
import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.buildmanager.dao.NotificationDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.dao.ReleaseDAO;
import com.xenon.buildmanager.domain.BuildDetails;
import com.xenon.buildmanager.domain.BuildExecDetails;
import com.xenon.constants.ExecutionType;
import com.xenon.testmanager.dao.ScenarioDAO;
import com.xenon.testmanager.dao.TestcaseDAO;
import com.xenon.testmanager.dao.TestcaseStepDAO;
import com.xenon.util.service.FileUploadForm;

@SuppressWarnings({ "unchecked" })
@Controller
public class BuildController {

	@Resource(name = "colorProperties")
	private Properties colorProperties;

	@Resource(name = "configurationProperties")
	private Properties configurationProperties;

	@Autowired
	ProjectDAO projectDao;

	@Autowired
	ModuleDAO moduleDao;

	@Autowired
	ScenarioDAO scenarioDao;

	@Autowired
	TestcaseDAO testcaseDao;

	@Autowired
	ReleaseDAO releaseDAO;

	@Autowired
	BuildDAO buildDAO;

	@Autowired
	BugDAO bugDao;

	@Autowired
	AutomationBuildDAO buildDAO1;

	@Autowired
	ExecuteBuildDAO exebuild;

	@Autowired
	ActivitiesDAO activitiesDAO;

	@Autowired
	TestcaseStepDAO testcaseStepDao;

	@Autowired
	BuildExecDetailsDAO buildExecDetailsDAO;

	@Autowired
	LoginService loginService;

	@Autowired
	BuildService buildService;

	@Autowired
	BugService bugService;

	@Autowired
	UtilsService utilsService;

	@Autowired
	LogMonitorService logMonitorService;

	@Autowired
	ServerOperationService serverOperationService;

	@Autowired
	RoleAccessService roleAccessService;

	@Autowired
	NotificationDAO notificationDAO;
	private static final Logger logger = LoggerFactory.getLogger(BuildController.class);

	/**
	 * @author suresh.adling
	 * @method createBuild GET Request
	 * @output get create build view
	 * @param Model
	 *            model,HttpSession session
	 * @description This method reads following list by using session stored
	 *              values(userId,projectId) and redirects user to create bug
	 *              view with the model
	 * @return Model (exeTypes,automationStatus,activeReleaseNames) & View
	 *         (onSuccess - createbuild ,onFailure - logout/trialerror/500)
	 */
	@RequestMapping(value = "/createbuild")
	public ModelAndView createBuild(Model model, HttpSession session) throws Exception {
		logger.info("User is  Redirecting to create build page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			logger.info("Checking user's access to create build service");
			if (buildDAO.checkBMRoleAccess("createbuild", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to create build");
				session.setAttribute("menuLiText", "Builds");
				int flag = buildDAO.createNewBuildStatus(session.getAttribute("cust_schema").toString(),
						Integer.parseInt(session.getAttribute("cust_type_id").toString()));
				logger.info("Checking wheather user has exceeded limit to create new build");
				if (flag == 1) {
					String schema = session.getAttribute("cust_schema").toString();
					Map<String, Object> data = buildDAO.getCreateBuildData(schema);
					model.addAttribute("activeReleaseNames", data.get("#result-set-1"));
					model.addAttribute("exeTypes", data.get("#result-set-2"));
					model.addAttribute("envList", data.get("#result-set-3"));
					if (session.getAttribute("duplicateBuildError") != null) {
						model.addAttribute("error", 1);
						session.removeAttribute("duplicateBuildError");
					}
					model.addAttribute("automationStatus",
							Integer.parseInt(session.getAttribute("automationStatus").toString()));
					model.addAttribute("scriptlessStatus",
							Integer.parseInt(session.getAttribute("custScriptlessStatus").toString()));
					logger.info("User is redirected to create build page");

					if (buildDAO.checkBMRoleAccess("createrelease", session.getAttribute("roleId").toString(),
							session.getAttribute("cust_schema").toString())) {
						model.addAttribute("createReleaseAccess", 1);
					} else {
						model.addAttribute("createReleaseAccess", 0);
					}
					// changes
					Map<String, Object> data1 = buildDAO.getCloneBuildData(schema);
					model.addAttribute("releaseDetails", (List<Map<String, Object>>) data1.get("#result-set-1"));
					model.addAttribute("manualBuildDetailsJSON", utilsService
							.convertListOfMapToJson((List<Map<String, Object>>) data1.get("#result-set-2")));
					model.addAttribute("manualBuildDetails", (List<Map<String, Object>>) data1.get("#result-set-2"));
					model.addAttribute("automationBuildDetails",
							(List<Map<String, Object>>) data1.get("#result-set-3"));
					model.addAttribute("automationBuildDetailsJSON", utilsService
							.convertListOfMapToJson((List<Map<String, Object>>) data1.get("#result-set-3")));
					model.addAttribute("automationStatus",
							Integer.parseInt(session.getAttribute("automationStatus").toString()));
					model.addAttribute("exeTypes", data1.get("#result-set-4"));
					// end changes
					return new ModelAndView("createbuild", "Model", model);
				} else {
					logger.error("User can not create new build so redirected to trial error page");
					return new ModelAndView("redirect:/trialerror");
				}
			} else {
				logger.error("User doesn't have access to create build service and is redirected to error page");
				return new ModelAndView("redirect:/500");
			}

		}
	}

	/**
	 * @author suresh.adling
	 * @method cloneBuild GET Request
	 * @output get clone build view
	 * @param Model
	 *            model,HttpSession session
	 * @return Model (exeTypes,automationStatus,activeReleaseNames) & View
	 *         (onSuccess - clonebuild ,onFailure - logout/500)
	 */
	@RequestMapping(value = "/clonebuild")
	public ModelAndView cloneBuild(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to clone build page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			logger.info("Checking user's access to clone build service");
			if (buildDAO.checkBMRoleAccess("createbuild", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to create build");
				session.setAttribute("menuLiText", "Builds");
				logger.info("User is redirected to clone build page");
				String schema = session.getAttribute("cust_schema").toString();
				Map<String, Object> data = buildDAO.getCloneBuildData(schema);
				model.addAttribute("releaseDetails", (List<Map<String, Object>>) data.get("#result-set-1"));
				model.addAttribute("manualBuildDetailsJSON",
						utilsService.convertListOfMapToJson((List<Map<String, Object>>) data.get("#result-set-2")));
				model.addAttribute("manualBuildDetails", (List<Map<String, Object>>) data.get("#result-set-2"));
				model.addAttribute("automationBuildDetails", (List<Map<String, Object>>) data.get("#result-set-3"));
				model.addAttribute("automationBuildDetailsJSON",
						utilsService.convertListOfMapToJson((List<Map<String, Object>>) data.get("#result-set-3")));
				model.addAttribute("automationStatus",
						Integer.parseInt(session.getAttribute("automationStatus").toString()));
				model.addAttribute("exeTypes", data.get("#result-set-4"));
				return new ModelAndView("clonebuild", "Model", model);
			} else {
				logger.error("User doesn't have access to clone build service and is redirected to error page");
				return new ModelAndView("redirect:/500");
			}
		}
	}

	/***
	 * 
	 * This request is used to view add remove test cases for manual build
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	/* Creating Json For Adding TestCases By Anmol Chadha */
	@RequestMapping(value = "/buildproject")
	public ModelAndView buildproject(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to build project and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			logger.info("Checking user's access to build project service");
			if (buildDAO.checkBMRoleAccess("buildproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to build project");
				String schema = session.getAttribute("cust_schema").toString();
				int testRemoveStatus = 0;
				try {
					testRemoveStatus = Integer.parseInt(session.getAttribute("UsertestRemoveStatus").toString());
				} catch (Exception e) {
					// TODO Auto-generat
				}
				session.setAttribute("menuLiText", "Builds");
				int userId = Integer.parseInt(session.getAttribute("userId").toString());
				int buildId = Integer.parseInt(session.getAttribute("BuildId").toString());

				List<Map<String, Object>> getAllBuild = buildDAO.getBuildProjectData(userId, buildId, schema);
				// System.out.println(getAllBuild);
				List<Map<String, Object>> projectsList = new ArrayList<Map<String, Object>>();
				List<Map<String, Object>> modulesList = new ArrayList<Map<String, Object>>();
				List<Map<String, Object>> scenarioList = new ArrayList<Map<String, Object>>();
				List<Map<String, Object>> testCaseList = new ArrayList<Map<String, Object>>();

				for (int i = 0; i < getAllBuild.size(); i++) {
					Map<String, Object> projectsListTemp = new HashMap<String, Object>();
					projectsListTemp.put("project_id", getAllBuild.get(i).get("project_id"));
					projectsListTemp.put("project_name", getAllBuild.get(i).get("project_name"));
					projectsListTemp.put("project_description", getAllBuild.get(i).get("project_description"));
					projectsListTemp.put("project_active", getAllBuild.get(i).get("project_active"));
					projectsList.add(projectsListTemp);

					Map<String, Object> modulesListTemp = new HashMap<String, Object>();
					if (!(getAllBuild.get(i).get("module_id") == null)) {
						modulesListTemp.put("project_id", getAllBuild.get(i).get("project_id"));
						modulesListTemp.put("project_name", getAllBuild.get(i).get("project_name"));
						modulesListTemp.put("module_id", getAllBuild.get(i).get("module_id"));
						modulesListTemp.put("module_name", getAllBuild.get(i).get("module_name"));
						modulesListTemp.put("module_description", getAllBuild.get(i).get("module_description"));
						modulesList.add(modulesListTemp);
					}
					Map<String, Object> scenarioListTemp = new HashMap<String, Object>();
					if (!(getAllBuild.get(i).get("scenario_id") == null)) {
						scenarioListTemp.put("module_id", getAllBuild.get(i).get("module_id"));
						scenarioListTemp.put("module_name", getAllBuild.get(i).get("module_name"));
						scenarioListTemp.put("scenario_id", getAllBuild.get(i).get("scenario_id"));
						scenarioListTemp.put("scenario_name", getAllBuild.get(i).get("scenario_name"));
						scenarioListTemp.put("scenario_description", getAllBuild.get(i).get("scenario_description"));
						scenarioList.add(scenarioListTemp);
					}

					Map<String, Object> testCaseListTemp = new HashMap<String, Object>();
					if (!(getAllBuild.get(i).get("testcase_id") == null)) {
						testCaseListTemp.put("testcase_id", getAllBuild.get(i).get("testcase_id"));
						testCaseListTemp.put("testcase_name", getAllBuild.get(i).get("testcase_name"));
						testCaseListTemp.put("scenario_id", getAllBuild.get(i).get("scenario_id"));
						testCaseListTemp.put("test_complete", getAllBuild.get(i).get("test_complete"));
						testCaseListTemp.put("test_executed", getAllBuild.get(i).get("test_executed"));
						testCaseListTemp.put("projectid", getAllBuild.get(i).get("projectid"));
						testCaseListTemp.put("mdl_id", getAllBuild.get(i).get("mdl_id"));
						testCaseListTemp.put("sce_id", getAllBuild.get(i).get("sce_id"));
						testCaseListTemp.put("test_prefix", getAllBuild.get(i).get("test_prefix"));
						if (getAllBuild.get(i).get("test_complete") == null) {
							testCaseListTemp.put("testStatus", 0);
						} else if (getAllBuild.get(i).get("test_complete") != null
								&& getAllBuild.get(i).get("test_executed") != null) {
							testCaseListTemp.put("testStatus", 1);
						} else {
							testCaseListTemp.put("testStatus", 2);
						}
						testCaseList.add(testCaseListTemp);
					}
				}
				testCaseList = utilsService.removeDuplicate(testCaseList);
				projectsList = utilsService.getTestCount(testCaseList, utilsService.removeDuplicate(projectsList),
						"project");
				modulesList = utilsService.getTestCount(testCaseList, utilsService.removeDuplicate(modulesList),
						"module");
				scenarioList = utilsService.getTestCount(testCaseList, utilsService.removeDuplicate(scenarioList),
						"scenario");
				model.addAttribute("projectsList", projectsList);
				model.addAttribute("modulesList", modulesList);
				model.addAttribute("addremovetestcaseBuildName", session.getAttribute("addremovetestcaseBuildName"));
				model.addAttribute("scenarioList", scenarioList);
				model.addAttribute("testcasesList", testCaseList);

				JSONArray jsonArray = new JSONArray();
				for (Map<String, Object> map : projectsList) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("id", "project_" + map.get("project_id"));
					jsonObject.put("text", map.get("project_name").toString().trim());
					jsonObject.put("parent", "#");
					jsonArray.put(jsonObject);
				}

				for (Map<String, Object> map : modulesList) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("id", "module_" + map.get("module_id"));
					jsonObject.put("text", map.get("module_name").toString().trim());
					jsonObject.put("parent", "project_" + map.get("project_id"));
					jsonArray.put(jsonObject);

				}
				for (Map<String, Object> map : scenarioList) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("id", "scenario_" + map.get("scenario_id"));
					jsonObject.put("text", map.get("scenario_name").toString().trim());
					jsonObject.put("parent", "module_" + map.get("module_id"));
					jsonArray.put(jsonObject);

				}
				JSONArray testCaseIdsJSON = new JSONArray();
				for (Map<String, Object> map : testCaseList) {
					JSONObject jsonObject = new JSONObject();
					String testCaseId = String.valueOf(map.get("testcase_id"));
					jsonObject.put("id", testCaseId);
					jsonObject.put("text", map.get("testcase_name").toString().replaceAll("\\W", " ").trim());
					jsonObject.put("parent", "scenario_" + map.get("scenario_id"));
					jsonArray.put(jsonObject);
					String testStatus = String.valueOf(map.get("testStatus"));
					if ("2".equals(testStatus) || "1".equals(testStatus))
						testCaseIdsJSON.put(testCaseId);

				}
				model.addAttribute("projectDetailsJSONArray", jsonArray);
				model.addAttribute("testCaseIdsJSON", testCaseIdsJSON);

				model.addAttribute("removeStatus", testRemoveStatus);
				model.addAttribute("UserProfilePhoto",
						loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
				model.addAttribute("userName", session.getAttribute("userFullName"));
				model.addAttribute("role", session.getAttribute("role"));
				Map<String, Object> manualReleasesAndBuilds = buildDAO.getReleasesAndBuildsManual(schema);
				model.addAttribute("releases",
						(List<Map<String, Object>>) manualReleasesAndBuilds.get("#result-set-1"));
				model.addAttribute("builds", (List<Map<String, Object>>) manualReleasesAndBuilds.get("#result-set-2"));
				logger.info("Model value for build project view : " + model);
				logger.info("User is redirected to  build project");
				return new ModelAndView("buildproject", "Model", model);
			} else {
				logger.error("User doesn't have access to build project service and is redirected to error page");
				return new ModelAndView("redirect:/500");
			}
		}
	}

	/**
	 * This request is used to add test cases
	 * 
	 * @param addTestCaseArr
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	/** AddTestCase By Anmol Chadha */
	@RequestMapping(value = "/addtestcase")
	public ModelAndView addtestcase(@RequestParam("testCaseArray[]") int addTestCaseArr[], Model model,

			HttpSession session) throws Exception {

		logger.info("In post method add test cases");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Builds");
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int buildId = Integer.parseInt(session.getAttribute("BuildId").toString());
			int count = 0;
			int flag = 0;
			int j = 0;
			StringBuilder testCaseArray = new StringBuilder();
			for (int i : addTestCaseArr) {
				j++;
				if (addTestCaseArr.length == j)
					testCaseArray.append(i);

				else
					testCaseArray.append(i + ",");
			}

			flag = buildDAO.addTestCaseById(testCaseArray.toString(), buildId, userId, schema);

			/*if (flag != 0) {
				logger.info("Test cases are added to the build");
				for (int index = 0; index < addTestCaseArr.length; index++) {
					Map<String, Object> insertTcData = buildDAO.insertTestcaseSummary(addTestCaseArr[index],
							Integer.parseInt(session.getAttribute("BuildId").toString()), 2,
							Integer.parseInt(session.getAttribute("userId").toString()), 5, "", 2,
							session.getAttribute("userFullName").toString(),
							Integer.parseInt(session.getAttribute("userId").toString()), schema);

					count = count + Integer.parseInt(insertTcData.get("#update-count-1").toString());

				}
				if (count > 0) {

					logger.info("Test cases set to default not run");
					return new ModelAndView("redirect:/");
				} else {

					logger.error("Something went wrong, User redirected to error page");
					return new ModelAndView("redirect:/500");
				}

			}*/
			if(flag!=0){
				return new ModelAndView("redirect:/");
			}else {
			
				logger.error("Something went wrong, User redirected to error page");
				return new ModelAndView("redirect:/500");
			}
		}
	}

	/**
	 * This request is used to drop removed test case from build
	 * 
	 * @param dropTestArray
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	/* Drop TestCases By Anmol Chadha */
	@RequestMapping(value = "/droptestcase")
	public ModelAndView droptestcase(@RequestParam("dropTestArray[]") int dropTestArray[], Model model,
			HttpSession session) throws Exception {
		logger.info("In post method drop test cases");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Builds");
			String schema = session.getAttribute("cust_schema").toString();
			int j = 0;
			StringBuilder droptestCaseArray = new StringBuilder();
			for (int i : dropTestArray) {
				j++;
				if (dropTestArray.length == j)
					droptestCaseArray.append(i);
				else
					droptestCaseArray.append(i + ",");
			}
			int buildId = Integer.parseInt(session.getAttribute("BuildId").toString());
			int flag = buildDAO.dropTestCaseBySce(droptestCaseArray.toString(), buildId, schema);
			if (flag != 0) {
				logger.info("Test cases are droped from the build");
				return new ModelAndView("redirect:/");
			} else {
				logger.error("Something went wrong, User redirected to error page");
				return new ModelAndView("redirect:/500");
			}
		}
	}

	/**
	 * This request is used to update complete build execution
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/completebuild")
	public @ResponseBody String completebuild(Model model, HttpSession session) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("menuLiText", "Builds");
		String schema = session.getAttribute("cust_schema").toString();
		String userName = session.getAttribute("userFullName").toString();
		int buildId = Integer.parseInt(session.getAttribute("BuildId").toString());
		int userId = Integer.parseInt(session.getAttribute("userId").toString());

		String buildName = session.getAttribute("addremovetestcaseBuildName").toString();
		Map<String, Object> returnData = buildService.completebuild(buildId, userId, schema, buildName, userName);
		model.addAttribute("addedTcCount", returnData.get("addedTcCount"));
		return returnData.get("result").toString();
	}

	/**
	 * Thsi request is used to set build is session
	 * 
	 * @param buildId
	 * @param buildName
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setBuildSession")
	public ModelAndView setBuildSession(@RequestParam("buildId") int buildId,
			@RequestParam("buildName") String buildName, Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Builds");
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());

			List<Map<String, Object>> getTestRemoveStatus = testcaseDao.getTestRemoveStatus(userId, schema);

			session.setAttribute("UsertestRemoveStatus", getTestRemoveStatus.get(0).get("removetestcases").toString());
			session.setAttribute("BuildId", buildId);
			session.setAttribute("addremovetestcaseBuildName", buildName);
			return new ModelAndView("redirect:/");
		}
	}

	/**
	 * This request is used to set automation build id in session
	 * 
	 * @param buildId
	 * @param buildName
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setautobuildsession")
	public ModelAndView setAutoBuildSession(@RequestParam("buildId") int buildId,
			@RequestParam("buildName") String buildName, Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Builds");
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			List<Map<String, Object>> getTestRemoveStatus = testcaseDao.getTestRemoveStatus(userId, schema);
			session.setAttribute("UsertestRemoveStatus", getTestRemoveStatus.get(0).get("removetestcases").toString());
			session.setAttribute("automationExecBuildId", buildId);
			session.setAttribute("automationExecBuildName", buildName);
			return new ModelAndView("redirect:/");
		}
	}

	/**
	 * This request is used to set assign build session
	 * 
	 * @param buildId
	 * @param buildName
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setAssignBuildSession")
	public ModelAndView setAssignBuildSession(@RequestParam("buildId") int buildId,
			@RequestParam("buildName") String buildName, Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			List<Map<String, Object>> getTestRemoveStatus = testcaseDao.getTestRemoveStatus(userId, schema);
			session.setAttribute("UsertestRemoveStatus", getTestRemoveStatus.get(0).get("removetestcases").toString());
			session.setAttribute("BuildId", buildId);
			session.setAttribute("assignBuildId", buildId);
			session.setAttribute("assignBuildName", buildName);
			session.setAttribute("selectedExecuteBuildName", buildName);
			return new ModelAndView("redirect:/");
		}
	}
	
	@RequestMapping(value = "/clonebuild1", method = RequestMethod.POST) //insertbuild
	public ModelAndView clonebuild(
			@RequestParam("buildName") String buildName, 
			@RequestParam("buildId") String buildId,
			@RequestParam("buildReleaseId") String releaseId,
			@RequestParam("buildDescription") String buildDescription,
			@RequestParam("buildStatus") String buildStatus,
			@RequestParam(value = "buildEnv", defaultValue = "1") String buildEnv,
			@RequestParam(required = false) String source,
			@RequestParam(value = "buildExecutionType", defaultValue = "1") String buildExecutionType,
			@RequestParam(value = "selectedReleaseName") String selectedReleaseName,
			Model model, HttpSession session) throws Exception {
	
		
		logger.info("Redirecting to build project and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired, user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			String redwoodId = null;
			if(Integer.parseInt(buildExecutionType) == ExecutionType.REDWOOD.getValue()) {
				RedwoodManagement redwoodManagement = new RedwoodManagement();
				redwoodId = redwoodManagement.saveOrUpdateTestSet(session.getAttribute("redwood_url").toString(), "", buildName, 
						session.getAttribute("UserCurrentProjectName").toString(), new String[0]);
			}
			//creates a new record in xe_automation_build table
			session.setAttribute("cloneBuildId", buildId);
			//to fetch releaseId of selectedReleaseName
			String schema = session.getAttribute("cust_schema").toString();
			releaseId = buildDAO.getReleaseIdOfSelectedRelease(selectedReleaseName, schema);
			int flag = buildService.insertBuild(buildName, buildDescription, Integer.parseInt(buildStatus),
					buildExecutionType, Integer.parseInt(buildEnv), Integer.parseInt(buildExecutionType),session.getAttribute("userFullName").toString(),
					session.getAttribute("cust_schema").toString(), Integer.parseInt(releaseId),
					Integer.parseInt(session.getAttribute("userId").toString()), utilsService.getCurrentDateTime(),
					session.getAttribute("roleId").toString(), redwoodId);
			if(flag == 200) {
			   if(Integer.parseInt(buildExecutionType) == ExecutionType.MANUAL.getValue()) {
				   session.setAttribute("newBuildTypeForTab", "manual");
			   }else if(Integer.parseInt(buildExecutionType) == ExecutionType.AUTOMATION.getValue()) {
				   session.setAttribute("newBuildTypeForTab", "automation");
			   }else {
				   session.setAttribute("newBuildTypeForTab", "scriptless");
			   }
			   if (source != null && "LeftNavMenuManual".equalsIgnoreCase(source))
				   return new ModelAndView("redirect:/manualCloneBuild"); //executebuild
			   else if(source != null && "LeftNavMenuAutomation".equalsIgnoreCase(source))
				   return new ModelAndView("redirect:/autoCloneBuild"); //autoexecute
			   if(Integer.parseInt(buildExecutionType) == ExecutionType.MANUAL.getValue()) {
				   return new ModelAndView("redirect:/manualCloneBuild"); // executebuild
			   } else {
				   return new ModelAndView("redirect:/autoCloneBuild"); //autoexecute
			   }
		} else if(flag == 208) {
			session.setAttribute("duplicateBuildError", "1");
			if(Integer.parseInt(buildExecutionType) == ExecutionType.MANUAL.getValue()) {
				return new ModelAndView("redirect:/executebuild"); 
			} else {
				return new ModelAndView("redirect:/autoexecute");  
			}
		} else {
			logger.error("User doesn't have access to create build service and is redirected to error page");
			return new ModelAndView("redirect:/" + flag);
		}
		}
	}
	
	@RequestMapping(value = "/manualCloneBuild")
	public ModelAndView manualCloneBuild(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired, user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Builds");
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int cloneBuildId = Integer.parseInt(session.getAttribute("cloneBuildId").toString());// old build id
			List<Map<String, Object>> lastManualDetails = buildDAO.getlastBuildDetails(userId, schema);
			int latestBuildId = (int) lastManualDetails.get(0).get("build_id");
			session.setAttribute("BuildId", latestBuildId);
			// to fetch TC no.s of particular testSet
			List<Map<String, Object>> getManualTcDetails = buildDAO.getManualTcDetails(cloneBuildId, schema);
			List<Integer> tcIds = new ArrayList<>();
			for (Map<String, Object> tcDetails : getManualTcDetails) {
				int testCaseId = (int) tcDetails.get("testcase_id");
				tcIds.add(testCaseId);
			}
			int flag = 0;
			int j = 0;
			StringBuilder testCaseArray = new StringBuilder();
			for (int i : tcIds) {
				j++;
				if (tcIds.size() == j)
					testCaseArray.append(i);
				else
					testCaseArray.append(i + ",");
			}
			flag = buildDAO.addTestCaseById(testCaseArray.toString(), latestBuildId, userId, schema);
			if (flag != 0) {
				System.out.println("added manual TCs successfully for clone build");
				session.setAttribute("selectedExecuteBuildId", latestBuildId);
				return new ModelAndView("redirect:/executebuild");
			} else {
				System.out.println("something went wrong in adding manual TC's for clone build");
				return new ModelAndView("redirect:/" + flag);
			}
		}
	}
	
	@RequestMapping(value = "/autoCloneBuild")
	public ModelAndView autoCloneExecute(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to build project and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired, user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			logger.info("Checking user's access to build project service");
			if (buildDAO.checkBMRoleAccess("buildproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to build project");
				String schema = session.getAttribute("cust_schema").toString();
				session.setAttribute("menuLiText", "Builds"); // ?
				int userId = Integer.parseInt(session.getAttribute("userId").toString());
				int cloneBuildId = Integer.parseInt(session.getAttribute("cloneBuildId").toString());// old build id 
				List<Map<String, Object>> lastAutoDetails = buildDAO.getlastAutoBuildDetails(userId, schema);
				int latestBuildId = (int) lastAutoDetails.get(0).get("build_id");
				model.addAttribute("automationExecBuildName", session.getAttribute("automationExecBuildName"));
				session.setAttribute("BuildId", latestBuildId);
				int testRemoveStatus = Integer.parseInt(session.getAttribute("UsertestRemoveStatus").toString());
				model.addAttribute("removeStatus", testRemoveStatus);
				// to fetch TC no.s of particular testSet
				List<Map<String, Object>> getAutomationTcDetails = buildDAO.getAutomationTcDetails(cloneBuildId, schema);
				List<Integer> tcIds = new ArrayList<>();
				for (Map<String, Object> tcDetails : getAutomationTcDetails) {
					int testCaseId = (int) tcDetails.get("testcase_id");
					tcIds.add(testCaseId);
				}
				int flag = 0;
				int j = 0;
				StringBuilder testCaseArray = new StringBuilder();
				for (int i : tcIds) {
					j++;
					if (tcIds.size() == j)
						testCaseArray.append(i);
					else
						testCaseArray.append(i + ",");
				}
				flag = buildDAO.addAutomationTestCase(testCaseArray.toString(), latestBuildId, userId, schema);
				if (flag != 0) {
					System.out.println("added automation TCs successfully for clone build");
					session.setAttribute("automationExecBuildId", latestBuildId);
					return new ModelAndView("redirect:/autoexecute");
				} else {
					System.out.println("something went wrong in adding automation TC's for clone build");
					return new ModelAndView("redirect:/" + flag);
				}
			} else {
				logger.error("User doesn't have access to clone build service and is redirected to error page");
				return new ModelAndView("redirect:/500");
			}
		}
	}
	
	/**
	 * 
	 * @author suresh.adling
	 * @method insertBuild POST Request
	 * @output new build is added
	 * @param buildName
	 * @param buildDescription
	 * @param releaseId
	 * @param buildStatus
	 * @param buildExecutionType
	 * @param model
	 * @param session
	 * @return view - createbuild
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/insertbuild", method = RequestMethod.POST)
	public ModelAndView insertBuild(@RequestParam("buildName") String buildName,
			@RequestParam("buildDescription") String buildDescription, @RequestParam("buildReleaseId") String releaseId,
			@RequestParam("buildStatus") String buildStatus, @RequestParam(required = false) String source,
			@RequestParam(value = "buildExecutionType", defaultValue = "1") String buildExecutionType,
			@RequestParam(value = "buildEnv", defaultValue = "1") String buildEnv, /*@RequestParam("testSetExecType") String testSetExecType,*/ Model model, HttpSession session)
			throws Exception {
		logger.info("In post request to insert a build");
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			String redwoodId = null;
			if ( Integer.parseInt(buildExecutionType) == ExecutionType.REDWOOD.getValue() ) {
				RedwoodManagement redwoodManagement = new RedwoodManagement();
				redwoodId = redwoodManagement.saveOrUpdateTestSet(session.getAttribute("redwood_url").toString(), "", buildName, 
								session.getAttribute("UserCurrentProjectName").toString(), new String[0] );
			}
			int flag = buildService.insertBuild(buildName, buildDescription, Integer.parseInt(buildStatus),
					buildExecutionType, Integer.parseInt(buildEnv), Integer.parseInt(buildExecutionType),session.getAttribute("userFullName").toString(),
					session.getAttribute("cust_schema").toString(), Integer.parseInt(releaseId),
					Integer.parseInt(session.getAttribute("userId").toString()), utilsService.getCurrentDateTime(),
					session.getAttribute("roleId").toString(), redwoodId);
			if (flag == 200) {
				if (Integer.parseInt(buildExecutionType) == ExecutionType.MANUAL.getValue() ) {
					session.setAttribute("newBuildTypeForTab", "manual");
				} else if (Integer.parseInt(buildExecutionType) == ExecutionType.AUTOMATION.getValue() ) {
					session.setAttribute("newBuildTypeForTab", "automation");
				} else {
					session.setAttribute("newBuildTypeForTab", "scriptless");
				}
				if (source != null && "LeftNavMenuManual".equalsIgnoreCase(source))
					return new ModelAndView("redirect:/executebuild");
				else if (source != null && "LeftNavMenuAutomation".equalsIgnoreCase(source))
					return new ModelAndView("redirect:/autoexecute");
				// return new ModelAndView("redirect:/viewbuild");
				if (Integer.parseInt(buildExecutionType) == ExecutionType.MANUAL.getValue()) {
					return new ModelAndView("redirect:/executebuild");
				} else {
					return new ModelAndView("redirect:/autoexecute");
				}
			} else if (flag == 208) {
				session.setAttribute("duplicateBuildError", "1");
				if (Integer.parseInt(buildExecutionType) == ExecutionType.MANUAL.getValue()) {
					return new ModelAndView("redirect:/executebuild");
				} else {  
					return new ModelAndView("redirect:/autoexecute");
				}
				//return new ModelAndView("redirect:/viewbuild");// previously it
																// was
																// /createbuild
			} else {
				logger.error("User doesn't have access to create build service and is redirected to error page");
				return new ModelAndView("redirect:/" + flag);
			}
		}
	}

	/**
	 * @author suresh.adling
	 * @method editBuild GET Request
	 * @output modify build details
	 * @param model
	 * @param session
	 * @return view - editbuild
	 */

	@RequestMapping(value = "/editbuild")
	public ModelAndView editBuild(Model model, HttpSession session) {
		logger.info("Redirecting to edit build  and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			if (buildDAO.checkBMRoleAccess("editbuild", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				session.setAttribute("menuLiText", "Builds");
				int buildId = 0;
				int type = 0;
				if (session.getAttribute("selectedBuildID") != null) {
					buildId = Integer.parseInt(session.getAttribute("selectedBuildID").toString());
					type = 1;
				} else if (session.getAttribute("selectedAutoBuildID") != null) {
					buildId = Integer.parseInt(session.getAttribute("selectedAutoBuildID").toString());
					type = 2;
				}
				String schema = session.getAttribute("cust_schema").toString();
				Map<String, Object> data = buildDAO.getEditBuildData(buildId, type, schema);
				List<Map<String, Object>> buildDetails = (List<Map<String, Object>>) data.get("#result-set-1");
				List<Map<String, Object>> activeReleaseNames = (List<Map<String, Object>>) data.get("#result-set-2");
				model.addAttribute("buildDetails", buildDetails);
				model.addAttribute("activeReleaseNames", activeReleaseNames);
				logger.info("Model value for edit build  view : " + model);
				logger.info("User is redirected to  edit build");
				return new ModelAndView("editbuild", "model", model);
			} else
				logger.error("User doesn't have access to edit build service and is redirected to error page");
			return new ModelAndView("redirect:/500");
		}
	}

	/**
	 * 
	 * @author suresh.adling
	 * @method updateBuild POST Request - update build details
	 * @param buildID
	 * @param buildName
	 * @param buildDescription
	 * @param releaseId
	 * @param bldStatus
	 * @param model
	 * @param session
	 * @return view - viewbuild
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatebuild", method = RequestMethod.POST)
	public ModelAndView updateBuild(@RequestParam("buildID") String buildId,
			@RequestParam("buildName") String buildName, @RequestParam("buildDescription") String buildDescription,
			@RequestParam("buildReleaseId") String releaseId, @RequestParam("buildStatus") String buildStatus,
			Model model, HttpSession session) throws Exception {
		logger.info("In post request to update build");
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("buildCreateStatus", 2);
			session.setAttribute("newBuildName", buildName.trim());
			int flag = 0;
			if (session.getAttribute("selectedBuildID") != null) {
				flag = buildService.updateBuild(buildName, buildDescription, Integer.parseInt(buildStatus), 1, 1,
						session.getAttribute("cust_schema").toString(), Integer.parseInt(releaseId),
						Integer.parseInt(session.getAttribute("userId").toString()), Integer.parseInt(buildId),
						utilsService.getCurrentDateTime(), session.getAttribute("roleId").toString());
				session.setAttribute("newBuildTypeForTab", "manual");
			} else if (session.getAttribute("selectedAutoBuildID") != null) {
				flag = buildService.updateBuild(buildName, buildDescription, Integer.parseInt(buildStatus), 2, 1,
						session.getAttribute("cust_schema").toString(), Integer.parseInt(releaseId),
						Integer.parseInt(session.getAttribute("userId").toString()), Integer.parseInt(buildId),
						utilsService.getCurrentDateTime(), session.getAttribute("roleId").toString());
				session.setAttribute("newBuildTypeForTab", "automation");
			}
			if (flag == 200) {

				logger.info("User is redirected to view build");
				return new ModelAndView("redirect:/viewbuild");
			} else {
				logger.error("Something went wrong,User  is redirected to error page");
				return new ModelAndView("redirect:/" + flag);
			}
		}
	}

	/**
	 * @author shantaram.tupe 04-Nov-2019:5:23:04 PM
	 * @param buildId
	 * @param buildName
	 * @param source
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatebuildname", method = RequestMethod.POST)
	public ModelAndView updateBuildName(@RequestParam Integer buildId, @RequestParam String buildName,
			@RequestParam String source, Model model, HttpSession session) throws Exception {
		logger.info("In post request to update build");
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("buildCreateStatus", 2);
			session.setAttribute("newBuildName", buildName.trim());
			boolean isManual = "LeftNavMenuManual".equals(source) ? true : false;
			int flag = 0;
			flag = buildService.updateBuildName(buildId, buildName, isManual,
					session.getAttribute("cust_schema").toString(), session.getAttribute("roleId").toString());
			if (flag == 200 || flag == 404) {
				if (isManual)
					return new ModelAndView("redirect:/executebuild");
				else
					return new ModelAndView("redirect:/autoexecute");
			} else {
				logger.error("Something went wrong,User  is redirected to error page");
				return new ModelAndView("redirect:/" + flag);
			}
		}
	}

	/**
	 * @author shantaram.tupe 01-Nov-2019:12:18:45 PM
	 * @param buildId
	 * @param source
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/deletetestset")
	public ModelAndView deleteBuild(@RequestParam Integer buildId, @RequestParam String source, Model model,
			HttpSession session) {
		logger.info("In post request to delete build");
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			boolean isManual = "LeftNavMenuManual".equals(source) ? true : false;
			int flag = 0;
			flag = this.buildService.deleteBuildSoftly(buildId, isManual,
					session.getAttribute("cust_schema").toString(), session.getAttribute("roleId").toString());
			if (flag == 200)
				if (isManual)
					session.removeAttribute("selectedExecuteBuildId");
				else
					session.removeAttribute("automationExecBuildId");
			if (flag == 200 || flag == 404) {
				if (isManual)
					return new ModelAndView("redirect:/executebuild");
				else
					return new ModelAndView("redirect:/autoexecute");
			} else {
				logger.error("Something went wrong,User  is redirected to error page");
				return new ModelAndView("redirect:/" + flag);
			}
		}
	}

	/**
	 * This request is to add remove test cases to build view
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addremovetestcase")
	public ModelAndView addremovetestcase(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			if (buildDAO.checkBMRoleAccess("addremovetestcase", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				session.setAttribute("menuLiText", "Builds");
				String schema = session.getAttribute("cust_schema").toString();
				String role = session.getAttribute("role").toString();
				List<Map<String, Object>> details = null;
				int userID = Integer.parseInt(session.getAttribute("userId").toString());
				if (role.equalsIgnoreCase("manager")) {
					details = buildDAO.getAddRemoveTCDetailsForManager(schema);
				} else {
					details = buildDAO.getAddRemoveTCDetailsForCreator(userID, schema);
				}
				List<Map<String, Object>> tcCountDetails = buildDAO.getBuildTcCount(userID, schema);

				model.addAttribute("details", details);
				model.addAttribute("tcCountDetails", tcCountDetails);
				model.addAttribute("removeStatus", buildDAO.checkBMRoleAccess("removetestcases",
						session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString()));
				return new ModelAndView("addremovetestcase", "model", model);
			} else
				return new ModelAndView("redirect:/500");
		}

	}

	/**
	 * This request is to view all builds
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/viewbuild")
	public ModelAndView viewBuild(@RequestParam(value = "manual", defaultValue = "1") int manual,
			@RequestParam(value = "auto", defaultValue = "1") int auto,
			@RequestParam(value = "sc", defaultValue = "1") int sc,
			@RequestParam(value = "buildType", defaultValue = "manual") String buildType, Model model,
			HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			if (buildDAO.checkBMRoleAccess("viewbuild", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				session.setAttribute("menuLiText", "Builds");

				boolean createStatus = buildDAO.checkBMRoleAccess("createbuild",
						session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString());
				if (createStatus) {
					model.addAttribute("createBuildAccessStatus", 1);
				}
				int pageSize = 10;
				model.addAttribute("pageSize", pageSize);

				int manualStartValue = (manual - 1) * pageSize;
				int autoStartValue = (auto - 1) * pageSize;
				int scStartValue = (sc - 1) * pageSize;

				String schema = session.getAttribute("cust_schema").toString();
				int userId = Integer.parseInt(session.getAttribute("userId").toString());

				int scriptlessStatus = Integer.parseInt(session.getAttribute("custScriptlessStatus").toString());
				int automationStatus = Integer.parseInt(session.getAttribute("automationStatus").toString());

				Map<String, Object> data = buildDAO.getViewBuildData(userId, automationStatus, scriptlessStatus,
						manualStartValue, autoStartValue, scStartValue, pageSize, schema);

				List<Map<String, Object>> allBuildDetails = (List<Map<String, Object>>) data.get("#result-set-3");
				int manualBuildCount = (Integer) data.get("manualbuildcount");
				model.addAttribute("manualBuildCount", manualBuildCount);

				if (automationStatus == 1) {
					List<Map<String, Object>> allAutomationBuilds = (List<Map<String, Object>>) data
							.get("#result-set-4");
					int autobuildcount = (Integer) data.get("autobuildcount");
					model.addAttribute("autobuildcount", autobuildcount);

					for (int j = 0; j < allAutomationBuilds.size(); j++) {
						int state = Integer.parseInt(allAutomationBuilds.get(j).get("build_state").toString());
						if (state == 3 || state == 4) {

							allAutomationBuilds.get(j).put("executeFlag", 1);
						}
					}

					for (int i = 0; i < allAutomationBuilds.size(); i++) {
						allAutomationBuilds.get(i).put("build_createdate", utilsService.getDateTimeOfTimezone(
								session.getAttribute("userTimezone").toString(),
								utilsService.getDate(allAutomationBuilds.get(i).get("build_createdate").toString())));
					}

					if (allAutomationBuilds.size() > 0)
						allAutomationBuilds.get(0).put("user_photo",
								loginService.getImage((byte[]) allAutomationBuilds.get(0).get("user_photo")));

					model.addAttribute("allAutomationBuilds", allAutomationBuilds);
				}

				List<Map<String, Object>> buildAssignee = (List<Map<String, Object>>) data.get("#result-set-2");
				if (buildAssignee != null) {
					buildAssignee = utilsService.removeDuplicate(buildAssignee);
				}

				for (int j = 0; j < allBuildDetails.size(); j++) {
					int state = Integer.parseInt(allBuildDetails.get(j).get("build_state").toString());
					if (state == 3 || state == 4 || state == 6) {

						// allBuildDetails.get(j).put("executeFlag",
						// checkBuildAssigner);
						allBuildDetails.get(j).put("executeFlag", 1);
					}
				}

				if (scriptlessStatus == 1) {
					List<Map<String, Object>> allScriptlessBuilds = (List<Map<String, Object>>) data
							.get("#result-set-5");
					int scbuildcount = (Integer) data.get("scbuildcount");
					model.addAttribute("scbuildcount", scbuildcount);
					for (int j = 0; j < allScriptlessBuilds.size(); j++) {
						String logo = loginService.getImage((byte[]) allScriptlessBuilds.get(j).get("user_photo"));
						if (j == 0)
							allScriptlessBuilds.get(j).put("user_photo", logo);
						allScriptlessBuilds.get(j).put("build_createdate", utilsService.getDateTimeOfTimezone(
								session.getAttribute("userTimezone").toString(),
								utilsService.getDate(allScriptlessBuilds.get(j).get("build_createdate").toString())));
						int state = Integer.parseInt(allScriptlessBuilds.get(j).get("build_state").toString());
						if (state == 3 || state == 4) {

							allScriptlessBuilds.get(j).put("executeFlag", 1);
						}
					}
					model.addAttribute("allScriptlessBuilds", allScriptlessBuilds);
				}
				for (int i = 0; i < allBuildDetails.size(); i++) {
					allBuildDetails.get(i).put("build_createdate",
							utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
									utilsService.getDate(allBuildDetails.get(i).get("build_createdate").toString())));
				}

				if (allBuildDetails.size() > 0)
					allBuildDetails.get(0).put("user_photo",
							loginService.getImage((byte[]) allBuildDetails.get(0).get("user_photo")));
				// get relase details
				List<Map<String, Object>> releaseDetails = releaseDAO
						.getAllReleaseDetails(session.getAttribute("cust_schema").toString());
				model.addAttribute("releaseDetails", releaseDetails);
				model.addAttribute("allBuildDetails", allBuildDetails);
				model.addAttribute("buildAssignee", buildAssignee);
				model.addAttribute("accessList", data.get("#result-set-1"));
				model.addAttribute("userID", userId);
				model.addAttribute("automationStatus", automationStatus);
				model.addAttribute("scriptlessStatus", scriptlessStatus);
				session.removeAttribute("automationProjectId");
				// session.removeAttribute("automationExecBuildId");
				return new ModelAndView("viewbuild", "model", model);
			} else
				return new ModelAndView("redirect:/500");
		}
	}

	/**
	 * This request is to execute build
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	/*
	 * @RequestMapping(value = "/executebuild") public ModelAndView
	 * executebuild(Model model, HttpSession session) throws Exception { if
	 * (session.getAttribute("userId") == null) { return new
	 * ModelAndView("redirect:/"); } else { utilsService.getLoggerUser(session);
	 * session.setAttribute("menuLiText", "Builds"); int buildId =
	 * Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString(
	 * )); int userId =
	 * Integer.parseInt(session.getAttribute("userId").toString()); String
	 * schema = session.getAttribute("cust_schema").toString();
	 * 
	 * Map<String, Object> buildExecuteData =
	 * buildDAO.getReportDataByBuildId(buildId, userId, schema);
	 * 
	 * List<Map<String, Object>> projectDetailsByBuildId =
	 * projectDao.selectProjectDetailsByBuildId( (List<Map<String, Object>>)
	 * buildExecuteData.get("#result-set-1"), (List<Map<String, Object>>)
	 * buildExecuteData.get("#result-set-10"), (List<Map<String, Object>>)
	 * buildExecuteData.get("#result-set-11"));
	 * model.addAttribute("projectsList", projectDetailsByBuildId);
	 * 
	 * model.addAttribute("projectsListing",
	 * utilsService.convertListOfMapToJson(projectDetailsByBuildId));
	 * 
	 * List<Map<String, Object>> moduleDetailsByBuildId =
	 * moduleDao.selectModuleDetailsByBuildId( (List<Map<String, Object>>)
	 * buildExecuteData.get("#result-set-2"), (List<Map<String, Object>>)
	 * buildExecuteData.get("#result-set-12"), (List<Map<String, Object>>)
	 * buildExecuteData.get("#result-set-13"));
	 * model.addAttribute("modulesList", moduleDetailsByBuildId);
	 * 
	 * List<Map<String, Object>> scenarioDetailsByBuildId =
	 * scenarioDao.selectScenarioDetailsByBuildId( (List<Map<String, Object>>)
	 * buildExecuteData.get("#result-set-3"), (List<Map<String, Object>>)
	 * buildExecuteData.get("#result-set-14"), (List<Map<String, Object>>)
	 * buildExecuteData.get("#result-set-15"));
	 * model.addAttribute("scenarioList", scenarioDetailsByBuildId);
	 * 
	 * List<Map<String, Object>> testcaseDetailsByBuildId = (List<Map<String,
	 * Object>>) buildExecuteData .get("#result-set-4");
	 * model.addAttribute("testcasesList", testcaseDetailsByBuildId);
	 * 
	 * List<Map<String, Object>> testcaseListing = (List<Map<String, Object>>)
	 * buildExecuteData .get("#result-set-4");
	 * model.addAttribute("testcasesListing",
	 * utilsService.convertListOfMapToJson(testcaseListing));
	 * 
	 * List<Map<String, Object>> executeBugs = (List<Map<String, Object>>)
	 * buildExecuteData.get("#result-set-5");
	 * 
	 * List<Map<String, Object>> testcaseStepsList = (List<Map<String, Object>>)
	 * buildExecuteData .get("#result-set-6");
	 * 
	 * model.addAttribute("testcaseStepsList", testcaseStepsList);
	 * model.addAttribute("step_bug", executeBugs);
	 * 
	 * List<Map<String, Object>> statusList = (List<Map<String, Object>>)
	 * buildExecuteData.get("#result-set-7");
	 * 
	 * model.addAttribute("statusList", statusList);
	 * 
	 * List<Map<String, Object>> tcExecDetailsList = (List<Map<String, Object>>)
	 * buildExecuteData .get("#result-set-8");
	 * model.addAttribute("tcExecDetailsList",
	 * utilsService.convertListOfMapToJson(tcExecDetailsList));
	 * model.addAttribute("exeStat", tcExecDetailsList);
	 * 
	 * List<Map<String, Object>> tcStepStatus = (List<Map<String, Object>>)
	 * buildExecuteData.get("#result-set-9"); model.addAttribute("tcStepStatus",
	 * utilsService.convertListOfMapToJson(tcStepStatus));
	 * 
	 * if (session.getAttribute("btStatus").toString().equals("1") &&
	 * bugDao.checkBTRoleAccess("createbug",
	 * session.getAttribute("roleId").toString(),
	 * session.getAttribute("cust_schema").toString())) {
	 * model.addAttribute("bugAccess", 1); } else {
	 * model.addAttribute("bugAccess", 0); }
	 * 
	 * model.addAttribute("executeBugs",
	 * utilsService.convertListOfMapToJson(executeBugs));
	 * model.addAttribute("Pass_Color",
	 * colorProperties.getProperty("testCase.Pass"));
	 * model.addAttribute("Fail_Color",
	 * colorProperties.getProperty("testCase.Fail"));
	 * model.addAttribute("Skip_Color",
	 * colorProperties.getProperty("testCase.Skip"));
	 * model.addAttribute("Block_Color",
	 * colorProperties.getProperty("testCase.blocked"));
	 * model.addAttribute("NotRun_Color",
	 * colorProperties.getProperty("testCase.NotRun"));
	 * 
	 * model.addAttribute("UserProfilePhoto", loginService.getImage((byte[])
	 * session.getAttribute("userProfilePhoto")));
	 * model.addAttribute("userName", session.getAttribute("userFullName"));
	 * model.addAttribute("role", session.getAttribute("role"));
	 * model.addAttribute("selectedExecuteBuildName",
	 * session.getAttribute("selectedExecuteBuildName").toString());
	 * 
	 * // get the attachments List<Map<String, Object>> attachmentsList =
	 * (List<Map<String, Object>>) buildExecuteData .get("#result-set-16");
	 * model.addAttribute("attachmentsList", attachmentsList);
	 * 
	 * // get build list on menuexecute List<Map<String, Object>> buildList =
	 * (List<Map<String, Object>>) buildExecuteData.get("#result-set-18");
	 * model.addAttribute("buildList", buildList);
	 * 
	 * List<Map<String, Object>> buildListing = (List<Map<String, Object>>)
	 * buildExecuteData.get("#result-set-18");
	 * model.addAttribute("buildListing",
	 * utilsService.convertListOfMapToJson(buildListing));
	 * 
	 * // get assigned user List<Map<String, Object>> assignedUser =
	 * (List<Map<String, Object>>) buildExecuteData.get("#result-set-19");
	 * model.addAttribute("assignedUser", assignedUser);
	 * 
	 * model.addAttribute("user_Id", session.getAttribute("userId").toString());
	 * 
	 * model.addAttribute("totalTestCaseCount",
	 * testcaseDetailsByBuildId.size()); return new ModelAndView("executebuild",
	 * "Model", model); } }
	 */

	@RequestMapping(value = "/executebuild")
	public ModelAndView executebuild(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Builds");
			Long relCount = 0L;
			int buildId = 0, releaseId = 0;
			String buildName = "";
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			List<Map<String, Object>> releaseCount = buildDAO.getReleaseCount(schema);
			for (int i = 0; i < releaseCount.size(); i++) {
				relCount = (Long) releaseCount.get(i).get("releaseCount");
			}
			if (relCount == 0) {
				return new ModelAndView("redirect:/createbuild");
			}

			if (session.getAttribute("selectedExecuteBuildId") == null) {
				List<Map<String, Object>> lastBuildDetails = buildDAO.getlastBuildDetails(userId, schema);
				for (int i = 0; i < lastBuildDetails.size(); i++) {
					buildId = (Integer) lastBuildDetails.get(i).get("build_id");
					buildName = (String) lastBuildDetails.get(i).get("build_name");
					releaseId = (Integer) lastBuildDetails.get(i).get("release_id");
				}
				session.setAttribute("selectedExecuteBuildId", buildId);
				session.setAttribute("selectedExecuteReleaseId", releaseId);
				session.setAttribute("BuildId", buildId);
				session.setAttribute("selectedExecuteBuildName", buildName);
			} else {
				buildId = Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());
				session.setAttribute("selectedExecuteBuildId", buildId);
				session.setAttribute("BuildId", buildId);
			}
			Map<String, Object> buildExecuteData = buildDAO.getReportDataByBuildId(buildId, userId, schema);

			List<Map<String, Object>> projectDetailsByBuildId = projectDao.selectProjectDetailsByBuildId(
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-1"),
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-10"),
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-11"));
			model.addAttribute("projectsList", projectDetailsByBuildId);

			model.addAttribute("projectsListing", utilsService.convertListOfMapToJson(projectDetailsByBuildId));

			List<Map<String, Object>> moduleDetailsByBuildId = moduleDao.selectModuleDetailsByBuildId(
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-2"),
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-12"),
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-13"));
			model.addAttribute("modulesList", moduleDetailsByBuildId);

			List<Map<String, Object>> scenarioDetailsByBuildId = scenarioDao.selectScenarioDetailsByBuildId(
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-3"),
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-14"),
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-15"));
			model.addAttribute("scenarioList", scenarioDetailsByBuildId);

			List<Map<String, Object>> testcaseDetailsByBuildId = (List<Map<String, Object>>) buildExecuteData
					.get("#result-set-4");
			model.addAttribute("testcasesList", testcaseDetailsByBuildId);

			List<Map<String, Object>> testcaseListing = (List<Map<String, Object>>) buildExecuteData
					.get("#result-set-4");
			model.addAttribute("testcasesListing", utilsService.convertListOfMapToJson(testcaseListing));

			List<Map<String, Object>> executeBugs = (List<Map<String, Object>>) buildExecuteData.get("#result-set-5");

			List<Map<String, Object>> testcaseStepsList = (List<Map<String, Object>>) buildExecuteData
					.get("#result-set-6");

			model.addAttribute("testcaseStepsList", testcaseStepsList);

			model.addAttribute("testcasesStepList1", utilsService.convertListOfMapToJson(testcaseStepsList));
			// System.out.println("testcasesStepList1:"+utilsService.convertListOfMapToJson(testcaseStepsList));
			model.addAttribute("step_bug", executeBugs);

			List<Map<String, Object>> statusList = (List<Map<String, Object>>) buildExecuteData.get("#result-set-7");
			model.addAttribute("statusList1", utilsService.convertListOfMapToJson(statusList));

			model.addAttribute("statusList", statusList);

			List<Map<String, Object>> tcExecDetailsList = (List<Map<String, Object>>) buildExecuteData
					.get("#result-set-8");
			model.addAttribute("tcExecDetailsList", utilsService.convertListOfMapToJson(tcExecDetailsList));
			model.addAttribute("exeStat", tcExecDetailsList);

			List<Map<String, Object>> tcStepStatus = (List<Map<String, Object>>) buildExecuteData.get("#result-set-9");
			model.addAttribute("tcStepStatus", utilsService.convertListOfMapToJson(tcStepStatus));
			model.addAttribute("selectedExecuteBuildName", session.getAttribute("selectedExecuteBuildName").toString());
			model.addAttribute("selectedExecuteBuildId", buildId);
			if (session.getAttribute("btStatus").toString().equals("1") && bugDao.checkBTRoleAccess("createbug",
					session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
				model.addAttribute("bugAccess", 1);
			} else {
				model.addAttribute("bugAccess", 0);
			}

			model.addAttribute("executeBugs", utilsService.convertListOfMapToJson(executeBugs));
			model.addAttribute("Pass_Color", colorProperties.getProperty("testCase.Pass"));
			model.addAttribute("Fail_Color", colorProperties.getProperty("testCase.Fail"));
			model.addAttribute("Skip_Color", colorProperties.getProperty("testCase.Skip"));
			model.addAttribute("Block_Color", colorProperties.getProperty("testCase.blocked"));
			model.addAttribute("NotRun_Color", colorProperties.getProperty("testCase.NotRun"));

			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			// get the attachments
			List<Map<String, Object>> attachmentsList = (List<Map<String, Object>>) buildExecuteData
					.get("#result-set-16");
			model.addAttribute("attachmentsList", attachmentsList);

			// get build list on menuexecute
			List<Map<String, Object>> buildList = (List<Map<String, Object>>) buildExecuteData.get("#result-set-18");
			model.addAttribute("buildList", buildList);

			List<Map<String, Object>> buildListing = (List<Map<String, Object>>) buildExecuteData.get("#result-set-18");
			if (buildListing != null) {
				model.addAttribute("buildListing", utilsService.convertListOfMapToJson(buildListing));
			} else {
				model.addAttribute("buildListing", null);
			}
			// get assigned user
			List<Map<String, Object>> assignedUser = (List<Map<String, Object>>) buildExecuteData.get("#result-set-19");
			model.addAttribute("assignedUser", assignedUser);

			model.addAttribute("user_Id", session.getAttribute("userId").toString());

			model.addAttribute("totalTestCaseCount", testcaseDetailsByBuildId.size());
			// Old code
			// List<Map<String, Object>> releases = (List<Map<String, Object>>)
			// buildExecuteData.get("#result-set-17");
			// model.addAttribute("releases", releases);
			//
			// List<Map<String, Object>> builds = (List<Map<String, Object>>)
			// buildExecuteData.get("#result-set-21");
			// model.addAttribute("builds", builds);
			List<Map<String, Object>> releases = (List<Map<String, Object>>) buildExecuteData.get("#result-set-17");
			if (releases == null || releases.size() == 0) {
				return new ModelAndView("redirect:/createbuild");
			} else {
				model.addAttribute("releases", releases);
				model.addAttribute("builds", (List<Map<String, Object>>) buildExecuteData.get("#result-set-21"));
			}

			/*
			 * List<Map<String, Object>> manualExecutionData = new
			 * ArrayList<Map<String, Object>>(); //Map<String, Object>
			 * manExeData = new HashMap<String, Object>(); //map.put("foo",
			 * "bar"); for (int projectIndex = 0; projectIndex <
			 * projectDetailsByBuildId.size(); projectIndex++) { for(int
			 * moduleIndex = 0; moduleIndex < moduleDetailsByBuildId.size();
			 * moduleIndex++) { for(int scenarioIndex = 0; scenarioIndex <
			 * scenarioDetailsByBuildId.size(); scenarioIndex++) { for(int
			 * testcaseIndex = 0; testcaseIndex <
			 * testcaseDetailsByBuildId.size(); testcaseIndex++) {
			 * if(projectDetailsByBuildId.get(projectIndex).get("project_id")==
			 * moduleDetailsByBuildId.get(moduleIndex).get("project_id")) {
			 * if(moduleDetailsByBuildId.get(moduleIndex).get("module_id")==
			 * scenarioDetailsByBuildId.get(scenarioIndex).get("module_id")) {
			 * if(scenarioDetailsByBuildId.get(scenarioIndex).get("scenario_id")
			 * ==testcaseDetailsByBuildId.get(testcaseIndex).get("scenario_id"))
			 * {
			 * //System.out.println(testcaseDetailsByBuildId.get(testcaseIndex).
			 * get("testcase_name")); Map<String, Object> manExeData = new
			 * HashMap<String, Object>(); manExeData.put("project_id",
			 * projectDetailsByBuildId.get(projectIndex).get("project_id"));
			 * manExeData.put("project_name",
			 * projectDetailsByBuildId.get(projectIndex).get("project_name"));
			 * manExeData.put("module_id",
			 * moduleDetailsByBuildId.get(moduleIndex).get("module_id"));
			 * manExeData.put("module_name",
			 * moduleDetailsByBuildId.get(moduleIndex).get("module_name"));
			 * manExeData.put("scenario_id",
			 * scenarioDetailsByBuildId.get(scenarioIndex).get("scenario_id"));
			 * manExeData.put("scenario_name",
			 * scenarioDetailsByBuildId.get(scenarioIndex).get("scenario_name"))
			 * ; manExeData.put("testcase_id",
			 * testcaseDetailsByBuildId.get(testcaseIndex).get("testcase_id"));
			 * manExeData.put("testcase_name",
			 * testcaseDetailsByBuildId.get(testcaseIndex).get("testcase_name"))
			 * ; manExeData.put("summary",
			 * testcaseDetailsByBuildId.get(testcaseIndex).get("summary"));
			 * manExeData.put("test_prefix",
			 * testcaseDetailsByBuildId.get(testcaseIndex).get("test_prefix"));
			 * manualExecutionData.add(manExeData); manExeData=null; }//end if
			 * for scenario }//end if for module }//end if for project }//end
			 * for testcaseIndex }//end for scenarioIndex }//end for moduleIndex
			 * }//end for projectIndex
			 * 
			 * 
			 * 
			 * 
			 * System.out.println("mannu:"+manualExecutionData);
			 * model.addAttribute("manTableData", manualExecutionData);
			 */

			return new ModelAndView("executebuild", "Model", model);
		}
	}

	/**
	 * @author Shailesh.Khot This request is to export Test case execute build
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/exportexecution")
	public ModelAndView exportexecution(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Builds");
			int buildId = Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			String schema = session.getAttribute("cust_schema").toString();

			Map<String, Object> buildExecuteData = buildDAO.getReportDataByBuildId(buildId, userId, schema);

			List<Map<String, Object>> projectDetailsByBuildId = projectDao.selectProjectDetailsByBuildId(
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-1"),
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-10"),
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-11"));
			model.addAttribute("projectsList", projectDetailsByBuildId);
			model.addAttribute("projectsListing", utilsService.convertListOfMapToJson(projectDetailsByBuildId));

			List<Map<String, Object>> moduleDetailsByBuildId = moduleDao.selectModuleDetailsByBuildId(
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-2"),
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-12"),
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-13"));
			model.addAttribute("modulesList", moduleDetailsByBuildId);

			List<Map<String, Object>> scenarioDetailsByBuildId = scenarioDao.selectScenarioDetailsByBuildId(
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-3"),
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-14"),
					(List<Map<String, Object>>) buildExecuteData.get("#result-set-15"));
			model.addAttribute("scenarioList", scenarioDetailsByBuildId);

			List<Map<String, Object>> testcaseDetailsByBuildId = (List<Map<String, Object>>) buildExecuteData
					.get("#result-set-4");
			model.addAttribute("testcasesList", testcaseDetailsByBuildId);

			List<Map<String, Object>> testcaseListing = (List<Map<String, Object>>) buildExecuteData
					.get("#result-set-4");
			model.addAttribute("testcasesListing", utilsService.convertListOfMapToJson(testcaseListing));

			List<Map<String, Object>> executeBugs = (List<Map<String, Object>>) buildExecuteData.get("#result-set-5");

			List<Map<String, Object>> testcaseStepsList = (List<Map<String, Object>>) buildExecuteData
					.get("#result-set-6");

			model.addAttribute("testcaseStepsList", testcaseStepsList);
			model.addAttribute("step_bug", executeBugs);

			List<Map<String, Object>> statusList = (List<Map<String, Object>>) buildExecuteData.get("#result-set-7");

			model.addAttribute("statusList", statusList);

			List<Map<String, Object>> tcExecDetailsList = (List<Map<String, Object>>) buildExecuteData
					.get("#result-set-8");
			model.addAttribute("tcExecDetailsList", utilsService.convertListOfMapToJson(tcExecDetailsList));
			model.addAttribute("exeStat", tcExecDetailsList);

			List<Map<String, Object>> tcStepStatus = (List<Map<String, Object>>) buildExecuteData.get("#result-set-9");
			model.addAttribute("tcStepStatus", utilsService.convertListOfMapToJson(tcStepStatus));

			if (session.getAttribute("btStatus").toString().equals("1") && bugDao.checkBTRoleAccess("createbug",
					session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
				model.addAttribute("bugAccess", 1);
			} else {
				model.addAttribute("bugAccess", 0);
			}

			model.addAttribute("executeBugs", utilsService.convertListOfMapToJson(executeBugs));
			model.addAttribute("Pass_Color", colorProperties.getProperty("testCase.Pass"));
			model.addAttribute("Fail_Color", colorProperties.getProperty("testCase.Fail"));
			model.addAttribute("Skip_Color", colorProperties.getProperty("testCase.Skip"));
			model.addAttribute("Block_Color", colorProperties.getProperty("testCase.blocked"));
			model.addAttribute("NotRun_Color", colorProperties.getProperty("testCase.NotRun"));

			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			model.addAttribute("selectedExecuteBuildName", session.getAttribute("selectedExecuteBuildName").toString());

			// get the attachments
			List<Map<String, Object>> attachmentsList = (List<Map<String, Object>>) buildExecuteData
					.get("#result-set-16");
			model.addAttribute("attachmentsList", attachmentsList);

			// get build list on menuexecute
			List<Map<String, Object>> buildList = (List<Map<String, Object>>) buildExecuteData.get("#result-set-18");
			model.addAttribute("buildList", buildList);

			List<Map<String, Object>> buildListing = (List<Map<String, Object>>) buildExecuteData.get("#result-set-18");
			model.addAttribute("buildListing", utilsService.convertListOfMapToJson(buildListing));

			// get assigned user
			List<Map<String, Object>> assignedUser = (List<Map<String, Object>>) buildExecuteData.get("#result-set-19");
			model.addAttribute("assignedUser", assignedUser);

			List<Map<String, Object>> statusUser = (List<Map<String, Object>>) buildExecuteData.get("#result-set-20");
			model.addAttribute("statusUser", statusUser);

			model.addAttribute("totalTestCaseCount", testcaseDetailsByBuildId.size());
			return new ModelAndView("exportexecution", "Model", model);
		}
	}

	/**
	 * @author suresh.adling
	 * @method insertBug POST Request - create bug in build execution for failed
	 *         step
	 * @param projectId
	 * @param moduleId
	 * @param summary
	 * @param bugDescription
	 * @param uploadForm
	 * @param tcId
	 * @param tcStatus
	 * @param notes
	 * @param stepId
	 * @param stepStatus
	 * @param stepComment
	 * @param testCaseInsertMethod
	 * @param stepInsertMethod
	 * @param session
	 * @return view - executebuild
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/raisebug", method = RequestMethod.POST)
	public ModelAndView insertBug(@RequestParam("projectId") String projectId,
			@RequestParam("moduleId") String moduleId, @RequestParam("bugSummary") String summary,
			@RequestParam("bugDescription") String bugDescription,
			@ModelAttribute("uploadForm") FileUploadForm uploadForm, @RequestParam("testcaseId") String tcId,
			@RequestParam("testcaseStatusId") String tcStatus, @RequestParam("testcaseNotes") String notes,
			@RequestParam("testStepId") int stepId, @RequestParam("stepStatusId") int stepStatus,
			@RequestParam("testStepComment") String stepComment,
			@RequestParam("testCaseInsertMethod") String testCaseInsertMethod,
			@RequestParam("stepInsertMethod") String stepInsertMethod, HttpSession session) throws Exception {
		logger.info("In post request to insert a build");
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDao.checkBTRoleAccess("createbug", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			String dtJira = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
			;
			String schema = session.getAttribute("cust_schema").toString();
			String userId = session.getAttribute("userId").toString();
			int buildId = Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());
			List<Map<String, Object>> dataForInsertBug = bugService.insertBug(projectId, moduleId, summary, "1", "1",
					"1", "1", "0", "3", userId, bugDescription, schema, "-1", null,
					configurationProperties.getProperty("XENON_REPOSITORY_PATH"), "", dtJira, "", "", dtJira, "");

			int bugId = (Integer) dataForInsertBug.get(0).get("bug_id");
			String bugPrefix = (String) dataForInsertBug.get(0).get("bug_prefix");
			if (bugId != 0) {
				session.setAttribute("selectedBugId", bugId);
				List<MultipartFile> files = uploadForm.getFiles();
				List<String> fileNames = new ArrayList<String>();
				String uploadDirectoryPath = configurationProperties.getProperty("XENON_REPOSITORY_PATH") + "repository"
						+ File.separator + "upload";
				if (null != files && files.size() > 0) {
					for (MultipartFile multipartFile : files) {

						String fileName = multipartFile.getOriginalFilename();
						if (!fileName.isEmpty()) {
							fileNames.add(fileName);
							byte[] bytes = multipartFile.getBytes();
							org.springframework.core.io.Resource resource = new ClassPathResource(uploadDirectoryPath);
							String filePath = resource.getFile().getAbsolutePath() + File.separator + "bugtracker"
									+ File.separator + fileName;
							String title = multipartFile.getOriginalFilename();
							File fileToWriteTo = new File(filePath);
							FileUtils.writeByteArrayToFile(fileToWriteTo, bytes);
							bugDao.insertAttachment(bugId, bytes, title, schema);
							fileToWriteTo.delete();
						}
					}
				}
				String buildName = session.getAttribute("selectedExecuteBuildName").toString();
				// bugDao.addCreateBugActivity(bugId, Integer.parseInt(userId),
				// summary, schema);
				session.setAttribute("currentTcExecId", tcId);
				int testCaseInsertStatus = 0, stepInsertStatus = 0;
				if (testCaseInsertMethod.equals("update")) {
					testCaseInsertStatus = 2;
				} else if (testCaseInsertMethod.equals("insert")) {
					testCaseInsertStatus = 1;
				}
				if (stepInsertMethod.equals("update")) {
					stepInsertStatus = 2;
				} else if (stepInsertMethod.equals("insert")) {
					stepInsertStatus = 1;
				}
				byte[] bytes;
				bytes = new byte[0];
				int flag = buildDAO.createBugOnExecution(Integer.parseInt(tcId), notes, Integer.parseInt(tcStatus),
						buildId, stepId, stepStatus, stepComment, testCaseInsertStatus, stepInsertStatus, // stepInsertStatus,
						Integer.parseInt(userId), bugId, bytes, schema);
				if (flag == 1) {
					logger.error("User is redirected to executebuild page");
					return new ModelAndView("redirect:/executebuild");
				} else {
					logger.error("Something went wrong, user is redirected to 500 error page");
					return new ModelAndView("redirect:/500");
				}
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return new ModelAndView("redirect:/500");
			}

		} else
			logger.error("User does not have access to createbug service, user is redirected to 500 error page");
		return new ModelAndView("500");
	}

	/**
	 * @author shailesh.khot
	 * @method insertBug POST Request - create bug in build execution for failed
	 *         step
	 * @param projectId
	 * @param moduleId
	 * @param summary
	 * @param bugDescription
	 * @param uploadForm
	 * @param tcId
	 * @param tcStatus
	 * @param notes
	 * @param stepId
	 * @param stepStatus
	 * @param stepComment
	 * @param testCaseInsertMethod
	 * @param stepInsertMethod
	 * @param session
	 * @return view - executebuild
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/raisebugfortc", method = RequestMethod.POST)
	public @ResponseBody String insertBug(@RequestParam("projectId") String projectId,
			@RequestParam("moduleId") String moduleId, @RequestParam("bugSummary") String summary,
			@RequestParam("bugDescription") String bugDescription, @RequestParam("testcaseId") String tcId,
			@RequestParam("testcaseStatusId") String tcStatus, @RequestParam("testcaseNotes") String notes,
			@RequestParam("testStepId") int stepId, @RequestParam("stepStatusId") int stepStatus,
			@RequestParam("testStepComment") String stepComment,
			@RequestParam("testCaseInsertMethod") String testCaseInsertMethod,
			@RequestParam("stepInsertMethod") String stepInsertMethod,
			@RequestParam(value = "uploadForm", required = false) CommonsMultipartFile uploadForm, HttpSession session)
			throws Exception {
		logger.info("In post request to insert a build");
		logger.info("Checking for active session");
		if (bugDao.checkBTRoleAccess("createbug", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			String dtJira = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
			;
			String schema = session.getAttribute("cust_schema").toString();
			String userId = session.getAttribute("userId").toString();
			int buildId = Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());

			String proId = "";
			String modId = "";
			int tid = Integer.parseInt(tcId);
			List<Map<String, Object>> tcData = buildDAO.getTestcaseData(tid, schema);
			for (int i = 0; i < tcData.size(); i++) {
				// = (Long) releaseCount.get(i).get("releaseCount");
				proId = String.valueOf(tcData.get(i).get("projectid"));
				modId = String.valueOf(tcData.get(i).get("module_id"));

			}

			/*
			 * List<Map<String, Object>> dataForInsertBug =
			 * bugService.insertBug(projectId, moduleId, summary, "1", "1", "1",
			 * "1", "0", "3", userId, bugDescription, schema, "-1", null,
			 * configurationProperties.getProperty("XENON_REPOSITORY_PATH"), "",
			 * dtJira, "", "", dtJira, "");
			 */

			List<Map<String, Object>> dataForInsertBug = bugService.insertBug(proId, modId, summary, "1", "1", "1", "1",
					"0", "3", userId, bugDescription, schema, "-1", null,
					configurationProperties.getProperty("XENON_REPOSITORY_PATH"), "", dtJira, "", "", dtJira, "");

			int bugId = (Integer) dataForInsertBug.get(0).get("bug_id");
			String bugPrefix = (String) dataForInsertBug.get(0).get("bug_prefix");
			if (bugId != 0) {
				String buildName = session.getAttribute("selectedExecuteBuildName").toString();
				// bugDao.addCreateBugActivity(bugId, Integer.parseInt(userId),
				// summary, schema);
				session.setAttribute("currentTcExecId", tcId);
				if (null != uploadForm) {
					byte[] filebytes = uploadForm.getBytes();
					String title = uploadForm.getFileItem().getName();
					bugDao.insertAttachment(bugId, filebytes, title, schema);
				}

				int testCaseInsertStatus = 0, stepInsertStatus = 0;
				if (testCaseInsertMethod.equals("update")) {
					testCaseInsertStatus = 2;
				} else if (testCaseInsertMethod.equals("insert")) {
					testCaseInsertStatus = 1;
				}
				if (stepInsertMethod.equals("update")) {
					stepInsertStatus = 2;
				} else if (stepInsertMethod.equals("insert")) {
					stepInsertStatus = 1;
				}

				byte[] bytes;
				bytes = new byte[0];
				int flag = buildDAO.createBugOnExecution(Integer.parseInt(tcId), notes, Integer.parseInt(tcStatus),
						buildId, stepId, stepStatus, stepComment, testCaseInsertStatus, stepInsertStatus, // stepInsertStatus,
						Integer.parseInt(userId), bugId, bytes, schema);
				if (flag == 1) {
					logger.error("User is redirected to executebuild page");
					// return new ModelAndView("redirect:/executebuild");
					return bugPrefix;
				} else {
					logger.error("Something went wrong, user is redirected to 500 error page");
					// return new ModelAndView("redirect:/500");
					return "error";
				}
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				// return new ModelAndView("redirect:/500");
				return "error";
			}

		} else
			logger.error("User does not have access to createbug service, user is redirected to 500 error page");
		// return new ModelAndView("500");
		return "error";
	}

	/**
	 * This request is to set build id session
	 * 
	 * @param model
	 * @param session
	 * @param buildID
	 * @param viewType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setbuildid", method = RequestMethod.POST)
	public ModelAndView setbuildID(Model model, HttpSession session, @RequestParam("buildID") String buildID,
			@RequestParam("viewType") String viewType) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("selectedBuildID", buildID);
		session.setAttribute("selectedBuildViewType", viewType);
		session.removeAttribute("selectedAutoBuildID");
		return viewBuild(1, 1, 1, "manual", model, session);
	}

	/**
	 * This request is used to set automation build id
	 * 
	 * @param model
	 * @param session
	 * @param buildID
	 * @param viewType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setautobuildid", method = RequestMethod.POST)
	public ModelAndView setAutoBuildId(Model model, HttpSession session, @RequestParam("buildID") String buildID,
			@RequestParam("viewType") String viewType) throws Exception {
		utilsService.getLoggerUser(session);
		session.removeAttribute("selectedBuildID");
		session.setAttribute("selectedAutoBuildID", buildID);
		session.setAttribute("selectedBuildViewType", viewType);
		return viewBuild(1, 1, 1, "manual", model, session);
	}

	/**
	 * This request is to insert step execution summary
	 * 
	 * @param stepId
	 * @param stepStatus
	 * @param stepComment
	 * @param bugRaised
	 * @param stepInsertMethod
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertStepExecSummary", method = RequestMethod.POST)
	public @ResponseBody int insertStepExecSummary(@RequestParam("stepId") int stepId,
			@RequestParam("stepStatus") int stepStatus, @RequestParam("stepComment") String stepComment,
			@RequestParam("bugRaised") String bugRaised, @RequestParam("stepInsertMethod") String stepInsertMethod,
			Model model, HttpSession session

	) throws Exception {
		utilsService.getLoggerUser(session);
		int buildId = Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());
		int tcId = Integer.parseInt(session.getAttribute("currentTcExecId").toString());
		String schema = session.getAttribute("cust_schema").toString();
		int result = buildService.insertStepExecSummary(buildId, tcId, schema, stepId, stepComment, stepStatus,
				bugRaised, stepInsertMethod, testcaseStepDao, buildDAO);
		return result;
	}

	@RequestMapping(value = "/getAutoStepExecSummary", method = RequestMethod.POST)
	public @ResponseBody List<Map<String, Object>> getAutoStepExecSummary(@RequestParam Integer buildId,@RequestParam Integer testCaseId,
			Model model, HttpSession session
	) throws Exception {
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		List<Map<String, Object>> result = buildService.getAutoStepExecSummary(buildId, testCaseId, schema);
//		System.out.println(buildId +" - "+testCaseId );
		return result;
	}
	
	/**
	 * This request is to insert test case execution summary
	 * 
	 * @param tcId
	 * @param tcStatus
	 * @param notes
	 * @param completeStatus
	 * @param testCaseInsertMethod
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertTcExecSummary", method = RequestMethod.POST)
	public @ResponseBody int insertTcExecSummary(@RequestParam("tcId") String tcId,
			@RequestParam("tcStatus") String tcStatus, @RequestParam("notes") String notes,
			@RequestParam("completeStatus") String completeStatus,
			@RequestParam("testCaseInsertMethod") String testCaseInsertMethod, Model model, HttpSession session)
			throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("currentTcExecId", tcId);
		int buildId = Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());
		String schema = session.getAttribute("cust_schema").toString();
		int methodType = 0;
		if (testCaseInsertMethod.equals("update")) {
			methodType = 1;
		} else if (testCaseInsertMethod.equals("insert")) {
			methodType = 2;
		}
		Map<String, Object> insertTcData = buildDAO.insertTestcaseSummary(Integer.parseInt(tcId), buildId, methodType,
				Integer.parseInt(session.getAttribute("userId").toString()), Integer.parseInt(tcStatus), notes,
				Integer.parseInt(completeStatus), session.getAttribute("userFullName").toString(),
				Integer.parseInt(session.getAttribute("userId").toString()), schema);
		List<Map<String, Object>> buildStatus = (List<Map<String, Object>>) insertTcData.get("#result-set-1");

		if (Integer.parseInt(buildStatus.get(0).get("build_status").toString()) == 1) {
			List<Map<String, Object>> totalTcCount = (List<Map<String, Object>>) insertTcData.get("#result-set-2");
			List<Map<String, Object>> executedTcCount = (List<Map<String, Object>>) insertTcData.get("#result-set-3");
			int status = Integer.parseInt(insertTcData.get("#update-count-1").toString());

			if (Integer.parseInt(totalTcCount.get(0).get("TotalTcCount").toString()) == Integer
					.parseInt(executedTcCount.get(0).get("ExecutedTcCount").toString()))
				buildDAO.sendMailOnManualBuildExecutionComplete(buildId, schema);

			return status;
		} else
			return -1;

	}

	/**
	 * Shailesh khot code for insert test case execution summary Multiple
	 */
	@RequestMapping(value = "/insertTcExecSummaryMultiple", method = RequestMethod.POST)
	public @ResponseBody int insertTcExecSummary(String execData, Model model, HttpSession session) {
		try {
			JSONArray arr = new JSONArray(execData);
			int methodType = 0;
			int count = 0;
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();

			for (int i = 0; i < arr.length(); i++) {
				if (arr.getJSONObject(i).getString("typeMethod").toString().equals("update")) {
					methodType = 1;
				} else if (arr.getJSONObject(i).getString("typeMethod").toString().equals("insert")) {
					methodType = 2;
				}
				System.out.println("operation:"+methodType); 
				Map<String, Object> insertTcData = buildDAO.insertTestcaseSummary(
						Integer.parseInt(arr.getJSONObject(i).getString("tcId")),
						Integer.parseInt(arr.getJSONObject(i).getString("buildId")), methodType,
						Integer.parseInt(arr.getJSONObject(i).getString("testerId")),
						Integer.parseInt(arr.getJSONObject(i).getString("statusId")),
						arr.getJSONObject(i).getString("notes").toString(),
						Integer.parseInt(arr.getJSONObject(i).getString("completeStatus")),
						session.getAttribute("userFullName").toString(),
						Integer.parseInt(session.getAttribute("userId").toString()), schema);

				count = count + Integer.parseInt(insertTcData.get("#update-count-1").toString());

			} // end loop
			if (count > 0) {
				return 1;
			} else {
				return -1;
			}
		} catch (Exception e) {
			return -1;
		}

	}// end Method

	/**
	 * Shailesh khot code for Dump
	 */
	@RequestMapping(value = "/dump", method = RequestMethod.POST)
	public @ResponseBody int dump(String execData, Model model, HttpSession session) throws Exception {

		JSONArray arr = new JSONArray(execData);
		int count = 0;
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();

		for (int i = 0; i < arr.length(); i++) {
			/*
			 * if(arr.getJSONObject(i).getString("typeMethod").toString().equals
			 * ("update")) { methodType=1; }else
			 * if(arr.getJSONObject(i).getString("typeMethod").toString().equals
			 * ("insert")) { methodType=2; }
			 */
			Map<String, Object> insertTcData = buildDAO.insertTestcaseSummary(
					Integer.parseInt(arr.getJSONObject(i).getString("tcId")),
					Integer.parseInt(arr.getJSONObject(i).getString("buildId")), 2,
					Integer.parseInt(arr.getJSONObject(i).getString("testerId")),
					Integer.parseInt(arr.getJSONObject(i).getString("statusId")),
					arr.getJSONObject(i).getString("notes").toString(),
					Integer.parseInt(arr.getJSONObject(i).getString("completeStatus")),
					session.getAttribute("userFullName").toString(),
					Integer.parseInt(session.getAttribute("userId").toString()), schema);

			count = count + Integer.parseInt(insertTcData.get("#update-count-1").toString());

		} // end loop
		if (count > 0) {
			return 1;
		} else {
			return -1;
		}
	}// end Method

	/**
	 * This request is to assign build
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/assignbuild")
	public ModelAndView assignbuild(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Builds");
			String schema = session.getAttribute("cust_schema").toString();
			int buildId = Integer.parseInt(session.getAttribute("assignBuildId").toString());
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			List<Map<String, Object>> projectDetailsByBuildId = projectDao.selectProjectsByBuildId(buildId, userId,
					schema);

			List<Map<String, Object>> moduleDetailByBuildId = projectDao.selectModuleDetail(buildId, userId, schema);
			List<Map<String, Object>> userDetail;
			if (buildDAO.checkBMRoleAccess("assigntoall", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				userDetail = projectDao.selectUserDetail(buildId, schema);
			} else {
				userDetail = projectDao.selectUserDetailByUser(buildId, userId, schema);
			}

			List<Map<String, Object>> singleUserDetail = projectDao.selectUserDetail(schema);
			List<Map<String, Object>> assigneeUserList = buildDAO.getModuleWiseAssignee(buildId, schema);
			model.addAttribute("assigneeUserList", assigneeUserList);

			for (int count1 = 0; count1 < moduleDetailByBuildId.size(); count1++) {
				moduleDetailByBuildId.get(count1).put("assignFlag", 0);
				for (int count2 = 0; count2 < assigneeUserList.size(); count2++) {
					if (Integer.parseInt(moduleDetailByBuildId.get(count1).get("module_id").toString()) == Integer
							.parseInt(assigneeUserList.get(count2).get("module_id").toString())) {
						moduleDetailByBuildId.get(count1).put("assignFlag", 1);
						break;
					}

				}
			}

			for (int i = 0; i < projectDetailsByBuildId.size(); i++) {
				projectDetailsByBuildId.get(i).put("project_createdate", utilsService.getDateTimeOfTimezone(
						session.getAttribute("userTimezone").toString(),
						utilsService.getDate(projectDetailsByBuildId.get(i).get("project_createdate").toString())));
			}

			model.addAttribute("userAssignmentStatus", moduleDetailByBuildId.size());
			model.addAttribute("projectDetails", projectDetailsByBuildId);
			model.addAttribute("moduleDetails", moduleDetailByBuildId);
			model.addAttribute("userDetail", userDetail);
			model.addAttribute("singleUser", singleUserDetail);
			model.addAttribute("addremovetestcaseBuildName", session.getAttribute("assignBuildName").toString());
			return new ModelAndView("assignbuild", "Model", model);
		}
	}

	/**
	 * This request is to set execute build id
	 * 
	 * @param model
	 * @param session
	 * @param buildID
	 * @param buildName
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setexecutebuildid", method = RequestMethod.POST)
	public ModelAndView setexecutebuildid(Model model, HttpSession session, @RequestParam("buildID") String buildID,
			@RequestParam("buildName") String buildName) throws Exception {
		utilsService.getLoggerUser(session);
		session.setAttribute("selectedExecuteBuildId", buildID);
		session.setAttribute("selectedExecuteBuildName", buildName);
		return new ModelAndView("redirect:/");
	}

	/**
	 * This request is to view execute build list
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/executebuildlist")
	public ModelAndView executebuildlist(@RequestParam(value = "manual", defaultValue = "1") int manual,
			@RequestParam(value = "auto", defaultValue = "1") int auto, Model model, HttpSession session)
			throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Builds");
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			// List<Map<String, Object>> allBuildDetails =
			// buildDAO.getAllBuildDetailsToExecute(userId, schema);
			List<Map<String, Object>> buildAssignee = new ArrayList<Map<String, Object>>();
			int pageSize = 10;
			model.addAttribute("pageSize", pageSize);

			int manualStartValue = (manual - 1) * pageSize;
			int autoStartValue = (auto - 1) * pageSize;
			Map<String, Object> buildAssigneeList = buildDAO.getExecuteBuildList(userId, manualStartValue,
					autoStartValue, pageSize, schema);

			int manualBuildCount = (Integer) buildAssigneeList.get("manualbuildcount");
			model.addAttribute("manualBuildCount", manualBuildCount);

			int autobuildcount = (Integer) buildAssigneeList.get("autobuildcount");
			model.addAttribute("autobuildcount", autobuildcount);

			/*
			 * int scbuildcount = (Integer)
			 * buildAssigneeList.get("scbuildcount");
			 * model.addAttribute("scbuildcount", scbuildcount);
			 */

			List<Map<String, Object>> allBuildDetails = (List<Map<String, Object>>) buildAssigneeList
					.get("#result-set-1");

			/* Get data for automation build */
			List<Map<String, Object>> allAutomationBuilds = (List<Map<String, Object>>) buildAssigneeList
					.get("#result-set-2");
			for (int i = 0; i < allAutomationBuilds.size(); i++) {
				String photo = loginService.getImage((byte[]) allAutomationBuilds.get(i).get("user_photo"));
				allAutomationBuilds.get(i).put("photo", photo);
				allAutomationBuilds.get(i).put("buildCreateDate",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(allAutomationBuilds.get(i).get("build_createdate").toString())));
			}
			model.addAttribute("allAutomationBuilds", allAutomationBuilds);
			model.addAttribute("automationStatus",
					Integer.parseInt(session.getAttribute("automationStatus").toString()));

			List<Map<String, Object>> allAssigneeList = (List<Map<String, Object>>) buildAssigneeList
					.get("#result-set-2");

			int counter = 3;
			for (int j = 0; j < allBuildDetails.size(); j++) {
				String resultSet = "#result-set-" + counter;
				List<Map<String, Object>> assigneeList = (List<Map<String, Object>>) buildAssigneeList.get(resultSet);
				counter++;
				for (int i = 0; i < assigneeList.size(); i++) {
					buildAssignee.add(assigneeList.get(i));
				}
			}
			for (int i = 0; i < allBuildDetails.size(); i++) {
				String photo = loginService.getImage((byte[]) allBuildDetails.get(i).get("user_photo"));
				allBuildDetails.get(i).put("photo", photo);
				allBuildDetails.get(i).put("build_createdate",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(allBuildDetails.get(i).get("build_createdate").toString())));
			}
			model.addAttribute("buildAssignee", buildAssignee);
			model.addAttribute("allBuildDetails", allBuildDetails);
			return new ModelAndView("executebuildlist", "Model", model);
		}
	}

	/**
	 * This request is to add code
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addcode")
	public ModelAndView addcode(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Builds");
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			return new ModelAndView("codeexe", "model", model);
		}
	}

	/**
	 * This request is to set build assign
	 * 
	 * @param model
	 * @param session
	 * @param projectID
	 * @param moduleID
	 * @param userID
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setbuildassign", method = RequestMethod.POST)
	public ModelAndView setbuildassign(Model model, HttpSession session, @RequestParam("projectID") int projectID,
			@RequestParam("moduleID") int moduleID, @RequestParam("userID") int userID) throws Exception {
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		int buildId = Integer.parseInt(session.getAttribute("assignBuildId").toString());
		/*
		 * Map<String, Object> assignData =
		 * projectDao.assignModuleToExecute(buildId, projectID, moduleID,
		 * userID, Integer.parseInt(session.getAttribute("userId").toString()),
		 * schema);
		 * 
		 * List<Map<String, Object>> assignedModules = (List<Map<String,
		 * Object>>) assignData.get("#result-set-1"); List<Map<String, Object>>
		 * addedModules = (List<Map<String, Object>>)
		 * assignData.get("#result-set-2"); if (assignedModules.size() ==
		 * addedModules.size()) { buildDAO.updateBuildStatus(3, buildId,
		 * schema); }
		 */
		int userIdSessionValue = Integer.parseInt(session.getAttribute("userId").toString());
		buildService.setbuildassign(schema, buildId, projectID, moduleID, userID, userIdSessionValue);

		return viewBuild(1, 1, 1, "manual", model, session);
	}

	/**
	 * This request is to reset build assign
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/resetbuildassign", method = RequestMethod.POST)
	public ModelAndView resetbuildassign(Model model, HttpSession session) throws Exception {
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		int buildId = Integer.parseInt(session.getAttribute("assignBuildId").toString());
		projectDao.removeBuildAssignee(buildId, schema);
		return viewBuild(1, 1, 1, "manual", model, session);
	}

	/**
	 * This request is to check assigned modules
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkassignedmodules", method = RequestMethod.POST)
	public ModelAndView checkAllAssignedModules(Model model, HttpSession session) throws Exception {
		utilsService.getLoggerUser(session);
		return viewBuild(1, 1, 1, "manual", model, session);
	}

	/**
	 * This request is to view automation builds
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/viewautomationbuild")
	public ModelAndView viewautomationbuild(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Reports");
			String schema = session.getAttribute("cust_schema").toString();
			List<Map<String, Object>> allBuildDetails = buildDAO1.getAllBuildDetails(schema);
			model.addAttribute("allBuildDetails", allBuildDetails);
			return new ModelAndView("viewautomationbuild", "model", model);

		}
	}

	/**
	 * This request is to clone build
	 * 
	 * @param buildName
	 * @param buildDescription
	 * @param releaseId
	 * @param bldStatus
	 * @param sourceBuildID
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/clonebuild", method = RequestMethod.POST)
	public ModelAndView cloneBuild(@RequestParam("buildName") String buildName,
			@RequestParam("buildDescription") String buildDescription, @RequestParam("buildReleaseId") String releaseId,
			@RequestParam("buildStatus") String bldStatus, @RequestParam("sourceBuildID") int sourceBuildID,
			Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			if (buildDAO.checkBMRoleAccess("createbuild", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				String schema = session.getAttribute("cust_schema").toString();
				int buildReleaseId = Integer.parseInt(releaseId);
				int buildStatus = Integer.parseInt(bldStatus);
				int buildCreatorID = Integer.parseInt(session.getAttribute("userId").toString());
				BuildDetails insertBuild = new BuildDetails();
				insertBuild.setBuildName(buildName.trim());
				insertBuild.setBuildDesc(buildDescription.trim());
				insertBuild.setBuildReleaseID(buildReleaseId);
				insertBuild.setBuildStatus(buildStatus);
				insertBuild.setBuildCreatorID(buildCreatorID);
				int flag = buildDAO.cloneBuild(insertBuild, schema, sourceBuildID);
				if (flag == 1)
					return new ModelAndView("redirect:/viewbuild");
				else {
					model.addAttribute("error", 1);
					session.setAttribute("duplicateBuildError", 1);
					return createBuild(model, session);
				}
			}
			return new ModelAndView("redirect:/500");
		}
	}

	/**
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/autoexe")
	public ModelAndView autoexe(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);

			if (buildDAO.checkBMRoleAccess("autoexe_build", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {

				session.setAttribute("menuLiText", "Builds");
				model.addAttribute("UserProfilePhoto",
						loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
				model.addAttribute("userName", session.getAttribute("userFullName"));
				model.addAttribute("role", session.getAttribute("role"));
				String schema = session.getAttribute("cust_schema").toString();
				List<Map<String, Object>> testCases = exebuild.getAutotestcases(schema);
				model.addAttribute("autoTest", testCases);
				return new ModelAndView("autoexe", "Model", model);
			} else
				return new ModelAndView("redirect:/500");

		}
	}

	/**
	 * This request is to execute automation build
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/buildexecute")
	public ModelAndView buildexecute(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to build project and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			// session.setAttribute("automationProjectId",0);
			utilsService.getLoggerUser(session);
			logger.info("Checking user's access to build project service");
			if (buildDAO.checkBMRoleAccess("buildproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to build project");
				String schema = session.getAttribute("cust_schema").toString();
				session.setAttribute("menuLiText", "Builds");
				int userId = Integer.parseInt(session.getAttribute("userId").toString());
				int buildId = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());
				int customerId = Integer.parseInt(session.getAttribute("customerId").toString());

				model.addAttribute("automationExecBuildName", session.getAttribute("automationExecBuildName"));
				session.setAttribute("BuildId", buildId);
				int testRemoveStatus = Integer.parseInt(session.getAttribute("UsertestRemoveStatus").toString());
				model.addAttribute("removeStatus", testRemoveStatus);
				if (!utilsService.checkSessionContainsAttribute("automationProjectId", session))
					session.setAttribute("automationProjectId", 0);

				List<Map<String, Object>> buildData = buildDAO.getBuildAddedTcCount(userId, buildId, schema);
				int addedTcCount = Integer.parseInt((buildData).get(0).get("TcCount").toString());
				String execType = buildData.get(0).get("execution_type").toString();
				session.setAttribute("redwoodBuildId", buildData.get(0).get("redwood_build_id") );
				/*if(execType.equals("2")) {
					execType=execType+",3";
				}
				if(execType.equals("3")) {
					execType=execType+",2";
				}*/
				
				if (addedTcCount > 0)
					session.setAttribute("automationProjectId", buildData.get(0).get("project_id"));
				Map<String, Object> automationBuildData = buildDAO.getAutomationBuildData(customerId, userId, buildId,
						Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString()),execType, schema);
				addedTcCount = Integer.parseInt(((List<Map<String, Object>>) automationBuildData.get("#result-set-1"))
						.get(0).get("TcCount").toString());
				model.addAttribute("addedTcCount", addedTcCount);
				model.addAttribute("automationProjectId",
						Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString()));
				List<Map<String, Object>> selectProjectsList = (List<Map<String, Object>>) automationBuildData
						.get("#result-set-2");
				model.addAttribute("selectProjectsList", selectProjectsList);
				model.addAttribute("projectsList", new ArrayList<Map<String, Object>>());
				model.addAttribute("modulesList", new ArrayList<Map<String, Object>>());
				model.addAttribute("scenarioList", new ArrayList<Map<String, Object>>());
				model.addAttribute("testcasesList", new ArrayList<Map<String, Object>>());
				List<Map<String, Object>> getAllBuild;
				List<Map<String, Object>> vmList = new ArrayList<Map<String, Object>>();
				List<Map<String, Object>> mailGroupList = new ArrayList<Map<String, Object>>();
				int onPremiseStatus = Integer.parseInt(session.getAttribute("customerOnPremiseStatus").toString());
				if (Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString()) > 0 || addedTcCount != 0) {
					getAllBuild = (List<Map<String, Object>>) automationBuildData.get("#result-set-3");
					vmList = (List<Map<String, Object>>) automationBuildData.get("#result-set-4");
					mailGroupList = (List<Map<String, Object>>) automationBuildData.get("#result-set-5");
					List<Map<String, Object>> projectsList = new ArrayList<Map<String, Object>>();
					List<Map<String, Object>> modulesList = new ArrayList<Map<String, Object>>();
					List<Map<String, Object>> scenarioList = new ArrayList<Map<String, Object>>();
					List<Map<String, Object>> testCaseList = new ArrayList<Map<String, Object>>();
					for (int i = 0; i < getAllBuild.size(); i++) {
						Map<String, Object> projectsListTemp = new HashMap<String, Object>();
						projectsListTemp.put("project_id", getAllBuild.get(i).get("project_id"));
						projectsListTemp.put("project_name", getAllBuild.get(i).get("project_name"));
						projectsListTemp.put("project_description", getAllBuild.get(i).get("project_description"));
						projectsListTemp.put("project_active", getAllBuild.get(i).get("project_active"));
						projectsList.add(projectsListTemp);

						Map<String, Object> modulesListTemp = new HashMap<String, Object>();
						if (!(getAllBuild.get(i).get("module_id") == null)) {
							modulesListTemp.put("project_id", getAllBuild.get(i).get("project_id"));
							modulesListTemp.put("project_name", getAllBuild.get(i).get("project_name"));
							modulesListTemp.put("module_id", getAllBuild.get(i).get("module_id"));
							modulesListTemp.put("module_name", getAllBuild.get(i).get("module_name"));
							modulesListTemp.put("module_description", getAllBuild.get(i).get("module_description"));
							modulesList.add(modulesListTemp);
						}
						Map<String, Object> scenarioListTemp = new HashMap<String, Object>();
						if (!(getAllBuild.get(i).get("scenario_id") == null)) {
							scenarioListTemp.put("module_id", getAllBuild.get(i).get("module_id"));
							scenarioListTemp.put("module_name", getAllBuild.get(i).get("module_name"));
							scenarioListTemp.put("scenario_id", getAllBuild.get(i).get("scenario_id"));
							scenarioListTemp.put("scenario_name", getAllBuild.get(i).get("scenario_name"));
							scenarioListTemp.put("scenario_description",
									getAllBuild.get(i).get("scenario_description"));
							scenarioList.add(scenarioListTemp);
						}

						Map<String, Object> testCaseListTemp = new HashMap<String, Object>();
						if (!(getAllBuild.get(i).get("testcase_id") == null)) {
							testCaseListTemp.put("datasheetTitle", getAllBuild.get(i).get("datasheetTitle"));
							testCaseListTemp.put("tc_id", getAllBuild.get(i).get("tc_id"));
							testCaseListTemp.put("excel_file_present_status",
									getAllBuild.get(i).get("excel_file_present_status"));
							testCaseListTemp.put("testcase_id", getAllBuild.get(i).get("testcase_id"));
							testCaseListTemp.put("testcase_name", getAllBuild.get(i).get("testcase_name"));
							testCaseListTemp.put("scenario_id", getAllBuild.get(i).get("scenario_id"));
							testCaseListTemp.put("test_complete", getAllBuild.get(i).get("test_complete"));
							testCaseListTemp.put("test_executed", getAllBuild.get(i).get("test_executed"));
							testCaseListTemp.put("projectid", getAllBuild.get(i).get("projectid"));
							testCaseListTemp.put("mdl_id", getAllBuild.get(i).get("mdl_id"));
							testCaseListTemp.put("sce_id", getAllBuild.get(i).get("sce_id"));
							testCaseListTemp.put("test_prefix", getAllBuild.get(i).get("test_prefix"));
							if( execType.contains( String.valueOf( ExecutionType.REDWOOD.getValue() ) ) )
								testCaseListTemp.put("redwood_tc_id", getAllBuild.get(i).get("redwood_tc_id"));
							if (getAllBuild.get(i).get("test_complete") == null) {
								testCaseListTemp.put("testStatus", 0);
							} else if (getAllBuild.get(i).get("test_complete") != null
									&& getAllBuild.get(i).get("test_executed") != null) {
								testCaseListTemp.put("testStatus", 1);
							} else {
								testCaseListTemp.put("testStatus", 2);
							}
							testCaseList.add(testCaseListTemp);
						}
					}
					model.addAttribute("testCasesAdded", 0);
					testCaseList = utilsService.removeDuplicate(testCaseList);
					for (int counter = 0; counter < testCaseList.size(); counter++) {
						if (Integer.parseInt(testCaseList.get(counter).get("testStatus").toString()) == 2) {
							model.addAttribute("testCasesAdded", 1);
							break;
						}

					}
					projectsList = utilsService.getTestCount(testCaseList, utilsService.removeDuplicate(projectsList),
							"project");
					modulesList = utilsService.getTestCount(testCaseList, utilsService.removeDuplicate(modulesList),
							"module");
					scenarioList = utilsService.getTestCount(testCaseList, utilsService.removeDuplicate(scenarioList),
							"scenario");
					model.addAttribute("projectsList", projectsList);
					model.addAttribute("modulesList", modulesList);
					model.addAttribute("addremovetestcaseBuildName",
							session.getAttribute("addremovetestcaseBuildName"));
					model.addAttribute("scenarioList", scenarioList);
					model.addAttribute("testcasesList", testCaseList);

					JSONArray jsonArray = new JSONArray();
					for (Map<String, Object> map : projectsList) {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("id", "project_" + map.get("project_id"));
						jsonObject.put("text", map.get("project_name").toString().trim());
						jsonObject.put("parent", "#");
						jsonArray.put(jsonObject);
					}

					for (Map<String, Object> map : modulesList) {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("id", "module_" + map.get("module_id"));
						jsonObject.put("text", map.get("module_name").toString().trim());
						jsonObject.put("parent", "project_" + map.get("project_id"));
						jsonArray.put(jsonObject);

					}
					for (Map<String, Object> map : scenarioList) {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("id", "scenario_" + map.get("scenario_id"));
						jsonObject.put("text", map.get("scenario_name").toString().trim());
						jsonObject.put("parent", "module_" + map.get("module_id"));
						jsonArray.put(jsonObject);

					}
					JSONArray testCaseIdsJSON = new JSONArray();
					for (Map<String, Object> map : testCaseList) {
						JSONObject jsonObject = new JSONObject();
						String testCaseId = String.valueOf(map.get("testcase_id"));
						jsonObject.put("id", testCaseId);
						jsonObject.put("text", map.get("testcase_name").toString().replaceAll("\\W", " "));
						jsonObject.put("parent", "scenario_" + map.get("scenario_id"));
						if( execType.contains( String.valueOf( ExecutionType.REDWOOD.getValue() ) ) )
							jsonObject.put("data", new JSONObject().put("redwood_tc_id", map.get("redwood_tc_id") ) );
						jsonArray.put(jsonObject);
						String testStatus = String.valueOf(map.get("testStatus"));
						if ("2".equals(testStatus) || "1".equals(testStatus))
							testCaseIdsJSON.put(testCaseId);

					}
					model.addAttribute("projectDetailsJSONArray", jsonArray);
					model.addAttribute("testCaseIdsJSON", testCaseIdsJSON);
				}

				model.addAttribute("isPremise", onPremiseStatus);
				model.addAttribute("mailGroupList", mailGroupList);
				model.addAttribute("vmList", vmList);
				model.addAttribute("vmJSONList", utilsService.convertListToJSONArray(vmList));
				model.addAttribute("UserProfilePhoto",
						loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
				model.addAttribute("userName", session.getAttribute("userFullName"));
				model.addAttribute("role", session.getAttribute("role"));
				logger.info("Model value for build project view : " + model);
				logger.info("User is redirected to  build project");
				if (utilsService.checkSessionContainsAttribute("DocumentUploadStatus", session)) {
					int documentUploadStatus = Integer
							.parseInt(session.getAttribute("DocumentUploadStatus").toString());
					session.removeAttribute("DocumentUploadStatus");
					model.addAttribute("documentUploadStatus", documentUploadStatus);
				} else {
					model.addAttribute("documentUploadStatus", 200);
				}
				Map<String, Object> automationReleasesAndBuilds = buildDAO.getReleasesAndBuildsAutomation(schema);
				model.addAttribute("releases",
						(List<Map<String, Object>>) automationReleasesAndBuilds.get("#result-set-1"));
				model.addAttribute("builds",
						(List<Map<String, Object>>) automationReleasesAndBuilds.get("#result-set-2"));

				return new ModelAndView("buildexecute", "Model", model);
			} else {
				logger.error("User doesn't have access to build project service and is redirected to error page");
				return new ModelAndView("redirect:/500");
			}

		}
	}

	/**
	 * This request is to set automation build project id
	 * 
	 * @param projectId
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setAutomationBuildProjectId")
	public ModelAndView setAutomationBuildProjectId(@RequestParam("projectId") int projectId, Model model,
			HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Builds");
			session.setAttribute("automationProjectId", projectId);
			return new ModelAndView("redirect:/");
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request is to add automation test cases
	 * 
	 * @param addTestCaseArr
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addautomationtestcase")
	public ModelAndView addautomationtestcase(@RequestParam("testCaseArray[]") int addTestCaseArr[], @RequestParam(name="redwoodTestCases[]",required=false) String redwoodTestCases[], 
			Model model, HttpSession session) throws Exception {
		logger.info("In post method add test cases");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Builds");
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int buildId = Integer.parseInt(session.getAttribute("BuildId").toString());

			int flag = 0;
			int j = 0;
			StringBuilder testCaseArray = new StringBuilder();
			for (int i : addTestCaseArr) {
				j++;
				if (addTestCaseArr.length == j)
					testCaseArray.append(i);
				else
					testCaseArray.append(i + ",");
			}
			flag = buildDAO.addAutomationTestCase(testCaseArray.toString(), buildId, userId, schema);

			if (flag != 0) {
				logger.info("Test cases are added to the build");
				String redwoodBuildId = null;
				String redwoodBuildIdResponse = null;
				if ( Integer.parseInt( session.getAttribute("automationExecBuildType").toString() ) == ExecutionType.REDWOOD.getValue() 
						&& ( redwoodBuildId = String.valueOf( session.getAttribute("redwoodBuildId") ) ) != null ) {
					RedwoodManagement redwoodManagement = new RedwoodManagement();
					redwoodBuildIdResponse = redwoodManagement.saveOrUpdateTestSet(session.getAttribute("redwood_url").toString(), redwoodBuildId, 
						session.getAttribute("automationExecBuildName").toString(), session.getAttribute("UserCurrentProjectName").toString(), redwoodTestCases );
				}
				return new ModelAndView("redirect:/");
			} else {
				logger.error("Something went wrong, User redirected to error page");
				return new ModelAndView("redirect:/500");
			}
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request is to drop automation test case
	 * 
	 * @param dropTestArray
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dropautomationtestcase")
	public ModelAndView dropautomationtestcase(@RequestParam("dropTestArray[]") int dropTestArray[], Model model,
			HttpSession session) throws Exception {
		logger.info("In post method drop test cases");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Builds");
			String schema = session.getAttribute("cust_schema").toString();
			int j = 0;
			StringBuilder droptestCaseArray = new StringBuilder();
			for (int i : dropTestArray) {
				j++;
				if (dropTestArray.length == j)
					droptestCaseArray.append(i);
				else
					droptestCaseArray.append(i + ",");
			}
			int buildId = Integer.parseInt(session.getAttribute("BuildId").toString());
			int flag = buildDAO.dropAutomationTestCase(droptestCaseArray.toString(), buildId, schema);
			if (flag != 0) {
				logger.info("Test cases are droped from the build");
				return new ModelAndView("redirect:/");
			} else {
				logger.error("Something went wrong, User redirected to error page");
				return new ModelAndView("redirect:/500");
			}
		}

	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request is used to insert automation build execution details
	 * 
	 * @param vmId
	 * @param browser
	 * @param mailGroupId
	 * @param stepWiseScreenshot
	 * @param reportBug
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/insertbuildexecdetails", method = RequestMethod.POST)
	public @ResponseBody String insertbuildexecdetails(@RequestParam("selectBrowser") String browser,
			@RequestParam(value = "selectMailGroup", defaultValue = "0") int mailGroupId,
			@RequestParam(value = "selectEnv", defaultValue = "0") String env,
			@RequestParam("stepwiseScreenshotStatus") int stepWiseScreenshot,
			@RequestParam("reportBugStatus") int reportBug, HttpSession session) throws Exception {

		utilsService.getLoggerUser(session);
		logger.info("In insert automation execution detail method");
		String result = "";
		try {
			if (buildDAO.checkBMRoleAccess("createbuild", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {

				String schema = session.getAttribute("cust_schema").toString();
				int userId = Integer.parseInt(session.getAttribute("userId").toString());
				int buildID = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());
				int customerId = Integer.parseInt(session.getAttribute("customerId").toString());
				int envStatus = 2;
				if (env.equalsIgnoreCase("0")) {
					envStatus = 2;
				} else {
					envStatus = 1;
				}
				BuildExecDetails buildExecDetails = buildExecDetailsDAO.setBuildExecDetailsValues(
						Integer.parseInt(session.getAttribute("automationExecBuildId").toString()), browser,
						mailGroupId, stepWiseScreenshot, reportBug, userId, 2, envStatus);

				String buildName = session.getAttribute("automationExecBuildName").toString();

				String vmJson = serverOperationService.checkVmStatus(configurationProperties.getProperty("XENON_VM_WS")
						+ "/checkVM?userId=" + userId + "&customerId=" + customerId);

				JSONArray jArray = new JSONArray(vmJson);
				if (jArray.length() > 0) {

					JSONObject jObject = jArray.getJSONObject(0);

					String isPremise = jObject.get("is_premise").toString();

					serverOperationService.checkVmStatus(configurationProperties.getProperty("XENON_VM_WS")
							+ "/startVM?vmID=" + jObject.get("idxevm_vmdetails").toString() + "&userID=" + userId
							+ "&buildID=" + buildID + "&schema=" + schema);

					List<Map<String, Object>> getVMDetails = buildExecDetailsDAO.getVMDetails(schema, userId, buildID);
					String CLIENT_VM_URL = getVMDetails.get(0).get("inetaddress").toString() + "/XenonClientWS";
					boolean insertBuildFlag = false;

					if (serverOperationService.getServerStatus(configurationProperties.getProperty("XENON_SERVER_WS"))
							.equals("OK")) {

						if (isPremise.equals("1")) {
							if (serverOperationService.getServerStatus(CLIENT_VM_URL).equals("OK")) {
								insertBuildFlag = true;

							} else {
								insertBuildFlag = false;
								logger.info("Client machine not running");
								result = "CLIENT NOT RUNNING";
							}
						} else {
							insertBuildFlag = true;
						}

						if (insertBuildFlag) {
							session.setAttribute("newBuildTypeForTab", "automation");
							int flag = buildExecDetailsDAO.insertBuildExecDetails(buildExecDetails, env, schema);
							if (flag == 1) {
								result = "STARTING CLIENT";

								/*
								 * buildDAO.sendMailOnSatrtOfAutoBuildExe(
								 * Integer.parseInt(session.getAttribute(
								 * "automationExecBuildId").toString()),
								 * schema);
								 */
							} else {
								return "INTERNAL SERVER ERROR";
							}
						}
					} else {
						logger.info("Server machine not running");
						result = "SERVER NOT RUNNING";
					}

				} else {
					logger.info("FREE VM NOT FOUND");
					result = "FREE VM NOT FOUND";
				}

			} else {
				logger.error("Something went wrong");
				return "INTERNAL SERVER ERROR";
			}
		} catch (Exception e) {
			logger.error("Something went wrong = " + e.getMessage());
			e.printStackTrace();
			return "INTERNAL SERVER ERROR";

		}
		return result;
	}

	/**
	 * @param exebrowser
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/buildautomation", method = RequestMethod.POST)
	public @ResponseBody String buildautomation(@RequestParam("exebrowser") String exebrowser, HttpSession session)
			throws Exception {

		utilsService.getLoggerUser(session);
		logger.info("In buildautomation execution  method");
		int userId = Integer.parseInt(session.getAttribute("userId").toString());
		int buildId = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());
		String result = "FAILED";
		String schema = session.getAttribute("cust_schema").toString();
		JSONObject json = new JSONObject();

		List<Map<String, Object>> autoBuildList = buildDAO1.getAutoBuildDetail(schema, buildId);
		List<Map<String, Object>> autoTestList = buildDAO1.getAutomationTestCases(schema, buildId);

		JSONArray testArray = new JSONArray(autoTestList);
		String userName = autoBuildList.get(0).get("user_name").toString();
		String buildName = autoBuildList.get(0).get("build_name").toString();
		String hostname = autoBuildList.get(0).get("host_name").toString();
		int vmId = Integer.parseInt(autoBuildList.get(0).get("vm_id").toString());

		List<Map<String, Object>> getVMDetails = buildExecDetailsDAO.getVMDetails(schema, userId, buildId);
		String CLIENT_VM_URL = getVMDetails.get(0).get("inetaddress").toString() + "/XenonClientWS";
		String isPremise = getVMDetails.get(0).get("is_premise").toString();

		String projectLocation = "C:/Automation/";// getVMDetails.get(0).get("project_location").toString();

		String stepWiseScreenShot = utilsService
				.getScreenShotStatusFromId(autoBuildList.get(0).get("step_wise_screenshot").toString());
		String reportBug = utilsService.getRerportBugStatusFromId(autoBuildList.get(0).get("report_bug").toString());
		String dirName = schema + userName + serverOperationService.getTimeStamp();
		String ouathPass = schema + userName;

		if (utilsService.createDirectory(configurationProperties.getProperty("XENON_REPOSITORY_PATH") + "repository"
				+ File.separator + "upload" + File.separator + "tmpFiles" + File.separator + dirName)) {
			if (utilsService.createLogfile(configurationProperties.getProperty("XENON_REPOSITORY_PATH") + "repository"
					+ File.separator + "upload" + File.separator + "logFiles" + File.separator + dirName + ".txt")) {
				int flag = buildDAO1.updateBuildLogfile(buildId, dirName, schema);
				if (flag == 1) {
					flag = buildDAO1.insertExecution(dirName, buildId, schema);
					if (flag == 1) {
						json.put("DIRENAME", dirName);
						json.put("BUILDNAME", buildName);
						json.put("USER_NAME", userName);
						json.put("SCHEMA", schema);
						json.put("USER", userId);
						json.put("BUILD_ID", buildId);
						json.put("EXE_BROWSER", exebrowser);
						json.put("OAUTH_PASS", ouathPass);
						json.put("USER_ID", session.getAttribute("userEmailId").toString());
						json.put("CLIENT_NAME", session.getAttribute("customerName").toString());
						json.put("SCREENSHOT", stepWiseScreenShot);
						json.put("RAISED_BUG", reportBug);
						json.put("AUTO_EXE_JSON", testArray.toString());
						json.put("CLIENT_VM_URL", CLIENT_VM_URL);
						json.put("SERVER_VM_URL", configurationProperties.getProperty("XENON_SERVER_WS"));
						json.put("PROJECT_PATH", projectLocation);

						if (isPremise.equals("1")) {
							result = serverOperationService.autoExe(configurationProperties.getProperty("XENON_VM_WS"),
									json.toString());
						} else {
							/*
							 * int
							 * customerId=Integer.parseInt(session.getAttribute(
							 * "customerId").toString());
							 * flag=buildDAO1.insertbuildExecDetails(json.
							 * toString(),hostname,vmId,buildId,userId,
							 * customerId,schema); if(flag != 1){ logger.info(
							 * "Something went wrong,Build details not inserted"
							 * ); result = "FAILED"; }
							 */
						}

					} else {

						logger.info("Something went wrong,Build details not inserted");
						result = "FAILED";
					}

				} else {
					logger.info("Something went wrong, Build Status Not changed");
					result = "FAILED";
				}

			} else {
				logger.info("Something went wrong, Log File Not Created");
				result = "FAILED";
			}
		} else {
			logger.info("Something went wrong,Execution Directory Not Created");
			result = "FAILED";
		}
		return result;
	}

	/**
	 * This request is to set log path
	 * 
	 * @param logPath
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setlogpath")
	public ModelAndView setlogpath(@RequestParam("logPath") String logPath, Model model, HttpSession session)
			throws Exception {
		logger.error("in setlogpath method");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			logger.error("session has been set for logPath attribute");
			session.setAttribute("logPath", configurationProperties.getProperty("XENON_REPOSITORY_PATH") + "repository"
					+ File.separator + "upload" + File.separator + "logFiles" + File.separator + logPath + ".txt");
			return new ModelAndView("redirect:/");
		}
	}

	/**
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/exelog")
	public ModelAndView exelog(Model model, HttpSession session) throws Exception {
		logger.error("in exelog method");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			String logPath = session.getAttribute("logPath").toString();
			JSONArray logLine = logMonitorService.readLog(logPath);
			model.addAttribute("Logline", logLine);
			model.addAttribute("logPath", logPath);
			logger.info("User is redirected to  codelog page");
			return new ModelAndView("codelog", "model", model);
		}
	}

	/**
	 * This request is a view for vm execution details
	 * 
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/vmexecdetails")
	public ModelAndView buildexecdetails(Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/logout");
		} else {
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			return new ModelAndView("vmexecdetails");
		}
	}
	
	@RequestMapping(value = "/autoexecute")
	public ModelAndView autoexecute(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to build project and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			// session.setAttribute("automationProjectId",0);
			utilsService.getLoggerUser(session);
			logger.info("Checking user's access to build project service");
			if (buildDAO.checkBMRoleAccess("buildproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to build project");
				String schema = session.getAttribute("cust_schema").toString();
				int customerId = Integer.parseInt(session.getAttribute("customerId").toString());

				int buildId = 0, releaseId = 0, buildState = 0, buildType=0;
				String buildName = "",redwoodBuildId=null;
				int buildStatus = 0;
				Long relCount = 0L;
				List<Map<String, Object>> vmList = null;
				List<Map<String, Object>> mailGroupList =null;
				List<Map<String, Object>> envList = null;
				List<Map<String, Object>> repoList = null;
				int userId = Integer.parseInt(session.getAttribute("userId").toString());
				List<Map<String, Object>> releaseCount = buildDAO.getReleaseCount(schema);
				for (int i = 0; i < releaseCount.size(); i++) {
					relCount = (Long) releaseCount.get(i).get("releaseCount");
				}
				if (relCount == 0) {
					return new ModelAndView("redirect:/createbuild");
				}
				if (session.getAttribute("automationExecBuildId") == null || session.getAttribute("automationExecBuildId").equals("0")) {
					List<Map<String, Object>> lastAutoBuildDetails = buildDAO.getlastAutoBuildDetails(userId, schema);
					for (int i = 0; i < lastAutoBuildDetails.size(); i++) {
						buildId = (Integer) lastAutoBuildDetails.get(i).get("build_id");
						buildName = (String) lastAutoBuildDetails.get(i).get("build_name");
						buildStatus = (Integer) lastAutoBuildDetails.get(i).get("build_status");
						releaseId = (Integer) lastAutoBuildDetails.get(i).get("release_id");
						buildType = (Integer) lastAutoBuildDetails.get(i).get("execution_type");
						redwoodBuildId = (String) lastAutoBuildDetails.get(0).get("redwood_build_id");
					}
					session.setAttribute("automationExecBuildId", buildId);
					session.setAttribute("automationExecReleaseId", releaseId);
					session.setAttribute("selectedAutoReportBuildId", buildId);
					session.setAttribute("BuildId", buildId);
					session.setAttribute("automationExecBuildName", buildName);
					session.setAttribute("UsertestRemoveStatus", buildStatus);
					session.setAttribute("selectedExecuteBuildName", buildName);
					session.setAttribute("automationExecBuildType", buildType);
					session.setAttribute("redwoodBuildId", redwoodBuildId );
				} else {
					buildId = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());
				}

				session.setAttribute("menuLiText", "Builds");

				model.addAttribute("automationExecBuildName", session.getAttribute("automationExecBuildName"));
				session.setAttribute("BuildId", buildId);
				// System.out.println("BuildId" + buildId);
				// int testRemoveStatus =
				// Integer.parseInt(session.getAttribute("UsertestRemoveStatus").toString());
				// model.addAttribute("removeStatus", testRemoveStatus);
				if (!utilsService.checkSessionContainsAttribute("automationProjectId", session))
					session.setAttribute("automationProjectId", 0);

				if(buildId>0){ 
				List<Map<String, Object>> buildData = buildDAO.getBuildAddedTcCount(userId, buildId, schema);
				int addedTcCount = Integer.parseInt((buildData).get(0).get("TcCount").toString());
				String execType = buildData.get(0).get("execution_type").toString();
				session.setAttribute("automationExecBuildType", execType);
				if(execType.equals("2")) {
					execType=execType+",3";
				}
				if(execType.equals("3")) {
					execType=execType+",2";
				}
				if (addedTcCount > 0)
					session.setAttribute("automationProjectId", buildData.get(0).get("project_id"));
				Map<String, Object> automationBuildData = buildDAO.getAutomationBuildData(customerId, userId, buildId,
						Integer.parseInt(session.getAttribute("automationProjectId").toString()),execType, schema);
				addedTcCount = Integer.parseInt(((List<Map<String, Object>>) automationBuildData.get("#result-set-1"))
						.get(0).get("TcCount").toString());
				model.addAttribute("addedTcCount", addedTcCount);
				model.addAttribute("automationProjectId",
						Integer.parseInt(session.getAttribute("automationProjectId").toString()));
				int onPremiseStatus = Integer.parseInt(session.getAttribute("customerOnPremiseStatus").toString());
				vmList = new ArrayList<Map<String, Object>>();
				mailGroupList = new ArrayList<Map<String, Object>>();
				envList = new ArrayList<Map<String, Object>>();
				repoList = new ArrayList<Map<String, Object>>();
				// ------------ get preview button details //
				// --------------------->

				List<Map<String, Object>> previewDetails = buildDAO.getPreviewDetails(userId, buildId, schema);
				// System.out.println("previewDetails:"+previewDetails);

				List<Map<String, Object>> autoBuildState = buildDAO.getAutoBuildState(buildId, schema);
				// System.out.println("autoBuildState:"+autoBuildState);
				for (int i = 0; i < autoBuildState.size(); i++) {
					buildState = (Integer) autoBuildState.get(i).get("build_state");
				}
				session.setAttribute("autoBuildState", buildState);
				model.addAttribute("state", autoBuildState);

				if (Integer.parseInt(session.getAttribute("automationProjectId").toString()) > 0 || addedTcCount != 0) {
					vmList = (List<Map<String, Object>>) automationBuildData.get("#result-set-5");
					// System.out.println("vmList:"+vmList);
					mailGroupList = (List<Map<String, Object>>) automationBuildData.get("#result-set-4");
					model.addAttribute("addremovetestcaseBuildName",
							session.getAttribute("addremovetestcaseBuildName"));
					envList = (List<Map<String, Object>>) automationBuildData.get("#result-set-7");

				}
				repoList = (List<Map<String, Object>>) automationBuildData.get("#result-set-8");
			
				model.addAttribute("repoList", repoList);
				model.addAttribute("mailGroupList", mailGroupList);
				model.addAttribute("vmList", vmList);
				model.addAttribute("envList", envList);
				model.addAttribute("isPremise", onPremiseStatus);
				model.addAttribute("previewDetails", previewDetails);
				model.addAttribute("vmJSONList", utilsService.convertListToJSONArray(vmList));
				model.addAttribute("UserProfilePhoto",
						loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
				model.addAttribute("userName", session.getAttribute("userFullName"));
				model.addAttribute("role", session.getAttribute("role"));
				}else{
					model.addAttribute("repoList", repoList);
					model.addAttribute("mailGroupList", mailGroupList);
					model.addAttribute("vmList", vmList);
					model.addAttribute("envList", envList);
					model.addAttribute("isPremise", null);
					model.addAttribute("previewDetails", null);
					model.addAttribute("vmJSONList", utilsService.convertListToJSONArray(vmList));
					model.addAttribute("UserProfilePhoto", 
							loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
					model.addAttribute("userName", session.getAttribute("userFullName"));
					model.addAttribute("role", session.getAttribute("role"));
					}
				Map<String, Object> automationReleasesAndBuilds = buildDAO.getReleasesAndBuildsAutomation(schema);
				List<Map<String, Object>> releases = (List<Map<String, Object>>) automationReleasesAndBuilds
						.get("#result-set-1");
				if (releases == null || releases.size() == 0) {
					return new ModelAndView("redirect:/createbuild");
				} else {
					model.addAttribute("releases", releases);
					model.addAttribute("builds",
							(List<Map<String, Object>>) automationReleasesAndBuilds.get("#result-set-2"));
				}
				if (session.getAttribute("redwood_url") != null) {
					model.addAttribute("redwood_url", session.getAttribute("redwood_url").toString());
				} else {
					model.addAttribute("redwood_url", "");
				}
								
				//call to check and update VM execute status based on before and after execution count.
				int preExecCount = buildDAO.getTcCountBeforeExecution(buildId, schema);
				int postExecCount = buildDAO.getTcCountAfterExecution(buildId, schema);
				if(preExecCount == postExecCount && buildState!=5 && preExecCount !=0){
					 int flag = buildDAO.updateBuildStateBasedOnCount(5, buildId, schema);
					 if(flag == 1){
						 System.out.println("Updated to Completed State successfully in xe_automation_build table as pre & post execution counts are equal");
						 int vmStopFlag = buildDAO.updateVMState(schema, 4, userId, buildId);
							if (vmStopFlag == 1) {
								System.out.println("Updated vm_state to 4, Stopped in xebm_vm_details table");
							}
							}
						 String allocatedVMId = session.getAttribute("cust_vm_id").toString();
						 System.out.println("allocated VM ID is "+allocatedVMId);
						 if(null != allocatedVMId){
						 int vmId = Integer.parseInt(allocatedVMId);
						
						 Map<String, Object> vmData = buildDAO.deallocateCustomerVM(vmId, userId, buildId, schema);
						 Map<String, Object> vmDetailsinfo =  (Map<String, Object>) vmData.get("#result-set-1");
						 System.out.println("deallocated customer VM by calling deallocateCustomerVM procedure");
						 }
					 }
					 else if(preExecCount>0 && postExecCount>0 && preExecCount == postExecCount){
						 System.out.println("Failed to update even after the counts are equal");
					 }
				
				
				
				logger.info("Model value for build project view : " + model);
				logger.info("User is redirected to  build project");
				return new ModelAndView("autoexecute", "Model", model);
			
			} else {
				logger.error("User doesn't have access to build project service and is redirected to error page");
				return new ModelAndView("redirect:/500");
			}

		}
	}
	
	
	// @RequestMapping(value = "/sendmailonexeccomplete")
	// public boolean sendmailonexeccomplete(@RequestParam("buildId") int
	// buildId,@RequestParam("schema") String schema,Model model, HttpSession
	// session) throws Exception {
	// logger.info("Redirecting to build project and checking for active
	// session");
	// if (session.getAttribute("userId") == null) {
	// logger.error("Session has expired,user is redirected to login page");
	// return false;
	// } else {
	// buildDAO.sendMailOnBuildExecutionComplete(buildId, schema);
	// return true;
	// }
	// }

	/**
	 * This request is to send mail on execution complete
	 * 
	 * @param buildId
	 * @param buildStamp
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sendmailonexeccomplete")
	public boolean sendmailonexeccomplete(@RequestParam("buildId") String buildId,
			@RequestParam("buildStamp") String buildStamp, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return false;
		} else {
			List<Map<String, Object>> buildDetail = buildDAO.getBuildDetailsByTimestampAndId(Integer.parseInt(buildId),
					buildStamp);
			String schema = buildDetail.get(0).get("schema_name").toString();
			buildDAO.sendMailOnBuildExecutionComplete(Integer.parseInt(buildId), schema);
			buildDAO.updateAutomationBuildStatus(Integer.parseInt(buildId), schema, buildStamp);

			return true;
		}
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request is to insert clone build data
	 * 
	 * @param sourceBuildId
	 * @param destiReleaseId
	 * @param destiBuildName
	 * @param buildDescription
	 * @param destiBuildStatus
	 * @param buildType
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/inserclonebuilddata", method = RequestMethod.POST)
	public ModelAndView inserclonebuilddata(@RequestParam("sourceBuildId") String sourceBuildId,
			@RequestParam("destiReleaseId") String destiReleaseId,
			@RequestParam("destiBuildName") String destiBuildName,
			@RequestParam("buildDescription") String buildDescription,
			@RequestParam(value = "destiBuildStatus", defaultValue = "1") String destiBuildStatus,
			@RequestParam(value = "buildType", defaultValue = "1") String buildType, Model model, HttpSession session)
			throws Exception {
		logger.info("In post request to insert a build");
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			int userID = Integer.parseInt(session.getAttribute("userId").toString());
			String userFullname = session.getAttribute("userFullName").toString();
			if (buildDAO.checkBMRoleAccess("createbuild", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {

				Map<String, Object> sessionData = buildService.insertCloneBuild(schema, sourceBuildId, buildType,
						destiBuildName, buildDescription, destiReleaseId, userID, destiBuildStatus, userFullname);
				int flag = Integer.parseInt(sessionData.get("flag").toString());
				session.setAttribute("newBuildTypeForTab", sessionData.get("newBuildTypeForTab"));
				if (flag >= 0) {

					session.setAttribute("newBuildName", destiBuildName.trim());
					session.setAttribute("buildCreateStatus", 1);
					return new ModelAndView("redirect:/viewbuild");
				} else {
					session.setAttribute("duplicateBuildError", "1");
					return new ModelAndView("redirect:/clonebuild");
				}
			}

			logger.error("User doesn't have access to create build service and is redirected to error page");
			return new ModelAndView("redirect:/500");
		}
	}

	/**
	 * 
	 * @author prafulla.pol
	 * @method tcAttachment
	 * @Description This method is used for uploading attachments from manual
	 *              build execution
	 * @param model
	 * @param session
	 * @param exeStatus
	 * @param tcId
	 * @param uploadForm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tcattachonbuildexe", method = RequestMethod.POST)
	@Consumes(MediaType.APPLICATION_JSON)
	public ModelAndView tcAttachment(Model model, HttpSession session, @RequestParam("exeStatus") String exeStatus,
			@RequestParam("tcId") String tcId, @ModelAttribute("uploadForm") FileUploadForm uploadForm)
			throws Exception {
		utilsService.getLoggerUser(session);
		logger.info("Post method to upload attachments to the database for manual build execution ");
		int status = Integer.parseInt(exeStatus);
		String schema = session.getAttribute("cust_schema").toString();
		int testCaseId = Integer.parseInt(tcId);
		int buildId = Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());
		// upload the attachments
		List<MultipartFile> files = uploadForm.getFiles();
		logger.info("Get uploaded files using multipart ");
		List<String> fileNames = new ArrayList<String>();
		String uploadDirectoryPath = configurationProperties.getProperty("XENON_REPOSITORY_PATH") + "repository"
				+ File.separator + "upload";
		if (null != files && files.size() > 0) {
			for (MultipartFile multipartFile : files) {
				String fileName = multipartFile.getOriginalFilename();
				fileNames.add(fileName);
				byte[] bytes = multipartFile.getBytes();
				org.springframework.core.io.Resource resource = new ClassPathResource(uploadDirectoryPath);
				String attachment = resource.getFile().getAbsolutePath() + File.separator + "bugtracker"
						+ File.separator + fileName;
				String title = multipartFile.getOriginalFilename();
				File fileToWriteTo = new File(attachment);
				FileUtils.writeByteArrayToFile(fileToWriteTo, bytes);
				// buildDAO.insertAttachmentOnTcExecution(testCaseId, buildId,
				// attachment, title, schema);
				buildDAO.insertAttachmentOnTcExecution(testCaseId, buildId, bytes, title, schema);
				fileToWriteTo.delete();
			}
		}

		if (status == 2) {
			logger.info("Attachments are successfully uploaded. Redirecting to view builds.");
			return new ModelAndView("redirect:/viewbuild");
		} else {
			session.setAttribute("sweetAlertStatusOnbuildExe", 1);
			logger.info("Attachments are successfully uploaded. Redirecting to execute builds.");
			return new ModelAndView("redirect:/executebuild");
		}
	}

	/**
	 * 
	 * @author shailesh.khot
	 * @method tcAttachment
	 * @Description This method is used for uploading attachments from manual
	 *              build execution
	 * @param model
	 * @param session
	 * @param exeStatus
	 * @param tcId
	 * @param uploadForm
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/tcattachonbuildexecution", method = RequestMethod.POST)
	@Consumes(MediaType.APPLICATION_JSON)
	public @ResponseBody int tcAttachment(Model model, HttpSession session, @RequestParam("exeStatus") String exeStatus,
			@RequestParam("tcId") String tcId, @RequestParam("uploadForm") CommonsMultipartFile uploadForm)
			throws Exception {
		utilsService.getLoggerUser(session);
		logger.info("Post method to upload attachments to the database for manual build execution ");
		String schema = session.getAttribute("cust_schema").toString();
		int testCaseId = Integer.parseInt(tcId);
		int buildId = Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());

		byte[] bytes = uploadForm.getBytes();
		String title = uploadForm.getFileItem().getName();

		buildDAO.insertAttachmentOnTcExecution(testCaseId, buildId, bytes, title, schema);
		return 1;
	}

	/**
	 * @author prafulla.pol
	 * @method getTestCaseAttchment POST Request
	 * @output download test case attachment on manual build execution
	 * @param Request
	 *            Parameter(attchmentId) Method Parameter(HttpSession session)
	 * @description This method accepts attachment id,gets byte array from
	 *              database and return converted encodeBase64 String
	 * @return encodeBase64 String
	 */
	@RequestMapping(value = "/gettcattchment", method = RequestMethod.POST)
	public @ResponseBody String getTestCaseAttchment(@RequestParam("attchmentId") String attchmentId,
			HttpSession session) throws Exception {
		utilsService.getLoggerUser(session);
		logger.info("Post method to download the attachments on build execution");
		String schema = session.getAttribute("cust_schema").toString();
		List<Map<String, Object>> tcAttachment = buildDAO.gettcAttachmentById(Integer.parseInt(attchmentId), schema);
		logger.info("Returning the attachment.");
		return utilsService.getFileData((byte[]) tcAttachment.get(0).get("attachment"));
	}

	/**
	 * @author prafulla.pol This method is used for uploading the build learning
	 *         and build status to complete.
	 * @param bldLearning
	 * @param session
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/uploadbuildlearning", method = RequestMethod.POST)
	public @ResponseBody int uploadBuildLearningAndStatus(@RequestParam("buildLearning") String buildLearning,
			HttpSession session, Model model) throws Exception {
		utilsService.getLoggerUser(session);
		logger.info("Uploading the build learning and build status to complete");
		int userID = Integer.parseInt(session.getAttribute("userId").toString());
		String userName = session.getAttribute("userFullName").toString();
		int buildID = Integer.parseInt(session.getAttribute("selectedExecuteBuildId").toString());
		String buildName = session.getAttribute("selectedExecuteBuildName").toString();
		String schema = session.getAttribute("cust_schema").toString();
		buildDAO.updateBuildLearningAndStatus(buildID, buildName, buildLearning, userID, userName, schema);
		return 1;
	}

	@RequestMapping(value = "/stopbuildexecution", method = RequestMethod.POST)
	public @ResponseBody int stopbuildexecution(Model model, HttpSession session,
			@RequestParam("buildID") String buildID) throws Exception {
		utilsService.getLoggerUser(session);
		int flag = 0;
		String schema = session.getAttribute("cust_schema").toString();
		flag = buildDAO.updateAutomationBuildStatus(7, Integer.parseInt(buildID), schema);
		if (flag != 0) {
			session.setAttribute("newBuildTypeForTab", "automation");
			return 1;
		} else {
			return 0;
		}

	}

	@RequestMapping(value = "/remotedesktop")
	public ModelAndView remotedesktop(Model model, HttpSession session) throws Exception {
		logger.error("in exelog method");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			session.setAttribute("menuLiText", "Builds");
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int buildId = Integer.parseInt(session.getAttribute("automationRemoteBuildId").toString());

			Map<String, Object> data = buildDAO.getBuildVmDetails(buildId, userId, schema);
			model.addAttribute("vmDetails", (List<Map<String, Object>>) data.get("#result-set-1"));

			int exeVMStatus = Integer.parseInt(
					((List<Map<String, Object>>) data.get("#result-set-1")).get(0).get("vm_state").toString());
			if (exeVMStatus == 1 || exeVMStatus == 4) {
				session.setAttribute("remoteMachineStatus", exeVMStatus);
				return new ModelAndView("redirect:/builddetails");
			} else {
				session.setAttribute("remoteMachineStatus", exeVMStatus);
			}
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			logger.info("User is redirected to  codelog page");
			return new ModelAndView("remotedesktop", "Model", model);
		}
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/setremoteautobuildsession")
	public ModelAndView setremoteautobuildsession(@RequestParam("buildId") int buildId,
			@RequestParam("buildName") String buildName, Model model, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("menuLiText", "Builds");
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			session.setAttribute("automationRemoteBuildId", buildId);
			session.setAttribute("automationRemoteBuildName", buildName);
			return new ModelAndView("redirect:/");
		}
	}

	@RequestMapping(value = "/checkmanualbuildname")
	public @ResponseBody boolean checkManualbuildname(Model model, HttpSession session,
			@RequestParam("relId") int relId, @RequestParam("buildName") String buildName) throws Exception {
		String schema = session.getAttribute("cust_schema").toString();
		// check for the manual build name
		int check = buildDAO.checkManualbuildname(relId, buildName, schema);

		if (check > 0)
			return true;
		else
			return false;
	}

	@RequestMapping(value = "/checkautobuildname")
	public @ResponseBody boolean checkAutoBuildName(Model model, HttpSession session, @RequestParam("relId") int relId,
			@RequestParam("buildName") String buildName) throws Exception {
		String schema = session.getAttribute("cust_schema").toString();
		// check for the manual build name
		int check = buildDAO.checkManualbuildname(relId, buildName, schema);

		if (check > 0)
			return true;
		else
			return false;
	}

	/**
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.getLoggerUser(session);
		utilsService.sendMailonExecption(ex, session);
		return new ModelAndView("redirect:/500");
	}

	/**
	 * @author dhananjay.deshmukh
	 * @param model
	 * @param request
	 * @return true if all test cases are valid for current build
	 */
	@RequestMapping(value = "/validatetestcases", method = RequestMethod.POST)
	public @ResponseBody String validateTestcases(Model model, HttpSession session) {
		int buildId = Integer.parseInt(session.getAttribute("BuildId").toString());
		String schema = session.getAttribute("cust_schema").toString();
		boolean isValid = buildService.validateTestCasesForBuild(buildId, schema);
		return "" + isValid;
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This request uploads datasheet
	 * 
	 * @param file
	 * @param uploadDatasheetTcId
	 * @param session
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/datasheetupload", method = RequestMethod.POST)
	public ModelAndView datasheetupload(@RequestParam(value = "uploadDatasheet", required = false) MultipartFile file,
			@RequestParam("uploadDatasheetTcId") int uploadDatasheetTcId, HttpSession session, Model model,
			HttpServletRequest request) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			logger.info("Uploading document");
			// int userId =
			// Integer.parseInt(session.getAttribute("userId").toString());
			int buildId = Integer.parseInt(session.getAttribute("BuildId").toString());

			String schema = session.getAttribute("cust_schema").toString();
			String fileName = file.getOriginalFilename();
			byte[] bytes = new byte[0];
			if (!file.isEmpty())
				bytes = file.getBytes();

			Map<String, String> response = buildService.uploadDatasheet(bytes, uploadDatasheetTcId, buildId, fileName,
					schema);

			if (Integer.parseInt(response.get("responseCode").toString()) == 201) {
				session.setAttribute("DocumentUploadStatus", "201");
				logger.info("Document uploaded successfully");
				return new ModelAndView("redirect:/buildexecute");
			} else if (Integer.parseInt(response.get("responseCode").toString()) == 417) {
				logger.info("Document upload unsuccessful");
				session.setAttribute("DocumentUploadStatus", "417");
				return new ModelAndView("redirect:/buildexecute");
			} else {
				session.setAttribute("DocumentUploadStatus", "500");
				logger.info("Document upload failed");
				return new ModelAndView("redirect:/500");
			}
		}
	}

	@RequestMapping(value = "/viewbuild", method = RequestMethod.POST)
	public @ResponseBody boolean removeBuild(Model model, HttpSession session, @RequestParam("buildId") int buildId) {
		logger.info("Ajax Post request to delete build");
		utilsService.getLoggerUser(session);
		String schema = session.getAttribute("cust_schema").toString();
		int flag = buildService.deleteBuild(buildId, schema);
		if (flag == 200) {
			return true;
		} else {
			return false;
		}
	}

	// Shailesh Khot
	@RequestMapping(value = "/uploadDataFile", method = RequestMethod.POST)
	public @ResponseBody String uploadDataFile(@RequestParam(value = "uploadForm") MultipartFile file,
			HttpSession session, Model model, HttpServletRequest request) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return "error";
		} else {
			utilsService.getLoggerUser(session);
			logger.info("Uploading document");
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int buildId = Integer.parseInt(session.getAttribute("BuildId").toString());

			String schema = session.getAttribute("cust_schema").toString();
			String fileName = file.getOriginalFilename();
			byte[] bytes = new byte[0];
			if (!file.isEmpty())
				bytes = file.getBytes();

			Map<String, String> response = buildService.uploadDatafile(bytes, buildId, userId, fileName, schema);

			if (Integer.parseInt(response.get("responseCode").toString()) == 201) {
				session.setAttribute("DocumentUploadStatus", "201");
				logger.info("Document uploaded successfully");
				return "success";
			} else if (Integer.parseInt(response.get("responseCode").toString()) == 417) {
				logger.info("Document upload unsuccessful");
				session.setAttribute("DocumentUploadStatus", "417");
				return "success";
			} else {
				session.setAttribute("DocumentUploadStatus", "500");
				logger.info("Document upload failed");
				return "error";
			}
		}
	}

	@RequestMapping(value = "/getVMDetails/{vmId}", method = RequestMethod.GET)
	public @ResponseBody String getVMDetails(Model model, HttpSession session, @PathVariable String vmId) {

		int customerId = Integer.parseInt(session.getAttribute("customerId").toString());
		int userId = Integer.parseInt(session.getAttribute("userId").toString());
		int buildId = Integer.parseInt(session.getAttribute("automationExecBuildId").toString());
		String schema = session.getAttribute("cust_schema").toString();
		int vmID = Integer.parseInt(vmId);
		List<Map<String, Object>> buildData = buildDAO.getBuildAddedTcCount(userId, buildId, schema);
		String execType = buildData.get(0).get("execution_type").toString();
		if(execType.equals("2")) {
			execType=execType+",3";
		}
		if(execType.equals("3")) {
			execType=execType+",2";
		}
		Map<String, Object> automationBuildData = buildDAO.getAutomationBuildData(customerId, userId, buildId,
				Integer.parseInt(session.getAttribute("automationProjectId").toString()),execType, schema);

		List<Map<String, Object>> vmList = (List<Map<String, Object>>) automationBuildData.get("#result-set-5");
		for (int i = 0; i < vmList.size(); i++) {
			int tempVmId = Integer.parseInt(vmList.get(i).get("cust_vm_id").toString());
			if (vmID == tempVmId) {
				return utilsService.convertMapToJson(vmList.get(i));

			}

		}
		return "error";
	}

	@RequestMapping(value = "/setSession", method = RequestMethod.POST)
	public @ResponseBody String setSession(@RequestParam(value = "buildId") String buildId,
			@RequestParam(value = "buildName") String buildName, HttpSession session, Model model,
			HttpServletRequest request) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return "error";
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("selectedExecuteBuildId", buildId);
			session.setAttribute("selectedExecuteBuildName", buildName);
		}

		return null;

	}

	
	/*
	 * @author Shailesh.khot
	 */
	@RequestMapping(value = "/refreshBuildState/{buildId}", method = RequestMethod.GET)
	public @ResponseBody int refreshBuildState(Model model, HttpSession session, @PathVariable String buildId) {
		logger.info("Redirecting to build project and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return 0;
		} else {
			// session.setAttribute("automationProjectId",0);
			utilsService.getLoggerUser(session);
			logger.info("Checking user's access to build project service");
			if (buildDAO.checkBMRoleAccess("buildproject", session.getAttribute("roleId").toString(),
					session.getAttribute("cust_schema").toString())) {
				logger.info("User has access to build project");
				String schema = session.getAttribute("cust_schema").toString();
				int buildID = Integer.parseInt(buildId);
				;

				int buildState = 0;
				List<Map<String, Object>> autoBuildState = buildDAO.getAutoBuildState(buildID, schema);
				// System.out.println("autoBuildState:"+autoBuildState);
				for (int i = 0; i < autoBuildState.size(); i++) {
					buildState = (Integer) autoBuildState.get(i).get("build_state");
				}
				session.setAttribute("autoBuildState", buildState);
				return buildState;
			}
		}
		return -1;
	}

	// Anmol Chadha
	@RequestMapping(value = "/restoreAutomation")
	public @ResponseBody Boolean restoreAutomation(@RequestParam int buildId, @RequestParam int releaseId,
			@RequestParam int releaseStatus, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return false;
		} else {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			return buildService.restoreAutomationBuildStatusForTrash(buildId, releaseId, schema);
		}

	}

	// Anmol Chadha
	@RequestMapping(value = "/restoreManual")
	public @ResponseBody Boolean restoreManual(@RequestParam int buildId, @RequestParam int releaseId,
			@RequestParam int releaseStatus, HttpSession session) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return false;
		} else {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			return buildService.restoreManualBuildStatusForTrash(buildId, releaseId, schema);
		}

	}

	// Anmol Chadha
	@RequestMapping(value = "/viewbuildtrash", method = RequestMethod.GET)
	public ModelAndView viewBuildTrash(HttpSession session, Model model, HttpServletRequest request) throws Exception {
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else {
			utilsService.getLoggerUser(session);
			logger.info("Uploading document");
			String schema = session.getAttribute("cust_schema").toString();
			List<Map<String, Object>> manualTrash = buildService.getTrashManualDetails(schema);
			List<Map<String, Object>> automationTrash = buildService.getTrashAutomationDetails(schema);
			model.addAttribute("manualTrash", manualTrash);
			model.addAttribute("automationTrash", automationTrash);
			return new ModelAndView("ViewBuildTrash", "Model", model);
		}
	}
	@RequestMapping(value = "/bpftVnc")
	public ModelAndView redwoodVnc(Model model, HttpSession session){
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else {
		return new ModelAndView("bpftVnc", "Model", model);
		}
	}
	@RequestMapping(value = "/bpft")
	public ModelAndView redwood(Model model, HttpSession session){
		if (session.getAttribute("userId") == null) {
			utilsService.getLoggerUser(session);
			logger.info("User is not logged in");
			logger.info("Redirecting to login page");
			return new ModelAndView("redirect:/logout");
		} else {
		return new ModelAndView("bpft", "Model", model);
		}
	}
	
	@RequestMapping(value = "/setDuplicateBuildSession", method = RequestMethod.POST)
	public @ResponseBody String setDuplicateBuildSession(Model model, HttpSession session){
		session.setAttribute("duplicateBuildError", "0");
		return "Success"; 
		 
	}  
}
package com.xenon.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.bugtracker.dao.BugDAO;
import com.xenon.bugtracker.dao.BugGroupDAO;
import com.xenon.bugtracker.domain.BugGroupDetails;

/**
 * 
 * @author navnath.damale
 *
 */
@Controller
public class BugGroupController {

	@Autowired
	BugGroupDAO bugGroupDAO;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	RoleAccessService roleAccessService;
	
	@Autowired
	BugDAO bugDAO;
	
	private static final Logger logger = LoggerFactory.getLogger(BugController.class);
	
	/**
	 * @author navnath.damale
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	/*@SuppressWarnings("unchecked")
	@RequestMapping(value = "/buggroup")
	public ModelAndView bugGroup(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to bug group  page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (access.checkBTAccess("buggroup", session)) {	
			
			Utils.getLoggerUser(session);
			model = common.getHeaderValues(model, session);
			if (model.containsAttribute("noProject")) {
				model.asMap().clear();
				return new ModelAndView("redirect:/xedashboard");
			}
			
			String schema = session.getAttribute("cust_schema").toString();
			Map<String, Object> bugGroupData = bugGroupDAO.getBugGroupData(schema);
			model.addAttribute("bugGroups", bugGroupData.get("#result-set-1"));
			List<Map<String, Object>> bugGroupDetails = (List<Map<String, Object>>) bugGroupData.get("#result-set-2");
			for (int i = 0; i < bugGroupDetails.size(); i++) {
				String logo = login.getImage((byte[]) bugGroupDetails.get(i).get("user_photo"));
				bugGroupDetails.get(i).put("user_photo", logo);
			}
			
			model.addAttribute("UserProfilePhoto", login.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("bugGroupDetails", bugGroupDetails);
			model.addAttribute("UserDetails", bugGroupData.get("#result-set-3"));
			model.addAttribute("role", session.getAttribute("role"));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			
			if (session.getAttribute("insertBugGroupError") != null) {
				model.addAttribute("insertError", session.getAttribute("insertBugGroupError").toString());
				session.removeAttribute("insertBugGroupError");
			}
			logger.info(" User is redirected to bug group page ");
			return new ModelAndView("buggroup", "Model", model);

		} else {
			logger.error("User does not have access to bug request, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
*/	
	/**
	 * @author navnath.damale
	 * @param model
	 * @param session
	 * @param bugGroupName
	 * @param users
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/insertbuggroup", method = RequestMethod.POST)
	public ModelAndView insertBugGroup(Model model, HttpSession session,
			@RequestParam(value = "bugGroupName") String bugGroupName,
			@RequestParam(value = "users", defaultValue = "No Users") String users) throws SQLException {
		logger.info("IN post Method - insert bug group");
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (bugDAO.checkBTRoleAccess("buggroup",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			String schema = session.getAttribute("cust_schema").toString();
			int bugGroupId = bugGroupDAO.insertBugGroup(bugGroupName, schema);
			if (bugGroupId != 0) {
				if (!(users.equals("No Users"))) {
					String[] tokens = users.split(",");
					List<BugGroupDetails> bugGroupList = new ArrayList<BugGroupDetails>();
					for (String token : tokens) {
						int userId = Integer.parseInt(token);
						
						BugGroupDetails bugGroup = new BugGroupDetails();
						bugGroup.setBugUserId(userId);
						bugGroup.setBugGroupId(bugGroupId);
						bugGroupList.add(bugGroup);
					}
					bugGroupDAO.insertBugGroupRecords(bugGroupList, schema);
				}
				logger.info("Bug group added succcesfully ");
				logger.info(" User is redirected to Bug group page ");
				return new ModelAndView("redirect:/buggroup");
			} else {
				logger.info("Bug group duplication error,bug group is not added");
				session.setAttribute("insertBugGroupError", 1);
				return new ModelAndView("redirect:/buggroup");
			}

		} else
			logger.error("Something went wrong, user is redirected to 500 error page");
		return new ModelAndView("500");
	}
	
	/**
	 * @author navnath.damale
	 * @param model
	 * @param session
	 * @param bugGroupId
	 * @param bugGroupName
	 * @param users
	 * @return
	 * @throws SQLException
	 */
	
	@RequestMapping(value = "/updatebuggroup", method = RequestMethod.POST)
	public ModelAndView updateBugGroup(Model model, HttpSession session,
			@RequestParam(value = "bugGroupId") String bugGroupId,
			@RequestParam(value = "bugGroupName") String bugGroupName,
			@RequestParam(value = "users", defaultValue = "No Users") String users) throws SQLException {
		logger.info("IN post Method - update bug group");
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (bugDAO.checkBTRoleAccess("buggroup",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			String schema = session.getAttribute("cust_schema").toString();
			int status = bugGroupDAO.updateBugGroupDetails(Integer.parseInt(bugGroupId), bugGroupName, schema);
			if (status == 1) {
				List<BugGroupDetails> bugGroupList = new ArrayList<BugGroupDetails>();
				if (!(users.equals("No Users"))) {
					String[] tokens = users.split(",");

					for (String token : tokens) {
						int userId = Integer.parseInt(token);

						BugGroupDetails bugGroup = new BugGroupDetails();
						bugGroup.setBugUserId(userId);
						bugGroup.setBugGroupId(Integer.parseInt(bugGroupId));
						bugGroupList.add(bugGroup);
					}
					bugGroupDAO.updateBugGroupRecords(bugGroupList, schema, 1);
				} else {
					BugGroupDetails bugGroup = new BugGroupDetails();
					bugGroup.setBugGroupId((Integer.parseInt(bugGroupId)));
					bugGroupList.add(bugGroup);
					bugGroupDAO.updateBugGroupRecords(bugGroupList, schema, 0);
				}
				logger.info("Bug group updated succcesfully ");
				logger.info(" User is redirected to bug group page ");
				return new ModelAndView("redirect:/buggroup");
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return new ModelAndView("500");
			}
		} else
			logger.error("Something went wrong, user is redirected to 500 error page");
		return new ModelAndView("500");
	}
	
	/**
	 * @author navnath.damale
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :" + ex);
		return new ModelAndView("redirect:/500");
	}	
}

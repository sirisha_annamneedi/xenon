package com.xenon.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.bugtracker.BugService;
import com.xenon.api.common.CommonService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.bugtracker.dao.BugDAO;
import com.xenon.bugtracker.dao.BugGroupDAO;
import com.xenon.bugtracker.dao.BugGroupDAOImpl;
import com.xenon.bugtracker.dao.BugSummaryDAO;
import com.xenon.buildmanager.dao.ModuleDAO;

/**
 * 
 * @author suresh.adling
 * @description Controller class to return views containing list of bugs
 * 
 */
@Component
@Controller
public class BugListViewController {
	@Resource(name = "colorProperties")
	private Properties colorProperties;
	
	
	@Autowired
	BugService bugService;
	
	@Autowired
	BugGroupDAO bugGroupDAO;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	UtilsService utilsService;

	@Autowired
	RoleAccessService roleAccessService;
	
	
	@Autowired
	BugSummaryDAO bugSummaryService;

	@Autowired
	ModuleDAO moduleDao;
	
	@Autowired
	BugGroupDAOImpl bugGroupDAOImpl;

	
	@Autowired
	BugDAO bugDAO;
	
	private static final Logger logger = LoggerFactory.getLogger(BugListViewController.class);

	/**
	 * @author suresh.adling
	 * @method viewAllBugs GET Request
	 * @output ModelAndView for all bugs
	 * @param Model
	 *            model,HttpSession session
	 * @description This method reads following list by using session stored
	 *              values(userId,projectId) and redirects user to all bugs view
	 *              with the model
	 * @return Model (viewallbugs) & View (onSuccess - viewallbugs ,onFailure -
	 *         logout/500)
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/viewallbugs")
	public ModelAndView viewAllBugs(@RequestParam(value="page",defaultValue="1") int page,
			Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to view all bugs page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("viewallbugs",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);

			int count = commonService.getLoginUserAssignedAppCount(session);
			if(count == 0){
				return new ModelAndView("redirect:/xedashboard");
			}
			
			int pageSize = 10;
			model.addAttribute("pageSize",pageSize);
			
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info("Session is active, userId is " + userId + " and projectId is " + projectId
					+ " Customer Schema " + schema);
			
			int startValue = (page-1)*pageSize;
			Map<String, Object> data = bugService.getAllBugDetails(projectId, userId, startValue, pageSize, schema);
			
			int allBugCount = (Integer) data.get("count");
			List<Map<String, Object>> allBugDetails = (List<Map<String, Object>>) data.get("#result-set-1");
			
			for (int i = 0; i < allBugDetails.size(); i++) {
				allBugDetails.get(i).put("create_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(allBugDetails.get(i).get("create_date").toString())));
				allBugDetails.get(i).put("update_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(allBugDetails.get(i).get("update_date").toString())));
				
				if(allBugDetails.get(i).get("assign_status").toString().equals("2"))
				{
					allBugDetails.get(i).put("assigneeFN", "None");
					allBugDetails.get(i).put("assigneeLN", "");
				}
				else if(allBugDetails.get(i).get("group_status").toString().equals("1"))
				{
					int bugGroupId=Integer.parseInt(allBugDetails.get(i).get("group_id").toString());
					List<Map<String, Object>> bugGroup=bugGroupDAO.getGroupbyId(bugGroupId, schema);
					
					allBugDetails.get(i).put("assigneeFN", bugGroup.get(0).get("bug_group_name"));
					allBugDetails.get(i).put("assigneeLN", "");
					
				}
			}

			model.addAttribute("allBugCount",allBugCount);
			model.addAttribute("allBugDetails", allBugDetails);
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			model.addAttribute("New", colorProperties.getProperty("bug.New"));
			model.addAttribute("Fixed", colorProperties.getProperty("bug.Fixed"));
			model.addAttribute("Closed", colorProperties.getProperty("bug.Closed"));
			model.addAttribute("Assigned", colorProperties.getProperty("bug.Assigned"));
			model.addAttribute("Verified", colorProperties.getProperty("bug.Verified"));
			model.addAttribute("Reject", colorProperties.getProperty("bug.Reject"));
			logger.info("User is redirected to viewallbugs  page ");
			return new ModelAndView("viewallbugs", "Model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	/**
	 * @author Chandradeep Patil
	 * @method setBtLinkSession GET Request
	 * @param model
	 * @param session
	 * @param filterText
	 * @param dashboard
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bugprioritylist")
	public ModelAndView bugprioritylist(@RequestParam(value="page",defaultValue="1") int page,
			Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to view all bugs page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("viewallbugs",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);

			int count = commonService.getLoginUserAssignedAppCount(session);
			if(count == 0){
				return new ModelAndView("redirect:/xedashboard");
			}
			
			int pageSize = 10;
			model.addAttribute("pageSize",pageSize);
			
			String filterText = session.getAttribute("filterText").toString();
			
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
			String schema = session.getAttribute("cust_schema").toString();
			logger.info("Session is active, userId is " + userId + " and projectId is " + projectId
					+ " Customer Schema " + schema);
			
			int startValue = (page-1)*pageSize;
			Map<String, Object> data = bugService.getAllBugDetails(projectId, userId, startValue, pageSize, schema);
			
			int allBugCount = (Integer) data.get("count");
			List<Map<String, Object>> allBugDetailsPriority = (List<Map<String, Object>>) data.get("#result-set-1");
			
			model.addAttribute("allBugDetailsPriority", allBugDetailsPriority);
			model.addAttribute("filterText", filterText);
		
			logger.info("User is redirected to bugprioritylist  page ");
			return new ModelAndView("bugprioritylist", "Model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	

	/**
	 * @author Chandradeep.patil
	 * @method bugSummary GET Request
	 * @output get bug summary view
	 * @param Model
	 *            model,HttpSession session
	 * @description This method gets bug id from session,reads following lists
	 *              by calling different methods and returns bug summary with
	 *              model
	 * @return Model (statusList,priorityList,userList,bugDetails,bugDetails,
	 *         bugAttachDetails,summaryDetails,summaryAttachment,
	 *         UserProfilePhoto,userName,role) View (onSuccess - bugsummary
	 *         ,onFailure - logout/500)
	 */
	@RequestMapping(value = "/releasebugsummary", method = RequestMethod.GET)
	public ModelAndView bugSummary(@RequestParam(value = "page", defaultValue = "1") int page, Model model,
			HttpSession session) throws Exception {
		List<Map<String, Object>> attachment = new ArrayList<Map<String, Object>>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-Y");
		Date date = new Date();

		logger.info("Redirecting to  bug summary page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("bugsummary", session.getAttribute("roleId").toString(),
				session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			
			int appCount = commonService.getLoginUserAssignedAppCount(session);
			if (appCount == 0) {
				return new ModelAndView("redirect:/xedashboard");
			}

			int pageSize = 5;
			model.addAttribute("pageSize", pageSize);
			int startValue = (page - 1) * pageSize;

			String schema = session.getAttribute("cust_schema").toString();
			int bugId = Integer.parseInt(session.getAttribute("selectedBugId").toString());
			logger.warn("Session is active, bugId is " + bugId + " Customer Schema " + schema);

			int projectId = Integer.parseInt(session.getAttribute("selectedBugProjectId").toString());
			Map<String, Object> data = bugSummaryService.getDataForBugSummary(projectId, bugId, startValue, pageSize,
					schema);
			List<Map<String, Object>> summaryDetails = (List<Map<String, Object>>) data.get("#result-set-5");
			for (int i = 0; i < summaryDetails.size(); i++) {
				int summaryId = Integer.parseInt(summaryDetails.get(i).get("bs_id").toString());
				List<Map<String, Object>> commentAttachments = bugService.getCommentAttachmentDeatils(summaryId,
						schema);
				for (int j = 0; j < commentAttachments.size(); j++) {
					attachment.add(commentAttachments.get(j));
				}
				String logo = loginService.getImage((byte[]) summaryDetails.get(i).get("user_photo"));
				summaryDetails.get(i).put("updater_logo", logo);
				summaryDetails.get(i).put("update_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(summaryDetails.get(i).get("update_date").toString())));

			}
			List<Map<String, Object>> bugDetails = (List<Map<String, Object>>) data.get("#result-set-3");
			for (int i = 0; i < bugDetails.size(); i++) {
				bugDetails.get(i).put("create_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(bugDetails.get(i).get("create_date").toString())));

				if (bugDetails.get(i).get("bug_id").toString().equals(summaryDetails.get(i).get("bug_id").toString())) {
					if (bugDetails.get(i).get("assign_status").toString().equals("2")) {
						summaryDetails.get(i).put("bug_assignee_name", "None");
						summaryDetails.get(i).put("prev_assignee_name", "None");
					} else if (bugDetails.get(i).get("group_status").toString().equals("1")) {
						int bugGroupId = Integer.parseInt(bugDetails.get(i).get("group_id").toString());
						List<Map<String, Object>> bugGroup = bugGroupDAO.getGroupbyId(bugGroupId, schema);
						summaryDetails.get(i).put("bug_assignee_name", bugGroup.get(0).get("bug_group_name"));
					}
				}
			}
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);

			// List<Map<String, Object>>
			// moduleId=modeuleService.getBugModule(bugId, schema);

			List<Map<String, Object>> moduleId = (List<Map<String, Object>>) data.get("#result-set-9");

			List<Map<String, Object>> groupList = (List<Map<String, Object>>) data.get("#result-set-10");
			List<Map<String, Object>> userList = (List<Map<String, Object>>) data.get("#result-set-4");
			boolean userBugAccess = false;
			for (int count = 0; count < userList.size(); count++) {
				if (Integer.parseInt(userList.get(count).get("user_id").toString()) == Integer
						.parseInt(session.getAttribute("userId").toString())) {
					userBugAccess = true;
					break;
				}
				if (userBugAccess)
					break;
			}
			if (!userBugAccess) {
				logger.error("Something went wrong, user is redirected to 401 error page");
				return new ModelAndView("401");
			}
			for (int i = 0; i < groupList.size(); i++) {
				int groupId = Integer.parseInt(groupList.get(i).get("bug_group_id").toString());
				List<Map<String, Object>> groupUser = bugGroupDAOImpl.getUserbyGroupId(groupId, schema);
				List<Map<String, Object>> moduleUser = moduleDao
						.moduleUserDetails(Integer.parseInt(moduleId.get(0).get("module").toString()), schema);

				moduleUser.retainAll(groupUser);
				if (moduleUser.size() > 0) {
					userList.add(groupList.get(i));
				}
			}
			model.addAttribute("buildList", data.get("#result-set-11"));
			model.addAttribute("statusList", data.get("#result-set-1"));
			model.addAttribute("priorityList", data.get("#result-set-2"));
			model.addAttribute("bugDetails", bugDetails);
			model.addAttribute("userList", userList);
			model.addAttribute("summaryDetails", summaryDetails);
			model.addAttribute("bugAttachDetails", data.get("#result-set-6"));
			model.addAttribute("severityList", data.get("#result-set-7"));
			model.addAttribute("categoryList", data.get("#result-set-8"));
			model.addAttribute("summaryAttachment", attachment);
			model.addAttribute("New", colorProperties.getProperty("bug.New"));
			model.addAttribute("Fixed", colorProperties.getProperty("bug.Fixed"));
			model.addAttribute("Closed", colorProperties.getProperty("bug.Closed"));
			model.addAttribute("Assigned", colorProperties.getProperty("bug.Assigned"));
			model.addAttribute("Verified", colorProperties.getProperty("bug.Verified"));
			model.addAttribute("Reject", colorProperties.getProperty("bug.Reject"));
			model.addAttribute("nowDate", dateFormat.format(date));
			model.addAttribute("prevDate", dateFormat.format(cal.getTime()));
			model.addAttribute("UserProfilePhoto",
					loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));

			int allCommentsCount = (Integer) data.get("commentcount");
			model.addAttribute("allCommentsCount", (allCommentsCount - 1));
			logger.info("User is redirected to bug summary page ");
			return new ModelAndView("releasebugsummary", "Model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	
	
	/**
	 * @author suresh.adling
	 * @method getUserAssignedBugs GET Request
	 * @output ModelAndView for user assigned bugs
	 * @param Model
	 *            model,HttpSession session
	 * @description This method reads following list by using session stored
	 *              values(userId,projectId) and redirects user to all bugs view
	 *              with the model
	 * @return Model (bugsassignedtome) & View (onSuccess - bugsassignedtome
	 *         ,onFailure - logout/500)
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bugsassignedtome")
	public ModelAndView getUserAssignedBugs(@RequestParam(value="page",defaultValue="1") int page,
			Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to view assigned bugs page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("bugsassignedtome",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			
			int count = commonService.getLoginUserAssignedAppCount(session);
			if(count == 0){
				return new ModelAndView("redirect:/xedashboard");
			}
			
			int pageSize = 10 ;
			model.addAttribute("pageSize",pageSize);
			
			String schema = session.getAttribute("cust_schema").toString();
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
			logger.warn("Session is active, userId is " + userId + " and projectId is " + projectId
					+ " Customer Schema " + schema);
			
			int startValue = (page-1)*pageSize;
			Map<String, Object> data = bugService.getAssignedToMeBugs(projectId, userId, startValue, pageSize, schema);
		
			int assignedBugCount = (Integer) data.get("count");		
			model.addAttribute("assignedBugCount",assignedBugCount);
			
			List<Map<String, Object>> assignedBugDetails = (List<Map<String, Object>>) data.get("#result-set-1");
			
			for (int i = 0; i < assignedBugDetails.size(); i++) {
				assignedBugDetails.get(i).put("create_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(assignedBugDetails.get(i).get("create_date").toString())));
				assignedBugDetails.get(i).put("update_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(assignedBugDetails.get(i).get("update_date").toString())));
			}

			model.addAttribute("assignedBugDetails", assignedBugDetails);
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			model.addAttribute("New", colorProperties.getProperty("bug.New"));
			model.addAttribute("Fixed", colorProperties.getProperty("bug.Fixed"));
			model.addAttribute("Closed", colorProperties.getProperty("bug.Closed"));
			model.addAttribute("Assigned", colorProperties.getProperty("bug.Assigned"));
			model.addAttribute("Verified", colorProperties.getProperty("bug.Verified"));
			model.addAttribute("Reject", colorProperties.getProperty("bug.Reject"));
			logger.info("User is redirected to bugsassignedtome  page ");
			return new ModelAndView("bugsassignedtome", "Model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	/**
	 * @author suresh.adling
	 * @method getBugsReportedByMe GET Request
	 * @output ModelAndView for bugs reported by me
	 * @param Model
	 *            model,HttpSession session
	 * @description This method reads following list by using session stored
	 *              values(userId,projectId) and redirects user to all bugs view
	 *              with the model
	 * @return Model (bugsreportedbyme) & View (onSuccess - bugsreportedbyme
	 *         ,onFailure - logout/500)
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bugsreportedbyme")
	public ModelAndView getBugsReportedByMe(@RequestParam(value="page",defaultValue="1") int page,
			Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to view reported bugs page and checking for active session");

		if (session.getAttribute("userId") == null) {
			logger.warn("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/logout");
		} else if (bugDAO.checkBTRoleAccess("bugsreportedbyme",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
		
			int count = commonService.getLoginUserAssignedAppCount(session);
			if(count == 0){
				return new ModelAndView("redirect:/xedashboard");
			}
			
			int pageSize = 10;
			model.addAttribute("pageSize",pageSize);
			
			String schema = session.getAttribute("cust_schema").toString();
			int projectId = Integer.parseInt(session.getAttribute("UserCurrentProjectId").toString());
			int userId = Integer.parseInt(session.getAttribute("userId").toString());
			logger.warn("Session is active, userId is " + userId + " and projectId is " + projectId
					+ " Customer Schema " + schema);
			
			int startValue = (page-1)*pageSize;
			Map<String, Object> data = bugService.getReportedByMeBugs(projectId, userId, startValue, pageSize, schema);
			
			int allBugCount = (Integer) data.get("count");
			List<Map<String, Object>> reportedBugDetails = (List<Map<String, Object>>) data.get("#result-set-1");
			 
			for (int i = 0; i < reportedBugDetails.size(); i++) {
				reportedBugDetails.get(i).put("create_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(reportedBugDetails.get(i).get("create_date").toString())));
				reportedBugDetails.get(i).put("update_date",
						utilsService.getDateTimeOfTimezone(session.getAttribute("userTimezone").toString(),
								utilsService.getDate(reportedBugDetails.get(i).get("update_date").toString())));
				
				if(reportedBugDetails.get(i).get("assign_status").toString().equals("2"))
				{
					reportedBugDetails.get(i).put("assigneeFN", "None");
					reportedBugDetails.get(i).put("assigneeLN", "");
				}
				else if(reportedBugDetails.get(i).get("group_status").toString().equals("1"))
				{
					int bugGroupId=Integer.parseInt(reportedBugDetails.get(i).get("group_id").toString());
					List<Map<String, Object>> bugGroup=bugGroupDAO.getGroupbyId(bugGroupId, schema);
					
					reportedBugDetails.get(i).put("assigneeFN", bugGroup.get(0).get("bug_group_name"));
					reportedBugDetails.get(i).put("assigneeLN", "");
					
				}
				
			}
			model.addAttribute("reportedBugCount",allBugCount);
			model.addAttribute("reportedBugDetails", reportedBugDetails);
			
			model.addAttribute("UserProfilePhoto", loginService.getImage((byte[]) session.getAttribute("userProfilePhoto")));
			model.addAttribute("userName", session.getAttribute("userFullName"));
			model.addAttribute("role", session.getAttribute("role"));
			model.addAttribute("New", colorProperties.getProperty("bug.New"));
			model.addAttribute("Fixed", colorProperties.getProperty("bug.Fixed"));
			model.addAttribute("Closed", colorProperties.getProperty("bug.Closed"));
			model.addAttribute("Assigned", colorProperties.getProperty("bug.Assigned"));
			model.addAttribute("Verified", colorProperties.getProperty("bug.Verified"));
			model.addAttribute("Reject", colorProperties.getProperty("bug.Reject"));
			logger.info("User is redirected to bugsreportedbyme  page ");
			return new ModelAndView("bugsreportedbyme", "Model", model);
		} else {
			logger.error("Something went wrong, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}
	
	/**
	 * @author Chandradeep Patil
	 * @method setBtLinkSession GET Request
	 */

	@RequestMapping(value = "/setbtlinktoallbugs")
	public ModelAndView setBtLinkSessionAppWise(Model model, HttpSession session,
			@RequestParam("filterText") String filterText, @RequestParam("dashboard") String dashboard)
			throws Exception {
		logger.info("Setting selecte choice for redirection to bug tracker data page");
		logger.info("checking for active session");

		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirecting to login page");
			return new ModelAndView("redirect:/");
		} else {
			utilsService.getLoggerUser(session);
			session.setAttribute("filterText", filterText);
			session.setAttribute("btDashboardType", dashboard);
			return new ModelAndView("redirect:/");
		}
	}
	
	
	/**
	 * @author suresh.adling
	 * @method setBugIdForSummary POST Request
	 * @output sets session attribute bug id for selected bug
	 * @param Request
	 *            Parameter(moduleId) Method Parameter(HttpSession session)
	 * @description This method accepts bug id,sets bug id and returns true
	 * 
	 * @return boolean output
	 */
	@RequestMapping(value = "/setbugidforsummary", method = RequestMethod.POST)
	public @ResponseBody boolean setBugIdForSummary(Model model, HttpSession session,
			@RequestParam("bugID") String bugId) {
		utilsService.getLoggerUser(session);
		if (session.getAttribute("userId") == null) {
			return false;
		} else {
			session.setAttribute("selectedBugId", bugId);
			session.setAttribute("selectedBugProjectId", session.getAttribute("UserCurrentProjectId").toString());
			return true;
		}
	}

	/**
	 * @author suresh.adling
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :" + ex);
		return new ModelAndView("redirect:/500");
	}
	
	@RequestMapping(value = "/getCount")
	public @ResponseBody int getCount(Model model, HttpSession session) {
		utilsService.getLoggerUser(session);
		int count = commonService.getLoginUserAssignedAppCount(session);
		return count;
	}
}

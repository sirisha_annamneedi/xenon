package com.xenon.controller.utils;

public class Constants {
	
	public static final String SMTP_HOST="smtp.nbttech.com";
	public static final String SMTP_USER="yogi.gar@riverbed.com";
	public static final String SMTP_PASS="Jadeglobal123#";
	public static final String SMTP_ENABLE="1";
	
	//constants for automation build execution
	public static final String XenonServerWS="http://192.168.57.89:8091/XenonServerWS";
	public static final String XENON_CLIENT_INACTIVE="CLIENT NOT RUNNING";
	public static final String XENON_VM_SERVER_INACTIVE="VM SERVER NOT RUNNING";
	public static final String XENON_SERVER_INACTIVE="SERVER NOT RUNNING";
	public static final String XENON_EXCEPTION="INTERNAL SERVER ERROR";
	public static final String XENON_VM_NOT_FOUND="FREE VM NOT FOUND";
	public static final String XENON_CLIENT_STARTED="STARTING CLIENT";
	public static final String XENON_STATUS_SUCCESS="OK";
	public static final String XENON_STATUS_FAILED="FAILED";
	public static final String EXECUTION_IN_PROGRESS="EXECUTION_IN_PROGRESS";
	public static final String EXECUTION_ADDED_IN_QUEUE="EXECUTION_ADDED_IN_QUEUE";
	public static final String EXECUTION_ADD_UNSUCCESSFUL="EXECUTION_ADD_UNSUCCESSFUL";
	public static final String EXE_TYPE="API";
	public static final String XENON_REPORT_API_REPOSITORY_PATH="D:";
	
	//File-Directory status constants
	public static final String FILE_STATUS_NOT_CREATED="FILE NOT CREATED";
	public static final String DIRECTORY_STATUS_NOT_CREATED="DIRECTORY NOT CREATED";

	public Constants() {
		
	}
}


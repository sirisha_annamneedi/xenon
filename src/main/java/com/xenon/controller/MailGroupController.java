package com.xenon.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xenon.api.buildmanager.SettingService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.common.dao.MailGroupDAO;

/**
 * 
 * @author suresh.adling
 * @description Controller includes requests to do create,update operations on a
 *              mail group
 * 
 */
@SuppressWarnings("unchecked")
@Controller
public class MailGroupController {

	@Autowired
	MailGroupDAO mailGroupDAO;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	RoleAccessService roleAccessService;
	
	@Autowired
	SettingService settingService;
	
	@Autowired
	BuildDAO buildDAO;

	private static final Logger logger = LoggerFactory.getLogger(BugController.class);

	/**
	 * @author suresh.adling
	 * @method Get request - mail group
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mailgroup")
	public ModelAndView mailGroup(Model model, HttpSession session) throws Exception {
		logger.info("Redirecting to mail group  page and checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (buildDAO.checkBMRoleAccess("mail",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			utilsService.getLoggerUser(session);
			String schema = session.getAttribute("cust_schema").toString();
			session.setAttribute("menuLiText", "Settings");
			Map<String, Object> mailGroupData = mailGroupDAO.getmailGroupData(schema);
			model.addAttribute("mailGroups", mailGroupData.get("#result-set-1"));
			List<Map<String, Object>> mailGroupDetails = (List<Map<String, Object>>) mailGroupData.get("#result-set-2");
			for (int i = 0; i < mailGroupDetails.size(); i++) {
				String logo = loginService.getImage((byte[]) mailGroupDetails.get(i).get("user_photo"));
				mailGroupDetails.get(i).put("user_photo", logo);
			}
			model.addAttribute("mailGroupDetails", mailGroupDetails);
			model.addAttribute("UserDetails", mailGroupData.get("#result-set-3"));
			if (session.getAttribute("insertMailGroupError") != null) {
				model.addAttribute("insertError", session.getAttribute("insertMailGroupError").toString());
				session.removeAttribute("insertMailGroupError");
			}
			logger.info(" User is redirected to mail group page ");
			return new ModelAndView("mailgroup", "Model", model);

		} else {
			logger.error("User does not have access to mail request, user is redirected to 500 error page");
			return new ModelAndView("500");
		}
	}

	/**
	 * @author suresh.adling
	 * @method insert a mail group
	 * @param model
	 * @param session
	 * @param mailGroupName
	 * @param users
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/insertmailgroup", method = RequestMethod.POST)
	public ModelAndView insertMailGroup(Model model, HttpSession session,
			@RequestParam(value = "mailGroupName") String mailGroupName,
			@RequestParam(value = "users", defaultValue = "No Users") String users) throws Exception {
		logger.info("IN post Method - insert mail group");
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (buildDAO.checkBMRoleAccess("mail",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			String schema = session.getAttribute("cust_schema").toString();
			/*int mailGroupId = mailGroupDAO.insertMailGroup(mailGroupName, schema);
			if (mailGroupId != 0) {
				if (!(users.equals("No Users"))) {
					String[] tokens = users.split(",");
					List<MailGroupDetails> mailGroupList = new ArrayList<MailGroupDetails>();
					for (String token : tokens) {
						int userId = Integer.parseInt(token);

						MailGroupDetails mailGroup = new MailGroupDetails();
						mailGroup.setUserId(userId);
						mailGroup.setMailGroupId(mailGroupId);
						mailGroupList.add(mailGroup);
					}
					mailGroupDAO.insertMailGroupRecords(mailGroupList, schema);
				}*/
			int mailGroupId=settingService.insertMailGroup(mailGroupName,schema,mailGroupDAO,users);
			if(mailGroupId!=0){
				logger.info("Mail group added succcesfully ");
				logger.info(" User is redirected to mail group page ");
				return new ModelAndView("redirect:/mailgroup");
			} else {
				logger.info("Mail group duplication error,mial group is not added");
				session.setAttribute("insertMailGroupError", 1);
				return new ModelAndView("redirect:/mailgroup");
			}

		} else
			logger.error("Something went wrong, user is redirected to 500 error page");
		return new ModelAndView("500");
	}

	/**
	 * @author suresh.adling
	 * @method update mail group(add/remove users,update mail group name)
	 * @param model
	 * @param session
	 * @param mailGroupId
	 * @param mailGroupName
	 * @param users
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/updatemailgroup", method = RequestMethod.POST)
	public ModelAndView updateMailGroup(Model model, HttpSession session,
			@RequestParam(value = "mailGroupId") String mailGroupId,
			@RequestParam(value = "mailGroupName") String mailGroupName,
			@RequestParam(value = "users", defaultValue = "No Users") String users) throws SQLException {
		logger.info("IN post Method - update mail group");
		logger.info("Checking for active session");
		if (session.getAttribute("userId") == null) {
			logger.error("Session has expired,user is redirected to login page");
			return new ModelAndView("redirect:/");
		} else if (buildDAO.checkBMRoleAccess("mail",session.getAttribute("roleId").toString(), session.getAttribute("cust_schema").toString())) {
			//String schema = session.getAttribute("cust_schema").toString();
			//int status = mailGroupDAO.updateMailGroupDetails(Integer.parseInt(mailGroupId), mailGroupName, schema);
			String schema = session.getAttribute("cust_schema").toString();
			int status=settingService.updatemailgroup(schema,users,mailGroupDAO,mailGroupId,mailGroupName);
			if (status == 1) {
				/*List<MailGroupDetails> mailGroupList = new ArrayList<MailGroupDetails>();
				if (!(users.equals("No Users"))) {
					String[] tokens = users.split(",");

					for (String token : tokens) {
						int userId = Integer.parseInt(token);

						MailGroupDetails mailGroup = new MailGroupDetails();
						mailGroup.setUserId(userId);
						mailGroup.setMailGroupId(Integer.parseInt(mailGroupId));
						mailGroupList.add(mailGroup);
					}
					mailGroupDAO.updateMailGroupRecords(mailGroupList, schema, 1);
				} else {
					MailGroupDetails mailGroup = new MailGroupDetails();
					mailGroup.setMailGroupId(Integer.parseInt(mailGroupId));
					mailGroupList.add(mailGroup);
					mailGroupDAO.updateMailGroupRecords(mailGroupList, schema, 0);
				}*/
				logger.info("Mail group updated succcesfully ");
				logger.info(" User is redirected to mail group page ");
				return new ModelAndView("redirect:/mailgroup");
			} else {
				logger.error("Something went wrong, user is redirected to 500 error page");
				return new ModelAndView("500");
			}
		} else
			logger.error("Something went wrong, user is redirected to 500 error page");
		return new ModelAndView("500");
	}

	/**
	 * @author suresh.adling
	 * @method errorResponse exception handler method
	 * @output catch all exceptions
	 * @param Exception
	 *            ex, HttpServletResponse response, HttpSession session
	 * @description This method will catch exception occurred through out all
	 *              methods from this controller
	 * @return Model & View (500)
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView errorResponse(Exception ex, HttpServletResponse response, HttpSession session) {
		utilsService.sendMailonExecption(ex, session);
		utilsService.getLoggerUser(session);
		logger.error("Something went wrong - Exception :" + ex);
		return new ModelAndView("redirect:/500");
	}
	
	
}

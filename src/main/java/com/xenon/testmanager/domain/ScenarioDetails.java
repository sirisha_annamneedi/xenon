package com.xenon.testmanager.domain;

import java.util.List;

import net.rcarz.jiraclient.Version;

/**
 * 
 * @author suresh.adling
 * @description Domain class to declare getter setter methods for Scenario
 * 
 * 
 */
public class ScenarioDetails {
	private int scenarioId;
	private String scenarioName;
	private String jiraStoryId;
	private String jiraStory;
	private List<Version> fixVersion;
	private String scenarioDescription;
	private int moduleId;
	private int status;
	private String createdDate;

	public List<TestCaseDetails> testCaseList;

	public int getScenarioId() {
		return scenarioId;
	}

	public void setScenarioId(int scenarioId) {
		this.scenarioId = scenarioId;
	}

	public String getScenarioName() {
		return scenarioName;
	}

	public void setScenarioName(String scenarioName) {
		this.scenarioName = scenarioName;
	}

	public String getJiraStoryId() {
		return jiraStoryId;
	}

	public void setJiraStoryId(String jiraStoryId) {
		this.jiraStoryId = jiraStoryId;
	}

	public String getJiraStory() {
		return jiraStory;
	}

	public void setJiraStory(String jiraStory) {
		this.jiraStory = jiraStory;
	}
	
	public String getScenarioDescription() {
		return scenarioDescription;
	}

	public void setScenarioDescription(String scenarioDescription) {
		this.scenarioDescription = scenarioDescription;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<TestCaseDetails> getTestCaseList() {
		return testCaseList;
	}

	public void setTestList(List<TestCaseDetails> testcaseList) {
		this.testCaseList = testcaseList;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public List<Version> getFixVersion() {
		return fixVersion;
	}

	public void setFixVersion(List<Version> versionList) {
		this.fixVersion = versionList;
	}

}

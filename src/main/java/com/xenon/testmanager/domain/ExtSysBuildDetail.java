package com.xenon.testmanager.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExtSysBuildDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6946935321928696400L;
	private String name;
	private String start_datetime;
	private String end_datetime;
	private int releaseId;
	
	public List<ExtSysTestCaseDetail> testcaselist;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStart_datetime() {
		return start_datetime;
	}
	public void setStart_datetime(String start_datetime) {
		this.start_datetime = start_datetime;
	}
	public String getEnd_datetime() {
		return end_datetime;
	}
	public void setEnd_datetime(String end_datetime) {
		this.end_datetime = end_datetime;
	}
	
	public int getReleaseId() {
		return releaseId;
	}
	public void setReleaseId(int releaseId) {
		this.releaseId = releaseId;
	}
	
	@JsonProperty("testcaselist")
	public List<ExtSysTestCaseDetail> getTestCaseList() {
		return testcaselist;
	}
	public void setTestCaseList(List<ExtSysTestCaseDetail> testCaseList) {
		this.testcaselist = testCaseList;
	}
	
	public String toString() {
		 return releaseId+" "+start_datetime+" "+end_datetime; 
	}
	
}

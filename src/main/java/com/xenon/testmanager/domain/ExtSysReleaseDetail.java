package com.xenon.testmanager.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExtSysReleaseDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String title;
	private String description;
	private String start_datetime;
	private String end_datetime;
	
	private String username;
	private String password;
	//Column to check if ext sys is qtp or selenium
	//values e.g Selenium = sel
	//QTP = qtp
	private String extSys;
	
	public List<ExtSysBuildDetail> buildList;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStart_datetime() {
		return start_datetime;
	}
	public void setStart_datetime(String start_datetime) {
		this.start_datetime = start_datetime;
	}
	public String getEnd_datetime() {
		return end_datetime;
	}
	public void setEnd_datetime(String end_datetime) {
		this.end_datetime = end_datetime;
	}
	
	@JsonProperty("builds")
	public List<ExtSysBuildDetail> getBuildList() {
		return buildList;
	}
	public void setBuildList(List<ExtSysBuildDetail> buildList) {
		this.buildList = buildList;
	}
	
	public String toString() {
		 return title+" "+description+" "+start_datetime+" "+end_datetime; 
	}
	public String getExtSys() {
		return extSys;
	}
	public void setExtSys(String extSys) {
		this.extSys = extSys;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}

package com.xenon.testmanager.domain;

/**
 * 
 * @author suresh.adling
 * @description Domain class to declare getter setter methods for Step in Build
 *              Execution
 * 
 * 
 */
public class StepExecSummary {
	int stexId;
	int testexId;
	int stepId;
	int status;
	int bugSatus;
	String comments;
	byte[] screenshot;

	public int getStexId() {
		return stexId;
	}

	public void setStexId(int stexId) {
		this.stexId = stexId;
	}

	public int getBugSatus() {
		return bugSatus;
	}

	public void setBugSatus(int bugSatus) {
		this.bugSatus = bugSatus;
	}

	public int getTestexId() {
		return testexId;
	}

	public void setTestexId(int testexId) {
		this.testexId = testexId;
	}

	public int getStepId() {
		return stepId;
	}

	public void setStepId(int stepId) {
		this.stepId = stepId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public byte[] getScreenshot() {
		return screenshot;
	}

	public void setScreenshot(byte[] screenshot) {
		this.screenshot = screenshot;
	}
}

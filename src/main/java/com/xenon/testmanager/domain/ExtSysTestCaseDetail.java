package com.xenon.testmanager.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExtSysTestCaseDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2834640890193988227L;
	private String number;
	private int id;
	private int buildId;
	private String title;
	private String description;
	private String expected_result;
	private String actual_result;
	private String lob;
	private int status;
	private String execution_date;
	private int user_id;
	private String start_datetime;
	private String end_datetime;
	
	private List<ExtSysTestStepDetail> testStepList;
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getExpected_result() {
		return expected_result;
	}
	public void setExpected_result(String expected_result) {
		this.expected_result = expected_result;
	}
	public String getActual_result() {
		return actual_result;
	}
	public void setActual_result(String actual_result) {
		this.actual_result = actual_result;
	}
	public String getLob() {
		return lob;
	}
	public void setLob(String lob) {
		this.lob = lob;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getExecution_date() {
		return execution_date;
	}
	public void setExecution_date(String execution_date) {
		this.execution_date = execution_date;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getStart_datetime() {
		return start_datetime;
	}
	public void setStart_datetime(String start_datetime) {
		this.start_datetime = start_datetime;
	}
	public String getEnd_datetime() {
		return end_datetime;
	}
	public void setEnd_datetime(String end_datetime) {
		this.end_datetime = end_datetime;
	}
	
	@JsonProperty("testStepList")
	public List<ExtSysTestStepDetail> getTestStepList() {
		return testStepList;
	}
	public void setTestStepList(List<ExtSysTestStepDetail> testStepList) {
		this.testStepList = testStepList;
	}
	public int getBuildId() {
		return buildId;
	}
	public void setBuildId(int buildId) {
		this.buildId = buildId;
	}
	
	public String toString() {
		 return number+" "+id+" "+buildId+" "+title+" "+description+" "+start_datetime+" "+end_datetime; 
	}
}

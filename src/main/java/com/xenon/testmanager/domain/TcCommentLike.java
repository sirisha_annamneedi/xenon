package com.xenon.testmanager.domain;

public class TcCommentLike {
	
	private int xetmCommentsLikesId; 
	private int commentId ;
	private int likedByUser ;
	public int getXetmCommentsLikesId() {
		return xetmCommentsLikesId;
	}
	public void setXetmCommentsLikesId(int xetmCommentsLikesId) {
		this.xetmCommentsLikesId = xetmCommentsLikesId;
	}
	public int getCommentId() {
		return commentId;
	}
	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}
	public int getLikedByUser() {
		return likedByUser;
	}
	public void setLikedByUser(int likedByUser) {
		this.likedByUser = likedByUser;
	}

}

package com.xenon.testmanager.domain;

import net.rcarz.jiraclient.Component;

public class ComponentDetails {
	private Component componentName;
	private String componentDescription;
	private int componentStatus;
	public Component getComponentName() {
		return componentName;
	}
	public void setComponentName(Component str) {
		this.componentName = str;
	}
	public String getComponentDescription() {
		return componentDescription;
	}
	public void setComponentDescription(String componentDescription) {
		this.componentDescription = componentDescription;
	}
	public int getComponentStatus() {
		return componentStatus;
	}
	public void setComponentStatus(int componentStatus) {
		this.componentStatus = componentStatus;
	}
}

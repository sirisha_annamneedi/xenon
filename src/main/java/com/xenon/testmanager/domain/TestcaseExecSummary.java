package com.xenon.testmanager.domain;

/**
 * 
 * @author suresh.adling
 * @description Domain class to declare getter setter methods for Test Case
 *              Execution Summary
 * 
 * 
 */
public class TestcaseExecSummary {

	int execId;
	int tcId;
	int buildId;
	int projectId;
	int muduleId;
	int scenarioId;
	int testerId;
	int statusId;
	int completeStatus;
	String notes;

	public int getExecId() {
		return execId;
	}

	public void setExecId(int execId) {
		this.execId = execId;
	}

	public int getCompleteStatus() {
		return completeStatus;
	}

	public void setCompleteStatus(int completeStatus) {
		this.completeStatus = completeStatus;
	}

	public int getTcId() {
		return tcId;
	}

	public void setTcId(int tcId) {
		this.tcId = tcId;
	}

	public int getBuildId() {
		return buildId;
	}

	public void setBuildId(int buildId) {
		this.buildId = buildId;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getMuduleId() {
		return muduleId;
	}

	public void setMuduleId(int muduleId) {
		this.muduleId = muduleId;
	}

	public int getScenarioId() {
		return scenarioId;
	}

	public void setScenarioId(int scenarioId) {
		this.scenarioId = scenarioId;
	}

	public int getTesterId() {
		return testerId;
	}

	public void setTesterId(int testerId) {
		this.testerId = testerId;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}

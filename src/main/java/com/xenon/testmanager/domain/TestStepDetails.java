package com.xenon.testmanager.domain;

/**
 * 
 * @author suresh.adling
 * @description Domain class to declare getter setter methods for Test Case
 *              Summary
 * 
 * 
 */
public class TestStepDetails {

	private int stepId;
	private String stepAction;
	private String expectedResult;
	private int testcaseId;
	private int testStepId;

	public void setStepId(int stepId) {
		this.stepId = stepId;
	}

	public int getStepId() {
		return stepId;
	}

	public void setTestcaseId(int testcaseId) {
		this.testcaseId = testcaseId;
	}

	public int getTestcaseId() {
		return testcaseId;
	}

	public void setStepAction(String stepAction) {
		this.stepAction = stepAction;
	}

	public String getStepAction() {
		return stepAction;
	}

	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}

	public String getExpectedResult() {
		return expectedResult;
	}

	public int getTestStepId() {
		return testStepId;
	}

	public void setTestStepId(int testStepId) {
		this.testStepId = testStepId;
	}

}

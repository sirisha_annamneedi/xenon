package com.xenon.testmanager.domain;

import java.util.List;

/**
 * 
 * @author suresh.adling
 * @description Domain class to declare getter setter methods for Test Case
 *              Summary
 * 
 * 
 */
public class TestCaseSummary {

	private int testCaseId;
	private String activityDescription;
	private int user;

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	public List<TestStepDetails> testStepList;

	public void setTestCaseId(int testCaseId) {
		this.testCaseId = testCaseId;
	}

	public int getTestCaseId() {
		return testCaseId;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

	public String getActivityDescription() {
		return activityDescription;
	}

}


package com.xenon.testmanager.domain;

import java.util.List;

/**
 * 
 * @author suresh.adling
 * @description Domain class to declare getter setter methods for Test Case
 * 
 * 
 */
public class TestCaseDetails {

	private int id;
	private String name;
	private String summary;
	private String precondition;
	private int statusId;
	private String status;
	private String executionType;
	private int executionTime;
	private int testScenarioId;
	private int executionTypeId;
	private int authorId;
	private int updaterId;
	private String authorFullName;
	private String testPrefix;
	private int projectId;
	private String testCaseURL;
	private String authorAboutMe;
	private String userPhoto;
	private String createdDate;
	private int datasheetStatus;
	private String redwoodTestCaseId;
	
	public int getDatasheetStatus() {
		return datasheetStatus;
	}

	public void setDatasheetStatus(int datasheetStatus) {
		this.datasheetStatus = datasheetStatus;
	}

	public String getTestPrefix() {
		return testPrefix;
	}

	public void setTestPrefix(String testPrefix) {
		this.testPrefix = testPrefix;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public List<TestStepDetails> testStepList;

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}

	public int getAuthorId() {
		return authorId;
	}

	public void setUpdaterId(int updaterId) {
		this.updaterId = updaterId;
	}

	public int getUpdaterId() {
		return updaterId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getSummary() {
		return summary;
	}

	public void setPrecondition(String precondition) {
		this.precondition = precondition;
	}

	public String getPrecondition() {
		return precondition;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public void setExecutionTime(int executionTime) {
		this.executionTime = executionTime;
	}

	public int getExecutionTime() {
		return executionTime;
	}

	public int getTestScenarioId() {
		return testScenarioId;
	}

	public void setTestScenarioId(int testScenarioId) {
		this.testScenarioId = testScenarioId;
	}

	public int getExecutionTypeId() {
		return executionTypeId;
	}

	public void setExecutionTypeId(int executionTypeId) {
		this.executionTypeId = executionTypeId;
	}

	public void setExecutionType(String executionType) {
		this.executionType = executionType;
	}

	public String getExecutionType() {
		return executionType;
	}

	public String getAuthorAboutMe() {
		return authorAboutMe;
	}

	public void setAuthorAboutMe(String authorAboutMe) {
		this.authorAboutMe = authorAboutMe;
	}

	public List<TestStepDetails> getTestStepList() {
		return testStepList;
	}

	public void setTestStepList(List<TestStepDetails> testStepList) {
		this.testStepList = testStepList;
	}

	public String getAuthorFullName() {
		return authorFullName;
	}

	public void setAuthorFullName(String authorFullName) {
		this.authorFullName = authorFullName;
	}

	public String getTestCaseURL() {
		return testCaseURL;
	}

	public void setTestCaseURL(String testCaseURL) {
		this.testCaseURL = testCaseURL;
	}

	public String getUserPhoto() {
		return this.userPhoto;
	}

	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}

	public String getRedwoodTestCaseId() {
		return redwoodTestCaseId;
	}

	public void setRedwoodTestCaseId(String redwoodTestCaseId) {
		this.redwoodTestCaseId = redwoodTestCaseId;
	}

	@Override
	public String toString() {
		return "TestCaseDetails [id=" + id + ", name=" + name + ", summary=" + summary + ", precondition="
				+ precondition + ", statusId=" + statusId + ", status=" + status + ", executionType=" + executionType
				+ ", executionTime=" + executionTime + ", testScenarioId=" + testScenarioId + ", executionTypeId="
				+ executionTypeId + ", authorId=" + authorId + ", updaterId=" + updaterId + ", authorFullName="
				+ authorFullName + ", testPrefix=" + testPrefix + ", projectId=" + projectId + ", testCaseURL="
				+ testCaseURL + ", authorAboutMe=" + authorAboutMe + ", userPhoto=" + userPhoto + ", createdDate="
				+ createdDate + ", datasheetStatus=" + datasheetStatus + ", redwoodTestCaseId=" + redwoodTestCaseId
				+ ", testStepList=" + testStepList + "]";
	}
	
	
}

package com.xenon.testmanager.domain;

public class TestcaseCommentDetails {
	
	private int xetmTestcaseCommentsId; 
	private String commentDescription;
	private int commentor ;
	private String commentTime;
	private int testcaseId;
	public int getXetmTestcaseCommentsId() {
		return xetmTestcaseCommentsId;
	}
	public void setXetmTestcaseCommentsId(int xetmTestcaseCommentsId) {
		this.xetmTestcaseCommentsId = xetmTestcaseCommentsId;
	}
	public String getCommentDescription() {
		return commentDescription;
	}
	public void setCommentDescription(String commentDescription) {
		this.commentDescription = commentDescription;
	}
	public int getCommentor() {
		return commentor;
	}
	public void setCommentor(int commentor) {
		this.commentor = commentor;
	}
	public int getTestcaseId() {
		return testcaseId;
	}
	public void setTestcaseId(int testcaseId) {
		this.testcaseId = testcaseId;
	}
	public String getCommentTime() {
		return commentTime;
	}
	public void setCommentTime(String commentTime) {
		this.commentTime = commentTime;
	}

}

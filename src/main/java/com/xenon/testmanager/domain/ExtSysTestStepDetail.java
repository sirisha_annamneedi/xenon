package com.xenon.testmanager.domain;

import java.io.Serializable;

public class ExtSysTestStepDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7637864194476480680L;
	private String number;
	private int id;
	private int testCaseId;
	private String title;
	private String description;
	private String expected_result;
	private String actual_result;
	private int status;
	private String screenshot;
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getExpected_result() {
		return expected_result;
	}
	public void setExpected_result(String expected_result) {
		this.expected_result = expected_result;
	}
	public String getActual_result() {
		return actual_result;
	}
	public void setActual_result(String actual_result) {
		this.actual_result = actual_result;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getScreenshot() {
		return screenshot;
	}
	public void setScreenshot(String screenshot) {
		this.screenshot = screenshot;
	}
	public int getTestCaseId() {
		return testCaseId;
	}
	public void setTestCaseId(int testCaseId) {
		this.testCaseId = testCaseId;
	}
	
	
}

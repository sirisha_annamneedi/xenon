package com.xenon.testmanager.dao;

import java.sql.SQLException;

import com.xenon.testmanager.domain.ExtSysBuildDetail;
import com.xenon.testmanager.domain.ExtSysReleaseDetail;
import com.xenon.testmanager.domain.ExtSysTestCaseDetail;
import com.xenon.testmanager.domain.ExtSysTestStepDetail;


public interface QTPReleaseDAO {
	int addReleaseDetails(ExtSysReleaseDetail releaseDetail,String schema) throws SQLException;
	int addBuildDetails(ExtSysBuildDetail buildDetail,String schema) throws SQLException;
	int addTestCaseDetails(ExtSysTestCaseDetail testCaseDetail,String schema) throws SQLException;
	int addTestStepDetails(ExtSysTestStepDetail testStepDetail,String schema) throws SQLException;
	int getReleaseId(ExtSysReleaseDetail releaseDetail,String schema) throws SQLException;
	int getBuildId(int releaseId,String buildId,  String schema) throws SQLException;
}

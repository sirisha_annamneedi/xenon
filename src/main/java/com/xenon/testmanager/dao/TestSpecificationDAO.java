package com.xenon.testmanager.dao;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.xenon.testmanager.domain.TestCaseDetails;

/**
 * 
 * @author suresh.adling
 * @description Interface to perform JDBC operations on test case
 *              Management(scenario,test case)
 * 
 */
public interface TestSpecificationDAO {

	/**
	 * @author suresh.adling
	 * @description get test specifications (list of modules from selected
	 *              project)
	 * @param projectId
	 * @param userId
	 * @param schema
	 * @return List Map
	 */
	Map<String, Object> getTestSpecification(int projectId, int userId, String schema);

	/**
	 * @author suresh.adling
	 * @description get test case data (all test cases for selected scenario)
	 * @param userId
	 * @param projectId
	 * @param moduleId
	 * @param scenarioId
	 * @param schema
	 * @return Multiple Result set for test case
	 */
	Map<String, Object> getTestCaseData(int userId, int projectId, int moduleId, int scenarioId, String schema,
			int startValue, int offsetValue);

	/**
	 * @author suresh.adling
	 * @description get test case steps data(all test case steps for selected
	 *              test case)
	 * @param userId
	 * @param projectId
	 * @param moduleId
	 * @param scenarioId
	 * @param testCaseId
	 * @param schema
	 * @return Multiple Result set for test case step
	 */
	Map<String, Object> getTestCaseStepData(int userId, int projectId, int moduleId, int scenarioId, int testCaseId,
			String schema);

	/**
	 * @author suresh.adling
	 * @description get scenario data (all scenarios for selected module)
	 * @param moduleId
	 * @param schema
	 * @return Multiple Result set for scenario
	 */
	Map<String, Object> getScenarioData(int moduleId, String schema);

	/**
	 * @author suresh.adling
	 * @description get test case List
	 * @param scenarioId
	 * @param projectId
	 * @param schema
	 * @return ScenarioDetails
	 * @throws InvalidResultSetAccessException
	 * @throws UnsupportedEncodingException
	 */
	/*
	 * ScenarioDetails getTestCaseList(int scenarioId, int projectId, String
	 * schema) throws InvalidResultSetAccessException,
	 * UnsupportedEncodingException;
	 */
	/**
	 * @author suresh.adling
	 * @description get test case step List
	 * @param testcaseId
	 * @param schema
	 * @return TestCaseDetails
	 */
	TestCaseDetails getTestStepsList(int testcaseId, String schema);

	int uploadTestcaseDatasheet(int testcaseId, int userId, byte[] fileBytes, String fileName, String schema);

	int deleteExistingTestcaseDatasheet(int testcaseId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method returns datasheet by testcase id
	 * 
	 * @param testcaseId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getDatasheetByTcId(int testcaseId, String schema);

	List<Map<String, Object>> getDatasheetByBuildId(int buildId, String schema);

	List<Map<String, Object>> getLastModule(Integer projectId, Integer userId, String schema);

	List<Map<String, Object>> getLastScenario(Integer moduleId, String schema);

	List<Map<String, Object>> getModuleTrash(String schema, int projectId);

	List<Map<String, Object>> getScenarioTrash(String schema, int projectId);

	List<Map<String, Object>> getTestCaseTrash(String schema, int projectId);

	int restoreTestcaseForTrash(int testcaseId, String schema);

	void saveFilterValues(String inputValue, String commonData,Integer userId,String schema);



}

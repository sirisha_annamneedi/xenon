package com.xenon.testmanager.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import com.xenon.testmanager.domain.StepExecSummary;

/**
 * 
 * @author suresh.adling
 * @description DAO implementation class to perform JDBC operations on test case
 *              steps
 * 
 */
public class TestcaseStepDAOImpl implements TestcaseStepDAO, XenonQuery {

	@Autowired
	DatabaseDao database;

	private static final Logger logger = LoggerFactory.getLogger(TestcaseStepDAOImpl.class);

	@Override
	public List<Map<String, Object>> getAllTestcaseSteps(String schema) {
		List<Map<String, Object>> testcaseList;
		logger.info("In method get All Test Case Steps ");
		logger.info("SQL " + testCaseStep.getAllTestcaseSteps);
		testcaseList = database.select(String.format(testCaseStep.getAllTestcaseSteps, schema));
		logger.info("Result " + testcaseList);
		return testcaseList;
	}

	@Override
	public int insertTestStepSummary(String schema, StepExecSummary stepExecSummary) {
		logger.info("In method get All Test Case Steps ");
		logger.info("SQL " + testCaseStep.insertStepExecSummary);
		database.update(String.format(testCaseStep.insertStepExecSummary, schema),
				new Object[] { stepExecSummary.getStexId(), stepExecSummary.getStepId(), stepExecSummary.getStatus(),
						stepExecSummary.getComments(), stepExecSummary.getScreenshot(), stepExecSummary.getBugSatus() },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.BLOB, Types.INTEGER, });
		return getStepExeIdByStepId(stepExecSummary.getStexId(), stepExecSummary.getStepId(), schema);

	}

	@Override
	public int updateTestStepSummary(String schema, StepExecSummary stepExecSummary) {
		logger.info("In method get All Test Case Steps ");
		logger.info("SQL " + testCaseStep.updateStepExecSummary);
		database.update(String.format(testCaseStep.updateStepExecSummary, schema),
				new Object[] { stepExecSummary.getStatus(), stepExecSummary.getComments(),
						stepExecSummary.getScreenshot(), stepExecSummary.getBugSatus(), stepExecSummary.getStexId(),
						stepExecSummary.getStepId() },
				new int[] { Types.INTEGER, Types.VARCHAR, Types.BLOB, Types.INTEGER, Types.INTEGER, Types.INTEGER });
		return getStepExeIdByStepId(stepExecSummary.getStexId(), stepExecSummary.getStepId(), schema);
	}

	@Override
	public int getStepExeIdByStepId(int testExeId, int stepId, String schema) {
		logger.info("In method get Test Case Step Execution Id By Step Id ");
		logger.info("SQL " + testCaseStep.getstepExecutionId);
		List<Map<String, Object>> testcaseStepList;
		testcaseStepList = database.select(String.format(testCaseStep.getstepExecutionId, schema),
				new Object[] { testExeId, stepId }, new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Result " + testcaseStepList.get(0).get("stex_id").toString());
		return Integer.parseInt(testcaseStepList.get(0).get("stex_id").toString());
	}

	@Override
	public int insertStepExeBug(int buildId, int stepExeId, int stepId, int bugId, int testcaseId, String schema) {
		logger.info("In method isert Step Execution Bug  ");
		logger.info("SQL " + testCaseStep.insertStepExecBug);
		int flag = database.update(String.format(testCaseStep.insertStepExecBug, schema),
				new Object[] { buildId, stepExeId, stepId, bugId, testcaseId },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER });
		logger.info("Result " + flag);
		return flag;

	}

	@Override
	public List<Map<String, Object>> getAllEexcuteBugs(int buildId, String schema) {
		logger.info("In method get All Execute bugs ");
		logger.info("SQL " + testCaseStep.getAllExecutedBugs);
		String sql = String.format(testCaseStep.getAllExecutedBugs, schema, schema);
		List<Map<String, Object>> details = database.select(sql, new Object[] { buildId }, new int[] { Types.INTEGER });
		logger.info("Result " + details);
		return details;
	}
}

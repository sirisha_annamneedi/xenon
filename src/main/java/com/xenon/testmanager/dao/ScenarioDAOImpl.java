package com.xenon.testmanager.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import com.xenon.testmanager.domain.ComponentDetails;
import com.xenon.testmanager.domain.ScenarioDetails;
import com.xenon.testmanager.domain.TestCaseDetails;

import net.rcarz.jiraclient.Component;

/**
 * 
 * @author suresh.adling,,bhagyashri.ajmera
 * @description DAO implementation class to perform JDBC operations on scenario
 * 
 */
public class ScenarioDAOImpl implements ScenarioDAO, XenonQuery {

	private static final Logger logger = LoggerFactory.getLogger(ScenarioDAOImpl.class);

	@Autowired
	DatabaseDao database;

	@Autowired
	TestcaseDAO testcaseDao;

	@Override
	public List<Map<String, Object>> getAllScenarios(String schema) {
		logger.info("In method get All Scenarios ");
		logger.info("SQL " + scenario.getAllScenarios);
		List<Map<String, Object>> scenarioList;
		scenarioList = database.select(String.format(scenario.getAllScenarios, schema));
		logger.info("Result " + scenarioList);
		return scenarioList;
	}

	@Override
	public List<Map<String, Object>> getAllScenariosWithTcCount(String schema) {
		logger.info("In method get All Test Case counts by Scenario ");
		logger.info("SQL " + scenario.getAllScenarios);
		List<Map<String, Object>> scenarioList;
		scenarioList = database.select(String.format(scenario.getAllScenarios, schema));

		for (int i = 0; i < scenarioList.size(); i++) {
			int scenarioId = Integer.parseInt(scenarioList.get(i).get("scenario_id").toString());
			int tcCount = (testcaseDao.getTestcasesByScenarioId(scenarioId, schema)).size();
			scenarioList.get(i).put("testCasesCount", tcCount);
		}
		logger.info("Result " + scenarioList);
		return scenarioList;
	}

	@Override
	public int insertScenario(ScenarioDetails scenarios, String schema) throws SQLException {
		logger.info("In method  insert a scenario ");
		logger.info("SQL " + scenario.insertScenario);
		int flag = database.update(String.format(scenario.insertScenario, schema),
				new Object[] { scenarios.getScenarioName(), scenarios.getScenarioDescription(), scenarios.getModuleId(),
						scenarios.getStatus(), null, null, null },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.VARCHAR,
						Types.VARCHAR });
		return flag;

	}

	@Override
	public int insertComponent(ComponentDetails component, String schema) {
		logger.info("In method  insert a component ");
		logger.info("SQL " + scenario.insertComponent);
		int flag = 0;
		List<Map<String, Object>> componentDetails = database.select(String.format(scenario.getComponentName, schema),
				new Object[] { component.getComponentName() }, new int[] { Types.VARCHAR });
		if (componentDetails.isEmpty()) {
			flag = database.update(String.format(scenario.insertComponent, schema),
					new Object[] { component.getComponentName(), component.getComponentDescription(),
							component.getComponentStatus() },
					new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER });
		} else if (!componentDetails.isEmpty())
			flag = 1;
		else
			flag = 0;

		return flag;

	}

	@Override
	public int insertScenarioComponent(int componentId, int scenarioId, String schema) {
		logger.info("In method  insert a scenario component");
		logger.info("SQL " + scenario.insertScenarioComponent);
		int flag = database.update(String.format(scenario.insertScenarioComponent, schema),
				new Object[] { componentId, scenarioId }, new int[] { Types.INTEGER, Types.INTEGER });
		return flag;

	}

	@Override
	public int insertJiraStory(ScenarioDetails scenarios, String schema) throws SQLException {
		logger.info("In method  insert a scenario with jira story ");
		logger.info("SQL " + scenario.insertScenario);
		int flag = database.update(String.format(scenario.insertScenario, schema),
				new Object[] { scenarios.getScenarioName(), scenarios.getScenarioDescription(), scenarios.getModuleId(),
						scenarios.getStatus(), scenarios.getJiraStoryId(), scenarios.getJiraStory(),
						scenarios.getFixVersion() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.VARCHAR,
						Types.VARCHAR });
		return flag;

	}

	@Override
	public int updateScenario(ScenarioDetails scenarios, String schema) {
		logger.info("In method update scenario details ");
		logger.info("SQL " + scenario.updateScenario);
		int flag = database.update(String.format(scenario.updateScenario, schema),
				new Object[] { scenarios.getScenarioName(), scenarios.getScenarioDescription(), null, null,
						scenarios.getScenarioId() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER });
		return flag;
	}

	@Override
	public int updateScenarioStatus(int scenarioStatus, int scenarioId, String schema) {
		logger.info("In method ScenarioDAOImpl.updateScenarioStatus()");
		logger.info("SQL " + scenario.updateScenarioStatus);
		int flag = database.update(String.format(scenario.updateScenarioStatus, schema),
				new Object[] { scenarioStatus, scenarioId }, new int[] { Types.INTEGER, Types.INTEGER });
		return flag;
	}

	@Override
	public int updateJiraScenario(ScenarioDetails scenarios, String schema) {
		logger.info("In method update scenario details ");
		logger.info("SQL " + scenario.updateScenario);
		int flag = database.update(String.format(scenario.updateScenario, schema),
				new Object[] { scenarios.getScenarioName(), scenarios.getScenarioDescription(),
						scenarios.getJiraStoryId(), scenarios.getJiraStory(), scenarios.getScenarioId() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER });
		return flag;
	}

	@Override
	public List<Map<String, Object>> getScenarioDetails(int scenarioId, String schema) {
		logger.info("In method get Scenario Details By Id ");
		logger.info("SQL " + scenario.scenarioDetails);
		List<Map<String, Object>> scenarioDetails = database.select(String.format(scenario.scenarioDetails, schema),
				new Object[] { scenarioId }, new int[] { Types.INTEGER });
		logger.info("Result " + scenarioDetails);
		return scenarioDetails;
	}

	@Override
	public List<Map<String, Object>> selectScenarioDetailsByBuildId(int buildId, String schema) {
		logger.info("reads scenario details by build id");
		String sql = String.format(scenario.selectScenarioDetailsByBuildId, schema, schema, schema);
		List<Map<String, Object>> scenarioDetails = database.select(sql, new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("reads pass,fail, skip and block count with total TC counts");
		for (int i = 0; i < scenarioDetails.size(); i++) {
			List<Map<String, Object>> moduleTcDetails = testcaseDao.getTestCaseDetailsByBuildAndScenarioId(buildId,
					Integer.parseInt(scenarioDetails.get(i).get("scenario_id").toString()), schema);
			List<Map<String, Object>> moduleExecTcDetails = testcaseDao.getExecutedTestCaseDetailsByBuildAndScenarioId(
					buildId, Integer.parseInt(scenarioDetails.get(i).get("scenario_id").toString()), schema);
			scenarioDetails.get(i).put("TcCount", moduleTcDetails.get(0).get("TcCount"));
			scenarioDetails.get(i).put("ExecutedTcCount", moduleExecTcDetails.get(0).get("TcCount"));
			if (Integer.parseInt(moduleExecTcDetails.get(0).get("TcCount").toString()) == 0) {
				scenarioDetails.get(i).put("PassCount", 0);
				scenarioDetails.get(i).put("FailCount", 0);
				scenarioDetails.get(i).put("SkipCount", 0);
				scenarioDetails.get(i).put("BlockCount", 0);
			} else {
				scenarioDetails.get(i).put("PassCount", moduleExecTcDetails.get(0).get("PassCount"));
				scenarioDetails.get(i).put("FailCount", moduleExecTcDetails.get(0).get("FailCount"));
				scenarioDetails.get(i).put("SkipCount", moduleExecTcDetails.get(0).get("SkipCount"));
				scenarioDetails.get(i).put("BlockCount", moduleExecTcDetails.get(0).get("BlockCount"));
			}
		}
		logger.info("Return list =" + scenarioDetails);
		return scenarioDetails;
	}

	@Override
	public int createNewScenarioStatus(String schema, int custTypeId) throws SQLException {
		logger.info("checks scenario create limit exceeds for customer or not");
		String createNewScenarioStatus = String.format(scenario.createNewScenarioStatus, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(createNewScenarioStatus);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setInt(2, custTypeId);
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("return value insertStatus = " + insertStatus);
		return insertStatus;

	}

	@Override
	public List<Map<String, Object>> selectScenarioDetailsByBuildId(List<Map<String, Object>> scenarioDetails,
			List<Map<String, Object>> scenarioTcDetails, List<Map<String, Object>> scenarioExecTcDetails) {
		logger.info("selects scenario details by build id");
		logger.info("reads pass,fail, skip and block count with total and executed TC counts");
		for (int i = 0; i < scenarioDetails.size(); i++) {

			int moduleId = Integer.parseInt(scenarioDetails.get(i).get("scenario_id").toString());

			for (int j = 0; j < scenarioTcDetails.size(); j++) {
				if (moduleId == Integer.parseInt(scenarioTcDetails.get(j).get("scenario_id").toString())) {
					scenarioDetails.get(i).put("TcCount", scenarioTcDetails.get(j).get("TcCount"));
					scenarioDetails.get(i).put("ExecutedTcCount", 0);
					scenarioDetails.get(i).put("PassCount", 0);
					scenarioDetails.get(i).put("FailCount", 0);
					scenarioDetails.get(i).put("SkipCount", 0);
					scenarioDetails.get(i).put("BlockCount", 0);
					break;
				}
			}

			for (int j = 0; j < scenarioExecTcDetails.size(); j++) {
				if (moduleId == Integer.parseInt(scenarioExecTcDetails.get(j).get("scenario_id").toString())) {
					if (Integer.parseInt(scenarioExecTcDetails.get(j).get("TcCount").toString()) == 0) {
						scenarioDetails.get(i).put("PassCount", 0);
						scenarioDetails.get(i).put("FailCount", 0);
						scenarioDetails.get(i).put("SkipCount", 0);
						scenarioDetails.get(i).put("BlockCount", 0);
					} else {
						scenarioDetails.get(i).put("PassCount", scenarioExecTcDetails.get(j).get("PassCount"));
						scenarioDetails.get(i).put("FailCount", scenarioExecTcDetails.get(j).get("FailCount"));
						scenarioDetails.get(i).put("SkipCount", scenarioExecTcDetails.get(j).get("SkipCount"));
						scenarioDetails.get(i).put("BlockCount", scenarioExecTcDetails.get(j).get("BlockCount"));
					}
					scenarioDetails.get(i).put("ExecutedTcCount", scenarioExecTcDetails.get(j).get("TcCount"));
					break;
				} else {
					scenarioDetails.get(i).put("ExecutedTcCount", 0);
					scenarioDetails.get(i).put("PassCount", 0);
					scenarioDetails.get(i).put("FailCount", 0);
					scenarioDetails.get(i).put("SkipCount", 0);
					scenarioDetails.get(i).put("BlockCount", 0);
				}
			}
		}
		logger.info("Return list =" + scenarioDetails);
		return scenarioDetails;
	}

	@Override
	public int getScenarioId(String scenarioName, int moduleId, String schema) {
		logger.info("In method update scenario details ");
		logger.info("SQL " + scenario.updateScenario);
		List<Map<String, Object>> scenarioDetails = database.select(String.format(scenario.getScenarioId, schema),
				new Object[] { scenarioName, moduleId }, new int[] { Types.VARCHAR, Types.INTEGER });
		int scenarioId = Integer.parseInt(scenarioDetails.get(0).get("scenario_id").toString());
		return scenarioId;
	}

	@Override
	public int getComponentId(Component ComponentName, String schema) {
		logger.info("In method update scenario details ");
		logger.info("SQL " + scenario.getComponentId);
		List<Map<String, Object>> componentDetails = database.select(String.format(scenario.getComponentId, schema),
				new Object[] { ComponentName }, new int[] { Types.VARCHAR });
		int componentId = Integer.parseInt(componentDetails.get(0).get("component_id").toString());
		return componentId;
	}

	@Override
	public int getScenarioIds(ScenarioDetails scenarios, String schema) {
		logger.info("In method update scenarios details ");
		List<Map<String, Object>> scenarioDetails = database.select(
				String.format(scenario.getScenarioIds, schema, schema),
				new Object[] { scenarios.getScenarioName(), scenarios.getModuleId() },
				new int[] { Types.VARCHAR, Types.INTEGER });
		int scenarioId = Integer.parseInt(scenarioDetails.get(0).get("scenario_id").toString());
		return scenarioId;
	}

	@Override
	public int deleteScenario(int scenarioId, String schema) {
		String sql = String.format(scenario.deleteScenario, schema);
		int flag = database.update(sql, new Object[] { scenarioId }, new int[] { Types.INTEGER });
		logger.info("Query result = " + flag);
		return flag;
	}

	/** @author anmol.chadha **/
	@Override
	public int restoreScenarioForTrash(int moduleId, int scenarioId, String schema) {
		logger.info("In method get trash test case");
		int updatedModules = 0, updatedScenarios = 0;
		updatedScenarios = database
				.update("UPDATE " + schema + ".xetm_scenario SET status = 1 WHERE scenario_id=" + scenarioId);
		if (updatedScenarios > 0)
			updatedModules = database
					.update("UPDATE " + schema + ".xe_module SET module_active = 1 WHERE module_id=" + moduleId);
		return updatedModules;
	}

}

package com.xenon.testmanager.dao;

import java.util.List;
import java.util.Map;
/**
 * 
 * @author suresh.adling
 * @description Interface to perform JDBC operations to get data for test manager dashboard
 * 
 */
public interface TmDashboardDAO {
	/**
	 * @author suresh.adling
	 * @description get all dashboard counts
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> getAllCounts(String schema);
	/**
	 * @author suresh.adling
	 * @description get test case counts for each project
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getProjectwiseCounts(String schema);
	/**
	 * @author suresh.adling
	 * @description get test case counts for each module
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getModuleTestCaseCounts(String schema);
	/**
	 * @author suresh.adling
	 * @description get data for test manager dashboard
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTMDashboard(String schema);
	/**
	 * @author suresh.adling
	 * @description get data for test manager dashboard by  user id
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getTMDashboardData(int userId,String schema);
	/**
	 * @author suresh.adling
	 * @description get data for test manager dashboard by project and user id
	 * @param projectId
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getTmProjectDashboard(int projectId,int userId,String schema);
	
	public boolean checkTMRoleAccess(String roleAccess,String roleId,String schema);
	
}

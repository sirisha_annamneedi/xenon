package com.xenon.testmanager.dao;

import java.io.UnsupportedEncodingException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.api.common.UtilsService;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import com.xenon.testmanager.domain.TcCommentLike;
import com.xenon.testmanager.domain.TestCaseSummary;
import com.xenon.testmanager.domain.TestcaseCommentDetails;

public class TestcaseCommentsDAOImpl implements TestcaseCommentsDAO, XenonQuery {

	private static final Logger logger = LoggerFactory.getLogger(TestcaseCommentsDAOImpl.class);
	
	@Autowired
	DatabaseDao database;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public List<Map<String, Object>> getTestcaseCommentByTcId(int userId, int testcaseId, String schema)
			throws UnsupportedEncodingException {
		logger.info("Reads test case comments by TC id");
		List<Map<String, Object>> testcaseList;
		testcaseList = database.select(String.format(testComments.getTestcaseCommentByTcId, schema, schema),
				new Object[] { testcaseId }, new int[] { Types.INTEGER });
		List<Map<String, Object>> tcCommentLike = getCommentLikeByUserId(userId, schema);
		List<Map<String, Object>> tcCommentLikeCounts = getCommentLikeCountByTcId(testcaseId, schema);
		for (int i = 0; i < testcaseList.size(); i++) {
			testcaseList.get(i).put("user_image", utilsService.getFileData((byte[]) testcaseList.get(i).get("user_photo")));
			testcaseList.get(i).put("likeStatus", "2");
			for (int j = 0; j < tcCommentLike.size(); j++) {
				if (Integer.parseInt(testcaseList.get(i).get("xetm_testcase_comments_id").toString()) == Integer
						.parseInt(tcCommentLike.get(j).get("comment_id").toString())
						&& Integer.parseInt(tcCommentLike.get(j).get("liked_by_user").toString()) == userId) {
					testcaseList.get(i).put("likeStatus", "1");
					break;
				}

			}
		}

		for (int i = 0; i < testcaseList.size(); i++) {
			for (int j = 0; j < tcCommentLikeCounts.size(); j++) {
				testcaseList.get(i).put("likeCount", "0");
				if (Integer.parseInt(testcaseList.get(i).get("xetm_testcase_comments_id").toString()) == Integer
						.parseInt(tcCommentLikeCounts.get(j).get("comment_id").toString())) {
					testcaseList.get(i).put("likeCount",
							Integer.parseInt(tcCommentLikeCounts.get(j).get("likeCount").toString()));
					break;
				}
			}
		}
		logger.info("Return list = "+testcaseList);
		return testcaseList;
	}

	@Override
	public int insertTestcaseComment(String schema, TestcaseCommentDetails testcaseCommentDetails) {
		logger.info("Inserts test case comment to database");
		logger.info("Query param `comment_description` = {}, `commnetor` = {}, `testcase_id` = {}",testcaseCommentDetails.getCommentDescription(), testcaseCommentDetails.getCommentor(),
						testcaseCommentDetails.getTestcaseId());
		int flag = database.update(String.format(testComments.insertTestcaseComment, schema),
				new Object[] { testcaseCommentDetails.getCommentDescription(), testcaseCommentDetails.getCommentor(),
						testcaseCommentDetails.getTestcaseId() },
				new int[] { Types.VARCHAR, Types.INTEGER, Types.INTEGER });
		logger.info("Return flag ="+flag);
		return flag;

	}

	@Override
	public int insertTestcaseSummary(String schema, TestCaseSummary summary) {
		int flag = database.update(String.format(testComments.insertTestcaseSummary, schema),
				new Object[] { summary.getTestCaseId(), summary.getActivityDescription(), summary.getUser() },
				new int[] { Types.INTEGER, Types.VARCHAR, Types.INTEGER });
		return flag;

	}

	@Override
	public int insertTestcaseCommentLike(String schema, TcCommentLike tcCommentLike) {
		logger.info("Inserts test case comments like to database");
		logger.info("Query Param `comment_id`= {} ,  `liked_by_user` = {}", tcCommentLike.getCommentId(), tcCommentLike.getLikedByUser());
		int flag = database.update(String.format(testComments.insertCommentLike, schema),
				new Object[] { tcCommentLike.getCommentId(), tcCommentLike.getLikedByUser() },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Return flag ="+flag);
		return flag;

	}

	@Override
	public int removeCommentLike(String schema, TcCommentLike tcCommentLike) {
		logger.info("Removes test case comment like");
		logger.info("Query Param  comment_id={} and liked_by_user={}",tcCommentLike.getCommentId(), tcCommentLike.getLikedByUser() );
		int flag = database.update(String.format(testComments.removeCommentLike, schema),
				new Object[] { tcCommentLike.getCommentId(), tcCommentLike.getLikedByUser() },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Return flag ="+flag);
		return flag;

	}

	@Override
	public List<Map<String, Object>> getCommentLikeByUserId(int userId, String schema)
			 {
		logger.info("Reads comment like by user id ");
		logger.info("Query param userId = {}",userId);
		List<Map<String, Object>> testcaseList;
		testcaseList = database.select(String.format(testComments.getCommentLikeByUserId, schema),
				new Object[] { userId }, new int[] { Types.INTEGER });
		logger.info("Return list = "+testcaseList);
		return testcaseList;
	}

	@Override
	public List<Map<String, Object>> getCommentLikeCountByTcId(int testcaseId, String schema) {
		logger.info("Reads comments like count by test case id");
		logger.info("Query param testcaseId= {}",testcaseId);
		List<Map<String, Object>> testcaseList;
		testcaseList = database.select(String.format(testComments.getCommentLikeCountByTcId, schema, schema),
				new Object[] { testcaseId }, new int[] { Types.INTEGER });
		logger.info("Return list = "+testcaseList);
		return testcaseList;
	}
	
	@Override
	public Map<String, Object> insertTestcaseComment(TestcaseCommentDetails testcaseCommentDetails, String schema) {
		logger.info("reads optimized edit user data");
		SqlParameter commentDescriptionTemp = new SqlParameter("commentDescription", Types.VARCHAR);
		SqlParameter commentorIdTemp = new SqlParameter("commentorId", Types.INTEGER);
		SqlParameter testcaseIdTemp = new SqlParameter("testcaseId", Types.INTEGER);
		SqlParameter currentTime =new SqlParameter("currentTime",Types.VARCHAR);
		
		SqlParameter[] paramArray = { commentDescriptionTemp,commentorIdTemp,testcaseIdTemp,currentTime };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("insertTestcaseComment")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(testcaseCommentDetails.getCommentDescription(),testcaseCommentDetails.getCommentor(),testcaseCommentDetails.getTestcaseId(),testcaseCommentDetails.getCommentTime());
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> insertTestcaseCommentLike(TcCommentLike tcCommentLike, String schema) {
		logger.info("reads optimized edit user data");
		SqlParameter commentIdTemp = new SqlParameter("commentId", Types.INTEGER);
		SqlParameter likedByUserTemp = new SqlParameter("likedByUser", Types.INTEGER);
		SqlParameter[] paramArray = { commentIdTemp,likedByUserTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("insertTestcaseCommentLike")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(tcCommentLike.getCommentId(),tcCommentLike.getLikedByUser());
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	
	

}

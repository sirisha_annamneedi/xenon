package com.xenon.testmanager.dao;

import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import com.xenon.testmanager.domain.TestCaseDetails;
import com.xenon.testmanager.domain.TestDataDetails;
import com.xenon.testmanager.domain.TestStepDetails;
import com.xenon.testmanager.domain.TestcaseExecSummary;

/**
 * 
 * @author suresh.adling,,bhagyashri.ajmera
 * @description DAO implementation class to perform JDBC operations on scenario
 * 
 */
public class TestcaseDAOImpl implements TestcaseDAO, XenonQuery {

	private static final Logger logger = LoggerFactory.getLogger(TestcaseDAOImpl.class);

	@Autowired
	DatabaseDao database;
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public List<Map<String, Object>> getTestcasesByProjectId(int projectId, String schema) {
		logger.info("In method get test case deatils by project id");
		List<Map<String, Object>> testcaseList;
		testcaseList = database.select(String.format(testcase.getTestcasesByProjectId, schema, schema, schema),
				new Object[] { projectId }, new int[] { Types.INTEGER });
		return testcaseList;
	}

	@Override
	public List<Map<String, Object>> getTestcasesByModuleId(int moduleId, String schema) {
		logger.info("In method get test case deatils by module id");
		List<Map<String, Object>> testcaseList;
		testcaseList = database.select(String.format(testcase.getTestcasesByModuleId, schema, schema),
				new Object[] { moduleId }, new int[] { Types.INTEGER });
		return testcaseList;
	}

	@Override
	public List<Map<String, Object>> getTestcasesByScenarioId(int scenarioId, String schema) {
		logger.info("In method get test case deatils by scenario id");
		List<Map<String, Object>> testcaseList;
		testcaseList = database.select(String.format(testcase.getTestcasesByScenarioId, schema),
				new Object[] { scenarioId }, new int[] { Types.INTEGER });
		return testcaseList;
	}

	@Override
	public List<Map<String, Object>> getActiveExecutionTypes(String schema) {
		logger.info("In method get active build execution types");
		List<Map<String, Object>> executionTypes;
		executionTypes = database.select(String.format(testcase.activeExecutionTypes, schema));
		return executionTypes;
	}

	@Override
	public int insertTestcase(int projectId, int moduleId, TestCaseDetails testcases, String schema) {
		logger.info("In method insert a test caset");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		testcases.setCreatedDate(dateFormat.format(date));
		int flag = database.update(String.format(testcase.insertTestcase, schema),
				new Object[] { projectId, moduleId, testcases.getName(), testcases.getTestScenarioId(),
						testcases.getSummary(), testcases.getPrecondition(), testcases.getStatusId(),
						testcases.getExecutionTypeId(), testcases.getExecutionTime(), testcases.getAuthorId(),
						testcases.getAuthorId(), testcases.getCreatedDate() ,testcases.getDatasheetStatus(), testcases.getRedwoodTestCaseId()},
				new int[] { Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.VARCHAR, Types.VARCHAR,
						Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR});
		return flag;
	}

	@Override
	public int isRedwoodTestCaseIdAlreadyMapped(String redwoodTestCaseId, String schema) {
		// TODO Auto-generated method stub
		logger.info("TestcaseDAOImpl.isRedwoodTestCaseIdMapped()");
		return database.select(String.format(testcase.IsRedwoodTestCaseIdAlreadyMapped, schema), new String[] {redwoodTestCaseId});
	}
	
	@Override
	public int getTestCaseId(TestCaseDetails testcases, String schema) {
		logger.info("In method get test case id from details");
		List<Map<String, Object>> testCase = database.select(String.format(testcase.getTestCaseId, schema),
				new Object[] { testcases.getName(), testcases.getTestScenarioId(), testcases.getSummary(),
						testcases.getPrecondition(), testcases.getAuthorId(), testcases.getCreatedDate() },
				new int[] { Types.VARCHAR, Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.VARCHAR });
		int testCaseId = Integer.parseInt(testCase.get(0).get("testcase_id").toString());
		return testCaseId;
	}

	@Override
	public List<Map<String, Object>> getTestRemoveStatus(int userId, String schema) {
		logger.info("In method get test remove status");
		List<Map<String, Object>> testRemoveStatus;
		testRemoveStatus = database.select(String.format(testcase.getTestRemoveStatus, schema, schema),
				new Object[] { userId }, new int[] { Types.INTEGER });
		return testRemoveStatus;
	}

	@Override
	public List<Map<String, Object>> getAllTestcases(int buildId, int user_id, String schema) {
		logger.info("In method get test cases by build id");
		List<Map<String, Object>> testcaseList;

		testcaseList = database.select(String.format(testcase.getAllTestcases, schema, schema),
				new Object[] { user_id }, new int[] { Types.INTEGER });

		for (int i = 0; i < testcaseList.size(); i++) {

			int testcase_id = Integer.parseInt(testcaseList.get(i).get("testcase_id").toString());
			List<Map<String, Object>> status = getTestCaseStatus(testcase_id, buildId, schema);

			List<Map<String, Object>> exeStatus = getTestExeStatus(testcase_id, buildId, schema);

			if (status.size() == 0) {
				testcaseList.get(i).put("testStatus", 0);
			} else if (status.size() != 0 && exeStatus.size() != 0) {
				testcaseList.get(i).put("testStatus", 1);

			} else {
				testcaseList.get(i).put("testStatus", 2);
			}

		}

		return testcaseList;
	}

	@Override
	public List<Map<String, Object>> getTestCaseStatus(int testcase_id, int buildId, String schema) {
		logger.info("In method get test case status");
		List<Map<String, Object>> testStatus;
		testStatus = database.select(String.format(testcase.getTestStatus, schema),
				new Object[] { testcase_id, buildId }, new int[] { Types.INTEGER, Types.INTEGER });
		return testStatus;
	}
	@Override
	public List<Map<String, Object>> getTestCaseData(String testcases,String schema) {
		logger.info("In method get test case data");
		List<Map<String, Object>> testDatas;
		testDatas = database.select("SELECT t.test_prefix,td.data FROM "+schema+".xetm_testcase_data td,"+schema+".xetm_testcase t WHERE t.testcase_id=td.testcase_id and td.testcase_id in ("+testcases+")");
		return testDatas;
	}
	@Override
	public List<Map<String, Object>> getTestCaseDataByBuildId(Integer bulidId,String schema) {
		logger.info("In method get test case data");
		List<Map<String, Object>> testDatas;
		testDatas = database.select("SELECT t.test_prefix,td.data FROM "+schema+".xetm_testcase_data td,"+schema+".xetm_testcase t WHERE t.testcase_id=td.testcase_id and td.testcase_id in (SELECT testcase_id FROM "+schema+".xebm_automation_build_testcase WHERE build_id="+bulidId+")");
		return testDatas;
	}
	@Override
	public List<Map<String, Object>> getTestExeStatus(int testcase_id, int buildId, String schema) {
		logger.info("In method get Test execution status list");
		List<Map<String, Object>> testStatus;
		testStatus = database.select(String.format(testcase.getTestExe, schema), new Object[] { testcase_id, buildId },
				new int[] { Types.INTEGER, Types.INTEGER });
		return testStatus;
	}

	@Override
	public int updateTestcase(TestCaseDetails testcases, String schema) {
		logger.info("In method update test case details");
		int flag = database.update(String.format(testcase.updateTestcase, schema),
				new Object[] { testcases.getName(), testcases.getSummary(), testcases.getPrecondition(),
						testcases.getStatusId(), testcases.getExecutionTypeId(), testcases.getExecutionTime(),
						testcases.getUpdaterId(), testcases.getId() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.INTEGER, Types.INTEGER });
		return flag;
	}

	@Override
	public List<Map<String, Object>> getTestcaseDetails(int testcaseId, String schema) {
		logger.info("In method get test case details by test case id");
		List<Map<String, Object>> testcaseList;
		testcaseList = database.select(String.format(testcase.getTestcaseDetails, schema), new Object[] { testcaseId },
				new int[] { Types.INTEGER });
		return testcaseList;
	}

	@Override
	public List<Map<String, Object>> getStatusList(String schema) {
		logger.info("In method get test case step status list");
		List<Map<String, Object>> statusList;
		statusList = database.select(String.format(testcase.getStatus, schema));
		return statusList;
	}

	@Override
	public int insertStep(TestStepDetails step, String schema) {
		logger.info("In method insert test case step ");
		int flag = database.update(String.format(testcase.insertStep, schema),
				new Object[] { step.getTestcaseId(), step.getStepAction(), step.getExpectedResult(),
						step.getTestStepId() },
				new int[] { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.INTEGER });
		return flag;
	}
	@Override
	public int insertData(TestDataDetails data, String schema) {
		logger.info("In method insert test case data ");
		int flag = database.update(String.format(testcase.insertData, schema),
				new Object[] {data.getTestcaseId(),data.getUserId(),data.getData()},
				new int[] { Types.INTEGER,Types.INTEGER, Types.VARCHAR });
		return flag;
	}
	@Override
	public int updateData(TestDataDetails data, String schema) {
		logger.info("In method update test case data ");
		int flag = database.update(String.format(testcase.updateData, schema),
				new Object[] {data.getUserId(),data.getData(),data.getTestcaseId()},
				new int[] { Types.INTEGER, Types.VARCHAR, Types.INTEGER});
		return flag;
	}
	@Override
	public int updateStep(TestStepDetails step, String schema) {
		logger.info("In method update step details");
		int flag = database.update(String.format(testcase.updateStep, schema),
				new Object[] { step.getStepAction(), step.getExpectedResult(), step.getStepId(), step.getTestStepId() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER });
		return flag;
	}

	@Override
	public List<Map<String, Object>> selectTestCaseDetailsByBuildId(int buildId, String schema) {
		logger.info("selects test case details by build id");
		logger.info("Query param buildId = {}", buildId);
		String sql = String.format(testcase.selectTestCaseDetailsByBuildId, schema, schema);
		List<Map<String, Object>> testcaseDetails = database.select(sql, new Object[] { buildId },
				new int[] { Types.INTEGER });
		return testcaseDetails;
	}

	@Override
	public List<Map<String, Object>> getProjectModuleScenarioIdByTcId(int testcaseId, String schema) {
		List<Map<String, Object>> testcaseDetails = database.select(
				String.format(testcase.getProjectModuleScenarioIdByTcId, schema, schema, schema),
				new Object[] { testcaseId }, new int[] { Types.INTEGER });
		return testcaseDetails;
	}

	@Override
	public int insertTestcaseExecSummary(TestcaseExecSummary testcases, String schema) {
		int flag = database.update(String.format(testcase.insertTestCaseExecSummary, schema),
				new Object[] { testcases.getTcId(), testcases.getBuildId(), testcases.getProjectId(),
						testcases.getMuduleId(), testcases.getScenarioId(), testcases.getTesterId(),
						testcases.getStatusId(), testcases.getNotes(), testcases.getCompleteStatus() },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.INTEGER, Types.VARCHAR, Types.INTEGER, });
		return flag;
	}

	@Override
	public int updateTestcaseExecSummary(TestcaseExecSummary testcases, String schema) {
		int flag = database.update(String.format(testcase.updateTestCaseExecSummary, schema),
				new Object[] { testcases.getStatusId(), testcases.getNotes(), testcases.getCompleteStatus(),
						testcases.getTcId(), testcases.getBuildId() },
				new int[] { Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER });
		return flag;
	}

	@Override
	public List<Map<String, Object>> getTestcaseDetailsByPrefix(String tcPrefix, String schema) {
		logger.info("reads test case details by TC prefix");
		logger.info("Query param tcPrefix = {}", tcPrefix);
		List<Map<String, Object>> testcaseList;
		testcaseList = database.select(String.format(testcase.getTestcaseDetailsByPrefix, schema),
				new Object[] { tcPrefix }, new int[] { Types.VARCHAR });
		return testcaseList;
	}
	
	@Override
	public List<Map<String, Object>> getTestcaseDetailsByProjectIdAndPrefixList(List<String> testPrefixList, int projectId, String schema) {
		List<Map<String, Object>> testcaseList;
		testcaseList = database.select(String.format(testcase.getTestcaseDetailsByPrefixList, schema), 
				new Object[] { testPrefixList, projectId }, new int[] { Types.VARCHAR, Types.INTEGER });
		return testcaseList;
	}
	
	@Override
	public List<Map<String, Object>> getProjectModuleScenarioIdByTcPrefix(String tcPrefix, String schema) {
		List<Map<String, Object>> testcaseDetails = database.select(
				String.format(testcase.getProjectModuleScenarioIdByTcPrefix, schema, schema, schema),
				new Object[] { tcPrefix }, new int[] { Types.VARCHAR });
		return testcaseDetails;
	}

	@Override
	public List<Map<String, Object>> getExecStatus(String schema) {
		List<Map<String, Object>> statusList;
		statusList = database.select(String.format(testcase.getExecStatus, schema));
		return statusList;
	}

	@Override
	public List<Map<String, Object>> getTestCaseDetailsByBuildAndModuleId(int buildId, int moduleId, String schema) {
		logger.info("reads test case details by build and module id");
		List<Map<String, Object>> testcaseList;
		testcaseList = database.select(String.format(testcase.getTestCaseDetailsByBuildAndModuleId, schema),
				new Object[] { buildId, moduleId }, new int[] { Types.INTEGER, Types.INTEGER });
		return testcaseList;
	}

	@Override
	public List<Map<String, Object>> getTestCaseDetailsByBuildAndScenarioId(int buildId, int moduleId, String schema) {
		logger.info("reads executed test case details by build and module id");
		logger.info("Query param buildId = {} , moduleId ={}", buildId, moduleId);
		List<Map<String, Object>> testcaseList;
		testcaseList = database.select(String.format(testcase.getTestCaseDetailsByBuildAndScenarioId, schema),
				new Object[] { buildId, moduleId }, new int[] { Types.INTEGER, Types.INTEGER });
		return testcaseList;
	}

	@Override
	public List<Map<String, Object>> getExecutedTestCaseDetailsByBuildAndModuleId(int buildId, int moduleId,
			String schema) {
		logger.info("reads executed test case details by build and scenario id");
		logger.info("Query param buildId = {} , moduleId ={}", buildId, moduleId);
		List<Map<String, Object>> testcaseList;
		testcaseList = database.select(String.format(testcase.getExecutedTestCaseDetailsByBuildAndModuleId, schema),
				new Object[] { buildId, moduleId }, new int[] { Types.INTEGER, Types.INTEGER });
		return testcaseList;
	}

	@Override
	public List<Map<String, Object>> getExecutedTestCaseDetailsByBuildAndScenarioId(int buildId, int scenarioId,
			String schema) {
		logger.info("reads test case details by build and scenario id");
		logger.info("Query param buildId = {} , scenarioId ={}", buildId, scenarioId);
		List<Map<String, Object>> testcaseList;
		testcaseList = database.select(String.format(testcase.getExecutedTestCaseDetailsByBuildAndScenarioId, schema),
				new Object[] { buildId, scenarioId }, new int[] { Types.INTEGER, Types.INTEGER });
		return testcaseList;
	}

	
	@Override
	public Map<String, Object> updateTestcaseDetails(TestCaseDetails testcases, String schema) {
		logger.info("Reads optimized update test case");
		SqlParameter tcTestcaseNameTemp = new SqlParameter("tcTestcaseName", Types.VARCHAR);
		SqlParameter tcSummaryTemp = new SqlParameter("tcSummary", Types.VARCHAR);
		SqlParameter tcPreconditionTemp = new SqlParameter("tcPrecondition", Types.VARCHAR);
		SqlParameter tcStatusTemp = new SqlParameter("tcStatus", Types.INTEGER);
		SqlParameter tcExecutionTypeTemp = new SqlParameter("tcExecutionType", Types.INTEGER);
		SqlParameter tcExecutionTimeTemp = new SqlParameter("tcExecutionTime", Types.INTEGER);
		SqlParameter tcUpdatedByTemp = new SqlParameter("tcUpdatedBy", Types.INTEGER);
		SqlParameter tcTestcaseIdTemp = new SqlParameter("tcTestcaseId", Types.INTEGER);
		SqlParameter datasheetStatus = new SqlParameter("datasheetStatus", Types.INTEGER);
		SqlParameter redwoodTestcaseId = new SqlParameter("redwood_tc_id", Types.VARCHAR);
		SqlParameter[] paramArray = { tcTestcaseNameTemp,tcSummaryTemp,tcPreconditionTemp,tcStatusTemp,tcExecutionTypeTemp,tcExecutionTimeTemp,
				tcUpdatedByTemp,tcTestcaseIdTemp,datasheetStatus,redwoodTestcaseId};

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("updateTestcase")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(testcases.getName(), testcases.getSummary(), testcases.getPrecondition(),
				testcases.getStatusId(), testcases.getExecutionTypeId(), testcases.getExecutionTime(),
				testcases.getUpdaterId(), testcases.getId(), testcases.getDatasheetStatus(), testcases.getRedwoodTestCaseId());
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getCreateTestCaseData(int userId, int projectId, int moduleId, int scenarioId,
			String schema) {
		logger.info("In procedure to get create test case data ");
		
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter project = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter module = new SqlParameter("moduleId", Types.INTEGER);
		SqlParameter scenario = new SqlParameter("scenarioId", Types.INTEGER);
		SqlParameter[] paramArray = { user, project, module, scenario};

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("createtestcase")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, projectId, moduleId, scenarioId);
		logger.info("Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	

	@Override
	public int deleteTestcase(Integer testcaseId, String schema) {
		logger.info("In method move test case to deleted mode ");
		int flag = database.update(String.format("UPDATE  "+schema+".xetm_testcase set is_deleted=1 WHERE testcase_id=?", schema),
				new Object[] {testcaseId},new int[] { Types.INTEGER});
		/*int flag = database.update(String.format("INSERT INTO "+schema+".xetm_testcase_trash (testcase_id,testcase_name,scenario_id,summary,precondition,status,execution_type,execution_time,author,updated_by,test_prefix,projectid,created_date,module_id,excel_file_present_status) SELECT testcase_id,testcase_name,scenario_id,summary,precondition,status,execution_type,execution_time,author,updated_by," + 
				"test_prefix,projectid,created_date,module_id,excel_file_present_status FROM "+schema+".xetm_testcase WHERE testcase_id=?", schema),
				new Object[] {testcaseId},new int[] { Types.INTEGER});
		if(flag==1) {
			flag = database.update(String.format("DELETE FROM "+schema+".xetm_testcase WHERE testcase_id=?", schema),
					new Object[] {testcaseId},new int[] { Types.INTEGER});
		}*/
		return flag;
	}
}

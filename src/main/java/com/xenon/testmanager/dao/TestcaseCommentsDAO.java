package com.xenon.testmanager.dao;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import com.xenon.testmanager.domain.TcCommentLike;
import com.xenon.testmanager.domain.TestCaseSummary;
import com.xenon.testmanager.domain.TestcaseCommentDetails;

/**
 * 
 * @author bhagyashri.ajmera
 * 
 * This interface handles all test case comment related operations
 *
 */
public interface TestcaseCommentsDAO {
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method inserts test case comment to database
	 * 
	 * @param schema
	 * @param testcaseCommentDetails
	 * @return
	 */
	int insertTestcaseComment(String schema, TestcaseCommentDetails testcaseCommentDetails);
	
	int insertTestcaseSummary(String schema, TestCaseSummary testCaseSummary);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads test case comments by TC id
	 * 
	 * @param userId
	 * @param testcaseId
	 * @param schema
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	List<Map<String, Object>> getTestcaseCommentByTcId(int userId, int testcaseId, String schema)
			throws UnsupportedEncodingException;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method inserts test case comments like to database
	 * 
	 * @param schema
	 * @param tcCommentLike
	 * @return
	 */
	int insertTestcaseCommentLike(String schema, TcCommentLike tcCommentLike);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method removes test case comment like
	 * 
	 * @param schema
	 * @param tcCommentLike
	 * @return
	 */
	int removeCommentLike(String schema, TcCommentLike tcCommentLike);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads comment like by user id 
	 * 
	 * @param userId
	 * @param schema
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	List<Map<String, Object>> getCommentLikeByUserId(int userId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads comments like count by test case id
	 * 
	 * @param testcaseId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getCommentLikeCountByTcId(int testcaseId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is used to insert test case comment to database
	 *
	 * @param testcaseCommentDetails
	 * @param schema
	 * @return
	 */
	Map<String, Object> insertTestcaseComment(TestcaseCommentDetails testcaseCommentDetails, String schema);

	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is used to insert test case comment like
	 * 
	 * @param tcCommentLike
	 * @param schema
	 * @return
	 */
	Map<String, Object> insertTestcaseCommentLike(TcCommentLike tcCommentLike,
			String schema);

	
}

package com.xenon.testmanager.dao;

import java.util.List;
import java.util.Map;
import com.xenon.testmanager.domain.StepExecSummary;

/**
 * 
 * @author suresh.adling
 * @description Interface to perform JDBC operations on test case steps
 * 
 */
public interface TestcaseStepDAO {
	/**
	 * @author suresh.adling
	 * @description get test case steps
	 * @param schema
	 * @return List of test cases
	 */
	List<Map<String, Object>> getAllTestcaseSteps(String schema);

	/**
	 * @author suresh.adling
	 * @description insert test case step
	 * @param schema
	 * @param stepExecSummary
	 * @return integer status
	 */
	int insertTestStepSummary(String schema, StepExecSummary stepExecSummary);

	/**
	 * @author suresh.adling
	 * @description update test case step
	 * @param schema
	 * @param stepExecSummary
	 * @return
	 */
	int updateTestStepSummary(String schema, StepExecSummary stepExecSummary);

	/**
	 * @author suresh.adling
	 * @description get test execution Id by step id
	 * @param testExeId
	 * @param stepId
	 * @param schema
	 * @return integer testExeId
	 */
	int getStepExeIdByStepId(int testExeId, int stepId, String schema);

	/**
	 * @author suresh.adling
	 * @description insert step execution bug
	 * @param buildId
	 * @param stepExeId
	 * @param stepId
	 * @param bugId
	 * @param testcaseId
	 * @param schema
	 * @return
	 */
	int insertStepExeBug(int buildId, int stepExeId, int stepId, int bugId, int testcaseId, String schema);

	/**
	 * @author suresh.adling
	 * @description insert all executed bugs by build id
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllEexcuteBugs(int buildId, String schema);
}

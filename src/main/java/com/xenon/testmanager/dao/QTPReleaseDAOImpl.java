package com.xenon.testmanager.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import com.xenon.testmanager.domain.ExtSysBuildDetail;
import com.xenon.testmanager.domain.ExtSysReleaseDetail;
import com.xenon.testmanager.domain.ExtSysTestCaseDetail;
import com.xenon.testmanager.domain.ExtSysTestStepDetail;

public class QTPReleaseDAOImpl implements QTPReleaseDAO,XenonQuery{

	private static final Logger logger = LoggerFactory.getLogger(QTPReleaseDAOImpl.class);
	
	@Autowired
	DatabaseDao database;
	
	@Override
	public int addReleaseDetails(final ExtSysReleaseDetail releaseDetail, String schema) throws SQLException {
		logger.info("SQL Query to insert Ext sys release detail into xebm_ext_sys_release table");
		KeyHolder holder = new GeneratedKeyHolder();
	
		final String sql = String.format(extSysReleaseModel.insertExtSysRelease, schema);
		final String primaryKey = "release_id";
		
		int createExtSysRel = database.update(new PreparedStatementCreator() {
	        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
	            PreparedStatement ps =
	                connection.prepareStatement(sql, new String[] {primaryKey});
	            ps.setString(1, releaseDetail.getTitle());
	            ps.setString(2, releaseDetail.getDescription());
	            ps.setString(3, releaseDetail.getStart_datetime());
	            ps.setString(4, releaseDetail.getEnd_datetime());
	            ps.setString(5, releaseDetail.getExtSys());
	            return ps;
	        }
	    },holder);
		logger.info("SQL Result Status is " + createExtSysRel);
		return holder.getKey().intValue();
	}

	@Override
	public int addBuildDetails(final ExtSysBuildDetail buildDetail, String schema) throws SQLException {
		logger.info("SQL Query to insert Ext sys build detail into xebm_ext_sys_build table");
		KeyHolder holder = new GeneratedKeyHolder();
		
		final String sql = String.format(extSysReleaseModel.insertExtSysBuild, schema);
		final String primaryKey = "build_id";
		
		int createExtSysBuild = database.update(new PreparedStatementCreator() {
	        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
	            PreparedStatement ps =
	                connection.prepareStatement(sql, new String[] {primaryKey});
	            ps.setString(1, buildDetail.getName());
	            ps.setInt(2, buildDetail.getReleaseId());
	            ps.setString(3, buildDetail.getStart_datetime());
	            ps.setString(4, buildDetail.getEnd_datetime());
	            return ps;
	        }
	    },holder);
		
		logger.info("SQL Result Status is " + createExtSysBuild);
		return holder.getKey().intValue();
	}

	@Override
	public int addTestCaseDetails(final ExtSysTestCaseDetail testCaseDetail, String schema) throws SQLException {
		logger.info("SQL Query to insert Ext sys testcase detail into xebm_ext_sys_teststepsynch table");
		KeyHolder holder = new GeneratedKeyHolder();
		
		final String sql = String.format(extSysReleaseModel.insertExtTestCase, schema);
		final String primaryKey = "test_case_id";
		
		int createExtSysTestCase = database.update(new PreparedStatementCreator() {
	        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
	            PreparedStatement ps =
	                connection.prepareStatement(sql, new String[] {primaryKey});
	            ps.setString(1, testCaseDetail.getNumber());
	            ps.setInt(2, testCaseDetail.getId());
	            ps.setInt(3, testCaseDetail.getBuildId());
	            ps.setString(4, testCaseDetail.getTitle());
	            ps.setString(5, testCaseDetail.getDescription());
	            ps.setString(6, testCaseDetail.getExpected_result());
	            ps.setString(7, testCaseDetail.getActual_result());
	            ps.setInt(8, testCaseDetail.getStatus());
	            ps.setString(9, testCaseDetail.getLob());
	            ps.setString(10, testCaseDetail.getExecution_date());
	            ps.setInt(11, testCaseDetail.getUser_id());
	            ps.setString(12, testCaseDetail.getStart_datetime());
	            ps.setString(13, testCaseDetail.getEnd_datetime());
	            
	            return ps;
	        }
	    },holder);
		logger.info("SQL Result Status is " + createExtSysTestCase);
		return holder.getKey().intValue();
	}

	@Override
	public int addTestStepDetails(final ExtSysTestStepDetail testStepDetail, String schema) throws SQLException {
		logger.info("SQL Query to insert Ext sys teststep detail into xebm_ext_sys_teststepsynch table");
		KeyHolder holder = new GeneratedKeyHolder();
		
		final String sql = String.format(extSysReleaseModel.insertExtTestStep, schema);
		final String primaryKey = "test_step_id";
		
		int createExtSysTestStep = database.update(new PreparedStatementCreator() {
	        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
	            PreparedStatement ps =
	                connection.prepareStatement(sql, new String[] {primaryKey});
	            ps.setString(1, testStepDetail.getNumber());
	            ps.setInt(2, testStepDetail.getId());
	            ps.setInt(3, testStepDetail.getTestCaseId());
	            ps.setString(4, testStepDetail.getTitle());
	            ps.setString(5, testStepDetail.getDescription());
	            ps.setString(6, testStepDetail.getExpected_result());
	            ps.setString(7, testStepDetail.getActual_result());
	            ps.setInt(8, testStepDetail.getStatus());
	            ps.setString(9, testStepDetail.getScreenshot());
	            
	            return ps;
	        }
	    },holder);
		
		logger.info("SQL Result Status is " + createExtSysTestStep);
		return holder.getKey().intValue();
	}

	@Override
	public int getReleaseId(final ExtSysReleaseDetail releaseDetail, String schema) throws SQLException {
		logger.info("SQL Query to get ext sys release summary from release summary details ");
		int id = 0;
		List<Map<String, Object>> releaseId = database.select(String.format(extSysReleaseModel.getReleaseDetails, schema),
				new Object[] { releaseDetail.getTitle()},
				new int[] {Types.VARCHAR });
		if(releaseId != null && !releaseId.isEmpty())
			id = (Integer) releaseId.get(0).get("release_id");
		
		logger.info("SQL Result Release Id  is " + id);
		return id;
	}

	@Override
	public int getBuildId(final int releaseId, String  buildId, String schema) throws SQLException {
		logger.info("SQL Query to get ext sys build summary from release summary details ");
		int id = 0;
		List<Map<String, Object>> exBuildId = database.select(String.format(extSysReleaseModel.getBuildDetails, schema),
				new Object[] {  buildId,releaseId },
				new int[] {Types.VARCHAR,Types.INTEGER });
		
		if(exBuildId != null && !exBuildId.isEmpty())
			id = (Integer) exBuildId.get(0).get("build_id");
		
		logger.info("SQL Result Build Id  is " + id);
		return id;

	}
	
}

package com.xenon.testmanager.dao;

import java.util.List;
import java.util.Map;

import com.xenon.testmanager.domain.TestCaseDetails;
import com.xenon.testmanager.domain.TestDataDetails;
import com.xenon.testmanager.domain.TestcaseExecSummary;
import com.xenon.testmanager.domain.TestStepDetails;

/**
 * 
 * @author suresh.adling,bhagyashri.ajmera
 * @description Interface to perform JDBC operations on test cases
 * 
 */
public interface TestcaseDAO {
	/**
	 * @author suresh.adling
	 * @description get test cases by project id
	 * @param projectId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTestcasesByProjectId(int projectId, String schema);

	/**
	 * @author suresh.adling
	 * @description get test cases by module id
	 * @param moduleId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTestcasesByModuleId(int moduleId, String schema);

	/**
	 * @author suresh.adling
	 * @description get test cases by scenario id
	 * @param scenarioId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTestcasesByScenarioId(int scenarioId, String schema);

	/**
	 * @author suresh.adling
	 * @description get test cases by build id and user id
	 * @param buildId
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllTestcases(int buildId, int userId, String schema);

	/**
	 * @author suresh.adling
	 * @description get active build execution types
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getActiveExecutionTypes(String schema);

	/**
	 * @author suresh.adling
	 * @description update test case details
	 * @param testcases
	 * @param schema
	 * @return
	 */
	int updateTestcase(TestCaseDetails testcases, String schema);

	/**
	 * 
	 * @author abhay.thakur
	 * @method deleteTestCase POST Request
	 * @output Modify test case details
	 * @param model
	 * @param session
	 * @param testcaseId
	 * @return Model & View (onSuccess - testcase ,onFailure - logout/500)
	 * @throws Exception
	 */
	int deleteTestcase(Integer testcaseId, String schema);

	/**
	 * @author suresh.adling
	 * @description get test case details by test case id
	 * @param testcaseId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTestcaseDetails(int testcaseId, String schema);

	/**
	 * @author suresh.adling
	 * @description get test case status list
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getStatusList(String schema);

	/**
	 * @author suresh.adling
	 * @description insert test case step
	 * @param step
	 * @param schema
	 * @return
	 */
	int insertStep(TestStepDetails step, String schema);

	/**
	 * @author abhay.thakur
	 * @description insert test case data
	 * @param data
	 * @param schema
	 * @return
	 */
	int insertData(TestDataDetails data, String schema);

	int updateData(TestDataDetails data, String schema);

	/**
	 * @author suresh.adling
	 * @description update test case step details
	 * @param step
	 * @param schema
	 * @return
	 */
	int updateStep(TestStepDetails step, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method selects test case details by build id
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> selectTestCaseDetailsByBuildId(int buildId, String schema);

	/**
	 * @author suresh.adling
	 * @description get project,module,scenario by test case id
	 * @param testcaseId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getProjectModuleScenarioIdByTcId(int testcaseId, String schema);

	/**
	 * @author suresh.adling
	 * @description insert test execution summary
	 * @param testcases
	 * @param schema
	 * @return
	 */
	int insertTestcaseExecSummary(TestcaseExecSummary testcases, String schema);

	/**
	 * @author suresh.adling
	 * @description update test case execution summary
	 * @param testcases
	 * @param schema
	 * @return
	 */
	int updateTestcaseExecSummary(TestcaseExecSummary testcases, String schema);

	/**
	 * @author suresh.adling
	 * @description insert a test case
	 * @param projectId
	 * @param moduleId
	 * @param testcase
	 * @param schema
	 * @return
	 */
	int insertTestcase(int projectId, int moduleId, TestCaseDetails testcase, String schema);

	/**
	 * @author suresh.adling
	 * @description get test case id by test case details
	 * @param testcases
	 * @param schema
	 * @return
	 */
	int getTestCaseId(TestCaseDetails testcases, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads test case details by TC prefix
	 * 
	 * @param tcPrefix
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTestcaseDetailsByPrefix(String tcPrefix, String schema);

	/**
	 * @author suresh.adling
	 * @description get test case details(project,module,scenario) by test case
	 *              prefix
	 * @param tcPrefix
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getProjectModuleScenarioIdByTcPrefix(String tcPrefix, String schema);

	/**
	 * @author suresh.adling
	 * @description get test execution status list
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getExecStatus(String schema);

	/**
	 * @author suresh.adling
	 * @description get test case status by build id and test case id
	 * @param testcase_id
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTestCaseStatus(int testcase_id, int buildId, String schema);

	/**
	 * @author suresh.adling
	 * @description get test execution status by build id and test case id
	 * @param testcase_id
	 * @param build_id
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTestExeStatus(int testcase_id, int build_id, String schema);

	/**
	 * @author suresh.adling
	 * @description Get test remove status
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTestRemoveStatus(int userId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads test case details by build and module id
	 * 
	 * @param buildId
	 * @param moduleId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTestCaseDetailsByBuildAndModuleId(int buildId, int moduleId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads executed test case details by build and module id
	 * 
	 * @param buildId
	 * @param moduleId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getExecutedTestCaseDetailsByBuildAndModuleId(int buildId, int moduleId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads executed test case details by build and scenario id
	 * 
	 * @param buildId
	 * @param scenarioId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getExecutedTestCaseDetailsByBuildAndScenarioId(int buildId, int scenarioId,
			String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads test case details by build and scenario id
	 * 
	 * @param buildId
	 * @param moduleId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTestCaseDetailsByBuildAndScenarioId(int buildId, int moduleId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method is used to update test case details
	 * 
	 * @param testcases
	 * @param schema
	 * @return
	 */
	Map<String, Object> updateTestcaseDetails(TestCaseDetails testcases, String schema);

	Map<String, Object> getCreateTestCaseData(int userId, int projectId, int moduleId, int scenarioId, String schema);


	List<Map<String, Object>> getTestCaseData(String testcases, String schema);

	List<Map<String, Object>> getTestCaseDataByBuildId(Integer buildId, String schema);

	/**
	 * @author shantaram.tupe 
	 * @description
	 * 11-Mar-2020 12:02:40 PM
	 * @param redwoodTestCaseId
	 * @param schema
	 * @return
	 * Boolean
	 */
	int isRedwoodTestCaseIdAlreadyMapped(String redwoodTestCaseId, String schema);

	List<Map<String, Object>> getTestcaseDetailsByProjectIdAndPrefixList(List<String> testPrefixList, int projectId,
			String schema);
	
}

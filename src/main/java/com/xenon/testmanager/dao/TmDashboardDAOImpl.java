package com.xenon.testmanager.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

/**
 * 
 * @author suresh.adling
 * @description DAO implementation class to perform JDBC operations to get data
 *              for TM dashboard
 * 
 */
public class TmDashboardDAOImpl implements TmDashboardDAO, XenonQuery {

	@Autowired
	DatabaseDao database;
	@Autowired
	JdbcTemplate jdbcTemplate;
	private static final Logger logger = LoggerFactory.getLogger(TmDashboardDAOImpl.class);

	@Override
	public List<Map<String, Object>> getAllCounts(String schema) {
		logger.info("In method get All counts for TM dashboard ");
		logger.info("SQL " + tmDash.allCounts);
		String sql = String.format(tmDash.allCounts, schema, schema, schema, schema);
		List<Map<String, Object>> allCounts = database.select(sql);
		logger.info("Result " + allCounts);
		return allCounts;
	}

	@Override
	public List<Map<String, Object>> getProjectwiseCounts(String schema) {
		logger.info("In method to get test case counts for each project ");
		logger.info("SQL " + tmDash.projectwiseCounts);
		String sql = String.format(tmDash.projectwiseCounts, schema);
		List<Map<String, Object>> projectwiseCounts = database.select(sql);
		logger.info("Result " + projectwiseCounts);
		return projectwiseCounts;
	}

	@Override
	public List<Map<String, Object>> getModuleTestCaseCounts(String schema) {
		logger.info("In method get Modulewise test case count ");
		logger.info("SQL " + tmDash.moduleTestCaseCounts);
		String sql = String.format(tmDash.moduleTestCaseCounts, schema, schema, schema);
		List<Map<String, Object>> projectwiseCounts = database.select(sql);
		logger.info("Result " + projectwiseCounts);
		return projectwiseCounts;

	}

	@Override
	public List<Map<String, Object>> getTMDashboard(String schema) {
		logger.info("In procedure to get data for test manager dashboard");
		logger.info("SQL " + testCaseStep.getAllTestcaseSteps);
		String sql = String.format(tmDash.tmDashboard, schema, schema, schema, schema, schema, schema, schema, schema,
				schema, schema);
		List<Map<String, Object>> dashBoardData = database.select(sql);
		logger.info("Result " + dashBoardData);
		return dashBoardData;

	}

	@Override
	public Map<String, Object> getTMDashboardData(int userId, String schema) {
		logger.info("In procedure to get data for test manager dashboardby  user id ");
		SqlParameter user = new SqlParameter("currentUser", Types.INTEGER);
		SqlParameter[] paramArray = { user };
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("tmDashboard")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId);
		logger.info("Result " + simpleJdbcCallResult);

		return simpleJdbcCallResult;

	}

	@Override
	public Map<String, Object> getTmProjectDashboard(int projectId, int userId, String schema) {
		logger.info("In procedure to get data for test manager dashboard by project and user id ");
		SqlParameter project = new SqlParameter("currentProject", Types.INTEGER);
		SqlParameter user = new SqlParameter("currentUser", Types.INTEGER);
		SqlParameter[] paramArray = { project, user };
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("tmProjectDashboard")
				.declareParameters(paramArray).withCatalogName(schema);
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(projectId, userId);
		logger.info("Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;

	}
	
	
	@Override
	public boolean checkTMRoleAccess(String roleAccess,String roleId,String schema) {
		boolean flag=false;
		logger.info("Calling Stored procedure to get build report data");
		SqlParameter roleID = new SqlParameter("roleId", Types.VARCHAR);
		SqlParameter access = new SqlParameter("roleAccess", Types.VARCHAR);
		SqlParameter[] paramArray = {access,roleID};

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("checkTMAccess")
				.declareParameters(paramArray).withCatalogName(schema);
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(roleAccess,roleId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		@SuppressWarnings("unchecked")
		List<Map<String,Object>> returnList=(List<Map<String, Object>>) simpleJdbcCallResult.get("#result-set-1");
			if(!returnList.isEmpty()){
			flag=returnList.get(0).containsValue(1);	
			}	
		return flag;
	}
	
}

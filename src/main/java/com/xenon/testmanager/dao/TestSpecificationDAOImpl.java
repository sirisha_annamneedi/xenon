package com.xenon.testmanager.dao;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.xenon.api.common.LoginService;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import com.xenon.testmanager.domain.TestCaseDetails;
import com.xenon.testmanager.domain.TestStepDetails;

/**
 * 
 * @author suresh.adling
 * @description DAO implementation class to perform JDBC operations on test case
 *              steps
 * 
 */
public class TestSpecificationDAOImpl implements TestSpecificationDAO, XenonQuery {

	@Autowired
	DatabaseDao database;

	@Autowired
	LoginService loginService;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LoggerFactory.getLogger(TestSpecificationDAOImpl.class);

	@Override
	public Map<String, Object> getTestSpecification(int projectId, int userId, String schema) {
		logger.info("In procedure to get test speccification data (Module list,Scenario List)");
		SqlParameter project = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = { project, user };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("testspecification")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(projectId, userId);
		logger.info("Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getTestCaseData(int userId, int projectId, int moduleId, int scenarioId, String schema,
			int startValue, int offsetValue) {
		logger.info("In procedure to get test case data (test case List)");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter project = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter module = new SqlParameter("moduleId", Types.INTEGER);
		SqlParameter scenario = new SqlParameter("scenarioId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, project, module, scenario, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("testcase")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, projectId, moduleId, scenarioId,
				startValue, offsetValue);
		logger.info("Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getTestCaseStepData(int userId, int projectId, int moduleId, int scenarioId,
			int testCaseId, String schema) {
		logger.info("In procedure to get test test case step data (test case step list)");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter project = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter module = new SqlParameter("moduleId", Types.INTEGER);
		SqlParameter scenario = new SqlParameter("scenarioId", Types.INTEGER);
		SqlParameter testCase = new SqlParameter("testCaseId", Types.INTEGER);

		SqlParameter[] paramArray = { user, project, module, scenario, testCase };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("testcasesteps")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, projectId, moduleId, scenarioId,
				testCaseId);
		logger.info("Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getScenarioData(int moduleId, String schema) {
		logger.info("In procedure to get test scenario data (Scenario List)");

		SqlParameter module = new SqlParameter("moduleId", Types.INTEGER);
		SqlParameter[] paramArray = { module };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("scenariolist")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(moduleId);
		logger.info("Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	/*
	 * @Override public ScenarioDetails getTestCaseList(int scenarioId, int
	 * projectId, String schema) throws InvalidResultSetAccessException,
	 * UnsupportedEncodingException {
	 * logger.info("In method to get Test Case List");
	 * 
	 * ScenarioDetails ScenarioTree = new ScenarioDetails();
	 * 
	 * List<TestCaseDetails> testList = new ArrayList<TestCaseDetails>();
	 * 
	 * SqlRowSet testcases = database.selectRow(
	 * String.format(testSpec.testCaseDetails, schema, schema, schema, schema),
	 * new Object[] { scenarioId, projectId }, new int[] { Types.INTEGER,
	 * Types.INTEGER }); while (testcases.next()) {
	 * 
	 * TestCaseDetails testcase = new TestCaseDetails();
	 * testcase.setId(testcases.getInt("testcase_id"));
	 * testcase.setName(testcases.getString("testcase_name"));
	 * testcase.setSummary(testcases.getString("summary"));
	 * testcase.setPrecondition(testcases.getString("precondition"));
	 * testcase.setExecutionTypeId(testcases.getInt("execution_type"));
	 * testcase.setExecutionType(testcases.getString("description"));
	 * testcase.setExecutionTime(testcases.getInt("execution_time"));
	 * testcase.setStatus(testcases.getString("status"));
	 * testcase.setStatusId(testcases.getInt("tc_status_id"));
	 * testcase.setTestPrefix(testcases.getString("test_prefix"));
	 * testcase.setAuthorFullName(testcases.getString("authorFullName"));
	 * testcase.setAuthorAboutMe(testcases.getString("authorAboutMe"));
	 * testcase.setUserPhoto(login.getImage((byte[])
	 * testcases.getObject("user_photo")));
	 * testcase.setTestCaseURL(Constants.host + "testcasebyprefix?tcPrefix=" +
	 * testcases.getString("test_prefix")); testList.add(testcase); }
	 * ScenarioTree.setTestList(testList); logger.info("Result " +
	 * ScenarioTree); return ScenarioTree; }
	 */
	@Override
	public List<Map<String, Object>> getLastModule(Integer projectId, Integer userId, String schema) {
		logger.info("In method get last module");
		List<Map<String, Object>> data;
		data = database.select("SELECT module_id,module_name FROM " + schema
				+ ".xe_module WHERE module_id IN (SELECT module_id FROM " + schema + ".xe_user_module where project_id="
				+ projectId + " and user_id = " + userId + ") and module_active = 1 limit 1");
		return data;
	}

	@Override
	public List<Map<String, Object>> getModuleTrash(String schema, int projectId) {
		logger.info("In method get trash module");
		List<Map<String, Object>> dataModuleTrash;
		dataModuleTrash = database.select("SELECT module_id,module_name FROM " + schema
				+ ".xe_module WHERE  module_active = 2 and project_id=" + projectId);
		return dataModuleTrash;
	}

	@Override
	public List<Map<String, Object>> getLastScenario(Integer moduleId, String schema) {
		logger.info("In method get last scenario");
		List<Map<String, Object>> data;
		data = database.select("SELECT scenario_id, scenario_name, module_id, scenario_description FROM  " + schema
				+ ".xetm_scenario WHERE module_id=" + moduleId + " and status=1 LIMIT 1");
		return data;
	}

	/** @author anmol.chadha **/
	@Override
	public List<Map<String, Object>> getScenarioTrash(String schema, int projectId) {
		logger.info("In method get trash Scenario");
		List<Map<String, Object>> dataScenarioTrash;
		dataScenarioTrash = database.select("SELECT xm.module_id,xm.module_name,xc.scenario_id,xc.scenario_name FROM "
				+ schema + ".xetm_scenario xc ," + schema
				+ ".xe_module xm WHERE xc.module_id=xm.module_id and xc.status=2 and xm.project_id=" + projectId);
		return dataScenarioTrash;
	}

	/** @author anmol.chadha **/
	@Override
	public List<Map<String, Object>> getTestCaseTrash(String schema, int projectId) {
		logger.info("In method get trash test case");
		List<Map<String, Object>> dataTestStepsTrash;
		// dataTestStepsTrash = database.select("SELECT
		// testcase_id,module_id,scenario_id,test_prefix,testcase_name FROM "
		// + schema + ".xetm_testcase_trash where projectid=" + projectId);

		dataTestStepsTrash = database.select(
				"SELECT * FROM " + schema + ".xetm_testcase where projectid=" + projectId + " and is_deleted = 1");

		return dataTestStepsTrash;
	}

	
	/** @author anmol.chadha **/
	@Override
	public int restoreTestcaseForTrash(int testcaseId, String schema) {
		logger.info("In method get trash test case");
		int updatedTestcases;
		updatedTestcases = database
				.update("UPDATE " + schema + ".xetm_testcase SET is_deleted = 0 WHERE testcase_id=" + testcaseId);
		return updatedTestcases;
	}

	@Override
	public TestCaseDetails getTestStepsList(int testcaseId, String schema) {
		logger.info("In procedure to get test case step list");

		TestCaseDetails testcaseTree = new TestCaseDetails();

		List<TestStepDetails> testStepList = new ArrayList<TestStepDetails>();

		SqlRowSet teststeps = database.selectRow(String.format(testSpec.testStepDetails, schema, schema),
				new Object[] { testcaseId }, new int[] { Types.INTEGER });
		while (teststeps.next()) {

			TestStepDetails teststep = new TestStepDetails();
			teststep.setStepId(teststeps.getInt("step_id"));
			teststep.setTestStepId(teststeps.getInt("test_step_id"));
			teststep.setStepAction(teststeps.getString("action"));
			teststep.setExpectedResult(teststeps.getString("result"));
			testStepList.add(teststep);
		}
		testcaseTree.setTestStepList(testStepList);
		logger.info("Result " + testStepList);
		return testcaseTree;
	}

	@Override
	public int uploadTestcaseDatasheet(int testcaseId, int userId, byte[] fileBytes, String fileName, String schema) {
		int flag = 1;
		flag = database.update(String.format(testSpec.deleteExistingTestcaseDatasheet, schema),
				new Object[] { testcaseId }, new int[] { Types.INTEGER });

		flag = database.update(String.format(testSpec.uploadTestcaseDatasheet, schema),
				new Object[] { testcaseId, userId, fileBytes, fileName },
				new int[] { Types.INTEGER, Types.INTEGER, Types.BLOB, Types.VARCHAR });
		return flag;
	}

	@Override
	public int deleteExistingTestcaseDatasheet(int testcaseId, String schema) {
		int flag = 1;
		flag = database.update(String.format(testSpec.deleteExistingTestcaseDatasheet, schema),
				new Object[] { testcaseId }, new int[] { Types.INTEGER });
		return flag;
	}

	@Override
	public List<Map<String, Object>> getDatasheetByTcId(int testcaseId, String schema) {
		List<Map<String, Object>> data = database.select(String.format(testSpec.getDatasheetByTcId, schema),
				new Object[] { testcaseId }, new int[] { Types.INTEGER });

		return data;
	}

	@Override
	public List<Map<String, Object>> getDatasheetByBuildId(int buildId, String schema) {
		List<Map<String, Object>> data = database.select(String.format(testSpec.getDatasheetByBuildId, schema),
				new Object[] { buildId }, new int[] { Types.INTEGER });

		return data;
	}
	
	public void saveFilterValues(String inputValue, String commonData,Integer userId,String schema) {
		database.update(String.format(testSpec.filterQuery, schema),
				new Object[] { inputValue, commonData,userId},
				new int[] {Types.VARCHAR,Types.VARCHAR, Types.INTEGER});
	}
}

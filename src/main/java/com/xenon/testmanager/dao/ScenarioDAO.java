package com.xenon.testmanager.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.xenon.testmanager.domain.ComponentDetails;
import com.xenon.testmanager.domain.ScenarioDetails;

import net.rcarz.jiraclient.Component;

/**
 * 
 * @author suresh.adling,bhagyashri.ajmera
 * @description Interface to perform JDBC operations on scenarios
 * 
 */
public interface ScenarioDAO {
	/**
	 * @author suresh.adling
	 * @description get all scenarios
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllScenarios(String schema);

	/**
	 * @author suresh.adling
	 * @description get all scenarios with test case count of each
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllScenariosWithTcCount(String schema);

	/**
	 * @author suresh.adling
	 * @description insert a scenario
	 * @param scenario
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int insertScenario(ScenarioDetails scenario, String schema) throws SQLException;

	/**
	 * @author suresh.adling
	 * @description update scenario details
	 * @param scenarios
	 * @param schemas
	 * @return
	 */
	int updateScenario(ScenarioDetails scenarios, String schemas);
	
	/**
	 * @author shantaram.tupe
	 * 31-Oct-2019:9:49:03 AM
	 * @param scenarioStatus
	 * @param scenarioId
	 * @param schema
	 * @return
	 */
	int updateScenarioStatus( int scenarioStatus, int scenarioId, String schema );

	/**
	 * @author suresh.adling
	 * @description get scenarion details by id
	 * @param scenarioId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getScenarioDetails(int scenarioId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads scenario details by build id
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> selectScenarioDetailsByBuildId(int buildId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method checks scenario create limit exceeds for customer or
	 *         not
	 * 
	 * @param schema
	 * @param custTypeId
	 * @return
	 * @throws SQLException
	 */
	int createNewScenarioStatus(String schema, int custTypeId) throws SQLException;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method selects scenario details by build id
	 * 
	 * @param scenarioDetails
	 * @param moduleTcDetails
	 * @param moduleExecTcDetails
	 * @return
	 */
	List<Map<String, Object>> selectScenarioDetailsByBuildId(List<Map<String, Object>> scenarioDetails,
			List<Map<String, Object>> moduleTcDetails, List<Map<String, Object>> moduleExecTcDetails);
	
	public int getScenarioId(String scenarioName,int moduleId,String schema);

	public int getScenarioIds(ScenarioDetails scenarios, String schema);

	int deleteScenario(int scenarioId, String schema);

	int insertJiraStory(ScenarioDetails scenarios, String schema) throws SQLException;

	int insertComponent(ComponentDetails com, String schema);

	int insertScenarioComponent(int componentId, int scenarioId, String schema);

	int getComponentId(Component str, String schema);

	int updateJiraScenario(ScenarioDetails scenarios, String schema);

	/** @author anmol.chadha **/
	int restoreScenarioForTrash(int moduleId, int scenarioId, String schema);
}

package com.xenon.buildmanager.domain;

public class ProjectDetails {
	
	private int projectId;
	private String projectName;
	private String jiraProjectName;
	private String projectDescription;
	private int projectStatus;
	private String tcPrefix;
	private String btPrefix;
	private String currentDate;
	
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getJiraProjectName() {
		return jiraProjectName;
	}
	public void setJiraProjectName(String jiraProjectName) {
		this.jiraProjectName = jiraProjectName;
	}
	public String getProjectDescription() {
		return projectDescription;
	}
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}
	public int getProjectStatus() {
		return projectStatus;
	}
	public void setProjectStatus(int projectStatus) {
		this.projectStatus = projectStatus;
	}
	public String getTcPrefix() {
		return tcPrefix;
	}
	public void setTcPrefix(String tcPrefix) {
		this.tcPrefix = tcPrefix;
	}
	public String getBtPrefix() {
		return btPrefix;
	}
	public void setBtPrefix(String btPrefix) {
		this.btPrefix = btPrefix;
	}
	public String getcurrentDate() {
		return currentDate;
	}
	public void setcurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}
	
}

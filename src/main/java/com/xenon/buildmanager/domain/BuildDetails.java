package com.xenon.buildmanager.domain;

public class BuildDetails {

	private int buildID;
	private String buildName;
	private String buildDesc;
	private int buildStatus;
	private int buildReleaseID;
	private int buildCompletedStatus;
	private int buildExecutedStatus;
	private int buildCreatorID;
	private String currentDate;
	private String updateDate;
	private int testSetExecType;
	private String redwoodId;
	
	public int getTestSetExecType() {
		return testSetExecType;
	}
	public void setTestSetExecType(int testSetExecType) {
		this.testSetExecType = testSetExecType;
	}
	public int getBuildID() {
		return buildID;
	}
	public void setBuildID(int buildID) {
		this.buildID = buildID;
	}
	public String getBuildName() {
		return buildName;
	}
	public void setBuildName(String buildName) {
		this.buildName = buildName;
	}
	public String getBuildDesc() {
		return buildDesc;
	}
	public void setBuildDesc(String buildDesc) {
		this.buildDesc = buildDesc;
	}
	public int getBuildStatus() {
		return buildStatus;
	}
	public void setBuildStatus(int buildStatus) {
		this.buildStatus = buildStatus;
	}
	public int getBuildReleaseID() {
		return buildReleaseID;
	}
	public void setBuildReleaseID(int buildReleaseID) {
		this.buildReleaseID = buildReleaseID;
	}
	public int getBuildCompletedStatus() {
		return buildCompletedStatus;
	}
	public void setBuildCompletedStatus(int buildCompletedStatus) {
		this.buildCompletedStatus = buildCompletedStatus;
	}
	public int getBuildExecutedStatus() {
		return buildExecutedStatus;
	}
	public void setBuildExecutedStatus(int buildExecutedStatus) {
		this.buildExecutedStatus = buildExecutedStatus;
	}
	public int getBuildCreatorID() {
		return buildCreatorID;
	}
	public void setBuildCreatorID(int buildCreatorID) {
		this.buildCreatorID = buildCreatorID;
	}
	
	public String getcurrentDate() {
		return currentDate;
	}
	public void setcurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}
	
	public String getUpdateDate()
	{
		return updateDate;
	}
	public void setUpdateDate(String updateDate)
	{
		this.updateDate=updateDate;
	}
	public String getRedwoodId() {
		return redwoodId;
	}
	public void setRedwoodId(String redwoodId) {
		this.redwoodId = redwoodId;
	}
}

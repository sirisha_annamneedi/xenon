package com.xenon.buildmanager.domain;

public class BuildTestCases {
	private int tcid;
	private int buildId;
	private int testCaseid;
	private int projectId;
	private int scenarioId;
	private int moduleId;
	private byte[]  datasheet;
	private String datasheetTitle;

	public int getTcid() {
		return tcid;
	}
	public void setTcid(int tcid) {
		this.tcid = tcid;
	}
	public int getBuildId() {
		return buildId;
	}
	public void setBuildId(int buildId) {
		this.buildId = buildId;
	}
	public int getTestCaseid() {
		return testCaseid;
	}
	public void setTestCaseid(int testCaseid) {
		this.testCaseid = testCaseid;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public int getScenarioId() {
		return scenarioId;
	}
	public void setScenarioId(int scenarioId) {
		this.scenarioId = scenarioId;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public byte[] getDatasheet() {
		return datasheet;
	}
	public void setDatasheet(byte[] datasheet) {
		this.datasheet = datasheet;
	}
	public String getDatasheetTitle() {
		return datasheetTitle;
	}
	public void setDatasheetTitle(String datasheetTitle) {
		this.datasheetTitle = datasheetTitle;
	}

}

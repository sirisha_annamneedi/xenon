package com.xenon.buildmanager.domain;

public class RecentActivity {
	
	private int activityId; 
	private int activity; 
	private String activityIesc; 
	private String module; 
	private String activityDate; 
	private int user; 
	private int activityStatus;
	public int getActivityId() {
		return activityId;
	}
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}
	public int getActivity() {
		return activity;
	}
	public void setActivity(int activity) {
		this.activity = activity;
	}
	public String getActivityDesc() {
		return activityIesc;
	}
	public void setActivityDesc(String activityIesc) {
		this.activityIesc = activityIesc;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getActivityDate() {
		return activityDate;
	}
	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public int getActivityStatus() {
		return activityStatus;
	}
	public void setActivityStatus(int activityStatus) {
		this.activityStatus = activityStatus;
	}

}

package com.xenon.buildmanager.domain;

public class TrialBuildDetails {

	private int buildID;
	private String buildName;
	private int buildCreatorID;
	private String buildCreatedate;
	
	public int getBuildID() {
		return buildID;
	}
	public void setBuildID(int buildID) {
		this.buildID = buildID;
	}
	public String getBuildName() {
		return buildName;
	}
	public void setBuildName(String buildName) {
		this.buildName = buildName;
	}
	
	public int getBuildCreatorID() {
		return buildCreatorID;
	}
	public void setBuildCreatorID(int buildCreatorID) {
		this.buildCreatorID = buildCreatorID;
	}
	public String getBuildCreatedate() {
		return buildCreatedate;
	}
	public void setBuildCreatedate(String buildCreatedate) {
		this.buildCreatedate = buildCreatedate;
	}
	
	
	
}

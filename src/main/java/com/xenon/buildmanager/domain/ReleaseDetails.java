package com.xenon.buildmanager.domain;

import java.util.Date;


public class ReleaseDetails {
	private int releaseId;
	private String releaseName;
	private String releaseDescription;
	private Date startDate;
	private Date endDate;
	private int releaseStatus;
	public int getReleaseId() {
		return releaseId;
	}
	public void setReleaseId(int releaseId) {
		this.releaseId = releaseId;
	}
	public String getReleaseName() {
		return releaseName;
	}
	public void setReleaseName(String releaseName) {
		this.releaseName = releaseName;
	}
	public String getReleaseDescription() {
		return releaseDescription;
	}
	public void setReleaseDescription(String releaseDescription) {
		this.releaseDescription = releaseDescription;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public int getReleaseStatus() {
		return releaseStatus;
	}
	public void setReleaseStatus(int releaseStatus) {
		this.releaseStatus = releaseStatus;
	}
	
	
}

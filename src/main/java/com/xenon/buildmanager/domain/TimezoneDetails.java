package com.xenon.buildmanager.domain;

public class TimezoneDetails {

	private int xeTimezoneId;
	private String timezoneName;
	private int timezoneStaus;
	public int getXeTimezoneId() {
		return xeTimezoneId;
	}
	public void setXeTimezoneId(int xeTimezoneId) {
		this.xeTimezoneId = xeTimezoneId;
	}
	public String getTimezoneName() {
		return timezoneName;
	}
	public void setTimezoneName(String timezoneName) {
		this.timezoneName = timezoneName;
	}
	public int getTimezoneStaus() {
		return timezoneStaus;
	}
	public void setTimezoneStaus(int timezoneStaus) {
		this.timezoneStaus = timezoneStaus;
	}
	
}

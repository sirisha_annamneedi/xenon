package com.xenon.buildmanager.domain;

public class ModuleDetails {

	private int moduleId;
	private String moduleName;
	private String moduleDescription;
	private int projectId;
	private int moduleStatus;

	
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getModuleDescription() {
		return moduleDescription;
	}
	public void setModuleDescription(String moduleDescription) {
		this.moduleDescription = moduleDescription;
	}
	public int getModuleStatus() {
		return moduleStatus;
	}
	public void setModuleStatus(int moduleStatus) {
		this.moduleStatus = moduleStatus;
	}
}


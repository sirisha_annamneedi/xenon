package com.xenon.buildmanager.domain;

public class EnvironmentDetails {
	String envName;
	String envDescription;
	String configParameterName;
	int envStatus;
	public String getEnvName() {
		return envName;
	}
	public void setEnvName(String envName) {
		this.envName = envName;
	}
	public String getEnvDescription() {
		return envDescription;
	}
	public void setEnvDescription(String envDescription) {
		this.envDescription = envDescription;
	}
	public String getConfigParameterName() {
		return configParameterName;
	}
	public void setConfigParameterName(String configParameterName) {
		this.configParameterName = configParameterName;
	}
	public int getEnvStatus() {
		return envStatus;
	}
	public void setEnvStatus(int envStatus) {
		this.envStatus = envStatus;
	}
}

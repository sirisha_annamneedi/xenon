package com.xenon.buildmanager.domain;

public class DocumentLibraryDetails {

	private int xeDocLibraryId;
	private String docTitle;
	private String docType;
	private byte[] docData;
	private String uploadTime;
	private int uploadedBy;
	private int projectId;
	
	
	public int getXeDocLibraryId() {
		return xeDocLibraryId;
	}
	public void setXeDocLibraryId(int xeDocLibraryId) {
		this.xeDocLibraryId = xeDocLibraryId;
	}
	public String getDocTitle() {
		return docTitle;
	}
	public void setDocTitle(String docTitle) {
		this.docTitle = docTitle;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public byte[] getDocData() {
		return docData;
	}
	public void setDocData(byte[] docData) {
		this.docData = docData;
	}
	public String getUploadTime() {
		return uploadTime;
	}
	public void setUploadTime(String upload_time) {
		this.uploadTime = upload_time;
	}
	public int getUploadedBy() {
		return uploadedBy;
	}
	public void setUploadedBy(int uploadedBy) {
		this.uploadedBy = uploadedBy;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	
}

package com.xenon.buildmanager.domain;

public class BuildExecDetails {


	private int buildExecId ;
	private int buildId ;
	private int vmId ;
	private String browser ;
	private int mailGroupId ;
	private int stepWiseScreenshot ;
	private int reportBug ;
	private int userId;
	private int envId;
	private String startTime ;
	private String endTime ;
	private String execJson;
	private int completeStatus;
	public int getBuildExecId() {
		return buildExecId;
	}
	public void setBuildExecId(int buildExecId) {
		this.buildExecId = buildExecId;
	}
	public int getBuildId() {
		return buildId;
	}
	public void setBuildId(int buildId) {
		this.buildId = buildId;
	}
	public int getVmId() {
		return vmId;
	}
	public void setVmId(int vmId) {
		this.vmId = vmId;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public int getMailGroupId() {
		return mailGroupId;
	}
	public void setMailGroupId(int mailGroupId) {
		this.mailGroupId = mailGroupId;
	}
	public int getStepWiseScreenshot() {
		return stepWiseScreenshot;
	}
	public void setStepWiseScreenshot(int stepWiseScreenshot) {
		this.stepWiseScreenshot = stepWiseScreenshot;
	}
	public int getReportBug() {
		return reportBug;
	}
	public void setReportBug(int reportBug) {
		this.reportBug = reportBug;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	@Override
	public String toString()
	{
		return "[buildExecId:"+buildExecId+" buildId:"+buildId+" vmId:"+vmId+" browser:"+browser
				+" mailGroupId:"+mailGroupId+" stepWiseScreenshot:"+stepWiseScreenshot+" userId:"+userId+"]";
	}
	public int getEnvId() {
		return envId;
	}
	public void setEnvId(int envId) {
		this.envId = envId;
	}
	public String getExecJson() {
		return execJson;
	}
	public void setExecJson(String execJson) {
		this.execJson = execJson;
	}
	public int getCompleteStatus() {
		return completeStatus;
	}
	public void setCompleteStatus(int completeStatus) {
		this.completeStatus = completeStatus;
	}

}

package com.xenon.buildmanager.domain;

public class VMDetails {

	private int xeVmId;
	private String hostname;
	private String ipAddress;
	private String username;
	private String password;
	private int portNumber;
	private String projectLocation;
	private int ieStatus;
	private int chromeStatus;
	private int firefoxStatus;
	private int safariStatus;
	private int concurrentExecStatus;
	private int concurrentExecs;
	private int isFree;
	private int repoStatus;
	
	public int getRepoStatus() {
		return repoStatus;
	}
	public void setRepoStatus(int repoStatus) {
		this.repoStatus = repoStatus;
	}
	private int status;
	public int getXeVmId() {
		return xeVmId;
	}
	public void setXeVmId(int xeVmId) {
		this.xeVmId = xeVmId;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getProjectLocation() {
		return projectLocation;
	}
	public void setProjectLocation(String projectLocation) {
		this.projectLocation = projectLocation;
	}
	public int getIeStatus() {
		return ieStatus;
	}
	public void setIeStatus(int ieStatus) {
		this.ieStatus = ieStatus;
	}
	public int getChromeStatus() {
		return chromeStatus;
	}
	public void setChromeStatus(int chromeStatus) {
		this.chromeStatus = chromeStatus;
	}
	public int getFirefoxStatus() {
		return firefoxStatus;
	}
	public void setFirefoxStatus(int firefoxStatus) {
		this.firefoxStatus = firefoxStatus;
	}
	public int getSafariStatus() {
		return safariStatus;
	}
	public void setSafariStatus(int safariStatus) {
		this.safariStatus = safariStatus;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getPortNumber() {
		return portNumber;
	}
	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}
	public int getConcurrentExecStatus() {
		return concurrentExecStatus;
	}
	public void setConcurrentExecStatus(int concurrentExecStatus) {
		this.concurrentExecStatus = concurrentExecStatus;
	}
	public int getConcurrentExecs() {
		return concurrentExecs;
	}
	public void setConcurrentExecs(int concurrentExecs) {
		this.concurrentExecs = concurrentExecs;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getIsFree() {
		return isFree;
	}
	public void setIsFree(int isFree) {
		this.isFree = isFree;
	}
	
}

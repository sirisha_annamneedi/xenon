package com.xenon.buildmanager.domain;

public class BuildExecEnvDetails {
	
	private int id;
	private int buildExecId;
	private int envId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBuildExecId() {
		return buildExecId;
	}
	public void setBuildExecId(int buildExecId) {
		this.buildExecId = buildExecId;
	}
	public int getEnvId() {
		return envId;
	}
	public void setEnvId(int envId) {
		this.envId = envId;
	}
}

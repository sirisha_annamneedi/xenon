package com.xenon.buildmanager.domain;

public class MailNotification {
	private int mailNotificationId;
	private int createUser;
	private int assignProject;
	private int assignModule;
	private int assignBuild;
	private int buildExecComplete;
	private int createBug;
	private int updateBug;
	public int getCreateUser() {
		return createUser;
	}
	public void setCreateUser(int createUser) {
		this.createUser = createUser;
	}
	public int getAssignProject() {
		return assignProject;
	}
	public void setAssignProject(int assignProject) {
		this.assignProject = assignProject;
	}
	public int getAssignModule() {
		return assignModule;
	}
	public void setAssignModule(int assignModule) {
		this.assignModule = assignModule;
	}
	public int getAssignBuild() {
		return assignBuild;
	}
	public void setAssignBuild(int assignBuild) {
		this.assignBuild = assignBuild;
	}
	public int getBuildExecComplete() {
		return buildExecComplete;
	}
	public void setBuildExecComplete(int buildExecComplete) {
		this.buildExecComplete = buildExecComplete;
	}
	public int getCreateBug() {
		return createBug;
	}
	public void setCreateBug(int createBug) {
		this.createBug = createBug;
	}
	public int getUpdateBug() {
		return updateBug;
	}
	public void setUpdateBug(int updateBug) {
		this.updateBug = updateBug;
	}
	public int getMailNotificationId() {
		return mailNotificationId;
	}
	public void setMailNotificationId(int mailNotificationId) {
		this.mailNotificationId = mailNotificationId;
	}
}

package com.xenon.buildmanager.domain;

public class UserDetails {

	private int userId;
	private int tmStatus;
	private int btStatus;
	private int roleId;
	private int userStatus;
	private String password;
	private String firstName;
	private String lastName;
	private String emailId;
	private String custName ;
	private byte[] userPhoto;
	private String userPhotoName;
	private String location;
	private String aboutMe;
	private int timezone;
	private String createDate;
	private String updateDate;
	private String userName;
	private int jenkinStatus;
	private int gitStatus;
	private int redwoodStatus;
	private int apiStatus;
	private int issueTracker;
	private int issueTrackerType;
	private String issueTrackerHost;
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getUserId() {
		return userId;
	}

	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getTMStatus() {
		return tmStatus;
	}
	public void setTMStatus(int tmStatus) {
		this.tmStatus = tmStatus;
	}
	public int getBTStatus() {
		return btStatus;
	}
	public void setBTStatus(int btStatus) {
		this.btStatus = btStatus;
	}
	public int getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public byte[] getUserPhoto() {
	    return userPhoto;
	}

	public void setUserPhoto(byte[] userPhoto) {
	    this.userPhoto =  userPhoto;
	}
	
	
	public String getCust() {
		return custName;
	}
	public void setCust(String custName) {
		this.custName = custName;
	}
	public String getUserPhotoName() {
		return userPhotoName;
	}
	public void setUserPhotoName(String userPhotoName) {
		this.userPhotoName = userPhotoName;
	}
	public int getTmStatus() {
		return tmStatus;
	}
	public void setTmStatus(int tmStatus) {
		this.tmStatus = tmStatus;
	}
	public int getBtStatus() {
		return btStatus;
	}
	public void setBtStatus(int btStatus) {
		this.btStatus = btStatus;
	}
	public String getCustName() {
		return custName;
	}
	public int getTimezone() {
		return timezone;
	}
	public void setTimezone(int timezone) {
		this.timezone = timezone;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getAboutMe() {
		return aboutMe;
	}
	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getJenkinStatus() {
		return jenkinStatus;
	}
	public void setJenkinStatus(int jenkinStatus) {
		this.jenkinStatus = jenkinStatus;
	}
	public int getGitStatus() {
		return gitStatus;
	}
	public void setGitStatus(int gitStatus) {
		this.gitStatus = gitStatus;
	}
	public void setIssueTracker(int issueTracker) {
		this.issueTracker = issueTracker;
	}
	public int getIssueTracker() {
		return issueTracker;
	}
	public void setIssueTrackerType(int issueTrackerType) {
		this.issueTrackerType = issueTrackerType;
	}
	public int getIssueTrackerType() {
		return issueTrackerType;
	}
	public void setIssueTrackerHost(String issueTrackerHost) {
		this.issueTrackerHost = issueTrackerHost;
	}
	public String getIssueTrackerHost() {
		return issueTrackerHost;
	}
	public int getRedwoodStatus() {
		return redwoodStatus;
	}
	public void setRedwoodStatus(int redwoodStatus) {
		this.redwoodStatus = redwoodStatus;
	}
	public int getApiStatus() {
		return apiStatus;
	}
	public void setApiStatus(int apiStatus) {
		this.apiStatus = apiStatus;
	}
	
}

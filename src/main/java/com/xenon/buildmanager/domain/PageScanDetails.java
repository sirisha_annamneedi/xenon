package com.xenon.buildmanager.domain;

public class PageScanDetails {

	private int scannerId;
	private int env;
	private int pagenameId;
	private int functionalityId;
	private int userId;
	public int getScannerId() {
		return scannerId;
	}
	public void setScannerId(int scannerId) {
		this.scannerId = scannerId;
	}
	public int getEnv() {
		return env;
	}
	public void setEnv(int env) {
		this.env = env;
	}
	public int getPagenameId() {
		return pagenameId;
	}
	public void setPagenameId(int pagenameId) {
		this.pagenameId = pagenameId;
	}
	public int getFunctionalityId() {
		return functionalityId;
	}
	public void setFunctionalityId(int functionalityId) {
		this.functionalityId = functionalityId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
}

package com.xenon.buildmanager.domain;

public class MailGroupDetails {

	private int userId;
	private int mailGroupId;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getMailGroupId() {
		return mailGroupId;
	}
	public void setMailGroupId(int mailGroupId) {
		this.mailGroupId = mailGroupId;
	}
	
}

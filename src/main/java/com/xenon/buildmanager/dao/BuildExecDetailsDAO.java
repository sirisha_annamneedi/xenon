package com.xenon.buildmanager.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.xenon.buildmanager.domain.BuildExecDetails;

/**
 * @author bhagyashri.ajmera
 * 
 * This interface is to handle build execution related operations 
 *
 */
public interface BuildExecDetailsDAO {
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to insert build execution details
	 * 
	 * @param buildExecDetails
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int insertBuildExecDetails(BuildExecDetails buildExecDetails,String env, String schema)
			throws SQLException;
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to set build execution details value
	 * 
	 * @param buildId
	 * @param vmId
	 * @param browser
	 * @param mailGroupId
	 * @param stepWiseScreenshot
	 * @param reportBug
	 * @param userId
	 * @return
	 */
	BuildExecDetails setBuildExecDetailsValues(int buildId,
			String browser, int mailGroupId, int stepWiseScreenshot,
			int reportBug, int userId,int vmId,int envId);
	
	List<Map<String, Object>> getVMDetails(String schema, int userId,int buildId);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method updates VM status
	 * 
	 * @param schema
	 * @param userId
	 * @param buildId
	 * @return
	 */
	public int updateVMStatus(String schema,int vm_exe_status,int userId,int buildId);

	int changeVMStatus(String vmId, int status);

	int insertSfdcBuildExecDetails(BuildExecDetails buildExecDetails, String schema) throws SQLException;

	List<Map<String, Object>> getVMSfdcDetails(String schema, int userId, int buildId);

	List<Map<String, Object>> getCustomerVMDetailsbyId(int customerId, int vmId);

	int updateBuildExecDetails(BuildExecDetails buildExecDetails, String schema);
}

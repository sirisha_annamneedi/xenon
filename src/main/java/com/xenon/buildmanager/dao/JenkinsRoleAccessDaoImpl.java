package com.xenon.buildmanager.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.controller.BugController;

public class JenkinsRoleAccessDaoImpl implements JenkinsRoleAccessDao {
	private static final Logger logger = LoggerFactory.getLogger(BugController.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public boolean checkJenkinJobAccess(String roleAccess,String userId,String jobId,String schema) {
		boolean flag=false;
		logger.info("Calling Stored procedure to get role access details");
		SqlParameter userID = new SqlParameter("userId", Types.VARCHAR);
		SqlParameter access = new SqlParameter("roleAccess", Types.VARCHAR);
		SqlParameter jobID = new SqlParameter("jobId", Types.VARCHAR);
		SqlParameter[] paramArray = {access,userID,jobID};

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("checkJenkinsRoleAccess")
				.declareParameters(paramArray).withCatalogName(schema);
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(roleAccess,userId,jobId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		List<Map<String,Object>> returnList=(List<Map<String, Object>>) simpleJdbcCallResult.get("#result-set-1");
			if(!returnList.isEmpty()){
			flag=returnList.get(0).containsValue(1);	
			}	
		return flag;
	}
}

package com.xenon.buildmanager.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.buildmanager.domain.EnvironmentDetails;
import com.xenon.controller.JenkinsDetailsController;
import com.xenon.database.EnvironmentDetail;
import com.xenon.database.XenonQuery;

public class EnvironmentDetailsDaoImpl implements EnvironmentDetailsDao,XenonQuery{
	
	private static final Logger logger = LoggerFactory.getLogger(JenkinsDetailsController.class);
	
	@Autowired
	private com.xenon.database.DatabaseDao database;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int insertEnvDetails(EnvironmentDetails environmentDetail,String schema){
		int flag = database.update(String.format(environmentDetails.insertEnvDetatils, schema),

				new Object[] {environmentDetail.getEnvName(),environmentDetail.getEnvDescription(),environmentDetail.getConfigParameterName(),environmentDetail.getEnvStatus()},
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,Types.INTEGER});

		logger.info("Result : " + flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> getEnvDetails(String schema) {

		logger.info("SQL Query to get all environments");
		List<Map<String, Object>> envList;
		String sql = String.format(environmentDetails.getEnvDetails, schema,schema);
		envList = database.select(sql);
		logger.info("Query Result " + envList);
		return envList;
	}
	@Override
	public List<Map<String, Object>> getEnvDetailsById(String envId,String schema) {

		logger.info("SQL Query to get  environment details by Id");
		List<Map<String, Object>> envList;
		String sql = String.format(environmentDetails.getEnvDetailById, schema);
		envList = database.select(sql, new Object[] { envId },
				new int[] { Types.VARCHAR,});
		logger.info("Query Result " + envList);
		return envList;
	}
	
	@Override
	public int updateEnvDetails(String envId, String envName, String configParameterName,int envStatus,String envDescription, String schema) {

		logger.info("SQL Query to update environment details by Id");
		int flag = database.update(String.format(environmentDetails.updateEnvDetails, schema),
				new Object[] {envName,envDescription,configParameterName,envStatus,envId},
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,Types.VARCHAR,Types.INTEGER});

		logger.info("Result : " + flag);
		return flag;
	}
	
	@Override
	public int deleteEnvDetails(String envId,String schema) {

		logger.info("SQL Query to delete environment details by Id");
		int flag = database.update(String.format(environmentDetails.deleteEnvDetails, schema),
				new Object[] {envId},
				new int[] {Types.VARCHAR});

		logger.info("Result : " + flag);
		return flag;
	}
}

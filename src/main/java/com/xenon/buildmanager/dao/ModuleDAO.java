package com.xenon.buildmanager.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import com.xenon.buildmanager.domain.ModuleDetails;
import com.xenon.buildmanager.domain.UserModuleDetails;

/**
 * 
 * @author prafulla.pol
 *
 */
public interface ModuleDAO {
	
	/**
	 * This method is used to get active projects
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getActiveProjects(int userId,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to insert module
	 * @param module
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int insertModule(ModuleDetails module,String schema) throws SQLException;
	
	/**
	 * @author prafulla.pol 
	 * This method is used to get module details
	 * @param moduleId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getModuleDetails(int moduleId,String schema);
	
	/**
	 * This method is used to get module ID
	 * @param module
	 * @param schema
	 * @return
	 */
	int getModuleId(ModuleDetails module,String schema);
	
	/**
	 * @author prafulla.pol 
	 * This method is used to update module
	 * @param module
	 * @param schema
	 * @return
	 */
	boolean updateModule(ModuleDetails module,String schema);
	
	/**
	 * @author shantaram.tupe
	 * 30-Oct-2019:4:55:21 PM
	 * @param moduleId
	 * @param moduleStatus
	 * @param schema
	 * @return
	 */
	boolean updateModuleStatus(int moduleId, int moduleStatus, String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get all modules
	 * @param userID
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllModules(int userID,String schema);
	
	/**
	 * This method is used to get all module with test case case count
	 * @param userID
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllModulesWithTcCounts(int userID,String schema);
	
	/**
	 * This method is used to select module details by build Id
	 * @param buildId
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> selectModuleDetailsByBuildId(int buildId,int userId,
			String schema);
	
	/**
	 * This method is used to get module assigned user details
	 * @param moduleID
	 * @param projectID
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> getModuleAssignedUserDetails(int moduleID,int projectID,String schema);
	
	/**
	 * This method is used to get module un assigned user details
	 * @param moduleID
	 * @param projectID
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> getModuleUnAssignedUserDetails(int moduleID,int projectID,String schema);
	
	/**
	 * This method is used to delete assigned module records
	 * @param moduleID
	 * @param schema
	 * @return
	 */
	int deleteAssignedModuleRecords(int userId,int moduleID , String schema);
	
	/**
	 * This method is used to insert assigned module records
	 * @param moduleID
	 * @param userID
	 * @param projectID
	 * @param schema
	 * @return
	 */
	int insertAssignedModuleRecords(int moduleID,int userID,int projectID,String schema);
	
	/**
	 * This method is used to create new module status
	 * @param schema
	 * @param custTypeId
	 * @return
	 * @throws SQLException
	 */
	int createNewModuleStatus(String schema, int custTypeId)
			throws SQLException;
	
	/**
	 * This method is used to get module details by Id
	 * @param moduleId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getModuleDetailsById(int moduleId, String schema);
	
	/**
	 * This method is used to remove unassigned current project modules 
	 * @param projectID
	 * @param schema
	 * @return
	 */
	int removeUnassignedCurrentProjectModules(int projectID, String schema);
	
	/**
	 * This method is used to get module user details
	 * @param moduleID
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> moduleUserDetails(int moduleID,String schema);
	
	/**
	 * @author navnath.damale
	 * @param bugId
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> getBugModule(int bugId,String schema);
	
	/**
	 * This method is used to get all modules
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllModules(String schema);
	
	/**
	 * This method is used to remove already assigned modules
	 * @param projectID
	 * @param userId
	 * @param schema
	 * @return
	 */
	int removeAlreadyAssignedModules(int projectID,int userId,String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to select module details by build Id
	 * 
	 * @param moduleDetails
	 * @param moduleTcDetails
	 * @param moduleExecTcDetails
	 * @return
	 */
	List<Map<String, Object>> selectModuleDetailsByBuildId(
			List<Map<String, Object>> moduleDetails,
			List<Map<String, Object>> moduleTcDetails,
			List<Map<String, Object>> moduleExecTcDetails);
	
	/**
	 * This method is used to get assign module data
	 * @param projectId
	 * @param moduleId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getAssignModuleData(int projectId, int moduleId,
			String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to assign or unassign modules
	 * 
	 * @param userModuleDetails
	 * @param schema
	 */
	void assignOrUnassignModules(List<UserModuleDetails> userModuleDetails,
			String schema);

	Map<String, Object> getDataForModuleSummary(int moduleId, String schema);

	List<Map<String, Object>> getModulesByProjectAndUserId(int projectId, int userID, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives module details by name
	 * 
	 * @param moduleName
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getModuleDetailsByName(String moduleName, String schema);

	String sendMailOnAssignModule(List<Map<String, Object>> emailDetails, int moduleId, String appName, int userId,
			String userEmailId, String schema) throws Exception;

	int deleteModule(int moduleId, String schema);

	List<Map<String, Object>> getModulesByProject(int projectId, String schema);
	

	/** @author anmol.chadha **/
	int restoreModuleForTrash(int moduleId, String schema);
}

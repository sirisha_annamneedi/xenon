package com.xenon.buildmanager.dao;

import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.buildmanager.domain.UserNotification;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

public class NotificationDAOImpl implements NotificationDAO,XenonQuery{
	
	private static final Logger logger = LoggerFactory.getLogger(NotificationDAOImpl.class);
	
	@Autowired
	DatabaseDao database;

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public Map<String, Object> getNotifications(int userId,String schema)
			 {
		logger.info("Calling Stored procedure to read notifications");
		SqlParameter userIdTemp = new SqlParameter("userId", Types.VARCHAR);
		SqlParameter[] paramArray = { userIdTemp };
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).declareParameters(paramArray).withProcedureName("getHeaderData")
				.declareParameters().withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;

	}
	
	@Override
	public void insertNotificationRecords(final List<UserNotification> userNotificationList, String schema) {
		String sql = String.format(userNotifications.inserNewNotification, schema);
		logger.info("insert multiple assigned project details list");
		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public int getBatchSize() {
				return userNotificationList.size();
			}

			@Override
			public void setValues(java.sql.PreparedStatement ps, int i) throws SQLException {
				UserNotification userNotification = userNotificationList.get(i);
				ps.setInt(1, userNotification.getActivity());
				ps.setString(2, userNotification.getNotificationDescription());
				ps.setString(3,userNotification.getNotificationUrl());
				ps.setInt(4,userNotification.getUserId());
				ps.setInt(5,userNotification.getCreatedBy());
				ps.setInt(6,userNotification.getReadStatus());
				ps.setInt(7,userNotification.getNotificationStatus());
			}
		});
	}
	
	@Override
	public
	UserNotification setUserNotificationValues(int activity,String notificationDesc,String notificationUrl,int userId,int createdBy,int readStatus,int notificationStatus)
	{
		UserNotification userNotification=new UserNotification();
		 userNotification.setActivity(activity);
		 userNotification.setNotificationDescription(notificationDesc);
		 userNotification.setUserId(userId);
		 userNotification.setCreatedBy(createdBy);
		 userNotification.setReadStatus(readStatus);
		 userNotification.setNotificationStatus(notificationStatus);
		 userNotification.setNotificationUrl(notificationUrl);
		return userNotification;
		 
	}
	
	@Override
	public int inserNotificationRecord(UserNotification userNotification,String schema) {
		logger.info("Inserting new notification");
		logger.info("Query parameters : activity = {} Description = {}  Module = {}  User = {} Created by ={}, read status = {}, Activity Status = {}",
				userNotification.getActivity(),userNotification.getNotificationDescription(),userNotification.getNotificationUrl(),userNotification.getUserId(),userNotification.getCreatedBy(),userNotification.getReadStatus(),userNotification.getNotificationStatus());
		int flag = database.update(String.format(userNotifications.inserNotification, schema),
				new Object[]{userNotification.getActivity(),userNotification.getNotificationDescription(),userNotification.getNotificationUrl(),userNotification.getUserId(),userNotification.getCreatedBy(),userNotification.getReadStatus(),userNotification.getNotificationStatus()},
				new int[] {Types.INTEGER,Types.VARCHAR,Types.VARCHAR,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER});
		return flag;
	}
	
	@Override
	public int markNotificationAsRead(int notificationId,int userId,String schema)
	{
		logger.info("Marking Notification As Read ");
		logger.info("Query parameters : notificationId = {}  read status = {}, ",
				notificationId,1);
		int flag = database.update(String.format(userNotifications.markNotificationAsRead, schema),
				new Object[]{1,notificationId,userId},
				new int[] {Types.INTEGER,Types.INTEGER,Types.INTEGER});
		
		flag=database.update(String.format(userNotifications.markNotificationUserAsRead, schema),
				new Object[]{1,notificationId,userId},
				new int[] {Types.INTEGER,Types.INTEGER,Types.INTEGER});
		return flag;
	}
	
	@Override
	public Map<String, Object> updateNotificationReadCountFlag(int userId,String schema)
			 {
		logger.info("Calling Stored procedure to read notifications");
		SqlParameter userIdTemp = new SqlParameter("userId", Types.VARCHAR);
		SqlParameter[] paramArray = { userIdTemp };
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).declareParameters(paramArray).withProcedureName("updateNotificationReadCountFlag")
				.declareParameters().withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;

	}

}

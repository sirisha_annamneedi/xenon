package com.xenon.buildmanager.dao;

import com.xenon.buildmanager.domain.ProjectDetails;
import com.xenon.buildmanager.domain.UserProjectDetails;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

/**
 * 
 * @author prafulla.pol
 *
 */
public interface ProjectDAO {
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to insert new project to database 
	 * 
	 * @param projectDetails
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int insertProject(ProjectDetails projectDetails ,String schema) throws SQLException;
	
	/**
	 * @author prafulla.pol
	 * This method is used to get all projects
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> getAllProjects(String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get single project
	 * @param projectID
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> getSingleProject(int projectID,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to update project
	 * @param updateProject
	 * @param schema
	 * @return
	 */
	int updateProject(ProjectDetails updateProject,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get all project details
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllProjectsDetails(int userId,String schema);
	
	/**
	 * This method is used to get project assigned users
	 * @param projectID 
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> getProjectAssignedUsers(int projectID , String schema);
	
	/**
	 * This method is used to get project not assigned users
	 * @param projectId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getProjectNotAssignedUsers(int projectId,String schema);
	
	/**
	 * This method is used to delete unassigned project records
	 * @param projectID
	 * @param schema
	 * @return
	 */
	int deleteUnassignedProjectRecords(int projectID,String schema);
	
	/**
	 * This method is used to  insert assigned project records
	 * @param projectID
	 * @param userID
	 * @param schema
	 * @return
	 */
	int insertAssignedProjectRecords(int projectID,int userID,String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to select project details by build id
	 * 
	 * @param buildId
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> selectProjectDetailsByBuildId(int buildId,int userId,String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to select projects by build id 
	 * 
	 * @param buildId
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> selectProjectsByBuildId(int buildId,int userId,String schema);
	
	/**
	 * This method is used to get project assigned user
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getProjectAssignedUser(int userId,String schema);
	
	/**
	 * This method is used to get current assigned project
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getcurrentAssignedProject(int userId,String schema);
	
	/**
	 * This method is used to update current project
	 * @param userId
	 * @param projectId
	 * @param schema
	 * @return
	 */
	int updateCurrentProject(int userId, int projectId,
			String schema);
	
	/**
	 * This method is used to insert current project
	 * @param userId
	 * @param projectId
	 * @param schema
	 * @return
	 */
	int insertCurrentProject(int userId, int projectId, String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method checks cutomer's create project limit exceeds or not
	 * 
	 * @param schema
	 * @param custTypeId
	 * @return
	 * @throws SQLException
	 */
	int createNewProjectStatus(String schema, int custTypeId)
			throws SQLException;
	List<Map<String, Object>> getProjectDetailsById(int projectID, String schema);
	
	/**
	 * This method is used to select module detail
	 * @param buildId
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> selectModuleDetail(int buildId,int userId,String schema);
	
	/**
	 * This method is used to select user detail
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> selectUserDetail(int buildId, String schema);
	
	/**
	 * This method is used to select user detail by user
	 * @param buildId
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> selectUserDetailByUser(int buildId,int userId,String schema);
	
	/**
	 * This method is used to select user detail
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> selectUserDetail(String schema);
	
	/**
	 * This method is used to assign build
	 * @param buildId
	 * @param projectId
	 * @param moduleId
	 * @param userId
	 * @param schema
	 * @return
	 */
	int assignBuild(int buildId, int projectId, int moduleId, int userId,
			String schema);
	
	/**
	 * This method is used to remove build assignee
	 * @param buildId
	 * @param schema
	 * @return
	 */
	int removeBuildAssignee(int buildId,String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to read project wise TC count
	 * 
	 * @param projectId
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> prjWiseExecTcCount(int projectId, int buildId,
			String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to read project wise total TC count
	 * 
	 * @param projectId
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> prjWiseTotalTcCount(int projectId, int buildId,
			String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to read build wise executed TC count 
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> buildWiseExecTcCount(int buildId, String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to read build wise total TC count
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> buildWiseTotalTcCount(int buildId, String schema);
	
	/**
	 * This method is used to check project subset
	 * @param userId
	 * @param buildCreator
	 * @param schema
	 * @return
	 */
	public int checkProjectSubset(int userId, int buildCreator,String schema);
	
	/**
	 * This method is used to remove unassigned current project
	 * @param projectID
	 * @param schema
	 * @return
	 */
	int removeUnassignedCurrentProject(int projectID, String schema);
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to select project details by build id
	 * 
	 * @param projectDetails
	 * @param prjTcDetails
	 * @param prjExecTcDetails
	 * @return
	 */
	List<Map<String, Object>> selectProjectDetailsByBuildId(
			List<Map<String, Object>> projectDetails,
			List<Map<String, Object>> prjTcDetails,
			List<Map<String, Object>> prjExecTcDetails);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to insert multiple assigned project details list
	 * 
	 * @param userProjectDetailsList
	 * @param schema
	 */
	void insertAssignedProjectRecords(
			List<UserProjectDetails> userProjectDetailsList, String schema);
	
	/**
	 * @author bhagyashri.ajmera 
	 * 
	 * This method is to read query optimized project data
	 * 
	 * @param schema
	 * @return
	 */
	Map<String, Object> getProjectData(String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to read query optimized edit project data 
	 * 
	 * @param projectId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getEditProjectData(int projectId, String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to read query optimized update project data
	 * 
	 * @param updateProject
	 * @param schema
	 * @return
	 */
	Map<String, Object> updateProjectData(ProjectDetails updateProject,
			String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to read query optimized assign project data
	 * 
	 * @param projectId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getAssignProjectData(int projectId, String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to read query optimized assign or un assign project data
	 * 
	 * @param projectId
	 * @param schema
	 * @return
	 */
	Map<String, Object> assignOrUnassignProjectToUser(int projectId,
			String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to send mail on assign project
	 * 
	 * @param emailDetails
	 * @param emailNotificationDetails
	 * @param projectId
	 * @param userId
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	String sendMailOnAssignProject(List<Map<String, Object>> emailDetails, int projectId, String appName,
			int userId, String userEmailId, String schema) throws Exception;

	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to remove un-assigned project records
	 * 
	 * @param userId
	 * @param projectId
	 * @param schema
	 * @return
	 */
	int deleteUnassignedProjectRecords(ArrayList<Integer> userId,
			int projectId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is used to assign module to execute
	 * 
	 * @param buildId
	 * @param projectId
	 * @param moduleId
	 * @param userId
	 * @param updater
	 * @param schema
	 * @return
	 */
	Map<String, Object> assignModuleToExecute(int buildId, int projectId,
			int moduleId, int userId, int updater, String schema);

	Map<String, Object> getApplicationSettingsData(int applicationID, String schema);
	
	Map<String, Object> updateApplicationDetails(ProjectDetails updateProject,
			String schema);
	
	int unAssignAppToUser(int appID, int userID, String schema);
	
	/**
	 * @author
	 * @param action
	 * @param schema
	 * @return email details. 
	 */
	Map<String, Object> getMailSettings(String userAction);
	
	int getLoginUserAssignedAppCount(HttpSession session);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives project details by name
	 * 
	 * @param projectName
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getProjectDetailsByName(String projectName, String schema);
	
	List<Map<String, Object>> getMailSettingsFroAssignApp(String schema);
	
	int getProjectID(String projectName,String schema);

	int insertJiraProject(ProjectDetails projectDetails, String schema) throws SQLException;
	
	/**
	 * @author Abhay.Thakur
	 * 
	 * This method gives Jira credentials
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getJiraDetails(String schema);
}

package com.xenon.buildmanager.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.api.common.MailService;
import com.xenon.buildmanager.domain.ModuleDetails;
import com.xenon.buildmanager.domain.UserModuleDetails;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import com.xenon.testmanager.dao.TestcaseDAO;

/**
 * 
 * @author prafulla.pol
 *
 */
public class ModuleDAOImpl implements ModuleDAO, XenonQuery {

	private static final Logger logger = LoggerFactory.getLogger(ModuleDAOImpl.class);
	
	@Resource(name = "mailProperties")
	private Properties mailProperties;
	
	@Autowired
	DatabaseDao database;
	
	@Autowired
	MailService mailService;

	@Autowired
	TestcaseDAO testcaseDao;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Map<String, Object>> getActiveProjects(int userId, String schema) {
		logger.info("SQL Query to get Active Projects");
		List<Map<String, Object>> userRole = database.select(String.format(module.getActiveProjects, schema, schema),
				new Object[] { userId }, new int[] { Types.INTEGER });
		logger.info("Query Result  " + userRole);
		return userRole;
	}

	@Override
	public int insertModule(ModuleDetails modules, String schema) throws SQLException {
		logger.info("SQL Query to insert module");
		String callInsert = String.format(module.createNewModule, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsert);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, modules.getModuleName());
		callableStmt.setString(3, modules.getModuleDescription());
		callableStmt.setInt(4, modules.getModuleStatus());
		callableStmt.setInt(5, modules.getProjectId());
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Query insertion  Status = "+insertStatus);
		return insertStatus;

	}

	@Override
	public List<Map<String, Object>> getModuleDetails(int moduleId, String schema) {
		logger.info("SQL Query to get all module details");
		List<Map<String, Object>> moduleDetails = database.select(String.format(module.getModuleDetails, schema),
				new Object[] { moduleId }, new int[] { Types.INTEGER });
		logger.info("Query result = "+moduleDetails);
		return moduleDetails;
	}

	@Override
	public int getModuleId(ModuleDetails modules, String schema) {
		logger.info("SQL Query to get all module ID");
		List<Map<String, Object>> moduleDetails = database.select(String.format(module.getModuleId, schema),
				new Object[] { modules.getModuleName(), modules.getModuleDescription(), modules.getProjectId(),
						modules.getModuleStatus() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER });
		return Integer.parseInt(moduleDetails.get(0).get("module_id").toString());

	}

	@Override
	public boolean updateModule(ModuleDetails modules, String schema) {
		logger.info("SQL Query to update module");
		int updateStatus = database.update(String.format(module.updateModule, schema),
				new Object[] { modules.getModuleName(), modules.getModuleDescription(), modules.getModuleStatus(),
						modules.getModuleId() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER });
		logger.info("Update  Status = "+updateStatus);
		if (updateStatus != 0) {
			return true;
		} else
			return false;
	}

	@Override
	public boolean updateModuleStatus(int moduleId, int moduleStatus, String schema) {
		logger.info("SQL Query to update module status");
		int updateStatus = database.update(String.format(module.updateModuleStatus, schema),
							new Object[] { moduleStatus, moduleId }, new int[] { Types.INTEGER, Types.INTEGER } );
		logger.info("Update  Status = "+updateStatus);
		/*if (updateStatus != 0) {
			database.update(String.format(module.updateScenarioStatusByModuleId, schema),
					new Object[] { moduleStatus, moduleId }, new int[] { Types.INTEGER, Types.INTEGER } );
			
//			database.update(String.format("UPDATE  "+schema+".xetm_testcase set is_deleted=1 WHERE module_id=?", schema),
//					new Object[] {moduleId},new int[] { Types.INTEGER});
			return true;
		} else*/
			return true;
	}
	
	@Override
	public List<Map<String, Object>> getAllModules(int userID, String schema) {
		logger.info("SQL Query to get all module details");
		List<Map<String, Object>> moduleList;
		String sql = String.format(module.getAllModulesById, schema, schema, schema);
		moduleList = database.select(sql, new Object[] { userID }, new int[] { Types.INTEGER });
		logger.info("Query Result " + moduleList);
		return moduleList;
	}

	@Override
	public List<Map<String, Object>> getAllModulesWithTcCounts(int userID, String schema) {
		List<Map<String, Object>> moduleList;
		logger.info("SQL Query to get All Modules With Tc Counts ");
		moduleList = database.select(String.format(module.getAllModulesWithTcCounts, schema, schema, schema, schema),
				new Object[] { userID, userID }, new int[] { Types.INTEGER, Types.INTEGER });
		for (int i = 0; i < moduleList.size(); i++) {
			int moduleId = Integer.parseInt(moduleList.get(i).get("module_id").toString());
			int tcCount = (testcaseDao.getTestcasesByModuleId(moduleId, schema)).size();
			moduleList.get(i).put("testCasesCount", tcCount);
		}
		logger.info("Query result = "+moduleList);
		return moduleList;
	}
	
	@Override
	public String sendMailOnAssignModule(List<Map<String, Object>> emailDetails,
			int moduleId, String appName, int userId, String userEmailId, String schema)
					throws Exception {
			logger.info("sending mail on assign module");
			logger.info("Reading send mail data");
			
			ArrayList<String> recepients = new ArrayList<String>();
			recepients.add(userEmailId);
			
			String mailSubject = mailProperties.getProperty("ASSIGN_MODULE_SUBJECT", "New user created");
			String mailBodyText = mailProperties.getProperty("ASSIGN_MODULE_BODY_TEXT", "None");

			String from = emailDetails.get(0).get("smtp_user_id").toString();
			
			String message = mailService.postMail(emailDetails, recepients, mailSubject,
					String.format(mailBodyText, appName), from);
			
			return message;
	}

	@Override
	public List<Map<String, Object>> selectModuleDetailsByBuildId(int buildId, int userId, String schema) {
		String sql = String.format(module.selectModuleDetailsByBuildId, schema, schema, schema, schema, schema);
		logger.info("SQL Query to select Module Details By Build Id "+sql);
		List<Map<String, Object>> moduleDetails = database.select(sql, new Object[] { buildId, userId },
				new int[] { Types.INTEGER, Types.INTEGER });

		for (int i = 0; i < moduleDetails.size(); i++) {
			List<Map<String, Object>> moduleTcDetails = testcaseDao.getTestCaseDetailsByBuildAndModuleId(buildId,
					Integer.parseInt(moduleDetails.get(i).get("module_id").toString()), schema);
			List<Map<String, Object>> moduleExecTcDetails = testcaseDao.getExecutedTestCaseDetailsByBuildAndModuleId(
					buildId, Integer.parseInt(moduleDetails.get(i).get("module_id").toString()), schema);
			moduleDetails.get(i).put("TcCount", moduleTcDetails.get(0).get("TcCount"));
			moduleDetails.get(i).put("ExecutedTcCount", moduleExecTcDetails.get(0).get("TcCount"));
			if (Integer.parseInt(moduleExecTcDetails.get(0).get("TcCount").toString()) == 0) {
				moduleDetails.get(i).put("PassCount", 0);
				moduleDetails.get(i).put("FailCount", 0);
				moduleDetails.get(i).put("SkipCount", 0);
				moduleDetails.get(i).put("BlockCount", 0);
			} else {
				moduleDetails.get(i).put("PassCount", moduleExecTcDetails.get(0).get("PassCount"));
				moduleDetails.get(i).put("FailCount", moduleExecTcDetails.get(0).get("FailCount"));
				moduleDetails.get(i).put("SkipCount", moduleExecTcDetails.get(0).get("SkipCount"));
				moduleDetails.get(i).put("BlockCount", moduleExecTcDetails.get(0).get("BlockCount"));
			}
		}
		logger.info("Query result = "+moduleDetails);
		return moduleDetails;
	}

	@Override
	public List<Map<String, Object>> getModuleAssignedUserDetails(int moduleID, int projectID, String schema) {
		String sql = String.format(module.moduleAssignedUserDetails, schema, schema, schema);
		logger.info("SQL Query to get Module Assigned User Details "+sql);
		List<Map<String, Object>> assignedUserDetails = database.select(sql, new Object[] { projectID, moduleID },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query result = "+assignedUserDetails);
		return assignedUserDetails;
	}

	@Override
	public List<Map<String, Object>> getModuleUnAssignedUserDetails(int moduleID, int projectID, String schema) {
		String sql = String.format(module.moduleUnAssignedUserDetails, schema, schema, schema);
		logger.info("SQL Query to get Module Un-Assigned User Details "+sql);
		List<Map<String, Object>> unAssignedUserDetails = database.select(sql, new Object[] { projectID, moduleID },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query result = "+unAssignedUserDetails);
		return unAssignedUserDetails;
	}

	@Override
	public int deleteAssignedModuleRecords(int user_id,int moduleID, String schema) {
		String sql = String.format(module.deleteAssignedModuleRecords, schema);
		logger.info("SQL Query to delete Assigned Module Records "+sql);
		int flag = database.update(sql, new Object[] { user_id,moduleID }, new int[] { Types.INTEGER,Types.INTEGER });
		logger.info("Query result = "+flag);
		return flag;
	}

	@Override
	public int insertAssignedModuleRecords(int moduleID, int userID, int projectID, String schema) {
		String sql = String.format(module.insertAssignedModuleRecords, schema);
		logger.info("SQL Query to insert Assigned Module Records "+sql);
		int flag = database.update(sql, new Object[] { moduleID, userID, projectID },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER });
		logger.info("Query result = "+flag);
		return flag;
	}

	@Override
	public int createNewModuleStatus(String schema, int custTypeId) throws SQLException {
		String createNewModuleStatus = String.format(module.createNewModuleStatus, schema);
		logger.info("SQL Query to create New Module Status "+createNewModuleStatus);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(createNewModuleStatus);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setInt(2, custTypeId);
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Query result = "+insertStatus);
		return insertStatus;

	}

	@Override
	public List<Map<String, Object>> getModuleDetailsById(int moduleId, String schema) {
		logger.info("SQL Query to get Module Details By Id ");
		List<Map<String, Object>> moduleDetails = database.select(String.format(module.getModuleDetailsById, schema),
				new Object[] { moduleId }, new int[] { Types.INTEGER });
		logger.info("Query result = "+moduleDetails);
		return moduleDetails;
	}

	@Override
	public int removeUnassignedCurrentProjectModules(int projectID, String schema) {
		logger.info("SQL Query to remove Unassigned Current Project Modules ");
		int flag = database.update(String.format(module.removeUnassignedCurrentProjectModule, schema, schema),
				new Object[] { projectID, projectID }, new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query result = "+flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> moduleUserDetails(int moduleID, String schema) {
		String sql = String.format(module.moduleUserDetails, schema, schema);
		logger.info("SQL Query to module User Details "+sql);
		List<Map<String, Object>> userDetails = database.select(sql, new Object[] { moduleID },
				new int[] { Types.INTEGER });
		logger.info("Query result = "+userDetails);
		return userDetails;
	}

	@Override
	public List<Map<String, Object>> getAllModules(String schema) {
		List<Map<String, Object>> moduleList;
		String sql = String.format(module.getAllModules, schema);
		logger.info("SQL Query to get All Modules "+sql);
		moduleList = database.select(sql);
		logger.info("Query result = "+moduleList);
		return moduleList;
	}

	@Override
	public int removeAlreadyAssignedModules(int projectID, int userId, String schema) {
		logger.info("SQL Query to remove Already Assigned Modules ");
		int flag = database.update(String.format(module.removeAlreadyAssignedModules, schema),
				new Object[] { projectID, userId }, new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query result = "+flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> selectModuleDetailsByBuildId(List<Map<String, Object>> moduleDetails,
			List<Map<String, Object>> moduleTcDetails, List<Map<String, Object>> moduleExecTcDetails) {
		logger.info("Selecting module details by build id");
		for (int i = 0; i < moduleDetails.size(); i++) {

			logger.info("Adding pass, fail, skip and block test case counts");
			int moduleId = Integer.parseInt(moduleDetails.get(i).get("module_id").toString());

			for (int j = 0; j < moduleTcDetails.size(); j++) {
				if (moduleId == Integer.parseInt(moduleTcDetails.get(j).get("module_id").toString())) {
					moduleDetails.get(i).put("TcCount", moduleTcDetails.get(j).get("TcCount"));
					moduleDetails.get(i).put("ExecutedTcCount", 0);
					moduleDetails.get(i).put("PassCount", 0);
					moduleDetails.get(i).put("FailCount", 0);
					moduleDetails.get(i).put("SkipCount", 0);
					moduleDetails.get(i).put("BlockCount", 0);
					break;
				}
			}

			for (int j = 0; j < moduleExecTcDetails.size(); j++) {
				if (moduleId == Integer.parseInt(moduleExecTcDetails.get(j).get("mudule_id").toString())) {
					if (Integer.parseInt(moduleExecTcDetails.get(j).get("TcCount").toString()) == 0) {
						moduleDetails.get(i).put("PassCount", 0);
						moduleDetails.get(i).put("FailCount", 0);
						moduleDetails.get(i).put("SkipCount", 0);
						moduleDetails.get(i).put("BlockCount", 0);
					} else {
						moduleDetails.get(i).put("PassCount", moduleExecTcDetails.get(j).get("PassCount"));
						moduleDetails.get(i).put("FailCount", moduleExecTcDetails.get(j).get("FailCount"));
						moduleDetails.get(i).put("SkipCount", moduleExecTcDetails.get(j).get("SkipCount"));
						moduleDetails.get(i).put("BlockCount", moduleExecTcDetails.get(j).get("BlockCount"));
					}
					moduleDetails.get(i).put("ExecutedTcCount", moduleExecTcDetails.get(j).get("ExecutedTcCount"));
					break;
				} else {
					moduleDetails.get(i).put("ExecutedTcCount", 0);
					moduleDetails.get(i).put("PassCount", 0);
					moduleDetails.get(i).put("FailCount", 0);
					moduleDetails.get(i).put("SkipCount", 0);
					moduleDetails.get(i).put("BlockCount", 0);
				}
			}

		}
		logger.info("Return list = "+moduleDetails);
		return moduleDetails;
	}

	@Override
	public Map<String, Object> getAssignModuleData(int projectId, int moduleId, String schema) {
		logger.info("Calling stored procedure to get Assign Module Data");
		SqlParameter projectIdTemp = new SqlParameter("projectId", Types.VARCHAR);
		SqlParameter moduleIdTemp = new SqlParameter("moduleId", Types.VARCHAR);
		SqlParameter[] paramArray = { projectIdTemp, moduleIdTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getAssignModuleData")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(projectId, moduleId);
		logger.info("Procedure Result  " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public void assignOrUnassignModules(final List<UserModuleDetails> userModuleDetails, String schema) {
		logger.info("Updating assign and un assign modules");
		String sql = String.format(module.insertAssignedModules, schema);
		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public int getBatchSize() {
				return userModuleDetails.size();
			}

			@Override
			public void setValues(java.sql.PreparedStatement ps, int i) throws SQLException {
				UserModuleDetails moduleDetails = userModuleDetails.get(i);
				ps.setInt(1, moduleDetails.getModuleId());
				ps.setInt(2, moduleDetails.getUserId());
				ps.setInt(3, moduleDetails.getProjectId());

			}
		});

	}

	@Override
	public Map<String, Object> getDataForModuleSummary(int moduleId, String schema) {
		logger.info("In a method to decalre procedure for bug summary ");
		SqlParameter tempModuleId = new SqlParameter("moduleId", Types.INTEGER);
		SqlParameter[] paramArray = { tempModuleId };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getModuleSummary")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(moduleId);
		logger.info("Result after procedure call " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	
	
	@Override
	public List<Map<String, Object>> getBugModule(int bugId, String schema) {
		String sql = String.format(module.moduleBybugId, schema, schema);
		logger.info("SQL Query to module User Details "+sql);
		List<Map<String, Object>> moduleDetails = database.select(sql, new Object[] { bugId },
				new int[] { Types.INTEGER });
		logger.info("Query result = "+moduleDetails);
		return moduleDetails;
	}
	
	@Override
	public List<Map<String, Object>> getModulesByProjectAndUserId(int projectId,int userID, String schema) {
		logger.info("SQL Query to get all module details");
		List<Map<String, Object>> moduleList;
		String sql = String.format(module.getModulesByProjectAndUserId, schema, schema, schema);
		moduleList = database.select(sql, new Object[] { userID ,projectId}, new int[] { Types.INTEGER,Types.INTEGER });
		logger.info("Query Result " + moduleList);
		return moduleList;
	}
	
	@Override
	public List<Map<String, Object>> getModulesByProject(int projectId,String schema) {
		logger.info("SQL Query to get all module details");
		List<Map<String, Object>> moduleList;
		String sql = String.format(module.getModuleDetailsByProject, schema);
		moduleList = database.select(sql, new Object[] { projectId}, new int[] { Types.INTEGER });
		logger.info("Query Result " + moduleList);
		return moduleList;
	}
	
	@Override
	public List<Map<String, Object>> getModuleDetailsByName(String moduleName,String schema) {
		List<Map<String, Object>> moduleDetails;
		String sql = String.format(module.getModuleDetailsByName, schema);
		logger.info("SQL Query to get All Modules "+sql);
		moduleDetails = database.select(sql,new Object[]{moduleName},new int[] {Types.VARCHAR});
		logger.info("Query result = "+moduleDetails);
		return moduleDetails;
	}
	
	@Override
	public int deleteModule(int moduleId, String schema) {
		String sql = String.format(module.deleteModule, schema);
		int flag = database.update(sql, new Object[] { moduleId }, new int[] { Types.INTEGER });
		logger.info("Query result = "+flag);
		return flag;
	}
	/** @author anmol.chadha **/
	@Override
	public int restoreModuleForTrash(int moduleId, String schema) {
		logger.info("In method get trash test case");
		int dataModuleTrash;
		dataModuleTrash = database
				.update("UPDATE " + schema + ".xe_module SET module_active = 1 WHERE module_id=" + moduleId);

		return dataModuleTrash;
	}

}

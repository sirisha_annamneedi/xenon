package com.xenon.buildmanager.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import com.xenon.buildmanager.domain.TrialBuildDetails;

/**
 * 
 * @author
 * @description Interface to perform JDBC operations on automation build
 * 
 */
public interface AutomationBuildDAO {
	/**
	 * @author suresh.adling
	 * @description insert a trial build by static input
	 * @param insertBuild
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int insertBuild(TrialBuildDetails insertBuild, String schema) throws SQLException;

	/**
	 * @author
	 * @description get all build details
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllBuildDetails(String schema);

	/**
	 * @author
	 * @description get build details by build name
	 * @param schema
	 * @param buildName
	 * @return
	 */
	List<Map<String, Object>> getBuildDetails(String schema, String buildName);

	/**
	 * @author
	 * @description update user trial log file
	 * @param buildName
	 * @param logFilename
	 * @param schema
	 * @return
	 */
	int updateTrialLogfile(String buildName, String logFilename, String schema);

	/**
	 * @author
	 * @description insert automation build
	 * @param buildName
	 * @param userId
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int insertAutoBuild(String buildName, int userId, String schema) throws SQLException;

	/**
	 * @author
	 * @description get automation build details
	 * @param schema
	 * @param buildName
	 * @return
	 */
	List<Map<String, Object>> getAutoBuildDetails(String schema, String buildName);

	/**
	 * @author
	 * @description get automation build details by build id
	 * @param schema
	 * @param buildId
	 * @return
	 */
	List<Map<String, Object>> getAutoBuildDetail(String schema, int buildId);

	/**
	 * @author
	 * @description get automation test cases
	 * @param schema
	 * @param buildId
	 * @return
	 */
	List<Map<String, Object>> getAutomationTestCases(String schema, int buildId);

	/**
	 * @author
	 * @description update build file
	 * @param buildId
	 * @param logFilename
	 * @param schema
	 * @return
	 */
	int updateBuildLogfile(int buildId, String logFilename, String schema);

	/**
	 * @author
	 * @description insert execution build record
	 * @param buildStamp
	 * @param buildId
	 * @param schema
	 * @return
	 */
	int insertExecution(String buildStamp, int buildId, String schema);
	
	/**
	 * @author shantaram.tupe 
	 * @description
	 * 06-Mar-2020 1:11:27 PM
	 * @param buildId
	 * @param schema
	 * @return
	 * List<String>
	 */
	List<String> getTestPrefixes( Integer buildId, String schema );
	
	/**
	 * 
	 * @param json
	 * @param vmIp
	 * @param vmPort
	 * @param vmId
	 * @param buildId
	 * @param userId
	 * @param custId
	 * @param schema
	 * @return
	 */
	int insertbuildExecDetails(String json, String vmIp,String vmPort, int vmId,int buildId, int userId, int custId, String schema);

	int updateScriptBuildLogfile(int buildId, String logFilename, String schema);

	int insertScriptBuildExecution(String buildStamp, int build_id, String schema);

	List<Map<String, Object>> getScriptBuildDetail(String schema, int buildId);

	List<Map<String, Object>> getScriptlessTestCases(String schema, int buildId);

	List<Map<String, Object>> getApiTestCases(String schema, int buildId);

}

package com.xenon.buildmanager.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.xenon.buildmanager.domain.EnvironmentVariableDetails;
import com.xenon.controller.JenkinsDetailsController;
import com.xenon.database.XenonQuery;

public class EnvVariableDetailsDaoImpl implements EnvVariableDetailsDao,XenonQuery{

private static final Logger logger = LoggerFactory.getLogger(JenkinsDetailsController.class);
	
	@Autowired
	private com.xenon.database.DatabaseDao database;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Override
	public int insertEnvVariableDetails(EnvironmentVariableDetails environmentVariableDetails,String schema){
		int flag = database.update(String.format(envVariable.insertVariables, schema),

				new Object[] {environmentVariableDetails.getVariableName(),environmentVariableDetails.getConfigValue(),environmentVariableDetails.getVariableValue(),environmentVariableDetails.getEnvId()},
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR});

		logger.info("Result : " + flag);
		return flag;
	}
	
	@Override
	public List<Map<String, Object>> getEnvVariableDetails(String envId,String schema) {

		logger.info("SQL Query to get all environment variables");
		List<Map<String, Object>> variableList;
		String sql = String.format(envVariable.getEnvVariableData, schema);
		variableList = database.select(sql, new Object[] { envId },
				new int[] { Types.VARCHAR,});
		logger.info("Query Result " + variableList);
		return variableList;
	}
	
	@Override
	public int deleteVariable(String schema,String variableId){
		logger.info("SQL Query to delete variable from table");
		String sql=String.format(envVariable.deleteVariable, schema);
		int flag=database.update(sql,new Object[]{variableId},new int[]{Types.VARCHAR});
		return flag;
	}
	
	@Override
	public int updateVariableDetails(String updateVariableName,String updateConfigValue,String updateVariableValue,String updateVariableId,String schema){
		logger.info("SQL Query to update variable from table");
		String sql=String.format(envVariable.updateVariable, schema);
		int flag=database.update(sql,new Object[]{updateVariableName,updateConfigValue,updateVariableValue,updateVariableId},new int[]{Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR});
		return flag;
	}
	
}

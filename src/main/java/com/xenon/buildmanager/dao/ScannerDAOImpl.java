package com.xenon.buildmanager.dao;

import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.buildmanager.domain.PageScanDetails;
import com.xenon.buildmanager.domain.RepositoryDetails;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

public class ScannerDAOImpl implements ScannerDAO,XenonQuery{
	
	private static final Logger logger = LoggerFactory.getLogger(ScannerDAOImpl.class);

	@Autowired
	DatabaseDao database;
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public Map<String, Object> insertPageScanDetails(PageScanDetails PageScanDetails, String schema) throws SQLException {
		
		logger.info("In procedure to insert page scan data");
		
		SqlParameter env = new SqlParameter("envId", Types.INTEGER);
		SqlParameter page = new SqlParameter("pageId", Types.INTEGER);
		SqlParameter fun = new SqlParameter("functionalityId", Types.INTEGER);
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = { env, page, fun, user};

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("insertpagescandetails")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(PageScanDetails.getEnv(), 
				PageScanDetails.getPagenameId(), PageScanDetails.getFunctionalityId(), PageScanDetails.getUserId());
		
		logger.info("Result " + simpleJdbcCallResult);
		
		return simpleJdbcCallResult;
	}

	
	@Override
	public Map<String, Object> getSfdcGetScanPageData(String schema) {
		logger.info("Calling Stored procedure to build Execution Details");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("sfdcGetScanPageData")
				.declareParameters().withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	
	@Override
	public Map<String, Object> getSfdcLocatorsPageData(int scannerId,String schema) {
		
		logger.info("Calling Stored procedure to build Execution Details");
		SqlParameter scannerIdTemp = new SqlParameter("scannerId", Types.INTEGER);
		SqlParameter[] paramArray = { scannerIdTemp};
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("sfdcLocatorsPageData")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(scannerId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	
	@Override
	public int getScannerIdByDetails(PageScanDetails pageScanDetails, String schema) {
		List<Map<String, Object>> scannerId = database.select(String.format(scanner.getScannerIdByDetails, schema),
				new Object[] { pageScanDetails.getEnv(),pageScanDetails.getPagenameId(),pageScanDetails.getFunctionalityId() },
				new int[] { Types.INTEGER, Types.INTEGER,Types.INTEGER });
		int Id = (Integer) scannerId.get(0).get("scanner_id");
		logger.info("SQL Result scanner Id  is " + Id);
		return Id;
	}
	
	@Override
	public void insertSfdcRepositoryRecords(final List<RepositoryDetails> repositoryDetailsList, String schema) {
		String sql = String.format(scanner.insertSfdcRepositoryRecords, schema);
		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public int getBatchSize() {
				return repositoryDetailsList.size();
			}

			@Override
			public void setValues(java.sql.PreparedStatement ps, int i) throws SQLException {
				RepositoryDetails repositoryDetails = repositoryDetailsList.get(i);
				ps.setInt(1, repositoryDetails.getScannerId());
				ps.setString(2, repositoryDetails.getLabel());
				ps.setInt(3, repositoryDetails.getType());
				ps.setString(4, repositoryDetails.getXpath());

			}
		});
	}
	
	@Override
	public Map<String, Object> getSfdcAddStepPageData(int scannerId,int testcaseId, int envId, String schema) {
		
		logger.info("Calling Stored procedure to build Execution Details");
		SqlParameter scannerIdTemp = new SqlParameter("scannerId", Types.INTEGER);
		SqlParameter testcaseIdTemp = new SqlParameter("testcaseId", Types.INTEGER);
		SqlParameter envIdTemp = new SqlParameter("envId", Types.INTEGER);
		SqlParameter[] paramArray = { scannerIdTemp, testcaseIdTemp, envIdTemp};
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("sfdcAddStepPageData")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(scannerId, testcaseId, envId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	
	@Override
	public List<Map<String, Object>> getAllLocatorType(String schema)
	{
		String sql = String.format(scanner.getAllLocatorType, schema);
		List<Map<String,Object>> locatorList = database.select(sql
				);
		return locatorList; 
	}
	
	
	@Override
	public PageScanDetails setPageScanDetailsValues(int env,int pagenameId,int funcId,int userId)
	{
		logger.info("Setting page scan details domain");
		PageScanDetails pageScanDetails=new PageScanDetails();
		pageScanDetails.setEnv(env);
		pageScanDetails.setPagenameId(pagenameId);
		pageScanDetails.setFunctionalityId(funcId);
		pageScanDetails.setUserId(userId);
		logger.info("Returning page scan details domain");
		return pageScanDetails;
	}


	@Override
	public int getScannerId(int sfdcenv, int sfdcpage, int sfdcfunc, int userID, String schema) {
		logger.info("Getting Scanner ID");
		String sql = String.format(scanner.getScannerId, schema);
		List<Map<String, Object>> testCaseData= database.select(sql, 
				new Object[]{sfdcenv, sfdcpage, sfdcfunc}, 
				new int[]{Types.INTEGER, Types.INTEGER, Types.INTEGER});
		
		int id = 0;
		
		if(testCaseData.size() > 0) {
			id = Integer.parseInt(testCaseData.get(0).get("scanner_id").toString());
		}
		
		logger.info("Return value = "+id);
		return id;
	}


	@Override
	public Map<String, Object> getSfdcCreateTCData(String schema) {
		logger.info("Calling Stored procedure to get create test case Details");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("sfdcCreateTCData")
				.declareParameters().withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	
	
}

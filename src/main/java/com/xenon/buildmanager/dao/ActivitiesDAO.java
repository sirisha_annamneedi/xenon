package com.xenon.buildmanager.dao;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import com.xenon.buildmanager.domain.RecentActivity;

/**
 * This interface handles recent activities related operations
 * 
 * @author bhagyashri.ajmera
 *
 */
public interface ActivitiesDAO {
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to retrieve top five recent activities from database
	 * 
	 * @param schema
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	List<Map<String,Object>> getTopActivities(String schema) throws UnsupportedEncodingException;
	
	/**
	 * 
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to insert new activity to database
	 * 
	 * @param recentActivity
	 * @param schema
	 * @return
	 */
	int inserNewActivity(RecentActivity recentActivity, String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to set recent activities domain values
	 * 
	 * @param activity
	 * @param activityDesc
	 * @param module
	 * @param user
	 * @param activityStatus
	 * @return
	 */
	RecentActivity setRecentActivitiesValues(int activity, String activityDesc,
			String module, int user, int activityStatus);
	

}

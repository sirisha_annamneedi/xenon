package com.xenon.buildmanager.dao.exception;

/**
 * Exception raised when expected user is  not found
 * @author Dhananjay Deshmukh
 * @version 1.0
 */
public class UserNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3562720915399370620L;

	public UserNotFoundException() {
		super();
	}

	public UserNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UserNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public UserNotFoundException(String message) {
		super(message);
	}

	public UserNotFoundException(Throwable cause) {
		super(cause);
	}
	
	@Override
	public String toString() {
		return "UserNotFoundException [Message=" + getMessage() + "]";
	}
}

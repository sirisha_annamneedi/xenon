package com.xenon.buildmanager.dao.exception;

/**
 * Exception raised when expected Project is  not found
 * @author Dhananjay Deshmukh
 * @version 1.0
 */
public class ProjectNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3403460583998446201L;

	public ProjectNotFoundException() {
		super();
	}

	public ProjectNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ProjectNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProjectNotFoundException(String message) {
		super(message);
	}

	public ProjectNotFoundException(Throwable cause) {
		super(cause);
	}

	@Override
	public String toString() {
		return "ProjectNotFoundException [Message=" + getMessage() + "]";
	}

}

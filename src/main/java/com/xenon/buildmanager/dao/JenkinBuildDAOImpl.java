package com.xenon.buildmanager.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.buildmanager.domain.JenkinBuildDetails;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

/**
 * 
 * @author bhagyashri.ajmera
 *
 */
public class JenkinBuildDAOImpl implements JenkinBuildDAO, XenonQuery {

	static Connection conn = null;
	final Logger logger = LoggerFactory.getLogger(JenkinBuildDAOImpl.class);

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	private DatabaseDao database;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method is used to insert build details in xenon
	 * 
	 * @param buildDetailsList
	 * @param url
	 * @param user
	 * @param pass
	 * @param schema
	 * @throws SQLException
	 */
	public int insertBuild(JenkinBuildDetails buildDetails, String schema) {
		logger.info("Insert Jenkins build");
		int flag = database.update(String.format(jenkinsDetails.insertJenkinBuild, schema),
				new Object[] { 0, buildDetails.getBuildId(), buildDetails.getBuildName(), buildDetails.getTotalCount(),
						buildDetails.getFailCount(), buildDetails.getSkipCount(), buildDetails.getDuration(),
						buildDetails.getEstimatedDuration(), buildDetails.getResult(), buildDetails.getTimestamp(),
						buildDetails.getUrl(), buildDetails.getJobId() },
				new int[] { Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER });
		logger.info("Result : " + flag);

		return flag;
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method is used to set build details
	 * 
	 * @param buildDetailsJson
	 * @return
	 * @throws JSONException
	 */
	public JenkinBuildDetails setBuildDetailsFromBuildJSON(JSONObject buildDetailsJson, int jobId)
			throws JSONException {
		JenkinBuildDetails buildDetails = new JenkinBuildDetails();
		buildDetails.setBuildId(Integer.parseInt(buildDetailsJson.get("id").toString()));
		buildDetails.setBuildName(buildDetailsJson.get("fullDisplayName").toString());
		buildDetails.setDuration(buildDetailsJson.get("duration").toString());
		buildDetails.setEstimatedDuration(buildDetailsJson.get("estimatedDuration").toString());
		JSONArray jsonArray = new JSONArray(buildDetailsJson.get("actions").toString());
		for (int j = 0; j < jsonArray.length(); j++) {
			if (jsonArray.get(j).toString().contains("totalCount")) {
				JSONObject countData = new JSONObject(jsonArray.get(j).toString());
				buildDetails.setTotalCount(Integer.parseInt(countData.get("totalCount").toString()));
				buildDetails.setSkipCount(Integer.parseInt(countData.get("skipCount").toString()));
				buildDetails.setFailCount(Integer.parseInt(countData.get("failCount").toString()));
				break;
			}
		}
		buildDetails.setJobId(jobId);
		if (buildDetailsJson.get("result").toString().equalsIgnoreCase("SUCCESS"))
			buildDetails.setResult(1);
		else if (buildDetailsJson.get("result").toString().equalsIgnoreCase("UNSTABLE"))
			buildDetails.setResult(2);
		else if (buildDetailsJson.get("result").toString().equalsIgnoreCase("FAILURE"))
			buildDetails.setResult(3);
		else
			buildDetails.setResult(4);

		buildDetails.setTimestamp(buildDetailsJson.get("timestamp").toString());
		buildDetails.setUrl(buildDetailsJson.get("url").toString());
		return buildDetails;
	}

	@Override
	public Map<String, Object> updateJenkinsBuildDetails(String jobName, String apiToken, String buildNo) {
		logger.info("Reads optimized edit user data");
		SqlParameter jobNameTemp = new SqlParameter("jobName", Types.VARCHAR);
		SqlParameter tokenTemp = new SqlParameter("apiToken", Types.VARCHAR);

		SqlParameter[] paramArray = { jobNameTemp, tokenTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getJenkinsInstanceDetails")
				.declareParameters(paramArray).withCatalogName("`xenon-core`");

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(jobName, apiToken);
		logger.info("Return list = " + simpleJdbcCallResult);

		return simpleJdbcCallResult;

	}

	@Override
	public List<Map<String, Object>> getBuildDetails(int jobId, String schema) {
		logger.info("get job details");
		String sql = String.format(jenkinsDetails.getBuildDetails, schema);
		logger.info("Running Sql query = " + sql);
		List<Map<String, Object>> jobDetails = database.select(sql, new Object[] { jobId },
				new int[] { Types.INTEGER });
		logger.info("Query details = { }", jobDetails);
		return jobDetails;
	}
	
	@Override
	public List<Map<String, Object>> getBuildDetailsByBuildNumber(int jobId, String schema, int buildId) {
		logger.info("get job details");
		String sql = String.format(jenkinsDetails.getBuildDetailsByBuildNumber, schema);
		logger.info("Running Sql query = " + sql);
		List<Map<String, Object>> eachBuild = database.select(sql, new Object[] { jobId, buildId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query details = { }", eachBuild);
		return eachBuild;
	}
	
	@Override
	public List<Map<String,Object>> getBuildTriggerDetails(String schema,int jobId) {
		logger.info("get job details");
		String sql = String.format(jenkinsDetails.getTriggerBuildData, schema,schema);
		logger.info("Running Sql query = " + sql);
		List<Map<String, Object>> jobDetails = database.select(sql,new Object[] { jobId},new int[] { Types.INTEGER});
		logger.info("Query details = { }", jobDetails);
		return jobDetails;
	}
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method is used to insert build details in xenon
	 * 
	 * @param buildDetailsList
	 * @param url
	 * @param user
	 * @param pass
	 * @param schema
	 * @throws SQLException
	 */
	@Override
	public int updateBuild(JenkinBuildDetails buildDetails, String schema) {
		logger.info("Insert Jenkins build");
		int flag = database.update(String.format(jenkinsDetails.updateJenkinBuild, schema),
				new Object[] {  buildDetails.getBuildId(),  buildDetails.getTotalCount(),
						buildDetails.getFailCount(), buildDetails.getSkipCount(), buildDetails.getDuration(),
						buildDetails.getEstimatedDuration(), buildDetails.getResult(), buildDetails.getTimestamp(),
						buildDetails.getUrl(), buildDetails.getJobId() ,buildDetails.getBuildName()},
				new int[] { Types.INTEGER,  Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.VARCHAR });
		logger.info("Result : " + flag);
		if(flag==1){
			flag = database.update(String.format(jenkinsDetails.updateJobStatus, schema),
					new Object[] {  buildDetails.getResult(), buildDetails.getJobId() },
					new int[] { Types.INTEGER,  Types.INTEGER });
			logger.info("Result : " + flag);
		}
		return flag;
	}

}

package com.xenon.buildmanager.dao;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.xenon.buildmanager.domain.UserDetails;

/**
 * @author dhananjay.deshmukh
 *
 */
/**
 * @author dhananjay.deshmukh
 *
 */
public interface UserDAO {
	/**
	 * @author suresh.adling
	 * @description insert user details
	 * @param user
	 * @param schema
	 * @return
	 */
	int insertUser(UserDetails user, String schema);

	/**
	 * @author suresh.adling
	 * @description get user roles
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getUserRole(String schema);

	/**
	 * @author suresh.adling
	 * @description get user id by user details
	 * @param user
	 * @param schema
	 * @return
	 */
	int getUserId(UserDetails user, String schema);

	/**
	 * @author suresh.adling
	 * @description get user details by id
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getUserDetails(int userId, String schema);

	
	/**
	 * @param emailId
	 * @return
	 */
	String checkUser(String emailId);

	/**
	 * @author suresh.adling
	 * @description get all user details
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllUserDetails(String schema);

	/**
	 * @author suresh.adling
	 * @description update user details
	 * @param user
	 * @param schema
	 * @return
	 */
	boolean updateUser(UserDetails user, String schema);

	/**
	 * @author suresh.adling
	 * @description insert user record into xenon core
	 * @param user
	 * @param schema
	 * @return
	 */
	int insertUserIntoCore(UserDetails user, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method is to insert admin user details
	 * 
	 * @param user
	 * @param schema
	 * @return
	 */
	int insertAdminUser(UserDetails user, String schema);

	/**
	 * @author
	 * @description get admin user id by user details
	 * @param user
	 * @param schema
	 * @return
	 */
	int getAdminUserId(UserDetails user, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method is to read admin user details
	 * 
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAdminUserDetails(String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method is to update user with profile photo
	 * 
	 * @param user
	 * @param schema
	 * @return
	 */
	boolean updateUserWithProfilePhoto(UserDetails user, String schema);

	/**
	 * @author
	 * @description get user details by email id
	 * @param emailId
	 * @return
	 * @throws Exception 
	 */
	List<Map<String, Object>> getUserDetailsByEmailId(String emailId);

	/**
	 * @author
	 * @description check user email id exist or not
	 * @param emailId
	 * @return
	 */
	boolean checkUserEmailIdExist(String emailId);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method sends mail on user forgot password
	 * 
	 * @param custDetails
	 * @param user
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	String sendMailOnForgotPassword(List<Map<String, Object>> custDetails, UserDetails user, String schema)
			throws Exception;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method is check customer's create user limit exceeds or not
	 * 
	 * @param schema
	 * @param custTypeId
	 * @return
	 * @throws SQLException
	 */
	int createNewUserStatus(String schema, int custTypeId) throws SQLException;

	/**
	 * @author suresh.adling
	 * @description get user detail by id
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getUserDetailsById(int userId, String schema);

	/**
	 * @author
	 * @description get update password user details
	 * @param userId
	 * @param schema
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	List<Map<String, Object>> getUpdatePasswordUserDetails(int userId, String schema) throws NoSuchAlgorithmException;

	/**
	 * @author
	 * @description update all users bug tracker and test manager status
	 * @param tmStatus
	 * @param btStatus
	 * @param schema
	 * @return
	 */
	int updateAllUsersStatus(int tmStatus, int btStatus, String schema);

	/**
	 * @author suresh.adling
	 * @description get user assigned project details
	 * @param userID
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> userAssignedProjectDetails(int userID, String schema);

	/**
	 * @author
	 * @description remove user (make user status 3)
	 * @param userid
	 * @param schema
	 * @return
	 */
	String removeUser(int userid, String schema);

	/**
	 * @author suresh.adling
	 * @description get user role by user id
	 * @param id
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getUserRoleById(int id, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads query optimized create user data
	 * 
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getCreateUserData(int userId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method is query optimized insert user details
	 * 
	 * @param user
	 * @param schema
	 * @return
	 */
	Map<String, Object> insertUserDetails(UserDetails user, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method sends mail on create user
	 * 
	 * @param emailDetails
	 * @param emailNotificationDetails
	 * @param customer_id
	 * @param user
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	String sendMail(List<Map<String, Object>> emailDetails,
			String customerName, UserDetails user, String schema) throws Exception;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads optimized edit user data
	 * 
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getEditUserData(int userId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads optimized view user data
	 * 
	 * @param schema
	 * @return
	 */
	Map<String, Object> getViewUserData(int startValue, int offsetValue, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads optimized update user data
	 * 
	 * @param user
	 * @param schema
	 * @return
	 */
	Map<String, Object> updateUserData(UserDetails user, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads optimized user assigned projects
	 * 
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getUserAssignedProjects(int userId, String schema);
	/**
	 *  @author suresh.adling
	 *  This method reads list of applications to assign  
	 * @param seletcedUser
	 * @param currentUser
	 * @param schema
	 * @return
	 */
	Map<String, Object> getApplicationsToAssign(int seletcedUser,int currentUser, String schema);
	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads user deatils by mail group id
	 * 
	 * @param mailGroupId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getUsersDetailsByMailGroupId(int mailGroupId, String schema);
	
	/**
	 * @author prafulla.pol
	 * @param userId
	 * @param schema
	 * @return
	 * This method is used to get the user email id based on the userID
	 */
	String getUserEmailID(int userId, String schema);
	
	/**
	 * @author prafulla.pol
	 * @param emailIDs
	 * @param schema
	 * @return
	 * Used to get the user details by using their Email IDs 
	 */
	List<Map<String, Object>> getUsersByEmailIds(String emailIDs, String schema);
	
	/**
	 * @author prafulla.pol
	 * @param emailIDs
	 * @param schema
	 * @return user Id 
	 * This method takes user email Id as its input parameter and based on that it returns users ID 
	 */
	int getUserIDbyEmail(String emailID, String schema);

	/**
	 * @author Dhananjay Deshmukh
	 * @param schema
	 * @return
	 */
	boolean isMailSettingEnabled(String schema);

	boolean updatePasswordByUserId(String userId, String password,String schema);

	boolean checkUserName(String userName);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * @param emailId
	 * @param userName
	 * @param password
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	String resetPasswordForLdapUsers(String emailId, String userName, String password, String schema) throws Exception;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllLdapInactiveUserDetails(String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllLdapActiveUserDetails(String schema);
}

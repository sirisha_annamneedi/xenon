package com.xenon.buildmanager.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.buildmanager.domain.BuildExecDetails;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

import bsh.org.objectweb.asm.Type;

public class BuildExecDetailsDAOImpl implements BuildExecDetailsDAO,XenonQuery{
	
	private static final Logger logger = LoggerFactory.getLogger(BuildExecDetailsDAOImpl.class);
	
	@Autowired
	DatabaseDao database;
	
	@Override
	public int insertBuildExecDetails(BuildExecDetails buildExecDetails,String env,String schema) throws SQLException {
		
		logger.info("Inserting build execution details");
		String callinsertVmDetails = String.format(build.insertBuildExecDetails, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callinsertVmDetails);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setInt(2, buildExecDetails.getBuildId());
		callableStmt.setInt(3, buildExecDetails.getVmId());
		callableStmt.setString(4, buildExecDetails.getBrowser());
		callableStmt.setInt(5, buildExecDetails.getMailGroupId());
		callableStmt.setInt(6, buildExecDetails.getStepWiseScreenshot());
		callableStmt.setInt(7, buildExecDetails.getReportBug());
		callableStmt.setInt(8, buildExecDetails.getUserId());
		callableStmt.setInt(9, buildExecDetails.getEnvId());
		callableStmt.setString(10, env);
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		logger.info("Insert status = "+insertStatus);
		callableStmt.close();
		return insertStatus;
	}
	
	@Override
	public BuildExecDetails setBuildExecDetailsValues(int buildId,String browser,int mailGroupId,int stepWiseScreenshot,int reportBug,int userId,int vmId,int envId)
	{
		logger.info("Setting build execution details value");
		BuildExecDetails buildExecDetails=new BuildExecDetails();
		buildExecDetails.setBuildId(buildId);
		buildExecDetails.setBrowser(browser);
		buildExecDetails.setMailGroupId(mailGroupId);
		buildExecDetails.setStepWiseScreenshot(stepWiseScreenshot);
		buildExecDetails.setReportBug(reportBug);
		buildExecDetails.setUserId(userId);
		buildExecDetails.setVmId(vmId);
		buildExecDetails.setEnvId(envId);
		logger.info("Returning build exec details domain");
		return buildExecDetails;
	}
	
	@Override
	public List<Map<String, Object>> getVMDetails(String schema,int userId,int buildId) {
		logger.info("Reading vm details");
		String sql = String.format(build.getVM,schema); 
		List<Map<String,Object>> getVMDetails = database.select(sql,new Object[]{userId,buildId},new int[]{Types.INTEGER,Types.INTEGER});
		logger.info("return list "+getVMDetails);
		return getVMDetails;
	}
	
	@Override
	public int updateVMStatus(String schema,int vm_exe_status,int userId,int buildId) {
		logger.info("Updating vm details");
		String sql = String.format(build.updateVMStatus,schema);
		int status = database.update(sql,new Object[]{vm_exe_status,buildId,userId},new int[]{Types.INTEGER,Types.INTEGER,Types.INTEGER});
		return status;
	}
	
	
	@Override
	public int changeVMStatus(String vmId, int status) {

		String sql = "UPDATE `xenonvm`.`xevm_customervm_details` SET `is_free`=? WHERE `cust_vm_id`=?;";
		int changeVMStatus = database.update(String.format(sql), new Object[] { status, vmId },
				new int[] { Types.INTEGER, Types.INTEGER });
		return changeVMStatus;
	}
	
	@Override
	public int insertSfdcBuildExecDetails(BuildExecDetails buildExecDetails,String schema) throws SQLException {
		
		logger.info("Inserting build execution details");
		String callinsertVmDetails = String.format(build.insertSfdcBuildExecDetails, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callinsertVmDetails);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setInt(2, buildExecDetails.getBuildId());
		callableStmt.setInt(3, buildExecDetails.getVmId());
		callableStmt.setString(4, buildExecDetails.getBrowser());
		callableStmt.setInt(5, buildExecDetails.getMailGroupId());
		callableStmt.setInt(6, buildExecDetails.getStepWiseScreenshot());
		callableStmt.setInt(7, buildExecDetails.getReportBug());
		callableStmt.setInt(8, buildExecDetails.getUserId());
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		logger.info("Insert status = "+insertStatus);
		callableStmt.close();
		return insertStatus;
	}
	
	@Override
	public List<Map<String, Object>> getVMSfdcDetails(String schema,int userId,int buildId) {
		logger.info("Reading vm details");
		String sql = String.format(build.getVMSfdc,schema); 
		List<Map<String,Object>> getVMDetails = database.select(sql,new Object[]{userId,buildId},new int[]{Types.INTEGER,Types.INTEGER});
		logger.info("return list "+getVMDetails);
		return getVMDetails;
	}
	
	@Override
	public List<Map<String, Object>> getCustomerVMDetailsbyId(int customerId,int vmId) {
		logger.info("Reading vm details");
		String sql = String.format(build.getCustomerVMDetailsbyId); 
		List<Map<String,Object>> getVMDetails=new ArrayList<Map<String,Object>>(1);
		getVMDetails = database.select(sql,new Object[]{vmId ,customerId},new int[]{Types.INTEGER,Types.INTEGER});
		logger.info("return list "+getVMDetails);
		return getVMDetails;
	}
	
	@Override
	public int updateBuildExecDetails(BuildExecDetails buildExecDetails,String schema) {
		logger.info("Updating BuildExecDetails");
		String sql = String.format(build.updateBuildExecDetails,schema); 
		int status= database.update(sql,new Object[]{buildExecDetails.getExecJson(),buildExecDetails.getCompleteStatus(),buildExecDetails.getBuildId(),buildExecDetails.getUserId(),buildExecDetails.getVmId()},new int[]{Types.VARCHAR,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER});
		logger.info("return status "+status);
		return status;
	}

}

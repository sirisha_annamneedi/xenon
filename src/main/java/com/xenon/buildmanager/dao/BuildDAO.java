package com.xenon.buildmanager.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.xenon.buildmanager.domain.BuildDetails;
import com.xenon.buildmanager.domain.BuildTestCases;

public interface BuildDAO {
	
	/**
	 * This method is to insert build
	 * @param insertBuild
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int insertBuild(BuildDetails insertBuild, String schema) throws SQLException;

	/**
	 * This method is to insert automation build
	 * @param insertBuild
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int insertAutomationBuild(BuildDetails insertBuild, String schema) throws SQLException;

	/**
	 * This method is to get build details
	 * @param buildID 
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBuildDetails(int buildID, String schema);

	/**
	 * This method is to update build
	 * @param updateBuild
	 * @param schema
	 * @return
	 */
	int updateBuild(BuildDetails updateBuild, String schema);

	/**
	 * This method is to update automation build
	 * @param updateBuild
	 * @param schema
	 * @return
	 */
	int updateAutoBuild(BuildDetails updateBuild, String schema);

	/**
	 * @author shantaram.tupe
	 * 31-Oct-2019:5:17:02 PM
	 * @param buildId
	 * @param buildName
	 * @param isManual
	 * @param schema
	 * @return
	 */
	int updateBuildName(int buildId, String buildName, boolean isManual, String schema);
	
	/**
	 * @author shantaram.tupe
	 * 01-Nov-2019:11:43:08 AM
	 * @param buildId
	 * @param isManual
	 * @param schema
	 * @return
	 */
	int deleteBuildSoftly( int buildId, boolean isManual, String schema );
	
	/**
	 * This method is to get all build details
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllBuildDetails(String schema);

	/**
	 * This method is to get all test cases
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllTestCases(int buildId, String schema);

	/**
	 * This method is to drop test cases by scenario
	 * @param string
	 * @param buildId
	 * @param schema
	 * @return
	 */
	int dropTestCaseBySce(String string, int buildId, String schema);

	/**
	 * This method is to add test case by scenario ID
	 * @param testCaseArray
	 * @param buildId
	 * @param userId
	 * @param schema
	 * @return
	 */
	int addTestCaseById(String testCaseArray, int buildId, int userId, String schema);

	/**
	 * This method is to update build status
	 * @param buildStatus
	 * @param buildId
	 * @param schema
	 * @return
	 */
	int updateBuildStatus(int buildStatus, int buildId, String schema);

	/**
	 * This method is to get add remove Test Case details for manager
	 * @param schema 
	 * @return
	 */
	List<Map<String, Object>> getAddRemoveTCDetailsForManager(String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to check create build status of customer exceeds limit or not 
	 * 
	 * @param schema
	 * @param custTypeId
	 * @return
	 * @throws SQLException
	 */
	int createNewBuildStatus(String schema, int custTypeId) throws SQLException;
	
	/**
	 * This method is to create bug on execution
	 * @param tcId
	 * @param notes
	 * @param tcStatus
	 * @param buildId
	 * @param stepId
	 * @param stepStatus
	 * @param stepComment
	 * @param testCaseInsertMethod
	 * @param stepInsertMethod
	 * @param userId
	 * @param bugId
	 * @param bytes
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int createBugOnExecution(int tcId,String notes,int tcStatus,int buildId,int stepId,int stepStatus,String stepComment,
            int testCaseInsertMethod,int stepInsertMethod,int userId,int bugId,byte[] bytes,String schema) throws SQLException;
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to get build TC count by user id
	 * 
	 * @param userID
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBuildTcCount(int userID, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to get build TC execute count by build id
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTcExecStatusByBuildId(int buildId, String schema);

	
	/**
	 * 
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to get execute TC count by build and TC id
	 * 
	 * @param buildId
	 * @param tcId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTcExecStatusByBuildIdAndTcId(int buildId, int tcId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to get execute TC step status by build
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTcStepsStatusByBuildId(int buildId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to get all build details to execute 
	 * 
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllBuildDetailsToExecute(int userId, String schema);

	/**
	 * This method is to get add remove Test Case details for creator
	 * @param userID
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAddRemoveTCDetailsForCreator(int userID, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to update build status by build id
	 * 
	 * @param buildStatus
	 * @param buildId
	 * @param schema
	 * @return
	 */
	int updateBuildStatusById(String buildStatus, int buildId, String schema);

	/**
	 * This method is used to get all executed build details
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllExecutedBuildDetails(String schema);

	/**
	 * This method is used to get automation executed build details
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAutomationExecutedBuildDetails(String schema);

	/** 
	 * This method is used to get bug by execution Id
	 * @param step_exe_id
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBugByExcutionId(int step_exe_id, String schema);

	/**
	 * This method is used to insert execution bug
	 * @param stepId
	 * @param bugId
	 * @param schema
	 * @return
	 */
	int insertExecutionBug(int stepId, int bugId, String schema);

	/**
	 * This method is used to check build assigner
	 * @param buildId
	 * @param userId
	 * @param schema
	 * @return
	 */
	int checkBuildAssigner(int buildId, int userId, String schema);

	/**
	 * This method is used to check view all builds access
	 * @param userId
	 * @param schema
	 * @return
	 */
	int checkViewAllBuildsAccess(int userId, String schema);

	/**
	 * This method is used to get assignee list by build
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAssigneeListByBuild(int buildId, String schema);

	/**
	 * This method is used to clone build
	 * @param insertBuild
	 * @param schema
	 * @param sourceBuildID
	 * @return
	 * @throws SQLException
	 */
	int cloneBuild(BuildDetails insertBuild, String schema, int sourceBuildID) throws SQLException;

	/**
	 * This method is used to clone test cases
	 * @param sourceBuild
	 * @param destinationBuildid
	 * @param schema
	 * @return
	 */
	int cloneTestcases(int sourceBuild, int destinationBuildid, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is get build TC execute count by build id
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBuildTcExecCountsByBuildId(int buildId, String schema);

	/**
	 * This method is used to check assigned modules
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> checkAssignedModules(int buildId, String schema);

	/**
	 * This method is used to get modules from test case
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getModulesFromTestcase(int buildId, String schema);

	/**
	 * This method is used to get build dashboard count
	 * @param user_id
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBuildDashboardCount(int user_id, String schema);

	/**
	 * This method is used to get top build graph
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTopBuildGraph(int userId, String schema);

	/**
	 * This method is used to get test case by build
	 * @param user_id
	 * @param schema
	 * @return
	 */
	public List<Map<String, Object>> getTestCasebyBuild(int user_id, String schema);

	/**
	 * This method is used to get report data by build Id
	 * @param buildId
	 * @param userId
	 * @param schema
	 * @return
	 */
	public Map<String, Object> getReportDataByBuildId(int buildId, int userId, String schema);

	/**
	 * This method is used to get build project data
	 * @param userId
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBuildProjectData(int userId, int buildId, String schema);

	/**
	 * This method is used to get view build data
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getViewBuildData(int userId, int automationStatus ,int scriptlessStatus, int manualStartValue, int autoStartValue, int scStartValue, int pageSize, String schema);

	/**
	 * This method is used to get create build data
	 * @param schema
	 * @return
	 */
	Map<String, Object> getCreateBuildData(String schema);

	/**
	 * This method is used to get edit build data
	 * @param buildId
	 * @param type
	 * @param schema
	 * @return
	 */
	Map<String, Object> getEditBuildData(int buildId, int type, String schema);

	/**
	 * This method is used to get build dashboard
	 * @param schema
	 * @return
	 */
	Map<String, Object> getBuildDashboard(int userId, String schema);

	/**
	 * This method is used to get build automation dashboard
	 * @param schema
	 * @return
	 */
	Map<String, Object> getBuildAutomationDashboard(String schema);

	/**
	 * This method is used to get build dashboard link data
	 * @param schema
	 * @return
	 */
	Map<String, Object> getBuildDashboardLinkData(int userId, String schema);

	/**
	 * This method is used to get build dashboard's top builds
	 * @param allBuildsTestcasesCount
	 * @param allBuildsExecutedTestcasesCount
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBmDashboardTopBuilds(List<Map<String, Object>> allBuildsTestcasesCount,
			List<Map<String, Object>> allBuildsExecutedTestcasesCount, String schema);

	/**
	 * This method is used to get build dashboard's top automation bugs
	 * @param allBuildsTestcasesCount
	 * @param allBuildsExecutedTestcasesCount
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBmDashboardTopAutoBuilds(List<Map<String, Object>> allBuildsTestcasesCount,
			List<Map<String, Object>> allBuildsExecutedTestcasesCount, String schema);

	/**
	 * This method is used to get execute build list
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getExecuteBuildList(int userId, int manualStartValue, int autoStartValue, 
			int pageSize, String schema);

	/**
	 * This method is used to get automation build data
	 * @param customerId 
	 * @param userId
	 * @param buildId
	 * @param projectId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getAutomationBuildData(int customerId, int userId, int buildId, int projectId, String execType, String schema);

	/**
	 * This method is used to get build added test case count
	 * @param userId
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBuildAddedTcCount(int userId, int buildId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to drop automation test cases array on removal of TC. 
	 * 
	 * @param dropTestArray
	 * @param buildId
	 * @param schema
	 * @return
	 */
	int dropAutomationTestCase(String dropTestArray, int buildId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to add automation test case array on addition of TC
	 * 
	 * @param testCaseArray
	 * @param buildId
	 * @param userId
	 * @param schema
	 * @return
	 */
	int addAutomationTestCase(String testCaseArray, int buildId, int userId, String schema);
	
	/**
	 * This method is used to get auto module wise test case count
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAutoModuleWiseTCCount(int buildId,String schema);

	/**
	 * This method is used to build execution details
	 * @param buildId
	 * @param schema
	 * @return
	 */
	Map<String, Object> buildExecutionDetails(int buildId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to send mail on automation build execution complete
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	String sendMailOnBuildExecutionComplete(int buildId,
			String schema) throws Exception;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to send mail on start of automation build execution
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	String sendMailOnSatrtOfAutoBuildExe(int buildId, String schema)
			throws Exception;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to read build execution details to send mail
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 */
	Map<String, Object> buildExecutionDetailsMail(int buildId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to update automation build status on complete execution
	 * 
	 * @param buildId
	 * @param schema
	 * @param buildTimeStamp
	 * @return
	 */
	Map<String, Object> updateAutomationBuildStatus(int buildId, String schema,
			String buildTimeStamp);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to get build details by build time stamp and id
	 * 
	 * @param build_id
	 * @param buildStamp
	 * @return
	 */
	List<Map<String, Object>> getBuildDetailsByTimestampAndId(int build_id,
			String buildStamp);
	/**
	 * @author suresh.adling
	 * 
	 * This method is to get data for mail on completion of manual build execution
	 * This method is used to build execution details
	 * @param buildId
	 * @param schema
	 * @return
	 */
	Map<String, Object> dataForMailOnCompleteBuild(int buildId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method sends mail after manual build execution completes
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	String sendMailOnManualBuildExecutionComplete(int buildId, String schema)
			throws Exception;

	/**
	 * 
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads manual build execution details
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 */
	Map<String, Object> manualBuildExecutionDetails(int buildId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is used to insert test case summary
	 * 
	 * @param tcId
	 * @param buildId
	 * @param methodType
	 * @param testerId
	 * @param statusId
	 * @param notesVal
	 * @param completeStatus
	 * @param userName
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String, Object> insertTestcaseSummary(int tcId, int buildId,
			int methodType, int testerId, int statusId, String notesVal,
			int completeStatus, String userName, int userId, String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to insert attachments for test cases for manual build execution
	 * @param tcId
	 * @param buildId
	 * @param attachment
	 * @param title
	 * @param schema
	 * @return
	 */
	int insertAttachmentOnTcExecution(int tcId, int buildId, byte[] bytes, String title, String schema);


	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is used to get clone build data
	 * 
	 * @param schema
	 * @return
	 */
	Map<String, Object> getCloneBuildData(String schema);

	/**
	 * @author prafulla.pol
	 * This method is used to get test case added attachment by its id
	 * @param attchmentId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> gettcAttachmentById(int attchmentId, String schema);

	/**
	 * @author prafulla.pol
	 * This method is used to get recent manual builds
	 * @param schema
	 * @return
	 */
	Map<String, Object> recentManualBuilds(String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get the top recent builds
	 * @param builds
	 * @param buildTotalTestCases
	 * @param buildExecutedTestCases
	 * @return
	 */
	List<Map<String,Object>> getTopRecentBuilds(List<Map<String,Object>> builds,List<Map<String,Object>> buildTotalTestCases,
			List<Map<String,Object>> buildExecutedTestCases);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get recent automation builds
	 * @param schema
	 * @return
	 */
	Map<String, Object> recentAutomationBuilds(String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to update the build status and build learning when build execution gets completed.
	 * @param buildID
	 * @param buildName
	 * @param buildLearning
	 * @param userID
	 * @param userName
	 * @param schema
	 * @return
	 */
	Map<String, Object> updateBuildLearningAndStatus(int buildID,String buildName,String buildLearning,int userID,String userName,String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to insert clone build data
	 * 
	 * @param cloneBuildId
	 * @param buildName
	 * @param buildDescription
	 * @param buildType
	 * @param releaseId
	 * @param buildCreator
	 * @param buildStatus
	 * @param schema
	 * @return
	 */
	
	Map<String, Object> insertCloneBuild(int cloneBuildId, String buildName,
			String buildDescription, int buildType, int releaseId,
			int buildCreator, int buildStatus, String schema,String currentDate);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads builds for notification
	 * 
	 * @param allBuildsTestcasesCount1
	 * @param allBuildsExecutedTestcasesCount1
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBmDashboardTopBuildsNf(List<Map<String, Object>> allBuilds,
			List<Map<String, Object>> allBuildsTestcasesCount1,
			List<Map<String, Object>> allBuildsExecutedTestcasesCount1,
			String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads builds for notification
	 * 
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllBuildsNf(String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method retrieves build report details
	 * 
	 * @param buildId
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getBuildReportDataByBuildId(int buildId, int userId,
			String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This gives module wise build assignee 
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getModuleWiseAssignee(int buildId, String schema);
	
	/**
	 * @author navnath.damale
	 * 
	 * This gives build wise bug count
	 * 
	 * @param schema
	 * @return
	 */
	
	List<Map<String, Object>> getBuggraphdata(String schema);
	
	/**
	 * @author navnath.damale
	 * 
	 * This updates automation build state
	 * @param buildStatus
	 * @param buildId
	 * @param schema
	 * @return
	 */
	int updateAutomationBuildStatus(int buildStatus, int buildId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method retrieves build vm details
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 */
	
	Map<String, Object> getBuildVmDetails(int buildId,int userId, String schema);

	List<Map<String, Object>> getAllProjectBugCount(int userId, String schema);

	int updateScriptlessBuildStatus(int buildStatus, int buildId, String schema);

	List<Map<String, Object>> getScriptBuildAddedTcCount(int buildId, String schema);

	Map<String, Object> getSfdcExecuteBuildData(int custId, int buildId, String schema);

	List<Map<String, Object>> getScriptExecutedBuildDetails(String schema);

	List<Map<String, Object>> getManualBuildAddedTcCount(int buildId,int userId, String schema);	

	int checkManualbuildname(int relId, String buildName, String schema);
	
	int checkAutobuildname(int relId, String buildName, String schema);
	
	Map<String, Object> getBuildReportData(int automationStatus ,int scriptlessStatus, int manualStartValue, int autoStartValue, int scStartValue, int pageSize, String schema);
	
	public boolean checkBMRoleAccess(String roleAccess,String roleId,String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method uploads datasheet
	 * 
	 * @param buildTc
	 * @param schema
	 * @return
	 */
	int uploadDatasheet(BuildTestCases buildTc, String schema);

	/**@author dhananjay.deshmukh
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> validateTestCasesForBuild(Integer buildId, String schema);

	int deleteBuild(int buildId, String schema);

	List<Map<String, Object>> getPreviewDetails(int userId, int buildId, String schema);
	
	int uploadDatafile(int buildId, int userId, byte[] datasheet, String title, String schema);

	int updateDataFile(int buildId, int userId, byte[] datasheet, String title, String schema);

	List<Map<String, Object>> getlastBuildDetails(int userId,String schema);

	List<Map<String, Object>> getlastAutoBuildDetails(int userId, String schema);

	List<Map<String, Object>> getReleaseCount(String schema);
	/**
	 * To get All releases and Manual Test Set
	 * @author shantaram.tupe
	 * 25-Oct-2019:2:03:25 PM
	 * @param schema
	 * @return
	 */
	Map<String, Object> getReleasesAndBuildsManual(String schema);
	
	/**
	 * To get All releases and Automation Test Set
	 * 
	 * @author shantaram.tupe
	 * 25-Oct-2019:10:42:03 AM
	 * @param schema
	 * @return
	 */
	Map<String, Object> getReleasesAndBuildsAutomation(String schema);

	List<Map<String, Object>> getAutoBuildState(int buildId, String schema);

	List<Map<String, Object>> getTestcaseData(int tid, String schema);

	List<Map<String, Object>> getReleaseTestDataCountAutomation(int userId,String schema);
	   
    List<Map<String, Object>> getReleaseTestDataCountManual(int userId,String schema);
   
    List<Map<String, Object>> getTotalReleaseTestData(int userId,String schema);

	List<Map<String, Object>> getAutoStepExecSummary(Integer buildId, Integer testCaseId, String schema);
    
    List<Map<String, Object>> getTrashManualDetails( String schema );

	List<Map<String, Object>> getTrashAutomationDetails(String schema);

	boolean restoreAutomationBuildStatusForTrash(int buildId, int releaseID, String schema);

	boolean restoreManualBuildStatusForTrash(int buildId, int releaseID, String schema);

	int insertRedwoodTCStartTime(int parseInt, String redwoodTCId, String schemaName);

	int insertcstarttime(int build, String testcasePrefix, String schema_name) throws Exception;
	void insertRedwoodExecutionSteps(String actions,String redwood_build_id,String redwood_tc_id ,String screenshot,String screenshotTitle, int testCaseStatus,String schema);

	List<Map<String, Object>> getBugDetailsByBuildId(String userId, Integer buildId, String projectId, String schema);
	
	int getTcCountBeforeExecution(int buildID, String schema);
	int getTcCountAfterExecution(int buildID, String schema);
	int updateBuildStateBasedOnCount(int buildState, int buildId, String schema);
	int updateVMState(String schema, int buildID, int userId, int vmState);
	int changeVMStatus(String vmId, int status);

	Map<String, Object> deallocateCustomerVM(int vmId, int userId, int buildId, String schema);

	List<Map<String, Object>> getAutomationTcDetails(int buildID, String schema);

	List<Map<String, Object>> getManualTcDetails(int buildID, String schema);

	String getReleaseIdOfSelectedRelease(String releaseName, String schema);

	List<Map<String, Object>> getAllAutoBuildsByReleaseId(int releaseId, String schema);

	int getBuildIdForAutoBuild(String buildName, String schema);

	int getBuildIdForManualBuild(String buildName, String schema);

	int insertExecutionStepsArray(String testCaseStatus, String build, String testcase, String stepId,
			String stepDetails, String stepInputType, String stepValue, String stepStatus, String attachmentPath,
			String title, String bugRaise, String schema_name, int datasetId, String iterationCounts, String timestamps) throws Exception;


}

package com.xenon.buildmanager.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.xenon.buildmanager.domain.TrialBuildDetails;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import com.xenon.testmanager.dao.TestcaseDAOImpl;

/**
 * 
 * @author
 * @description DAO implementation class to perform JDBC operations on
 *              automation build
 * 
 */
public class AutomationBuildDAOImpl implements AutomationBuildDAO, XenonQuery {
	@Autowired
	TestcaseDAOImpl testCase;

	@Autowired
	DatabaseDao database;
	private static final Logger logger = LoggerFactory.getLogger(AutomationBuildDAOImpl.class);

	@Override
	public int insertBuild(TrialBuildDetails insertBuild, String schema) throws SQLException {
		logger.info("In a method Insert Trial Build ");
		String callInsertBuild = String.format(automationBuild.createNewTrialBuild, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsertBuild);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, insertBuild.getBuildName());
		callableStmt.setInt(3, insertBuild.getBuildCreatorID());
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Result : " + insertStatus);
		return insertStatus;

	}

	@Override
	public int insertAutoBuild(String buildName, int userId, String schema) throws SQLException {
		logger.info("In a method Insert Automation Build ");
		String callInsertBuild = String.format(automationBuild.createNewAutoBuild, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsertBuild);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, buildName);
		callableStmt.setInt(3, userId);
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Result : " + insertStatus);
		return insertStatus;

	}

	@Override
	public int updateTrialLogfile(String buildName, String logFilename, String schema) {
		logger.info("In a method update Trial Log File ");
		logger.info("SQL : " + automationBuild.updatetrialLogfile);
		String sql = String.format(automationBuild.updatetrialLogfile, schema);
		int flag = database.update(sql, new Object[] { logFilename, buildName },
				new int[] { Types.VARCHAR, Types.VARCHAR });
		logger.info("Result : " + flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> getAllBuildDetails(String schema) {
		logger.info("In a method Get All Build Details");
		logger.info("SQL : " + automationBuild.getAllBuildDetails);
		String sql = String.format(automationBuild.getAllBuildDetails, schema, schema);
		List<Map<String, Object>> allBuildDetails = database.select(sql);
		logger.info("Result : " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public List<Map<String, Object>> getBuildDetails(String schema, String buildName) {
		logger.info("In a method Get  Build Details by Build Name");
		logger.info("SQL : " + automationBuild.getBuildTrial);
		String sql = String.format(automationBuild.getBuildTrial, schema, schema);
		List<Map<String, Object>> allBuildDetails = database.select(sql, new Object[] { buildName },
				new int[] { Types.VARCHAR });
		logger.info("Result : " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public List<Map<String, Object>> getAutoBuildDetails(String schema, String buildName) {
		logger.info("In a method Get Automation Build Details by Build Name");
		logger.info("SQL : " + automationBuild.getAutoBuild);
		String sql = String.format(automationBuild.getAutoBuild, schema, schema);
		List<Map<String, Object>> allBuildDetails = database.select(sql, new Object[] { buildName },
				new int[] { Types.VARCHAR });
		logger.info("Result : " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public List<Map<String, Object>> getAutoBuildDetail(String schema, int buildId) {
		logger.info("In a method Get Automation Build Details by Build Id");
		logger.info("SQL : " + automationBuild.getAutomationBuild);
		String sql = String.format(automationBuild.getAutomationBuild, schema, schema, schema, schema, schema);
		List<Map<String, Object>> allBuildDetails = database.select(sql, new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Result : " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public List<Map<String, Object>> getAutomationTestCases(String schema, int buildId) {
		logger.info("In a method Get Automation Test Cases ");
		logger.info("SQL : " + automationBuild.getAutomationTest);
		String sql = String.format(automationBuild.getAutomationTest, schema, schema, schema);
		List<Map<String, Object>> autoTest = database.select(sql, new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Result : " + autoTest);
		return autoTest;
	}

	@Override
	public int updateBuildLogfile(int buildId, String logFilename, String schema) {
		logger.info("In a method Update Build Log File");
		logger.info("SQL : " + automationBuild.updateBuildLogfile);
		String sql = String.format(automationBuild.updateBuildLogfile, schema);
		int flag = database.update(sql, new Object[] { logFilename, buildId },
				new int[] { Types.VARCHAR, Types.INTEGER });
		logger.info("Result : " + flag);
		return flag;
	}

	@Override
	public int insertExecution(String buildStamp, int build_id, String schema) {
		logger.info("In a method Insert Execution Build ");
		logger.info("SQL : " + automationBuild.insertExecution);
		String sql = String.format(automationBuild.insertExecution);
		int flag = database.update(sql, new Object[] { buildStamp, schema, build_id },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER });
		logger.info("Result : " + flag);
		return flag;
	}

	@Override
	public List<String> getTestPrefixes(Integer buildId, String schema) {
		logger.info("AutomationBuildDAOImpl.getTestPrefixes()");
		logger.info("SQL : " + automationBuild.GET_TEST_PREFIXES);
		final String sql = String.format(automationBuild.GET_TEST_PREFIXES, schema, schema); 
		List<String> testPrefixes;
		try {
			testPrefixes = database.select(sql, new Integer[] {buildId}, String.class);
		} catch (Exception e) {
			testPrefixes = new ArrayList<>();
			e.printStackTrace();
			logger.error( e.getMessage() );
		}
		return testPrefixes;
	}
	
	@Override
	public int insertbuildExecDetails(String json, String vmIp,String vmPort, int vmId, int buildId, int userId, int custId,
			String custSchema) {
		logger.info("In a method Insert Execution Build ");
		logger.info("SQL : " + automationBuild.insertbuildExecDetails);
		String sql = String.format(automationBuild.insertbuildExecDetails);
		int flag = database.update(sql, new Object[] { json, vmIp, vmId,vmPort, buildId, userId, custId, custSchema },
				new int[] { Types.VARCHAR, Types.VARCHAR,Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.VARCHAR });
		logger.info("Result : " + flag);
		return flag;
	}
	
	@Override
	public int updateScriptBuildLogfile(int buildId, String logFilename, String schema) {
		logger.info("In a method Update Build Log File");
		logger.info("SQL : " + automationBuild.updateScriptBuildLogfile);
		String sql = String.format(automationBuild.updateScriptBuildLogfile, schema);
		int flag = database.update(sql, new Object[] { logFilename, buildId },
				new int[] { Types.VARCHAR, Types.INTEGER });
		logger.info("Result : " + flag);
		return flag;
	}
	
	@Override
	public int insertScriptBuildExecution(String buildStamp, int build_id, String schema) {
		logger.info("In a method Insert Execution Build ");
		logger.info("SQL : " + automationBuild.insertScriptBuildExecution);
		String sql = String.format(automationBuild.insertScriptBuildExecution);
		int flag = database.update(sql, new Object[] { buildStamp, schema, build_id },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER });
		logger.info("Result : " + flag);
		return flag;
	}
	
	@Override
	public List<Map<String, Object>> getScriptBuildDetail(String schema, int buildId) {
		logger.info("In a method Get Automation Build Details by Build Id");
		logger.info("SQL : " + automationBuild.getScriptlessBuild);
		String sql = String.format(automationBuild.getScriptlessBuild, schema, schema, schema, schema, schema,schema,schema);
		List<Map<String, Object>> allBuildDetails = database.select(sql, new Object[] { buildId,buildId},
				new int[] { Types.INTEGER,Types.INTEGER });
		logger.info("Result : " + allBuildDetails);
		return allBuildDetails;
	}
	
	@Override
	public List<Map<String, Object>> getScriptlessTestCases(String schema, int buildId) {
		logger.info("In a method Get Automation Test Cases ");
		logger.info("SQL : " + automationBuild.getScriptlessTest);
		String sql = String.format(automationBuild.getScriptlessTest, schema, schema, schema,schema);
		List<Map<String, Object>> autoTest = database.select(sql, new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Result : " + autoTest);
		return autoTest;
	}

	/**
	 * author @shailesh.khot
	 * */
	@Override
	public List<Map<String, Object>> getApiTestCases(String schema, int buildId) {
		logger.info("In a method Get Automation Test Cases ");
		logger.info("SQL : " + automationBuild.getApiTest);
		String sql = String.format(automationBuild.getApiTest, schema, schema, schema, schema, schema);
		List<Map<String, Object>> autoTest = database.select(sql, new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Result : " + autoTest);
		return autoTest;
	}


}

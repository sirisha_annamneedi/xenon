package com.xenon.buildmanager.dao;

import java.util.List;
import java.util.Map;

import org.springframework.util.MultiValueMap;

/**
 * 
 * @author prafulla.pol
 *
 */
public interface ReportDAO {
	/**
	 * @author prafulla.pol
	 * This method is used to get test case bug analysis data
	 * @param userID
	 * @param schema
	 * @return
	 */
	Map<String, Object> getTCBugAnalysisData(int userID,String schema);
	
	/**
	 * @author prafulla.pol 
	 * This method is used to get test case filter data
	 * @param userID
	 * @param release
	 * @param build
	 * @param status
	 * @param priority
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> getTCFilterData(int userID,String release,String build,String status,String priority,String severity,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get test case filterd data
	 * @param userID
	 * @param query
	 * @param queryName
	 * @param queryVariables
	 * @param schema
	 * @return
	 */
	int saveTCQuery(int userID,String query,String queryName,String queryVariables,String schema);
	
	/**
	 * @author prafulla.pol 
	 * This method is used to get test case query data
	 * @param userID
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> getTCQueryData(int userID,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to update test case query
	 * @param queryID
	 * @param query
	 * @param queryVariables
	 * @param schema
	 * @return
	 */
	int updateTCQuery(int queryID,String query,String queryVariables,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get test case query
	 * @param userID
	 * @param release
	 * @param build
	 * @param status
	 * @param priority
	 * @param schema
	 * @return
	 */
	String getTcQuery(int userID,String release,String build,String status,String priority,String severity,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get build analysis data
	 * @param userID
	 * @param schema
	 * @return
	 */
	Map<String, Object> getBugAnalysisData(int userID,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to save bug query
	 * @param userID
	 * @param query
	 * @param queryName
	 * @param queryVariables
	 * @param schema
	 * @return
	 */
	int saveBugQuery(int userID,String query,String queryName,String queryVariables,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to update bug query
	 * @param queryID
	 * @param query
	 * @param queryVariables
	 * @param schema
	 * @return
	 */
	int updateBugQuery(int queryID,String query,String queryVariables,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get bug filter data
	 * @param userID
	 * @param release
	 * @param build
	 * @param status
	 * @param priority
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> getBugFilterData(int userID,String release,String build,String status,String priority,String severity,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get automation summary data
	 * @param buildId
	 * @param userID
	 * @param schema
	 * @return
	 */
	Map<String, Object> getAutomationSummaryData(int buildId,int userID,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get bug analysis query
	 * @param userID
	 * @param release
	 * @param build
	 * @param status
	 * @param priority
	 * @param schema
	 * @return
	 */
	String getBugAnalysisQuery(int userID,String release,String build,String status,String priority,String severity,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get bug filter data for automation builds
	 * @param userID
	 * @param release
	 * @param build
	 * @param status
	 * @param priority
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> getAutomationBugFilterData(int userID,String release,String build,String status,String priority,String severity,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get automation bug analysis query
	 * @param userID
	 * @param release
	 * @param build
	 * @param status
	 * @param priority
	 * @param schema
	 * @return
	 */
	String getAutomationBugAnalysisQuery(int userID,String release,String build,String status,String priority,String severity,String schema);
	
	public List<Map<String,Object>> getAllBugs(String schema);

	Map<String, Object> getDatasetData(String testcaseid, int userId, int buildId, String schema);

	/**
	 * @author sushant.arora
	 * This method is used to get External System releases query
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllExtSysReleases(String schema);

	/**
	 * @author sushant.arora
	 * This method is used to get External System builds query
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllExtSysBuilds(String schema);

	/**
	 * @author sushant.arora
	 * This method is used to get External System test cases query
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllExtSysTestcases(String schema);
	
	/**
	 * @author sushant.arora
	 * This method is used to get External System test steps query
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllExtSysTeststeps(String schema);
	
	/**
	 * @author sushant.arora
	 * This method is used to get External System test case count
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getExtSysTCCount(String schema);

	/**
	 * @author sushant.arora
	 * This method is used to get External System Release test case count
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getReleaseTCCount(String build, String schema);

	/**
	 * @author sushant.arora
	 * This method is used to get External System Release test case count
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBuildTCCount(String buildId, String schema);

	List<Map<String, Object>> getAtomationBuilds(String schema);

	List<Map<String, Object>> getManualBuilds(String schema);
	
	
	/**
	* @author shantaram.tupe
	* 13-Nov-2019:11:35:01 AM
	* @param userID
	* @param whereClauseFilters
	* @param schema
	* @return
	*/
	List<Map<String,Object>> getBugFilterData(int userID, Map<String, String> whereClauseFilters, String schema);



	/**
	* @author shantaram.tupe
	* 13-Nov-2019:11:58:29 AM
	* @param userID
	* @param whereClauseFilters
	* @param schema
	* @return
	*/
	List<Map<String,Object>> getAutomationBugFilterData(int userID, Map<String, String> whereClauseFilters, String schema);

	List<Map<String, Object>> buildReportManual(String schema, MultiValueMap<String, String> whereClauseFilters);

	List<Map<String, Object>> buildReportAutomation(String schema, MultiValueMap<String, String> whereClauseFilters);

	Map<String, Object> getBuildReportAutomationFilteringData(String schema);

	Map<String, Object> getBuildReportManualFilteringData(String schema);

	void saveFilterValues(int userId, String inputValue, String commonData, String schema);
	
	void saveFilterValues1(int userId, String inputValue, String commonData, String schema);
	
	void saveFilterValues2(int userId, String inputValue, String commonData, String schema);

	List<Map<String, Object>> getFilteredvaluesforBugTracker(int userId, String schema);
	
	List<Map<String, Object>> getQueryforTEMTracker(int userId, String schema);
	
	List<Map<String, Object>> getQueryforTEATracker(int userId, String schema);
	
	List<Map<String, Object>> ExecutionManualQueryFilter(int userId, int projectID, String schema);
	
	List<Map<String, Object>> ExecutionAytomationQueryFilter(int userId,int projectID, String schema);

	List<Map<String, Object>> SummaryExecutionManualQueryFilter(int userId, int projectID, String schema);

	List<Map<String, Object>> SummaryExecutionAutomationQueryFilter(int userId, int projectID, String schema);



	
}
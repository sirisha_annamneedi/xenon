package com.xenon.buildmanager.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.xenon.buildmanager.domain.ReleaseDetails;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

public class ReleaseDAOImpl implements ReleaseDAO, XenonQuery {
	@Autowired
	DatabaseDao database;

	private static final Logger logger = LoggerFactory.getLogger(ReleaseDAOImpl.class);

	@Override
	public int insertRelease(ReleaseDetails insertRelease, String schema) throws SQLException {
		logger.info("Inserting new release ");
		String callInsertProject = String.format(release.createNewRelease, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsertProject);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, insertRelease.getReleaseName());
		callableStmt.setString(3, insertRelease.getReleaseDescription());
		callableStmt.setTimestamp(4, new java.sql.Timestamp(insertRelease.getStartDate().getTime()));
		callableStmt.setTimestamp(5, new java.sql.Timestamp(insertRelease.getEndDate().getTime()));
		callableStmt.setInt(6, insertRelease.getReleaseStatus());
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		logger.info("Return status ="+insertStatus);
		callableStmt.close();
		return insertStatus;

	}
	
	public String getReleaseId(String schema,String releaseName){
		String sql = String.format(release.getReleaseId, schema,releaseName);
		List<Map<String, Object>> releaseId = database.select(sql);
		String id = ""+releaseId.get(0).get("release_id");		
		return id;
	}

	@Override
	public List<Map<String, Object>> getAllReleaseDetails(String schema) {
		logger.info("Reading all release details");
		String sql = String.format(release.allReleaseDetails, schema, schema);
		List<Map<String, Object>> details = database.select(sql);
		logger.info("Return list = "+details);
		return details;
	}

	@Override
	public List<Map<String, Object>> getReleaseDetails(int releaseID, String schema) {
		logger.info("Reading release details");
		String sql = String.format(release.getReleaseDetails, schema, schema);
		List<Map<String, Object>> releaseDetails = database.select(sql, new Object[] { releaseID },
				new int[] { Types.INTEGER });
		logger.info("Return list ="+releaseDetails);
		return releaseDetails;
	}

	@Override
	public int updateRelease(ReleaseDetails updateRelease, String schema) {
		logger.info("Updating release details");
		logger.info("Query Param =   release_description={}, release_end_date={}, release_active={} WHERE release_id={}",updateRelease
				.getReleaseDescription(), /* updateRelease.getStartDate(), */
				updateRelease.getEndDate(), updateRelease.getReleaseStatus(), updateRelease.getReleaseId() );
		String sql = String.format(release.updateRelease, schema);
		int flag = database.update(sql, new Object[] { updateRelease.getReleaseName(), updateRelease
				.getReleaseDescription(), /* updateRelease.getStartDate(), */
				updateRelease.getEndDate(), updateRelease.getReleaseStatus(), updateRelease.getReleaseId() },
				new int[] { Types.VARCHAR, Types.VARCHAR, /* Types.DATE, */Types.TIMESTAMP, Types.INTEGER, Types.INTEGER });
		logger.info("Return status ="+flag);
		return flag;
	}

	@Override
	public int updateReleaseName(String releaseName, int releaseId, String schema) {
		logger.info("ReleaseDAOImpl.updateReleaseName()");
		logger.info("Query Param =   release_name={} WHERE release_id={}",releaseName, releaseId );
		String sql = String.format(release.updateReleaseName, schema);
		int flag = database.update(sql, new Object[] {releaseName, releaseId },	new int[] { Types.VARCHAR, Types.INTEGER });
		logger.info("Return status ="+flag);
		return flag;
	}
	
	@Override
	public int deleteRelease( int releaseId, String schema) {
		logger.info("Deleting release details");
		logger.info("Query Param = WHERE release_id={}", releaseId );
		String sql = String.format(release.deleteRelease, schema);
		int flag = database.update(sql, new Object[] { releaseId },	new int[] { Types.INTEGER } );
		if( flag == 1 ) {
			String deleteManualBuilds = String.format(release.deleteReleaseBuildsSoftlyManual, schema);
			database.update(deleteManualBuilds, new Object[] { releaseId },	new int[] { Types.INTEGER } );
			String deleteAutomationBuilds = String.format(release.deleteReleaseBuildsSoftlyAutomation, schema);
			database.update(deleteAutomationBuilds, new Object[] { releaseId },	new int[] { Types.INTEGER } );
		}
		logger.info("Return status ="+flag);
		return flag;
	}
	
	@Override
	public List<Map<String, Object>> getActiveReleaseNames(String schema) {
		logger.info("In method getActiveRleaseNames");
		String sql = String.format(release.activeReleaseNames, schema);
		logger.info("SQL : " + release.activeReleaseNames);
		List<Map<String, Object>> activeReleaseNames = database.select(sql);
		logger.info("SQL Result (List of active releases) : " + activeReleaseNames);
		return activeReleaseNames;
	}
	
	@Override
	public List<Map<String, Object>> getReleaseTCCount(String schema) {
		logger.info("Reading all release details");
		String sql = String.format(release.releaseTCCount, schema, schema, schema, schema, schema, schema);
		List<Map<String, Object>> details = database.select(sql);
		logger.info("Return list = "+details);
		return details;
	}

}

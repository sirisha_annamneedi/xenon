package com.xenon.buildmanager.dao;

import java.sql.Types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.buildmanager.domain.UserDetails;
import com.xenon.database.XenonQuery;

public class UserProfileDAOImpl implements UserProfileDAO, XenonQuery {

	private static final Logger logger = LoggerFactory.getLogger(UserProfileDAOImpl.class);
	
	@Autowired
	private com.xenon.database.DatabaseDao database;

	@Override
	public boolean updateUser(UserDetails user,String schema) {
		logger.info("SQL Query to update user details");
		int updateStatus = database.update(String.format(users.updateProfile,schema),
				new Object[] { user.getFirstName(), user.getLastName(),user.getLocation(),user.getAboutMe(),user.getTimezone(), user.getUserId()},
				new int[] { Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.INTEGER,Types.INTEGER});
		logger.info("Update  Status = "+updateStatus);
		if (updateStatus != 0) {
			return true;
		} else
			return false;
	}
	
	@Override
	public boolean updateProfile(UserDetails user,String schema) {
		logger.info("SQL Query to update user profile");
		int updateStatus = database.update(String.format(users.updateProfile1,schema),
				new Object[] { user.getFirstName(), user.getLastName(),user.getUpdateDate(), user.getUserId()},
				new int[] { Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.INTEGER});
		logger.info("Update  Status = "+updateStatus);
		if (updateStatus != 0) {
			return true;
		} else
			return false;
	}
	

	
	@Override
	public boolean updatePassword(UserDetails user,String schema) {
		logger.info("SQL Query to update user password");
		int updateStatus = database.update(String.format(users.updatePassword,schema),
				new Object[] { user.getPassword(), user.getUserId() },
				new int[] { Types.VARCHAR, Types.INTEGER });
		logger.info("Update  Status = "+updateStatus);
		if (updateStatus != 0) {
			return true;
		} else
			return false;
	}
	
	@Override
	public boolean updateUserWithProfilePhoto(UserDetails user,String schema) {
		logger.info("update user with profile photo");
		int updateStatus = database.update(String.format(users.updateUserProfileWithProfilePhoto,schema),
				new Object[] { user.getFirstName(), user.getLastName(),user.getUserPhotoName(),user.getLocation(),user.getAboutMe(), user.getUserId() },
				new int[] { Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR, Types.INTEGER });
		logger.info("Update  Status = "+updateStatus);
		if (updateStatus != 0) {
			return true;
		} else
			return false;
	}

	@Override
	public boolean updateIssueTracker(UserDetails user, String schema) {
		
			logger.info("SQL Query to update issue tracker");
			int updateStatus = database.update(String.format(users.updateIssueTracker,schema),
					new Object[] { user.getIssueTracker(), user.getIssueTrackerType(),user.getIssueTrackerHost(), user.getUserId()},
					new int[] { Types.INTEGER,Types.INTEGER,Types.VARCHAR,Types.INTEGER});
			logger.info("Update  Status = "+updateStatus);
			if (updateStatus != 0) {
				return true;
			} else
				return false;
	}
}

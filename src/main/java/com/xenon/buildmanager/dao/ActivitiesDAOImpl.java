package com.xenon.buildmanager.dao;

import java.io.UnsupportedEncodingException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.domain.RecentActivity;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

public class ActivitiesDAOImpl implements ActivitiesDAO,XenonQuery{
	
	private static final Logger logger = LoggerFactory.getLogger(ActivitiesDAOImpl.class);
	
	@Autowired
	DatabaseDao database;
	
	@Autowired
	UtilsService utilsService;
	
	@Override
	public List<Map<String, Object>> getTopActivities(String schema) throws UnsupportedEncodingException {
		logger.info("Reading recent activities");
		String sql = String.format(activities.getAllActivities, schema,schema,schema);
		List<Map<String,Object>> activities = database.select(sql
				);
		logger.info("Formatting activity text");
		for (int i=0;i<activities.size();i++)
		{
			
			if(Integer.parseInt(activities.get(i).get("activity").toString())!=7)
			{
				String[] activityValues=(activities.get(i).get("activity_desc").toString()).split("#");
				String activityDesc=activities.get(i).get("activity_format").toString();
				for(String str : activityValues)
				{
					activityDesc =activityDesc.toString().replaceFirst("\"","");
					activityDesc =activityDesc.toString().replaceFirst("\"","");
					activityDesc =activityDesc.toString().replaceFirst("#",str);
				}
				activities.get(i).put("activity_desc",activityDesc);
			}
			else
			{
				String[] activityFormat=(activities.get(i).get("activity_format").toString()).split("<br/>");
				String[] activityValues=(activities.get(i).get("activity_desc").toString()).split("#");
				String activityDesc="";
				
				activityFormat[0]=activityFormat[0].replaceAll("#BUG_ID", activityValues[0]);
				if(!activityValues[2].equalsIgnoreCase(activityValues[3]))
				{
					activityFormat[1]=activityFormat[1].replaceFirst("#OLD_STATUS", activityValues[2]);
					activityFormat[1]=activityFormat[1].replaceFirst("#NEW_STATUS", activityValues[3])+"<br/>";
				}
				else
				{
					activityFormat[1]="";
				}
				
				if(!activityValues[4].equalsIgnoreCase(activityValues[5]))
				{
					activityFormat[2]=activityFormat[2].replaceFirst("#OLD_ASSIGNTO", activityValues[4]);
					activityFormat[2]=activityFormat[2].replaceFirst("#NEW_ASSIGNTO", activityValues[5])+"<br/>";
				}
				else
				{
					activityFormat[2]="";
				}
				
				if(!activityValues[6].equalsIgnoreCase(activityValues[7]))
				{
					activityFormat[3]=activityFormat[3].replaceFirst("#OLD_PRIORITY", activityValues[6]);
					activityFormat[3]=activityFormat[3].replaceFirst("#NEW_PRIORITY", activityValues[7])+"<br/>";
				}
				else
				{
					activityFormat[3]="";
				}
				if(!activityValues[8].equalsIgnoreCase(activityValues[9]))
				{
					activityFormat[4]=activityFormat[4].replaceFirst("#OLD_SEVERITY", activityValues[8]);
					activityFormat[4]=activityFormat[4].replaceFirst("#NEW_SEVERITY", activityValues[9])+"<br/>";
				}
				else
				{
					activityFormat[4]="";
				}
				activityFormat[5]=activityFormat[5].replaceFirst("#COMMENT", activityValues[10])+"<br/>";
				
				//check for the previous activities 
				
				if(activityValues.length == 13){
					//only if the Category activity is present
					if(!activityValues[11].equalsIgnoreCase(activityValues[12])){
						activityFormat[6]=activityFormat[6].replaceFirst("#OLD_CATEGORY", activityValues[11]);
						activityFormat[6]=activityFormat[6].replaceFirst("#NEW_CATEGORY", activityValues[12])+"<br/>";
					}else {
						activityFormat[6]="";
					}
				}else{
					activityFormat[6]="";
				}
				
				activityDesc=activityFormat[0]+"<br/>"+activityFormat[1]+activityFormat[2]+activityFormat[3]+activityFormat[4]+activityFormat[6]+activityFormat[5];
				activities.get(i).put("activity_desc",activityDesc);
				
			}
			activities.get(i).put("user_image", utilsService.getFileData((byte[]) activities.get(i).get("user_photo")));
		}
		return activities; 
	}

	@Override
	public int inserNewActivity(RecentActivity recentActivity,String schema) {
		logger.info("Inserting new activity");
		logger.info("Query parameters : activity = {} Description = {}  Module = {}  User = {}  Activity Status = {}",
				recentActivity.getActivity(),recentActivity.getActivityDesc(),recentActivity.getModule(),recentActivity.getUser(),recentActivity.getActivityStatus());
		int flag = database.update(String.format(activities.inserNewActivity, schema),
				new Object[]{recentActivity.getActivity(),recentActivity.getActivityDesc(),recentActivity.getModule(),recentActivity.getUser(),recentActivity.getActivityStatus()},
				new int[] {Types.INTEGER,Types.VARCHAR,Types.VARCHAR,Types.INTEGER,Types.INTEGER});
		return flag;
	}
	
	@Override
	public RecentActivity setRecentActivitiesValues(int activity,String activityDesc,String module,int user,int activityStatus)
	{
		logger.info("Setting values to domain");
		RecentActivity recentActivity=new RecentActivity();
		recentActivity.setActivity(activity);
		recentActivity.setActivityDesc(activityDesc);
		recentActivity.setModule(module);
		recentActivity.setUser(user);
		recentActivity.setActivityStatus(activityStatus);
		logger.info("Returning domain");
		return recentActivity;
		
	}

}

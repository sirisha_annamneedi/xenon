package com.xenon.buildmanager.dao;

import java.util.List;
import java.util.Map;

import com.xenon.buildmanager.domain.EnvironmentVariableDetails;

public interface EnvVariableDetailsDao {
	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to insert variable details
	 * 
	 * @param environmentVariableDetails
	 * @param schema
	 */
	int insertEnvVariableDetails(EnvironmentVariableDetails environmentVariableDetails, String schema);

	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to get variables details
	 * 
	 * @param envId
	 * @param schema
	 */
	List<Map<String, Object>> getEnvVariableDetails(String envId, String schema);

	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to delete variables details
	 * 
	 * @param variableId
	 * @param schema
	 */
	int deleteVariable(String schema, String variableId);

	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to update variables details
	 * 
	 * @param updateVariableName
	 * @param updateConfigValue
	 * @param updateVariableValue
	 * @param updateVariableId
	 * @param schema
	 */
	int updateVariableDetails(String updateVariableName, String updateConfigValue, String updateVariableValue,
			String updateVariableId, String schema);
}

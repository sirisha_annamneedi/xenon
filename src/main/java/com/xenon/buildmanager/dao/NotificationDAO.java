package com.xenon.buildmanager.dao;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import com.xenon.buildmanager.domain.UserNotification;

/**
 * This interface handles recent activities related operations
 * 
 * @author bhagyashri.ajmera
 *
 */
public interface NotificationDAO {
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to retrieve top five recent activities from database
	 * 
	 * @param schema
	 * @return
	 * @throws UnsupportedEncodingException
	 */

	Map<String, Object> getNotifications(int userId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method inserts notification records
	 * 
	 * @param userNotificationList
	 * @param schema
	 */
	void insertNotificationRecords(List<UserNotification> userNotificationList,
			String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method used to set values for user notification domain
	 * 
	 * @param activity
	 * @param notificationDesc
	 * @param notificationUrl
	 * @param userId
	 * @param createdBy
	 * @param readStatus
	 * @param notificationStatus
	 * @return
	 */
	UserNotification setUserNotificationValues(int activity,
			String notificationDesc, String notificationUrl, int userId, int createdBy,
			int readStatus, int notificationStatus);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to insert notification record to database
	 * 
	 * @param userNotification
	 * @param schema
	 * @return
	 */
	int inserNotificationRecord(UserNotification userNotification, String schema);

	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is used to mark notification as read
	 * 
	 * @param notificationId
	 * @param schema
	 * @return
	 */
	int markNotificationAsRead(int notificationId,int userId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is used to mark notification as read flag count
	 * 
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String, Object> updateNotificationReadCountFlag(int userId,
			String schema);
	
}

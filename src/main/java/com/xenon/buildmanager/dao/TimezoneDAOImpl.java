package com.xenon.buildmanager.dao;

import java.io.UnsupportedEncodingException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.buildmanager.domain.TimezoneDetails;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

public class TimezoneDAOImpl implements TimezoneDAO,XenonQuery{
	
	private static final Logger logger = LoggerFactory.getLogger(TimezoneDAOImpl.class);
	
	@Autowired
	DatabaseDao database;
	
	@Override
	public List<Map<String, Object>> getTimezones(String schema) throws UnsupportedEncodingException {
		logger.info("Reading all timezones");
		String sql = String.format(timezone.getAllTimezones, schema);
		List<Map<String,Object>> timezones = database.select(sql
				);
		logger.info("Return List = "+timezones);
		return timezones; 
	}

	
	@Override
	public TimezoneDetails setTimezoneValues(String timezoneName,int timezoneStaus)
	{
		logger.info("Setting time zone values");
		TimezoneDetails timezoneDetails=new TimezoneDetails();
		timezoneDetails.setTimezoneName(timezoneName);
		timezoneDetails.setTimezoneStaus(timezoneStaus);
		logger.info("Returning domain");
		return timezoneDetails;
	}

	@Override
	public List<Map<String, Object>> getTimezonesDetailsById(int id,String schema) {
		logger.info("Reading timezone details by id");
		logger.info("Query param id = {}",id);
		String sql = String.format(timezone.getTimezonesDetailsById, schema);
		List<Map<String,Object>> timezoneDetails = database.select(sql,new Object[]{id},new int[]{Types.INTEGER}
				);
		logger.info("Return list "+timezoneDetails);
		return timezoneDetails; 
	}
}

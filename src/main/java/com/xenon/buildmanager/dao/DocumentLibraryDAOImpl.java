package com.xenon.buildmanager.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.buildmanager.domain.DocumentLibraryDetails;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

public class DocumentLibraryDAOImpl implements DocumentLibraryDAO, XenonQuery {

	private static final Logger logger = LoggerFactory.getLogger(DocumentLibraryDAOImpl.class);
	
	@Autowired
	DatabaseDao database;

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int uploadDocument(DocumentLibraryDetails docDetails, String schema) {
		logger.info("Uploading document");
		logger.info("Query param doc_title ={}, doc_type={}, doc_data={},upload_time={},uploaded_by={},project_id={} ",docDetails.getDocTitle(),docDetails.getDocType(),docDetails.getDocData(),docDetails.getUploadedBy(),docDetails.getProjectId());
		int flag = database.update(
				String.format(documentLibrary.uploadDocument, schema),
				new Object[] { docDetails.getDocTitle(),docDetails.getDocType(),docDetails.getDocData(),docDetails.getUploadedBy(),docDetails.getProjectId() }, new int[] { Types.VARCHAR,
						Types.VARCHAR, Types.BLOB,Types.INTEGER,Types.INTEGER });
		logger.info("Return flag = "+flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> getDocumentsByProjectId(int projectId,String schema) {
		logger.info("Reading documents by project id");
		logger.info("Query param = "+projectId);
		String sql = String.format(documentLibrary.getDocumentDetails, schema,schema);
		List<Map<String, Object>> listFiles = database.select(sql,new Object[]{projectId},new int[]{Types.INTEGER});
		logger.info("List files "+listFiles);
		return listFiles;
	}
	
	
	@Override
	public List<Map<String, Object>> getDocumentById(int doclibraryId, String schema) {
		logger.info("Reading document by id");
		logger.info("Query param doclibraryId = "+doclibraryId);
		List<Map<String, Object>> documentById = database.select(String.format(documentLibrary.getDocumentById,schema),
				new Object[] { doclibraryId }, new int[] { Types.INTEGER });
		logger.info("Return List ="+documentById);
		return documentById;
	}
	
	@Override
	public Map<String, Object> getDocumentLibraryData(int projectId, int userId, String schema) {
		logger.info("Reading document library data by project id ");
		logger.info("Query param = "+projectId);
		SqlParameter projectIdTemp = new SqlParameter("projectId", Types.VARCHAR);
		SqlParameter tempUserId = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = {projectIdTemp, tempUserId};
		
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getDocumentLibraryData").declareParameters(paramArray).withCatalogName(schema);
 
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(projectId, userId);
		
		return simpleJdbcCallResult;
	}
	
	
}

package com.xenon.buildmanager.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.MultiValueMap;

import com.xenon.controller.ReportSummaryController;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

public class ReportDAOImpl implements ReportDAO,XenonQuery{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	DatabaseDao database;
	private static final Logger logger = LoggerFactory.getLogger(ReportSummaryController.class);
	
	@Override
	public Map<String, Object> getTCBugAnalysisData(int userID,String schema) {
		logger.info("Calling Stored procedure to get list of user assigned projects , modules and active status and priorities with user previously stored queries");
		SqlParameter tempUserID = new SqlParameter("userID", Types.INTEGER);
		SqlParameter[] paramArray = {tempUserID};
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("tcBugAnalysis").declareParameters(paramArray).withCatalogName(schema);
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userID);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public List<Map<String, Object>> getTCFilterData(int userID,String project,String module,String status,String priority,String severity,String schema) {
		logger.info("SQL Query to get filtered data for Test Case Analysis");
		report.setTcWiseFilteredData(userID,project,module, status, priority,severity);
		String query = report.getTcWiseFilteredData();
		//String sql=String.format(query,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema);
		String sql=String.format(query,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema);
		logger.info("SQL : " + sql);
		List<Map<String,Object>> details = database.select(sql);
		logger.info("SQL Result " + details);
		return details;
	}

	@Override
	public int saveTCQuery(int userID, String query,String queryName,String queryVariables, String schema) {
		logger.info("Saving SQL Query for Test Case Analysis");
		String sql = String.format(report.saveTCQuery,schema);
		logger.info("SQL : " + sql);
		int status = database.update(sql,
				new Object[]{userID,query,queryName,queryVariables},
				new int[]{Types.INTEGER,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR});
		logger.info("SQL Result " + status);
		return status;
	}

	@Override
	public List<Map<String, Object>> getTCQueryData(int userID, String schema) {
		String sql=String.format(report.getTCQuery,schema);
		logger.info("SQL Query to get TC Query Data "+ sql);
		List<Map<String,Object>> details = database.select(sql,
				new Object[]{userID}, new int[]{Types.INTEGER});
		logger.info("Query Result  " + details);
		return details;
	}

	@Override
	public int updateTCQuery(int queryID, String query,String queryVariables, String schema) {
		logger.info("Updating SQL Query for Test Case Analysis");
		String sql=String.format(report.updateTCQuery,schema);
		logger.info("SQL : " + sql);
		int status = database.update(sql, 
				new Object[]{query,queryVariables,queryID}, 
				new int[]{Types.VARCHAR,Types.VARCHAR,Types.INTEGER});
		logger.info("SQL Result " + status);
		return status;
	}

	@Override
	public String getTcQuery(int userID,String project, String module, String status,
			String priority,String severity, String schema) {
		logger.info("SQL Query to get filtered data for Test Case Analysis");
		report.setTcWiseFilteredData(userID,project,module, status, priority,severity);
		String query = report.getTcWiseFilteredData();
		String formatedQuery = String.format(query,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema);
		logger.info("SQL : " + formatedQuery);
		return formatedQuery;
	}

	@Override
	public Map<String, Object> getBugAnalysisData(int userID, String schema) {
		logger.info("Calling Stored procedure to get list of releases and user assigned builds , and active status and priorities with user previously stored queries");
		SqlParameter tempUserID = new SqlParameter("userID", Types.INTEGER);
		SqlParameter[] paramArray = {tempUserID};
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("bugAnalysis").declareParameters(paramArray).withCatalogName(schema);
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userID);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public int saveBugQuery(int userID, String query, String queryName,
			String queryVariables, String schema) {
		String sql = String.format(report.saveBugQuery,schema);
		logger.info("SQL Query to save Bug Query "+ sql);
		int status = database.update(sql,
				new Object[]{userID,query,queryName,queryVariables},
				new int[]{Types.INTEGER,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR});
		logger.info("SQL Result " + status);
		return status;
	}

	@Override
	public int updateBugQuery(int queryID, String query, String queryVariables,
			String schema) {
		String sql=String.format(report.updateBugQuery,schema);
		logger.info("SQL Query to update Bug Query "+ sql);
		int status = database.update(sql, 
				new Object[]{query,queryVariables,queryID}, 
				new int[]{Types.VARCHAR,Types.VARCHAR,Types.INTEGER});
		logger.info("SQL Result " + status);
		return status;
	}

	@Override
	public List<Map<String, Object>> getBugFilterData(int userID,
			String release, String build, String status, String priority,String severity,
			String schema) {
		logger.info("SQL Query to get filtered data for Bug Analysis");
		report.setBugFilteredData(userID,build, status, priority,severity);
		String query = report.getBugFilteredData();
		String sql=String.format(query,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema);
		//String sql=String.format(query,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema);
		logger.info("SQL : " + sql);
		List<Map<String,Object>> details = database.select(sql);
		logger.info("SQL Result " + details);
		System.out.println("Details:"+details);
		return details;
	}

	@Override
	public Map<String, Object> getAutomationSummaryData(int buildId,int userID,String schema) {
		logger.info("Calling Stored procedure to get automation build details for generating reports");
		SqlParameter tempBuildId = new SqlParameter("buildId", Types.INTEGER);
		SqlParameter tempUserId = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = { tempBuildId, tempUserId };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("automationSummary")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(buildId, userID);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public String getBugAnalysisQuery(int userID, String release, String build,
			String status, String priority,String severity, String schema) {
		logger.info("Creating SQL Query for Bug Analysis of manual build");
		report.setBugFilteredData(userID,build, status, priority,severity);
		String query = report.getBugFilteredData();
		String formatedQuery=String.format(query,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema);
		logger.info("SQL : " + formatedQuery);
		return formatedQuery;
	}

	@Override
	public List<Map<String, Object>> getAutomationBugFilterData(int userID,
			String release, String build, String status, String priority,String severity,
			String schema) {
		logger.info("SQL Query to get filtered data for Bug Analysis of automation builds");
		report.setAutomationBugFilteredData(userID,build, status, priority,severity);
		String query = report.getAutomationBugFilteredData();
		String sql=String.format(query,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema);
		logger.info("SQL : " + sql);
		List<Map<String,Object>> details = database.select(sql);
		logger.info("SQL Result " + details);
		return details;
	}

	@Override
	public String getAutomationBugAnalysisQuery(int userID, String release,
			String build, String status, String priority, String severity,String schema) {
		logger.info("Creating SQL Query for Bug Analysis of automation build");
		report.setAutomationBugFilteredData(userID,build, status, priority,severity);
		String query = report.getAutomationBugFilteredData();
		String formatedQuery=String.format(query,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema);
		logger.info("SQL : " + formatedQuery);
		return formatedQuery;
	}
	
	@Override
	public List<Map<String,Object>> getAllBugs(String schema){
		String sql=String.format(report.getAllbugs,schema);
		logger.info("SQL Query to get bug data "+ sql);
		List<Map<String,Object>> details = database.select(sql,
				new Object[]{}, new int[]{});
		logger.info("Query Result  " + details);
		
		return details;
	}
	@Override
	public Map<String, Object> getDatasetData(String testcaseid,int userId,int buildID,String schema) {
		SqlParameter userID=new SqlParameter("userId", Types.INTEGER);
		SqlParameter testcaseId=new SqlParameter("testcaseid", Types.VARCHAR);
		SqlParameter buildId=new SqlParameter("buildID", Types.INTEGER);
		SqlParameter [] paramArray={testcaseId,userID,buildId};
		SimpleJdbcCall simpleJdbcCall=new SimpleJdbcCall(jdbcTemplate).withProcedureName("dataset").declareParameters(paramArray).withCatalogName(schema);
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId,testcaseid,buildID);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public List<Map<String,Object>> getAllExtSysReleases(String schema){
		String sql = String.format(report.getAllExtSysReleases, schema);
		logger.info("SQL Query to get ExtSysReleases "+ sql);
		List<Map<String,Object>> details = database.select(sql,
				new Object[]{}, new int[]{});
		logger.info("Query Result  " + details);
		return details;
	}
	
	@Override
	public List<Map<String,Object>> getAllExtSysBuilds(String schema){
		String sql = String.format(report.getAllExtSysBuilds, schema);
		logger.info("SQL Query to get ExtSysBuilds "+ sql);
		List<Map<String,Object>> details = database.select(sql,
				new Object[]{}, new int[]{});
		logger.info("Query Result  " + details);
		return details;
	} 
	
	@Override
	public List<Map<String,Object>> getAllExtSysTestcases(String schema){
		String sql = String.format(report.getAllExtSysTestcases, schema, schema);
		logger.info("SQL Query to get ExtSysTestcases "+ sql);
		List<Map<String,Object>> details = database.select(sql,
				new Object[]{}, new int[]{});
		logger.info("Query Result  " + details);
		return details;
	}
	
	@Override
	public List<Map<String,Object>> getAllExtSysTeststeps(String schema){
		String sql = String.format(report.getAllExtSysTeststeps, schema, schema);
		logger.info("SQL Query to get ExtSysTeststeps "+ sql);
		List<Map<String,Object>> details = database.select(sql,
				new Object[]{}, new int[]{});
		logger.info("Query Result  " + details);
		return details;
	}
	
	@Override
	public List<Map<String,Object>> getExtSysTCCount(String schema){
		String sql = String.format(report.getExtSysTCCount, schema, schema, schema);
		logger.info("SQL Query to get bug data "+ sql);
		List<Map<String,Object>> details = database.select(sql,
				new Object[]{}, new int[]{});
		logger.info("Query Result  " + details);
		return details;
	}
	
	@Override
	public List<Map<String, Object>> getReleaseTCCount(String releaseId, String schema) {
		logger.info("SQL Query to get filtered data for Bug Analysis of automation builds");
		report.setReleaseTCCount(releaseId);
		String query = report.getReleaseTCCount();
		String sql = String.format(query, schema, schema, schema);
		logger.info("SQL : " + sql);
		List<Map<String,Object>> details = database.select(sql);
		logger.info("SQL Result " + details);
		return details;
	}
	
	@Override
	public List<Map<String, Object>> getBuildTCCount(String buildId, String schema) {
		logger.info("SQL Query to get filtered data for Bug Analysis of automation builds");
		report.setBuildTCCount(buildId);
		String query = report.getBuildTCCount();
		String sql = String.format(query, schema, schema);
		logger.info("SQL : " + sql);
		List<Map<String,Object>> details = database.select(sql);
		logger.info("SQL Result " + details);
		return details;
	}

	@Override
	public List<Map<String, Object>> getAtomationBuilds(String schema) {
		String sql = String.format(report.getAllAutomationBuilds, schema);
		List<Map<String,Object>> autoBuildData = database.select(sql);
		logger.info("Query Result  " + autoBuildData);
		return autoBuildData;
	}

	@Override
	public List<Map<String, Object>> getManualBuilds(String schema) {
		String sql = String.format(report.getAllManualBuilds, schema);
		List<Map<String,Object>> manBuildData = database.select(sql);
		logger.info("Query Result  " + manBuildData);
		return manBuildData;
	}
	
	@Override
	public List<Map<String, Object>> getBugFilterData(int userID, Map<String, String> whereClauseFilters,
	String schema) {
	logger.info("SQL Query to get filtered data for Bug Analysis");
	report.setBugFilteredData(userID, whereClauseFilters);
	String query = report.getBugFilteredData();
	String sql=String.format(query,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema);
	//String sql=String.format(query,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema);
	logger.info("SQL : " + sql);
	List<Map<String,Object>> details = database.select(sql);
	logger.info("SQL Result " + details);
	return details;
	}



	@Override
	public List<Map<String, Object>> getAutomationBugFilterData(int userID, Map<String, String> whereClauseFilters,
	String schema) {
	logger.info("SQL Query to get filtered data for Bug Analysis of automation builds");
	report.setAutomationBugFilteredData(userID, whereClauseFilters);
	String query = report.getAutomationBugFilteredData();
	String sql=String.format(query,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema,schema);
	logger.info("SQL : " + sql);
	List<Map<String,Object>> details = database.select(sql);
	logger.info("SQL Result " + details);
	return details;
	}

	@Override
	public Map<String, Object> getBuildReportManualFilteringData(String schema) {
		logger.info("BuildDAOImpl.getBuildReportFilteringData()");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBuildReportFilteringData").withCatalogName(schema);
		Map<String, Object> buildReportFilteringData = simpleJdbcCall.execute();
		logger.info("Stored Procedure : Result " + buildReportFilteringData);
		return buildReportFilteringData;
	}
	
	@Override
	public List<Map<String, Object>> buildReportManual(String schema, MultiValueMap<String, String> whereClauseFilters) {
		logger.info("ReportDAOImpl.buildReportManual()");
		String sqlSelectJoin = "SELECT xtc.testcase_id, xtc.testcase_name, xtc.test_prefix, xp.project_id, xp.project_name, xr.release_id, xr.release_name, "
				+ "xb.build_id, xb.build_name, xm.module_id, xm.module_name, xs.scenario_id, xs.scenario_name, "
				+ "xud.user_id AS tester_id, CONCAT(xud.first_name, ' ', xud.last_name) AS tester_name, xes.exst_id, xes.description "
				+ "FROM "+schema+".xe_testcase_execution_summary xtes "
				+ "JOIN "+schema+".xetm_testcase xtc ON xtc.testcase_id = xtes.tc_id "
				+ "JOIN "+schema+".xe_project xp ON xp.project_id = xtes.project_id "
				+ "JOIN "+schema+".xe_build xb ON xb.build_id = xtes.build_id "
				+ "JOIN "+schema+".xe_release xr ON xr.release_id = xb.release_id "
				+ "JOIN "+schema+".xe_module xm ON xm.module_id = xtes.mudule_id "
				+ "JOIN "+schema+".xetm_scenario xs ON xs.scenario_id = xtes.scenario_id "
				+ "JOIN "+schema+".xe_user_details xud ON xud.user_id = xtes.tester_id "
				+ "JOIN "+schema+".xetm_exec_status xes ON xes.exst_id = xtes.status_id ";
		StringBuffer whereClauseSB = new StringBuffer(sqlSelectJoin);
		int mapSize = whereClauseFilters.size();
		int filterCount = 1;
		if( mapSize > 0 )
			whereClauseSB.append(" where ");
		for(Entry<String, List<String>> entry : whereClauseFilters.entrySet() ) {
			switch (entry.getKey()) {
			case "Application[]":
				whereClauseSB.append(" xtes.project_id in ( "+ StringUtils.join(entry.getValue(), ",") +" ) " );
				break;

			case "Release[]":
				whereClauseSB.append(" xr.release_id in ( "+ StringUtils.join(entry.getValue(), ",") +" ) " );
				break;
			case "TestSet[]":
				whereClauseSB.append(" xtes.build_id in ( "+ StringUtils.join(entry.getValue(), ",") +" ) " );
				break;
			case "Module[]":
				whereClauseSB.append(" xtes.mudule_id in ( "+ StringUtils.join(entry.getValue(), ",") +" ) " );
				break;
			case "Scenario[]":
				whereClauseSB.append(" xtes.scenario_id in ( "+ StringUtils.join(entry.getValue(), ",") +" ) " );
				break;
			case "Status[]":
				whereClauseSB.append(" xtes.status_id in ( "+ StringUtils.join(entry.getValue(), ",") +" ) " );
				break;
			case "Tester[]":
				whereClauseSB.append(" xtes.tester_id in ( "+ StringUtils.join(entry.getValue(), ",")+" ) " );
				break;

			default:
				break;
			}
			if( filterCount < mapSize )
				whereClauseSB.append(" and ");
			++filterCount;
		}
		List<Map<String,Object>> details = database.select( whereClauseSB.toString() );
		return details;
	}
	@Override
	public Map<String, Object> getBuildReportAutomationFilteringData(String schema) {
		logger.info("BuildDAOImpl.getBuildReportFilteringData()");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBuildReportAutomationFilteringData").withCatalogName(schema);
		Map<String, Object> buildReportFilteringData = simpleJdbcCall.execute();
		logger.info("Stored Procedure : Result " + buildReportFilteringData);
		return buildReportFilteringData;
	}
	@Override
	public List<Map<String, Object>> buildReportAutomation(String schema, MultiValueMap<String, String> whereClauseFilters) {
		logger.info("ReportDAOImpl.buildReportManual()");
		String sqlSelectJoin = "SELECT xtc.testcase_id, xtc.testcase_name, xtc.test_prefix, xab.build_id, xab.build_name, xp.project_id, xp.project_name, "
				+ "xr.release_id, xr.release_name, "
				+ "xm.module_id, xm.module_name, xs.scenario_id, xs.scenario_name, xes.exst_id, xes.description "
				+ "FROM "+schema+".xebm_automation_execution_summary xaes "
				+ "JOIN "+schema+".xetm_testcase xtc ON xtc.testcase_id = xaes.testcase "
				+ "JOIN "+schema+".xe_project xp ON xp.project_id = xaes.project "
				+ "JOIN "+schema+".xe_automation_build xab ON xab.build_id = xaes.build "
				+ "JOIN "+schema+".xe_release xr ON xr.release_id = xab.release_id "
				+ "JOIN "+schema+".xe_module xm ON xm.module_id = xaes.module "
				+ "JOIN "+schema+".xetm_scenario xs ON xs.scenario_id = xaes.scenario "
				+ "JOIN "+schema+".xetm_exec_status xes ON xes.exst_id = xaes.testcase_status ";
		StringBuffer whereClauseSB = new StringBuffer(sqlSelectJoin);
		int mapSize = whereClauseFilters.size();
		int filterCount = 1;
		if( mapSize > 0 )
			whereClauseSB.append(" where ");
		for(Entry<String, List<String>> entry : whereClauseFilters.entrySet() ) {
			switch (entry.getKey()) {
			case "Application[]":
				whereClauseSB.append(" xaes.project in ( "+ StringUtils.join(entry.getValue(), ",") +" ) " );
				break;

			case "Release[]":
				whereClauseSB.append(" xr.release_id in ( "+ StringUtils.join(entry.getValue(), ",") +" ) " );
				break;
			case "TestSet[]":
				whereClauseSB.append(" xaes.build in ( "+ StringUtils.join(entry.getValue(), ",") +" ) " );
				break;
			case "Module[]":
				whereClauseSB.append(" xaes.module in ( "+ StringUtils.join(entry.getValue(), ",") +" ) " );
				break;
			case "Scenario[]":
				whereClauseSB.append(" xaes.scenario in ( "+ StringUtils.join(entry.getValue(), ",") +" ) " );
				break;
			case "Status[]":
				whereClauseSB.append(" xaes.testcase_status in ( "+ StringUtils.join(entry.getValue(), ",") +" ) " );
				break;
			/*case "Tester[]":
				whereClauseSB.append(" xaes.tester_id in ( "+ StringUtils.join(entry.getValue(), ",")+" ) " );
				break;*/

			default:
				break;
			}
			if( filterCount < mapSize )
				whereClauseSB.append(" and ");
			++filterCount;
		}
		List<Map<String,Object>> details = database.select( whereClauseSB.toString() );
		return details;
	}
	

	@Override
	public void saveFilterValues(int userId, String inputValue, String commonData, String schema) {
		// TODO Auto-generated method stub
		database.update(String.format(report.QueryforBugTracker, schema),
				new Object[] { userId, inputValue, commonData},
				new int[] {Types.INTEGER, Types.VARCHAR,Types.VARCHAR});

	}
	
	@Override
	public void saveFilterValues1(int userId, String inputValue, String commonData, String schema) {
		// TODO Auto-generated method stub
		database.update(String.format(report.QueryforTEMTracker, schema),
				new Object[] { userId, inputValue, commonData},
				new int[] {Types.INTEGER, Types.VARCHAR,Types.VARCHAR});

	}
	
	
	public void saveFilterValues2(int userId, String inputValue, String commonData, String schema) {
		// TODO Auto-generated method stub
		database.update(String.format(report.QueryforTEATracker, schema),
				new Object[] { userId, inputValue, commonData},
				new int[] {Types.INTEGER, Types.VARCHAR,Types.VARCHAR});

	}

	public  List<Map<String, Object>> getFilteredvaluesforBugTracker(int userId,String schema) {
		/*return database.select(String.format(report.getQueryforBugTracker, schema),
				new Object[] { userId, inputValue, commonData},
				new int[] {Types.INTEGER, Types.VARCHAR,Types.VARCHAR});*/
		String sql=String.format(report.getQueryforBugTracker, schema);
		List<Map<String,Object>> data=database.select(sql);
		return data;
	}
	
	public  List<Map<String, Object>> getQueryforTEMTracker(int userId,String schema) {
		/*return database.select(String.format(report.getQueryforBugTracker, schema),
				new Object[] { userId, inputValue, commonData},
				new int[] {Types.INTEGER, Types.VARCHAR,Types.VARCHAR});*/
		String sql=String.format(report.getQueryforTEMTracker, schema);
		List<Map<String,Object>> data=database.select(sql);
		return data;
	}

	public  List<Map<String, Object>> getQueryforTEATracker(int userId,String schema) {
		/*return database.select(String.format(report.getQueryforBugTracker, schema),
				new Object[] { userId, inputValue, commonData},
				new int[] {Types.INTEGER, Types.VARCHAR,Types.VARCHAR});*/
		String sql=String.format(report.getQueryforTEATracker, schema);
		List<Map<String,Object>> data=database.select(sql);
		return data;
	}
	
	public  List<Map<String, Object>> ExecutionManualQueryFilter(int userId,int projectID,String schema)
	 {
		
		/*String sql=String.format(report.ExecutionManualQueryFilter, schema, schema, schema, schema, schema, schema, schema,schema,schema,
				new Object[] { projectID},
				new int[] {Types.INTEGER});*/
		report.setManualCustomizeQuery(projectID);
		String query = report.getManualCustomizeQuery();
		String sql = String.format(query, schema, schema, schema, schema, schema, schema, schema,schema,schema);
		List<Map<String,Object>> data=database.select(sql);
		return data;
	}
	
	public List<Map<String, Object>> SummaryExecutionManualQueryFilter(int userId,int projectID,String schema)
	 {

		String query = "SELECT SUM(case when xt.status_id = '1' then 1 else 0 end) as Pass,SUM(case when xt.status_id = '2' then 1 else 0 end) as Fail,SUM(case when xt.status_id = '3' then 1 else 0 end) as Skip,SUM(case when xt.status_id = '4' then 1 else 0 end) as Block,SUM(case when xt.status_id = '5' then 1 else 0 end) as notrun, xm.module_name,xr.release_name,xb.build_name "
			    +"FROM "+schema+".xe_testcase_execution_summary xt, "+schema+".xe_user_module xum, "+schema+".xe_module xm,"+schema+".xe_build xb,"+schema+".xe_release xr "
						+"WHERE xum.module_id=xm.module_id and xr.release_id=xb.release_id and xt.build_id=xb.build_id and xum.module_id=xt.mudule_id and xum.project_id="+projectID+"  and xum.user_id="+userId+" and xm.module_active IN(1,2) GROUP BY xm.module_name";
	    List<Map<String,Object>> data=database.select(query);
		return data;
	}
	
	public List<Map<String, Object>> SummaryExecutionAutomationQueryFilter(int userId,int projectID,String schema)
	 {
		String query = "SELECT SUM(case when xt.testcase_status = '1' then 1 else 0 end) as Pass,SUM(case when xt.testcase_status = '2' then 1 else 0 end) as Fail,SUM(case when xt.testcase_status = '3' then 1 else 0 end) as Skip,SUM(case when xt.testcase_status = '4' then 1 else 0 end) as Block,SUM(case when xt.testcase_status = '5' then 1 else 0 end) as notrun, xm.module_name,xr.release_name,xb.build_name "
         +"FROM "+schema+".xebm_automation_execution_summary xt, "+schema+".xe_user_module xum, "+schema+".xe_module xm,"+schema+".xe_release xr,"+schema+".xe_automation_build xb "
           +"WHERE xum.module_id=xm.module_id and xr.release_id=xb.release_id and xt.build=xb.build_id and xum.module_id=xt.module and xum.project_id="+projectID+" and xum.user_id="+userId+" and xm.module_active IN(1,2) GROUP BY xb.build_name";
	    List<Map<String,Object>> data=database.select(query);
		return data;
	}
	
	public  List<Map<String, Object>> ExecutionAytomationQueryFilter(int userId,int projectID,String schema) {
		report.setAutomationCustomizeQuery(projectID);
		String query = report.getAutomationCustomizeQuery();
		String sql = String.format(query, schema, schema, schema, schema, schema, schema, schema,schema,schema);
		List<Map<String,Object>> data=database.select(sql);
		return data;
	}


	
}

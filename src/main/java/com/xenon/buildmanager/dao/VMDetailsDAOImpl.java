package com.xenon.buildmanager.dao;

import java.io.UnsupportedEncodingException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.buildmanager.domain.VMDetails;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

public class VMDetailsDAOImpl implements VMDetailsDAO,XenonQuery{
	
	private static final Logger logger = LoggerFactory.getLogger(VMDetailsDAOImpl.class);
	
	@Autowired
	DatabaseDao database;
	
	@Override
	public List<Map<String, Object>> getAllVmDetails(String customerId) throws UnsupportedEncodingException {
		String sql = String.format(vmDetail.getAllVmDetails);
		List<Map<String,Object>> vmList = database.select(sql,new Object[]{Integer.parseInt(customerId)},new int[]{Types.INTEGER});
		return vmList; 
	}

	@Override
	public int insertVMDetails(VMDetails vmDetails,String customerId) throws SQLException {
		logger.info("Inserting VM details");
		String callinsertVmDetails = String.format(vmDetail.insertVMDetails);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callinsertVmDetails);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, vmDetails.getHostname());
		callableStmt.setString(3, vmDetails.getIpAddress());
		callableStmt.setInt(4, vmDetails.getPortNumber());
		callableStmt.setString(5, vmDetails.getProjectLocation());
		callableStmt.setInt(6, vmDetails.getIeStatus());
		callableStmt.setInt(7, vmDetails.getChromeStatus());
		callableStmt.setInt(8, vmDetails.getFirefoxStatus());
		callableStmt.setInt(9, vmDetails.getSafariStatus());
		callableStmt.setInt(10, vmDetails.getConcurrentExecStatus());
		callableStmt.setInt(11, vmDetails.getConcurrentExecs());
		callableStmt.setInt(12, vmDetails.getStatus());
		callableStmt.setString(13, vmDetails.getUsername());
		callableStmt.setString(14, vmDetails.getPassword());
		callableStmt.setInt(15, Integer.parseInt(customerId));
		callableStmt.setInt(16, vmDetails.getRepoStatus());
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		logger.info("Insert status");
		callableStmt.close();
		return insertStatus;
	}
	
	@Override
	public VMDetails setVMDetailsValues(int isFree,String hostname,String ipAddress,int portNumber,String remoteUsername,String remotePassword,String projectLocation,int concurrentExecStatus,int concurrentExecs,int ieStatus,int chromeStatus,int firefoxStatus,int safariStatus, int status,int repoStatus)
	{
		logger.info("Setting VM details domain");
		VMDetails vmDetails=new VMDetails();
		vmDetails.setHostname(hostname);
		vmDetails.setIpAddress(ipAddress);
		vmDetails.setProjectLocation(projectLocation);
		vmDetails.setIeStatus(ieStatus);
		vmDetails.setChromeStatus(chromeStatus);
		vmDetails.setFirefoxStatus(firefoxStatus);
		vmDetails.setSafariStatus(safariStatus);
		vmDetails.setStatus(status);
		vmDetails.setUsername(remoteUsername);
		vmDetails.setPassword(remotePassword);
		vmDetails.setPortNumber(portNumber);
		vmDetails.setConcurrentExecStatus(concurrentExecStatus);
		vmDetails.setConcurrentExecs(concurrentExecs);
		vmDetails.setIsFree(isFree);
		vmDetails.setRepoStatus(repoStatus);
		
		logger.info("Returning VM details domain");
		return vmDetails;
	}

	
	@Override
	public List<Map<String, Object>> getVmDetailsById(int id,String customerId) throws UnsupportedEncodingException {
		logger.info("Reading VM details by Id");
		logger.info("Query Param  id ={} "+id);
		String sql = String.format(vmDetail.getVmDetailsById);
		List<Map<String,Object>> vmList = database.select(sql,new Object[]{id,Integer.parseInt(customerId)},new int[]{Types.INTEGER,Types.INTEGER}
				);
		logger.info("Return list ="+vmList);
		return vmList; 
	}
	
	@Override
	public int updateVmDetailsById(VMDetails vmDetails,String customerId) {
		logger.info("Updating VM details by id");
		logger.info("Query param  `hostname`={},`ip_address`={},`port`={},`project_location`={},`ie_status`={},`chrome_status`={},`firefox_status`={},`safari_status`={},`concurrent_exec_status`={},`concurrent_execs`={},`status`={} where  xe_vm_id={}",vmDetails.getHostname(),vmDetails.getIpAddress(),vmDetails.getPortNumber(),vmDetails.getProjectLocation(),vmDetails.getIeStatus(),vmDetails.getChromeStatus(),vmDetails.getFirefoxStatus(),vmDetails.getSafariStatus(),vmDetails.getConcurrentExecStatus(),vmDetails.getConcurrentExecs(),vmDetails.getStatus(),vmDetails.getXeVmId());
		String sql = String.format(vmDetail.updateVmDetailsById);
		int flag = database
				.update(sql,
						new Object[] { vmDetails.getHostname(),vmDetails.getIpAddress(),vmDetails.getPortNumber(),vmDetails.getUsername(),vmDetails.getPassword(),vmDetails.getProjectLocation(),vmDetails.getIeStatus(),vmDetails.getChromeStatus(),vmDetails.getFirefoxStatus(),vmDetails.getSafariStatus(),vmDetails.getConcurrentExecStatus(),vmDetails.getConcurrentExecs(),
								vmDetails.getStatus(),vmDetails.getIsFree(),vmDetails.getRepoStatus(),vmDetails.getXeVmId(),Integer.parseInt(customerId) },
						new int[] { Types.VARCHAR,Types.VARCHAR, Types.INTEGER,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR, Types.INTEGER, Types.INTEGER,Types.INTEGER, Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER ,Types.INTEGER});
		logger.info("Return value = "+flag);
		return flag;
	}
	
	@Override
	public int insertCloudVMDetails(VMDetails vmDetails) throws SQLException {
		logger.info("Inserting VM details");
		String callinsertVmDetails = String.format(vmDetail.insertCloudVMDetails);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callinsertVmDetails);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, vmDetails.getHostname());
		callableStmt.setString(3, vmDetails.getIpAddress());
		callableStmt.setInt(4, vmDetails.getPortNumber());
		callableStmt.setString(5, vmDetails.getProjectLocation());
		callableStmt.setInt(6, vmDetails.getIeStatus());
		callableStmt.setInt(7, vmDetails.getChromeStatus());
		callableStmt.setInt(8, vmDetails.getFirefoxStatus());
		callableStmt.setInt(9, vmDetails.getSafariStatus());
		callableStmt.setInt(10, vmDetails.getConcurrentExecStatus());
		callableStmt.setInt(11, vmDetails.getConcurrentExecs());
		callableStmt.setInt(12, vmDetails.getStatus());
		callableStmt.setString(13, vmDetails.getUsername());
		callableStmt.setString(14, vmDetails.getPassword());
		callableStmt.setInt(15, vmDetails.getRepoStatus());
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		logger.info("Insert status");
		callableStmt.close();
		return insertStatus;
	}
	
	@Override
	public List<Map<String, Object>> getAllCloudVmDetails() throws UnsupportedEncodingException {
		String sql = String.format(vmDetail.getAllCloudVmDetails);
		List<Map<String,Object>> vmList = database.select(sql);
		return vmList; 
	}
	
	
	@Override
	public int updateCloudVmDetailsById(VMDetails vmDetails) {
		logger.info("Updating VM details by id");
		logger.info("Query param  `hostname`={},`ip_address`={},`port`={},`project_location`={},`ie_status`={},`chrome_status`={},`firefox_status`={},`safari_status`={},`concurrent_exec_status`={},`concurrent_execs`={},`status`={} where  xe_vm_id={}",vmDetails.getHostname(),vmDetails.getIpAddress(),vmDetails.getPortNumber(),vmDetails.getProjectLocation(),vmDetails.getIeStatus(),vmDetails.getChromeStatus(),vmDetails.getFirefoxStatus(),vmDetails.getSafariStatus(),vmDetails.getConcurrentExecStatus(),vmDetails.getConcurrentExecs(),vmDetails.getStatus(),vmDetails.getXeVmId());
		String sql = String.format(vmDetail.updateCloudVmDetailsById);
		int flag = database
				.update(sql,
						new Object[] { vmDetails.getHostname(),vmDetails.getIpAddress(),vmDetails.getPortNumber(),vmDetails.getUsername(),vmDetails.getPassword(),vmDetails.getProjectLocation(),vmDetails.getIeStatus(),vmDetails.getChromeStatus(),vmDetails.getFirefoxStatus(),vmDetails.getSafariStatus(),vmDetails.getConcurrentExecStatus(),vmDetails.getConcurrentExecs(),
								vmDetails.getStatus(),vmDetails.getIsFree(),vmDetails.getRepoStatus(),vmDetails.getXeVmId() },
						new int[] { Types.VARCHAR,Types.VARCHAR, Types.INTEGER,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR, Types.INTEGER, Types.INTEGER,Types.INTEGER, Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER});
		logger.info("Return value = "+flag);
		return flag;
	}
	
	@Override
	public List<Map<String, Object>> getCloudVmDetailsById(int id) throws UnsupportedEncodingException {
		logger.info("Reading VM details by Id");
		logger.info("Query Param  id ={} "+id);
		String sql = String.format(vmDetail.getCloudVmDetailsById);
		List<Map<String,Object>> vmList = database.select(sql,new Object[]{id},new int[]{Types.INTEGER}
				);
		logger.info("Return list ="+vmList);
		return vmList; 
	}
	
	@Override
	public int updateVMStatus(String vmId) {
		
		String sql = "UPDATE `xenonvm`.`xevm_cloudevm_details` SET `is_free`=? WHERE `cloude_vm_id`=?;";
		int updateVMStatus = database.update(String.format(sql),
				new Object[] { 1,vmId},
				new int[] { Types.INTEGER,Types.INTEGER});
		
		return updateVMStatus;
	}
}

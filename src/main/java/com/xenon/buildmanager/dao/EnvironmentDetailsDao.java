package com.xenon.buildmanager.dao;

import java.util.List;
import java.util.Map;

import com.xenon.buildmanager.domain.EnvironmentDetails;

public interface EnvironmentDetailsDao {
	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to insert Environment details
	 * 
	 * @param environmentDetails
	 * @param schema
	 */
	int insertEnvDetails(EnvironmentDetails environmentDetails,String schema);
	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to get Environment details
	 * 
	 * @param schema
	 */
	List<Map<String, Object>> getEnvDetails(String schema);
	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to get Environment details
	 * 
	 * @param schema
	 * @param envId
	 */
	List<Map<String, Object>> getEnvDetailsById(String envId, String schema);
	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to get Environment details
	 * 
	 * @param schema
	 * @param envId
	 * @param envName
	 * @param configParameterName
	 * @param envDescription
	 * @param envId
	 */
	int updateEnvDetails(String envId, String envName, String configParameterName,int envStatus,String envDescription, String schema);
	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to delete Environment details
	 * 
	 * @param schema
	 * @param envId
	 */
	int deleteEnvDetails(String envId, String schema);
}

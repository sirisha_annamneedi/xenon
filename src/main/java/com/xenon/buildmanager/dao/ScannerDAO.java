package com.xenon.buildmanager.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.xenon.buildmanager.domain.PageScanDetails;
import com.xenon.buildmanager.domain.RepositoryDetails;


/**
 * 
 * This interface handles all scanner related operations
 * 
 * @author bhagyashri.ajmera
 *
 */
public interface ScannerDAO {
	
	Map<String, Object> insertPageScanDetails(PageScanDetails PageScanDetails, String schema) throws SQLException;

	PageScanDetails setPageScanDetailsValues(int env, int pagenameId, int funcId, int userId);

	Map<String, Object> getSfdcGetScanPageData(String schema);

	Map<String, Object> getSfdcLocatorsPageData(int scannerId, String schema);

	int getScannerIdByDetails(PageScanDetails pageScanDetails, String schema);

	void insertSfdcRepositoryRecords(List<RepositoryDetails> repositoryDetailsList, String schema);

	List<Map<String, Object>> getAllLocatorType(String schema);

	Map<String, Object> getSfdcAddStepPageData(int scannerId, int testcaseId, int envId, String schema);
	
	int getScannerId(int sfdcenv, int sfdcpage, int sfdcfunc, int userID, String schema);
	
	Map<String, Object> getSfdcCreateTCData(String schema);
}

package com.xenon.buildmanager.dao;


import com.xenon.buildmanager.domain.UserDetails;

public interface UserProfileDAO {
		
		/**
		 * @author prafulla.pol
		 * @param user
		 * @param schema
		 * @return
		 */
		boolean updateUser(UserDetails user,String schema);
		
		/**
		 * @author prafulla.pol
		 * @param user
		 * @param schema
		 * @return
		 */
		boolean updatePassword(UserDetails user,String schema);
		
		/**
		 * @author bhagyashri.ajmera
		 * 
		 * This method is to update user with profile photo
		 * 
		 * @param user
		 * @param schema
		 * @return
		 */
		boolean updateUserWithProfilePhoto(UserDetails user, String schema);
		
		/**
		 * @author 
		 * @param user
		 * @param schema
		 * @return
		 */
		boolean updateProfile(UserDetails user, String schema);

		boolean updateIssueTracker(UserDetails user, String schema);
		
}

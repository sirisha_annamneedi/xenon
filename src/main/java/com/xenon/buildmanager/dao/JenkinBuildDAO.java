package com.xenon.buildmanager.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.xenon.buildmanager.domain.JenkinBuildDetails;



/**
 * @author bhagyashri.ajmera
 *
 * This interface is used to handle build related operations
 * 
 */
public interface JenkinBuildDAO {

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is used to insert build details in xenon
	 * 
	 * @param buildDetailsList
	 * @param url
	 * @param user
	 * @param pass
	 * @param schema
	 * @return 
	 * @throws SQLException
	 */
	int insertBuild(JenkinBuildDetails buildDetails,String schema);

	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is used to set build details
	 * 
	 * @param buildDetailsJson
	 * @param jobId 
	 * @return
	 * @throws JSONException
	 */
	JenkinBuildDetails setBuildDetailsFromBuildJSON(JSONObject buildDetailsJson, int jobId) throws JSONException;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is used to update jenkins build details
	 * 
	 * @param jobName
	 * @param apiToken
	 * @param buildNo
	 * @return
	 */
	Map<String, Object> updateJenkinsBuildDetails(String jobName, String apiToken, String buildNo);
	
	/**
	 * @author pooja.mugade
	 * 
	 * This method is used to get jenkins build details
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBuildDetails(int jobId, String schema);
	
	
	/**
	 * @author pooja.mugade
	 * 
	 * This method is used to get jenkins trigger build details
	 * @param schema
	 * @param jobId
	 * @return
	 */
	List<Map<String,Object>> getBuildTriggerDetails(String schema,int jobId);


	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method updates jenkins build
	 * 
	 * @param buildDetails
	 * @param schema
	 * @return
	 */
	int updateBuild(JenkinBuildDetails buildDetails, String schema);


	List<Map<String, Object>> getBuildDetailsByBuildNumber(int jobId, String schema, int buildId);
	
	

}

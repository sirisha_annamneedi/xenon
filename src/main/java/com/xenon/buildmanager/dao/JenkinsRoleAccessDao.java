package com.xenon.buildmanager.dao;

public interface JenkinsRoleAccessDao {
	boolean checkJenkinJobAccess(String roleAccess,String userId,String jobId, String schema);
}

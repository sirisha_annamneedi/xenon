package com.xenon.buildmanager.dao;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import com.xenon.buildmanager.domain.TimezoneDetails;


/**
 * @author bhagyashri.ajmera
 * 
 * This interface handles all time zone related operations 
 *
 */
public interface TimezoneDAO {

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives all time zone details
	 * 
	 * @param schema
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	List<Map<String, Object>> getTimezones(String schema)
			throws UnsupportedEncodingException;
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method sets time zone values
	 * 
	 * @param timezoneName
	 * @param timezoneStaus
	 * @return
	 */
	TimezoneDetails setTimezoneValues(String timezoneName, int timezoneStaus);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives time zone details by id
	 * 
	 * @param id
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getTimezonesDetailsById(int id, String schema);
	

}

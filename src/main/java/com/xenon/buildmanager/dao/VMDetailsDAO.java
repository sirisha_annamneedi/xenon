package com.xenon.buildmanager.dao;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.xenon.buildmanager.domain.VMDetails;

/**
 * 
 * This interface handles all VM related operations
 * 
 * @author bhagyashri.ajmera
 *
 */
public interface VMDetailsDAO {

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads all vm details
	 * 
	 * @param schema
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	List<Map<String, Object>> getAllVmDetails(String schema) throws UnsupportedEncodingException;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method inserts VM details to database
	 * 
	 * @param vmDetails
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int insertVMDetails(VMDetails vmDetails, String schema) throws SQLException;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method gives VM details by VM id
	 * 
	 * @param id
	 * @param schema
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	List<Map<String, Object>> getVmDetailsById(int id, String schema) throws UnsupportedEncodingException;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method updates VM details
	 * 
	 * @param vmDetails
	 * @param schema
	 * @return
	 */
	int updateVmDetailsById(VMDetails vmDetails, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method sets the VM details domain
	 * 
	 * @param hostname
	 * @param ipAddress
	 * @param portNumber
	 * @param projectLocation
	 * @param projectLocation2
	 * @param remotePassword
	 * @param concurrentExecStatus
	 * @param concurrentExecs
	 * @param ieStatus
	 * @param chromeStatus
	 * @param firefoxStatus
	 * @param safariStatus
	 * @param status
	 * @param  repoStatus
	 * @return
	 */
	public VMDetails setVMDetailsValues(int isFree, String hostname, String ipAddress, int portNumber,
			String remoteUsername, String remotePassword, String projectLocation, int concurrentExecStatus,
			int concurrentExecs, int ieStatus, int chromeStatus, int firefoxStatus, int safariStatus, int status,int repoStatus);
	/**
	 * @author bhagyashri.ajmera
	 * 
	 *  This method inserts Cloud VM details to database
	 * 
	 * @param vmDetails
	 * @return
	 * @throws SQLException
	 */
	int insertCloudVMDetails(VMDetails vmDetails) throws SQLException;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method retrieves all cloud VM details
	 * 
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	List<Map<String, Object>> getAllCloudVmDetails() throws UnsupportedEncodingException;

	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method updates cloud VM details
	 * 
	 * @param vmDetails
	 * @return
	 */
	int updateCloudVmDetailsById(VMDetails vmDetails);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method retrieves cloud VM details by Id
	 * 
	 * @param id
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	List<Map<String, Object>> getCloudVmDetailsById(int id) throws UnsupportedEncodingException;

	int updateVMStatus(String vmId);

}

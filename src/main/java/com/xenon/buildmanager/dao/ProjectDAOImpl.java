package com.xenon.buildmanager.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.api.common.MailService;
import com.xenon.buildmanager.domain.ProjectDetails;
import com.xenon.buildmanager.domain.UserProjectDetails;
import com.xenon.common.dao.MailDAO;
import com.xenon.controller.XenonController;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

/**
 * 
 * @author prafulla.pol
 *
 */
public class ProjectDAOImpl implements ProjectDAO, XenonQuery {

	@Resource(name = "mailProperties")
	private Properties mailProperties;

	@Autowired
	DatabaseDao database;

	@Autowired
	JdbcTemplate jdbctemplate;

	@Autowired
	MailDAO mailDAO;

	@Autowired
	UserDAO userDao;

	@Autowired
	BuildDAO buildDAO;
	
	@Autowired
	MailService mailService;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LoggerFactory.getLogger(XenonController.class);

	@Override
	public int insertProject(ProjectDetails projectDetails, String schema) throws SQLException {
		logger.info("Inserting new project to database ");
		String callInsertProject = String.format(project.createNewProject, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsertProject);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, projectDetails.getProjectName());
		callableStmt.setString(3, null);
		callableStmt.setString(4, projectDetails.getProjectDescription());
		callableStmt.setInt(5, projectDetails.getProjectStatus());
		callableStmt.setString(6, projectDetails.getTcPrefix());
		callableStmt.setString(7, projectDetails.getBtPrefix());
		callableStmt.setString(8, projectDetails.getcurrentDate());
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Insert status");
		return insertStatus;

	}
	
	@Override
	public int insertJiraProject(ProjectDetails projectDetails, String schema) throws SQLException {
		logger.info("Inserting new project to database ");
		String callInsertProject = String.format(project.createNewProject, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsertProject);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, projectDetails.getProjectName());
		callableStmt.setString(3, projectDetails.getJiraProjectName());
		callableStmt.setString(4, projectDetails.getProjectDescription());
		callableStmt.setInt(5, projectDetails.getProjectStatus());
		callableStmt.setString(6, projectDetails.getTcPrefix());
		callableStmt.setString(7, projectDetails.getBtPrefix());
		callableStmt.setString(8, projectDetails.getcurrentDate());
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Insert status");
		return insertStatus;

	}

	@Override
	public List<Map<String, Object>> getAllProjects(String schema) {
		String sql = String.format(project.getAllProjectDetails, schema, schema);
		List<Map<String, Object>> allProjectDetails = database.select(sql);
		logger.info("Query to get All Project Details" + sql);
		logger.info("Result 'All Project Details' " + allProjectDetails);
		return allProjectDetails;
	}

	@Override
	public List<Map<String, Object>> getSingleProject(int projectID, String schema) {
		String sql = String.format(project.getSingleProjectDetails, schema);
		logger.info("Query to get selected project details " + sql + "values(projectID,schema) (" + projectID + " , "
				+ schema + ")");
		List<Map<String, Object>> singleProjectDetails = database.select(sql, new Object[] { projectID },
				new int[] { Types.INTEGER });
		logger.info("Query Result " + singleProjectDetails);
		return singleProjectDetails;
	}

	@Override
	public int updateProject(ProjectDetails updateProject, String schema) {
		String sql = String.format(project.updateSingleProject, schema);
		logger.info("Query to update project " + sql);
		int flag = database
				.update(sql,
						new Object[] { updateProject.getProjectDescription(), updateProject.getProjectStatus(),
								updateProject.getProjectId() },
						new int[] { Types.VARCHAR, Types.INTEGER, Types.INTEGER });
		logger.info("Update  Status = "+flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> getAllProjectsDetails(int userId, String schema) {
		logger.info("Query to get all  project details");
		List<Map<String, Object>> allProjectDetails = database.select(
				String.format(project.getAllProjects, schema, schema), new Object[] { userId },
				new int[] { Types.INTEGER });
		logger.info("Query Result " + allProjectDetails);
		return allProjectDetails;
	}

	@Override
	public List<Map<String, Object>> getProjectAssignedUsers(int projectID, String schema) {
		String sql = String.format(project.projectAssignedUsers, schema, schema);
		logger.info("Query to get Project Assigned Users "+sql);
		List<Map<String, Object>> usersList = database.select(sql, new Object[] { projectID },
				new int[] { Types.INTEGER });
		logger.info("Query Result " + usersList);
		return usersList;
	}

	@Override
	public List<Map<String, Object>> getProjectNotAssignedUsers(int projectId, String schema) {
		String sql = String.format(project.projectNotAssignedUsers, schema, schema);
		logger.info("Query to get Project Un-Assigned Users "+sql);
		List<Map<String, Object>> usersList = database.select(sql, new Object[] { projectId },
				new int[] { Types.INTEGER });
		logger.info("Query Result " + usersList);
		return usersList;
	}

	@Override
	public int deleteUnassignedProjectRecords(int projectID, String schema) {
		String sql = String.format(project.deleteUnassignedProjectRecord, schema);
		logger.info("Query to delete Unassigned Project Records "+sql);
		int flag = database.update(sql, new Object[] { projectID }, new int[] { Types.INTEGER });
		logger.info("Query Result " + flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> selectUserDetail(String schema) {
		String sql = String.format(project.getUser, schema);
		logger.info("Query to delete Unassigned Project Records "+sql);
		List<Map<String, Object>> userdetails = database.select(sql);
		logger.info("Query Result " + userdetails);
		return userdetails;
	}

	@Override
	public List<Map<String, Object>> selectUserDetail(int buildId, String schema) {
		String sql = String.format(project.getUserDeatil, schema, schema, schema);
		logger.info("Query to select User Detail "+sql);
		List<Map<String, Object>> userdetails = database.select(sql, new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Query Result " + userdetails);
		return userdetails;
	}

	@Override
	public List<Map<String, Object>> selectUserDetailByUser(int buildId, int userId, String schema) {
		String sql = String.format(project.getUserDeatilByUser, schema, schema, schema);
		logger.info("Query to select User Detail By User "+sql);
		List<Map<String, Object>> userdetails = database.select(sql, new Object[] { buildId, userId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result " + userdetails);
		return userdetails;
	}

	@Override
	public List<Map<String, Object>> selectModuleDetail(int buildId, int userId, String schema) {
		String sql = String.format(project.getModuleDetail, schema, schema, schema, schema, schema,schema);
		logger.info("Query to get Module Detail "+sql);
		List<Map<String, Object>> moduleDetail = database.select(sql, new Object[] { buildId, userId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result " + moduleDetail);
		return moduleDetail;
	}

	@Override
	public List<Map<String, Object>> selectProjectDetailsByBuildId(int buildId, int userId, String schema) {
		logger.info("selecting project details by build id");
		String sql = String.format(project.getProjectDetail, schema, schema, schema, schema, schema, schema, schema);
		List<Map<String, Object>> projectDetails = database.select(sql, new Object[] { buildId, userId, userId },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER });
		logger.info("Adding project pass, fail, skip and block count with total and executed counts");
		for (int i = 0; i < projectDetails.size(); i++) {
			List<Map<String, Object>> prjTcDetails = prjWiseTotalTcCount(
					Integer.parseInt(projectDetails.get(i).get("project_id").toString()), buildId, schema);
			List<Map<String, Object>> prjExecTcDetails = prjWiseExecTcCount(
					Integer.parseInt(projectDetails.get(i).get("project_id").toString()), buildId, schema);
			projectDetails.get(i).put("TcCount", prjTcDetails.get(0).get("TotalTcCount"));
			projectDetails.get(i).put("ExecutedTcCount", prjExecTcDetails.get(0).get("ExecutedTcCount"));
			if (Integer.parseInt(prjExecTcDetails.get(0).get("ExecutedTcCount").toString()) == 0) {
				projectDetails.get(i).put("PassCount", 0);
				projectDetails.get(i).put("FailCount", 0);
				projectDetails.get(i).put("SkipCount", 0);
				projectDetails.get(i).put("BlockCount", 0);
			} else {
				projectDetails.get(i).put("PassCount", prjExecTcDetails.get(0).get("PassCount"));
				projectDetails.get(i).put("FailCount", prjExecTcDetails.get(0).get("FailCount"));
				projectDetails.get(i).put("SkipCount", prjExecTcDetails.get(0).get("SkipCount"));
				projectDetails.get(i).put("BlockCount", prjExecTcDetails.get(0).get("BlockCount"));
			}

		}
		logger.info("Return list = "+projectDetails);
		return projectDetails;
	}

	@Override
	public List<Map<String, Object>> selectProjectsByBuildId(int buildId, int userId, String schema) {
		logger.info("selecting projects by build id ");
		String sql = String.format(project.getProjectsFromBuild, schema, schema, schema, schema, schema, schema);
		List<Map<String, Object>> projectDetails = database.select(sql, new Object[] { buildId, userId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Return list = "+projectDetails);
		return projectDetails;
	}

	@Override
	public int insertAssignedProjectRecords(int projectID, int userID, String schema) {
		String sql = String.format(project.insertAssignedProjectRecords, schema);
		logger.info("SQL Query to insert Assigned Project Records "+sql);
		int flag = database.update(sql, new Object[] { 0, projectID, userID },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> getProjectAssignedUser(int userId, String schema) {
		String sql = String.format(project.getAssignedUser, schema, schema);
		logger.info("SQL Query to get Project Assigned User "+sql);
		List<Map<String, Object>> assignedUser = database.select(sql, new Object[] { userId },
				new int[] { Types.INTEGER });
		logger.info("Query Result  " + assignedUser);
		return assignedUser;
	}

	@Override
	public List<Map<String, Object>> getcurrentAssignedProject(int userId, String schema) {
		String sql = String.format(project.getCurrentProject, schema, schema, schema);
		logger.info("SQL Query to get current Assigned Project "+sql);
		List<Map<String, Object>> currentProject = database.select(sql, new Object[] { userId, userId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + currentProject);
		return currentProject;
	}

	@Override
	public int updateCurrentProject(int userId, int projectId, String schema) {
		String sql = String.format(project.updateCurrentProject, schema);
		logger.info("SQL Query to update Current Project "+sql);
		int flag = database.update(sql, new Object[] { projectId, userId }, new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public int assignBuild(int buildId, int projectId, int moduleId, int userId, String schema) {
		String sql = String.format(project.assignBuild, schema);
		logger.info("SQL Query to assign Build "+sql);
		int flag = database.update(sql, new Object[] { buildId, projectId, moduleId, userId },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public int removeBuildAssignee(int buildId, String schema) {
		String sql = String.format(project.removeAssignedBuild, schema);
		logger.info("SQL Query to remove assign Build "+sql);
		int flag = database.update(sql, new Object[] { buildId }, new int[] { Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public int insertCurrentProject(int userId, int projectId, String schema) {
		List<Map<String, Object>> currentProject = getcurrentAssignedProject(userId, schema);
		
		int flag;
		if (currentProject.size() == 0) {
			String sql = String.format(project.insertCurentProject, schema);
			logger.info("SQL Query to insert Current Project "+sql);
			flag = database.update(sql, new Object[] { projectId, userId }, new int[] { Types.INTEGER, Types.INTEGER });
		} else {
			flag = updateCurrentProject(userId, projectId, schema);
		}
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public int createNewProjectStatus(String schema, int custTypeId) throws SQLException {
		logger.info("Checking cutomer's create project limit exceeds or not");
		String callInsertProject = String.format(project.createNewProjectStatus, schema);
		String log = project.createNewProjectStatus + "values(schema,custTypeId) (" + schema + " , " + custTypeId + ")";
		logger.info("Log message at createNewProjectStatus. Sql " + log);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsertProject);
		logger.info("Query Result " + callableStmt);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setInt(2, custTypeId);
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		return insertStatus;

	}

	@Override
	public List<Map<String, Object>> getProjectDetailsById(int projectID, String schema) {
		logger.info("SQL Query to get Project Details By Id ");
		List<Map<String, Object>> projectDetails = database.select(String.format(project.getProjectDetailsById, schema),
				new Object[] { projectID }, new int[] { Types.INTEGER });
		logger.info("Query Result  " +projectDetails );
		return projectDetails;
	}

	@Override
	public String sendMailOnAssignProject(List<Map<String, Object>> emailDetails,
			int projectId, String appName, int userId, String userEmailId, String schema)
					throws Exception {
			logger.info("sending mail on assign project");
			logger.info("Reading send mail data");
			
			ArrayList<String> recepients = new ArrayList<String>();
			recepients.add(userEmailId);
			
			String mailSubject = mailProperties.getProperty("ASSIGN_PROJECT_SUBJECT", "New user created");
			String mailBodyText = mailProperties.getProperty("ASSIGN_PROJECT_BODY_TEXT", "None");

			String from = emailDetails.get(0).get("smtp_user_id").toString();
			
			String message = mailService.postMail(emailDetails, recepients, mailSubject,
					String.format(mailBodyText, appName), from);
			
			return message;
	}

	@Override
	public List<Map<String, Object>> prjWiseExecTcCount(int projectId, int buildId, String schema) {
		logger.info("read project wise TC count");
		String sql = String.format(project.prjWiseExecTcCount, schema);
		List<Map<String, Object>> prjDetails = database.select(sql, new Object[] { projectId, buildId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Return list = "+prjDetails);
		return prjDetails;

	}

	@Override
	public List<Map<String, Object>> prjWiseTotalTcCount(int projectId, int buildId, String schema) {
		logger.info("read project wise total TC count");
		String sql = String.format(project.prjWiseTotalTcCount, schema);
		List<Map<String, Object>> prjDetails = database.select(sql, new Object[] { projectId, buildId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Return list = "+prjDetails);
		return prjDetails;

	}

	@Override
	public List<Map<String, Object>> buildWiseExecTcCount(int buildId, String schema) {
		logger.info("read build wise executed TC count ");
		String sql = String.format(project.buildWiseExecTcCount, schema);
		List<Map<String, Object>> prjDetails = database.select(sql, new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Return list = "+prjDetails);
		return prjDetails;

	}

	@Override
	public List<Map<String, Object>> buildWiseTotalTcCount(int buildId, String schema) {
		logger.info("read build wise total TC count");
		String sql = String.format(project.buildWiseTotalTcCount, schema);
		List<Map<String, Object>> prjDetails = database.select(sql, new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Return list = "+prjDetails);
		return prjDetails;
	}

	@Override
	public int checkProjectSubset(int userId, int buildCreator, String schema) {
		logger.info("SQL Query to check Project Subset ");
		List<Map<String, Object>> userAssignedProjects = getProjectAssignedUser(userId, schema);
		List<Map<String, Object>> buildCreatorProjects = getProjectAssignedUser(buildCreator, schema);
		int flag = 0, userProject = 0, creatorProject = 0;
		for (int i = 0; i < userAssignedProjects.size(); i++) {
			userProject = Integer.parseInt(userAssignedProjects.get(i).get("project_id").toString());
			for (int j = 0; j < buildCreatorProjects.size(); j++) {
				creatorProject = Integer.parseInt(buildCreatorProjects.get(j).get("project_id").toString());
				if (userProject == creatorProject) {
					flag = 1;
					break;
				}
			}
		}
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public int removeUnassignedCurrentProject(int projectID, String schema) {
		logger.info("SQL Query to remove Unassigned Current Project ");
		int flag = database.update(String.format(project.removeUnassignedCurrentProject, schema, schema),
				new Object[] { projectID, projectID }, new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> selectProjectDetailsByBuildId(List<Map<String, Object>> projectDetails,
			List<Map<String, Object>> prjTcDetails, List<Map<String, Object>> prjExecTcDetails) {
		logger.info("select project details by build id");
		for (int i = 0; i < projectDetails.size(); i++) {
			logger.info("Reading pass, fail, skip and block with total and executed counts");
			int projectId = Integer.parseInt(projectDetails.get(i).get("project_id").toString());

			for (int j = 0; j < prjTcDetails.size(); j++) {
				if (projectId == Integer.parseInt(prjTcDetails.get(j).get("project_id").toString())) {
					projectDetails.get(i).put("TcCount", prjTcDetails.get(j).get("TotalTcCount"));
					projectDetails.get(i).put("ExecutedTcCount", 0);
					projectDetails.get(i).put("PassCount", 0);
					projectDetails.get(i).put("FailCount", 0);
					projectDetails.get(i).put("SkipCount", 0);
					projectDetails.get(i).put("BlockCount", 0);
					projectDetails.get(i).put("NotRunCount", 0);
					break;
				}
			}

			for (int j = 0; j < prjExecTcDetails.size(); j++) {
				if (projectId == Integer.parseInt(prjExecTcDetails.get(j).get("project_id").toString())) {
					if (Integer.parseInt(prjExecTcDetails.get(j).get("ExecutedTcCount").toString()) == 0) {
						projectDetails.get(i).put("PassCount", 0);
						projectDetails.get(i).put("FailCount", 0);
						projectDetails.get(i).put("SkipCount", 0);
						projectDetails.get(i).put("BlockCount", 0);
						projectDetails.get(i).put("NotRunCount", 0);
					} else {
						projectDetails.get(i).put("PassCount", prjExecTcDetails.get(j).get("PassCount"));
						projectDetails.get(i).put("FailCount", prjExecTcDetails.get(j).get("FailCount"));
						projectDetails.get(i).put("SkipCount", prjExecTcDetails.get(j).get("SkipCount"));
						projectDetails.get(i).put("BlockCount", prjExecTcDetails.get(j).get("BlockCount"));
						projectDetails.get(i).put("NotRunCount", prjExecTcDetails.get(j).get("NotRunCount"));
					}
					projectDetails.get(i).put("ExecutedTcCount", prjExecTcDetails.get(j).get("ExecutedTcCount"));
					break;
				} else {
					projectDetails.get(i).put("ExecutedTcCount", 0);
					projectDetails.get(i).put("PassCount", 0);
					projectDetails.get(i).put("FailCount", 0);
					projectDetails.get(i).put("SkipCount", 0);
					projectDetails.get(i).put("BlockCount", 0);
					projectDetails.get(i).put("NotRunCount", 0);
				}
			}
		}
		logger.info("Return list = "+projectDetails);
		return projectDetails;
	}

	@Override
	public void insertAssignedProjectRecords(final List<UserProjectDetails> userProjectDetailsList, String schema) {
		String sql = String.format(project.insertAssignedProjectRecords, schema);
		
		logger.info("insert multiple assigned project details list");
		jdbctemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public int getBatchSize() {
				return userProjectDetailsList.size();
			}

			@Override
			public void setValues(java.sql.PreparedStatement ps, int i) throws SQLException {
				UserProjectDetails userProjectDetails = userProjectDetailsList.get(i);
				
				ps.setInt(1, userProjectDetails.getUserProjectId());
				ps.setInt(2, userProjectDetails.getProjectId());
				ps.setInt(3, userProjectDetails.getUserId());

			}
		});
	}

	@Override
	public Map<String, Object> getProjectData(String schema) {
		logger.info("read query optimized project data");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getProjectData")
				.withCatalogName(schema);

		Map<String, Object> projectData = simpleJdbcCall.execute();
		logger.info("Query Result  " + projectData);
		return projectData;
	}

	@Override
	public Map<String, Object> getEditProjectData(int projectId, String schema) {
		logger.info("read query optimized edit project data ");
		SqlParameter projectIdTemp = new SqlParameter("projectId", Types.VARCHAR);
		SqlParameter[] paramArray = { projectIdTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getEditProjectData")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(projectId);
		logger.info("Query Result  " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> updateProjectData(ProjectDetails updateProject, String schema) {
		logger.info("read query optimized update project data");
		SqlParameter projectDescription = new SqlParameter("projectDescription", Types.VARCHAR);
		SqlParameter projectActive = new SqlParameter("projectActive", Types.INTEGER);
		SqlParameter projectId = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter[] paramArray = { projectDescription, projectActive, projectId };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("updateProjectData")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(updateProject.getProjectDescription(),
				updateProject.getProjectStatus(), updateProject.getProjectId());
		logger.info("Query Result  " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getAssignProjectData(int projectId, String schema) {
		logger.info("read query optimized assign project data");
		SqlParameter projectIdTemp = new SqlParameter("projectId", Types.VARCHAR);
		SqlParameter[] paramArray = { projectIdTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getAssignProjectData")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(projectId);
		logger.info("Query Result  " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> assignOrUnassignProjectToUser(int projectId, String schema) {
		logger.info("read query optimized assign or un assign project data");
		SqlParameter projectIdTemp = new SqlParameter("projectId", Types.VARCHAR);
		SqlParameter[] paramArray = { projectIdTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("assignOrUnassignProjectToUser").declareParameters(paramArray)
				.withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(projectId);
		logger.info("Query Result  " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	
	@Override
	public int deleteUnassignedProjectRecords(ArrayList<Integer> userId,int projectId, String schema) {
		
		
		String deleteId="";
		
		
		for(int i=0;i<userId.size();i++)
		{	
			deleteId+=userId.get(i);
			if(i<userId.size()-1)
				deleteId+=",";
		}
		String deleteQuery="DELETE FROM `%s`.xe_user_project  WHERE user IN ("+deleteId+") and project_id=?";
		String sql = String.format(deleteQuery, schema);
		logger.info("Query to delete Unassigned Project Records "+sql);
		int flag = database.update(sql, new Object[] { projectId }, new int[] { Types.INTEGER });
		logger.info("Query Result " + flag);
		return flag;
	}


	@Override
	public Map<String, Object> assignModuleToExecute(int buildId,int projectId,int moduleId,int userId,int updater, String schema) {
		logger.info("Read query optimized assign assign module to execute");
		SqlParameter buildIdTemp = new SqlParameter("buildId", Types.VARCHAR);
		SqlParameter projectIdTemp = new SqlParameter("projectId", Types.VARCHAR);
		SqlParameter moduleIdTemp = new SqlParameter("moduleId", Types.VARCHAR);
		SqlParameter userIdTemp = new SqlParameter("userId", Types.VARCHAR);
		SqlParameter updaterTemp = new SqlParameter("updater", Types.VARCHAR);
		SqlParameter[] paramArray = {buildIdTemp, projectIdTemp, moduleIdTemp,userIdTemp,updaterTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("assignModuleToExecute").declareParameters(paramArray)
				.withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(buildId,projectId,moduleId,userId,updater);
		logger.info("Query Result  " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getApplicationSettingsData(int applicationID, String schema) {
		logger.info("Query to get application settings data");
		SqlParameter applicationIDTemp = new SqlParameter("applicationID", Types.INTEGER);
		SqlParameter[] paramArray = { applicationIDTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("applicationSettings")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(applicationID);
		logger.info("Query Result  " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> updateApplicationDetails(ProjectDetails updateProject, String schema) {
		logger.info("Query to update application details");
		SqlParameter projectDescription = new SqlParameter("projectDescription", Types.VARCHAR);
		SqlParameter projectActive = new SqlParameter("projectActive", Types.INTEGER);
		SqlParameter projectId = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter[] paramArray = { projectDescription, projectActive, projectId };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("updateApplication")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(updateProject.getProjectDescription(),
				updateProject.getProjectStatus(), updateProject.getProjectId());
		logger.info("Query Result  " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public int unAssignAppToUser(int appID, int userID, String schema) {
		logger.info("SQL Query to un-assign application to user ");
		int flag = database.update(String.format(project.unAssignAppToUser, schema),
				new Object[] { appID, userID }, new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public Map<String, Object> getMailSettings(String schema) {
		logger.info("Query to get mail settings data");
	
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getMailSettings")
				.withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Query Result  " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public int getLoginUserAssignedAppCount(HttpSession session) {
		logger.info("SQL Query to get login user assigned applocation count ");
		int userId=Integer.parseInt(session.getAttribute("userId").toString());
		String schema=session.getAttribute("cust_schema").toString();

		int count = database.select(String.format(project.loginUserAssignedAppCount, schema, schema, schema),
				new Object[] { userId, userId });
		if(count==0){
			List<Map<String, Object>> resultSet=null;
			resultSet = database.select("SELECT project_id FROM "+schema+".xe_user_project where user="+userId+" limit 1");
			if(resultSet==null || resultSet.size() ==0){
			resultSet = database.select("SELECT project_id FROM "+schema+".xe_project  limit 1");
			}
		    int projectId= Integer.parseInt(resultSet.get(0).get("project_id").toString());
		    session.setAttribute("UserCurrentProjectId", projectId);
		    count=insertCurrentProject(userId, projectId, schema);
		}
		logger.info("Query Result  " + count);
		return count;

	}
	
	@Override
	public List<Map<String, Object>> getProjectDetailsByName(String projectName,String schema) {
		String sql = String.format(project.getProjectDetailsByName, schema, schema);
		List<Map<String, Object>> projectDetailsByName = database.select(sql,new Object[]{projectName},new int[] {Types.VARCHAR});
		logger.info("Query to get All Project Details" + sql);
		logger.info("Result 'All Project Details' " + projectDetailsByName);
		return projectDetailsByName;
	}

	@Override
	public List<Map<String, Object>> getMailSettingsFroAssignApp(String schema) {
		logger.info("SQL Query to get mail settings data for the assign application ");
		
		List<Map<String,Object>> mailDetails = database.select(String.format(project.getMailSettingsForAssignApp, schema, schema));
		
		logger.info("Query Result  " + mailDetails);
		
		
		return mailDetails;
	}
	
	
	@Override
	public int getProjectID(String projectName,String schema) {
		logger.info("SQL Query to get project ID ");
		
		String sql = String.format(project.getProjectId, schema, schema);
		
		//int projectId = database.select(sql,new Object[]{projectName},new int[] {Types.VARCHAR});
		//int projectId = database.update(sql, new Object[] { projectName }, new int[] { Types.VARCHAR });
		
		List<Map<String,Object>> resultSet= database.select(sql,new Object[]{projectName},new int[] {Types.VARCHAR});	    
	    int projectId= Integer.parseInt(resultSet.get(0).get("project_id").toString());
		logger.info("Query Result  " + projectId);
		
		
		return projectId;
	}

	/**
	 * @author Abhay.Thakur
	 * 
	 * This method gives Jira credentials
	 * @param schema
	 * @return
	 */
	@Override
	public List<Map<String, Object>> getJiraDetails(String schema) {
		String sql = "SELECT * FROM "+schema+".xead_jira_details";
		List<Map<String, Object>> jiraDetails = database.select(sql);
		logger.info("Query to get All jira Details" + sql);
		logger.info("Result 'Jira Details' " + jiraDetails);
		return jiraDetails;
	}
	
}

package com.xenon.buildmanager.dao;

import java.util.List;
import java.util.Map;

import com.xenon.buildmanager.domain.DocumentLibraryDetails;

/**
 * @author bhagyashri.ajmera
 *
 * This interface handles all document library related operations
 */
public interface DocumentLibraryDAO {
		/**
		 * @author bhagyashri.ajmera
		 * 
		 * This method is to upload document
		 * 
		 * @param docDetails
		 * @param schema
		 * @return
		 */
		int uploadDocument(DocumentLibraryDetails docDetails, String schema);
		
		/**
		 * @author bhagyashri.ajmera
		 * 
		 * This method is to get documents by project id
		 * 
		 * @param projectId
		 * @param schema
		 * @return
		 */
		List<Map<String, Object>> getDocumentsByProjectId(int projectId,String schema);
		
		/**
		 * @author bhagyashri.ajmera
		 * 
		 * This method gives document by document id
		 * 
		 * @param doclibraryId
		 * @param schema
		 * @return
		 */
		List<Map<String, Object>> getDocumentById(int doclibraryId, String schema);
		
		/**
		 * @author bhagyashri.ajmera
		 * 
		 * This method gives document library data
		 * 
		 * @param projectId
		 * @param schema
		 * @return
		 */
		Map<String, Object> getDocumentLibraryData(int projectId, int userId, String schema); 
}

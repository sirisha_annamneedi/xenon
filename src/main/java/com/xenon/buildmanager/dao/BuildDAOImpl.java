package com.xenon.buildmanager.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.api.common.MailService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.domain.BuildDetails;
import com.xenon.buildmanager.domain.BuildTestCases;
import com.xenon.common.dao.MailDAO;
import com.xenon.common.dao.MailGroupDAO;
import com.xenon.core.dao.CustomerDAO;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import com.xenon.testmanager.dao.TestcaseDAOImpl;

@SuppressWarnings("unchecked")
public class BuildDAOImpl implements BuildDAO, XenonQuery {

	private static final Logger logger = LoggerFactory.getLogger(BuildDAOImpl.class);

	@Resource(name = "mailProperties")
	private Properties mailProperties;

	@Resource(name = "configurationProperties")
	private Properties configurationProperties;

	@Autowired
	TestcaseDAOImpl testCase;

	@Autowired
	DatabaseDao database;

	@Autowired
	MailDAO mailDAO;

	@Autowired
	MailGroupDAO mailGroupDAO;

	@Autowired
	CustomerDAO customerDao;

	@Autowired
	UserDAO userDao;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	UtilsService utilsService;

	@Autowired
	MailService mailService;

	@Autowired
	VMDetailsDAO vmDetailsDAO;

	@Override
	public int insertBuild(BuildDetails insertBuild, String schema) throws SQLException {
		String callInsertBuild = String.format(build.createNewBuild, schema);
		logger.info("Query to insert build" + callInsertBuild);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsertBuild);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, insertBuild.getBuildName());
		callableStmt.setString(3, insertBuild.getBuildDesc());
		callableStmt.setInt(4, insertBuild.getBuildStatus());
		callableStmt.setInt(5, insertBuild.getBuildReleaseID());
		callableStmt.setInt(6, insertBuild.getBuildCreatorID());
		callableStmt.setString(7, insertBuild.getcurrentDate());
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Query Result  " + insertStatus);
		return insertStatus;
	}

	@Override
	public int insertAutomationBuild(BuildDetails insertBuild, String schema) throws SQLException {

		String callInsertBuild = String.format(build.createNewAutomationBuild, schema);
		logger.info("Query to insert automation build" + callInsertBuild);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsertBuild);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, insertBuild.getBuildName());
		callableStmt.setString(3, insertBuild.getBuildDesc());
		callableStmt.setInt(4, insertBuild.getBuildStatus());
		callableStmt.setInt(5, insertBuild.getBuildReleaseID());
		callableStmt.setInt(6, insertBuild.getBuildCreatorID());
		callableStmt.setString(7, insertBuild.getcurrentDate());
		callableStmt.setInt(8, insertBuild.getTestSetExecType());
		callableStmt.setString(9, insertBuild.getRedwoodId());
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Query Result  " + insertStatus);
		return insertStatus;
	}

	@Override
	public int cloneBuild(BuildDetails insertBuild, String schema, int sourceBuildID) throws SQLException {

		String callInsertBuild = String.format(build.createNewBuild, schema);
		logger.info("Query to clone build" + callInsertBuild);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsertBuild);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, insertBuild.getBuildName());
		callableStmt.setString(3, insertBuild.getBuildDesc());
		callableStmt.setInt(4, insertBuild.getBuildStatus());
		callableStmt.setInt(5, insertBuild.getBuildReleaseID());
		callableStmt.setInt(6, insertBuild.getBuildCreatorID());
		callableStmt.executeUpdate(callInsertBuild, CallableStatement.RETURN_GENERATED_KEYS);
		int insertStatus = callableStmt.getInt(1);

		ResultSet rs = callableStmt.getGeneratedKeys();

		@SuppressWarnings("unused")
		int autoIncValue = -1;

		if (rs.next()) {
			autoIncValue = rs.getInt(1);

		}
		callableStmt.close();
		logger.info("Query Result  " + insertStatus);
		return insertStatus;
	}

	@Override
	public List<Map<String, Object>> getBuildDetails(int buildID, String schema) {
		String sql = String.format(build.getBuildDetails, schema, schema, schema, schema, schema);
		logger.info("Query to get build details " + sql);
		List<Map<String, Object>> buildDetails = database.select(sql, new Object[] { buildID },
				new int[] { Types.INTEGER });
		logger.info("Query Result  " + buildDetails);
		return buildDetails;
	}

	@Override
	public int updateBuild(BuildDetails updateBuild, String schema) {
		String sql = String.format(build.updateBuild, schema);
		logger.info("Query to update build details " + sql);
		int flag = database.update(sql,
				new Object[] { updateBuild.getBuildName(), updateBuild.getBuildDesc(), updateBuild.getBuildStatus(),
						updateBuild.getBuildReleaseID(), updateBuild.getUpdateDate(), updateBuild.getBuildID() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public int updateAutoBuild(BuildDetails updateBuild, String schema) {
		String sql = String.format(build.updateAutoBuild, schema);
		logger.info("Query to update automation build details " + sql);
		int flag = database.update(sql,
				new Object[] { updateBuild.getBuildName(), updateBuild.getBuildDesc(), updateBuild.getBuildStatus(),
						updateBuild.getBuildReleaseID(), updateBuild.getUpdateDate(), updateBuild.getBuildID() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public int updateBuildName(int buildId, String buildName, boolean isManual, String schema) {
		String sql = String.format(isManual ? build.updateBuildNameManual : build.updateBuildNameAutomation, schema);
		logger.info("Query to update automation build details " + sql);
		int flag = database.update(sql, new Object[] { buildName, buildId },
				new int[] { Types.VARCHAR, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public int deleteBuildSoftly(int buildId, boolean isManual, String schema) {
		String sql = String.format(isManual ? build.deleteBuildSoftlyManual : build.deleteBuildSoftlyAutomation,
				schema);
		logger.info("Query to update automation build details " + sql);
		int flag = database.update(sql, new Object[] { buildId }, new int[] { Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> getAllBuildDetails(String schema) {
		String sql = String.format(build.getAllBuildDetails, schema, schema, schema, schema, schema);
		logger.info("Query to get all build details " + sql);
		List<Map<String, Object>> allBuildDetails = database.select(sql);
		logger.info("Query Result  " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public List<Map<String, Object>> getAddRemoveTCDetailsForManager(String schema) {
		String sql = String.format(build.addRemoveTCDetailsForManager, schema, schema, schema, schema);
		logger.info("Query to get Add Remove TCDetails For Manager " + sql);
		List<Map<String, Object>> details = database.select(sql);
		logger.info("Query Result  " + details);
		return details;
	}

	@Override
	public List<Map<String, Object>> getAllTestCases(int buildId, String schema) {
		String sql = String.format(build.getAllBuildDetails, schema, schema, schema, schema);
		logger.info("Query to get All Test Cases " + sql);
		List<Map<String, Object>> allBuildDetails = database.select(sql);
		logger.info("Query Result  " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public int addTestCaseById(String testCaseArray, int buildId, int userId, String schema) {
		String sql = String.format(build.addTestcase, schema);
		logger.info("Query to add Test Case By Id " + sql);
		int flag = database.update(sql, new Object[] { testCaseArray, buildId, userId },
				new int[] { Types.VARCHAR, Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public int dropTestCaseBySce(String dropTestArray, int buildId, String schema) {
		String sql = String.format(build.dropTestcase, schema);
		logger.info("Query to drop Tes tCase By Sce " + sql);
		int flag = database.update(sql, new Object[] { dropTestArray, buildId },
				new int[] { Types.VARCHAR, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public int cloneTestcases(int sourceBuild, int destinationBuildid, String schema) {
		String sql = String.format(build.cloneTestcases, schema);
		int flag = database.update(sql, new Object[] { sourceBuild, destinationBuildid, },
				new int[] { Types.INTEGER, Types.INTEGER });
		return flag;
	}

	@Override
	public int updateBuildStatus(int buildStatus, int buildId, String schema) {
		String sql = String.format(build.setBuildStatus, schema);
		logger.info("Query to update Build Status " + sql);
		int flag = database.update(sql, new Object[] { buildStatus, buildId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public int updateAutomationBuildStatus(int buildStatus, int buildId, String schema) {
		String sql = String.format(build.setAutomationBuildStatus, schema);
		logger.info("Query to update Build Status " + sql);
		int flag = database.update(sql, new Object[] { buildStatus, buildId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public int createNewBuildStatus(String schema, int custTypeId) throws SQLException {
		String createNewBuildStatus = String.format(build.createNewBuildStatus, schema);
		logger.info("This method is to check customer to build count exceeds or not" + createNewBuildStatus);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(createNewBuildStatus);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setInt(2, custTypeId);
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		logger.info("Insert status  = " + insertStatus);
		callableStmt.close();
		logger.info("Query Result  " + insertStatus);
		return insertStatus;

	}

	@Override
	public int createBugOnExecution(int tcId, String notes, int tcStatus, int buildId, int stepId, int stepStatus,
			String stepComment, int testCaseInsertMethod, int stepInsertMethod, int userId, int bugId, byte[] bytes,
			String schema) throws SQLException {

		String createBugOnExecution = String.format(build.createBugOnExecution, schema);
		logger.info("Query to create Bug On Execution " + createBugOnExecution);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(createBugOnExecution);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setInt(2, tcId);
		callableStmt.setString(3, notes);
		callableStmt.setInt(4, tcStatus);
		callableStmt.setInt(5, buildId);
		callableStmt.setInt(6, stepId);
		callableStmt.setInt(7, stepStatus);
		callableStmt.setString(8, stepComment);
		callableStmt.setInt(9, testCaseInsertMethod);
		callableStmt.setInt(10, stepInsertMethod);
		callableStmt.setInt(11, userId);
		callableStmt.setInt(12, bugId);
		callableStmt.setBytes(13, bytes);
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Query Result  " + insertStatus);
		return insertStatus;

	}

	@Override
	public List<Map<String, Object>> getBuildTcCount(int userID, String schema) {
		logger.info("Reading build TC count");
		logger.info("Query param userID ={} ", userID);
		String sql = String.format(build.buildTcCount, schema, schema, schema, schema, schema);
		List<Map<String, Object>> details = database.select(sql, new Object[] { userID, userID },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Return List = " + details);
		return details;
	}

	@Override
	public List<Map<String, Object>> getTcExecStatusByBuildId(int buildId, String schema) {
		logger.info("Reading Test case execution status by build id");
		logger.info("Query param buildId ={} ", buildId);
		String sql = String.format(build.getTcExecStatusByBuildId, schema, schema, schema);
		List<Map<String, Object>> details = database.select(sql, new Object[] { buildId }, new int[] { Types.INTEGER });
		logger.info("Return List = " + details);
		return details;
	}

	@Override
	public List<Map<String, Object>> getTcExecStatusByBuildIdAndTcId(int buildId, int tcId, String schema) {
		logger.info("Reading TC execution status by build and TC id");
		logger.info("Query param buildId ={} , tcId = ", buildId, tcId);
		String sql = String.format(build.getTcExecStatusByBuildIdAndTcId, schema);
		List<Map<String, Object>> details = database.select(sql, new Object[] { buildId, tcId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Return List = " + details);
		return details;
	}

	@Override
	public List<Map<String, Object>> getTcStepsStatusByBuildId(int buildId, String schema) {
		logger.info("Reading test case step status by build id");
		logger.info("Query Param  buildId = {}", buildId);
		String sql = String.format(build.getTcStepsStatusByBuildId, schema, schema);
		List<Map<String, Object>> details = database.select(sql, new Object[] { buildId }, new int[] { Types.INTEGER });
		logger.info("Return List = " + details);
		return details;
	}

	@Override
	public List<Map<String, Object>> getAllBuildDetailsToExecute(int userId, String schema) {
		logger.info("Reading all build details to execute");
		logger.info("Query param  userId = {}", userId);
		String sql = String.format(build.getAllBuildDetailsToExecute, schema, schema, schema, schema, schema, schema);
		List<Map<String, Object>> details = database.select(sql, new Object[] { userId }, new int[] { Types.INTEGER });
		logger.info("Return List = " + details);
		return details;
	}

	@Override
	public List<Map<String, Object>> getAddRemoveTCDetailsForCreator(int userID, String schema) {
		String sql = String.format(build.addRemoveTCDetailsForCreator, schema, schema, schema, schema);
		logger.info("Query to get Add Remove TCDetails For Creator " + sql);
		List<Map<String, Object>> details = database.select(sql, new Object[] { userID }, new int[] { Types.INTEGER });
		logger.info("Query Result  " + details);
		return details;
	}

	@Override
	public int updateBuildStatusById(String buildStatus, int buildId, String schema) {
		String sql = String.format(build.setBuildStatusById, schema, schema);
		logger.info("Query to update Build Status ById " + sql);
		int flag = database.update(sql, new Object[] { buildStatus, buildId },
				new int[] { Types.VARCHAR, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> getAllExecutedBuildDetails(String schema) {
		logger.info("SQL Query to get manual build details for report");
		String sql = String.format(build.getAllExecutedBuildDetails, schema, schema, schema, schema, schema);
		logger.info("SQL : " + sql);
		List<Map<String, Object>> allBuildDetails = database.select(sql);
		logger.info("SQL Result " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public List<Map<String, Object>> getAutomationExecutedBuildDetails(String schema) {
		logger.info("SQL Query to get Automation build details for report");
		String sql = String.format(build.getAutoExecutedBuildDetails, schema, schema, schema, schema, schema);
		logger.info("SQL : " + sql);
		List<Map<String, Object>> allBuildDetails = database.select(sql);
		logger.info("SQL Result " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public List<Map<String, Object>> getBugByExcutionId(int step_exe_id, String schema) {
		String sql = String.format(build.getBugByExcutionId, schema);
		logger.info("Query to get Bug By Excution Id " + sql);
		List<Map<String, Object>> allBuildDetails = database.select(sql);
		logger.info("Query Result  " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public int insertExecutionBug(int stepId, int bugId, String schema) {
		String sql = String.format(build.insertExecutionBuild, schema);
		logger.info("Query to insert Execution Bug " + sql);
		int flag = database.update(sql, new Object[] { stepId, bugId }, new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public int checkBuildAssigner(int buildId, int userId, String schema) {
		String sql = String.format(build.checkBuildAssigner, schema);
		logger.info("Query to check Build Assigner " + sql);
		List<Map<String, Object>> details = database.select(sql, new Object[] { buildId, userId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + details);
		if (details.size() > 0)
			return 1;
		else
			return 0;
	}

	@Override
	public int checkViewAllBuildsAccess(int userId, String schema) {
		logger.info("Query to check View All Builds Access ");
		List<Map<String, Object>> details = database.select(
				String.format(build.checkViewAllBuildsAccess, schema, schema), new Object[] { userId },
				new int[] { Types.INTEGER });
		logger.info("Query Result  " + details);
		return Integer.parseInt(details.get(0).get("viewallbuild").toString());
	}

	@Override
	public List<Map<String, Object>> getAssigneeListByBuild(int buildId, String schema) {
		logger.info("Query to get Assignee List By Build ");
		List<Map<String, Object>> allBuildDetails = database.select(
				String.format(build.getAssigneeListByBuild, schema, schema), new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Query Result  " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public List<Map<String, Object>> checkAssignedModules(int buildId, String schema) {
		logger.info("Query to check Assigned Modules ");
		List<Map<String, Object>> allBuildDetails = database.select(
				String.format(build.getAssigneeModulesByBuild, schema, schema), new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Query Result  " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public List<Map<String, Object>> getModulesFromTestcase(int buildId, String schema) {
		logger.info("Query to get Modules From Test case ");
		List<Map<String, Object>> allBuildDetails = database.select(
				String.format(build.getModulesFromTestcase, schema, schema), new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Query Result  " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public List<Map<String, Object>> getBuildTcExecCountsByBuildId(int buildId, String schema) {
		logger.info("Query to get Build Tc Exec Counts By Build Id ");
		List<Map<String, Object>> allBuildDetails = database.select(
				String.format(build.getBuildTcExecCountsByBuildId, schema, schema, schema, schema, schema, schema),
				new Object[] { buildId, buildId, buildId, 1, buildId, 2, buildId, 3, buildId, 4 },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public List<Map<String, Object>> getBuildDashboardCount(int user_id, String schema) {
		logger.info("Query to get Build Dashboard Count ");
		List<Map<String, Object>> countBuildDetails = database
				.select(String.format(build.getBuildDashboard, schema, schema, schema, schema, schema, schema, schema));
		logger.info("Query Result  " + countBuildDetails);
		return countBuildDetails;
	}

	public List<Map<String, Object>> getAllBuilds(String schema) {
		logger.info("Query to get All Builds ");
		List<Map<String, Object>> countBuildDetails = database
				.select(String.format(build.getAllBuilds, schema, schema, schema));
		logger.info("Query Result  " + countBuildDetails);
		return countBuildDetails;
	}
	
	public List<Map<String, Object>> getAllAutoBuilds(String schema) {
		logger.info("Query to get All Auto Builds ");
		List<Map<String, Object>> countBuildDetails = database
				.select(String.format(build.getAllAutoBuilds, schema, schema, schema));
		logger.info("Query Result  " + countBuildDetails);
		return countBuildDetails;
	}
	
	@Override
	public List<Map<String, Object>> getAllAutoBuildsByReleaseId(int releaseId, String schema){
		logger.info("Query to get all Auto Builds based on releaseId");
		List<Map<String,Object>> buildDetails = database.select(String.format(build.getAllAutoBuildsByReleaseId, schema), new Object[] { releaseId },
				new int[] { Types.INTEGER });
		return buildDetails;
	}
	
	public List<Map<String, Object>> getTestCasebyBuild(int user_id, String schema) {
		logger.info("Query to get Test Case by Build ");
		List<Map<String, Object>> getTestCasebyBuild = database
				.select(String.format(build.getTCbyBuild, schema, schema));
		logger.info("Query Result  " + getTestCasebyBuild);
		return getTestCasebyBuild;
	}

	public List<Map<String, Object>> getAllBuildsTestcasesCount(String schema) {
		logger.info("Query to get All Builds Test cases Count");
		List<Map<String, Object>> countBuildDetails = database
				.select(String.format(build.getAllBuildsTestcasesCount, schema));
		logger.info("Query Result  " + countBuildDetails);
		return countBuildDetails;
	}

	public List<Map<String, Object>> getAllBuildsExecutedTestcasesCount(String schema) {
		logger.info("Query to get All Builds Executed Test cases Count");
		List<Map<String, Object>> countBuildDetails = database
				.select(String.format(build.getAllBuildsExecutedTestcasesCount, schema));
		logger.info("Query Result  " + countBuildDetails);
		return countBuildDetails;
	}

	public List<Map<String, Object>> getExetestcaseStatus(int build_id, String schema) {
		Boolean flag = true;
		logger.info("Query to get Exe test case Status ");
		List<Map<String, Object>> countBuildDetails = database.select(
				String.format(build.getTCstatuscount, schema, schema), new Object[] { build_id },
				new int[] { Types.INTEGER });

		List<Map<String, Object>> not_run = getNotRuntestcaseCount(build_id, schema);
		for (int i = 0; i < countBuildDetails.size(); i++) {
			if (countBuildDetails.get(i).get("tc_status").equals("Not run")) {
				flag = false;
				countBuildDetails.get(i).put("tc_count", not_run.get(0).get("not_run"));
			}
		}
		if (flag) {
			Map<String, Object> no_run = new HashMap<String, Object>();
			no_run.put("tc_count", not_run.get(0).get("not_run"));
			no_run.put("build_id", build_id);
			no_run.put("tc_status", "Not run");
			countBuildDetails.add(no_run);
		}
		logger.info("Query Result  " + countBuildDetails);
		return countBuildDetails;
	}

	public List<Map<String, Object>> getNotRuntestcaseCount(int build_id, String schema) {
		logger.info("Query to get Not Run test cases Count ");
		List<Map<String, Object>> getNotRuntestcaseCount = database.select(
				String.format(build.getNotrun, schema, schema, schema), new Object[] { build_id, build_id },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + getNotRuntestcaseCount);
		return getNotRuntestcaseCount;
	}

	public List<Map<String, Object>> getTopBuildGraph(int userId, String schema) {
		logger.info("Query to get Top Build Graph ");
		List<Map<String, Object>> countBuildDetails = database.select(String.format(build.topBuild, schema));
		for (int i = 0; i < countBuildDetails.size(); i++) {
			int buildId = Integer.parseInt(countBuildDetails.get(i).get("build_id").toString());
			countBuildDetails.get(i).put("testcase_status", getExetestcaseStatus(buildId, schema));
		}
		logger.info("Query Result  " + countBuildDetails);
		return countBuildDetails;
	}

	@Override
	public List<Map<String, Object>> getBmDashboardTopBuilds(List<Map<String, Object>> allBuildsTestcasesCount1,
			List<Map<String, Object>> allBuildsExecutedTestcasesCount1, String schema) {
		logger.info("Query to get Bm Dashboard Top Builds ");
		List<Map<String, Object>> allBuilds = getAllBuilds(schema);
		List<Map<String, Object>> allBuildsTestcasesCount = allBuildsTestcasesCount1;
		List<Map<String, Object>> allBuildsExecutedTestcasesCount = allBuildsExecutedTestcasesCount1;

		for (int i = 0; i < allBuilds.size(); i++) {
			if (allBuildsTestcasesCount.size() > 0) {
				for (int j = 0; j < allBuildsTestcasesCount.size(); j++) {
					if (Integer.parseInt(allBuildsTestcasesCount.get(j).get("build_id").toString()) == Integer
							.parseInt(allBuilds.get(i).get("build_id").toString())) {
						allBuilds.get(i).put("TotalTcCount", allBuildsTestcasesCount.get(j).get("TotalTcCount"));
						break;
					} else {
						allBuilds.get(i).put("TotalTcCount", 0);
					}
				}
			} else
				allBuilds.get(i).put("TotalTcCount", 0);

		}

		for (int i = 0; i < allBuilds.size(); i++) {

			if (allBuildsExecutedTestcasesCount.size() > 0) {
				for (int k = 0; k < allBuildsExecutedTestcasesCount.size(); k++) {
					if (Integer.parseInt(allBuildsExecutedTestcasesCount.get(k).get("build_id").toString()) == Integer
							.parseInt(allBuilds.get(i).get("build_id").toString())) {
						allBuilds.get(i).put("ExecutedTcCount",
								allBuildsExecutedTestcasesCount.get(k).get("ExecutedTcCount"));
						break;
					} else {
						allBuilds.get(i).put("ExecutedTcCount", 0);
					}
				}
			} else
				allBuilds.get(i).put("ExecutedTcCount", 0);
		}

		for (int i = 0; i < allBuilds.size(); i++) {
			if (Integer.parseInt(allBuilds.get(i).get("ExecutedTcCount").toString()) == 0
					|| Integer.parseInt(allBuilds.get(i).get("TotalTcCount").toString()) == 0)
				allBuilds.get(i).put("percentCompleted", 0);
			else
				allBuilds.get(i).put("percentCompleted",
						String.format("%.2f", (Float.valueOf(allBuilds.get(i).get("ExecutedTcCount").toString())
								/ Float.valueOf(allBuilds.get(i).get("TotalTcCount").toString()) * 100)));
		}
		logger.info("Query Result  " + allBuilds);
		return allBuilds;

	}

	@Override
	public List<Map<String, Object>> getBmDashboardTopAutoBuilds(List<Map<String, Object>> allBuildsTestcasesCount1,
			List<Map<String, Object>> allBuildsExecutedTestcasesCount1, String schema) {
		logger.info("Query to get Bm Dashboard Top Auto Builds");
		List<Map<String, Object>> allBuilds = getAllAutoBuilds(schema);
		List<Map<String, Object>> allBuildsTestcasesCount = allBuildsTestcasesCount1;
		List<Map<String, Object>> allBuildsExecutedTestcasesCount = allBuildsExecutedTestcasesCount1;

		for (int i = 0; i < allBuilds.size(); i++) {
			if (allBuildsTestcasesCount.size() > 0) {
				for (int j = 0; j < allBuildsTestcasesCount.size(); j++) {
					if (Integer.parseInt(allBuildsTestcasesCount.get(j).get("build_id").toString()) == Integer
							.parseInt(allBuilds.get(i).get("build_id").toString())) {
						allBuilds.get(i).put("TotalTcCount", allBuildsTestcasesCount.get(j).get("TotalTcCount"));
						break;
					} else {
						allBuilds.get(i).put("TotalTcCount", 0);
					}
				}
			} else
				allBuilds.get(i).put("TotalTcCount", 0);

		}

		for (int i = 0; i < allBuilds.size(); i++) {

			if (allBuildsExecutedTestcasesCount.size() > 0) {
				for (int k = 0; k < allBuildsExecutedTestcasesCount.size(); k++) {
					if (Integer.parseInt(allBuildsExecutedTestcasesCount.get(k).get("build_id").toString()) == Integer
							.parseInt(allBuilds.get(i).get("build_id").toString())) {
						allBuilds.get(i).put("ExecutedTcCount",
								allBuildsExecutedTestcasesCount.get(k).get("ExecutedTcCount"));
						break;
					} else {
						allBuilds.get(i).put("ExecutedTcCount", 0);
					}
				}
			} else
				allBuilds.get(i).put("ExecutedTcCount", 0);
		}

		for (int i = 0; i < allBuilds.size(); i++) {
			if (Integer.parseInt(allBuilds.get(i).get("ExecutedTcCount").toString()) == 0
					|| Integer.parseInt(allBuilds.get(i).get("TotalTcCount").toString()) == 0)
				allBuilds.get(i).put("percentCompleted", 0);
			else
				allBuilds.get(i).put("percentCompleted",
						String.format("%.2f", (Float.valueOf(allBuilds.get(i).get("ExecutedTcCount").toString())
								/ Float.valueOf(allBuilds.get(i).get("TotalTcCount").toString()) * 100)));
		}
		logger.info("Query Result  " + allBuilds);
		return allBuilds;

	}

	@Override
	public Map<String, Object> getReportDataByBuildId(int buildId, int userId, String schema) {
		logger.info("Calling Stored procedure to get build report data");
		SqlParameter tempBuildId = new SqlParameter("buildId", Types.INTEGER);
		SqlParameter tempUserId = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = { tempBuildId, tempUserId };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getReportByBuildId")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(buildId, userId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public List<Map<String, Object>> getBuildProjectData(int userId, int buildId, String schema) {
		logger.info("Query to get Build Project Data ");
		List<Map<String, Object>> getBuildProjectData = database.select(String.format(build.getBuildData, schema),
				new Object[] { userId, buildId }, new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + getBuildProjectData);
		return getBuildProjectData;
	}

	@Override
	public List<Map<String, Object>> getBuggraphdata(String schema) {
		logger.info("Query to get Build bug Data ");
		List<Map<String, Object>> getBuggraphdata = database.select(
				String.format(build.getBuggraphData, schema, schema, schema, schema, schema, schema, schema, schema));
		logger.info("Query Result  " + getBuggraphdata);
		return getBuggraphdata;
	}

	@Override
	public Map<String, Object> getCreateBuildData(String schema) {
		logger.info("Calling Stored procedure to get Create Build Data");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("createbuild")
				.declareParameters().withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getViewBuildData(int userId, int automationStatus, int scriptlessStatus,
			int manualStartValue, int autoStartValue, int scStartValue, int pageSize, String schema) {
		logger.info("Calling Stored procedure to get View Build Data");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter autoStatus = new SqlParameter("automationStatus", Types.INTEGER);
		SqlParameter scriptStatus = new SqlParameter("scriptlessStatus", Types.INTEGER);
		SqlParameter manualSValue = new SqlParameter("manualStartValue", Types.INTEGER);
		SqlParameter autoSValue = new SqlParameter("autoStartValue", Types.INTEGER);
		SqlParameter scSValue = new SqlParameter("scStartValue", Types.INTEGER);
		SqlParameter pgSize = new SqlParameter("pageSize", Types.INTEGER);
		SqlParameter[] paramArray = { user, autoStatus, scriptStatus, manualSValue, autoSValue, scSValue, pgSize };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("viewbuild")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, automationStatus, scriptlessStatus,
				manualStartValue, autoStartValue, scStartValue, pageSize);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getEditBuildData(int buildId, int type, String schema) {
		logger.info("Calling Stored procedure to get get Edit Build Data");
		SqlParameter build = new SqlParameter("buildId", Types.INTEGER);
		SqlParameter exeType = new SqlParameter("type", Types.INTEGER);
		SqlParameter[] paramArray = { build, exeType };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("editbuild")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(buildId, type);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getBuildDashboard(int userId, String schema) {
		logger.info("Calling Stored procedure to get Build Dashboard");
		SqlParameter userIdTemp = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = { userIdTemp };
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBuildDashboard")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getBuildAutomationDashboard(String schema) {
		logger.info("Calling Stored procedure to get Build Automation Dashboard");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getAutomationBuildsDash")
				.declareParameters().withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getBuildDashboardLinkData(int userId, String schema) {
		SqlParameter userIdTemp = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = { userIdTemp };
		logger.info("Calling Stored procedure to get Build Dashboard Link Data");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("buildManagerData")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getExecuteBuildList(int userId, int manualStartValue, int autoStartValue, int pageSize,
			String schema) {
		logger.info("Calling Stored procedure to get Execute Build List");
		SqlParameter userIdTemp = new SqlParameter("userId", Types.VARCHAR);
		SqlParameter manualSValue = new SqlParameter("manualStartValue", Types.INTEGER);
		SqlParameter autoSValue = new SqlParameter("autoStartValue", Types.INTEGER);
		SqlParameter pgSize = new SqlParameter("pageSize", Types.INTEGER);
		SqlParameter[] paramArray = { userIdTemp, manualSValue, autoSValue, pgSize };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getExecuteBuildList")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, manualStartValue, autoStartValue,
				pageSize);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getAutomationBuildData(int custId, int userId, int buildId, int projectId,
			String execType, String schema) {
		logger.info("Calling Stored procedure to get Automation Build Data");
		if (execType.equals("2")) {
			SqlParameter customerId = new SqlParameter("custId", Types.VARCHAR);
			SqlParameter userIdTemp = new SqlParameter("userIdTemp", Types.VARCHAR);
			SqlParameter buildIdTemp = new SqlParameter("buildIdTemp", Types.VARCHAR);
			SqlParameter projectIdTemp = new SqlParameter("projectIdTemp", Types.VARCHAR);
			SqlParameter[] paramArray = { customerId, buildIdTemp, userIdTemp, projectIdTemp };

			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("executeAutomationBuild")
					.declareParameters(paramArray).withCatalogName(schema);

			Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(custId, buildId, userId, projectId);
			logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
			return simpleJdbcCallResult;
		} else {
			SqlParameter customerId = new SqlParameter("custId", Types.VARCHAR);
			SqlParameter userIdTemp = new SqlParameter("userIdTemp", Types.VARCHAR);
			SqlParameter buildIdTemp = new SqlParameter("buildIdTemp", Types.VARCHAR);
			SqlParameter projectIdTemp = new SqlParameter("projectIdTemp", Types.VARCHAR);
			SqlParameter executionType = new SqlParameter("execType", Types.VARCHAR);
			SqlParameter[] paramArray = { customerId, buildIdTemp, userIdTemp, projectIdTemp, executionType };

			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName("executeAutomationBuildApiRedwood").declareParameters(paramArray)
					.withCatalogName(schema);

			Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(custId, buildId, userId, projectId,
					execType);
			logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
			return simpleJdbcCallResult;
		}

	}

	@Override
	public List<Map<String, Object>> getBuildAddedTcCount(int userId, int buildId, String schema) {
		logger.info("Query to get Build Added Tc Count");
		List<Map<String, Object>> getBuildProjectData = database.select(
				String.format(build.getBuildAddedTcCount, schema, schema), new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Query Result  " + getBuildProjectData);
		return getBuildProjectData;
	}

	@Override
	public List<Map<String, Object>> getPreviewDetails(int userId, int buildId, String schema) {
		logger.info("Query to get Build total test case details");
		List<Map<String, Object>> getBuildProjectData = database
				.select("SELECT xt.testcase_id,xt.testcase_name,xt.test_prefix,xt.summary FROM " + schema
						+ ".xebm_automation_build_testcase xb," + schema
						+ ".xetm_testcase xt where xb.testcase_id=xt.testcase_id  and xt.is_deleted = 0 and xb.build_id="
						+ buildId);
		logger.info("Query Result  " + getBuildProjectData);
		return getBuildProjectData;
	}

	@Override
	public int dropAutomationTestCase(String dropTestArray, int buildId, String schema) {
		logger.info("Dropping test cases");
		logger.info("Query param dropTestArray ={} , buildId = {}", dropTestArray, buildId);
		String sql = String.format(build.dropAutomationTestCase, schema);
		int flag = database.update(sql, new Object[] { dropTestArray, buildId },
				new int[] { Types.VARCHAR, Types.INTEGER });
		logger.info("Return value = " + flag);
		return flag;
	}

	@Override
	public int addAutomationTestCase(String testCaseArray, int buildId, int userId, String schema) {
		logger.info("Adding test cases");
		logger.info("Query param testCaseArray ={} , buildId = {} , userId = {} ", testCaseArray, buildId, userId);
		String sql = String.format(build.addAutomationTestCase, schema);
		int flag = database.update(sql, new Object[] { testCaseArray, buildId, userId },
				new int[] { Types.VARCHAR, Types.INTEGER, Types.INTEGER });
		logger.info("Return value = " + flag);
		return flag;
	}

	@Override
	public Map<String, Object> buildExecutionDetails(int buildId, String schema) {
		logger.info("Calling Stored procedure to build Execution Details");
		SqlParameter build = new SqlParameter("buildId", Types.INTEGER);
		SqlParameter[] paramArray = { build };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("buildExecutionDetails")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(buildId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> buildExecutionDetailsMail(int buildId, String schema) {
		logger.info("Reading build execution mail details");
		SqlParameter build = new SqlParameter("buildId", Types.INTEGER);
		SqlParameter[] paramArray = { build };
		logger.info("Query param build Id= {}", build);
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("buildExecutionDetailsMail")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(buildId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public String sendMailOnBuildExecutionComplete(int buildId, String schema) throws Exception {
		logger.info("Sending mail on build execution compltete");
		Map<String, Object> buildExecDetails = buildExecutionDetails(buildId, schema);
		List<Map<String, Object>> moduleTcDetails = (List<Map<String, Object>>) buildExecDetails.get("#result-set-1");
		List<Map<String, Object>> moduleDetails = (List<Map<String, Object>>) buildExecDetails.get("#result-set-2");
		List<Map<String, Object>> buildDetails = (List<Map<String, Object>>) buildExecDetails.get("#result-set-3");

		List<Map<String, Object>> emailDetails = (List<Map<String, Object>>) buildExecDetails.get("#result-set-4");
		List<Map<String, Object>> emailNotificationDetails = (List<Map<String, Object>>) buildExecDetails
				.get("#result-set-5");

		if (emailDetails.size() == 0) {
			logger.info("Mail details not available");
			return "Mail details not available";
		}
		int sendStatus = Integer.parseInt(emailNotificationDetails.get(0).get("build_exec_complete").toString());
		if (sendStatus == 1) {
			logger.info("Reading details");
			List<Map<String, Object>> userDetails = (List<Map<String, Object>>) buildExecDetails.get("#result-set-6");
			ArrayList<String> recepients = new ArrayList<String>();
			for (int i = 0; i < userDetails.size(); i++) {
				recepients.add(userDetails.get(i).get("email_id").toString());
			}

			if (buildDetails.get(0).get("browser").toString().equalsIgnoreCase("1"))
				buildDetails.get(0).put("browserVal", "IE");
			else if (buildDetails.get(0).get("browser").toString().equalsIgnoreCase("2"))
				buildDetails.get(0).put("browserVal", "Chrome");
			else if (buildDetails.get(0).get("browser").toString().equalsIgnoreCase("3"))
				buildDetails.get(0).put("browserVal", "Firefox");
			else if (buildDetails.get(0).get("browser").toString().equalsIgnoreCase("4"))
				buildDetails.get(0).put("browserVal", "Safari");

			if (buildDetails.get(0).get("step_wise_screenshot").toString().equalsIgnoreCase("1"))
				buildDetails.get(0).put("step_wise_screenshot", "All Cases");
			else if (buildDetails.get(0).get("step_wise_screenshot").toString().equalsIgnoreCase("2"))
				buildDetails.get(0).put("step_wise_screenshot", "Failed Cases");
			else if (buildDetails.get(0).get("step_wise_screenshot").toString().equalsIgnoreCase("3"))
				buildDetails.get(0).put("step_wise_screenshot", "Disable");

			if (buildDetails.get(0).get("report_bug").toString().equalsIgnoreCase("1"))
				buildDetails.get(0).put("report_bug", "Active");
			else if (buildDetails.get(0).get("report_bug").toString().equalsIgnoreCase("2"))
				buildDetails.get(0).put("report_bug", "Inactive");

			String mailSubject = mailProperties.getProperty("AUTOMATION_BUILD_EXEC_COMPLETE_SUBJECT");// "Build
																										// Execution
																										// Completed";
			String tableText = "";

			for (int i = 0; i < moduleDetails.size(); i++) {

				int moduleId = Integer.parseInt(moduleDetails.get(i).get("module").toString());

				for (int j = 0; j < moduleTcDetails.size(); j++) {
					if (moduleId == Integer.parseInt(moduleTcDetails.get(j).get("module_id").toString())) {
						moduleDetails.get(i).put("TotalTcCount", moduleTcDetails.get(j).get("TotalTcCount"));
						break;
					}
				}
			}
			for (int count = 0; count < moduleDetails.size(); count++) {
				tableText += "<tr style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> "
						+ "<td class='tableClass'>" + moduleDetails.get(count).get("module_name") + "</td> "
						+ "<td class='tableClass'>" + moduleDetails.get(count).get("PassCount") + "</td> "
						+ "<td class='tableClass'>" + moduleDetails.get(count).get("FailCount") + "</td> "
						+ "<td class='tableClass'>" + moduleDetails.get(count).get("SkipCount") + "</td> "
						+ "<td class='tableClass'>" + moduleDetails.get(count).get("ExecutedTcCount") + "</td> </tr> ";

			}

			// String mailBodyText
			// =String.format(mailProperties.getProperty("AUTOMATION_BUILD_EXEC_COMPLETE_BODY_TEXT"),
			// buildDetails.get(0).get("build_name"),buildDetails.get(0).get("build_name"),
			// buildDetails.get(0).get("first_name"),buildDetails.get(0).get("last_name"),buildDetails.get(0).get("hostname"),
			// buildDetails.get(0).get("browserVal"),buildDetails.get(0).get("step_wise_screenshot"),
			// buildDetails.get(0).get("report_bug"),buildDetails.get(0).get("build_name"),tableText);

			String mailBodyText = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'> <html xmlns='http://www.w3.org/1999/xhtml' style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <head style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <meta name='viewport' content='width=device-width' style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <title style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'>Alerts e.g. approaching your limit</title> </head> "
					+ "<style> * { margin: 0; padding: 0; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; } img { max-width: 100%; } body { -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6; } /* Let's make sure all tables have defaults */ table td { vertical-align: top; } /* ------------------------------------- BODY & CONTAINER ------------------------------------- */ body { background-color: #f6f6f6; } .body-wrap { background-color: #f6f6f6; width: 100%; } .container { display: block !important; max-width: 600px !important; margin: 0 auto !important; /* makes it centered */ clear: both !important; } .content { max-width: 600px; margin: 0 auto; display: block; padding: 20px; }"
					+ ".main { background: #fff; border: 1px solid #e9e9e9; border-radius: 3px; } .content-wrap { padding: 20px; } .content-block { padding: 0 0 20px; } .header { width: 100%; margin-bottom: 20px; } .footer { width: 100%; clear: both; color: #999; padding: 20px; } .footer a { color: #999; } .footer p, .footer a, .footer unsubscribe, .footer td { font-size: 12px; } /* ------------------------------------- TYPOGRAPHY ------------------------------------- */ h1, h2, h3 { font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; color: #000; margin: 40px 0 0; line-height: 1.2; font-weight: 400; } h1 { font-size: 32px; font-weight: 500; } h2 { font-size: 24px; } h3 { font-size: 18px; } h4 { font-size: 14px; font-weight: 600; } p, ul, ol { margin-bottom: 10px; font-weight: normal; } p li, ul li, ol li { margin-left: 5px; list-style-position: inside; } "
					+ " .last { margin-bottom: 0; } .first { margin-top: 0; } .aligncenter { text-align: center; } .alignright { text-align: right; } .alignleft { text-align: left; } .clear { clear: both; } .alert { font-size: 16px; color: #fff; font-weight: 500; padding: 20px; text-align: center; border-radius: 3px 3px 0 0; } .alert a { color: #fff; text-decoration: none; font-weight: 500; font-size: 16px; } .alert.alert-warning { background: #f8ac59; } .alert.alert-bad { background: #ed5565; } .alert.alert-good { background: #1c84c6; } "
					+ " .invoice { margin: 40px auto; text-align: left; width: 80%; } .invoice td { padding: 5px 0; } .invoice .invoice-items { width: 100%; } .invoice .invoice-items td { border-top: #eee 1px solid; } .invoice .invoice-items .total td { border-top: 2px solid #333; border-bottom: 2px solid #333; font-weight: 700; } /* ------------------------------------- RESPONSIVE AND MOBILE FRIENDLY STYLES ------------------------------------- */ @media only screen and (max-width: 640px) { h1, h2, h3, h4 { font-weight: 600 !important; margin: 20px 0 5px !important; } h1 { font-size: 22px !important; } h2 { font-size: 18px !important; } h3 { font-size: 16px !important; } .container { width: 100% !important; } .content, .content-wrap { padding: 10px !important; } .invoice { width: 100% !important; } } /* COLORS */ .text-navy { color: #1c84c6; } .text-primary { color: inherit; } .text-success { color: #1c84c6; } .text-info { color: #23c6c8; } .text-warning { color: #f8ac59; } .text-danger { color: #ed5565; } .text-muted { color: #888888; } .text-white { color: #ffffff; } </style> "
					+ "<style> .tableClass { border: 1px solid black; padding: 3px; text-align: center; } </style> <body style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;line-height: 1.6;background-color: #f6f6f6;width: 100% !important;'> <table class='body-wrap' style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;background-color: #f6f6f6;width: 100%;'> <tr style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'>"
					+ " <td style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;'></td> <td class='container' width='600' style='margin: 0 auto !important;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;display: block !important;max-width: 600px !important;clear: both !important;width: 100% !important;'> <div class='content' style='margin: 0 auto;padding: 10px !important;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;max-width: 600px;display: block;'> "
					+ "<table class='main' width='100%' cellpadding='0' cellspacing='0' style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;background: #fff;border: 1px solid #e9e9e9;border-radius: 3px;'>"

					+ " <tr style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <td  style='margin: 0;padding: 20px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 16px;vertical-align: top;color: #fff;font-weight: 500;text-align: center;border-radius: 3px 3px 0 0;background: #1c84c6;'> <center><img src='http://xenon.jadeglobal.com/wp-content/uploads/2015/03/cropped-cropped-xeon-logo-web1.png' width='150' height='65px' alt='Xenon Logo'></center> </td> </tr> "

					+ "<tr style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <td class='content-wrap' style='margin: 0;padding: 10px !important;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;'> <table width='100%' cellpadding='0' cellspacing='0' style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'>"
					+ "<tr style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <td class='content-block' style='margin: 0;padding: 0 0 20px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;padding-left: 5%;'> Build <i style='text-transform: capitalize;margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'>"
					+ "'" + buildDetails.get(0).get("build_name") + "'</i> execution completed successfully. "
					+ "<br style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'><br style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <label style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'><strong style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'>"
					+ "Build Name : </strong></label>" + buildDetails.get(0).get("build_name")
					+ "<br style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <label style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'><strong style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'>"
					+ "Executed By : </strong></label> " + buildDetails.get(0).get("first_name") + " "
					+ buildDetails.get(0).get("last_name")
					+ "<br style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <label style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'><strong style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'>"
					+ "Virtual Machine Name : </strong> </label>" + buildDetails.get(0).get("hostname")
					+ "<br style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <label style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'><strong style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'>"
					+ "Browser : </strong> " + buildDetails.get(0).get("browserVal")
					+ "</label><br style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <label style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'><strong style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'>"
					+ "Stepwise Screenshot : </strong></label> " + buildDetails.get(0).get("step_wise_screenshot")
					+ "<br style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <label style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'><strong style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'>"
					+ "Raise Bug : </strong></label> " + buildDetails.get(0).get("report_bug")
					+ "<br style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> </td> </tr> <tr style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <td class='content-block' style='margin: 0;padding: 0 0 20px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;'> <center style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <b style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'>"
					+ "Build '" + buildDetails.get(0).get("build_name") + "' Summary</b> </center> </td> </tr> "
					+ "<tr style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> "
					+ "<td class='content-block' style='margin: 0;padding: 0 0 20px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;'> <center style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> "
					+ "<table cellpadding='10' class='tableClass' style='margin: 0;padding: 3px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;border: 1px solid black;text-align: center;'> <thead style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> </thead><tbody style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <tr style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> "
					+ "<th class='tableClass' style='margin: 0;padding: 3px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;border: 1px solid black;text-align: center;'>Module Name</th> "
					+ "<th class='tableClass' style='margin: 0;padding: 3px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;border: 1px solid black;text-align: center;'>Pass</th> "
					+ "<th class='tableClass' style='margin: 0;padding: 3px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;border: 1px solid black;text-align: center;'>Fail</th> "
					+ "<th class='tableClass' style='margin: 0;padding: 3px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;border: 1px solid black;text-align: center;'>Skip</th>"
					+ "<th class='tableClass' style='margin: 0;padding: 3px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;border: 1px solid black;text-align: center;'>Total</th> </tr> </tbody>"
					+ "<tbody style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> "
					+ tableText
					+ "</tbody> </table></center> </td> </tr> <!--<tr style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <td class='content-block' style='margin: 0;padding: 0 0 20px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;'> <center style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> "
					+ "<a href='#' class='btn-primary' style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;color: #FFF;text-decoration: none;background-color: #1c84c6;border: solid #1c84c6;border-width: 5px 10px;line-height: 2;font-weight: bold;text-align: center;cursor: pointer;display: inline-block;border-radius: 5px;text-transform: capitalize;'>View details</a></center> </td> </tr>--> </table> </td> </tr> </table> <div class='footer' style='margin: 0;padding: 20px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;width: 100%;clear: both;color: #999;'> <table width='100%' style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> "
					+ "<tr style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> <td class='aligncenter content-block' style='margin: 0;padding: 0 0 20px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 12px;vertical-align: top;text-align: center;'><a href='#' style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 12px;color: #999;text-decoration: underline;'>Copyright@2020 Jade Global Inc. All rights reserved.</a></td> </tr> </table> </div></div> </td> <td style='margin: 0;padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;'></td> </tr> </table> </body> </html>";

			String from = emailDetails.get(0).get("smtp_user_id").toString();
			String message = mailService.postMail(emailDetails, recepients, mailSubject, mailBodyText, from);
			return message;
		} else {
			logger.info("Send mail status is inactive");
			return "Send mail status is inactive";
		}
	}

	@Override
	public String sendMailOnSatrtOfAutoBuildExe(int buildId, String schema) throws Exception {
		logger.info("Sending mail on build execution start");
		Map<String, Object> buildExecDetails = buildExecutionDetailsMail(buildId, schema);
		List<Map<String, Object>> buildDetails = (List<Map<String, Object>>) buildExecDetails.get("#result-set-1");
		List<Map<String, Object>> emailDetails = (List<Map<String, Object>>) buildExecDetails.get("#result-set-2");
		List<Map<String, Object>> emailNotificationDetails = (List<Map<String, Object>>) buildExecDetails
				.get("#result-set-3");
		if (emailDetails.size() == 0) {
			logger.info("Mail details not available");
			return "Mail details not available";
		}
		int sendStatus = Integer.parseInt(emailNotificationDetails.get(0).get("build_exec_complete").toString());
		if (sendStatus == 1) {
			List<Map<String, Object>> userDetails = (List<Map<String, Object>>) buildExecDetails.get("#result-set-4");
			ArrayList<String> recepients = new ArrayList<String>();
			for (int i = 0; i < userDetails.size(); i++) {
				recepients.add(userDetails.get(i).get("email_id").toString());
			}

			if (buildDetails.get(0).get("browser").toString().equalsIgnoreCase("1"))
				buildDetails.get(0).put("browserVal", "IE");
			else if (buildDetails.get(0).get("browser").toString().equalsIgnoreCase("2"))
				buildDetails.get(0).put("browserVal", "Chrome");
			else if (buildDetails.get(0).get("browser").toString().equalsIgnoreCase("3"))
				buildDetails.get(0).put("browserVal", "Firefox");
			else if (buildDetails.get(0).get("browser").toString().equalsIgnoreCase("4"))
				buildDetails.get(0).put("browserVal", "Safari");

			if (buildDetails.get(0).get("step_wise_screenshot").toString().equalsIgnoreCase("1"))
				buildDetails.get(0).put("step_wise_screenshot", "All Cases");
			else if (buildDetails.get(0).get("step_wise_screenshot").toString().equalsIgnoreCase("2"))
				buildDetails.get(0).put("step_wise_screenshot", "Failed Cases");
			else if (buildDetails.get(0).get("step_wise_screenshot").toString().equalsIgnoreCase("3"))
				buildDetails.get(0).put("step_wise_screenshot", "Disable");

			if (buildDetails.get(0).get("report_bug").toString().equalsIgnoreCase("1"))
				buildDetails.get(0).put("report_bug", "Active");
			else if (buildDetails.get(0).get("report_bug").toString().equalsIgnoreCase("2"))
				buildDetails.get(0).put("report_bug", "Inactive");
			List<Map<String, Object>> testCaseCounts = getAutoModuleWiseTCCount(buildId, schema);
			String mailSubject = mailProperties.getProperty("START_AUTO_BUILD_EXEC_SUBJECT", "Build Execution Started");
			String mailBodyText = mailProperties.getProperty("START_AUTO_BUILD_EXEC_BODY_TEXT", "None");
			String tr = "";
			for (int i = 0; i < testCaseCounts.size(); i++) {
				tr = tr + "<tr><td class='tableClass'>" + testCaseCounts.get(i).get("module_name")
						+ "</td><td class='tableClass'>" + testCaseCounts.get(i).get("test_case_count") + "</td></tr>";
			}

			String tBody = tr;
			// System.out.println("mailBodyText "+mailBodyText);

			mailBodyText = String.format(mailBodyText, buildDetails.get(0).get("build_name"),
					buildDetails.get(0).get("first_name") + " " + buildDetails.get(0).get("last_name"),
					buildDetails.get(0).get("hostname"), buildDetails.get(0).get("browserVal"),
					buildDetails.get(0).get("step_wise_screenshot"), buildDetails.get(0).get("report_bug"), tBody);

			String from = emailDetails.get(0).get("smtp_user_id").toString();
			String message = mailService.postMail(emailDetails, recepients, mailSubject, mailBodyText, from);
			return message;
		} else {
			logger.info("Send mail status is inactive");
			return "Send mail status is inactive";
		}

	}

	@Override
	public List<Map<String, Object>> getAutoModuleWiseTCCount(int buildId, String schema) {
		logger.info("Query to get Auto Module Wise TC Count");
		String sql = String.format(build.getAutoModuleWiseTCCount, schema, schema, schema);
		List<Map<String, Object>> tcCounts = database.select(sql, new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Query Result  " + tcCounts);
		return tcCounts;
	}

	@Override
	public List<Map<String, Object>> getBuildDetailsByTimestampAndId(int build_id, String buildStamp) {
		logger.info("Reading build details by build time stamp and build id");
		logger.info("Query param build_id ={} , build_stamp = {}", build_id, buildStamp);
		String sql = String.format("SELECT * FROM `xenon-core`.xebm_execution where build_id=? and build_stamp=?");
		List<Map<String, Object>> allBuildDetails = database.select(sql, new Object[] { build_id, buildStamp },
				new int[] { Types.INTEGER, Types.VARCHAR });
		logger.info("Return list =" + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public int insertcstarttime(int build, String testcasePrefix, String schema_name) throws Exception {
		String insertExecutuionStep = "CALL `%s`.`insertTestCaseDetails`(?,?)";
		int status = database.update(String.format(insertExecutuionStep, schema_name),
				new Object[] { build, testcasePrefix }, new int[] { Types.INTEGER, Types.VARCHAR });

		return status;
	}

	@Override
	public int insertRedwoodTCStartTime(int build, String redwoodTCId, String schemaName) {
		String insertExecutuionStep = "CALL `%s`.`insertTestCaseDetailsRedwood`(?,?)";
		int status = database.update(String.format(insertExecutuionStep, schemaName),
				new Object[] { build, redwoodTCId }, new int[] { Types.INTEGER, Types.VARCHAR });

		return status;
	}

	@Override
	public Map<String, Object> updateAutomationBuildStatus(int buildId, String schema, String buildTimeStamp) {
		logger.info("Updating automation build status");
		logger.info("Query param build_id ={} , build_stamp = {}", buildId, buildTimeStamp);
		SqlParameter build = new SqlParameter("buildId", Types.INTEGER);
		SqlParameter buildStamp = new SqlParameter("buildStamp", Types.VARCHAR);
		SqlParameter[] paramArray = { build, buildStamp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("updateAutomationBuildStatus").declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(buildId, buildTimeStamp);

		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> dataForMailOnCompleteBuild(int buildId, String schema) {
		logger.info("Calling Stored procedure to complete manual build execution");
		SqlParameter build = new SqlParameter("buildId", Types.INTEGER);
		SqlParameter[] paramArray = { build };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("completemanualbuild")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(buildId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public String sendMailOnManualBuildExecutionComplete(int buildId, String schema) throws Exception {

		logger.info("Reading build details for manual build");
		Map<String, Object> buildExecDetails = manualBuildExecutionDetails(buildId, schema);
		List<Map<String, Object>> moduleTcDetails = (List<Map<String, Object>>) buildExecDetails.get("#result-set-1");
		List<Map<String, Object>> moduleDetails = (List<Map<String, Object>>) buildExecDetails.get("#result-set-2");
		List<Map<String, Object>> buildDetails = (List<Map<String, Object>>) buildExecDetails.get("#result-set-5");
		List<Map<String, Object>> userDetails = (List<Map<String, Object>>) buildExecDetails.get("#result-set-6");
		List<Map<String, Object>> emailDetails = (List<Map<String, Object>>) buildExecDetails.get("#result-set-3");
		List<Map<String, Object>> emailNotificationDetails = (List<Map<String, Object>>) buildExecDetails
				.get("#result-set-4");

		if (emailDetails.size() == 0) {
			logger.info("Mail details not available");
			return "Mail details not available";
		}
		int sendStatus = Integer.parseInt(emailNotificationDetails.get(0).get("build_exec_complete").toString());
		if (sendStatus == 1) {
			logger.info("Reading user mail id");
			// List<Map<String, Object>> userDetails =(List<Map<String, Object>>)
			// buildExecDetails.get("#result-set-6");
			ArrayList<String> recepients = new ArrayList<String>();
			for (int i = 0; i < userDetails.size(); i++) {
				recepients.add(userDetails.get(i).get("email_id").toString());
			}

			logger.info("Reading mail subject");
			String mailSubject = mailProperties.getProperty("MANUAL_BUILD_EXEC_COMPLETE_SUBJECT");
			String tableText = "";

			int totalPassCount = 0, totalFailCount = 0, totalSkipCount = 0, totalBlockCount = 0, totalNotRunCount = 0,
					notRunCount = 0, totalCount = 0;
			for (int i = 0; i < moduleDetails.size(); i++) {

				int moduleId = Integer.parseInt(moduleDetails.get(i).get("mudule_id").toString());

				for (int j = 0; j < moduleTcDetails.size(); j++) {
					if (moduleId == Integer.parseInt(moduleTcDetails.get(j).get("module_id").toString())) {
						moduleDetails.get(i).put("TotalTcCount", moduleTcDetails.get(j).get("TotalTcCount"));
						break;
					}
				}
			}

			logger.info("Creating table data");

			for (int count = 0; count < moduleDetails.size(); count++) {
				notRunCount = 0;
				notRunCount = (Integer.parseInt(moduleDetails.get(count).get("ExecutedTcCount").toString())
						- (Integer.parseInt(moduleDetails.get(count).get("PassCount").toString())
								+ Integer.parseInt(moduleDetails.get(count).get("FailCount").toString())
								+ Integer.parseInt(moduleDetails.get(count).get("SkipCount").toString())
								+ Integer.parseInt(moduleDetails.get(count).get("BlockCount").toString())));
				tableText += "<tr>" + "<td class='tableClass'>" + moduleDetails.get(count).get("project_name")
						+ "</td> " + "<td class='tableClass'>" + moduleDetails.get(count).get("module_name") + "</td> "
						+ "<td class='tableClass'>" + moduleDetails.get(count).get("PassCount") + "</td> "
						+ "<td class='tableClass'>" + moduleDetails.get(count).get("FailCount") + "</td> "
						+ "<td class='tableClass'>" + moduleDetails.get(count).get("SkipCount") + "</td> "
						+ "<td class='tableClass'>" + moduleDetails.get(count).get("BlockCount") + "</td> "
						+ "<td class='tableClass'>" + notRunCount + "</td> " + "<td class='tableClass'>"
						+ moduleDetails.get(count).get("ExecutedTcCount") + "</td> </tr> ";
				totalPassCount += Integer.parseInt(moduleDetails.get(count).get("PassCount").toString());
				totalFailCount += Integer.parseInt(moduleDetails.get(count).get("FailCount").toString());
				totalSkipCount += Integer.parseInt(moduleDetails.get(count).get("SkipCount").toString());
				totalBlockCount += Integer.parseInt(moduleDetails.get(count).get("BlockCount").toString());
				totalNotRunCount += notRunCount;
				totalCount += Integer.parseInt(moduleDetails.get(count).get("ExecutedTcCount").toString());
			}
			tableText += "<tr>" + "<td class='tableClass'></td> " + "<td class='tableClass'>Total</td> "
					+ "<td class='tableClass'>" + totalPassCount + "</td> " + "<td class='tableClass'>" + totalFailCount
					+ "</td> " + "<td class='tableClass'>" + totalSkipCount + "</td> " + "<td class='tableClass'>"
					+ totalBlockCount + "</td> " + "<td class='tableClass'>" + totalNotRunCount + "</td> "
					+ "<td class='tableClass'>" + totalCount + "</td> </tr> ";
			logger.info("Creating chart");
			String chartName = utilsService.createPieChart(totalPassCount, totalFailCount, totalSkipCount,
					totalBlockCount, totalNotRunCount, configurationProperties.getProperty("XENON_REPOSITORY_PATH"));

			logger.info("Reading body text");
			String mailBodyText = String.format(mailProperties.getProperty("MANUAL_BUILD_EXEC_COMPLETE_BODY_TEXT"),
					buildDetails.get(0).get("build_name"),
					userDetails.get(0).get("first_name") + " " + userDetails.get(0).get("last_name"), totalCount,
					tableText);

			String from = emailDetails.get(0).get("smtp_user_id").toString();
			logger.info("Sending Mail");
			String message = mailService.postReportMail(emailDetails, recepients, mailSubject, mailBodyText, from,
					chartName, configurationProperties.getProperty("XENON_REPOSITORY_PATH"));
			logger.info("Return status = " + message);
			return message;
		} else {
			logger.info("Send mail status is inactive");
			return "Send mail status is inactive";
		}
	}

	@Override
	public Map<String, Object> manualBuildExecutionDetails(int buildId, String schema) {
		logger.info("Calling Stored procedure to read manual build Execution Details");
		SqlParameter build = new SqlParameter("buildId", Types.INTEGER);
		SqlParameter[] paramArray = { build };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("manualBuildExecutionDetails").declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(buildId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> insertTestcaseSummary(int tcId, int buildId, int methodType, int testerId, int statusId,
			String notesVal, int completeStatus, String userName, int userId, String schema) {
		logger.info("Calling Stored procedure to insert testcase summary");
		SqlParameter tcIdTemp = new SqlParameter("tcId", Types.INTEGER);
		SqlParameter build = new SqlParameter("buildId", Types.INTEGER);
		SqlParameter methodTypeTemp = new SqlParameter("methodType", Types.INTEGER);
		SqlParameter testerIdTemp = new SqlParameter("testerId", Types.INTEGER);
		SqlParameter statusIdTemp = new SqlParameter("statusId", Types.INTEGER);
		SqlParameter notes = new SqlParameter("notesVal", Types.VARCHAR);
		SqlParameter completeStatusTemp = new SqlParameter("completeStatus", Types.INTEGER);
		SqlParameter userNameTemp = new SqlParameter("userName", Types.VARCHAR);
		SqlParameter userIdTemp = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = { tcIdTemp, build, methodTypeTemp, testerIdTemp, statusIdTemp, notes,
				completeStatusTemp, userNameTemp, userIdTemp };
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("insertTestcaseSummary")
				.declareParameters(paramArray).withCatalogName(schema);
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(tcId, buildId, methodType, testerId, statusId,
				notesVal, completeStatus, userName, userId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public int insertAttachmentOnTcExecution(int tcId, int buildId, byte[] bytes, String title, String schema) {

		logger.info("SQL Query to insert test case attachment on build execution");
		int status = database.update(String.format(build.insertTcAttachmentOnExecution, schema),
				new Object[] { tcId, buildId, bytes, title },
				new int[] { Types.INTEGER, Types.INTEGER, Types.BLOB, Types.VARCHAR });
		logger.info("SQL Result Status is " + status);
		return status;
	}

	@Override
	public List<Map<String, Object>> gettcAttachmentById(int attchmentId, String schema) {
		logger.info("SQL Query to get test case attachment details");
		List<Map<String, Object>> tcAttachDetails = database.select(String.format(build.getTcAttachmentByID, schema),
				new Object[] { attchmentId }, new int[] { Types.INTEGER });
		logger.info("SQL : " + build.getTcAttachmentByID + " Values : attchmentId = " + attchmentId);
		logger.info("SQL Result " + tcAttachDetails);
		return tcAttachDetails;

	}

	@Override
	public Map<String, Object> getCloneBuildData(String schema) {
		logger.info("Calling Stored procedure to get clone Build data");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getCloneBuildData")
				.declareParameters().withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> recentManualBuilds(String schema) {
		logger.info("Calling Stored procedure to get recent manual builds ");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("recentManualBuilds")
				.withCatalogName(schema);
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public List<Map<String, Object>> getTopRecentBuilds(List<Map<String, Object>> allBuilds,
			List<Map<String, Object>> allBuildsTestcasesCount,
			List<Map<String, Object>> allBuildsExecutedTestcasesCount) {
		logger.info("Adding the complition percentage to each recent build");
		for (int i = 0; i < allBuilds.size(); i++) {
			if (allBuildsTestcasesCount.size() > 0) {
				for (int j = 0; j < allBuildsTestcasesCount.size(); j++) {
					if (Integer.parseInt(allBuildsTestcasesCount.get(j).get("build_id").toString()) == Integer
							.parseInt(allBuilds.get(i).get("build_id").toString())) {
						allBuilds.get(i).put("TotalTcCount", allBuildsTestcasesCount.get(j).get("TotalTcCount"));
						break;
					} else {
						allBuilds.get(i).put("TotalTcCount", 0);
					}
				}
			} else
				allBuilds.get(i).put("TotalTcCount", 0);

		}

		for (int i = 0; i < allBuilds.size(); i++) {

			if (allBuildsExecutedTestcasesCount.size() > 0) {
				for (int k = 0; k < allBuildsExecutedTestcasesCount.size(); k++) {
					if (Integer.parseInt(allBuildsExecutedTestcasesCount.get(k).get("build_id").toString()) == Integer
							.parseInt(allBuilds.get(i).get("build_id").toString())) {
						allBuilds.get(i).put("ExecutedTcCount",
								allBuildsExecutedTestcasesCount.get(k).get("ExecutedTcCount"));
						break;
					} else {
						allBuilds.get(i).put("ExecutedTcCount", 0);
					}
				}
			} else
				allBuilds.get(i).put("ExecutedTcCount", 0);
		}

		for (int i = 0; i < allBuilds.size(); i++) {
			if (Integer.parseInt(allBuilds.get(i).get("ExecutedTcCount").toString()) == 0
					|| Integer.parseInt(allBuilds.get(i).get("TotalTcCount").toString()) == 0)
				allBuilds.get(i).put("percentCompleted", 0);
			else
				allBuilds.get(i).put("percentCompleted",
						String.format("%.2f", (Float.valueOf(allBuilds.get(i).get("ExecutedTcCount").toString())
								/ Float.valueOf(allBuilds.get(i).get("TotalTcCount").toString()) * 100)));
		}
		logger.info("Result  " + allBuilds);
		return allBuilds;
	}

	@Override
	public Map<String, Object> recentAutomationBuilds(String schema) {
		logger.info("Calling Stored procedure to get recent automation builds ");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("recentAutomationBuilds")
				.withCatalogName(schema);
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> updateBuildLearningAndStatus(int buildID, String buildName, String buildLearning,
			int userID, String userName, String schema) {
		logger.info("Calling Stored procedure to update build state to complete and entering the build learning");
		SqlParameter bldID = new SqlParameter("buildID", Types.INTEGER);
		SqlParameter bldName = new SqlParameter("buildName", Types.VARCHAR);
		SqlParameter bldLearning = new SqlParameter("buildLearning", Types.VARCHAR);
		SqlParameter usrID = new SqlParameter("userID", Types.INTEGER);
		SqlParameter usrName = new SqlParameter("userName", Types.VARCHAR);

		SqlParameter[] paramArray = { bldID, bldName, bldLearning, usrID, usrName };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("insertBuildLearning")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(buildID, buildName, buildLearning, userID,
				userName, schema);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);

		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> insertCloneBuild(int cloneBuildId, String buildName, String buildDescription,
			int buildType, int releaseId, int buildCreator, int buildStatus, String schema, String currentTime) {

		logger.info("Calling Stored procedure to insert testcase summary");

		SqlParameter cloneBuildIdTemp = new SqlParameter("cloneBuildId", Types.INTEGER);
		SqlParameter buildNameTemp = new SqlParameter("buildName", Types.VARCHAR);
		SqlParameter buildDescriptionTemp = new SqlParameter("buildDescription", Types.VARCHAR);
		SqlParameter buildTypeTemp = new SqlParameter("buildType", Types.INTEGER);
		SqlParameter releaseIdTemp = new SqlParameter("releaseId", Types.INTEGER);
		SqlParameter buildCreatorTemp = new SqlParameter("buildCreator", Types.INTEGER);
		SqlParameter buildStatusTemp = new SqlParameter("buildStatus", Types.VARCHAR);
		SqlParameter currentDate = new SqlParameter("currentdate", Types.VARCHAR);
		SqlParameter[] paramArray = { cloneBuildIdTemp, buildNameTemp, buildDescriptionTemp, buildTypeTemp,
				releaseIdTemp, buildCreatorTemp, buildStatusTemp, currentDate };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("insertCloneBuild")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(cloneBuildId, buildName, buildDescription,
				buildType, releaseId, buildCreator, buildStatus, currentTime);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;

	}

	@Override
	public List<Map<String, Object>> getAllBuildsNf(String schema) {
		logger.info("Query to get All Builds ");
		List<Map<String, Object>> countBuildDetails = database
				.select(String.format(build.getAllBuilds, schema, schema, schema));
		logger.info("Query Result  " + countBuildDetails);
		return countBuildDetails;
	}

	@Override
	public List<Map<String, Object>> getBmDashboardTopBuildsNf(List<Map<String, Object>> allBuilds,
			List<Map<String, Object>> allBuildsTestcasesCount1,
			List<Map<String, Object>> allBuildsExecutedTestcasesCount1, String schema) {
		logger.info("Query to get Bm Dashboard Top Builds ");
		// List<Map<String, Object>> allBuilds = getAllBuildsNf(schema);
		List<Map<String, Object>> allBuildsTestcasesCount = allBuildsTestcasesCount1;
		List<Map<String, Object>> allBuildsExecutedTestcasesCount = allBuildsExecutedTestcasesCount1;

		for (int i = 0; i < allBuilds.size(); i++) {
			if (allBuildsTestcasesCount.size() > 0) {
				for (int j = 0; j < allBuildsTestcasesCount.size(); j++) {
					if (Integer.parseInt(allBuildsTestcasesCount.get(j).get("build_id").toString()) == Integer
							.parseInt(allBuilds.get(i).get("build_id").toString())) {
						allBuilds.get(i).put("TotalTcCount", allBuildsTestcasesCount.get(j).get("TotalTcCount"));
						break;
					} else {
						allBuilds.get(i).put("TotalTcCount", 0);
					}
				}
			} else
				allBuilds.get(i).put("TotalTcCount", 0);

		}

		for (int i = 0; i < allBuilds.size(); i++) {

			if (allBuildsExecutedTestcasesCount.size() > 0) {
				for (int k = 0; k < allBuildsExecutedTestcasesCount.size(); k++) {
					if (Integer.parseInt(allBuildsExecutedTestcasesCount.get(k).get("build_id").toString()) == Integer
							.parseInt(allBuilds.get(i).get("build_id").toString())) {
						allBuilds.get(i).put("ExecutedTcCount",
								allBuildsExecutedTestcasesCount.get(k).get("ExecutedTcCount"));
						break;
					} else {
						allBuilds.get(i).put("ExecutedTcCount", 0);
					}
				}
			} else
				allBuilds.get(i).put("ExecutedTcCount", 0);
		}

		for (int i = 0; i < allBuilds.size(); i++) {
			if (Integer.parseInt(allBuilds.get(i).get("ExecutedTcCount").toString()) == 0
					|| Integer.parseInt(allBuilds.get(i).get("TotalTcCount").toString()) == 0)
				allBuilds.get(i).put("percentCompleted", 0);
			else
				allBuilds.get(i).put("percentCompleted",
						String.format("%.2f", (Float.valueOf(allBuilds.get(i).get("ExecutedTcCount").toString())
								/ Float.valueOf(allBuilds.get(i).get("TotalTcCount").toString()) * 100)));
		}
		logger.info("Query Result  " + allBuilds);
		return allBuilds;

	}

	@Override
	public Map<String, Object> getBuildReportDataByBuildId(int buildId, int userId, String schema) {
		logger.info("Calling Stored procedure to get build report data");
		SqlParameter tempBuildId = new SqlParameter("buildId", Types.INTEGER);
		SqlParameter tempUserId = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = { tempBuildId, tempUserId };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBuildReportByBuildId")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(buildId, userId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public List<Map<String, Object>> getModuleWiseAssignee(int buildId, String schema) {
		logger.info("Query to check Assigned Modules ");
		List<Map<String, Object>> allBuildDetails = database.select(
				String.format(build.getModuleWiseAssignee, schema, schema), new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Query Result  " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public Map<String, Object> getBuildVmDetails(int buildId, int userId, String schema) {
		logger.info("Calling Stored procedure to get build report data");
		SqlParameter tempBuildId = new SqlParameter("buildId", Types.INTEGER);
		SqlParameter tempUserId = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = { tempBuildId, tempUserId };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBuildVmDetails")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(buildId, userId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public List<Map<String, Object>> getAllProjectBugCount(int userId, String schema) {
		logger.info("SQL Query to get list of allbugs which are from assigned and active module");
		String sql = String.format(build.getAllprojectbug, schema, schema, schema, schema, schema, schema);
		List<Map<String, Object>> allBugDetails = database.select(sql, new Object[] { userId },
				new int[] { Types.INTEGER });
		logger.info("SQL : " + bug.allBugDetails + " Values : user_id = " + userId);
		logger.info("SQL Result " + allBugDetails);
		return allBugDetails;
	}

	@Override
	public int updateScriptlessBuildStatus(int buildStatus, int buildId, String schema) {
		String sql = String.format(build.setScriptlessBuildStatus, schema);
		logger.info("Query to update Build Status " + sql);
		int flag = database.update(sql, new Object[] { buildStatus, buildId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result  " + flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> getScriptBuildAddedTcCount(int buildId, String schema) {
		logger.info("Query to get Build Added Tc Count");
		List<Map<String, Object>> getBuildProjectData = database.select(
				String.format(build.getScriptBuildAddedTcCount, schema), new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Query Result  " + getBuildProjectData);
		return getBuildProjectData;
	}

	@Override
	public Map<String, Object> getSfdcExecuteBuildData(int custId, int buildId, String schema) {
		logger.info("Calling Stored procedure to get Automation Build Data");
		SqlParameter customerId = new SqlParameter("custId", Types.VARCHAR);
		SqlParameter buildIdTemp = new SqlParameter("buildIdTemp", Types.VARCHAR);
		SqlParameter[] paramArray = { customerId, buildIdTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("sfdcExecuteBuildData")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(custId, buildId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;

	}

	@Override
	public List<Map<String, Object>> getScriptExecutedBuildDetails(String schema) {
		logger.info("SQL Query to get Automation build details for report");
		String sql = String.format(build.getScriptExecutedBuildDetails, schema, schema, schema, schema, schema);
		logger.info("SQL : " + sql);
		List<Map<String, Object>> allBuildDetails = database.select(sql);
		logger.info("SQL Result " + allBuildDetails);
		return allBuildDetails;
	}

	@Override
	public List<Map<String, Object>> getManualBuildAddedTcCount(int buildId, int userId, String schema) {
		logger.info("Query to get Build Added Tc Count");
		List<Map<String, Object>> getBuildProjectData = database.select(
				String.format(build.getManualBuildAddedTcCount, schema, schema), new Object[] { buildId },
				new int[] { Types.INTEGER });
		logger.info("Query Result  " + getBuildProjectData);
		return getBuildProjectData;
	}

	@Override
	public int checkManualbuildname(int relId, String buildName, String schema) {
		String sql = String.format(build.checkManualbuildname, schema);
		List<Map<String, Object>> data = database.select(sql, new Object[] { buildName, relId },
				new int[] { Types.VARCHAR, Types.INTEGER });

		int count = Integer.parseInt(data.get(0).get("count").toString());
		logger.info("Query Result  " + count);
		return count;
	}

	@Override
	public int checkAutobuildname(int relId, String buildName, String schema) {
		String sql = String.format(build.checkAutobuildname, schema);
		List<Map<String, Object>> data = database.select(sql, new Object[] { buildName, relId },
				new int[] { Types.VARCHAR, Types.INTEGER });

		int count = Integer.parseInt(data.get(0).get("count").toString());
		logger.info("Query Result  " + count);
		return count;
	}

	@Override
	public Map<String, Object> getBuildReportData(int automationStatus, int scriptlessStatus, int manualStartValue,
			int autoStartValue, int scStartValue, int pageSize, String schema) {
		logger.info("Calling Stored procedure to get View Build Data");
		SqlParameter autoStatus = new SqlParameter("automationStatus", Types.INTEGER);
		SqlParameter scriptStatus = new SqlParameter("scriptlessStatus", Types.INTEGER);
		SqlParameter manualSValue = new SqlParameter("manualStartValue", Types.INTEGER);
		SqlParameter autoSValue = new SqlParameter("autoStartValue", Types.INTEGER);
		SqlParameter scSValue = new SqlParameter("scStartValue", Types.INTEGER);
		SqlParameter pgSize = new SqlParameter("pageSize", Types.INTEGER);
		SqlParameter[] paramArray = { autoStatus, scriptStatus, manualSValue, autoSValue, scSValue, pgSize };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("buildReportData")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(automationStatus, scriptlessStatus,
				manualStartValue, autoStartValue, scStartValue, pageSize);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public boolean checkBMRoleAccess(String roleAccess, String roleId, String schema) {
		boolean flag = false;
		logger.info("Calling Stored procedure to get build report data");
		SqlParameter roleID = new SqlParameter("roleId", Types.VARCHAR);
		SqlParameter access = new SqlParameter("roleAccess", Types.VARCHAR);
		SqlParameter[] paramArray = { access, roleID };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("checkBMAccess")
				.declareParameters(paramArray).withCatalogName(schema);
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(roleAccess, roleId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		List<Map<String, Object>> returnList = (List<Map<String, Object>>) simpleJdbcCallResult.get("#result-set-1");
		if (!returnList.isEmpty()) {
			flag = returnList.get(0).containsValue(1);
		}
		return flag;
	}

	@Override
	public int uploadDatasheet(BuildTestCases buildTc, String schema) {
		String sql = String.format(build.uploadDatasheet, schema);
		int status = database
				.update(sql,
						new Object[] { buildTc.getDatasheet(), buildTc.getDatasheetTitle(), buildTc.getTestCaseid(),
								buildTc.getBuildId() },
						new int[] { Types.BLOB, Types.VARCHAR, Types.INTEGER, Types.INTEGER });

		logger.info("Query Result  " + status);
		return status;
	}

	@Override
	public int uploadDatafile(int buildId, int userId, byte[] datasheet, String title, String schema) {
		String sql = String.format(build.uploadDatafile, schema);
		int status = database.update(sql, new Object[] { buildId, userId, datasheet, title },
				new int[] { Types.INTEGER, Types.INTEGER, Types.BLOB, Types.VARCHAR });

		logger.info("Query Result  " + status);
		return status;
	}

	@Override
	public int updateDataFile(int buildId, int userId, byte[] datasheet, String title, String schema) {
		String sql = String.format(build.updateDatafile, schema);
		int status = database.update(sql, new Object[] { userId, datasheet, title, buildId },
				new int[] { Types.INTEGER, Types.BLOB, Types.VARCHAR, Types.INTEGER });

		logger.info("Query Result  " + status);
		return status;
	}

	@Override
	public List<Map<String, Object>> validateTestCasesForBuild(Integer buildId, String schema) {
		String sql = String.format(build.validateAllTestcases, schema, schema);
		List<Map<String, Object>> data = database.select(sql, new Object[] { buildId }, new int[] { Types.INTEGER });
		return data;
	}

	@Override
	public int deleteBuild(int buildId, String schema) {
		String sql = String.format(build.deleteBuild, schema);
		int flag = database.update(sql, new Object[] { buildId }, new int[] { Types.INTEGER });
		logger.info("Query result = " + flag);
		return flag;
	}

	// author Shailesh.khot
	@Override
	public List<Map<String, Object>> getlastBuildDetails(int userId, String schema) {
		String sql = String.format(build.getLastBuildDetails, schema);
		List<Map<String, Object>> data = database.select(sql, new Object[] { userId }, new int[] { Types.INTEGER });
		return data;
	}

	@Override
	public List<Map<String, Object>> getlastAutoBuildDetails(int userId, String schema) {
		String sql = "select X.build_id,X.build_name,X.build_status,X.execution_type,X.release_id,X.redwood_build_id from "
				+ schema + ".xe_automation_build  as X , " + schema
				+ ".xe_release as Y where X.release_id=Y.release_id and X.build_status=1 and Y.release_active=1 order by X.build_updatedate desc limit 1";
		List<Map<String, Object>> data = database.select(sql);
		return data;
	}

	@Override
	public List<Map<String, Object>> getReleaseCount(String schema) {
		String sql = String.format(build.getReleaseCount, schema);
		List<Map<String, Object>> data = database.select(sql);
		return data;
	}

	@Override
	public Map<String, Object> getReleasesAndBuildsManual(String schema) {
		logger.info("Calling Stored procedure to get All Releases And Manual Builds");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getReleasesAndBuildsManual")
				.declareParameters().withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getReleasesAndBuildsAutomation(String schema) {
		logger.info("Calling Stored procedure to get All Releases And Automation Builds");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("getReleasesAndBuildsAutomation").declareParameters().withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	// @author shailesh.khot
	@Override
	public List<Map<String, Object>> getAutoBuildState(int buildId, String schema) {
		String sql = String.format(build.getAutoBuildState, schema, schema);
		List<Map<String, Object>> data = database.select(sql, new Object[] { buildId }, new int[] { Types.INTEGER });
		return data;
	}

	@Override
	public List<Map<String, Object>> getTestcaseData(int tid, String schema) {
		String sql = String.format(build.getTestcaseDetails, schema, schema);
		List<Map<String, Object>> data = database.select(sql, new Object[] { tid }, new int[] { Types.INTEGER });
		return data;
	}

	@Override
	public List<Map<String, Object>> getReleaseTestDataCountAutomation(int userId, String schema) {
		logger.info("Query to get Build bug Data ");
		List<Map<String, Object>> releaseTestDataCountAutomation = database.select(
				String.format(build.getReleaseTestDataCountAutomation, schema, schema, schema, schema, schema),
				new Object[] { userId }, new int[] { Types.INTEGER });
		logger.info("Query Result  " + releaseTestDataCountAutomation);
		return releaseTestDataCountAutomation;
	}

	@Override
	public List<Map<String, Object>> getReleaseTestDataCountManual(int userId, String schema) {
		logger.info("Query to get Build bug Data ");
		List<Map<String, Object>> releaseTestDataCountManual = database.select(
				String.format(build.getReleaseTestDataCountManual, schema, schema, schema, schema, schema),
				new Object[] { userId }, new int[] { Types.INTEGER });
		logger.info("Query Result  " + releaseTestDataCountManual);
		return releaseTestDataCountManual;
	}

	public List<Map<String, Object>> getTotalReleaseTestData(int userId, String schema) {
		logger.info("Query to get Total release Test set Data ");
		String query = "(SELECT SUM(case when testcase_status in (1,2,3,4) then 1 else 0 end) as  ExecutedTcCount, "
				+ " SUM(case when testcase_status = 1 then 1 else 0 end) as PassCount, "
				+ " SUM(case when testcase_status = 2 then 1 else 0 end) as FailCount,SUM(case when testcase_status = 3 then 1 else 0 end) as SkipCount, "
				+ " SUM(case when testcase_status = 4 then 1 else 0 end) as BlockCount,xb.build_id,xb.build_name,xr.release_id,xr.release_name ,'Automation' as type "
				+ " FROM " + schema + ".xebm_automation_execution_summary as trialSummary, " + schema
				+ ".xe_project as project, " + schema + ".xe_automation_build as xb, " + schema + ".xe_release as xr "
				+ " where trialSummary.project=project.project_id and trialSummary.build=xb.build_id and xb.build_status=1 "
				+ " and xr.release_active=1 and xr.release_id=xb.release_id "
				+ " and trialSummary.project in (SELECT project_id FROM " + schema + ".xe_user_project where user ="
				+ userId + ") "
				+ " GROUP BY trialSummary.build order by xr.release_id) union (SELECT SUM(case when status_id in (1,2,3,4) then 1 else 0 end) as  ExecutedTcCount, "
				+ "         SUM(case when status_id = 1 then 1 else 0 end) as PassCount, "
				+ " SUM(case when status_id = 2 then 1 else 0 end) as FailCount,SUM(case when status_id = 3 then 1 else 0 end) as SkipCount, "
				+ " SUM(case when status_id = 4 then 1 else 0 end) as BlockCount,xb.build_id,xb.build_name,xr.release_id,xr.release_name,'Manual' as type "
				+ " FROM " + schema + ".xe_testcase_execution_summary as trialSummary, " + schema
				+ ".xe_project as project, " + schema + ".xe_build as xb, " + schema + ".xe_release as xr "
				+ " where trialSummary.project_id=project.project_id and trialSummary.build_id=xb.build_id and xb.build_status=1 "
				+ " and xr.release_active=1 and xr.release_id=xb.release_id "
				+ "and trialSummary.project_id in (SELECT project_id FROM " + schema + ".xe_user_project where user = "
				+ userId + ") " + "GROUP BY trialSummary.build_id order by xr.release_id)";
		List<Map<String, Object>> releaseTestDataTotal = database.select(query);
		logger.info("Query Result  " + releaseTestDataTotal);
		return releaseTestDataTotal;
	}

	@Override
	public List<Map<String, Object>> getAutoStepExecSummary(Integer buildId, Integer testCaseId, String schema) {
		logger.info(
				"Query to get Automation Steps Execution Summary buildId : " + buildId + ", testCaseId: " + testCaseId);
		String query = "SELECT x.trial_step_id, x.testcase_id, xt.testcase_name, x.step_id, x.step_details, x.build_id,xab.build_name, x.iteration_number, x.exe_timestamp, x.step_value, x.step_status, x.screenshot_title, x.actual_result, x.is_bug_raised, x.dataset_id "
				+ "FROM " + schema + ".xebm_automation_steps_execution_summary X " + "JOIN " + schema
				+ ".xetm_testcase xt ON x.testcase_id = xt.testcase_id " + "JOIN " + schema
				+ ".xe_automation_build xab ON x.build_id = xab.build_id " + "WHERE x.build_id=" + buildId
				+ " AND x.testcase_id=" + testCaseId + " order by screenshot_title";
		List<Map<String, Object>> list = database.select(query);
		logger.info("Query Result  " + list);
		return list;
	}

	// @author anmol.chadha
	@Override
	public List<Map<String, Object>> getTrashManualDetails(String schema) {
		String sql = String.format(build.getTrashManualDetails, schema, schema);
		List<Map<String, Object>> data = database.select(sql);
		return data;
	}

	// @author anmol.chadha
	@Override
	public List<Map<String, Object>> getTrashAutomationDetails(String schema) {
		String sql = String.format(build.getTrashAutomationDetails, schema, schema);
		List<Map<String, Object>> data = database.select(sql);
		return data;
	}

	// @author anmol.chadha
	@Override
	public boolean restoreAutomationBuildStatusForTrash(int buildId, int releaseID, String schema) {
		int flag;
		try {
			String sql = String.format(build.restoreReleaseStatusForTrash, schema);
			flag = database.update(sql, new Object[] { releaseID }, new int[] { Types.INTEGER });
			/* if( flag == 1 ) { */
			String setAutomationBuildFlag = String.format(build.restoreAutomationBuildStatusForTrash, schema);
			database.update(setAutomationBuildFlag, new Object[] { buildId }, new int[] { Types.INTEGER });
			// }
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// @author anmol.chadha
	@Override
	public boolean restoreManualBuildStatusForTrash(int buildId, int releaseID, String schema) {
		int flag;
		try {
			String sql = String.format(build.restoreReleaseStatusForTrash, schema);
			flag = database.update(sql, new Object[] { releaseID }, new int[] { Types.INTEGER });
			/* if( flag == 1 ) { */
			String setManualBuildFlag = String.format(build.restoreManualBuildStatusForTrash, schema);
			database.update(setManualBuildFlag, new Object[] { buildId }, new int[] { Types.INTEGER });
			// }
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void insertRedwoodExecutionSteps(String actions, String redwood_build_id, String redwood_tc_id,
			String screenshot, String screenshotTitle, int testCaseStatus, String schema) {
		logger.info("Calling Stored procedure to insert redwood report");
		SqlParameter actionS = new SqlParameter("actions", Types.VARCHAR);
		SqlParameter redwoodBuildId = new SqlParameter("redwood_build_id", Types.VARCHAR);
		SqlParameter redwoodTcId = new SqlParameter("redwood_tc_id", Types.VARCHAR);
		SqlParameter screenshots = new SqlParameter("screenshot", Types.VARCHAR);
		SqlParameter testStatus = new SqlParameter("testCaseStatus", Types.INTEGER);
		SqlParameter[] paramArray = { actionS, redwoodBuildId, redwoodTcId, screenshots, testStatus };
		try {
			int redBuildId = database.select(
					String.format("SELECT build_id FROM " + schema + ".xe_automation_build where redwood_build_id=?"),
					new String[] { redwood_build_id });
			int redTcId = database.select(
					String.format("SELECT testcase_id FROM " + schema + ".xetm_testcase where redwood_tc_id=?", schema),
					new String[] { redwood_tc_id });
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName("insertRedwoodExecutionSteps").declareParameters(paramArray)
					.withCatalogName(schema);
			simpleJdbcCall.execute(actions, redBuildId, redTcId, screenshot, screenshotTitle, testCaseStatus);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<Map<String, Object>> getBugDetailsByBuildId(String userId, Integer buildId, String projectId, String schema) {
		logger.info("Query to get Build total test case details");
		List<Map<String, Object>> getBugDetails = database
				.select("SELECT xb.* FROM " + schema + ".xe_automation_step_execution_bug xeb," + schema
						+ ".xebt_bug xb where  xeb.bug_id=xb.bug_id and xb.jira_id='' and  xeb.build_id=" + buildId);
		logger.info("Query Result  " + getBugDetails);
		return getBugDetails;
	}
	
	@Override
	public int getTcCountBeforeExecution(int buildID, String schema) {
		String sql = String.format(build.getExecPreCount, schema);
		List<Map<String, Object>> data = database.select(sql, new Object[] {buildID}, new int[]{Types.INTEGER});
		int count = Integer.parseInt(data.get(0).get("count").toString());
		logger.info("Query Result " + count);
		return count;
	}
	
	@Override
	public List<Map<String, Object>> getAutomationTcDetails(int buildID, String schema) {
		logger.info("Query to get automation TC details based on build id");
		String sql = String.format(build.getAutomationTCDetails, schema);
		List<Map<String, Object>> getAutomationTcDetails = database.select(sql, new Object[] {buildID}, new int[] {Types.INTEGER});
		return getAutomationTcDetails;
		
	}
	
	@Override
	public List<Map<String, Object>> getManualTcDetails(int buildID, String schema) {
		logger.info("Query to get manual TC details based on build id");
		String sql = String.format(build.getManualTCDetails, schema);
		List<Map<String, Object>> getManualTcDetails = database.select(sql, new Object[] {buildID}, new int[] {Types.INTEGER});
		return getManualTcDetails;
	}
	
	@Override
	public String getReleaseIdOfSelectedRelease(String releaseName, String schema) {
		logger.info("Query to get releaseId based on release name");
		String sql = String.format(build.getReleaseId, schema);
		String releaseId = database.stringSelect(sql, new Object[] {releaseName});
		return releaseId;
	}
	
	@Override
	public int getBuildIdForManualBuild(String buildName, String schema) {
		String sql = String.format(build.getBuildIdForManualBuild, schema);
		int buildId = database.select(sql, new Object[] {buildName});
		return buildId;
	}
	
	@Override
	public int getBuildIdForAutoBuild(String buildName, String schema) {
		String sql = String.format(build.getBuildIdForAutoBuild, schema);
		int buildId = database.select(sql, new Object[] {buildName});
		return buildId;
	}
	
	@Override
	public int getTcCountAfterExecution(int buildID, String schema) {
		String sql = String.format(build.getExecPostCount, schema);
		List<Map<String, Object>> data = database.select(sql, new Object[] {buildID}, new int[]{Types.INTEGER});
		int count = Integer.parseInt(data.get(0).get("count").toString());
		logger.info("Query Result " + count);
		return count;
	}
	
	@Override
	public int updateBuildStateBasedOnCount(int buildState, int buildId, String schema) {
		String sql = String.format(build.setBuildState, schema);
		logger.info("Query to update Build Status " +sql);
		int flag = database.update(sql, new Object[] {buildState, buildId}, new int[]{Types.INTEGER, Types.INTEGER});
		logger.info("Query Result " + flag);
		return flag;
	}
	
	@Override
	public int updateVMState(String schema, int vm_exe_status, int userId, int buildId) {
		String sql = String.format(build.setVmStateToStop, schema);
		logger.info("Query to update VM state " +sql);
		int flag = database.update(sql, new Object[] {vm_exe_status, buildId, userId}, new int[]{Types.INTEGER, Types.INTEGER, Types.INTEGER});
		logger.info("Query Result " + flag);
		return flag;
	}
	
	@Override
	public int changeVMStatus(String vmId, int status) {
		String sql = "UPDATE `xenonvm`.`xevm_customervm_details` SET `is_free`=? WHERE `cust_vm_id`=?;";
		if(status==1){
			sql = "UPDATE `xenonvm`.`xevm_customervm_details` SET `is_free`=?, `current_execs`=current_execs-1 WHERE `cust_vm_id`=?;";
		}
		int changeVMStatus = database.update(String.format(sql), new Object[] { status, vmId },
				new int[] { Types.INTEGER, Types.INTEGER });
		return changeVMStatus;
	}

	@Override
	public Map<String, Object> deallocateCustomerVM(int vmId, int userId, int buildId, String schemaName) {

		SqlParameter vmIdTemp = new SqlParameter("vmId", Types.INTEGER);
		SqlParameter userIDTemp = new SqlParameter("userId", Types.INTEGER);
		SqlParameter buildIDTemp = new SqlParameter("buildId", Types.INTEGER);
		SqlParameter schemaTemp = new SqlParameter("schemaName", Types.VARCHAR);
		
		SqlParameter[] paramArray = { vmIdTemp, userIDTemp, buildIDTemp,schemaTemp };
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("deallocateCustomerVM")
				.declareParameters(paramArray).withCatalogName("`xenonvm`");
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(vmId, userId, buildId,schemaName);

		return simpleJdbcCallResult;
	}

	@Override
	public int insertExecutionStepsArray(String testCaseStatus,String build,String testcase,String stepId,String stepDetails,String stepInputType,String stepValue,String stepStatus,String attachmentPath,String title,String bugRaise, String schema_name, int datasetId, String iterationCounts, String timestamps) throws Exception {
		System.out.println("dataset ID *************** "+datasetId);
		String insertExecutuionStep = "CALL `%s`.`insertExecutionSteps`(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		int status = database.update(String.format(insertExecutuionStep, schema_name),
				new Object[] { Integer.parseInt(testCaseStatus),Integer.parseInt(build),testcase,stepId,
						stepDetails,stepInputType,stepValue,stepStatus,attachmentPath,title,bugRaise,datasetId, iterationCounts, timestamps },
				new int[] { Types.INTEGER,Types.INTEGER,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.INTEGER,Types.VARCHAR,Types.VARCHAR});
		
		return status;
	}
	

	

	
}

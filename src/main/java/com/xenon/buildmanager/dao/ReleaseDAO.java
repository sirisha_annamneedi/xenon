package com.xenon.buildmanager.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.xenon.buildmanager.domain.ReleaseDetails;

/**
 * This interface includes all release related operation
 *
 */
public interface ReleaseDAO {
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to insert new release 
	 * 
	 * @param insertRelease
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	public int insertRelease(ReleaseDetails insertRelease,String schema) throws SQLException;
	
	/**
	 * 
	 * This method reads all release details 
	 * 
	 * @param schema
	 * @return
	 */
	public List<Map<String,Object>> getAllReleaseDetails(String schema);
	
	/**
	 * 
	 * This method reads release details by id
	 * 
	 * @param releaseID
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> getReleaseDetails(int releaseID,String schema);
	
	/**
	 * 
	 * This method updates release
	 * 
	 * @param updateRelease
	 * @param schema
	 * @return
	 */
	int updateRelease(ReleaseDetails updateRelease,String schema);
	
	/**
	 * @author shantaram.tupe
	 * 04-Nov-2019:10:33:29 AM
	 * @param releaseName
	 * @param releaseId
	 * @param schema
	 * @return
	 */
	int updateReleaseName( String releaseName, int releaseId, String schema );
	/**
	 * @author shantaram.tupe
	 * 01-Nov-2019:10:39:40 AM
	 * @param releaseStatus
	 * @param releaseId
	 * @param schema
	 * @return
	 */
	int deleteRelease( int releaseId, String schema );
	
	/**
	 * 
	 * This method reads active release names
	 * 
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> getActiveReleaseNames(String schema);

	List<Map<String, Object>> getReleaseTCCount(String schema);

	String getReleaseId(String schema, String releaseName);
}

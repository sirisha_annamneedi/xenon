package com.xenon.buildmanager.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import com.xenon.testmanager.dao.TestcaseDAOImpl;

public class ExecuteBuildDAOImpl implements ExecuteBuildDAO,XenonQuery{
	@Autowired
	TestcaseDAOImpl testCase;
	
	@Autowired
	DatabaseDao database;
	private static final Logger logger = LoggerFactory.getLogger(ExecuteBuildDAOImpl.class);
	
	@Override
	public List<Map<String,Object>> getExecBuildDetails(int buildId, String schema) {
		logger.info("Reads build execution details");
		logger.info("Query Param : buildId = {}",buildId);
		String sql = String.format(exeBuild.getExecuteBuildDetails, schema,schema,schema,schema,schema,schema,schema);
		List<Map<String,Object>> executeBuildDetails = database.select(sql,
				new Object[]{buildId},
				new int[]{Types.INTEGER});
		logger.info("Return list = "+executeBuildDetails);
		return executeBuildDetails;
	}
	@Override
	public List<Map<String, Object>> getScreenshotById(int exeId, String schema) {
		logger.info("SQL Query to get excute build attachment details from execute build id");
		List<Map<String, Object>> attachDetails = database.select(String.format(exeBuild.screenshotById,schema),
				new Object[] { exeId }, new int[] { Types.INTEGER });
		logger.info("SQL : "+bug.bugDetails +" Values : executeId = "+exeId);
		logger.info("SQL Result "+attachDetails);
		return attachDetails;
	}
	
	
	
	@Override
	public List<Map<String, Object>> getAutotestcases(String schema) {
		logger.info("Reads automation test cases ");
		String sql=String.format(exeBuild.autoTestCases, schema);
		List<Map<String, Object>> autoExe =  database.select(sql);
		logger.info("Return list = "+autoExe);
		return autoExe;

	}
	
}

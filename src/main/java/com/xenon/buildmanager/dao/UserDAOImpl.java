package com.xenon.buildmanager.dao;

import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.api.common.MailService;
import com.xenon.buildmanager.domain.UserDetails;
import com.xenon.common.dao.MailDAO;
import com.xenon.core.dao.CustomerDAO;
import com.xenon.database.XenonQuery;

public class UserDAOImpl implements UserDAO, XenonQuery {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

	@Resource(name = "mailProperties")
	private Properties mailProperties;

	@Autowired
	private com.xenon.database.DatabaseDao database;

	@Autowired
	MailService mailService;

	@Autowired
	MailDAO mailDAO;

	@Autowired
	CustomerDAO customerDao;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int insertUser(UserDetails user, String schema) {
		logger.info("In Method - insert user details");
		int flag = database.update(String.format(users.insertUser, schema),
				new Object[] { user.getPassword(), user.getEmailId(), user.getFirstName(), user.getLastName(),
						user.getRoleId(), user.getTMStatus(), user.getBTStatus(), user.getUserStatus(),
						user.getUserPhoto() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER,
						Types.INTEGER, Types.INTEGER, Types.BLOB });

		logger.info("Result : " + flag);

		return flag;
	}

	@Override
	public int insertUserIntoCore(UserDetails user, String schema) {
		logger.info("In Method - insert user into xenon core");
		List<Map<String, Object>> custid = database.select(
				"select cust.customer_id from `xenon-core`.xe_customer_details as cust where cust.customer_schema ='"
						+ schema + "'");
		int flag = database.update(String.format(users.insertUserIntoCore, schema),
				new Object[] { user.getUserName(), user.getEmailId(),
						Integer.parseInt(custid.get(0).get("customer_id").toString()) },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER });
		logger.info("Result : " + flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> getUserRole(String schema) {
		logger.info("In Method - get user role");
		List<Map<String, Object>> userRole;
		userRole = database.select(String.format(users.getUserRole, schema));
		logger.info("Result : " + userRole);
		return userRole;
	}

	@Override
	public int getUserId(UserDetails user, String schema) {
		logger.info("In Method - get user id by user details");
		List<Map<String, Object>> userDetails = database.select(String.format(users.getUserId, schema),
				new Object[] { user.getFirstName(), user.getLastName(), user.getEmailId(), user.getBTStatus(),
						user.getTMStatus(), user.getUserStatus(), user.getRoleId() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.INTEGER });
		return Integer.parseInt(userDetails.get(0).get("user_id").toString());
	}

	@Override
	public List<Map<String, Object>> getUserDetails(int userId, String schema) {
		logger.info("In Method - get user details by user id");
		List<Map<String, Object>> userDetails = database.select(String.format(users.getUserDetails, schema),
				new Object[] { userId }, new int[] { Types.INTEGER });
		logger.info("Result : " + userDetails);
		return userDetails;
	}

	@Override
	public String checkUser(String emailId) {
		logger.info("In Method - check duplicate email id");
		List<Map<String, Object>> userDetails = database.select(users.checkUser, new Object[] { emailId },
				new int[] { Types.VARCHAR });
		if (userDetails.size() == 0) {
			return "true";
		} else {
			return "false";
		}
	}

	@Override
	public String removeUser(int userid, String schema) {
		logger.info("In Method - remove user");
		int updateStatus = database.update(String.format(users.removeUser, schema), new Object[] { userid },
				new int[] { Types.INTEGER });

		if (updateStatus != 0) {
			return "200";
		} else
			return "500";
	}

	@Override
	public List<Map<String, Object>> getAllUserDetails(String schema) {
		logger.info("In Method - get all users details");
		List<Map<String, Object>> allUserDetails;
		allUserDetails = database
				.select(String.format(users.getAllUserDetails, schema, schema, schema, schema, schema));
		logger.info("Result : " + allUserDetails);
		return allUserDetails;
	}

	@Override
	public boolean updateUser(UserDetails user, String schema) {
		logger.info("In Method - update user details");
		int updateStatus = database.update(String.format(users.updateUser, schema),
				new Object[] { user.getFirstName(), user.getLastName(), user.getRoleId(), user.getTMStatus(),
						user.getBTStatus(), user.getUserStatus(), user.getUserId() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.INTEGER });

		if (updateStatus != 0) {
			return true;
		} else
			return false;
	}

	@Override
	public int insertAdminUser(UserDetails user, String schema) {
		logger.info("insert admin user details");
		String insertUser = "insert into `" + user.getCust()
				+ "`.xe_user_details (user_password, email_id, first_name, last_name, role_id, tm_status, bt_status, user_status,user_photo,user_timezone,user_name,jenkin_status,git_status) values (SHA1(?),?,?,?,?,?,?,?,?,?,?,?,?)";
		int flag = database.update(insertUser,
				new Object[] { user.getPassword(), user.getEmailId(), user.getFirstName(), user.getLastName(),
						user.getRoleId(), user.getTMStatus(), user.getBTStatus(), user.getUserStatus(),
						user.getUserPhoto(), 2, user.getUserName(), user.getJenkinStatus(), user.getGitStatus() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER,
						Types.INTEGER, Types.INTEGER, Types.BLOB, Types.INTEGER, Types.VARCHAR, Types.INTEGER,
						Types.INTEGER });
		logger.info("Return flag = " + flag);
		return flag;

	}

	@Override
	public int getAdminUserId(UserDetails user, String schema) {
		logger.info("In Method - get admin user id from  details");
		String getUserId = "select user_id from `" + user.getCust()
				+ "`.xe_user_details where first_name=? and last_name=? and email_id=? and bt_status =? and tm_status=? and user_status =? and role_id =?";
		List<Map<String, Object>> userDetails = database.select(getUserId,
				new Object[] { user.getFirstName(), user.getLastName(), user.getEmailId(), user.getBTStatus(),
						user.getTMStatus(), user.getUserStatus(), user.getRoleId() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.INTEGER });
		return Integer.parseInt(userDetails.get(0).get("user_id").toString());
	}

	@Override
	public List<Map<String, Object>> getAdminUserDetails(String schema) {
		logger.info("Reading admin user details");
		int adminRoleId = 2;
		List<Map<String, Object>> userDetails = database.select(String.format(users.getAdminUserDetails, schema),
				new Object[] { adminRoleId }, new int[] { Types.INTEGER });
		logger.info("Return list = " + userDetails);
		return userDetails;
	}

	@Override
	public boolean updateUserWithProfilePhoto(UserDetails user, String schema) {
		logger.info("update user with profile photo");
		int updateStatus = database.update(String.format(users.updateUserWithProfilePhoto, schema),
				new Object[] { user.getFirstName(), user.getLastName(), user.getEmailId(), user.getRoleId(),
						user.getTMStatus(), user.getBTStatus(), user.getUserStatus(), user.getUserPhotoName(),
						user.getUserId() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.INTEGER, Types.VARCHAR, Types.INTEGER });
		logger.info("Return status = " + updateStatus);
		if (updateStatus != 0) {
			return true;
		} else
			return false;
	}

	@Override
	public String sendMail(List<Map<String, Object>> emailDetails, String customerName, UserDetails user, String schema)
			throws Exception {

		// List<Map<String, Object>> custDetails =
		// customerDao.getCustomerDetailsById(customer_id);
		// String custName =
		// custDetails.get(0).get("customer_name").toString();
		ArrayList<String> recepients = new ArrayList<String>();
		recepients.add(user.getEmailId());

		String mailSubject = mailProperties.getProperty("CREATE_USER_SUBJECT",
				"Welcome to Xenon. Please find your login details below");
		String mailBodyText = mailProperties.getProperty("CREATE_USER_BODY_TEXT", "None");

		String from = emailDetails.get(0).get("smtp_user_id").toString();

		String message = mailService.postMail(emailDetails, recepients, mailSubject,
				String.format(mailBodyText, customerName, user.getEmailId(), "Your Riverbed LDAP Password",
						(getUserRoleById(user.getRoleId(), schema)).get(0).get("role_type").toString()),
				from);
		return message;

	}

	@Override
	public List<Map<String, Object>> getUserDetailsByEmailId(String emailId) {
		logger.info("In Method - get user details by email id");
		int custId = customerDao.getCustomerIdByEmailId(emailId);
		List<Map<String, Object>> userDetails = new ArrayList<Map<String, Object>>();
		if (custId != 0) {
			List<Map<String, Object>> custDetails = customerDao.getCustomerDetailsById(custId);
			String schema = custDetails.get(0).get("customer_schema").toString();
			userDetails = database.select(String.format(users.getUserDetailsByEmailId, schema),
					new Object[] { emailId }, new int[] { Types.VARCHAR });
			logger.info("Result : " + userDetails);
		}
		return userDetails;

	}

	@Override
	public boolean checkUserEmailIdExist(String emailId) {
		logger.info("In Method - check user email id exist or not");
		List<Map<String, Object>> userDetails = database.select(users.checkUser, new Object[] { emailId },
				new int[] { Types.VARCHAR });
		if (userDetails.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String sendMailOnForgotPassword(List<Map<String, Object>> custDetails, UserDetails user, String schema)
			throws Exception {
		logger.info("sends mail on user forgot password");
		List<Map<String, Object>> emailDetails = mailDAO.allSMTPDetails(schema);
		if (emailDetails.size() == 0) {
			logger.info("Mail details not available");
			return "Mail details not available";
		}
		ArrayList<String> recepients = new ArrayList<String>();
		recepients.add(user.getEmailId());
		String subject = "Password for Xenon";
		logger.info("sending mail to user");
		String bodyText = "<html xmlns=\"http://www.w3.org/1999/xhtml\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"><head style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">    <meta name=\"viewport\" content=\"width=device-width\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">    <title style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">Actionable emails e.g. reset password</title>    </head><body style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;line-height: 1.6;background-color: #f6f6f6;width: 100% !important;\"><table class=\"body-wrap\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;background-color: #f6f6f6;width: 100%;\">    <tbody style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">"
				+ "<tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">        <td style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\"></td>        <td class=\"container\" width=\"600\" style=\"margin: 0 auto !important;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;display: block !important;max-width: 600px !important;clear: both !important;\">            <div class=\"content\" style=\"margin: 0 auto;padding: 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;max-width: 600px;display: block;\">                <table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;background: #fff;border: 1px solid #e9e9e9;border-radius: 3px;\">                    <tbody style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">"
				+ "<tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                        <td class=\"content-wrap\" style=\"margin: 0;padding: 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">                            <table cellpadding=\"0\" cellspacing=\"0\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                <tbody style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">"
				+ "<tr style=\"background-color: #e7e7e7;margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                    <td style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">   "
				+ "<img class=\"img-responsive\" src=\"http://xenon.jadeglobal.com/wp-content/uploads/2015/03/cropped-cropped-xeon-logo-web1.png\" "
				+ "style=\"height: 130px;  margin-left: 150px;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;"
				+ "Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;max-width: 100%;\">                                    </td>                                </tr>                                <tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                    <td class=\"content-block\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">                                        <h3 style=\"margin: 40px 0 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif;box-sizing: border-box;font-size: 18px;color: #000;line-height: 1.2;font-weight: 400;\"><br>Welcome to Xenon<br style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"><br style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"> Your password is reset</h3>                                    </td>                                </tr>                                <tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                    <td class=\"content-block\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">User name : <i>"
				+ user.getEmailId()
				+ "</i><br style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">Password : <i>"
				+ user.getPassword()
				+ "<br style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"></td>                                </tr>                                <tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                    <td class=\"content-block\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">                                        "
				+ ""
				+ "</td></tr><tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                    <td class=\"content-block aligncenter\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;text-align: center;\">                                        <a href=\"xenon-core.cloudapp.net:8080\" class=\"btn-primary\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;color: #FFF;text-decoration: none;background-color: #1ab394;border: solid #1ab394;border-width: 5px 10px;line-height: 2;font-weight: bold;text-align: center;cursor: pointer;display: inline-block;border-radius: 5px;text-transform: capitalize;\">Login to Xenon dashboard</a>                                    </td>                               "
				+ "</tr></tbody></table>                        </td>                    </tr>                "
				+ "</tbody></table>                "
				+ "<div class=\"footer\" style=\"margin: 0;padding: 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;width: 100%;clear: both;color: #999;\">                    <table width=\"100%\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                        <tbody style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"><tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                            <td class=\"aligncenter content-block\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 12px;vertical-align: top;text-align: center;\"><a href=\"#\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 12px;color: #999;text-decoration: underline;\">Copyright © 2019, Jade Global, Inc. All rights reserved.</a></td>                        </tr>                    </tbody></table>                </div></div>        </td>        <td style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\"></td>    </tr></tbody></table></body></html>";
		String from = emailDetails.get(0).get("smtp_user_id").toString();
		String message = mailService.postMail(emailDetails, recepients, subject, bodyText, from);
		logger.info("Message  = " + message);
		return message;
	}

	@Override
	public int createNewUserStatus(String schema, int custTypeId) throws SQLException {
		logger.info("check customer's create user limit exceeds or not");
		String createNewUserStatus = String.format(users.createNewUserStatus, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(createNewUserStatus);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setInt(2, custTypeId);
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Insert status = " + insertStatus);
		return insertStatus;

	}

	@Override
	public List<Map<String, Object>> getUserDetailsById(int userId, String schema) {
		logger.info("In Method - get user details by id");
		List<Map<String, Object>> userDetails = database.select(String.format(users.getUserDetailsById, schema),
				new Object[] { userId }, new int[] { Types.INTEGER });
		logger.info("Result : " + userDetails);
		return userDetails;
	}

	@Override
	public List<Map<String, Object>> getUpdatePasswordUserDetails(int userId, String schema)
			throws NoSuchAlgorithmException {
		logger.info("In Method - get update password user details ");
		List<Map<String, Object>> userDetails = database.select(
				String.format(users.getUpdatePasswordUserDetails, schema), new Object[] { userId },
				new int[] { Types.INTEGER });
		logger.info("Result : " + userDetails);
		return userDetails;
	}

	@Override
	public int updateAllUsersStatus(int tmStatus, int btStatus, String schema) {
		logger.info("In Method - update All Users Status");
		String sql = String.format(users.updateAllUsersStatus, schema);
		int flag = database.update(sql, new Object[] { tmStatus, btStatus },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Result : " + flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> userAssignedProjectDetails(int userID, String schema) {
		logger.info("In Method - get user assigned project details");
		String sql = String.format(users.userAssignedProjectDetails, schema, schema, schema, schema);
		List<Map<String, Object>> details = database.select(sql, new Object[] { userID }, new int[] { Types.INTEGER });
		logger.info("Result : " + details);
		return details;
	}

	@Override
	public List<Map<String, Object>> getUserRoleById(int id, String schema) {
		logger.info("In Method - get user role by user id");
		List<Map<String, Object>> userRole;
		userRole = database.select(String.format(users.getUserRoleById, schema), new Object[] { id },
				new int[] { Types.INTEGER });
		logger.info("Result : " + userRole);
		return userRole;
	}

	@Override
	public Map<String, Object> getCreateUserData(int userId, String schema) {
		logger.info("Reads query optimized create user data");
		SqlParameter userIdTemp = new SqlParameter("userId", Types.VARCHAR);
		SqlParameter[] paramArray = { userIdTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getCreateUserData")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

/*	@Override
	public Map<String, Object> insertUserDetails(UserDetails user, String schema) {
		logger.info("Query optimized insert user details");
		SqlParameter emailId = new SqlParameter("emailId", Types.VARCHAR);
		SqlParameter upassword = new SqlParameter("userPassword", Types.VARCHAR);
		SqlParameter fNameParam = new SqlParameter("firstName", Types.VARCHAR);
		SqlParameter lNameParam = new SqlParameter("lastName", Types.VARCHAR);
		SqlParameter roleId = new SqlParameter("roleId", Types.INTEGER);
		SqlParameter tmStatus = new SqlParameter("tmStatus", Types.INTEGER);
		SqlParameter btStatus = new SqlParameter("btStatus", Types.INTEGER);
		SqlParameter userStatus = new SqlParameter("userStatus", Types.INTEGER);
		SqlParameter userPhoto = new SqlParameter("userPhoto", Types.BLOB);
		SqlParameter schemaName = new SqlParameter("schemaName", Types.VARCHAR);
		SqlParameter createDate = new SqlParameter("createDate", Types.VARCHAR);
		SqlParameter userName = new SqlParameter("userName", Types.VARCHAR);
		SqlParameter jenkinStatus = new SqlParameter("jenkinStatus", Types.VARCHAR);
		SqlParameter gitStatus = new SqlParameter("gitStatus", Types.VARCHAR);
		SqlParameter redwoodStatus = new SqlParameter("jenkinStatus", Types.INTEGER);
		SqlParameter apiStatus = new SqlParameter("gitStatus", Types.INTEGER);
		SqlParameter[] paramArray = { upassword, emailId, fNameParam, lNameParam, roleId, tmStatus, btStatus,
				userStatus, userPhoto, schemaName, createDate, userName, jenkinStatus, gitStatus, redwoodStatus,
				apiStatus };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("insertUser")
				.declareParameters(paramArray).withCatalogName(schema);
		// String CreateDate=user.getCreateDate();
		//// System.out.println("value from getCreateDate "+CreateDate);
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(user.getPassword(), user.getEmailId(),
				user.getFirstName(), user.getLastName(), user.getRoleId(), user.getTMStatus(), user.getBTStatus(),
				user.getUserStatus(), user.getUserPhoto(), schema, user.getCreateDate(), user.getUserName(),
				user.getJenkinStatus(), user.getGitStatus());
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}*/

	 @Override
	    public Map<String, Object> insertUserDetails(UserDetails user, String schema) {
	        logger.info("Query optimized insert user details");
	        SqlParameter emailId = new SqlParameter("emailId", Types.VARCHAR);
	        SqlParameter upassword = new SqlParameter("userPassword", Types.VARCHAR);
	        SqlParameter fNameParam = new SqlParameter("firstName", Types.VARCHAR);
	        SqlParameter lNameParam = new SqlParameter("lastName", Types.VARCHAR);
	        SqlParameter roleId = new SqlParameter("roleId", Types.INTEGER);
	        SqlParameter tmStatus = new SqlParameter("tmStatus", Types.INTEGER);
	        SqlParameter btStatus = new SqlParameter("btStatus", Types.INTEGER);
	        SqlParameter userStatus = new SqlParameter("userStatus", Types.INTEGER);
	        SqlParameter userPhoto = new SqlParameter("userPhoto", Types.BLOB);
	        SqlParameter schemaName = new SqlParameter("schemaName", Types.VARCHAR);
	        SqlParameter createDate = new SqlParameter("createDate", Types.VARCHAR);
	        SqlParameter userName = new SqlParameter("userName", Types.VARCHAR);
	        SqlParameter jenkinStatus = new SqlParameter("jenkinStatus", Types.VARCHAR);
	        SqlParameter gitStatus = new SqlParameter("gitStatus", Types.VARCHAR);
	        SqlParameter redwoodStatus = new SqlParameter("jenkinStatus", Types.INTEGER);
	        SqlParameter apiStatus = new SqlParameter("gitStatus", Types.INTEGER);
	        SqlParameter[] paramArray = { upassword, emailId, fNameParam, lNameParam, roleId, tmStatus, btStatus,
	                userStatus, userPhoto, schemaName, createDate, userName, jenkinStatus, gitStatus, redwoodStatus,
	                apiStatus };

	 

	        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("insertUser")
	                .declareParameters(paramArray).withCatalogName(schema);
	        // String CreateDate=user.getCreateDate();
	        //// System.out.println("value from getCreateDate "+CreateDate);
	        Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(user.getPassword(), user.getEmailId(),
	                user.getFirstName(), user.getLastName(), user.getRoleId(), user.getTMStatus(), user.getBTStatus(),
	                user.getUserStatus(), user.getUserPhoto(), schema, user.getCreateDate(), user.getUserName(),
	                user.getJenkinStatus(), user.getGitStatus(), user.getRedwoodStatus(), user.getApiStatus());
	        logger.info("Return list = " + simpleJdbcCallResult);
	        return simpleJdbcCallResult;
	    }
	
	@Override
	public Map<String, Object> getEditUserData(int userId, String schema) {
		logger.info("Reads optimized edit user data");
		SqlParameter userIdTemp = new SqlParameter("userId", Types.VARCHAR);
		SqlParameter[] paramArray = { userIdTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getEditUserData")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getViewUserData(int startValue, int offsetValue, String schema) {
		logger.info("Reads optimized view user data");

		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getViewUserData")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(startValue, offsetValue);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> updateUserData(UserDetails user, String schema) {
		logger.info("Reads optimized update user data");
		SqlParameter userId = new SqlParameter("userId", Types.VARCHAR);
		SqlParameter fNameParam = new SqlParameter("firstName", Types.VARCHAR);
		SqlParameter lNameParam = new SqlParameter("lastName", Types.VARCHAR);
		SqlParameter roleId = new SqlParameter("roleId", Types.INTEGER);
		SqlParameter tmStatus = new SqlParameter("tmStatus", Types.INTEGER);
		SqlParameter btStatus = new SqlParameter("btStatus", Types.INTEGER);
		SqlParameter userStatus = new SqlParameter("userStatus", Types.INTEGER);
		SqlParameter updatedate = new SqlParameter("updateDate", Types.VARCHAR);
		SqlParameter jenkinStatus = new SqlParameter("jenkinStatus", Types.VARCHAR);
		SqlParameter gitStatus = new SqlParameter("gitStatus", Types.VARCHAR);
		SqlParameter redwoodStatus = new SqlParameter("redwoodStatus", Types.INTEGER);
		SqlParameter apiStatus = new SqlParameter("apiStatus", Types.INTEGER);
		SqlParameter[] paramArray = { userId, fNameParam, lNameParam, roleId, tmStatus, btStatus, userStatus,
				updatedate, jenkinStatus, gitStatus, redwoodStatus, apiStatus };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("updateUserData")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(user.getUserId(), user.getFirstName(),
				user.getLastName(), user.getRoleId(), user.getTMStatus(), user.getBTStatus(), user.getUserStatus(),
				user.getUpdateDate(), user.getJenkinStatus(), user.getGitStatus(), user.getRedwoodStatus(),
				user.getApiStatus());
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getUserAssignedProjects(int userId, String schema) {
		logger.info("Reads optimized user assigned projects");
		SqlParameter userIdTemp = new SqlParameter("userId", Types.VARCHAR);
		SqlParameter[] paramArray = { userIdTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getUserAssignedProjects")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getApplicationsToAssign(int seletcedUser, int currentUser, String schema) {
		logger.info("reads optimized user assigned projects");
		SqlParameter userId = new SqlParameter("seletcedUser", Types.INTEGER);
		SqlParameter currentUserId = new SqlParameter("currentUser", Types.INTEGER);
		SqlParameter[] paramArray = { userId, currentUserId };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getapplicationstoassign")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(seletcedUser, currentUser);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public List<Map<String, Object>> getUsersDetailsByMailGroupId(int mailGroupId, String schema) {
		logger.info("Reads user details by mail group id");
		List<Map<String, Object>> userDetails = database.select(
				String.format(users.getUsersDetailsByMailGroupId, schema, schema), new Object[] { mailGroupId },
				new int[] { Types.INTEGER });
		logger.info("Return list = " + userDetails);
		return userDetails;
	}

	@Override
	public String getUserEmailID(int userId, String schema) {
		logger.info("SQL Query to get user Email ID ");

		String mailID = database.stringSelect(String.format(users.getUserEmailId, schema), new Object[] { userId });

		logger.info("Query Result  " + mailID);

		return mailID;
	}

	@Override
	public List<Map<String, Object>> getUsersByEmailIds(String emailIDs, String schema) {
		logger.info("Reads the user details by their Email IDs");
		String sql = String.format(users.getUserDetailsByEmailIDs, schema);
		List<Map<String, Object>> userDetails = database.select(sql);
		logger.info("Return list = " + userDetails);
		return userDetails;
	}

	@Override
	public int getUserIDbyEmail(String emailID, String schema) {
		logger.info("Read the user id by their email IDS");
		String sql = String.format(users.getUserIDByEmailIDs, schema);
		int userID = database.select(sql, new Object[] { emailID });
		logger.info("Query result = " + userID);
		return userID;
	}

	@Override
	public boolean isMailSettingEnabled(String schema) {
		String sql = String.format(users.isEmailSettingEnabled, schema);
		int emailEnabled = database.select(sql, new Object[] {});
		if (emailEnabled == 1)
			return true;
		return false;
	}

	@Override
	public boolean updatePasswordByUserId(String userId, String password, String schema) {
		logger.info("update user with profile photo");
		int updateStatus = database.update(String.format(users.updatePasswordByUserId, schema),
				new Object[] { password, userId }, new int[] { Types.VARCHAR, Types.INTEGER });
		logger.info("Return status = " + updateStatus);
		if (updateStatus != 0) {
			return true;
		} else
			return false;
	}

	@Override
	public boolean checkUserName(String userName) {
		logger.info("In Method - check duplicate email id");
		List<Map<String, Object>> userDetails = database.select(users.checkUserName, new Object[] { userName },
				new int[] { Types.VARCHAR });
		if (userDetails.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String resetPasswordForLdapUsers(String emailId, String userName, String password, String schema)
			throws Exception {
		logger.info("sends mail on user forgot password");
		List<Map<String, Object>> emailDetails = mailDAO.allSMTPDetails(schema);
		if (emailDetails.size() == 0) {
			logger.info("Mail details not available");
			return "Mail details not available";
		}
		ArrayList<String> recepients = new ArrayList<String>();
		recepients.add(emailId);
		String subject = "Password for Xenon";
		logger.info("sending mail to user");
		String bodyText = "<html xmlns=\"http://www.w3.org/1999/xhtml\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"><head style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">    <meta name=\"viewport\" content=\"width=device-width\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">    <title style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">Actionable emails e.g. reset password</title>    </head><body style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;line-height: 1.6;background-color: #f6f6f6;width: 100% !important;\"><table class=\"body-wrap\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;background-color: #f6f6f6;width: 100%;\">    <tbody style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">"
				+ "<tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">        <td style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\"></td>        <td class=\"container\" width=\"600\" style=\"margin: 0 auto !important;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;display: block !important;max-width: 600px !important;clear: both !important;\">            <div class=\"content\" style=\"margin: 0 auto;padding: 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;max-width: 600px;display: block;\">                <table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;background: #fff;border: 1px solid #e9e9e9;border-radius: 3px;\">                    <tbody style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">"
				+ "<tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                        <td class=\"content-wrap\" style=\"margin: 0;padding: 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">                            <table cellpadding=\"0\" cellspacing=\"0\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                <tbody style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">"
				+ "<tr style=\"background-color: #e7e7e7;margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                    <td style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">   "
				+ "<img class=\"img-responsive\" src=\"http://xenon.jadeglobal.com/wp-content/uploads/2015/03/cropped-cropped-xeon-logo-web1.png\" "
				+ "style=\"height: 130px;  margin-left: 150px;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;"
				+ "Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;max-width: 100%;\">                                    </td>                                </tr>                                <tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                    <td class=\"content-block\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">                                        <h3 style=\"margin: 40px 0 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif;box-sizing: border-box;font-size: 18px;color: #000;line-height: 1.2;font-weight: 400;\"><br>Welcome to Xenon<br style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"><br style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"> Your password is reset</h3>                                    </td>                                </tr>                                <tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                    <td class=\"content-block\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">User name : <i>"
				+ userName
				+ "</i><br style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">Password : <i>"
				+ password
				+ "<br style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"></td>                                </tr>                                <tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                    <td class=\"content-block\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">                                        "
				+ ""
				+ "</td></tr><tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                    <td class=\"content-block aligncenter\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;text-align: center;\">                                        <a href=\"xenon-core.cloudapp.net:8080\" class=\"btn-primary\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;color: #FFF;text-decoration: none;background-color: #1ab394;border: solid #1ab394;border-width: 5px 10px;line-height: 2;font-weight: bold;text-align: center;cursor: pointer;display: inline-block;border-radius: 5px;text-transform: capitalize;\">Login to Xenon dashboard</a>                                    </td>                               "
				+ "</tr></tbody></table>                        </td>                    </tr>                "
				+ "</tbody></table>                "
				+ "<div class=\"footer\" style=\"margin: 0;padding: 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;width: 100%;clear: both;color: #999;\">                    <table width=\"100%\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                        <tbody style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"><tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                            <td class=\"aligncenter content-block\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 12px;vertical-align: top;text-align: center;\"><a href=\"#\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 12px;color: #999;text-decoration: underline;\">Copyright © 2019, Jade Global, Inc. All rights reserved.</a></td>                        </tr>                    </tbody></table>                </div></div>        </td>        <td style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\"></td>    </tr></tbody></table></body></html>";
		String from = emailDetails.get(0).get("smtp_user_id").toString();
		String message = mailService.postMail(emailDetails, recepients, subject, bodyText, from);
		logger.info("Message  = " + message);
		return message;
	}

	@Override
	public List<Map<String, Object>> getAllLdapInactiveUserDetails(String schema) {
		logger.info("In Method - get all users details");
		List<Map<String, Object>> allUserDetails;
		allUserDetails = database
				.select(String.format(users.getAllLdapInactiveUserDetails, schema, schema, schema, schema, schema));
		logger.info("Result : " + allUserDetails);
		return allUserDetails;
	}

	@Override
	public List<Map<String, Object>> getAllLdapActiveUserDetails(String schema) {
		logger.info("In Method - get all users details");
		List<Map<String, Object>> allUserDetails;
		allUserDetails = database
				.select(String.format(users.getAllLdapActiveUserDetails, schema, schema, schema, schema, schema));
		logger.info("Result : " + allUserDetails);
		return allUserDetails;
	}
}

package com.xenon.buildmanager.dao;

import java.util.List;
import java.util.Map;

public interface ExecuteBuildDAO {

	/**
	 * 
	 * This method reads build execution details
	 * 
	 * @param buildId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getExecBuildDetails(int buildId, String schema);
	
	/**
	 * 
	 * This method reads screenshot by Id
	 * 
	 * @param exeId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getScreenshotById(int exeId, String schema);
	
	/**
	 * 
	 * This method reads automation test cases 
	 * 
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAutotestcases(String schema);

}

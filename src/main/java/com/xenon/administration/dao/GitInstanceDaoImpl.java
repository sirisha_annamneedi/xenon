package com.xenon.administration.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.administration.domain.GitRepositoryDetails;
import com.xenon.administration.domain.GitProjectDetails;
import com.xenon.database.XenonQuery;

public class GitInstanceDaoImpl implements GitInstanceDao,XenonQuery{

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(GitInstanceDaoImpl.class);
	
	
	@Autowired
	private com.xenon.database.DatabaseDao database;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Map<String, Object>> getAllInstances(String userId, String schema) {
		
		logger.info("SQL Query to get all Git instances");
		List<Map<String, Object>> instanceList;
		String sql = String.format(gitDetails.getAllInstances, schema, schema);
		instanceList = database.select(sql, new Object[] { userId }, new int[] { Types.INTEGER });
		logger.info("Query Result " + instanceList);
		return instanceList;
	}
	@Override
	public int insertGitProject(GitProjectDetails gitProjectDetails,String schema){
		int flag = database.update(String.format(gitDetails.insertGitProject, schema),

				new Object[] {gitProjectDetails.getGitProjectName(),gitProjectDetails.getGitUrl(),gitProjectDetails.getApiToken(),gitProjectDetails.getUserName(),gitProjectDetails.getUserId(),gitProjectDetails.getGitStatus()},
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER,Types.VARCHAR});

		logger.info("Result : " + flag);
		return flag;
	}
	
	@Override
	public int insertGitRepository(GitRepositoryDetails gitRepositoryDetails,String schema){
		int flag = database.update(String.format(gitDetails.insertGitRepository, schema),

				new Object[] {gitRepositoryDetails.getInstanceId(),gitRepositoryDetails.getUserId(),gitRepositoryDetails.getRepositoryName(),gitRepositoryDetails.getBranchName()},
				new int[] { Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.VARCHAR});

		logger.info("Result : " + flag);
		return flag;
	}

	
	@Override
	public int updateGitProject(String serverId,String serverName,String username,String gitUrl,String apiToken,String instanceStatus,String schema) {
		
		int flag = database.update(String.format(gitDetails.updateGitInstance, schema),

				new Object[] {serverName,gitUrl,apiToken,username,instanceStatus,serverId},
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,Types.VARCHAR,Types.VARCHAR});

		logger.info("Result : " + flag);
		return flag;
	}

	public Map<String, Object> getInstancePermissionWiseMembers(String instanceId, String schema) {
		logger.info("Query to get permission wise members data");
		SqlParameter applicationIDTemp = new SqlParameter("instanceId", Types.INTEGER);
		SqlParameter[] paramArray = { applicationIDTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("gitInstanceSettings")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(instanceId);
		logger.info("Query Result  " + simpleJdbcCallResult);
		return simpleJdbcCallResult;

	}
	
	@Override
	public int removeUsersInstancePermission(String userId, String instanceId, String schema) {
		int flag = 1;
		flag = database.update(String.format(gitDetails.removeUsersInstancePermission, schema),
				new Object[] {userId,instanceId},
				new int[] { Types.INTEGER,Types.INTEGER});	
		return flag;
	}
	
	@Override
	public int assignInstancePermissionToUsers(String[] allUserIds, String instanceId, String schema) {
		int flag=1;
		for (String userId : allUserIds) {			
			flag = database.update(String.format(gitDetails.insertInstancePermissionToUser, schema),
					new Object[] {userId,instanceId},
					new int[] { Types.INTEGER, Types.INTEGER});		
		}

		logger.info("Result : " + flag);
		return flag;
	}
	
	@Override
	public int updateUsersInstancePermission(String[] editPermissionUsers, String instanceId,
			String coreSchema) {
		int flag = 1;
		
		for (String editPermissionUserId : editPermissionUsers) {
			flag = database.update(String.format(gitDetails.updateUsersUpdateInstancePermission, coreSchema),
					new Object[] {editPermissionUserId},
					new int[] { Types.VARCHAR});
		}
		
		return flag;
	}
	
	@Override
	public int revokeUsersInstancePermission(String userId, String instanceId,String schema) {
		int flag = 1;
		flag = database.update(String.format(gitDetails.revokeUsersUpdateInstancePermission, schema),
				new Object[] {instanceId,userId},
				new int[] { Types.INTEGER,Types.INTEGER});
		flag = database.update(String.format(gitDetails.revokeUsersExecuteInstancePermission, schema),
				new Object[] {instanceId,userId},
				new int[] { Types.INTEGER,Types.INTEGER});
		return flag;
	}
	
	@Override
	public List<Map<String, Object>> getInstancesDetailsById(int instanceId,String userId, String schema) {
		
		logger.info("SQL Query to get Git instances details by Id");
		List<Map<String, Object>> instanceList;
		String sql = String.format(gitDetails.getInstancesByDetails, schema, schema);
		instanceList = database.select(sql, new Object[] { userId,instanceId }, new int[] { Types.VARCHAR ,Types.VARCHAR });
		
		/*List<Map<String, Object>> jobDetails = database.select(sql, new Object[] {userId,instanceId },
				new int[] {Types.VARCHAR , Types.INTEGER});*/
		logger.info("Query Result " + instanceList);
		return instanceList;
	}
	
	
	@Override
	public List<Map<String, Object>> getProjectDetailsByRepositoryId(int repoId,int userId, String schema) {
		
		logger.info("SQL Query to get Git instances details by Id");
		List<Map<String, Object>> instanceList;
		String sql = String.format(gitDetails.getProjectDetailsByRepositoryId, schema, schema);
		instanceList = database.select(sql, new Object[] { repoId }, new int[] { Types.INTEGER });
		
		/*List<Map<String, Object>> jobDetails = database.select(sql, new Object[] {userId,instanceId },
				new int[] {Types.VARCHAR , Types.INTEGER});*/
		logger.info("Query Result " + instanceList);
		return instanceList;
	}
	
	
}
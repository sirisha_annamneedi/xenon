package com.xenon.administration.dao;

import java.util.List;
import java.util.Map;

import com.xenon.administration.domain.GitRepositoryDetails;
import com.xenon.administration.domain.GitProjectDetails;

public interface GitInstanceDao {

	List<Map<String, Object>> getAllInstances(String userId, String schema);

	int insertGitRepository(GitRepositoryDetails gitRepositoryDetails, String schema);

	int insertGitProject(GitProjectDetails GitInstanceDetails, String schema);

	int updateGitProject(String serverId, String serverName, String username, String gitUrl, String apiToken,
			String instanceStatus, String schema);

	Map<String, Object> getInstancePermissionWiseMembers(String instanceId, String schema);

	public int removeUsersInstancePermission(String userId, String instanceId, String schema);

	public int assignInstancePermissionToUsers(String[] allUserIds, String instanceId, String schema);

	public int updateUsersInstancePermission(String[] editPermissionUsersArray, String instancesId, String coreSchema);

	public int revokeUsersInstancePermission(String userId, String instanceId, String schema);

	List<Map<String, Object>> getInstancesDetailsById(int instanceId, String userId, String schema);

	List<Map<String, Object>> getProjectDetailsByRepositoryId(int repoId, int userId, String schema);

}

package com.xenon.administration.dao;

import java.util.List;
import java.util.Map;


import com.xenon.administration.domain.JenkinsDetails;
import com.xenon.buildmanager.domain.JenkinsServerDetails;

public interface JenkinsDetailsDao {

	public int insertJenkinsInstance(String serverName,String username,String jenkinsUrl,String apiToken,String instanceStatus, String schema);
	
	/**
	 * 
	 * @author pooja.mugade
	 * @method insertJenkinsJob
	 * @param schema
	 * @param jenkinsDetails
	 * @param userID
	 * used for insert jenkins job
	 */
	public int insertJenkinsJob(JenkinsDetails jenkinsDetails,String schema,int userID);
	
	/**
	 * 
	 * @author pooja.mugade
	 * @method getJobsDetails
	 * @param schema
	 * @param instanceID
	 * used for get all job details
	 */
	List<Map<String, Object>> getJobsDetails(int userId, String instanceID,String schema);
	
	public List<Map<String, Object>> getAllInstances(String schema, String userId);

	public int getServerIdByServerName(String serverName, String currentSchema);

	public List<Map<String, Object>> getApiTokenDetails(int serverInstanceId);

	public int insertNewApiToken(int serverInstanceId, int custId, int userId, String token, String windowsScript,
			String powershellScript);
	
	List<Map<String, Object>> getServerById(int serverId,String schema);
	
	int updateInstanceDetails(JenkinsServerDetails jenkinsServerDetails,String schema);

	public Map<String, Object> getJobPermissionWiseMembers(String jobId, String schema);
	
	public Map<String, Object> getInstancePermissionWiseMembers(String instanceId, String schema);

	public int assignJobPermissionToUsers(String[] allUserIds, String jobId, String schema);
	
	public int assignInstancePermissionToUsers(String[] allUserIds, String instanceId, String schema);

	public int removeUsersJobPermission(String userId, String jobId,String schema);
	
	public int removeUsersInstancePermission(String userId,String instanceId , String schema);

	public int updateUsersJobPermission(String[] editPermissionUsersArray, String[] executePermissionUsersArray, String jobId,
			String coreSchema);
	
	public int updateUsersInstancePermission(String[] editPermissionUsersArray, String instancesId,
			String coreSchema);

	public int revokeUsersJobPermission(String userId, String jobId, String schema);
	
	public int revokeUsersInstancePermission(String userId,String instanceId,  String schema);

	public List<Map<String, Object>> getJenkinsJobDetails(int jobId, String schema);

	public int updateJenkinsJob(int jobId, String jobname, String jobDesciption, int status, String buildToken,
			String schema);

	public List<Map<String, Object>> checkIsUpdateEnabled(int jobId, int userID, String schema);
	
	public List<Map<String, Object>> checkIsExecuteEnabled(int jobId, int userID, String schema);

}

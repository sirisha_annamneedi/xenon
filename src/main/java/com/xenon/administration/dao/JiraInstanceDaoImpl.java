package com.xenon.administration.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.xenon.database.XenonQuery;

public class JiraInstanceDaoImpl implements JiraInstanceDao, XenonQuery {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(JiraInstanceDaoImpl.class);

	@Autowired
	private com.xenon.database.DatabaseDao database;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int insertJiraInstance(String username, String password, String schema) {

		logger.info("In Method - Insert Jira Credentials");
		int flag = database.update(String.format(jiraDetails.insertInstance, schema),
				new Object[] { username, password }, new int[] { Types.VARCHAR, Types.VARCHAR });
		logger.info("Result : " + flag);

		return flag;
	}

	@Override
	public List<Map<String, Object>> getJiraDetails(String schema) {
		logger.info("get job details");
		/* String sql = String.format(jenkinsDetails.getJobDetails,schema); */
		String sql = String.format(jiraDetails.getInstance, schema);
		logger.info("Running Sql query = " + sql);
		List<Map<String, Object>> jobDetails = database.select(sql, new Object[] {}, new int[] {});
		logger.info("Query details = { }", jobDetails);
		return jobDetails;
	}

	@Override
	public int updatJiraInstance(String username, String password, int jiraId, String schema) {
		logger.info("In Method - Update Jenkins Instance");
		int flag = database.update(String.format(jiraDetails.updateJiraDetails, schema),
				new Object[] { username, password, jiraId }, new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER });
		logger.info("Result : " + flag);

		return flag;
	}
}

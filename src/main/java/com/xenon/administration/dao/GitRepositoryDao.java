package com.xenon.administration.dao;

import java.util.List;
import java.util.Map;

import com.xenon.administration.domain.GitRepositoryDetails;

public interface GitRepositoryDao {

	List<Map<String, Object>> getRepositories(String userId,String projectId, String schema);

	List<Map<String, Object>> getRepositoryById(String repositoryId, String schema);

	int updateGitRepository(GitRepositoryDetails gitRepositoryDetails, String schema);

}

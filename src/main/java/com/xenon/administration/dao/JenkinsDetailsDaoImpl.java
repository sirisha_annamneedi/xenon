package com.xenon.administration.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.administration.domain.JenkinsDetails;
import com.xenon.buildmanager.domain.JenkinsServerDetails;
import com.xenon.database.XenonQuery;


public class JenkinsDetailsDaoImpl implements JenkinsDetailsDao, XenonQuery {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(JenkinsDetailsDaoImpl.class);

	@Autowired
	private com.xenon.database.DatabaseDao database;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int insertJenkinsInstance(String serverName, String username, String jenkinsUrl, String apiToken,
			String instanceStatus, String schema) {

		logger.info("In Method - Insert Jenkins Instance");
		int flag = database.update(String.format(jenkinsDetails.insertInstance, schema),
				new Object[] { jenkinsUrl, apiToken, username, serverName, instanceStatus },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER });
		logger.info("Result : " + flag);

		return flag;
	}

	@Override
	public int insertJenkinsJob(JenkinsDetails jenkinsDetail, String schema, int userID) {

		int flag = database.update(String.format(jenkinsDetails.insertJob, schema),

				new Object[] {jenkinsDetail.getInstanceId(),  jenkinsDetail.getjenkinsJobName(),jenkinsDetail.getjobName(), jenkinsDetail.getjobDescription(), jenkinsDetail.getjobStatus(),jenkinsDetail.getbuildStatus(),jenkinsDetail.getbuildToken(),userID},
				new int[] { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER,Types.INTEGER,Types.VARCHAR,Types.INTEGER});

		logger.info("Result : " + flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> getJobsDetails(int userId, String instanceID,String schema) {
		logger.info("get job details");
		/*String sql = String.format(jenkinsDetails.getJobDetails,schema);*/
		String sql = String.format(jenkinsDetails.getJobDetails, schema,schema,schema,schema);
		logger.info("Running Sql query = " + sql);
		List<Map<String, Object>> jobDetails = database.select(sql, new Object[] { instanceID,userId },
				new int[] {Types.VARCHAR , Types.INTEGER});
		logger.info("Query details = { }", jobDetails);
		return jobDetails;
	}

	@Override
	public List<Map<String, Object>> getAllInstances(String schema, String userId) {

		logger.info("SQL Query to get all Jenkins instances");
		List<Map<String, Object>> instanceList;
		String sql = String.format(jenkinsDetails.getAllInstances, schema, schema);
		instanceList = database.select(sql, new Object[] { userId }, new int[] { Types.INTEGER });
		logger.info("Query Result " + instanceList);
		return instanceList;
	}

	@Override
	public int getServerIdByServerName(String serverName, String currentSchema) {

		int serverId = 0;
		String sql = String.format(jenkinsDetails.getServerIdByServerName, currentSchema);
		List<Map<String, Object>> row = database.select(sql, new Object[] { serverName }, new int[] { Types.VARCHAR });

		if (row != null && !row.isEmpty()) {
			serverId = Integer.parseInt("" + row.get(0).get("jenkin_id"));
		}
		return serverId;
	}

	@Override
	public List<Map<String, Object>> getApiTokenDetails(int serverInstanceId) {
		List<Map<String, Object>> tokenList;
		String sql = String.format(jenkinsDetails.getAllApiTokens);
		tokenList = database.select(sql, new Object[] { serverInstanceId }, new int[] { Types.INTEGER });
		logger.info("Query Result " + tokenList);
		return tokenList;
	}

	@Override
	public int insertNewApiToken(int serverInstanceId, int custId, int userId, String token, String windowsScript,
			String powershellScript) {
		try {
			String insertTokenQuery = String.format(jenkinsDetails.insertToken);
			Connection dbConnection = database.getConnection();
			CallableStatement callableStmt = dbConnection.prepareCall(insertTokenQuery);
			callableStmt.setInt(1, serverInstanceId);
			callableStmt.setInt(2, custId);
			callableStmt.setInt(3, userId);
			callableStmt.setString(4, token);
			callableStmt.setString(5, windowsScript);
			callableStmt.setString(6, powershellScript);
			callableStmt.execute();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error(e.toString());
			return 0;
		}

	}
	
	@Override
	public List<Map<String, Object>> getServerById(int serverId,String schema) {

		logger.info("SQL Query to get Jenkins instances");
		List<Map<String, Object>> instanceList;
		String sql = String.format(jenkinsDetails.getAllInstancesById, schema);
		instanceList = database.select(sql,new Object[] { serverId }, new int[] { Types.INTEGER });
		logger.info("Query Result " + instanceList);
		return instanceList;
	}

	@Override
	public int updateInstanceDetails(JenkinsServerDetails jenkinsServerDetails,String schema){
		logger.info("In Method - Update Jenkins Instance");
		int flag = database.update(String.format(jenkinsDetails.updateServerDetails, schema),
				new Object[] { jenkinsServerDetails.getServerName(),jenkinsServerDetails.getJenkinsUrl(),jenkinsServerDetails.getUsername(),jenkinsServerDetails.getaApiToken(),jenkinsServerDetails.getServerStatus(),jenkinsServerDetails.getServerId()},
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER });
		logger.info("Result : " + flag);

		return flag;
	}

	public Map<String, Object> getJobPermissionWiseMembers(String jobId,String schema) {
		logger.info("Query to get permission wise members data");
		SqlParameter applicationIDTemp = new SqlParameter("jobId", Types.INTEGER);
		SqlParameter[] paramArray = { applicationIDTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("jenkinJobSettings")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(jobId);
		logger.info("Query Result  " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	
	public Map<String, Object> getInstancePermissionWiseMembers(String jobId,String schema) {
		logger.info("Query to get permission wise members data");
		SqlParameter applicationIDTemp = new SqlParameter("instanceId", Types.INTEGER);
		SqlParameter[] paramArray = { applicationIDTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("jenkinInstanceSettings")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(jobId);
		logger.info("Query Result  " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public int assignJobPermissionToUsers(String[] allUserIds, String jobId, String schema) {
		int flag=1;
		for (String userId : allUserIds) {			
			flag = database.update(String.format(jenkinsDetails.insertJobPermissionToUser, schema),
					new Object[] {userId,jobId},
					new int[] { Types.INTEGER, Types.INTEGER});		
		}

		logger.info("Result : " + flag);
		return flag;
	}
	
	@Override
	public int assignInstancePermissionToUsers(String[] allUserIds, String instanceId, String schema) {
		int flag=1;
		for (String userId : allUserIds) {			
			flag = database.update(String.format(jenkinsDetails.insertInstancePermissionToUser, schema),
					new Object[] {userId,instanceId},
					new int[] { Types.INTEGER, Types.INTEGER});		
		}

		logger.info("Result : " + flag);
		return flag;
	}

	@Override
	public int removeUsersJobPermission(String userId, String jobId,String schema) {
		int flag = 1;
		flag = database.update(String.format(jenkinsDetails.removeUsersJobPermission, schema),
				new Object[] {userId,jobId},
				new int[] { Types.INTEGER,Types.INTEGER});	
		return flag;
	}
	
	@Override
	public int removeUsersInstancePermission(String userId, String instanceId, String schema) {
		int flag = 1;
		flag = database.update(String.format(jenkinsDetails.removeUsersInstancePermission, schema),
				new Object[] {userId,instanceId},
				new int[] { Types.INTEGER,Types.INTEGER});	
		return flag;
	}

	@Override
	public int updateUsersJobPermission(String[] editPermissionUsers, String[] executePermissionUsers, String jobId,
			String coreSchema) {
		int flag = 1;
		
		for (String editPermissionUserId : editPermissionUsers) {
			flag = database.update(String.format(jenkinsDetails.updateUsersUpdateJobPermission, coreSchema),
					new Object[] {editPermissionUserId},
					new int[] { Types.VARCHAR});
		}
		for (String executePermissionUserId : executePermissionUsers) {
			flag = database.update(String.format(jenkinsDetails.updateUsersExecuteJobPermission, coreSchema),
					new Object[] {executePermissionUserId},
					new int[] { Types.VARCHAR});
		}
		return flag;
	}
	
	@Override
	public int updateUsersInstancePermission(String[] editPermissionUsers, String instanceId,
			String coreSchema) {
		int flag = 1;
		
		for (String editPermissionUserId : editPermissionUsers) {
			flag = database.update(String.format(jenkinsDetails.updateUsersUpdateInstancePermission, coreSchema),
					new Object[] {editPermissionUserId},
					new int[] { Types.VARCHAR});
		}
		
		return flag;
	}

	@Override
	public int revokeUsersJobPermission(String userId, String jobId ,String schema) {
		int flag = 1;
		flag = database.update(String.format(jenkinsDetails.revokeUsersUpdateJobPermission, schema),
				new Object[] {jobId,userId},
				new int[] { Types.INTEGER,Types.INTEGER});
		flag = database.update(String.format(jenkinsDetails.revokeUsersExecuteJobPermission, schema),
				new Object[] {jobId,userId},
				new int[] { Types.INTEGER,Types.INTEGER});
		return flag;
	}
	
	@Override
	public int revokeUsersInstancePermission(String userId, String instanceId,String schema) {
		int flag = 1;
		flag = database.update(String.format(jenkinsDetails.revokeUsersUpdateInstancePermission, schema),
				new Object[] {instanceId,userId},
				new int[] { Types.INTEGER,Types.INTEGER});
		flag = database.update(String.format(jenkinsDetails.revokeUsersExecuteInstancePermission, schema),
				new Object[] {instanceId,userId},
				new int[] { Types.INTEGER,Types.INTEGER});
		return flag;
	}

	@Override
	public List<Map<String, Object>> getJenkinsJobDetails(int jobId, String schema) {

		logger.info("SQL Query to get Jenkins job details for "+jobId);
		List<Map<String, Object>> jobList;
		String sql = String.format(jenkinsDetails.getJenkinsJobDetails, schema);
		jobList = database.select(sql,new Object[] { jobId }, new int[] { Types.INTEGER });
		logger.info("Query Result " + jobList);
		return jobList;
	}

	@Override
	public int updateJenkinsJob(int jobId, String jobname, String jobDesciption, int status, String buildToken,
			String schema) {
		
		int flag = database.update(String.format(jenkinsDetails.updateJob, schema),

				new Object[] {jobname,jobDesciption,status,buildToken,jobId},
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.VARCHAR,Types.INTEGER});

		logger.info("Result : " + flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> checkIsUpdateEnabled(int jobId, int userID, String schema) {
		List<Map<String, Object>> jobList;
		String sql = String.format(jenkinsDetails.getUpdateEnabledJobs, schema);
		jobList = database.select(sql,new Object[] { jobId, userID }, new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result " + jobList);
		return jobList;
	}
	
	@Override
	public List<Map<String, Object>> checkIsExecuteEnabled(int jobId, int userID, String schema) {
		List<Map<String, Object>> jobList;
		String sql = String.format(jenkinsDetails.getExecuteEnabledJobs, schema);
		jobList = database.select(sql,new Object[] { jobId, userID }, new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Query Result " + jobList);
		return jobList;
	}

}

package com.xenon.administration.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.xenon.administration.domain.JiraInstanceReleaseDetails;
import com.xenon.database.XenonQuery;

public class JiraInstanceReleaseDaoImpl implements JiraInstanceReleaseDao, XenonQuery  {
	

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(GitInstanceDaoImpl.class);
	
	
	@Autowired
	private com.xenon.database.DatabaseDao database;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int insertJiraInstanceRelease(JiraInstanceReleaseDetails jiraReleaseDetails,String schema){
		int flag = database.update(String.format(jiraDetails.insertJiraRelease, schema),

				new Object[] {jiraReleaseDetails.getJiraReleaseVersionName(),jiraReleaseDetails.getJiraProjectKey(),jiraReleaseDetails.getJiraReleaseStatus(),
						jiraReleaseDetails.getJiraReleaseStartDate(),jiraReleaseDetails.getJiraReleaseDate(),jiraReleaseDetails.getReleaseDescription(),jiraReleaseDetails.getJiraReleaseVersionId()},
				new int[] { Types.VARCHAR, Types.VARCHAR,Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR,Types.VARCHAR});

		logger.info("Result : " + flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> getJiraRelease(String jiraReleaseVersionId, String schema) {
		logger.info("SQL Query to get release id from jira release details ");
		List<Map<String, Object>> release = database.select(String.format(jiraDetails.getJiraRelease, schema),
				new Object[] { jiraReleaseVersionId},
				new int[] { Types.INTEGER });
		logger.info("SQL : " + jiraDetails.getJiraRelease + " Values :  jira release version id = " + jiraReleaseVersionId);
		return release;
	}

	@Override
	public int updateJiraInstanceRelease(JiraInstanceReleaseDetails jiraReleaseDetails, String schema) {
		int flag = database.update(String.format(jiraDetails.updateJiraRelease, schema),

				new Object[] {jiraReleaseDetails.getJiraReleaseVersionName(),jiraReleaseDetails.getJiraProjectKey(),jiraReleaseDetails.getJiraReleaseStatus(),
						jiraReleaseDetails.getJiraReleaseDate(),jiraReleaseDetails.getReleaseDescription(),jiraReleaseDetails.getJiraReleaseVersionId()},
				new int[] { Types.VARCHAR, Types.VARCHAR,Types.VARCHAR, Types.TIMESTAMP, Types.VARCHAR,Types.VARCHAR});

		logger.info("Result : " + flag);
		return flag;
	}
}

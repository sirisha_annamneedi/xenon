package com.xenon.administration.dao;

import java.sql.SQLException;
import java.sql.Types;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import com.xenon.administration.domain.LDAPDetails;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.buildmanager.domain.UserDetails;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

/**
 * 
 * @author bhagyashri.ajmera
 *
 */
public class LdapDAOImpl implements LdapDAO, XenonQuery {

	@Autowired
	DatabaseDao database;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired 
	UserDAO userDAO;

	private static final Logger logger = LoggerFactory.getLogger(LdapDAOImpl.class);

	@Override
	public Map<String, Object> insertLdapDetails(LDAPDetails ldapDetails,String schema) throws SQLException {
		logger.info("Inserting ldap details to database ");
		SqlParameter ldapUrlTemp = new SqlParameter("ldapUrl", Types.VARCHAR);
		SqlParameter rootDnTemp = new SqlParameter("rootDn", Types.VARCHAR);
		SqlParameter userSearchBaseTemp = new SqlParameter("userSearchBase", Types.VARCHAR);
		SqlParameter userSearchFilterTemp = new SqlParameter("userSearchFilter", Types.VARCHAR);
		//SqlParameter ldapUserTemp = new SqlParameter("ldapUser", Types.VARCHAR);
		SqlParameter userDnTemp = new SqlParameter("userDn", Types.VARCHAR);
		SqlParameter userPasswordTemp = new SqlParameter("userPassword", Types.VARCHAR);
		SqlParameter displayNameAttributeTemp = new SqlParameter("displayNameAttribute", Types.VARCHAR);
		SqlParameter emailAddrAttributeTemp = new SqlParameter("emailAddrAttribute", Types.VARCHAR);
		SqlParameter statusValTemp = new SqlParameter("statusVal", Types.VARCHAR);
		SqlParameter emailDomainNameTemp = new SqlParameter("emailDomainName", Types.VARCHAR);
		SqlParameter customerIdTemp = new SqlParameter("customerId", Types.VARCHAR);
		SqlParameter userMailTemp = new SqlParameter("userMail", Types.VARCHAR);
		SqlParameter userNameTemp = new SqlParameter("userName", Types.VARCHAR);
		SqlParameter[] paramArray = { ldapUrlTemp,rootDnTemp,userSearchBaseTemp,userSearchFilterTemp,userDnTemp,userPasswordTemp,displayNameAttributeTemp,emailAddrAttributeTemp,statusValTemp,
				emailDomainNameTemp,customerIdTemp,userMailTemp,userNameTemp};
		logger.info("Query param = LdapUrl, RootDn, UserSearchBase, UserSearchFilter, UserDn, UserPassword,UserMail = "+ldapDetails.getLdapUrl(), ldapDetails.getRootDn(), ldapDetails.getUserSearchBase(), ldapDetails.getUserSearchFilter(), ldapDetails.getUserDn(), ldapDetails.getUserPassword(),ldapDetails.getUserMail());
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("insertLdapDetails")
				.declareParameters(paramArray).withCatalogName("`xenon-core`");
		Map<String, Object> adminDetails = getLdapAdminUserDetails(ldapDetails.getLdapUrl(), ldapDetails.getRootDn(), ldapDetails.getUserSearchBase(), ldapDetails.getUserSearchFilter(), ldapDetails.getUserDn(), ldapDetails.getUserPassword(),ldapDetails.getUserMail());
		
		
		Map<String, Object> ldapResult = simpleJdbcCall.execute(ldapDetails.getLdapUrl(),ldapDetails.getRootDn(),
				ldapDetails.getUserSearchBase(),ldapDetails.getUserSearchFilter(),ldapDetails.getUserDn(),ldapDetails.getUserPassword(),
				ldapDetails.getDisplayNameAttribute(),ldapDetails.getEmailAddrAttribute(),ldapDetails.getStatus(),ldapDetails.getEmailDomainName(),ldapDetails.getCustId(),
				ldapDetails.getUserMail(),adminDetails.get("UserName").toString());
			ldapResult =updateLdapAdminUserDetails(adminDetails.get("UserName").toString(), schema);
		return ldapResult;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public boolean testLdapDetails(String ldapURL, String rootDN, String userSearchBase, String ldapSearchFilter,
			String userDn, String userPassword,String ldapUserMail) {
		logger.info("Testing LDAP settings for user ldapURL = "+ldapURL+"rootDN ="+rootDN+"userSearchBase = "+userSearchBase+"ldapSearchFilter = "+ldapSearchFilter
				+"userDn = "+userDn+"userPassword = "+userPassword+"ldapUserMail = "+ldapUserMail);
		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		String url = ldapURL;
		String ldapUrl = url + "/" + rootDN;
		if (userSearchBase != null)
			if (!userSearchBase.isEmpty() || userSearchBase.length() != 0)
				ldapUrl = url + "/" + userSearchBase + "," + rootDN;
		env.put(Context.PROVIDER_URL, ldapUrl);
		env.put(Context.SECURITY_PRINCIPAL, userDn);
		env.put(Context.SECURITY_CREDENTIALS, userPassword);
		env.put(Context.REFERRAL, "follow");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");

		try {
			new InitialDirContext(env);
			logger.info("User LDAP Authentication Successful");
			return true;
		} catch (Exception e) {
			logger.error("User LDAP Authentication failed ===" + e.getMessage());
			return false;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	@Override
	public Map<String, Object> authenticateLdapUser(String ldapURL, String rootDN, String userSearchBase,
			String ldapSearchFilter, String managerDn, String managerPass, String userDn, String userPassword,
			boolean IsNewUser) {
		logger.info("Authenticating LDAP settings for user ldapURL = "+ldapURL+"rootDN ="+rootDN+"userSearchBase = "+userSearchBase+"ldapSearchFilter = "+ldapSearchFilter
				+"userDn = "+userDn+"userPassword = "+userPassword);
		
		Map<String, Object> details = new HashMap<String, Object>();
		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		String url = ldapURL;
		String ldapUrl = url + "/" + rootDN;
		if (userSearchBase != null)
			if (!userSearchBase.isEmpty() || userSearchBase.length() != 0)
				ldapUrl = url + "/" + userSearchBase + "," + rootDN;
		env.put(Context.PROVIDER_URL, ldapUrl);
		env.put(Context.SECURITY_PRINCIPAL, managerDn);
		env.put(Context.SECURITY_CREDENTIALS, managerPass);
		env.put(Context.REFERRAL, "follow");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");

		DirContext ctx;
		try {
			ctx = new InitialDirContext(env);
		} catch (Exception e) {
			logger.info("======================================" + e.getMessage());
			logger.info("Manager Authentication failed");
			e.printStackTrace();
			details.put("authFlag", false);
			return details;
		}
		List<String> list = new LinkedList<String>();
		NamingEnumeration results = null;
		try {
			SearchControls controls = new SearchControls();
			controls.setCountLimit(1);
			controls.setTimeLimit(5000);
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			results = ctx.search("", MessageFormat.format(
					"(&(objectCategory=person)(objectClass=user)(" + ldapSearchFilter + "))", userDn), controls);
			if (results.hasMore()) {
				SearchResult searchResult = (SearchResult) results.next();
				Attributes attributes = searchResult.getAttributes();
				String namesToDisplay = ldapSearchFilter.split("=")[0];
				Attribute attr = attributes.get(namesToDisplay);
				String names = attr.get().toString();
				Attribute dnAttr = attributes.get("distinguishedName");
				String dn = (String) dnAttr.get();
				// User Exists, Validate the Password
				env.put(Context.SECURITY_PRINCIPAL, dn);
				env.put(Context.SECURITY_CREDENTIALS, userPassword);
				new InitialDirContext(env);
				details.put("authFlag", true);
				if (IsNewUser) {
					UserDetails userDetails = new UserDetails();
					userDetails.setUserName(userDn);
					userDetails.setEmailId(attributes.get("mail").get().toString());
					userDetails.setFirstName(attributes.get("cn").get().toString());
					userDetails.setLastName("");
					details.put("UserDetails", userDetails);
				}
				return details;
			} else {
				details.put("authFlag", false);
				return details;
			}

		} catch (Exception e) {
			logger.info("User Authentication failed");
			e.printStackTrace();
			details.put("authFlag", false);
			return details;
		}
	}

	@Override
	public int updateCustomerLdapStatus(int custId,String custSchema) {
		logger.info("Updating customer status by customer id");
		String sql = ldap.updateCustomerLdapStatus;
		logger.info("Running Sql query = " + sql);
		int status = database.update(sql, new Object[] { 2, custId }, new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Customer update status = { }", status);
		 sql = String.format(ldap.updateAllUsersPassword, custSchema);
		logger.info("Running Sql query = " + sql);
		 status = database.update(sql);
		return status;
	}

	@Override
	public List<Map<String, Object>> getCustomerLdapDetails(int custId) {
		logger.info("Updating customer status by customer id");
		String sql = ldap.getCustomerLdapDetails;
		logger.info("Running Sql query = " + sql);
		List<Map<String, Object>> details = database.select(sql, new Object[] { custId }, new int[] { Types.INTEGER });
		logger.info("Customer details = { }", details);
		return details;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<Map<String, Object>> getLdapUserList(String ldapURL, String rootDN, String userSearchBase, String ldapSearchFilter,
			String userDn, String userPassword, String custSchema) {
		logger.info("Reading LDAP settings for user ldapURL = "+ldapURL+"rootDN ="+rootDN+"userSearchBase = "+userSearchBase+"ldapSearchFilter = "+ldapSearchFilter
				+"userDn = "+userDn+"userPassword = "+userPassword);
		
		List<Map<String, Object>> userDetailsList=new ArrayList<Map<String,Object>>();
		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		String url = ldapURL;
		String ldapUrl = url + "/" + rootDN;
		if (userSearchBase != null)
			if (!userSearchBase.isEmpty() || userSearchBase.length() != 0)
				ldapUrl = url + "/" + userSearchBase + "," + rootDN;
		env.put(Context.PROVIDER_URL, ldapUrl);
		env.put(Context.SECURITY_PRINCIPAL, userDn);
		env.put(Context.SECURITY_CREDENTIALS, userPassword);
		env.put(Context.REFERRAL, "follow");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		DirContext ctx = null;
		boolean flag = false;
		try {
			ctx = new InitialDirContext(env);
			logger.info("User LDAP Authentication Successful");
			flag = true;
		} catch (Exception e) {
			logger.error("User LDAP Authentication failed ===" + e.getMessage());
			flag = false;
		}
		if (flag) {
			NamingEnumeration results = null;
			try {
				logger.info("reading user details");
				SearchControls controls = new SearchControls();
				controls.setCountLimit(50000);
				controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
				controls.setTimeLimit(0);
				//MessageFormat.format("(&(objectCategory=person)(objectClass=user)(mail=*)(!(mail={0})))","bhagyashri.ajmera@jadeglobal.com")
				List<Map<String, Object>> ldapActiveUserList = userDAO.getAllLdapActiveUserDetails(custSchema);
				
				results = ctx.search("", "(&(objectCategory=person)(objectClass=user)(mail=*))", controls);

				while (results.hasMore()) {
					Map<String, Object> userDetails= new HashMap<String, Object>();
					SearchResult searchResult = (SearchResult) results.next();
					Attributes attributes = searchResult.getAttributes();
					String namesToDisplay = ldapSearchFilter.split("=")[0];
					Attribute attr = attributes.get(namesToDisplay);
					if (attr != null) {
						String name = attr.get().toString();
						userDetails.put("user_name", name);
						userDetails.put("mail", attributes.get("mail").get());
						userDetailsList.add(userDetails);
					}

				}
				
				
				for(int cnt1=0;cnt1<ldapActiveUserList.size();cnt1++){
					boolean foundFlag=false;
					for(int cnt2=0;cnt2<userDetailsList.size();cnt2++){
						
						if(userDetailsList.get(cnt2).get("user_name").toString().equalsIgnoreCase(ldapActiveUserList.get(cnt1).get("user_name").toString())){
							userDetailsList.remove(cnt2);
							foundFlag=true;
						}else{
							foundFlag=false;
						}
						
						if(foundFlag){
							break;
						}
						
					}
				}
			} catch (Exception e) {
				// The base context was not found.
				// Just clean up and exit.
				e.printStackTrace();
				logger.info("Something went wrong while reading user details");

			} finally {
				if (results != null) {
					try {
						results.close();
					} catch (Exception e) {
					}
				}
				if (ctx != null) {
					try {
						ctx.close();
					} catch (Exception e) {
					}
				}
			}
		}
		return userDetailsList;
	}
	
	
	@Override
	public int mapXenonUserToLdapUser(String[] xenonUserList,String[] ldapUserList,String custSchema) {
		logger.info("Mapping xeon users with ldap users");
		for(int cnt=0;cnt<ldapUserList.length;cnt++ ){
			if(!ldapUserList[cnt].equalsIgnoreCase("None")){
				logger.info("Reads optimized edit user data");
				SqlParameter xenonUserTemp = new SqlParameter("xenonUser", Types.VARCHAR);
				SqlParameter ldapUserTemp = new SqlParameter("ldapUser", Types.VARCHAR);
				SqlParameter custSchemaTemp = new SqlParameter("custSchema", Types.VARCHAR);
				SqlParameter[] paramArray = { xenonUserTemp,ldapUserTemp,custSchemaTemp };
		
				SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("mapXenonUserToLdapUser")
						.declareParameters(paramArray).withCatalogName(custSchema);
		
				Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(xenonUserList[cnt],ldapUserList[cnt],custSchema);
				logger.info("Return list = " + simpleJdbcCallResult);
			}else{
				logger.info("Updating user status");
				String sql = String.format(ldap.deactivateUnmappedUser,custSchema);
				logger.info("Running Sql query = " + sql);
				int status = database.update(sql, new Object[] { xenonUserList[cnt] }, new int[] { Types.VARCHAR });
				logger.info("User details = { }", status);
			}
			
		}
		return 1;
		
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, Object> getLdapAdminUserDetails(String ldapURL, String rootDN, String userSearchBase,
			String ldapSearchFilter, String managerDn, String managerPass,String ldapUserMail) {
		Map<String, Object> details = new HashMap<String, Object>();
		details.put("UserFound", false);
		details.put("authFlag", false);
		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		String url = ldapURL;
		String ldapUrl = url + "/" + rootDN;
		if (userSearchBase != null)
			if (!userSearchBase.isEmpty() || userSearchBase.length() != 0)
				ldapUrl = url + "/" + userSearchBase + "," + rootDN;
		env.put(Context.PROVIDER_URL, ldapUrl);
		env.put(Context.SECURITY_PRINCIPAL, managerDn);
		env.put(Context.SECURITY_CREDENTIALS, managerPass);
		env.put(Context.REFERRAL, "follow");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");

		DirContext ctx;
		try {
			ctx = new InitialDirContext(env);
		} catch (Exception e) {
			logger.info("======================================" + e.getMessage());
			logger.info("Manager Authentication failed");
			e.printStackTrace();
			details.put("authFlag", false);
			return details;
		}
		NamingEnumeration results = null;
		try {
			
			SearchControls controls = new SearchControls();
			controls.setCountLimit(1);
			controls.setTimeLimit(5000);
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			results = ctx.search("", MessageFormat.format(
					"(&(objectCategory=person)(objectClass=user)(mail={0}))",ldapUserMail), controls);
			
			if (results.hasMore()) {
				SearchResult searchResult = (SearchResult) results.next();
				Attributes attributes = searchResult.getAttributes();
				String namesToDisplay = ldapSearchFilter.split("=")[0];
				Attribute attr = attributes.get(namesToDisplay);
				String names = attr.get().toString();
				details.put("UserName", names);
				details.put("UserFound", true);
				}
			else {
				details.put("UserFound", false);
			}
				return details;
			

		} catch (Exception e) {
			logger.info("User Authentication failed");
			e.printStackTrace();
			details.put("authFlag", false);
			return details;
		}
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public Map<String, Object> getLdapAdminUserDetails(String ldapUserMail, DirContext ctx, String ldapSearchFilter) {
		Map<String, Object> details = new HashMap<String, Object>();
		NamingEnumeration results = null;
		try {
			
			SearchControls controls = new SearchControls();
			controls.setCountLimit(1);
			controls.setTimeLimit(5000);
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			results = ctx.search("", 
					"(&(objectCategory=person)(objectClass=user)(mail ="+ldapUserMail + "))", controls);
			
			if (results.hasMore()) {
				SearchResult searchResult = (SearchResult) results.next();
				Attributes attributes = searchResult.getAttributes();
				String namesToDisplay = ldapSearchFilter.split("=")[0];
				Attribute attr = attributes.get(namesToDisplay);
				String names = attr.get().toString();
				details.put("userName", names);
				}
				return details;
			

		} catch (Exception e) {
			logger.info("User Authentication failed");
			e.printStackTrace();
			details.put("authFlag", false);
			return details;
		}
	}

	@Override
	public Map<String, Object> updateLdapAdminUserDetails(String userName,String schema) throws SQLException {
		logger.info("Inserting ldap details to database ");
		SqlParameter userNameTemp = new SqlParameter("userName", Types.VARCHAR);
		SqlParameter[] paramArray = { userNameTemp};

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("updateLdapAdminUserDetails")
				.declareParameters(paramArray).withCatalogName(schema);
		
		Map<String, Object> ldapResult = simpleJdbcCall.execute(userName);

		logger.info("Return list = " + ldapResult);
		return ldapResult;
	}
	
}

package com.xenon.administration.dao;

import java.util.List;
import java.util.Map;

public interface JiraInstanceDao {

	public int insertJiraInstance(String username,String password, String schema);

	List<Map<String, Object>> getJiraDetails(String schema);

	public int updatJiraInstance(String username, String password, int jiraId, String schema);
}

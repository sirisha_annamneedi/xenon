package com.xenon.administration.dao;

import java.util.List;
import java.util.Map;

import com.xenon.administration.domain.JiraInstanceReleaseDetails;

public interface JiraInstanceReleaseDao {

	int insertJiraInstanceRelease(JiraInstanceReleaseDetails jiraReleaseDetails, String schema);

	List<Map<String, Object>> getJiraRelease(String releaseId, String schema);

	int updateJiraInstanceRelease(JiraInstanceReleaseDetails jiraInstanceRelDetails, String schema);
	
	

}

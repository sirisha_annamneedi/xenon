package com.xenon.administration.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.xenon.administration.domain.GitRepositoryDetails;
import com.xenon.database.XenonQuery;

public class GitRepositoryDaoImpl implements GitRepositoryDao,XenonQuery{
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(GitInstanceDaoImpl.class);
	
	@Autowired
	private com.xenon.database.DatabaseDao database;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Map<String, Object>> getRepositories(String userId, String ProjectId,String schema) {
		
		logger.info("SQL Query to get all Git repositories");
		List<Map<String, Object>> instanceList;
		String sql = String.format(gitDetails.getAllRepositories, schema, schema);
		instanceList = database.select(sql, new Object[] {ProjectId}, new int[] { Types.INTEGER  });
		logger.info("Query Result " + instanceList);
		return instanceList;
	}

	@Override
	public List<Map<String, Object>> getRepositoryById(String repositoryId, String schema) {
		logger.info("SQL Query to get repository information for "+ repositoryId);
		List<Map<String, Object>> instanceList;
		String sql = String.format(gitDetails.getRepositoryByID, schema);
		instanceList = database.select(sql, new Object[] { repositoryId }, new int[] { Types.INTEGER });
		logger.info("Query Result " + instanceList);
		return instanceList;
	}

	@Override
	public int updateGitRepository(GitRepositoryDetails gitRepositoryDetails, String schema) {
		int flag = database.update(String.format(gitDetails.updateGitRepository, schema),
				new Object[] {gitRepositoryDetails.getInstanceId(),gitRepositoryDetails.getRepositoryName(),gitRepositoryDetails.getBranchName(),gitRepositoryDetails.getRepositoryId()},
				new int[] { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.INTEGER});

		logger.info("Result : " + flag);
		return flag;
	}
}

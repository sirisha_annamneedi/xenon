package com.xenon.administration.dao;

import com.xenon.administration.domain.LDAPDetails;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.naming.directory.DirContext;

/**
 * 
 * @author bhagyashri.ajmera
 *
 */
public interface LdapDAO {
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * @param ldapDetails
	 * @param schema 
	 * @return
	 * @throws SQLException
	 */

	Map<String, Object> insertLdapDetails(LDAPDetails ldapDetails, String schema) throws SQLException;

	boolean testLdapDetails(String ldapBaseURL, String ldapRootDN, String userSearchBase, String ldapSearchFilter,
			String managerDn, String managerPass, String ldapUserMail);
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method authenticates ldap user
	 * 
	 * @param ldapURL
	 * @param rootDN
	 * @param userSearchBase
	 * @param ldapSearchFilter
	 * @param managerDn
	 * @param managerPass
	 * @param userDn
	 * @param userPassword
	 * @return
	 */

	Map<String, Object> authenticateLdapUser(String ldapURL, String rootDN, String userSearchBase,
			String ldapSearchFilter, String managerDn, String managerPass, String userDn, String userPassword,
			boolean IsNewUser);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method updates customer ldap status
	 * 
	 * @param custId
	 * @return
	 */
	int updateCustomerLdapStatus(int custId, String custSchema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives customer ldap details
	 * 
	 * @param custId
	 * @return
	 */
	List<Map<String, Object>> getCustomerLdapDetails(int custId);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads all users from LDAP
	 * 
	 * @param ldapURL
	 * @param rootDN
	 * @param userSearchBase
	 * @param ldapSearchFilter
	 * @param userDn
	 * @param userPassword
	 * @return
	 */
	List<Map<String, Object>> getLdapUserList(String ldapURL, String rootDN, String userSearchBase,
			String ldapSearchFilter, String userDn, String userPassword, String custSchema);

	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method maps xenon user to ldap user
	 * 
	 * @param xenonUser
	 * @param ldapUser
	 * @param custSchema
	 * @return
	 */
	int mapXenonUserToLdapUser(String[] xenonUser, String[] ldapUser, String custSchema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is for read ldap admin details
	 * 
	 * @param ldapURL
	 * @param rootDN
	 * @param userSearchBase
	 * @param ldapSearchFilter
	 * @param managerDn
	 * @param managerPass
	 * @return
	 */
	Map<String, Object> getLdapAdminUserDetails(String ldapURL, String rootDN, String userSearchBase,
			String ldapSearchFilter, String managerDn, String managerPass, String ldapUserMail);

	/**
	 * 
	 * @param ldapUserMail
	 * @param ctx
	 * @param ldapSearchFilter
	 * @return
	 */
	Map<String, Object> getLdapAdminUserDetails(String ldapUserMail, DirContext ctx, String ldapSearchFilter);

	/**
	 * 
	 * @param userName
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	Map<String, Object> updateLdapAdminUserDetails(String userName, String schema) throws SQLException;

}

package com.xenon.administration.domain;

public class JiraRequestBodyDetails {

	String url;
	String username;
	String password;
	
	public String getURL(){
		return this.url;
	}
	
	public void setURL(String url){
		this.url=url;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public void setUsername(String username){
		this.username=username;
	}
	
	public String getPassword(){
		return this.password;
	}
	
	public void setPassword(String password){
		this.password=password;
	}
}

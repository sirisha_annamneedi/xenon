package com.xenon.administration.domain;

public class GitRepositoryDetails {
	Integer instanceId;
	String branchName;
	String repositoryName;
	String userId;
	Integer repositoryId;
	
	public Integer getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(Integer instanceId) {
		this.instanceId = instanceId;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getRepositoryName() {
		return repositoryName;
	}
	public void setRepositoryName(String repositoryName) {
		this.repositoryName = repositoryName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Override
	public String toString() {
		return "GitRepositoryDetails [instanceId=" + instanceId + ", branchName=" + branchName + ", repositoryName="
				+ repositoryName + ", userId=" + userId + ", repositoryId=" + repositoryId + "]";
	}
	public Integer getRepositoryId() {
		return repositoryId;
	}
	public void setRepositoryId(Integer repositoryId) {
		this.repositoryId = repositoryId;
	}

	
}

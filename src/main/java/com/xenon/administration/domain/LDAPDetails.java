package com.xenon.administration.domain;
/**
 * 
 * @author bhagyashri.ajmera
 * @description Domain class to declare getter setter methods for LDAP 
 *              
 * 
 */
public class LDAPDetails {

	private int ldapId;
	private String ldapUrl;
	private String rootDn;
	private String userSearchBase;
	private String userSearchFilter;
	private String userDn;
	private String userPassword;
	private String displayNameAttribute;
	private String emailAddrAttribute;
	private String emailDomainName;
	private int status;
	private int custId;
	private String userMail;
	public int getLdapId() {
		return ldapId;
	}
	public void setLdapId(int ldapId) {
		this.ldapId = ldapId;
	}
	public String getLdapUrl() {
		return ldapUrl;
	}
	public void setLdapUrl(String ldapUrl) {
		this.ldapUrl = ldapUrl;
	}
	public String getRootDn() {
		return rootDn;
	}
	public void setRootDn(String rootDn) {
		this.rootDn = rootDn;
	}
	public String getUserSearchBase() {
		return userSearchBase;
	}
	public void setUserSearchBase(String userSearchBase) {
		this.userSearchBase = userSearchBase;
	}
	public String getUserSearchFilter() {
		return userSearchFilter;
	}
	public void setUserSearchFilter(String userSearchFilter) {
		this.userSearchFilter = userSearchFilter;
	}
	public String getUserDn() {
		return userDn;
	}
	public void setUserDn(String userDn) {
		this.userDn = userDn;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getDisplayNameAttribute() {
		return displayNameAttribute;
	}
	public void setDisplayNameAttribute(String displayNameAttribute) {
		this.displayNameAttribute = displayNameAttribute;
	}
	public String getEmailAddrAttribute() {
		return emailAddrAttribute;
	}
	public void setEmailAddrAttribute(String emailAddrAttribute) {
		this.emailAddrAttribute = emailAddrAttribute;
	}
	public String getEmailDomainName() {
		return emailDomainName;
	}
	public void setEmailDomainName(String emailDomainName) {
		this.emailDomainName = emailDomainName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public String getUserMail() {
		return userMail;
	}
	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}
	
}
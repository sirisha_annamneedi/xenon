package com.xenon.administration.domain;

public class GitProjectDetails {
	String gitProjectName;
	String gitUrl;
	String userName;
	String apiToken;
	String gitStatus;
	int userId;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getGitProjectName() {
		return gitProjectName;
	}
	public void setGitProjectName(String gitName) {
		this.gitProjectName = gitName;
	}
	public String getGitUrl() {
		return gitUrl;
	}
	public void setGitUrl(String gitUrl) {
		this.gitUrl = gitUrl;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getApiToken() {
		return apiToken;
	}
	public void setApiToken(String apiToken) {
		this.apiToken = apiToken;
	}
	public String getGitStatus() {
		return gitStatus;
	}
	public void setGitStatus(String gitStatus) {
		this.gitStatus = gitStatus;
	}
	
	
}

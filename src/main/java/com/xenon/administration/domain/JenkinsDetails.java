package com.xenon.administration.domain;

public class JenkinsDetails {
	private int instanceId;
	private String jenkinsJobName;
	private String jobName;
	private String jobDescription;
	private int jobStatus;
	private int buildStatus;
	private String buildToken;
	private int jobId;
	
	public int getJobId() {
		return jobId;
	}
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}
	public int getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(int instanceId) {
		this.instanceId = instanceId;
	}
	public String getjenkinsJobName() {
		return jenkinsJobName;
	}
	public void setjenkinsJobName(String jenkinsJobName) {
		this.jenkinsJobName = jenkinsJobName;
	}
	
	public String getjobName() {
		return jobName;
	}
	public void setjobName(String jobName) {
		this.jobName = jobName;
	}
	
	public String getjobDescription() {
		return jobDescription;
	}
	public void setjobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}
	
	public int getjobStatus() {
		return jobStatus;
	}
	public void setjobStatus(int jobStatus) {
		this.jobStatus = jobStatus;
	}
	
	public int getbuildStatus() {
		return buildStatus;
	}
	public void setbuildStatus(int buildStatus) {
		this.buildStatus = buildStatus;
	}
	
	public String getbuildToken() {
		return buildToken;
	}
	public void setbuildToken(String buildToken) {
		this.buildToken = buildToken;
	}
}

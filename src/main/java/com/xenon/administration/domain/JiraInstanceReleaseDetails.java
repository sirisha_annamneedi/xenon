package com.xenon.administration.domain;

public class JiraInstanceReleaseDetails {
	private String jiraReleaseVersionName;
	private String jiraProjectKey;
	private int jiraReleaseStatus;
	private String jiraReleaseStartDate;
	private String jiraReleaseDate;
	private String releaseDescription;
	private String releaseVersionId;;
	
	public String getJiraReleaseVersionName() {
		return jiraReleaseVersionName;
	}
	public void setJiraReleaseVersionNam(String jiraReleaseVersionName) {
		this.jiraReleaseVersionName = jiraReleaseVersionName;
	}
	
	public String getJiraProjectKey() {
		return jiraProjectKey;
	}
	public void setJiraProjectKey(String jiraProjectKey) {
		this.jiraProjectKey = jiraProjectKey;
	}
	
	public int getJiraReleaseStatus() {
		return jiraReleaseStatus;
	}
	public void setJiraReleaseStatus(int jiraReleaseStatus) {
		this.jiraReleaseStatus = jiraReleaseStatus;
	}
	
	public String getJiraReleaseStartDate() {
		return jiraReleaseStartDate;
	}
	public void setJiraReleaseStartDate(String jiraReleaseStartDate) {
		this.jiraReleaseStartDate = jiraReleaseStartDate;
	}
	
	public String getJiraReleaseDate() {
		return jiraReleaseDate;
	}
	public void setJiraReleaseDate(String jiraReleaseDate) {
		this.jiraReleaseDate = jiraReleaseDate;
	}
	

	public String getReleaseDescription() {
		return releaseDescription;
	}
	public void setReleaseDescription(String releaseDescription) {
		this.releaseDescription = releaseDescription;
	}
	public String getJiraReleaseVersionId() {
		return releaseVersionId;
	}
	public void setJiraReleaseVersionId(String releaseVersionId) {
		this.releaseVersionId = releaseVersionId;
	}
}

package com.xenon.constants;

/**
 * @author shantaram.tupe
 * 26-Feb-2020 11:54:49 AM
 */
public enum ExecutionType {
	/**
	 * result of 
	 * SELECT * FROM xetm_execution_type;
	 */
	MANUAL(1),AUTOMATION(2),BOTH(3),API(4),REDWOOD(5);
	private final int value;
	private ExecutionType( int value ) {
		this.value = value;
	}
	public int getValue() {
		return this.value;
	}
	/*public String getValueString() {
		return String.valueOf(this.value);
	}*/
	/*@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.valueOf( this.value );
	}*/
}

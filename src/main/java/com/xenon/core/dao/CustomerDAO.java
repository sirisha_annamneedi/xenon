package com.xenon.core.dao;

import java.util.List;
import java.util.Map;

import com.xenon.buildmanager.domain.UserDetails;
import com.xenon.core.domain.CustomerDetails;

public interface CustomerDAO {

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method checks customer status
	 * 
	 * @param custId
	 * @return
	 */
	boolean checkCustStatus(int custId);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method updates customer status
	 * 
	 * @param custId
	 * @return
	 */
	int updateCustStatus(int custId);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method inserts customer details
	 * 
	 * @param custDetails
	 * @return
	 */
	int insertCustomerDetails(CustomerDetails custDetails);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives customer id by details
	 * 
	 * @param custDetails
	 * @return
	 */
	int getCustomerIdByDetails(CustomerDetails custDetails);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives customer details by customer id
	 *  
	 * @param custId
	 * @return
	 */
	List<Map<String, Object>> getCustomerDetailsById(int custId);
	
	/**
	 ** @author bhagyashri.ajmera
	 * 
	 * This method edits customer details
	 * 
	 * @param custDetails
	 * @return
	 */
	int editCustomerDetails(CustomerDetails custDetails);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads all customer details
	 * 
	 * @return
	 */
	List<Map<String, Object>> getAllCustomers();
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads all customer admin details
	 * 
	 * @param listCust
	 * @return
	 */
	List<Map<String, Object>> getAllCustomerAdmins(
			List<Map<String, Object>> listCust);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method edits customer logo
	 * 
	 * @param custDetails
	 * @return
	 */
	int editCustomerLogo(CustomerDetails custDetails);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives customer details by user email id
	 * 
	 * @param emailId
	 * @return
	 */
	int getCustomerIdByEmailId(String emailId);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method sends mail
	 * 
	 * @param user
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	String sendMail(UserDetails user, String schema) throws Exception;
	
	/**
	 * 
	 * This method reads cloud space
	 * 
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getCloudeSpace(String schema);
	
	/**
	 * This method gives the user count
	 * 
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getUserCount(String schema);
	
	/**
	 * This method gives the project count
	 * 
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getProjectCount(String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads account setting data
	 * 
	 * @param customerId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getAccountSettingsData(int customerId, String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method updates automation status by schema name
	 * 
	 * @param automationStatus
	 * @param schema
	 * @return
	 */
	Map<String, Object> updateAutomationStatus(int automationStatus, int scriptlessStatus, String schema);
	
	/**
	  * @author bhagyashri.ajmera
	 * 
	 * This method updates automation status by customer id 
	 * 
	 * @param custId
	 * @return
	 */
	Map<String, Object> updateAutomationStatus(int custId);

	String getCustomerSchema(String customerID);

	int editCustomerJiraDetails(CustomerDetails custDetails);
}

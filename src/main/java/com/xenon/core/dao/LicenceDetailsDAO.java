package com.xenon.core.dao;

import java.util.List;
import java.util.Map;
/**
 * @author bhagyashri.ajmera
 * 
 * This interface handles all licence details related operation
 * 
 */
public interface LicenceDetailsDAO {

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to read licence details by type
	 * 
	 * @param typeId
	 * @return
	 */
	List<Map<String, Object>> getLicenceDetailsByTypeId(int typeId);

}

package com.xenon.core.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.database.DatabaseDao;

public class LicenceDetailsDAOImpl implements LicenceDetailsDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(LicenceDetailsDAOImpl.class);
	
	@Autowired
	DatabaseDao database;
	
	@Override
	public List<Map<String, Object>> getLicenceDetailsByTypeId(int typeId) {
		logger.info("Reading licence details by id");
		String sql = "SELECT * from `xenon-core`.xe_licence_details where type_id=?";
		List<Map<String, Object>> licenceDetails = database.select(sql,new Object[]{typeId},new int[]{Types.INTEGER});
		logger.info("Return list ="+licenceDetails);
		return licenceDetails;
	}
}

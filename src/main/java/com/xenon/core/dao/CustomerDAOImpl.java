package com.xenon.core.dao;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.api.common.MailService;
import com.xenon.buildmanager.domain.UserDetails;
import com.xenon.common.dao.MailDAO;
import com.xenon.core.domain.CustomerDetails;
import com.xenon.database.DatabaseDao;

/**
 * This class includes the customer DAO implementation
 * 
 * @author bhagyashri.ajmera
 *
 */
public class CustomerDAOImpl implements CustomerDAO {

	private static final Logger logger = LoggerFactory.getLogger(CustomerDAOImpl.class);

	@Autowired
	DatabaseDao database;

	@Autowired
	MailService mailService;

	@Autowired
	MailDAO mailDAO;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int updateCustStatus(int custId) {
		logger.info("Updating customer status by customer id");
		String sql = "Update xe_customer_details SET customer_status=? WHERE customer_id=?";
		logger.info("Running Sql query = " + sql);
		int status = database.update(sql, new Object[] { 2, custId }, new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("Customer update status = { }", status);
		return status;
	}

	@Override
	public boolean checkCustStatus(int custId) {
		logger.info("Checking customer status");
		String sql = "SELECT * FROM xe_customer_details where customer_id=?";
		logger.info("Running Sql query = " + sql);
		List<Map<String, Object>> listCust = database.select(sql, new Object[] { custId }, new int[] { Types.INTEGER });
		logger.info("Customer Status = " + Integer.parseInt(listCust.get(0).get("customer_status").toString()));
		if (Integer.parseInt(listCust.get(0).get("customer_status").toString()) == 1)
			return true;
		else
			return false;
	}

	@Override
	public int insertCustomerDetails(CustomerDetails custDetails) {
		logger.info("Inserting customer details");
		String query = "insert into xe_customer_details (customer_id,customer_name,customer_address,customer_logo, customer_type,created_date, start_date, end_date, tm_status, bt_status,scriptless_status,on_premise, customer_status,customer_schema,automation_status,jenkin_status,git_status,redwood_status,api_status,redwood_url) "
				+ "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		logger.info("Running query = " + query);
		int flag = database.update(query,
				new Object[] { 0, custDetails.getName(), custDetails.getAddress(), custDetails.getLogoUrl(),
						custDetails.getCustomerType(), custDetails.getCreatedDate(), custDetails.getStartDate(),
						custDetails.getEndDate(), custDetails.getTmStatus(), custDetails.getBtStatus(),
						custDetails.getScriptlessStatus(), custDetails.getOnPremiseStatus(),
						custDetails.getCustStatus(), custDetails.getCustomerSchema(), custDetails.getAutomationStatus(),
						custDetails.getJenkinStatus(), custDetails.getGitStatus(), custDetails.getRedwoodStatus(),
						custDetails.getApiStatus(), custDetails.getRedwoodUrl() },
				new int[] { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.BLOB, Types.INTEGER, Types.VARCHAR,
						Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.INTEGER, Types.VARCHAR });
		logger.info("Insert status ={}", flag);
		if (flag != 0)
			return getCustomerIdByDetails(custDetails);
		else
			return flag;
	}

	@Override
	public int getCustomerIdByDetails(CustomerDetails custDetails) {
		logger.info("Reading customer id by customer details");
		String sql = "SELECT * FROM xe_customer_details where customer_name=? and customer_address=?  and customer_type=? and start_date=? and end_date=? and tm_status=? and bt_status=? and customer_status=? and customer_schema=?";
		logger.info("Running Query = " + sql);
		List<Map<String, Object>> listCust = database.select(sql,
				new Object[] { custDetails.getName(), custDetails.getAddress(), custDetails.getCustomerType(),
						custDetails.getStartDate(), custDetails.getEndDate(), custDetails.getTmStatus(),
						custDetails.getBtStatus(), custDetails.getCustStatus(), custDetails.getCustomerSchema() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.INTEGER,
						Types.INTEGER, Types.INTEGER, Types.VARCHAR });
		logger.info("Read customer id = " + listCust.get(0).get("customer_id").toString());
		return Integer.parseInt(listCust.get(0).get("customer_id").toString());
	}

	@Override
	public List<Map<String, Object>> getCustomerDetailsById(int custId) {
		logger.info("Reading customer details by customer id");
		String sql = "SELECT customer_id,customer_name,customer_address,customer_logo,customer_type,created_date,start_date,end_date,tm_status,automation_status,bt_status,scriptless_status,on_premise,customer_status,customer_schema,DATE_FORMAT(created_date , '%d-%m-%Y %h:%i %p') as createdDate,DATE_FORMAT(start_date , '%d-%m-%Y %h:%i %p') as startDate,DATE_FORMAT(end_date , '%d-%m-%Y %h:%i %p') as endDate,jenkin_status,git_status,issue_tracker,issue_tracker_type,issue_tracker_host,redwood_status,api_status,redwood_url FROM `xenon-core`.xe_customer_details where customer_id=?";
		logger.info("Running Query = " + sql);
		List<Map<String, Object>> listCust = database.select(sql, new Object[] { custId }, new int[] { Types.INTEGER });
		logger.info("Read customer details  = " + listCust);
		return listCust;
	}

	@Override
	public int editCustomerDetails(CustomerDetails custDetails) {
		logger.info("Editing customer details");
		String query = "UPDATE xe_customer_details  SET `customer_name`=?, `customer_address`=?, `customer_type`=?, `tm_status`=?, `bt_status`=?,`scriptless_status`=?, `customer_status`=?, `start_date`=? , `end_date`=? ,`automation_status`=? ,`jenkin_status`=?, `git_status`=?,`redwood_status`=?, `api_status`=?,`redwood_url`=?  WHERE `customer_id`=?";
		logger.info("Running Query = " + query);

		int flag = database.update(query,
				new Object[] { custDetails.getName(), custDetails.getAddress(), custDetails.getCustomerType(),
						custDetails.getTmStatus(), custDetails.getBtStatus(), custDetails.getScriptlessStatus(),
						custDetails.getCustStatus(), custDetails.getStartDate(), custDetails.getEndDate(),
						custDetails.getAutomationStatus(), custDetails.getJenkinStatus(), custDetails.getGitStatus(),
						custDetails.getRedwoodStatus(), custDetails.getApiStatus(), custDetails.getRedwoodUrl(),
						custDetails.getCustId() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.INTEGER, });
		logger.info("Edit customer status = " + flag);
		return flag;
	}

	@Override
	public int editCustomerJiraDetails(CustomerDetails custDetails) {
		logger.info("Editing customer details");
		String query = "UPDATE xe_customer_details  SET `issue_tracker`=?, `issue_tracker_type`=?"
				+ ", `issue_tracker_host`=?  WHERE `customer_id`=?";
		logger.info("Running Query = " + query);
		int flag = database.update(query,
				new Object[] { custDetails.getIssueTracker(), custDetails.getissueTrackerType(),
						custDetails.getIssueTrackerHost(), custDetails.getCustId() },
				new int[] { Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.INTEGER });
		logger.info("Edit customer status = " + flag);
		return flag;
	}

	@Override
	public List<Map<String, Object>> getAllCustomers() {
		logger.info("Reading all customers");
		String sql = "SELECT * FROM xe_customer_details";
		logger.info("Running Query = " + sql);
		List<Map<String, Object>> listCust = database.select(sql);
		logger.info("All customer details list = " + listCust);
		return listCust;
	}

	@Override
	public List<Map<String, Object>> getCloudeSpace(String schema) {

		String sql = "SELECT table_schema `%s`,  round((sum(data_length + index_length) / 1024 / 1024), 2)'Used_Space', sum( data_free )/ 1024 / 1024 'Free_Space' FROM information_schema.TABLES WHERE table_schema='%s' GROUP BY table_schema";
		List<Map<String, Object>> cloudeSpace = database.select(String.format(sql, schema, schema));

		return cloudeSpace;
	}

	@Override
	public List<Map<String, Object>> getProjectCount(String schema) {

		String sql = "SELECT count(*) as project_count FROM `%s`.xe_project";
		List<Map<String, Object>> projectCount = database.select(String.format(sql, schema));

		return projectCount;
	}

	@Override
	public List<Map<String, Object>> getUserCount(String schema) {

		String sql = "SELECT count(*) as user_count FROM `%s`.xe_user_details";
		List<Map<String, Object>> userCount = database.select(String.format(sql, schema));

		return userCount;
	}

	@Override
	public List<Map<String, Object>> getAllCustomerAdmins(List<Map<String, Object>> listCust) {
		logger.info("Get all customer admins details");
		List<Map<String, Object>> listAdmins = new ArrayList<Map<String, Object>>();
		int adminRoleId = 2;
		for (int i = 0; i < listCust.size(); i++) {
			String query = String.format(
					"SELECT UD.user_id,UD.about_me,UD.user_password,UD.email_id,UD.user_photo,UD.first_name,UD.last_name,UD.role_id, A.status as user_status,A.active_id as userStatus, B.status as tm_status, B.active_id as tmStatus,C.status as bt_status,C.active_id as btStatus, R.role_type FROM `%s`.xe_user_details  AS UD INNER JOIN `%s`.xe_active as A ON UD.user_status=A.active_id INNER JOIN `%s`.xe_active as B ON UD.tm_status=B.active_id INNER JOIN `%s`.xe_active as C ON UD.bt_status=C.active_id INNER JOIN `%s`.xe_role as R ON UD.role_id=R.role_id WHERE UD.role_id=?",
					listCust.get(i).get("customer_schema").toString(),
					listCust.get(i).get("customer_schema").toString(),
					listCust.get(i).get("customer_schema").toString(),
					listCust.get(i).get("customer_schema").toString(),
					listCust.get(i).get("customer_schema").toString());
			logger.info("Running query = " + query);
			List<Map<String, Object>> listUser = database.select(query, new Object[] { adminRoleId },
					new int[] { Types.INTEGER });
			for (int count3 = 0; count3 < listUser.size(); count3++) {
				listUser.get(count3).put("customer_name", listCust.get(i).get("customer_name").toString());
				listUser.get(count3).put("customer_id",
						Integer.parseInt(listCust.get(i).get("customer_id").toString()));
				listAdmins.add(listUser.get(count3));
			}
		}
		logger.info("Admin details read successfully \nList admins = " + listAdmins);
		return listAdmins;
	}

	@Override
	public int editCustomerLogo(CustomerDetails custDetails) {
		logger.info("Editing customer logo");
		String query = "UPDATE xe_customer_details  SET `customer_logo`=LOAD_FILE(?) WHERE `customer_id`=?";
		logger.info("Running query = " + query);
		int flag = database.update(query, new Object[] { custDetails.getLogoName(), custDetails.getCustId() },
				new int[] { Types.VARCHAR, Types.INTEGER });
		logger.info("Edit customer logo status = " + flag);
		return flag;
	}

	@Override
	public int getCustomerIdByEmailId(String emailId) {
		logger.info("Reading customer id by user email id");
		List<Map<String, Object>> userDetails;
		String sql = "SELECT * FROM xe_user where user_email=?";
		logger.info("Running query = " + sql);
		userDetails = database.select(sql, new Object[] { emailId }, new int[] { Types.VARCHAR });
		if (userDetails.size() == 0)
			return 0;
		logger.info("Customer id = " + userDetails.get(0).get("customer_id"));
		return Integer.parseInt(userDetails.get(0).get("customer_id").toString());
	}

	@Override
	public String sendMail(UserDetails user, String schema) throws Exception {
		logger.info("Sending mail to created admin user");
		List<Map<String, Object>> custDetails = database.select(
				"select cust.customer_id,cust.customer_name from xe_customer_details as cust where cust.customer_schema ='"
						+ schema + "'");
		logger.info("Reading customer details of user");
		List<Map<String, Object>> emailDetails = mailDAO.xenonSMTPDetails();
		if (emailDetails.size() == 0)
			return "Mail details not available";
		String custName = custDetails.get(0).get("customer_name").toString();
		ArrayList<String> recepients = new ArrayList<String>();
		recepients.add(user.getEmailId());
		String subject = "New Customer Created : Admin Credentials for Xenon Account";

		String bodyText = "<html xmlns=\"http://www.w3.org/1999/xhtml\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"><head style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">    <meta name=\"viewport\" content=\"width=device-width\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">    <title style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">Actionable emails e.g. reset password</title>    </head><body style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;line-height: 1.6;background-color: #f6f6f6;width: 100% !important;\"><table class=\"body-wrap\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;background-color: #f6f6f6;width: 100%;\">    <tbody style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">"
				+ "<tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">        <td style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\"></td>        <td class=\"container\" width=\"600\" style=\"margin: 0 auto !important;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;display: block !important;max-width: 600px !important;clear: both !important;\">            <div class=\"content\" style=\"margin: 0 auto;padding: 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;max-width: 600px;display: block;\">                <table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;background: #fff;border: 1px solid #e9e9e9;border-radius: 3px;\">                    <tbody style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">"
				+ "<tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                        <td class=\"content-wrap\" style=\"margin: 0;padding: 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">                            <table cellpadding=\"0\" cellspacing=\"0\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                <tbody style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">"
				+ "<tr style=\"background-color: #e7e7e7;margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                    <td style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">   "
				+ "<img class=\"img-responsive\" src=\"http://xenon.jadeglobal.com/wp-content/uploads/2015/03/cropped-cropped-xeon-logo-web1.png\" "
				+ "style=\"height: 130px;  margin-left: 150px;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;"
				+ "Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;max-width: 100%;\">                                    </td>                                </tr>                                "
				+ "<tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                   "
				+ " <td class=\"content-block\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">                                        <h3 style=\"margin: 40px 0 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif;box-sizing: border-box;font-size: 18px;color: #000;line-height: 1.2;font-weight: 400;\"><br>Welcome to Xenon<br style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"><br style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"> Your new account has been created for customer '<i><b>"
				+ custName
				+ "</b></i>' </h3>                                    </td>                                </tr>                                <tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                    "
				+ "<td class=\"content-block\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">User name : <i>"
				+ user.getEmailId() + "</i>"
				+ "<br style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">Password : <i>"
				+ user.getPassword() + "</i>"
				+ "<br style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">Role : <i>Admin</i>"
				+ "<br style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"></td>                                </tr>                                <tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                    <td class=\"content-block\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\">                                        "
				+ ""
				+ "</td></tr><tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                                    <td class=\"content-block aligncenter\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;text-align: center;\">                                        <a href=\"http://xenon-core.cloudapp.net:8080/\" class=\"btn-primary\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;color: #FFF;text-decoration: none;background-color: #1ab394;border: solid #1ab394;border-width: 5px 10px;line-height: 2;font-weight: bold;text-align: center;cursor: pointer;display: inline-block;border-radius: 5px;text-transform: capitalize;\">Login to Xenon dashboard</a>                                    </td>                               "
				+ "</tr></tbody></table>                        </td>                    </tr>                "
				+ "</tbody></table>                "
				+ "<div class=\"footer\" style=\"margin: 0;padding: 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;width: 100%;clear: both;color: #999;\">                    <table width=\"100%\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                        <tbody style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\"><tr style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;\">                            <td class=\"aligncenter content-block\" style=\"margin: 0;padding: 0 0 20px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 12px;vertical-align: top;text-align: center;\"><a href=\"#\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 12px;color: #999;text-decoration: underline;\">Copyright © 2019, Jade Global, Inc. All rights reserved.</a></td>                        </tr>                    </tbody></table>                </div></div>        </td>        <td style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;\"></td>    </tr></tbody></table></body></html>";
		String from = emailDetails.get(0).get("smtp_user_id").toString();
		logger.info("Posting mail");
		String message = mailService.postMail(emailDetails, recepients, subject, bodyText, from);
		logger.info("Post mail status = " + message);
		return message;
	}

	@Override
	public Map<String, Object> getAccountSettingsData(int customerId, String schema) {
		logger.info("Reading account setting data");
		SqlParameter customerIdTemp = new SqlParameter("customerId", Types.VARCHAR);
		SqlParameter[] paramArray = { customerIdTemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getAccountSettingsData")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(customerId);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> updateAutomationStatus(int automationStatus, int scriptlessStatus, String schema) {
		logger.info("Updating automation status");
		SqlParameter automation_status = new SqlParameter("automation_status", Types.INTEGER);
		SqlParameter scriptless_status = new SqlParameter("scriptless_status", Types.INTEGER);
		SqlParameter customer_schema = new SqlParameter("customer_schema", Types.VARCHAR);
		SqlParameter[] paramArray = { automation_status, scriptless_status, customer_schema };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("updateAutomationStatus")
				.declareParameters(paramArray).withCatalogName("`xenon-core`");

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(automationStatus, scriptlessStatus, schema);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> updateAutomationStatus(int custId) {
		logger.info("Updating automation status");
		SqlParameter customerId = new SqlParameter("customerId", Types.INTEGER);
		SqlParameter[] paramArray = { customerId };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("updateAutomationStatusByCustId").declareParameters(paramArray)
				.withCatalogName("`xenon-core`");

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(custId);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public String getCustomerSchema(String customerID) {
		logger.info("SQL Query to get Customer schema");

		logger.info("Checking customer status");
		/*
		 * String sql = "SELECT * FROM xe_customer_details where customer_id=?";
		 * logger.info("Running Sql query = " + sql); List<Map<String, Object>> listCust
		 * = database.select(sql, new Object[] { custId }, new int[] { Types.INTEGER });
		 * logger.info("Customer Status = " +
		 * Integer.parseInt(listCust.get(0).get("customer_status").toString()));
		 */

		String sql = "SELECT * FROM `xenon-core`.xe_customer_details where customer_id=?";
		List<Map<String, Object>> resultSet = database.select(sql, new Object[] { customerID },
				new int[] { Types.VARCHAR });
		String customerSchema = resultSet.get(0).get("customer_schema").toString();
		logger.info("Query Result  " + customerSchema);

		return customerSchema;
	}
}

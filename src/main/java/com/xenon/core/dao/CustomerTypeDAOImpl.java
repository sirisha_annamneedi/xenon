package com.xenon.core.dao;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.database.DatabaseDao;

public class CustomerTypeDAOImpl implements CustomerTypeDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(CustomerTypeDAOImpl.class);
	
	@Autowired
	DatabaseDao database;
	
	@Override
	public List<Map<String, Object>> getAllCustomerTypes() {
		logger.info("reads all customer types");
		String sql = "SELECT * from `xenon-core`.xe_customer_type";
		List<Map<String, Object>> custTypes = database.select(sql);
		logger.info("Return list ="+custTypes);
		return custTypes;
	}
}

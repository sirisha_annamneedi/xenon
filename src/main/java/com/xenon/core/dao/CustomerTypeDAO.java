package com.xenon.core.dao;

import java.util.List;
import java.util.Map;
/**
 * 
 * @author bhagyashri.ajmera
 * 
 * This interface handles all customer type related operations
 *
 */
public interface CustomerTypeDAO {

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads all customer types
	 * 
	 * @return
	 */
	List<Map<String, Object>> getAllCustomerTypes();

}

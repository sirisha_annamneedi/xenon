package com.xenon.core.domain;

import java.util.Arrays;

public class CustomerDetails {
	private int custId;
	private String name;
	private String address;
	private byte[] logoUrl;
	private int customerType;
	private String startDate;
	private String createdDate;
	private String endDate;
	private int tmStatus;
	private int btStatus;
	private int onPremiseStatus;
	private int custStatus;
	private int automationStatus;
	private int scriptlessStatus;
	private String customerSchema;
	private String logoName;
	private int jenkinStatus;
	private int gitStatus;
	private int redwoodStatus;
	private int apiStatus;
	private int issueTracker;
	private int issueTrackerType;
	private String redwoodUrl;
	private String issueTrackerHost;
	
	
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public byte[] getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(byte[] logoUrl) {
		this.logoUrl = logoUrl;
	}
	public int getCustomerType() {
		return customerType;
	}
	public void setCustomerType(int customerType) {
		this.customerType = customerType;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getTmStatus() {
		return tmStatus;
	}
	public void setTmStatus(int tmStatus) {
		this.tmStatus = tmStatus;
	}
	public int getBtStatus() {
		return btStatus;
	}
	public void setBtStatus(int btStatus) {
		this.btStatus = btStatus;
	}
	public int getAutomationStatus() {
		return automationStatus;
	}
	public void setAutomationStatus(int automationStatus) {
		this.automationStatus = automationStatus;
	}
	
	public int getCustStatus() {
		return custStatus;
	}
	public void setCustStatus(int custStatus) {
		this.custStatus = custStatus;
	}
	public String getCustomerSchema() {
		return customerSchema;
	}
	public void setCustomerSchema(String customerSchema) {
		this.customerSchema = customerSchema;
	}
	
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getLogoName() {
		return logoName;
	}
	public void setLogoName(String logoName) {
		this.logoName = logoName;
	}
	public int getOnPremiseStatus() {
		return onPremiseStatus;
	}
	public void setOnPremiseStatus(int onPremiseStatus) {
		this.onPremiseStatus = onPremiseStatus;
	}
	public int getScriptlessStatus() {
		return scriptlessStatus;
	}
	public void setScriptlessStatus(int scriptlessStatus) {
		this.scriptlessStatus = scriptlessStatus;
	}
	
	public int getJenkinStatus() {
		return jenkinStatus;
	}
	public void setJenkinStatus(int jenkinStatus) {
		this.jenkinStatus = jenkinStatus;
	}
	public int getGitStatus() {
		return gitStatus;
	}
	public void setGitStatus(int gitStatus) {
		this.gitStatus = gitStatus;
	}
	public int getIssueTracker() {
		return issueTracker;		
	}
	public int getissueTrackerType() {

		return issueTrackerType;
	}
	public String getIssueTrackerHost() {
		return issueTrackerHost;
		
	}
	public void setIssueTracker(int issueTracker) {
		this.issueTracker=issueTracker;
	}
	public void setIssueTrackerHost(String issueTrackerHost) {
		this.issueTrackerHost=issueTrackerHost;
		
	}
	public void setIssueTrackerType(int issueTrackerType) {
		this.issueTrackerType=issueTrackerType;		
	}
	public int getRedwoodStatus() {
		return redwoodStatus;
	}
	public void setRedwoodStatus(int redwoodStatus) {
		this.redwoodStatus = redwoodStatus;
	}
	public int getApiStatus() {
		return apiStatus;
	}
	public void setApiStatus(int apiStatus) {
		this.apiStatus = apiStatus;
	}
	public int getIssueTrackerType() {
		return issueTrackerType;
	}
	public String getRedwoodUrl() {
		return redwoodUrl;
	}
	public void setRedwoodUrl(String redwoodUrl) {
		this.redwoodUrl = redwoodUrl;
	}
	@Override
	public String toString() {
		return "CustomerDetails [custId=" + custId + ", name=" + name + ", address=" + address + ", logoUrl="
				+ Arrays.toString(logoUrl) + ", customerType=" + customerType + ", startDate=" + startDate
				+ ", createdDate=" + createdDate + ", endDate=" + endDate + ", tmStatus=" + tmStatus + ", btStatus="
				+ btStatus + ", onPremiseStatus=" + onPremiseStatus + ", custStatus=" + custStatus
				+ ", automationStatus=" + automationStatus + ", scriptlessStatus=" + scriptlessStatus
				+ ", customerSchema=" + customerSchema + ", logoName=" + logoName + ", jenkinStatus=" + jenkinStatus
				+ ", gitStatus=" + gitStatus + ", redwoodStatus=" + redwoodStatus + ", apiStatus=" + apiStatus
				+ ", issueTracker=" + issueTracker + ", issueTrackerType=" + issueTrackerType + ", redwoodUrl="
				+ redwoodUrl + ", issueTrackerHost=" + issueTrackerHost + "]";
	}
	
}

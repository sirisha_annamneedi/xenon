package com.xenon.jmeter.dao;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author navnath.damale
 *
 */
public interface JMeterDAO {
	/**
	 * 
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getResponseGraph(int userId, String schema);

}

package com.xenon.jmeter.dao;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
/**
 * 
 * @author navnath.damale
 *
 */
public class JMeterDAOImpl implements JMeterDAO, XenonQuery{
	private static final Logger logger = LoggerFactory.getLogger(JMeterDAOImpl.class);
	@Autowired
	DatabaseDao database;
	
	@Override
	public List<Map<String, Object>> getResponseGraph(int userId,String schema) {
		String sql = String.format(jMeter.getResponseGraph, schema);
		//List<Map<String, Object>> responseGraph = database.select(sql,new Object[]{userId},new int[]{ Types.INTEGER });
		List<Map<String, Object>> responseGraph = database.select(sql);
		logger.info("Query Result  " + responseGraph);
		return responseGraph;
	}
	
}

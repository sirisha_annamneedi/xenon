package com.xenon.jmeter.domain;
/**
 * 
 * @author navnath.damale
 *
 */
public class JMeterSummary {

	private int id;
	private int user_id;
	private int graph_id;
	private String x_data;
	private String xy_data;
	private String graph_lable;

	public JMeterSummary()
	{}

	public JMeterSummary(int user_id,int graph_id,String x_data,String xy_data,String graph_lable)
	{
		this.user_id=user_id;
		this.graph_id=graph_id;
		this.x_data=x_data;
		this.xy_data=xy_data;
		this.graph_lable=graph_lable;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getGraph_id() {
		return graph_id;
	}

	public void setGraph_id(int graph_id) {
		this.graph_id = graph_id;
	}

	public String getX_data() {
		return x_data;
	}

	public void setX_data(String x_data) {
		this.x_data = x_data;
	}

	public String getXy_data() {
		return xy_data;
	}

	public void setXy_data(String xy_data) {
		this.xy_data = xy_data;
	}

	public String getGraph_lable() {
		return graph_lable;
	}

	public void setGraph_lable(String graph_lable) {
		this.graph_lable = graph_lable;
	}
	
	@Override
	public String toString() {
		return "Graph_Data [user_id=" + user_id + ", graph_id="+ graph_id + ","
				+ "x_data="+x_data+",xy_data="+xy_data+",graph_lable="+graph_lable+"]";
	} 
}

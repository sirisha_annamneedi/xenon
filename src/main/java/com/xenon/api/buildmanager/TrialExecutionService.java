package com.xenon.api.buildmanager;

import java.util.List;
import java.util.Map;

import com.xenon.buildmanager.dao.ReportDAO;

/**
 * This Service is responsible for Trial Execution related operations.
 * <br><b>5th May 2017</b>
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface TrialExecutionService {
	
	/**
	 * @author suresh.adling
	 * @description insert a build execution step for a trial purpose using values of pinpu parameters 
	 * @param build
	 * @param project
	 * @param module
	 * @param scenario
	 * @param testcase
	 * @param stepId
	 * @param stepDetails
	 * @param stepInputType
	 * @param stepValue
	 * @param stepStatus
	 * @param attachmentPath
	 * @param title
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	public int insertExecutionSteps(String build, String project, String module, String scenario, String testcase,
			String stepId, String stepDetails, String stepInputType, String stepValue, String stepStatus,
			String attachmentPath, String title, String schema);
	
	public int saveTcQuery(String projectState,String moduleState,String statusState,String priorityState,String severityState,String project,
			String module,String status,String priority,String severity,ReportDAO reportService,int userID,String schema,String queryName);
	
	public int updateTcQuery(String queryId,String projectState,String moduleState,String statusState,String priorityState,String severityState,
			String project,String module,String status,String priority,String severity,ReportDAO reportService,int userID,String schema);
	
	public int  saveBugQuery(String releaseState,String buildState,String statusState,String priorityState,String severityState,String buildTypeState,
			String release,String build,String status,String priority,String severity,String buildType,int automationAccess,String schema,int userID,
			ReportDAO reportService,String queryName);
	
	public List<Map<String,Object>> bugReport(String releaseState,String buildState,String statusState,String priorityState,String severityState,String buildTypeState,
			String release,String build,String status,String priority,String severity,int automationAccess,String buildType,ReportDAO reportService,int userID,
			String schema);
	
	public int updateBugQuery(String queryId,String releaseState,String buildState,String statusState,String priorityState,String severityState,String buildTypeState,
			String release,String build,String status,String priority,String severity,String buildType,ReportDAO reportService,int automationAccess,int userID,String schema);
	
	public  List<Map<String, Object>>  tcBugReport(String proState,String modState,String statusState,String priorityState,String severityState,String project,String module,
			String status,String priority,String severity,int userID,String schema,ReportDAO reportService);
	
	
}

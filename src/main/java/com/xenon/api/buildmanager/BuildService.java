package com.xenon.api.buildmanager;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.testmanager.dao.TestcaseStepDAO;

/**
 * This Service is responsible for build related operations.
 * <br><b>5th May 2017</b>
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface BuildService {

	/**
	 * @author bhagyashri.ajmera
	 * @param bmAccess
	 * @param buildName
	 * @param buildDescription
	 * @param buildStatus
	 * @param buildExecutionType
	 * @param buildEnv
	 * @param userName
	 * @param schema
	 * @param buildReleaseId
	 * @param buildCreatorID
	 * @return
	 * @throws JSONException
	 */
	public int insertBuild(String buildName, String buildDescription, int buildStatus, String buildExecutionType, int buildEnv,int testSetExecType, 
			String userName, String schema, int buildReleaseId,	int buildCreatorID, String currentDate,String roleId, String redwoodId);

	/**
	 * 
	 * @author bhagyashri.ajmera
	 * 
	 * @param bmAccess
	 * @param buildName
	 * @param buildDescription
	 * @param buildStatus
	 * @param buildExecutionType
	 * @param buildEnv
	 * @param schema
	 * @param buildReleaseId
	 * @param buildUpdater
	 * @param buildId
	 * @return
	 * @throws JSONException
	 */
	public int updateBuild(String buildName, String buildDescription, int buildStatus,
			int buildExecutionType, int buildEnv, String schema, int buildReleaseId, int buildUpdater, int buildId,
			String updateDate,String roleId);
	
	/**
	 * @author shantaram.tupe
	 * 31-Oct-2019:5:27:30 PM
	 * @param buildId
	 * @param buildName
	 * @param isManual
	 * @param schema
	 * @return
	 */
	int updateBuildName(int buildId, String buildName, boolean isManual, String schema, String roleId);
	
	/**
	 * @author shantaram.tupe
	 * 01-Nov-2019:11:50:45 AM
	 * @param buildId
	 * @param isManual
	 * @param schema
	 * @return
	 */
	int deleteBuildSoftly( int buildId, boolean isManual, String schema, String roleId );
	
	public void setbuildassign(String schema, int buildId,int projectID, int moduleID,
			int userID, int userIdSessionValue);

	public Map<String, Object> completebuild(int buildId, int userId, String schema, String buildName, String userName);

	public Map<String, Object> insertCloneBuild(String schema, String sourceBuildId, String buildType,
			String destiBuildName, String buildDescription, String destiReleaseId, int userID, String destiBuildStatus,
			String userFullname);

	public int insertStepExecSummary(int buildId, int tcId, String schema, int stepId, String stepComment,
			int stepStatus, String bugRaised, String stepInsertMethod, TestcaseStepDAO testcaseStepDao,
			BuildDAO buildDAO);
	
	List<Map<String, Object>> getAutoStepExecSummary(Integer buildId, Integer testCaseId, String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads builds for notification
	 * 
	 * @param allBuildsTestcasesCount1
	 * @param allBuildsExecutedTestcasesCount1
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBmDashboardTopBuildsNf(List<Map<String, Object>> allBuilds,
			List<Map<String, Object>> allBuildsTestcasesCount1,
			List<Map<String, Object>> allBuildsExecutedTestcasesCount1,
			String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method uploads datasheet to database
	 * 
	 * @param datasheet
	 * @param tcId
	 * @param buildId
	 * @param title
	 * @param schema
	 * @return
	 * @throws IOException 
	 */
	Map<String, String> uploadDatasheet(byte[] datasheet, int tcId, int buildId, String title, String schema) throws IOException;

	/**
	 * @author dhananjay.deshmukh
	 * @param buildId
	 * @param schema
	 * @return true if All testcases are valid
	 */
	public boolean validateTestCasesForBuild(Integer buildId, String schema);

	int deleteBuild(int buildId, String schema);

	Map<String, String> uploadDatafile(byte[] datasheet, int buildId, int userId, String title, String schema) throws IOException;
	
	/* @author anmol.chadha*/
	List<Map<String, Object>> getTrashManualDetails( String schema);

	/* @author anmol.chadha*/
	List<Map<String, Object>> getTrashAutomationDetails(String schema);
	
	/* @author anmol.chadha*/
	boolean restoreAutomationBuildStatusForTrash( int buildId, int releaseID, String schema );
	
	/* @author anmol.chadha*/
	boolean restoreManualBuildStatusForTrash( int buildId, int releaseID, String schema );
	
}

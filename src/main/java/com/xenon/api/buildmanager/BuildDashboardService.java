package com.xenon.api.buildmanager;

import java.util.Map;
import java.util.Properties;

import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.buildmanager.dao.BuildDAO;

/**
 * This Service is responsible for Build Dashboard related operations
 * <br><b>5th May 2017</b>
 * @author Dhananjay Deshmukh
 * @version 2.0
 *
 */
public interface BuildDashboardService {

	public Map<String, Object> getDashboardData(String schema,int userId,String userTimeZone,int autoStatus,Object testcaseBuildName,BuildDAO buildDAO,ActivitiesDAO activitiesDAO,Properties colorProperties);
}

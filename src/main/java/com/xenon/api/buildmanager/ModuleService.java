package com.xenon.api.buildmanager;

import java.util.List;

/**
 * This Service is responsible for Module related operations <br>
 * <b>5th May 2017</b>
 * 
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface ModuleService {

	/**
	 * 
	 * @author suresh.adling
	 * @description create a module using input parameters
	 * @param moduleName
	 * @param moduleDescription
	 * @param projectId
	 * @param moduleStatus
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	public int insertModule(List<String> bmAccess, String moduleName, String moduleDescription, int projectId,
				String moduleStatus, String schema,String roleId);
	 
	public int updateModule(List<String> bmAccess, String moduleId, String moduleName, String moduleDescription,
			String moduleStatus, String schema,String roleId);

	public int updateModuleStatus(List<String> bmAccess, int moduleId, int moduleStatus, String schema, String roleId);
	
	public int unassignModule(int userId, int moduleId, String schema);

	public void assignModuleToUsers(String projectName, int moduleID, String moduleName, String users, int loginUser,
			String schema);
	
	public int createModule(String emailId,String schema,String projectName,String moduleName,String moduleStatus,String moduleDescription);
	
	public int updateModuleByName(String newModuleName,String currentModuleName,String schema,String userEmailId,String moduleDescription,String moduleStatus);
	
	public int assignModuleToUsers(String moduleName,String projectName,String assignUsersEmailId,String schema);
	
	public int unAssignModuleToUsers(String userEmailId,String moduleName,String schema);

	int assignOrUnassignModules(int moduleID, int projectID, String selectedUsers, String schema, String moduleName);

	int deleteModule(int moduleId, String schema);
	
}

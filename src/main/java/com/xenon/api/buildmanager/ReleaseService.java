package com.xenon.api.buildmanager;

import java.util.List;

/**
 * This Service is responsible for Release related operations <br>
 * <b>5th May 2017</b>
 * 
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface ReleaseService {
	
	public int insertRelease(String releaseName, String releaseDescription, String releaseStatus,
			String sDate, String eDate, String schema,String roleId) ;
	
	public int updateRelease(String releaseID, String releaseName, String releaseDescription,
			String releaseStatus, String eDate, String schema,String roleId);
	
	int updateReleaseName( String releaseName, int releaseId, String schema, String roleId );
	
	int deleteRelease( int releaseId, String schema, String roleId );
	
	public int createReleaseByUnitTest(String releaseName, String releaseDescription, String releaseStatus,String sDate, String eDate, String schema,String roleId);
	
	public int updateReleaseByUnitTest(String releaseID, String releaseName, String releaseDescription,String releaseStatus, String eDate, String schema,String roleId);
	

}

package com.xenon.api.buildmanager;

import java.util.Map;

public interface EnvironmentService {

	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to insert environment details
	 * 
	 * @param envName
	 * @param envStatus
	 * @param envDescription
	 * @param configParameterName
	 * @param schema
	 */
	int insertEnvDetails(String envName, String envDescription, String configParameterName,int envStatus, String schema);

	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to get environment details
	 * 
	 * @param schema
	 */
	Map<String, Object> getEnvDetails(String schema);
	
	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to get environment details
	 * 
	 * @param schema
	 * @param envId
	 */
	Map<String, Object> getEnvDetailsById(String envId, String schema);
	
	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to update environment details
	 * 
	 * @param schema
	 * @param envName
	 * @param configParameterName
	 * @param envDescription
	 * @param envId
	 */
	int updateEnvDetails(String envId, String envName, String configParameterName, String envDescription,int envStatus,
			String schema);
	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to delete environment details
	 * 
	 * @param schema
	 * @param envId
	 */
	int  deleteEnvironment(String envId, String schema);
}

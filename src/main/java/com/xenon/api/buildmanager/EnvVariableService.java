package com.xenon.api.buildmanager;

import java.util.Map;

public interface EnvVariableService {
	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to insert variable details
	 * 
	 * @param variableName
	 * @param configValue
	 * @param variableValue
	 * @param envId
	 * @param schema
	 */
	int insertEnvVariableDetails(String variableName,String configValue,String variableValue,String envId,String schema);

	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to get variable details
	 * 
	 * @param envId
	 * @param schema
	 */
	Map<String, Object> getVariableDetails(String envId, String schema);

	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to delete variable details
	 * 
	 * @param variableId
	 * @param schema
	 */
	int deleteVariable(String variableId, String schema);

	/**
	 * @author Pooja.Mugade
	 * 
	 *         This method is used to update variable details
	 * 
	 * @param updateVariableName
	 * @param updateConfigValue
	 * @param updateVariableValue
	 * @param updateVariableId
	 * @param schema
	 */
	int updateVariableDetails(String updateVariableName, String updateConfigValue, String updateVariableValue,
			String updateVariableId, String schema);
}

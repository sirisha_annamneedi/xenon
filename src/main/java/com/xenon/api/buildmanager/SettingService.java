package com.xenon.api.buildmanager;

import com.xenon.common.dao.MailGroupDAO;

/**
 * This Service is responsible for Setting related operations <br>
 * <b>5th May 2017</b>
 * 
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface SettingService {
	
	public int insertvmdetails(String hostname, String ipAddress, int portNumber,
			String remoteUsername, String remotePassword, String projectLocation, int concExecStatus,
			int concurrentExecNumber, int ieStatus, int chromeStatus, int firefoxStatus, int safariStatus, int vmStatus,
			String customerId,String schema,String roleId,int repoStatus);
	
	public int updatevmdetails(int vmStatusFree, String hostname, String ipAddress,
			int portNumber, String remoteUsername, String remotePassword, String projectLocation, int concExecStatus,
			int concurrentExecNumber, int ieStatus, int chromeStatus, int firefoxStatus, int safariStatus, int vmStatus,
			String customerId, int selectedVmId,String schema,String roleId,int gitStatus);
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method inserts cloud VM details
	 * 
	 * @param bmAccess
	 * @param hostname
	 * @param ipAddress
	 * @param portNumber
	 * @param remoteUsername
	 * @param remotePassword
	 * @param projectLocation
	 * @param concExecStatus
	 * @param concurrentExecNumber
	 * @param ieStatus
	 * @param chromeStatus
	 * @param firefoxStatus
	 * @param safariStatus
	 * @param vmStatus
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public int insertCloudVmDetails(String hostname, String ipAddress, int portNumber,
			String remoteUsername, String remotePassword, String projectLocation, int concExecStatus,
			int concurrentExecNumber, int ieStatus, int chromeStatus, int firefoxStatus, int safariStatus, int vmStatus,
			String customerId,String schema,String roleId,int repositoryStatus);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method updates cloud VM details
	 * 
	 * @param bmAccess
	 * @param vmStatusFree
	 * @param hostname
	 * @param ipAddress
	 * @param portNumber
	 * @param remoteUsername
	 * @param remotePassword
	 * @param projectLocation
	 * @param concExecStatus
	 * @param concurrentExecNumber
	 * @param ieStatus
	 * @param chromeStatus
	 * @param firefoxStatus
	 * @param safariStatus
	 * @param vmStatus
	 * @param selectedVmId
	 * @return
	 */
	public int updateCloudvmdetails(int vmStatusFree, String hostname, String ipAddress,
			int portNumber, String remoteUsername, String remotePassword, String projectLocation, int concExecStatus,
			int concurrentExecNumber, int ieStatus, int chromeStatus, int firefoxStatus, int safariStatus, int vmStatus,
			int selectedVmId,String schema,String roleId,int gitStatus);
	
	public Integer updatesmtpdetails( String schema,String smtpID,String mailStatus, String smtpHost,String smtpPort,String smtpPassword,String smtpUserID);
	
	public Integer updateMailNotificationBm(String schema,String mailNotificationId,String mailStatusCreateUser,String mailStatusAssignProject,String mailStatusAssignModule, String mailStatusAssignBuild,
			String mailStatusBuildExecComplete);
	
	public Integer updateMailNotificationBt(String schema,String mailNotificationId,String mailStatusCreateBug,String mailStatusUpdateBug);
	
	public Integer updatemailgroup(String schema,String users,MailGroupDAO mailGroupDAO,String mailGroupId, String mailGroupName);
	
	public int insertMailGroup(String mailGroupName,String schema,MailGroupDAO mailGroupDAO,String users) ;
}

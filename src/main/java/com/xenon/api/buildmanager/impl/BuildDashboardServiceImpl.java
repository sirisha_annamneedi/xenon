package com.xenon.api.buildmanager.impl;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.buildmanager.BuildDashboardService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.buildmanager.dao.BuildDAO;

public class BuildDashboardServiceImpl implements BuildDashboardService{
	
	@Autowired
	UtilsService utilsService;

	private static final Logger logger = LoggerFactory.getLogger(BuildDashboardServiceImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getDashboardData(String schema, int userId, String userTimeZone, int autoStatus,
			Object testcaseBuildName, BuildDAO buildDAO, ActivitiesDAO activitiesDAO, Properties colorProperties) {
		List<Map<String, Object>> graphData = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> recentBuilds = new ArrayList<Map<String, Object>>();
		Map<String, Object> modelData=new HashMap<String, Object>();
		
		logger.info(" Session is active,  Customer Schema " + schema);
		
		Map<String, Object> buildDashboard = buildDAO.getBuildDashboard(userId,schema);
		Map<String, Object> buildAutoDashboard = buildDAO.getBuildAutomationDashboard(schema);
		
		List<Map<String, Object>> bugGraph =buildDAO.getBuggraphdata(schema);
				
		List<Map<String, Object>> allBuildsTestcasesCount = (List<Map<String, Object>>) buildDashboard
				.get("#result-set-1");
		List<Map<String, Object>> allBuildsExecutedTestcasesCount = (List<Map<String, Object>>) buildDashboard
				.get("#result-set-2");
		List<Map<String, Object>> autoBuildsTestcasesCount = (List<Map<String, Object>>) buildAutoDashboard
				.get("#result-set-1");
		List<Map<String, Object>> autoBuildsExecutedTestcasesCount = (List<Map<String, Object>>) buildAutoDashboard
				.get("#result-set-2");
		List<Map<String, Object>> graphAutoBuild = (List<Map<String, Object>>) buildAutoDashboard
				.get("#result-set-3");

		List<Map<String, Object>> bmCount = (List<Map<String, Object>>) buildDashboard.get("#result-set-5");
		List<Map<String, Object>> graphBuild = (List<Map<String, Object>>) buildDashboard.get("#result-set-6");
		List<Map<String, Object>> manualBuilds = (List<Map<String, Object>>) buildDashboard.get("#result-set-3");

		List<Map<String, Object>> automationBuilds = (List<Map<String, Object>>) buildDashboard.get("#result-set-4");

		if(manualBuilds.size()>=automationBuilds.size())
		{
			for(int  i=0;i<manualBuilds.size();i++)
			{
				boolean flag=false;
				if(automationBuilds.size()>0)
				{
					for(int j=0;j<automationBuilds.size();j++)
					{
						if(manualBuilds.get(i).get("dateOnly").toString().equalsIgnoreCase(automationBuilds.get(j).get("dateOnly").toString()))
						{
							manualBuilds.get(i).put("automationbuilds", automationBuilds.get(j).get("automationbuilds").toString());
							flag=true;
						}
						if(flag)
							break;
						
						if(j==automationBuilds.size()-1)
						{
							if(!flag)
							{
								manualBuilds.get(i).put("automationbuilds",0);
							}
						}
					}
				}
				else
				{
					manualBuilds.get(i).put("automationbuilds",0);
				}
				
			}
			 try {
				modelData.put("buildGraphData",  utilsService.convertListOfMapToJson(manualBuilds));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else
		{
			for(int  i=0;i<automationBuilds.size();i++)
			{
				boolean flag=false;
				if(manualBuilds.size()>0)
				{
					for(int j=0;j<manualBuilds.size();j++)
					{
						if(automationBuilds.get(i).get("dateOnly").toString().equalsIgnoreCase(manualBuilds.get(j).get("dateOnly").toString()))
						{
							automationBuilds.get(i).put("manualbuilds", automationBuilds.get(j).get("automationbuilds").toString());
							flag=true;
						}
						if(flag)
							break;
						if(j==manualBuilds.size()-1)
						{
							if(!flag)
							{
								automationBuilds.get(i).put("manualbuilds",0);
							}
						}
					}
				}
				else
				{
					automationBuilds.get(i).put("manualbuilds",0);
				}
			}
			try {
				modelData.put("buildGraphData",utilsService.convertListOfMapToJson(automationBuilds));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		modelData.put("addremovetestcaseBuildName", testcaseBuildName);
		List<Map<String, Object>> topBuilds = buildDAO
				.getBmDashboardTopBuilds(allBuildsTestcasesCount,
						allBuildsExecutedTestcasesCount, schema);
		for (int i = 0; i < topBuilds.size(); i++) {
			topBuilds.get(i).put("build_createdate",
					utilsService.getDateTimeOfTimezone(userTimeZone,
							utilsService.getDate(topBuilds.get(i).get("build_createdate").toString())));
		}
		if (autoStatus == 1) {
			List<Map<String, Object>> topAutoBuilds = buildDAO.getBmDashboardTopAutoBuilds(autoBuildsTestcasesCount,
					autoBuildsExecutedTestcasesCount, schema);

			for (int i = 0; i < topAutoBuilds.size(); i++) {
				topAutoBuilds.get(i).put("build_createdate",
						utilsService.getDateTimeOfTimezone(userTimeZone,
								utilsService.getDate(topAutoBuilds.get(i).get("build_createdate").toString())));
			}
			modelData.put("topAutoBuilds", topAutoBuilds);
		}
		for (int i = 0; i < graphBuild.size(); i++) {
			List<Map<String, Object>> graphCount = (List<Map<String, Object>>) buildDashboard
					.get("#result-set-" + (7 + i));
			graphBuild.get(i).put("testcase_status", graphCount);
			recentBuilds.add(graphBuild.get(i));
		}

		for (int i = 0; i < graphAutoBuild.size(); i++) {
			List<Map<String, Object>> graphCount = (List<Map<String, Object>>) buildAutoDashboard
					.get("#result-set-" + (4 + i));
			graphAutoBuild.get(i).put("testcase_status", graphCount);
			recentBuilds.add(graphAutoBuild.get(i));
		}
		Collections.sort(recentBuilds, new Comparator<Map<String, Object>>() {
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				try {
					Date date1 = dateFormat.parse(o1.get("build_createdate").toString());
					Date date2 = dateFormat.parse(o2.get("build_createdate").toString());
					return date2.compareTo(date1);
				} catch (ParseException e1) {
					e1.printStackTrace();
					return 0;
				}
			}
		});
		for (int i = 0; i < recentBuilds.size(); i++) {
			if (i < 10) {
				graphData.add(recentBuilds.get(i));
			}
		}
		List<Map<String, Object>> topActivities = null;
		try {
			topActivities = activitiesDAO.getTopActivities(schema);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < topActivities.size(); i++) {
			String dateInString = topActivities.get(i).get("activity_date").toString();
			topActivities.get(i).put("activity_date",
					utilsService.getDateTimeOfTimezone(userTimeZone, dateInString));
		}
		
		for(int i=0;i<graphData.size();i++)
		{
			List<Map<String, Object>> testcaseStatus=(List<Map<String, Object>>) graphData.get(i).get("testcase_status");
			int passCount=0,failCount=0,skipCount=0,blockCount=0,notRunCount=0;
			for(int j=0;j<testcaseStatus.size();j++)
			{
				String status = testcaseStatus.get(j).get("tc_status").toString();
				if(status.equalsIgnoreCase("pass"))
				{
					passCount+=Integer.parseInt(testcaseStatus.get(j).get("tc_count").toString());
				}
				else if(status.equalsIgnoreCase("fail"))
				{
					failCount+=Integer.parseInt(testcaseStatus.get(j).get("tc_count").toString());
				}
				else if(status.equalsIgnoreCase("skip"))
				{
					skipCount+=Integer.parseInt(testcaseStatus.get(j).get("tc_count").toString());
				}
				else if(status.equalsIgnoreCase("block"))
				{
					blockCount+=Integer.parseInt(testcaseStatus.get(j).get("tc_count").toString());
				}
				else if(status.equalsIgnoreCase("not run"))
				{
					notRunCount+=Integer.parseInt(testcaseStatus.get(j).get("tc_count").toString());
				}
			}
			graphData.get(i).put("totalPassCount", passCount);
			graphData.get(i).put("totalFailCount", failCount);
			graphData.get(i).put("totalSkipCount", skipCount);
			graphData.get(i).put("totalBlockCount", blockCount);
			graphData.get(i).put("totalNotRunCount", notRunCount);
			
			graphData.get(i).put("totalTcCount", passCount+failCount+skipCount+blockCount+notRunCount);
			graphData.get(i).put("passPercentCompleted", (int) (((passCount)*100.0f)/(passCount+failCount+skipCount+blockCount+notRunCount)));
			graphData.get(i).put("failPercentCompleted", (int) (((failCount)*100.0f)/(passCount+failCount+skipCount+blockCount+notRunCount)));
			graphData.get(i).put("skipPercentCompleted", (int) (((skipCount)*100.0f)/(passCount+failCount+skipCount+blockCount+notRunCount)));
			graphData.get(i).put("blockPercentCompleted", (int) (((blockCount)*100.0f)/(passCount+failCount+skipCount+blockCount+notRunCount)));
			graphData.get(i).put("notRunPercentCompleted", (int) (((notRunCount)*100.0f)/(passCount+failCount+skipCount+blockCount+notRunCount)));
		}
		JSONArray jarray = new JSONArray(graphData);
		JSONArray jarrayBugGraph = new JSONArray(bugGraph);

		List<Map<String, Object>> allProjectBug =buildDAO.getAllProjectBugCount(userId, schema);

		Set<String> graphProjects=new TreeSet<String>();
		for (int i = 0; i < allProjectBug.size(); i++)
		{
			graphProjects.add(allProjectBug.get(i).get("project_name").toString());
		}
		
		int newGraphBug=0,readyGraphBug=0,closedGraphBug=0,devGraphBug=0,x=0;
		List<Map<String, Object>> getGraphBugData = new ArrayList<Map<String, Object>>();
		Iterator<String> projectSet=graphProjects.iterator();
		while (projectSet.hasNext()) {
			String projectBug=projectSet.next();
			for (int i = 0; i < allProjectBug.size(); i++)
			{
				if(projectBug.equals(allProjectBug.get(i).get("project_name").toString()))
				{
					if(allProjectBug.get(i).get("bugStatus").toString().equals("New"))
					{
						newGraphBug++;
					}
					if(allProjectBug.get(i).get("bugStatus").toString().equals("Ready for QA"))
					{
						readyGraphBug++;
					}
					if(allProjectBug.get(i).get("bugStatus").toString().equals("Closed"))
					{
						closedGraphBug++;
					}
					if(allProjectBug.get(i).get("bugStatus").toString().equals("Dev in Progress"))
					{
						devGraphBug++;
					}
				}
			}
			Map<String, Object> internalMap=new HashMap<String, Object>();
			internalMap.put("New", newGraphBug);
			internalMap.put("ReadyforQA", readyGraphBug);
			internalMap.put("Closed", closedGraphBug);
			internalMap.put("DevinProgress", devGraphBug);
			internalMap.put("projectName", projectBug);
			getGraphBugData.add(x++, internalMap);
			newGraphBug=0;readyGraphBug=0;closedGraphBug=0;devGraphBug=0;
		}
		
		JSONArray jArray = new JSONArray(getGraphBugData);
		modelData.put("allProjectBug", jArray);
		modelData.put("bugGraph", jarrayBugGraph);
		modelData.put("automationStatus", autoStatus);
		modelData.put("Count", bmCount);
		modelData.put("GraphData", jarray);
		modelData.put("topBuilds", topBuilds);
		modelData.put("nowDate", utilsService.getNowDateOfTimezone(userTimeZone));
		modelData.put("prevDate", utilsService.getPrevDateOfTimezone(userTimeZone));
		modelData.put("topActivities", topActivities);
		modelData.put("Pass", colorProperties.getProperty("testCase.Pass"));
		modelData.put("Fail",  colorProperties.getProperty("testCase.Fail"));
		modelData.put("Skip", colorProperties.getProperty("testCase.Skip"));
		modelData.put("NotRun",  colorProperties.getProperty("testCase.NotRun"));
		modelData.put("graphDataList", graphData);
		return modelData;
	}

}

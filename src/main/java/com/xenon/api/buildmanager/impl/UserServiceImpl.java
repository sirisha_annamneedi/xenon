package com.xenon.api.buildmanager.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.xenon.api.buildmanager.UserService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.buildmanager.dao.NotificationDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.buildmanager.domain.UserDetails;
import com.xenon.buildmanager.domain.UserModuleDetails;
import com.xenon.buildmanager.domain.UserNotification;
import com.xenon.buildmanager.domain.UserProjectDetails;
import com.xenon.common.dao.OauthUserDAO;
import com.xenon.common.domain.OauthUser;
import com.xenon.core.dao.CustomerDAO;
import com.xenon.util.service.OauthTokenGenerator;

public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDao;

	@Autowired
	private CustomerDAO customerDAO;

	@Autowired
	private OauthUserDAO oauthUserDAO;

	@Autowired
	private ProjectDAO projectDAO;

	@Autowired
	private ModuleDAO moduleDAO;

	@Autowired
	private NotificationDAO notificationDAO;

	@Autowired
	LoginService loginService;

	@Autowired
	RoleAccessService roleAccessService;

	@Autowired
	BuildDAO buildDAO;

	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public int insertUser(String emailId, String firstName, String lastName, String passWord, String userRole,
			String tmStatus, String btStatus, String userStatus, String schema, String customerName, String projects,
			String createDateTime, String repoPath, String userLoginName, int jenkinStatus, int gitStatus,
			int redwoodStatus, int apiStatus) {

		int flag = 201;
		try {
			if (buildDAO.checkBMRoleAccess("createuser", "2", schema)) {
				logger.info("User API to insert user details");
				String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				Random random = new Random();
				StringBuilder password = new StringBuilder(10);
				for (int i = 0; i < 8; i++)
					password.append(characters.charAt(random.nextInt(characters.length())));
				// Resource resource = new ClassPathResource(filepath);
				// final File image = resource.getFile();
				// imageIs = new FileInputStream(image);
				// byte[] bytes = IOUtils.toByteArray(imageIs);
				UserDetails user = new UserDetails();
				logger.info("Set user details to object of class UserDetails");

				Pattern userNamePattern = Pattern.compile("[^A-Za-z0-9@_.]");
				Matcher userNameMatcher = userNamePattern.matcher(emailId);
				if (!userNameMatcher.matches()) {
					emailId = emailId.replaceAll("[^A-Za-z0-9@_.]", "");
				}

				user.setEmailId(emailId.toLowerCase());
				user.setFirstName(firstName);
				user.setLastName(lastName);
				user.setRoleId(Integer.parseInt(userRole));
				user.setBTStatus(Integer.parseInt(btStatus));
				user.setTMStatus(Integer.parseInt(tmStatus));
				user.setUserStatus(Integer.parseInt(userStatus));
				// user.setUserPhoto(bytes);
				user.setCreateDate(createDateTime);
				user.setUserName(userLoginName);
				user.setJenkinStatus(jenkinStatus);
				user.setGitStatus(gitStatus);
				user.setRedwoodStatus(redwoodStatus);
				user.setApiStatus(apiStatus);
				if (passWord.isEmpty()) {
					passWord = password.toString();
				}
				user.setPassword(passWord);

				Map<String, Object> insertData = userDao.insertUserDetails(user, schema);

				List<Map<String, Object>> emailDetails = (List<Map<String, Object>>) insertData.get("#result-set-3");
				/*
				 * List<Map<String, Object>> emailNotificationDetails = (List<Map<String,
				 * Object>>) insertData .get("#result-set-4");
				 */

				if (emailDetails.size() > 0) {
					logger.info("Sending mail to user on succesful creation of new user");
					userDao.sendMail(emailDetails, customerName, user, schema);
				} else {
					logger.info("Mail details not available");
				}

				List<Map<String, Object>> userDetails = (List<Map<String, Object>>) insertData.get("#result-set-2");
				int userId = Integer.parseInt(userDetails.get(0).get("user_id").toString());

				// code for adding user details for Oauth
				String passToken = schema + firstName + "_" + lastName;
				OauthUser oauthUser = new OauthUser();
				oauthUser.setUserName(emailId);
				oauthUser.setPassword(OauthTokenGenerator.getPasstoken(passToken));
				oauthUserDAO.insertOauthUser(oauthUser);

				if (userId != 0) {
					if (!(projects.equals("No Projects"))) {
						String[] tokens = projects.split(",");
						List<UserProjectDetails> userProjectDetailsList = new ArrayList<UserProjectDetails>();
						for (String token : tokens) {
							int projectId = Integer.parseInt(token);

							UserProjectDetails userProjectDetails = new UserProjectDetails();
							userProjectDetails.setUserProjectId(0);
							userProjectDetails.setProjectId(projectId);
							userProjectDetails.setUserId(userId);
							userProjectDetailsList.add(userProjectDetails);
						}
						projectDAO.insertAssignedProjectRecords(userProjectDetailsList, schema);
						flag = 201;
					}
				}
			} else {
				logger.warn("User has no access to create user . Redirected to 404 page");
				flag = 403;
			}
		} catch (Exception e) {
			flag = 500;
			e.printStackTrace();
		}
		return flag;
	}

	@Override
	public boolean updateUser(int userId, String firstName, String lastName, String userRole, String tmStatus,
			String btStatus, String userStatus, String schema, String updateDate, int jenkinStatus, int gitStatus,
			int redwoodStatus, int apiStatus) {

		logger.info("User API to update user details");
		UserDetails user = new UserDetails();
		logger.info("Set user details to object of class UserDetails");
		user.setUserId(userId);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setRoleId(Integer.parseInt(userRole));
		user.setBTStatus(Integer.parseInt(btStatus));
		user.setTMStatus(Integer.parseInt(tmStatus));
		user.setUserStatus(Integer.parseInt(userStatus));
		user.setUpdateDate(updateDate);
		user.setJenkinStatus(jenkinStatus);
		user.setGitStatus(gitStatus);
		user.setRedwoodStatus(redwoodStatus);
		user.setApiStatus(apiStatus);
		Map<String, Object> updateUserData = userDao.updateUserData(user, schema);
		boolean flag = false;
		if (Integer.parseInt(updateUserData.get("#update-count-1").toString()) == 1) {
			flag = true;
			logger.info("User deatils are successfully updated");
		}

		return flag;
	}

	@Override
	public int insertAdminUser(String emailId, String firstName, String lastName, String userRole, String tmStatus,
			String btStatus, String userStatus, String schema, String repoPath, String userLoginName, int jenkinStatus,
			int gitStatus) {

		logger.info("User API to insert admin user details");
		String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		Random random = new Random();
		StringBuilder password = new StringBuilder(10);
		for (int i = 0; i < 8; i++)
			password.append(characters.charAt(random.nextInt(characters.length())));
		String filepath = repoPath + File.separator + "repository" + File.separator + "user" + File.separator
				+ "photo.png";
		File image = null;
		try {
			Resource resource = new ClassPathResource(filepath);
			image = resource.getFile();
			// image = ResourceUtils.getFile(filepath);
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		InputStream imageIs;
		byte[] bytes = null;
		try {
			imageIs = new FileInputStream(image);
			bytes = IOUtils.toByteArray(imageIs);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		UserDetails user = new UserDetails();
		logger.info("Set user details to object of class UserDetails");
		user.setEmailId(emailId.toLowerCase());
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setRoleId(Integer.parseInt(userRole));
		user.setBTStatus(Integer.parseInt(btStatus));
		user.setTMStatus(Integer.parseInt(tmStatus));
		user.setUserStatus(Integer.parseInt(userStatus));
		user.setUserPhoto(bytes);
		user.setPassword("admin@123");
		user.setCust(schema);
		user.setUserName(userLoginName);
		user.setJenkinStatus(jenkinStatus);
		user.setGitStatus(gitStatus);

		int flag = userDao.insertAdminUser(user, schema);
		if (flag == 1) {
			int coreflag = userDao.insertUserIntoCore(user, schema);
			if (coreflag == 1) {
				logger.info("Admin user is created successfully");
				logger.info("Sending Mail to user");
				try {
					customerDAO.sendMail(user, schema);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return userDao.getAdminUserId(user, schema);
			}
		}
		return 0;

	}

	@Override
	public String removeUser(int userid, String schema) {
		String flag = userDao.removeUser(userid, schema);
		return flag;
	}

	@Override
	public void assignProjectsToUser(int userId, String userMailId, String appIds, String schema) {

		List<Map<String, Object>> mailData = projectDAO.getMailSettingsFroAssignApp(schema);

		boolean sendMailStatus = false;
		if (mailData.size() > 0) {
			sendMailStatus = true;
		} else {
			logger.info("Mail details not available");
		}

		List<UserProjectDetails> userProjectDetailsList = new ArrayList<UserProjectDetails>();

		// List<UserNotification> userNotificationsList = new
		// ArrayList<UserNotification>();

		String[] tokens = appIds.split(",");
		for (String token : tokens) {
			String[] subTokens = token.split("::");

			int appId = Integer.parseInt(subTokens[0]);
			String appName = subTokens[1];

			UserProjectDetails userProjectDetails = new UserProjectDetails();
			userProjectDetails.setUserId(userId);
			userProjectDetails.setProjectId(appId);
			userProjectDetails.setUserProjectId(0);
			userProjectDetailsList.add(userProjectDetails);

			/*
			 * As the view project access is restricted for some user roles This code is
			 * commented
			 * 
			 * userNotificationsList.add(notificationDAO. setUserNotificationValues(1,
			 * "Assigned project '" +appName+"'", "viewproject", userId,
			 * Integer.parseInt(session.getAttribute("userId").toString( )), 2, 1));
			 */

			if (sendMailStatus) {
				try {
					projectDAO.sendMailOnAssignProject(mailData, appId, appName, userId, userMailId, schema);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		logger.info("Inserting assigned projects");
		projectDAO.insertAssignedProjectRecords(userProjectDetailsList, schema);

		/*
		 * As the view project access is restricted for some user roles This code is
		 * commented
		 * 
		 * logger.info("Inserting notifications assign project");
		 * notificationDAO.insertNotificationRecords(userNotificationsList, schema);
		 */

	}

	@Override
	public boolean assignModuleToUser(int userId, int projectID, String selectedModules, String schema) {

		List<UserModuleDetails> userModuleDetailsList = new ArrayList<UserModuleDetails>();
		int moduleId;
		if (!(selectedModules.equals("No Modules"))) {
			String[] tokens = selectedModules.split(",");
			for (String token : tokens) {
				moduleId = Integer.parseInt(token);
				UserModuleDetails userModuleDetails = new UserModuleDetails();
				userModuleDetails.setModuleId(moduleId);
				userModuleDetails.setProjectId(projectID);
				userModuleDetails.setUserId(userId);
				userModuleDetailsList.add(userModuleDetails);
			}
			try {
				moduleDAO.assignOrUnassignModules(userModuleDetailsList, schema);
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;

	}

	@Override
	public int unAssignProjectsToUser(int app, String appName, int user, String schema, int loginUserId) {
		int status = projectDAO.unAssignAppToUser(app, user, schema);
		if (status == 1) {
			List<UserNotification> userNotificationsList = new ArrayList<UserNotification>();

			UserNotification userNotification = notificationDAO.setUserNotificationValues(2,
					"Un-assigned project '" + appName + "'", "viewproject", user, loginUserId, 2, 1);

			userNotificationsList.add(userNotification);

			logger.info("Inserting notifications un assign project");
			notificationDAO.insertNotificationRecords(userNotificationsList, schema);
			return 200;
		} else
			return 500;
	}

	@Override
	public String checkUser(String emailId) {
		return userDao.checkUser(emailId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> createUser(String schema, int custTypeId, int userId, String btStatus, String tmStatus,
			int jenkinStatus, int gitStatus) {
		Map<String, Object> modelData = new HashMap<String, Object>();
		int flag = 0;
		try {
			flag = userDao.createNewUserStatus(schema, custTypeId);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		logger.info(" Session is active, userId is " + userId + " and " + " Customer Schema " + schema);
		modelData.put("flag", flag);
		if (flag == 1) {
			String mailFlag;
			Map<String, Object> createUserData = userDao.getCreateUserData(userId, schema);
			List<Map<String, Object>> projects = (List<Map<String, Object>>) createUserData.get("#result-set-1");
			List<Map<String, Object>> userRoles = (List<Map<String, Object>>) createUserData.get("#result-set-2");
			List<Map<String, Object>> emailSetting = (List<Map<String, Object>>) createUserData.get("#result-set-3");

			if (emailSetting.size() == 0) {
				mailFlag = "disable";
			} else {
				if (emailSetting.get(0).get("is_enabled").toString().equals("1")) {
					mailFlag = "enable";
				} else {
					mailFlag = "disable";
				}
			}
			// session.setAttribute("emailSetting", mailFlag);
			modelData.put("userRoles", userRoles);
			modelData.put("bugTStatus", Integer.parseInt(btStatus));
			modelData.put("testMStatus", Integer.parseInt(tmStatus));
			modelData.put("jenkinStatus", jenkinStatus);
			modelData.put("gitStatus", gitStatus);
			modelData.put("projects", projects);
			modelData.put("emailSetting", mailFlag);

		}
		return modelData;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> viewUser(int page, boolean createStatus, String schema) {
		Map<String, Object> modelData = new HashMap<String, Object>();
		if (createStatus) {
			// model.addAttribute("createUserAccessStatus", 1);
			modelData.put("createUserAccessStatus", 1);
		}

		int pageSize = 10;
		// model.addAttribute("pageSize",pageSize);
		modelData.put("pageSize", pageSize);

		int startValue = (page - 1) * pageSize;

		Map<String, Object> data = userDao.getViewUserData(startValue, pageSize, schema);

		int allUserCount = (Integer) data.get("count");
		// System.out.println("All user count=" + allUserCount);

		List<Map<String, Object>> listUser = (List<Map<String, Object>>) data.get("#result-set-1");
		for (int i = 0; i < listUser.size(); i++) {
			String logo = null;
			logo = loginService.getImage((byte[]) listUser.get(i).get("user_photo"));
			listUser.get(i).put("user_photo", logo);
		}
		boolean isMailEnabled = userDao.isMailSettingEnabled(schema);
		modelData.put("emailSettingEnabled", isMailEnabled);
		modelData.put("allUserCount", allUserCount);
		modelData.put("allUserDetails", listUser);
		return modelData;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> userSetting(String schema, int userId, String role, int btStatus, int tmStatus,
			String actionForTab, String userFullName, int jenkinStatus, int gitStatus, int redwoodStatus,
			int apiStatus) {

		Map<String, Object> editUserData = userDao.getEditUserData(userId, schema);
		Map<String, Object> modelData = new HashMap<String, Object>();

		List<Map<String, Object>> userDetails = (List<Map<String, Object>>) editUserData.get("#result-set-1");
		List<Map<String, Object>> userRoles = (List<Map<String, Object>>) editUserData.get("#result-set-2");
		/*
		 * userModel.addAttribute("userDetails", userDetails);
		 * userModel.addAttribute("userRoles", userRoles);
		 * userModel.addAttribute("recentFlag", 0); userModel.addAttribute("role",
		 * session.getAttribute("role").toString());
		 * userModel.addAttribute("bugTStatus",
		 * Integer.parseInt(session.getAttribute("btStatus").toString()));
		 * userModel.addAttribute("testMStatus",
		 * Integer.parseInt(session.getAttribute("tmStatus").toString()));
		 */
		modelData.put("userDetails", userDetails);
		modelData.put("userRoles", userRoles);
		modelData.put("recentFlag", 0);
		modelData.put("role", role);
		modelData.put("bugTStatus", btStatus);
		modelData.put("testMStatus", tmStatus);
		modelData.put("jenkinStatus", jenkinStatus);
		modelData.put("gitStatus", gitStatus);
		modelData.put("redwoodStatus", redwoodStatus);
		modelData.put("apiStatus", apiStatus);
		Map<String, Object> projects = userDao.getUserAssignedProjects(userId, schema);
		List<Map<String, Object>> assignProject = (List<Map<String, Object>>) projects.get("#result-set-1");
		List<Map<String, Object>> unassignProject = (List<Map<String, Object>>) projects.get("#result-set-2");
		List<Map<String, Object>> assignModule = (List<Map<String, Object>>) projects.get("#result-set-3");
		List<Map<String, Object>> unassignModule = (List<Map<String, Object>>) projects.get("#result-set-4");

		/*
		 * userModel.addAttribute("assignProject", assignProject);
		 * userModel.addAttribute("unassignProject", unassignProject);
		 * userModel.addAttribute("assignModule", assignModule);
		 * userModel.addAttribute("unassignModule", unassignModule);
		 */

		modelData.put("assignProject", assignProject);
		modelData.put("unassignProject", unassignProject);
		modelData.put("assignModule", assignModule);
		modelData.put("unassignModule", unassignModule);

		JSONArray jarray = new JSONArray(unassignModule);

		modelData.put("unassignModuleJson", jarray);
		modelData.put("actionForTab", actionForTab);
		// userModel.addAttribute("unassignModuleJson", jarray);
		// userModel.addAttribute("actionForTab",
		// session.getAttribute("actionForTab").toString());

		// userModel.addAttribute("userFullName",
		// session.getAttribute("selectedUserFullName"));
		// userModel.addAttribute("userID", userId);
		modelData.put("userFullName", userFullName);
		modelData.put("userID", userId);
		return modelData;
	}

	@Override
	public boolean updatePasswordByUserId(String userId, String password, String schema) {
		try {
			userDao.updatePasswordByUserId(userId, password, schema);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean checkUserName(String userName) {
		return userDao.checkUserName(userName);
	}

	@SuppressWarnings("unchecked")
	@Override
	public int insertNewLdapUser(UserDetails userDetail, String emailId, String schema, String repoPath,
			String customerName, String projects) {
		int flag = 500;
		try {

			logger.info("User API to insert user details");
			InputStream imageIs;
			String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			Random random = new Random();
			StringBuilder password = new StringBuilder(10);
			for (int i = 0; i < 8; i++)
				password.append(characters.charAt(random.nextInt(characters.length())));
			String filepath = repoPath + File.separator + "repository" + File.separator + "user" + File.separator
					+ "photo.png";
			Resource resource = new ClassPathResource(filepath);
			final File image = resource.getFile();
			imageIs = new FileInputStream(image);
			byte[] bytes = IOUtils.toByteArray(imageIs);
			UserDetails user = new UserDetails();
			logger.info("Set user details to object of class UserDetails");

			Pattern userNamePattern = Pattern.compile("[^A-Za-z0-9@_.]");
			Matcher userNameMatcher = userNamePattern.matcher(emailId);
			if (!userNameMatcher.matches()) {
				emailId = emailId.replaceAll("[^A-Za-z0-9@_.]", "");
			}

			user.setEmailId(emailId.toLowerCase());
			user.setFirstName(userDetail.getFirstName());
			user.setLastName(userDetail.getLastName());
			user.setRoleId(userDetail.getRoleId());
			user.setBTStatus(userDetail.getBtStatus());
			user.setTMStatus(userDetail.getTMStatus());
			user.setUserStatus(userDetail.getUserStatus());
			user.setUserPhoto(bytes);
			user.setCreateDate(userDetail.getCreateDate());
			user.setUserName(userDetail.getUserName());

			String passWord = userDetail.getPassword();
			if (passWord.isEmpty()) {
				passWord = password.toString();
			}
			user.setPassword(passWord);

			Map<String, Object> insertData = userDao.insertUserDetails(user, schema);

			List<Map<String, Object>> emailDetails = (List<Map<String, Object>>) insertData.get("#result-set-3");
			/*
			 * List<Map<String, Object>> emailNotificationDetails = (List<Map<String,
			 * Object>>) insertData .get("#result-set-4");
			 */

			if (emailDetails.size() > 0) {
				logger.info("Sending mail to user on succesful creation of new user");
				userDao.sendMail(emailDetails, customerName, user, schema);
			} else {
				logger.info("Mail details not available");
			}

			List<Map<String, Object>> userDetails = (List<Map<String, Object>>) insertData.get("#result-set-2");
			int userId = Integer.parseInt(userDetails.get(0).get("user_id").toString());

			// code for adding user details for Oauth
			String passToken = schema + userDetail.getFirstName() + "_" + userDetail.getLastName();
			OauthUser oauthUser = new OauthUser();
			oauthUser.setUserName(emailId);
			oauthUser.setPassword(OauthTokenGenerator.getPasstoken(passToken));
			oauthUserDAO.insertOauthUser(oauthUser);

			if (userId != 0) {
				if (!(projects.equals("No Projects"))) {
					String[] tokens = projects.split(",");
					List<UserProjectDetails> userProjectDetailsList = new ArrayList<UserProjectDetails>();
					for (String token : tokens) {
						int projectId = Integer.parseInt(token);

						UserProjectDetails userProjectDetails = new UserProjectDetails();
						userProjectDetails.setUserProjectId(0);
						userProjectDetails.setProjectId(projectId);
						userProjectDetails.setUserId(userId);
						userProjectDetailsList.add(userProjectDetails);
					}
					projectDAO.insertAssignedProjectRecords(userProjectDetailsList, schema);
					flag = 201;
				}
			}
		} catch (Exception e) {
			flag = 500;
			e.printStackTrace();
		}
		return flag;

	}

}

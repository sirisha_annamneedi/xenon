package com.xenon.api.buildmanager.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.buildmanager.ModuleService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.buildmanager.dao.NotificationDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.buildmanager.domain.ModuleDetails;
import com.xenon.buildmanager.domain.UserModuleDetails;
import com.xenon.buildmanager.domain.UserNotification;

public class ModuleServiceImpl implements ModuleService{
	
	@Autowired
	ModuleDAO moduleDao;
	
	@Autowired
	ProjectDAO projectDao;
	
	@Autowired
	UserDAO userDao;
	
	@Autowired
	NotificationDAO notificationDao;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	RoleAccessService roleAccessService;
	
	@Autowired
	BuildDAO buildDAO;
	
	private static final Logger logger = LoggerFactory.getLogger(ModuleServiceImpl.class);

	@Override
	public int insertModule(List<String> bmAccess, String moduleName, String moduleDescription, int projectId,
			String moduleStatus, String schema,String roleId) {
		int flag = 0;
		try {
			if (buildDAO.checkBMRoleAccess("createbuild",roleId,schema)) {
				logger.info("User has access to create module");
				logger.info("Bug API to insert module details");
				
				ModuleDetails module = new ModuleDetails();
				logger.info("Set module details to object of class ModuleDetails");
				module.setModuleName(moduleName);
				module.setModuleDescription(moduleDescription);
				module.setProjectId(projectId);
				module.setModuleStatus(Integer.parseInt(moduleStatus));
				

				flag = moduleDao.insertModule(module, schema);
				if (flag != 0) {
					logger.info("New Module is Created. Redirected to View modules page ");
					return 200;
				} else {
					logger.warn("Error : Module name already exists . Redirected to create module");
					return 208;
				}
			} else{
				logger.warn("User has no access to create an application . Redirected to 404 page");
				return 401;
			}
			
		} catch (Exception e) {
			utilsService.sendMailonExecptionAPI(e, "insertModule");
			return 500;
		}
	}

	@Override
	public int updateModule(List<String> bmAccess, String moduleId, String moduleName, String moduleDescription,
			String moduleStatus, String schema,String roleId) {
		try
		{
		if (buildDAO.checkBMRoleAccess("editmodule",roleId,schema)) {
			logger.info("User has access to Edit module");
			String[] textArray = { moduleName, moduleDescription };
			JSONObject resultJson = utilsService.convertToPlainText(textArray);
			moduleName = resultJson.get(moduleName).toString();
			ModuleDetails module = new ModuleDetails();
			logger.info("Set module details to object of class ModuleDetails");
			module.setModuleId(Integer.parseInt(moduleId));
			module.setModuleName(moduleName);
			module.setModuleDescription(moduleDescription);
			module.setModuleStatus(Integer.parseInt(moduleStatus));
			
			boolean flag = moduleDao.updateModule(module, schema);
			if (flag) {
				logger.info(" User is redirected to view Modules");
				return 200;
			} else {
				logger.warn("Error : Error occured while updating module");
				return 500;
			}
		} else{
			logger.warn("User has no access to Edit module. Redirected to 404 page");
			return 401;
		}
		} catch (Exception e) {
			utilsService.sendMailonExecptionAPI(e, "updateModule");
			return 500;
		}
	}

	@Override
	public int updateModuleStatus(List<String> bmAccess, int moduleId, int moduleStatus, String schema,	String roleId) {
		try
		{
			if (buildDAO.checkBMRoleAccess("editmodule",roleId,schema)) {
				logger.info("User has access to Edit module");
				boolean flag = moduleDao.updateModuleStatus(moduleId, moduleStatus, schema);
				if (flag) {
					logger.info(" User is redirected to test case");
					return 200;
				} else {
					logger.warn("Error : Error occured while updating module");
					return 500;
				}
			} else{
				logger.warn("User has no access to Edit module. Redirected to 404 page");
				return 401;
			}
		} catch (Exception e) {
			utilsService.sendMailonExecptionAPI(e, "updateModule");
			return 500;
		}
	}
	
	@Override
	public int assignOrUnassignModules(int moduleID, int projectID, String selectedUsers, String schema, String moduleName) {
		List<UserModuleDetails> userModuleDetailsList = new ArrayList<UserModuleDetails>();
		List<Map<String, Object>> mailData = projectDao.getMailSettingsFroAssignApp(schema);
		boolean sendMailStatus = false;
		if(mailData.size() > 0){
			sendMailStatus = true;
		}else{
			logger.info("Mail details not available");
		}
		try{
			int userID;
		if (!(selectedUsers.equals("No Users"))) {
			String[] tokens = selectedUsers.split(",");
			for (String token : tokens) {
				userID = Integer.parseInt(token);
				String userEmailId = userDao.getUserEmailID(userID, schema);
				UserModuleDetails userModuleDetails = new UserModuleDetails();
				userModuleDetails.setModuleId(moduleID);
				userModuleDetails.setProjectId(projectID);
				userModuleDetails.setUserId(userID);
				userModuleDetailsList.add(userModuleDetails);
				
				
				if(sendMailStatus) {
					try {
						moduleDao.sendMailOnAssignModule(mailData,moduleID, moduleName, userID, userEmailId, schema);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		logger.info("Returning to ajax post request");
		moduleDao.assignOrUnassignModules(userModuleDetailsList, schema);
		return 200;
		}
		catch (Exception e) {
			utilsService.sendMailonExecptionAPI(e, "assignOrUnassignModules");
			return 500;
		}
	}

	@Override
	public int unassignModule(int userId, int moduleId, String schema) {
		int flag = moduleDao.deleteAssignedModuleRecords(userId, moduleId, schema);
		if (flag != 0) {
			return 200;
		} else {
			return 500;
		}
	}

	@Override
	public void assignModuleToUsers(String projectName, int moduleID, String moduleName, String users, int loginUser,
			String schema) {
				List<Map<String, Object>> mailData = projectDao.getMailSettingsFroAssignApp(schema);
				int projectId=projectDao.getProjectID(projectName, schema);
				
				boolean sendMailStatus = false;
				if(mailData.size() > 0){
					sendMailStatus = true;
				}else{
					logger.info("Mail details not available");
				}
				//UserModuleDetails
				List<UserModuleDetails> userModuleDetailsList = new ArrayList<UserModuleDetails>();
				List<UserNotification> userNotificationsList = new ArrayList<UserNotification>();

				String[] tokens = users.split(",");
				for (String token : tokens) {
					String[] subtTokens = token.split("::");
					
					int userId = Integer.parseInt(subtTokens[0]);
					String userEmailId = subtTokens[1];
					
					UserModuleDetails userModuleDetails = new UserModuleDetails();
					userModuleDetails.setUserId(userId);
					userModuleDetails.setModuleId(moduleID);
					userModuleDetails.setProjectId(projectId);
					userModuleDetailsList.add(userModuleDetails);

					userNotificationsList.add(notificationDao.setUserNotificationValues(1,
							"Assigned module '" + moduleName + "'", "viewmodule", userId,
							loginUser, 2, 1));

					if(sendMailStatus) {
						try {
							projectDao.sendMailOnAssignProject(mailData,moduleID, moduleName, userId, userEmailId, schema);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}

				logger.info("Inserting assigned projects");
				moduleDao.assignOrUnassignModules(userModuleDetailsList, schema);
				logger.info("Inserting notifications assign project");
				notificationDao.insertNotificationRecords(userNotificationsList, schema);
	}

	@Override
	public int createModule(String emailId, String schema, String projectName, String moduleName, String moduleStatus,
			String moduleDescription) {
		List<Map<String, Object>> userDetails=userDao.getUserDetailsByEmailId(emailId);
		String roleId=userDetails.get(0).get("role_id").toString();
		int projectId=projectDao.getProjectID(projectName,schema);
		List<String> userRole=roleAccessService.getBMAccessList(schema,Integer.parseInt(roleId));
		int flag=insertModule(userRole,moduleName,moduleDescription,projectId,moduleStatus,schema,roleId);
		return flag;
	}

	@Override
	public int updateModuleByName(String newModuleName, String currentModuleName, String schema, String userEmailId,
			String moduleDescription, String moduleStatus) {
		List<Map<String,Object>>moduleDetails=moduleDao.getModuleDetailsByName(currentModuleName,schema);
		String moduleId=moduleDetails.get(0).get("module_id").toString();
		List<Map<String, Object>> userDetails=userDao.getUserDetailsByEmailId(userEmailId);
		String roleId=userDetails.get(0).get("role_id").toString();
		List<String> userRole=roleAccessService.getBMAccessList(schema,Integer.parseInt(roleId));
		int flag=updateModule(userRole,moduleId,newModuleName,moduleDescription,moduleStatus,schema,roleId);
		return flag;
	}

	@Override
	public int assignModuleToUsers(String moduleName, String projectName, String assignUsersEmailId, String schema) {
		List<String> userIdList=new ArrayList<String>();
		List<Map<String,Object>>moduleDetails=moduleDao.getModuleDetailsByName(moduleName,schema);
		int moduleID=Integer.parseInt(moduleDetails.get(0).get("module_id").toString());
		int projectID=projectDao.getProjectID(projectName,schema);
		String[] tokens=assignUsersEmailId.split(",");
		for(String token:tokens){
			String[] subtokens = token.split(",");
			String userEmailId = subtokens[0];
			List<Map<String,Object>> assignUserDetails =userDao.getUserDetailsByEmailId(userEmailId);
			String userId=assignUserDetails.get(0).get("user_id").toString();		
			userIdList.add(userId);	
		}
		String IdList=userIdList.toString();
		String userIdlist = IdList.substring(1,IdList.length()-1).replaceAll("\\s+","");
		int flag = assignOrUnassignModules(moduleID,projectID,userIdlist,schema,moduleName);
		return flag;
	}

	@Override
	public int unAssignModuleToUsers(String userEmailId, String moduleName, String schema) {
		List<Map<String,Object>>moduleDetails=moduleDao.getModuleDetailsByName(moduleName,schema);
		int moduleID=Integer.parseInt(moduleDetails.get(0).get("module_id").toString());
		List<Map<String,Object>> assignUserDetails =userDao.getUserDetailsByEmailId(userEmailId);
		int userId=Integer.parseInt(assignUserDetails.get(0).get("user_id").toString());
		int flag=unassignModule(userId,moduleID,schema);
		return flag;
	}
	
	@Override
	public int deleteModule(int moduleId, String schema) {
		int flag = moduleDao.deleteModule(moduleId, schema);
		if (flag != 0) {
			return 200;
		} else {
			return 500;
		}
	}

}

package com.xenon.api.buildmanager.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.buildmanager.ReleaseService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.ReleaseDAO;
import com.xenon.buildmanager.domain.ReleaseDetails;

public class ReleaseServiceImpl implements ReleaseService {

	@Autowired
	ReleaseDAO releaseDao;

	@Autowired
	RoleAccessService roleAccessService;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	BuildDAO buildDAO;
	
	private static final Logger logger = LoggerFactory.getLogger(ReleaseServiceImpl.class);
	
	@Override
	public int insertRelease(String releaseName, String releaseDescription, String releaseStatus,
			String sDate, String eDate, String schema,String roleId) {
		int flag = 201;
		try {
			if (buildDAO.checkBMRoleAccess("createrelease",roleId,schema)) {
				String[] textArray = { releaseName, releaseDescription };
				JSONObject resultJson = utilsService.convertToPlainText(textArray);
				releaseName = resultJson.get(releaseName).toString();

				Calendar cal = Calendar.getInstance();
				DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				DateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
				String currentTime = sdf2.format(cal.getTime());

				java.util.Date startDate = sdf.parse(sDate + " " + currentTime);
				java.util.Date endDate = sdf.parse(eDate + " " + currentTime);

				int status = Integer.parseInt(releaseStatus);

				ReleaseDetails insertRelease = new ReleaseDetails();
				insertRelease.setReleaseName(releaseName);
				insertRelease.setReleaseDescription(releaseDescription);
				insertRelease.setStartDate(startDate);
				insertRelease.setEndDate(endDate);
				insertRelease.setReleaseStatus(status);

				int returnFlag=releaseDao.insertRelease(insertRelease, schema);
					if (returnFlag != 0) {
						logger.info("New Release is Created. Redirected to View modules page ");
						
						//get created Release ID
						String releaseIdStr = releaseDao.getReleaseId(schema,releaseName);
						int releaseId = Integer.parseInt(releaseIdStr);
						return releaseId;
					} else {
						logger.warn("Error : Release name already exists . Redirected to create module");
						return -208;
					}
			} else {
				logger.warn("User has no access to create release . Redirected to 404 page");
				flag = -403;
			}
		} catch (Exception e) {
			flag = -500;
			e.printStackTrace();
		}
		return flag;
	}

	@Override
	public int updateRelease(String releaseID, String releaseName, String releaseDescription,
			String releaseStatus, String eDate, String schema,String roleId) {

		int flag = 201;
		try {
			if (buildDAO.checkBMRoleAccess("editrelease",roleId,schema)) {
				logger.info("User has access to edit release");
				Calendar cal = Calendar.getInstance();
				DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				DateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
				String currentTime = sdf2.format(cal.getTime());

				java.util.Date endDate = sdf.parse(eDate + " " + currentTime);

				int status = Integer.parseInt(releaseStatus);
				int id = Integer.parseInt(releaseID);

				ReleaseDetails updateRelease = new ReleaseDetails();
				updateRelease.setReleaseId(id);
				updateRelease.setReleaseDescription(releaseDescription);
				updateRelease.setReleaseName(releaseName);
				updateRelease.setEndDate(endDate);
				updateRelease.setReleaseStatus(status);

				if (releaseDao.updateRelease(updateRelease, schema) != 1)
					flag = 500;
			} else {
				logger.warn("User has no access to create release . Redirected to 404 page");
				flag = 403;
			}
		} catch (Exception e) {
			flag = 500;
			e.printStackTrace();
		}
		return flag;
	}
	
	@Override
	public int updateReleaseName(String releaseName, int releaseId, String schema, String roleId) {
		int flag = 201;
		try {
			if (buildDAO.checkBMRoleAccess("editrelease",roleId,schema)) {
				logger.info("User has access to edit release");
				if (releaseDao.updateReleaseName(releaseName, releaseId, schema) != 1)
					flag = 500;
			} else {
				logger.warn("User has no access to create release . Redirected to 404 page");
				flag = 403;
			}
		} catch (Exception e) {
			flag = 500;
			e.printStackTrace();
		}
		return flag;
	}
	
	@Override
	public int deleteRelease( int releaseId, String schema, String roleId) {
		int flag = 204;
		try {
			if ( this.buildDAO.checkBMRoleAccess("editrelease",roleId,schema)) {
				logger.info("User has access to edit release");
				if ( this.releaseDao.deleteRelease( releaseId, schema) != 1 )
					flag = 500;
			} else {
				logger.warn("User has no access to delete release . Redirected to 401 page");
				flag = 401;
			}
		} catch (Exception e) {
			flag = 500;
			e.printStackTrace();
		}
		return flag;
	}
	
	public int createReleaseByUnitTest(String releaseName, String releaseDescription, String releaseStatus,String sDate, String eDate, String schema,String roleId){
		int flag=insertRelease(releaseName,releaseDescription,releaseStatus,sDate,eDate,schema, roleId);
		return flag;
	}

	public int updateReleaseByUnitTest(String releaseID, String releaseName, String releaseDescription,String releaseStatus, String eDate, String schema,String roleId){
		//List<String> userRole=roleAccessService.getBMAccessList(schema,Integer.parseInt(roleId));
		int flag=updateRelease(releaseID,releaseName,releaseDescription,releaseStatus,eDate,schema, roleId);
		return flag;
	}

}

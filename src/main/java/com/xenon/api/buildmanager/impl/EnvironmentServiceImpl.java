package com.xenon.api.buildmanager.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.buildmanager.EnvironmentService;
import com.xenon.buildmanager.dao.EnvironmentDetailsDao;
import com.xenon.buildmanager.domain.EnvironmentDetails;

public class EnvironmentServiceImpl implements EnvironmentService {

	@Autowired
	EnvironmentDetailsDao environmentDetailsDao;

	@Override
	public int insertEnvDetails(String envName, String envDescription, String configParameterName,int envStatus,String schema) {

		EnvironmentDetails environmentDetails = new EnvironmentDetails();
		environmentDetails.setEnvName(envName);
		environmentDetails.setEnvDescription(envDescription);
		environmentDetails.setConfigParameterName(configParameterName);
		environmentDetails.setEnvStatus(envStatus);
		int flag = environmentDetailsDao.insertEnvDetails(environmentDetails, schema);
		if (flag == 1) {
			return 201;
		} else {
			return 500;
		}
	}

	@Override
	public Map<String, Object> getEnvDetails(String schema) {
		Map<String, Object> modelData = new HashMap<String, Object>();
		List<Map<String, Object>> envList = environmentDetailsDao.getEnvDetails(schema);
		modelData.put("envDetails", envList);
		return modelData;
	}
	
	@Override
	public Map<String, Object> getEnvDetailsById(String envId,String schema) {
		Map<String, Object> modelData = new HashMap<String, Object>();
		List<Map<String, Object>> envList = environmentDetailsDao.getEnvDetailsById(envId,schema);
		modelData.put("envDetails", envList);
		return modelData;
	}
	
	@Override
	public int updateEnvDetails(String envId,String envName,String configParameterName,String envDescription,int envStatus,String schema) {
		
		int flag = environmentDetailsDao.updateEnvDetails(envId,envName,configParameterName,envStatus,envDescription,schema);
		if (flag == 1) {
			return 201;
		} else {
			return 500;
		}
	}
	
	@Override
	public int deleteEnvironment(String envId,String schema) {
		
		int flag = environmentDetailsDao.deleteEnvDetails(envId,schema);
		if (flag == 1) {
			return 201;
		} else {
			return 500;
		}
		
	}
	
	
}

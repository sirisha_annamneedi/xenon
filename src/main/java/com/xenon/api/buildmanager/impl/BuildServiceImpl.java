package com.xenon.api.buildmanager.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.buildmanager.BuildService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.api.testmanager.TestCaseService;
import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.domain.BuildDetails;
import com.xenon.buildmanager.domain.BuildTestCases;
import com.xenon.constants.ExecutionType;
import com.xenon.testmanager.dao.TestcaseStepDAO;
import com.xenon.testmanager.domain.StepExecSummary;
import com.xenon.util.service.ExcelOperations;

public class BuildServiceImpl implements BuildService{
	
	
	private static final Logger logger = LoggerFactory.getLogger(BuildServiceImpl.class);

	@Autowired
	RoleAccessService roleAccessService;
	
	@Autowired
	BuildDAO buildDAO;

	@Autowired
	ActivitiesDAO activitiesDAO;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	ProjectDAO projectDao;
	
	@Autowired
	TestCaseService testCaseService;
	
	private ExcelOperations excel = new ExcelOperations();

	@Override
	public int insertBuild(String buildName, String buildDescription, int buildStatus,
			String buildExecutionType, int buildEnv, int testSetExecType, String userName, String schema, int buildReleaseId,
			int buildCreatorID, String currentDate,String roleId, String redwoodId) {
		int flag = 0;
		try {
			if (buildDAO.checkBMRoleAccess("createbuild",roleId,schema)) {
				String[] textArray = { buildName, buildDescription };
				JSONObject resultJson = utilsService.convertToPlainText(textArray);

				if (Integer.parseInt(buildExecutionType) == ExecutionType.MANUAL.getValue()) {
					BuildDetails insertBuild = new BuildDetails();
					insertBuild.setBuildName(resultJson.get(buildName).toString());
					insertBuild.setBuildDesc(buildDescription);
					insertBuild.setBuildReleaseID(buildReleaseId);
					insertBuild.setBuildStatus(buildStatus);
					insertBuild.setBuildCreatorID(buildCreatorID);
					insertBuild.setcurrentDate(currentDate);
					insertBuild.setTestSetExecType(testSetExecType);
					flag = buildDAO.insertBuild(insertBuild, schema);
				} else if (Integer.parseInt(buildExecutionType) == ExecutionType.AUTOMATION.getValue() 
						|| Integer.parseInt(buildExecutionType) == ExecutionType.REDWOOD.getValue() 
						|| Integer.parseInt(buildExecutionType) == ExecutionType.API.getValue() ) {
					BuildDetails insertBuild = new BuildDetails();
					insertBuild.setBuildName(resultJson.get(buildName).toString());
					insertBuild.setBuildDesc(buildDescription);
					insertBuild.setBuildReleaseID(buildReleaseId);
					insertBuild.setBuildStatus(buildStatus);
					insertBuild.setBuildCreatorID(buildCreatorID);
					insertBuild.setcurrentDate(currentDate);
					insertBuild.setTestSetExecType(testSetExecType);
					insertBuild.setRedwoodId(redwoodId);
					flag = buildDAO.insertAutomationBuild(insertBuild, schema);
				} 
				if (flag == 1) {
					/*RecentActivity recentActivity = activitiesDAO.setRecentActivitiesValues(9,
							resultJson.get(buildName).toString() + "#" + userName, "BM", buildCreatorID, 1);
					activitiesDAO.inserNewActivity(recentActivity, schema);*/
					return 200;
				} else {
					return 208;
				}
			} else {
				return 401;
			}
		} catch (SQLException e) {
			utilsService.sendMailonExecptionAPI(e, "insertBuild");
			return 500;
		}
	}

	@Override
	public int updateBuild(String buildName, String buildDescription, int buildStatus,
			int buildExecutionType, int buildEnv, String schema, int buildReleaseId, int buildUpdater, int buildId,
			String updateDate,String roleId) {
		if (buildDAO.checkBMRoleAccess("editbuild",roleId,schema)) {

			String[] textArray = { buildName, buildDescription };
			JSONObject resultJson = utilsService.convertToPlainText(textArray);
			BuildDetails updateBuild = new BuildDetails();
			updateBuild.setBuildID(buildId);
			updateBuild.setBuildName(resultJson.get(buildName).toString());
			updateBuild.setBuildDesc(buildDescription);
			updateBuild.setBuildReleaseID(buildReleaseId);
			updateBuild.setBuildStatus(buildStatus);
			updateBuild.setUpdateDate(updateDate);
			int flag = 0;
			if (buildExecutionType== 1) {
				flag = buildDAO.updateBuild(updateBuild, schema);
			} else if (buildExecutionType== 2) {
				flag = buildDAO.updateAutoBuild(updateBuild, schema);
			}
			if (flag == 1) {
				/*RecentActivity recentActivity = activitiesDAO.setRecentActivitiesValues(10,
						resultJson.get(buildName).toString(), "BM", buildUpdater, 1);
				activitiesDAO.inserNewActivity(recentActivity, schema);
				logger.info("User is redirected to view build");*/
				return 200;
			} else {
				logger.error("Something went wrong,User  is redirected to error page");
				return 500;
			}
		} else {
			logger.error("User doesn't have access to edit build service and is redirected to error page");
			return 401;
		}
	}

	@Override
	public int updateBuildName(int buildId, String buildName, boolean isManual, String schema, String roleId) {
		if (buildDAO.checkBMRoleAccess("editbuild",roleId,schema)) {
			int flag = 0;
			try {
				flag = buildDAO.updateBuildName(buildId, buildName, isManual, schema);
				if (flag == 1) {
					/*RecentActivity recentActivity = activitiesDAO.setRecentActivitiesValues(10,
							resultJson.get(buildName).toString(), "BM", buildUpdater, 1);
					activitiesDAO.inserNewActivity(recentActivity, schema);
					logger.info("User is redirected to view build");*/
					return 200;
				} else {
					logger.error("Something went wrong,User  is redirected to error page");
					return 404;
				}
			} catch (Exception e) {
				return 500;
			}
		} else {
			logger.error("User doesn't have access to edit build service and is redirected to error page");
			return 401;
		}
	}
	
	@Override
	public int deleteBuildSoftly(int buildId, boolean isManual, String schema, String roleId) {
		logger.trace("BuildServiceImpl.deleteBuildSoftly()");
		logger.info("BuildServiceImpl.deleteBuildSoftly()");
		if (buildDAO.checkBMRoleAccess("editbuild",roleId,schema)) {
			int flag = 0;
			try {
				flag = buildDAO.deleteBuildSoftly(buildId, isManual, schema);
				if (flag == 1) {
					/*RecentActivity recentActivity = activitiesDAO.setRecentActivitiesValues(10,
							resultJson.get(buildName).toString(), "BM", buildUpdater, 1);
					activitiesDAO.inserNewActivity(recentActivity, schema);
					logger.info("User is redirected to view build");*/
					return 200;
				} else {
					logger.error("Something went wrong, build not found to delete.");
					return 404;
				}
			} catch (Exception e) {
				return 500;
			}
		} else {
			logger.error("User doesn't have access to delete build service and is redirected to error page");
			return 401;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void setbuildassign(String schema, int buildId,int projectID, int moduleID,
			int userID, int userIdSessionValue) {

		Map<String, Object> assignData = projectDao.assignModuleToExecute(buildId, projectID, moduleID, userID,
				userIdSessionValue, schema);

		List<Map<String, Object>> assignedModules = (List<Map<String, Object>>) assignData.get("#result-set-1");
		List<Map<String, Object>> addedModules = (List<Map<String, Object>>) assignData.get("#result-set-2");
		if (assignedModules.size() == addedModules.size()) {
			buildDAO.updateBuildStatus(3, buildId, schema);
		}	
	}

	@Override
	public Map<String, Object> completebuild(int buildId, int userId, String schema, String buildName, String userName) {
		Map<String,Object> returnData =new HashMap<String,Object>();
		int flag=0;
		String result="";
		List<Map<String, Object>> buildData = buildDAO.getManualBuildAddedTcCount(buildId,userId, schema);
		int addedTcCount = Integer.parseInt((buildData).get(0).get("TcCount").toString());
		//model.addAttribute("addedTcCount", addedTcCount);
		returnData.put("addedTcCount", addedTcCount);
		if (addedTcCount > 0) {
			flag = buildDAO.updateBuildStatus(6, buildId, schema);
			
			/*RecentActivity recentActivity = activitiesDAO.setRecentActivitiesValues(10,
					"Build assignment is completed:<br/><strong> Build Name: </strong> " + buildName
							+ "<br/> <strong> Completed by: </strong> " + userName
							+ "<br/> <strong> Status: </strong> Waiting to assign user",
					"BM", userId, 1);
			activitiesDAO.inserNewActivity(recentActivity, schema);*/
			if (flag > 0)
				result="Success";
				//return result;
			else
				//return "Failure";
				result="Failure";
		} else {
			flag = buildDAO.updateBuildStatus(2, buildId, schema);
			result="Failure";
		}
		
		returnData.put("result", result);
		return returnData;
	}

	@Override
	public Map<String, Object> insertCloneBuild(String schema, String sourceBuildId, String buildType,
			String destiBuildName, String buildDescription, String destiReleaseId, int userID, String destiBuildStatus,
			String userFullname) {
		String[] buildId = sourceBuildId.split(",");
		Map<String,Object> sessionData=new HashMap<String,Object>();
		
		int flag = -1;

		if (Integer.parseInt(buildType) == 1) {
			if (buildId.length > 0)
				flag = Integer.parseInt((buildDAO.insertCloneBuild(Integer.parseInt(buildId[0]), destiBuildName,
						buildDescription, Integer.parseInt(buildType), Integer.parseInt(destiReleaseId),userID,
						Integer.parseInt(destiBuildStatus), schema,utilsService.getCurrentDateTime())).get("#update-count-1").toString());
			//session.setAttribute("newBuildTypeForTab", "manual");
			sessionData.put("newBuildTypeForTab", "manual");
		} else {
			if (buildId.length > 0)
				flag = Integer.parseInt((buildDAO.insertCloneBuild(Integer.parseInt(buildId[1]), destiBuildName,
						buildDescription, Integer.parseInt(buildType), Integer.parseInt(destiReleaseId),userID,
						Integer.parseInt(destiBuildStatus), schema,utilsService.getCurrentDateTime())).get("#update-count-1").toString());
			else
				flag = Integer.parseInt((buildDAO.insertCloneBuild(Integer.parseInt(buildId[0]), destiBuildName,
						buildDescription, Integer.parseInt(buildType), Integer.parseInt(destiReleaseId),
						userID,
						Integer.parseInt(destiBuildStatus), schema,utilsService.getCurrentDateTime())).get("#update-count-1").toString());
			//session.setAttribute("newBuildTypeForTab", "automation");
			sessionData.put("newBuildTypeForTab", "automation");
			sessionData.put("flag", flag);
		}
		if (flag >= 0) {

			/*RecentActivity recentActivity = activitiesDAO.setRecentActivitiesValues(9,
					destiBuildName + "#" + userFullname, "BM",userID, 1);
			activitiesDAO.inserNewActivity(recentActivity, schema);*/
		} 
		sessionData.put("flag", flag);
		return sessionData;
	}

	@Override
	public int insertStepExecSummary(int buildId, int tcId, String schema, int stepId, String stepComment,
			int stepStatus, String bugRaised, String stepInsertMethod, TestcaseStepDAO testcaseStepDao,
			BuildDAO buildDAO) {
		StepExecSummary stepExecSummary = new StepExecSummary();
		stepExecSummary.setStepId(stepId);
		stepExecSummary.setStexId(Integer.parseInt(
				buildDAO.getTcExecStatusByBuildIdAndTcId(buildId, tcId, schema).get(0).get("exec_id").toString()));
		stepExecSummary.setComments(stepComment);
		stepExecSummary.setStatus(stepStatus);
		stepExecSummary.setBugSatus(Integer.parseInt(bugRaised));
		byte[] bytes;
		bytes = new byte[0];
		stepExecSummary.setScreenshot(bytes);
		int stepExeId = 0;
		if (stepInsertMethod.equals("update")) {
			stepExeId = testcaseStepDao.updateTestStepSummary(schema, stepExecSummary);
		} else if (stepInsertMethod.equals("insert")) {
			stepExeId = testcaseStepDao.insertTestStepSummary(schema, stepExecSummary);
		}

		if (stepExeId != 0)
			return 1;
		else
			return 0;
	}

	@Override
	public List<Map<String, Object>> getAutoStepExecSummary(Integer buildId, Integer testCaseId, String schema) {
		List<Map<String, Object>> maps = this.buildDAO.getAutoStepExecSummary(buildId, testCaseId, schema);
		/*Map<String, TestStepReport> map = new LinkedHashMap<String, TestStepReport>();
		List<TestStepReport> testStepReports = new ArrayList<TestStepReport>();
		for (Iterator iterator = maps.iterator(); iterator.hasNext();) {
			Map<String, Object> map = (Map<String, Object>) iterator.next();
			testStepReports.add( new TestStepReport( Integer.valueOf(String.valueOf(map.get("trial_step_id"))) , Integer.valueOf(String.valueOf(map.get("testcase_id"))), String.valueOf(map.get("testcase_name")), Integer.valueOf(String.valueOf(map.get("step_id"))), String.valueOf(map.get("step_details")), 
					buildId, String.valueOf(map.get("build_name")), String.valueOf(map.get("step_value")), Integer.valueOf(String.valueOf(map.get("step_status"))), String.valueOf(map.get("screenshot_title")), String.valueOf(map.get("actual_result")), Integer.valueOf(String.valueOf(map.get("is_bug_raised"))),
					Integer.valueOf(String.valueOf(map.get("iteration_number"))), String.valueOf(map.get("exe_timestamp"))));
			System.out.println(map );
		}*/
		return maps;
	}
	
	@Override
	public List<Map<String, Object>> getBmDashboardTopBuildsNf(List<Map<String, Object>> allBuilds,
			List<Map<String, Object>> allBuildsTestcasesCount1,
			List<Map<String, Object>> allBuildsExecutedTestcasesCount1, String schema) {
		return buildDAO.getBmDashboardTopBuildsNf(allBuilds, allBuildsTestcasesCount1, allBuildsExecutedTestcasesCount1, schema);
	}
	
	
	
	

	
	@Override
	public Map<String, String> uploadDatasheet(byte[] datasheet, int tcId, int buildId, String title, String schema)
			throws IOException {
		Map<String, String> reponseData = new HashMap<String, String>();

		BuildTestCases buildTc = new BuildTestCases();
		buildTc.setDatasheetTitle(title);
		buildTc.setDatasheet(datasheet);
		buildTc.setBuildId(buildId);
		buildTc.setTestCaseid(tcId);

		List<Map<String, Object>> sourceData = testCaseService.getDatasheetByTcId(tcId, schema);
		if (sourceData.size() > 0) {

			if (excel.compareExcels((byte[]) sourceData.get(0).get("datasheet_file"), datasheet)) {

				int status = buildDAO.uploadDatasheet(buildTc, schema);

				if (status == 1) {
					reponseData.put("responseCode", "201");
					reponseData.put("status", "File uploaded successfully.");

				} else {
					reponseData.put("responseCode", "500");
					reponseData.put("status", "Something went wrong while uploading file");
				}
			}else {
				reponseData.put("responseCode", "417");
				reponseData.put("status", "Something went wrong while uploading file");
			}
		} else {
			reponseData.put("responseCode", "417");
			reponseData.put("status", "Something went wrong while uploading file");
		}

		return reponseData;

	}
	
	@Override
	public Map<String, String> uploadDatafile(byte[] datasheet,int buildId, int userId, String title, String schema)
			throws IOException {
		Map<String, String> reponseData = new HashMap<String, String>();

		BuildTestCases buildTc = new BuildTestCases();
		buildTc.setDatasheetTitle(title);
		buildTc.setDatasheet(datasheet);
		buildTc.setBuildId(buildId);

		/*List<Map<String, Object>> sourceData = testCaseService.getDatasheetByTcId(buildId, schema);
		if (sourceData.size() > 0) {

			if (excel.compareExcels((byte[]) sourceData.get(0).get("datasheet_file"), datasheet)) {

				int status = buildDAO.uploadDatasheet(buildTc, schema);

				if (status == 1) {
					reponseData.put("responseCode", "201");
					reponseData.put("status", "File uploaded successfully.");

				} else {
					reponseData.put("responseCode", "500");
					reponseData.put("status", "Something went wrong while uploading file");
				}
			}else {
				reponseData.put("responseCode", "417");
				reponseData.put("status", "Something went wrong while uploading file");
			}
		} else {
			reponseData.put("responseCode", "417");
			reponseData.put("status", "Something went wrong while uploading file");
		}*/
		List<Map<String, Object>> sourceData = testCaseService.getDatasheetByBuildId(buildId, schema);
		if (sourceData.size() > 0) {
			int updateStatus=buildDAO.updateDataFile(buildId,userId,datasheet, title, schema);
			if (updateStatus == 1) {
				reponseData.put("responseCode", "201");
				reponseData.put("status", "File uploaded successfully.");
	
			} else {
				reponseData.put("responseCode", "500");
				reponseData.put("status", "Something went wrong while uploading file");
			}
			return reponseData;
		}else {
				int status = buildDAO.uploadDatafile(buildId,userId,datasheet, title, schema);
				if (status == 1) {
					reponseData.put("responseCode", "201");
					reponseData.put("status", "File uploaded successfully.");
		
				} else {
					reponseData.put("responseCode", "500");
					reponseData.put("status", "Something went wrong while uploading file");
				}
				return reponseData;
		}
	}

	@Override
	public boolean validateTestCasesForBuild(Integer buildId, String schema) {
		List<Map<String, Object>> invalidTestcases = buildDAO.validateTestCasesForBuild(buildId, schema);
		if (invalidTestcases!=null && invalidTestcases.size()>0) {
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public int deleteBuild(int buildId, String schema) {
		int flag = buildDAO.deleteBuild(buildId, schema);
		if (flag != 0) {
			return 200;
		} else {
			return 500;
		}
	}
	
	/* @author anmol.chadha*/
	@Override
	public List<Map<String, Object>> getTrashManualDetails(String schema) {
		return buildDAO.getTrashManualDetails(schema);
	}
	
	/* @author anmol.chadha*/
	@Override
	public List<Map<String, Object>> getTrashAutomationDetails(String schema) {
		return buildDAO.getTrashAutomationDetails(schema);
	}
	
	/* @author anmol.chadha*/
	@Override
	public boolean restoreAutomationBuildStatusForTrash(int buildId, int releaseID, String schema) {
		return buildDAO.restoreAutomationBuildStatusForTrash(buildId,releaseID,schema);
	}
	
	/* @author anmol.chadha*/
	@Override
	public boolean restoreManualBuildStatusForTrash(int buildId, int releaseID, String schema) {
		return buildDAO.restoreManualBuildStatusForTrash(buildId,releaseID,schema);
	}
}

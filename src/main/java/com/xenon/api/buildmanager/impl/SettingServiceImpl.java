package com.xenon.api.buildmanager.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.buildmanager.SettingService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.buildmanager.dao.BuildDAO;
import com.xenon.buildmanager.dao.VMDetailsDAO;
import com.xenon.buildmanager.domain.MailDetails;
import com.xenon.buildmanager.domain.MailGroupDetails;
import com.xenon.buildmanager.domain.MailNotification;
import com.xenon.buildmanager.domain.VMDetails;
import com.xenon.common.dao.MailDAO;
import com.xenon.common.dao.MailGroupDAO;

public class SettingServiceImpl implements SettingService {

	private static final Logger logger = LoggerFactory.getLogger(SettingServiceImpl.class);
	
	@Autowired
	RoleAccessService roleAccessService;
	
	@Autowired
	VMDetailsDAO vmDetailsDao;
	
	@Autowired
	BuildDAO buildDAO;
	
	@Autowired
	MailDAO mailDAO;
	
	@Override
	public int insertvmdetails(String hostname, String ipAddress, int portNumber,
			String remoteUsername, String remotePassword, String projectLocation, int concExecStatus,
			int concurrentExecNumber, int ieStatus, int chromeStatus, int firefoxStatus, int safariStatus, int vmStatus,
			String customerId,String schema,String roleId,int repoStatus) {
		int flag = 201;
		try {
			if (buildDAO.checkBMRoleAccess("createvm",roleId,schema)) {
				VMDetails vmDetails = vmDetailsDao.setVMDetailsValues(1, hostname, ipAddress, portNumber,
						remoteUsername, remotePassword, projectLocation, concExecStatus, concurrentExecNumber, ieStatus,
						chromeStatus, firefoxStatus, safariStatus, vmStatus,repoStatus);
				logger.info("Inserting VM details to database");
				if (vmDetailsDao.insertVMDetails(vmDetails, customerId) != 1)
					flag = 208;
			} else {
				logger.warn("User has no access to create release . Redirected to 404 page");
				flag = 403;
			}
		} catch (Exception e) {
			flag = 500;
			e.printStackTrace();
		}
		return flag;
	}

	@Override
	public int updatevmdetails(int vmStatusFree, String hostname, String ipAddress,
			int portNumber, String remoteUsername, String remotePassword, String projectLocation, int concExecStatus,
			int concurrentExecNumber, int ieStatus, int chromeStatus, int firefoxStatus, int safariStatus, int vmStatus,
			String customerId, int selectedVmId,String schema,String roleId,int gitStatus) {
		int flag = 201;
		try {
			if (buildDAO.checkBMRoleAccess("createvm",roleId,schema)) {
				VMDetails vmDetails = vmDetailsDao.setVMDetailsValues(vmStatusFree, hostname, ipAddress, portNumber,
						remoteUsername, remotePassword, projectLocation, concExecStatus, concurrentExecNumber, ieStatus,
						chromeStatus, firefoxStatus, safariStatus, vmStatus,gitStatus);
				logger.info("Updating VM details to database");
				vmDetails.setXeVmId(selectedVmId);
				if (vmDetailsDao.updateVmDetailsById(vmDetails, customerId) != 1)
					flag = 208;
			} else {
				logger.warn("User has no access to create release . Redirected to 404 page");
				flag = 403;
			}
		} catch (Exception e) {
			flag = 500;
			e.printStackTrace();
		}
		return flag;
	}

	@Override
	public int insertCloudVmDetails(String hostname, String ipAddress, int portNumber,
			String remoteUsername, String remotePassword, String projectLocation, int concExecStatus,
			int concurrentExecNumber, int ieStatus, int chromeStatus, int firefoxStatus, int safariStatus, int vmStatus,
			String customerId,String schema,String roleId,int gitStatus) {
		int flag = 201;
		try {
			if (buildDAO.checkBMRoleAccess("createvm",roleId,schema)) {
				VMDetails vmDetails = vmDetailsDao.setVMDetailsValues(1, hostname, ipAddress, portNumber,
						remoteUsername, remotePassword, projectLocation, concExecStatus, concurrentExecNumber, ieStatus,
						chromeStatus, firefoxStatus, safariStatus, vmStatus,gitStatus);
				logger.info("Inserting VM details to database");
				if (vmDetailsDao.insertCloudVMDetails(vmDetails) != 1)
					flag = 208;
			} else {
				logger.warn("User has no access to create release . Redirected to 404 page");
				flag = 403;
			}
		} catch (Exception e) {
			flag = 500;
			e.printStackTrace();
		}
		return flag;
	}

	@Override
	public int updateCloudvmdetails(int vmStatusFree, String hostname, String ipAddress,
			int portNumber, String remoteUsername, String remotePassword, String projectLocation, int concExecStatus,
			int concurrentExecNumber, int ieStatus, int chromeStatus, int firefoxStatus, int safariStatus, int vmStatus,
			int selectedVmId,String schema,String roleId,int gitStatus) {
		int flag = 201;
		try {
			if (buildDAO.checkBMRoleAccess("createvm",roleId,schema)) {
				VMDetails vmDetails = vmDetailsDao.setVMDetailsValues(vmStatusFree, hostname, ipAddress, portNumber,
						remoteUsername, remotePassword, projectLocation, concExecStatus, concurrentExecNumber, ieStatus,
						chromeStatus, firefoxStatus, safariStatus, vmStatus,gitStatus);
				logger.info("Updating VM details to database");
				vmDetails.setXeVmId(selectedVmId);
				if (vmDetailsDao.updateCloudVmDetailsById(vmDetails) != 1)
					flag = 208;
			} else {
				logger.warn("User has no access to create release . Redirected to 404 page");
				flag = 403;
			}
		} catch (Exception e) {
			flag = 500;
			e.printStackTrace();
		}
		return flag;
	}

	@Override
	public Integer updatesmtpdetails(String schema, String smtpID, String mailStatus, String smtpHost, String smtpPort,
			String smtpPassword, String smtpUserID) {
		int id = Integer.parseInt(smtpID);
		logger.info("Setting mail details domain");
		MailDetails mailDetails=new MailDetails();
		mailDetails.setIsEnabled(Integer.parseInt(mailStatus));
		mailDetails.setSmtpHost(smtpHost);
		mailDetails.setSmtpPassword(smtpPassword);
		mailDetails.setSmtpPort(Integer.parseInt(smtpPort));
		mailDetails.setSmtpUserId(smtpUserID);
		mailDetails.setId(id);
		int flag = mailDAO.updateSmtpDetails(mailDetails, schema);
		return flag;
	}

	@Override
	public Integer updateMailNotificationBm(String schema, String mailNotificationId, String mailStatusCreateUser,
			String mailStatusAssignProject, String mailStatusAssignModule, String mailStatusAssignBuild,
			String mailStatusBuildExecComplete) {
		logger.info("Setting mail notification details");
		MailNotification mailNotificationDetails = new MailNotification();
		mailNotificationDetails.setCreateUser(Integer.parseInt(mailStatusCreateUser));
		mailNotificationDetails.setAssignProject(Integer.parseInt(mailStatusAssignProject));
		mailNotificationDetails.setAssignModule(Integer.parseInt(mailStatusAssignModule));
		mailNotificationDetails.setAssignBuild(Integer.parseInt(mailStatusAssignBuild));
		mailNotificationDetails.setBuildExecComplete(Integer.parseInt(mailStatusBuildExecComplete));
		mailNotificationDetails.setMailNotificationId(Integer.parseInt(mailNotificationId));
		logger.info("Updating mail notifications");
		int flag = mailDAO.updateMailNotificationDetailsBm(mailNotificationDetails, schema);
		return flag;
	}

	@Override
	public Integer updateMailNotificationBt(String schema, String mailNotificationId, String mailStatusCreateBug,
			String mailStatusUpdateBug) {
		logger.info("Setting mail notification details");
		MailNotification mailNotificationDetails = new MailNotification();
		mailNotificationDetails.setCreateBug(Integer.parseInt(mailStatusCreateBug));
		mailNotificationDetails.setUpdateBug(Integer.parseInt(mailStatusUpdateBug));
		mailNotificationDetails.setMailNotificationId(Integer.parseInt(mailNotificationId));
		int flag = mailDAO.updateMailNotificationDetailsBt(mailNotificationDetails, schema);		
		return flag;
	}

	@Override
	public Integer updatemailgroup(String schema, String users, MailGroupDAO mailGroupDAO, String mailGroupId,
			String mailGroupName) {
		List<MailGroupDetails> mailGroupList = new ArrayList<MailGroupDetails>();
		//String schema = session.getAttribute("cust_schema").toString();
		int status = mailGroupDAO.updateMailGroupDetails(Integer.parseInt(mailGroupId), mailGroupName, schema);
		if(status==1)
		{
				if (!(users.equals("No Users"))) {
					String[] tokens = users.split(",");
			
					for (String token : tokens) {
						int userId = Integer.parseInt(token);
			
						MailGroupDetails mailGroup = new MailGroupDetails();
						mailGroup.setUserId(userId);
						mailGroup.setMailGroupId(Integer.parseInt(mailGroupId));
						mailGroupList.add(mailGroup);
					}
					mailGroupDAO.updateMailGroupRecords(mailGroupList, schema, 1);
				} else {
					MailGroupDetails mailGroup = new MailGroupDetails();
					mailGroup.setMailGroupId(Integer.parseInt(mailGroupId));
					mailGroupList.add(mailGroup);
					mailGroupDAO.updateMailGroupRecords(mailGroupList, schema, 0);
				}
			}
		return status;
	}

	@Override
	public int insertMailGroup(String mailGroupName, String schema, MailGroupDAO mailGroupDAO, String users) {
		int mailGroupId = 0;
		try {
			mailGroupId = mailGroupDAO.insertMailGroup(mailGroupName, schema);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (mailGroupId != 0) {
			if (!(users.equals("No Users"))) {
				String[] tokens = users.split(",");
				List<MailGroupDetails> mailGroupList = new ArrayList<MailGroupDetails>();
				for (String token : tokens) {
					int userId = Integer.parseInt(token);

					MailGroupDetails mailGroup = new MailGroupDetails();
					mailGroup.setUserId(userId);
					mailGroup.setMailGroupId(mailGroupId);
					mailGroupList.add(mailGroup);
				}
				mailGroupDAO.insertMailGroupRecords(mailGroupList, schema);
			}
			
		}
		return mailGroupId;
	}

}

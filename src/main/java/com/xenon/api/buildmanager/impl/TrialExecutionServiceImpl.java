package com.xenon.api.buildmanager.impl;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.buildmanager.TrialExecutionService;
import com.xenon.buildmanager.dao.ReportDAO;
import com.xenon.database.DatabaseDao;

public class TrialExecutionServiceImpl implements TrialExecutionService{

	@Autowired
	DatabaseDao databaseDao;

	
	private static final Logger logger = LoggerFactory.getLogger(TrialExecutionServiceImpl.class);

	
	@Override
	public int insertExecutionSteps(String build, String project, String module, String scenario, String testcase,
			String stepId, String stepDetails, String stepInputType, String stepValue, String stepStatus,
			String attachmentPath, String title, String schema) {
		logger.info("Bug API to insert build execution step for trial purpose");
		String insertExecutuionStep = "INSERT INTO `%s`.xebm_trial_execution_summary (`build`,`project`,`module`,`scenario`,`testcase`,`step_id`,`step_details`,`step_input_type`,`step_value`,`step_status`,`screenshot`,`screenshot_title`)VALUES (?,?,?,?,?,?,?,?,?,?,LOAD_FILE(?),?)";
		int status = databaseDao.update(String.format(insertExecutuionStep, schema),
				new Object[] { Integer.parseInt(build), Integer.parseInt(project), Integer.parseInt(module),
						Integer.parseInt(scenario), Integer.parseInt(testcase), Integer.parseInt(stepId), stepDetails,
						stepInputType, stepValue, stepStatus, attachmentPath, title },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER,
						Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR });
		logger.info("SQL Statement : "+insertExecutuionStep);
		logger.info("Result - Insert Status : "+status);
		return status;
	}

	@Override
	public int saveTcQuery(String projectState, String moduleState, String statusState, String priorityState,
			String severityState, String project, String module, String status, String priority, String severity,
			ReportDAO reportService, int userID, String schema, String queryName) {
		int proState =  Integer.parseInt(projectState);
		int mdlState =  Integer.parseInt(moduleState);
		int statState =  Integer.parseInt(statusState);
		int proiState =  Integer.parseInt(priorityState);
		int seveState =  Integer.parseInt(severityState);
		String proQuery = null;
		String moduleQuery = null;
		String statusQuery = null;
		String priorityQuery = null;
		String severityQuery = null;

		String relToken = "( " + project +  " )";
		String buildToken = "( " + module + " )";	
		String statusToken = "( " + status + " )";
		String priorityToken = "( " + priority + " )";	
		String severityToken = "( " + severity + " )";	
		
		if(proState == 1 || proState == 3){
			proQuery = " in "+relToken; 			
		}
		if(proState == 2){
			proQuery = " not in "+relToken; 
		}

		if(mdlState == 1 || mdlState == 3){
			moduleQuery = " in "+buildToken; 			
		}
		if(mdlState == 2){
			moduleQuery = " not in "+buildToken; 
		}
			
		if(statState == 1 || statState == 3){
			statusQuery = " in "+statusToken; 			
		}
		if(statState == 2){
			statusQuery = " not in "+statusToken; 
		}
		
		if(proiState == 1 || proiState == 3){
			priorityQuery = " in "+priorityToken; 			
		}
		if(proiState == 2){
			priorityQuery = " not in "+priorityToken; 
		}
		
		if(seveState == 1 || seveState == 3){
			severityQuery = " in "+severityToken; 			
		}
		if(seveState == 2){
			severityQuery = " not in "+severityToken; 
		}

		//Form the query based on the user inputs
		String query = reportService.getTcQuery(userID,proQuery,moduleQuery,statusQuery,priorityQuery,severityQuery,schema);
		String queryVariables = "project "+proQuery+" # module "+moduleQuery+" # status "+statusQuery+" # priority "+priorityQuery+" # severity "+severityQuery;
		//saving the query
		int flag = reportService.saveTCQuery(userID, query,queryName, queryVariables,schema);
		
		return flag;
	}

	@Override
	public int updateTcQuery(String queryId, String projectState, String moduleState, String statusState,
			String priorityState, String severityState, String project, String module, String status, String priority,
			String severity, ReportDAO reportService, int userID, String schema) {
		int queryID = Integer.parseInt(queryId);
		int proState =  Integer.parseInt(projectState);
		int mdlState =  Integer.parseInt(moduleState);
		int statState =  Integer.parseInt(statusState);
		int proiState =  Integer.parseInt(priorityState);
		int seveState =  Integer.parseInt(severityState);

		String projectQuery = null;
		String moduleQuery = null;
		String statusQuery = null;
		String priorityQuery = null;
		String severityQuery = null;

		String relToken = "( " + project +  " )";
		String buildToken = "( " + module + " )";	
		String statusToken = "( " + status + " )";
		String priorityToken = "( " + priority + " )";	
		String severityToken = "( " + severity + " )";
		
		if(proState == 1 || proState == 3){
			projectQuery = " in "+relToken; 			
		}
		if(proState == 2){
			projectQuery = " not in "+relToken; 
		}
		
		if(mdlState == 1 || mdlState == 3){
			moduleQuery = " in "+buildToken; 			
		}
		if(mdlState == 2){
			moduleQuery = " not in "+buildToken; 
		}
			
		if(statState == 1 || statState == 3){
			statusQuery = " in "+statusToken; 			
		}
		if(statState == 2){
			statusQuery = " not in "+statusToken; 
		}

		if(proiState == 1 || proiState == 3){
			priorityQuery = " in "+priorityToken; 			
		}
		if(proiState == 2){
			priorityQuery = " not in "+priorityToken; 
		}
		if(seveState == 1 || seveState == 3){
			severityQuery = " in "+severityToken; 			
		}
		if(seveState == 2){
			severityQuery = " not in "+severityToken; 
		}

		String query = reportService.getTcQuery(userID,projectQuery,moduleQuery,statusQuery,priorityQuery,severityQuery,schema);
		String queryVariables = "project "+projectQuery+" # module "+moduleQuery+" # status "+statusQuery+" # priority "+priorityQuery+" # severity "+severityQuery;

		int flag = reportService.updateTCQuery(queryID, query,queryVariables, schema);
		return flag;
	}

	@Override
	public int saveBugQuery(String releaseState, String buildState, String statusState, String priorityState,
			String severityState, String buildTypeState, String release, String build, String status, String priority,
			String severity, String buildType, int automationAccess, String schema, int userID, ReportDAO reportService,
			String queryName) {
		int relState =  Integer.parseInt(releaseState);
		int bldState =  Integer.parseInt(buildState);
		int statState =  Integer.parseInt(statusState);
		int proiState =  Integer.parseInt(priorityState);
		int seveState =  Integer.parseInt(severityState);
		int bldTypeState = Integer.parseInt(buildTypeState);
		
		String relQuery = null;
		String buildQuery = null;
		String statusQuery = null;
		String priorityQuery = null;
		String severityQuery = null;			
		String buildTypeQuery = null;

		String relToken = "( " + release +  " )";
		String buildToken = "( " + build + " )";
		String statusToken = "( " + status + " )";	
		String priorityToken = "( " + priority + " )";	
		String severityToken = "( " + severity + " )";				
		String buildTypeToken = "( " + buildType + " )";
		
		if(relState == 1 || relState == 3){
			relQuery = " in "+relToken; 			
		}
		if(relState == 2){
			relQuery = " not in "+relToken; 
		}
		
		if(bldState == 1 || bldState == 3){
			buildQuery = " in "+buildToken; 			
		}
		if(bldState == 2){
			buildQuery = " not in "+buildToken; 
		}
		
		if(statState == 1 || statState == 3){
			statusQuery = " in "+statusToken; 			
		}
		if(statState == 2){
			statusQuery = " not in "+statusToken; 
		}
		
		if(proiState == 1 || proiState == 3){
			priorityQuery = " in "+priorityToken; 			
		}
		if(proiState == 2){
			priorityQuery = " not in "+priorityToken; 
		}
		if(seveState == 1 || seveState == 3){
			severityQuery = " in "+severityToken; 			
		}
		if(seveState == 2){
			severityQuery = " not in "+severityToken; 
		}
		if(bldTypeState == 1 || bldTypeState == 3){
			buildTypeQuery = " in "+buildTypeToken; 			
		}
		if(bldTypeState == 2){
			buildTypeQuery = " not in "+buildTypeToken; 
		}

		//generate the query variables
		String queryVariables = "release "+relQuery+" # build "+buildQuery+" # status "+statusQuery+" # priority "+priorityQuery+" # severity "+severityQuery+" # buildType "+buildTypeQuery;
		
		//check for clients access to automation build
		//int automationAccess = Integer.parseInt(session.getAttribute("automationStatus").toString());
		String query = null;
		if(automationAccess == 1){
			//customer is having automation access
			if(bldTypeState == 2){
				//build type status is not in
				if(buildType.length() == 1){
					//single build type is selected
					int bldType = Integer.parseInt(buildType);
					if(bldType == 1){
						//manual build type is selected, get the automation build query
						query =  reportService.getAutomationBugAnalysisQuery(userID,relQuery,buildQuery,statusQuery,priorityQuery,severityQuery,schema);
					}else{
						//automation build type is selected, get the manual build query
						query = reportService.getBugAnalysisQuery(userID,relQuery,buildQuery,statusQuery,priorityQuery,severityQuery,schema);
					}
				}else{
					//multiple build types are selected
					query = null;
				} 
			}else{
				//build type status is in
			if(buildType.length() == 1){
				//single build type is selected
				int bldType = Integer.parseInt(buildType);
				if(bldType == 1){
					//manual build type is selected
					query = reportService.getBugAnalysisQuery(userID,relQuery,buildQuery,statusQuery,priorityQuery,severityQuery,schema);
				}else{
					//automation build type is selected
					query = reportService.getAutomationBugAnalysisQuery(userID,relQuery,buildQuery,statusQuery,priorityQuery,severityQuery,schema);
				}
			}else{            
				//multiple build types are selected. So saving manual and automation query separated by #
				query = reportService.getBugAnalysisQuery(userID,relQuery,buildQuery,statusQuery,priorityQuery,severityQuery,schema);
				query = query + " # " + reportService.getAutomationBugAnalysisQuery(userID,relQuery,buildQuery,statusQuery,priorityQuery,severityQuery,schema);
			}
		}
		}else{
			//customer is not having automation access
			if(bldTypeState == 2){
				//build type status is not in
				query = null;
			}else{
				//build type status is in. Manual build type is selected
				query = reportService.getBugAnalysisQuery(userID,relQuery,buildQuery,statusQuery,priorityQuery,severityQuery,schema);
			}
		}
		int flag = reportService.saveBugQuery(userID, query,queryName, queryVariables,schema);
		return flag;
	}

	@Override
	public List<Map<String, Object>> bugReport(String releaseState, String buildState, String statusState,
			String priorityState, String severityState, String buildTypeState, String release, String build,
			String status, String priority, String severity, int automationAccess, String buildType,
			ReportDAO reportService, int userID, String schema) {
		int relState =  Integer.parseInt(releaseState);
		int bldState =  Integer.parseInt(buildState);
		int statState =  Integer.parseInt(statusState);
		int proiState =  Integer.parseInt(priorityState);
		int seveState =  Integer.parseInt(severityState);
		int bldTypeState = Integer.parseInt(buildTypeState);
		List<Map<String,Object>> filteredData = null;
		
		/*String relQuery = null;
		String buildQuery = null;
		String statusQuery = null;
		String priorityQuery = null;
		String severityQuery = null;*/

		String relToken = "( " + release +  " )";
		String buildToken = "( " + build + " )";	
		String statusToken = "( " + status + " )";	
		String priorityToken = "( " + priority + " )";
		String severityToken = "( " + severity + " )";
		
		Map<String, String> whereClauseFilters = new HashMap<String, String>();
		if( !"0".equals(release) )
			if(relState == 1 || relState == 3)
				whereClauseFilters.put("release", " in "+relToken);
			else if(relState == 2)
				whereClauseFilters.put("release", " in "+" not in "+relToken);
		
		if( !"0".equals(build) )
			if(bldState == 1 || bldState == 3)
				whereClauseFilters.put("build", " in "+buildToken);
			else if(bldState == 2)
				whereClauseFilters.put("build", " in "+" not in "+buildToken);
		
		if( !"0".equals(status) )
			if(statState == 1 || statState == 3)
				whereClauseFilters.put("status", " in "+statusToken);
			else if(statState == 2)
				whereClauseFilters.put("status", " in "+" not in "+statusToken);
		
		if( !"0".equals(priority) )
			if(proiState == 1 || proiState == 3)
				whereClauseFilters.put("priority", " in "+priorityToken);
			else if(proiState == 2)
				whereClauseFilters.put("priority", " in "+" not in "+priorityToken);
		
		if( !"0".equals(severity) )
			if(seveState == 1 || seveState == 3)
				whereClauseFilters.put("severity", " in "+severityToken);
			else if(seveState == 2)
				whereClauseFilters.put("severity", " in "+" not in "+severityToken);
			
		//check for customers access to the automation build
		//int automationAccess = Integer.parseInt(session.getAttribute("automationStatus").toString());
		if(automationAccess == 1){
			//customers is having automation access
			if(bldTypeState == 2){
				//build type status is not in
				if(buildType.length() == 1){
					//single build type is selected
					int bldType = Integer.parseInt(buildType);
					if(bldType == 1){
						//manual build type is selected, get the automation build data
						filteredData = reportService.getAutomationBugFilterData(userID,whereClauseFilters,schema);
					}else{
						//automation build type is selected, get the manual build data
						filteredData = reportService.getBugFilterData(userID,whereClauseFilters,schema);
					}
				}else{
					//multiple build types are selected so no data
					filteredData = null;
				} 
				logger.info("Returning the data to ajax request for bug analysis");
				return filteredData;
			}else{
				//build type status is in
			if(buildType.length() == 1){
				//single build type is selected
				int bldType = Integer.parseInt(buildType);
				if(bldType == 1){
					//manual build type is selected
					filteredData = reportService.getBugFilterData(userID,whereClauseFilters,schema);
				}else{
					//automation build type is selected
					filteredData = reportService.getAutomationBugFilterData(userID,whereClauseFilters,schema);
				}
			}else{
				//multiple build types are selected
				 filteredData = reportService.getBugFilterData(userID,whereClauseFilters,schema);
				 List<Map<String,Object>> automationData = reportService.getAutomationBugFilterData(userID,whereClauseFilters,schema);
				 filteredData.addAll(automationData);
			}
			logger.info("Returning the data to ajax request for bug analysis");
			return filteredData;
			}
		}else{
			//customers is not having automation access
			if(bldTypeState == 2){
				//build type status is not in
				filteredData = null;
			}else{
				//build type status is in. Manual build type is selected
				filteredData = reportService.getBugFilterData(userID,whereClauseFilters,schema);
			}
			logger.info("Returning the data to ajax request for bug analysis");
			return filteredData;
		}
	}

	@Override
	public int updateBugQuery(String queryId, String releaseState, String buildState, String statusState,
			String priorityState, String severityState, String buildTypeState, String release, String build,
			String status, String priority, String severity, String buildType, ReportDAO reportService,
			int automationAccess, int userID, String schema) {
		int queryID = Integer.parseInt(queryId);
		int relState =  Integer.parseInt(releaseState);
		int bldState =  Integer.parseInt(buildState);
		int statState =  Integer.parseInt(statusState);
		int proiState =  Integer.parseInt(priorityState);
		int seveState =  Integer.parseInt(severityState);
		int bldTypeState = Integer.parseInt(buildTypeState);
		
		String relQuery = null;
		String buildQuery = null;
		String statusQuery = null;
		String priorityQuery = null;
		String severityQuery = null;
		String buildTypeQuery = null;
		
		String relToken = "( " + release +  " )";
		String buildToken = "( " + build + " )";	
		String statusToken = "( " + status + " )";	
		String priorityToken = "( " + priority + " )";	
		String severityToken = "( " + severity + " )";	
		String buildTypeToken = "( " + buildType + " )";
		
		if(relState == 1 || relState == 3){
			relQuery = " in "+relToken; 			
		}
		if(relState == 2){
			relQuery = " not in "+relToken; 
		}

		if(bldState == 1 || bldState == 3){
			buildQuery = " in "+buildToken; 			
		}
		if(bldState == 2){
			buildQuery = " not in "+buildToken; 
		}
		
		if(statState == 1 || statState == 3){
			statusQuery = " in "+statusToken; 			
		}
		if(statState == 2){
			statusQuery = " not in "+statusToken; 
		}

		if(proiState == 1 || proiState == 3){
			priorityQuery = " in "+priorityToken; 			
		}
		if(proiState == 2){
			priorityQuery = " not in "+priorityToken; 
		}
		
		if(seveState == 1 || seveState == 3){
			severityQuery = " in "+severityToken; 			
		}
		if(seveState == 2){
			severityQuery = " not in "+severityToken; 
		}
		if(bldTypeState == 1 || bldTypeState == 3){
			buildTypeQuery = " in "+buildTypeToken; 			
		}
		if(bldTypeState == 2){
			buildTypeQuery = " not in "+buildTypeToken; 
		}

		//generate the query variables
		String queryVariables = "release "+relQuery+" # build "+buildQuery+" # status "+statusQuery+" # priority "+priorityQuery+" # severity "+severityQuery+" # buildType "+buildTypeQuery;
		
		//check for clients access to automation build
		//int automationAccess = Integer.parseInt(session.getAttribute("automationStatus").toString());
		String query = null;
		if(automationAccess == 1){
			//customer is having automation access
			if(bldTypeState == 2){
				//build type status is not in
				if(buildType.length() == 1){
					//single build type is selected
					int bldType = Integer.parseInt(buildType);
					if(bldType == 1){
						//manual build type is selected, get the automation build query
						query =  reportService.getAutomationBugAnalysisQuery(userID,relQuery,buildQuery,statusQuery,priorityQuery,severityQuery,schema);
					}else{
						//automation build type is selected, get the manual build query
						query = reportService.getBugAnalysisQuery(userID,relQuery,buildQuery,statusQuery,priorityQuery,severityQuery,schema);
					}
				}else{
					//multiple build types are selected
					query = null;
				} 
			}else{
				//build type status is in
			if(buildType.length() == 1){
				//single build type is selected
				int bldType = Integer.parseInt(buildType);
				if(bldType == 1){
					//manual build type is selected
					query = reportService.getBugAnalysisQuery(userID,relQuery,buildQuery,statusQuery,priorityQuery,severityQuery,schema);
				}else{
					//automation build type is selected
					query = reportService.getAutomationBugAnalysisQuery(userID,relQuery,buildQuery,statusQuery,priorityQuery,severityQuery,schema);
				}
			}else{            
				//multiple build types are selected. So saving manual and automation query separated by #
				query = reportService.getBugAnalysisQuery(userID,relQuery,buildQuery,statusQuery,priorityQuery,severityQuery,schema);
				query = query + " # " + reportService.getAutomationBugAnalysisQuery(userID,relQuery,buildQuery,statusQuery,priorityQuery,severityQuery,schema);
			}
		}
		}else{
			//customer is not having automation access
			if(bldTypeState == 2){
				//build type status is not in
				query = null;
			}else{
				//build type status is in. Manual build type is selected
				query = reportService.getBugAnalysisQuery(userID,relQuery,buildQuery,statusQuery,priorityQuery,severityQuery,schema);
			}
		}

		//updating the query
		int flag = reportService.updateBugQuery(queryID, query,queryVariables, schema);
		
		return flag;
	}

	@Override
	public List<Map<String, Object>> tcBugReport(String proState, String modState, String statusState,
			String priorityState, String severityState, String project, String module, String status, String priority,
			String severity, int userID, String schema, ReportDAO reportService) {
		int projectState =  Integer.parseInt(proState);
		int moduleState =  Integer.parseInt(modState);
		int statState =  Integer.parseInt(statusState);
		int proiState =  Integer.parseInt(priorityState);
		int seveState =  Integer.parseInt(severityState);
		
		String projectQuery = null;
		String moduleQuery = null;
		String statusQuery = null;
		String priorityQuery = null;
		String severityQuery = null;

		String projectToken = "( " + project +  " )";
		String moduleToken = "( " + module + " )";
		String statusToken = "( " + status + " )";	
		String priorityToken = "( " + priority + " )";
		String severityToken = "( " + severity + " )";
		
		if(projectState == 1 || projectState == 3){
			projectQuery = " in "+projectToken; 			
		}
		if(projectState == 2){
			projectQuery = " not in "+projectToken; 
		}
	
		if(moduleState == 1 || moduleState == 3){
			moduleQuery = " in "+moduleToken; 			
		}
		if(moduleState == 2){
			moduleQuery = " not in "+moduleToken; 
		}

		if(statState == 1 || statState == 3){
			statusQuery = " in "+statusToken; 			
		}
		if(statState == 2){
			statusQuery = " not in "+statusToken; 
		}

		if(proiState == 1 || proiState == 3){
			priorityQuery = " in "+priorityToken; 			
		}
		if(proiState == 2){
			priorityQuery = " not in "+priorityToken; 
		}
		
		if(seveState == 1 || seveState == 3){
			severityQuery = " in "+severityToken; 			
		}
		if(seveState == 2){
			severityQuery = " not in "+severityToken; 
		}
		List<Map<String,Object>> filteredData = reportService.getTCFilterData(userID,projectQuery,moduleQuery,statusQuery,priorityQuery,severityQuery,schema);
		
		return filteredData;
	}

}

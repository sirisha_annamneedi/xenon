package com.xenon.api.buildmanager.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.buildmanager.EnvVariableService;
import com.xenon.buildmanager.dao.EnvVariableDetailsDao;
import com.xenon.buildmanager.domain.EnvironmentVariableDetails;

public class EnvVariableServiceImpl implements EnvVariableService{
	
	@Autowired
	EnvVariableDetailsDao envVariableDetailsDao;
	
	@Override
	public int insertEnvVariableDetails(String variableName,String configValue,String variableValue,String envId,String schema){
		
		EnvironmentVariableDetails envVariableDetails=new EnvironmentVariableDetails();
		envVariableDetails.setVariableName(variableName);
		envVariableDetails.setConfigValue(configValue);
		envVariableDetails.setVariableValue(variableValue);
		envVariableDetails.setEnvId(envId);
		int flag=envVariableDetailsDao.insertEnvVariableDetails(envVariableDetails,schema);
			if(flag==1){
				return 201;
			}
			else
			{
				return 500;
			}
		}
	@Override
	public Map<String,Object> getVariableDetails(String envId,String schema){
		Map<String,Object> modelData=new HashMap<String,Object>();
		List<Map<String,Object>> returnList=envVariableDetailsDao.getEnvVariableDetails(envId,schema);
		modelData.put("variableList", returnList);
		return modelData;
		
	}
	
	@Override
	public int deleteVariable(String schema,String variableId){
		int flag= envVariableDetailsDao.deleteVariable(schema, variableId);
		if(flag==1){
			return 201;
		}
		else
		{
			return 500;
		}
	}
	
	@Override
	public int updateVariableDetails(String updateVariableName,String updateConfigValue,String updateVariableValue,String updateVariableId,String schema){
		int flag=envVariableDetailsDao.updateVariableDetails(updateVariableName,updateConfigValue, updateVariableValue,updateVariableId,schema);
		if(flag==1){
			return 201;
		}else
		{
			return 500;
		}
	}
}

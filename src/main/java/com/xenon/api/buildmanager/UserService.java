package com.xenon.api.buildmanager;

import java.util.Map;

import com.xenon.buildmanager.domain.UserDetails;

public interface UserService {

	/**
	 * @author abhay.thakur
	 * @param list
	 * @description create a user using input parameters
	 * @param emailId
	 * @param firstName
	 * @param lastName
	 * @param passWord
	 * @param userRole
	 * @param tmStatus
	 * @param btStatus
	 * @param userStatus
	 * @param emailSetting
	 * @param schema
	 * @param customer_id
	 * @return
	 * @throws Exception
	 */
	int insertUser(String emailId, String firstName, String lastName, String passWord, String userRole, String tmStatus,
			String btStatus, String userStatus, String schema, String customerName, String projects,
			String createDateTime, String repoPath, String userLoginName, int jenkinStatus, int gitStatus,
			int redwoodStatus, int apiStatus);

	/**
	 * @author abhay.thakur
	 * @description update user details using input parameters
	 * @param userId
	 * @param firstName
	 * @param lastName
	 * @param userRole
	 * @param tmStatus
	 * @param btStatus
	 * @param userStatus
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	public boolean updateUser(int userId, String firstName, String lastName, String userRole, String tmStatus,
			String btStatus, String userStatus, String schema, String updateDate, int jenkinStatus, int gitStatus,
			int redwoodStatus, int apiStatus);

	/**
	 * @author
	 * @description create admin user using input parameters
	 * @param emailId
	 * @param firstName
	 * @param lastName
	 * @param userRole
	 * @param tmStatus
	 * @param btStatus
	 * @param userStatus
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	int insertAdminUser(String emailId, String firstName, String lastName, String userRole, String tmStatus,
			String btStatus, String userStatus, String schema, String repoPath, String userLoginName, int jenkinStatus,
			int gitStatus);

	public String removeUser(int userid, String schema);

	public void assignProjectsToUser(int userId, String userMailId, String appIds, String schema);

	public boolean assignModuleToUser(int userId, int projectID, String selectedModules, String schema);

	public int unAssignProjectsToUser(int app, String appName, int user, String schema, int loginUserId);

	public String checkUser(String emailId);

	public Map<String, Object> createUser(String schema, int custTypeId, int userId, String btStatus, String tmStatus,
			int jenkinStatus, int gitStatus);

	public Map<String, Object> viewUser(int page, boolean createStatus, String schema);

	public Map<String, Object> userSetting(String schema, int userId, String role, int btStatus, int tmStatus,
			String actionForTab, String userFullName, int jenkinStatus, int gitStatus, int redwoodStatus,
			int apiStatus);

	public boolean updatePasswordByUserId(String userId, String password, String schema);

	boolean checkUserName(String userName);

	public int insertNewLdapUser(UserDetails userDetail, String emailId, String schema, String repoPath,
			String customerName, String projects);
}

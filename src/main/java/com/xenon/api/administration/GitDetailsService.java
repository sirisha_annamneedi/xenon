package com.xenon.api.administration;

import java.util.List;
import java.util.Map;

public interface GitDetailsService {

	int insertGitProject(String gitName, String gitUrl, String userName, String apiToken, String gitStatus, int userId,
			String schema);

	List<Map<String, Object>> getAllInstances(String userId, String schema);
	
	int updateGitProject(String serverId,String serverName,String username,String jenkinsUrl,String apiToken,String instanceStatus,String schema);

	public int removeUsersInstancePermission(String userId,String instanceId, String schema);

	public int assignInstancePermissionToUsers(String[] allUserIds, String instanceId, String schema);

	public int updateUsersInstancePermission(String editPermissionUsers,  String instanceId,
			String coreSchema);

	public int revokeUsersInstancePermission(String userId,String instanceId ,String coreSchema);

	public Map<String, Object> getInstancePermissionWiseMembers(String instanceId, String schema);

	int testGitConnection(String url, String userName, String apiToken);

	List<Map<String, Object>> getInstanceDetailsById(int instanceId, String userId, String schema);

	int getCheckOut(String projectUrl, String username, String apiToken, String branchName);

	int insertGitRepository(String instanceId, String branchName, String repositoryName, String userId, String schema);

	List<Map<String, Object>> getProjectDetailsByRepositoryId(int repoId, int userId, String schema);
}

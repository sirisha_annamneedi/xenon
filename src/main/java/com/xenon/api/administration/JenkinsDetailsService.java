package com.xenon.api.administration;

import java.util.List;
import java.util.Map;

import com.xenon.administration.domain.JenkinsDetails;

/**
 * This Service is responsible for Jenkins related operations. <br>
 * <b>20th July 2017</b>
 * 
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface JenkinsDetailsService {

	public int insertJenkinsInstance(String serverName, String username, String jenkinsUrl, String apiToken,
			String instanceStatus, String schema);

	/**
	 * 
	 * @author pooja.mugade
	 * @method insertJenkinsJob
	 * @param instanceName
	 * @param jenkinsJobName
	 * @param jobName
	 * @param jobDescription
	 * @param jobStatus
	 */

	int insertJenkinsJob(int instanceName, String jenkinsJobName, String jobName, String jobDescription, int jobStatus,
			int buildStatus, String buildToken, String schema, int userID);

	/**
	 * 
	 * @author pooja.mugade This method used to get all instance details
	 * @method getAllInstances
	 * @param schema
	 */
	public List<Map<String, Object>> getAllInstances(String schema, String userId);

	/**
	 * 
	 * @author pooja.mugade This method used to create jenkins job
	 * @method createJenkinsJob
	 * @param schema
	 * @param userId
	 */
	public Map<String, Object> createJenkinsJob(String schema, int userId);

	public JenkinsDetails getJenkinsJobDetails(int jobId, String schema);

	/**
	 * 
	 * @author pooja.mugade This method used to get jenkins report
	 * @method jenkinsReport
	 * @param schema
	 * @param userId
	 * @param instanceID
	 */
	Map<String, Object> jenkinsReport(String schema, int userId, String instanceID);

	/**
	 * 
	 * @author pooja.mugade This method used to get server details by Id
	 * @method getServerDetailsById
	 * @param schema
	 * @param serverId
	 */
	Map<String, Object> getServerDetailsById(int serverId, String schema);

	/**
	 * 
	 * @author pooja.mugade This method used to update server details
	 * @method updateServerDetails
	 * @param serverId
	 * @param serverName
	 * @param username
	 * @param jenkinsUrl
	 * @param apiToken
	 * @param instanceStatus
	 * @param schema
	 */
	Map<String, Object> updateServerDetails(int serverId, String serverName, String username, String jenkinsUrl,
			String apiToken, int instanceStatus, String schema);

	public int getServerIdByServerName(String serverName, String currentSchema);

	public List<Map<String, Object>> getApiTokenDetails(int serverInstanceId);

	public int insertNewApiToken(int serverInstanceId, int custId, int userId, String token, String windowsScript,
			String powershellScript);

	public Map<String, Object> getJobPermissionWiseMembers(String jobId, String schema);
	
	public Map<String, Object> getInstancePermissionWiseMembers(String instanceId, String schema);

	public int assignJobPermissionToUsers(String[] allUserIds, String jobId, String schema);
	
	public int assignInstancePermissionToUsers(String[] allUserIds, String instanceId, String schema);

	public int removeUsersJobPermission(String userId, String jobId, String schema);

	public int removeUsersInstancePermission(String userId,String instanceId, String schema);
	
	public int updateUsersJobPermission(String editPermissionUsers, String executePermissionUsers, String jobId,
			String coreSchema);
	
	public int updateUsersInstancePermission(String editPermissionUsers,  String instanceId,
			String coreSchema);

	public int revokeUsersJobPermission(String userId, String jobId,String coreSchema);
	
	public int revokeUsersInstancePermission(String userId,String instanceId ,String coreSchema);

	public Map<String, Object> getInstanceInformationById(int instanceId, String schema);

	public int updateJenkinsJob(int jobId, String jobname, String jobDesciption, int status, String buildToken,
			String schema);


	/**
	 * 
	 * @author pooja.mugade This method used to check role access
	 * @method checkRoleAccess
	 * @param userId
	 * @param schema
	 * @param roleAccess
	 */
	boolean checkJobAccess(String roleAccess, String userId, String jobId, String schema);

	public int checkIsUpdateEnabled(int jobId, int userID, String schema);
	
	public int checkIsExecuteEnabled(int jobId, int userID, String schema);


}

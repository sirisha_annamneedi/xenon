package com.xenon.api.administration;

import java.util.Map;

import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.common.dao.AdminDashboardDAO;

/**
 * This Service is responsible for Dashboard related operations.
 * <br><b>5th May 2017</b>
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface DashboardService {

	public Map<String,Object> getDashboardData(String schema,int page,AdminDashboardDAO adminDao,ActivitiesDAO activitiesDAO,int custTypeId
			,String customerId,String timeZone);
}

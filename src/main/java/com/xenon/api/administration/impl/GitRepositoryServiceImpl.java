package com.xenon.api.administration.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.administration.dao.GitInstanceDao;
import com.xenon.administration.dao.GitRepositoryDao;
import com.xenon.administration.domain.GitRepositoryDetails;
import com.xenon.api.administration.GitRepositoryService;

public class GitRepositoryServiceImpl implements GitRepositoryService {

	@Autowired
	GitRepositoryDao gitRepositoryDao;
	
	@Autowired
	GitInstanceDao gitInstanceDao;

	@Override
	public List<Map<String, Object>> getAllrepositories(String userId, String projectId, String schema) {
		return gitRepositoryDao.getRepositories(userId, projectId, schema);
	}

	@Override
	public Map<String, Object> getRepositoryById(String repositoryId, String schema) {
		List<Map<String, Object>> returnData = gitRepositoryDao.getRepositoryById(repositoryId, schema);
		if(returnData!=null && !returnData.isEmpty()) {
			return returnData.get(0);
		}
		return null;
	}

	@Override
	public int updateGitRepository(String instanceId, String branchName, String repositoryName, String repositoryId,
			String schema) {
		GitRepositoryDetails gitRepositoryDetails = new GitRepositoryDetails();
		gitRepositoryDetails.setBranchName(branchName);
		gitRepositoryDetails.setInstanceId(Integer.parseInt(instanceId));
		gitRepositoryDetails.setRepositoryName(repositoryName);
		gitRepositoryDetails.setRepositoryId(Integer.parseInt(repositoryId));
				
		int flag = gitRepositoryDao.updateGitRepository(gitRepositoryDetails, schema);
		if (flag == 1)
			return 201;
		else
			return 500;
	}
}

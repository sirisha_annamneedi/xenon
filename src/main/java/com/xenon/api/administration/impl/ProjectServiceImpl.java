package com.xenon.api.administration.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.administration.ProjectService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.NotificationDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.buildmanager.dao.exception.ProjectNotFoundException;
import com.xenon.buildmanager.dao.exception.UserNotFoundException;
import com.xenon.buildmanager.domain.ProjectDetails;
import com.xenon.buildmanager.domain.UserNotification;
import com.xenon.buildmanager.domain.UserProjectDetails;

public class ProjectServiceImpl implements ProjectService {

	@Autowired
	ProjectDAO projectDao;

	@Autowired
	NotificationDAO notificationDao;

	@Autowired
	UserDAO userDAO;
	
	@Autowired
	UtilsService utilsService;

	@Autowired
	RoleAccessService roleAccessService;
	
	@Resource(name = "colorProperties")
	private Properties colorProperties;

	private static final Logger logger = LoggerFactory.getLogger(ProjectServiceImpl.class);

	private Map<String, String> reponseData = new HashMap<String, String>();

	@Override
	public Map<String, String> insertProject(String projectName, String jiraProjectName, String projectDescription, String bugPrefix,
			String tcPrefix, String proStatus, String schema, String currentDate) {
		String[] textArray = { projectName, projectDescription, bugPrefix, tcPrefix };
		JSONObject resultJson = utilsService.convertToPlainText(textArray);
		projectName = resultJson.get(projectName).toString();
		bugPrefix = resultJson.get(bugPrefix).toString();
		tcPrefix = resultJson.get(tcPrefix).toString();

		int projectStatus = Integer.parseInt(proStatus);

		ProjectDetails projectDetails = new ProjectDetails();
		projectDetails.setProjectName(projectName);
		projectDetails.setJiraProjectName(jiraProjectName);
		projectDetails.setProjectDescription(projectDescription);
		projectDetails.setProjectStatus(projectStatus);
		projectDetails.setTcPrefix(tcPrefix);
		projectDetails.setBtPrefix(bugPrefix);
		projectDetails.setcurrentDate(currentDate);

		int flag = 0;
		try {
			flag = projectDao.insertJiraProject(projectDetails, schema);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (flag == 0) {
			reponseData.put("responseCode", "208");
			reponseData.put("status", "Duplicate Project details : Project Name.");
			return reponseData;
		} else if (flag == 2) {
			reponseData.put("responseCode", "208");
			reponseData.put("status", "Duplicate Project details : Test Name.");
			return reponseData;
		} else if (flag == 3) {
			reponseData.put("responseCode", "208");
			reponseData.put("status", "Duplicate Project details : Bug Prifix.");
			return reponseData;
		} else {
			reponseData.put("responseCode", "201");
			reponseData.put("status", "Project Created successfully.");
			return reponseData;
		}
	}

	@Override
	public Map<String, String> insertProject(String projectName, String projectDescription, String bugPrefix,
			String tcPrefix, String proStatus, String schema, String currentDate) {
		logger.info( "ProjectServiceImpl.insertProject()" );
		String[] textArray = { projectName, projectDescription, bugPrefix, tcPrefix };
		JSONObject resultJson = utilsService.convertToPlainText(textArray);
		projectName = resultJson.get(projectName).toString();
		bugPrefix = resultJson.get(bugPrefix).toString();
		tcPrefix = resultJson.get(tcPrefix).toString();

		int projectStatus = Integer.parseInt(proStatus);

		ProjectDetails projectDetails = new ProjectDetails();
		projectDetails.setProjectName(projectName);
		projectDetails.setProjectDescription(projectDescription);
		projectDetails.setProjectStatus(projectStatus);
		projectDetails.setTcPrefix(tcPrefix);
		projectDetails.setBtPrefix(bugPrefix);
		projectDetails.setcurrentDate(currentDate);

		int flag = 0;
		try {
			flag = projectDao.insertProject(projectDetails, schema);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (flag == 0) {
			reponseData.put("responseCode", "208");
			reponseData.put("status", "Duplicate Project details : Project Name.");
			return reponseData;
		} else if (flag == 2) {
			reponseData.put("responseCode", "208");
			reponseData.put("status", "Duplicate Project details : Test Name.");
			return reponseData;
		} else if (flag == 3) {
			reponseData.put("responseCode", "208");
			reponseData.put("status", "Duplicate Project details : Bug Prifix.");
			return reponseData;
		} else {
			reponseData.put("responseCode", "201");
			reponseData.put("status", "Project Created successfully.");
			try {
				reponseData.put("projectId", String.valueOf( this.projectDao.getProjectID(projectName, schema) ) );
			} catch (Exception e) {
				logger.error("Error while getting project Id for added project : "+projectName, e);
			}
			return reponseData;
		}
	}
	
	@Override
	public int updateProject(int projectId, String projectDescription, int projectStatus, String schema) {
		ProjectDetails updateProject = new ProjectDetails();
		updateProject.setProjectId(projectId);
		updateProject.setProjectDescription(projectDescription);
		updateProject.setProjectStatus(projectStatus);

		Map<String, Object> updateProjectData = projectDao.updateApplicationDetails(updateProject, schema);

		int flag = Integer.parseInt(updateProjectData.get("#update-count-1").toString());

		return flag;
	}

	@Override
	public void assignProjectToUsers(int appId, String appName, String users, int loginUser, String schema) {
		List<Map<String, Object>> mailData = projectDao.getMailSettingsFroAssignApp(schema);

		boolean sendMailStatus = false;
		if (mailData.size() > 0) {
			sendMailStatus = true;
		} else {
			logger.info("Mail details not available");
		}

		List<UserProjectDetails> userProjectDetailsList = new ArrayList<UserProjectDetails>();
		List<UserNotification> userNotificationsList = new ArrayList<UserNotification>();

		String[] tokens = users.split(",");
		for (String token : tokens) {
			String[] subtTokens = token.split("::");

			int userId = Integer.parseInt(subtTokens[0]);
			String userEmailId = subtTokens[1];

			UserProjectDetails userProjectDetails = new UserProjectDetails();
			userProjectDetails.setUserId(userId);
			userProjectDetails.setProjectId(appId);
			userProjectDetails.setUserProjectId(0);
			userProjectDetailsList.add(userProjectDetails);

			userNotificationsList.add(notificationDao.setUserNotificationValues(1, "Assigned project '" + appName + "'",
					"viewproject", userId, loginUser, 2, 1));

			if (sendMailStatus) {
				try {
					projectDao.sendMailOnAssignProject(mailData, appId, appName, userId, userEmailId, schema);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		logger.info("Inserting assigned projects");
		projectDao.insertAssignedProjectRecords(userProjectDetailsList, schema);
		logger.info("Inserting notifications assign project");
		notificationDao.insertNotificationRecords(userNotificationsList, schema);
	}

	@Override
	public int setCurrentProject(int userId, int projectId, String SchemeName) {
		logger.info("inserting current project");
		int flag = projectDao.insertCurrentProject(userId, projectId, SchemeName);
		if (flag != 0)
			return 201;
		else
			return 500;
	}

	@Override
	public int updateCurrentProject(int userId, int projectId, String SchemeName) {
		logger.info("inserting current project");
		int flag = projectDao.updateCurrentProject(userId, projectId, SchemeName);
		if (flag != 0)
			return 201;
		else
			return 500;
	}

	@Override
	public int updateProjectByName(String projectName, String Projectdescription, int projectStatus, String schema) {
		int projectId = projectDao.getProjectID(projectName, schema);
		int result = updateProject(projectId, Projectdescription, projectStatus, schema);

		return result;
	}

	@Override
	public void assignProjectToUsersByEmail(String projectName, String emailId, String schema, String assignUserEmail) {
		int projectId = projectDao.getProjectID(projectName, schema);
		if (projectId == 0) {
			try {
				throw new ProjectNotFoundException("No project found with name " + projectName);
			} catch (ProjectNotFoundException e) {
				e.printStackTrace();
			}
		}

		List<Map<String, Object>> userDetails = userDAO.getUserDetailsByEmailId(emailId);
		if (userDetails.size() <= 0) {
			try {
				throw new UserNotFoundException("No User found with Email " + emailId);
			} catch (UserNotFoundException e) {
				e.printStackTrace();
			}
		}
		int loginUserID = Integer.parseInt(userDetails.get(0).get("user_id").toString());

		List<Map<String, Object>> assignUserDetails = userDAO.getUserDetailsByEmailId(assignUserEmail);
		if (assignUserDetails.size() <= 0) {
			try {
				throw new UserNotFoundException("No User found with Email " + assignUserEmail);
			} catch (UserNotFoundException e) {
				e.printStackTrace();
			}
		}
		String userId = assignUserDetails.get(0).get("user_id").toString();
		String userEmailId = assignUserDetails.get(0).get("email_id").toString();
		String fullName = userId + "::" + userEmailId;
		assignProjectToUsers(projectId, projectName, fullName, loginUserID, schema);

	}

	@Override
	public List<Map<String, Object>> getcurrentAssignedProject(int userId, String schema) {
		return projectDao.getcurrentAssignedProject(userId, schema);
	}

	@Override
	public List<Map<String, Object>> getProjectAssignedUser(int userId, String schema) {
		return projectDao.getProjectAssignedUser(userId, schema);
	}

	@Override
	public void assignProjectToUsers(String projectName, String schema, String loginUserEmailId,
			String assignUserEmail) {
		List<Map<String, Object>> mailData = projectDao.getMailSettingsFroAssignApp(schema);
		boolean sendMailStatus = false;
		List<Map<String,Object>> userDetails =userDAO.getUserDetailsByEmailId(loginUserEmailId);
		int loginUserID=Integer.parseInt(userDetails.get(0).get("user_id").toString());
		if(mailData.size() > 0){
			sendMailStatus = true;
		}else{
			logger.info("Mail details not available");
		}
		int projectId=projectDao.getProjectID(projectName,schema);
		List<UserProjectDetails> userProjectDetailsList = new ArrayList<UserProjectDetails>();
		List<UserNotification> userNotificationsList = new ArrayList<UserNotification>();
		String[] tokens = assignUserEmail.split(",");
		for (String token : tokens) {
			String[] subtokens = token.split(",");
			String userEmailId = subtokens[0];
			
			List<Map<String,Object>> assignUserDetails =userDAO.getUserDetailsByEmailId(userEmailId);
			int userId=Integer.parseInt(assignUserDetails.get(0).get("user_id").toString());
			UserProjectDetails userProjectDetails = new UserProjectDetails();
			userProjectDetails.setUserId(userId);
			userProjectDetails.setProjectId(projectId);
			userProjectDetails.setUserProjectId(0);
			userProjectDetailsList.add(userProjectDetails);

			userNotificationsList.add(notificationDao.setUserNotificationValues(1,
					"Assigned project '" + projectName + "'", "viewproject", userId,
					loginUserID, 2, 1));

			if(sendMailStatus) {
				try {
					projectDao.sendMailOnAssignProject(mailData,projectId, projectName, userId, userEmailId, schema);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		logger.info("Inserting assigned projects");
		projectDao.insertAssignedProjectRecords(userProjectDetailsList, schema);
		logger.info("Inserting notifications assign project");
		notificationDao.insertNotificationRecords(userNotificationsList, schema);
		
	}

	@Override
	public int unAssignProjectToUsers(String projectName, String emailId, String schema, String loginUserEmailId) {
		int projectId=projectDao.getProjectID(projectName,schema);
		List<Map<String,Object>> userDetails =userDAO.getUserDetailsByEmailId(emailId);
		int user=Integer.parseInt(userDetails.get(0).get("user_id").toString());
		List<Map<String,Object>> loginUserDetails =userDAO.getUserDetailsByEmailId(loginUserEmailId);
		int loginUser=Integer.parseInt(loginUserDetails.get(0).get("user_id").toString());
		int status = projectDao.unAssignAppToUser(projectId, user, schema);
		if (status == 1) {
			List<UserNotification> userNotificationsList = new ArrayList<UserNotification>();

			UserNotification userNotification = notificationDao.setUserNotificationValues(2,
					"Un-assigned project '" + projectName + "'", "viewproject", user, loginUser, 2, 1);

			userNotificationsList.add(userNotification);

			logger.info("Inserting notifications un assign project");
			notificationDao.insertNotificationRecords(userNotificationsList, schema);
			return 200;
		}else
			return 500;
	}

	@Override
	public List<Map<String, Object>> getSingleProject(int projectID, String schema) {
		// TODO Auto-generated method stub
		List<Map<String, Object>> singleProjectObject = projectDao.getSingleProject(projectID,schema);
		return singleProjectObject;
	}

	@Override
	public List<Map<String, Object>> getProjectAssignedUsers(int projectID, String schema) {
		return this.projectDao.getProjectAssignedUsers(projectID, schema);
	}
}

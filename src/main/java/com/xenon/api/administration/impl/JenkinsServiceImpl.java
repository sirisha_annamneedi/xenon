package com.xenon.api.administration.impl;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.administration.JenkinsService;
import com.xenon.api.common.JenkinsRequestHandlerService;
import com.xenon.buildmanager.domain.JenkinBuildDetails;

/**
 * 
 * @author bhagyashri.ajmera
 *
 */
public class JenkinsServiceImpl implements JenkinsService {

	private Map<String, Object> reponseData = new HashMap<String, Object>();
	private static final Logger logger = LoggerFactory.getLogger(JenkinsServiceImpl.class);

	@Autowired
	JenkinsRequestHandlerService jenkinsRequestHandlerService;

	@Autowired
	@Resource(name = "jenkinsProperties")
	private Properties jenkinsProperties;

	@Override
	public Map<String, Object> verifyRequestUrl(String url, String username, String passwordOrToken) {
		logger.info("Verify Request Url");
		Map<String, String> jenkinsResponse;
		try {
			jenkinsResponse = jenkinsRequestHandlerService.handleGetRequest(url, username, passwordOrToken);
			reponseData.put("responseCode", jenkinsResponse.get("responseCode").toString());
			reponseData.put("status", jenkinsResponse.get("status").toString());
		} catch (java.net.UnknownHostException e) {
			reponseData.put("responseCode", "401");
			reponseData.put("status", "Invalid Jenkins Url");
		} catch (IOException e) {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Something went wrong verifying token");
			e.printStackTrace();
		}
		return reponseData;
	}

	@Override
	public Map<String, Object> readJenkinsAllJobs(String url, String username, String passwordOrToken,String jenkinId) {
		logger.info("Reading Jenkins All Jobs");

		url = url + jenkinsProperties.getProperty("READ_JOBS");
		JSONObject jenkinsResponse;

		try {
			jenkinsResponse = jenkinsRequestHandlerService.handleGetRequestWithJSONResponse(url, username,
					passwordOrToken);

			if (jenkinsResponse.get("responseCode").toString().equalsIgnoreCase("200")) {
				reponseData.put("responseCode", jenkinsResponse.get("responseCode").toString());
				reponseData.put("status", getJobsList(jenkinsResponse,jenkinId));
				
			} else {
				reponseData.put("responseCode", jenkinsResponse.get("responseCode").toString());
				reponseData.put("status", jenkinsResponse.get("status"));
			}
		} catch (java.net.UnknownHostException e) {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Invalid Jenkins Url");
		} catch (IOException e) {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Something went wrong verifying token");
			e.printStackTrace();
		}
		return reponseData;
	}

	public List<Map<String, Object>> getJobsList(JSONObject jenkinsResponse,String jenkinId) {
		List<Map<String, Object>> jobsList = new ArrayList<Map<String, Object>>();
		JSONArray jobsJsonList = jenkinsResponse.getJSONArray("jobs");
		for (int cnt = 0; cnt < jobsJsonList.length(); cnt++) {
			JSONObject job = new JSONObject(jobsJsonList.get(cnt).toString());
			Map<String, Object> jobData = new HashMap<String, Object>();

			jobData.put("jobName", job.get("name"));
			jobData.put("url", job.get("url"));
			jobData.put("instanceId", jenkinId);
			if (job.get("color").toString().equalsIgnoreCase("blue"))
				jobData.put("status", "1");
			else if (job.get("color").toString().equalsIgnoreCase("yellow"))
				jobData.put("status", "2");
			else if (job.get("color").toString().equalsIgnoreCase("blue"))
				jobData.put("status", "3");
			else if (job.get("color").toString().equalsIgnoreCase("notbuilt"))
				jobData.put("status", "4");
			else
				jobData.put("status", "5");
			jobsList.add(jobData);
		}
		return jobsList;
	}
	
	@Override
	public Map<String, Object> readJenkinBuildDetails(String url, String username, String passwordOrToken,String jobName,int jobId,int buildno) {
		logger.info("Reading Jenkins build");

		url = url + (jenkinsProperties.getProperty("READ_BUILDS").replace("JOB_NAME", jobName).replace("BUILD_NUMBER", String.valueOf(buildno)));
		JSONObject jenkinsResponse;

		try {
			jenkinsResponse = jenkinsRequestHandlerService.handleGetRequestWithJSONResponse(url, username,
					passwordOrToken);
			
			if (jenkinsResponse.get("responseCode").toString().equalsIgnoreCase("200")) {
				reponseData.put("responseCode", jenkinsResponse.get("responseCode").toString());
				reponseData.put("status", setBuildDetailsFromBuildJSON(jenkinsResponse, jobId));
				
			} else {
				reponseData.put("responseCode", jenkinsResponse.get("responseCode").toString());
				reponseData.put("status", jenkinsResponse.get("status"));
			}
		} catch (java.net.UnknownHostException e) {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Invalid Jenkins Url");
		} catch (IOException e) {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Something went wrong verifying token");
			e.printStackTrace();
		}
		return reponseData;
	}
	
	@Override
	public Map<String, Object> fetchAllJenkinBuild(String url, String username, String passwordOrToken,String jobName,int jobId) {
		logger.info("Reading Jenkins build");

		url = url + (jenkinsProperties.getProperty("READ_BUILD_NUMBERS").replace("JOB_NAME", jobName));
		JSONObject jenkinsResponse;

		try {
			jenkinsResponse = jenkinsRequestHandlerService.handleGetRequestWithJSONResponse(url, username,
					passwordOrToken);
			
			if (jenkinsResponse.get("responseCode").toString().equalsIgnoreCase("200")) {
				reponseData.put("responseCode", jenkinsResponse.get("responseCode").toString());
				reponseData.put("status", setBuildNumbersFromBuildJSON(jenkinsResponse, jobId));
				
			} else {
				reponseData.put("responseCode", jenkinsResponse.get("responseCode").toString());
				reponseData.put("status", jenkinsResponse.get("status"));
			}
		} catch (java.net.UnknownHostException e) {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Invalid Jenkins Url");
		} catch (IOException e) {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Something went wrong verifying token");
			e.printStackTrace();
		}
		return reponseData;
	}
	
	@Override
	public JenkinBuildDetails setBuildDetailsFromBuildJSON(JSONObject buildDetailsJson,int jobId) throws JSONException {
		JenkinBuildDetails buildDetails = new JenkinBuildDetails();
		buildDetails.setBuildId(Integer.parseInt(buildDetailsJson.get("id").toString()));
		buildDetails.setBuildName(buildDetailsJson.get("displayName").toString());
		buildDetails.setDuration(buildDetailsJson.get("duration").toString());
		buildDetails.setEstimatedDuration(buildDetailsJson.get("estimatedDuration").toString());
		JSONArray jsonArray = new JSONArray(buildDetailsJson.get("actions").toString());
		for (int j = 0; j < jsonArray.length(); j++) {
			if (jsonArray.get(j).toString().contains("totalCount")) {
				JSONObject countData = new JSONObject(jsonArray.get(j).toString());
				buildDetails.setTotalCount(Integer.parseInt(countData.get("totalCount").toString()));
				buildDetails.setSkipCount(Integer.parseInt(countData.get("skipCount").toString()));
				buildDetails.setFailCount(Integer.parseInt(countData.get("failCount").toString()));
				break;
			}
		}
		buildDetails.setJobId(jobId);
		if(buildDetailsJson.get("result").toString().equalsIgnoreCase("SUCCESS"))
			buildDetails.setResult(1);
		else if(buildDetailsJson.get("result").toString().equalsIgnoreCase("UNSTABLE"))
			buildDetails.setResult(2);
		else if(buildDetailsJson.get("result").toString().equalsIgnoreCase("FAILURE"))
			buildDetails.setResult(3);
		else 
			buildDetails.setResult(4);
		Timestamp time=new Timestamp(Long.parseLong(buildDetailsJson.get("timestamp").toString()));
		buildDetails.setTimestamp(time.toString());
		buildDetails.setUrl(buildDetailsJson.get("url").toString());
		return buildDetails;
	}
	
	@Override
	public List<Integer> setBuildNumbersFromBuildJSON(JSONObject buildDetailsJson,int jobId) throws JSONException {
		List<Integer> allBuildNumbers = new ArrayList<Integer>();
		if(buildDetailsJson.get("builds") != null && !buildDetailsJson.get("builds").toString().isEmpty()) {
			JSONArray jsonArray = new JSONArray(buildDetailsJson.get("builds").toString());
			for (int j = 0; j < jsonArray.length(); j++) {
				JSONObject buildRecord = new JSONObject(jsonArray.get(j).toString());
				allBuildNumbers.add(Integer.valueOf(buildRecord.get("id").toString()));
			}
		}
		
		return allBuildNumbers;
	}
	
	@Override
	public Map<String, Object> triggerBuild(String url, String username, String passwordOrToken,String jobName,String buildToken) {
		logger.info("trigger new build");
		url = url + (jenkinsProperties.getProperty("TRIGGER_BUILD").replace("JOB_NAME", jobName)).replace("TOKEN_NAME", buildToken);
		Map<String, String> jenkinsResponse;
		try {
			jenkinsResponse = jenkinsRequestHandlerService.handleGetRequest(url, username, passwordOrToken);
			reponseData.put("responseCode", jenkinsResponse.get("responseCode").toString());
			reponseData.put("status", jenkinsResponse.get("status").toString());
		} catch (java.net.UnknownHostException e) {
			reponseData.put("responseCode", "401");
			reponseData.put("status", "Invalid Jenkins Url");
		} catch (IOException e) {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Something went wrong verifying token");
			e.printStackTrace();
		}
		return reponseData;
	}
}

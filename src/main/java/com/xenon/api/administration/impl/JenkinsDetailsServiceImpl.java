package com.xenon.api.administration.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.administration.dao.JenkinsDetailsDao;
import com.xenon.administration.domain.JenkinsDetails;
import com.xenon.api.administration.JenkinsDetailsService;
import com.xenon.api.common.JenkinsRequestHandlerService;
import com.xenon.buildmanager.domain.JenkinsServerDetails;
import com.xenon.buildmanager.dao.JenkinsRoleAccessDao;


public class JenkinsDetailsServiceImpl implements JenkinsDetailsService{


	private static final Logger logger = LoggerFactory.getLogger(JenkinsDetailsServiceImpl.class);
	
	private Map<String, Object> reponseData = new HashMap<String, Object>();

	@Autowired
	JenkinsRequestHandlerService jenkinsRequestHandlerService;

	@Autowired
	@Resource(name = "jenkinsProperties")
	private Properties jenkinsProperties;
	
	@Autowired
	private JenkinsDetailsDao jenkinsDetailsDao;
	
	@Autowired
	private JenkinsRoleAccessDao jenkinsRoleAccessDao;
	
	@Override
	public int insertJenkinsInstance(String serverName, String username, String jenkinsUrl, String apiToken,
			String instanceStatus, String schema) {
		
		return jenkinsDetailsDao.insertJenkinsInstance(serverName, username, jenkinsUrl, apiToken, instanceStatus, schema);
	}

	@Override
	public List<Map<String, Object>> getAllInstances(String schema, String userId) {
		return jenkinsDetailsDao.getAllInstances(schema,userId);
	}	
	
	@Override
	public int insertJenkinsJob(int instanceName,String jenkinsJobName,String jobName,String jobDescription,int jobStatus,int buildStatus,String buildToken,String schema,int userID){
		JenkinsDetails jenkinsDetails=new JenkinsDetails();
		jenkinsDetails.setjenkinsJobName(jenkinsJobName);
		jenkinsDetails.setjobName(jobName);
		jenkinsDetails.setjobDescription(jobDescription);
		jenkinsDetails.setInstanceId(instanceName);
		jenkinsDetails.setjobStatus(jobStatus);
		jenkinsDetails.setbuildStatus(buildStatus);
		jenkinsDetails.setbuildToken(buildToken);
		
		int flag=jenkinsDetailsDao.insertJenkinsJob(jenkinsDetails,schema,userID);
		if (flag == 1) {
			
			return 201;
		} else {
			return 200;
		}
		
	}
	

	@Override
	public JenkinsDetails getJenkinsJobDetails(int jobId,String schema) {
		JenkinsDetails jenkinsDetails = new JenkinsDetails();
		List<Map<String, Object>> jobDetailsFromDatabase=jenkinsDetailsDao.getJenkinsJobDetails(jobId,schema);
		if(jobDetailsFromDatabase!=null && jobDetailsFromDatabase.size()>0 && !jobDetailsFromDatabase.get(0).isEmpty()) {
			jenkinsDetails.setJobId(Integer.parseInt(""+jobDetailsFromDatabase.get(0).get("jenkin_jobId")));
			jenkinsDetails.setInstanceId(Integer.parseInt(""+jobDetailsFromDatabase.get(0).get("instance")));
			jenkinsDetails.setjenkinsJobName(jobDetailsFromDatabase.get(0).get("jenkin_job").toString());
			jenkinsDetails.setjobName(jobDetailsFromDatabase.get(0).get("job_name").toString());
			jenkinsDetails.setjobDescription(jobDetailsFromDatabase.get(0).get("job_description").toString());
			jenkinsDetails.setjobStatus(Integer.parseInt(""+jobDetailsFromDatabase.get(0).get("job_status")));
			jenkinsDetails.setbuildToken(jobDetailsFromDatabase.get(0).get("build_trigger_token").toString());
		}
		return jenkinsDetails;
	}

	
	@Override
	public Map<String,Object> createJenkinsJob(String schema,int userId){
		Map<String,Object> modelData=new HashMap<String,Object>();
		List<Map<String, Object>> jenkinsInstance=jenkinsDetailsDao.getAllInstances(schema,""+userId);
		modelData.put("instanceData", jenkinsInstance);
		return modelData;
	}

	@Override
	public int getServerIdByServerName(String serverName, String currentSchema) {
	
		return jenkinsDetailsDao.getServerIdByServerName(serverName, currentSchema);
	}

	@Override
	public List<Map<String, Object>> getApiTokenDetails(int serverInstanceId) {
		return jenkinsDetailsDao.getApiTokenDetails(serverInstanceId);
	}

	@Override
	public int insertNewApiToken(int serverInstanceId, int custId, int userId, String token, String windowsScript,
			String powershellScript) {
		return jenkinsDetailsDao.insertNewApiToken(serverInstanceId,custId,userId,token,windowsScript,powershellScript);
	}
		
	
	@Override
	public Map<String,Object> jenkinsReport(String schema,int userId,String instanceID){
		Map<String,Object> modelData=new HashMap<String,Object>();
		List<Map<String, Object>> jenkinsJob=jenkinsDetailsDao.getJobsDetails(userId,instanceID,schema);
		modelData.put("JobData", jenkinsJob);
		return modelData;
	}

	@Override
	public Map<String, Object> getJobPermissionWiseMembers(String jobId,String schema) {
		return jenkinsDetailsDao.getJobPermissionWiseMembers(jobId,schema);
	}
	
	@Override
	public Map<String, Object> getInstancePermissionWiseMembers(String instanceId,String schema) {
		return jenkinsDetailsDao.getInstancePermissionWiseMembers(instanceId,schema);
	}
	
	@Override
	public int assignJobPermissionToUsers(String[] allUserIds, String jobId,String schema) {
		return jenkinsDetailsDao.assignJobPermissionToUsers(allUserIds, jobId, schema);
	}
	
	@Override
	public int assignInstancePermissionToUsers(String[] allUserIds, String instanceId,String schema) {
		return jenkinsDetailsDao.assignInstancePermissionToUsers(allUserIds, instanceId, schema);
	}

	@Override
	public int removeUsersJobPermission(String userId,String jobId ,String schema) {
		return jenkinsDetailsDao.removeUsersJobPermission(userId,jobId, schema);
	}
	
	@Override
	public int removeUsersInstancePermission(String userId, String instanceId, String schema) {
		return jenkinsDetailsDao.removeUsersInstancePermission(userId, instanceId , schema);
	}

	@Override
	public int updateUsersJobPermission(String editPermissionUsers, String executePermissionUsers, String jobId,
			String coreSchema) {
		String[] editPermissionUsersArray=new String[0];
		String[] executePermissionUsersArray=new String[0];
		
		if(executePermissionUsers!=null && !executePermissionUsers.trim().equals("")) {
			if(executePermissionUsers.contains(",")) {
				executePermissionUsersArray=executePermissionUsers.split(",");
			}
			else {
				executePermissionUsersArray = new String[1];
				executePermissionUsersArray[0] = executePermissionUsers;
			}
		}
		
		if(editPermissionUsers!=null && !editPermissionUsers.trim().equals("")) {
			if(editPermissionUsers.contains(",")) {
				editPermissionUsersArray=editPermissionUsers.split(",");
			}
			else {
				editPermissionUsersArray = new String[1];
				editPermissionUsersArray[0] = editPermissionUsers;
			}
		}
		return jenkinsDetailsDao.updateUsersJobPermission(editPermissionUsersArray, executePermissionUsersArray, jobId, coreSchema);
	}
	
	@Override
	public int updateUsersInstancePermission(String editPermissionUsers,  String instanceId,
			String coreSchema) {
		String[] editPermissionUsersArray=new String[0];

		if(editPermissionUsers!=null && !editPermissionUsers.trim().equals("")) {
			if(editPermissionUsers.contains(",")) {
				editPermissionUsersArray=editPermissionUsers.split(",");
			}
			else {
				editPermissionUsersArray = new String[1];
				editPermissionUsersArray[0] = editPermissionUsers;
			}
		}
		return jenkinsDetailsDao.updateUsersInstancePermission(editPermissionUsersArray, instanceId, coreSchema);
	}

	@Override
	public int revokeUsersJobPermission(String userId,String jobId, String coreSchema) {
		return jenkinsDetailsDao.revokeUsersJobPermission(userId,jobId, coreSchema);
	}
	
	@Override
	public int revokeUsersInstancePermission(String userId,String instanceId ,String coreSchema) {
		return jenkinsDetailsDao.revokeUsersInstancePermission(userId,instanceId ,coreSchema);
	}
	
	@Override
	public Map<String,Object> getServerDetailsById(int serverId,String schema){
		Map<String,Object> modelData=new HashMap<String,Object>();
		List<Map<String, Object>> instanceDetails=jenkinsDetailsDao.getServerById(serverId,schema);
		modelData.put("instanceDetails", instanceDetails);
		return modelData;
	}

	@Override
	public Map<String, Object> getInstanceInformationById(int instanceId, String schema) {
		List<Map<String, Object>> instanceData = jenkinsDetailsDao.getServerById(instanceId, schema);
		if(instanceData!=null && instanceData.size()>0) {
			return instanceData.get(0);
		}
		return null;
	}

	@Override
	public int updateJenkinsJob(int jobId, String jobname, String jobDesciption, int status, String buildToken,
			String schema) {
		
		int flag=jenkinsDetailsDao.updateJenkinsJob(jobId, jobname, jobDesciption, status, buildToken, schema);
		if (flag == 1) {
			
			return 201;
		} else {
			return 200;
		}
	}
	public Map<String,Object> updateServerDetails(int serverId,String serverName,String username,String jenkinsUrl,String apiToken,int serverStatus,String schema){
		Map<String,Object> modelData=new HashMap<String,Object>();
		JenkinsServerDetails jenkinsServerDetails=new JenkinsServerDetails();
		jenkinsServerDetails.setServerId(serverId);
		jenkinsServerDetails.setServerName(serverName);
		jenkinsServerDetails.setUsername(username);
		jenkinsServerDetails.setJenkinsUrl(jenkinsUrl);
		jenkinsServerDetails.setApiToken(apiToken);
		jenkinsServerDetails.setServerStatus(serverStatus);
		int flag=jenkinsDetailsDao.updateInstanceDetails(jenkinsServerDetails,schema);
		if(flag==1){
			modelData.put("responseCode",201);
		}
		else{
			modelData.put("responseCode",500);
		}
	
		return modelData;
	}


	@Override
	public boolean checkJobAccess(String roleAccess,String userId,String jobId,String schema){
		boolean flag=jenkinsRoleAccessDao.checkJenkinJobAccess(roleAccess,userId,jobId,schema);
		return flag;
	}



	@Override
	public int checkIsUpdateEnabled(int jobId, int userID, String schema) {
		List<Map<String, Object>> availableJobs = jenkinsDetailsDao.checkIsUpdateEnabled(jobId, userID, schema);
		if(availableJobs!=null && availableJobs.size()>0 && !availableJobs.get(0).isEmpty()) {
			return 1;
		}
		return 0;
	}
	
	@Override
	public int checkIsExecuteEnabled(int jobId, int userID, String schema) {
		List<Map<String, Object>> availableJobs = jenkinsDetailsDao.checkIsExecuteEnabled(jobId, userID, schema);
		if(availableJobs!=null && availableJobs.size()>0 && !availableJobs.get(0).isEmpty()) {
			return 1;
		}
		return 0;
	}

}

package com.xenon.api.administration.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.administration.JenkinsBuildService;
import com.xenon.api.administration.JenkinsService;
import com.xenon.buildmanager.dao.JenkinBuildDAO;
import com.xenon.buildmanager.domain.JenkinBuildDetails;

/**
 * 
 * @author bhagyashri.ajmera
 *
 */
public class JenkinsBuildServiceImpl implements JenkinsBuildService {

	private Map<String, Object> reponseData = new HashMap<String, Object>();
	private static final Logger logger = LoggerFactory.getLogger(JenkinsBuildServiceImpl.class);

	@Autowired
	JenkinsService jenkinsService;

	@Autowired
	JenkinBuildDAO jenkinBuildDAO;
	
	@Autowired
	@Resource(name = "jenkinsProperties")
	private Properties jenkinsProperties;

	@Override
	public Map<String, Object> insertJenkinsBuild(String url, String username, String passwordOrToken, String schema) {
		logger.info("Verify Request Url");
		Map<String, String> jenkinsResponse = new HashMap<String, String>();
		try {
			JenkinBuildDetails buildDetails = null;
			int flag = jenkinBuildDAO.insertBuild(buildDetails, schema);
			
			if(flag==1){
				reponseData.put("responseCode", jenkinsResponse.get("responseCode").toString());
				reponseData.put("status", jenkinsResponse.get("status").toString());
			}else{
				reponseData.put("responseCode", "500");
				reponseData.put("status", "Build insertion unsuccessful");
			}
			
		}  catch (Exception e) {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Something went wrong inserting build");
			e.printStackTrace();
		}
		return reponseData;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object>  insertJenkinsBuildDetails(String jobName,String apiToken,String buildNo){
		int flag=0;
		
		Map<String, Object> details = jenkinBuildDAO.updateJenkinsBuildDetails(jobName, apiToken, buildNo);
		
		List<Map<String, Object>> instanceDetails= (List<Map<String, Object>>) details.get("#result-set-1");
		//[{jenkin_id=1, jenkin_url=http://localhost, jenkin_token=3af63a024c1fdc6f3e33b1e32ae27898, jenkin_username=bajmera, jenkin_name=Local, jenkin_instance_status=1}]
		List<Map<String, Object>> jobDetails= (List<Map<String, Object>>) details.get("#result-set-2");
		
		List<Map<String, Object>> custDetails= (List<Map<String, Object>>) details.get("#result-set-3");
		
		Map<String, Object> buildDetails = new HashMap<String, Object>();
		Map<String, Object> allBuilds = new HashMap<String, Object>();
		JenkinBuildDetails jenkinBuildDetails=new JenkinBuildDetails();
		if(instanceDetails.size()>0  &&  jobDetails.size()>0)
		{
			allBuilds = jenkinsService.fetchAllJenkinBuild(instanceDetails.get(0).get("jenkin_url").toString(), instanceDetails.get(0).get("jenkin_username").toString(), instanceDetails.get(0).get("jenkin_token").toString(), jobName, Integer.parseInt(jobDetails.get(0).get("jenkin_jobId").toString()));
			logger.info("All builds from jenkins server: "+allBuilds);
			
			if(allBuilds.get("responseCode").toString().equalsIgnoreCase("200")){
				List<Integer> allBuildNumbers =  (List<Integer>) allBuilds.get("status");
				for(Integer eachBuildNumber : allBuildNumbers) {
					List<Map<String, Object>> eachBuildData = jenkinBuildDAO.getBuildDetailsByBuildNumber(Integer.parseInt(jobDetails.get(0).get("jenkin_jobId").toString()), custDetails.get(0).get("customer_schema").toString(), eachBuildNumber);
					logger.info("Each build Data: "+eachBuildNumber+" : "+eachBuildData);
					if(eachBuildData.isEmpty()) {
						buildDetails = jenkinsService.readJenkinBuildDetails(instanceDetails.get(0).get("jenkin_url").toString(), instanceDetails.get(0).get("jenkin_username").toString(), instanceDetails.get(0).get("jenkin_token").toString(), jobName, Integer.parseInt(jobDetails.get(0).get("jenkin_jobId").toString()), eachBuildNumber);
						if(buildDetails.get("responseCode").toString().equalsIgnoreCase("200")){
							jenkinBuildDetails =(JenkinBuildDetails) buildDetails.get("status");
							
							flag = jenkinBuildDAO.insertBuild(jenkinBuildDetails, custDetails.get(0).get("customer_schema").toString());
							if(flag==1){
								reponseData.put("responseCode", "");
								reponseData.put("status", "");
							}else{
								reponseData.put("responseCode", "500");
								reponseData.put("status", "Build insertion unsuccessful");
							}
						}else{
							reponseData.put("responseCode", "500");
							reponseData.put("status", "Build insertion unsuccessful");
						}
					}
				}
				
				
			}	
			
			
		}else{
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Build insertion unsuccessful");
		}
		
		return reponseData;
	}
	
	@Override
	public Map<String,Object> getBuildHistory(int jobId,String schema){
		Map<String,Object> modelData=new HashMap<String,Object>();
		List<Map<String, Object>> jenkinsJob=jenkinBuildDAO.getBuildDetails(jobId,schema);
		modelData.put("buildData", jenkinsJob);
		return modelData;
	}
	
	@Override
	public Map<String,Object> getBuildTriggerData(String schema,int jobId){
		Map<String,Object> modelData=new HashMap<String,Object>();
		List<Map<String, Object>> buildData=jenkinBuildDAO.getBuildTriggerDetails(schema,jobId);
		modelData.put("buildTrigger", buildData);
		return modelData;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object>  updateJenkinsBuildDetails(String jobName,String apiToken,String buildNo){
		int flag=0;
		
		Map<String, Object> details = jenkinBuildDAO.updateJenkinsBuildDetails(jobName, apiToken, buildNo);
		
		List<Map<String, Object>> instanceDetails= (List<Map<String, Object>>) details.get("#result-set-1");
		//[{jenkin_id=1, jenkin_url=http://localhost, jenkin_token=3af63a024c1fdc6f3e33b1e32ae27898, jenkin_username=bajmera, jenkin_name=Local, jenkin_instance_status=1}]
		List<Map<String, Object>> jobDetails= (List<Map<String, Object>>) details.get("#result-set-2");
		
		List<Map<String, Object>> custDetails= (List<Map<String, Object>>) details.get("#result-set-3");
		
		Map<String, Object> buildDetails = new HashMap<String, Object>();
		JenkinBuildDetails jenkinBuildDetails=new JenkinBuildDetails();
		if(instanceDetails.size()>0  &&  jobDetails.size()>0)
		{
			buildDetails = jenkinsService.readJenkinBuildDetails(instanceDetails.get(0).get("jenkin_url").toString(), instanceDetails.get(0).get("jenkin_username").toString(), instanceDetails.get(0).get("jenkin_token").toString(), jobName, Integer.parseInt(jobDetails.get(0).get("jenkin_jobId").toString()), Integer.parseInt(buildNo));
			if(buildDetails.get("responseCode").toString().equalsIgnoreCase("200")){
				jenkinBuildDetails =(JenkinBuildDetails) buildDetails.get("status");
				
				flag = jenkinBuildDAO.updateBuild(jenkinBuildDetails, custDetails.get(0).get("customer_schema").toString());
				if(flag==1){
					reponseData.put("responseCode", "");
					reponseData.put("status", "");
				}else{
					reponseData.put("responseCode", "500");
					reponseData.put("status", "Build insertion unsuccessful");
				}
			}else{
				reponseData.put("responseCode", "500");
				reponseData.put("status", "Build insertion unsuccessful");
			}
			
		}else{
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Build insertion unsuccessful");
		}
		
		return reponseData;
	}
	

}
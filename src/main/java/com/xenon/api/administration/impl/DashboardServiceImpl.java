package com.xenon.api.administration.impl;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.administration.DashboardService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.common.dao.AdminDashboardDAO;

public class DashboardServiceImpl implements DashboardService{


	@Autowired
	UtilsService utilsService;
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getDashboardData(String schema, int page, AdminDashboardDAO adminDao,
			ActivitiesDAO activitiesDAO, int custTypeId, String customerId, String timeZone) {
		Map<String,Object> modelData=new HashMap<String,Object>();
		int pageSize = 5;
		modelData.put("pageSize", pageSize);
		
		int startValue = (page-1)*pageSize;
		
		Map<String, Object> data = adminDao.getAdminDashData(startValue, pageSize, schema, custTypeId,customerId);
		int userSize = (Integer) data.get("count");
		List<Map<String, Object>> userList = (List<Map<String, Object>>) data.get("#result-set-1");
		List<Map<String, Object>> topActivities = null;
		try {
			topActivities = activitiesDAO.getTopActivities(schema);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < topActivities.size(); i++) {
			String dateInString = topActivities.get(i).get("activity_date").toString();
			topActivities.get(i).put("activity_date",
					utilsService.getDateTimeOfTimezone(timeZone, dateInString));
		}		
		modelData.put("nowDate", utilsService.getNowDateOfTimezone(timeZone));
		modelData.put("prevDate", utilsService.getPrevDateOfTimezone(timeZone));
		modelData.put("topActivities",topActivities);
		modelData.put("Users",userList);
		modelData.put("userSize", userSize);
		modelData.put("Projects", data.get("#result-set-2"));
		modelData.put("Counts",data.get("#result-set-3"));
		return modelData;
	}

}

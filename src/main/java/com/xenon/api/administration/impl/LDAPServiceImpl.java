package com.xenon.api.administration.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.administration.dao.LdapDAO;
import com.xenon.administration.domain.LDAPDetails;
import com.xenon.api.administration.LDAPService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.buildmanager.dao.UserDAO;

public class LDAPServiceImpl implements LDAPService {

	@Autowired
	LdapDAO ldapDao;

	@Autowired
	RoleAccessService roleAccessService;
	
	@Autowired
	UserDAO userDAO;

	private static final Logger logger = LoggerFactory.getLogger(LDAPServiceImpl.class);

	private Map<String, String> reponseData = new HashMap<String, String>();

	@SuppressWarnings("unused")
	@Override
	public Map<String, String> insertLDAPDetails(String ldapURL, String rootDN, String searchBase, String searchFilter,
			String userDistinguishedName, String userPassword, String displayNameAttr, String emailAddrAttr,
			String emailDomainName, int customerId,String ldapUserMail,String schema) {
		
		logger.info("Setting ldap details");
		LDAPDetails ldapDetails =new LDAPDetails();
		ldapDetails.setLdapUrl(ldapURL);
		ldapDetails.setRootDn(rootDN);
		ldapDetails.setUserSearchBase(searchBase);
		ldapDetails.setUserSearchFilter(searchFilter);
		ldapDetails.setUserDn(userDistinguishedName);
		ldapDetails.setUserPassword(userPassword);
		ldapDetails.setDisplayNameAttribute(displayNameAttr);
		ldapDetails.setEmailAddrAttribute(emailAddrAttr);
		ldapDetails.setEmailDomainName(emailDomainName);
		ldapDetails.setCustId(customerId);
		ldapDetails.setStatus(1);
		ldapDetails.setUserMail(ldapUserMail);
		try {
			Map<String, Object> result = ldapDao.insertLdapDetails(ldapDetails,schema);
			reponseData.put("responseCode", "201");
			reponseData.put("status", "LDAP details inserted successfully.");
			
		} catch (SQLException e) {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Something went wrong while inserting LDAP details");
		}
		/*if (flag == 1){
			reponseData.put("responseCode", "201");
			reponseData.put("status", "LDAP details inserted successfully.");
		}else{
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Something went wrong while inserting LDAP details");
		}*/
		return reponseData;
	}
	
	@Override
	public Map<String, String> testLdapDetails(String ldapURL, String rootDN, String userSearchBase,
			String ldapSearchFilter, String userDn, String userPassword,String ldapUserMail) {
		
		boolean status= ldapDao.testLdapDetails(ldapURL, rootDN, userSearchBase, ldapSearchFilter, userDn, userPassword,ldapUserMail);
		
		if(status){
			Map<String, Object> details = ldapDao.getLdapAdminUserDetails(ldapURL, rootDN, userSearchBase, ldapSearchFilter, userDn, userPassword, ldapUserMail);
			if((Boolean) details.get("UserFound")){
				reponseData.put("responseCode", "201");
				reponseData.put("status", "LDAP details tested successfully.");
			}else{
				reponseData.put("responseCode", "204");
				reponseData.put("status", "User mail does not matches.");
			}
			
		}else{
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Something went wrong while testing LDAP details");
		}
		return reponseData;
		
	}
	
	@Override
	public Map<String, String> authenticateLdapUser(String ldapURL, String rootDN, String userSearchBase,
			String ldapSearchFilter, String managerDn, String managerPass,String userDn,String userPassword, boolean IsNewUser) {
		
		Map<String, Object> details= ldapDao.authenticateLdapUser(ldapURL, rootDN, userSearchBase, ldapSearchFilter, managerDn, managerPass, userDn, userPassword,IsNewUser);
		if((Boolean) details.get("authFlag")){
			reponseData.put("responseCode", "201");
			reponseData.put("status", "LDAP details tested successfully.");
		}else{
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Something went wrong while testing LDAP details");
		}
		return reponseData;
		
	}
	
	@Override
	public Map<String, String> updateCstomerLdapStatus(int custId,String custSchema) {
		
		int status= ldapDao.updateCustomerLdapStatus(custId,custSchema);
		if(status>0){
			reponseData.put("responseCode", "201");
			reponseData.put("status", "LDAP details updated successfully.");
		}else{
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Something went wrong while updating LDAP details");
		}
		return reponseData;
		
	}
	
	@Override
	public Map<String, Object> getCustomerLdapDetails(int custId) {
		
		Map<String, Object> reponseMap=new HashMap<String, Object>();
		List<Map<String, Object>> ldapDetails= ldapDao.getCustomerLdapDetails(custId);
		reponseMap.put("responseCode", "201");
		reponseMap.put("ldapDetails",ldapDetails);
		reponseMap.put("status", "LDAP details read successfully.");
		return reponseMap;
		
	}
	
	@Override
	public Map<String, Object> getLdapAndOwnUserList(int custId,String custSchema) {
		
		Map<String, Object> reponseMap=new HashMap<String, Object>();
		List<Map<String, Object>> ldapDetails= ldapDao.getCustomerLdapDetails(custId);
		if(ldapDetails.size()>0){
			
			List<Map<String, Object>> ldapInactiveUserList = userDAO.getAllLdapInactiveUserDetails(custSchema);
			
			List<Map<String, Object>> ldapUserDetails= ldapDao.getLdapUserList(ldapDetails.get(0).get("ldap_url").toString(), ldapDetails.get(0).get("root_dn").toString(), ldapDetails.get(0).get("user_search_base").toString(),
					ldapDetails.get(0).get("user_search_filter").toString(), ldapDetails.get(0).get("user_dn").toString(), ldapDetails.get(0).get("user_password").toString(),custSchema);
			reponseMap.put("responseCode", "201");
			reponseMap.put("ldapDetails",ldapDetails);
			reponseMap.put("userDetailsList", ldapInactiveUserList);
			reponseMap.put("ldapUserDetails", ldapUserDetails);
			reponseMap.put("status", "LDAP details read successfully.");
		}else{
			reponseMap.put("responseCode", "500");
			reponseMap.put("status", "LDAP details read successfully.");
		}
		
		return reponseMap;
		
	}
	
	@Override
	public Map<String, String> mapXenonUserToLdapUser(String[] xenonUser,String[] ldapUser,String custSchema) {
		
		int status= ldapDao.mapXenonUserToLdapUser(xenonUser,ldapUser,custSchema);
		if(status>0){
			reponseData.put("responseCode", "201");
			reponseData.put("status", "LDAP details updated successfully.");
		}else{
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Something went wrong while updating LDAP details");
		}
		return reponseData;
		
	}

}

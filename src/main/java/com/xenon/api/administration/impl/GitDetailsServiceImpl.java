package com.xenon.api.administration.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LsRemoteCommand;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.util.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.administration.dao.GitInstanceDao;
import com.xenon.administration.domain.GitProjectDetails;
import com.xenon.administration.domain.GitRepositoryDetails;
import com.xenon.api.administration.GitDetailsService;

public class GitDetailsServiceImpl implements GitDetailsService {

	@Autowired
	GitInstanceDao gitInstanceDao;

	@Override
	public int insertGitProject(String gitName, String gitUrl, String userName, String apiToken, String gitStatus,
			int userId, String schema) {
		GitProjectDetails gitProjectDetail = new GitProjectDetails();
		gitProjectDetail.setGitProjectName(gitName);
		gitProjectDetail.setGitUrl(gitUrl);
		gitProjectDetail.setUserName(userName);
		gitProjectDetail.setApiToken(apiToken);
		gitProjectDetail.setGitStatus(gitStatus);
		gitProjectDetail.setUserId(userId);
		int flag = gitInstanceDao.insertGitProject(gitProjectDetail, schema);
		if (flag == 1)
			return 201;
		else
			return 500;
	}
	
	@Override
	public int insertGitRepository(String instanceId, String branchName, String repositoryName, String userId, String schema) {
		GitRepositoryDetails gitRepositoryDetails = new GitRepositoryDetails();
		gitRepositoryDetails.setBranchName(branchName);
		gitRepositoryDetails.setInstanceId(Integer.parseInt(instanceId));
		gitRepositoryDetails.setRepositoryName(repositoryName);
		gitRepositoryDetails.setUserId(userId);
				
		int flag = gitInstanceDao.insertGitRepository(gitRepositoryDetails, schema);
		if (flag == 1)
			return 201;
		else
			return 500;
	}

	@Override
	public int updateUsersInstancePermission(String editPermissionUsers, String instanceId, String coreSchema) {
		String[] editPermissionUsersArray = new String[0];

		if (editPermissionUsers != null && !editPermissionUsers.trim().equals("")) {
			if (editPermissionUsers.contains(",")) {
				editPermissionUsersArray = editPermissionUsers.split(",");
			} else {
				editPermissionUsersArray = new String[1];
				editPermissionUsersArray[0] = editPermissionUsers;
			}
		}
		return gitInstanceDao.updateUsersInstancePermission(editPermissionUsersArray, instanceId, coreSchema);
	}

	@Override
	public int assignInstancePermissionToUsers(String[] allUserIds, String instanceId, String schema) {
		return gitInstanceDao.assignInstancePermissionToUsers(allUserIds, instanceId, schema);
	}

	@Override
	public List<Map<String, Object>> getAllInstances(String userId, String schema) {
		return gitInstanceDao.getAllInstances(userId, schema);
	}

	@Override
	public int updateGitProject(String serverId, String serverName, String username, String jenkinsUrl,
			String apiToken, String instanceStatus, String schema) {

		int flag = gitInstanceDao.updateGitProject(serverId, serverName, username, jenkinsUrl, apiToken,
				instanceStatus, schema);
		if (flag == 1) {

			return 201;
		} else {
			return 200;
		}
	}

	@Override
	public Map<String, Object> getInstancePermissionWiseMembers(String instanceId, String schema) {
		return gitInstanceDao.getInstancePermissionWiseMembers(instanceId, schema);
	}

	@Override
	public int removeUsersInstancePermission(String userId, String instanceId, String schema) {
		return gitInstanceDao.removeUsersInstancePermission(userId, instanceId, schema);
	}

	@Override
	public int revokeUsersInstancePermission(String userId, String instanceId, String coreSchema) {
		return gitInstanceDao.revokeUsersInstancePermission(userId, instanceId, coreSchema);
	}

	@Override
	public int testGitConnection(String url, String userName, String apiToken) {
		CredentialsProvider cp = new UsernamePasswordCredentialsProvider(userName, apiToken);
		try {
			LsRemoteCommand remote = Git.lsRemoteRepository().setRemote(url).setCredentialsProvider(cp);
			remote.call();
			return 201;
		} catch (Exception e) {
			return 500;
		}
	}

	@Override
	public List<Map<String, Object>> getInstanceDetailsById(int instanceId, String userId, String schema) {
		return gitInstanceDao.getInstancesDetailsById(instanceId, userId, schema);
	}

	@Override
	public int getCheckOut(String projectUrl, String username, String apiToken, String branchName) {
		CredentialsProvider cp = new UsernamePasswordCredentialsProvider(username, apiToken);
		String location = "D:\\GitRepo\\";
		try {
			File dir = new File(location + "Project");
			if (dir.exists()) {
				FileUtils.delete(dir,FileUtils.RECURSIVE); 
				dir.mkdirs();
			}
			CloneCommand cc = new CloneCommand().setCredentialsProvider(cp).setDirectory(dir).setURI(projectUrl)
					.setBranch(branchName);
		    Git git=cc.call();
		    git.close();
			return 201;
		} catch (Exception e) {
				return 500;
		}
	}
	
	@Override
	public List<Map<String, Object>> getProjectDetailsByRepositoryId(int repoId, int userId, String schema) {
		return gitInstanceDao.getProjectDetailsByRepositoryId(repoId, userId, schema);
	}
}

package com.xenon.api.administration;

import java.util.Map;

/**
 * This Service is responsible for LDAP operations handling.
 * <br><b>16th May 2017</b>
 * @author bhagyashri.ajmera
 * @version 2.0
 */
public interface LDAPService {
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method inserts LDAP details to database
	 * 
	 * @param ldapURL
	 * @param rootDN
	 * @param searchBase
	 * @param searchFilter
	 * @param userDistinguishedName
	 * @param userPassword
	 * @param displayNameAttr
	 * @param emailAddrAttr
	 * @param emailDomainName
	 * @param customerId
	 * @param schema 
	 * @return
	 */
	public Map<String, String> insertLDAPDetails(String ldapURL, String rootDN, String searchBase, String searchFilter,
			String userDistinguishedName, String userPassword,String displayNameAttr,String emailAddrAttr,String emailDomainName,int customerId,String ldapUserMail, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is used to test ldap settings
	 * 
	 * @param ldapURL
	 * @param rootDN
	 * @param userSearchBase
	 * @param ldapSearchFilter
	 * @param userDn
	 * @param userPassword
	 * @param ldapUserMail 
	 * @return
	 */
	Map<String, String> testLdapDetails(String ldapURL, String rootDN, String userSearchBase, String ldapSearchFilter,
			String userDn, String userPassword, String ldapUserMail);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method authenticates ldap user
	 * 
	 * @param ldapURL
	 * @param rootDN
	 * @param userSearchBase
	 * @param ldapSearchFilter
	 * @param managerDn
	 * @param managerPass
	 * @param userDn
	 * @param userPassword
	 * @return
	 */
	
	Map<String, String> authenticateLdapUser(String ldapURL, String rootDN, String userSearchBase,
			String ldapSearchFilter, String managerDn, String managerPass, String userDn, String userPassword,
			boolean IsNewUser);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method updates customer ldap status
	 * 
	 * @param custId
	 * @return
	 */
	Map<String, String> updateCstomerLdapStatus(int custId, String custSchema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives customer ldap details
	 * 
	 * @param custId
	 * @return
	 */
	Map<String, Object> getCustomerLdapDetails(int custId);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives ldap user details
	 * 
	 * @param custId
	 * @return
	 */
	Map<String, Object> getLdapAndOwnUserList(int custId,String custSchema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method updates the ldap user
	 * 
	 * @param xenonUser
	 * @param ldapUser
	 * @param custId
	 * @return
	 */

	Map<String, String> mapXenonUserToLdapUser(String[] xenonUser, String[] ldapUser, String custSchema);

}

package com.xenon.api.administration;

import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.xenon.buildmanager.domain.JenkinBuildDetails;

public interface JenkinsService {

	Map<String, Object> verifyRequestUrl(String url, String username, String passwordOrToken);

	Map<String, Object> readJenkinsAllJobs(String url, String username, String passwordOrToken, String jenkinId);

	Map<String, Object> readJenkinBuildDetails(String url, String username, String passwordOrToken, String jobName,
			int jobId, int buildno);

	Map<String, Object> fetchAllJenkinBuild(String url, String username, String passwordOrToken, String jobName,
			int jobId );

	
	JenkinBuildDetails setBuildDetailsFromBuildJSON(JSONObject buildDetailsJson, int jobId) throws JSONException;

	Map<String, Object> triggerBuild(String url, String username, String passwordOrToken, String jobName,
			String buildToken);

	List<Integer> setBuildNumbersFromBuildJSON(JSONObject buildDetailsJson, int jobId) throws JSONException;

}

/**
 * 
 */
package com.xenon.api.administration;

import java.util.Map;

/**
 * @author bhagyashri.ajmera
 *
 */
public interface JenkinsBuildService {

	Map<String, Object> insertJenkinsBuild(String url, String username, String passwordOrToken, String schema);

	Map<String, Object> updateJenkinsBuildDetails(String jobName, String apiToken, String buildNo);
   
    Map<String,Object> getBuildTriggerData(String schema,int jobId);

	Map<String, Object> getBuildHistory(int jobId, String schema);

	Map<String, Object> insertJenkinsBuildDetails(String jobName, String apiToken, String buildNo);
}

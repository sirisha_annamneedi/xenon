package com.xenon.api.administration;

import java.util.List;
import java.util.Map;

public interface GitRepositoryService {

	List<Map<String, Object>> getAllrepositories(String userId,String projectId, String schema);

	Map<String, Object> getRepositoryById(String repositoryId, String schema);

	int updateGitRepository(String instanceId, String branchName, String repositoryName, String repositoryId,
			String schema);

}

package com.xenon.api.administration;

import java.util.List;
import java.util.Map;

/**
 * This Service is responsible for Project related operations.
 * <br><b>5th May 2017</b>
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface ProjectService {
	
	public Map<String, String> insertProject(String projectName, String projectDescription, String bugPrefix, String tcPrefix,
			String proStatus, String schema,String currentDate);
	
	public int updateProject(int projectId, String projectDescription, int projectStatus, String schema);
	
	public void assignProjectToUsers(int appId, String appName, String users, int loginUser, String schema);
	
	public int setCurrentProject(int userId, int projectId, String SchemeName);
	
	public int updateCurrentProject(int userId, int projectId, String SchemeName);
	
	public int updateProjectByName(String projectName,String Projectdescription,int projectStatus,String schema);
	
	public void assignProjectToUsersByEmail(String projectName,String emailId,String schema,String assignUserEmail);
	
	/**
	 * This method is used to get current assigned project
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getcurrentAssignedProject(int userId,String schema);
	
	/**
	 * This method is used to get project assigned user
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getProjectAssignedUser(int userId,String schema);
	
	List<Map<String, Object>> getSingleProject(int projectID,String schema);
	
	public void assignProjectToUsers(String projectName,String schema,String loginUserEmailId,String assignUserEmail);
	
	public int unAssignProjectToUsers(String projectName, String emailId, String schema, String loginUserEmailId);

	Map<String, String> insertProject(String projectName, String jiraProjectName, String projectDescription,
			String bugPrefix, String tcPrefix, String proStatus, String schema, String currentDate);
	
	List<Map<String,Object>> getProjectAssignedUsers(int projectID , String schema);
}

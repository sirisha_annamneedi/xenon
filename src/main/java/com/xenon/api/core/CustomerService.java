package com.xenon.api.core;

import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.springframework.web.multipart.MultipartFile;

import com.xenon.buildmanager.dao.UserProfileDAO;
import com.xenon.core.dao.CustomerDAO;

/**
 * This Service is responsible for Customer related operations. <br>
 * <b>5th May 2017</b>
 * 
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface CustomerService {

	/**
	 * This is API to insert a customer details to database
	 * 
	 * @param custName
	 * @param custAddress
	 * @param custType
	 * @param bugTrackerStatus
	 * @param testManagerStatus
	 * @param custStatus
	 * @param customerSchema
	 * @return
	 */
	public int insertCustomer(String custName, String custAddress, int custType, int bugTrackerStatus,
			int testManagerStatus, int automationStatus, int onPremiseStatus, int scriptlessStatus, int custStatus,
			String customerSchema, String repoPath, int pluginJenkinStatus, int pluginGitStatus, int redwoodStatus,
			int apiStatus,String redwoodUrl);

	/**
	 * Description : This is API to edit a customer details
	 * 
	 * @param custName
	 * @param custAddress
	 * @param custStatus
	 * @param bugTrackerStatus
	 * @param testManagerStatus
	 * @param custType
	 * @param custId
	 * @return
	 */
	public int editcustomer(String custName, String custAddress, int custStatus, int bugTrackerStatus,
			int testManagerStatus, int scriptlessStatus, int automationStatus, int custType, int custId,
			int pluginJenkinStatus, int pluginGitStatus,int redwoodStatus,int apiStatus,String redwoodUrl);

	public Map<String, Object> editcustomeradmin(String schema, int userId, int custId, String firstName,
			String lastName, MultipartFile file, Properties configurationProperties, CustomerDAO custDetailsDAO,
			UserProfileDAO userProfileService);

	public String createCustomerSchema(String schema, String repoPath, String driver, String url, String username,
			String password);
	
	Map<String, Object> editIssueTracker(String schema, int userId, int custId, int issueTracker, int issueTrackerType,
			String issueTrackerHost, String integUser, String integPass, int jiraId, Properties configurationProperties,
			CustomerDAO custDetailsDAO, UserProfileDAO userProfileService, HttpSession session);

}

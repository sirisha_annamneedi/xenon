package com.xenon.api.core.impl;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import com.xenon.api.common.JiraIntegrationService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.api.core.CustomerService;
import com.xenon.buildmanager.dao.UserProfileDAO;
import com.xenon.buildmanager.domain.UserDetails;
import com.xenon.core.dao.CustomerDAO;
import com.xenon.core.dao.LicenceDetailsDAO;
import com.xenon.core.domain.CustomerDetails;
import com.xenon.database.DatabaseDao;

public class CustomerServiceImpl implements CustomerService {

	private static final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	LicenceDetailsDAO licenceDetailsDAO;

	@Autowired
	LoginService loginService;

	@Autowired
	UtilsService utilsService;

	@Autowired
	JiraIntegrationService jiraService;

	@Autowired
	DatabaseDao databaseDao;

	private BufferedReader bufferedReader = null;

	@Override
	public int insertCustomer(String custName, String custAddress, int custType, int bugTrackerStatus,
			int testManagerStatus, int automationStatus, int onPremiseStatus, int scriptlessStatus, int custStatus,
			String customerSchema, String repoPath, int pluginJenkinStatus, int pluginGitStatus, int redwoodStatus,
			int apiStatus, String redwoodUrl) {
		logger.info("inserting customer details");
		String filepath = repoPath + File.separator + "repository" + File.separator + "logo" + File.separator
				+ "xenon.png";
		File image = null;
		try {
			Resource resource = new ClassPathResource(filepath);
			image = resource.getFile();
			// image = ResourceUtils.getFile(filepath);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		InputStream imageIs = null;
		try {
			imageIs = new FileInputStream(image);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		logger.info("Getting image byte array");
		byte[] bytes = null;
		try {
			bytes = IOUtils.toByteArray(imageIs);
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info("Reading customer's licence details");
		List<Map<String, Object>> typeDetails = licenceDetailsDAO.getLicenceDetailsByTypeId(custType);
		int numberOfDays = Integer.parseInt(typeDetails.get(0).get("no_of_days").toString());
		logger.info("Getting customer details domain");
		CustomerDetails custDetails = new CustomerDetails();
		Pattern pt = Pattern.compile("[^a-zA-Z0-9]");
		Matcher match = pt.matcher(customerSchema);
		while (match.find()) {
			String s = match.group();
			customerSchema = customerSchema.replaceAll("\\" + s, "");
		}
		logger.info("Setting cutomer details");
		custDetails.setName(custName);
		custDetails.setAddress(custAddress);
		custDetails.setLogoUrl(bytes);
		custDetails.setCustomerType(custType);
		custDetails.setStartDate(utilsService.getCurrentDate());
		custDetails.setCreatedDate(utilsService.getCurrentDate());
		custDetails.setEndDate(utilsService.getCustomerEndDate(numberOfDays));
		custDetails.setBtStatus(bugTrackerStatus);
		custDetails.setTmStatus(testManagerStatus);
		custDetails.setAutomationStatus(automationStatus);
		custDetails.setOnPremiseStatus(onPremiseStatus);
		custDetails.setCustStatus(custStatus);
		custDetails.setCustomerSchema(customerSchema);
		custDetails.setScriptlessStatus(scriptlessStatus);
		custDetails.setJenkinStatus(pluginJenkinStatus);
		custDetails.setGitStatus(pluginGitStatus);
		custDetails.setRedwoodStatus(redwoodStatus);
		custDetails.setApiStatus(apiStatus);
		custDetails.setRedwoodUrl(redwoodUrl);
		logger.info("inserting cutomer details");
		int flag = customerDAO.insertCustomerDetails(custDetails);
		if (flag > 0)
			customerDAO.updateAutomationStatus(automationStatus, scriptlessStatus, customerSchema);
		return flag;
	}

	@Override
	public int editcustomer(String custName, String custAddress, int custStatus, int bugTrackerStatus,
			int testManagerStatus, int scriptlessStatus, int automationStatus, int custType, int custId,
			int pluginJenkinStatus, int pluginGitStatus, int redwoodStatus, int apiStatus, String redwoodUrl) {
		logger.info("Editing customer details");
		logger.info("Reading customer's licence details");
		List<Map<String, Object>> typeDetails = licenceDetailsDAO.getLicenceDetailsByTypeId(custType);
		int numberOfDays = Integer.parseInt(typeDetails.get(0).get("no_of_days").toString());
		CustomerDetails custDetails = new CustomerDetails();
		logger.info("Setting cutomer details");
		custDetails.setCustId(custId);
		custDetails.setAddress(custAddress);
		custDetails.setName(custName);
		custDetails.setBtStatus(bugTrackerStatus);
		custDetails.setTmStatus(testManagerStatus);
		custDetails.setAutomationStatus(automationStatus);
		custDetails.setCustStatus(custStatus);
		custDetails.setCustomerType(custType);
		custDetails.setStartDate(utilsService.getCurrentDate());
		custDetails.setCreatedDate(utilsService.getCurrentDate());
		custDetails.setScriptlessStatus(scriptlessStatus);
		custDetails.setEndDate(utilsService.getCustomerEndDate(numberOfDays));
		custDetails.setJenkinStatus(pluginJenkinStatus);
		custDetails.setGitStatus(pluginGitStatus);
		custDetails.setRedwoodStatus(redwoodStatus);
		custDetails.setApiStatus(apiStatus);
		custDetails.setRedwoodUrl(redwoodUrl);
		logger.info("Editing cutomer details");
		int flag = customerDAO.editCustomerDetails(custDetails);
		if (flag == 1)
			customerDAO.updateAutomationStatus(custId);
		return flag;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> editcustomeradmin(String schema, int userId, int custId, String firstName,
			String lastName, MultipartFile file, Properties configurationProperties, CustomerDAO custDetailsDAO,
			UserProfileDAO userProfileService) {
		logger.info("Editing customer details");
		Map<String, Object> modelData = new HashMap<String, Object>();
		int updateStatus = 0;
		boolean flag = false;
		logger.info("Setting values to domain");
		UserDetails user = new UserDetails();
		user.setUserId(userId);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setUpdateDate(utilsService.getCurrentDateTime());
		if (!file.isEmpty()) {
			byte[] bytes = null;
			try {
				bytes = file.getBytes();
			} catch (IOException e) {
				e.printStackTrace();
			}
			InputStream in = new ByteArrayInputStream(bytes);
			BufferedImage bImageFromConvert = null;
			try {
				bImageFromConvert = ImageIO.read(in);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String path = configurationProperties.getProperty("XENON_REPOSITORY_PATH") + File.separator + "repository"
					+ File.separator + "upload" + File.separator + "" + file.getOriginalFilename() + ".jpg";
			Resource resource = new ClassPathResource(path);
			File custLogo = null;
			try {
				custLogo = resource.getFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				ImageIO.write(bImageFromConvert, "png", custLogo);
			} catch (IOException e) {
				e.printStackTrace();
			}
			user.setUserPhotoName(path);
			logger.info("Setting customer customer details");
			CustomerDetails custDetails = new CustomerDetails();
			custDetails.setLogoName(path);
			custDetails.setCustId(custId);
			updateStatus = custDetailsDAO.editCustomerLogo(custDetails);
			if (updateStatus > 0) {
				logger.info("Customer edited successfully");
				flag = true;
			}
			custLogo.delete();

		}
		logger.info("Updating customer admin details");
		flag = userProfileService.updateProfile(user, schema);

		if (flag) {
			logger.info("Customer admin edited successfully");

			Map<String, Object> custData = custDetailsDAO.getAccountSettingsData(custId, schema);

			List<Map<String, Object>> custDetails = (List<Map<String, Object>>) custData.get("#result-set-1");
			modelData.put("customerLogo", loginService.getImage((byte[]) custDetails.get(0).get("customer_logo")));

			List<Map<String, Object>> custAdminDetails = (List<Map<String, Object>>) custData.get("#result-set-4");

			modelData.put("custTypes", (List<Map<String, Object>>) custData.get("#result-set-1"));
			modelData.put("CustDetails", custDetails);
			modelData.put("custAdminDetails", custAdminDetails);
			logger.info("Redirecting to account settings");

		}
		modelData.put("flag", flag);
		return modelData;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> editIssueTracker(String schema, int userId, int custId, int issueTracker,
			int issueTrackerType, String issueTrackerHost, String integUser, String integPass, int jiraId,
			Properties configurationProperties, CustomerDAO custDetailsDAO, UserProfileDAO userProfileService,
			HttpSession session) {
		logger.info("Editing customer details");
		Map<String, Object> modelData = new HashMap<String, Object>();
		int flag;
		logger.info("Setting values to domain");
		CustomerDetails customerDetails = new CustomerDetails();
		customerDetails.setCustId(custId);
		customerDetails.setIssueTracker(issueTracker);
		customerDetails.setIssueTrackerHost(issueTrackerHost);
		customerDetails.setIssueTrackerType(issueTrackerType);

		logger.info("Setting customer customer details");
		flag = custDetailsDAO.editCustomerJiraDetails(customerDetails);

		logger.info("Updating jira instance details");

		String currentSchema = session.getAttribute("cust_schema").toString();
		int updateJiraFlag;
		if (!(jiraService.getJiraDetails(currentSchema).size() > 0))
			updateJiraFlag = jiraService.insertJiraInstance(integUser, integPass, currentSchema);
		else
			updateJiraFlag = jiraService.updateJiraInstance(integUser, integPass, jiraId, currentSchema);

		if (flag > 0 && updateJiraFlag > 0) {
			logger.info("Customer admin edited successfully");

			Map<String, Object> custData = custDetailsDAO.getAccountSettingsData(custId, schema);

			List<Map<String, Object>> custDetails = (List<Map<String, Object>>) custData.get("#result-set-2");
			for (int i = 0; i < custDetails.size(); i++) {

				if (Integer.parseInt(custDetails.get(i).get("issue_tracker").toString()) == 1) {
					custDetails.get(i).put("connected_status", "Connected");
				} else {
					custDetails.get(i).put("connected_status", "Not Connected");
				}
			}
			List<Map<String, Object>> jiraDetails = jiraService.getJiraDetails(schema);
			modelData.put("CustDetails", custDetails);
			modelData.put("JiraDetails", jiraDetails);

			List<Map<String, Object>> issueTrackerTypeObject = (List<Map<String, Object>>) custData
					.get("#result-set-3");
			modelData.put("issueTrackerType", issueTrackerTypeObject);

			logger.info("Redirecting to account settings");

		}
		modelData.put("flag", flag);
		return modelData;
	}

	@Override
	public String createCustomerSchema(String schema, String repoPath, String driver, String url, String username,
			String password) {
		schema = schema + new SimpleDateFormat("MMddyyyyHHmmss").format(new Date());
		String sql = "CREATE SCHEMA `" + schema + "` ;";
		databaseDao.createTable(sql);
		int index = url.lastIndexOf("/");
		url = url.substring(0, index) + "/";
		try {
			// System.out.println("Repository Path: "+repoPath);
			createTables(schema, repoPath + "repository" + File.separator + "schema" + File.separator + "query.sql",
					driver, url, username, password);
			createFunctions(schema,
					repoPath + "repository" + File.separator + "schema" + File.separator + "functions.sql", driver, url,
					username, password);
			createTables(schema, repoPath + "repository" + File.separator + "schema" + File.separator + "data.sql",
					driver, url, username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return schema;
	}

	private void createFunctions(String schema, String sourcePath, String driver, String url, String username,
			String password) throws Exception {

		String q = "";

		Resource resource = new ClassPathResource(sourcePath);
		File f = resource.getFile();

		// File f = ResourceUtils.getFile(sourcePath); // source path is the
		// absolute path of
		// dumpfile.
		try {
			bufferedReader = new BufferedReader(new FileReader(f));
			String line = null;
			line = bufferedReader.readLine();
			while (line != null) {
				q = q + line + "\n";
				line = bufferedReader.readLine();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		String[] commands = q.split(";;");
		Connection conn = connectDB(schema, driver, url, username, password);

		Statement statement = conn.createStatement();
		for (String s : commands) {
			if (s.length() > 1) {
				statement.execute(s);
				// System.out.println(s);
			}
		}
		conn.close();
	}

	private void createTables(String schema, String sourcePath, String driver, String url, String username,
			String password) throws Exception {
		String q = "";

		Resource resource = new ClassPathResource(sourcePath);
		File f = resource.getFile();

		// File f = ResourceUtils.getFile(sourcePath); // source path is the
		// absolute path of
		// dumpfile.
		try {
			bufferedReader = new BufferedReader(new FileReader(f));
			String line = null;
			line = bufferedReader.readLine();
			while (line != null) {
				q = q + line + "\n";
				line = bufferedReader.readLine();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		String[] commands = q.split(";");
		Connection conn = connectDB(schema, driver, url, username, password);

		Statement statement = conn.createStatement();
		for (String s : commands) {
			if (s.length() > 1) {
				statement.execute(s);
				// System.out.println(s);
			}
		}
		conn.close();
	}

	private Connection connectDB(String schema, String driver, String url, String username, String password)
			throws Exception {
		Class.forName(driver);
		// System.out.println("Connecting to a selected database...");
		Connection conn = DriverManager.getConnection(url + schema, username, password);
		// System.out.println("Connected database successfully...");
		return conn;
	}

}

package com.xenon.api.bugtracker;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.xenon.util.service.FileUploadForm;

/**
 * This Service is responsible for Bug related operations.
 * <br><b>5th May 2017</b>
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface BugService {

	/**
	 * @author suresh.adling
	 * @description insert a bug using input parameters and send mail on
	 *              successful bug creation
	 * @param projectId
	 * @param moduleId
	 * @param summary
	 * @param statusId
	 * @param priorityId
	 * @param userId
	 * @param bugDescription
	 * @param schema
	 * @param assignedUserID
	 * @return boolean value
	 * @throws Exception
	 */
	public int insertBug(String projectId, String moduleId, String summary, String statusId, String priorityId,
			String severityId, String categoryId, String buildTypeId, String selectedBuildTypeId, String userId,
			String bugDescription, String schema, String assignedUserID,FileUploadForm uploadForm,String repoPath);
	
	/**
	 * @author suresh.adling
	 * @description update a bug using input parameters and send mail on
	 *              successful bug modification
	 * @param bugId
	 * @param statusId
	 * @param priorityId
	 * @param assignee
	 * @param prevStatusId
	 * @param prevPriorityId
	 * @param prevAssignee
	 * @param comments
	 * @param userId
	 * @param schema
	 * @return boolean value
	 * @throws Exception
	 */
	
	

	List<Map<String, Object>> insertBug(String projectId, String moduleId, String summary, String statusId, String priorityId,
			String severityId, String categoryId, String buildTypeId, String selectedBuildTypeId, String userId,
			String bugDescription, String schema, String assignedUserID, FileUploadForm uploadForm, String repoPath,
			String jiraId, String updated_date, String jiraResolution, String fixVersion, String create_date,String jiraReleaseId);
	
	
	public int updateBug(String bugId, String statusId, String priorityId, String assignee, String prevStatusId,
			String prevPriorityId, String severityId, String prevSeverityId, String categoryId, String prevCategoryId,
			String prevAssignee, String buildId, String prevBuildId, String prevBuildType, String comments,
			String userId, String schema,FileUploadForm uploadForm,String repoPath);
	
	/**
	 * @author navnath.damale
	 * @description update a bug asiign status using input parameters.
	 * 
	 * @param bugId
	 * @param assignee
	 * @param userId
	 * @param assignStatus
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	public int updateBugStatus(String bugId, String assignee, String userId, String assignStatus,
			String groupStatus, String groupId, String schema);
	/**
	 * @author abhay.thakur
	 * @description update jira id by bug id.
	 * 
	 * @param bugId
	 * @param jiraId
	 * @param schema
	 * @param schema
	 */
	public int updateJiraIdByBigId(String bugId, String jiraId, String userId,String schema);

	/**
	 * @author suresh.adling
	 * @description get all bug details
	 * @param projectId
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getAllBugDetails(int projectId, int userId, int startValue, int offsetValue, String schema);

	/**
	 * @author suresh.adling
	 * @description get bugs assigned to me
	 * @param projectId
	 * @param userID
	 * @param schema
	 * @return
	 */
	Map<String, Object> getAssignedToMeBugs(int projectId, int userID, int startValue, int offsetValue, String schema);

	/**
	 * @author
	 * @description get bugs reported by me
	 * @param projectId
	 * @param userID
	 * @param schema
	 * @return
	 */
	Map<String, Object> getReportedByMeBugs(int projectId, int userID, int startValue, int offsetValue, String schema);
	
	/**
	 * 
	 * @author bhagyashri.ajmera
	 * 
	 *         This method is to check whether customer exceeds his bug creation
	 *         limit or not
	 * 
	 * @param schema
	 * @param custTypeId
	 * @return
	 * @throws SQLException
	 */
	int createNewBugStatus(String schema, int custTypeId) throws SQLException;
	
	/**
	 * @author suresh.adling
	 * @description get data for create bug
	 * @param projectId
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getDataForCreateBug(int projectId, int userId, String schema);
	
	/**
	 * @author suresh.adling
	 * @description get bug details by bug prefix
	 * @param bugPrefix
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBugDetailsByPrefix(String bugPrefix, String schema);
	
	/**
	 * @author suresh.adling
	 * @description get comment attachment details by comment id
	 * @param commentId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getCommentAttachmentDeatils(int commentId, String schema);
	
	/**
	 * @author suresh.adling
	 * @description get bug attachment by attachment id
	 * @param attchmentId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBugAttachmentById(int attchmentId, String schema);
	
	/**
	 * @author suresh.adling
	 * @description get comment attachment by attachment id
	 * @param attchmentId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getCommentAttachmentById(int attchmentId, String schema);
	
	Map<String,Object> getPieChartReportData(String SourceData,String schema,int userID,int projectId);

	public int insertUpdateJira(String trackerType,String issue_tracker_project_key, String issue_tracker_current_release_id,String schema);

	List<Map<String, Object>> getBugIdMappedWithJiraId(String jiraId, String schema);

	int updateBug(String bugId, String statusId, String priorityId, String assignee, String prevStatusId,
			String prevPriorityId, String severityId, String prevSeverityId, String categoryId, String prevCategoryId,
			String prevAssignee, String buildId, String prevBuildId, String prevBuildType, String comments,
			String userId, String schema, FileUploadForm uploadForm, String repoPath,String bugTitle,String bugDescription, String updateJiraIssueDate,
			String resoultionDescription, String fixVersion,String jiraReleaseId);

	int deleteBug(int bugId, String schema);

	public int insertJiraRelease(String jiraReleaseName,String issue_tracker_project_key, String releaseDescription, String releaseStatus,
			String releaseDate, String jiraReleaseId, String schema);

	public int updateJiraRelease(String jiraReleaseId, String jiraReleaseName, String jiraProject,
			String releaseDescription, String releaseStatus, String releaseDate, String schema);

	
	int deleteAllBugs(String schema);
	

}

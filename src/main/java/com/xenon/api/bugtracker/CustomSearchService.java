package com.xenon.api.bugtracker;

import java.util.List;
import java.util.Map;

/**
 * This Service is responsible for Bug related operations. <br>
 * <b>6th July 2017</b>
 * 
 * @author bhagyashri.ajmera
 */
public interface CustomSearchService {

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method inserts custom search query
	 * 
	 * @param schema
	 * @param queryName
	 * @param query
	 * @param queryJson
	 * @param userId
	 * @return
	 */
	public Map<String, String> insertCustomSearchQuery(String schema, String queryName, String query, String queryJson,
			int userId);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads custom saved queries of user
	 * 
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getUserSavedCustomQueries(int userId, String schema);
	
	/**
	 * @author pooja.mugade
	 * 
	 * This method get bug data
	 * 
	 * @param userId
	 * @param schema
	 * @param getAllBugs
	 * @return
	 */
	Map<String, Object> getBuData(int userId, String schema,List<Map<String, Object>> getAllBugs);
	
	/**
	 * @author pooja.mugade
	 * 
	 * This method get bug data
	 * 
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String,Object>  getCustomQueryData(String userId, String schema,String jsonANDData,String queryName,int project);
	

	/**
	 * @author pooja.mugade
	 * 
	 * This method execute custom query
	 * 
	 * @param jsonANDData
	 * @param schema
	 * @return
	 */
	Map<String, Object> executeCustomQuery(String jsonANDData, String schema, int userId,int project);
	
	

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method inserts custom search query
	 * 
	 * @param schema
	 * @param queryName
	 * @param query
	 * @param queryJson
	 * @param userId
	 * @return
	 */
	Map<String, String> updateCustomSearchQuery(String schema, String queryName, String query, String queryJson,
			int userId, int queryId);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method inserts custom search query
	 *         
	 * @param schema
	 * @param queryName
	 * @param query
	 * @param queryJson
	 * @param userId
	 * @return
	 */
	Map<String, String> insertPiechartCustomSearchQuery(String schema, String queryName, String query, String queryJson,
			int userId);
	
	/**
	 * @author pooja.mugade
	 * 
	 * This method reads custom saved queries of user for pie chart
	 * 
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getUserSavedPieChartCustomQueries(int userId, String schema);
	
	/**
	 * @author pooja.mugade
	 * 
	 * This method reads custom saved queries of user for pie chart
	 * 
	 * @param userId
	 * @param schema
	 * @param jsonANDData
	 * @param queryName
	 * @return
	 */
	Map<String,Object>  savePieChartQuery(String userId, String schema,String jsonANDData,String queryName);
	
	
	/**
	 * @author pooja.mugade
	 * 
	 * This method reads custom saved queries of user for pie chart
	 * 
	 * @param schema
	 * @param jsonANDData
	 * @return
	 */
	 Map<String,Object> executeCustomPieChartQuery(String jsonANDData,String schema);
	
	 	/**
		 * @author pooja.mugade
		 * 
		 * This method show pie chart data
		 * 
		 * @param userID
		 * @param schema
		 * @return
		 */
	 Map<String,Object> pieChartData(int userID,String schema );
	 
	 /**
		 * @author pooja.mugade
		 * 
		 * This method update pie chart data of table
		 * 
		 * @param query
		 * @param schema
		 * @param queryJson
		 * @param queryName
		 * @return
		 */

	 Map<String, String> updatePieChartQuery(String schema, String queryName, String query, String queryJson,
				int userId,int queryId);
	 
	 /**
		 * @author pooja.mugade
		 * 
		 * This method get pie chart data
		 * 
		 * @param schema
		 * @param jsonANDData
		 * @return
		 */
	 Map<String,Object> getCustomQueryPieChartData(String userId, String schema,String jsonANDData,String queryName);

	 /**
	  * @author bhagyashri.ajmera
	  * 
	  * This method is used to delete custom search query
	  * 
	  * @param queryId
	  * @param schema
	  * @return
	  */
	Map<String, String> deleteCustomSearchQuery(int queryId, String schema);

}

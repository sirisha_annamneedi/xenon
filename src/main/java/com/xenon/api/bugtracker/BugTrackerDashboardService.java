package com.xenon.api.bugtracker;

import java.util.Map;
import java.util.Properties;

import com.xenon.bugtracker.dao.BugDAO;
import com.xenon.bugtracker.dao.BugGroupDAO;
import com.xenon.buildmanager.dao.ActivitiesDAO;

/**
 * This Service is responsible for BugTracker Dashboard related operations.
 * <br><b>5th May 2017</b>
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface BugTrackerDashboardService {

	public Map<String,Object> getDashboardData(String schema,int userId,int projectId,String userTimeZone,Object userprofilePhoto,String fullName,
			String role,int page);
	
	public Map<String,Object> viewAllBTDAshboard(int userId,String schema,String userTimeZone,Object userProfilePhoto,
			String userFullName,String role);
	
}

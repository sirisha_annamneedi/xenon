package com.xenon.api.bugtracker.impl;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.Resource;

import org.apache.commons.lang3.ArrayUtils;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.bugtracker.BugTrackerDashboardService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.bugtracker.dao.BugDAO;
import com.xenon.bugtracker.dao.BugGroupDAO;
import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.controller.BugTrackerDashboardController;

public class BugTrackerDashboardServiceImpl implements BugTrackerDashboardService {

	private static final Logger logger = LoggerFactory.getLogger(BugTrackerDashboardController.class);

	@Resource(name = "colorProperties")
	private Properties colorProperties;

	@Autowired
	BugDAO bugDAO;

	@Autowired
	BugGroupDAO bugGroupDAO;

	@Autowired
	ActivitiesDAO activitiesDAO;

	@Autowired
	LoginService loginService;

	@Autowired
	UtilsService utilsService;

	@SuppressWarnings({ "unchecked" })
	@Override
	public Map<String, Object> getDashboardData(String schema, int userId, int projectId, String userTimeZone,
			Object userprofilePhoto, String fullName, String role, int page) {
		List<Map<String, Object>> OpenBugList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> getBugtable = new ArrayList<Map<String, Object>>();
		Map<String, Object> bugStat = new HashMap<String, Object>();
		Map<String, Object> modelData = new HashMap<String, Object>();

		int pageSize = 10;
		// model.addAttribute("pageSize",pageSize);
		modelData.put("pageSize", pageSize);
		int startValue = (page - 1) * pageSize;
		logger.info("Session is active, userId is " + userId + " and projectId is " + projectId + " Customer Schema "
				+ schema);
		Map<String, Object> bugGraph = bugDAO.getBugDashboard(projectId, userId, startValue, pageSize, schema);

		List<Map<String, Object>> userWiseBugs = (List<Map<String, Object>>) bugGraph.get("#result-set-1");

		List<Map<String, Object>> userList = (List<Map<String, Object>>) bugGraph.get("#result-set-2");

		List<Map<String, Object>> bugStatusList = (List<Map<String, Object>>) bugGraph.get("#result-set-3");
		List<Map<String, Object>> CatwisebugCount = (List<Map<String, Object>>) bugGraph.get("#result-set-7");
		List<Map<String, Object>> JirareleaseBug = (List<Map<String, Object>>) bugGraph.get("#result-set-8");
		List<Map<String, Object>> BugList = (List<Map<String, Object>>) bugGraph.get("#result-set-9");

		List<Map<String, Object>> BugCountPerPriority = (List<Map<String, Object>>) bugGraph.get("#result-set-10");
		List<Map<String, Object>> ReleaseIdWithPriority = (List<Map<String, Object>>) bugGraph.get("#result-set-11");
		List<Map<String, Object>> CurrentRelease = (List<Map<String, Object>>) bugGraph.get("#result-set-12");
		List<Map<String, Object>> bugsWithPriority = (List<Map<String, Object>>) bugGraph.get("#result-set-13");
		// Iterator<String> projectSet=BugCountPerPriority.iterator();
		String priortyCritical = "[";
		String priortyBlocker = "[";
		String priortyCriticalBusiness = "[";
		String priortyMinor = "[";
		String priortyTrivial = "[";
		String priortyDiscoverer = "[";
		String priortyMedium = "[";
		String priortyMajor = "[";
		Hashtable<String, String> bugPriorityByBlocker = new Hashtable<String, String>();
		Hashtable<String, String> bugPriorityByCritical = new Hashtable<String, String>();
		Hashtable<String, String> bugPriorityByCriticalBusiness = new Hashtable<String, String>();
		Hashtable<String, String> bugPriorityByMinor = new Hashtable<String, String>();
		Hashtable<String, String> bugPriorityByTrival = new Hashtable<String, String>();
		Hashtable<String, String> bugPriorityByDiscoverer = new Hashtable<String, String>();
		Hashtable<String, String> bugPriorityByMedium = new Hashtable<String, String>();
		Hashtable<String, String> bugPriorityByMajor = new Hashtable<String, String>();

		int index = 0;
		Set<String> topThreeReleaseHashSet = new LinkedHashSet<String>();
		for (int iCount = 0; iCount < BugCountPerPriority.size(); iCount++) {

			for (int p = 0; p < CurrentRelease.size(); p++) {
				if (Integer.parseInt(CurrentRelease.get(p).get("R_vid").toString()) == Integer
						.parseInt(BugCountPerPriority.get(iCount).get("issue_tracker_release_id").toString())) {

					if (BugCountPerPriority.get(iCount).get("bug_priority").toString().equals("5")) {
						bugPriorityByBlocker.put(CurrentRelease.get(p).get("R_vid").toString(),
								BugCountPerPriority.get(iCount).get("bugPCount").toString());
					}

					if (BugCountPerPriority.get(iCount).get("bug_priority").toString().equalsIgnoreCase("6")) {

						bugPriorityByCritical.put(CurrentRelease.get(p).get("R_vid").toString(),
								BugCountPerPriority.get(iCount).get("bugPCount").toString());

					}

					if (BugCountPerPriority.get(iCount).get("bug_priority").toString().equalsIgnoreCase("7")) {

						bugPriorityByCriticalBusiness.put(CurrentRelease.get(p).get("R_vid").toString(),
								BugCountPerPriority.get(iCount).get("bugPCount").toString());
					}

					if (BugCountPerPriority.get(iCount).get("bug_priority").toString().equalsIgnoreCase("8")) {

						bugPriorityByMinor.put(CurrentRelease.get(p).get("R_vid").toString(),
								BugCountPerPriority.get(iCount).get("bugPCount").toString());

					}

					if (BugCountPerPriority.get(iCount).get("bug_priority").toString().equalsIgnoreCase("9")) {

						bugPriorityByTrival.put(CurrentRelease.get(p).get("R_vid").toString(),
								BugCountPerPriority.get(iCount).get("bugPCount").toString());

					}

					if (BugCountPerPriority.get(iCount).get("bug_priority").toString().equalsIgnoreCase("10")) {
						bugPriorityByDiscoverer.put(CurrentRelease.get(p).get("R_vid").toString(),
								BugCountPerPriority.get(iCount).get("bugPCount").toString());

					}

					if (BugCountPerPriority.get(iCount).get("bug_priority").toString().equalsIgnoreCase("11")) {

						bugPriorityByMedium.put(CurrentRelease.get(p).get("R_vid").toString(),
								BugCountPerPriority.get(iCount).get("bugPCount").toString());

					}

					if (BugCountPerPriority.get(iCount).get("bug_priority").toString().equalsIgnoreCase("12")) {
						bugPriorityByMajor.put(CurrentRelease.get(p).get("R_vid").toString(),
								BugCountPerPriority.get(iCount).get("bugPCount").toString());
						index = p;
					}
				}
			}

		}

		JSONObject releaseWiseJson = new JSONObject();
		JSONArray releaseWiseJsonArray = new JSONArray();
		JSONObject releaseWiseDetailsJson = new JSONObject();

		String[] ChartColor = { "#1EC0FB", "#2ECC71", "#F1948A", "#85C1E9", "#C39BD3", "#AA85E9", "#2EC7CC", "#9C2ECC",
				"#BBFF33", "#CC492E", "#EA185E", "#827017", "#F76E85", "#FFDD33", "#FF7733" };
		String[] priority_name = { "Blocker", "Critical", "Critical - pending with business", "Minor", "Trivial",
				"Discoverer", "Medium", "Major" };

		for (int jCount = 0; jCount < CurrentRelease.size(); jCount++) {
			topThreeReleaseHashSet.add("\"" + CurrentRelease.get(jCount).get("R_Name").toString() + "\"");
			if (bugPriorityByBlocker.size() > 0) {
				if (bugPriorityByBlocker.containsKey(CurrentRelease.get(jCount).get("R_vid")))
					priortyBlocker += bugPriorityByBlocker.get(CurrentRelease.get(jCount).get("R_vid")) + ",";
				else
					priortyBlocker += "0,";
			}

			if (bugPriorityByCritical.size() > 0) {

				if (bugPriorityByCritical.containsKey(CurrentRelease.get(jCount).get("R_vid")))
					priortyCritical += bugPriorityByCritical.get(CurrentRelease.get(jCount).get("R_vid")) + ",";
				else
					priortyCritical += "0,";
			}
			if (bugPriorityByCriticalBusiness.size() > 0) {

				if (bugPriorityByCriticalBusiness.containsKey(CurrentRelease.get(jCount).get("R_vid")))
					priortyCriticalBusiness += bugPriorityByCriticalBusiness
							.get(CurrentRelease.get(jCount).get("R_vid")) + ",";
				else
					priortyCriticalBusiness += "0,";
			}
			if (bugPriorityByMinor.size() > 0) {

				if (bugPriorityByMinor.containsKey(CurrentRelease.get(jCount).get("R_vid")))
					priortyMinor += bugPriorityByMinor.get(CurrentRelease.get(jCount).get("R_vid")) + ",";
				else
					priortyMinor += "0,";
			}
			if (bugPriorityByDiscoverer.size() > 0) {

				if (bugPriorityByDiscoverer.containsKey(CurrentRelease.get(jCount).get("R_vid")))
					priortyDiscoverer += bugPriorityByDiscoverer.get(CurrentRelease.get(jCount).get("R_vid")) + ",";
				else
					priortyDiscoverer += "0,";
			}
			if (bugPriorityByMajor.size() > 0) {

				if (bugPriorityByMajor.containsKey(CurrentRelease.get(jCount).get("R_vid")))
					priortyMajor += bugPriorityByMajor.get(CurrentRelease.get(jCount).get("R_vid")) + ",";
				else
					priortyMajor += "0,";
			}
			if (bugPriorityByMedium.size() > 0) {

				if (bugPriorityByMedium.containsKey(CurrentRelease.get(jCount).get("R_vid")))
					priortyMedium += bugPriorityByMedium.get(CurrentRelease.get(jCount).get("R_vid")) + ",";
				else
					priortyMedium += "0,";
			}
			if (bugPriorityByTrival.size() > 0) {

				if (bugPriorityByTrival.containsKey(CurrentRelease.get(jCount).get("R_vid")))
					priortyTrivial += bugPriorityByTrival.get(CurrentRelease.get(jCount).get("R_vid")) + ",";
				else
					priortyTrivial += "0,";
			}
		}

		if (priortyCritical.lastIndexOf(",") > 0) {
			priortyCritical = priortyCritical.substring(0, priortyCritical.lastIndexOf(","));
			priortyCritical += "]";
			releaseWiseDetailsJson.put("label", priority_name[1]);
			releaseWiseDetailsJson.put("backgroundColor", ChartColor[1]);
			releaseWiseDetailsJson.put("data", priortyCritical);
			releaseWiseJsonArray.put(releaseWiseDetailsJson);
		}

		if (priortyBlocker.lastIndexOf(",") > 0) {
			priortyBlocker = priortyBlocker.substring(0, priortyBlocker.lastIndexOf(","));
			priortyBlocker += "]";
			releaseWiseDetailsJson.put("label", priority_name[0]);
			releaseWiseDetailsJson.put("backgroundColor", ChartColor[0]);
			releaseWiseDetailsJson.put("data", priortyBlocker);
			releaseWiseJsonArray.put(releaseWiseDetailsJson);
		}

		if (priortyCriticalBusiness.lastIndexOf(",") > 0) {
			priortyCriticalBusiness = priortyCriticalBusiness.substring(0, priortyCriticalBusiness.lastIndexOf(","));
			priortyCriticalBusiness += "]";
			releaseWiseDetailsJson.put("label", priority_name[2]);
			releaseWiseDetailsJson.put("backgroundColor", ChartColor[2]);
			releaseWiseDetailsJson.put("data", priortyCriticalBusiness);
			releaseWiseJsonArray.put(releaseWiseDetailsJson);
		}
		if (priortyMinor.lastIndexOf(",") > 0) {
			priortyMinor = priortyMinor.substring(0, priortyMinor.lastIndexOf(","));
			priortyMinor += "]";
			releaseWiseDetailsJson.put("label", priority_name[3]);
			releaseWiseDetailsJson.put("backgroundColor", ChartColor[3]);
			releaseWiseDetailsJson.put("data", priortyMinor);
			releaseWiseJsonArray.put(releaseWiseDetailsJson);
		}
		if (priortyTrivial.lastIndexOf(",") > 0) {
			priortyTrivial = priortyTrivial.substring(0, priortyTrivial.lastIndexOf(","));
			priortyTrivial += "]";
			releaseWiseDetailsJson.put("label", priority_name[4]);
			releaseWiseDetailsJson.put("backgroundColor", ChartColor[4]);
			releaseWiseDetailsJson.put("data", priortyTrivial);
			releaseWiseJsonArray.put(releaseWiseDetailsJson);
		}
		if (priortyDiscoverer.lastIndexOf(",") > 0) {
			priortyDiscoverer = priortyDiscoverer.substring(0, priortyDiscoverer.lastIndexOf(","));
			priortyDiscoverer += "]";
			releaseWiseDetailsJson.put("label", priority_name[5]);
			releaseWiseDetailsJson.put("backgroundColor", ChartColor[5]);
			releaseWiseDetailsJson.put("data", priortyDiscoverer);
			releaseWiseJsonArray.put(releaseWiseDetailsJson);
		}
		if (priortyMedium.lastIndexOf(",") > 0) {
			priortyMedium = priortyMedium.substring(0, priortyMedium.lastIndexOf(","));
			priortyMedium += "]";
			releaseWiseDetailsJson.put("label", priority_name[6]);
			releaseWiseDetailsJson.put("backgroundColor", ChartColor[6]);
			releaseWiseDetailsJson.put("data", priortyMedium);
			releaseWiseJsonArray.put(releaseWiseDetailsJson);
		}
		if (priortyMajor.lastIndexOf(",") > 0) {
			priortyMajor = priortyMajor.substring(0, priortyMajor.lastIndexOf(","));
			priortyMajor += "]";
			releaseWiseDetailsJson.put("label", priority_name[7]);
			releaseWiseDetailsJson.put("backgroundColor", ChartColor[7]);
			releaseWiseDetailsJson.put("data", priortyMajor);
			releaseWiseJsonArray.put(releaseWiseDetailsJson);
		}

		// releaseWiseJsonArray.put(releaseWiseDetailsJson);

		releaseWiseJson.put("labels", topThreeReleaseHashSet);
		releaseWiseJson.put("datasets", releaseWiseJsonArray);
		String releaseWiseJsonString = releaseWiseJson.toJSONString().replaceAll("\"(\\w+)\":", "$1:")
				.replace("\"[", "[").replace("]\"", "]");

		String userColors[] = { colorProperties.getProperty("btDashboard.user0"),
				colorProperties.getProperty("btDashboard.user1"), colorProperties.getProperty("btDashboard.user2"),
				colorProperties.getProperty("btDashboard.user3"), colorProperties.getProperty("btDashboard.user4"),
				colorProperties.getProperty("btDashboard.user5"), colorProperties.getProperty("btDashboard.user6"),
				colorProperties.getProperty("btDashboard.user7"), colorProperties.getProperty("btDashboard.user8"),
				colorProperties.getProperty("btDashboard.user9") };
		for (int i = 0; i < userList.size(); i++) {
			userList.get(i).put("color", userColors[i % 10]);
			userList.get(i).put("fullName",
					userList.get(i).get("first_name").toString() + " " + userList.get(i).get("last_name").toString());
		}
		try {
			modelData.put("userWiseBugs",userWiseBugs);
			modelData.put("userList", utilsService.convertListOfMapToJson(userList));
			modelData.put("bugStatusList", utilsService.convertListOfMapToJson(bugStatusList));
			modelData.put("CatwisebugCount", CatwisebugCount);
			modelData.put("JirareleaseBug", JirareleaseBug);
			modelData.put("BugList", BugList);
			modelData.put("BugCountPerPriority", BugCountPerPriority);
			modelData.put("ReleaseIdWithPriority", ReleaseIdWithPriority);
			modelData.put("CurrentRelease", CurrentRelease);
			modelData.put("ReleaseWiseJson", releaseWiseJsonString);

		} catch (Exception e1) {
			e1.printStackTrace();
		}
		List<Map<String, Object>> getBugGraphData = (List<Map<String, Object>>) bugGraph.get("#result-set-5");
		List<Map<String, Object>> dashboardData = (List<Map<String, Object>>) bugGraph.get("#result-set-6");
		List<Map<String, Object>> userAssignedModuleList = (List<Map<String, Object>>) bugGraph.get("#result-set-4");

		// System.out.println("dashboard data "+getBugGraphData);

		int[] moduleArray = new int[userAssignedModuleList.size()];
		for (int count = 0; count < userAssignedModuleList.size(); count++)
			moduleArray[count] = Integer.parseInt(userAssignedModuleList.get(count).get("module_id").toString());
		int priorityCount = 0, statusNewCount = 0, statusClosedCount = 0, assignedToMeCount = 0, assignStatus = 0,
				newBugStatus = 0, statusOpenCount = 0, statusInprogressCount = 0, statusResolvedCount = 0,
				statusReopenedCount = 0;
		for (int i = 0; i < dashboardData.size(); i++) {
			int assignedStatus = Integer.parseInt(dashboardData.get(i).get("user_count").toString());
			int project = Integer.parseInt(dashboardData.get(i).get("project").toString());
			int status = Integer.parseInt(dashboardData.get(i).get("bug_status").toString());
			int priority = Integer.parseInt(dashboardData.get(i).get("bug_priority").toString());
			int assignTo = Integer.parseInt(dashboardData.get(i).get("assignee").toString());
			int bugAssignStatus = Integer.parseInt(dashboardData.get(i).get("assign_status").toString());
			int groupAssignStatus = Integer.parseInt(dashboardData.get(i).get("group_assign").toString());

			if (assignedStatus > 0 && projectId == project) {

				if (status == 1) {
					newBugStatus++;
				}
				if (status == 7) {
					statusOpenCount++;
				}
				if (status == 8) {
					statusReopenedCount++;
				}
				if (status == 4) {
					statusResolvedCount++;
				}
				if (status == 3) {
					statusInprogressCount++;
				}

				if (status == 2) {
					assignStatus++;
				}

				if (status == 5) {
					statusClosedCount++;
				} else {
					if (priority == 5) {
						priorityCount++;
					}
					statusNewCount++;
					OpenBugList.add(dashboardData.get(i));
				}

				if (assignTo == userId && !(status == 5 || status == 6) && (bugAssignStatus == 1)
						&& (groupAssignStatus == 1)) {
					assignedToMeCount++;
				}
			}
			if (priority == 1 && status != 5) {
				if (ArrayUtils.contains(moduleArray, Integer.parseInt(dashboardData.get(i).get("module").toString())))
					getBugtable.add(dashboardData.get(i));
			}
		}
		bugStat.put("status_closed", statusClosedCount);
		bugStat.put("status_new", statusNewCount);
		bugStat.put("priority", priorityCount);
		bugStat.put("assigned_to_me", assignedToMeCount);
		modelData.put("bugsWithPriority", bugsWithPriority);
		modelData.put("status_closed", statusClosedCount);
		modelData.put("statusCount_new", newBugStatus);
		modelData.put("statusCount_assign", assignStatus);
		modelData.put("statusCount_open", statusOpenCount);
		modelData.put("statusCount_inProgress", statusInprogressCount);
		modelData.put("statusCount_Resolved", statusResolvedCount);
		modelData.put("statusCount_Reopened", statusReopenedCount);

		Set<String> graphWeeks = new TreeSet<String>();
		for (int i = 0; i < getBugGraphData.size(); i++) {
			if (graphWeeks.size() == 6) {
				break;
			}
			graphWeeks.add(getBugGraphData.get(i).get("weekday").toString());
		}

		int newGraphBug = 0, readyGraphBug = 0, closedGraphBug = 0, devGraphBug = 0, assigned = 0, x = 0,
				reopenendGraphBug = 0;
		List<Map<String, Object>> getGraphBugData = new ArrayList<Map<String, Object>>();
		Iterator<String> weekdaySet = graphWeeks.iterator();
		//System.out.println("Weekday " + weekdaySet);
		while (weekdaySet.hasNext()) {
			String day = weekdaySet.next();

			// String actualDate = "2016-03-20";
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
			DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("MMM yyyy", Locale.ENGLISH);
			LocalDate ld = LocalDate.parse(day, dtf);
			String month_name = dtf2.format(ld);

			for (int i = 0; i < getBugGraphData.size(); i++) {

				if (day.equals(getBugGraphData.get(i).get("weekday").toString())) {
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Open")) {
						newGraphBug++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Resolved")) {
						readyGraphBug++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Closed")) {
						closedGraphBug++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Assigned")) {
						assigned++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("In Progress")) {
						devGraphBug++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Reopened")) {
						reopenendGraphBug++;
					}
				}
			}
			

			Map<String, Object> internalMap = new HashMap<String, Object>();
			internalMap.put("New", newGraphBug);
			internalMap.put("Assign", assigned);
			internalMap.put("ReadyforQA", readyGraphBug);
			internalMap.put("Closed", closedGraphBug);
			internalMap.put("DevinProgress", devGraphBug);
			internalMap.put("Reopened", reopenendGraphBug);
			internalMap.put("weekday", day);
			// internalMap.put("month",month_name);
			getGraphBugData.add(x++, internalMap);
			newGraphBug = 0;
			readyGraphBug = 0;
			closedGraphBug = 0;
			devGraphBug = 0;
			reopenendGraphBug = 0;
			assigned = 0;
		}

		JSONArray jArray = new JSONArray(getGraphBugData);
		modelData.put("graphData", jArray);
		/*
		 * System.out.println("jArray"); System.out.println(jArray);
		 */

		for (int j = 0; j < OpenBugList.size(); j++) {
			OpenBugList.get(j).put("create_date", utilsService.getDateTimeOfTimezone(userTimeZone,
					utilsService.getDate(OpenBugList.get(j).get("create_date").toString())));

			String logo = loginService.getImage((byte[]) OpenBugList.get(j).get("user_photo"));
			OpenBugList.get(j).put("user_photo", logo);

			if (OpenBugList.get(j).get("assign_status").toString().equals("2")) {
				OpenBugList.get(j).put("assigneeFN", "None");
				OpenBugList.get(j).put("assigneeLN", "");
			} else if (OpenBugList.get(j).get("group_status").toString().equals("1")) {
				int bugGroupId = Integer.parseInt(OpenBugList.get(j).get("group_id").toString());
				List<Map<String, Object>> bugGroup = bugGroupDAO.getGroupbyId(bugGroupId, schema);

				OpenBugList.get(j).put("assigneeFN", bugGroup.get(0).get("bug_group_name"));
				OpenBugList.get(j).put("assigneeLN", "");
			}
		}

		modelData.put("allBugDetails", OpenBugList);
		modelData.put("bugCount", bugStat);
		modelData.put("bugTable", getBugtable);
		modelData.put("UserProfilePhoto", userprofilePhoto);
		modelData.put("userName", fullName);
		modelData.put("role", role);
		List<Map<String, Object>> topActivities = null;
		try {
			topActivities = activitiesDAO.getTopActivities(schema);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < topActivities.size(); i++) {
			topActivities.get(i).put("activity_date", utilsService.getDateTimeOfTimezone(userTimeZone,
					topActivities.get(i).get("activity_date").toString()));
		}

		modelData.put("nowDate", utilsService.getNowDateOfTimezone(userTimeZone));
		modelData.put("prevDate", utilsService.getPrevDateOfTimezone(userTimeZone));
		modelData.put("topActivities", topActivities);
		modelData.put("New", colorProperties.getProperty("bug.New"));
		modelData.put("Fixed", colorProperties.getProperty("bug.Fixed"));
		modelData.put("Closed", colorProperties.getProperty("bug.Closed"));
		modelData.put("Assigned", colorProperties.getProperty("bug.Assigned"));
		modelData.put("Verified", colorProperties.getProperty("bug.Verified"));
		modelData.put("Reject", colorProperties.getProperty("bug.Reject"));
		modelData.put("Reopened", colorProperties.getProperty("bug.Reopened"));

		return modelData;
	}

	@Override
	public Map<String, Object> viewAllBTDAshboard(int userId, String schema, String userTimeZone,
			Object userProfilePhoto, String userFullName, String role) {
		List<Map<String, Object>> allOpenBugList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> graphData = new ArrayList<Map<String, Object>>();
		HashMap<String, Integer> bugStat = new HashMap<String, Integer>();
		Map<String, Object> modelData = new HashMap<String, Object>();
		int compareProject = 0;
		List<Map<String, Object>> dashboardData = bugDAO.getDashboardData(userId, schema);
		int priorityCount = 0, statusNewCount = 0, statusClosedCount = 0, assignedToMeCount = 0;
		int graphNew = 0, graphClosed = 0, graphDevInProgress = 0, newBugStatus = 0, statusOpenCount = 0,
				statusReopenedCount = 0, statusResolvedCount = 0, statusInprogressCount = 0, assignStatus = 0;

		for (int i = 0; i < dashboardData.size(); i++) {
			dashboardData.get(i).put("create_date", utilsService.getDateTimeOfTimezone(userTimeZone,
					utilsService.getDate(dashboardData.get(i).get("create_date").toString())));
			dashboardData.get(i).put("update_date", utilsService.getDateTimeOfTimezone(userTimeZone,
					utilsService.getDate(dashboardData.get(i).get("update_date").toString())));
			int assignedStatus = Integer.parseInt(dashboardData.get(i).get("user_count").toString());
			int status = Integer.parseInt(dashboardData.get(i).get("bug_status").toString());
			int priority = Integer.parseInt(dashboardData.get(i).get("bug_priority").toString());
			int assignTo = Integer.parseInt(dashboardData.get(i).get("assignee").toString());
			int project = Integer.parseInt(dashboardData.get(i).get("project").toString());
			int bugAssignStatus = Integer.parseInt(dashboardData.get(i).get("assign_status").toString());
			int groupAssignStatus = Integer.parseInt(dashboardData.get(i).get("group_assign").toString());
			if (compareProject != project) {

				graphClosed = 0;
				graphDevInProgress = 0;
				graphNew = 0;
				if (status == 1) {
					newBugStatus++;
				}
				if (status == 7) {
					statusOpenCount++;
				}
				if (status == 8) {
					statusReopenedCount++;
				}
				if (status == 4) {
					statusResolvedCount++;
				}
				if (status == 3) {
					statusInprogressCount++;
				}

				if (status == 2) {
					assignStatus++;
				}

				if (status == 5) {
					statusClosedCount++;
				}
				compareProject = project;
			} else {

				if (i == dashboardData.size() - 1) {
					String projectName = dashboardData.get(i).get("project_name").toString();
					Map<String, Object> data = new HashMap<String, Object>();
					// data.put("new_bug", graphNew);
					// data.put("closed_bug", graphClosed);
					data.put("statusCount_inProgress", statusInprogressCount);
					data.put("project_name", projectName);
					// data.put("project_id", project);

					data.put("status_closed", statusClosedCount);
					data.put("status_new", statusNewCount);
					data.put("priority", priorityCount);
					data.put("assigned_to_me", assignedToMeCount);

					modelData.put("status_closed", statusClosedCount);
					modelData.put("statusCount_new", newBugStatus);
					modelData.put("statusCount_assign", assignStatus);

					modelData.put("statusCount_open", statusOpenCount);
					modelData.put("statusCount_inProgress", statusInprogressCount);
					modelData.put("statusCount_Resolved", statusResolvedCount);
					modelData.put("statusCount_Reopened", statusReopenedCount);

					graphData.add(data);

				}
			}
			if (assignedStatus > 0) {
				if (status == 5) {
					statusClosedCount++;
				} else {
					if (priority == 1) {
						priorityCount++;
					}
					statusNewCount++;
					allOpenBugList.add(dashboardData.get(i));
				}

				if (assignTo == userId && !(status == 5 || status == 6) && (bugAssignStatus == 1)
						&& (groupAssignStatus == 1)) {
					assignedToMeCount++;
				}
			}

		}

		List<Map<String, Object>> getBugGraphData = bugDAO.getAllBugGraphDashboardData(userId, schema);
		Set<String> graphWeeks = new TreeSet<String>();
		for (int i = 0; i < getBugGraphData.size(); i++) {
			if (graphWeeks.size() == 6) {
				break;
			}
			graphWeeks.add(getBugGraphData.get(i).get("weekday").toString());
		}
		//System.out.println("**");
		//System.out.println(graphWeeks);
		//System.out.println("**");

		int newGraphBug = 0, readyGraphBug = 0, closedGraphBug = 0, devGraphBug = 0, assigned = 0,
				reopenendGraphBug = 0, x = 0;
		List<Map<String, Object>> getGraphBugData = new ArrayList<Map<String, Object>>();
		Iterator<String> weekdaySet = graphWeeks.iterator();
		while (weekdaySet.hasNext()) {
			String day = weekdaySet.next();
			//System.out.println("day " + day);
			for (int i = 0; i < getBugGraphData.size(); i++) {
				if (day.equals(getBugGraphData.get(i).get("weekday").toString())) {
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Open")) {
						newGraphBug++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Resolved")) {
						readyGraphBug++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Closed")) {
						closedGraphBug++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("Reopened")) {
						reopenendGraphBug++;
					}
					if (getBugGraphData.get(i).get("bugStatus").toString().equals("In Progress")) {
						devGraphBug++;
					}
				}
			}

			Map<String, Object> internalMap = new HashMap<String, Object>();
			internalMap.put("New", newGraphBug);
			internalMap.put("Assign", assigned);
			internalMap.put("ReadyforQA", readyGraphBug);
			internalMap.put("Closed", closedGraphBug);
			internalMap.put("DevinProgress", devGraphBug);
			internalMap.put("Reopened", reopenendGraphBug);
			internalMap.put("weekday", day);
			getGraphBugData.add(x++, internalMap);
			newGraphBug = 0;
			readyGraphBug = 0;
			closedGraphBug = 0;
			reopenendGraphBug = 0;
			devGraphBug = 0;
			assigned = 0;
		}

		JSONArray jArray = new JSONArray(getGraphBugData);
		modelData.put("graphDataProject", jArray);

		List<Map<String, Object>> topActivities = null;
		try {
			topActivities = activitiesDAO.getTopActivities(schema);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		for (int i = 0; i < topActivities.size(); i++) {
			String dateInString = topActivities.get(i).get("activity_date").toString();
			topActivities.get(i).put("activity_date", utilsService.getDateTimeOfTimezone(userTimeZone, dateInString));
		}

		for (int j = 0; j < allOpenBugList.size(); j++) {
			if (allOpenBugList.get(j).get("assign_status").toString().equals("2")) {
				allOpenBugList.get(j).put("assigneeFN", "None");
				allOpenBugList.get(j).put("assigneeLN", "");
			} else if (allOpenBugList.get(j).get("group_status").toString().equals("1")) {
				int bugGroupId = Integer.parseInt(allOpenBugList.get(j).get("group_id").toString());
				List<Map<String, Object>> bugGroup = bugGroupDAO.getGroupbyId(bugGroupId, schema);

				allOpenBugList.get(j).put("assigneeFN", bugGroup.get(0).get("bug_group_name"));
				allOpenBugList.get(j).put("assigneeLN", "");
			}
		}

		bugStat.put("status_closed", statusClosedCount);
		bugStat.put("status_new", statusNewCount);
		bugStat.put("priority", priorityCount);
		bugStat.put("assigned_to_me", assignedToMeCount);

		try {
			modelData.put("graphData", utilsService.convertListOfMapToJson(graphData));
		} catch (Exception e) {
			e.printStackTrace();
		}
		modelData.put("allBugDetails", allOpenBugList);
		modelData.put("bugCount", bugStat);
		modelData.put("UserProfilePhoto", loginService.getImage((byte[]) userProfilePhoto));
		modelData.put("userName", userFullName);
		modelData.put("role", role);
		modelData.put("nowDate", utilsService.getNowDateOfTimezone(userTimeZone));
		modelData.put("prevDate", utilsService.getPrevDateOfTimezone(userTimeZone));
		modelData.put("topActivities", topActivities);
		modelData.put("New", colorProperties.getProperty("bug.New"));
		modelData.put("Fixed", colorProperties.getProperty("bug.Fixed"));
		modelData.put("Closed", colorProperties.getProperty("bug.Closed"));
		modelData.put("Assigned", colorProperties.getProperty("bug.Assigned"));
		modelData.put("Verified", colorProperties.getProperty("bug.Verified"));
		modelData.put("Reject", colorProperties.getProperty("bug.Reject"));
		modelData.put("Reopened", colorProperties.getProperty("bug.Reopened"));
		return modelData;
	}

}

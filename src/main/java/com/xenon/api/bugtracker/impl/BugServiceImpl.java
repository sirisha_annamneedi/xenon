package com.xenon.api.bugtracker.impl;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import com.xenon.administration.dao.JiraInstanceReleaseDao;
import com.xenon.administration.domain.JiraInstanceReleaseDetails;
import com.xenon.api.bugtracker.BugService;
import com.xenon.bugtracker.dao.BugDAO;
import com.xenon.bugtracker.dao.BugGroupDAO;
import com.xenon.bugtracker.dao.BugSummaryDAO;
import com.xenon.bugtracker.domain.BugDetails;
import com.xenon.bugtracker.domain.BugSummary;
import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.util.service.FileUploadForm;

public class BugServiceImpl implements BugService {

	private static final Logger logger = LoggerFactory.getLogger(BugServiceImpl.class);

	@Autowired
	BugDAO bugDao;

	@Autowired
	BugGroupDAO bugGroupDao;

	@Autowired
	ModuleDAO modeuleDao;

	@Autowired
	BugSummaryDAO bugSummaryDao;
	

	@Autowired
	JiraInstanceReleaseDao jiraInstanceReleaseDao;

	@SuppressWarnings("unchecked")
	@Override
	public int insertBug(String projectId, String moduleId, String summary, String statusId, String priorityId,
			String severityId, String categoryId, String buildTypeId, String selectedBuildTypeId, String userId,
			String bugDescription, String schema, String assignedUserID, FileUploadForm uploadForm, String repoPath) {
		logger.info("Bug API to insert bug details and summary details");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		BugDetails bug = new BugDetails();
		bug.setProjectId(Integer.parseInt(projectId));
		bug.setModuleId(Integer.parseInt(moduleId));
		bug.setBugTitle(summary);
		bug.setAssignStatus(2);
		bug.setGroupStatus(2);
		bug.setGroupId(0);
		// String text = bugDescription.replaceAll("\\<.*?\\>", "");
		// bug.setDescription(text);
		bug.setDescription(bugDescription);
		bug.setCreatedBy(Integer.parseInt(userId));
		logger.info("Set bug details to object of class BugDetails");
		bug.setCreatedDate(dateFormat.format(date));
		int status = 0;
		String assignGroupID = null;
		boolean bugGroupFLAG = false;
		boolean bugNoneFLAG = false;

		if (assignedUserID.equals("-1")) {
			assignedUserID = userId;
			bugNoneFLAG = true;
		} else {
			if (assignedUserID.contains("USR")) {
				assignedUserID = assignedUserID.substring(3);

			} else if (assignedUserID.contains("GRP")) {
				assignGroupID = assignedUserID.substring(3);
				bug.setGroupId(Integer.parseInt(assignGroupID));
				bug.setGroupStatus(1);
				bugGroupFLAG = true;
			} else {
				assignedUserID = userId;
			}
			bug.setAssignStatus(1);
		}

		status = bugDao.insertBug(bug, schema);

		if (status == 1) {
			logger.info("A row is inserted into xebt_bug table with bug deatils");
			int bugId = bugDao.getBugId(bug, schema);

			logger.info("Get bug id of inserted row");
			BugSummary bugSummary = new BugSummary();
			bugSummary.setBugId(bugId);
			bugSummary.setStatus(Integer.parseInt(statusId));
			bugSummary.setPriority(Integer.parseInt(priorityId));
			bugSummary.setSeverity(Integer.parseInt(severityId));
			bugSummary.setCategory(Integer.parseInt(categoryId));
			bugSummary.setPrevStatus(Integer.parseInt(statusId));
			bugSummary.setPrevPriority(Integer.parseInt(priorityId));
			bugSummary.setPrevSeverity(Integer.parseInt(severityId));
			bugSummary.setUpdatedDate(dateFormat.format(date));
			bugSummary.setUpdatedBy(Integer.parseInt(userId));
			bugSummary.setPrevCategory(Integer.parseInt(categoryId));
			bugSummary.setIsBuildAssigned(Integer.parseInt(buildTypeId));
			if (Integer.parseInt(buildTypeId) == 1) {
				bugSummary.setManualBuild(Integer.parseInt(selectedBuildTypeId));
				bugSummary.setPrevManualBuild(Integer.parseInt(selectedBuildTypeId));
				bugSummary.setPrevAutomationBuild(0);
				bugSummary.setAutomationBuild(0);
			} else if (Integer.parseInt(buildTypeId) == 2) {
				bugSummary.setPrevAutomationBuild(Integer.parseInt(selectedBuildTypeId));
				bugSummary.setAutomationBuild(Integer.parseInt(selectedBuildTypeId));
				bugSummary.setManualBuild(0);
				bugSummary.setPrevManualBuild(0);
			} else {
				bugSummary.setManualBuild(0);
				bugSummary.setPrevManualBuild(0);
				bugSummary.setPrevAutomationBuild(0);
				bugSummary.setAutomationBuild(0);
			}
			bugSummary.setGroup_assign(1);
			int flag = 0;
			logger.info("Set bug summary details to object of class BugSummary");
			if (bugGroupFLAG) {
				List<Map<String, Object>> userList = modeuleDao.moduleUserDetails(Integer.parseInt(moduleId), schema);
				List<Map<String, Object>> groupUser = bugGroupDao.getUserbyGroupId(Integer.parseInt(assignGroupID),
						schema);

				userList.retainAll(groupUser);
				if (userList.size() > 0) {
					for (int i = 0; i < userList.size(); i++) {
						bugSummary.setAssignee(Integer.parseInt(userList.get(i).get("user_id").toString()));
						bugSummary.setPrevAssignee(Integer.parseInt(userList.get(i).get("user_id").toString()));
						flag = bugSummaryDao.insertBugSummary(bugSummary, schema);
					}

					if (flag == 1) {
						try {
							bugDao.sendMailOnCreateBug(bugId, bug, schema, bugSummary, bugGroupFLAG);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

				}
				bugSummaryDao.insertBugUserNotification(bug.getAssignStatus(), bug.getGroupStatus(), bug.getGroupId(),
						bugId, Integer.parseInt(userId), schema);
			} else {
				bugSummary.setAssignee(Integer.parseInt(assignedUserID));
				bugSummary.setPrevAssignee(Integer.parseInt(assignedUserID));
				flag = bugSummaryDao.insertBugSummary(bugSummary, schema);
				if (flag == 1 && (bugNoneFLAG != true)) {
					try {
						bugDao.sendMailOnCreateBug(bugId, bug, schema, bugSummary, bugGroupFLAG);
					} catch (Exception e) {
						e.printStackTrace();
					}
					logger.info("Mail has been sent to author and assignee");
				}
				bugSummaryDao.insertBugUserNotification(bug.getAssignStatus(), bug.getGroupStatus(), bug.getGroupId(),
						bugId, Integer.parseInt(userId), schema);
			}

			if (bugId != 0) {
				if (uploadForm != null) {
					List<MultipartFile> files = uploadForm.getFiles();
					logger.info("Get uploaded files using multipart ");
					List<String> fileNames = new ArrayList<String>();
					String uploadDirectoryPath = repoPath + "repository" + File.separator + "upload";
					logger.info("Create Folder ");
					if (null != files && files.size() > 0) {
						for (MultipartFile multipartFile : files) {
							String fileName = multipartFile.getOriginalFilename();
							if (!fileName.isEmpty()) {
								fileNames.add(fileName);
								byte[] bytes = null;
								try {
									bytes = multipartFile.getBytes();
								} catch (IOException e) {
									e.printStackTrace();
								}
								String title = multipartFile.getOriginalFilename();
								try {
									Resource resource = new ClassPathResource(uploadDirectoryPath);
									String filePath = resource.getFile().getAbsolutePath() + File.separator
											+ "bugtracker" + File.separator + fileName;
									File fileToWriteTo = null;
									fileToWriteTo = new File(filePath);
									FileUtils.writeByteArrayToFile(fileToWriteTo, bytes);
									//bugDao.insertAttachment(bugId, filePath, title, schema);
									bugDao.insertAttachment(bugId, bytes, title, schema);
									fileToWriteTo.delete();
								} catch (IOException e1) {
									e1.printStackTrace();
								}
							}
						}
					}
				}
				Map<String, Object> data = bugDao.getDataForInsertBug(bugId, schema);
				List<Map<String, Object>> bugDetails = (List<Map<String, Object>>) data.get("#result-set-2");

				if (assignedUserID.equals("-1")) {
					bugDetails.get(0).put("bug_assignee_name", "None");
				} else if (assignedUserID.contains("GRP")) {
					String groupId = assignedUserID.substring(3);
					int bugGroupId = Integer.parseInt(groupId);
					List<Map<String, Object>> bugGroup = bugGroupDao.getGroupbyId(bugGroupId, schema);
					bugDetails.get(0).put("bug_assignee_name", bugGroup.get(0).get("bug_group_name"));
				}

				try {
					bugDao.addCreateBugActivity(bugId, Integer.parseInt(userId), summary, schema);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}

				logger.info("User is redirected to bug summary page ");
			}
			if (flag == 1) {
				logger.info("A row is inserted into xebt_bugsummary table with bug summary details");

				return bugId;
			} else {
				return flag;
			}
		} else {
			return status;
		}
	}

	@Override
	public int insertUpdateJira(String trackerType,String issue_tracker_project_key, String issue_tracker_current_release_id,String schema){
		logger.info("Update Synch details in xead_jira_details_update");		
		List<Map<String, Object>> getJiraUpdate = bugDao.getJiraLastUpdate(issue_tracker_project_key,schema);
		int getUpdated;
		if(getJiraUpdate.size()>0){
			getUpdated = bugDao.updateLastSynchWithJira("bugTracker", (getJiraUpdate.get(0).get("jira_update_id").toString()),issue_tracker_current_release_id, schema);
			
		}else
		{
			getUpdated=bugDao.insertJiraUpdatedDateWithJira(issue_tracker_project_key,issue_tracker_current_release_id,schema);
			
		}
		return getUpdated;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> insertBug(String projectId, String moduleId, String summary, String statusId, String priorityId,
			String severityId, String categoryId, String buildTypeId, String selectedBuildTypeId, String userId,
			String bugDescription, String schema, String assignedUserID, FileUploadForm uploadForm, String repoPath,
			String jiraId, String updated_date, String jiraResolution, String fixVersion, String create_date,String jiraReleaseId) {
		logger.info("Bug API to insert bug details and summary details");
		SimpleDateFormat dtJira = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		Date createddateObj = null;
		Date updateddateObj = null;
		try {
			createddateObj = dtJira.parse(create_date);
			updateddateObj = dtJira.parse(updated_date);
		} catch (Exception e) {
			logger.info(e.getMessage());

		}

		// *** same for the format String below
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		BugDetails bug = new BugDetails();
		bug.setProjectId(Integer.parseInt(projectId));
		bug.setModuleId(Integer.parseInt(moduleId));
		bug.setBugTitle(summary);
		bug.setAssignStatus(2);
		bug.setGroupStatus(2);
		bug.setGroupId(0);
		bug.setJiraId(jiraId);
		bug.setUpdatedDate(dateFormat.format(updateddateObj));
		bug.setJiraResolution(jiraResolution);
		bug.setCreatedBy(Integer.parseInt(userId));
		bug.setfixVersion(fixVersion);
		if (!(bugDescription == null))
			bug.setDescription(bugDescription);
		else
			bug.setDescription("");

		bug.setCreatedBy(Integer.parseInt(userId));
		logger.info("Set bug details to object of class BugDetails");
		bug.setCreatedDate(dateFormat.format(createddateObj));
		int status = 0;
		String assignGroupID = null;
		boolean bugGroupFLAG = false;
		boolean bugNoneFLAG = false;

		if (assignedUserID.equals("-1")) {
			assignedUserID = userId;
			bugNoneFLAG = true;
		} else {
			if (assignedUserID.contains("USR")) {
				assignedUserID = assignedUserID.substring(3);

			} else if (assignedUserID.contains("GRP")) {
				assignGroupID = assignedUserID.substring(3);
				bug.setGroupId(Integer.parseInt(assignGroupID));
				bug.setGroupStatus(1);
				bugGroupFLAG = true;
			} else {
				assignedUserID = userId;
			}
			bug.setAssignStatus(1);
		}

		status = bugDao.insertBugFromJira(bug, schema);

		if (status == 1) {
			logger.info("A row is inserted into xebt_bug table with bug deatils");
			int bugId = bugDao.getBugId(bug, schema);

			logger.info("Get bug id of inserted row");
			BugSummary bugSummary = new BugSummary();
			bugSummary.setBugId(bugId);

			bugSummary.setStatus(Integer.parseInt(statusId));

			bugSummary.setPriority(Integer.parseInt(priorityId));
			bugSummary.setSeverity(Integer.parseInt(severityId));
			bugSummary.setCategory(Integer.parseInt(categoryId));
			bugSummary.setPrevStatus(Integer.parseInt(statusId));
			bugSummary.setPrevPriority(Integer.parseInt(priorityId));
			bugSummary.setPrevSeverity(Integer.parseInt(severityId));
			bugSummary.setUpdatedDate(dateFormat.format(date));
			bugSummary.setUpdatedBy(Integer.parseInt(userId));
			bugSummary.setPrevCategory(Integer.parseInt(categoryId));
			bugSummary.setIsBuildAssigned(Integer.parseInt(buildTypeId));
			bugSummary.setJiraReleaseID(jiraReleaseId);
			if (Integer.parseInt(buildTypeId) == 1) {
				bugSummary.setManualBuild(Integer.parseInt(selectedBuildTypeId));
				bugSummary.setPrevManualBuild(Integer.parseInt(selectedBuildTypeId));
				bugSummary.setPrevAutomationBuild(0);
				bugSummary.setAutomationBuild(0);
			} else if (Integer.parseInt(buildTypeId) == 2) {
				bugSummary.setPrevAutomationBuild(Integer.parseInt(selectedBuildTypeId));
				bugSummary.setAutomationBuild(Integer.parseInt(selectedBuildTypeId));
				bugSummary.setManualBuild(0);
				bugSummary.setPrevManualBuild(0);
			} else {
				bugSummary.setManualBuild(0);
				bugSummary.setPrevManualBuild(0);
				bugSummary.setPrevAutomationBuild(0);
				bugSummary.setAutomationBuild(0);
			}
			bugSummary.setGroup_assign(1);
			int flag = 0;
			logger.info("Set bug summary details to object of class BugSummary");
			if (bugGroupFLAG) {
				List<Map<String, Object>> userList = modeuleDao.moduleUserDetails(Integer.parseInt(moduleId), schema);
				List<Map<String, Object>> groupUser = bugGroupDao.getUserbyGroupId(Integer.parseInt(assignGroupID),
						schema);

				userList.retainAll(groupUser);
				if (userList.size() > 0) {
					for (int i = 0; i < userList.size(); i++) {
						bugSummary.setAssignee(Integer.parseInt(userList.get(i).get("user_id").toString()));
						bugSummary.setPrevAssignee(Integer.parseInt(userList.get(i).get("user_id").toString()));
						flag = bugSummaryDao.insertBugSummary(bugSummary, schema);
					}

					if (flag == 1) {
						try {
							bugDao.sendMailOnCreateBug(bugId, bug, schema, bugSummary, bugGroupFLAG);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

				}
				bugSummaryDao.insertBugUserNotification(bug.getAssignStatus(), bug.getGroupStatus(), bug.getGroupId(),
						bugId, Integer.parseInt(userId), schema);
			} else {
				bugSummary.setAssignee(Integer.parseInt(assignedUserID));
				bugSummary.setPrevAssignee(Integer.parseInt(assignedUserID));
				flag = bugSummaryDao.insertBugSummary(bugSummary, schema);
				if (flag == 1 && (bugNoneFLAG != true)) {
					try {
						bugDao.sendMailOnCreateBug(bugId, bug, schema, bugSummary, bugGroupFLAG);
					} catch (Exception e) {
						e.printStackTrace();
					}
					logger.info("Mail has been sent to author and assignee");
				}
				bugSummaryDao.insertBugUserNotification(bug.getAssignStatus(), bug.getGroupStatus(), bug.getGroupId(),
						bugId, Integer.parseInt(userId), schema);
			}

			if (bugId != 0) {
				if (uploadForm != null) {
					List<MultipartFile> files = uploadForm.getFiles();
					logger.info("Get uploaded files using multipart ");
					List<String> fileNames = new ArrayList<String>();
					String uploadDirectoryPath = repoPath + "repository" + File.separator + "upload";
					logger.info("Create Folder ");
					if (null != files && files.size() > 0) {
						for (MultipartFile multipartFile : files) {
							String fileName = multipartFile.getOriginalFilename();
							if (!fileName.isEmpty()) {
								fileNames.add(fileName);
								byte[] bytes = null;
								try {
									bytes = multipartFile.getBytes();
								} catch (IOException e) {
									e.printStackTrace();
								}
								String title = multipartFile.getOriginalFilename();
								try {
									Resource resource = new ClassPathResource(uploadDirectoryPath);
									String filePath = resource.getFile().getAbsolutePath() + File.separator
											+ "bugtracker" + File.separator + fileName;
									File fileToWriteTo = null;
									fileToWriteTo = new File(filePath);
									FileUtils.writeByteArrayToFile(fileToWriteTo, bytes);
									//bugDao.insertAttachment(bugId, filePath, title, schema);
									bugDao.insertAttachment(bugId, bytes, title, schema);
									fileToWriteTo.delete();
								} catch (IOException e1) {
									e1.printStackTrace();
								}
							}
						}
					}
				}
				Map<String, Object> data = bugDao.getDataForInsertBug(bugId, schema);
				List<Map<String, Object>> bugDetails = (List<Map<String, Object>>) data.get("#result-set-2");
				List<Map<String, Object>> bugPrifix=(List<Map<String, Object>>) data.get("#result-set-3");

				if (assignedUserID.equals("-1")) {
					bugDetails.get(0).put("bug_assignee_name", "None");
				} else if (assignedUserID.contains("GRP")) {
					String groupId = assignedUserID.substring(3);
					int bugGroupId = Integer.parseInt(groupId);
					List<Map<String, Object>> bugGroup = bugGroupDao.getGroupbyId(bugGroupId, schema);
					bugDetails.get(0).put("bug_assignee_name", bugGroup.get(0).get("bug_group_name"));
				}

				/*try {
					bugDao.addCreateBugActivity(bugId, Integer.parseInt(userId), summary, schema);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}*/

				logger.info("User is redirected to bug summary page ");
				if (flag == 1) {
					logger.info("A row is inserted into xebt_bugsummary table with bug summary details");
				
					return bugPrifix;
				} 
			}
			
		} 
		List<Map<String,Object>> err = null;
		return err;
	
	}

	@Override
	public int updateBug(String bugId, String statusId, String priorityId, String assignee, String prevStatusId,
			String prevPriorityId, String severityId, String prevSeverityId, String categoryId, String prevCategoryId,
			String prevAssignee, String buildId, String prevBuildId, String prevBuildType, String comments,
			String userId, String schema, FileUploadForm uploadForm, String repoPath,String bugTitle,String bugDescription,String updateJiraIssueDate,
			String resoultionDescription, String fixVersion,String jiraReleaseId) {
		logger.info("Bug API to update bug details and bug summary details");
		
		SimpleDateFormat dtJira = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		
		Date updateddateObj = null;
		try {
			updateddateObj = dtJira.parse(updateJiraIssueDate);
		} catch (Exception e) {
			logger.info(e.getMessage());
			updateddateObj = new Date();

		}
		// *** same for the format String below
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		BugDetails bug = new BugDetails();
		bug.setBugTitle(bugTitle);
		if (!(bugDescription == null))
			bug.setDescription(bugDescription);
		else
			bug.setDescription("");
		bug.setUpdatedDate(dateFormat.format(updateddateObj));
		bug.setJiraResolution(resoultionDescription);
		bug.setfixVersion(fixVersion);
		bug.setBugId(Integer.parseInt(bugId));
		
		int status = bugDao.updateBugFromJira(bug, schema);
		
		if(status==1){
		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		date = new Date();
		BugSummary bugSummary = new BugSummary();
		bugSummary.setBugId(Integer.parseInt(bugId));
		bugSummary.setStatus(Integer.parseInt(statusId));
		bugSummary.setPriority(Integer.parseInt(priorityId));
		bugSummary.setSeverity(Integer.parseInt(severityId));
		bugSummary.setPrevStatus(Integer.parseInt(prevStatusId));
		bugSummary.setPrevPriority(Integer.parseInt(prevPriorityId));
		bugSummary.setPrevSeverity(Integer.parseInt(prevSeverityId));
		bugSummary.setCategory(Integer.parseInt(categoryId));
		bugSummary.setPrevCategory(Integer.parseInt(prevCategoryId));
		bugSummary.setPrevAssignee(Integer.parseInt(prevAssignee));
		bugSummary.setDescription(comments);
		bugSummary.setJiraReleaseID(jiraReleaseId);
		bugSummary.setIsBuildAssigned(Integer.parseInt(prevBuildType));
		if (Integer.parseInt(prevBuildType) == 1) {
			bugSummary.setManualBuild(Integer.parseInt(buildId));
			bugSummary.setPrevManualBuild(Integer.parseInt(prevBuildId));
			bugSummary.setPrevAutomationBuild(0);
			bugSummary.setAutomationBuild(0);
		} else if (Integer.parseInt(prevBuildType) == 2) {
			bugSummary.setPrevAutomationBuild(Integer.parseInt(prevBuildId));
			bugSummary.setAutomationBuild(Integer.parseInt(buildId));
			bugSummary.setManualBuild(0);
			bugSummary.setPrevManualBuild(0);
		} else {
			bugSummary.setManualBuild(0);
			bugSummary.setPrevManualBuild(0);
			bugSummary.setPrevAutomationBuild(0);
			bugSummary.setAutomationBuild(0);
		}
		bugSummary.setUpdatedDate(dateFormat.format(date));
		bugSummary.setUpdatedBy(Integer.parseInt(userId));
		bugSummary.setGroup_assign(1);

		String assignStatus = "1";
		String groupStatus = "2";

		String assignGroupID = "0";

		int flag = 0;

		logger.info("Set bug details to object of class BugDetails");

		if (assignee.equals("-1")) {
			assignee = userId;
			assignStatus = "2";
			bugSummary.setAssignee(Integer.parseInt(assignee));
			bugSummaryDao.updateBugSummaryStatus(bugSummary, schema);
			flag = bugSummaryDao.updateBugSummary(bugSummary, schema);
		} else if (assignee.contains("USR")) {
			assignee = assignee.substring(3);
			bugSummary.setAssignee(Integer.parseInt(assignee));
			bugSummaryDao.updateBugSummaryStatus(bugSummary, schema);
			flag = bugSummaryDao.updateBugSummary(bugSummary, schema);
			if (flag == 1) {
				try {
					bugSummaryDao.sendMailOnUpdateBug(bugSummary, "0", schema, false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else if (assignee.contains("GRP")) {
			assignGroupID = assignee.substring(3);
			groupStatus = "1";

			List<Map<String, Object>> moduleId = modeuleDao.getBugModule(Integer.parseInt(bugId), schema);
			List<Map<String, Object>> groupUser = bugGroupDao.getUserbyGroupId(Integer.parseInt(assignGroupID), schema);
			List<Map<String, Object>> userList = modeuleDao
					.moduleUserDetails(Integer.parseInt(moduleId.get(0).get("module").toString()), schema);

			userList.retainAll(groupUser);
			bugSummaryDao.updateBugSummaryStatus(bugSummary, schema);
			if (userList.size() > 0) {
				for (int i = 0; i < userList.size(); i++) {
					bugSummary.setGroup_assign(1);
					bugSummary.setAssignee(Integer.parseInt(userList.get(i).get("user_id").toString()));
					flag = bugSummaryDao.updateBugSummary(bugSummary, schema);
				}
				if (flag == 1) {
					try {
						bugSummaryDao.sendMailOnUpdateBug(bugSummary, assignGroupID, schema, true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			bugSummary.setAssignee(Integer.parseInt(assignee));
			bugSummaryDao.updateBugSummaryStatus(bugSummary, schema);
			flag = bugSummaryDao.updateBugSummary(bugSummary, schema);

			if (flag == 1) {
				try {
					bugSummaryDao.sendMailOnUpdateBug(bugSummary, "0", schema, false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		if (flag == 1) {
			updateBugStatus(bugId, assignee, userId, assignStatus, groupStatus, assignGroupID, schema);
			bugSummaryDao.updateBugUserNotification(Integer.parseInt(groupStatus), Integer.parseInt(bugId),
					Integer.parseInt(userId), schema);
			int summaryId = bugSummaryDao.getSummaryId(bugSummary, schema);
			logger.info("A bug is updated succesfully");
			if (summaryId != 0) {
				List<MultipartFile> files = uploadForm.getFiles();
				List<String> fileNames = new ArrayList<String>();
				String uploadDirectoryPath = repoPath + "repository" + File.separator + "upload";
				if (null != files && files.size() > 0) {
					for (MultipartFile multipartFile : files) {

						String fileName = multipartFile.getOriginalFilename();
						if (!fileName.isEmpty()) {
							fileNames.add(fileName);
							byte[] bytes = null;
							try {
								bytes = multipartFile.getBytes();
							} catch (IOException e) {
								e.printStackTrace();
							}

							try {
								Resource resource = new ClassPathResource(uploadDirectoryPath);
								String filePath = resource.getFile().getAbsolutePath() + File.separator + "bugtracker"
										+ File.separator + fileName;
								String title = multipartFile.getOriginalFilename();
								File fileToWriteTo = new File(filePath);
								FileUtils.writeByteArrayToFile(fileToWriteTo, bytes);
								bugDao.insertCommentAttachment(summaryId, filePath, title, schema);
								fileToWriteTo.delete();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				}
				try {
					bugDao.addUpdateBugActivity(Integer.parseInt(bugId), Integer.parseInt(userId), comments, schema);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				logger.info("User is redirected to bug summary page ");
			}

			return summaryId;
		} else
			return flag;
		} else {
			return status;
		}
	}

	@Override
	public int updateBugStatus(String bugId, String assignee, String userId, String assignStatus, String groupStatus,
			String groupId, String schema) {
		logger.info("Bug API to update bug assignee status in bug Details");
		BugDetails bug = new BugDetails();
		bug.setBugId(Integer.parseInt(bugId));
		bug.setAssignStatus(Integer.parseInt(assignStatus));
		bug.setGroupStatus(Integer.parseInt(groupStatus));
		bug.setGroupId(Integer.parseInt(groupId));
		logger.info("Set bug details to object of bug details");
		int flag = bugSummaryDao.updateBugAssignStatus(bug, schema);
		if (flag == 1) {
			logger.info("Bug status updated successfully");
			return flag;
		} else
			return flag;
	}

	@Override
	public Map<String, Object> getAllBugDetails(int projectId, int userId, int startValue, int offsetValue,
			String schema) {
		return bugDao.getAllBugDetails(projectId, userId, startValue, offsetValue, schema);
	}

	@Override
	public Map<String, Object> getAssignedToMeBugs(int projectId, int userID, int startValue, int offsetValue,
			String schema) {
		return bugDao.getAssignedToMeBugs(projectId, userID, startValue, offsetValue, schema);
	}

	@Override
	public List<Map<String, Object>> getBugIdMappedWithJiraId(String jiraId,String schema) {
		return bugDao.getBugIdMappedWithJiraId(jiraId,schema);
	}
	@Override
	public Map<String, Object> getReportedByMeBugs(int projectId, int userID, int startValue, int offsetValue,
			String schema) {
		return bugDao.getReportedByMeBugs(projectId, userID, startValue, offsetValue, schema);
	}

	@Override
	public int createNewBugStatus(String schema, int custTypeId) throws SQLException {
		return bugDao.createNewBugStatus(schema, custTypeId);
	}

	@Override
	public Map<String, Object> getDataForCreateBug(int projectId, int userId, String schema) {
		return bugDao.getDataForCreateBug(projectId, userId, schema);
	}

	@Override
	public List<Map<String, Object>> getBugDetailsByPrefix(String bugPrefix, String schema) {
		return bugDao.getBugDetailsByPrefix(bugPrefix, schema);
	}

	@Override
	public List<Map<String, Object>> getCommentAttachmentDeatils(int commentId, String schema) {
		return bugDao.getCommentAttachmentDeatils(commentId, schema);
	}

	@Override
	public List<Map<String, Object>> getBugAttachmentById(int attchmentId, String schema) {
		return bugDao.getBugAttachmentById(attchmentId, schema);
	}

	@Override
	public List<Map<String, Object>> getCommentAttachmentById(int attchmentId, String schema) {
		return bugDao.getCommentAttachmentById(attchmentId, schema);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public Map<String, Object> getPieChartReportData(String SourceData, String schema, int userID, int projectId) {
		// Map<String, Object> bugStat = new HashMap<String, Object>();
		Map<String, Object> modelData = new HashMap<String, Object>();
		Map<String, Object> bugGraph = bugDao.getBugDashboard(projectId, userID, 0, 10, schema);
		// List<Map<String, Object>> getBugGraphData = (List<Map<String,
		// Object>>) bugGraph.get("#result-set-5");
		List<Map<String, Object>> dashboardData = (List<Map<String, Object>>) bugGraph.get("#result-set-6");
		List<Map<String, Object>> userAssignedModuleList = (List<Map<String, Object>>) bugGraph.get("#result-set-4");

		int priorityCount = 0, statusNewCount = 0, statusClosedCount = 0, assignedToMeCount = 0, assignStatus = 0,
				newBugStatus = 0, immdediatePriCount = 0, highPriCount = 0, lowPriCount = 0, mediumPriCount = 0;
		for (int i = 0; i < dashboardData.size(); i++) {
			int assignedStatus = Integer.parseInt(dashboardData.get(i).get("user_count").toString());
			int project = Integer.parseInt(dashboardData.get(i).get("project").toString());
			int status = Integer.parseInt(dashboardData.get(i).get("bug_status").toString());
			int priority = Integer.parseInt(dashboardData.get(i).get("bug_priority").toString());
			int assignTo = Integer.parseInt(dashboardData.get(i).get("assignee").toString());
			int bugAssignStatus = Integer.parseInt(dashboardData.get(i).get("assign_status").toString());
			int groupAssignStatus = Integer.parseInt(dashboardData.get(i).get("group_assign").toString());

			if (assignedStatus > 0 && projectId == project) {

				if (status == 1) {
					newBugStatus++;
				}
				if (status == 2) {
					assignStatus++;
				}

				if (status == 5) {
					statusClosedCount++;
				} else {
					if (priority == 1) {
						priorityCount++;
					}
					statusNewCount++;

				}

				if (assignTo == userID && !(status == 5 || status == 6) && (bugAssignStatus == 1)
						&& (groupAssignStatus == 1)) {
					assignedToMeCount++;
				}
			}
			

		}

		for (int i = 0; i < dashboardData.size(); i++) {
			int assignedStatus = Integer.parseInt(dashboardData.get(i).get("user_count").toString());
			int project = Integer.parseInt(dashboardData.get(i).get("project").toString());
			// int status =
			// Integer.parseInt(dashboardData.get(i).get("bug_status").toString());
			int priority = Integer.parseInt(dashboardData.get(i).get("bug_priority").toString());

			if (projectId == project) {

				if (assignedStatus > 0 && projectId == project) {

					if (priority == 1) {
						immdediatePriCount++;
					}
					if (priority == 2) {
						highPriCount++;
					}

					if (priority == 3) {
						mediumPriCount++;
					}
					if (priority == 4) {
						lowPriCount++;
					}

				}

			}

			modelData.put("ImmediateBug", immdediatePriCount);
			modelData.put("HighBug", highPriCount);
			modelData.put("MediumBug", mediumPriCount);
			modelData.put("LowBug", lowPriCount);

			modelData.put("closedBug", statusClosedCount);
			modelData.put("NewBug", newBugStatus);
			modelData.put("assignBug", assignStatus);
			modelData.put("Source", SourceData);
			// System.out.println(newBugStatus);

		}
		return modelData;
	}

	@Override
	public int updateBug(String bugId, String statusId, String priorityId, String assignee, String prevStatusId,
			String prevPriorityId, String severityId, String prevSeverityId, String categoryId, String prevCategoryId,
			String prevAssignee, String buildId, String prevBuildId, String prevBuildType, String comments,
			String userId, String schema, FileUploadForm uploadForm, String repoPath) {
		
		logger.info("Bug API to update bug summary details");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		BugSummary bugSummary = new BugSummary();
		bugSummary.setBugId(Integer.parseInt(bugId));
		bugSummary.setStatus(Integer.parseInt(statusId));
		bugSummary.setPriority(Integer.parseInt(priorityId));
		bugSummary.setSeverity(Integer.parseInt(severityId));
		bugSummary.setPrevStatus(Integer.parseInt(prevStatusId));
		bugSummary.setPrevPriority(Integer.parseInt(prevPriorityId));
		bugSummary.setPrevSeverity(Integer.parseInt(prevSeverityId));
		bugSummary.setCategory(Integer.parseInt(categoryId));
		bugSummary.setPrevCategory(Integer.parseInt(prevCategoryId));
		bugSummary.setPrevAssignee(Integer.parseInt(prevAssignee));
		bugSummary.setDescription(comments);
		bugSummary.setIsBuildAssigned(Integer.parseInt(prevBuildType));
		if (Integer.parseInt(prevBuildType) == 1) {
			bugSummary.setManualBuild(Integer.parseInt(buildId));
			bugSummary.setPrevManualBuild(Integer.parseInt(prevBuildId));
			bugSummary.setPrevAutomationBuild(0);
			bugSummary.setAutomationBuild(0);
		} else if (Integer.parseInt(prevBuildType) == 2) {
			bugSummary.setPrevAutomationBuild(Integer.parseInt(prevBuildId));
			bugSummary.setAutomationBuild(Integer.parseInt(buildId));
			bugSummary.setManualBuild(0);
			bugSummary.setPrevManualBuild(0);
		} else {
			bugSummary.setManualBuild(0);
			bugSummary.setPrevManualBuild(0);
			bugSummary.setPrevAutomationBuild(0);
			bugSummary.setAutomationBuild(0);
		}
		bugSummary.setUpdatedDate(dateFormat.format(date));
		bugSummary.setUpdatedBy(Integer.parseInt(userId));
		bugSummary.setGroup_assign(1);

		String assignStatus = "1";
		String groupStatus = "2";

		String assignGroupID = "0";

		int flag = 0;

		logger.info("Set bug details to object of class BugDetails");

		if (assignee.equals("-1")) {
			assignee = userId;
			assignStatus = "2";
			bugSummary.setAssignee(Integer.parseInt(assignee));
			bugSummaryDao.updateBugSummaryStatus(bugSummary, schema);
			flag = bugSummaryDao.updateBugSummary(bugSummary, schema);
		} else if (assignee.contains("USR")) {
			assignee = assignee.substring(3);
			bugSummary.setAssignee(Integer.parseInt(assignee));
			bugSummaryDao.updateBugSummaryStatus(bugSummary, schema);
			flag = bugSummaryDao.updateBugSummary(bugSummary, schema);
			if (flag == 1) {
				try {
					bugSummaryDao.sendMailOnUpdateBug(bugSummary, "0", schema, false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else if (assignee.contains("GRP")) {
			assignGroupID = assignee.substring(3);
			groupStatus = "1";

			List<Map<String, Object>> moduleId = modeuleDao.getBugModule(Integer.parseInt(bugId), schema);
			List<Map<String, Object>> groupUser = bugGroupDao.getUserbyGroupId(Integer.parseInt(assignGroupID), schema);
			List<Map<String, Object>> userList = modeuleDao
					.moduleUserDetails(Integer.parseInt(moduleId.get(0).get("module").toString()), schema);

			userList.retainAll(groupUser);
			bugSummaryDao.updateBugSummaryStatus(bugSummary, schema);
			if (userList.size() > 0) {
				for (int i = 0; i < userList.size(); i++) {
					bugSummary.setGroup_assign(1);
					bugSummary.setAssignee(Integer.parseInt(userList.get(i).get("user_id").toString()));
					flag = bugSummaryDao.updateBugSummary(bugSummary, schema);
				}
				if (flag == 1) {
					try {
						bugSummaryDao.sendMailOnUpdateBug(bugSummary, assignGroupID, schema, true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			bugSummary.setAssignee(Integer.parseInt(assignee));
			bugSummaryDao.updateBugSummaryStatus(bugSummary, schema);
			flag = bugSummaryDao.updateBugSummary(bugSummary, schema);

			if (flag == 1) {
				try {
					bugSummaryDao.sendMailOnUpdateBug(bugSummary, "0", schema, false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		if (flag == 1) {
			updateBugStatus(bugId, assignee, userId, assignStatus, groupStatus, assignGroupID, schema);
			bugSummaryDao.updateBugUserNotification(Integer.parseInt(groupStatus), Integer.parseInt(bugId),
					Integer.parseInt(userId), schema);
			int summaryId = bugSummaryDao.getSummaryId(bugSummary, schema);
			logger.info("A bug is updated succesfully");
			if (summaryId != 0) {
				List<MultipartFile> files = uploadForm.getFiles();
				List<String> fileNames = new ArrayList<String>();
				String uploadDirectoryPath = repoPath + "repository" + File.separator + "upload";
				if (null != files && files.size() > 0) {
					for (MultipartFile multipartFile : files) {

						String fileName = multipartFile.getOriginalFilename();
						if (!fileName.isEmpty()) {
							fileNames.add(fileName);
							byte[] bytes = null;
							try {
								bytes = multipartFile.getBytes();
							} catch (IOException e) {
								e.printStackTrace();
							}

							try {
								Resource resource = new ClassPathResource(uploadDirectoryPath);
								String filePath = resource.getFile().getAbsolutePath() + File.separator + "bugtracker"
										+ File.separator + fileName;
								String title = multipartFile.getOriginalFilename();
								File fileToWriteTo = new File(filePath);
								FileUtils.writeByteArrayToFile(fileToWriteTo, bytes);
								bugDao.insertCommentAttachment(summaryId, filePath, title, schema);
								fileToWriteTo.delete();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				}
				try {
					bugDao.addUpdateBugActivity(Integer.parseInt(bugId), Integer.parseInt(userId), comments, schema);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				logger.info("User is redirected to bug summary page ");
			}

			return summaryId;
		} else
			return flag;

	}
	
	@Override
	public int deleteBug(int bugId, String schema) {
		int flag = bugDao.deleteBug(bugId, schema);
		if (flag != 0) {
			return 200;
		} else {
			return 500;
		}
	}

	@Override
	public int insertJiraRelease(String jiraReleaseName,String jiraProjectKey, String releaseDescription, String releaseStatus,
			String releaseDate, String jiraReleaseId,String schema) {
		JiraInstanceReleaseDetails jiraInstanceRelDetails = new JiraInstanceReleaseDetails();
		jiraInstanceRelDetails.setJiraProjectKey(jiraProjectKey);
		jiraInstanceRelDetails.setJiraReleaseVersionId(jiraReleaseId);
		jiraInstanceRelDetails.setJiraReleaseVersionNam(jiraReleaseName);
		jiraInstanceRelDetails.setReleaseDescription(releaseDescription);
		jiraInstanceRelDetails.setJiraReleaseStatus(Integer.parseInt(releaseStatus));
		jiraInstanceRelDetails.setJiraReleaseDate(releaseDate);
		
		int flag = jiraInstanceReleaseDao.insertJiraInstanceRelease(jiraInstanceRelDetails, schema);
		return flag;
		// TODO Auto-generated method stub
		
	}

	@Override
	public int updateJiraRelease(String jiraReleaseId, String jiraReleaseName, String jiraProjectKey,
			String releaseDescription, String releaseStatus, String releaseDate, String schema) {
		JiraInstanceReleaseDetails jiraInstanceRelDetails = new JiraInstanceReleaseDetails();
		jiraInstanceRelDetails.setJiraProjectKey(jiraProjectKey);
		jiraInstanceRelDetails.setJiraReleaseVersionId(jiraReleaseId);
		jiraInstanceRelDetails.setJiraReleaseVersionNam(jiraReleaseName);
		jiraInstanceRelDetails.setReleaseDescription(releaseDescription);
		jiraInstanceRelDetails.setJiraReleaseStatus(Integer.parseInt(releaseStatus));
		jiraInstanceRelDetails.setJiraReleaseDate(releaseDate);
		
		int flag = jiraInstanceReleaseDao.updateJiraInstanceRelease(jiraInstanceRelDetails, schema);
		return flag;
		
	}
	
	@Override
	public int deleteAllBugs(String schema) {
		int flag = bugDao.deleteAllBugs(schema);
		if (flag != 0) {
			return 200;
		} else {
			return 500;
		}
	}

	@Override
	public int updateJiraIdByBigId(String bugId, String jiraId, String userId, String schema) {
		// TODO Auto-generated method stub
		return 0;
	}

}

package com.xenon.api.bugtracker.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jadelabs.custmoze.query.release.CreateQuery;
import com.jadelabs.custmoze.query.release.Queryparser;
import com.xenon.api.bugtracker.CustomSearchService;
import com.xenon.bugtracker.dao.CustomSearchDAO;
import com.xenon.bugtracker.domain.CustomSearchDetails;
import com.xenon.bugtracker.domain.PieChartSearchQuery;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.dao.ReportDAO;

public class CustomSearchServiceImpl implements CustomSearchService {

	private static final Logger logger = LoggerFactory.getLogger(CustomSearchServiceImpl.class);
	private Map<String, String> reponseData = new HashMap<String, String>();

	@Autowired
	CustomSearchDAO customSearchDAO;
	
	@Autowired
	ReportDAO reportService;
	
	@Autowired
	ProjectDAO projectDAO;
	
	

	@Override
	public Map<String, String> insertCustomSearchQuery(String schema, String queryName, String query, String queryJson,
			int userId) {
		logger.info("Inserting search query");
		CustomSearchDetails customSearchDetails = new CustomSearchDetails();
		customSearchDetails.setQuery(query);
		customSearchDetails.setQueryname(queryName);
		customSearchDetails.setUserId(userId);
		customSearchDetails.setQueryjson(queryJson);
		int flag = 0;
		try {
			flag = customSearchDAO.insertCustomSearchQuery(customSearchDetails, schema);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (flag == 1) {
			reponseData.put("responseCode", "201");
			reponseData.put("status", "Query inserted successfully.");
			return reponseData;
		} else {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Query insertion unsuccessful");
			return reponseData;
		}
	}

	@Override
	public List<Map<String, Object>> getUserSavedCustomQueries(int userId, String schema) {
		return customSearchDAO.getUserSavedCustomQueries(userId, schema);
	}
	

	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getBuData(int userId, String schema,List<Map<String, Object>> getAllBugs){
		
		Map<String, Object> modelData = new HashMap<String, Object>();
		Map<String, Object> data = customSearchDAO.getAllBugData(userId, schema);
		List<Map<String, Object>> releaseList = (List<Map<String, Object>>) data.get("#result-set-1");
		modelData.put("releaseList", releaseList);
		List<Map<String, Object>> buildList = (List<Map<String, Object>>) data.get("#result-set-2");
		modelData.put("buildList", buildList);
		List<Map<String, Object>> statusList = (List<Map<String, Object>>) data.get("#result-set-3");
		modelData.put("statusList", statusList);
		List<Map<String, Object>> priorityList = (List<Map<String, Object>>) data.get("#result-set-4");
		modelData.put("priorityList", priorityList);
		List<Map<String, Object>> severityList = (List<Map<String, Object>>) data.get("#result-set-5");
		modelData.put("severityList", severityList);
		List<Map<String, Object>> queryData = (List<Map<String, Object>>) data.get("#result-set-6");
		modelData.put("queryData", queryData);
		List<Map<String, Object>> buildExeType = (List<Map<String, Object>>) data.get("#result-set-7");
		modelData.put("buildExeType", buildExeType);
		List<Map<String, Object>> autoBuildList = (List<Map<String, Object>>) data.get("#result-set-8");

		modelData.put("autoBuildList", autoBuildList);
		
		if(getAllBugs!=null){
			modelData.put("getAllBugs", getAllBugs);
		}
		else{
			modelData.put("getAllBugs", "");
		}

		modelData.put("UserSavedQueries", getUserSavedCustomQueries(userId, schema));
		
		return modelData;
		
	}
	
	@Override
	public Map<String,Object> getCustomQueryData(String userId, String schema,String jsonANDData,String queryName,int project){
		 	Map<String,Object> returnData = new HashMap<String, Object>();
		   String JsonObjAND = jsonANDData.substring(1, jsonANDData.length());
	        String stringAND = JsonObjAND.substring(0, JsonObjAND.length() - 1);
	        stringAND = stringAND.replace("\\", "");
	       
	      //  JSONObject jsonResponse = new JSONObject();
	        HashMap<String, List<JSONObject>> queryData = new HashMap<String, List<JSONObject>>();
	        List<JSONObject>temp = new ArrayList<JSONObject>();
	        List<JSONObject>buildTemp = new ArrayList<JSONObject>();
	        List<JSONObject>statusTemp = new ArrayList<JSONObject>();
	        List<JSONObject>priorityTemp = new ArrayList<JSONObject>();
	        List<JSONObject>serverityTemp = new ArrayList<JSONObject>();
	        List<JSONObject>categoryTemp = new ArrayList<JSONObject>();
	    	List<JSONObject> projectTemp = new ArrayList<JSONObject>();
	    	
	        String queryString="";
	  
	        Queryparser parser = new Queryparser();
	        CreateQuery query = new CreateQuery();
	 
	        queryData.put("release", temp);
	        queryData.put("build", buildTemp);
	        queryData.put("status", statusTemp);
	        queryData.put("priority", priorityTemp);
	        queryData.put("serverity", serverityTemp);
	        queryData.put("category", categoryTemp);
	        queryData.put("project", projectTemp);
	        
	             
	             
	              JSONObject jsnobject = new JSONObject(stringAND);
	              int objectLength = jsnobject.length();
	              for (int i = 1; i <= objectLength; i++) {
	                  String arrayName = "AND" + i + "";
	                  JSONArray array = jsnobject.getJSONArray(arrayName);
	                  if (stringAND.length() > 20) {

	                        for (int j = 0; j < array.length(); j++) {
	                              JSONObject jsonObj = array.getJSONObject(j);
	                             
	                              queryString+=jsonObj.getString("operator");
	                              if(queryString!=""){
	                            	  queryString+=" "; 
	                            	 
	                              }
	                              
	                              queryString+=jsonObj.getString("parameter");
	                              queryString+=" ";
	                              queryString+=jsonObj.getString("condition");
	                              queryString+=" ";
	                              queryString+=jsonObj.getString("selectedData");
	                              queryString+=" ";
	                              queryString+=jsonObj.getString("buildType");
	                              queryString+=" ";
	                             
	                        }
	                        queryString=queryString.substring(1, queryString.length()-1);
	                  }
	            }
	              for (int i = 1; i <= objectLength; i++) {
	                    String arrayName = "AND" + i + "";
	                    JSONArray array = jsnobject.getJSONArray(arrayName);
	                    if (stringAND.length() > 20) {

	                          for (int j = 0; j < array.length(); j++) {
	                                JSONObject jsonObj = array.getJSONObject(j);
	                               // jsonObj.getString("parameter");
	                                queryData = parser.readQuery(jsonObj,queryData);                                    
	                          }
	                    }
	              }
	             // String customQuery = query.getNewQuery(queryData,schema,Integer.parseInt(userId));
	              
	              Map<String, String> customQueries = query.getNewQuery(queryData,schema,Integer.parseInt(userId),project);
	           
	              returnData.put("customQuery", customQueries.get("currentQuery"));
	              returnData.put("severityQuery", customQueries.get("severityQuery"));
	              returnData.put("priorityQuery", customQueries.get("priorityQuery"));
	              returnData.put("statusQuery", customQueries.get("statusQuery"));
	           
	              returnData.put("jsonObject", queryString);
	          return returnData;
	}
	
	
	

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String,Object> executeCustomQuery(String jsonANDData,String schema,int userId,int project){
		Map<String,Object> modelData=new HashMap<String, Object>();
		String JsonObjAND = jsonANDData.substring(1, jsonANDData.length());
        String stringAND = JsonObjAND.substring(0, JsonObjAND.length() - 1);
        stringAND = stringAND.replace("\\", "");
        HashMap<String, List<JSONObject>> queryData = new HashMap<String, List<JSONObject>>();
        List<JSONObject>temp = new ArrayList<JSONObject>();
        List<JSONObject>buildTemp = new ArrayList<JSONObject>();
        List<JSONObject>statusTemp = new ArrayList<JSONObject>();
        List<JSONObject>priorityTemp = new ArrayList<JSONObject>();
        List<JSONObject>serverityTemp = new ArrayList<JSONObject>();
        List<JSONObject>categoryTemp = new ArrayList<JSONObject>();
        List<JSONObject> projectTemp = new ArrayList<JSONObject>();
        ArrayList statusCountArray=new ArrayList();
        ArrayList statusBugArray=new ArrayList();  
        ArrayList priorityCountArray=new ArrayList();
        ArrayList priorityBugArray=new ArrayList();  
        ArrayList severityCountArray=new ArrayList();
        ArrayList severityBugArray=new ArrayList();  
        Map <String,Object> statusData=new HashMap<String,Object>();
        Map<String,Object> priorityData=new HashMap<String,Object>();
        Map<String,Object> severityData=new HashMap <String,Object>();
        String queryString="";
  
        Queryparser parser = new Queryparser();
        CreateQuery query = new CreateQuery();
 
        queryData.put("release", temp);
        queryData.put("build", buildTemp);
        queryData.put("status", statusTemp);
        queryData.put("priority", priorityTemp);
        queryData.put("serverity", serverityTemp);
        queryData.put("category", categoryTemp);
        queryData.put("project", projectTemp);
        
        try {
             
              JSONObject jsnobject = new JSONObject(stringAND);
              int objectLength = jsnobject.length();
              for (int i = 1; i <= objectLength; i++) {
                  String arrayName = "AND" + i + "";
                  JSONArray array = jsnobject.getJSONArray(arrayName);
                  if (stringAND.length() > 20) {

                        for (int j = 0; j < array.length(); j++) {
                              JSONObject jsonObj = array.getJSONObject(j);
                             
                              queryString+=jsonObj.getString("operator");
                              if(queryString!=""){
                            	  queryString+=" "; 
                            	 
                              }
                              
                              queryString+=jsonObj.getString("parameter");
                              queryString+=" ";
                              queryString+=jsonObj.getString("condition");
                              queryString+=" ";
                              queryString+=jsonObj.getString("selectedData");
                              queryString+=" ";
                              queryString+=jsonObj.getString("buildType");
                              queryString+=" ";    
                        }
                        queryString=queryString.substring(1, queryString.length()-1);
                  }
            }
              for (int i = 1; i <= objectLength; i++) {
                    String arrayName = "AND" + i + "";
                    JSONArray array = jsnobject.getJSONArray(arrayName);
                    if (stringAND.length() > 20) {

                          for (int j = 0; j < array.length(); j++) {
                                JSONObject jsonObj = array.getJSONObject(j);
                               // jsonObj.getString("parameter");
                                queryData = parser.readQuery(jsonObj,queryData);                                    
                          }
                    }
              }
              Map<String, String> customQueries = query.getNewQuery(queryData,schema,userId,project);
              System.out.println("currentQuery = "+customQueries.get("currentQuery"));
              System.out.println("severityQuery = "+customQueries.get("severityQuery"));
              System.out.println("priorityQuery = "+customQueries.get("priorityQuery"));
              System.out.println("statusQuery = "+customQueries.get("statusQuery"));
              List<Map<String,Object>> currentQueryResult=customSearchDAO.executeQuery(customQueries.get("currentQuery"),schema);
              List<Map<String,Object>> severityQueryResult=customSearchDAO.executeQuery(customQueries.get("severityQuery"),schema);
              List<Map<String,Object>> priorityQueryResult=customSearchDAO.executeQuery(customQueries.get("priorityQuery"),schema);
              List<Map<String,Object>> statusQueryResult=customSearchDAO.executeQuery(customQueries.get("statusQuery"),schema);
              priorityData.put("Immediate",0);
              priorityData.put("High", 0);
              priorityData.put("Medium", 0);
              priorityData.put("Low", 0);
	      	 
	      	  
              severityData.put("Critical", 0);
              severityData.put("Major", 0);
              severityData.put("Moderate", 0);
              severityData.put("Minor",0);
	      	  
		      statusData.put("New", 0);
		      statusData.put("Assigned", 0);
		      statusData.put("DevinProgress", 0);
		      statusData.put("ReadyforQA", 0);
		      statusData.put("Closed", 0);
		      statusData.put("Reject", 0);
		      
            	  for(int cnt=0;cnt<statusQueryResult.size();cnt++){
            		  Map<String, Object> statusDataValues=statusQueryResult.get(cnt);
            		  for(int i=0;i<statusDataValues.size();i++){
            			  statusData.put(statusDataValues.get("bugStatus").toString().replaceAll("\\s+", ""), statusDataValues.get("Count"));
            		  }
            	  }
            	  
            	  for(int cnt=0;cnt<priorityQueryResult.size();cnt++){
            		  Map<String, Object> priorityDataValues=priorityQueryResult.get(cnt);
            		  for(int i=0;i<priorityDataValues.size();i++){ 
            			  priorityData.put(priorityDataValues.get("bugPriority").toString().replaceAll("\\s+", ""), priorityDataValues.get("Count"));
            		  }
            	  }
            	  for(int cnt=0;cnt<severityQueryResult.size();cnt++){
            		  Map<String, Object> severityDataValues=severityQueryResult.get(cnt);
            		
            		  for(int i=0;i<severityDataValues.size();i++){
            			  
            			  severityData.put(severityDataValues.get("bugSeverity").toString().replaceAll("\\s+", ""), severityDataValues.get("Count"));
            		  }
            	  }
            	
              
              modelData.put("getAllBugs", currentQueryResult);
              modelData.put("severityQueryResult", severityData);
              modelData.put("priorityQueryResult", priorityData);
              modelData.put("statusQueryResult", statusData);
              
              modelData.put("JSonData", queryString);
              System.out.println(" currentQueryResult ="+currentQueryResult);
              System.out.println(" severityQueryResult ="+severityQueryResult);
              System.out.println(" priorityQueryResult ="+priorityQueryResult);
              System.out.println(" statusQueryResult ="+statusQueryResult);
              
        } catch (Exception ex) {
        	
              ex.printStackTrace();
        }
		return modelData;
		
	}
	@Override
	public Map<String, String> updateCustomSearchQuery(String schema, String queryName, String query, String queryJson,
			int userId,int queryId) {
		logger.info("Updating search query");
		CustomSearchDetails customSearchDetails = new CustomSearchDetails();
		customSearchDetails.setQuery(query);
		customSearchDetails.setQueryname(queryName);
		customSearchDetails.setUserId(userId);
		customSearchDetails.setQueryjson(queryJson);
		customSearchDetails.setIdxebt(queryId);
		int flag = 0;
		try {
			flag = customSearchDAO.updateCustomSearchQuery(customSearchDetails, schema);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (flag == 1) {
			reponseData.put("responseCode", "201");
			reponseData.put("status", "Query inserted successfully.");
			return reponseData;
		} else {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Query insertion unsuccessful");
			return reponseData;
		}
	}

	@Override
	public Map<String, String> insertPiechartCustomSearchQuery(String schema, String queryName, String query, String queryJson,
			int userId) {
		logger.info("Inserting search query");
		CustomSearchDetails customSearchDetails = new CustomSearchDetails();
		customSearchDetails.setQuery(query);
		customSearchDetails.setQueryname(queryName);
		customSearchDetails.setUserId(userId);
		customSearchDetails.setQueryjson(queryJson);
		int flag = 0;
		try {
			flag = customSearchDAO.insertPiechartCustomSearchQuery(customSearchDetails, schema);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (flag == 1) {
			reponseData.put("responseCode", "201");
			reponseData.put("status", "Query inserted successfully.");
			return reponseData;
		} else {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Query insertion unsuccessful");
			return reponseData;
		}
	}
	
	@Override
	public List<Map<String, Object>> getUserSavedPieChartCustomQueries(int userId, String schema) {
		return customSearchDAO.getUserSavedPieChartCustomQueries(userId, schema);
	}
	
	@Override
	public  Map<String,Object> savePieChartQuery(String userId, String schema,String jsonANDData,String queryName){
			Map<String, Object> modelData = new HashMap<String, Object>();
		    String JsonObjAND = jsonANDData.substring(1, jsonANDData.length());
	        String stringAND = JsonObjAND.substring(0, JsonObjAND.length() - 1);
	        stringAND = stringAND.replace("\\", "");

	        HashMap<String, List<JSONObject>> queryData = new HashMap<String, List<JSONObject>>();
	        List<JSONObject>temp = new ArrayList<JSONObject>();
	        List<JSONObject>buildTemp = new ArrayList<JSONObject>();
			List<JSONObject> projectTemp = new ArrayList<JSONObject>();
			List<JSONObject> sourceTemp = new ArrayList<JSONObject>();
	        

	        String queryString="";
	  
	        Queryparser parser = new Queryparser();
	        CreateQuery query = new CreateQuery();
	 
	        queryData.put("release", temp);
	        queryData.put("build", buildTemp);
	        queryData.put("project", projectTemp);
	        queryData.put("source", sourceTemp);
	       
	              System.out.println("Query Data=" + "\n");
	             
	              JSONObject jsnobject = new JSONObject(stringAND);
	              int objectLength = jsnobject.length();
	              
	              for (int i = 1; i <= objectLength; i++) {
	                  String arrayName = "AND" + i + "";
	                  JSONArray array = jsnobject.getJSONArray(arrayName);
	                  if (stringAND.length() > 20) {

	                        for (int j = 0; j < array.length(); j++) {
	                              JSONObject jsonObj = array.getJSONObject(j);
	                             
	                              queryString+=jsonObj.getString("operator");
	                              if(queryString!=""){
	                            	  queryString+=" "; 
	                            	 
	                              }
	                              
	                              queryString+=jsonObj.getString("parameter");
	                              queryString+=" ";
	                              queryString+=jsonObj.getString("condition");
	                              queryString+=" ";
	                              queryString+=jsonObj.getString("selectedData");
	                              queryString+=" ";
	                              queryString+=jsonObj.getString("buildType");
	                              queryString+=" ";
	                              //System.out.println(jsonObj.getString("operator")+"\t"+jsonObj.getString("parameter")+"\t"+jsonObj.getString("condition")+"\t"+jsonObj.getString("selectedData")+"\t"+jsonObj.getString("buildType"));
	                             
	                        }
	                        queryString=queryString.substring(1, queryString.length()-1);
	                        System.out.println(queryString);
	                  }
	            }
	              for (int i = 1; i <= objectLength; i++) {
	                    String arrayName = "AND" + i + "";
	                    JSONArray array = jsnobject.getJSONArray(arrayName);
	                    if (stringAND.length() > 20) {

	                          for (int j = 0; j < array.length(); j++) {
	                                JSONObject jsonObj = array.getJSONObject(j);
	                               // jsonObj.getString("parameter");
	                                queryData = parser.readQuery(jsonObj,queryData);                                    
	                          }
	                    }
	              }
	            String customQuery = "" ;
	            modelData.put("pieChartQueryQuery", customQuery);
	            modelData.put("jsonObject", queryString);
	            
	            return modelData;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String,Object> executeCustomPieChartQuery(String jsonANDData,String schema){
		Map<String,Object> modelData=new HashMap<String, Object>();
		Map<String,Object> pieChartData=new HashMap<String,Object>();
		String JsonObjAND = jsonANDData.substring(1, jsonANDData.length());
        String stringAND = JsonObjAND.substring(0, JsonObjAND.length() - 1);
        stringAND = stringAND.replace("\\", "");
        HashMap<String, List<JSONObject>> queryData = new HashMap<String, List<JSONObject>>();
        ArrayList countArray=new ArrayList();
        ArrayList bugStatusArray=new ArrayList();
        List<JSONObject>temp = new ArrayList<JSONObject>();
        List<JSONObject>buildTemp = new ArrayList<JSONObject>();

		List<JSONObject> projectTemp = new ArrayList<JSONObject>();
		List<JSONObject> sourceTemp = new ArrayList<JSONObject>();
        
        String queryString="";
  
        Queryparser parser = new Queryparser();
        CreateQuery query = new CreateQuery();
 
        queryData.put("release", temp);
        queryData.put("build", buildTemp);
        queryData.put("project", projectTemp);
        queryData.put("source", sourceTemp);

        
        try {
    
             
              JSONObject jsnobject = new JSONObject(stringAND);
    
              System.out.println("Query Data=" + "\n"+jsnobject);
              int objectLength = jsnobject.length();
              for (int i = 1; i <= objectLength; i++) {
                  String arrayName = "AND" + i + "";
                  JSONArray array = jsnobject.getJSONArray(arrayName);
                  if (stringAND.length() > 20) {

                        for (int j = 0; j < array.length(); j++) {
                              JSONObject jsonObj = array.getJSONObject(j);
                             
                              queryString+=jsonObj.getString("operator");
                              if(queryString!=""){
                            	  queryString+=" "; 
                            	 
                              }
                              
                              queryString+=jsonObj.getString("parameter");
                              queryString+=" ";
                              queryString+=jsonObj.getString("condition");
                              queryString+=" ";
                              queryString+=jsonObj.getString("selectedData");
                              if(j==0){
                            	  String source=jsonObj.getString("selectedData");
                            	  modelData.put("source", source);
                              }
                              queryString+=" ";
                              queryString+=jsonObj.getString("buildType");
                              queryString+=" ";    
                        }
                        queryString=queryString.substring(1, queryString.length()-1);
                        System.out.println(queryString);
                  }
            }
              for (int i = 1; i <= objectLength; i++) {
                    String arrayName = "AND" + i + "";
                    JSONArray array = jsnobject.getJSONArray(arrayName);
                    if (stringAND.length() > 20) {

                          for (int j = 0; j < array.length(); j++) {
                                JSONObject jsonObj = array.getJSONObject(j);
                               // jsonObj.getString("parameter");
                                queryData = parser.readQuery(jsonObj,queryData);                                    
                          }
                    }
              }
              
              modelData.put("Immediate",0);
	      	  modelData.put("High", 0);
	      	  modelData.put("Medium", 0);
	      	  modelData.put("Low", 0);
	      	 
	      	  
	      	  modelData.put("critical", 0);
	      	  modelData.put("major", 0);
	      	  modelData.put("moderate", 0);
	      	  modelData.put("minor",0);
	      	  
	      	  modelData.put("New", 0);
	      	  modelData.put("Assigned", 0);
	      	  modelData.put("DevinProgress", 0);
	      	  modelData.put("ReadyforQA", 0);
	      	  modelData.put("Closed", 0);
	      	  modelData.put("Reject", 0);
              String customQuery= "" ;
              System.out.println(customQuery);
              List<Map<String,Object>> queryResult=customSearchDAO.executePieChartQuery(customQuery,schema);
              for (Map<String, Object> map : queryResult) {
            	    for (Map.Entry<String, Object> entry : map.entrySet()) {
            	        
            	       // pieChartData.put(entry.getKey(), entry.getValue());
            	    	
            	    	if(entry.getKey().toString().equals("count(*)")){
            	    		countArray.add(entry.getValue().toString());
            	    	}
            	    	else if(entry.getKey().toString().equals("bug_status")){
            	    		bugStatusArray.add(entry.getValue().toString());
            	    	}
            	    	else if(entry.getKey().toString().equals("bug_priority")){
            	    		bugStatusArray.add(entry.getValue().toString());
            	    	}
            	    	else if(entry.getKey().toString().equals("bug_severity")){
            	    		bugStatusArray.add(entry.getValue().toString());
            	    	}
            	    }
            	}
             System.out.println(countArray+""+bugStatusArray);
             
             for(int i=0;i<countArray.size();i++){
            	  System.out.println(countArray.get(i)+""+bugStatusArray.get(i));
            	  if(bugStatusArray.get(i).equals("New")){
	      				modelData.put("New",countArray.get(i));
	      			}
            	  else if(bugStatusArray.get(i).equals("Assigned")){
	      				modelData.put("Assigned",countArray.get(i));
	      			}
            	  else if(bugStatusArray.get(i).equals("Dev in Progress")){
	      				modelData.put("DevinProgress",countArray.get(i));
	      			}
            	  else if(bugStatusArray.get(i).equals("Ready for QA")){
	      				modelData.put("ReadyforQA",countArray.get(i));
	      			}
            	  else if(bugStatusArray.get(i).equals("Closed")){
	      				modelData.put("Closed",countArray.get(i));
	      			}
            	  else if(bugStatusArray.get(i).equals("Reject")){
	      				modelData.put("Reject",countArray.get(i));
	      			}
            	  else if(bugStatusArray.get(i).equals("Immediate")){
	      				modelData.put("Immediate",countArray.get(i));
	      			}
            	  else if(bugStatusArray.get(i).equals("High")){
	      				modelData.put("High",countArray.get(i));
	      			}
            	  else if(bugStatusArray.get(i).equals("Medium")){
	      				modelData.put("Medium",countArray.get(i));
	      			}
            	  else if(bugStatusArray.get(i).equals("Low")){
	      				modelData.put("Low",countArray.get(i));
	      			}
            	  else if(bugStatusArray.get(i).equals("Critical")){
	      				modelData.put("Critical",countArray.get(i));
	      			}
            	  else if(bugStatusArray.get(i).equals("Major")){
	      				modelData.put("Major",countArray.get(i));
	      			}
            	  else if(bugStatusArray.get(i).equals("Moderate")){
	      				modelData.put("Moderate",countArray.get(i));
	      			}
            	  else if(bugStatusArray.get(i).equals("Minor")){
	      				modelData.put("Minor",countArray.get(i));
	      			}
	      			
             }
             
              modelData.put("getAllBugs", queryResult);
              modelData.put("JSonData", queryString);
              System.out.println(queryResult);
              
        } catch (Exception ex) {
        	
              ex.printStackTrace();
        }
		return modelData;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object> pieChartData(int userID,String schema ){
		Map<String,Object> modeldata=new HashMap<String,Object>();
		Map<String, Object> data = reportService.getBugAnalysisData(userID,schema);
		List<Map<String,Object>> releaseList = (List<Map<String, Object>>) data.get("#result-set-1");
		modeldata.put("releaseList",releaseList);
		List<Map<String,Object>> buildList = (List<Map<String, Object>>) data.get("#result-set-2");
		modeldata.put("buildList",buildList);
		List<Map<String,Object>> statusList = (List<Map<String, Object>>) data.get("#result-set-3");
		modeldata.put("statusList",statusList);
		List<Map<String,Object>> priorityList = (List<Map<String, Object>>) data.get("#result-set-4");
		modeldata.put("priorityList",priorityList);
		List<Map<String,Object>> severityList = (List<Map<String, Object>>) data.get("#result-set-5");
		modeldata.put("severityList",severityList);
		List<Map<String,Object>> queryData = (List<Map<String, Object>>) data.get("#result-set-6");
		modeldata.put("queryData",queryData);
		List<Map<String,Object>> buildExeType = (List<Map<String, Object>>) data.get("#result-set-7");
		modeldata.put("buildExeType",buildExeType);
		List<Map<String,Object>> autoBuildList = (List<Map<String, Object>>) data.get("#result-set-8");
		modeldata.put("autoBuildList",autoBuildList);
		List<Map<String, Object>> projectList  = projectDAO.getAllProjects(schema);
		modeldata.put("projectList",projectList);
		return modeldata;
	}
	
	@Override
	public Map<String, String> updatePieChartQuery(String schema, String queryName, String query, String queryJson,
			int userId,int queryId) {
		logger.info("Updating search query");
		PieChartSearchQuery pieChartSearchDetails = new PieChartSearchQuery();
		pieChartSearchDetails.setQuery(query);
		pieChartSearchDetails.setQueryname(queryName);
		pieChartSearchDetails.setUserId(userId);
		pieChartSearchDetails.setQueryjson(queryJson);
		pieChartSearchDetails.setIdxebt(queryId);
		int flag = 0;
		try {
			flag = customSearchDAO.updatePieChartQuery(pieChartSearchDetails, schema);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (flag == 1) {
			reponseData.put("responseCode", "201");
			reponseData.put("status", "Query inserted successfully.");
			return reponseData;
		} else {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Query insertion unsuccessful");
			return reponseData;
		}
	}
	
	@Override
	public Map<String,Object> getCustomQueryPieChartData(String userId, String schema,String jsonANDData,String queryName){
		 	Map<String,Object> returnData = new HashMap<String, Object>();
		   String JsonObjAND = jsonANDData.substring(1, jsonANDData.length());
	        String stringAND = JsonObjAND.substring(0, JsonObjAND.length() - 1);
	        stringAND = stringAND.replace("\\", "");
	       
	      //  JSONObject jsonResponse = new JSONObject();
	        HashMap<String, List<JSONObject>> queryData = new HashMap<String, List<JSONObject>>();
	        List<JSONObject>temp = new ArrayList<JSONObject>();
	        List<JSONObject>buildTemp = new ArrayList<JSONObject>();

			List<JSONObject> projectTemp = new ArrayList<JSONObject>();
			List<JSONObject> sourceTemp = new ArrayList<JSONObject>();
	        
	        String queryString="";
	  
	        Queryparser parser = new Queryparser();
	        CreateQuery query = new CreateQuery();
	 
	        queryData.put("release", temp);
	        queryData.put("build", buildTemp);
	        queryData.put("project", projectTemp);
	        queryData.put("source", sourceTemp);
	              System.out.println("Query Data=" + "\n");
	             
	              JSONObject jsnobject = new JSONObject(stringAND);
	              int objectLength = jsnobject.length();
	              for (int i = 1; i <= objectLength; i++) {
	                  String arrayName = "AND" + i + "";
	                  JSONArray array = jsnobject.getJSONArray(arrayName);
	                  if (stringAND.length() > 20) {

	                        for (int j = 0; j < array.length(); j++) {
	                              JSONObject jsonObj = array.getJSONObject(j);
	                             
	                              queryString+=jsonObj.getString("operator");
	                              if(queryString!=""){
	                            	  queryString+=" "; 
	                            	 
	                              }
	                              
	                              queryString+=jsonObj.getString("parameter");
	                              queryString+=" ";
	                              queryString+=jsonObj.getString("condition");
	                              queryString+=" ";
	                              queryString+=jsonObj.getString("selectedData");
	                              queryString+=" ";
	                              queryString+=jsonObj.getString("buildType");
	                              queryString+=" ";
	                              //System.out.println(jsonObj.getString("operator")+"\t"+jsonObj.getString("parameter")+"\t"+jsonObj.getString("condition")+"\t"+jsonObj.getString("selectedData")+"\t"+jsonObj.getString("buildType"));
	                             
	                        }
	                        queryString=queryString.substring(1, queryString.length()-1);
	                        System.out.println(queryString);
	                  }
	            }
	              for (int i = 1; i <= objectLength; i++) {
	                    String arrayName = "AND" + i + "";
	                    JSONArray array = jsnobject.getJSONArray(arrayName);
	                    if (stringAND.length() > 20) {

	                          for (int j = 0; j < array.length(); j++) {
	                                JSONObject jsonObj = array.getJSONObject(j);
	                               // jsonObj.getString("parameter");
	                                queryData = parser.readQuery(jsonObj,queryData);                                    
	                          }
	                    }
	              }
	              String customQuery = "" ;
	           
	              returnData.put("customQuery", customQuery);
	              returnData.put("jsonObject", queryString);
	          return returnData;
	}

	@Override
	public Map<String, String> deleteCustomSearchQuery(int queryId, String schema){
		int flag = 0;
		try {
			flag = customSearchDAO.deleteCustomSearchQuery(queryId, schema);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (flag > 0) {
			reponseData.put("responseCode", "201");
			reponseData.put("status", "Query deleted successfully.");
			return reponseData;
		} else {
			reponseData.put("responseCode", "500");
			reponseData.put("status", "Query deletion unsuccessful");
			return reponseData;
		}
	}

}

package com.xenon.api.common;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.xenon.administration.domain.JiraInstanceReleaseDetails;

import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.JiraException;

public interface  JiraIntegrationService {

	JiraClient jiraConnect(HttpSession session) throws IOException;
	
	boolean testJiraConnection(String jiraURL,String userName,String password);

	int insertJiraInstance(String username, String integPass, String schema);

	List<Map<String, Object>> getJiraDetails(String schema);

	int updateJiraInstance(String username, String password, int jiraId, String schema);

	boolean isJiraConnected(String customerId) throws Exception;

	int insertJiraInstanceRelease(JiraInstanceReleaseDetails jiraReleaseDetails, String schema);
	
	List<Map<String, Object>> getJiraRelease(String releaseId,String schema);
	
	JiraClient getJiraObject(String custId,String schema) throws IOException;
	
	void fetchAndUpdateReleaseVersion(String custId,String schema,String jiraProject) throws IOException, ParseException, JiraException;
	
	boolean searchJiraIssueByUpdatedDate(String jiraProject, String schema,String custId,int projectID,String  moduleId) throws IOException, ParseException, JiraException;
}

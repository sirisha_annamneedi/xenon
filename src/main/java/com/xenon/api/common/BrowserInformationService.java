package com.xenon.api.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.mobile.device.Device;

/**
 * This Service is responsible for browser information related operations.
 * <br><b>5th May 2017</b>
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface BrowserInformationService {

	public void getUserRequestDetails(HttpServletRequest request, Device device, HttpSession session);
}

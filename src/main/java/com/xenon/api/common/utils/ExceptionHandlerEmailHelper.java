package com.xenon.api.common.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.common.MailService;
import com.xenon.common.dao.MailDAO;

/**
 * 
 * This class handles the exceptions occurred for trail executions and sends mail of exception to users
 *
 */
@SuppressWarnings("serial")
public class ExceptionHandlerEmailHelper extends Exception {
	// Parameterless Constructor
	
	private static final Logger logger = LoggerFactory.getLogger(ExceptionHandlerEmailHelper.class);
	
	@Autowired
	MailDAO mailDao;
	
	@Autowired
	MailService mailService;
	
	
	/**
	 * 
	 * This method is used to handle runtime exceptions and sending mail for trail executions exceptions
	 * 
	 * @param printStackTrace
	 */
	public ExceptionHandlerEmailHelper(Object printStackTrace) {
		logger.info("Exception occurred");
		List<Map<String, Object>> emailDetails = new ArrayList<Map<String, Object>>();
		Map<String, Object> email = new HashMap<String, Object>();
		
		Map<String, Object> properties = mailDao.getMailProperties();
		email.put("smtp_host", properties.get("SMTP_HOST"));
		email.put("smtp_user_id", properties.get("SMTP_USER"));
		email.put("smtp_password", properties.get("SMTP_PASS"));
		email.put("is_enabled", properties.get("SMTP_ENABLE"));
		emailDetails.add(email);

		ArrayList<String> recipients = new ArrayList<String>();
		recipients.add("xenon.developers@jadeglobal.com");
		
		logger.info("Sending mail to users");
		
		try {
			mailService.postMail(emailDetails, recipients, "Exception Xenon Trial User Execution",  printStackTrace.toString(), "xenon.support@jadeglobal.com");
			logger.info("Mail sent successfully");
		} catch (Exception ex) {
			logger.error("Send mail unsuccessful");
			//System.out.println("Exception :"+ex);
		}
	}
}
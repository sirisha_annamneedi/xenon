package com.xenon.api.common;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;

/**
 * This Service is responsible for providing common operations.
 * <br><b>5th May 2017</b>
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface CommonService {

	/**
	 * @author 
	 * @description 
	 * @param model
	 * @param session
	 * @return
	 */
	public Model getHeaderValues(Model model, HttpSession session);
	
	/**
	 * @author suresh.adling
	 * @description get current session details for log file 
	 * @param session
	 * @return
	 */
	public String getLogDetails(HttpSession session);
	
	/**
	 * @author suresh.adling
	 * @description get file name for log to create separate files wit respect to session
	 * @param session
	 * @return
	 */
	public String getFileName(HttpSession session);
	
	/**
	 * @author prafulla.pol
	 * @param session
	 * @return
	 */
	public int getLoginUserAssignedAppCount(HttpSession session);
	
	public List<Map<String, Object>> getHeaderValues(HttpSession session, List<Map<String, Object>> projectList);
	
	
}

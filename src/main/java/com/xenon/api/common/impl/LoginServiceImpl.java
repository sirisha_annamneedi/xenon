package com.xenon.api.common.impl;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.administration.dao.LdapDAO;
import com.xenon.api.buildmanager.UserService;
import com.xenon.api.common.LoginService;
import com.xenon.api.common.RoleAccessService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.TimezoneDAO;
import com.xenon.buildmanager.domain.UserDetails;
import com.xenon.common.dao.AdminDashboardDAO;
import com.xenon.common.dao.UserDetailsDAO;

public class LoginServiceImpl implements LoginService {

	private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

	@Autowired
	AdminDashboardDAO adminDashboardDAO;

	@Autowired
	UserDetailsDAO userDetailsDAO;

	@Autowired
	TimezoneDAO timezoneDAO;

	@Autowired
	RoleAccessService roleAccessService;
	
	@Resource(name = "configurationProperties")
	private Properties configurationProperties;
	
	@Autowired
	LdapDAO ldapDAO;
	
	@Autowired
	UserService userService;
	
	
	@Autowired
	UtilsService utilsService;
	

	@SuppressWarnings("unchecked")
	@Override
	public String authenticateUser(String userName, String password, HttpSession session) {
		boolean flag = false;
		String loginResult = "";

		session.setAttribute("logoutFlag", "0");
		//Pattern regex = Pattern.compile("([\"%&'()*+,-./:;<=>?\\[\\]^_`{|}~])");
		//Matcher matcher = regex.matcher(password);
		//while (matcher.find()) {
		//	password.replaceAll(matcher.group(0), "");
		//}

		Pattern userNamePattern = Pattern.compile("[^A-Za-z0-9@_.]");
		Matcher userNameMatcher = userNamePattern.matcher(userName);
		if (!userNameMatcher.matches()) {
			userName = userName.replaceAll("[^A-Za-z0-9@_.]", "");
		}

		logger.info("Authenticating user");

		List<Map<String, Object>> custDetails = null;
		try {
			custDetails = userDetailsDAO.validateCustomerStatus(userName);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		logger.info("Validating for customer status");
		if (custDetails.size() > 0) {
			if (Integer.parseInt(custDetails.get(0).get("StatusFalg").toString()) == 1) {

				if (Integer.parseInt(custDetails.get(0).get("ldap_status").toString()) == 2 ) {
					logger.info("Setting customer session values");

					Map<String, Object> loginData = userDetailsDAO.callLogin(userName,password, custDetails.get(0).get("customer_schema").toString());
					try {
						//System.out.println("Authenticating User with username = " + userName);
						logger.info("Reading login data");
						List<Map<String, Object>> userDetailsList = (List<Map<String, Object>>) loginData
								.get("#result-set-1");
						if (userDetailsList.size() > 0) {
							logger.info("Setting user session values");
							session.setAttribute("cust_type", custDetails.get(0).get("customer_type"));
							session.setAttribute("cust_type_id", custDetails.get(0).get("cust_type_id"));
							session.setAttribute("cust_schema", custDetails.get(0).get("customer_schema"));
							session.setAttribute("redwoodStatus", custDetails.get(0).get("redwood_status"));
							session.setAttribute("apiStatus", custDetails.get(0).get("api_status"));
							session.setAttribute("redwood_url", custDetails.get(0).get("redwood_url"));
							session.setAttribute("gitStatus", custDetails.get(0).get("git_status"));
							session.setAttribute("issue_tracker", custDetails.get(0).get("issue_tracker"));
							session.setAttribute("issue_tracker_type", custDetails.get(0).get("issue_tracker_type"));
							session.setAttribute("issue_tracker_host", custDetails.get(0).get("issue_tracker_host"));
							session.setAttribute("customerId", custDetails.get(0).get("customer_id"));
					  		session.setAttribute("automationStatus", custDetails.get(0).get("automation_status"));
							session.setAttribute("customerName", custDetails.get(0).get("customer_name"));
							session.setAttribute("custScriptlessStatus", custDetails.get(0).get("scriptless_status"));
							session.setAttribute("customerOnPremiseStatus", custDetails.get(0).get("on_premise"));
							session.setAttribute("customerLDAPStatus", custDetails.get(0).get("ldap_status"));
							session.setAttribute("customerLogo",
									getImage((byte[]) custDetails.get(0).get("customer_logo")));
							session.setAttribute("userId", userDetailsList.get(0).get("user_id"));
							session.setAttribute("userFirstname", userDetailsList.get(0).get("first_name"));
							session.setAttribute("roleId", userDetailsList.get(0).get("role_id"));
							session.setAttribute("userLastname", userDetailsList.get(0).get("last_name"));
							session.setAttribute("userProfilePhoto", userDetailsList.get(0).get("user_photo"));
							session.setAttribute("userEmailId", userDetailsList.get(0).get("email_id"));
							session.setAttribute("role", userDetailsList.get(0).get("role_type"));
							session.setAttribute("btStatus", userDetailsList.get(0).get("bt_status"));
							session.setAttribute("tmStatus", userDetailsList.get(0).get("tm_status"));
							session.setAttribute("userPhoto",
									getImage((byte[]) userDetailsList.get(0).get("user_photo")));
							session.setAttribute("userFullName", userDetailsList.get(0).get("first_name") + " "
									+ userDetailsList.get(0).get("last_name"));
							session.setAttribute("passFlag", userDetailsList.get(0).get("pass_flag"));
							session.setAttribute("userTimezone", userDetailsList.get(0).get("timezone_id"));
							session.setAttribute("jenkinStatus", custDetails.get(0).get("jenkin_status"));
							session.setAttribute("gitStatus", custDetails.get(0).get("git_status"));
							flag = true;
						} else {
							session.setAttribute("LogoutFlag", "-1");
							loginResult = "";
						}
					} catch (Exception e) {
						//System.out.println("Exception is = " + e.getMessage());
						//System.out.println("Problem while authenticating User...........");
						loginResult = "";
					}

					if (flag) {
						if (Integer.parseInt(session.getAttribute("passFlag").toString()) == 2) {
							loginResult = "password";
						} else {
							logger.info("Reading access details");
							int userRole = Integer.parseInt(session.getAttribute("roleId").toString());
							String schema = session.getAttribute("cust_schema").toString();

							List<Map<String, Object>> bmAccessList1 = (List<Map<String, Object>>) loginData
									.get("#result-set-2");
							List<Map<String, Object>> tmAccessList1 = (List<Map<String, Object>>) loginData
									.get("#result-set-3");
							List<Map<String, Object>> btAccessList1 = (List<Map<String, Object>>) loginData
									.get("#result-set-4");

							List<Map<String, Object>> supportAccessList1 = (List<Map<String, Object>>) loginData
									.get("#result-set-5");

							List<String> supportList = roleAccessService.getAccessList(supportAccessList1, schema,
									userRole);
							session.setAttribute("support_access", supportList);
							logger.info("Setting user BM access details to session");
							List<String> bmList = roleAccessService.getAccessList(bmAccessList1, schema, userRole);
							session.setAttribute("logoutFlag", "1");
							session.setAttribute("bm_access", bmList);

							logger.info("Setting user TM access details to session");
							/* role wise access for test manager */
							List<String> tmAccessList = null;
							int tmStatus = Integer.parseInt(session.getAttribute("tmStatus").toString());
							if (tmStatus == 1) {
								tmAccessList = roleAccessService.getAccessList(tmAccessList1, schema, userRole);
							}
							session.setAttribute("tm_access", tmAccessList);
							logger.info("Setting user BT access details to session");
							/* role wise access for bug tracker */
							List<String> btAccessList = null;
							int btStatus = Integer.parseInt(session.getAttribute("btStatus").toString());
							if (btStatus == 1) {
								btAccessList = roleAccessService.getAccessList(btAccessList1, schema, userRole);
							}
							session.setAttribute("bt_access", btAccessList);
							if (Integer.parseInt(session.getAttribute("roleId").toString()) == 1) {
								loginResult = "spadmindashboard";

							} else {
								loginResult = "xedashboard";
							}
						}

					} else {
						session.setAttribute("LogoutFlag", "-1");
						loginResult = "";
					}
				} else if (Integer.parseInt(custDetails.get(0).get("ldap_status").toString()) == 1 &&  Integer.parseInt(custDetails.get(0).get("on_premise").toString()) == 1 ) {
				Map<String, Object> ldapCustDetails = userDetailsDAO.getCustomerLdapDetails();
				List<Map<String, Object>> ldapDetails = (List<Map<String, Object>>) ldapCustDetails
						.get("#result-set-1");
				if(ldapDetails.size()>0){
					for(int cnt=0;cnt<ldapDetails.size();cnt++){
						Map<String, Object> authDetails=ldapDAO.authenticateLdapUser(ldapDetails.get(cnt).get("ldap_url").toString(), ldapDetails.get(cnt).get("root_dn").toString(), 
								ldapDetails.get(cnt).get("user_search_base").toString()	, ldapDetails.get(cnt).get("user_search_filter").toString(), 
								ldapDetails.get(cnt).get("user_dn").toString()	, ldapDetails.get(cnt).get("user_password").toString(), userName, password,false);
						if((Boolean) authDetails.get("authFlag")){
							
							loginResult =setUserDetails(session, custDetails, userName,true);
						}else{
							session.setAttribute("LogoutFlag", "-1");
							loginResult = "";
						}
					}
				}else{
					session.setAttribute("LogoutFlag", "-1");
					loginResult = "";
				}
			}
		}
		} else {

			Map<String, Object> ldapCustDetails = userDetailsDAO.getCustomerLdapDetails();
			List<Map<String, Object>> ldapDetails = (List<Map<String, Object>>) ldapCustDetails
					.get("#result-set-1");
			if(ldapDetails.size()>0){
				for(int cnt=0;cnt<ldapDetails.size();cnt++){
					Map<String, Object> authDetails=ldapDAO.authenticateLdapUser(ldapDetails.get(cnt).get("ldap_url").toString(), ldapDetails.get(cnt).get("root_dn").toString(), 
							ldapDetails.get(cnt).get("user_search_base").toString()	, ldapDetails.get(cnt).get("user_search_filter").toString(), 
							ldapDetails.get(cnt).get("user_dn").toString()	, ldapDetails.get(cnt).get("user_password").toString(), userName, password,true);
					if((Boolean) authDetails.get("authFlag")){
						UserDetails userDetail=(UserDetails) authDetails.get("UserDetails");
						userDetail.setBTStatus(2);
						userDetail.setTMStatus(2);
						userDetail.setPassword("admin@123");
						userDetail.setUserStatus(1);
						userDetail.setRoleId(8);
						userDetail.setUserName(userName);
						String CurrentDate=utilsService.getCurrentDateTime();
						userDetail.setCreateDate(CurrentDate);
						String repoPath=configurationProperties.getProperty("XENON_REPOSITORY_PATH");
						
						//userService.insertUser(bmAccess, emailId, firstName, lastName, "Admin@123", 7, 2, 2, 1, ldapDetails.get(cnt).get("customer_schema").toString(), ldapDetails.get(cnt).get("customer_name").toString(), projects, CurrentDate, configurationProperties.getProperty("XENON_REPOSITORY_PATH"), userName);
						int aa = userService.insertNewLdapUser(userDetail, userDetail.getEmailId(), ldapDetails.get(cnt).get("customer_schema").toString(), repoPath, ldapDetails.get(cnt).get("customer_name").toString(), "No Projects");
						session.setAttribute("passFlag", 1);
						loginResult =setUserDetails(session, ldapDetails, userName,true);
				
						break;
					}else{
						session.setAttribute("LogoutFlag", "-1");
						loginResult = "";
					}
				}
			}else{
				session.setAttribute("LogoutFlag", "-1");
				loginResult = "";
			}
			
		}
		return loginResult;
	}

	@Override
	public List<Map<String, Object>> getUserCustomerValidDateByUsername(String username) {
		logger.info("Validating customer details for user");
		try {
			return userDetailsDAO.getUserCustomerValidDateByUsername(username);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getImage(byte[] image) {
		logger.info("Reading image as string");
		if (image == null)
			return "";
		else {
			byte[] encodeBase64 = Base64.encodeBase64(image);
			try {
				return new String(encodeBase64, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String setUserDetails(HttpSession session, List<Map<String, Object>> custDetails, String userName,boolean isLdapuser){

		String loginResult;
		logger.info("Setting customer session values");

		Map<String, Object> loginData = userDetailsDAO.getLoginUserDetails(userName, custDetails.get(0).get("customer_schema").toString());
		boolean flag=false;;
		try {
			//System.out.println("Authenticating User with username = " + userName);
			logger.info("Reading login data");
			List<Map<String, Object>> userDetailsList = (List<Map<String, Object>>) loginData
					.get("#result-set-1");
			if (userDetailsList.size() > 0) {
				logger.info("Setting user session values");
				session.setAttribute("cust_type", custDetails.get(0).get("customer_type"));
				session.setAttribute("cust_type_id", custDetails.get(0).get("cust_type_id"));
				session.setAttribute("cust_schema", custDetails.get(0).get("customer_schema"));
				session.setAttribute("customerId", custDetails.get(0).get("customer_id"));
				session.setAttribute("automationStatus", custDetails.get(0).get("automation_status"));
				session.setAttribute("customerName", custDetails.get(0).get("customer_name"));
				session.setAttribute("custScriptlessStatus", custDetails.get(0).get("scriptless_status"));
				session.setAttribute("customerOnPremiseStatus", custDetails.get(0).get("on_premise"));
				session.setAttribute("customerLDAPStatus", custDetails.get(0).get("ldap_status"));
				session.setAttribute("customerLogo",
						getImage((byte[]) custDetails.get(0).get("customer_logo")));
				session.setAttribute("userId", userDetailsList.get(0).get("user_id"));
				session.setAttribute("userFirstname", userDetailsList.get(0).get("first_name"));
				session.setAttribute("roleId", userDetailsList.get(0).get("role_id"));
				session.setAttribute("userLastname", userDetailsList.get(0).get("last_name"));
				session.setAttribute("userProfilePhoto", userDetailsList.get(0).get("user_photo"));
				session.setAttribute("userEmailId", userDetailsList.get(0).get("email_id"));
				session.setAttribute("role", userDetailsList.get(0).get("role_type"));
				session.setAttribute("btStatus", userDetailsList.get(0).get("bt_status"));
				session.setAttribute("tmStatus", userDetailsList.get(0).get("tm_status"));
				session.setAttribute("userPhoto",
						getImage((byte[]) userDetailsList.get(0).get("user_photo")));
				session.setAttribute("userFullName", userDetailsList.get(0).get("first_name") + " "
						+ userDetailsList.get(0).get("last_name"));
				if(!isLdapuser)
				session.setAttribute("passFlag", userDetailsList.get(0).get("pass_flag"));
				else
					session.setAttribute("passFlag", 1);
				session.setAttribute("userTimezone", userDetailsList.get(0).get("timezone_id"));
				session.setAttribute("jenkinStatus", custDetails.get(0).get("jenkin_status"));
				session.setAttribute("gitStatus", custDetails.get(0).get("git_status"));
				flag = true;
			} else {
				session.setAttribute("LogoutFlag", "-1");
				loginResult = "";
			}
		} catch (Exception e) {
			//System.out.println("Exception is = " + e.getMessage());
			//System.out.println("Problem while authenticating User...........");
			loginResult = "";
		}

		if (flag) {
			if (Integer.parseInt(session.getAttribute("passFlag").toString()) == 2) {
				loginResult = "password";
			} else {
				logger.info("Reading access details");
				int userRole = Integer.parseInt(session.getAttribute("roleId").toString());
				String schema = session.getAttribute("cust_schema").toString();

				List<Map<String, Object>> bmAccessList1 = (List<Map<String, Object>>) loginData
						.get("#result-set-2");
				List<Map<String, Object>> tmAccessList1 = (List<Map<String, Object>>) loginData
						.get("#result-set-3");
				List<Map<String, Object>> btAccessList1 = (List<Map<String, Object>>) loginData
						.get("#result-set-4");

				List<Map<String, Object>> supportAccessList1 = (List<Map<String, Object>>) loginData
						.get("#result-set-5");

				List<String> supportList = roleAccessService.getAccessList(supportAccessList1, schema,
						userRole);
				session.setAttribute("support_access", supportList);
				logger.info("Setting user BM access details to session");
				List<String> bmList = roleAccessService.getAccessList(bmAccessList1, schema, userRole);
				session.setAttribute("logoutFlag", "1");
				session.setAttribute("bm_access", bmList);

				logger.info("Setting user TM access details to session");
				/* role wise access for test manager */
				List<String> tmAccessList = null;
				int tmStatus = Integer.parseInt(session.getAttribute("tmStatus").toString());
				if (tmStatus == 1) {
					tmAccessList = roleAccessService.getAccessList(tmAccessList1, schema, userRole);
				}
				session.setAttribute("tm_access", tmAccessList);
				logger.info("Setting user BT access details to session");
				/* role wise access for bug tracker */
				List<String> btAccessList = null;
				int btStatus = Integer.parseInt(session.getAttribute("btStatus").toString());
				if (btStatus == 1) {
					btAccessList = roleAccessService.getAccessList(btAccessList1, schema, userRole);
				}
				session.setAttribute("bt_access", btAccessList);
				if (Integer.parseInt(session.getAttribute("roleId").toString()) == 1) {
					loginResult = "spadmindashboard";

				} else {
					loginResult = "xedashboard";
				}
			}

		} else {
			session.setAttribute("LogoutFlag", "-1");
			loginResult = "";
		}
		return loginResult;
	}

}

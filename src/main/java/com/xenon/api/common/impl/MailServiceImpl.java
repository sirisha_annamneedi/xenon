package com.xenon.api.common.impl;

import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.xenon.api.common.MailService;
import com.xenon.common.dao.MailDAO;
import com.xenon.controller.ProjectController;
import com.xenon.controller.utils.Constants;

public class MailServiceImpl implements MailService {

	@Autowired
	MailDAO mailDao;

	private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);

	@SuppressWarnings("deprecation")
	@Override
	public String postMail(List<Map<String, Object>> emailDetails, ArrayList<String> recipients, String subject,
			String bodyText, String from) {
		try {
			logger.info("Checking for send mail status");
			final String strSmtpHost = emailDetails.get(0).get("smtp_host").toString();
			final String strUserID = emailDetails.get(0).get("smtp_user_id").toString();
			final String strPasswd = emailDetails.get(0).get("smtp_password").toString();
			String testString = "";
			for (int counter = 0; counter < recipients.size(); counter++) {
				testString += recipients.get(counter) + ",";
			}
			testString = testString.substring(0, testString.length() - 1);
			JSONObject postParameter = new JSONObject();
			postParameter.put("strSmtpHost", strSmtpHost);
			postParameter.put("recipients", testString);
			postParameter.put("strUserID", strUserID);
			postParameter.put("strPasswd", strPasswd);
			postParameter.put("subject", subject);
			postParameter.put("from", from);
			postParameter.put("bodyText", bodyText);

			try {
		
				ClientConfig config = new DefaultClientConfig();
				Client client = Client.create(config);
				MultivaluedMapImpl formData = new MultivaluedMapImpl();
				formData.add("mailParam", postParameter.toString());
				WebResource resource = client.resource(Constants.XenonServerWS + "/sendMail");
				ClientResponse response = resource.type("application/x-www-form-urlencoded").post(ClientResponse.class,
						formData);

				
				//Client client = Client.create();
				//WebResource webResource = client.resource(
				//Constants.XenonServerWS + "/sendMail?mailParam=" + URLEncoder.encode(postParameter.toString()));
				//ClientResponse response = webResource.accept("application/json").post(ClientResponse.class);
				//System.out.println("Request Status = " + response.getStatus());
				if (response.getStatus() != 200) {
					logger.info("Send mail FAILED");
				}
				logger.info("Send mail " + response.getStatus());
			} catch (Exception e) {
				logger.info("Send mail FAILED");
				e.printStackTrace();

			}

			logger.info("Mail is successfully send");
			return "Success";

		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

	@Override
	public String postMail(ArrayList<String> recipients, String subject, String bodyText, String from) {
		try {
			logger.info("Checking for send mail status");
			Map<String, Object> properties = mailDao.getMailProperties();
			final String strSmtpHost = properties.get("SMTP_HOST").toString();
			final String strUserID = properties.get("SMTP_USER").toString();
			final String strPasswd = properties.get("SMTP_PASS").toString();
			String testString = "";
			for (int counter = 0; counter < recipients.size(); counter++) {
				testString += recipients.get(counter) + ",";
			}
			testString = testString.substring(0, testString.length() - 1);
			JSONObject postParameter = new JSONObject();
			postParameter.put("strSmtpHost", strSmtpHost);
			postParameter.put("recipients", testString);
			postParameter.put("strUserID", strUserID);
			postParameter.put("strPasswd", strPasswd);
			postParameter.put("subject", subject);
			postParameter.put("from", from);
			postParameter.put("bodyText", bodyText);

			try {
				ClientConfig config = new DefaultClientConfig();
				Client client = Client.create(config);

				MultivaluedMapImpl formData = new MultivaluedMapImpl();
				formData.add("mailParam", postParameter.toString());

				WebResource resource = client.resource(Constants.XenonServerWS + "/sendMail");
				ClientResponse response1 = resource.type("application/x-www-form-urlencoded").post(ClientResponse.class,
						formData);

				/*
				 * Client client = Client.create(); WebResource webResource =
				 * client.resource(Constants.XenonServerWS + "/sendMail" );
				 * String
				 * input="{\"mailParam\":\""+postParameter.toString()+"\"}";
				 * ClientResponse response1 =
				 * webResource.accept("application/json").post(ClientResponse.
				 * class,input);
				 */
				//System.out.println("Request Status = " + response1.getStatus());
				if (response1.getStatus() != 200) {
					logger.info("Send mail FAILED");
				}
				logger.info("Send mail " + response1.getStatus());
			} catch (Exception e) {
				logger.info("Send mail FAILED");
				e.printStackTrace();

			}

			logger.info("Mail is successfully send");
			return "Success";

		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

	@Override
	public String postReportMail(List<Map<String, Object>> emailDetails, ArrayList<String> recipients, String subject,
			String bodyText, String from, String chartName, String filePath) {
		try {
			logger.info("Sending report mail to user");
			if (Integer.parseInt(emailDetails.get(0).get("is_enabled").toString()) == 1) {
				final String strSmtpHost = emailDetails.get(0).get("smtp_host").toString();
				final String strUserID = emailDetails.get(0).get("smtp_user_id").toString();
				final String strPasswd = emailDetails.get(0).get("smtp_password").toString();

				// Get the session object
				Properties props = new Properties();
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", strSmtpHost);
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.ssl.trust", strSmtpHost);

				Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(strUserID, strPasswd);
					}
				});

				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(strUserID));

				InternetAddress[] addressTo = new InternetAddress[recipients.size()];
				for (int i = 0; i < recipients.size(); i++) {
					addressTo[i] = new InternetAddress(recipients.get(i));
				}
				message.setRecipients(Message.RecipientType.TO, addressTo);
				message.setSubject(subject);

				MimeMultipart multipart = new MimeMultipart("related");
				BodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(bodyText, "text/html");
				multipart.addBodyPart(messageBodyPart);

				messageBodyPart = new MimeBodyPart();
				Resource resource = new ClassPathResource(
						filePath + "repository" + File.separator + chartName + ".jpeg");
				File image = resource.getFile();
				DataSource fds = new FileDataSource(image);
				messageBodyPart.setDataHandler(new DataHandler(fds));
				messageBodyPart.setHeader("Content-ID", "<image>");
				multipart.addBodyPart(messageBodyPart);

				message.setContent(multipart);
				Transport.send(message);

				Resource classPathResource = new ClassPathResource(filePath + "repository"+File.separator + chartName + ".jpeg");
				File file = classPathResource.getFile();

				if (file.delete()) {
					logger.info(file.getName() + " is deleted!");
				} else {
					logger.info("Delete operation is failed.");
				}
				logger.info("message sent successfully...");
				return "Success";
			} else {
				logger.info("Send mail disabled...");
				return "Success";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

}

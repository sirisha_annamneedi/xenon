package com.xenon.api.common.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.common.RoleAccessService;
import com.xenon.common.dao.AccessDAO;

public class RoleAccessServiceImpl implements RoleAccessService {

	@Autowired
	AccessDAO accessDao;

	private List<Map<String, Object>> rolesAccess = new ArrayList<Map<String, Object>>();
	private List<String> roles = new ArrayList<String>();
	private List<String> tempRole = new ArrayList<String>();

	@Override
	public boolean checkBMAccess(String url, List<String> bmAccess) {
		if (verifyRoles(url, bmAccess))
			return true;
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean checkBMAccess(String url, HttpSession session) {
		tempRole = new ArrayList<String>();
		tempRole = (List<String>) session.getAttribute("bm_access");

		if (verifyRoles(url, tempRole))
			return true;
		return false;
	}

	@Override
	public List<String> getBMAccessList(String schema, int userRole) {
		rolesAccess = accessDao.getBMAccessList(schema, userRole);
		Set<String> rolesList = rolesAccess.get(0).keySet();
		roles = new ArrayList<String>();
		for (String role : rolesList) {
			if (Integer.parseInt(rolesAccess.get(0).get(role).toString()) == 1) {
				roles.add(role);
			}
		}
		return roles;
	}

	@Override
	public List<String> getTMAccessList(String schema, int userRole) {
		rolesAccess = accessDao.getTMAccessList(schema, userRole);
		roles = new ArrayList<String>();
		Set<String> urlList = rolesAccess.get(0).keySet();
		for (String url : urlList) {
			if (Integer.parseInt(rolesAccess.get(0).get(url).toString()) == 1) {
				roles.add(url);
			}
		}
		return roles;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean checkTMAccess(String url, HttpSession session) {
		tempRole = new ArrayList<String>();
		tempRole = (List<String>) session.getAttribute("tm_access");

		if (verifyRoles(url, tempRole))
			return true;
		else
			return false;
	}

	@Override
	public List<String> getBTAccessList(String schema, int userRole) {
		rolesAccess = accessDao.getBTAccessList(schema, userRole);
		roles = new ArrayList<String>();
		Set<String> urlList = rolesAccess.get(0).keySet();
		for (String url : urlList) {
			if (Integer.parseInt(rolesAccess.get(0).get(url).toString()) == 1) {
				roles.add(url);
			}
		}
		return roles;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean checkBTAccess(String url, HttpSession session) {
		tempRole = new ArrayList<String>();
		tempRole = (List<String>) session.getAttribute("bt_access");

		if (verifyRoles(url, tempRole))
			return true;
		else
			return false;
	}

	@Override
	public List<String> getAccessList(List<Map<String, Object>> rolesAccessList, String schema, int userRole) {
		rolesAccess = rolesAccessList;
		Set<String> rolesList = rolesAccess.get(0).keySet();
		roles = new ArrayList<String>();
		for (String role : rolesList) {
			if (Integer.parseInt(rolesAccess.get(0).get(role).toString()) == 1) {
				roles.add(role);
			}
		}
		return roles;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean checkSupportAccess(String url, HttpSession session) {
		tempRole = new ArrayList<String>();
		tempRole = (List<String>) session.getAttribute("support_access");

		if (verifyRoles(url, tempRole))
			return true;
		else
			return false;
	}

	private boolean verifyRoles(String url, List<String> rolesdata) {
		for (String role : rolesdata) {
			if (role.trim().equalsIgnoreCase(url.trim())) {
				return true;
			}
		}
		return false;
	}

}

package com.xenon.api.common.impl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xenon.administration.dao.JiraInstanceDao;
import com.xenon.administration.dao.JiraInstanceReleaseDao;
import com.xenon.administration.domain.JiraInstanceReleaseDetails;
import com.xenon.api.bugtracker.BugService;
import com.xenon.api.common.JiraIntegrationService;
import com.xenon.bugtracker.dao.BugDAO;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.core.dao.CustomerDAO;
import com.xenon.util.service.FileUploadForm;

import net.rcarz.jiraclient.BasicCredentials;
import net.rcarz.jiraclient.Issue;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.JiraException;
import net.rcarz.jiraclient.Version;

public class JiraIntegrationServiceImpl implements JiraIntegrationService {

	@Autowired
	@Resource(name = "thirdPartyIssueTrackerProperties")
	private Properties thirdPartyIssueTrackerProperties;

	@Autowired
	private JiraInstanceDao jiraInstanceDao;
	
	
	@Autowired
	private JiraInstanceReleaseDao jiraInstanceReleaseDao;
	

	@Autowired
	CustomerDAO custDetailsDAO;

	@Autowired
	UserDAO userDetailsDAO;
	
	@Autowired
	BugDAO bugDAO;
	
	@Autowired
	UserDAO userDAO;
	
	@Autowired
	BugService bugService;
	
	@Resource(name = "configurationProperties")
	private Properties configurationProperties;

	private static final Logger logger = LoggerFactory.getLogger(JiraIntegrationServiceImpl.class);
	
	@Override
	public JiraClient jiraConnect(HttpSession session) throws IOException {

		List<Map<String, Object>> custDetails = custDetailsDAO
				.getCustomerDetailsById(Integer.valueOf(session.getAttribute("customerId").toString()));
		String custHost = custDetails.get(0).get("issue_tracker_host").toString();
		List<Map<String, Object>> jiraDetail = jiraInstanceDao
				.getJiraDetails(session.getAttribute("cust_schema").toString());
		BasicCredentials creds = new BasicCredentials(jiraDetail.get(0).get("jira_integ_username").toString(),
				jiraDetail.get(0).get("jira_integ_password").toString());
		JiraClient jira = new JiraClient(custHost, creds);
		return jira;
	}
	
	

	@Override
	public boolean isJiraConnected(String customerId) throws Exception {

		List<Map<String, Object>> custDetails = custDetailsDAO
				.getCustomerDetailsById(Integer.valueOf(customerId));
		String jiraStatus;
		if(custDetails.get(0)!=null){
		jiraStatus = custDetails.get(0).get("issue_tracker").toString();
		}else{
			jiraStatus="";
		}
		if(jiraStatus.equalsIgnoreCase("1")){
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean testJiraConnection(String jiraURL, String userName, String password) {
		BasicCredentials creds = new BasicCredentials(userName, password);
		JiraClient jira = new JiraClient(jiraURL, creds);
		try {
			if (!(jira.getProjects().get(0).equals(null)))
				return true;
			else
				return false;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public int insertJiraInstance(String username, String password, String schema) {
		return jiraInstanceDao.insertJiraInstance(username, password, schema);
	}

	@Override
	public int updateJiraInstance(String username, String password, int jiraId, String schema) {
		return jiraInstanceDao.updatJiraInstance(username, password, jiraId, schema);
	}

	@Override
	public List<Map<String, Object>> getJiraDetails(String schema) {
		return jiraInstanceDao.getJiraDetails(schema);
	}

	@Override
	public List<Map<String, Object>> getJiraRelease(String releaseId,String schema) {
		return jiraInstanceReleaseDao.getJiraRelease(releaseId,schema);
	}
	
	@Override
	public int insertJiraInstanceRelease(JiraInstanceReleaseDetails jiraReleaseDetails, String schema) {
		return jiraInstanceReleaseDao.insertJiraInstanceRelease(jiraReleaseDetails, schema);
	}
	
	@Override
	public boolean searchJiraIssueByUpdatedDate(String jiraProject, String schema,String custId,int projectID,String  moduleId) throws IOException, ParseException, JiraException {
		logger.info("Jira Project  : " + jiraProject);
		
		JiraClient jira = null;
		try {
			jira = getJiraObject(custId, schema);
		} catch (IOException e1) {
			logger.error("Exception occurred while connecting to Jira: "+e1.getMessage());
			throw e1;
		}		
		
		/* Search for issues */
		Issue.SearchResult sr = null;
	//	List<Map<String, Object>> lastUpdateDate = bugDAO.getJiraLastUpdate(jiraProject,schema);
		try {
		
			sr = jira.searchIssues(
					"project = " + jiraProject + " AND issuetype=Bug ORDER BY priority DESC, updated DESC", 1000);
			logger.info("Jira JQL search Query :" + "project = " + jiraProject
					+ " AND issuetype=Bug ORDER BY priority DESC, updated DESC");
	
		if(sr != null && !sr.issues.isEmpty()) {
		int result = bugService.deleteAllBugs(schema);
		logger.info("Deleted status of existined Bug records  : " + result);
		
		logger.info("Total Issues identified  : " + sr.total);
		for (Issue i : sr.issues) {
			logger.info("Issue Key  : " + i);
			logger.info("Issue Priority  : " + i.getPriority());
			logger.info("Issue Reporter Email  : " + i.getReporter().getEmail());
			logger.info("Issue Status  : " + i.getStatus().getName());
			logger.info("Issue Key  : " + i);

			Object cfvalueRootCause = i.getField("customfield_10360");
			String rootCause = "Not selected";
			if (!cfvalueRootCause.equals(null)) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonInString = mapper.writeValueAsString(cfvalueRootCause);

				JSONObject jsonObject = new JSONObject(jsonInString);

				rootCause = jsonObject.get("value").toString();

			}

			String createJiraIssueDate = i.getField("created").toString();
			String updateJiraIssueDate = i.getField("updated").toString();
			Hashtable<String, String> jiraStatusList = bugDAO
					.getStatusHashList(schema);
			Hashtable<String, String> jiraPriorityList = bugDAO
					.getPriorityHashList(schema);
			Hashtable<String, String> jiraCategoryList = bugDAO
					.getCategoryHashList(schema);
			List<Map<String, Object>> reporterDetails = userDAO.getUserDetailsByEmailId(i.getReporter().getEmail());
			String reporterUserId = Integer.toString(userDAO.getUserIDbyEmail("yogi.garg@riverbed.com", schema));
			try {
				reporterUserId = reporterDetails.get(0).get("user_id").toString();
				if (reporterDetails.isEmpty()) {
					logger.info("Not able to find reporter in Xenon database hence defaulting to user id 1");
				}
			} catch (Exception e) {
				logger.info("Not able to find reporter in Xenon database hence defaulting to user id 1");
			}
			

			String assigneeUserId = Integer.toString(userDAO.getUserIDbyEmail("yogi.garg@riverbed.com", schema));
			try {
				List<Map<String, Object>> assigneeDetails = userDAO.getUserDetailsByEmailId(i.getAssignee().getEmail());
			
				assigneeUserId = reporterDetails.get(0).get("user_id").toString();
				if (assigneeDetails.isEmpty()) {
					logger.info("Not able to find assignee in Xenon database hence defaulting to user id 3");
				}
			} catch (Exception e) {
				logger.info("Not able to find assignee in Xenon database hence defaulting to user id 3");
			}
			String jiraStatus = jiraStatusList.get(i.getStatus().getName());
			String jiraPriority = jiraPriorityList.get(i.getPriority().getName());
			
			String jiraReleaseId="0";
			if(i.getVersions().size()>0)
			jiraReleaseId = i.getVersions().get(0).getId();
			String jiraRootCauseId = jiraCategoryList.get(rootCause);

			FileUploadForm uploadForm = new FileUploadForm();
			String createdDate = createJiraIssueDate;
			String fixVersion = "";
			String resoultionDescription = "";
			if (!(i.getResolution() == null)) {
				resoultionDescription = i.getResolution().getDescription();
			}
			if (i.getFixVersions().size() > 0) {
				fixVersion = i.getFixVersions().get(0).getName();
			}

			

			
			List<Map<String, Object>> dataForInsertBug = bugService.insertBug(Integer.toString(projectID), moduleId, i.getSummary(), jiraStatus,
				jiraPriority, "1", jiraRootCauseId, "1", "1", reporterUserId, i.getDescription(),
				schema, assigneeUserId, uploadForm,
				configurationProperties.getProperty("XENON_REPOSITORY_PATH"), i.toString(),
				updateJiraIssueDate, resoultionDescription, fixVersion, createdDate,jiraReleaseId);
			int bugId=(Integer) dataForInsertBug.get(0).get("bug_id");
		logger.info("Newly Created Bug Id  : " + bugId);
			
		}
		return true;
		}else {
			logger.info("No issues found");
			return false;
		}
		} catch (JsonProcessingException e) {
			logger.error("Exception occurred while connecting to Jira: "+e.getMessage());
			throw e;
		} catch (JiraException e) {
			logger.error("Exception occurred while connecting to Jira: "+e.getMessage());
			throw e;
		}

	}
	
	@Override
	public void fetchAndUpdateReleaseVersion(String custId,String schema,String jiraProject) throws IOException, ParseException, JiraException {
		JiraClient jira = null;
		try {
			jira = getJiraObject(custId,schema);
		} catch (IOException e1) {
			logger.error("Exception occurred while connecting to Jira: "+e1.getMessage());
			throw e1;
		}	
		Map<Long, String> treeMap = new TreeMap<Long, String>();
		//  int days = d.getDays();
		try {
		 List<Version> fetchCurrentReleaseVersions = jira.getProject(jiraProject).getVersions();
		 for(int ver=0;ver<fetchCurrentReleaseVersions.size();ver++){
			 String jiraReleaseName = fetchCurrentReleaseVersions.get(ver).getName();
			 String jiraReleaseId = fetchCurrentReleaseVersions.get(ver).getId();
			 String releaseDescription = fetchCurrentReleaseVersions.get(ver).getDescription();
			 String releaseStatus="2";
			 if(fetchCurrentReleaseVersions.get(ver).isReleased()){
				 releaseStatus="1";
			 }
			 String releaseDate=fetchCurrentReleaseVersions.get(ver).getReleaseDate();
			 if(getJiraRelease(jiraReleaseId, schema).size()>0){
				 bugService.updateJiraRelease(jiraReleaseId,jiraReleaseName,jiraProject,releaseDescription,releaseStatus,releaseDate,schema);
			 }else{
				 bugService.insertJiraRelease(jiraReleaseName,jiraProject,releaseDescription,releaseStatus,releaseDate,jiraReleaseId,schema);
			 }
			 
			 if(!fetchCurrentReleaseVersions.get(ver).isReleased()){
				 //if(fetchCurrentReleaseVersions.get(ver).getReleaseDate()
				 LocalDate startDate = LocalDate.now();
				 SimpleDateFormat formatterJiraUpdateDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				 Date jiraReleaseDate;
				
					jiraReleaseDate = formatterJiraUpdateDateFormat.parse(fetchCurrentReleaseVersions.get(ver).getReleaseDate());
				
				 Days d = Days.daysBetween(new LocalDate(startDate), new LocalDate(jiraReleaseDate));
				 if(d.getDays()>0){
				 treeMap.put((long) d.getDays(), fetchCurrentReleaseVersions.get(ver).getId());
				 
			 }
		 }
		 }
		String currentReleaseId="0";
		currentReleaseId=(treeMap.values().toArray()[0]).toString();
		 
		bugService.insertUpdateJira("bugTracker",jiraProject, currentReleaseId,schema);
		} catch (ParseException e) {
			logger.error("Exception occurred while parsing Jira: "+e.getMessage());
			throw e;
		} catch (JiraException e) {
			logger.error("Exception occurred in Jira connection: "+e.getMessage());
			throw e;
		}
		
	}
	
	@Override
	public JiraClient getJiraObject(String custId,String schema) throws IOException {

		List<Map<String, Object>> custDetails = custDetailsDAO
				.getCustomerDetailsById(Integer.valueOf(custId));
		String custHost = custDetails.get(0).get("issue_tracker_host").toString();
		List<Map<String, Object>> jiraDetail = jiraInstanceDao
				.getJiraDetails(schema);
		BasicCredentials creds = new BasicCredentials(jiraDetail.get(0).get("jira_integ_username").toString(),
				jiraDetail.get(0).get("jira_integ_password").toString());
		JiraClient jira = new JiraClient(custHost, creds);
		return jira;
	}
}
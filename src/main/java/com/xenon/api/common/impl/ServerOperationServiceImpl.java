package com.xenon.api.common.impl;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.xenon.api.common.ServerOperationService;
import com.xenon.api.common.utils.ExceptionHandlerEmailHelper;
import com.xenon.buildmanager.dao.VMDetailsDAO;
import com.xenon.controller.ProjectController;

public class ServerOperationServiceImpl implements ServerOperationService {

	private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);

	@Override
	public String autoExe(String serverUrl, String exeJson) {
		String result = null;
		logger.info("Starting automation execution ");
		try {
			Client client = Client.create();
			@SuppressWarnings("deprecation")
			WebResource webResource = client.resource(serverUrl + "/autoexe?autoexe=" + URLEncoder.encode(exeJson));
			ClientResponse response = webResource.accept("application/json").post(ClientResponse.class);

			if (response.getStatus() != 200) {
				result = "FAILED";
				logger.info("Automation execution FAILED");
				throw new ExceptionHandlerEmailHelper(
						"\nException : server responded message with HTTP error code -------- " + response);

			}
			result = response.getEntity(String.class);
			logger.info("Automation execution " + result);
		} catch (Exception e) {
			result = "FAILED";
			logger.info("Automation execution FAILED");
			e.printStackTrace();

		}
		logger.info("Returning from automation execution ");
		return result;
	}

	@Override
	public String getServerStatus(String url) {
		String result = "";
		logger.info(" Getting server status ");
		try {
			URL siteURL = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) siteURL.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();

			int code = connection.getResponseCode();
			if (code == 200) {
				result = "OK";
				logger.info("Server status is OK");
			} else if (code == 401) {
				result = "OK";
				logger.info("Server status is OK");
			} else if (code == 404) {
				result = "FAILED";
				logger.info("Server status is FAILED");
			} else {
				result = "OK";
				logger.info("Server status is OK");
			}
		} catch (Exception e) {
			result = "FAILED";
			logger.error("Server status is FAILED");
		}
		logger.info("Returning server status ");
		return result;
	}

	@Override
	public String getDirectoryStatus(String dirPath) {
		String result = null;
		logger.info("Checking Directory status");
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(dirPath);
			ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
			if (response.getStatus() != 200) {
				result = "FAILED";
				throw new ExceptionHandlerEmailHelper(
						"\nException : server responded message with HTTP error code -------- " + response);
			}
			result = response.getEntity(String.class);
			logger.info("Server status response " + result);
		} catch (Exception e) {
			result = "FAILED";
			logger.error("Internal server exception " + e);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String checkVmStatus(String url) {
		String result = "";
		logger.info(" Getting server status ");
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(url);
			ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);

			if (response.getStatus() != 200) {
				result = "FAILED";
				throw new ExceptionHandlerEmailHelper(
						"Exception : server responded message with HTTP error code -------- " + response.getStatus());
			}
			result = response.getEntity(String.class);

		} catch (Exception e) {
			result = "FAILED";
			//System.out.println("Exception :");

		}
		return result;
	}

	@Override
	public String getTimeStamp() {
		logger.info("Gettting time stamp ");
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyhh:mm:ss:S");
		String formattedDate = sdf.format(date);
		logger.info("Returning time stamp ");
		return formattedDate.replaceAll(":", "");
	}

	@SuppressWarnings("deprecation")
	@Override
	public String createpropertiesfile(String serverUrl, String exeJson) {
		String result = null;
		logger.info("Creating properties file ");
		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource(serverUrl + "/createpropertiesfile?properties=" + URLEncoder.encode(exeJson));
			ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);

			if (response.getStatus() != 200) {
				result = "FAILED";
				logger.info("Properties file creation failed");
				throw new ExceptionHandlerEmailHelper(
						"\nException : server responded message with HTTP error code -------- " + response);

			}
			result = response.getEntity(String.class);
			logger.info("Properties file creation" + result);
		} catch (Exception e) {
			result = "FAILED";
			logger.info("Properties file creation failed");
			e.printStackTrace();

		}
		logger.info("Returning from Properties file creation ");
		return result;
	}

	@Override
	public void pollWSRequest(int time, String url, String schema, int userId, int buildId, VMDetailsDAO vmdetails,
			String vmID) {
		int iterations = time;
		String success = "FAIL";
		for (int i = 0; i < iterations; i++) {
			try {
				success = getServerStatus(url);
				if (success.equalsIgnoreCase("ok")) {
					break;
				}
				Thread.sleep(10000);

			} catch (InterruptedException ie) {
				logger.error(ie.getMessage());
			}
		}
	}

}

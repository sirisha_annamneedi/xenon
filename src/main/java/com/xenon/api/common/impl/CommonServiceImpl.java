package com.xenon.api.common.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import com.xenon.api.common.CommonService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.dao.ProjectDAO;

public class CommonServiceImpl implements CommonService {

	@Autowired
	ProjectDAO projectDao;
	
	@Autowired
	UtilsService utilsService;
	
	@Override
	public Model getHeaderValues(Model model, HttpSession session) {
		List<Map<String, Object>> projectList = projectDao.getProjectAssignedUser(
				Integer.parseInt(session.getAttribute("userId").toString()),
				session.getAttribute("cust_schema").toString());
		
		String projectName ="";
		String project="";
		boolean existFlag=false;
		boolean noProjectFlag=false;

		if (utilsService.checkSessionContainsAttribute("UserCurrentProjectName",
				session)) {
			projectName = session.getAttribute("UserCurrentProjectName")
					.toString();
			project = session.getAttribute("UserCurrentProjectId").toString();
			for (int i = 0; i < projectList.size(); i++) {
				if (Integer.parseInt(projectList.get(i).get("project_id")
						.toString()) == Integer.parseInt(project)) {
					existFlag = true;
					break;
				}
			}
			if (existFlag == false)
				noProjectFlag = true;

		} else {
			noProjectFlag = true;
		}
		if (noProjectFlag) {
			session.setAttribute("noProject", 1);
			session.removeAttribute("UserCurrentProjectName");
			session.removeAttribute("UserCurrentProjectId");
			return model.addAttribute("noProject", 1);
		}
		if (projectName.equals(null) && project.equals(null)) {
			projectList.get(0).put("projectName", "NoCurrentProject");
			projectList.get(0).put("projectId", "NoCurrentProject");
		} else {
			projectList.get(0).put("projectName", projectName);
			projectList.get(0).put("projectId", project);
		}
		model.addAttribute("projectList", projectList);
		return model;
	}

	@Override
	public String getLogDetails(HttpSession session) {
		return session.getAttribute("customerName").toString()+"_"+session.getAttribute("userFirstname").toString() +"_"+session.getAttribute("userLastname").toString() +"_"
			     +session.getAttribute("ipAddress").toString() +"_"+session.getAttribute("system").toString() +"_"+session.getAttribute("systemBrowser").toString();
	}

	@Override
	public String getFileName(HttpSession session) {
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH-mm-ss");
		Date date = new Date();
		String dateTime = dateFormat.format(date).toString();
		return dateTime+"_"+session.getAttribute("customerName").toString()+"_"+session.getAttribute("userFirstname").toString() +"_"+session.getAttribute("userLastname").toString()+"_"+session.getAttribute("systemBrowser").toString();
	
	}

	@Override
	public int getLoginUserAssignedAppCount(HttpSession session) {
		int count = projectDao.getLoginUserAssignedAppCount(session);
		return count;
	}

	@Override
	public List<Map<String, Object>> getHeaderValues(HttpSession session, List<Map<String, Object>> projectList) {
		String projectName ="";
		String project="";
		boolean existFlag=false;
		boolean noProjectFlag=false;

		if (utilsService.checkSessionContainsAttribute("UserCurrentProjectName",
				session)) {
			projectName = session.getAttribute("UserCurrentProjectName")
					.toString();
			project = session.getAttribute("UserCurrentProjectId").toString();
			for (int i = 0; i < projectList.size(); i++) {
				if (Integer.parseInt(projectList.get(i).get("project_id")
						.toString()) == Integer.parseInt(project)) {
					existFlag = true;
					break;
				}
			}
			if (existFlag == false)
				noProjectFlag = true;

		} else {
			noProjectFlag = true;
		}
		if (noProjectFlag) {
			session.setAttribute("noProject", 1);
			session.removeAttribute("UserCurrentProjectName");
			session.removeAttribute("UserCurrentProjectId");
		}
		if (projectName.equals(null) && project.equals(null)) {
			projectList.get(0).put("projectName", "NoCurrentProject");
			projectList.get(0).put("projectId", "NoCurrentProject");
		} else {
			projectList.get(0).put("projectName", projectName);
			projectList.get(0).put("projectId", project);
		}
		return projectList;
	}

}

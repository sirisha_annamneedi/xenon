package com.xenon.api.common.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xenon.api.common.JenkinsRequestHandlerService;

@SuppressWarnings("deprecation")
public class JenkinsRequestHandlerServiceImpl implements JenkinsRequestHandlerService {

	final Logger logger = LoggerFactory.getLogger(JenkinsRequestHandlerServiceImpl.class);

	@Override
	@SuppressWarnings("resource")
	public JSONObject handleGetRequestWithJSONResponse(String url, String userName, String passwordOrToken)
			throws IOException {
		if (url.startsWith("http://")) {
			url = url.replace("http://", "");
		}
		StringBuilder strBuild = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		logger.info("Calling API : " + "http://" + userName + ":" + passwordOrToken + "@" + url);
		HttpGet request = new HttpGet("http://" + userName + ":" + passwordOrToken + "@" + url);
		HttpResponse response = client.execute(request);
		// logger.info("Status Response from jenkins =
		// "+response.getStatusLine());
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		java.lang.String line = "";
		while ((line = rd.readLine()) != null) {
			strBuild.append(line);
		}
		JSONObject jsonResponse = new JSONObject();
		if (response.getStatusLine().getStatusCode() == 200) {
			try {
				jsonResponse = new JSONObject(strBuild.toString());
				jsonResponse.put("responseCode", response.getStatusLine().getStatusCode());
				return jsonResponse;
			} catch (JSONException e) {
				logger.info("Something went wrong " + e.getMessage());
				e.printStackTrace();
			}
		} else {
			jsonResponse.put("responseCode", response.getStatusLine().getStatusCode());
			jsonResponse.put("status", response.getStatusLine().getReasonPhrase());
		}
		return jsonResponse;
	}

	@SuppressWarnings("resource")
	@Override
	public Map<String, String> handleGetRequest(String url, String userName, String passwordOrToken)
			throws IOException {

		Map<String, String> reponseData = new HashMap<String, String>();
		if (url.startsWith("http://")) {
			url = url.replace("http://", "");
		}
		HttpClient client = new DefaultHttpClient();
		logger.info("Calling API : " + "http://" + userName + ":" + passwordOrToken + "@" + url);
		HttpGet request = new HttpGet("http://" + userName + ":" + passwordOrToken + "@" + url);
		HttpResponse response = client.execute(request);
		reponseData.put("responseCode", "" + response.getStatusLine().getStatusCode());
		reponseData.put("status", response.getStatusLine().getReasonPhrase());
		return reponseData;
	}

}

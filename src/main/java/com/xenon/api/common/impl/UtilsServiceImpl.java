package com.xenon.api.common.impl;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
//import java.util.Base64;    
  
import javax.crypto.KeyGenerator;   
import javax.crypto.SecretKey; 
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.common.CommonService;
import com.xenon.api.common.MailService;
import com.xenon.api.common.UtilsService;
import com.xenon.common.dao.exceptions.LdapAuthenticationException;

public class UtilsServiceImpl implements UtilsService {

	@Autowired
	CommonService commonService;
	
	@Autowired
	MailService mailService;
	
	@Resource(name = "configurationProperties")
	private Properties configurationProperties;
	
	private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
	private String dateformat = "dd-MMM-yyyy hh:mm a";
	static Cipher cipher;

	/**
	 * This method sends mail on exception
	 * 
	 * @param ex
	 * @param session
	 */
	@Override
	public void sendMailonExecption(Exception ex, HttpSession session) {
		// send mail to xenon admin
		//System.out.println("-----Execption log-----------------");
		getLoggerUser(session);
		logger.info("-----Start Execption log-----------------");
		logger.info(printStacktrace(ex));
		logger.info("-----End Execption log-----------------");

		ArrayList<String> recipients = new ArrayList<String>();
		recipients.add("xenon.developers@jadeglobal.com");
		String cust_name = session.getAttribute("customerName").toString();
		String user_name = session.getAttribute("userFullName").toString();
		String subject = "Exception : User - " + user_name + " Customer - " + cust_name;
		String bodyText = printStacktrace(ex);
		String from = "";
		try {

			mailService.postMail(recipients, subject, bodyText, from);

		} catch (Exception e) {
			e.printStackTrace();
		}
		ex.printStackTrace();
		//System.out.println(ex);
		//System.out.println("-----Execption log-----------------"+ex.getMessage());
	}
	
	@Override
	public void sendMailonExecptionAPI(Exception ex,String apiName) {
		// send mail to xenon admin
		//System.out.println("-----Execption log-----------------");
		logger.info("-----Start Execption log-----------------");
		logger.info(printStacktrace(ex));
		logger.info("-----End Execption log-----------------");

		ArrayList<String> recipients = new ArrayList<String>();
		recipients.add("xenon.web@jadeglobal.com");
		String subject = "Exception for API : "+apiName;
		String bodyText = printStacktrace(ex);
		String from = "";
		try {

			mailService.postMail( recipients, subject, bodyText, from);

		} catch (Exception e) {
			e.printStackTrace();
		}
		ex.printStackTrace();
		//System.out.println(ex);
		//System.out.println("-----Execption log-----------------");
	}


	/**
	 * This method returns blob image as string
	 * 
	 * @param image
	 * @return
	 */
	@Override
	public String getImage(byte[] image) {
		logger.info("returns blob image as string");
		byte[] encodeBase64 = Base64.encodeBase64(image);
		try {
			return new String(encodeBase64, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * This method generates random string
	 * 
	 * @param strLen
	 * @return
	 */
	@Override
	public String generateRandomString(int strLen) {
		logger.info("generates random string");
		String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		Random random = new Random();
		StringBuilder randomeStr = new StringBuilder(strLen);
		for (int i = 0; i < strLen; i++)
			randomeStr.append(characters.charAt(random.nextInt(characters.length())));
		return randomeStr.toString();
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method converts map to JSON
	 * 
	 * @param inputMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public String convertMapToJson(Map<String, Object> inputMap){
		logger.info("converts map to JSON");
		ObjectMapper mapperObj = new ObjectMapper();
		String jsonResp = null;
		try {
			jsonResp = mapperObj.writeValueAsString(inputMap);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return jsonResp;
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method converts list map to JSON
	 * 
	 * @param inputList
	 * @return
	 * @throws Exception
	 */

	@Override
	public ArrayList<String> convertListOfMapToJson(List<Map<String, Object>> inputList) {
		logger.info("converts list map to JSON");
		ArrayList<String> arrList = new ArrayList<String>();

		for (int i = 0; i < inputList.size(); i++) {
			arrList.add(convertMapToJson(inputList.get(i)));
		}
		return arrList;
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method gives the password
	 * 
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	@Override
	public String getPassword(String password) {
		logger.info("gives the password");
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		md.update(password.getBytes());

		byte byteData[] = md.digest();

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method checks whether session contains provided attribute or
	 *         not
	 * 
	 * @param attributeName
	 * @param session
	 * @return
	 */
	@Override
	public boolean checkSessionContainsAttribute(String attributeName, HttpSession session) {
		logger.info("checks whether session contains provided attribute or not");
		if (session != null) {
			Enumeration<String> attributes = session.getAttributeNames();
			while (attributes.hasMoreElements()) {
				if (attributes.nextElement().toString().equalsIgnoreCase(attributeName)) {
					return true;
				}
			}
		}
		return false;

	}

	/**
	 * This method converts byte array to file
	 * 
	 * @param imageInByte
	 * @return
	 */
	@Override
	public String convertByteArrayToFile(byte[] imageInByte) {
		logger.info("converts byte array to file");
		BufferedImage bImageFromConvert;
		String filePath = "";
		try {
			//filePath = "C:\\repository\\upload\\temp.jpg";
            filePath = ".\\XenonWeb\\repository\\upload\\temp.jpg";

			byte[] encodeBase64 = Base64.encodeBase64(imageInByte);
			// String abc = new String(encodeBase64, "UTF-8");

			// convert byte array back to BufferedImage

			bImageFromConvert = ImageIO.read(new ByteArrayInputStream(encodeBase64));

			ImageIO.write(bImageFromConvert, "jpg", new File(filePath));
			return filePath;
		} catch (IOException e) {
			//System.out.println(e.getMessage());

		}
		return filePath;

	}

	/**
	 * This method creates directory
	 * 
	 * @param dirName
	 * @return
	 */
	@Override
	public boolean createDirectory(String dirName) {
		boolean result = false;
		logger.info("creates directory");
		File dir = new File(dirName);

		if (!dir.exists()) {
			try {

				dir.mkdir();
				result = true;
			} catch (SecurityException se) {
			}
			if (result) {
				//System.out.println("DIR created ");
			}
		} else {
			result = true;
		}

		return result;
	}

	/**
	 * 
	 * This method creates log file
	 * 
	 * @param filePath
	 * @return
	 */
	@Override
	public boolean createLogfile(String filePath) {
		logger.info("creates log file");
		boolean flag = false;
		try {

			File file = new File(filePath);

			if (file.createNewFile()) {
				//System.out.println("File is created!");
				flag = true;
			} else {
				//System.out.println("File already exists.");
				flag = true;
			}

		} catch (IOException e) {
			flag = false;
			e.printStackTrace();
		}

		return flag;
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method gives customer end date
	 * 
	 * @param numberOfDays
	 * @return
	 */
	@Override
	public String getCustomerEndDate(int numberOfDays) {
		logger.info("gives customer end date");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, numberOfDays);
		String output = dateFormat.format(c.getTime());
		// get current date time with Date()

		String end_date = output;
		return end_date;
	}

	/**
	 * This method gives logger user
	 * 
	 * @param session
	 */
	@Override
	public void getLoggerUser(HttpSession session) {
		if (checkSessionContainsAttribute("userFileName", session)) {
			MDC.remove("user");
			MDC.put("user", session.getAttribute("userFileName").toString());
			MDC.put("details", commonService.getLogDetails(session));
		}
	}

	/**
	 * This method gives file data
	 * 
	 * @param data
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@Override
	public String getFileData(byte[] data) {
		logger.info(" gives file data");
		if (data != null) {
			byte[] encodeBase64 = Base64.encodeBase64(data);
			try {
				return new String(encodeBase64, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return "";
	}

	/**
	 * This method converts string to plain text
	 * 
	 * @param userString
	 * @return
	 * @throws JSONException 
	 */
	@Override
	public JSONObject convertToPlainText(String[] userString) throws JSONException {
		logger.info("converts string to plain text");
		JSONObject plainTesxt = new JSONObject();
		String temp = "";
		String regex = "^[a-zA-Z0-9]+$";
		for (String text : userString) {
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(text);
			if (!matcher.matches()) {
				temp = text.replaceAll("[^A-Za-z0-9 _ ]", "");
				if (temp == "" || temp.equals(null) || temp.equals("")) {
					temp = temp.replace("", "_");
				}
				plainTesxt.put(text, temp);
			} else {
				plainTesxt.put(text, text);
			}
		}
		return plainTesxt;
	}

	/**
	 * This method converts string to plain text
	 * 
	 * @param userString
	 * @return
	 * @throws JSONException 
	 */
	@Override
	public JSONObject convertToPlainTextForCustomer(String[] userString) throws JSONException {
		logger.info("converts string to plain text");
		JSONObject plainTesxt = new JSONObject();
		String temp = "";
		String regex = "^[a-zA-Z0-9]+$";
		for (String text : userString) {
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(text);
			if (!matcher.matches()) {
				temp = text.replaceAll("[^A-Za-z0-9]", "");
				plainTesxt.put(text, temp);
			} else {
				plainTesxt.put(text, text);
			}
		}
		return plainTesxt;
	}

	/**
	 * This method removes duplicate
	 * 
	 * @param userList
	 * @return
	 */
	@Override
	public List<Map<String, Object>> removeDuplicate(List<Map<String, Object>> userList) {
		logger.info("removes duplicate");
		Set<Map<String, Object>> removeDuplicate = new HashSet<Map<String, Object>>();
		removeDuplicate.addAll(userList);
		userList.clear();
		userList.addAll(removeDuplicate);
		return userList;
	}

	/**
	 * This method gives test count
	 * 
	 * @param test_list
	 * @param custom_list
	 * @param state
	 * @return
	 */
	@Override
	public List<Map<String, Object>> getTestCount(List<Map<String, Object>> test_list,
			List<Map<String, Object>> custom_list, String state) {
		logger.info("gives test count");
		int count = 0;
		String against;
		String custom_id;
		if (state.equals("project")) {
			against = "projectid";
			custom_id = "project_id";
		} else if (state.equals("module")) {
			against = "mdl_id";
			custom_id = "module_id";
		} else {
			against = "sce_id";
			custom_id = "scenario_id";
		}
		for (int j = 0; j < custom_list.size(); j++) {
			for (int i = 0; i < test_list.size(); i++) {
				if (!(test_list.get(i).get("testcase_id") == null)) {
					if (test_list.get(i).get(against) != null && test_list.get(i).get(against).equals(custom_list.get(j).get(custom_id))) {
						count++;
					}
				}
			}
			custom_list.get(j).put("testCasesCount", count);
			count = 0;
		}
		return custom_list;
	}

	/**
	 * This method prints stacktrace
	 * 
	 * @param ex
	 * @return
	 */
	@Override
	public String printStacktrace(Exception ex) {
		logger.info("prints stacktrace");
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		return stringWriter.toString();
	}


	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method converts list map to JSON array
	 * 
	 * @param listMap
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public JSONArray convertListToJSONArray(List<Map<String, Object>> listMap) {
		logger.info("converts list map to JSON array");
		return new JSONArray(Arrays.asList(listMap));
	}

	/**
	 * This method gives screenshot status from id
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public String getScreenShotStatusFromId(String id) {
		logger.info("gives screenshot status from id");
		String result = "";

		if (id.equals("1")) {
			result = "YES";
		} else if (id.equals("2")) {
			result = "ONFAIL";
		} else if (id.equals("3")) {
			result = "NO";
		}

		return result;
	}

	/**
	 * This method gives report bug status from id
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public String getRerportBugStatusFromId(String id) {
		logger.info("gives report bug status from id");
		String result = "";

		if (id.equals("1")) {
			result = "YES";
		} else if (id.equals("2")) {
			result = "NO";
		}
		return result;
	}

	@Override
	@SuppressWarnings("deprecation")
	public String createPieChart(int totalPassCount, int totalFailCount, int totalSkipCount, int totalBlockCount,
			int totalNotRunCount,String filePath){
		DefaultPieDataset dataset = new DefaultPieDataset();
		if (totalPassCount != 0)
			dataset.setValue("Pass", new Double(totalPassCount));
		else
			dataset.setValue("Pass", 0);
		if (totalFailCount != 0)
			dataset.setValue("Fail", new Double(totalFailCount));
		else
			dataset.setValue("Fail", 0);
		if (totalSkipCount != 0)
			dataset.setValue("Skip", new Double(totalSkipCount));
		else
			dataset.setValue("Skip", 0);
		if (totalBlockCount != 0)
			dataset.setValue("Block", new Double(totalBlockCount));
		else
			dataset.setValue("Block", 0);
		if (totalNotRunCount != 0)
			dataset.setValue("Not Run", new Double(totalNotRunCount));
		else
			dataset.setValue("Not Run", 0);

		JFreeChart chart = ChartFactory.createPieChart("", // chart title
				dataset, // data
				false, // include legend
				true, false);

		int width = 350; /* Width of the image */
		int height = 280; /* Height of the image */

		PiePlot piePlot = (PiePlot) chart.getPlot();

		piePlot.setSectionPaint(0, java.awt.Color.decode("#009900"));
		piePlot.setSectionPaint(1, java.awt.Color.decode("#cc2900"));
		piePlot.setSectionPaint(2, java.awt.Color.decode("#e6e600"));
		piePlot.setSectionPaint(3, java.awt.Color.decode("#804000"));
		piePlot.setSectionPaint(4, java.awt.Color.decode("#787878"));
		chart.setBackgroundPaint(Color.WHITE);
		chart.getPlot().setBackgroundPaint(Color.WHITE);
		PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator("{0}: {1} ({2})", new DecimalFormat("0"),
				new DecimalFormat("0%"));
		piePlot.setLabelGenerator(gen);
		piePlot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
		piePlot.setNoDataMessage("No data available");
		piePlot.setSimpleLabels(true);
		piePlot.setIgnoreZeroValues(true);
		piePlot.setOutlineVisible(true);
		String chartName = getCurrentTimeStamp();
		File pieChart = new File(filePath+"/repository/" + chartName + ".jpeg");
		try {
			ChartUtilities.saveChartAsJPEG(pieChart, chart, width, height);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return chartName;
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method gives current month number
	 * 
	 * @return
	 */
	@Override
	public int getCurrentMonthNumber() {
		java.util.Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.MONTH);
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method gives date time zone
	 * 
	 * @param timezone
	 * @param dateInString
	 * @return
	 */
	@Override
	public String getDateTimeOfTimezone(String timezone, String dateInString) {
		try {
			logger.info("gives date time zone");
			SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a");
			Date date;
			date = formatter.parse(dateInString);

			DateTime dt = new DateTime(date);
			DateTimeZone dtZone = DateTimeZone.forID(timezone);
			DateTime dtus = dt.withZone(dtZone);
			Date dateOfTimezone = dtus.toLocalDateTime().toDate();

			SimpleDateFormat dateFormatter = new SimpleDateFormat(dateformat);
			return dateFormatter.format(dateOfTimezone);
		} catch (ParseException e) {
			e.printStackTrace();

		}
		return null;
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method gives current time zone
	 * 
	 * @param timezone
	 * @return
	 */
	@Override
	public String getNowDateOfTimezone(String timezone) {
		logger.info("gives current time zone");
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a");
		Date date = new Date();
		return getDateTimeOfTimezone(timezone, dateFormat.format(date));
	}

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method gives previous date of time zone
	 * 
	 * @param timezone
	 * @return
	 */
	@Override
	public String getPrevDateOfTimezone(String timezone) {
		logger.info("gives previous date of time zone");
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);

		return getDateTimeOfTimezone(timezone, dateFormat.format(cal.getTime()));
	}
	
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method returns current date time
	 * 
	 * @return
	 */
	@Override
	public String getCurrentDate() {
		logger.info("returns current date time");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		return currentDate;
	}

	@Override
	public String getCurrentTimeStamp() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyhmmss");
		String formattedDate = sdf.format(date);
		return formattedDate;
	}

	/**
	 * This method gives date time formatted
	 * 
	 * @param inputString
	 * @return
	 * @throws ParseException
	 */
	@Override
	public String getDate(String inputString){
		logger.info("Convert input string into a date");
		// Convert input string into a date
		DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");

		Date date = null;
		try {
			date = inputFormat.parse(inputString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		logger.info("Format date into output format");
		// Format date into output format
		DateFormat outputFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a");
		String outputString = outputFormat.format(date);
		return outputString;
	}
	
	/**
	 * @author dhiraj.bendale
	 * @return
	 */
	@Override
	public String getCurrentDate(String dateformat) {
		DateFormat dateFormat = new SimpleDateFormat(dateformat);
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	/**
	 * @author dhiraj.bendale
	 * Change date format
	 * 
	 * @Date , OldFormat ,newdateFormat
	 */
	@Override
	public String changeDateFormat(String dateStr, String oldDateFormat, String newDateFormat) {
		logger.info("Convert date format from " + oldDateFormat + " to " + newDateFormat);
		try {
			DateFormat srcDf = new SimpleDateFormat(oldDateFormat);
			Date date = srcDf.parse(dateStr);
			DateFormat destDf = new SimpleDateFormat(newDateFormat);
			dateStr = destDf.format(date);
		} catch (ParseException e) {
			logger.error(e.getMessage());
		}
		return dateStr;
	}
	
	/**
	 * @author prafulla.pol
	 * 
	 *         This method returns current date time
	 * 
	 * @return
	 */
	@Override
	public String getCurrentDateTime() {
		logger.info("returns current date time");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * @param strToDecrypt
	 * @return
	 * @throws Exception
	 */
	public String getDecryptedString(String strToDecrypt) throws Exception {

		logger.info("Reading decryption keys");
	    String psk = configurationProperties.getProperty("SECRET_KEY"); 
	    String iv = configurationProperties.getProperty("IV_KEY");  
	    try {
	        String encryptionKey = psk;
	        logger.info("Decrypting string");
	        final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
	        final SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes(StandardCharsets.UTF_8), "AES");
	        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv.getBytes(StandardCharsets.UTF_8)));
	        byte[] cipherText=Base64.decodeBase64(strToDecrypt);
	        
			return new String(cipher.doFinal(cipherText), StandardCharsets.UTF_8);
	    } catch (Exception ex) {
	    	logger.error("Decyption failed ="+ex.getMessage());
	        throw new LdapAuthenticationException(ex.getMessage());
	    }
	}
	
	public String encrypt(String plainText, SecretKey secretKey)
            throws Exception {
        byte[] plainTextByte = plainText.getBytes();
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] encryptedByte = cipher.doFinal(plainTextByte);
         byte[] encoder = Base64.encodeBase64(encryptedByte);
        String encryptedText = encoder.toString();
        return encryptedText;
    }

    public String decrypt(String encryptedText, SecretKey secretKey)
            throws Exception {
        byte[] decoder = Base64.decodeBase64(encryptedText);
       // byte[] encryptedTextByte = decoder.decode(encryptedText);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decryptedByte = cipher.doFinal(decoder);
        String decryptedText = new String(decryptedByte);
        return decryptedText;
    }
}
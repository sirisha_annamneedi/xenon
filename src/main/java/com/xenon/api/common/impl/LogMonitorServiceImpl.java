package com.xenon.api.common.impl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.xenon.api.common.LogMonitorService;

public class LogMonitorServiceImpl implements LogMonitorService {

	BufferedReader bufferedReader;

	@Override
	public JSONArray readLog(String logPath) {
		JSONArray logArray = new JSONArray();

		try {
			FileReader fileReader = new FileReader(logPath);
			Thread.sleep(1 * 500);
			bufferedReader = new BufferedReader(fileReader);
			while (true) {
				JSONObject logJson = new JSONObject();
				String line = bufferedReader.readLine();
				if (line == null) {
					break;

				} else {
					logJson.put("log", line);
					logArray.put(logJson);
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return logArray;
	}

}

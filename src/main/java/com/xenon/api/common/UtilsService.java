package com.xenon.api.common;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * @author bhagyashri.ajmera
 * 
 *         This class includes util's related operations
 *
 */
public interface UtilsService {

	/**
	 * This method sends mail on exception
	 * 
	 * @param ex
	 * @param session
	 */
	public void sendMailonExecption(Exception ex, HttpSession session);

	public void sendMailonExecptionAPI(Exception ex, String apiName);

	/**
	 * This method returns blob image as string
	 * 
	 * @param image
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public String getImage(byte[] image);

	/**
	 * This method generates random string
	 * 
	 * @param strLen
	 * @return
	 */
	public String generateRandomString(int strLen);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method converts map to JSON
	 * 
	 * @param inputMap
	 * @return
	 * @throws Exception
	 */
	public String convertMapToJson(Map<String, Object> inputMap);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method converts list map to JSON
	 * 
	 * @param inputList
	 * @return
	 * @throws Exception
	 */

	public ArrayList<String> convertListOfMapToJson(List<Map<String, Object>> inputList);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method gives the password
	 * 
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public String getPassword(String password);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method checks whether session contains provided attribute or
	 *         not
	 * 
	 * @param attributeName
	 * @param session
	 * @return
	 */
	public boolean checkSessionContainsAttribute(String attributeName, HttpSession session);

	/**
	 * This method converts byte array to file
	 * 
	 * @param imageInByte
	 * @return
	 */
	public String convertByteArrayToFile(byte[] imageInByte);

	/**
	 * This method creates directory
	 * 
	 * @param dirName
	 * @return
	 */
	public boolean createDirectory(String dirName);

	/**
	 * 
	 * This method creates log file
	 * 
	 * @param filePath
	 * @return
	 */
	public boolean createLogfile(String filePath);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method gives customer end date
	 * 
	 * @param numberOfDays
	 * @return
	 */
	public String getCustomerEndDate(int numberOfDays);

	/**
	 * This method gives logger user
	 * 
	 * @param session
	 */
	public void getLoggerUser(HttpSession session);

	/**
	 * This method gives file data
	 * 
	 * @param data
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public String getFileData(byte[] data);

	/**
	 * This method converts string to plain text
	 * 
	 * @param userString
	 * @return
	 * @throws JSONException
	 */
	public JSONObject convertToPlainText(String[] userString);

	/**
	 * This method converts string to plain text
	 * 
	 * @param userString
	 * @return
	 * @throws JSONException
	 */
	public JSONObject convertToPlainTextForCustomer(String[] userString);

	/**
	 * This method removes duplicate
	 * 
	 * @param userList
	 * @return
	 */
	public List<Map<String, Object>> removeDuplicate(List<Map<String, Object>> userList);

	/**
	 * This method gives test count
	 * 
	 * @param test_list
	 * @param custom_list
	 * @param state
	 * @return
	 */
	public List<Map<String, Object>> getTestCount(List<Map<String, Object>> test_list,
			List<Map<String, Object>> custom_list, String state);

	/**
	 * This method prints stacktrace
	 * 
	 * @param ex
	 * @return
	 */
	public String printStacktrace(Exception ex);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method converts list map to JSON array
	 * 
	 * @param listMap
	 * @return
	 */
	public JSONArray convertListToJSONArray(List<Map<String, Object>> listMap);

	/**
	 * This method gives screenshot status from id
	 * 
	 * @param id
	 * @return
	 */
	public String getScreenShotStatusFromId(String id);

	/**
	 * This method gives report bug status from id
	 * 
	 * @param id
	 * @return
	 */
	public String getRerportBugStatusFromId(String id);

	public String createPieChart(int totalPassCount, int totalFailCount, int totalSkipCount, int totalBlockCount,
			int totalNotRunCount, String filePath);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method gives current month number
	 * 
	 * @return
	 */
	public int getCurrentMonthNumber();

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method gives date time zone
	 * 
	 * @param timezone
	 * @param dateInString
	 * @return
	 */
	public String getDateTimeOfTimezone(String timezone, String dateInString);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method gives current time zone
	 * 
	 * @param timezone
	 * @return
	 */
	public String getNowDateOfTimezone(String timezone);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method gives previous date of time zone
	 * 
	 * @param timezone
	 * @return
	 */
	public String getPrevDateOfTimezone(String timezone);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method returns current date time
	 * 
	 * @return
	 */
	public String getCurrentDate();

	public String getCurrentTimeStamp();

	/**
	 * This method gives date time formatted
	 * 
	 * @param inputString
	 * @return
	 * @throws ParseException
	 */
	public String getDate(String inputString);

	/**
	 * @author dhiraj.bendale
	 * @return
	 */
	public String getCurrentDate(String dateformat);

	/**
	 * @author dhiraj.bendale Change date format
	 * 
	 * @Date , OldFormat ,newdateFormat
	 */
	public String changeDateFormat(String dateStr, String oldDateFormat, String newDateFormat);

	/**
	 * @author prafulla.pol
	 * 
	 *         This method returns current date time
	 * 
	 * @return
	 */
	public String getCurrentDateTime();
}

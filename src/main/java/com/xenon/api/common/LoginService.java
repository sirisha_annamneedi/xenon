package com.xenon.api.common;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

/**
 * 
 * @author bhagyashri.ajmera
 * 
 *         This class handles all login related operations
 *
 */
public interface LoginService {

	/**
	 * 
	 * @author bhagyashri.ajmera
	 * 
	 *         This method authenticates user by user name and password
	 * 
	 * @param userName
	 * @param password
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public String authenticateUser(String userName, String password, HttpSession session);

	public List<Map<String, Object>> getUserCustomerValidDateByUsername(String username);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This reads the BLOB image as string
	 * 
	 * @param image
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public String getImage(byte[] image);

}

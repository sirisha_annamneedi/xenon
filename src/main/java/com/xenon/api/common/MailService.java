package com.xenon.api.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface MailService {

	/**
	 * @author
	 * @method postMail
	 * @param emailDetails
	 * @param recipients
	 * @param subject
	 * @param bodyText
	 * @param from
	 * @return
	 * @throws Exception
	 */
	public String postMail(List<Map<String, Object>> emailDetails, ArrayList<String> recipients, String subject,
			String bodyText, String from);
	
	/**
	 * @author
	 * @method postMail
	 * @param emailDetails
	 * @param recipients
	 * @param subject
	 * @param bodyText
	 * @param from
	 * @return
	 * @throws Exception
	 */
	public String postMail(ArrayList<String> recipients, String subject,
			String bodyText, String from);
	
	/**
	 * 
	 * @author bhagyashri.ajmera
	 * 
	 *         This method is used to send report mail with chart
	 * 
	 * @param emailDetails
	 * @param recipients
	 * @param subject
	 * @param bodyText
	 * @param from
	 * @param chartName
	 * @return
	 * @throws Exception
	 */
	public String postReportMail(List<Map<String, Object>> emailDetails, ArrayList<String> recipients, String subject,
			String bodyText, String from, String chartName,String filePath);
}

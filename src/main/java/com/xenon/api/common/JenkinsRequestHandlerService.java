package com.xenon.api.common;

import java.io.IOException;
import java.util.Map;

import org.json.JSONObject;

public interface JenkinsRequestHandlerService {

	JSONObject handleGetRequestWithJSONResponse(String url, String userName, String passwordOrToken) throws IOException;

	Map<String, String> handleGetRequest(String url, String userName, String passwordOrToken) throws IOException;

}

package com.xenon.api.common;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

/**
 * This Service is responsible for Role Access related operations.
 * <br><b>9th May 2017</b>
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface RoleAccessService {

	public boolean checkBMAccess(String url, List<String> bmAccess);
	
	public boolean checkBMAccess(String url, HttpSession session);
	
	public List<String> getBMAccessList(String schema, int userRole);
	
	public List<String> getTMAccessList(String schema, int userRole);
	
	public boolean checkTMAccess(String url, HttpSession session);
	
	public List<String> getBTAccessList(String schema, int userRole);
	
	public boolean checkBTAccess(String url, HttpSession session);
	
	public List<String> getAccessList(List<Map<String, Object>> rolesAccessList, String schema, int userRole);
	
	public boolean checkSupportAccess(String url, HttpSession session);
}

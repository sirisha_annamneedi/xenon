package com.xenon.api.common;

import com.xenon.buildmanager.dao.VMDetailsDAO;

/**
 * This Service is responsible for Server related operations. <br>
 * <b>5th May 2017</b>
 * 
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface ServerOperationService {

	/**
	 * @author navnath.damale This method is used to automation execution
	 * @param url
	 * @param exeJson
	 * @return
	 */
	public String autoExe(String serverUrl, String exeJson);

	/**
	 * @author navnath.damale This method is used to get server status
	 * @param url
	 * @return
	 */
	public String getServerStatus(String url);

	/**
	 * @author navnath.damale This method is used to check if directory exists
	 *         or not on client VM
	 * @param dirPath
	 * @return
	 */
	public String getDirectoryStatus(String dirPath);

	/**
	 * @author navnath.damale
	 * @param url
	 * @return
	 */
	public String checkVmStatus(String url);

	/**
	 * @author navnath.damale This method is used to get time stamp
	 * @return
	 */

	public String getTimeStamp();

	public String createpropertiesfile(String serverUrl, String exeJson);

	public void pollWSRequest(final int time, final String url, final String schema, final int userId,
			final int buildId, final VMDetailsDAO vmdetails, final String vmID);

}

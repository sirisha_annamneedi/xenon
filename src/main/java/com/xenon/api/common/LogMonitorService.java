package com.xenon.api.common;

import org.json.JSONArray;

/**
 * This Service is responsible for Log Monitor related operations.
 * <br><b>5th May 2017</b>
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface LogMonitorService {

	/**
	 * @author navnath.damale
	 * @description Method retrieves logs form given file path 
	 * @param logPath
	 * @return
	 */
	public JSONArray readLog(String logPath);
}

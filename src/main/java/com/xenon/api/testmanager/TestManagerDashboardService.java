package com.xenon.api.testmanager;

import java.util.Map;
import java.util.Properties;

import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.testmanager.dao.TmDashboardDAO;

/**
 * This Service is responsible for Test Manager Dashboard related operations.
 * <br><b>5th May 2017</b>
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface TestManagerDashboardService {

	public Map<String,Object> getDashboardData(String schema,int projectId,int userId,String userTimeZone,Object setAllProjectTM,String userFullName,String userProfilePhoto,String role,TmDashboardDAO tmDashDAO,ActivitiesDAO activitiesDAO,Properties colorProperties);
	
	public Map<String,Object> getTMAllAppDashboard(TmDashboardDAO tmDashDAO,int userId,String schema,Properties colorProperties,ActivitiesDAO activitiesDAO,
			String userTimeZone,Object userprofilePotho,String userFullName,String role);
}

package com.xenon.api.testmanager;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.springframework.web.multipart.MultipartFile;
/**
 * This Service is responsible for Test Case related operations.
 * <br><b>5th May 2017</b>
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface TestCaseService {
	
	/**
	 * @author suresh.adling
	 * @description insert a test case using input parameters
	 * @param projectId
	 * @param moduleId
	 * @param scenarioId
	 * @param testcaseName
	 * @param testcaseSummary
	 * @param testcasePrecondition
	 * @param executionType
	 * @param executionTime
	 * @param statusId
	 * @param authorId
	 * @param schema
	 * @param datasheetStatus 
	 * @return
	 * @throws Exception
	 */
	public int insertTestcase(String projectId, String moduleId, String scenarioId, String testcaseName,
			String testcaseSummary, String testcasePrecondition, String executionType, String executionTime,
			String statusId, int authorId,String scenarioName, String moduleName, String projectName, String schema, int datasheetStatus, 
			String redwoodTestCaseId);
	
	/**
	 * @author shantaram.tupe 
	 * @description
	 * 11-Mar-2020 12:00:31 PM
	 * @param redwoodTestCaseId
	 * @param schema
	 * @return
	 * Boolean
	 */
	Boolean isRedwoodTestCaseIdAlreadyMapped(String redwoodTestCaseId, String schema);
	
	/**
	 * @author suresh.adling
	 * @description update test case details details
	 * @param testcaseId
	 * @param testcaseName
	 * @param testcaseSummary
	 * @param testcasePrecondition
	 * @param executionType
	 * @param executionTime
	 * @param statusId
	 * @param updater
	 * @param schema
	 * @return
	 * @throws JSONException 
	 * @throws Exception
	 */
	public int updateTestcase(String testcaseId, String testcaseName, String testcaseSummary,
			String testcasePrecondition, String executionType, String executionTime, String statusId, int updater,
			String schema, String datasheetStatus, String redwoodTestCaseId);
	
	/**
	 * @author suresh.adling
	 * @description insert test case steps
	 * @param testcaseId
	 * @param testStep
	 * @param action
	 * @param result
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	public int insertSteps(String testcaseId, String testStep[], String action[], String result[],String testcaseName, String scenarioName,String moduleName, String projectName,int userId, String schema);
	/**
	 * @author abhay.thakur
	 * @description insert test case data
	 * @param testcaseId
	 * @param testData
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	public int insertData(String testcaseId,String testData, int userId, String schema);
	public int updateData(String testcaseId,String testData, int userId, String schema);
	
	/**
	 * @author suresh.adling
	 * @description update test case step details
	 * @param stepId
	 * @param testStepId
	 * @param action
	 * @param result
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	public int updateSteps(String stepId, String testStepId, String action, String result,int testCaseId,int userId, String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to insert test case comment to database
	 * 
	 * @param commentDescription
	 * @param commnetor
	 * @param testcaseId
	 * @param schema
	 * @return
	 */
	public int insertTestcaseComment(String commentDescription, int commnetor, int testcaseId, String schema,String currentDate);
	
	/**
	 * @author suresh.adling
	 * @description insert test case summary
	 * @param testCaseId
	 * @param description
	 * @param userId
	 * @param schema
	 * @return
	 */
	public int insertTestcaseSummary(int testCaseId, String description, int userId, String schema);
	
	public int createTestcase(String moduleName,String schema,String userEmailId,String projectName,String testcaseName,String testcaseSummary,String preCondition,
			   String executionType,String executionTime,String testCaseStatus,String scenarioName,int datasheetStatus);
	
	//public int updateTestcaseTest(String prefix,String schema,String testcaseName,String testcaseSummary,String preCondition,String executionType,  String executionTime,String status,String moduleName,String scenarioName,String projectName,String userEmailId);
	
	public int insertCommentForTestcase(String commentDescription,String userEmailId,String prefix,String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives datasheet by testcase id
	 * 
	 * @param testcaseId
	 * @param schema
	 * @return
	 */
	
	List<Map<String, Object>> getDatasheetByTcId(int testcaseId, String schema);
	
	List<Map<String, Object>> getDatasheetByBuildId(int buildId, String schema);


	int bulkInsertTestCase(String projectId, String moduleId, String scenarioId, int authorId, String scenarioName,
			String moduleName, String projectName, MultipartFile file, String schema) throws IOException;
	/**
	 * @author abhay.thakur
	 * @description insert test case data
	 * @param testcaseId
	 * @param testData
	 * @param schema
	 * @return
	 * @throws Exception
	 */
	int deleteTestCase(int testcaseId, String schema);

	int bulkInsertScenario(String projectId, String moduleId, String scenarioId, int authorId, String scenarioName,
			String moduleName, String projectName, MultipartFile file, String schema) throws IOException;
}

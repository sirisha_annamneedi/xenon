package com.xenon.api.testmanager;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import net.rcarz.jiraclient.Component;
import net.rcarz.jiraclient.Version;

/**
 * This Service is responsible for Scenario related operations.
 * <br><b>5th May 2017</b>
 * @author Dhananjay Deshmukh
 * @version 2.0
 */
public interface ScenarioService {

	/**
	 * @author suresh.adling
	 * @description insert a scenario using input parameters
	 * @param moduleId
	 * @param scenarioName
	 * @param scenarioDescription
	 * @param status
	 * @param schema
	 * @return
	 */
	public int insertScenario(String moduleId, String scenarioName, String scenarioDescription, String status,String projectName,String moduleName, int userId,
			String schema);
	
	/**
	 * @author suresh.adling
	 * @description update  scenario details 
	 * @param scenarioName
	 * @param scenarioDescription
	 * @param scenarioId
	 * @param schema
	 * @return
	 */
	public int updateScenario(String scenarioName, String scenarioDescription, String scenarioId,String projectName,String moduleName, int userId, String schema);
	
	/**
	 * @author shantaram.tupe
	 * 31-Oct-2019:10:06:46 AM
	 * @param scenarioStatus
	 * @param scenarioId
	 * @param userId
	 * @param schema
	 * @return
	 */
	int updateScenarioStatus( int scenarioStatus, int scenarioId, int userId, String schema);
	
	public int createScenarioTest(String moduleName,String schema,String scenarioName,String scenarioDesription,String Status,String projectName,String userEmailId);

	int bulkInsertScenario(String projectId, String moduleId, String moduleName, String projectName, MultipartFile file,
			int usedId, String schema, String status) throws Exception;

	int insertScenarios(String moduleId, String scenarioName, String scenarioDescription, String status,
			String projectName, String moduleName, int userId, String schema);

	int deleteScenario(int scenarioId, String schema);

	int insertComponent(List<Component> componentList, String scenarioName, String moduleId, int userId, String schema) throws Exception;

	int updateScenario(String scenarioName, String jiraStroyId, String jiraStory, String scenarioDescription, String scenarioId,
			String projectName, String moduleName, int userId, String schema);

	int insertScenario(String moduleId, String scenarioName, String jiraStoryId, String jiraStory,
			List<Version> versionList, String scenarioDescription, String status, String projectName, String moduleName,
			int userId, String schema);
	
	
	
	
}

package com.xenon.api.testmanager.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.xenon.api.common.UtilsService;
import com.xenon.api.testmanager.TestCaseService;
import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.constants.ExecutionType;
import com.xenon.testmanager.dao.ScenarioDAO;
import com.xenon.testmanager.dao.TestSpecificationDAO;
import com.xenon.testmanager.dao.TestcaseCommentsDAO;
import com.xenon.testmanager.dao.TestcaseDAO;
import com.xenon.testmanager.domain.TestCaseDetails;
import com.xenon.testmanager.domain.TestCaseSummary;
import com.xenon.testmanager.domain.TestDataDetails;
import com.xenon.testmanager.domain.TestStepDetails;
import com.xenon.testmanager.domain.TestcaseCommentDetails;
import com.xenon.util.service.ExcelOperations;

public class TestCaseServiceImpl implements TestCaseService {

	@Autowired
	TestcaseDAO testCaseDAO;

	@Autowired
	TestcaseCommentsDAO testCaseCommentsDAO;

	@Autowired
	ActivitiesDAO activitiesDAO;

	@Autowired
	UtilsService utilsService;

	@Autowired
	UserDAO userDAO;

	@Autowired
	ModuleDAO moduleDAO;

	@Autowired
	ProjectDAO projectDAO;

	@Autowired
	ScenarioDAO scenarioDAO;

	@Autowired
	TestSpecificationDAO testSpecificationDao;

	private static final Logger logger = LoggerFactory.getLogger(TestCaseServiceImpl.class);

	@Override
	public int insertTestcase(String projectId, String moduleId, String scenarioId, String testcaseName,
			String testcaseSummary, String testcasePrecondition, String executionType, String executionTime,
			String statusId, int authorId, String scenarioName, String moduleName, String projectName, String schema,
			int datasheetStatus, String redwoodTestCaseId) {
		logger.info("Test case API to insert test case details");
		TestCaseDetails testcase = new TestCaseDetails();
		testcase.setTestScenarioId(Integer.parseInt(scenarioId));
		testcase.setName(testcaseName);
		testcase.setSummary(testcaseSummary);
		testcase.setPrecondition(testcasePrecondition);
		testcase.setExecutionTypeId( ( redwoodTestCaseId == null || "".equals( redwoodTestCaseId ) ) ? Integer.parseInt(executionType) : ExecutionType.REDWOOD.getValue() );
		testcase.setExecutionTime(Integer.parseInt(executionTime));
		testcase.setStatusId(Integer.parseInt(statusId));
		testcase.setAuthorId(authorId);
		testcase.setDatasheetStatus(datasheetStatus);
		testcase.setRedwoodTestCaseId(("".equals( redwoodTestCaseId )) ? null : redwoodTestCaseId );

		logger.info("Set test case details to object of class TestCaseDetails");
		int flag = testCaseDAO.insertTestcase(Integer.parseInt(projectId), Integer.parseInt(moduleId), testcase,
				schema);
		int testCaseId = 0;
		if (flag == 1) {
			testCaseId = testCaseDAO.getTestCaseId(testcase, schema);
			if (testCaseId != 0) {
				/*
				 * RecentActivity recentActivity = activitiesDAO.setRecentActivitiesValues(2,
				 * testcaseName + "# " + scenarioName + "#" + moduleName + "#" + projectName,
				 * "TM", authorId, 1); activitiesDAO.inserNewActivity(recentActivity, schema);
				 */
				String description = "Test case is added ";
				insertTestcaseSummary(testCaseId, description, authorId, schema);
				logger.info(" User is redirected to testcase page ");
			}
		}
		return testCaseId;
	}

	@Override
	public Boolean isRedwoodTestCaseIdAlreadyMapped(String redwoodTestCaseId, String schema) {
		logger.info("TestCaseServiceImpl.isRedwoodTestCaseIdAlreadyMapped()");
		return this.testCaseDAO.isRedwoodTestCaseIdAlreadyMapped(redwoodTestCaseId, schema) != 0 ;
	}
	
	@Override
	public int bulkInsertTestCase(String projectId, String moduleId, String scenarioId, int authorId,
			String scenarioName, String moduleName, String projectName, MultipartFile file, String schema)
			throws IOException {

		StringBuffer testcaseIds = new StringBuffer();
		int testCaseId = 0;
		if (file != null) {
			//FileInputStream fileInputStream = null;
			InputStream fileInputStream = null;
			XSSFWorkbook workbook = null;
			XSSFSheet sheet = null;
			XSSFCell cell = null;
			if (!file.isEmpty()) {
				String[][] tabArray = null;
				int ci = 0, cj = 0, noOfRow = 0, noOfCols = 0;
				cell = null;
				//fileInputStream = (FileInputStream) file.getInputStream();
				fileInputStream = file.getInputStream();
				workbook = new XSSFWorkbook(fileInputStream);
				sheet = workbook.getSheetAt(0);
				noOfRow = sheet.getLastRowNum();
				noOfCols = workbook.getSheetAt(0).getRow(0).getLastCellNum();
				tabArray = new String[noOfRow][noOfCols];
				int celcounter = 0;
				cj = 0;
				boolean testcaseCreated = false;

				int totCol = sheet.getRow(0).getLastCellNum();
				for (ci = 0; ci < noOfRow; ci++) {
					cj = celcounter;
					String testcaseName = null;
					String testcaseSummary = null;
					String testcasePrecondition = null;
					List<String> testStep = new ArrayList<String>();
					List<String> action = new ArrayList<String>();
					List<String> result = new ArrayList<String>();
					for (int c = 0; c < totCol; c++) {
						cell = sheet.getRow(ci + 1).getCell(c, Row.RETURN_BLANK_AS_NULL);
						tabArray[ci][cj] = new ExcelOperations().getCellValueAsString(cell);

						cj++;
					}
					if (tabArray[ci][1] != null) {
						testcaseName = tabArray[ci][1];
						testcaseCreated = false;
					}
					if (tabArray[ci][2] != null)
						testcaseSummary = tabArray[ci][2];
					if (tabArray[ci][3] != null)
						testcasePrecondition = tabArray[ci][3];
					if (tabArray[ci][4] != null && tabArray[ci][5] != null && tabArray[ci][6] != null
							&& !testStep.contains(tabArray[ci][4])) {
						testStep.add(tabArray[ci][4]);
						action.add(tabArray[ci][5]);
						result.add(tabArray[ci][6]);
					}

					if (!testcaseCreated && !testcaseName.isEmpty()) {
						//RedwoodTestcaseId null
						testCaseId = insertTestcase(projectId, moduleId, scenarioId, testcaseName, testcaseSummary,
								testcasePrecondition, "3", "0", "1", authorId, scenarioName, moduleName, projectName,
								schema, 2, null);
						testcaseCreated = true;
						testcaseIds.append(String.valueOf(testCaseId)).append(",");
					}
					try {
						if (testCaseId != 0) {
							insertSteps(String.valueOf(testCaseId), testStep.toArray(new String[testStep.size()]),
									action.toArray(new String[action.size()]),
									result.toArray(new String[result.size()]), testcaseName, scenarioName, moduleName,
									projectName, authorId, schema);
						}
					} catch (NumberFormatException exception) {

					}
				}
				fileInputStream.close();
			}
		}
		if (testCaseId > 0)
			return 1;
		else
			return 0;
	}

	@Override
	public int bulkInsertScenario(String projectId, String moduleId, String scenarioId, int authorId,
			String scenarioName, String moduleName, String projectName, MultipartFile file, String schema)
			throws IOException {

		StringBuffer testcaseIds = new StringBuffer();
		int testCaseId = 0;
		if (file != null) {
			FileInputStream fileInputStream = null;
			XSSFWorkbook workbook = null;
			XSSFSheet sheet = null;
			XSSFCell cell = null;
			if (!file.isEmpty()) {
				String[][] tabArray = null;
				int ci = 0, cj = 0, noOfRow = 0, noOfCols = 0;
				cell = null;
				fileInputStream = (FileInputStream) file.getInputStream();
				workbook = new XSSFWorkbook(fileInputStream);
				sheet = workbook.getSheet(scenarioName);
				noOfRow = sheet.getLastRowNum();
				noOfCols = workbook.getSheet(scenarioName).getRow(0).getLastCellNum();
				tabArray = new String[noOfRow][noOfCols];
				int celcounter = 0;
				cj = 0;
				boolean testcaseCreated = false;

				int totCol = sheet.getRow(0).getLastCellNum();
				for (ci = 0; ci < noOfRow; ci++) {
					cj = celcounter;
					String testcaseName = null;
					String testcaseSummary = null;
					String testcasePrecondition = null;
					List<String> testStep = new ArrayList<String>();
					List<String> action = new ArrayList<String>();
					List<String> result = new ArrayList<String>();
					for (int c = 0; c < totCol; c++) {
						cell = sheet.getRow(ci + 1).getCell(c, Row.RETURN_BLANK_AS_NULL);
						tabArray[ci][cj] = new ExcelOperations().getCellValueAsString(cell);

						cj++;
					}
					if (tabArray[ci][1] != null) {
						testcaseName = tabArray[ci][1];
						testcaseCreated = false;
					}
					if (tabArray[ci][2] != null)
						testcaseSummary = tabArray[ci][2];
					if (tabArray[ci][3] != null)
						testcasePrecondition = tabArray[ci][3];
					if (tabArray[ci][4] != null && tabArray[ci][5] != null && tabArray[ci][6] != null
							&& !testStep.contains(tabArray[ci][4])) {
						testStep.add(tabArray[ci][4]);
						action.add(tabArray[ci][5]);
						result.add(tabArray[ci][6]);
					}

					if (!testcaseCreated && !testcaseName.isEmpty()) {
						//RedwoodTestcaseId null
						testCaseId = insertTestcase(projectId, moduleId, scenarioId, testcaseName, testcaseSummary,
								testcasePrecondition, "3", "0", "1", authorId, scenarioName, moduleName, projectName,
								schema, 2, null);
						testcaseCreated = true;
						testcaseIds.append(String.valueOf(testCaseId)).append(",");
					}
					try {
						if (testCaseId != 0) {
							insertSteps(String.valueOf(testCaseId), testStep.toArray(new String[testStep.size()]),
									action.toArray(new String[action.size()]),
									result.toArray(new String[result.size()]), testcaseName, scenarioName, moduleName,
									projectName, authorId, schema);
						}
					} catch (NumberFormatException exception) {

					}
				}
				fileInputStream.close();
			}
		}
		if (testCaseId > 0)
			return 1;
		else
			return 0;
	}

	@Override
	public int updateTestcase(String testcaseId, String testcaseName, String testcaseSummary,
			String testcasePrecondition, String executionType, String executionTime, String statusId, int updater,
			String schema, String datasheetStatus, String redwoodTestCaseId) {
		logger.info("Test case API to insert test case details");
		String[] inputStrings = { testcaseName };
		JSONObject plainText = utilsService.convertToPlainText(inputStrings);
		testcaseName = plainText.get(testcaseName).toString();
		TestCaseDetails testcase = new TestCaseDetails();
		testcase.setId(Integer.parseInt(testcaseId));
		testcase.setName(testcaseName);
		testcase.setSummary(testcaseSummary);
		testcase.setPrecondition(testcasePrecondition);
		testcase.setExecutionTypeId( ( redwoodTestCaseId == null || "".equals( redwoodTestCaseId ) ) ? Integer.parseInt(executionType) : ExecutionType.REDWOOD.getValue() );
		testcase.setExecutionTime(Integer.parseInt(executionTime));
		testcase.setStatusId(Integer.parseInt(statusId));
		testcase.setUpdaterId(updater);
		testcase.setDatasheetStatus(Integer.parseInt("" + datasheetStatus));
		testcase.setRedwoodTestCaseId(("".equals( redwoodTestCaseId )) ? null : redwoodTestCaseId );
		logger.info("Set test case details to object of class TestCaseDetails");
		Map<String, Object> testcaseDetails = testCaseDAO.updateTestcaseDetails(testcase, schema);
		int flag = Integer.parseInt(testcaseDetails.get("#update-count-1").toString());
		if (flag == 1) {
			String description = "Test case is updated ";
			insertTestcaseSummary(Integer.parseInt(testcaseId), description, updater, schema);
		}
		return flag;
	}

	@Override
	public int insertSteps(String testcaseId, String[] testStep, String[] action, String[] result, String testcaseName,
			String scenarioName, String moduleName, String projectName, int userId, String schema) {
		logger.info("Test case  API to insert test case steps");
		int flag = 1;
		for (int count = 0; count < action.length; count++) {
			TestStepDetails step = new TestStepDetails();
			step.setTestcaseId(Integer.parseInt(testcaseId));
			step.setStepAction(action[count]);
			step.setExpectedResult(result[count]);
			step.setTestStepId(Integer.parseInt(testStep[count]));
			flag = testCaseDAO.insertStep(step, schema);

			if (flag == 1) {
				/*
				 * RecentActivity recentActivity = activitiesDAO.setRecentActivitiesValues(4,
				 * testcaseName + "#" + scenarioName + "# " + moduleName + "#" + projectName,
				 * "TM", userId, 1); activitiesDAO.inserNewActivity(recentActivity, schema);
				 */
				String description = "Step is added ";
				insertTestcaseSummary(Integer.parseInt(testcaseId), description, userId, schema);
				logger.info(" User is redirected to teststeps page ");
			}
			if (flag == 0) {
				return flag;
			}
		}
		return flag;
	}

	@Override
	public int insertData(String testcaseId, String testData, int userId, String schema) {
		logger.info("Test data  API to insert test case data");
		int flag = 1;
		TestDataDetails data = new TestDataDetails();
		data.setTestcaseId(Integer.parseInt(testcaseId));
		data.setUserId(userId);
		data.setData(testData);
		flag = testCaseDAO.insertData(data, schema);

		return flag;
	}

	@Override
	public int updateData(String testcaseId, String testData, int userId, String schema) {
		logger.info("Test data  API to update test case data");
		int flag = 1;
		TestDataDetails data = new TestDataDetails();
		data.setTestcaseId(Integer.parseInt(testcaseId));
		data.setUserId(userId);
		data.setData(testData);
		flag = testCaseDAO.updateData(data, schema);

		return flag;
	}

	@Override
	public int updateSteps(String stepId, String testStepId, String action, String result, int testCaseId, int userId,
			String schema) {
		logger.info("Test case  API to update test case step details");
		TestStepDetails step = new TestStepDetails();
		step.setStepId(Integer.parseInt(stepId));
		step.setStepAction(action);
		step.setExpectedResult(result);
		step.setTestStepId(Integer.parseInt(testStepId));
		int flag = testCaseDAO.updateStep(step, schema);
		if (flag == 1) {
			String description = "Step is updated";
			insertTestcaseSummary(testCaseId, description, userId, schema);
			logger.info(" User is redirected to teststeps page ");
		}
		if (flag == 0) {
			return flag;
		}

		return flag;
	}

	@Override
	public int insertTestcaseComment(String commentDescription, int commnetor, int testcaseId, String schema,
			String currentDate) {
		TestcaseCommentDetails testcaseCommentDetails = new TestcaseCommentDetails();
		testcaseCommentDetails.setCommentDescription(commentDescription);
		testcaseCommentDetails.setCommentor(commnetor);
		testcaseCommentDetails.setTestcaseId(testcaseId);
		testcaseCommentDetails.setCommentTime(currentDate);

		Map<String, Object> testcaseComment = testCaseCommentsDAO.insertTestcaseComment(testcaseCommentDetails, schema);
		String description = "Comment is added <br> " + commentDescription;
		insertTestcaseSummary(testcaseId, description, commnetor, schema);
		return Integer.parseInt(testcaseComment.get("#update-count-1").toString());
	}

	@Override
	public int insertTestcaseSummary(int testCaseId, String description, int userId, String schema) {
		logger.info("Test case API to insert test case summary");
		TestCaseSummary summary = new TestCaseSummary();
		summary.setTestCaseId(testCaseId);
		summary.setActivityDescription(description);
		summary.setUser(userId);
		int flag = testCaseCommentsDAO.insertTestcaseSummary(schema, summary);
		return flag;
	}

	@Override
	public int createTestcase(String moduleName, String schema, String userEmailId, String projectName,
			String testcaseName, String testcaseSummary, String preCondition, String executionType,
			String executionTime, String testCaseStatus, String scenarioName, int datasheetStatus) {
		List<Map<String, Object>> moduleDetails = moduleDAO.getModuleDetailsByName(moduleName, schema);
		String moduleID = moduleDetails.get(0).get("module_id").toString();
		List<Map<String, Object>> assignUserDetails = userDAO.getUserDetailsByEmailId(userEmailId);
		int userId = Integer.parseInt(assignUserDetails.get(0).get("user_id").toString());
		int projectId = projectDAO.getProjectID(projectName, schema);
		int scenarioId = scenarioDAO.getScenarioId(scenarioName, Integer.parseInt(moduleID), schema);

		//RedwoodTestcaseId null
		int result = insertTestcase(Integer.toString(projectId), moduleID, Integer.toString(scenarioId), testcaseName,
				testcaseSummary, preCondition, executionType, executionTime, testCaseStatus, userId, scenarioName,
				moduleName, projectName, schema, datasheetStatus, null);
		return result;
	}

	/*
	 * @Override public int updateTestcaseTest(String prefix,String schema,String
	 * testcaseName,String testcaseSummary,String preCondition,String executionType,
	 * String executionTime,String status,String moduleName,String
	 * scenarioName,String projectName,String userEmailId){ List<Map<String,Object>>
	 * testcaseDeatails=testCaseDAO.getTestcaseDetailsByPrefix(prefix,schema);
	 * String testCaseID=testcaseDeatails.get(0).get("testcase_id").toString();
	 * List<Map<String,Object>> assignUserDetails
	 * =userDAO.getUserDetailsByEmailId(userEmailId); int
	 * userId=Integer.parseInt(assignUserDetails.get(0).get("user_id").toString());
	 * int flag=updateTestcase(testCaseID,testcaseName,testcaseSummary,preCondition,
	 * executionType,executionTime,status,userId,schema); return flag; }
	 */
	@Override
	public List<Map<String, Object>> getDatasheetByBuildId(int buildId, String schema) {
		return testSpecificationDao.getDatasheetByBuildId(buildId, schema);
	}

	@Override
	public int insertCommentForTestcase(String commentDescription, String userEmailId, String prefix, String schema) {
		List<Map<String, Object>> assignUserDetails = userDAO.getUserDetailsByEmailId(userEmailId);
		int userId = Integer.parseInt(assignUserDetails.get(0).get("user_id").toString());
		List<Map<String, Object>> testcaseDeatails = testCaseDAO.getTestcaseDetailsByPrefix(prefix, schema);
		int testCaseID = Integer.parseInt(testcaseDeatails.get(0).get("testcase_id").toString());
		int flag = insertTestcaseComment(commentDescription, userId, testCaseID, schema,
				utilsService.getCurrentDateTime());
		return flag;
	}

	@Override
	public List<Map<String, Object>> getDatasheetByTcId(int testcaseId, String schema) {
		return testSpecificationDao.getDatasheetByTcId(testcaseId, schema);
	}

	@Override
	public int deleteTestCase(int testcaseId, String schema) {
		int flag = testCaseDAO.deleteTestcase(testcaseId, schema);
		return flag;
	}

}

package com.xenon.api.testmanager.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.xenon.api.common.UtilsService;
import com.xenon.api.testmanager.ScenarioService;
import com.xenon.api.testmanager.TestCaseService;
import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.buildmanager.domain.RecentActivity;
import com.xenon.testmanager.dao.ScenarioDAO;
import com.xenon.testmanager.domain.ScenarioDetails;
import com.xenon.testmanager.domain.ComponentDetails;
import com.xenon.testmanager.domain.TestCaseDetails;
import com.xenon.util.service.ExcelOperations;

import net.rcarz.jiraclient.Component;
import net.rcarz.jiraclient.Version;



public class ScenarioServiceImpl implements ScenarioService{

	@Autowired
	ScenarioDAO scenarioDAO;
	
	@Autowired
	ActivitiesDAO activitiesDAO;

	@Autowired
	TestCaseService testCaseService;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	ModuleDAO moduleDAO;
	
	@Autowired
	UserDAO userDAO;

	
	private static final Logger logger = LoggerFactory.getLogger(ScenarioServiceImpl.class);
	
	@Override
	public int insertScenario(String moduleId, String scenarioName, String jiraStoryId, String jiraStory, List<Version> versionList, String scenarioDescription, String status,
			String projectName, String moduleName, int userId, String schema) {
		logger.info("Scenario API to insert scenario details");
		String[] inputStrings = { scenarioName};
		//JSONObject plainText = utilsService.convertToPlainText(inputStrings);
		scenarioName = inputStrings[0];
		ScenarioDetails scenario = new ScenarioDetails();
		logger.info("Set scenario details to object of class ScenarioDetails");
		scenario.setScenarioName(scenarioName);
		scenario.setJiraStoryId(jiraStoryId);
		scenario.setJiraStory(jiraStory);
		if(versionList != null)
			scenario.setFixVersion(versionList);  
		else
			scenario.setFixVersion(null);
		scenario.setScenarioDescription(scenarioDescription);
		scenario.setModuleId(Integer.parseInt(moduleId));
		scenario.setStatus(Integer.parseInt(status));
		int flag = 0;
		try {
			flag = scenarioDAO.insertJiraStory(scenario, schema);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (flag == 1) {
			logger.info("scenario is added successfully");
			/*RecentActivity recentActivity = activitiesDAO.setRecentActivitiesValues(1,
					scenarioName + "#" + moduleName + "#" + projectName, "TM", userId, 1);
			activitiesDAO.inserNewActivity(recentActivity, schema);*/
			logger.info(" User is redirected to testspecification page ");
		}
		return flag;
	}
	
	@Override
	public int insertScenarios(String moduleId, String scenarioName, String scenarioDescription, String status,
			String projectName, String moduleName, int userId, String schema) {
		logger.info("Scenario API to insert scenario details");
		String[] inputStrings = { scenarioName};
		//JSONObject plainText = utilsService.convertToPlainText(inputStrings);
		scenarioName = inputStrings[0];
		ScenarioDetails scenario = new ScenarioDetails();
		logger.info("Set scenario details to object of class ScenarioDetails");
		scenario.setScenarioName(scenarioName);
		scenario.setScenarioDescription(scenarioDescription);
		scenario.setModuleId(Integer.parseInt(moduleId));
		scenario.setStatus(Integer.parseInt(status));
		logger.info("Set test case details to object of class TestCaseDetails");
		int flag = 0;
		try {
			flag = scenarioDAO.insertScenario(scenario, schema);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int scenarioId = 0;
		if (flag == 1) {
			scenarioId = scenarioDAO.getScenarioIds(scenario, schema);
			if (scenarioId != 0) {
				logger.info("scenario is added successfully");
				/*RecentActivity recentActivity = activitiesDAO.setRecentActivitiesValues(1,
						scenarioName + "#" + moduleName + "#" + projectName, "TM", userId, 1);
				activitiesDAO.inserNewActivity(recentActivity, schema);*/
			}
		}
		return scenarioId;
	}

	@Override
	public int updateScenario(String scenarioName, String scenarioDescription, String scenarioId, String projectName,
			String moduleName, int userId, String schema) {
		logger.info("Scenario API to update scenario details");
		String[] inputStrings = {scenarioName};
		JSONObject plainText = utilsService.convertToPlainText(inputStrings);
		scenarioName = plainText.get(scenarioName).toString();
		ScenarioDetails scenario = new ScenarioDetails();
		logger.info("Set scenario details to object of class ScenarioDetails");
		scenario.setScenarioId(Integer.parseInt(scenarioId));
		scenario.setScenarioName(scenarioName);
		scenario.setScenarioDescription(scenarioDescription);
		int flag = scenarioDAO.updateScenario(scenario, schema);
		
		if (flag == 1) {
			logger.info("scenario is updated successfully");
			/*RecentActivity recentActivity = activitiesDAO.setRecentActivitiesValues(3,
					scenarioName + "#" + moduleName + "#" + projectName, "TM", userId, 1);
			activitiesDAO.inserNewActivity(recentActivity, schema);*/
			logger.info(" User is redirected to testspecification page ");
		}
		return flag;
	}
	
	@Override
	public int updateScenarioStatus(int scenarioStatus, int scenarioId, int userId, String schema) {
		logger.info("ScenarioServiceImpl.updateScenarioStatus()");
		int flag = 0;
		try {
			flag = scenarioDAO.updateScenarioStatus(scenarioStatus, scenarioId, schema);
			if (flag == 1) {
				logger.info("scenario status is updated successfully");
				/*RecentActivity recentActivity = activitiesDAO.setRecentActivitiesValues(3,
						scenarioName + "#" + moduleName + "#" + projectName, "TM", userId, 1);
				activitiesDAO.inserNewActivity(recentActivity, schema);*/
				logger.info(" User is redirected to testcase page ");
				flag = 200;
			} else if (flag == 0) {
				logger.info("scenario status is not updated successfully.");
				flag = 404;
			}
		} catch (Exception e) {
			flag = 500;
		}
		return flag;
	}
	
	@Override
	public int updateScenario(String scenarioName, String jiraStoryId, String jiraStory, String scenarioDescription, String scenarioId, String projectName,
			String moduleName, int userId, String schema) {
		logger.info("Scenario API to update scenario details");
		String[] inputStrings = {scenarioName};
		JSONObject plainText = utilsService.convertToPlainText(inputStrings);
		scenarioName = plainText.get(scenarioName).toString();
		ScenarioDetails scenario = new ScenarioDetails();
		logger.info("Set scenario details to object of class ScenarioDetails");
		scenario.setJiraStoryId(jiraStoryId);
		scenario.setJiraStory(jiraStory);
		scenario.setScenarioId(Integer.parseInt(scenarioId));
		scenario.setScenarioName(scenarioName);
		scenario.setScenarioDescription(scenarioDescription);
		int flag = scenarioDAO.updateJiraScenario(scenario, schema);
		
		if (flag == 1) {
			logger.info("scenario is updated successfully");
			/*RecentActivity recentActivity = activitiesDAO.setRecentActivitiesValues(3,
					scenarioName + "#" + moduleName + "#" + projectName, "TM", userId, 1);
			activitiesDAO.inserNewActivity(recentActivity, schema);*/
			logger.info(" User is redirected to testspecification page ");
		}
		return flag;
	}
	
	public int createScenarioTest(String moduleName,String schema,String scenarioName,String scenarioDesription,String Status,String projectName,String userEmailId){
		List<Map<String,Object>>moduleDetails=moduleDAO.getModuleDetailsByName(moduleName,schema);
		String moduleID=moduleDetails.get(0).get("module_id").toString();
		List<Map<String,Object>> assignUserDetails =userDAO.getUserDetailsByEmailId(userEmailId);
		int userId=Integer.parseInt(assignUserDetails.get(0).get("user_id").toString());
		int flag=insertScenario(moduleID,scenarioName,scenarioDesription,Status,projectName,moduleName,userId,schema);
		return flag;
	}
	
/*	public int updateScenarioTest(String newScenarioName,String currentScenarioName,String moduleName,String moduleDescription,String schema,String userEmailID,String projectName){
		List<Map<String,Object>>moduleDetails=moduleDAO.getModuleDetailsByName(moduleName,schema);
		String moduleID=moduleDetails.get(0).get("module_id").toString();
		List<Map<String,Object>> assignUserDetails =userDAO.getUserDetailsByEmailId(userEmailID);
		int userId=Integer.parseInt(assignUserDetails.get(0).get("user_id").toString());
		int result=scenarioDAO.getScenarioId(currentScenarioName,Integer.parseInt(moduleID),schema);
		String scenarioId=Integer.toString(result);
		int flag=updateScenario(newScenarioName,moduleDescription,scenarioId,projectName,moduleName,userId,schema);
		return flag;
	}*/
	
	@Override
	public int bulkInsertScenario(String projectId, String moduleId, String moduleName, String projectName,
			MultipartFile file, int usedId, String schema, String status) throws Exception {
		
		
		StringBuffer scenarioIds = new StringBuffer();
		String scenarioId = null;
		if( file != null) {
			FileInputStream fileInputStream = null;
			XSSFWorkbook workbook = null;
			XSSFSheet sheet = null;
			XSSFCell cell = null;
			if (!file.isEmpty()) {
				String[][] tabArray = null;
				int ci = 0, cj = 0, noOfRow = 0, noOfCols = 0;
				fileInputStream = (FileInputStream)file.getInputStream();
				workbook = new XSSFWorkbook(fileInputStream);
				String scenarioName = null;
				String sheetName = null;
				sheet = workbook.getSheetAt(0);
				noOfRow = sheet.getLastRowNum();
				noOfCols = workbook.getSheetAt(0).getRow(0).getLastCellNum();
				tabArray = new String[noOfRow][noOfCols];
				int celcounter = 0;
				int totCol = sheet.getRow(0).getLastCellNum();
				for (ci = 0; ci < noOfRow; ci++) {
					cj = celcounter;
					for (int c = 0; c < totCol; c++) {
						cell = sheet.getRow(ci + 1).getCell(c, Row.RETURN_BLANK_AS_NULL);
						tabArray[ci][cj] = new ExcelOperations().getCellValueAsString(cell);
						cj++;
					}
					if (tabArray[ci][0] != null)
						sheetName = tabArray[ci][0];
					if (tabArray[ci][1] != null)
						scenarioName = tabArray[ci][1];
					
				if (!scenarioName.isEmpty()) {
					String scenarioDescription = "";
					int noOfSheets = workbook.getNumberOfSheets();
					for(int sheetIndex=1;sheetIndex<noOfSheets;sheetIndex++)
						if(workbook.getSheetName(sheetIndex).equals(sheetName)) {
							scenarioId = String.valueOf(insertScenarios(moduleId, scenarioName, scenarioDescription, status, projectName, moduleName, usedId, schema));
							scenarioIds.append(String.valueOf(scenarioId)).append(",");
							if(!scenarioId.isEmpty())
							testCaseService.bulkInsertScenario(projectId, moduleId, scenarioId, usedId, sheetName, moduleName, projectName, file, schema);
			
				}
				}
		}
				fileInputStream.close();
}
		
	}
		if(scenarioIds.length() > 0)
			return 1;
		else 
			return 0;

	}
	
	@Override
	public int deleteScenario(int scenarioId, String schema) {
		int flag = scenarioDAO.deleteScenario(scenarioId, schema);
		if (flag != 0) {
			return 200;
		} else {
			return 500;
		}
	}
	

	@Override
	public int insertScenario(String moduleId, String scenarioName, String scenarioDescription, String status,
			String projectName, String moduleName, int userId, String schema) {
		logger.info("Scenario API to insert scenario details");
		String[] inputStrings = { scenarioName};
		//JSONObject plainText = utilsService.convertToPlainText(inputStrings);
		scenarioName = inputStrings[0];
		ScenarioDetails scenario = new ScenarioDetails();
		logger.info("Set scenario details to object of class ScenarioDetails");
		scenario.setScenarioName(scenarioName);
		scenario.setScenarioDescription(scenarioDescription);
		scenario.setModuleId(Integer.parseInt(moduleId));
		scenario.setStatus(Integer.parseInt(status));
		int flag = 0;
		try {
			flag = scenarioDAO.insertScenario(scenario, schema);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (flag == 1) {
			logger.info("scenario is added successfully");
			/*RecentActivity recentActivity = activitiesDAO.setRecentActivitiesValues(1,
					scenarioName + "#" + moduleName + "#" + projectName, "TM", userId, 1);
			activitiesDAO.inserNewActivity(recentActivity, schema);*/
			logger.info(" User is redirected to testspecification page ");
		}
		return flag;
		
	}
	
	@Override
	public int insertComponent(List<Component> componentList, String scenarioName, String moduleId, int userId, String schema) throws Exception {
		logger.info("Scenario API to insert component details");
		int active = 1;
		String componentDescription = "success";
		int flag = 0;
		int scenarioId = scenarioDAO.getScenarioId(scenarioName, Integer.parseInt(moduleId), schema);
		ComponentDetails com = null;
		for(Component componentName : componentList) {
			com = new ComponentDetails();
			com.setComponentName(componentName);
			com.setComponentDescription(componentDescription);
			com.setComponentStatus(active);
			flag = scenarioDAO.insertComponent(com, schema);
			if(flag == 1) {
				int componentID = scenarioDAO.getComponentId(componentName, schema);
				scenarioDAO.insertScenarioComponent(componentID, scenarioId, schema);
			}
		}
		logger.info(" User is redirected to testspecification page ");
		return flag;
	}
	
}
		
package com.xenon.api.testmanager.impl;

import java.io.UnsupportedEncodingException;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.common.LoginService;
import com.xenon.api.common.UtilsService;
import com.xenon.api.testmanager.TestManagerDashboardService;
import com.xenon.buildmanager.dao.ActivitiesDAO;
import com.xenon.testmanager.dao.TmDashboardDAO;

public class TestManagerDashboardServiceImpl implements TestManagerDashboardService {


	private static final Logger logger = LoggerFactory.getLogger(TestManagerDashboardServiceImpl.class);
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	UtilsService utilsService;
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getDashboardData(String schema, int projectId, int userId, String userTimeZone,
			Object setAllProjectTM, String userFullName, String userProfilePhoto, String role, TmDashboardDAO tmDashDAO,
			ActivitiesDAO activitiesDAO, Properties colorProperties) {
		List<Map<String, Object>> allCounts = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> moduleTestCaseCounts = new ArrayList<Map<String, Object>>();
		Map<String, Object> allCountsTemp = new HashMap<String, Object>();
		Map<String,Object> modelData = new HashMap<String,Object>();
		logger.info("Session is active, userId is " + userId + " and projectId is " + projectId
				+ " Customer Schema " + schema);
		Map<String, Object> resultSet = tmDashDAO.getTmProjectDashboard(projectId, userId, schema);
		List<Map<String, Object>> tmDashboardCount = (List<Map<String, Object>>) resultSet.get("#result-set-1");
		List<Map<String, Object>> projectwiseCount = (List<Map<String, Object>>) resultSet.get("#result-set-2");
		List<Map<String, Object>> project = (List<Map<String, Object>>) resultSet.get("#result-set-3");
		List<Map<String, Object>> modules = (List<Map<String, Object>>) resultSet.get("#result-set-4");
		List<Map<String, Object>> testScenario = (List<Map<String, Object>>) resultSet.get("#result-set-5");
		List<Map<String, Object>> testcase = (List<Map<String, Object>>) resultSet.get("#result-set-6");
		List<Map<String, Object>> testcasebugs = (List<Map<String, Object>>) resultSet.get("#result-set-7");
	
		//for getting test cases status
		List<Map<String, Object>> testcaseDraft = (List<Map<String, Object>>) resultSet.get("#result-set-11");
		List<Map<String, Object>> testcaseReadyForReview = (List<Map<String, Object>>) resultSet.get("#result-set-12");
		List<Map<String, Object>> testcaseReviewInProgress = (List<Map<String, Object>>) resultSet.get("#result-set-13");
		List<Map<String, Object>> testcaseReviewCompleted = (List<Map<String, Object>>) resultSet.get("#result-set-14");
		List<Map<String, Object>> testcaseFinal = (List<Map<String, Object>>) resultSet.get("#result-set-15");
		//for getting module name and id
		List<Map<String, Object>> moduleName = (List<Map<String, Object>>) resultSet.get("#result-set-16");
		
		// for getting count scenario
		List<Map<String, Object>> moduleScenarioCount = (List<Map<String, Object>>) resultSet.get("#result-set-17");
		List<Map<String, Object>> ComponentName = (List<Map<String, Object>>) resultSet.get("#result-set-18");
		List<Map<String, Object>> ComponentSceCount = (List<Map<String, Object>>) resultSet.get("#result-set-19");
		List<Map<String, Object>> CompSceID = (List<Map<String, Object>>) resultSet.get("#result-set-20");
		List<Map<String, Object>> AMTestCase = (List<Map<String, Object>>) resultSet.get("#result-set-21");
		List<Map<String, Object>> totalTestCountByExecutionType = (List<Map<String, Object>>) resultSet.get("#result-set-22");
		
		List<Map<String, Object>> donutChartData = (List<Map<String, Object>>) resultSet.get("#result-set-8");
		String moduleColors[] = {colorProperties.getProperty("tmDashbaord.module0"), colorProperties.getProperty("tmDashbaord.module1"),
				colorProperties.getProperty("tmDashbaord.module2"), colorProperties.getProperty("tmDashbaord.module3"),
				colorProperties.getProperty("tmDashbaord.module4"), colorProperties.getProperty("tmDashbaord.module5"),
				colorProperties.getProperty("tmDashbaord.module6"), colorProperties.getProperty("tmDashbaord.module7"),
				colorProperties.getProperty("tmDashbaord.module8"), colorProperties.getProperty("tmDashbaord.module9")
				};
		for (int i = 0; i < donutChartData.size(); i++) {
			donutChartData.get(i).put("color", moduleColors[i%10]);
		}
		modelData.put("donutChartData", utilsService.convertListOfMapToJson(donutChartData));
		
		List<Map<String, Object>> moduleDetails = (List<Map<String, Object>>) resultSet.get("#result-set-9");
		for (int i = 0; i < moduleDetails.size(); i++) {
			moduleDetails.get(i).put("color", moduleColors[i%10]);
		}
		
		modelData.put("moduleDetails", utilsService.convertListOfMapToJson(moduleDetails));
		modelData.put("modules", moduleDetails);
		
		List<Map<String, Object>> moduleWiseCount = (List<Map<String, Object>>) resultSet.get("#result-set-10");
		for (int i = 0; i < moduleWiseCount.size(); i++) {
			moduleWiseCount.get(i).put("monthName",  new DateFormatSymbols().getMonths()[Integer.parseInt(moduleWiseCount.get(i).get("month").toString())-1]);
		}
		List<Map<String, Object>> moduleWiseCountTemp = new ArrayList<Map<String,Object>>();
		int month=utilsService.getCurrentMonthNumber()+1;
		int startMonth=month+1;
		ArrayList<String> monthList=new ArrayList<String>();
		int currentYear=Calendar.getInstance().get(Calendar.YEAR);
		boolean monthFlag=false;
		for(int i=0;i<12;i++)
		{
			if(startMonth>12)
			{
				startMonth=1;
				currentYear=currentYear+1;
			}
			
			if(!monthFlag){
				currentYear=currentYear-1;
				monthFlag=true;
			}
			
			monthList.add("\""+(new DateFormatSymbols().getMonths()[startMonth-1]).substring(0, 3)+" "+currentYear+"\"");
			startMonth++;
			
		}
		modelData.put("currentMonth", month);
		boolean breakFlag=false;
		
		for (int i = 0; i < moduleWiseCount.size(); i++) {
			int moduleMonth=Integer.parseInt(moduleWiseCount.get(i).get("month").toString());
			if(month<=6)
			{
				if(moduleMonth>=month)
				{
					int currentCount=i;
					for (int j = i; j < moduleWiseCount.size(); j++) {
						moduleWiseCountTemp.add(moduleWiseCount.get(j));
					}
					for (int j = 0; j < currentCount-1; j++) {
						moduleWiseCountTemp.add(moduleWiseCount.get(j));
					}
					breakFlag=true;
				}
			}
			else if(month>6)
			{
				if(moduleMonth<=month)
				{
					int currentCount=i;
					for (int j = i; j < moduleWiseCount.size(); j++) {
						moduleWiseCountTemp.add(moduleWiseCount.get(j));
					}
					for (int j = 0; j < currentCount-1; j++) {
						moduleWiseCountTemp.add(moduleWiseCount.get(j));
					}
					breakFlag=true;
				}
			}
				
			if (breakFlag) {
				break;
			}
		}
		modelData.put("moduleWiseCount",  utilsService.convertListOfMapToJson(moduleWiseCountTemp));
		modelData.put("monthList", monthList);
		for (int i = 0; i < tmDashboardCount.size(); i++) {
			Map<String, Object> moduleTestCaseCountsTemp = new HashMap<String, Object>();
			moduleTestCaseCountsTemp.put("project_id", tmDashboardCount.get(i).get("project_id"));
			moduleTestCaseCountsTemp.put("project_name", tmDashboardCount.get(i).get("project_name"));
			moduleTestCaseCountsTemp.put("test_count", tmDashboardCount.get(i).get("test_count"));
			moduleTestCaseCountsTemp.put("module_count", tmDashboardCount.get(i).get("module_count"));
			moduleTestCaseCountsTemp.put("sce_count", tmDashboardCount.get(i).get("sce_count"));
			moduleTestCaseCounts.add(moduleTestCaseCountsTemp);
		}
		allCountsTemp.put("projectCount", project.size());
		allCountsTemp.put("moduleCount", modules.size());
		allCountsTemp.put("scenarioCount", testScenario.size());
		allCountsTemp.put("testCaseCount", testcase.size());
		allCountsTemp.put("testBugCount", testcasebugs.size());
		allCountsTemp.put("testcaseDraft", testcaseDraft.size());
		allCountsTemp.put("testcaseReadyForReview", testcaseReadyForReview.size());
		allCountsTemp.put("testcaseReviewInProgress", testcaseReviewInProgress.size());
		allCountsTemp.put("testcaseReviewCompleted", testcaseReviewCompleted.size());
		allCountsTemp.put("testcaseFinal", testcaseFinal.size());
		allCountsTemp.put("moduleScenarioCount", moduleScenarioCount);
		allCountsTemp.put("moduleName", moduleName);
		allCountsTemp.put("ComponentName", ComponentName);
		allCountsTemp.put("ComponentSceCount", ComponentSceCount);
		allCountsTemp.put("CompSceID", CompSceID);
		allCountsTemp.put("AMTestCase", AMTestCase);
		allCountsTemp.put("totalTestCountByExecutionType", totalTestCountByExecutionType);
		
		allCounts.add(allCountsTemp);
		allCounts = utilsService.removeDuplicate(allCounts);
		moduleTestCaseCounts = utilsService.removeDuplicate(moduleTestCaseCounts);
		List<Map<String, Object>> topActivities = null;
		try {
			topActivities = activitiesDAO.getTopActivities(schema);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String color[] = { colorProperties.getProperty("tmDashbaord.app1"),
				colorProperties.getProperty("tmDashbaord.app2"), colorProperties.getProperty("tmDashbaord.app3"),
				colorProperties.getProperty("tmDashbaord.app4"), colorProperties.getProperty("tmDashbaord.app5"),
				colorProperties.getProperty("tmDashbaord.app6"), colorProperties.getProperty("tmDashbaord.app7"),
				colorProperties.getProperty("tmDashbaord.app8"), colorProperties.getProperty("tmDashbaord.app9"),
				colorProperties.getProperty("tmDashbaord.app10") };
		for (int i = 0; i < project.size(); i++) {
			project.get(i).put("color", color[i%10]);
		}
		for (int i = 0; i < topActivities.size(); i++) {
			String dateInString = topActivities.get(i).get("activity_date").toString();
			topActivities.get(i).put("activity_date",
					utilsService.getDateTimeOfTimezone(userTimeZone, dateInString));
		}

		
		String appFlag= "false";
		if(setAllProjectTM!=null){
			modelData.put("setAllProjectTM", false);
		}
		
		
		modelData.put("appFlag", appFlag);
		modelData.put("allCounts", allCounts);
		modelData.put("UserProfilePhoto", userProfilePhoto);
		modelData.put("userName", userFullName);
		modelData.put("role", role);
		modelData.put("projectwiseCount",  utilsService.convertListOfMapToJson(projectwiseCount));
		modelData.put("project", project);
		modelData.put("projects", utilsService.convertListOfMapToJson(project));
		modelData.put("moduleTestCaseCounts", moduleTestCaseCounts);
		modelData.put("nowDate", utilsService.getNowDateOfTimezone(userTimeZone));
		modelData.put("prevDate", utilsService.getPrevDateOfTimezone(userTimeZone));
		modelData.put("topActivities", topActivities);
		return modelData;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getTMAllAppDashboard(TmDashboardDAO tmDashDAO, int userId, String schema,
			Properties colorProperties, ActivitiesDAO activitiesDAO, String userTimeZone, Object userprofilePotho,
			String userFullName, String role) {
		List<Map<String, Object>> allCounts = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> moduleTestCaseCounts = new ArrayList<Map<String, Object>>();
		Map<String, Object> allCountsTemp = new HashMap<String, Object>();
		Map<String,Object> modelData=new HashMap<String,Object>();
		
		Map<String, Object> resultSet = tmDashDAO.getTMDashboardData(userId, schema);
		List<Map<String, Object>> tmDashboardCount = (List<Map<String, Object>>) resultSet.get("#result-set-1");
		List<Map<String, Object>> projectwiseCount = (List<Map<String, Object>>) resultSet.get("#result-set-2");
		List<Map<String, Object>> project = (List<Map<String, Object>>) resultSet.get("#result-set-3");
		List<Map<String, Object>> modules = (List<Map<String, Object>>) resultSet.get("#result-set-4");
		List<Map<String, Object>> testScenario = (List<Map<String, Object>>) resultSet.get("#result-set-5");
		List<Map<String, Object>> testcase = (List<Map<String, Object>>) resultSet.get("#result-set-6");
		
		List<Map<String, Object>> testcaseDraft = (List<Map<String, Object>>) resultSet.get("#result-set-7");
		List<Map<String, Object>> testcaseReadyForReview = (List<Map<String, Object>>) resultSet.get("#result-set-8");
		List<Map<String, Object>> testcaseReviewInProgress = (List<Map<String, Object>>) resultSet.get("#result-set-9");
		List<Map<String, Object>> testcaseReviewCompleted = (List<Map<String, Object>>) resultSet.get("#result-set-10");
		List<Map<String, Object>> testcaseFinal = (List<Map<String, Object>>) resultSet.get("#result-set-11");

		for (int i = 0; i < tmDashboardCount.size(); i++) {
			Map<String, Object> moduleTestCaseCountsTemp = new HashMap<String, Object>();
			moduleTestCaseCountsTemp.put("project_id", tmDashboardCount.get(i).get("project_id"));
			moduleTestCaseCountsTemp.put("project_name", tmDashboardCount.get(i).get("project_name"));
			moduleTestCaseCountsTemp.put("test_count", tmDashboardCount.get(i).get("test_count"));
			moduleTestCaseCountsTemp.put("module_count", tmDashboardCount.get(i).get("module_count"));
			moduleTestCaseCountsTemp.put("sce_count", tmDashboardCount.get(i).get("sce_count"));
			moduleTestCaseCounts.add(moduleTestCaseCountsTemp);
		}
		
		
		moduleTestCaseCounts = utilsService.removeDuplicate(moduleTestCaseCounts);
		allCountsTemp.put("projectCount", project.size());
		allCountsTemp.put("moduleCount", modules.size());
		allCountsTemp.put("scenarioCount", testScenario.size());
		allCountsTemp.put("testCaseCount", testcase.size());
		allCountsTemp.put("testcaseDraft", testcaseDraft.size());
		allCountsTemp.put("testcaseReadyForReview", testcaseReadyForReview.size());
		allCountsTemp.put("testcaseReviewInProgress", testcaseReviewInProgress.size());
		allCountsTemp.put("testcaseReviewCompleted", testcaseReviewCompleted.size());
		allCountsTemp.put("testcaseFinal", testcaseFinal.size());
		allCounts.add(allCountsTemp);
		allCounts = utilsService.removeDuplicate(allCounts);
		String color[] = { colorProperties.getProperty("tmDashbaord.app1"),
				colorProperties.getProperty("tmDashbaord.app2"), colorProperties.getProperty("tmDashbaord.app3"),
				colorProperties.getProperty("tmDashbaord.app4"), colorProperties.getProperty("tmDashbaord.app5"),
				colorProperties.getProperty("tmDashbaord.app6"), colorProperties.getProperty("tmDashbaord.app7"),
				colorProperties.getProperty("tmDashbaord.app8"), colorProperties.getProperty("tmDashbaord.app9"),
				colorProperties.getProperty("tmDashbaord.app10") };
		for (int i = 0; i < project.size(); i++) {
			project.get(i).put("color", color[i%10]);
		}
		List<Map<String, Object>> topActivities = null;
		try {
			topActivities = activitiesDAO.getTopActivities(schema);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < topActivities.size(); i++) {
			String dateInString = topActivities.get(i).get("activity_date").toString();
			topActivities.get(i).put("activity_date",
					utilsService.getDateTimeOfTimezone(userTimeZone, dateInString));
		}

		
		modelData.put("allCounts", allCounts);
		modelData.put("UserProfilePhoto", loginService.getImage((byte[])userprofilePotho));
		modelData.put("userName", userFullName);
		modelData.put("role",role);
		modelData.put("projectwiseCount", utilsService.convertListOfMapToJson(projectwiseCount));
		modelData.put("project", project);
		modelData.put("projects", utilsService.convertListOfMapToJson(project));
		modelData.put("moduleTestCaseCounts", moduleTestCaseCounts);
		modelData.put("nowDate", utilsService.getNowDateOfTimezone(userTimeZone));
		modelData.put("prevDate",utilsService.getPrevDateOfTimezone(userTimeZone));
		modelData.put("topActivities", topActivities);
		
		return modelData;
	}

}

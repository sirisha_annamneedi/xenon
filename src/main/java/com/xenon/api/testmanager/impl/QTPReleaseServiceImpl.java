package com.xenon.api.testmanager.impl;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.api.testmanager.QTPRelaseService;
import com.xenon.testmanager.dao.QTPReleaseDAO;
import com.xenon.testmanager.domain.ExtSysBuildDetail;
import com.xenon.testmanager.domain.ExtSysReleaseDetail;
import com.xenon.testmanager.domain.ExtSysTestCaseDetail;
import com.xenon.testmanager.domain.ExtSysTestStepDetail;

public class QTPReleaseServiceImpl implements QTPRelaseService{

	@Autowired
	private QTPReleaseDAO qtpReleaseDAO;

	private static final Logger logger = LoggerFactory.getLogger(QTPReleaseServiceImpl.class);
	
	@Override
	public int insertExtSysReleaseDetails(ExtSysReleaseDetail parsedReleaseDetails,String schema) {
		
		int flag = 0;
		int releaseId = 0;
		try {
			//Check if release details are already in db
			int dbReleaseId = qtpReleaseDAO.getReleaseId(parsedReleaseDetails, schema);
			boolean checkForBuildId = false;
			if(dbReleaseId != 0) {
				checkForBuildId = true;
				releaseId = Integer.valueOf(dbReleaseId);
			}
			else
				releaseId = qtpReleaseDAO.addReleaseDetails(parsedReleaseDetails, schema);
				
			
			if(releaseId != 0) {
				List<ExtSysBuildDetail> buildDetail = parsedReleaseDetails.getBuildList();
				if(buildDetail != null && !buildDetail.isEmpty()) {
					for (Iterator<ExtSysBuildDetail> iterator = buildDetail.iterator(); iterator.hasNext();) {
						int buildId = 0;
						ExtSysBuildDetail extSysBuildDetail = (ExtSysBuildDetail) iterator.next();
						extSysBuildDetail.setReleaseId(releaseId);
						if(checkForBuildId) {
							int dbBuildId = qtpReleaseDAO.getBuildId(releaseId, extSysBuildDetail.getName(), schema);
							if(dbBuildId != 0)
								buildId = Integer.valueOf(dbBuildId);
							else	
								buildId = qtpReleaseDAO.addBuildDetails(extSysBuildDetail, schema);
						}else {
							buildId = qtpReleaseDAO.addBuildDetails(extSysBuildDetail, schema);
						}
						
						if(buildId != 0) {
							List<ExtSysTestCaseDetail> testCaseDetails = extSysBuildDetail.getTestCaseList();
							for (Iterator<ExtSysTestCaseDetail> iterator1 = testCaseDetails.iterator(); iterator1.hasNext();) {
								int testCaseId = 0;
								ExtSysTestCaseDetail extSysTestCaseDetail = (ExtSysTestCaseDetail) iterator1.next();
								extSysTestCaseDetail.setBuildId(buildId);
								testCaseId = qtpReleaseDAO.addTestCaseDetails(extSysTestCaseDetail, schema);
								
								if(testCaseId != 0) {
									List<ExtSysTestStepDetail> stepDetails = extSysTestCaseDetail.getTestStepList();
									
									for (Iterator<ExtSysTestStepDetail> iterator2 = stepDetails.iterator(); iterator2.hasNext();) {
										ExtSysTestStepDetail extSysTestStepDetail = (ExtSysTestStepDetail) iterator2.next();
										extSysTestStepDetail.setTestCaseId(testCaseId);
										qtpReleaseDAO.addTestStepDetails(extSysTestStepDetail, schema);
										flag = 1;
										logger.info("Ext System Release details added successfully");
									}	
								}
								
							}	
						}
					}
				}
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return flag;
	}
	
}

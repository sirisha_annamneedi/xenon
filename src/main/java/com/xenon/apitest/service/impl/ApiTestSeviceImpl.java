package com.xenon.apitest.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xenon.apitest.dao.ApiTestDAO;
import com.xenon.apitest.domain.ApiTestCaseDetails;
import com.xenon.apitest.service.ApiTestService;

@Service
public class ApiTestSeviceImpl implements ApiTestService{
	@Autowired
	ApiTestDAO apitestcasedao;

	final CloseableHttpClient httpClient = HttpClients.createDefault();

	@Override
	public CloseableHttpResponse methodGet(String url) throws ClientProtocolException, IOException{
		
		HttpGet request = new HttpGet(url);
		CloseableHttpResponse response = httpClient.execute(request);
		return response;
			
	}

	@Override
	public CloseableHttpResponse methodPost(String url, String body) throws ClientProtocolException, IOException {
		
		HttpPost request= new HttpPost(url);
		StringEntity entity1 = new StringEntity(body);
		request.setEntity(entity1);
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");
		
		CloseableHttpResponse response = httpClient.execute(request);
		return response;
	}

	@Override
	public Map<String, Object> getResponseResult(CloseableHttpResponse response,String allurltext,String method1) throws ParseException, IOException {
		
		Map<String,Object> respData= new HashedMap();
		respData.put("statusCode",String.valueOf(response.getStatusLine().getStatusCode()));
		respData.put("statusMsg", String.valueOf(response.getStatusLine().getReasonPhrase()));
        respData.put("urlText", allurltext);
        respData.put("method", method1);
        
        org.apache.http.Header allHeaders[]=response.getAllHeaders();
        for(int i=0;i<allHeaders.length;i++) {
     	   respData.put(allHeaders[i].getName(),allHeaders[i].getValue());
        }
        
        HttpEntity entity=null;
        try {
			entity = response.getEntity();
		} catch (Exception e) {
			e.printStackTrace();
		}
           if (entity != null) {
               String result = EntityUtils.toString(entity);
               respData.put("responseData", result);
           }else {
        	   String res=String.valueOf(response.getStatusLine().getStatusCode());
           }
           return respData;
	}

	@Override
	public int insertApiTestCase(String testcaseName, int scenarioId, String bodyParam, String url, int status,
			int executionType, int executionTime, int author, int updatedBy, String methodName, int projectId, int moduleId, int contentTypeId, int isDeleted,String header, String result, int authenticationTypeId, byte[] validations, String schema) {
		
			ApiTestCaseDetails apiTestCase=new ApiTestCaseDetails();
			apiTestCase.setTestcaseName(testcaseName);
			apiTestCase.setScenarioId(scenarioId);
			apiTestCase.setBodyParam(bodyParam);
			apiTestCase.setUrl(url);
			apiTestCase.setStatus(status);
			apiTestCase.setExecutionType(executionType);
			apiTestCase.setExecutionTime(executionTime);
			apiTestCase.setAuthor(author);
			apiTestCase.setUpdatedBy(updatedBy);
			apiTestCase.setMethodName(methodName);
			apiTestCase.setProjectId(projectId);
			apiTestCase.setModuleId(moduleId);
			apiTestCase.setContentTypeId(contentTypeId);
			apiTestCase.setIsDeleted(isDeleted);
			apiTestCase.setHeader(header);
			apiTestCase.setResult(result);
			apiTestCase.setAuthenticationTypeId(authenticationTypeId);
			apiTestCase.setValidations(validations);
			
			int flag=apitestcasedao.insertApiTestCase(apiTestCase, schema);
			return flag;
	}
	
	
	
	

}

/**
 * 
 */
package com.xenon.apitest.service;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;

/**
 * @author Shailesh.Khot
 *
 */
public interface ApiTestService {

	public CloseableHttpResponse methodGet(String url) throws ClientProtocolException, IOException;
	
	public CloseableHttpResponse methodPost(String url, String body) throws ClientProtocolException, IOException;
	
	public Map<String,Object> getResponseResult(CloseableHttpResponse response,String allurltext,String method1) throws ParseException, IOException;
	
	public int insertApiTestCase(String testcaseName, int scenarioId, String bodyParam, String url, int status, int executionType, int executionTime, int author, int updatedBy, String methodName, int projectId, int moduleId, int contentTypeId, int isDeleted, String header, String result, int authenticationTypeId,byte[] validations, String schema );
}

package com.xenon.apitest.dao;

import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import com.xenon.apitest.domain.ApiTestCaseDetails;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import com.xenon.testmanager.dao.TestSpecificationDAOImpl;


@Service
public class ApiTestDaoImpl implements ApiTestDAO, XenonQuery {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	DatabaseDao database;

	
	@Override
	public Map<String, Object> getApiTestCaseData(int userId, int projectId, int moduleId, int scenarioId,
			String schema) {
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter project = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter module = new SqlParameter("moduleId", Types.INTEGER);
		SqlParameter scenario = new SqlParameter("scenarioId", Types.INTEGER);
		SqlParameter[] paramArray = { user, project, module, scenario};

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("apitestcase")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, projectId, moduleId, scenarioId);
		return simpleJdbcCallResult;
	}

	@Override
	public int insertApiTestCase(ApiTestCaseDetails testcases, String schema) {
		//logger.info("In method insert a test caset");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		testcases.setCreatedDate(dateFormat.format(date));
		int flag = database.update(String.format(apitestcase.insertApiTestCase, schema),
				new Object[] {testcases.getTestcaseName(),
						testcases.getScenarioId(),
						testcases.getBodyParam(),
						testcases.getUrl(),
						testcases.getStatus(),
						testcases.getExecutionType(),
						testcases.getExecutionTime(),
						testcases.getAuthor(),
						testcases.getUpdatedBy(),
						testcases.getMethodName(),
						testcases.getProjectId(),
						testcases.getModuleId(),
						testcases.getContentTypeId(),
						testcases.getIsDeleted(),
						testcases.getHeader(),
						testcases.getResult(),
						testcases.getAuthenticationTypeId(),
						testcases.getCreatedDate(),
						testcases.getValidations()},
				new int[] {Types.VARCHAR,
						Types.INTEGER,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.INTEGER,
						Types.INTEGER,
						Types.INTEGER,
						Types.INTEGER,
						Types.INTEGER,
						Types.VARCHAR,
						Types.INTEGER,
						Types.INTEGER,
						Types.INTEGER,
						Types.INTEGER,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.INTEGER,
						Types.VARCHAR,
						Types.BLOB});
		return flag;
	}

}

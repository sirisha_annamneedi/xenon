package com.xenon.apitest.dao;

import java.util.Map;

import com.xenon.apitest.domain.ApiTestCaseDetails;
import com.xenon.testmanager.domain.TestCaseDetails;

public interface ApiTestDAO {

	
	Map<String, Object> getApiTestCaseData(int userId, int projectId, int moduleId, int scenarioId, String schema);
	
	public int insertApiTestCase(ApiTestCaseDetails testcases, String schema);
}

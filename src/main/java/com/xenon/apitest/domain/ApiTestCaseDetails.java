package com.xenon.apitest.domain;

public class ApiTestCaseDetails {

	private String testcaseName;
	private int scenarioId;
	private String bodyParam;
	private String url;
	private int status; 
	private int executionType;
	private int executionTime;
	private int author;
	private int updatedBy;
	private String methodName;
	private int projectId;
	private int moduleId;
	private int contentTypeId;
	private int isDeleted;
	private String header;
	private String result;
	private int authenticationTypeId;
	private String createdDate;
	private byte[] validations;
	

	public byte[] getValidations() {
		return validations;
	}
	public void setValidations(byte[] validations) {
		this.validations = validations;
	}
	public String getTestcaseName() {
		return testcaseName;
	}
	public void setTestcaseName(String testcaseName) {
		this.testcaseName = testcaseName;
	}
	public int getScenarioId() {
		return scenarioId;
	}
	public void setScenarioId(int scenarioId) {
		this.scenarioId = scenarioId;
	}
	public String getBodyParam() {
		return bodyParam;
	}
	public void setBodyParam(String bodyParam) {
		this.bodyParam = bodyParam;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getExecutionType() {
		return executionType;
	}
	public void setExecutionType(int executionType) {
		this.executionType = executionType;
	}
	public int getExecutionTime() {
		return executionTime;
	}
	public void setExecutionTime(int executionTime) {
		this.executionTime = executionTime;
	}
	public int getAuthor() {
		return author;
	}
	public void setAuthor(int author) {
		this.author = author;
	}
	public int getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public int getContentTypeId() {
		return contentTypeId;
	}
	public void setContentTypeId(int contentTypeId) {
		this.contentTypeId = contentTypeId;
	}
	public int getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public int getAuthenticationTypeId() {
		return authenticationTypeId;
	}
	public void setAuthenticationTypeId(int authenticationTypeId) {
		this.authenticationTypeId = authenticationTypeId;
	}
	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
	
}

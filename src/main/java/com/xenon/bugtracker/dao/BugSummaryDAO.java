package com.xenon.bugtracker.dao;

import java.util.List;
import java.util.Map;

import com.xenon.bugtracker.domain.BugDetails;
import com.xenon.bugtracker.domain.BugSummary;

/**
 * 
 * @author suresh.adling
 * @description Interface to perform JDBC operations on bug summary
 * 
 */
public interface BugSummaryDAO {

	/**
	 * @author suresh.adling
	 * @description get data for bug summary
	 * @param projectId
	 * @param bugId
	 * @param schema
	 * @return Multiple result set
	 */
	Map<String, Object> getDataForBugSummary(int projectId, int bugId, int startValue, int offsetValue, String schema);

	/**
	 * @author suresh.adling
	 * @description get summary details by bug id
	 * @param bugId
	 * @param schema
	 * @return List map - Summary details
	 */
	List<Map<String, Object>> getSummaryDetails(int bugId, String schema);

	/**
	 * @author suresh.adling
	 * @description insert bug summary
	 * @param bugSummary
	 * @param schema
	 * @return integer value- status
	 */
	int insertBugSummary(BugSummary bugSummary, String schema);

	/**
	 * @author suresh.adling
	 * @description update bug summary details
	 * @param bugSummary
	 * @param schema
	 * @return integer - status
	 */
	int updateBugSummary(BugSummary bugSummary, String schema);
	
	/**
	 * @author navnath.damale
	 * @description update bug summary details
	 * @param bugDetails
	 * @param schema
	 * @return integer - status
	 */
	int updateBugAssignStatus(BugDetails bug, String schema);

	/**
	 * @author suresh.adling
	 * @description send mail on update bug details
	 * @param bugSummary
	 * @param schema
	 * @return String - status
	 * @throws Exception
	 */
	String sendMailOnUpdateBug(BugSummary bugSummary,String assignGroupID, String schema,boolean bugGroupFLAG) throws Exception;

	/**
	 * @author suresh.adling
	 * @description get summary id by bug summary details
	 * @param bugSummary
	 * @param schema
	 * @return integer summary Id
	 */
	int getSummaryId(BugSummary bugSummary, String schema);
	/**
	 * @author suresh.adling
	 * @description get data for send mail after updating bug details
	 * @param bugId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getDataForMailOnUpdateBug(int bugId,String assignGroupId, String schema);

	/**
	 * @author navnath.damale
	 * @param bugSummary
	 * @param schema
	 * @return
	 */
	int updateBugSummaryStatus(BugSummary bugSummary, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method inserts comment on bug activities
	 * 
	 * @param bugId
	 * @param activityDesc
	 * @param actUser
	 * @param nfUserId
	 * @param createdBy
	 * @param schema
	 * @return
	 */
	Map<String, Object> addCommentOnBugActivity(int bugId, String activityDesc,
			int actUser, int nfUserId, int createdBy, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method inserts notification to database
	 * 
	 * @param assignStatus
	 * @param groupStatus
	 * @param groupId
	 * @param bugIdId
	 * @param createdBy
	 * @param schema
	 * @return
	 */
	Map<String, Object> insertBugUserNotification(int assignStatus,
			int groupStatus, int groupId, int bugIdId, int createdBy,
			String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method inserts notification to database
	 * 
	 * @param groupStatus
	 * @param bugIdId
	 * @param createdBy
	 * @param schema
	 * @return
	 */
	Map<String, Object> updateBugUserNotification(int groupStatus,int bugId, int createdBy,
			String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method retrieves bug comments of selected bug
	 * 
	 * @param projectId
	 * @param bugId
	 * @param startValue
	 * @param offsetValue
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBugComments(int projectId, int bugId, int startValue, int offsetValue, String schema);
	
}

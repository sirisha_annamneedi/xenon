package com.xenon.bugtracker.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.JdbcTemplate;

import com.xenon.bugtracker.domain.CustomSearchDetails;
import com.xenon.bugtracker.domain.PieChartSearchQuery;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

/**
 * 
 * @author bhagyashri.ajmera
 * @description DAO Implementation to perform JDBC operations on custom search
 *              query
 * 
 */
public class CustomSearchDAOImpl implements CustomSearchDAO, XenonQuery {

	@Autowired
	DatabaseDao database;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LoggerFactory.getLogger(CustomSearchDAOImpl.class);

	@Override
	public int insertCustomSearchQuery(CustomSearchDetails customSearchDetails, String schema) throws SQLException {
		logger.info("Inserting new query to database ");
		String callInsertProject = String.format(customSearch.insertCustomSearchQuery, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsertProject);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, customSearchDetails.getQueryname());
		callableStmt.setString(3, customSearchDetails.getQuery());
		callableStmt.setString(4, customSearchDetails.getQueryjson());
		callableStmt.setInt(5, customSearchDetails.getUserId());
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Insert status = " + insertStatus);
		return insertStatus;
	}

	@Override
	public List<Map<String, Object>> getUserSavedCustomQueries(int userId, String schema) {
		logger.info("Reading user saved custom queries");
		String sql = String.format(customSearch.getUserSavedCustomQueries, schema);
		logger.info("Running Sql query = " + sql);
		List<Map<String, Object>> searchDetails = database.select(sql, new Object[] { userId },
				new int[] { Types.INTEGER });
		logger.info("Query details = { }", searchDetails);
		return searchDetails;
	}
	
	@Override
	public int updateCustomSearchQuery(CustomSearchDetails customSearchDetails, String schema) throws SQLException {
		logger.info("Updating user saved custom queries");
		String sql = String.format(customSearch.updateCustomSearchQuery, schema);
		logger.info("Running Sql query = " + sql);
		int updateStatus = database.update(sql, new Object[] { customSearchDetails.getQueryname(),customSearchDetails.getQuery(),customSearchDetails.getQueryjson(),customSearchDetails.getUserId(),customSearchDetails.getIdxebt() },
				new int[] { Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.INTEGER,Types.INTEGER });
		logger.info("Query details = { }", updateStatus);
		return updateStatus;
	}
	
	@Override
	public int insertPiechartCustomSearchQuery(CustomSearchDetails customSearchDetails, String schema) throws SQLException {
		logger.info("Inserting new query to database ");
		String callInsertProject = String.format(customSearch.insertPiechartCustomSearchQuery, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsertProject);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, customSearchDetails.getQueryname());
		callableStmt.setString(3, customSearchDetails.getQuery());
		callableStmt.setString(4, customSearchDetails.getQueryjson());
		callableStmt.setInt(5, customSearchDetails.getUserId());
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Insert status = " + insertStatus);
		return insertStatus;
	}

	@Override
	public Map<String, Object> getAllBugData(int userID, String schema) {
		logger.info("Calling Stored procedure to get list of releases and user assigned builds , and active status and priorities with user previously stored queries");
		SqlParameter tempUserID = new SqlParameter("userID", Types.INTEGER);
		SqlParameter[] paramArray = {tempUserID};
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("bugAnalysis").declareParameters(paramArray).withCatalogName(schema);
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userID);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	
	@Override
	public List<Map<String,Object>> executeQuery(String customQuery,String schema) {
		logger.info("Running Sql query = " + customQuery);	
		//String sql = String.format(customQuery,schema);
		System.out.println(customQuery);
		List<Map<String, Object>> queryData = database.select(customQuery);
		logger.info("Query details = { }", queryData);
		return queryData;
	}
	
	@Override
	public List<Map<String, Object>> getUserSavedPieChartCustomQueries(int userId, String schema) {
		logger.info("Reading user saved custom queries");
		String sql = String.format(customSearch.getUserSavedPieChartCustomQueries, schema);
		logger.info("Running Sql query = " + sql);
		List<Map<String, Object>> searchDetails = database.select(sql, new Object[] { userId },
				new int[] { Types.INTEGER });
		logger.info("Query details = { }", searchDetails);
		return searchDetails;
	}
	
	@Override
	public List<Map<String,Object>> executePieChartQuery(String customPieChartQuery,String schema){
		logger.info("Running Sql query = " + customPieChartQuery);	
		//String sql = String.format(customQuery,schema);
		System.out.println(customPieChartQuery);
		List<Map<String, Object>> queryData = database.select(customPieChartQuery);
		logger.info("Query details = { }", queryData);
		return queryData;
	}
	
	
	@Override
	public int updatePieChartQuery(PieChartSearchQuery pieChartSearchDetails, String schema) throws SQLException {
		logger.info("Updating user saved pie chart custom queries");
		String sql = String.format(customSearch.updatePieChartSearchQuery, schema);
		logger.info("Running Sql query = " + sql);
		int updateStatus = database.update(sql, new Object[] { pieChartSearchDetails.getQueryname(),pieChartSearchDetails.getQuery(),pieChartSearchDetails.getQueryjson(),pieChartSearchDetails.getUserId(),pieChartSearchDetails.getIdxebt() },
				new int[] { Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.INTEGER,Types.INTEGER });
		logger.info("Query details = { }", updateStatus);
		return updateStatus;
	}
	
	@Override
	public int deleteCustomSearchQuery(int queryId, String schema) throws SQLException {
		logger.info("Updating user saved custom queries");
		String sql = String.format(customSearch.deleteCustomSearchQuery, schema);
		logger.info("Running Sql query = " + sql);
		int updateStatus = database.update(sql, new Object[] { queryId },
				new int[] {Types.INTEGER });
		logger.info("Query details = { }", updateStatus);
		return updateStatus;
	}
	
}

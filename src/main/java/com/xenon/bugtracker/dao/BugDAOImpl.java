package com.xenon.bugtracker.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.api.common.MailService;
import com.xenon.bugtracker.domain.BugDetails;
import com.xenon.bugtracker.domain.BugSummary;
import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.common.dao.MailDAO;
import com.xenon.controller.BugController;
import com.xenon.core.dao.CustomerDAO;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;;

@SuppressWarnings("unchecked")
public class BugDAOImpl implements BugDAO, XenonQuery {

	@Resource(name = "mailProperties")
	private Properties mailProperties;

	@Autowired
	DatabaseDao database;

	@Autowired
	MailDAO mailDAO;

	@Autowired
	CustomerDAO customerDao;

	@Autowired
	UserDAO userDao;

	@Autowired
	ProjectDAO projectDao;

	@Autowired
	ModuleDAO moduleDao;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	MailService mailService;

	private static final Logger logger = LoggerFactory.getLogger(BugController.class);

	@Override
	public List<Map<String, Object>> getPriorities(String schema) {
		logger.info("SQL Query to get list of bug priority ");
		List<Map<String, Object>> listOfPriorities = database.select(String.format(bug.priority, schema));
		logger.info("SQL : " + bug.priority);
		logger.info("SQL Result " + listOfPriorities);
		return listOfPriorities;
	}

	@Override
	public List<Map<String, Object>> getUserList(int projectId, int moduleId, String schema) {
		logger.info("SQL Query to List of users who has assigned  project and  module of selected bug");
		List<Map<String, Object>> userList = database.select(
				String.format(bug.getUserList, schema, schema, schema, schema), new Object[] { moduleId, projectId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("SQL : " + bug.getUserList + " Values : module = " + moduleId + " project = " + projectId);
		logger.info("SQL Result " + userList);
		return userList;
	}

	@Override
	public List<Map<String, Object>> getStatus(String schema) {
		logger.info("SQL Query to get list of bug status ");
		List<Map<String, Object>> listOfStatus = database.select(String.format(bug.status, schema));
		logger.info("SQL : " + bug.status);
		logger.info("SQL Result " + listOfStatus);
		return listOfStatus;
	}

	public Hashtable<String, String> getStatusHashList(String schema) {
		logger.info("SQL Query to get list of bug status ");
		Hashtable<String, String> hmStatus = new Hashtable<String, String>();
		List<Map<String, Object>> listOfStatus = database.select(String.format(bug.status, schema));
		logger.info("SQL : " + bug.status);
		logger.info("SQL Result " + listOfStatus);
		for (int i = 0; i < listOfStatus.size(); i++) {
			if (listOfStatus.get(i).get("bug_state").equals(1)) {
				hmStatus.put(listOfStatus.get(i).get("bug_status").toString(),
						listOfStatus.get(i).get("status_id").toString());
			}
		}
		return hmStatus;
	}

	public Hashtable<String, String> getPriorityHashList(String schema) {
		logger.info("SQL Query to get list of bug priority ");
		Hashtable<String, String> hmPriority = new Hashtable<String, String>();
		List<Map<String, Object>> listOfPriority = database.select(String.format(bug.priority, schema));
		logger.info("SQL : " + bug.priority);
		logger.info("SQL Result " + listOfPriority);
		for (int i = 0; i < listOfPriority.size(); i++) {
			if (listOfPriority.get(i).get("priority_status").equals(1)) {
				hmPriority.put(listOfPriority.get(i).get("bug_priority").toString(),
						listOfPriority.get(i).get("priority_id").toString());
			}
		}
		return hmPriority;
	}

	@Override
	public Hashtable<String, String> getCategoryHashList(String schema) {
		logger.info("SQL Query to get list of bug priority ");
		Hashtable<String, String> hmPriority = new Hashtable<String, String>();
		List<Map<String, Object>> listOfCategory = database.select(String.format(bug.category, schema));
		logger.info("SQL : " + bug.priority);
		logger.info("SQL Result " + listOfCategory);
		for (int i = 0; i < listOfCategory.size(); i++) {
			if (listOfCategory.get(i).get("category_status").equals(1)) {
				hmPriority.put(listOfCategory.get(i).get("category_name").toString(),
						listOfCategory.get(i).get("category_id").toString());
			}
		}
		return hmPriority;

	}

	@Override
	public List<Map<String, Object>> getActiveModules(int projectId, int userId, String schema) {
		logger.info("SQL Query to get list of active modules which are assigned to user");
		List<Map<String, Object>> moduleList = database.select(String.format(bug.activeModules, schema, schema),
				new Object[] { projectId, userId }, new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("SQL : " + bug.activeModules + " Values : project = " + projectId + " user_id = " + userId);
		logger.info("SQL Result " + moduleList);
		return moduleList;
	}

	@Override
	public Map<String, Object> getDataForCreateBug(int projectId, int userId, String schema) {
		logger.info("Reading data for create bug");
		SqlParameter project = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = { project, user };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("createbug")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(projectId, userId);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getDataForInsertBug(int bug_id, String schema) {
		logger.info("Reading data for create bug");
		SqlParameter tempbug_id = new SqlParameter("bug_id", Types.INTEGER);
		SqlParameter[] paramArray = { tempbug_id };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getInsertBugsummary")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(bug_id);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public int insertBug(BugDetails bugs, String schema) {
		logger.info("SQL Query to insert bug detail into xebt_bug table");
		int createBug = database.update(String.format(bug.insertBug, schema),
				new Object[] { bugs.getProjectId(), bugs.getBugTitle(), bugs.getDescription(), bugs.getModuleId(),
						bugs.getCreatedDate(), bugs.getCreatedBy(), bugs.getAssignStatus(), bugs.getGroupStatus(),
						bugs.getGroupId() },
				new int[] { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.TIMESTAMP, Types.INTEGER,
						Types.INTEGER, Types.INTEGER, Types.INTEGER });
		logger.info("SQL : " + bug.insertBug + " Values : project = " + bugs.getProjectId() + " bug title = "
				+ bugs.getBugTitle() + " bug Description = " + bugs.getDescription() + " module = " + bugs.getModuleId()
				+ " created date =" + bugs.getCreatedDate() + "bug author id = " + bugs.getCreatedBy());
		logger.info("SQL Result Status is " + createBug);
		return createBug;
	}

	@Override
	public int insertBugFromJira(BugDetails bugs, String schema) {
		logger.info("SQL Query to insert bug detail into xebt_bug table");
		int createBug = database.update(String.format(bug.insertBug, schema),
				new Object[] { bugs.getProjectId(), bugs.getBugTitle(), bugs.getDescription(), bugs.getModuleId(),
						bugs.getCreatedDate(), bugs.getCreatedBy(), bugs.getAssignStatus(), bugs.getGroupStatus(),
						bugs.getGroupId(), bugs.getJiraId(), bugs.getUpdatedDate(), bugs.getJiraResolution(),
						bugs.getFixVersion() },
				new int[] { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.TIMESTAMP, Types.INTEGER,
						Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.TIMESTAMP, Types.VARCHAR,
						Types.VARCHAR });
		logger.info("SQL : " + bug.insertBug + " Values : project = " + bugs.getProjectId() + " bug title = "
				+ bugs.getBugTitle() + " bug Description = " + bugs.getDescription() + " module = " + bugs.getModuleId()
				+ " created date =" + bugs.getCreatedDate() + "bug author id = " + bugs.getCreatedBy()
				+ "Jira bug id = " + bugs.getJiraId() + " Jira bug updated date = " + bugs.getUpdatedDate()
				+ " Jira resoultion " + bugs.getJiraResolution() + " Fix version " + bugs.getFixVersion());
		logger.info("SQL Result Status is " + createBug);
		return createBug;
	}
	
	@Override
	public int updateBugFromJira(BugDetails bugs, String schema) {
		logger.info("SQL Query to insert bug detail into xebt_bug table");
		int updatedBug = database.update(String.format(bug.updateBugFromJira, schema),
				new Object[] { bugs.getBugTitle(),bugs.getDescription(),bugs.getUpdatedDate(), bugs.getJiraResolution(),bugs.getFixVersion(),bugs.getBugId() },
				new int[] { Types.VARCHAR,Types.VARCHAR,Types.TIMESTAMP, Types.VARCHAR,
						Types.VARCHAR,Types.INTEGER });
		logger.info("SQL : " + bug.updateBugFromJira + " Values : updated_date = " + bugs.getUpdatedDate() + " Resolution = "
				+ bugs.getJiraResolution() + " bug Description = " + bugs.getDescription() + " bug title = " + bugs.getBugTitle() + " bug_id = "+bugs.getBugId()
				);
		logger.info("SQL Result Status is " + updatedBug);
		return updatedBug;
	}
	

	@Override
	public int insertAttachment(int bugId, byte[] path, String title, String schema) {
		logger.info("SQL Query to insert bug attachment into xebt_bug_attachment");
		int status = database.update(String.format(bug.insertBugAttachment, schema),
				new Object[] { bugId, path, title }, new int[] { Types.INTEGER, Types.BLOB, Types.VARCHAR });
		logger.info("SQL Result Status is " + status);
		return status;
	}

	@Override
	public int insertCommentAttachment(int summaryId, String path, String title, String schema) {
		logger.info("SQL Query to insert bug attachment into xebt_bugcomment_attachment");
		int status = database.update(String.format(bug.insertCommentAttachment, schema),
				new Object[] { summaryId, path, title }, new int[] { Types.INTEGER, Types.VARCHAR, Types.VARCHAR });
		logger.info("SQL Result Status is " + status);
		return status;
	}

	@Override
	public int getBugId(BugDetails bugs, String schema) {
		logger.info("SQL Query to get bug id from bug details ");
		List<Map<String, Object>> bugId = database.select(String.format(bug.getBugId, schema),
				new Object[] { bugs.getBugTitle(), bugs.getDescription(), bugs.getProjectId(), bugs.getModuleId(),
						bugs.getCreatedDate(), bugs.getCreatedBy() },
				new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.TIMESTAMP,
						Types.INTEGER });
		logger.info("SQL : " + bug.getBugId + " Values :  bug title = " + bugs.getBugTitle() + " bug Description = "
				+ bugs.getDescription() + "project = " + bugs.getProjectId() + " module = " + bugs.getModuleId()
				+ " created date =" + bugs.getCreatedDate() + "bug author id = " + bugs.getCreatedBy());
		int Id = (Integer) bugId.get(0).get("bug_id");
		logger.info("SQL Result Bug Id  is " + Id);
		return Id;
	}

	@Override
	public List<Map<String, Object>> getBugDetails(int bugId, String schema) {
		logger.info("SQL Query to get bug details from bug id");
		List<Map<String, Object>> bugDetails = database.select(
				String.format(bug.bugDetails, schema, schema, schema, schema), new Object[] { bugId },
				new int[] { Types.INTEGER });
		logger.info("SQL : " + bug.bugDetails + " Values : bugId = " + bugId);
		logger.info("SQL Result " + bugDetails);
		return bugDetails;
	}

	@Override
	public int createNewBugStatus(String schema, int custTypeId) throws SQLException {

		logger.info("Checking create bug status for customer exceeds or not");
		String createNewBugStatus = String.format(bug.createNewBugStatus, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(createNewBugStatus);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setInt(2, custTypeId);
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		logger.info("Status returned by procedure is = " + insertStatus);
		callableStmt.close();
		return insertStatus;

	}

	@Override
	public int addUpdateBugActivity(int bugId, int userId, String comment, String schema) throws SQLException {
		logger.info("Adding bug update sctivity");
		String updatebug = String.format(bug.addUpdateBugActivity, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(updatebug);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setInt(2, bugId);
		callableStmt.setInt(3, userId);
		callableStmt.setString(4, comment);
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		logger.info("Return status : " + insertStatus);
		callableStmt.close();
		return insertStatus;

	}

	@Override
	public int addCreateBugActivity(int bugId, int userId, String bugSummary, String schema) throws SQLException {
		logger.info("Adding bug update activity");
		String updatebug = String.format(bug.addInsertBugActivity, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(updatebug);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setInt(2, bugId);
		callableStmt.setInt(3, userId);
		callableStmt.setString(4, bugSummary);
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		logger.info("Return status : " + insertStatus);
		callableStmt.close();
		return insertStatus;

	}

	@Override
	public Map<String, Object> getAllBugDetails(int projectId, int userId, int startValue, int offsetValue,
			String schema) {

		logger.info("Calling Stored procedure to get View All bugs Data");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter proID = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, proID, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("viewallbugs")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, projectId, startValue, offsetValue);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;

	}

	@Override
	public List<Map<String, Object>> getBugGraphData(int projectId, int userId, String schema) {
		logger.info("SQL Query to get list of allbugs which are from assigned and active module");
		String sql = String.format(bug.getBugGraphData, schema, schema, schema, schema, schema, schema, schema, schema);
		List<Map<String, Object>> allBugDetails = database.select(sql, new Object[] { projectId, userId },
				new int[] { Types.INTEGER, Types.INTEGER });
		for (int i = 0; i < allBugDetails.size(); i++) {
			String date = allBugDetails.get(i).get("weekday").toString();
			allBugDetails.get(i).put("bug_status", getBugStatus(projectId, userId, date, schema));
		}

		logger.info("SQL : " + bug.allBugDetails + " Values : user_id = " + userId);
		logger.info("SQL Result " + allBugDetails);
		return allBugDetails;
	}

	@Override
	public List<Map<String, Object>> getBugStatus(int projectId, int userId, String date, String schema) {
		logger.info("SQL Query to get list of allbugs which are from assigned and active module");
		String sql = String.format(bug.getBugStatus, schema, schema, schema, schema, schema, schema, schema, schema);
		List<Map<String, Object>> allBugDetails = database.select(sql, new Object[] { projectId, userId, date },
				new int[] { Types.INTEGER, Types.INTEGER, Types.VARCHAR });
		logger.info("SQL : " + bug.allBugDetails + " Values : user_id = " + userId);
		logger.info("SQL Result " + allBugDetails);
		return allBugDetails;
	}

	@Override
	public List<Map<String, Object>> getBugGraphDashboardData(int projectId, int userId, String schema) {
		logger.info("SQL Query to get list of allbugs which are from assigned and active module");
		String sql = String.format(bug.getBugGraphStatus, schema, schema, schema, schema, schema);
		List<Map<String, Object>> allBugDetails = database.select(sql, new Object[] { projectId, userId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("SQL : " + bug.allBugDetails + " Values : user_id = " + userId);
		logger.info("SQL Result " + allBugDetails);
		return allBugDetails;
	}

	@Override
	public List<Map<String, Object>> getAllBugGraphDashboardData(int userId, String schema) {
		logger.info("SQL Query to get list of allbugs which are from assigned and active module");
		String sql = String.format(bug.getAllBugGraphStatus, schema, schema, schema, schema, schema, schema, schema);
		List<Map<String, Object>> allBugDetails = database.select(sql, new Object[] { userId },
				new int[] { Types.INTEGER });
		logger.info("SQL : " + bug.allBugDetails + " Values : user_id = " + userId);
		logger.info("SQL Result " + allBugDetails);
		return allBugDetails;
	}

	@Override
	public List<Map<String, Object>> getBugreport(int userId, String schema) {
		logger.info("SQL Query to get list of allbugs which are from assigned and active module");
		String sql = String.format(bug.getBugreporttable, schema, schema, schema, schema, schema);
		List<Map<String, Object>> allBugDetails = database.select(sql);
		logger.info("SQL : " + bug.allBugDetails + " Values : user_id = " + userId);
		logger.info("SQL Result " + allBugDetails);
		return allBugDetails;
	}

	@Override
	public Map<String, Object> getAssignedToMeBugs(int projectId, int userID, int startValue, int offsetValue,
			String schema) {
		logger.info("Calling Stored procedure to get View Assigned to me bugs Data");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter proID = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, proID, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("viewassignedbugs")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userID, projectId, startValue, offsetValue);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getReportedByMeBugs(int projectId, int userId, int startValue, int offsetValue,
			String schema) {
		logger.info("Calling Stored procedure to get View Reported bugs Data");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter proID = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, proID, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("viewreportedbugs")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, projectId, startValue, offsetValue);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public String sendMailOnCreateBug(int bugId, BugDetails bug, String schema, BugSummary bugSummary,
			boolean bugGroupFLAG) throws Exception {
		logger.info("Sending mail on create bug");
		Map<String, Object> data = getDataForMailOnCreateBug(bugSummary.getBugId(), schema);
		List<Map<String, Object>> emailDetails = (List<Map<String, Object>>) data.get("#result-set-1");
		List<Map<String, Object>> mailNotification = (List<Map<String, Object>>) data.get("#result-set-2");
		List<Map<String, Object>> bugDetails = (List<Map<String, Object>>) data.get("#result-set-3");
		List<Map<String, Object>> bugGroupUserList = (List<Map<String, Object>>) data.get("#result-set-4");
		List<Map<String, Object>> bugGroupName = (List<Map<String, Object>>) data.get("#result-set-5");
		if (emailDetails.size() == 0) {
			logger.info("Mail details not available");
			return "Mail details not available";
		}
		int sendStatus = Integer.parseInt(mailNotification.get(0).get("create_bug").toString());
		if (sendStatus == 1) {
			logger.info("Reading details to send mail");
			ArrayList<String> recepients = new ArrayList<String>();
			String bugAssignee = bugDetails.get(0).get("assignee_email_id").toString();
			recepients.add(bugDetails.get(0).get("bug_author_email_id").toString());
			if (bugGroupFLAG) {
				for (int i = 0; i < bugGroupUserList.size(); i++) {
					recepients.add(bugGroupUserList.get(i).get("user_emailId").toString());
				}
				bugAssignee = bugGroupName.get(0).get("bug_group_name").toString();
			} else {
				recepients.add(bugAssignee);
			}

			logger.info("Reading bug mail text and details");
			String mailSubject = mailProperties.getProperty("CREATE_BUG_SUBJECT", "New bug created");
			String mailBodyText = mailProperties.getProperty("CREATE_BUG_BODY_TEXT", "None");
			mailBodyText = String.format(mailBodyText, bugDetails.get(0).get("bug_prefix"),
					bugDetails.get(0).get("bug_prefix"), bug.getBugTitle(),
					bugDetails.get(0).get("bug_author_email_id"), bugDetails.get(0).get("project_name"),
					bugDetails.get(0).get("module_name"), bug.getCreatedDate(), bugAssignee,
					bugDetails.get(0).get("currentPriority"), bugDetails.get(0).get("currentStatus"),
					bug.getDescription());

			String from = emailDetails.get(0).get("smtp_user_id").toString();
			String message = mailService.postMail(emailDetails, recepients, mailSubject, mailBodyText, from);
			return message;
		} else {
			logger.info("Send mail status is inactive");
			return "Send mail status is inactive";
		}
	}

	@Override
	public List<Map<String, Object>> getPriorityDetailsById(int priorityId, String schema) {
		logger.info("SQL Query to get priority deatils by id");
		List<Map<String, Object>> listOfPriorities = database.select(String.format(bug.getPriorityDetailsById, schema),
				new Object[] { priorityId }, new int[] { Types.INTEGER });
		logger.info("SQL : " + bug.getPriorityDetailsById + " Values : priorityId = " + priorityId);
		logger.info("SQL Result " + listOfPriorities);
		return listOfPriorities;
	}

	@Override
	public List<Map<String, Object>> getStatusDetailsById(int statusId, String schema) {
		logger.info("SQL Query to get status deatils by id");
		List<Map<String, Object>> listOfStatus = database.select(String.format(bug.getStatusDetailsById, schema),
				new Object[] { statusId }, new int[] { Types.INTEGER });
		logger.info("SQL : " + bug.getPriorityDetailsById + " Values : satus = " + statusId);
		logger.info("SQL Result " + listOfStatus);
		return listOfStatus;
	}

	@Override
	public List<Map<String, Object>> getAllOpenBugs(int projectId, int userId, String schema) {
		logger.info("SQL Query to get all open bugs from active and assigned modules");
		String sql = String.format(bug.allOpenBugs, schema, schema, schema, schema, schema, schema, schema, schema);
		logger.info("SQL : " + sql + " Values : bug.project = " + projectId + ", user_id = " + userId);
		List<Map<String, Object>> allBugDetails = database.select(sql, new Object[] { projectId, userId },
				new int[] { Types.INTEGER, Types.INTEGER });
		logger.info("SQL Result All Open Bugs List " + allBugDetails);
		return allBugDetails;
	}

	@Override
	public List<Map<String, Object>> getBugStatistics(int userId, int projectId, String schema) {
		logger.info("SQL Query to get count of open,closed,assigned,priority bugs ");
		String sql = String.format(bug.bugStatistics, schema, schema, schema, schema);
		List<Map<String, Object>> allBugDetails = database.select(sql, new Object[] { userId, projectId, userId },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER });
		logger.info("SQL : " + sql + " Values : assignee = " + userId + " bug.project = " + projectId + " user_id = "
				+ userId);
		logger.info("SQL Result " + allBugDetails);
		return allBugDetails;
	}

	@Override
	public List<Map<String, Object>> getDashboardData(int userId, String schema) {
		logger.info("SQL Query to get count of open,closed,assigned,priority bugs ");
		String sql = String.format(bug.getDashboardData, schema, schema, schema, schema, schema, schema, schema, schema,
				schema, schema, schema, schema, schema, schema);
		List<Map<String, Object>> allBugDetails = database.select(sql, new Object[] { userId, userId, userId },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER });
		;
		logger.info("SQL Result " + allBugDetails);
		return allBugDetails;
	}

	@Override
	public List<Map<String, Object>> getBugAttachmentDeatils(int bugId, String schema) {
		logger.info("SQL Query to get bug attachment details from bug id");
		List<Map<String, Object>> bugAttachDetails = database.select(String.format(bug.bugAttachments, schema),
				new Object[] { bugId }, new int[] { Types.INTEGER });
		logger.info("SQL : " + bug.bugDetails + " Values : bugId = " + bugId);
		logger.info("SQL Result " + bugAttachDetails);
		return bugAttachDetails;
	}

	@Override
	public List<Map<String, Object>> getBugAttachmentById(int attchmentId, String schema) {
		logger.info("SQL Query to get bug attachment details from bug id");
		List<Map<String, Object>> bugAttachDetails = database.select(String.format(bug.bugAttachmentById, schema),
				new Object[] { attchmentId }, new int[] { Types.INTEGER });
		logger.info("SQL : " + bug.bugDetails + " Values : attchmentId = " + attchmentId);
		logger.info("SQL Result " + bugAttachDetails);
		return bugAttachDetails;
	}

	@Override
	public List<Map<String, Object>> getCommentAttachmentDeatils(int commentId, String schema) {
		logger.info("SQL Query to get comment attachment details from comment id");
		List<Map<String, Object>> commentAttachDetails = database.select(String.format(bug.commentAttachments, schema),
				new Object[] { commentId }, new int[] { Types.INTEGER });
		logger.info("SQL : " + bug.commentAttachments + " Values : commentId = " + commentId);
		logger.info("SQL Result " + commentAttachDetails);
		return commentAttachDetails;
	}

	@Override
	public List<Map<String, Object>> getCommentAttachmentById(int attchmentId, String schema) {
		logger.info("SQL Query to get bug attachment details from bug id");
		List<Map<String, Object>> commentAttachDetails = database.select(
				String.format(bug.commentAttachmentById, schema), new Object[] { attchmentId },
				new int[] { Types.INTEGER });
		logger.info("SQL : " + bug.bugDetails + " Values : attchmentId = " + attchmentId);
		logger.info("SQL Result " + commentAttachDetails);
		return commentAttachDetails;
	}

	@Override
	public List<Map<String, Object>> getBugDetailsByPrefix(String bugPrefix, String schema) {
		logger.info("SQL Query to get bug id from  bugPrefix ");
		List<Map<String, Object>> bugDetails = database.select(String.format(bug.getBugIdByPrefix, schema),
				new Object[] { bugPrefix }, new int[] { Types.VARCHAR });

		return bugDetails;
	}

	@Override
	public List<Map<String, Object>> getActiveUsers(int projectId, String schema) {
		logger.info("SQL Query to get list of active users to whome selected project is assigned");
		String sql = String.format(bug.getActiveUsers, schema, schema);
		List<Map<String, Object>> userDetails = database.select(sql, new Object[] { projectId },
				new int[] { Types.INTEGER });
		return userDetails;
	}

	@Override
	public List<Map<String, Object>> getAllPriorityDetails(String schema) {
		logger.info("SQL Query to get all priority deatils");
		List<Map<String, Object>> listOfPriorities = database.select(String.format(bug.getAllPriorityDetails, schema));
		logger.info("SQL Result " + listOfPriorities);
		return listOfPriorities;
	}

	@Override
	public List<Map<String, Object>> getAllStatusDetails(String schema) {
		logger.info("SQL Query to get all status deatils");
		List<Map<String, Object>> listOfPriorities = database.select(String.format(bug.getAllStatusDetails, schema));
		logger.info("SQL Result " + listOfPriorities);
		return listOfPriorities;
	}

	@Override
	public Map<String, Object> getDataForMailOnCreateBug(int bugId, String schema) {
		logger.info("Reading data for mail on create bug");
		SqlParameter bug = new SqlParameter("bugId", Types.INTEGER);
		SqlParameter[] paramArray = { bug };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("mailoncreatebug")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(bugId);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getBugDashboard(int projectId, int userId, int startValue, int offsetValue,
			String schema) {
		logger.info("Reading data for mail on create bug");
		SqlParameter projectIdTemp = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter userIdTemp = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = { projectIdTemp, userIdTemp };
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBugDashboard")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(projectId, userId);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getBtDashLinkClosedBugs(int projectId, int userId, int startValue, int offsetValue,
			String schema) {

		logger.info("Calling Stored procedure to get Bug tracker dashboard link bugs");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter proID = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, proID, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBtDashLinkClosedBugs")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, projectId, startValue, offsetValue);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;

	}

	@Override
	public Map<String, Object> getBtDashLinkOpenBugs(int projectId, int userId, int startValue, int offsetValue,
			String schema) {

		logger.info("Calling Stored procedure to get Bug tracker dashboard link bugs");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter proID = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, proID, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBtDashLinkOpenBugs")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, projectId, startValue, offsetValue);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;

	}
	
	
	//mycode
	@Override
	public Map<String, Object> getBtDashLinkInprogressBugs(int projectId, int userId, int startValue, int offsetValue,
			String schema) {

		logger.info("Calling Stored procedure to get Bug tracker dashboard link bugs");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter proID = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, proID, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBtDashLinkInprogressBugs")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, projectId, startValue, offsetValue);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;

	}
	
	
	@Override
	public Map<String, Object> getBtDashLinkResolvedBugs(int projectId, int userId, int startValue, int offsetValue,
			String schema) {

		logger.info("Calling Stored procedure to get Bug tracker dashboard link bugs");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter proID = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, proID, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBtDashLinkResolvedBugs")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, projectId, startValue, offsetValue);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;

	}
	
	@Override
	public Map<String, Object> getBtDashLinkReopenedBugs(int projectId, int userId, int startValue, int offsetValue,
			String schema) {

		logger.info("Calling Stored procedure to get Bug tracker dashboard link bugs");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter proID = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, proID, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBtDashLinkReopenedBugs")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, projectId, startValue, offsetValue);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;

	}
	

	@Override
	public Map<String, Object> getBtDashLinkAssignedBugs(int projectId, int userId, int startValue, int offsetValue,
			String schema) {
		logger.info("Calling Stored procedure to get Bug tracker dashboard link bugs");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter proID = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, proID, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBtDashLinkAssignedBugs")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, projectId, startValue, offsetValue);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getBtDashLinkImmediateBugs(int projectId, int userId, int startValue, int offsetValue,
			String schema) {
		logger.info("Calling Stored procedure to get Bug tracker dashboard link bugs");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter proID = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, proID, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBtDashLinkImmediateBugs")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, projectId, startValue, offsetValue);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getBtDashLinkAllClosedBugs(int userId, int startValue, int offsetValue, String schema) {
		logger.info("Calling Stored procedure to get Bug tracker dashboard link bugs");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBtDashLinkAllClosedBugs")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, startValue, offsetValue);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getBtDashLinkAllOpenBugs(int userId, int startValue, int offsetValue, String schema) {
		logger.info("Calling Stored procedure to get Bug tracker dashboard link bugs");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getBtDashLinkAllOpenBugs")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, startValue, offsetValue);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getBtDashLinkAllAssignedBugs(int userId, int startValue, int offsetValue,
			String schema) {
		logger.info("Calling Stored procedure to get Bug tracker dashboard link bugs");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("getBtDashLinkAllAssignedBugs").declareParameters(paramArray)
				.withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, startValue, offsetValue);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public Map<String, Object> getBtDashLinkAllImmediateBugs(int userId, int startValue, int offsetValue,
			String schema) {
		logger.info("Calling Stored procedure to get Bug tracker dashboard link bugs");
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { user, start, off };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("getBtDashLinkAllImmediateBugs").declareParameters(paramArray)
				.withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId, startValue, offsetValue);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public boolean checkBTRoleAccess(String roleAccess, String roleId, String schema) {
		boolean flag = false;
		logger.info("Calling Stored procedure to get build report data");
		SqlParameter roleID = new SqlParameter("roleId", Types.VARCHAR);
		SqlParameter access = new SqlParameter("roleAccess", Types.VARCHAR);
		SqlParameter[] paramArray = { access, roleID };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("checkBTAccess")
				.declareParameters(paramArray).withCatalogName(schema);
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(roleAccess, roleId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		List<Map<String, Object>> returnList = (List<Map<String, Object>>) simpleJdbcCallResult.get("#result-set-1");
		if (!returnList.isEmpty()) {
			flag = returnList.get(0).containsValue(1);
		}
		return flag;
	}

	@Override
	public int updateLastSynchWithJira(String moduleType,String jiraUpdateId,String jiraCurrentReleaseId, String schema) {
		logger.info("SQL Query to insert bug jira synch last updated date in xead_jira_details_update");
		Calendar cal = Calendar.getInstance(); 
		java.sql.Timestamp bugTrackerTimestamp = new Timestamp(cal.getTimeInMillis());
		
		int updateBugDateTime = database.update(String.format(bug.updateJiraBugTrackerLastUpdateDate, schema),
				new Object[] { bugTrackerTimestamp,jiraCurrentReleaseId,jiraUpdateId},
				new int[] { Types.TIMESTAMP,Types.INTEGER,Types.INTEGER});
		logger.info("SQL Result Status is " + updateBugDateTime);
		return updateBugDateTime;
	}

	@Override
	public int insertJiraUpdatedDateWithJira(String issue_tracker_project_key,String current_release_id,String schema) {
		logger.info("SQL Query to insert bug jira synch last updated date in xead_jira_details_update");
		Calendar cal = Calendar.getInstance(); 
		java.sql.Timestamp bugTrackerTimestamp = new Timestamp(cal.getTimeInMillis());
		java.sql.Timestamp testManagerTimestamp = new Timestamp(cal.getTimeInMillis());
		
		int lastUpdateDateTime = database.update(String.format(bug.insertJiraBugTrackerLastUpdateDate, schema),
				new Object[] { issue_tracker_project_key,current_release_id,bugTrackerTimestamp,testManagerTimestamp},
				new int[] { Types.VARCHAR,Types.INTEGER,Types.TIMESTAMP,Types.TIMESTAMP});
		logger.info("SQL : " + bug.insertJiraBugTrackerLastUpdateDate + " Values : issue_tracker_project_key = "
				+ ""+ issue_tracker_project_key +" current_release_id = "+ current_release_id +" jira_bug_tracker_last_update = " + bugTrackerTimestamp + " jira_test_management_last_update " +testManagerTimestamp);
		
		logger.info("SQL Result Status is " + lastUpdateDateTime);
		return lastUpdateDateTime;
	}
	
	@Override
	public List<Map<String, Object>> getJiraLastUpdate(String issue_tracker_project_key, String schema) {
	logger.info("SQL Query to get all priority deatils");
	
	String sql = String.format(bug.getAllUpdateDateFromJiraSynch, schema);
	List<Map<String, Object>> bugUpdateDatetimeFromJira = database.select(sql, new Object[] { issue_tracker_project_key },
			new int[] { Types.VARCHAR });
	logger.info("SQL Result " + bugUpdateDatetimeFromJira);
	return bugUpdateDatetimeFromJira;
	}
	/*
	 * @Override public List<Map<String,Object>> getBugColumns(String schema){
	 * logger.info("SQL Query to get coulmns of xebt_bugsummary table");
	 * List<Map<String, Object>> listOfColumns =
	 * database.select(String.format(bug.getListOfColumns, schema));
	 * logger.info("SQL Result " + listOfColumns); return listOfColumns;
	 * 
	 * }
	 */

	@Override
	public List<Map<String, Object>> getBugIdMappedWithJiraId(String jiraId, String schema) {
		logger.info("SQL Query to get bug id based on Jira id deatils");
		String sql = String.format(bug.getBugIdFromJiraId, schema,jiraId);
		List<Map<String, Object>> bugId = database.select(sql, new Object[] { jiraId },
				new int[] { Types.VARCHAR });
		logger.info("SQL : " + bug.getBugIdFromJiraId + " Values : jira_id = " + jiraId);
		logger.info("SQL Result " + bugId);
		return bugId;
	}

	@Override
	public int deleteBug(int bugId, String schema) {
		String sql = String.format(bug.deleteBug, schema);
		int flag = database.update(sql, new Object[] { bugId }, new int[] { Types.INTEGER });
		logger.info("Query result = "+flag);
		return flag;
	}
	
	@Override
	public int deleteAllBugs(String schema) {
		String sql = String.format(bug.deleteAllBugs, schema);
		int flag = database.update(sql);
		logger.info("Query result = "+flag);
		return flag;
	}

	@Override
	public Map<String, Object> getCommonDashboardData(int projectId, int userId, String schema) {
		logger.info("Calling Stored procedure to get common dashboard data");
		SqlParameter tempProjectId = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter tempUserId = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = { tempProjectId, tempUserId };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getDefaultDashboard")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(projectId, userId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public int updateJiraIdByBigId(String bugId, String jiraId, String userId, String schema) {
		int flag = database.update("UPDATE "+schema+".xebt_bug SET jira_id='"+jiraId+"' WHERE bug_id="+bugId);
		logger.info("Query result = "+flag);
		return flag;
	}
	
}

package com.xenon.bugtracker.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.bugtracker.domain.BugGroupDetails;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

/**
 * 
 * @author suresh.adling
 * @description DAO Implementation to perform JDBC operations on bug group
 * 
 */
public class BugGroupDAOImpl implements BugGroupDAO, XenonQuery {

	@Autowired
	DatabaseDao database;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LoggerFactory.getLogger(BugGroupDAOImpl.class);

	@Override
	public Map<String, Object> getBugGroupData(String schema) {
		logger.info("In a method to decalre procedure for get bug group data");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("bugGroup")
				.declareParameters().withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Result after procedure call " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public int insertBugGroup(String bugGroupName, String schema) throws SQLException {
		logger.info("In a method to decalre function for insert bug group");
		String callInsert = String.format(bugGroup.createNewBugGroup, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsert);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, bugGroupName);
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Result after method call " + insertStatus);
		return insertStatus;
	}

	@Override
	public void insertBugGroupRecords(final List<BugGroupDetails> bugGroups, String schema) {

		logger.info(
				"In a method to decalre batch update for function to insert bug group records(bug group id,userid) ");
		String sql = String.format(bugGroup.insertBugGroupRecords, schema);

		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public int getBatchSize() {
				return bugGroups.size();
			}

			@Override
			public void setValues(java.sql.PreparedStatement ps, int i) throws SQLException {
				BugGroupDetails bug = bugGroups.get(i);
				ps.setInt(1, bug.getBugUserId());
				ps.setInt(2, bug.getBugGroupId());

			}
		});
	}

	@Override
	public void updateBugGroupRecords(final List<BugGroupDetails> bugGroups, String schema, int status) {
		logger.info("In a method update Bug group records ");
		String SQL = String.format(bugGroup.updateBugGroups, schema);
		if (bugGroups.get(0) != null) {
			database.update(SQL, new Object[] { bugGroups.get(0).getBugGroupId() }, new int[] { Types.INTEGER });
		}
		if (status == 1) {
			String sql = String.format(bugGroup.insertBugGroupRecords, schema);
			jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return bugGroups.size();
				}

				@Override
				public void setValues(java.sql.PreparedStatement ps, int i) throws SQLException {
					BugGroupDetails bug = bugGroups.get(i);
					ps.setInt(1, bug.getBugUserId());
					ps.setInt(2, bug.getBugGroupId());

				}
			});
		}
	}

	@Override
	public int updateBugGroupDetails(int bugGroupId, String bugGroupName, String schema) {
		logger.info("In a method update bug group details (name)");
		logger.info("SQL : " + bugGroup.updateBugGroupDetails);
		String sql = String.format(bugGroup.updateBugGroupDetails, schema);
		int flag = database.update(sql, new Object[] { bugGroupName, bugGroupId },
				new int[] { Types.VARCHAR, Types.INTEGER });
		logger.info("Reult : " + flag);
		return flag;
	}

	
	@Override
	public List<Map<String, Object>> getBugGroups(String schema) {
		logger.info("A method to get all bug groups");
		String sql = String.format(bugGroup.getBuggroups, schema);
		List<Map<String, Object>> bugGroups = database.select(sql);
		return bugGroups;
	}
	
	@Override
	public List<Map<String, Object>> getUserbyGroupId(int groupId,String schema) {
		logger.info("A method to get all bug group users by id");
		String sql = String.format(bugGroup.getUserbyGroup, schema,schema);
		List<Map<String, Object>> bugUsers = database.select(sql,new Object[]{groupId},new int[]{ Types.INTEGER });
		return bugUsers;
	}
	
	@Override
	public List<Map<String, Object>> getGroupbyId(int groupId,String schema) {
		logger.info("A method to get all bug group users by id");
		String sql = String.format(bugGroup.getGroupbyId, schema);
		List<Map<String, Object>> bugGroup = database.select(sql,new Object[]{groupId},new int[]{ Types.INTEGER });
		return bugGroup;
	}
}

package com.xenon.bugtracker.dao;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.api.common.MailService;
import com.xenon.bugtracker.domain.BugDetails;
import com.xenon.bugtracker.domain.BugSummary;
import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.common.dao.MailDAO;
import com.xenon.core.dao.CustomerDAO;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;;

/**
 * 
 * @author suresh.adling
 * @description DAO implementation class to perform JDBC operations on bug
 *              summary
 * 
 */
@SuppressWarnings("unchecked")
public class BugSummaryDAOImpl implements BugSummaryDAO, XenonQuery {

	@Resource(name = "mailProperties")
	private Properties mailProperties;

	@Autowired
	DatabaseDao database;

	@Autowired
	MailDAO mailDAO;

	@Autowired
	CustomerDAO customerDao;

	@Autowired
	UserDAO userDao;

	@Autowired
	ProjectDAO projectDao;

	@Autowired
	ModuleDAO moduleDao;

	@Autowired
	BugDAO bugDao;
	
	@Autowired
	MailService mailService;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LoggerFactory.getLogger(BugSummaryDAOImpl.class);

	@Override
	public Map<String, Object> getDataForBugSummary(int projectId, int bugId, int startValue, int offsetValue, String schema) {
		logger.info("In a method to decalre procedure for bug summary ");
		SqlParameter fNameParam = new SqlParameter("projectId", Types.INTEGER);
		SqlParameter lNameParam = new SqlParameter("bugId", Types.INTEGER);
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter[] paramArray = { fNameParam, lNameParam, start, off};

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("bugsummary")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(projectId, bugId, startValue, offsetValue);
		logger.info("Result after procedure call " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	

	@Override
	public int getSummaryId(BugSummary bugSummary, String schema) {
		logger.info("SQL Query to get bug summary id from bug summary details ");
		List<Map<String, Object>> bugId = database.select(String.format(bug.getSummaryId, schema),
				new Object[] { bugSummary.getBugId(), bugSummary.getStatus(), bugSummary.getPriority(),
						bugSummary.getAssignee(), bugSummary.getUpdatedDate(), bugSummary.getUpdatedBy(),
						bugSummary.getPrevStatus(), bugSummary.getPrevPriority(), bugSummary.getPrevAssignee(),
						bugSummary.getDescription() },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.TIMESTAMP, Types.INTEGER,
						Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.VARCHAR });
		logger.info("SQL : " + bug.getSummaryId + " Values : bug id = " + bugSummary.getBugId() + " bug status = "
				+ bugSummary.getStatus() + " bug priority = " + bugSummary.getPriority() + " assignee id = "
				+ bugSummary.getAssignee() + " updated date =" + bugSummary.getUpdatedDate() + "bug updater id = "
				+ bugSummary.getUpdatedBy() + "bug prev status id = " + bugSummary.getPrevStatus()
				+ "bug prev priority id = " + bugSummary.getPrevPriority() + "bug prev assignee id = "
				+ bugSummary.getPrevAssignee() + "bug summary description = " + bugSummary.getDescription());
		
		int Id = (Integer) bugId.get(0).get("bs_id");
		logger.info("SQL Result Bug Id  is " + Id);
		return Id;
	}

	@Override
	public int insertBugSummary(BugSummary bugSummary, String schema) {
		logger.info("SQL Query to insert bug summary into xebt_bugsummary table");
		int createBug = database.update(String.format(bug.insertBugSummary, schema),
				new Object[] { bugSummary.getBugId(), bugSummary.getStatus(), bugSummary.getPriority(),
						bugSummary.getAssignee(), bugSummary.getUpdatedDate(), bugSummary.getUpdatedBy(),
						bugSummary.getPrevStatus(), bugSummary.getPrevPriority(), bugSummary.getPrevAssignee(), bugSummary.getSeverity() , bugSummary.getPrevSeverity(),bugSummary.getGroup_assign(),bugSummary.getCategory(),bugSummary.getPrevCategory(),bugSummary.getManualBuild(),bugSummary.getPrevManualBuild(),bugSummary.getAutomationBuild(),bugSummary.getPrevAutomationBuild(),
						bugSummary.getIsBuildAssigned(),bugSummary.getJiraReleaseID()},
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.TIMESTAMP, Types.INTEGER,
						Types.INTEGER, Types.INTEGER, Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER });
		logger.info("SQL : " + bug.insertBugSummary + " Values : bug id = " + bugSummary.getBugId() + " bug status = "
				+ bugSummary.getStatus() + " bug priority = " + bugSummary.getPriority() + " assignee id = "
				+ bugSummary.getAssignee() + " updated date =" + bugSummary.getUpdatedDate() + "bug updater id = "
				+ bugSummary.getUpdatedBy() + "bug prev status id = " + bugSummary.getPrevStatus()
				+ "bug prev priority id = " + bugSummary.getPrevPriority() + "bug prev assignee id = "
				+ bugSummary.getPrevAssignee()+ " Jira release id "+bugSummary.getJiraReleaseID());
		logger.info("SQL Result Status is " + createBug);
		return createBug;
	}

	@Override
	public List<Map<String, Object>> getSummaryDetails(int bugId, String schema) {
		logger.info("SQL Query to get bug summary deatils from bug id");
		List<Map<String, Object>> bugSummaryDetails = database.select(
				String.format(bug.summaryDetails, schema, schema, schema, schema, schema, schema, schema, schema, schema),
				new Object[] { bugId }, new int[] { Types.INTEGER });
		logger.info("SQL : " + bug.bugDetails + " Values : bugId = " + bugId);
		logger.info("SQL Result " + bugSummaryDetails);
		return bugSummaryDetails;
	}

	@Override
	public int updateBugSummary(BugSummary bugSummary, String schema) {
		logger.info("SQL Query to update bug summary details");
		
		logger.info("SQL : " + bug.updateBugSummary + " Values : bug id = " + bugSummary.getBugId() + " bug status = "
				+ bugSummary.getStatus() + " bug priority = " + bugSummary.getPriority() + " assignee id = "
				+ bugSummary.getAssignee() + " updated date =" + bugSummary.getUpdatedDate() + "bug updater id = "
				+ bugSummary.getUpdatedBy() + "bug prev status id = " + bugSummary.getPrevStatus()
				+ "bug prev priority id = " + bugSummary.getPrevPriority() + "bug prev assignee id = "
				+ bugSummary.getPrevAssignee() + "bug summary description = " + bugSummary.getDescription()+ " jira release id = "+bugSummary.getJiraReleaseID());
		int updateBug = database.update(String.format(bug.updateBugSummary, schema),
				new Object[] { bugSummary.getStatus(), bugSummary.getPriority(),
						bugSummary.getAssignee(), bugSummary.getUpdatedDate(), bugSummary.getUpdatedBy(),
						bugSummary.getPrevStatus(), bugSummary.getPrevPriority(), bugSummary.getPrevAssignee(),
						bugSummary.getDescription(),bugSummary.getSeverity(), bugSummary.getPrevSeverity(),bugSummary.getCategory(),bugSummary.getPrevCategory(),bugSummary.getGroup_assign(),bugSummary.getManualBuild(),bugSummary.getPrevManualBuild(),bugSummary.getAutomationBuild(),bugSummary.getPrevAutomationBuild(),bugSummary.getIsBuildAssigned(),bugSummary.getJiraReleaseID(),bugSummary.getBugId() },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.TIMESTAMP, Types.INTEGER,
						Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.INTEGER,Types.INTEGER, Types.INTEGER, Types.INTEGER , Types.INTEGER, Types.INTEGER,Types.INTEGER, Types.INTEGER, Types.INTEGER,Types.INTEGER,Types.INTEGER });
		logger.info("SQL : " + bug.updateBugSummary + " Values : bug id = " + bugSummary.getBugId() + " bug status = "
				+ bugSummary.getStatus() + " bug priority = " + bugSummary.getPriority() + " assignee id = "
				+ bugSummary.getAssignee() + " updated date =" + bugSummary.getUpdatedDate() + "bug updater id = "
				+ bugSummary.getUpdatedBy() + "bug prev status id = " + bugSummary.getPrevStatus()
				+ "bug prev priority id = " + bugSummary.getPrevPriority() + "bug prev assignee id = "
				+ bugSummary.getPrevAssignee() + "bug summary description = " + bugSummary.getDescription()+ " jira release id"+bugSummary.getJiraReleaseID());
		logger.info("SQL Result Status is " + updateBug);
		return updateBug;
	}

	@Override
	public int updateBugSummaryStatus(BugSummary bugSummary, String schema) {
		logger.info("SQL Query to update bug summary details");
		int updateBug = database.update(String.format(bug.updateBugsumaryStatus, schema),
				new Object[] { bugSummary.getBugId()},
				new int[] { Types.INTEGER});
		logger.info("SQL Result Status is " + updateBug);
		return updateBug;
	}
	
	
	@Override
	public String sendMailOnUpdateBug(BugSummary bugSummary,String assignGroupID, String schema,boolean bugGroupFLAG) throws Exception {
		logger.info("Sending mail on update bug");

		Map<String, Object> data = getDataForMailOnUpdateBug(bugSummary.getBugId(),assignGroupID, schema);

		List<Map<String, Object>> emailDetails = (List<Map<String, Object>>) data.get("#result-set-1");
		List<Map<String, Object>> mailNotification = (List<Map<String, Object>>) data.get("#result-set-2");
		List<Map<String, Object>> bugDetails = (List<Map<String, Object>>) data.get("#result-set-3");
		List<Map<String, Object>> bugGroupUserList = (List<Map<String, Object>>) data.get("#result-set-4");
		List<Map<String, Object>> bugGroupName = (List<Map<String, Object>>) data.get("#result-set-5");
		if (emailDetails.size() == 0) {
			logger.info("Mail details not available");
			return "Mail details not available";
		}
		int sendStatus = Integer.parseInt(mailNotification.get(0).get("update_bug").toString());
		if (sendStatus == 1) {
			
			ArrayList<String> recepients = new ArrayList<String>();
			recepients.add(bugDetails.get(0).get("bug_updated_by_email_id").toString());
			
			String bugAssignee=bugDetails.get(0).get("assignee_email_id").toString();
			String prevAssignee=bugDetails.get(0).get("prev_assignee_email_id").toString();
			String groupFlag=bugDetails.get(0).get("group_id").toString();
			
			if(!groupFlag.equals("0"))
			{
				prevAssignee = bugDetails.get(0).get("prevGroupName").toString();
			}
			
			if(bugGroupFLAG)
			{
				for(int i=0;i<bugGroupUserList.size();i++)
				{
					recepients.add(bugGroupUserList.get(i).get("user_emailId").toString());
				}
				bugAssignee = bugGroupName.get(0).get("bug_group_name").toString();
			}
			else
			{
				recepients.add(bugAssignee);
			}
			
			String mailSubject = mailProperties.getProperty("UPDATE_BUG_SUBJECT", "Bug updated");
			String mailBodyText = mailProperties.getProperty("UPDATE_BUG_BODY_TEXT", "None");

			logger.info("Reading mail details");
			mailBodyText = String.format(mailBodyText, bugDetails.get(0).get("bug_prefix"),bugDetails.get(0).get("bug_prefix"),
					bugDetails.get(0).get("bug_title"), bugDetails.get(0).get("bug_author_email_id"),
					bugDetails.get(0).get("project_name"), bugDetails.get(0).get("module_name"),
					bugDetails.get(0).get("create_date"), prevAssignee,
					bugAssignee, bugDetails.get(0).get("prevPriority"),
					bugDetails.get(0).get("currentPriority"), bugDetails.get(0).get("prevStatus"),
					bugDetails.get(0).get("currentStatus"), bugSummary.getDescription());
			
			String from = emailDetails.get(0).get("smtp_user_id").toString();
			logger.info("Posting mail");
			String message = mailService.postMail(emailDetails, recepients, mailSubject, mailBodyText, from);
			return message;
		} else {
			logger.info("Send mail status is inactive");
			return "Send mail status is inactive";
		}
	}

	@Override
	public Map<String, Object> getDataForMailOnUpdateBug(int bugId,String assignGroupID, String schema) {
		logger.info("Reading data for mail on update bug");
		SqlParameter bug = new SqlParameter("bugId", Types.INTEGER);
		SqlParameter bugGroupId = new SqlParameter("bugGroupId", Types.INTEGER);
		
		SqlParameter[] paramArray = { bug,bugGroupId };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("mailonupdatebug")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(bugId,Integer.parseInt(assignGroupID));
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public int updateBugAssignStatus(BugDetails bugDetails, String schema) {
		logger.info("Reading data for update bug status");
		int updateAssignBug = database.update(String.format(bug.updateBugAssignStatus, schema),
				new Object[] { bugDetails.getAssignStatus(),bugDetails.getGroupStatus(),bugDetails.getGroupId(),bugDetails.getBugId() },
				new int[] { Types.INTEGER, Types.INTEGER,Types.INTEGER,Types.INTEGER });
		logger.info("Return status ="+updateAssignBug);
		return updateAssignBug;
	}
	
	
	@Override
	public Map<String, Object> addCommentOnBugActivity(int bugId,String activityDesc,int actUser,int nfUserId,int createdBy, String schema) {
		logger.info("Reading data for mail on update bug");
		SqlParameter bugtemp = new SqlParameter("bugId", Types.INTEGER);
		SqlParameter activityDesctemp = new SqlParameter("activityDesc", Types.VARCHAR);
		SqlParameter actUsertemp = new SqlParameter("actUser", Types.INTEGER);
		SqlParameter nfUserIdtemp = new SqlParameter("nfUserId", Types.INTEGER);
		SqlParameter createdBytemp = new SqlParameter("createdBy", Types.INTEGER);
		SqlParameter[] paramArray = { bugtemp,activityDesctemp,actUsertemp,nfUserIdtemp,createdBytemp };

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("addCommentOnBugActivity")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(bugId,activityDesc,actUser,nfUserId,createdBy);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}	
	
	@Override
	public Map<String, Object> insertBugUserNotification(int assignStatus,int groupStatus,int groupId,int bugId,int createdBy, String schema) {
		logger.info("Reading data for mail on update bug");
		SqlParameter assignStatustemp = new SqlParameter("assignStatus", Types.INTEGER);
		SqlParameter groupStatustemp = new SqlParameter("groupStatus", Types.INTEGER);
		SqlParameter groupIdtemp = new SqlParameter("groupId", Types.INTEGER);
		SqlParameter bugIdtemp = new SqlParameter("bugId", Types.INTEGER);
		SqlParameter createdBytemp = new SqlParameter("createdBy", Types.INTEGER);
		
		SqlParameter[] paramArray = {assignStatustemp,groupStatustemp,groupIdtemp,bugIdtemp,createdBytemp};

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("insertBugUserNotification")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(assignStatus,groupStatus,groupId,bugId,createdBy);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	
	
	@Override
	public Map<String, Object> updateBugUserNotification(int groupStatus,int bugId,int createdBy, String schema) {
		logger.info("Reading data for mail on update bug");
		SqlParameter groupStatustemp = new SqlParameter("groupStatus", Types.INTEGER);
		SqlParameter bugIdtemp = new SqlParameter("bugId", Types.INTEGER);
		SqlParameter createdBytemp = new SqlParameter("createdBy", Types.INTEGER);
		
		SqlParameter[] paramArray = {groupStatustemp,bugIdtemp,createdBytemp};
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("updateBugUserNotification")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(groupStatus,bugId,createdBy);
		logger.info("Return list = " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	
	
	@Override
	public List<Map<String, Object>> getBugComments(int projectId, int bugId, int startValue, int offsetValue, String schema) {
		logger.info("SQL Query to get bug summary deatils from bug id");
		List<Map<String, Object>> bugSummaryDetails = database.select(
				String.format(bug.summaryDetails, schema, schema, schema, schema, schema, schema, schema, schema, schema),
				new Object[] { bugId,startValue,offsetValue }, new int[] { Types.INTEGER,Types.INTEGER, Types.INTEGER});
		logger.info("SQL : " + bug.bugDetails + " Values : bugId = " + bugId);
		logger.info("SQL Result " + bugSummaryDetails);
		return bugSummaryDetails;
	}
	
}

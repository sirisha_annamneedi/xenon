package com.xenon.bugtracker.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.xenon.bugtracker.domain.CustomSearchDetails;
import com.xenon.bugtracker.domain.PieChartSearchQuery;

/**
 * This service handles custom search related operations
 * 
 * @author bhagyashri.ajmera <br>
 *         <b>6th July 2017</b>
 *
 */
public interface CustomSearchDAO {

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method inserts custom search query data
	 * 
	 * @param customSearchDetails
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int insertCustomSearchQuery(CustomSearchDetails customSearchDetails, String schema) throws SQLException;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads custom saved queries of user
	 * 
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getUserSavedCustomQueries(int userId, String schema);

	
	
	/**
	 * @author pooja.mugade
	 * 
	 *         This method reads all bug data for UI
	 * 
	 * @param userId
	 * @param schema
	 * @return
	 */
	public Map<String, Object> getAllBugData(int userID, String schema);
	
	/**
	 * @author pooja.mugade
	 * 
	 *         This method execute custom query
	 * 
	 * @param customQuery
	 * @return
	 */
	public List<Map<String,Object>> executeQuery(String customQuery,String schema);


	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method updates custom search query data
	 * 
	 * @param customSearchDetails
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int updateCustomSearchQuery(CustomSearchDetails customSearchDetails, String schema) throws SQLException;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method reads custom saved queries of user
	 *         
	 * @param customSearchDetails
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int insertPiechartCustomSearchQuery(CustomSearchDetails customSearchDetails, String schema) throws SQLException;

	/**
	 * @author pooja.mugade
	 * 
	 *         This method reads custom saved queries of user
	 * 
	 * @param userId
	 * @param schema
	 * @return
	 */
	 List<Map<String, Object>> getUserSavedPieChartCustomQueries(int userId, String schema);
	 

	 
	 /**
		 * @author pooja.mugade
		 * 
		 *         This method for execute piechart query
		 *         
		 * @param customQuery
		 * @param schema
		 * @return
		 * @throws SQLException
		 */
	 List<Map<String,Object>> executePieChartQuery(String customPieChartQuery,String schema);
	 
		/**
		 * @author pooja.mugade
		 * 
		 *         This method update pie chart data
		 * 
		 * @param customSearchDetails
		 * @param schema
		 * @return
		 * @throws SQLException 
		 */ 
	 int updatePieChartQuery(PieChartSearchQuery pieChartSearchDetails, String schema) throws SQLException;

	 /***
	  * @author bhagyashri.ajmera
	  * 
	  * This method is used to delete custom search query
	  * 
	  * @param queryId
	  * @param schema
	  * @return
	  * @throws SQLException
	  */
	 int deleteCustomSearchQuery(int queryId, String schema) throws SQLException;
}

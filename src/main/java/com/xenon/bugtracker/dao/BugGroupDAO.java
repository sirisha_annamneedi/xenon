package com.xenon.bugtracker.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.xenon.bugtracker.domain.BugGroupDetails;
/**
 * 
 * @author navnath.damale
 * @description Interface to perform JDBC operations on bug group
 * 
 */
public interface BugGroupDAO {
	/**
	 * @author navnath.damale
	 * @param schema
	 * @return
	 */
	Map<String, Object> getBugGroupData(String schema);
	
	/**
	 * @author navnath.damale
	 * @param bugGroups
	 * @param schema
	 */
	public void insertBugGroupRecords(final List<BugGroupDetails> bugGroups, String schema);
	
	/**
	 * @author navnath.damale
	 * @param bugGroup
	 * @param schema
	 * @param status
	 */
	void updateBugGroupRecords(List<BugGroupDetails> bugGroup, String schema,int status);
	
	/**
	 * @author navnath.damale
	 * @param bugGroupName
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	public int insertBugGroup(String bugGroupName, String schema) throws SQLException;
	
	/**
	 * @author navnath.damale
	 * @param bugGroupId
	 * @param bugGroupName
	 * @param schema
	 * @return
	 */
	int updateBugGroupDetails(int bugGroupId,String bugGroupName, String schema);
	
	/**
	 * @author navnath.damale
	 * @param schema
	 * @return
	 */
	public List<Map<String, Object>> getBugGroups(String schema);
	
	/**
	 * @author navnath.damale
	 * @param groupId
	 * @param schema
	 * @return
	 */
	public List<Map<String, Object>> getUserbyGroupId(int groupId,String schema);
	
	/**
	 * @author navnath.damale
	 * @param groupId
	 * @param schema
	 * @return
	 */
	public List<Map<String, Object>> getGroupbyId(int groupId,String schema);
	
}

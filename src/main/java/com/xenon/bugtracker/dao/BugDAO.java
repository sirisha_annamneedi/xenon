package com.xenon.bugtracker.dao;

import java.sql.SQLException;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.xenon.bugtracker.domain.BugDetails;
import com.xenon.bugtracker.domain.BugSummary;

public interface BugDAO {
	/**
	 * @author suresh.adling
	 * @description get bug priority list
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getPriorities(String schema);

	/**
	 * @author
	 * @description get user list for input project
	 * @param project
	 * @param module
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getUserList(int project, int module, String schema);

	/**
	 * @author suresh.adling
	 * @description get bug status list
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getStatus(String schema);

	/**
	 * @author suresh.adling
	 * @description get active and assigned modules
	 * @param projectId
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getActiveModules(int projectId, int userId, String schema);

	/**
	 * @author suresh.adling
	 * @description get data for create bug
	 * @param projectId
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getDataForCreateBug(int projectId, int userId, String schema);

	/**
	 * @author suresh.adling
	 * @description get bug details by id
	 * @param bugId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBugDetails(int bugId, String schema);

	/**
	 * @author suresh.adling
	 * @description insert bug details
	 * @param bugs
	 * @param schema
	 * @return
	 */
	int insertBug(BugDetails bugs, String schema);

	/**
	 * @author suresh.adling
	 * @description get bug id by bug details
	 * @param bug
	 * @param schema
	 * @return
	 */
	int getBugId(BugDetails bug, String schema);

		
	/**
	 * 
	 * @author bhagyashri.ajmera
	 * 
	 *         This method is to check whether customer exceeds his bug creation
	 *         limit or not
	 * 
	 * @param schema
	 * @param custTypeId
	 * @return
	 * @throws SQLException
	 */
	int createNewBugStatus(String schema, int custTypeId) throws SQLException;

	/**
	 * @author suresh.adling
	 * @description add update bug activity
	 * @param bugId
	 * @param userId
	 * @param comment
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int addUpdateBugActivity(int bugId, int userId, String comment, String schema) throws SQLException;
	/**
	 * @author suresh.adling
	 * @description add insert bug activity
	 * @param bugId
	 * @param userId
	 * @param comment
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int addCreateBugActivity(int bugId, int userId, String bugSummary, String schema) throws SQLException;
	/**
	 * @author suresh.adling
	 * @description get all bug details
	 * @param projectId
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getAllBugDetails(int projectId, int userId, int startValue, int offsetValue, String schema);

	/**
	 * @author suresh.adling
	 * @description get bugs assigned to me
	 * @param projectId
	 * @param userID
	 * @param schema
	 * @return
	 */
	Map<String, Object> getAssignedToMeBugs(int projectId, int userID, int startValue, int offsetValue, String schema);

	/**
	 * @author
	 * @description get bugs reported by me
	 * @param projectId
	 * @param userID
	 * @param schema
	 * @return
	 */
	Map<String, Object> getReportedByMeBugs(int projectId, int userID, int startValue, int offsetValue, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 *         This method is to send mail after create new bug with all bug
	 *         details
	 * 
	 * @param bugId
	 * @param bug
	 * @param schema
	 * @param bugSummary
	 * @return
	 * @throws Exception
	 */
	String sendMailOnCreateBug(int bugId, BugDetails bug, String schema, BugSummary bugSummary,boolean bugGroupFLAG) throws Exception;

	/**
	 * @author
	 * @description get priority details by id
	 * @param priorityId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getPriorityDetailsById(int priorityId, String schema);

	/**
	 * @author
	 * @description get status details by status id
	 * @param statusId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getStatusDetailsById(int statusId, String schema);

	/**
	 * @author
	 * @description get all open bugs
	 * @param projectId
	 * @param userId
	 * @param schema
	 * @return
	 */

	List<Map<String, Object>> getAllOpenBugs(int projectId, int userId, String schema);

	/**
	 * @author suresh.adling
	 * @description get bug statistics
	 * @param userId
	 * @param projectId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBugStatistics(int userId, int projectId, String schema);

	/**
	 * @author suresh.adling
	 * @description get bug dashboard data
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getDashboardData(int userId, String schema);

	/**
	 * @author suresh.adling
	 * @description insert attachment
	 * @param bugId
	 * @param path
	 * @param title
	 * @param schema
	 * @return
	 */
	int insertAttachment(int bugId, byte[] path, String title, String schema);

	/**
	 * @author suresh.adling
	 * @description insert comment attachment
	 * @param bugId
	 * @param path
	 * @param title
	 * @param schema
	 * @return
	 */
	int insertCommentAttachment(int bugId, String path, String title, String schema);

	/**
	 * @author suresh.adling
	 * @description get bug attachment details by id
	 * @param bugId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBugAttachmentDeatils(int bugId, String schema);

	/**
	 * @author suresh.adling
	 * @description get bug attachment by attachment id
	 * @param attchmentId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBugAttachmentById(int attchmentId, String schema);

	/**
	 * @author suresh.adling
	 * @description get comment attachment details by comment id
	 * @param commentId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getCommentAttachmentDeatils(int commentId, String schema);

	/**
	 * @author suresh.adling
	 * @description get comment attachment by attachment id
	 * @param attchmentId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getCommentAttachmentById(int attchmentId, String schema);

	/**
	 * @author suresh.adling
	 * @description get bug details by bug prefix
	 * @param bugPrefix
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBugDetailsByPrefix(String bugPrefix, String schema);

	/**
	 * @author suresh.adling
	 * @description get active users
	 * @param projectId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getActiveUsers(int projectId, String schema);

	/**
	 * @author suresh.adling
	 * @description get all priority details
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllPriorityDetails(String schema);

	/**
	 * @author
	 * @description get all status details
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getAllStatusDetails(String schema);

	/**
	 * @author suresh.adling
	 * @description get bug graph data
	 * @param userId
	 * @param userId2
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBugGraphData(int userId, int userId2, String schema);

	/**
	 * @author
	 * @description get bug status
	 * @param project
	 * @param userId
	 * @param date
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBugStatus(int project, int userId, String date, String schema);

	/**
	 * @author
	 * @description get bug report
	 * @param userId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getBugreport(int userId, String schema);
	/**
	 * @author suresh.adling
	 * @description get data for send mail after creating bug
	 * @param bugId
	 * @param schema
	 * @return
	 */
	Map<String, Object> getDataForMailOnCreateBug(int bugId, String schema);

	/**
	 * @author navnath.damale
	 * @param projectId
	 * @param userId
	 * @param schema
	 * @return
	 */
		List<Map<String, Object>> getBugGraphDashboardData(int projectId, int userId, String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method retrieves bug  dashboard data from database
	 * 
	 * @param schema
	 * @return
	 */
	Map<String, Object> getBugDashboard(int projectId, int userId, int startValue, int offsetValue, String schema);

	List<Map<String, Object>> getAllBugGraphDashboardData(int userId, String schema);

	Map<String, Object> getDataForInsertBug(int bug_id, String schema);

	/**
	 * @author prafulla.pol
	 * This method is used to get the closed bug details of the single project
	 * @param projectId
	 * @param userId
	 * @param startValue
	 * @param offsetValue
	 * @param schema
	 * @return closed bugs details of selected application
	 */
	Map<String, Object> getBtDashLinkClosedBugs(int projectId, int userId, int startValue, int offsetValue, String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get the Open bug details of the single project
	 * @param projectId
	 * @param userId
	 * @param startValue
	 * @param offsetValue
	 * @param schema
	 * @return
	 */
	Map<String, Object> getBtDashLinkOpenBugs(int projectId, int userId, int startValue, int offsetValue, String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get the assigned bug details of the single project of login user
	 * @param projectId
	 * @param userId
	 * @param startValue
	 * @param offsetValue
	 * @param schema
	 * @return
	 */
	Map<String, Object> getBtDashLinkAssignedBugs(int projectId, int userId, int startValue, int offsetValue, String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get the immediate priority bug details of the single project
	 * @param projectId
	 * @param userId
	 * @param startValue
	 * @param offsetValue
	 * @param schema
	 * @return
	 */
	Map<String, Object> getBtDashLinkImmediateBugs(int projectId, int userId, int startValue, int offsetValue, String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get the closed bug details of all project
	 * @param userId
	 * @param startValue
	 * @param offsetValue
	 * @param schema
	 * @return
	 */
	Map<String, Object> getBtDashLinkAllClosedBugs(int userId, int startValue, int offsetValue, String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to get the open bug details of all project
	 * @param userId
	 * @param startValue
	 * @param offsetValue
	 * @param schema
	 * @return
	 */
	Map<String, Object> getBtDashLinkAllOpenBugs(int userId, int startValue, int offsetValue, String schema);

	/**
	 * @author prafulla.pol
	 * This method is used to get the assigned bug details of all project for login user
	 * @param userId
	 * @param startValue
	 * @param offsetValue
	 * @param schema
	 * @return
	 */
	
	Map<String, Object> getBtDashLinkAllAssignedBugs(int userId, int startValue, int offsetValue, String schema);
	/**
	 * @author abhay.thakur
	 * @description update jira id by bug id.
	 * 
	 * @param bugId
	 * @param jiraId
	 * @param schema
	 * @param schema
	 */
	public int updateJiraIdByBigId(String bugId, String jiraId, String userId,String schema);
	/**
	 * @author prafulla.pol
	 * This method is used to get the Immediate bug details of all project
	 * @param userId
	 * @param startValue
	 * @param offsetValue
	 * @param schema
	 * @return
	 */
	
	Map<String, Object> getBtDashLinkAllImmediateBugs(int userId, int startValue, int offsetValue, String schema);
	
	public boolean checkBTRoleAccess(String roleAccess,String roleId,String schema);

	int deleteBug(int bugId, String schema);

	int insertBugFromJira(BugDetails bugs, String schema);

	Hashtable<String, String> getCategoryHashList(String schema);
	
	int updateLastSynchWithJira(String moduleType,String jiraUpdateId,String jiraCurrentReleaseId, String schema);

	int insertJiraUpdatedDateWithJira(String issue_tracker_project_key,String current_release_id,String schema);

	List<Map<String, Object>> getJiraLastUpdate(String issue_tracker_project_key,String schema);

	List<Map<String, Object>> getBugIdMappedWithJiraId(String jiraId, String schema);

	int updateBugFromJira(BugDetails bugs, String schema);

	Hashtable<String, String> getStatusHashList(String string);

	Hashtable<String, String> getPriorityHashList(String string);
	
	int deleteAllBugs(String schema);

	Map<String, Object> getBtDashLinkInprogressBugs(int projectId, int userId, int startValue, int offsetValue,
			String schema);

	Map<String, Object> getBtDashLinkResolvedBugs(int projectId, int userId, int startValue, int offsetValue,
			String schema);

	Map<String, Object> getBtDashLinkReopenedBugs(int projectId, int userId, int startValue, int pageSize,
			String schema);

	Map<String, Object> getCommonDashboardData(int projectId, int userId, String schema);

	
	
	//public List<Map<String,Object>> getBugColumns(String Schema);
}

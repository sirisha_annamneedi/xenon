package com.xenon.bugtracker.domain;

public class PieChartSearchQuery {
	private int idxebt;
	private String queryname;
	private String query;
	private String queryjson;
	private int userId;

	public int getIdxebt() {
		return idxebt;
	}

	public void setIdxebt(int idxebt) {
		this.idxebt = idxebt;
	}

	public String getQueryname() {
		return queryname;
	}

	public void setQueryname(String queryname) {
		this.queryname = queryname;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getQueryjson() {
		return queryjson;
	}

	public void setQueryjson(String queryjson) {
		this.queryjson = queryjson;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
}

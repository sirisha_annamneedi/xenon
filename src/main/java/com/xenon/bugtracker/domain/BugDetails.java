package com.xenon.bugtracker.domain;
/**
 * 
 * @author suresh.adling
 * @description Domain class to declare getter setter methods for bug 
 *              
 * 
 */
public class BugDetails {

	private int bugId;
	private int moduleId;
	private int projectId;
	private int createdBy;
	private String bugTitle;
	private String description;
	private String createdDate;
	private int assignStatus;
	private int groupStatus;
	private int groupId;
	private String jiraId;
	private String jiraResolution;
	private String fixVersion;
	private String updatedDate;
	
	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public int getGroupStatus() {
		return groupStatus;
	}

	public void setGroupStatus(int groupStatus) {
		this.groupStatus = groupStatus;
	}

	public int getAssignStatus() {
		return assignStatus;
	}

	public void setAssignStatus(int assignStatus) {
		this.assignStatus = assignStatus;
	}
	
	public int getBugId() {
		return bugId;
	}

	public void setBugId(int bugId) {
		this.bugId = bugId;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public String getBugTitle() {
		return bugTitle;
	}

	public void setBugTitle(String bugTitle) {
		this.bugTitle = bugTitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getJiraId() {
		return jiraId;		
	}

	public void setJiraId(String jiraId) {
		this.jiraId=jiraId;
		
	}

	public String getJiraResolution() {
		return jiraResolution;
		
	}
	
	public void setJiraResolution(String jiraResolution) {
		this.jiraResolution=jiraResolution;
		
	}
	
	public String getFixVersion() {
		return fixVersion;
		
	}
	public void setfixVersion(String fixVersion) {
		this.fixVersion=fixVersion;
		
	}
	public String getUpdatedDate() {
		return updatedDate;
		
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate=updatedDate;
		
	}
}
package com.xenon.bugtracker.domain;

public class BugGroupDetails {

	private int bugGroupId;
	private int bugUserId;
	
	public int getBugGroupId() {
		return bugGroupId;
	}
	public void setBugGroupId(int bugGroupId) {
		this.bugGroupId = bugGroupId;
	}
	public int getBugUserId() {
		return bugUserId;
	}
	public void setBugUserId(int bugUserId) {
		this.bugUserId = bugUserId;
	}
	
}

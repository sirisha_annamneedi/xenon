package com.xenon.bugtracker.domain;
/**
 * 
 * @author suresh.adling
 * @description Domain class to declare getter setter methods for bug summary
 *              
 * 
 */
public class BugSummary {
	private int summaryId;
	private int bugId;
	private int status;
	private int priority;
	private int severity;
	private int category;
	private int assignee;
	private int prevStatus;
	private int prevPriority;
	private int prevSeverity;
	private int prevCategory;
	private int manualBuild;
	private int prevManualBuild;
	private int automationBuild;
	private int prevAutomationBuild;
	private int isBuildAssigned;
	private int prevAssignee;
	private int updatedBy;
	private String updatedDate;
	private String commentDescription;
	private String attachmentTitle;
	private byte[] bugAttachment;
	private int group_assign;
	private String jiraReleaseID;

	public int getGroup_assign() {
		return group_assign;
	}

	public void setGroup_assign(int group_assign) {
		this.group_assign = group_assign;
	}

	public int setSummaryId() {
		return summaryId;
	}

	public void getSummaryId(int summaryId) {
		this.summaryId = summaryId;
	}

	public int getBugId() {
		return bugId;
	}

	public void setBugId(int bugId) {
		this.bugId = bugId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSeverity() {
		return severity;
	}

	public void setSeverity(int severity) {
		this.severity = severity;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
	public int getAssignee() {
		return assignee;
	}

	public void setAssignee(int assignee) {
		this.assignee = assignee;
	}

	public int getPrevStatus() {
		return prevStatus;
	}

	public void setPrevStatus(int prevStatus) {
		this.prevStatus = prevStatus;
	}

	public int getPrevPriority() {
		return prevPriority;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public void setPrevPriority(int prevPriority) {
		this.prevPriority = prevPriority;
	}

	public int getPrevSeverity() {
		return prevSeverity;
	}

	public void setPrevSeverity(int prevSeverity) {
		this.prevSeverity = prevSeverity;
	}
	public int getPrevAssignee() {
		return prevAssignee;
	}

	public void setPrevAssignee(int prevAssignee) {
		this.prevAssignee = prevAssignee;
	}

	public int getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getDescription() {
		return commentDescription;
	}

	public void setDescription(String commentDescription) {
		this.commentDescription = commentDescription;
	}

	public byte[] getBugAttachment() {
		return bugAttachment;
	}

	public void setBugAttachment(byte[] bugAttachment) {
		this.bugAttachment = bugAttachment;
	}

	public String getAttachmentTitle() {
		return attachmentTitle;
	}

	public void setAttachmentTitle(String attachmentTitle) {
		this.attachmentTitle = attachmentTitle;
	}
	public int getPrevCategory() {
		return prevCategory;
	}

	public void setPrevCategory(int prevCategory) {
		this.prevCategory = prevCategory;
	}

	public int getManualBuild() {
		return manualBuild;
	}

	public void setManualBuild(int manualBuild) {
		this.manualBuild = manualBuild;
	}

	public int getPrevManualBuild() {
		return prevManualBuild;
	}

	public void setPrevManualBuild(int prevManualBuild) {
		this.prevManualBuild = prevManualBuild;
	}

	public int getAutomationBuild() {
		return automationBuild;
	}

	public void setAutomationBuild(int automationBuild) {
		this.automationBuild = automationBuild;
	}

	public int getPrevAutomationBuild() {
		return prevAutomationBuild;
	}

	public void setPrevAutomationBuild(int prevAutomationBuild) {
		this.prevAutomationBuild = prevAutomationBuild;
	}

	public int getIsBuildAssigned() {
		return isBuildAssigned;
	}

	public void setIsBuildAssigned(int isBuildAssigned) {
		this.isBuildAssigned = isBuildAssigned;
	}

	public void setJiraReleaseID(String jiraReleaseId) {
		this.jiraReleaseID=jiraReleaseId;
		
	}
	
	public int getJiraReleaseID() {
		if(jiraReleaseID!=""){
		return Integer.parseInt(jiraReleaseID);
		}else{
			return 0;
		}
	}
}
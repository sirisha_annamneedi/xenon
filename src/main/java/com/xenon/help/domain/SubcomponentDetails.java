package com.xenon.help.domain;

/**
 * 
 * @author bhagyashri.ajmera
 * 
 * This class gives getters and setters for sub component
 *
 */
public class SubcomponentDetails {
	private int id;
	private String subcmpName;
	private String subcmpDesc;
	private int componentId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubcmpName() {
		return subcmpName;
	}
	public void setSubcmpName(String subcmpName) {
		this.subcmpName = subcmpName;
	}
	public String getSubcmpDesc() {
		return subcmpDesc;
	}
	public void setSubcmpDesc(String subcmpDesc) {
		this.subcmpDesc = subcmpDesc;
	}
	public int getComponentId() {
		return componentId;
	}
	public void setComponentId(int componentId) {
		this.componentId = componentId;
	}

}

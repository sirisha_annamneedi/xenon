package com.xenon.help.domain;

public class FaqDetails {
	private int faqId;
	private String title;
	private String description;
	private String createdDate;
	private String updatedDate;
	private int faqCreator;
	public int getFaqId() {
		return faqId;
	}
	public void setFaqId(int faqId) {
		this.faqId = faqId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public int getFaqCreator() {
		return faqCreator;
	}
	public void setFaqCreator(int faqCreator) {
		this.faqCreator = faqCreator;
	}
	
}

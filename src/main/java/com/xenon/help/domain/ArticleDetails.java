package com.xenon.help.domain;

/**
 * 
 * @author bhagyashri.ajmera
 * 
 * This class gives getters and setters for article
 *
 */
public class ArticleDetails {
	private int id;
	private String artName;
	private String artDesc;
	private String createdDate;
	private String updatedDate;
	private int customerId;
	private int userId;
	private int subcomponentId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getArtName() {
		return artName;
	}
	public void setArtName(String artName) {
		this.artName = artName;
	}
	public String getArtDesc() {
		return artDesc;
	}
	public void setArtDesc(String artDesc) {
		this.artDesc = artDesc;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getSubcomponentId() {
		return subcomponentId;
	}
	public void setSubcomponentId(int subcomponentId) {
		this.subcomponentId = subcomponentId;
	}
	
	

}

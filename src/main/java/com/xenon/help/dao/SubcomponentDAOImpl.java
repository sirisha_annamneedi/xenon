package com.xenon.help.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import com.xenon.help.domain.SubcomponentDetails;

public class SubcomponentDAOImpl implements SubcomponentDAO, XenonQuery {

	private static final Logger logger = LoggerFactory.getLogger(SubcomponentDAOImpl.class);
	
	@Autowired
	DatabaseDao database;
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public int createNewSubcomponent(SubcomponentDetails subcomponentDetails, String schema) throws SQLException {
		logger.info("Query to insert subcomponent" + subcomponent.createNewSubcomponent);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(subcomponent.createNewSubcomponent);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, subcomponentDetails.getSubcmpName());
		callableStmt.setString(3, subcomponentDetails.getSubcmpDesc());
		callableStmt.setInt(4, subcomponentDetails.getComponentId());
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Query Result  " + insertStatus);
		return insertStatus;
	}
	
	
	@Override
	public Map<String, Object> getComponentData(int componentId) {
		logger.info("Calling Stored procedure to get View component Data");
		
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = { user };
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getComponentData")
				.declareParameters(paramArray).withCatalogName("xenonsupport");

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(componentId);
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	
	@Override
	public List<Map<String, Object>> getComponentbyiId(int cmpId,String schema) {
		List<Map<String, Object>> cmpData = database.select(subcomponent.getComponent,new Object[]{cmpId},new int[]{ Types.INTEGER });
		return cmpData;
	}
	
	@Override
	public List<Map<String, Object>> getSubComponentbyiId(int subCmpId,String schema) {
		List<Map<String, Object>> subcmpData = database.select(subcomponent.getSubcomponent,new Object[]{subCmpId},new int[]{ Types.INTEGER });
		return subcmpData;
	}
	
}

package com.xenon.help.dao;

import java.util.Map;

public interface HelpDAO {
	Map<String, Object> getHelpData();
}

package com.xenon.help.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import com.xenon.help.domain.ArticleDetails;

public class ArticleDAOImpl implements ArticleDAO, XenonQuery {

	private static final Logger logger = LoggerFactory.getLogger(ArticleDAOImpl.class);
	
	@Autowired
	DatabaseDao database;

	@Override
	public int createNewArticle(ArticleDetails articleDetails, String schema) throws SQLException {
		logger.info("Query to insert article" + article.createNewArticle);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(article.createNewArticle);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, articleDetails.getArtName());
		callableStmt.setString(3, articleDetails.getArtDesc());
		callableStmt.setInt(4, articleDetails.getCustomerId());
		callableStmt.setInt(5, articleDetails.getUserId());
		callableStmt.setInt(6, articleDetails.getSubcomponentId());
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Query Result  " + insertStatus);
		return insertStatus;
	}
	
	@Override
	public List<Map<String, Object>> getArticle(int articleId,String schema) {
		String sql = String.format(article.getArticle, schema);
		List<Map<String, Object>> articleDetails = database.select(sql,new Object[]{articleId},new int[]{ Types.INTEGER });
		return articleDetails;
	}
	
	@Override
	public List<Map<String, Object>> getArticleData(int subCmpid,String schema) {
		String sql = String.format(article.getArticleDetails, schema);
		List<Map<String, Object>> articleDetails = database.select(sql,new Object[]{subCmpid},new int[]{ Types.INTEGER });
		return articleDetails;
	}
	
	@Override
	public int updateArticle(int articleId,String articleDescr, String schema) {
		String SQL = String.format(article.updateArticle, schema);
	    int flag=database.update(SQL, new Object[] {articleDescr, articleId }, new int[] {Types.VARCHAR,Types.INTEGER });
		return flag;
	}
}

package com.xenon.help.dao;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.buildmanager.dao.BuildDAOImpl;

public class HelpDAOImpl implements HelpDAO{

	private static final Logger logger = LoggerFactory.getLogger(BuildDAOImpl.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public Map<String, Object> getHelpData() {
		logger.info("Calling Stored procedure to get help data");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getHelpData")
				.withCatalogName("xenonsupport");
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Stored Procedure : Result " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

}

package com.xenon.help.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.xenon.help.domain.SubcomponentDetails;

public interface SubcomponentDAO {

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to insert subcomponent to database
	 * 
	 * @param subcomponentDetails
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int createNewSubcomponent(SubcomponentDetails subcomponentDetails, String schema)
			throws SQLException;

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads component data
	 * 
	 * @param componentId
	 * @return
	 */

	Map<String, Object> getComponentData(int componentId);

	List<Map<String, Object>> getComponentbyiId(int cmpId, String schema);

	List<Map<String, Object>> getSubComponentbyiId(int subCmpId, String schema);

	
	
	
	

}

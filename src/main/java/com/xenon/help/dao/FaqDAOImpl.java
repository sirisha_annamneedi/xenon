package com.xenon.help.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.xenon.common.dao.AdminDashboardDAOImpl;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;
import com.xenon.help.domain.FaqDetails;

public class FaqDAOImpl implements FaqDAO,XenonQuery{

	@Autowired
	DatabaseDao database;

	private static final Logger logger = LoggerFactory.getLogger(AdminDashboardDAOImpl.class);
	
	@Override
	public int insertFaq(FaqDetails faqDetails) {
		String query = faq.insertFaq;
		int status = database.update(query,
				new Object[]{faqDetails.getTitle(),faqDetails.getDescription(),faqDetails.getFaqCreator()}, 
				new int[]{Types.VARCHAR,Types.VARCHAR,Types.INTEGER});
		return status;
	}

	@Override
	public List<Map<String, Object>> viewFaq() {
		String query = faq.viewFaq;
		logger.info("Query to get all faq : "+query);
		List<Map<String,Object>> data =  database.select(query);
		logger.info("Query Result : "+data);
		return data;
	}

	@Override
	public List<Map<String, Object>> viewLastFiveFaq() {
		String query = faq.viewLastFiveFaq;
		logger.info("Query to get all faq : "+query);
		List<Map<String,Object>> data =  database.select(query);
		logger.info("Query Result : "+data);
		return data;
	}

	@Override
	public List<Map<String, Object>> viewSingleFaq(int faqId) {
		String query = faq.viewSingleFaq;
		logger.info("Query to get single faq details : "+query);
		List<Map<String,Object>> data =  database.select(query,
				new Object[]{faqId},
				new int[]{Types.INTEGER});
		logger.info("Query Result : "+data);
		return data;
	}

	
	@Override
	public List<Map<String, Object>> getHelpMenu() {
		String query = faq.getComponent;
		logger.info("");
		List<Map<String,Object>> data =  database.select(query);
		logger.info("");
		return data;
	}

	@Override
	public int updateFaq(int faqId, String faqDescription, String schema) {
		String SQL = String.format(faq.updateFaq, schema);
	    int flag=database.update(SQL, 
	    		new Object[] {faqDescription, faqId }, 
	    		new int[] {Types.VARCHAR, Types.INTEGER });
		return flag;
	}
	
}

package com.xenon.help.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.xenon.help.domain.ArticleDetails;

public interface ArticleDAO {

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to insert article to database
	 * 
	 * @param articleDetails
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int createNewArticle(ArticleDetails articleDetails, String schema)
			throws SQLException;

	/**
	 * 
	 * @param articleId
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getArticle(int articleId, String schema);

	/**
	 * 
	 * @param articleId
	 * @param schema
	 * @return
	 */
	int updateArticle(int articleId,String articleDescr, String schema);

	List<Map<String, Object>> getArticleData(int subCmpid, String schema);

	
	
	
	

}

package com.xenon.help.dao;

import java.util.List;
import java.util.Map;

import com.xenon.help.domain.FaqDetails;

/**
 * 
 * @author prafulla.pol
 * This interface is used to define methods related to the faq operations
 *
 */
public interface FaqDAO {

	/**
	 * @author prafulla.pol
	 * This method is used to insert the FAQ details into the database
	 * @param faq
	 * @return
	 */
	int insertFaq(FaqDetails faq);
	
	/**
	 * @author prafulla.pol
	 * This method is used to view all FAQ's
	 * @return
	 */
	List<Map<String,Object>> viewFaq();
	
	/**
	 * @author prafulla.pol
	 * This method is used to get the last five created FAQ details
	 * @return
	 */
	List<Map<String,Object>> viewLastFiveFaq();
	
	/**
	 * @author prafulla.pol
	 * This method is used to get the single FAQ details
	 * @param faqId
	 * @return
	 */
	List<Map<String,Object>> viewSingleFaq(int faqId);
	
	List<Map<String, Object>> getHelpMenu();
	
	int updateFaq(int faqId, String faqDescription, String schema);
}

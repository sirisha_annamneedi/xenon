package com.xenon.common.dao;

import com.xenon.common.domain.OauthClient;
import com.xenon.common.domain.OauthUser;

public interface OauthUserDAO {

	int insertOauthUser(OauthUser oauthUser);
	int insertOauthClient(OauthClient oauthClient);
}

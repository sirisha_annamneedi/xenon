package com.xenon.common.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.database.DatabaseDao;


public class AccessDAOImpl implements AccessDAO{
	
	private static final Logger logger = LoggerFactory.getLogger(AccessDAOImpl.class);
	
	@Autowired
	DatabaseDao database;

	@Override
	public List<Map<String, Object>> getCoreAccessList() {
		logger.info("Select core access list");
		String sql = "SELECT * from xe_core_access";
		List<Map<String, Object>> coreAccessList =  database.select(sql);
		logger.info("Core access list = "+coreAccessList);
		return coreAccessList;
	}
	
	@Override
	public List<Map<String, Object>> getBMAccessList(String schema , int userRole) {
		logger.info("Select BM access list");
		String sql = "SELECT * from `%s`.xe_bm_access  where role_id = ?";
		List<Map<String, Object>> bmAccessList =  database.select(String.format(sql,schema),new Object[]{userRole},new int[]{Types.INTEGER});
		logger.info("BM access list = "+bmAccessList);
		return bmAccessList;
	}
	
	@Override
	public List<Map<String, Object>> getTMAccessList(String schema , int userRole) {
		logger.info("Select TM access list");
		String sql = "SELECT * from `%s`.xe_tm_access where roleID = ?";
		List<Map<String, Object>> tmAccessList =  database.select(String.format(sql,schema),new Object[]{userRole},new int[]{Types.INTEGER});
		logger.info("TM access list = "+tmAccessList);
		return tmAccessList;
	}
	
	@Override
	public List<Map<String, Object>> getBTAccessList(String schema , int userRole) {
		logger.info("Select BT access list");
		String sql = "SELECT * from `%s`.xe_bt_access where roleID = ?";
		List<Map<String, Object>> btAccessList =  database.select(String.format(sql,schema),new Object[]{userRole},new int[]{Types.INTEGER});
		logger.info("BT access list = "+btAccessList);
		return btAccessList;
	}
	
	@Override
	public List<Map<String, Object>> getSupportAccessList(String schema , int userRole) {
		logger.info("Select support access list");
		String sql = "SELECT * from `%s`.xe_support_access where roleID = ?";
		List<Map<String, Object>> tmAccessList =  database.select(String.format(sql,schema),new Object[]{userRole},new int[]{Types.INTEGER});
		logger.info("Support access list = "+tmAccessList);
		return tmAccessList;
	}
}

package com.xenon.common.dao;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * @author bhagyashri.ajmera
 * 
 * This interface handles all user details operations with customer
 *
 */
public interface UserDetailsDAO {
	
	/**
	 * 
	 * @author bhagyashri.ajmera
	 * 
	 * This method validates customer by user name
	 * 
	 * @param username
	 * @return
	 * @throws ParseException
	 */
	List<Map<String, Object>>  getUserCustomerValidDateByUsername(String username) throws ParseException;
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method reads user by username and password
	 * 
	 * @param userName
	 * @param password
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getUserByUsernameAndPassword(String userName,String password,String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method validates customer status
	 * 
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	List<Map<String, Object>> validateCustomerStatus(String userName)
			throws Exception;
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method get login data
	 * 
	 * @param emailId
	 * @param upassword
	 * @param schema
	 * @return
	 */
	Map<String, Object> callLogin(String username, String upassword,
			String schema);

	List<Map<String, Object>> getUserImageById(int userId, String schema);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method returns customer's ldap details
	 * 
	 * @return
	 */
	Map<String, Object> getCustomerLdapDetails();

	
	/***
	 * @author bhagyashri.ajmera
	 * 
	 * This method retrieves the login user details 
	 * 
	 * @param username
	 * @param schema
	 * @return
	 */
	Map<String, Object> getLoginUserDetails(String username, String schema);
}

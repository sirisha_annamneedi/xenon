package com.xenon.common.dao;

import java.sql.Types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.common.domain.OauthClient;
import com.xenon.common.domain.OauthUser;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

public class OauthUserDAOImpl implements OauthUserDAO, XenonQuery {

	private static final Logger logger = LoggerFactory.getLogger(UtilizationDAOImpl.class);

	@Autowired
	DatabaseDao database;

	@Override
	public int insertOauthUser(OauthUser oauthUserDetails) {
		logger.info("Adding oauth user details to database");
		String sql = String.format(oauthUser.insertOauthUser, "xenonoauth2");
		int flag = database.update(sql, new Object[] { oauthUserDetails.getUserName(), oauthUserDetails.getPassword() },
				new int[] { Types.VARCHAR, Types.VARCHAR });
		return flag;
	}

	@Override
	public int insertOauthClient(OauthClient oauthClient) {
		logger.info("Adding oauth client details to database");
		String sql = String.format(oauthUser.insertOauthClient, "xenonoauth2");
		int flag = database.update(sql, new Object[] { oauthClient.getClientId() },
				new int[] { Types.VARCHAR});
		return flag;
	}
}

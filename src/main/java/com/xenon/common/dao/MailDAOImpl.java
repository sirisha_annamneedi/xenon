package com.xenon.common.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.xenon.api.common.MailService;
import com.xenon.api.common.UtilsService;
import com.xenon.buildmanager.domain.MailDetails;
import com.xenon.buildmanager.domain.MailNotification;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

public class MailDAOImpl implements MailDAO, XenonQuery {

	private static final Logger logger = LoggerFactory.getLogger(MailDAOImpl.class);
	
	@Autowired
	DatabaseDao database;
	
	@Autowired
	UtilsService utilsService;
	
	@Autowired
	MailService mailService;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Resource(name = "mailProperties")
	private Properties mailProperties;
	
	@Resource(name = "configurationProperties")
	private Properties configurationProperties;
	
	@Override
	public int insertSMTPDetails(MailDetails mailDetails,
			String schema) {
		logger.info("insert SMTP details to database");
		String sql = String.format(mail.insertSMTPDetails, schema);
		int flag = database.update(sql, new Object[] { mailDetails.getSmtpHost(), mailDetails.getSmtpPort(), mailDetails.getSmtpUserId(), mailDetails.getSmtpPassword(), mailDetails.getIsEnabled() },
				new int[] { Types.VARCHAR, Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR });
		return flag;
	}

	@Override
	public List<Map<String, Object>> allSMTPDetails(String schema) {
		logger.info("read all SMTP details");
		String sql = String.format(mail.allSMTPDetails, schema);
		List<Map<String, Object>> allSMTPDetails = database.select(sql);
		logger.info("Return list = "+allSMTPDetails);
		return allSMTPDetails;
	}

	@Override
	public List<Map<String, Object>> xenonSMTPDetails() {
		logger.info("read Xenon SMTP details");
		String sql = String.format(mail.xenonSMTPDetails);
		List<Map<String, Object>> allSMTPDetails = database.select(sql);
		logger.info("Return list = "+allSMTPDetails);
		return allSMTPDetails;
	}

	@Override
	public List<Map<String, Object>> getMailNotificationDetails(String schema) {
		logger.info("read mail notification details");
		String sql = String.format(mail.getMailNotificationDetails, schema);
		List<Map<String, Object>> mailNotificationDetails = database.select(sql);
		logger.info("Return list = "+mailNotificationDetails);
		return mailNotificationDetails;
	}

	@Override
	public int updateMailNotificationDetailsBm(MailNotification mailNotificationDetails, String schema) {
		logger.info("update Mail Notification Details BM");
		String sql = String.format(mail.updateMailNotificationDetailsBm, schema);
		int flag = database.update(sql,
				new Object[] { mailNotificationDetails.getCreateUser(), mailNotificationDetails.getAssignProject(),
						mailNotificationDetails.getAssignModule(), mailNotificationDetails.getAssignBuild(),
						mailNotificationDetails.getBuildExecComplete(),
						mailNotificationDetails.getMailNotificationId() },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER });
		logger.info("Return value ="+flag);
		return flag;
	}

	@Override
	public int updateMailNotificationDetailsBt(MailNotification mailNotificationDetails, String schema) {
		logger.info("update Mail Notification Details BT");
		String sql = String.format(mail.updateMailNotificationDetailsBt, schema);
		int flag = database.update(sql,
				new Object[] { mailNotificationDetails.getCreateBug(), mailNotificationDetails.getUpdateBug(),
						mailNotificationDetails.getMailNotificationId() },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER });
		logger.info("Return value ="+flag);
		return flag;
	}

	@Override
	public Map<String, Object> getMailSettings(String schema) {
		logger.info("read mail setting data");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getMailSettings")
				.withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Return list ="+simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public int updateSmtpDetails(MailDetails mailDetails, String schema) {
		String sql = String.format(mail.updateSmtp, schema);
		int flag = database.update(sql, new Object[] { mailDetails.getSmtpHost(), mailDetails.getSmtpPort(), mailDetails.getSmtpUserId(), mailDetails.getSmtpPassword(), mailDetails.getIsEnabled(), mailDetails.getId() },
				new int[] { Types.VARCHAR, Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER });
		return flag;
		
	}

	@Override
	public Map<String, Object> testEmailSettings(int userId,String schema) {
		logger.info("Reading mail setting data to test the email details");
		
		SqlParameter user = new SqlParameter("userId", Types.INTEGER);
		SqlParameter[] paramArray = { user };
		
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("mailOnSettingsTest")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(userId);
		
		logger.info("Data ="+simpleJdbcCallResult);
		
		return simpleJdbcCallResult;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String sendTestMail(int userId, Map<String, Object> emailDetails) {
		logger.info("Sending the user test mail to check his mail details");
		
		List<Map<String, Object>> recipientData = (List<Map<String, Object>>) emailDetails.get("#result-set-1");
		List<Map<String, Object>> settingData = (List<Map<String, Object>>) emailDetails.get("#result-set-2");
		
		String receipant = recipientData.get(0).get("email_id").toString();

		final String smtpHost = settingData.get(0).get("smtp_host").toString();
		final String smtpUserId = settingData.get(0).get("smtp_user_id").toString();
		final String smtpPassword = settingData.get(0).get("smtp_password").toString();
		
		String mailSubject = mailProperties.getProperty("EMAIL_SETTINGS_TEST_SUBJECT", "Email settings test");
		String mailBodyText = mailProperties.getProperty("EMAIL_SETTINGS_TEST_BODY_TEXT", "None");
		
		Properties props = new Properties();
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.ssl.trust", smtpHost);

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(smtpUserId, smtpPassword);
			}
		});
		
		MimeMessage message = new MimeMessage(session);
		try {
			message.setFrom(new InternetAddress(smtpUserId));
			message.setRecipients(Message.RecipientType.TO, receipant);
			message.setSubject(mailSubject);
			message.setContent(mailBodyText, "text/html; charset=utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		try {
			Transport.send(message);
			return "success";
			
		} catch (Exception e) {
			e.printStackTrace();
			/*session = null;
			message = null;
			props = null;*/
			return "failure";
		}
	}
	
	@Override
	public Map<String,Object> getMailProperties(){
		
		Map<String,Object> mailProperties=new HashMap<String, Object>();
		
		mailProperties.put("SMTP_HOST", configurationProperties.get("SMTP_HOST"));
		mailProperties.put("SMTP_USER", configurationProperties.get("SMTP_USER"));
		mailProperties.put("SMTP_PASS", configurationProperties.get("SMTP_PASS"));
		mailProperties.put("SMTP_ENABLE", configurationProperties.get("SMTP_ENABLE"));
		return mailProperties;
		
	}
	
	@Override
	public void sendMailonExecption(Exception ex, HttpSession session) {
		// send mail to xenon admin
		//System.out.println("-----Execption log-----------------");
		utilsService.getLoggerUser(session);
		logger.info("-----Start Execption log-----------------");
		logger.info(printStacktrace(ex));
		logger.info("-----End Execption log-----------------");

		List<Map<String, Object>> emailDetails = new ArrayList<Map<String, Object>>();
		Map<String, Object> email = new HashMap<String, Object>();

		email.put("smtp_host", configurationProperties.get("SMTP_HOST"));
		email.put("smtp_user_id", configurationProperties.get("SMTP_USER"));
		email.put("smtp_password", configurationProperties.get("SMTP_PASS"));
		email.put("is_enabled",  configurationProperties.get("SMTP_ENABLE"));
		emailDetails.add(email);

		ArrayList<String> recipients = new ArrayList<String>();
		recipients.add("xenon.developers@jadeglobal.com");
		String cust_name = session.getAttribute("customerName").toString();
		String user_name = session.getAttribute("userFullName").toString();
		String subject = "Exception : User - " + user_name + " Customer - " + cust_name;
		String bodyText = printStacktrace(ex);
		String from = "";
		try {

			mailService.postMail(recipients, subject, bodyText, from);

		} catch (Exception e) {
			e.printStackTrace();
		}
		ex.printStackTrace();
		//System.out.println(ex);
		//System.out.println("-----Execption log-----------------");
	}
	
	/**
	 * This method prints stacktrace
	 * 
	 * @param ex
	 * @return
	 */
	public static String printStacktrace(Exception ex) {
		logger.info("prints stacktrace");
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		return stringWriter.toString();
	}
}

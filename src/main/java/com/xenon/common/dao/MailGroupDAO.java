package com.xenon.common.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import com.xenon.buildmanager.domain.MailGroupDetails;
/**
 * 
 * @author suresh.adling
 * @description Interface to perform JDBC operations on mail group
 * 
 */
public interface MailGroupDAO {
	/**
	 * @author suresh.adling
	 * @description get data for mail group
	 * @param schema
	 * @return
	 */
	Map<String, Object> getmailGroupData(String schema);
	
	/**
	 * @author suresh.adling
	 * @description insert mail group record(users)
	 * @param mailGroup
	 * @param schema
	 */
	void insertMailGroupRecords(List<MailGroupDetails> mailGroup, String schema);
	
	/**
	 * @author suresh.adling
	 * @description update mail group record(users)
	 * @param mailGroup
	 * @param schema
	 * @param status
	 */
	void updateMailGroupRecords(List<MailGroupDetails> mailGroup, String schema,int status);
	
	/**
	 * @author suresh.adling
	 * @description insert mail group(group name)
	 * @param mailGroupName
	 * @param schema
	 * @return
	 * @throws SQLException
	 */
	int insertMailGroup(String mailGroupName, String schema) throws SQLException;
	
	/**
	 * @author suresh.adling
	 * @description update mail group(group name)
	 * @param mailGroupId
	 * @param mailGroupName
	 * @param schema
	 * @return
	 */
	int updateMailGroupDetails(int mailGroupId,String mailGroupName, String schema);
	/**
	 * @author suresh.adling
	 * @description update mail group
	 * @param mailGroupId
	 * @param mailGroupName
	 * @param schema
	 * @return integer
	 * @throws SQLException
	 */
	int updateMailGroup(int mailGroupId,String mailGroupName, String schema) throws SQLException;
	/**
	 * @author suresh.adling
	 * @description get get user list by mail group id
	 * @param mailGroupId
	 * @param schema
	 * @return List Map 
	 */
	List<Map<String, Object>> getMailGroupUsers(int mailGroupId, String schema);
	
}

package com.xenon.common.dao;

import com.xenon.common.domain.Utilization;

public interface UtilizationDAO {
	/**
	 * @author navnath.damale
	 * @param usage
	 * @param schema
	 * @return
	 */
	int updateUtilization(Utilization usage, String schema);
	
}

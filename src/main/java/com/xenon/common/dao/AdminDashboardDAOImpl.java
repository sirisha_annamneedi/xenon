package com.xenon.common.dao;

import java.sql.Types;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

/**
 * 
 * @author suresh.adling
 * @description DAO implementation class to perform JDBC operations to get data
 *              for admin dashboard
 * 
 */
public class AdminDashboardDAOImpl implements AdminDashboardDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	//SimpleJdbcCall call=new SimpleJdbcCall(jdbcTemplate);
	private static final Logger logger = LoggerFactory.getLogger(AdminDashboardDAOImpl.class);

	@Override
	public Map<String, Object> getAdminDashData(int startValue, int offsetValue, String schema, int typeId, String custId) {
		logger.info("In a method to decalre procedure for get admin dahboard ");
		SqlParameter start = new SqlParameter("startValue", Types.INTEGER);
		SqlParameter off = new SqlParameter("offsetValue", Types.INTEGER);
		SqlParameter custSchema = new SqlParameter("schema", Types.VARCHAR);
		SqlParameter custTypeId = new SqlParameter("typeId", Types.INTEGER);
		SqlParameter custIdTypeId = new SqlParameter("custId", Types.INTEGER);
		SqlParameter[] paramArray = { custSchema, custTypeId, custIdTypeId };
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("admindashboard")
				.declareParameters(paramArray).withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(startValue, offsetValue, schema, typeId, custId);
		logger.info("Result after procedure call " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
}

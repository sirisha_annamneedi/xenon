package com.xenon.common.dao.exceptions;

public class LdapAuthenticationException extends Exception {
	
	private static final long serialVersionUID = 930509421765650251L;

	/**
	 * constructor
	 * 
	 */
	public LdapAuthenticationException() {
	}

	/**
	 * 
	 * @param msg
	 * message
	 */
	public LdapAuthenticationException(String msg) {
		super();
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Ldap Authentication Exception ="+getMessage();
	}
}

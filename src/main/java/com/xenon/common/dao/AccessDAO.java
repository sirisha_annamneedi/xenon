package com.xenon.common.dao;

import java.util.List;
import java.util.Map;

/**
 * @author bhagyashri.ajmera
 * 
 * This interface is handles access related operations
 */
public interface AccessDAO {
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to read core access list
	 * 
	 * @return
	 */
	public List<Map<String,Object>> getCoreAccessList();
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives BM access list
	 * 
	 * @param schema
	 * @param userRole
	 * @return
	 */
	public List<Map<String,Object>> getBMAccessList(String schema , int userRole);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives TM access list
	 * 
	 * @param schema
	 * @param userRole
	 * @return
	 */
	public List<Map<String,Object>> getTMAccessList(String schema , int userRole);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives BT access list
	 * 
	 * @param schema
	 * @param userRole
	 * @return
	 */
	
	public List<Map<String,Object>> getBTAccessList(String schema , int userRole);

	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method gives support access list
	 * 
	 * @param schema
	 * @param userRole
	 * @return
	 */
	List<Map<String, Object>> getSupportAccessList(String schema, int userRole);

}

package com.xenon.common.dao;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.xenon.buildmanager.domain.MailDetails;
import com.xenon.buildmanager.domain.MailNotification;

public interface MailDAO {
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to insert SMTP details to database
	 * 
	 * @param mailDetails
	 * @param schema
	 * @return
	 */
	int insertSMTPDetails(MailDetails mailDetails, String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to update SMTP details
	 * 
	 * @param mailDetails
	 * @param schema
	 * @return
	 */
	int updateSmtpDetails(MailDetails mailDetails, String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to read all SMTP details
	 * 
	 * @param schema
	 * @return
	 */
	List<Map<String,Object>> allSMTPDetails(String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to read Xenon SMTP details
	 * 
	 * @return
	 */
	List<Map<String, Object>> xenonSMTPDetails();
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This mathod is to read mail notification details
	 * 
	 * @param schema
	 * @return
	 */
	List<Map<String, Object>> getMailNotificationDetails(String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to update Mail Notification Details BM
	 * 
	 * @param mailNotificationDetails
	 * @param schema
	 * @return
	 */
	int updateMailNotificationDetailsBm(
			MailNotification mailNotificationDetails, String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to update Mail Notification Details BT
	 * 
	 * @param mailNotificationDetails
	 * @param schema
	 * @return
	 */
	int updateMailNotificationDetailsBt(
			MailNotification mailNotificationDetails, String schema);
	
	/**
	 * @author bhagyashri.ajmera
	 * 
	 * This method is to read mail setting data
	 * 
	 * @param schema
	 * @return
	 */
	Map<String, Object> getMailSettings(String schema);

	/**
	 * @author prafulla.pol
	 * This method is used to get the get the email settings details
	 * @param userId
	 * @param schema
	 * @return
	 */
	Map<String,Object> testEmailSettings(int userId,String schema);
	
	/**
	 * @author prafulla.pol
	 * This method is used to send test mail to check the mail settings
	 * @param userId
	 * @param emailDetails
	 * @return
	 */
	String sendTestMail(int userId,Map<String,Object> emailDetails);

	Map<String, Object> getMailProperties();

	void sendMailonExecption(Exception ex, HttpSession session);
	
}

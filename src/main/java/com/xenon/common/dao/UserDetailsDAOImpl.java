package com.xenon.common.dao;

import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import com.xenon.core.dao.CustomerDAO;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

public class UserDetailsDAOImpl implements UserDetailsDAO, XenonQuery {

	private static final Logger logger = LoggerFactory.getLogger(UserDetailsDAOImpl.class);
	
	@Autowired
	DatabaseDao database;

	@Autowired
	CustomerDAO custDAO;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Map<String, Object>> getUserCustomerValidDateByUsername(String userName) throws ParseException {
		logger.info("Validates customer by user name");
		String sql = "SELECT customer_schema ,cust.customer_name,ct.customer_type,cust.customer_id,cust.customer_logo,ct.id as cust_type_id,cust.start_date,cust.end_date,cust.customer_id FROM `xenon-core`.xe_user as us , `xenon-core`.xe_customer_details as cust ,`xenon-core`.xe_customer_type as ct where us.customer_id = cust.customer_id  and cust.customer_type =ct.id and us.user_email= ?";

		List<Map<String, Object>> listOfPerson = database.select(sql, new Object[] { userName },
				new int[] { Types.VARCHAR });
		logger.info("Return List ="+listOfPerson);
		return listOfPerson;
	}

	@Override
	public List<Map<String, Object>> getUserByUsernameAndPassword(String userName, String password, String schema) {
		logger.info("Reads user by username and password");
		String sql = String.format(lguser.validateUser, schema, schema);
		List<Map<String, Object>> listOfPerson = database.select(sql, new Object[] { password, userName },
				new int[] { Types.VARCHAR, Types.VARCHAR });
		logger.info("Return List ="+listOfPerson);
		return listOfPerson;
	}

	@Override
	public List<Map<String, Object>> validateCustomerStatus(String userName) throws ParseException {
		logger.info("Validating customer status");
		String sql = lguser.validateCustomerStatus;
		logger.info("Query Param userName = {}",userName);
		List<Map<String, Object>> custDetails = database.select(sql, new Object[] { userName },
				new int[] { Types.VARCHAR });
		if (custDetails.size() > 0) {
			logger.info("Validating customer status for customer expire or not");
			int custId = Integer.parseInt(custDetails.get(0).get("customer_id").toString());
			String endDate = custDetails.get(0).get("end_date").toString();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date1 = new Date();
			Date date2 = sdf.parse(endDate);
			if (date1.before(date2)) {
				//get the cloud space use for customer in MB
				String schemaName = custDetails.get(0).get("customer_schema").toString();
				logger.info("Reading cloud space");
				Float maxSpace = Float.parseFloat(custDetails.get(0).get("max_cloud_space").toString());
				String spaceQuery = lguser.getCustomerUsedCloudSpace;
				List<Map<String, Object>> cloudSpace = database.select(spaceQuery, new Object[] { schemaName },
						new int[] { Types.VARCHAR });
				float usedSpace = Float.parseFloat(cloudSpace.get(0).get("Used_Space").toString());
				if(usedSpace < maxSpace)
					custDetails.get(0).put("StatusFalg", 1);
				else{
					logger.info("Customer date expired");
					custDetails.get(0).put("StatusFalg", 2);
					custDAO.updateCustStatus(custId);
				}
			}
			else
			{
				logger.info("Customer status expired");
				custDetails.get(0).put("StatusFalg", 2);
				custDAO.updateCustStatus(custId);
			}
		} else {
			logger.info("Return List ="+custDetails);
			return custDetails;
		}
		logger.info("Return List ="+custDetails);
		return custDetails;
	}
	
	
	@Override
	public Map<String, Object> callLogin(String username,String upassword,String schema) {
		logger.info("Reading login data");
		SqlParameter fNameParam = new SqlParameter("userName", Types.VARCHAR);
		SqlParameter lNameParam = new SqlParameter("upassword", Types.VARCHAR);
		SqlParameter[] paramArray = {fNameParam, lNameParam};
		logger.info("Query Param fNameParam={}  ,  lNameParam = {}",fNameParam,lNameParam);
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getLogin").declareParameters(paramArray).withCatalogName(schema);
 
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(username,upassword);
		logger.info("Return list ="+simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	
	@Override
	public List<Map<String, Object>> getUserImageById(int userId, String schema) {
		logger.info("Reads user by username and password");
		String sql = String.format(lguser.getUserImageById, schema);
		List<Map<String, Object>> userPhoto = database.select(sql, new Object[] { userId },
				new int[] { Types.INTEGER });
		return userPhoto;
	}

	@Override
	public Map<String, Object> getCustomerLdapDetails() {
		logger.info("Reading login data");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getCustomerLdapDetails").withCatalogName("`xenon-core`");
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Return list ="+simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	
	@Override
	public Map<String, Object> getLoginUserDetails(String username,String schema) {
		logger.info("Reading login data");
		SqlParameter userNameTemp = new SqlParameter("userName", Types.VARCHAR);
		SqlParameter[] paramArray = {userNameTemp};
		logger.info("Query Param fNameParam={} ",userNameTemp);
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getLoginUserDetails").declareParameters(paramArray).withCatalogName(schema);
 
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(username);
		logger.info("Return list ="+simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}
	
}

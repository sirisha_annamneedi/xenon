package com.xenon.common.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import com.xenon.buildmanager.domain.MailGroupDetails;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

/**
 * 
 * @author suresh.adling
 * @description DAO Implementation to perform JDBC operations on mail group
 * 
 */
public class MailGroupDAOImpl implements MailGroupDAO, XenonQuery {

	@Autowired
	DatabaseDao database;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LoggerFactory.getLogger(MailGroupDAOImpl.class);

	@Override
	public Map<String, Object> getmailGroupData(String schema) {
		logger.info("In a method to decalre procedure for get mail group data");
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("mailgroup")
				.declareParameters().withCatalogName(schema);

		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		logger.info("Result after procedure call " + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	@Override
	public int insertMailGroup(String mailGroupName, String schema) throws SQLException {
		logger.info("In a method to decalre function for insert mail group");
		String callInsert = String.format(mail.createNewMailGroup, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsert);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, mailGroupName);
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		logger.info("Result after method call " + insertStatus);
		return insertStatus;

	}

	@Override
	public void insertMailGroupRecords(final List<MailGroupDetails> mailGroups, String schema) {

		logger.info(
				"In a method to decalre batch update for function to insert mail group records(mail group id,userid) ");
		String sql = String.format(mail.insertMailGroupRecords, schema);

		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public int getBatchSize() {
				return mailGroups.size();
			}

			@Override
			public void setValues(java.sql.PreparedStatement ps, int i) throws SQLException {
				MailGroupDetails mail = mailGroups.get(i);
				ps.setInt(1, mail.getUserId());
				ps.setInt(2, mail.getMailGroupId());

			}
		});
	}

	@Override
	public void updateMailGroupRecords(final List<MailGroupDetails> mailGroups, String schema, int status) {
		logger.info("In a method update mail group records ");
		String SQL = String.format(mail.updateMailGroups, schema);
		if (mailGroups.get(0) != null) {
			database.update(SQL, new Object[] { mailGroups.get(0).getMailGroupId() }, new int[] { Types.INTEGER });
		}
		if (status == 1) {
			String sql = String.format(mail.insertMailGroupRecords, schema);
			jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return mailGroups.size();
				}

				@Override
				public void setValues(java.sql.PreparedStatement ps, int i) throws SQLException {
					MailGroupDetails mail = mailGroups.get(i);
					ps.setInt(1, mail.getUserId());
					ps.setInt(2, mail.getMailGroupId());

				}
			});
		}
	}

	@Override
	public int updateMailGroupDetails(int mailGroupId, String mailGroupName, String schema) {
		logger.info("In a method update mail group details (name)");
		logger.info("SQL : " + mail.updateMailGroupDetails);
		String sql = String.format(mail.updateMailGroupDetails, schema);
		int flag = database.update(sql, new Object[] { mailGroupName, mailGroupId },
				new int[] { Types.VARCHAR, Types.INTEGER });
		logger.info("Reult : " + flag);
		return flag;
	}

	@Override
	public int updateMailGroup(int mailGroupId, String mailGroupName, String schema) throws SQLException {
		logger.info("In a method to decalre function to update mail group ");
		String callInsert = String.format(mail.updateMailGroup, schema);
		Connection dbConnection = database.getConnection();
		CallableStatement callableStmt = dbConnection.prepareCall(callInsert);
		callableStmt.registerOutParameter(1, Types.INTEGER);
		callableStmt.setString(2, mailGroupName);
		callableStmt.setInt(3, mailGroupId);
		callableStmt.execute();
		int insertStatus = callableStmt.getInt(1);
		callableStmt.close();
		return insertStatus;

	}

	@Override
	public List<Map<String, Object>> getMailGroupUsers(int mailGroupId, String schema) {
		logger.info("In a method to decalre procedure for get mail group users");
		String sql = String.format(mail.getMailGroupUsers, schema, schema);
		List<Map<String, Object>> userList = database.select(sql, new Object[] { mailGroupId },
				new int[] { Types.INTEGER });
		return userList;
	}
}

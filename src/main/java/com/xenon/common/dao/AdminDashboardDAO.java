package com.xenon.common.dao;

import java.util.Map;

/**
 * 
 * @author suresh.adling
 * @description Interface to perform JDBC operations to get data on admin
 *              dashboard
 * 
 */
public interface AdminDashboardDAO {
	/**
	 * @author suresh.adling
	 * @description get admin dashboard data form procedure
	 * @param schema
	 * @param typeId
	 * @returns
	 */
	Map<String, Object> getAdminDashData(int startValue, int offsetValue, String schema, int typeId,String custId);
}

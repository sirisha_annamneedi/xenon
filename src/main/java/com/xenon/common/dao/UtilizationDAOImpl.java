package com.xenon.common.dao;

import java.sql.Types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.common.domain.Utilization;
import com.xenon.database.DatabaseDao;
import com.xenon.database.XenonQuery;

public class UtilizationDAOImpl implements UtilizationDAO, XenonQuery {

	private static final Logger logger = LoggerFactory.getLogger(UtilizationDAOImpl.class);
	
	@Autowired
	DatabaseDao database;

	@Override
	public int updateUtilization(Utilization bandwidthUsage, String schema) {
		logger.info("Update bandwidth usage details to database");
		String sql = String.format(usage.updateUsage, schema);
		int flag = database.update(sql, new Object[] {bandwidthUsage.getBandwidthUsage(),bandwidthUsage.getUserName()},new int[] {Types.DOUBLE,Types.VARCHAR});
		return flag;
	}
}

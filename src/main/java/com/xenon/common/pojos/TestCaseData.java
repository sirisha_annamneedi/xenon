package com.xenon.common.pojos;

import java.util.List;


public class TestCaseData {

	private String testPrefixId;
	private List<TestCaseDataDetails> tcDataDetails;
	
	public String getTestPrefixId() {
		return testPrefixId;
	}
	public void setTestPrefixId(String testPrefixId) {
		this.testPrefixId = testPrefixId;
	}
	public List<TestCaseDataDetails> getTcDataDetails() {
		return tcDataDetails;
	}
	public void setTcDataDetails(List<TestCaseDataDetails> tcDataDetails) {
		this.tcDataDetails = tcDataDetails;
	}
	
	
	
	
	
}

package com.xenon.common.pojos;

public class ReportBuildDetails {

	private String buildName, buildDesc, currentDate;
	private int buildStatus, buildExecType;
	
	public String getBuildName() {
		return buildName;
	}
	public void setBuildName(String buildName) {
		this.buildName = buildName;
	}
	public String getBuildDesc() {
		return buildDesc;
	}
	public void setBuildDesc(String buildDesc) {
		this.buildDesc = buildDesc;
	}
	public String getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}
	public int getBuildStatus() {
		return buildStatus;
	}
	public void setBuildStatus(int buildStatus) {
		this.buildStatus = buildStatus;
	}
	public int getBuildExecType() {
		return buildExecType;
	}
	public void setBuildExecType(int buildExecType) {
		this.buildExecType = buildExecType;
	}
   
	
}

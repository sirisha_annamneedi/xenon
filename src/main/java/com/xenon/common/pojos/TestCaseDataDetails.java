package com.xenon.common.pojos;

public class TestCaseDataDetails {
 
	private String iterationCount,takeScreenshot,screenshotLink,executedTimestamp,stepDescription,
	raiseBug,buildId,value,actualResult,status,dirName,screenshotTitle;

	public String getIterationCount() {
		return iterationCount;
	}

	public void setIterationCount(String iterationCount) {
		this.iterationCount = iterationCount;
	}

	public String getTakeScreenshot() {
		return takeScreenshot;
	}

	public void setTakeScreenshot(String takeScreenshot) {
		this.takeScreenshot = takeScreenshot;
	}

	public String getScreenshotLink() {
		return screenshotLink;
	}

	public void setScreenshotLink(String screenshotLink) {
		this.screenshotLink = screenshotLink;
	}

	public String getExecutedTimestamp() {
		return executedTimestamp;
	}

	public void setExecutedTimestamp(String executedTimestamp) {
		this.executedTimestamp = executedTimestamp;
	}

	public String getStepDescription() {
		return stepDescription;
	}

	public void setStepDescription(String stepDescription) {
		this.stepDescription = stepDescription;
	}

	public String getRaiseBug() {
		return raiseBug;
	}

	public void setRaiseBug(String raiseBug) {
		this.raiseBug = raiseBug;
	}

	public String getBuildId() {
		return buildId;
	}

	public void setBuildId(String buildId) {
		this.buildId = buildId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getActualResult() {
		return actualResult;
	}

	public void setActualResult(String actualResult) {
		this.actualResult = actualResult;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDirName() {
		return dirName;
	}

	public void setDirName(String dirName) {
		this.dirName = dirName;
	}

	public String getScreenshotTitle() {
		return screenshotTitle;
	}

	public void setScreenshotTitle(String screenshotTitle) {
		this.screenshotTitle = screenshotTitle;
	}
	
	
    
}

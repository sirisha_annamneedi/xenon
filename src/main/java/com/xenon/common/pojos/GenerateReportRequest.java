package com.xenon.common.pojos;

import java.util.List;

public class GenerateReportRequest {

	private String projectName;
	private String releaseName;
	private List<String> testPrefixList;
	private ReportBuildDetails reportBuildDetails;
	private List<TestCaseData> testCaseDataList;
	
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getReleaseName() {
		return releaseName;
	}
	public void setReleaseName(String releaseName) {
		this.releaseName = releaseName;
	}
	public List<String> getTestPrefixList() {
		return testPrefixList;
	}
	public void setTestPrefixList(List<String> testPrefixList) {
		this.testPrefixList = testPrefixList;
	}
	public ReportBuildDetails getReportBuildDetails() {
		return reportBuildDetails;
	}
	public void setReportBuildDetails(ReportBuildDetails reportBuildDetails) {
		this.reportBuildDetails = reportBuildDetails;
	}
	public List<TestCaseData> getTestCaseDataList() {
		return testCaseDataList;
	}
	public void setTestCaseDataList(List<TestCaseData> testCaseDataList) {
		this.testCaseDataList = testCaseDataList;
	}
	
	
}

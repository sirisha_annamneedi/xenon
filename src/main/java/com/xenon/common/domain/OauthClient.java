package com.xenon.common.domain;

public class OauthClient {

	private int id;
	private String clientId;
	private String clientSecrete;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getClientSecrete() {
		return clientSecrete;
	}
	public void setClientSecrete(String clientSecrete) {
		this.clientSecrete = clientSecrete;
	}
}

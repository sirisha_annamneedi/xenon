package com.xenon.common.domain;

public class Utilization {
	private int id;
	private String userName;
	private double bandwidthUsage;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public double getBandwidthUsage() {
		return bandwidthUsage;
	}
	public void setBandwidthUsage(double bandwidthUsage) {
		this.bandwidthUsage = bandwidthUsage;
	}
	
}

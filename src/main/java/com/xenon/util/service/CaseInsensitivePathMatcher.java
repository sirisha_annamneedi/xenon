package com.xenon.util.service;
import java.util.Map;
import org.springframework.util.AntPathMatcher;

/**
 * 
 * @author prafulla.pol
 * @Description CaseInsensitivePathMatcher class is used for handling case insensitive url
 */
public class CaseInsensitivePathMatcher extends AntPathMatcher {
    
	/**
	 * @author prafulla.pol
	 * @method doMatch
	 * @return boolean status of Case Insensitive Path Matching 
	 */
	@Override
    protected boolean doMatch(String pattern, String path, boolean fullMatch, Map<String, String> uriTemplateVariables) {
        return super.doMatch(pattern.toLowerCase(), path.toLowerCase(), fullMatch, uriTemplateVariables);
    }
}
package com.xenon.util.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.PathMatcher;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * 
 * @author prafulla.pol
 * @Description CaseInsensitiveAnnotationHandlerMapping class is used for handling case insensitive url
 *
 */
@Configuration
public class CaseInsensitiveAnnotationHandlerMapping  extends WebMvcConfigurationSupport{
	
	/**
	 * @author prafulla.pol
	 * @method pathMatcher
	 * @return object of CaseInsensitiveAnnotationHandlerMapping class
	 */
	@Bean
    public PathMatcher pathMatcher(){
        return new CaseInsensitivePathMatcher();
    }

	/**
	 * @author prafulla.pol
	 * @method requestMappingHandlerMapping
	 * @return object of RequestMappingHandlerMapping class
	 */
    @Bean
    public RequestMappingHandlerMapping requestMappingHandlerMapping() {
        RequestMappingHandlerMapping handlerMapping = new RequestMappingHandlerMapping();
        handlerMapping.setOrder(0);
        handlerMapping.setInterceptors(getInterceptors());
        handlerMapping.setPathMatcher(pathMatcher());
        return handlerMapping;
    }
}

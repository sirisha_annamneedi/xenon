package com.xenon.util.service;

import javax.naming.AuthenticationException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;

public class JiraRESTClient {
	public JiraRESTClient() {
		
	}
	public static String BASE_URL = "http://jira.jadeglobal.com:8080";

	public static void main(String[] args) throws AuthenticationException {
		
		//String auth = new String(Base64.encode("abhay.thakur:Akt@2017"));
		
		String auth = new String(Base64.encode("anmol chadha:Admin@123"));
		
		try {
			//get all issues
//			String createIssueData = "{\"fields\":{\"project\":{\"key\":\"XNI\"}}";
//			String issue = invokeGetMethod(auth, BASE_URL+"/rest/api/2/search?jql=project=XNI&startAt=1&maxResults=10&fields=id,key,summary,description");
//			System.out.println(issue);
//			JSONObject issueObj = new JSONObject(issue);
//			String newKey = issueObj.getString("key");
//			System.out.println("Key:"+newKey);
//			
			//Get Projects
//			String projects = invokeGetMethod(auth, BASE_URL+"/rest/api/2/project");
//			System.out.println(projects);	
//			JSONArray projectArray = new JSONArray(projects);
//			for (int i = 0; i < projectArray.length(); i++) {
//				JSONObject proj = projectArray.getJSONObject(i);
//				System.out.println("Key:"+proj.getString("key")+", Name:"+proj.getString("name"));
//			}
//			JSONObject issuetype =new JSONObject();
//			issuetype.put("name", "Bug");
//			JSONObject project =new JSONObject();
//			project.put("key", "XNI");
//			JSONObject fields =new JSONObject();
//			fields.put("summary", "REST DEMO");
//			fields.put("project", project);
//			fields.put("issuetype", issuetype);
//			JSONObject request =new JSONObject();
//			request.put("fields", fields);
			//Create Issue
	//	String createIssueData = "{\"fields\":{\"project\":{\"key\":\"XNI\"},\"summary\":\"REST DEMO_TEST\",\"issuetype\":{\"name\":\"Bug\"}}}";
//			String issue = invokePostMethod(auth, BASE_URL+"/rest/api/2/issue", request.toString());
//			System.out.println(issue);
//			JSONObject issueObj = new JSONObject(issue);
//			String newKey = issueObj.getString("key");
//			System.out.println("Key:"+newKey);
//			
//			//Update Issue
			String statusData = "{\"update\": { \"status\": [{ \"update\": { \"name\": \"Closed\"}}] }, \"transition\": {\"status\": \"Closed\"}}";
			String editIssueData=" {\"fields\":{\"assignee\":{\"name\":\"Abhay Thakur\"}}}";
			//invokePutMethod(auth, BASE_URL+"/rest/api/2/issue/XNI-204", statusData);
// invokePutMethod(auth,  BASE_URL+"/rest/api/2/issue/XNI-199", editIssueData);
//	invokeDeleteMethod(auth, BASE_URL+"/rest/api/2/issue/XNI-198");
			invokeGetMethod(auth, BASE_URL+"/rest/api/2/issue/XNI-204/transitions/?expand=transitions.fields&transitionId=5");	
//System.out.println(request);
		} catch (ClientHandlerException e) {
			System.out.println("Error invoking REST method");
			e.printStackTrace();
		} catch (JSONException e) {
			System.out.println("Invalid JSON output");
			e.printStackTrace();
		}

	}

	public static String invokeGetMethod(String auth, String url) throws AuthenticationException, ClientHandlerException {
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json")
				.accept("application/json").get(ClientResponse.class);
		int statusCode = response.getStatus();
		if (statusCode == 401) {
			throw new AuthenticationException("Invalid Username or Password");
		}
		System.out.println(response);
		return response.getEntity(String.class);
	}
	
	public static String invokePostMethod(String auth, String url, String data) throws AuthenticationException, ClientHandlerException {
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json")
				.accept("application/json").post(ClientResponse.class, data);
		int statusCode = response.getStatus();
		if (statusCode == 401) {
			throw new AuthenticationException("Invalid Username or Password");
		}else if(statusCode == 400) {
			throw new AuthenticationException("Bad Request.");
		}
		System.out.println(response);
		return response.getEntity(String.class);
	}
	
	
	public static void updatePostMethod(String auth, String url, String data) throws AuthenticationException, ClientHandlerException {
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json")
				.accept("application/json").post(ClientResponse.class, data);
		int statusCode = response.getStatus();
		if (statusCode == 401) {
			throw new AuthenticationException("Invalid Username or Password");
		}else if(statusCode == 400) {
			throw new AuthenticationException("Bad Request.");
		}
		System.out.println(statusCode);
		
	}

	
	public static void updatePutMethod(String auth, String url, String data) throws AuthenticationException, ClientHandlerException {
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json")
				.accept("application/json").post(ClientResponse.class, data);
		int statusCode = response.getStatus();
		if (statusCode == 401) {
			throw new AuthenticationException("Invalid Username or Password");
		}else if(statusCode == 400) {
			throw new AuthenticationException("Bad Request.");
		}
		System.out.println(statusCode);
		
	}
	
	public static void updatePutMethod1(String auth, String url, String data) throws AuthenticationException, ClientHandlerException {
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json")
				.accept("application/json").put(ClientResponse.class, data);
		int statusCode = response.getStatus();
		if (statusCode == 401) {
			throw new AuthenticationException("Invalid Username or Password");
		}else if(statusCode == 400) {
			throw new AuthenticationException("Bad Request.");
		}
		System.out.println(statusCode);
		
	}
	
//	public static void updatePutMethod2(String auth, String url, String data) throws AuthenticationException, ClientHandlerException {
//		Client client = Client.create();
//		WebResource webResource = client.resource(url);
//		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json")
//				.accept("application/json").put(ClientResponse.class, data);
//		int statusCode = response.getStatus();
//		if (statusCode == 401) {
//			throw new AuthenticationException("Invalid Username or Password");
//		}else if(statusCode == 400) {
//			throw new AuthenticationException("Bad Request.");
//		}
//		System.out.println(statusCode);
//		
//	}
	
	@SuppressWarnings("unused")
	public static String createBug(String auth, String url, String data) throws AuthenticationException, ClientHandlerException {
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json")
				.accept("application/json").post(ClientResponse.class, data);
		int statusCode = response.getStatus();
		if (statusCode == 401) {
			throw new AuthenticationException("Invalid Username or Password");
		}
		return response.getEntity(String.class);
	}
	public static void invokePutMethod(String auth, String url, String data) throws AuthenticationException, ClientHandlerException {
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json")
				.accept("application/json").put(ClientResponse.class, data);
		System.out.println(response);

		int statusCode = response.getStatus();
		if (statusCode == 401) {
			throw new AuthenticationException("Invalid Username or Password");
		}
	}
	
	public static void invokeDeleteMethod(String auth, String url) throws AuthenticationException, ClientHandlerException {
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json")
				.accept("application/json").delete(ClientResponse.class);
		int statusCode = response.getStatus();
		if (statusCode == 401) {
			throw new AuthenticationException("Invalid Username or Password");
		}
	}

}

package com.xenon.util.service;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author prafulla.pol
 * @Description NoCacheFilter class is used as filter
 *
 */
@WebFilter
public class NoCacheFilter implements Filter {

	/**
	 * @author prafulla.pol
	 * @method doFilter
	 */
	@SuppressWarnings("unused")
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse) res;
		HttpServletRequest request = (HttpServletRequest) req;
		response.setHeader("Pragma", "cache");
		response.setDateHeader("Expires", 200000);
		chain.doFilter(req, res);
	}

	/**
	 * @author prafulla.pol
	 * @method init
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	/**
	 * @author prafulla.pol 
	 * @method destroy
	 */
	@Override
	public void destroy() {

	}

}
package com.xenon.util.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
/**
 * 
 * @author navnath.damale
 *
 */
public class  OauthTokenGenerator{
		public static String getPasstoken(String rawToken)
		{
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String hashedPassword = passwordEncoder.encode(rawToken);
			return hashedPassword;
		}
}

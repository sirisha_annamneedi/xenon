package com.xenon.util.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Assert;

import com.xenon.oauthclient.OAuthUtils;

public class ExcelOperations {

	//private static final Logger logger = Logger.getLogger(ExcelOperations.class);
	final static Logger logger = LogManager.getLogger(ExcelOperations.class);
	private XSSFWorkbook workbook = null;
	private XSSFSheet sheet = null;
	private XSSFCell cell = null;
	private HashMap<String, Object[][]> exceldata = new HashMap<String, Object[][]>();

	@SuppressWarnings("deprecation")
	public String[][] getExcelData(String xlsFilePath, String sheetName) {
		String[][] tabArray = null;
		try {
			logger.info("Reading xlsx file: " + xlsFilePath + " for factory method.");

			int ci = 0, cj = 0, noOfRow = 0, noOfCols = 0;
			cell = null;
			FileInputStream file;

			file = new FileInputStream(new File(xlsFilePath));

			workbook = new XSSFWorkbook(file);
			sheet = workbook.getSheet(sheetName);
			noOfRow = sheet.getLastRowNum();
			noOfCols = workbook.getSheet(sheetName).getRow(0).getLastCellNum();
			tabArray = new String[noOfRow][noOfCols];
			int celcounter = 0;
			cj = 0;
			int totCol = sheet.getRow(0).getLastCellNum();
			for (ci = 0; ci < noOfRow; ci++) {
				cj = celcounter;
				for (int c = 0; c < totCol; c++) {
					cell = sheet.getRow(ci + 1).getCell(c, Row.RETURN_BLANK_AS_NULL);
					tabArray[ci][cj] = getCellValueAsString(cell);
					cj++;
				}
			}

			file.close();
			return (tabArray);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail("Not able to read excel file: " + xlsFilePath);
		}
		return (tabArray);
	}

	@SuppressWarnings("deprecation")
	public HashMap<String, Object[][]> getExcelSheetAllData(String xlsFilePath) {
		String[][] tabArray = null;
		try {
			logger.info("Reading xlsx file: " + xlsFilePath + " for factory method.");
			FileInputStream file;

			file = new FileInputStream(new File(xlsFilePath));
			List<String> sheets = getExcelSheet(xlsFilePath);
			workbook = new XSSFWorkbook(file);

			for (String sheetName : sheets) {
				int ci = 0, cj = 0, noOfRow = 0, noOfCols = 0;
				cell = null;
				sheet = workbook.getSheet(sheetName);
				noOfRow = sheet.getLastRowNum();
				noOfCols = workbook.getSheet(sheetName).getRow(0).getLastCellNum();
				tabArray = new String[noOfRow][noOfCols];
				int celcounter = 0;
				cj = 0;
				int totCol = sheet.getRow(0).getLastCellNum();
				for (ci = 0; ci < noOfRow; ci++) {
					cj = celcounter;
					for (int c = 0; c < totCol; c++) {
						cell = sheet.getRow(ci + 1).getCell(c, Row.RETURN_BLANK_AS_NULL);
						tabArray[ci][cj] = getCellValueAsString(cell);
						cj++;
					}
				}
				exceldata.put(sheetName, checkOption(tabArray));
			}

			file.close();
			return (exceldata);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail("Not able to read excel file: " + xlsFilePath);
		}
		return (exceldata);
	}

	@SuppressWarnings("resource")
	private List<String> getExcelSheet(String xlsFilePath) {
		List<String> sheetList = new ArrayList<String>();
		try {
			logger.info("Reading xlsx file: " + xlsFilePath + " for factory method.");
			FileInputStream file = new FileInputStream(new File(xlsFilePath));
			XSSFWorkbook my_xlsx_workbook = new XSSFWorkbook(file);
			int noOfSheet = my_xlsx_workbook.getNumberOfSheets();
			for (int sheetno = 0; sheetno < noOfSheet; sheetno++) {
				XSSFSheet sheet = my_xlsx_workbook.getSheetAt(sheetno);
				sheetList.add(sheet.getSheetName().toString());
			}
			file.close();
			return (sheetList);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail("Not able to read excel file: " + xlsFilePath);
		}
		return (sheetList);
	}

	@SuppressWarnings("deprecation")
	public String getCellValueAsString(Cell cell) {
		String strCellValue = null;
		if (cell != null) {
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				strCellValue = cell.toString();
				break;
			case Cell.CELL_TYPE_NUMERIC:
				if (DateUtil.isCellDateFormatted(cell)) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
					strCellValue = dateFormat.format(cell.getDateCellValue());
				} else {
					Double value = cell.getNumericCellValue();
					Long longValue = value.longValue();
					strCellValue = new String(longValue.toString());
				}
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				strCellValue = new String(new Boolean(cell.getBooleanCellValue()).toString());
				break;
			case Cell.CELL_TYPE_BLANK:
				strCellValue = "";
				break;
			}
		} else {
			strCellValue = "";
		}
		return strCellValue;
	}

	private String[][] checkOption(String xlsData[][]) {

		String[][] tabArray = new String[0][0];
		String data = "";
		int noOfRow = xlsData.length;
		if (noOfRow > 0) {
			int noOfCols = xlsData[0].length;
			int newRows = 0;
			// Calculate no of rows with y option only for second Column.
			for (int ci = 0; ci < noOfRow; ci++) {
				data = xlsData[ci][1];
				if (data.equalsIgnoreCase("y")) {
					newRows++;
				}
			}
			int rowNo = 0;
			if (newRows > 0) {
				tabArray = new String[newRows][noOfCols];
				for (int ci = 0; ci < noOfRow; ci++) {
					data = xlsData[ci][1];
					if (data.equalsIgnoreCase("y")) {
						for (int cj = 0; cj < noOfCols; cj++) {
							tabArray[rowNo][cj] = xlsData[ci][cj];
						}
						rowNo++;
					}
				}
			}
			// logger.info("No. of Rows with Y option :" + newRows);
			return removeCol(tabArray, 1);
		} else {
			// logger.info("Input excel sheet is empty.");
			return tabArray;
		}
	}

	private String[][] removeCol(String[][] tabArray, int colRemove) {
		int row = tabArray.length;
		int col = tabArray[0].length;
		String[][] newArray = new String[row][col - 1];
		for (int i = 0; i < row; i++) {
			for (int j = 0, currColumn = 0; j < col; j++) {
				if (j != colRemove) {
					newArray[i][currColumn++] = tabArray[i][j];
				}
			}
		}
		return newArray;
	}

	@SuppressWarnings("resource")
	public boolean compareExcels(byte[] sourceExcel, byte[] destExcel) {
		XSSFWorkbook workbook1 = null;
		XSSFWorkbook workbook2 = null;
		boolean flag = false;
		cell = null;
		InputStream file1 = new ByteArrayInputStream(sourceExcel);
		InputStream file2 = new ByteArrayInputStream(destExcel);
		try {
			workbook1 = new XSSFWorkbook(file1);
			workbook2 = new XSSFWorkbook(file2);

			if (workbook1.getNumberOfSheets() == workbook2.getNumberOfSheets()) {

				for (int i = 0; i < workbook1.getNumberOfSheets(); i++) {
					logger.info("================== Compare Sheet " + workbook1.getSheetName(i));

					if (workbook1.getSheetName(i).equalsIgnoreCase(workbook2.getSheetName(i))) {

						XSSFSheet sheet_1 = workbook1.getSheetAt(i);
						XSSFSheet sheet_2 = workbook2.getSheetAt(i);

						// Compare sheets
						if (compareTwoSheets(sheet_1, sheet_2)) {
							logger.info("\n\nThe two excel sheets are Equal");
							flag = true;
							continue;
						} else {
							logger.info("\n\nThe two excel sheets are Not Equal");

							flag = false;
							break;
						}

					} else
						return false;
				}
			} else
				return flag;
		} catch (Exception e) {
			logger.info(e.getMessage());
			logger.error("Something went wrong =" + e.getMessage());
			flag = false;
		} finally {
			try {
				file1.close();
				file2.close();
			} catch (IOException e) {
				e.printStackTrace();
				flag = false;
			}
		}
		return flag;

	}

	// Compare Two Sheets
	public static boolean compareTwoSheets(XSSFSheet sheet1, XSSFSheet sheet2) {
		boolean equalSheets = true;
		logger.info("\n\nComparing Row 0 ");
		XSSFRow row1 = sheet1.getRow(0);
		XSSFRow row2 = sheet2.getRow(0);
		if (!compareTwoRows(row1, row2)) {
			equalSheets = false;
			logger.info("Row 0 - Not Equal");
		} else {
			logger.info("Row 0 - Equal");
		}
		return equalSheets;
	}

	// Compare Two Rows
	public static boolean compareTwoRows(XSSFRow row1, XSSFRow row2) {
		if ((row1 == null) && (row2 == null)) {
			return true;
		} else if ((row1 == null) || (row2 == null)) {
			return false;
		}

		int firstCell1 = row1.getFirstCellNum();
		int lastCell1 = row1.getLastCellNum();
		boolean equalRows = true;

		// Compare all cells in a row
		for (int i = firstCell1; i <= lastCell1; i++) {
			XSSFCell cell1 = row1.getCell(i);
			XSSFCell cell2 = row2.getCell(i);
			if (!compareTwoCells(cell1, cell2)) {
				equalRows = false;
				logger.info("       Cell " + i + " - NOt Equal");
				break;
			} else {
				logger.info("       Cell " + i + " - Equal");
			}
		}
		return equalRows;
	}

	// Compare Two Cells
	@SuppressWarnings("deprecation")
	public static boolean compareTwoCells(XSSFCell cell1, XSSFCell cell2) {
		if ((cell1 == null) && (cell2 == null)) {
			return true;
		} else if ((cell1 == null) || (cell2 == null)) {
			return false;
		}

		boolean equalCells = false;
		int type1 = cell1.getCellType();
		int type2 = cell2.getCellType();
		if (type1 == type2) {
				// Compare cells based on its type
				switch (cell1.getCellType()) {
				case HSSFCell.CELL_TYPE_FORMULA:
					if (cell1.getCellFormula().equals(cell2.getCellFormula())) {
						equalCells = true;
					}
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					if (cell1.getNumericCellValue() == cell2.getNumericCellValue()) {
						equalCells = true;
					}
					break;
				case HSSFCell.CELL_TYPE_STRING:
					if (cell1.getStringCellValue().equals(cell2.getStringCellValue())) {
						equalCells = true;
					}
					break;
				case HSSFCell.CELL_TYPE_BLANK:
					if (cell2.getCellType() == HSSFCell.CELL_TYPE_BLANK) {
						equalCells = true;
					}
					break;
				case HSSFCell.CELL_TYPE_BOOLEAN:
					if (cell1.getBooleanCellValue() == cell2.getBooleanCellValue()) {
						equalCells = true;
					}
					break;
				case HSSFCell.CELL_TYPE_ERROR:
					if (cell1.getErrorCellValue() == cell2.getErrorCellValue()) {
						equalCells = true;
					}
					break;
				default:
					if (cell1.getStringCellValue().equals(cell2.getStringCellValue())) {
						equalCells = true;
					}
					break;
				}
			
		} else {
			return false;
		}
		return equalCells;
	}
}

package com.xenon.util.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClients;
import org.springframework.web.multipart.MultipartFile;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;



import com.xenon.controller.utils.Constants;

/**
 * 
 * @author 
 * @Description FileUploadForm class is used for getting and setting multipart files
 *
 */
public class FileUploadForm {

	private List<MultipartFile> files;

	/**
	 * @method getFiles
	 * @return object of List<MultipartFile>
	 */
	public List<MultipartFile> getFiles() {
		return files;
	}

	/**
	 * @method setFiles
	 * @param List<MultipartFile> files
	 */
	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}
	
	@SuppressWarnings("unused")
	public boolean sendFile(String filepath, String directoryName) {
		boolean flag = false;
		// String BASE_URL = ReportConstants.XENON_SERVER_WS + "api/uploadFile";
		String BASE_URL = Constants.XenonServerWS + "/uploadFile"; //xenonWeb
		String DIR_NAME = directoryName;
		// XenonOAuth2Client xenonOAuth2Client=XenonOAuth2Client.getInstance();
		try {
			File file = new File(filepath);
			HttpClient httpClient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(BASE_URL);
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			FileBody fileBody = new FileBody(file, ContentType.APPLICATION_OCTET_STREAM);
			builder.addPart("file", fileBody);
			builder.addTextBody("filedir", DIR_NAME, ContentType.TEXT_PLAIN);
			// builder.addTextBody("access_token",
			// xenonOAuth2Client.getAccessToken(), ContentType.TEXT_PLAIN);
			HttpEntity multipart = builder.build();
			httpPost.setEntity(multipart);
			HttpResponse response = httpClient.execute(httpPost);
			flag = true;
		} catch (ClientProtocolException e) {
			flag = false;
			e.printStackTrace();
		} catch (IOException e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}
	
	public void raiseBug(String testCase, Integer step, String stepDescription, String actualResult,
			String screenshotLink, String buildId, String dirName) {
		String executionType="";
		
		//adding try catch because EXE_TYPE comes in picture for API test type only, in case of automation, 
		//xenon does not find exe_type in production file and fails
		
		try {
			if (Constants.EXE_TYPE.equalsIgnoreCase("API")) {
				executionType = "API";
			}
		} catch (Exception e1) {
			executionType = "Automation";
			e1.printStackTrace();
		}

		try {
			if (executionType.equalsIgnoreCase("Automation")) {
				sendFile(screenshotLink, dirName);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}

	}
}
package com.xenon.test.database;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.xenon.buildmanager.dao.UserDAO;
import com.xenon.database.DatabaseDao;

public class UserDB {

	@Autowired
	DatabaseDao database;
	@Autowired
	UserDAO userDAO;

	private static UserDAO userService;

	@PostConstruct
	public void init() {

		UserDB.userService = userDAO;
	}

	private static final Logger logger = LoggerFactory.getLogger(UserDB.class);

	public List<Map<String, Object>> getUserDetails(String emailId) {
		logger.info("Query to get all  project details");
		List<Map<String, Object>> allProjectDetails = userService.getUserDetailsByEmailId(emailId);
		logger.info("Query Result " + allProjectDetails);
		return allProjectDetails;
	}

	public void getUserDetails(int userId, String schema) {
		logger.info("Query to get all  project details");
		List<Map<String, Object>> allProjectDetails = userService.getUserDetails(userId, schema);
		logger.info("Query Result " + allProjectDetails);
	}
	
	public String getUsersByEmailID(String emailIds, String schema) {
		logger.info("Query to get user details");
		List<Map<String, Object>> allUserDetails = userService.getUsersByEmailIds(emailIds, schema);
		logger.info("Query Result " + allUserDetails);
		
		String userIDs = "";
		String usersName[] = emailIds.split(",");
		for (int cnt = 0; cnt < usersName.length; cnt++) {
			boolean foundFlag = false;
			for (int cnt2 = 0; cnt2 < allUserDetails.size(); cnt2++) {
				if (usersName[cnt].equalsIgnoreCase(allUserDetails.get(cnt2).get("email_id").toString())) {
					userIDs += allUserDetails.get(cnt2).get("user_id").toString() + "::"+ allUserDetails.get(cnt2).get("email_id") +",";
					foundFlag = true;
				}
				if (foundFlag) {
					break;
				}
			}
		}
		return userIDs;
	}
	
	public int getUserIDbyEmail(String emailID, String schema) {
		logger.info("Query to get userID by their email ID");
		int userID = userService.getUserIDbyEmail(emailID, schema);
		logger.info("Query Result " + userID);
		return userID;
	}
}

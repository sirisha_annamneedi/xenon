package com.xenon.test.database;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.buildmanager.dao.ModuleDAO;
import com.xenon.database.DatabaseDao;

public class ModuleDB {
	@Autowired
	DatabaseDao database;

	@Autowired
	ModuleDAO moduleDAO;

	private static ModuleDAO moduleService;

	@PostConstruct
	public void init() {

		ModuleDB.moduleService = moduleDAO;
	}

	private static final Logger logger = LoggerFactory.getLogger(ModuleDB.class);

	public List<Map<String, Object>> getAllModules(String schema) {
		logger.info("Query to get all  project details");
		List<Map<String, Object>> allProjectDetails = moduleService.getAllModules(schema);
		logger.info("Query Result " + allProjectDetails);
		return allProjectDetails;
	}

	public List<Map<String, Object>> getModulesByProjectAndUserId(int projectId, int userId, String schema) {
		logger.info("Query to get all  project details");
		List<Map<String, Object>> moduleDetails = moduleService.getModulesByProjectAndUserId(projectId, userId, schema);
		logger.info("Query Result " + moduleDetails);
		return moduleDetails;
	}

	public String getModuleIdsFromNames(String moduleNames, int projectId, int userId, String schema) {
		String modules = "";
		String[] modulesArray = moduleNames.split(",");
		List<Map<String, Object>> modulesList = getModulesByProjectAndUserId(projectId, userId, schema);

		for (int cnt = 0; cnt < modulesArray.length; cnt++) {
			boolean foundFlag = false;
			for (int cnt2 = 0; cnt2 < modulesList.size(); cnt2++) {
				if (modulesArray[cnt].equalsIgnoreCase(modulesList.get(cnt2).get("module_name").toString())) {
					modules += modulesList.get(cnt2).get("module_id").toString() + ",";
					foundFlag = true;
				}
				if (foundFlag) {
					break;
				}
			}

		}
		return modules;
	}
	
	public List<Map<String, Object>> getModuleDetailsByName(String moduleName,String schema) {
		logger.info("Query to get all  project details");
		List<Map<String, Object>> moduleDetails = moduleService.getModuleDetailsByName(moduleName, schema);
		logger.info("Query Result " + moduleDetails);
		return moduleDetails;
	}
	
	public int getModuleIdByName(String moduleName,String schema) {
		List<Map<String, Object>> moduleDetails=getModuleDetailsByName(moduleName, schema);
		if(moduleDetails.size()>0)
			return Integer.parseInt(moduleDetails.get(0).get("module_id").toString());
		else
		return 0;
		
	}
}

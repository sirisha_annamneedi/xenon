package com.xenon.test.database;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xenon.buildmanager.dao.ProjectDAO;
import com.xenon.database.DatabaseDao;

public class ProjectDB {

	@Autowired
	DatabaseDao database;

	@Autowired
	ProjectDAO projectDAO;

	private static ProjectDAO projectService;

	@PostConstruct
	public void init() {

		ProjectDB.projectService = projectDAO;
	}

	private static final Logger logger = LoggerFactory.getLogger(ProjectDB.class);

	public List<Map<String, Object>> getAllProjectDetails(String schema) {
		logger.info("Query to get all  project details");
		List<Map<String, Object>> allProjectDetails = projectService.getAllProjects(schema);
		logger.info("Query Result " + allProjectDetails);
		return allProjectDetails;
	}

	public List<Map<String, Object>> getProjectDetailsByName(String projectName, String schema) {
		logger.info("Query to get  project details");
		List<Map<String, Object>> projectDetailsByName = projectService.getProjectDetailsByName(projectName, schema);
		logger.info("Query Result " + projectDetailsByName);
		return projectDetailsByName;
	}

	public String getProjectIdsFromNames(String projectNames, String schema) {
		List<Map<String, Object>> projects = getAllProjectDetails(schema);
		String appIds = "";
		String projectsName[] = projectNames.split(",");
		for (int cnt = 0; cnt < projectsName.length; cnt++) {
			boolean foundFlag = false;
			for (int cnt2 = 0; cnt2 < projects.size(); cnt2++) {
				if (projectsName[cnt].equalsIgnoreCase(projects.get(cnt2).get("project_name").toString())) {
					appIds += projects.get(cnt2).get("project_id").toString() + "::"+ projects.get(cnt2).get("project_name") +",";
					foundFlag = true;
				}
				if (foundFlag) {
					break;
				}
			}
		}
		return appIds;
	}
	
	public int getProjectIdByName(String projectName, String schema) {
		logger.info("Query to get  project details");
		List<Map<String, Object>> projectDetailsByName =getProjectDetailsByName(projectName, schema);
		logger.info("Query Result " + projectDetailsByName);
		if(projectDetailsByName.size()>0){
			return Integer.parseInt(projectDetailsByName.get(0).get("project_id").toString());
		}
		return 0;
	}

}

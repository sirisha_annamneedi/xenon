package com.xenon.database;

public class UserNotifications {
	private static UserNotifications instance = null;

	protected UserNotifications() {

	}

	public static UserNotifications getInstance() {
		if (instance == null) {
			instance = new UserNotifications();
		}
		return instance;
	}

	public String inserNewNotification="INSERT INTO `%s`.`xe_user_notifications` (`notification_id`, `activity`, `notification_description`, `notification_url`,  `notification_date`,`user_id`, `created_by`, `read_status`, `notification_status`) VALUES (0, ?, ?,?,NOW(), ?,?,?, ?);";
	
	public String inserNotification="INSERT INTO `%s`.`xe_user_notifications` (`notification_id`, `activity`, `notification_description`, `notification_url`, `notification_date`, `user_id`, `created_by`, `read_status`, `notification_status`) VALUES (0, ?, ?,?,NOW(), ?,?,?, ?);";
	
	public String markNotificationAsRead="Update `%s`.xe_user_notifications SET read_status=? where notification_id=? and user_id=?;";
	
	public String markNotificationUserAsRead="Update `%s`.xe_notification_users SET nf_read_status=? where nf_notification_id=? and nf_user_id=?;";
	
} 

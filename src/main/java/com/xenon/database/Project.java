package com.xenon.database;

public class Project {

	private static Project instance = null;

	protected Project() {

	}

	public static Project getInstance() {
		if (instance == null) {
			instance = new Project();
		}
		return instance;
	}

	public String insertProject = "INSERT INTO `%s`.xe_project (project_name,project_description,project_active,project_createdate,tm_prefix,bt_prefix)"
			+ "VALUES(?,?,?,now(),?,?)";
	public String getAllProjectDetails = "SELECT project_id,project_name,project_description,active.status as projectStatus,project_createdate"
			+ " FROM `%s`.xe_project as project"
			+ " INNER JOIN `%s`.xe_active as active on active.active_id=project.project_active";
	public String getSingleProjectDetails = "SELECT project_id,project_name,project_description,issue_tracker_project_key,bt_prefix,tm_prefix,active.status as projectStatus,project_createdate"
			+ " FROM `%s`.xe_project as project"
			+ " INNER JOIN xe_active as active on active.active_id=project.project_active" + " WHERE project.project_id = ?";
	public String updateSingleProject = "UPDATE `%s`.xe_project" + " SET project_description = ?,"
			+ " project_active = ?" + " WHERE project_id = ?";
	
	public String getAllProjects = "select * from `%s`.xe_project where project_id IN"
						+"(SELECT project_id FROM `%s`.xe_user_project where user=?) and project_Active = 1";
	
	public String projectAssignedUsers = "SELECT * FROM `%s`.xe_user_details as ud where user_id in (SELECT user FROM `%s`.xe_user_project where project_id in (?)) and ud.user_status = 1";
	
	public String projectNotAssignedUsers = "SELECT * FROM `%s`.xe_user_details as ud where user_id not in (SELECT user FROM `%s`.xe_user_project where project_id in (?)) and ud.user_status = 1";
	
	public String deleteUnassignedProjectRecord = "DELETE FROM `%s`.xe_user_project "
			+ " WHERE project_id = ?";
	
	public String insertAssignedProjectRecords = "INSERT INTO  `%s`.xe_user_project (user_project_id ,project_id,user) "
			+ " values(?,?,?)";
	
	
	public String getAssignedUser="SELECT project_id,project_name FROM `%s`.xe_project "
			+ "where project_id IN "
			+ "(SELECT project_id FROM `%s`.xe_user_project where user =?) and project_active=1 order by project_name";
	
	public String getCurrentProject="SELECT project_id,project_name FROM `%s`.xe_project "
			+ "where project_id IN "
			+ "(SELECT project_id FROM `%s`.xe_current_project where user_id=?) and project_id IN (SELECT project_id FROM `%s`.xe_user_project where user=?)  and project_active=1";
	
	public String updateCurrentProject="update `%s`.xe_current_project set project_id=? where user_id=?";
	
	public String insertCurentProject="insert into `%s`.xe_current_project(project_id,user_id) values(?,?)";
	
	public String createNewProjectStatus="{?= CALL `%s`.createNewProjectStatus(?)}";
	

	public String getProjectDetailsById = "SELECT *"
			+ " FROM `%s`.xe_project"
			+ " WHERE project_id = ?";

	public String getProjectDetail="SELECT * FROM  %s.xe_project where project_id IN (SELECT project_id FROM  %s.xe_module where module_id IN (SELECT module_id FROM  %s.xetm_scenario where scenario_id IN (SELECT scenario_id FROM  %s.xetm_testcase where testcase_id IN (SELECT testcase_id FROM  %s.xebm_buildtestcase where build_id=?)))) and project_id IN "
			+ "(SELECT project_id FROM `%s`.xe_user_project where user =?) and project_active=1 and project_id IN "
			+ "(SELECT project_id FROM `%s`.xebm_assign_build where user_id =?)";
	
	public String getProjectsFromBuild="SELECT * FROM  %s.xe_project where project_id IN (SELECT project_id FROM  %s.xe_module where module_id IN (SELECT module_id FROM  %s.xetm_scenario where scenario_id IN (SELECT scenario_id FROM  %s.xetm_testcase where testcase_id IN (SELECT testcase_id FROM  %s.xebm_buildtestcase where build_id=?)))) and project_id IN "
			+ "(SELECT project_id FROM `%s`.xe_user_project where user =?) and project_active=1";
	
	public String getModuleDetail="SELECT * FROM %s.xe_module WHERE module_id IN (SELECT distinct module_id FROM %s.xe_user_module natural join  %s.xebm_buildtestcase where build_id=?)and module_id IN "
			+ "(SELECT module_id FROM `%s`.xe_user_module where user_id =?) and project_id IN (SELECT project_id FROM `%s`.xe_project where project_active =1)";
	public String getUserDeatil="SELECT distinct user_id,module_id FROM %s.xe_user_module natural join %s.xebm_buildtestcase where build_id=?";
	public String getUserDeatilByUser="SELECT distinct user_id,module_id FROM %s.xe_user_module natural join %s.xebm_buildtestcase where build_id=? and user_id=?";
	
	public String getUser="SELECT * FROM %s.xe_user_details WHERE user_status=1";
	
	public String assignBuild="INSERT INTO `%s`.`xebm_assign_build` (`build_id`,`project_id`,`module_id`,`user_id`) VALUES(?,?,?,?);";
	public String removeAssignedBuild="DELETE FROM `%s`.`xebm_assign_build` where build_id = ?";
	public String prjWiseExecTcCount="SELECT count(*) ExecutedTcCount,SUM(case when status_id = 1 then 1 else 0 end) as PassCount,SUM(case when status_id = 2 then 1 else 0 end) as FailCount,SUM(case when status_id = 3 then 1 else 0 end) as SkipCount,SUM(case when status_id = 4 then 1 else 0 end) as BlockCount FROM `%s`.xe_testcase_execution_summary where project_id= ? and build_id=? and complete_status=1";
	
	public String prjWiseTotalTcCount="SELECT count(*) TotalTcCount,project_id FROM `%s`.xebm_buildtestcase where project_id= ? and build_id=?";
	

	public String buildWiseExecTcCount="SELECT count(*) ExecutedTcCount,SUM(case when status_id = 1 then 1 else 0 end) as PassCount,SUM(case when status_id = 2 then 1 else 0 end) as FailCount,SUM(case when status_id = 3 then 1 else 0 end) as SkipCount,SUM(case when status_id = 4 then 1 else 0 end) as BlockCount FROM `%s`.xe_testcase_execution_summary where  build_id=? and complete_status=1";
	
	public String buildWiseTotalTcCount="SELECT count(*) TotalTcCount,project_id FROM `%s`.xebm_buildtestcase where  build_id=?";
	
	public String createNewProject="{?= CALL `%s`.createNewProject(?,?,?,?,?,?,?)}";
	
	public String removeUnassignedCurrentProject= "DELETE FROM `%s`.xe_current_project WHERE project_id =? and user_id IN (SELECT user FROM `%s`.xe_user_project where project_id in (?));";
	
	public String unAssignAppToUser = "DELETE FROM `%s`.xe_user_project "
									+ "WHERE project_id = ? and user = ?";
	
	public String loginUserAssignedAppCount = "SELECT count(*) FROM `%s`.xe_current_project as currPro "
			+ " where currPro.project_id IN (SELECT userPro.project_id FROM `%s`.xe_user_project userPro, `%s`.xe_project as pro "
			+ " where userPro.user = ? and userPro.project_id = pro.project_id and pro.project_active = 1) "
			+ " and currPro.user_id = ?";
	
	public String getProjectDetailsByName = "SELECT project_id,project_name,project_description,active.status as projectStatus,project_active,project_createdate"
			+ " FROM `%s`.xe_project as project"
			+ " INNER JOIN `%s`.xe_active as active on active.active_id=project.project_active where project_name=?";
	
	public String getMailSettingsForAssignApp = "select email.smtp_host,email.smtp_port,email.smtp_user_id,email.smtp_password "
			+ " from `%s`.xe_email_details as email , `%s`.xe_mail_notifications as notify "
			+ " where email.is_enabled=1 and notify.assign_project = 1";
	
	public String getProjectId=" select project_id from `%s`.xe_project where project_name=?";
}

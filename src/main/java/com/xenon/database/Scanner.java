package com.xenon.database;

public class Scanner {
	private static Scanner instance = null;

	protected Scanner() {

	}

	public static Scanner getInstance() {
		if (instance == null) {
			instance = new Scanner();
		}
		return instance;
	}

	public String insertPageScanDetails="INSERT INTO `%s`.`xesfdc_scanner` (`scanner_id`, `env`, `pagename_id`, `functionality_id`, `user_id`) VALUES (0, ?,?, ?,?)";
	
	public String getAllActivities = "SELECT a.activity_id,a.activity,a.activity_desc,a.module,DATE_FORMAT(a.activity_date , '%%d-%%m-%%Y %%h:%%i:%%s %%p') as activity_date,a.activity_date as actDate,TIMESTAMPDIFF(MINUTE,a.activity_date,CURRENT_TIME()) AS TimeDiff,u.first_name,u.last_name,a.user,u.user_photo,act.activity_desc as activityState,act.activity_format FROM `%s`.xe_recent_activities a INNER JOIN `%s`.xe_user_details u ON u.user_id=a.user  INNER JOIN %s.xe_activities act ON act.xetm_activity_id=a.activity and activity_status=1 ORDER BY a.activity_id DESC LIMIT 5";
	
	public String insertSfdcTcDetails="INSERT INTO `%s`.`xesfdc_tc` (`sfdc_tc_id`, `testcase_name`, `testcase_desc`, `sfdc_page_id`, `sfdc_functionality_id`, `sfdc_env`,`user_id`) VALUES (0,?,?,?,?,?,?)";
	
	public String getScannerIdByDetails="Select scanner_id FROM `%s`.`xesfdc_scanner` where env=? and pagename_id=? and functionality_id=?";
	
	public String insertSfdcRepositoryRecords="INSERT INTO `%s`.`xesfdc_repository` (`scanner_id`, `label`, `type`, `xpath`) VALUES (0, ?, ?,?);";
	
	public String getAllLocatorType="SELECT * FROM `%s`.xesfdc_locator_type where type_status=1;";
	
	public String getTestCaseData = "SELECT tc.sfdc_tc_id ,tc.testcase_name, tc.sfdc_page_id, pages.pagename, tc.sfdc_functionality_id, tc.sfdc_env "
								  + " FROM `%s`.xesfdc_tc as tc, `%s`.xesfdc_pages as pages "
								  + " WHERE tc.sfdc_env = ? and "
								  + " tc.sfdc_page_id = pages.page_id "
								  + " Order by tc.sfdc_page_id ";

	public String insertSfdcTcStepRecords="INSERT INTO `%s`.`xesfdc_test_steps` (`sfdc_tc_steps`, `sfdc_testcase`, `repo_id`, `step_desc`, `step_no`) VALUES (?,?,?,?,?);";

	public String getScannerId = "SELECT scanner_id FROM `%s`.xesfdc_scanner "
								+ " WHERE env = ? and pagename_id = ? and functionality_id = ? "
								+ " LIMIT 1";
	
	public String insertBuildTcRecords="INSERT INTO `%s`.`xesfdc_automation_build_testcase` (`build_tc_id`, `sfdc_build_id`, `sfdc_testcase_id`) VALUES (?,?,?);";
	
	public String removeBuildTcRecords="DELETE FROM `%s`.`xesfdc_automation_build_testcase` where `sfdc_build_id` =? and `sfdc_testcase_id`=?";

	public String getActiveEnvData = "SELECT env_id, env_name FROM `%s`.xesfdc_env where env_status = 1 ";
	
	public String insertSfdcTcStepValRecords="INSERT INTO `%s`.`xesfdc_build_tc_step_val` (`build_tc_step_val_id`, `build_tc_id`, `tc_step_id`, `step_val`,`build_id`) VALUES (?,?,?,?,?);";
	
	public String removeSfdcTcStepValRecords="DELETE FROM `%s`.`xesfdc_build_tc_step_val` where `build_id` =? and `build_tc_id`=?";
	
	public String createNewEnv="{?= CALL `%s`.sfdcCreateNewEnv(?,?,?,?)}";
	
	public String getAllEnvData = "SELECT * FROM `%s`.xesfdc_env E INNER JOIN `%s`.xe_active A ON E.env_status=A.active_id;";
	
	public String getEnvDetailsById = "SELECT * FROM `%s`.xesfdc_env E INNER JOIN `%s`.xe_active A ON E.env_status=A.active_id where env_id=? ;";
	
 	public String updateEnvDetailsById =  "UPDATE `%s`.`xesfdc_env` SET `env_name`=?, `env_desc`=?, `login_url`=?, `env_status`=? WHERE `env_id`=?;";

 	
 	public String getActiveActionData = "SELECT * FROM `%s`.xesfdc_func where func_status = 1 ";
 	
 	public String createNewAction="{?= CALL `%s`.sfdcCreateNewAction(?,?)}";
	
	public String getAllActionData = "SELECT * FROM `%s`.xesfdc_func F INNER JOIN `%s`.xe_active A ON F.func_status=A.active_id;";
	
	public String getActionDetailsById = "SELECT * FROM `%s`.xesfdc_func F INNER JOIN `%s`.xe_active A ON F.func_status=A.active_id where functionality_id=? ;";
	
 	public String updateActionDetailsById =  "UPDATE `%s`.`xesfdc_func` SET `functionality`=?, `func_status`=? WHERE `functionality_id`=?;";

} 

package com.xenon.database;

public class Subcomponent {
	private static Subcomponent instance = null;

	protected Subcomponent() {

	}

	public static Subcomponent getInstance() {
		if (instance == null) {
			instance = new Subcomponent();
		}
		return instance;
	}

	public String createNewSubcomponent="{?= CALL xenonsupport.createNewSubcomponent(?,?,?)};";
	public String getComponent="SELECT * FROM xenonsupport.xes_component AS cmp WHERE cmp.id=?";
	public String getSubcomponent="SELECT * FROM xenonsupport.xes_subcomponents WHERE id=?";
}

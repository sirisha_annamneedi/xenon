package com.xenon.database;

public class JMeter {
	private static JMeter instance = null;

	protected JMeter() {

	}

	public static JMeter getInstance() {
		if (instance == null) {
			instance = new JMeter();
		}
		return instance;
	}

	public String getResponseGraph="SELECT timeStamp,elapsed,label,threadName FROM `%s`.jmeter_graph group by threadName,label order by threadName";
	
}

package com.xenon.database;

public class Timezone {
	private static Timezone instance = null;

	protected Timezone() {

	}

	public static Timezone getInstance() {
		if (instance == null) {
			instance = new Timezone();
		}
		return instance;
	}

	public String getAllTimezones = "SELECT * FROM `%s`.xe_timezone_details where timezone_staus=1";
	public String getTimezonesDetailsById = "SELECT * FROM `%s`.xe_timezone_details where timezone_staus=1 and xe_timezone_id=?";
} 

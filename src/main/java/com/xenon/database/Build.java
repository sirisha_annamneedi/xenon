package com.xenon.database;

public class Build {
	private static Build instance = null;

	protected Build() {

	}

	public static Build getInstance() {
		if (instance == null) {
			instance = new Build();
		}
		return instance;
	}
	
	public String insertbuild = "INSERT INTO `%s`.xe_build "
			+ " (build_name,build_desc,build_status,release_id,build_creator,build_state,build_createdate)"
			+ " VALUES(?,?,?,?,?,1,now())";
	
	public String getBuildDetails = "SELECT build_id,build_name,build_desc,a1.status as buildStatus, build.build_creator,"
			+ " r1.release_name as releaseName"
			+ " FROM `%s`.xe_build as build"
			+ " INNER JOIN `%s`.xe_active as a1 ON a1.active_id = build.build_status"
			+ " INNER JOIN `%s`.xe_release as r1 on r1.release_id = build.release_id"
			+ " WHERE build_id= ?";
	
	public String updateBuild = "UPDATE `%s`.xe_build "
			+ " SET build_name=?,build_desc=?,build_status=?,release_id=?,build_updatedate=?"
			+ " WHERE build_id= ?";
	
	public String updateAutoBuild = "UPDATE `%s`.xe_automation_build "
			+ " SET build_name=?,build_desc=?,build_status=?,release_id=?,build_updatedate=?"
			+ " WHERE build_id= ?";
	
	public String updateBuildNameManual = "UPDATE `%s`.xe_build SET build_name=? WHERE build_id= ?";
	
	public String updateBuildNameAutomation = "UPDATE `%s`.xe_automation_build SET build_name=?  WHERE build_id= ?";

	public String deleteBuildSoftlyManual = "UPDATE `%s`.xe_build SET build_status = 2 WHERE build_id= ?";
	
	public String deleteBuildSoftlyAutomation = "UPDATE `%s`.xe_automation_build SET build_status = 2  WHERE build_id= ?";

	public String getAllBuildDetails = "SELECT build_id,build_name,build_desc,a1.status as buildStatus,build_status, "
			+ " r1.release_name as releaseName,build_createdate,state.desc as buildState,build_state,Concat(user.first_name ,' ', user.last_name) as creator ,build.build_creator as creatorID,user.about_me,user.user_photo"
			+ " FROM `%s`.xe_build as build"
			+ " INNER JOIN `%s`.xe_active as a1 ON a1.active_id = build.build_status"
			+ " INNER JOIN `%s`.xe_release as r1 on r1.release_id = build.release_id"
			+ " inner join `%s`.xebm_build_state as state on state.id=build.build_state"
			+ " inner join `%s`.xe_user_details as user on user.user_id = build.build_creator where r1.release_active=1";

	
	public String addTestcase = "CALL `%s`.`addTestCase`(?,?,?)";
	
	//public String dropTestcase="DELETE FROM `%s`.`xebm_buildtestcase` WHERE testcase_id=? and build_id=?";
	public String dropTestcase="CALL `%s`.`dropTestCase`(?,?)";
	
	
	
	public String setBuildStatus="UPDATE `%s`.`xe_build` SET build_state=? WHERE build_id=?";
	
	public String setAutomationBuildStatus="UPDATE `%s`.`xe_automation_build` SET build_state=? WHERE build_id=?";
	
	public String setBuildStatusById="UPDATE `%s`.`xe_build` SET build_state= ( SELECT id FROM `%s`.xebm_build_state where `desc`=?) , build_enddate=NOW() WHERE build_id=?";
	
 
 
	public String addRemoveTCDetailsForManager = "SELECT build.build_id,build.build_name,build.build_state,rel.release_name,state.desc as state,build.build_desc"
			+ " FROM `%s`.xe_build as build"
			+ " INNER JOIN `%s`.xe_release as rel on build.release_id = rel.release_id"
			+ " INNER JOIN `%s`.xebm_build_state as state on state.id = build.build_state"
			+ " where build.build_state not in(5) and build.build_status = 1 and  rel.release_active=1";
	
	public String createNewBuildStatus="{?= CALL %s.createNewBuildStatus(?)}";
	
	public String createBugOnExecution="{?= CALL %s.createBugOnExecution(?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public String buildTcCount = "SELECT buildTC.build_id,buildTC.project_id,count(*) as proWiseCount,pro.project_name"
			+ " FROM `%s`.xebm_buildtestcase as buildTC"
			+ " inner join `%s`.xe_project as pro on pro.project_id = buildTC.project_id "
			+ " where buildTC.build_id IN (SELECT build_id FROM `%s`.xe_build)"
			+ " and buildTC.project_id IN(SELECT uPro.project_id FROM `%s`.xe_user_project as uPro where uPro.user = ?)"
			+ " and buildTC.module_id IN(SELECT uMod.module_id FROM `%s`.xe_user_module as uMod where uMod.user_id = ?)"
			+ " group by buildTC.build_id, buildTC.project_id";
	
	
	//public String getTcExecStatusByBuildId="SELECT * FROM `%s`.xe_testcase_execution_summary where build_id=?"; 
	
	public String getTcExecStatusByBuildId = "SELECT distinct TS.exec_id,TS.tc_id,TS.build_id,TS.project_id,TS.mudule_id, TS.scenario_id,TS.tester_id,TS.status_id,TS.notes,ES.description,TS.complete_status FROM `%s`.xe_testcase_execution_summary TS INNER JOIN `%s`.xebm_buildtestcase BT on BT.testcase_id=TS.tc_id INNER JOIN `%s`.xetm_exec_status ES on TS.status_id=ES.exst_id where TS.build_id=?";
	
	
	public String getTcExecStatusByBuildIdAndTcId="SELECT * FROM `%s`.xe_testcase_execution_summary where build_id=? and tc_id=?"; 
	
	public String getTcStepsStatusByBuildId="SELECT * FROM `%s`.xe_steps_execution_summary where testex_id IN  (SELECT exec_id FROM `%s`.xe_testcase_execution_summary where build_id=?);"; 
	
//	public String getAllBuildDetailsToExecute = "SELECT * FROM `%s`.xe_build B where B.build_state IN (SELECT id FROM `%s`.xebm_build_state where description='Testing in Progress' or description='Ready For Testing') and build_status=1 GROUP BY B.build_id ;";
	
	public String getAllBuildDetailsToExecute = "SELECT build_id,build_name,build_desc,a1.status as buildStatus, "
			+ " r1.release_name as releaseName,build_createdate,state.desc as buildState,build_state,Concat(user.first_name ,' ', user.last_name) as creator,user.user_photo"
			+ " FROM `%s`.xe_build as build"
			+ " INNER JOIN `%s`.xe_active as a1 ON a1.active_id = build.build_status"
			+ " INNER JOIN `%s`.xe_release as r1 on r1.release_id = build.release_id" 
			+ " inner join `%s`.xebm_build_state as state on state.id=build.build_state "
			+ " inner join `%s`.xe_user_details as user on user.user_id = build.build_creator where (state.id=3 OR state.id=4) and build_id IN(select build_id From `%s`.xebm_assign_build where user_id =?) and build.build_status = 1  and r1.release_active=1";

	public String addRemoveTCDetailsForCreator = "SELECT build.build_id,build.build_name,build.build_state,rel.release_name,state.desc as state,build.build_desc"
			+ " FROM `%s`.xe_build as build"
			+ " INNER JOIN `%s`.xe_release as rel on build.release_id = rel.release_id"
			+ " INNER JOIN `%s`.xebm_build_state as state on state.id = build.build_state"
			+ " where build.build_state not in(5) and build.build_status = 1 and build.build_creator = ? and  rel.release_active=1";

	public String getAllExecutedBuildDetails = "SELECT build_id,build_name,build_desc,a1.status as buildStatus, "
			+ " r1.release_name as releaseName,build_createdate,state.desc as buildState,build_state,Concat(user.first_name ,' ', user.last_name) as creator,user.user_photo,user.about_me"
			+ " FROM `%s`.xe_build as build"
			+ " INNER JOIN `%s`.xe_active as a1 ON a1.active_id = build.build_status"
			+ " INNER JOIN `%s`.xe_release as r1 on r1.release_id = build.release_id"
			+ " inner join `%s`.xebm_build_state as state on state.id=build.build_state "
			+ " inner join `%s`.xe_user_details as user on user.user_id = build.build_creator where (state.id=4 or state.id=5) and r1.release_active = 1";
	public String getAutoExecutedBuildDetails = "SELECT build_id,build_name,build_desc,build_logpath,a1.status as buildStatus, "
			+ " r1.release_name as releaseName,build_createdate,state.desc as buildState,build_state,Concat(user.first_name ,' ', user.last_name) as creator,user.user_photo,user.about_me"
			+ " FROM `%s`.xe_automation_build as build"
			+ " INNER JOIN `%s`.xe_active as a1 ON a1.active_id = build.build_status"
			+ " INNER JOIN `%s`.xe_release as r1 on r1.release_id = build.release_id"
			+ " inner join `%s`.xebm_build_state as state on state.id=build.build_state "
			+ " inner join `%s`.xe_user_details as user on user.user_id = build.build_creator where (state.id=4 or state.id=5) and r1.release_active = 1";
	public String getBugByExcutionId="SELECT * FROM `%s`.xe_step_excution_bug where step_exe_id=?"; 
	public String insertExecutionBuild = "INSERT INTO `%s`.xe_step_excution_bug "
			+ " (test_step_id,bug_id)"
			+ " VALUES(?,?)";
	public String createNewBuild="{?= CALL `%s`.createNewBuild(?,?,?,?,?,?)}";
	public String createNewAutomationBuild="{?= CALL `%s`.createAutomationBuild(?,?,?,?,?,?,?,?)}";
	public String createNewScriptlessBuild="{?= CALL `%s`.createScriptlessBuild(?,?,?,?,?,?,?)}";
	
	public String checkBuildAssigner="SELECT * FROM `%s`.xebm_assign_build where build_id=? and user_id = ?"; 
	public String checkViewAllBuildsAccess="SELECT viewallbuild FROM `%s`.xe_bm_access where role_id IN(select role_id FROM `%s`.xe_user_details where user_id = ?)"; 
	public String getAssigneeListByBuild="SELECT xab.build_id,xud.user_id,xud.first_name,xud.last_name FROM `%s`.xebm_assign_build xab,`%s`.xe_user_details xud where xab.user_id = xud.user_id and build_id = ? group by user_id"; 
	public String getAssigneeModulesByBuild="SELECT build_id,module_id FROM `%s`.xebm_assign_build where build_id = ? group by module_id"; 
	public String getModulesFromTestcase="SELECT build_id,module_id FROM `%s`.xebm_buildtestcase where build_id = ? group by module_id"; 
	public String cloneTestcases="INSERT INTO `%s`.xebm_buildtestcase (build_id, testcase_id, project_id, scenario_id, module_id) SELECT ?,testcase_id, project_id, scenario_id, module_id FROM `%s`.xebm_buildtestcase WHERE build_id=?";
	
	public String getBuildTcExecCountsByBuildId="SELECT"+ 
			"(SELECT count(*) FROM `%s`.xebm_buildtestcase where build_id=? ) as TotalTcCount ,"+
			"(SELECT count(*) FROM `%s`.xe_testcase_execution_summary where build_id=?) AS ExecutedTcCount,(SELECT count(*) FROM `%s`.xe_testcase_execution_summary where build_id=? and status_id=?) AS PassTcCount,(SELECT count(*) FROM `%s`.xe_testcase_execution_summary where build_id=? and status_id=?) AS FailTcCount,(SELECT count(*) FROM `%s`.xe_testcase_execution_summary where build_id=? and status_id=?) AS SkipTcCount,(SELECT count(*) FROM `%s`.xe_testcase_execution_summary where build_id=? and status_id=?) AS BlockTcCount";
	public String getBuildDashboard="SELECT (SELECT COUNT(*) FROM `%s`.xe_build WHERE build_status=1) as total_build, (SELECT COUNT(*) FROM `%s`.xe_release WHERE release_active=1) as total_release,(select ifnull(sum(bug_count), 0) total from (SELECT count(*) bug_count FROM `%s`.xe_step_execution_bug where build_id in (SELECT build_id FROM `%s`.xe_build where build_status=1) group by build_id) src) as total_bug,(SELECT ifnull(sum(test_count), 0) test_count from(SELECT count(*) as test_count FROM `%s`.xebm_buildtestcase WHERE build_id not in (SELECT build_id FROM `%s`.xe_build where build_state=5) group by build_id) src) AS total_test";
	
	public String getAllBuilds="SELECT B.build_id,B.build_name,B.build_desc,B.build_status,B.release_id,B.build_state,B.build_createdate,B.build_createdate as creDate , TIMESTAMPDIFF(MINUTE,B.build_createdate,CURRENT_TIME()) AS TimeDiff,B.build_enddate,B.build_creator ,XU.user_photo, BS.desc,XU.first_name,XU.last_name  FROM `%s`.xe_build B INNER JOIN `%s`.xebm_build_state BS ON BS.id=B.build_state INNER JOIN `%s`.xe_user_details XU  ON  B.build_creator=XU.user_id   ORDER BY B.build_id DESC LIMIT 5; ";
	
	public String getAllAutoBuilds="SELECT B.build_id,B.build_name,B.build_desc,B.build_status,B.release_id,B.build_state,B.build_createdate,B.build_createdate as creDate,B.build_enddate,B.build_creator , BS.desc,XU.first_name,XU.last_name  FROM `%s`.xe_automation_build B INNER JOIN `%s`.xebm_build_state BS ON BS.id=B.build_state INNER JOIN `%s`.xe_user_details XU  ON  B.build_creator=XU.user_id   ORDER BY B.build_id DESC LIMIT 5 ; ";
	public String getAllBuildsTestcasesCount="SELECT count(*) TotalTcCount , build_id FROM `%s`.xebm_buildtestcase GROUP BY build_id;";
	
	public String getAllBuildsExecutedTestcasesCount="SELECT count(*) AS ExecutedTcCount,build_id FROM `%s`.xe_testcase_execution_summary where complete_status=1 GROUP BY build_id;";
	public String getTCstatuscount="select Count(*) as tc_count,build_id,(SELECT description FROM `%s`.xetm_exec_status WHERE exst_id=status_id) as tc_status FROM `%s`.xe_testcase_execution_summary where build_id=? group by status_id;";
	public String topBuild="SELECT build_id,build_name FROM `%s`.xe_build where build_status=1 order by build_name desc limit 5";
	public String getTCbyBuild="select ifnull(sum(test_count), 0) test_count from(SELECT count(*) as test_count FROM `%s`.xebm_buildtestcase where build_id not in (SELECT build_id FROM `%s`.xe_build where build_state=5) group by build_id) src";
	public String getNotrun="SELECT (count(*)-(select ifnull(sum(tc_count), 0) from (select Count(*) as tc_count,build_id,(SELECT description FROM `%s`.xetm_exec_status WHERE exst_id=status_id) as tc_status FROM `%s`.xe_testcase_execution_summary where build_id=? group by status_id) src where tc_status<>'Not run')) as not_run FROM `%s`.xebm_buildtestcase where build_id=?";
	
	public String getBuildData="CALL %s.build_project(?,?)";
	
	public String getBuggraphData="SELECT count(bug_id) as bug_count, (SELECT bug_status FROM `%s`.xebt_status where status_id=xebt_bugsummary.bug_status) as bug_status FROM `%s`.xebt_bugsummary where xebt_bugsummary.bug_id IN (SELECT bug_id FROM `%s`.xe_step_execution_bug where bug_id IN (SELECT bug_id FROM `%s`.xebt_bugsummary where bs_id IN (SELECT max(bs_id) FROM `%s`.xebt_bugsummary GROUP BY bug_id) and bug_status IN (1,2,3,4)) and build_id in (SELECT build_id FROM `%s`.xe_build where build_status=1 AND release_id IN(SELECT release_id FROM `%s`.xe_release WHERE release_active=1))) and bs_id IN (SELECT max(bs_id) FROM `%s`.xebt_bugsummary GROUP BY bug_id) group by bug_status";

	public String executeAutomationBuild="CALL %s.executeAutomationBuild(?,?,?)";
	
	public String getBuildAddedTcCount="SELECT count(*) AS TcCount,X.project_id,B.execution_type, B.redwood_build_id FROM `%s`.xebm_automation_build_testcase as X join `%s`.xe_automation_build as B where X.build_id=B.build_id and X.build_id=?;"; 
	
	public String addAutomationTestCase = "CALL `%s`.`addAutomationTestCase`(?,?,?)";
	
	public String dropAutomationTestCase="CALL `%s`.`dropAutomationTestCase`(?,?)";

	public String getCreateBuildData="CALL %s.createbuild()";

	public String insertBuildExecDetails="{?= CALL %s.insertBuildExecDetails(?,?,?,?,?,?,?,?,?)}";
	public String getAutoModuleWiseTCCount="SELECT module_id,(select module_name from `%s`.xe_module where module_id = `%s`.xebm_automation_build_testcase.module_id )as module_name,count(*)as test_case_count from `%s`.xebm_automation_build_testcase where build_id =? group by module_id";
	public String getVM="SELECT port,ip_address,xe_vm_id,concat('http://',ip_address,':',port) AS inetaddress FROM `%s`.xebm_vm_details WHERE userId =? and build_id=? ORDER BY xe_vm_id DESC LIMIT 1;";
		
	public String insertTcAttachmentOnExecution = "INSERT INTO `%s`.xe_testcase_execution_attachments (`testcase_id`,`build_id`,`attachment`,`attachment_title`)VALUES (?,?,?,?)";
	public String getTcAttachmentByID = "SELECT attach_id,attachment FROM `%s`.xe_testcase_execution_attachments WHERE attach_id = ?;";
	
	public String getAllBuildsNf="SELECT B.build_id,B.build_name,B.build_desc,B.build_status,B.release_id,B.build_state,B.build_createdate,B.build_createdate as creDate , TIMESTAMPDIFF(MINUTE,B.build_createdate,CURRENT_TIME()) AS TimeDiff,B.build_enddate,B.build_creator ,XU.user_photo, BS.desc,XU.first_name,XU.last_name  FROM `%s`.xe_build B INNER JOIN `%s`.xebm_build_state BS ON BS.id=B.build_state INNER JOIN `%s`.xe_user_details XU  ON  B.build_creator=XU.user_id   ORDER BY B.build_id DESC ; ";
	
	public String getModuleWiseAssignee="SELECT XBA.build_id,XBA.module_id,XBA.user_id,XU.first_name,XU.last_name FROM `%s`.xebm_assign_build XBA,`%s`.xe_user_details XU where XBA.build_id = ? and XBA.user_id=XU.user_id group by XBA.module_id";
	
	public String getAllprojectbug="SELECT (SELECT xe_project.project_name FROM `%s`.xe_project where xe_project.project_id=bug.project) as project_name,summary.bug_id, DATE(DATE_ADD(update_date, INTERVAL(1-DAYOFWEEK(update_date)) DAY)) weekday,stat.bug_status bugStatus FROM `%s`.xebt_bugsummary summary,`%s`.xebt_bug bug , `%s`.xe_module xemodule,`%s`.xebt_status as stat where bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and bug.module = xemodule.module_id and xemodule.module_active = 1 and bug.module IN ( select module_id from `%s`.xe_user_module where user_id=? ) order by bs_id desc;";
	
	public String updateVMStatus="UPDATE `%s`.`xebm_build_vm_details` SET `vm_status`='1' and `vm_exe_state`=? WHERE `build_ID`=? and user_Id=?;";
	
	public String setScriptlessBuildStatus="UPDATE `%s`.`xesfdc_automation_build` SET build_state=? WHERE build_id=?";
	public String getScriptBuildAddedTcCount="SELECT count(*) AS TcCount FROM `%s`.xesfdc_automation_build_testcase where sfdc_build_id=?;";
	
	public String insertSfdcBuildExecDetails="{?= CALL %s.insertSfdcBuildExecDetails(?,?,?,?,?,?,?)}";
	public String getVMSfdc="SELECT xe_vm_id,concat('http://',ip_address,':',port) AS inetaddress FROM `%s`.xesfdc_vm_details WHERE userId =? and build_id=? ORDER BY xe_vm_id DESC LIMIT 1;";
	
	public String getScriptExecutedBuildDetails = "SELECT build_id,build_name,build_desc,build_logpath,a1.status as buildStatus, "
			+ " r1.release_name as releaseName,build_createdate,state.desc as buildState,build_state,Concat(user.first_name ,' ', user.last_name) as creator,user.user_photo,user.about_me"
			+ " FROM `%s`.xesfdc_automation_build as build"
			+ " INNER JOIN `%s`.xe_active as a1 ON a1.active_id = build.build_status"
			+ " INNER JOIN `%s`.xe_release as r1 on r1.release_id = build.release_id"
			+ " inner join `%s`.xebm_build_state as state on state.id=build.build_state "
			+ " inner join `%s`.xe_user_details as user on user.user_id = build.build_creator where (state.id=4 or state.id=5) and r1.release_active = 1";
	
	public String getManualBuildAddedTcCount="SELECT count(*) AS TcCount FROM `%s`.xebm_buildtestcase where build_id=? and project_id IN (SELECT project_id FROM `%s`.xe_project where project_active =1)";

	public String checkManualbuildname = "SELECT count(*) as count FROM `%s`.xe_build where build_name= ? and release_id= ?";
	
	public String checkAutobuildname = "SELECT count(*) as count FROM `%s`.xe_automation_build where build_name= ? and release_id= ?";
	
	public String getCustomerVMDetailsbyId="SELECT * FROM xenonvm.xevm_customervm_details where cust_vm_id=? and customer_id=?;";
	
	public String uploadDatasheet="UPDATE `%s`.`xebm_automation_build_testcase` SET datasheet=? , `datasheet_title`=? WHERE `testcase_id`=?  and build_id=?;";
	
	public String updateBuildExecDetails="UPDATE `%s`.`xebm_build_exec_details` SET `exec_json`=?,complete_status=?  WHERE `build_id`=? and user_id=? and vm_id=?;";
	
	public String validateAllTestcases = "SELECT * FROM %s.`xebm_automation_build_testcase` xb, %s.`xetm_testcase` xt where xb.testcase_id = xt.testcase_id and xt.excel_file_present_status = 1 "
			+ "and xb.build_id = ? and datasheet_title is null;";
	
	public String deleteBuild="delete from `%s`.xe_build where build_id=?";
	
	public String uploadDatafile="INSERT INTO `%s`.xebm_build_datafile (`build_id`,`user_id`,`data_file`,`datafile_name`)VALUES (?,?,?,?)";
	
	public String updateDatafile="UPDATE `%s`.xebm_build_datafile SET `user_id`=? , `data_file`=? , `datafile_name`=? WHERE `build_id`=?";

	public String getLastBuildDetails="select build_id,build_name,release_id from `%s`.xe_build where build_creator=? and build_status = 1 order by build_updatedate desc limit 1;";

	//public String getLastAutoBuildDetails="select build_id,build_name,build_status,release_id from `%s`.xe_automation_build where build_creator=? and build_status=1 order by build_updatedate desc limit 1;";

	public String getLastAutoBuildDetails="select X.build_id,X.build_name,X.build_status,X.execution_type,X.release_id,X.redwood_build_id from `%s`.xe_automation_build  as X join `%s`.xe_release as Y where X.release_id=Y.release_id and X.build_status=1 and Y.release_active=1 and X.build_creator=? order by X.build_updatedate desc limit 1;";

	
	public String getReleaseCount="select count(*) as releaseCount from `%s`.xe_release";

	public String getAutoBuildState="select XAB.build_state,XBS.desc from `%s`.xe_automation_build as XAB join `%s`.xebm_build_state as XBS where build_id=? and XAB.build_state=XBS.id;";
	
	public String getReleaseTestDataCountAutomation = "SELECT SUM(case when testcase_status in (1,2,3,4) then 1 else 0 end) as  ExecutedTcCount, "
			+ " SUM(case when testcase_status = 1 then 1 else 0 end) as PassCount, "
			+ " SUM(case when testcase_status = 2 then 1 else 0 end) as FailCount,SUM(case when testcase_status = 3 then 1 else 0 end) as SkipCount, "
			+ " SUM(case when testcase_status = 4 then 1 else 0 end) as BlockCount,xb.build_id,xb.build_name,xr.release_id,xr.release_name ,'Automation' as type "
			+ " FROM `%s`.xebm_automation_execution_summary as trialSummary, `%s`.xe_project as project, `%s`.xe_automation_build as xb, `%s`.xe_release as xr "
			+ " where trialSummary.project=project.project_id and trialSummary.build=xb.build_id and xb.build_status=1 "
			+ " and xr.release_id=xb.release_id "
			+ " and trialSummary.project in (SELECT project_id FROM `%s`.xe_user_project where user = ?) "
			+ " GROUP BY trialSummary.build ";
	public String getReleaseTestDataCountManual = "SELECT SUM(case when status_id in (1,2,3,4) then 1 else 0 end) as  ExecutedTcCount, "
			+ "	SUM(case when status_id = 1 then 1 else 0 end) as PassCount, "
			+ " SUM(case when status_id = 2 then 1 else 0 end) as FailCount,SUM(case when status_id = 3 then 1 else 0 end) as SkipCount, "
			+ " SUM(case when status_id = 4 then 1 else 0 end) as BlockCount,xb.build_id,xb.build_name,xr.release_id,xr.release_name,'Manual' as type "
			+ " FROM `%s`.xe_testcase_execution_summary as trialSummary, `%s`.xe_project as project, `%s`.xe_build as xb, `%s`.xe_release as xr "
			+ " where trialSummary.project_id=project.project_id and trialSummary.build_id=xb.build_id and xb.build_status=1 "
			+ " and xr.release_id=xb.release_id "
			+ "and trialSummary.project_id in (SELECT project_id FROM `%s`.xe_user_project where user = ?) "
			+ "GROUP BY trialSummary.build_id ";
	
	public String getTestcaseDetails="select * from `%s`.xetm_testcase where testcase_id=?;";
	
	public String getTrashManualDetails = "SELECT xb.build_id,xb.build_name,xb.build_status,xr.release_id,xr.release_name,xr.release_active FROM `%s`.xe_build as xb ,`%s`.xe_release as xr where xb.release_id = xr.release_id and  xb.build_status = 2;";
	
	public String getTrashAutomationDetails = "SELECT xb.build_id,build_name,xb.build_status,xr.release_id,xr.release_name,xr.release_active FROM `%s`.xe_automation_build as xb ,`%s`.xe_release as xr where xb.release_id = xr.release_id and  xb.build_status = 2;";
	
	public String restoreAutomationBuildStatusForTrash = "UPDATE `%s`.xe_automation_build as xb SET xb.build_status = 1 WHERE xb.build_id=?; ";
	
	public String restoreManualBuildStatusForTrash = "UPDATE `%s`.xe_build as xb SET xb.build_status = 1 WHERE xb.build_id=?; ";

	public String restoreReleaseStatusForTrash = "UPDATE `%s`.xe_release as xr SET xr.release_active = 1 WHERE xr.release_id=?;";
	
	public String getExecPostCount = "SELECT count(*) as count FROM `%s`.xebm_automation_execution_summary where build= ?";
	
	public String getExecPreCount = "SELECT count(*) as count FROM `%s`.xebm_automation_build_testcase where build_id= ?";
	
	public String setBuildState = "UPDATE `%s`.`xe_automation_build` SET build_state= ? WHERE build_id = ?";
	
	public String setVmStateToStop = " UPDATE `%s`.`xebm_vm_details` SET `vm_state`=? WHERE `build_id`=? and userId=? ORDER BY xe_vm_id desc LIMIT 1";
	
	public String getAutomationTCDetails = "SELECT * FROM `%s`.xebm_automation_build_testcase where build_id = ?";
	
	public String getManualTCDetails = "SELECT * FROM `%s`.xebm_buildtestcase where build_id = ?";
	
	public String getLatestBuildId = "SELECT * FROM `%s`.xe_automation_build ORDER BY build_id desc LIMIT 1"; 
	
	public String getReleaseId = "SELECT release_id FROM `%s`.xe_release where release_name = ?";
	
	public String getBuildIdForAutoBuild = "SELECT build_id FROM `%s`.xe_automation_build WHERE build_name = ?";
	
	public String getBuildIdForManualBuild = "SELECT build_id FROM `%s`.xe_build WHERE build_name = ?";
	
	public String getAllAutoBuildsByReleaseId="SELECT * FROM `%s`.xe_automation_build WHERE release_id = ?";
			
}

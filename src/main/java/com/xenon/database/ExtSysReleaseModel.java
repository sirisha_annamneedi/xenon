package com.xenon.database;

public class ExtSysReleaseModel {

	private static ExtSysReleaseModel instance = null;
	
	protected ExtSysReleaseModel(){
		
	}
	
	public static ExtSysReleaseModel getInstance() {
		if (instance == null) {
			instance = new ExtSysReleaseModel();
		}
		return instance;
	}
	
	public  String insertExtSysRelease = "INSERT INTO "
			+"`%s`.xebm_ext_sys_release (release_title,release_description,release_startdatetime,release_enddatetime,extsys_name)"
			+ " VALUES(?,?,?,?,?)";
	
	public  String insertExtSysBuild = "INSERT INTO "
			+"`%s`.xebm_ext_sys_build (build_name,release_id,build_startdatetime,build_enddatetime)"
			+ " VALUES(?,?,?,?)";
	
	public  String insertExtTestCase = "INSERT INTO "
			+"`%s`.xebm_ext_sys_testcasesynch (qtp_test_case_number,qtp_test_case_id,build_id,test_case_title,test_case_description,"
			+ "expected_result,actual_result,status,testcase_lob,testcase_executiondate,testcase_userid,testcase_startdatetime,testcase_enddatetime)"
			+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public  String insertExtTestStep = "INSERT INTO "
			+"`%s`.xebm_ext_sys_teststepsynch (qtp_test_step_number,qtp_test_case_id,test_case_id,step_title,step_description,"
			+ "expected_result,actual_result,status,screenshot)"
			+ " VALUES(?,?,?,?,?,?,?,?,?)";
	
	public String getReleaseDetails = "Select release_id from `%s`.xebm_ext_sys_release where release_title = ? ";
	
	public String getBuildDetails = "Select build_id from `%s`.xebm_ext_sys_build where build_name = ? and release_id = ?";
}

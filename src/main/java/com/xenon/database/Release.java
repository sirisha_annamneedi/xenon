package com.xenon.database;

public class Release {
	private static Release instance = null;

	protected Release() {

	}

	public static Release getInstance() {
		if (instance == null) {
			instance = new Release();
		}
		return instance;
	}

	public  String insertRelease = "INSERT INTO "
			+"`%s`.xe_release (release_name,release_description,release_start_date,release_end_date,release_active)"
			+ " VALUES(?,?,?,?,?)";
	
	public  String allReleaseDetails = "SELECT release_id,release_name,release_description,"
			+ " release_start_date as startDate,"
			+ " release_end_date as endDate,"
			+ " active.status as releaseStatus"
			+ " FROM `%s`.xe_release as rel"
					+ " INNER JOIN `%s`.xe_active as active ON active.active_id=rel.release_active and rel.release_active=1 ORDER BY release_id DESC";
	
	public  String getReleaseDetails = "SELECT release_id,release_name,release_description,"
			+ " release_start_date as startDate,"
			+ " release_end_date as endDate,"
			+ " active.status as releaseStatus"
			+ " FROM `%s`.xe_release as rel"
					+ " INNER JOIN `%s`.xe_active as active ON active.active_id=rel.release_active"
					+ " WHERE release_id=?";
	
	public  String updateRelease = "UPDATE `%s`.xe_release"
			+ " SET "
			+ " release_name=?,"
			+ " release_description=?,"
			/*+ " release_start_date=?,"*/
			+ " release_end_date=?,"
			+ " release_active=?"
			+ " WHERE release_id=?";

	public  String updateReleaseName = "UPDATE `%s`.xe_release SET release_name=? WHERE release_id=?";
	
	public  String deleteRelease = "UPDATE `%s`.xe_release SET release_active = 2 WHERE release_id=?";
	public String deleteReleaseBuildsSoftlyManual = "UPDATE `%s`.xe_build SET build_status = 2 WHERE release_id= ?";
	public String deleteReleaseBuildsSoftlyAutomation = "UPDATE `%s`.xe_automation_build SET build_status = 2  WHERE release_id= ?";
	
	public String activeReleaseNames = "SELECT release_id,release_name"
			+ " FROM `%s`.xe_release"
			+ " WHERE release_active=1;";
	
	
	public String createNewRelease="{?= CALL `%s`.createNewRelease(?,?,?,?,?)}";
	
	public String getReleaseId = "SELECT release_id FROM `%s`.xe_release WHERE release_name='%s'";
	
	public String releaseTCCount = "select count(xebm_buildtestcase.tc_id) as tc_counter, xe_release.release_id, xe_release.release_name "
			+ " from `%s`.xebm_buildtestcase, `%s`.xe_build, `%s`.xe_release "
			+ " where xebm_buildtestcase.build_id = xe_build.build_id and xe_build.release_id = xe_release.release_id "
			+ " and xebm_buildtestcase.build_id IN (select xe_build.build_id from `%s`.xe_build, `%s`.xe_release where xe_build.release_id = xe_release.release_id "
			+ " and xe_build.release_id IN (select xe_release.release_id from `%s`.xe_release)) group by xe_release.release_id;";
}

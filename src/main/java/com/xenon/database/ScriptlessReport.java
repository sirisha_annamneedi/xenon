package com.xenon.database;

public class ScriptlessReport {
	private static ScriptlessReport instance = null;

	protected ScriptlessReport() {

	}

	public static ScriptlessReport getInstance() {
		if (instance == null) {
			instance = new ScriptlessReport();
		}
		return instance;
	}
	
	public String getStepScreenshot = "SELECT screenshot FROM `%s`.xesfdc_automation_steps_execution_summary WHERE step_exe_id = ?";
}

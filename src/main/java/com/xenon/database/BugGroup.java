package com.xenon.database;

/**
 * 
 * @author navnath.damale
 *
 */
public class BugGroup {

	private static BugGroup instance = null;
	protected BugGroup() {

	}
	public static BugGroup getInstance() {
		if (instance == null) {
			instance = new BugGroup();
		}
		return instance;
	}
	
	public String insertBugGroupRecords= "INSERT INTO `%s`.xebt_bug_group_details(user,group_id)"
			+ " values(?,?)";
	
	public String createNewBugGroup="{?= CALL %s.createNewBugGroup(?)}";
	
	public String updateBugGroups = "Delete FROM `%s`.xebt_bug_group_details where group_id = ?";
	
	public String  updateBugGroupDetails  = "UPDATE `%s`.xebt_bug_group SET bug_group_name = ? where bug_group_id = ?";
	
	public String getBuggroups=" SELECT * FROM `%s`.xebt_bug_group;";
	
	public String getUserbyGroup="SELECT bgd.user as user_id,(SELECT CONCAT(first_name,' ',last_name) as userName FROM `%s`.xe_user_details where user_id=bgd.user) AS userName FROM `%s`.xebt_bug_group_details AS bgd where bgd.group_id=?";
	
	public String getGroupbyId="SELECT bug_group_name FROM `%s`.xebt_bug_group where bug_group_id=?";
}

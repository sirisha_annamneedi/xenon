package com.xenon.database;

public class EnvironmentDetail {
	public String insertEnvDetatils="insert into `%s`.xebm_environment_details(env_name,description,config_parameter,status) values(?,?,?,?)";
	
	public String getEnvDetails="select xed.*,xa.status as envStatus from `%s`.xebm_environment_details xed,`%s`.xe_active xa where xed.status not in (3) and xed.status=xa.active_id";
	
	public String getEnvDetailById="select * from `%s`.xebm_environment_details where env_id=?";
	
	public String updateEnvDetails="update `%s`.xebm_environment_details set env_name=?,description=?,config_parameter=?,status=? where env_id=?";
	
	public String deleteEnvDetails="update `%s`.xebm_environment_details set status=3 where env_id=?";

}

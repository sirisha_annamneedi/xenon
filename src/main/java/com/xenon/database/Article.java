package com.xenon.database;

public class Article {
	private static Article instance = null;

	protected Article() {

	}

	public static Article getInstance() {
		if (instance == null) {
			instance = new Article();
		}
		return instance;
	}

	public String createNewArticle="{?= CALL xenonsupport.createNewArticle(?,?,?,?,?)};";
	public String getArticle="SELECT * FROM `%s`.xes_article WHERE id=? AND article_status=1";
	public String updateArticle="UPDATE `%s`.`xes_article` SET  `art_desc` = ?,`updated_date`=NOW() WHERE `id` = ?";
	public String getArticleDetails="SELECT * FROM `%s`.xes_article WHERE subcomponent_id=? AND article_status=1";
}

package com.xenon.database;

public class VMDetail {
	private static VMDetail instance = null;

	protected VMDetail() {

	}

	public static VMDetail getInstance() {
		if (instance == null) {
			instance = new VMDetail();
		}
		return instance;
	}
	public String getAllVmDetails = "SELECT * FROM `xenonvm`.xevm_customervm_details where customer_id=?";
	public String getVmDetailsById = "SELECT * FROM `xenonvm`.xevm_customervm_details where cust_vm_id=? and customer_id=?";
	public String updateVmDetailsById = "Update `xenonvm`.xevm_customervm_details SET `hostname`=?,`ip_address`=?,`port`=?,`vm_username`=?,`vm_password`=?,`project_location`=?,`ie_status`=?,`chrome_status`=?,`firefox_status`=?,`safari_status`=?,`concurrent_exec_status`=?,`concurrent_execs`=?,`status`=?,`is_free`=?,`git_status`=? where  cust_vm_id=? and customer_id=?";
	public String insertVMDetails="{?= CALL `xenonvm`.createNewVM(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	public String insertCloudVMDetails="{?= CALL `xenonvm`.insertCloudVMDetails(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	public String getAllCloudVmDetails = "SELECT * FROM `xenonvm`.xevm_cloudevm_details";
	public String updateCloudVmDetailsById = "Update `xenonvm`.xevm_cloudevm_details SET `hostname`=?,`ip_address`=?,`port`=?,`vm_username`=?,`vm_password`=?,`project_location`=?,`ie_status`=?,`chrome_status`=?,`firefox_status`=?,`safari_status`=?,`concurrent_exec_status`=?,`concurrent_execs`=?,`status`=?,`is_free`=?,`git_status`=?  where cloude_vm_id=?";
	public String getCloudVmDetailsById = "SELECT * FROM `xenonvm`.xevm_cloudevm_details where cloude_vm_id=?";
	
} 

package com.xenon.database;

public class Testcase {

	private static Testcase instance = null;

	protected Testcase() {

	}

	public static Testcase getInstance() {
		if (instance == null) {
			instance = new Testcase();
		}
		return instance;
	}

	public String getTestcasesByProjectId = "SELECT * FROM `%s`.xetm_testcase WHERE scenario_id IN (SELECT scenario_id FROM `%s`.xetm_scenario WHERE module_id IN (SELECT module_id FROM `%s`.xe_module WHERE project_id=?))"; // "SELECT
	public String getTestcasesByModuleId = "SELECT * FROM `%s`.xetm_testcase WHERE scenario_id IN (SELECT scenario_id FROM `%s`.xetm_scenario WHERE module_id =?)";
	public String getTestcasesByScenarioId = "SELECT * FROM `%s`.xetm_testcase WHERE scenario_id=?";
	public String activeExecutionTypes = "SELECT * FROM `%s`.xetm_execution_type WHERE status=1";

	/*public String insertTestcase = "INSERT INTO `%s`.xetm_testcase (testcase_name,scenario_id,summary,precondition,status,execution_type,execution_time,author,updated_by)"
			+ "VALUES(?,?,?,?,?,?,?,?,?)";*/
	public String insertTestcase="CALL `%s`.`setPrefixes`(?,?, ?, ?, ?, ?, ?,?,?, ?, ?,?,?,?)";
	public String getTestCaseId = "SELECT testcase_id FROM `%s`.xetm_testcase WHERE testcase_name=? and scenario_id = ? and summary = ? and precondition = ? and author =? and created_date=?";
	//public String getAllTestcases = "SELECT * FROM `%s`.xetm_testcase";
	public String getAllTestcases = "SELECT * FROM `%s`.xetm_testcase where module_id in (SELECT module_id FROM `%s`.xe_user_module where user_id=?)";
	
	public String IsRedwoodTestCaseIdAlreadyMapped = "select exists(select * from `%s`.xetm_testcase where redwood_tc_id=? limit 1)";
	
	public String updateTestcase = "UPDATE `%s`.xetm_testcase  SET `testcase_name`=?,`summary`=?,`precondition`=?,`status`=?,`execution_type`=?,`execution_time`=?,`updated_by`=? where `testcase_id`=?";
	public String getProjectModuleScenarioIdByTcId="SELECT M.project_id,S.module_id,T.scenario_id FROM `%s`.xe_module M,`%s`.xetm_scenario S,`%s`.xetm_testcase T where T.testcase_id=? and T.scenario_id=S.scenario_id and M.module_id=S.module_id";
	
	public String insertTestCaseExecSummary="INSERT INTO `%s`.`xe_testcase_execution_summary` ( `tc_id`, `build_id`, `project_id`, `mudule_id`, `scenario_id`, `tester_id`, `status_id`, `notes`,`complete_status`) VALUES ( ?, ?, ?,?,?, ?, ?, ?,?);";
	public String updateTestCaseExecSummary="Update `%s`.`xe_testcase_execution_summary` SET  `status_id`=?, `notes`=?,`complete_status`=? where `tc_id`=? and `build_id`=?";
	
	
	public String getTestcaseDetails = "SELECT * FROM `%s`.xetm_testcase WHERE testcase_id=? ";
	public String getStatus = "SELECT * FROM `%s`.xetm_testcase_status";
	public String insertStep = "INSERT INTO `%s`.xetm_testcase_steps (testcase_id,action,result,test_step_id)"
			+ "VALUES(?,?,?,?)";
	public String insertData = "INSERT INTO `%s`.xetm_testcase_data (testcase_id,user_id,data)"
			+ "VALUES(?,?,?)";
	public String updateData = "UPDATE `%s`.xetm_testcase_data  SET `user_id`=?,`data`=? where `testcase_id`=?";
	
	public String getTestData = "SELECT t.test_prefix,td.data FROM `%s`.xetm_testcase_data td,`%s`.xetm_testcase t WHERE t.testcase_id=td.testcase_id and td.testcase_id in (?)";

	public String updateStep = "UPDATE `%s`.xetm_testcase_steps  SET `action`=?,`result`=? where `step_id`=? and `test_step_id`=?";
	
	public String getAllTestcaseSteps = "SELECT * FROM `%s`.xetm_testcase_steps";

	
	public String getTestStatus="SELECT testcase_id FROM `%s`.xebm_buildtestcase where testcase_id=? and build_id=?";
	
	public String getTestExe="SELECT tc_id FROM `%s`.xe_testcase_execution_summary WHERE tc_id=? and build_id=?";

	
	public String getTestcaseDetailsByPrefix = "SELECT * FROM `%s`.xetm_testcase WHERE test_prefix=? ";
	public String getTestcaseDetailsByPrefixList = "SELECT * FROM `%s`.xetm_testcase WHERE test_prefix IN (?) AND projectid =?";
	public String getProjectModuleScenarioIdByTcPrefix="SELECT M.project_id,S.module_id,T.scenario_id,T.testcase_id FROM `%s`.xe_module M,`%s`.xetm_scenario S,`%s`.xetm_testcase T where T.test_prefix=? and T.scenario_id=S.scenario_id and M.module_id=S.module_id";

	public String getExecStatus = "SELECT * FROM `%s`.xetm_exec_status where status=1";
	public String getTestRemoveStatus="SELECT removetestcases FROM `%s`.xe_bm_access WHERE role_id IN (select role_id FROM `%s`.xe_user_details WHERE user_id=?)";
	
	
	public String selectTestCaseDetailsByBuildId="SELECT * FROM `%s`.xetm_testcase where testcase_id IN (SELECT testcase_id FROM `%s`.xebm_buildtestcase where build_id=?);";
	
	public String getTestCaseDetailsByBuildAndModuleId = "SELECT count(*) TcCount FROM `%s`.xebm_buildtestcase where build_id=? and module_id=? ";
	
	public String getTestCaseDetailsByBuildAndScenarioId = "SELECT count(*) TcCount FROM `%s`.xebm_buildtestcase where build_id=? and scenario_id=? ";
	
	public String getExecutedTestCaseDetailsByBuildAndModuleId="SELECT count(*) TcCount,SUM(case when status_id = 1 then 1 else 0 end) as PassCount,SUM(case when status_id = 2 then 1 else 0 end) as FailCount,SUM(case when status_id = 3 then 1 else 0 end) as SkipCount,SUM(case when status_id = 4 then 1 else 0 end) as BlockCount FROM `%s`.xe_testcase_execution_summary where build_id=? and mudule_id=? and complete_status=1";
	
	public String getExecutedTestCaseDetailsByBuildAndScenarioId="SELECT count(*) TcCount,SUM(case when status_id = 1 then 1 else 0 end) as PassCount,SUM(case when status_id = 2 then 1 else 0 end) as FailCount,SUM(case when status_id = 3 then 1 else 0 end) as SkipCount,SUM(case when status_id = 4 then 1 else 0 end) as BlockCount FROM `%s`.xe_testcase_execution_summary where build_id=? and scenario_id=? and complete_status=1";
	
	public String deleteTestCase="delete from `%s`.xetm_testcase where author=? and testcase_id=?";
}

package com.xenon.database;

public class GitDetails {

	public String getAllInstances = "SELECT * from `%s`.xead_git_project_details xj,`%s`.xead_user_git_instance xi where xj.git_instance_id = xi.instance_id and xi.user_id = ?";

	public String insertGitProject = "insert into `%s`.xead_git_project_details(git_instance_name,git_server_url,api_token,username,created_by_user,status) values(?,?,?,?,?,?)";

	public String insertGitRepository = "insert into `%s`.xead_git_repositories(project_id,user_id,repo_name,branch_name) values(?,?,?,?)";

	public String updateGitRepository = "update `%s`.xead_git_repositories set project_id = ?,repo_name = ?, branch_name = ? where repo_id = ?";

	public String removeUsersInstancePermission = "delete from `%s`.xead_user_git_instance where user_id= ? and instance_id = ?";

	public String insertInstancePermissionToUser = "insert into `%s`.xead_user_git_instance (user_id,instance_id,update_status,view_status) "
			+ "values (?,?,'2','2')";

	public String updateUsersUpdateInstancePermission = "update `%s`.xead_user_git_instance set update_status = 1 where user_id in (?)";

	public String revokeUsersUpdateInstancePermission = "update `%s`.xead_user_git_instance set update_status = 2 where instance_id = ? and user_id in (?)";

	public String revokeUsersExecuteInstancePermission = "update `%s`.xead_user_git_instance set view_status = 2 where instance_id = ? and user_id in (?)";

	public String updateGitInstance = "update `%s`.xead_git_project_details set git_instance_name=?,git_server_url=?,api_token=?,username=?,status=? where git_instance_id=?";

	public String getInstancesByDetails = "SELECT * from `%s`.xead_git_project_details where created_by_user = ? and git_instance_id=?";

	public String getRepositoryByID = "SELECT * from `%s`.xead_git_repositories where repo_id=?";
	
	public String getAllRepositories = "SELECT * FROM `%s`.xead_git_repositories xgp,`%s`.xead_git_project_details xgpd where git_instance_id=project_id and git_instance_id=?";
	
	public String getProjectDetailsByRepositoryId = "SELECT P.git_server_url,P.api_token,P.username,R.branch_name FROM `%s`.xead_git_repositories R, `%s`.xead_git_project_details P where R.project_id=P.git_instance_id and P.status=1 and R.repo_id=?;";

}
package com.xenon.database;

public class TmDashboard {
	private static TmDashboard instance = null;
	
	protected TmDashboard() {

	}

	public static TmDashboard getInstance() {
		if (instance == null) {
			instance = new TmDashboard();
		}
		return instance;
	}
	
	public String allCounts = "select (SELECT count(*)  FROM `%s`.xe_project) as projectCount, "
			+ " (SELECT count(*) FROM `%s`.xe_module)  as moduleCount, "
			+ " (SELECT count(*) FROM `%s`.xetm_scenario)  as scenarioCount,"
			+ " (SELECT count(*) FROM `%s`.xetm_testcase) as testCaseCount";
	
	public String projectwiseCounts ="SELECT YEAR(created_date) AS year, MONTH(created_date) AS month,projectid as project , "
									 +"COUNT(DISTINCT testcase_id) as count "
									 +"FROM `%s`.xetm_testcase where YEAR(created_date)=YEAR(CURDATE())"
									 +"GROUP BY project,year, month";
	public String moduleTestCaseCounts ="SELECT project_id,project_name,(SELECT count(*) from `%s`.xetm_testcase WHERE xetm_testcase.projectid=xe_project.project_id) as test_count,(SELECT count(*) from `%s`.xe_module WHERE xe_module.project_id=xe_project.project_id) as module_count from `%s`.xe_project";
	//public String tmDashboard="SELECT * FROM (SELECT (SELECT count(*) FROM `%s`.xe_project) as projectCount, (SELECT count(*) FROM `%s`.xe_module) as moduleCount, (SELECT count(*) FROM `%s`.xetm_scenario) as scenarioCount, (SELECT count(*) FROM `%s`.xetm_testcase) as testCaseCount ) AS A join (SELECT YEAR(created_date) AS year, MONTH(created_date) AS month,projectid as project , COUNT(DISTINCT testcase_id) as count FROM `%s`.xetm_testcase where YEAR(created_date)=YEAR(CURDATE())GROUP BY project,year, month) AS B JOIN (SELECT project_id,project_name,(SELECT count(*) from `%s`.xetm_testcase WHERE xetm_testcase.projectid=xe_project.project_id) as test_count, (SELECT count(*) from `%s`.xe_module WHERE xe_module.project_id=xe_project.project_id) as module_count from `%s`.xe_project) AS C";//group by project_id,projectCount,project
	public String tmDashboard="SELECT * FROM (SELECT (SELECT count(*) FROM `%s`.xe_project) as projectCount, (SELECT count(*) FROM `%s`.xe_module) as moduleCount, (SELECT count(*) FROM `%s`.xetm_scenario) as scenarioCount, (SELECT count(*) FROM `%s`.xetm_testcase) as testCaseCount ) AS A JOIN (SELECT project_id,project_name,(SELECT count(*) from `%s`.xetm_testcase WHERE xetm_testcase.projectid=xe_project.project_id) as test_count, (SELECT count(*) from `%s`.xe_module WHERE xe_module.project_id=xe_project.project_id) as module_count, (SELECT count(*) from `%s`.xetm_scenario WHERE module_id IN (select module_id from `%s`.xe_module where xe_module.project_id=xe_project.project_id))as sce_count from `%s`.xe_project) AS C ";//group by project_id,projectCount,project
	public String getDashboardData="CALL %s.tmDashboard()";
}

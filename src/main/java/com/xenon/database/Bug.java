package com.xenon.database;

public class Bug {
	private static Bug instance = null;

	protected Bug() {

	}

	public static Bug getInstance() {
		if (instance == null) {
			instance = new Bug();
		}
		return instance;
	}

	public String priority = "SELECT * FROM `%s`.xebt_priority";
	public String status = "SELECT * FROM `%s`.xebt_status";
	public String category = "SELECT * FROM `%s`.xebt_category";
	public String activeModules = "SELECT * FROM `%s`.xe_module where module_active = 1 and project_id = ? and module_id IN(select module_id from `%s`.xe_user_module where user_id = ? )";
	public String insertBug = "CALL `%s`.`setBTPrefixes`(?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public String getBugId = "Select bug_id from `%s`.xebt_bug where bug_title = ? and bug_desc = ? and project = ? and module = ? and create_date = ? and created_by = ? ";
	public String insertBugSummary = "INSERT INTO `%s`.xebt_bugsummary (`bug_id`,`bug_status`,`bug_priority`,`assignee`,`update_date`,`updated_by`, `prev_status`, `prev_priority`,`prev_assignee`,`bug_severity`,`prev_severity`,`group_assign`,`category`,`prev_category`,`manual_build`,`prev_manual_build`,	`automation_build`,`prev_automation_build`,`is_build_assigned`,`issue_tracker_release_id`) "
			+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public String insertJiraBugTrackerLastUpdateDate = "INSERT INTO `%s`.xead_jira_details_update (`issue_tracker_project_key`,`issue_tracker_current_release_id`,`jira_bug_tracker_last_update`,`jira_test_management_last_update`) "
			+ "VALUES (?,?,?,?)";
	public String updateJiraBugTrackerLastUpdateDate="UPDATE `%s`.`xead_jira_details_update` SET `xead_jira_details_update`.`jira_bug_tracker_last_update` = ? ,`xead_jira_details_update`.`issue_tracker_current_release_id` = ?  WHERE `xead_jira_details_update`.`jira_update_id` = ?";
	
	public String getAllUpdateDateFromJiraSynch="SELECT * FROM `%s`.xead_jira_details_update where issue_tracker_project_key = ?";
	
	public String bugDetails = "Select distinct xb.bug_id,xb.bug_prefix,xb.bug_title,xb.bug_desc,xb.project,xp.project_name,xb.module,xm.module_name,DATE_FORMAT(xb.create_date , '%%d-%%b-%%Y') as create_date,xb.created_by,"
			+ "CONCAT(xud.first_name, ' ',xud.last_name) bug_author_name,xud.user_photo "
			+ "from `%s`.xebt_bug xb,`%s`.xe_project xp,`%s`.xe_module xm ,`%s`.xe_user_details xud "
			+ "where xb.project=xp.project_id and xb.module=xm.module_id and xb.created_by= xud.user_id and xb.bug_id = ? ";

	public String summaryDetails = "Select distinct (xb.bs_id),xb.bug_id,xb.bug_status,xs.bug_status currentStatus,xb.prev_status,xps.bug_status prevStatus,"
			+ "xb.bug_priority,xp.bug_priority currentPriority,xb.assignee,CONCAT(xud.first_name, ' ',xud.last_name) bug_assignee_name,  "
			+ "xb.prev_priority ,xpp.bug_priority prevPriority,xb.prev_assignee,CONCAT(xpud.first_name, ' ',xpud.last_name) prev_assignee_name,  "
			+ "DATE_FORMAT(xb.update_date , '%%d-%%b-%%Y') as update_date,DATE_FORMAT(xb.update_date , '%%d-%%m-%%Y %%h:%%i %%p') as date,xb.comment_desc,xb.updated_by,"
			+ "TIMESTAMPDIFF(MINUTE,xb.update_date,CURRENT_TIME()) AS TimeDiff,CONCAT(xu.first_name, ' ',xu.last_name) bug_updated_by,xu.user_photo, category.category_name  "
			+ "from `%s`.xebt_bugsummary xb,`%s`.xebt_status xs,`%s`.xebt_status xps ,`%s`.xebt_priority xp,`%s`.xebt_priority xpp,"
			+ "`%s`.xe_user_details xud,`%s`.xe_user_details xpud,`%s`.xe_user_details xu, `%s`.xebt_category as category  "
			+ "where xb.bug_status=xs.status_id and xb.bug_priority=xp.priority_id and xb.assignee = xud.user_id "
			+ "and xb.prev_status=xps.status_id and xb.prev_priority=xpp.priority_id and xb.prev_assignee = xpud.user_id and xb.updated_by = xu.user_id "
			+ "and xb.category = category.category_id "
			+ "and xb.bug_id = ? GROUP BY xb.bs_id DESC";
	
	public String updateBugSummary = "UPDATE `%s`.`xebt_bugsummary` SET `xebt_bugsummary`.`bug_status` = ?,`xebt_bugsummary`.`bug_priority` = ?,`xebt_bugsummary`.`assignee` = ?,`xebt_bugsummary`.`update_date` = ?,`xebt_bugsummary`.`updated_by` = ? ,`xebt_bugsummary`.`prev_status` = ?,"
			+ "`xebt_bugsummary`.`prev_priority` = ?,`xebt_bugsummary`.`prev_assignee` = ?,`xebt_bugsummary`.`comment_desc` = ?,`xebt_bugsummary`.`bug_severity` = ?,`xebt_bugsummary`.`prev_severity` = ?,`xebt_bugsummary`.`category` = ?,`xebt_bugsummary`.`prev_category` = ?,`xebt_bugsummary`.`group_assign` = ?,"
			+ "`xebt_bugsummary`.`manual_build` = ?,`xebt_bugsummary`.`prev_manual_build` = ?,`xebt_bugsummary`.`automation_build` = ?,`xebt_bugsummary`.`prev_automation_build` = ? ,`xebt_bugsummary`.`is_build_assigned` = ? ,`xebt_bugsummary`.`issue_tracker_release_id` = ? WHERE `xebt_bugsummary`.`bug_id` = ?";
	
	public String updateBugFromJira="UPDATE `%s`.`xebt_bug` SET `xebt_bug`.`bug_title` = ?,`xebt_bug`.`bug_desc` = ?,`xebt_bug`.`updated_date` = ?,`xebt_bug`.`jira_resolution`=?,`xebt_bug`.`fix_version`=? WHERE `xebt_bug`.`bug_id` = ?";
	
	public String updateBugAssignStatus="UPDATE `%s`.`xebt_bug` SET `xebt_bug`.`assign_status` = ?,`xebt_bug`.`group_status`=?,`xebt_bug`.`group_id`=? WHERE `xebt_bug`.`bug_id` = ?";
	public String createNewBugStatus="{?= CALL %s.createNewBugStatus(?)}";
	public String addUpdateBugActivity="{?= CALL %s.addUpdateBugActivity(?,?,?)}";
	public String addInsertBugActivity="{?= CALL %s.addInsertBugActivity(?,?,?)}";
	

	public String allBugDetails = "SELECT summary.bug_id,bug.group_status,bug.group_id,bug.assign_status,bug.bug_title,bug.bug_prefix,summary.update_date,bug.create_date,stat.bug_status as bugStatus,priority.bug_priority,severity.bug_severity as bugSeverity,assignee.first_name as assigneeFN ,assignee.last_name as assigneeLN,reporter.first_name as reporterFN ,reporter.last_name as reporterLN, category.category_name, bug.module, moduleTable.module_name "
			+ " FROM `%s`.xebt_bugsummary summary,`%s`.xebt_bug bug ,`%s`.xe_module as xemodule,`%s`.xebt_status as stat,`%s`.xebt_priority as priority,"
			+ " `%s`.xebt_severity as severity,`%s`.xe_user_details as assignee , `%s`.xe_user_details as reporter, `%s`.xebt_category as category, `%s`.xe_module as moduleTable"
			+ " where bug.project=? and bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and severity.severity_id= summary.bug_severity"
			+ " and priority.priority_id= summary.bug_priority and assignee.user_id = summary.assignee and bug.created_by = reporter.user_id"
			+ " and bug.module = xemodule.module_id and xemodule.module_active = 1 and bug.module IN (select module_id from `%s`.xe_user_module where user_id=?) and summary.bs_id in (select max(bs_id) from `%s`.xebt_bugsummary group by bug_id)"
			+ " and summary.category = category.category_id and moduleTable.module_id = bug.module ORDER BY summary.update_date DESC";
	
			
	public String allAssignedToMeBugDetails = "SELECT * FROM  (SELECT summary.bug_id,summary.assignee,summary.bug_status,bug.bug_prefix,bug.bug_title,summary.update_date,bug.create_date,stat.bug_status as bugStatus,priority.bug_priority,severity.bug_severity as bugSeverity,assignee.first_name as assigneeFN ,assignee.last_name as assigneeLN,reporter.first_name as reporterFN ,reporter.last_name as reporterLN, category.category_name, bug.module, moduleTable.module_name"
			+ " FROM `%s`.xebt_bugsummary summary,`%s`.xebt_bug bug ,`%s`.xe_module as xemodule ,`%s`.xebt_status as stat,`%s`.xebt_severity as severity,`%s`.xebt_priority as priority,`%s`.xe_user_details as assignee , `%s`.xe_user_details as reporter, `%s`.xebt_category as category, `%s`.xe_module as moduleTable"
			+ " where bug.assign_status=1 and summary.group_assign=1 and bug.project=? and bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority and severity.severity_id= summary.bug_severity and assignee.user_id = summary.assignee and bug.created_by = reporter.user_id "
			+ " and summary.category = category.category_id"
			+ " and bug.module = xemodule.module_id and xemodule.module_active = 1 and bug.module IN (select module_id from `%s`.xe_user_module where user_id=?) and summary.bs_id in (select bs_id from `%s`.xebt_bugsummary group by bug_id) and moduleTable.module_id = bug.module ) as table2 WHERE assignee=? and bug_status not in (5,6) "
			+ " ORDER BY update_date DESC ";

	public String allReportedByMeBugDetails = "SELECT summary.bug_id,bug.group_status,bug.group_id,bug.assign_status,bug.bug_title,bug.bug_prefix,summary.update_date,bug.create_date,stat.bug_status as bugStatus,priority.bug_priority,severity.bug_severity as bugSeverity,assignee.first_name as assigneeFN ,assignee.last_name as assigneeLN,reporter.first_name as reporterFN ,reporter.last_name as reporterLN, category.category_name, bug.module, moduleTable.module_name "
			+ " FROM `%s`.xebt_bugsummary summary,`%s`.xebt_bug bug ,`%s`.xe_module as xemodule ,`%s`.xebt_status as stat,`%s`.xebt_severity as severity,`%s`.xebt_priority as priority,`%s`.xe_user_details as assignee , `%s`.xe_user_details as reporter, `%s`.xebt_category as category , `%s`.xe_module as moduleTable"
			+ " where bug.project=? and bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority and severity.severity_id= summary.bug_severity and assignee.user_id = summary.assignee and bug.created_by = reporter.user_id and bug.created_by = ? "
			+ " and bug.module = xemodule.module_id and xemodule.module_active = 1 and bug.module IN (select module_id from `%s`.xe_user_module where user_id=?) and summary.bs_id in (select max(bs_id) from `%s`.xebt_bugsummary group by bug_id)"
			+ " and summary.category = category.category_id and moduleTable.module_id = bug.module ORDER BY summary.update_date DESC";

	public String getUserList = "SELECT user_id,first_name,last_name from `%s`.xe_user_details userDetails where user_id  in (SELECT module.user_id FROM `%s`.xe_user_module as module WHERE module_id = ?) and user_id in (SELECT project.user FROM `%s`.xe_user_project as project WHERE project_id = ?) and user_id IN ( SELECT user_id FROM `%s`.xe_user_details where user_status=1) ";

	public String getPriorityDetailsById = "SELECT * FROM `%s`.xebt_priority where priority_id=?";
	public String getStatusDetailsById = "SELECT * FROM `%s`.xebt_status where status_id=?";


	
	public String allOpenBugs = "SELECT * FROM  (SELECT * "
			+ " FROM (SELECT summary.bug_id,summary.bug_status,bug.bug_title,bug.bug_prefix,update_date,bug.create_date,stat.bug_status as bugStatus,priority.bug_priority,assignee.first_name as assigneeFN ,assignee.last_name as assigneeLN,reporter.first_name as reporterFN ,reporter.last_name as reporterLN"
			+ " FROM `%s`.xebt_bugsummary summary,`%s`.xebt_bug bug ,`%s`.xe_module as xemodule,`%s`.xebt_status as stat,`%s`.xebt_priority as priority,`%s`.xe_user_details as assignee , `%s`.xe_user_details as reporter"
			+ " where bug.project=? and bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority and assignee.user_id = summary.assignee and bug.created_by = reporter.user_id"
			+ " and bug.module = xemodule.module_id and xemodule.module_active = 1 and bug.module IN (select module_id from `%s`.xe_user_module where user_id=?) order by bs_id desc) as table1 GROUP BY bug_id) as table2 WHERE bug_status  not in (5) ";
	
	public String bugStatistics = "SELECT SUM(IF(bug_priority=1,1,0)) priority," 
					+"SUM(!(IF(bug_status=5,1,0))) status_new,"
					+"SUM(IF(bug_status=5,1,0)) status_closed," 
					+"SUM(IF(assignee=?,1,0) && !(IF(bug_status=5,1,0) || IF(bug_status=6,1,0))  ) assigned_to_me "
					+"FROM (SELECT *  FROM (SELECT summary.bug_id,summary.bs_id,summary.bug_priority,summary.bug_status,summary.assignee "
                    +"FROM `%s`.xebt_bugsummary summary,`%s`.xe_module as xemodule,`%s`.xebt_bug bug  where bug.project=? and bug.bug_id = summary.bug_id  and bug.module = xemodule.module_id and xemodule.module_active = 1 and bug.module IN (select module_id from `%s`.xe_user_module where user_id=?) order by bs_id desc) as table1 GROUP BY bug_id)bugtable";
 
	public String insertBugAttachment = "INSERT INTO `%s`.xebt_bug_attachment (`bug_id`,`attach_path`,`attach_title`)VALUES (?,?,?)";
	public String insertCommentAttachment = "INSERT INTO `%s`.xebt_bugcomment_attachment (`comment_id`,`attach_path`,`attach_title`)VALUES (?,LOAD_FILE(?),?)";
	   
	public String bugAttachments = "SELECT attach_id,attach_title FROM `%s`.xebt_bug_attachment where bug_id = ?";
	public String bugAttachmentById = "SELECT attach_id,attach_path FROM `%s`.xebt_bug_attachment where attach_id = ?";
	public String getSummaryId = "Select bs_id from `%s`.xebt_bugsummary where bug_id = ? and bug_status = ? and bug_priority=? and assignee=? and update_date = ? and  updated_by=? and prev_status=? and prev_priority=? and prev_assignee=? and comment_desc=?";
	public String commentAttachments = "SELECT attach_id,attach_title,comment_id FROM `%s`.xebt_bugcomment_attachment where comment_id = ?";
	public String commentAttachmentById = "SELECT attach_id,attach_path FROM `%s`.xebt_bugcomment_attachment where attach_id = ?";
	public String getBugIdByPrefix = "Select * from `%s`.xebt_bug where bug_prefix = ?";
	public String getActiveUsers = "SELECT userPro.project_id,userPro.user as user_id,CONCAT(user.first_name,' ',user.last_name) as userName "
			+ " FROM `%s`.xe_user_project as userPro "
			+ " LEFT JOIN `%s`.xe_user_details as user ON user.user_id = userPro.user "
			+ " where userPro.project_id = ? and user.user_status = 1;";

	
	public String getAllPriorityDetails = "SELECT * FROM `%s`.xebt_priority where priority_status=1;";
	
	public String getAllStatusDetails = "SELECT * FROM `%s`.xebt_status where bug_state=1;";
	
	public String getBugGraphData="SELECT count(*) as bug_count,DATE(update_date) as weekday, "
			+ " bugStatus FROM (SELECT bug.project,summary.bug_id,bug.bug_title,"
			+ "bug.bug_prefix,update_date,bug.create_date,stat.bug_status as bugStatus,priority.bug_priority,assignee.first_name "
			+ "as assigneeFN ,assignee.last_name as assigneeLN,reporter.first_name as reporterFN ,reporter.last_name as reporterLN "
			+ "FROM `%s`.xebt_bugsummary summary,`%s`.xebt_bug bug , `%s`.xe_module as xemodule,`%s`.xebt_status as stat,`%s`.xebt_priority as priority,"
			+ "`%s`.xe_user_details as assignee , `%s`.xe_user_details as reporter where bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id "
			+ "and priority.priority_id= summary.bug_priority and bug.project=? and assignee.user_id = summary.assignee and bug.created_by = reporter.user_id and"
			+ " bug.module = xemodule.module_id and xemodule.module_active = 1 and bug.module IN (select module_id from `%s`.xe_user_module "
			+ "where user_id=?) order by bs_id desc) as table1 group by  DATE(update_date) limit 7;";
	
	public String getBugStatus="SELECT count(*) count, bugStatus FROM (SELECT * FROM (SELECT bug.project,summary.bug_id,bug.bug_title,bug.bug_prefix,update_date,bug.create_date,stat.bug_status as bugStatus,priority.bug_priority,assignee.first_name as assigneeFN ,assignee.last_name as assigneeLN,reporter.first_name as reporterFN ,reporter.last_name as reporterLN FROM `%s`.xebt_bugsummary summary,`%s`.xebt_bug bug , `%s`.xe_module as xemodule,`%s`.xebt_status as stat,`%s`.xebt_priority as priority,`%s`.xe_user_details as assignee , `%s`.xe_user_details as reporter where bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority and bug.project=? and assignee.user_id = summary.assignee and bug.created_by = reporter.user_id and bug.module = xemodule.module_id and xemodule.module_active = 1 and bug.module IN (select module_id from `%s`.xe_user_module where user_id=?) order by bs_id desc) as table1 WHERE DATE(update_date)=? group by bug_id) as table3 group by bugStatus";
	
	public String getBugIdFromJiraId="SELECT bug_id from `%S`.xebt_bug where jira_id = ?";
	
	public String getBugGraphStatus="SELECT distinct(update_date),summary.bug_id, DATE(DATE_ADD(update_date, INTERVAL(1-DAYOFWEEK(update_date)) DAY)) weekday,stat.bug_status bugStatus FROM `%s`.xebt_bugsummary summary,`%s`.xebt_bug bug , `%s`.xe_module xemodule,`%s`.xebt_status as stat where bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and bug.project=? and bug.module = xemodule.module_id and xemodule.module_active = 1 and bug.module IN ( select module_id from `%s`.xe_user_module where user_id=? ) group by bug_id,bugStatus order by bs_id desc";
	
	public String getAllBugGraphStatus="SELECT distinct(update_date),summary.bug_id, DATE(DATE_ADD(update_date, INTERVAL(1-DAYOFWEEK(update_date)) DAY)) weekday,stat.bug_status bugStatus FROM `%s`.xebt_bugsummary summary,`%s`.xebt_bug bug , `%s`.xe_module xemodule,`%s`.xebt_status as stat where bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and bug.project IN (SELECT distinct(xu.project_id) FROM `%s`.xe_user_project xu RIGHT JOIN `%s`.xe_project xp ON xp.project_id=xu.project_id WHERE xu.user=1 and xp.project_active=1) and bug.module = xemodule.module_id and xemodule.module_active = 1 and bug.module IN ( select module_id from `%s`.xe_user_module where user_id=? ) group by bug_id,bugStatus order by bs_id desc";
	
	public String getBugreporttable="SELECT bug_id,bug_title, (SELECT project_name FROM `%s`.xe_project WHERE project_id=project) as project_name, (SELECT concat(first_name,' ' ,last_name) From `%s`.xe_user_details WHERE user_id=created_by) as creator, bug_prefix  FROM `%s`.xebt_bug as bg Where bug_id in (SELECT bug_id FROM `%s`.xebt_bugsummary WHERE bs_id IN (SELECT MAX(bs_id) FROM `%s`.xebt_bugsummary where bug_priority=1 GROUP BY bug_id));";

	public String getDashboardData = "select * from (select * from (select xbs.bug_id,xbs.group_assign,xb.group_status,xb.group_id,xb.assign_status,xb.bug_title,xb.bug_desc,xb.project,@moduleId:=xb.module as module,xb.create_date,xb.created_by,xb.bug_prefix,xbs.bug_status,stat.bug_status as bugStatus,priority.bug_priority as bugPriority,xbs.bug_priority,severity.bug_severity as bugSeverity,xbs.bug_severity,xbs.assignee,xbs.update_date,xbs.updated_by,category.category_name, xp.project_name,xp.project_description,xm.module_name,xm.module_description,xum.user_id,xud.user_photo,xud.first_name as assigneeFN ,xud.last_name as assigneeLN,xu.first_name as reporterFN ,xu.last_name as reporterLN,(select count(user_id) from `%s`.xe_user_module  where @moduleId = xe_user_module.module_id and xe_user_module.user_id=?) as user_count from `%s`.xebt_bug xb,`%s`.xebt_bugsummary xbs,`%s`.xe_project xp,`%s`.xe_module xm , `%s`.xe_user_module xum,`%s`.xe_user_details xu,`%s`.xe_user_details xud,`%s`.xebt_status as stat,`%s`.xebt_priority as priority ,`%s`.xebt_severity as severity, `%s`.xebt_category as category where xb.module IN (SELECT module_id FROM `%s`.xe_user_module xum WHERE xum.user_id=?) AND xb.project IN (SELECT project_id FROM `%s`.xe_user_project xup WHERE xup.user=?) AND xb.bug_id=xbs.bug_id and xb.project=xp.project_id and xb.module = xm.module_id and xum.module_id = xm.module_id and  xu.user_id = xb.created_by and xud.user_id = xbs.assignee and xbs.bug_status = stat.status_id and priority.priority_id = xbs.bug_priority and xbs.category = category.category_id and severity.severity_id = xbs.bug_severity and xp.project_active = 1 and xm.module_active = 1 order by bs_id desc ) as table1 group by bug_id)as table2 order by project";
	
	public String updateBugsumaryStatus="UPDATE `%S`.`xebt_bugsummary` SET `group_assign` = 2 WHERE `bug_id` = ?";
	
	public String getBugComments="SELECT distinct (xb.bs_id),xb.group_assign,xb.bug_id,xb.bug_status,xs.bug_status currentStatus,xb.prev_status,xb.prev_severity,xb.bug_severity,xb.category,xb.prev_category,xps.bug_status prevStatus, xb.bug_priority,xp.bug_priority currentPriority,xb.bug_severity,severity.bug_severity currentSeverity,curCategory.category_name currentCategory,xb.assignee,CONCAT(xud.first_name, ' ',xud.last_name) bug_assignee_name, xb.prev_priority ,xpp.bug_priority prevPriority,xb.prev_priority ,prev_severity.bug_severity prevSeverity,previousCategory.category_name prevCategory,xb.prev_assignee,CONCAT(xpud.first_name, ' ',xpud.last_name) prev_assignee_name,  xb.update_date,DATE_FORMAT(xb.update_date , '%d-%m-%Y %h:%i %p') as date,xb.comment_desc,xb.updated_by, TIMESTAMPDIFF(MINUTE,xb.update_date,CURRENT_TIME()) AS TimeDiff,CONCAT(xu.first_name, ' ',xu.last_name) bug_updated_by,xu.user_photo  from `%s`.xebt_bugsummary xb,`%s`.xebt_status xs,xebt_status xps ,`%s`.xebt_priority xp,xebt_priority xpp,`%s`.xebt_severity severity,`%s`.xebt_severity prev_severity,`%s`.xebt_category curCategory,`%s`.xebt_category previousCategory,	`%s`.xe_user_details xud,`%s`.xe_user_details xpud,`%s`.xe_user_details xu where xb.bug_status=xs.status_id and xb.bug_priority=xp.priority_id and xb.bug_severity=severity.severity_id and xb.category=curCategory.category_id and xb.assignee = xud.user_id  and xb.prev_status=xps.status_id and xb.prev_priority=xpp.priority_id and xb.prev_severity=prev_severity.severity_id and xb.prev_category=previousCategory.category_id  and xb.prev_assignee = xpud.user_id and xb.updated_by = xu.user_id and xb.bug_id = ? GROUP BY xb.update_date DESC LIMIT ?,?; ";
	
	public String getListOfColumns="SHOW COLUMNS FROM `%S`.xebt_bugsummary";
	
	public String deleteBug="delete from `%s`.xebt_bug where bug_id=?";
	
	public String deleteAllBugs="delete from `%s`.xebt_bug";

} 

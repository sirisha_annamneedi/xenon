package com.xenon.database;

public class OauthUser {

	private static OauthUser oauthUserInstance=null;
	
	protected OauthUser()
	{}
	
	public static OauthUser getInstance()
	{
		if(oauthUserInstance==null)
		{
			oauthUserInstance= new OauthUser();
		}
		return oauthUserInstance;
	}
	
	public String insertOauthUser="insert into `%s`.auth_details(username, password) values(?,?)";
	public String insertOauthClient="INSERT INTO `%s`.oauth_client_details(client_id, resource_ids, client_secret, scope, authorized_grant_types, authorities, access_token_validity, refresh_token_validity)"
			+ "VALUES (?, 'rest_api', '$2a$10$WAQZbr4C58raVdWTVWka7OR/0BnUKBRBfAAQ23fMgLAzOQxvjhYba', 'trust,read,write', 'password,authorization_code,refresh_token,implicit', 'ROLE_USER', '120', '1000')";
}
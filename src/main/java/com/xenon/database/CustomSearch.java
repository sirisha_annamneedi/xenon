package com.xenon.database;

public class CustomSearch {

	private static CustomSearch instance = null;

	protected CustomSearch() {

	}

	public static CustomSearch getInstance() {
		if (instance == null) {
			instance = new CustomSearch();
		}
		return instance;
	}

	public String insertCustomSearchQuery = "{?= CALL `%s`.insertCustomSearchQuery(?,?,?,?)}";

	public String getUserSavedCustomQueries = "SELECT * FROM `%s`.xebt_customizesearch where user_id=?;";

	public String updateCustomSearchQuery="UPDATE `%s`.`xebt_customizesearch` SET `queryname`=?, `query`=?, `queryjson`=?, `user_id`=? WHERE `idxebt`=?;";

	public String insertPiechartCustomSearchQuery = "{?= CALL `%s`.insertPiechartCustomSearchQuery(?,?,?,?)}";
	
	public String getUserSavedPieChartCustomQueries = "SELECT * FROM `%s`.xebt_piechartsearch where user_id=?;";
	
	public String updatePieChartSearchQuery="UPDATE `%s`.`xebt_piechartsearch` SET `queryname`=?, `query`=?, `queryjson`=?, `user_id`=? WHERE `idxebt`=?;";
	
	public String deleteCustomSearchQuery="DELETE FROM `%s`.`xebt_customizesearch` WHERE `idxebt`=?;";


}

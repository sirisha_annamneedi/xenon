package com.xenon.database;

public class AutomationBuild {
	private static AutomationBuild instance = null;

	protected AutomationBuild() {

	}

	public static AutomationBuild getInstance() {
		if (instance == null) {
			instance = new AutomationBuild();
		}
		return instance;
	}
	
	public String insertbuild = "INSERT INTO `%s`.xebm_trial_build "
			+ " (build_name,build_creator,build_createdate)"
			+ " VALUES(?,?,now())";
	
	public String getAllBuildDetails = "SELECT build_id,build_name,DATE_FORMAT(build_createdate , '%%d-%%m-%%Y %%h:%%i %%p') build_createdate,Concat(user.first_name ,' ', user.last_name) as creator"
			+ " FROM `%s`.xebm_trial_build as build"
			+ " inner join `%s`.xe_user_details as user on user.user_id = build.build_creator";
	
	public String getBuildTrial ="SELECT build_id ,first_name,last_name FROM %s.xebm_trial_build as tb ,%s.xe_user_details as ud where tb.build_creator = ud.user_id and tb.build_name =?";
	public String getAutoBuild ="SELECT build_id ,first_name,last_name FROM %s.xebm_auto_build as tb ,%s.xe_user_details as ud where tb.build_creator = ud.user_id and tb.build_name =?";
	public String getAutomationBuild = "SELECT (SELECT build_name FROM `%s`.xe_automation_build WHERE build_id=auto_build.build_id) AS build_name,(SELECT project_location FROM `%s`.xebm_vm_details WHERE build_id=auto_build.build_id ORDER BY xe_vm_id DESC LIMIT 1) AS project_location ,(SELECT concat('http://',ip_address,':',port) FROM `%s`.xebm_vm_details WHERE xebm_vm_details.build_id=auto_build.build_id ORDER BY xe_vm_id DESC LIMIT 1) AS inetaddress, step_wise_screenshot,build_id,vm_id,report_bug, (SELECT concat(first_name,'_',last_name) FROM `%s`.xe_user_details WHERE user_id=auto_build.user_id) AS user_name FROM `%s`.xebm_build_exec_details AS auto_build WHERE auto_build.build_id=? ORDER BY start_time desc limit 1";
	public String getAutomationTest ="SELECT (SELECT test_prefix FROM `%s`.xetm_testcase WHERE testcase_id=auto_build.testcase_id) AS test_prefix,(SELECT testcase_name FROM `%s`.xetm_testcase WHERE testcase_id=auto_build.testcase_id) AS test_name FROM `%s`.xebm_automation_build_testcase AS auto_build WHERE build_id=?";
	public String getApiTest="SELECT XT.test_prefix,XT.testcase_name,XT.summary AS url,XT.precondition AS bodyParam, XM.method_name, XCT.content_type, XTS.action AS header FROM `%s`.xebm_automation_build_testcase AS XBT join `%s`.xetm_testcase AS XT join `%s`.xetm_testcase_steps AS XTS join `%s`.xetm_apitestcase_method AS XM join `%s`.xetm_apitestcase_content_type AS XCT WHERE XT.testcase_id=XBT.testcase_id AND XT.testcase_id=XTS.testcase_id AND XT.execution_time=XM.method_id AND XT.excel_file_present_status=XCT.content_type_id AND XBT.build_id=?;";
	
	public String createNewTrialBuild="{?= CALL `%s`.createNewTrialBuild(?,?)}";
	
	public String createNewAutoBuild="{?= CALL `%s`.createNewAutoBuild(?,?)}";
	
	public String updatetrialLogfile="UPDATE `%s`.`xebm_trial_build` SET `build_logfile` = ?  WHERE `build_name` = ?";
	public String updateBuildLogfile="UPDATE `%s`.`xe_automation_build` SET `build_logpath` = ?  WHERE `build_id` = ?";
	public String insertExecution="INSERT INTO `xebm_execution`(`build_stamp`,`schema_name`,`build_id`)VALUES(?,?,?)";
	
	public String GET_TEST_PREFIXES = "SELECT xt.test_prefix FROM `%s`.xetm_testcase xt " +
										"JOIN `%s`.xebm_automation_build_testcase xabt ON xt.testcase_id = xabt.testcase_id " +
										"WHERE xabt.build_id = ?";
	

	public String insertbuildExecDetails="INSERT INTO `xenonvm`.`xevm_build_exec_details` (`exec_id`, `exec_json`, `vm_ip`,`vm_id`,`vm_port`,`build_id`, `user_id`, `customer_id`, `customer_schema`) VALUES (0, ?,?,?,?,?,?,?,?);";

	public String updateScriptBuildLogfile="UPDATE `%s`.`xesfdc_automation_build` SET `build_logpath` = ?  WHERE `build_id` = ?";
	
	public String insertScriptBuildExecution="INSERT INTO `xesfdc_execution`(`build_stamp`,`schema_name`,`build_id`)VALUES(?,?,?)";
	
	public String getScriptlessBuild = "SELECT (SELECT build_name FROM `%s`.xesfdc_automation_build WHERE build_id=auto_build.build_id) AS build_name,(SELECT A.login_url FROM `%s`.xesfdc_env A,`%s`.xesfdc_automation_build B where B.build_env=A.env_id and B.build_id=?) AS loginUrl,(SELECT project_location FROM `%s`.xesfdc_vm_details WHERE build_id=auto_build.build_id ORDER BY xe_vm_id DESC LIMIT 1) AS project_location ,(SELECT concat('http://',ip_address,':',port) FROM `%s`.xesfdc_vm_details WHERE xesfdc_vm_details.build_id=auto_build.build_id ORDER BY xe_vm_id DESC LIMIT 1) AS inetaddress, step_wise_screenshot,build_id,vm_id,report_bug, (SELECT concat(first_name,'_',last_name) FROM `%s`.xe_user_details WHERE user_id=auto_build.user_id) AS user_name FROM `%s`.xesfdc_build_exec_details AS auto_build WHERE auto_build.build_id=? ORDER BY start_time desc limit 1";
	
	public String getScriptlessTest ="SELECT sfdc_testcase_id FROM `%s`.xesfdc_automation_build_testcase where sfdc_build_id=?;";

}

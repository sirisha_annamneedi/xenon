package com.xenon.database;

public class JenkinsDetails {

	public String insertInstance = "insert into `%s`.xead_jenkins_details (jenkin_url, jenkin_token, jenkin_username, jenkin_name,jenkin_instance_status) "

			+ "values (?,?,?,?,?)";	
	
	public String insertJob = "insert into `%s`.xead_jenkin_jobs (instance, jenkin_job, job_name, job_description,job_status,build_status,build_trigger_token,user_id) "
			+ "values (?,?,?,?,?,?,?,?)";	
	
	public String updateJob = "update `%s`.xead_jenkin_jobs set job_name = ?, job_description = ? , job_status = ? , build_trigger_token = ?"
			+ "where  jenkin_jobId = ?";
	
	public String insertJobPermissionToUser = "insert into `%s`.xead_user_jobs (user_id,job_id,update_status,execute_status) "
			+ "values (?,?,'2','2')";
	
	public String insertInstancePermissionToUser = "insert into `%s`.xead_user_jenkins_instance (user_id,instance_id,update_status,view_status) "
			+ "values (?,?,'2','2')";
	
	public String removeUsersJobPermission = "delete from `%s`.xead_user_jobs where user_id= ? and job_id = ?";
	
	public String removeUsersInstancePermission = "delete from `%s`.xead_user_jenkins_instance where user_id= ? and instance_id = ?";
	
	public String revokeUsersUpdateJobPermission = "update `%s`.xead_user_jobs set update_status = 2 where job_id = ? and user_id in (?)";
	
	public String revokeUsersExecuteJobPermission = "update `%s`.xead_user_jobs set execute_status = 2 where job_id = ? and user_id in (?)";
	
	public String revokeUsersUpdateInstancePermission = "update `%s`.xead_user_jenkins_instance set update_status = 2 where instance_id = ? and user_id in (?)";
	
	public String revokeUsersExecuteInstancePermission = "update `%s`.xead_user_jenkins_instance set view_status = 2 where instance_id = ? and user_id in (?)";
	
	public String updateUsersExecuteJobPermission = "update `%s`.xead_user_jobs set execute_status = 1 where user_id in (?)";
	
	public String updateUsersUpdateJobPermission = "update `%s`.xead_user_jobs set update_status = 1 where user_id in (?)";
	
	public String updateUsersUpdateInstancePermission = "update `%s`.xead_user_jenkins_instance set update_status = 1 where user_id in (?)";
	
	public String insertToken = "CALL insertTokenProcedure(?,?,?,?,?,?)";
		
	public String getJobDetails="SELECT * ,xjbs.build_status as buildStatus,xjbs.id as statusId FROM `%s`.xead_jenkin_jobs xjj,`%s`.xead_jenkins_details xjd,`%s`.xead_jenkins_build_status xjbs WHERE xjj.instance=xjd.jenkin_id AND xjd.jenkin_id=? AND xjj.build_status=xjbs.id and xjj.jenkin_jobId in(select job_id from `%s`.xead_user_jobs xuj where xuj.user_id=?)";
								
	public String getAllInstances = "SELECT * from `%s`.xead_jenkins_details xj,`%s`.xead_user_jenkins_instance xi where xj.jenkin_id = xi.instance_id and xi.user_id = ?";
	
	public String getAllApiTokens = "SELECT * from xead_jenkins_api_token_details where instance_id=?";
	
	public String getServerIdByServerName = "SELECT jenkin_id from `%s`.xead_jenkins_details where jenkin_name=?";

	public String getInstance="SELECT * from `%s`.xead_jenkins_details";
	
	public String insertJenkinBuild="INSERT INTO `%s`.xead_jenkins_build_details VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public String getBuildDetails="SELECT * FROM `%s`.xead_jenkins_build_details where jenkin_jobId=? order by details_id DESC";
	
	public String getTriggerBuildData="SELECT  * FROM `%s`.xead_jenkins_details as jendet ,`%s`.xead_jenkin_jobs as jenJob where jenkin_jobId = ? and jenJob.instance=jendet.jenkin_id";
	

	public String getAllInstancesById = "SELECT * from `%s`.xead_jenkins_details where jenkin_id=?";
	
	public String getJenkinsJobDetails = "SELECT * from `%s`.xead_jenkin_jobs where jenkin_jobId=?";

	public String getUpdateEnabledJobs = "SELECT * from `%s`.xead_user_jobs where job_id=? and user_id= ? and update_status = 1";
	
	public String getExecuteEnabledJobs = "SELECT * from `%s`.xead_user_jobs where job_id=? and user_id= ? and execute_status = 1";
	
	public String updateJenkinBuild="Update `%s`.xead_jenkins_build_details SET build_id = ? ,total_count = ?, fail_count = ?,skip_count = ?,duration = ?,estimated_duration = ?,result = ?,timestamp = ?,url = ?,jenkin_jobId = ? where build_name = ?";

	public String updateJobStatus ="UPDATE `%s`.`xead_jenkin_jobs` SET `build_status`=? WHERE `jenkin_jobId`=?;";

	public String updateServerDetails="UPDATE `%s`.xead_jenkins_details set jenkin_name=?,jenkin_url=?,jenkin_username=?,jenkin_token=?,jenkin_instance_status=? where jenkin_id=?";
	
	public String getBuildDetailsByBuildNumber = "SELECT * FROM `%s`.xead_jenkins_build_details where jenkin_jobId=? and build_id = ? order by details_id DESC";
	
}
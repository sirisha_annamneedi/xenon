package com.xenon.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class DatabaseDAOImpl implements DatabaseDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int insert(String sql) {

		//System.out.println("SQL :  " + sql);
		int status = jdbcTemplate.update(sql);
		return status;

	}

	@Override
	public int update(String sql) {
		//System.out.println("SQL :  " + sql);
		int status = jdbcTemplate.update(sql);
		return status;
	}
	
	@Override
	public int update(String sql,Object[] obj,int[] data) {
		//System.out.println("SQL :  " + sql);
		int status = jdbcTemplate.update(sql,obj,data);
		return status;
	}

	@Override
	public void delete(String sql) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Map<String, Object>> select(String sql) {
		List<Map<String, Object>> listOfBugs = jdbcTemplate.queryForList(sql);
		//System.out.println("SQL " + sql);

		return listOfBugs;
	}
	@Override
	public List<Map<String, Object>> select(String sql, Object[] objects, int[] data) {
		List<Map<String, Object>> listOfBugs = jdbcTemplate.queryForList(sql,objects,data);
		//System.out.println("SQL " + sql);
		return listOfBugs;
	}
	
	@Override
	public SqlRowSet selectRow(String sql, Object[] objects, int[] data) {
		SqlRowSet listOfBugs = jdbcTemplate.queryForRowSet(sql,objects,data);
		//System.out.println("SQL " + sql);
		return listOfBugs;
	}
	@Override
	public void createTable(String sql) {
		jdbcTemplate.execute(sql);		
		//System.out.println("SQL " + sql);
	}
	
	@Override
	public Connection getConnection() throws SQLException {
		return jdbcTemplate.getDataSource().getConnection();
	}

	@Override
	public int select(String sql, Object[] objects) {
		int count;
		try{
			count = jdbcTemplate.queryForObject(sql, objects, Integer.class);
		}catch(EmptyResultDataAccessException e){
			count = 0;
		}
		//System.out.println("SQL " + sql);
		return count;
	}

	@Override
	public String stringSelect(String sql, Object[] objects) {
		String result;
		try{
			result = jdbcTemplate.queryForObject(sql, objects, String.class);
		}catch(EmptyResultDataAccessException e){
			result = null;
		}
		//System.out.println("SQL " + sql);
		return result;
	}
	
	@Override
	public int update(PreparedStatementCreator preparedStatementCreator, KeyHolder keyHolder) {
		//System.out.println("SQL :  " + sql);
		int generatedKey = jdbcTemplate.update(preparedStatementCreator,keyHolder);
		
		return generatedKey;
	}
	
	@Override
	public <T> List<T> select(String sql, Object[] args, Class<T> clazz) throws Exception {
		List<T> list = new ArrayList<T>();
		list = jdbcTemplate.queryForList(sql, args, clazz);
		return list;
	}
}

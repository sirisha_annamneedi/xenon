package com.xenon.database;

public class TestCaseStep {
	private static Testcase instance = null;

	protected TestCaseStep() {

	}

	public static Testcase getInstance() {
		if (instance == null) {
			instance = new Testcase();
		}
		return instance;
	}

	public String getAllTestcaseSteps = "SELECT * FROM `%s`.xetm_testcase_steps";
	public String insertStepExecSummary="INSERT INTO `%s`.`xe_steps_execution_summary` ( `testex_id`, `step_id`, `status`, `comments`, `screenshot`,`bug_raised`) VALUES (?, ?,?,?, ?, ?);";
	public String updateStepExecSummary="Update `%s`.`xe_steps_execution_summary` SET `status`=?, `comments`=?, `screenshot`=?,`bug_raised`=? where `testex_id`=? and `step_id`=? ";
	public String getstepExecutionId = "SELECT stex_id FROM `%s`.xe_steps_execution_summary where testex_id=? and step_id =?";
	public String insertStepExecBug= "INSERT INTO `%s`.`xe_step_execution_bug` ( `build_id`,`step_exe_id`, `test_step_id`, `bug_id`,`test_case_id`) VALUES (?,?, ?,?,?);";
	public String getAllExecutedBugs = "SELECT * FROM `%s`.xe_step_execution_bug where build_id=?";
	
	
}

package com.xenon.database;

import java.util.Map;

public class Report {
	private static Report instance = null;
	/*String build=null;
	String status=null;
	String priority=null;
	String severity=null;*/
	String tcWiseFilteredData =null;
	String bugFilteredData = null;
	String automationBugFilteredData = null;
	String buildTCCount = null;
	String releaseTCCount = null;
	String manualCustomizeQuery=null;
	String automationCustomizeQuery=null;
	public String getManualCustomizeQuery() {
		return manualCustomizeQuery;
	}
	public String getAutomationCustomizeQuery() {
		return automationCustomizeQuery;
	}

	public void setManualCustomizeQuery(int projectID) {
		this.manualCustomizeQuery = "SELECT xtc.testcase_id, xtc.testcase_name, xtc.test_prefix, xp.project_id, xp.project_name, xr.release_id, xr.release_name, "
				+ "xb.build_id, xb.build_name, xm.module_id, xm.module_name, xs.scenario_id, xs.scenario_name, "
				+ "xud.user_id AS tester_id, CONCAT(xud.first_name, ' ', xud.last_name) AS tester_name, xes.exst_id, xes.description "
				+ "FROM `%s`.xe_testcase_execution_summary xtes "
				+ "JOIN `%s`.xetm_testcase xtc ON xtc.testcase_id = xtes.tc_id "
				+ "JOIN `%s`.xe_project xp ON xp.project_id = xtes.project_id "
				+ "JOIN `%s`.xe_build xb ON xb.build_id = xtes.build_id "
				+ "JOIN `%s`.xe_release xr ON xr.release_id = xb.release_id "
				+ "JOIN `%s`.xe_module xm ON xm.module_id = xtes.mudule_id "
				+ "JOIN `%s`.xetm_scenario xs ON xs.scenario_id = xtes.scenario_id "
				+ "JOIN `%s`.xe_user_details xud ON xud.user_id = xtes.tester_id "
				+ "JOIN `%s`.xetm_exec_status xes ON xes.exst_id = xtes.status_id where xp.project_id="+projectID+";";
	}
	
	public void setAutomationCustomizeQuery(int projectID) {
		this.automationCustomizeQuery = "SELECT xtc.testcase_id, xtc.testcase_name, xtc.test_prefix, xab.build_id, xab.build_name, xp.project_id, xp.project_name, "
			+ "xr.release_id, xr.release_name, "
			+ "xm.module_id, xm.module_name, xs.scenario_id, xs.scenario_name, xes.exst_id, xes.description "
			+ "FROM `%s`.xebm_automation_execution_summary xaes "
			+ "JOIN `%s`.xetm_testcase xtc ON xtc.testcase_id = xaes.testcase "
			+ "JOIN `%s`.xe_project xp ON xp.project_id = xaes.project "
			+ "JOIN `%s`.xe_automation_build xab ON xab.build_id = xaes.build "
			+ "JOIN `%s`.xe_release xr ON xr.release_id = xab.release_id "
			+ "JOIN `%s`.xe_module xm ON xm.module_id = xaes.module "
			+ "JOIN `%s`.xetm_scenario xs ON xs.scenario_id = xaes.scenario "
			+ "JOIN `%s`.xetm_exec_status xes ON xes.exst_id = xaes.testcase_status  where xp.project_id="+projectID+";";
	}
	
	protected Report() {

	}

	public String getTcWiseFilteredData() {
		return tcWiseFilteredData;
	}

	public void setTcWiseFilteredData(int userID,String project,String module,String status,String priority,String severity) {
		/*this.tcWiseFilteredData = "SELECT * FROM (SELECT testcase.projectid,project.project_name,testcase.module_id,xemodule.module_name ,summary.bug_status as btstatus,summary.bug_priority as btpriority,summary.bug_severity as btseverity,summary.bug_id,bug.bug_title,bug.bug_prefix,summary.update_date,bug.create_date,stat.bug_status as bugStatus, "
				+ " priority.bug_priority,severity.bug_severity,assignee.first_name as assigneeFN ,assignee.last_name as assigneeLN,reporter.first_name as reporterFN ,reporter.last_name as reporterLN , "
				+ " rel.release_name,build.build_name,testcase.testcase_name "
				+ " FROM `%s`.xe_step_execution_bug as stepexec , `%s`.xetm_testcase as testcase ,`%s`.xe_build as build , `%s`.xe_release as rel  ,`%s`.xebt_bugsummary summary, "
				+ " `%s`.xebt_bug bug ,`%s`.xe_module as xemodule,`%s`.xebt_status as stat,`%s`.xebt_priority as priority,`%s`.xebt_severity as severity,`%s`.xe_user_details as assignee , `%s`.xe_user_details as reporter , `%s`.xe_project as project"
				+ " where bug.project "+project+" "
				+ " and bug.bug_id = summary.bug_id "
				+ " and summary.bs_id in (SELECT max(bSumm.bs_id) FROM `%s`.xebt_bugsummary as bSumm where bSumm.bug_id = bug.bug_id)"
				+ " and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority  and severity.severity_id= summary.bug_severity "
				+ " and assignee.user_id = summary.assignee and bug.created_by = reporter.user_id "
				+ " and bug.module = xemodule.module_id and xemodule.module_active = 1  "
				+ " and bug.module "+module+" "
				+ " and testcase.projectid = project.project_id"
				+ " and bug.bug_id = stepexec.bug_id "
				+ " and testcase.testcase_id = stepexec.test_case_id  "
				+ " and build.build_id = stepexec.build_id  "
				+ " and rel.release_id = build.release_id  "
				+ " and stepexec.build_id  in ( SELECT distinct build_id FROM `%s`.xe_step_execution_bug ) "
				+ " order by bs_id desc) as bugTable "
				+ " where bugTable.btstatus "+ status +" and bugTable.btpriority "+ priority +" and bugTable.btseverity "+ severity +" "
				+ " GROUP BY bug_id ";*/
		this.tcWiseFilteredData="select xebt_bug.bug_id, xebt_bug.bug_title ,xe_build.build_name, xe_release.release_name, xebt_status.bug_status, xebt_priority.bug_priority," 
		+"xebt_severity.bug_severity, xetm_testcase.testcase_name, xe_project.project_name, xe_module.module_name, xebt_bug.bug_prefix "
		+"from `%s`.xebt_bug join `%s`.xe_build join `%s`.xe_release join `%s`.xebt_status join `%s`.xebt_priority join `%s`.xebt_severity join `%s`.xetm_testcase join `%s`.xe_project join `%s`.xebt_bugsummary join `%s`.xe_step_execution_bug join `%s`.xe_module "
		+"on xebt_bug.bug_id = xebt_bugsummary.bug_id "
		+"and xe_build.build_id = xe_step_execution_bug.build_id "
		+"and xe_release.release_id = xe_build.release_id "
		+"and xebt_status.status_id = xebt_bugsummary.bug_status "
		+"and xebt_priority.priority_id = xebt_bugsummary.bug_priority "
		+"and xebt_severity.severity_id = xebt_bugsummary.bug_severity "
		+"and xetm_testcase.testcase_id = xe_step_execution_bug.test_case_id "
		+"and xe_project.project_id = xebt_bug.project "
		+"and xe_module.module_id = xebt_bug.module "
		+"where " 
		+"xebt_bug.project "+project+" " 
		+"and xebt_bug.module "+module+" " 
		+"and xebt_bugsummary.bug_status "+status+" " 
		+"and xebt_bugsummary.bug_priority "+priority+" " 
		+"and xebt_bugsummary.bug_severity "+severity+";";
	}

	public static Report getInstance() {
		if (instance == null) {
			instance = new Report();
		}
		return instance;
	}
	
	public String saveTCQuery = "INSERT INTO `%s`.xebm_customize_query (user_id ,query_text,query_name ,query_variables) values (?,?,?,?)";
	
	public String getTCQuery = "SELECT query_id,query_name,query_variables FROM `%s`.xebm_customize_query WHERE user_id = ? ORDER BY query_id DESC";
	
	public String updateTCQuery = "update `%s`.xebm_customize_query set query_text = ? , query_variables = ? WHERE query_id = ?";
	
	public String saveBugQuery = "INSERT INTO `%s`.xe_bug_customize_query (user_id ,query_text,query_name ,query_variables) values (?,?,?,?)";
	
	public String updateBugQuery = "update `%s`.xe_bug_customize_query set query_text = ? , query_variables = ? WHERE query_id = ?";
	
	public String getBugFilteredData() {
		return bugFilteredData;
	}

	public void setBugFilteredData(int userID,String build,String status,String priority,String severity) {
		
		this.bugFilteredData = "SELECT * FROM (SELECT summary.bug_status as btstatus,summary.bug_priority as btpriority,summary.bug_severity as btseverity,summary.bug_id,bug.bug_title,bug.bug_prefix,summary.update_date,bug.create_date,stat.bug_status as bugStatus, "
				+ " priority.bug_priority,severity.bug_severity,assignee.first_name as assigneeFN ,assignee.last_name as assigneeLN,reporter.first_name as reporterFN ,reporter.last_name as reporterLN , "
				+ " rel.release_name,build.build_name,testcase.testcase_name,'Manual' as buildExecutionType "
				+ " FROM `%s`.xe_step_execution_bug as stepexec , `%s`.xetm_testcase as testcase ,`%s`.xe_build as build , `%s`.xe_release as rel  ,`%s`.xebt_bugsummary summary, "
				+ " `%s`.xebt_bug bug ,`%s`.xe_module as xemodule,`%s`.xebt_status as stat,`%s`.xebt_priority as priority,`%s`.xebt_severity as severity,`%s`.xe_user_details as assignee , `%s`.xe_user_details as reporter "
				+ " where bug.project in (SELECT project_id FROM `%s`.xe_user_project where user = "+ userID +" ) and bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority and severity.severity_id= summary.bug_severity "
				+ " and assignee.user_id = summary.assignee and bug.created_by = reporter.user_id "
				+ " and bug.module = xemodule.module_id and xemodule.module_active = 1  "
				+ " and bug.module IN (select module_id from `%s`.xe_user_module where user_id= "+ userID +" ) "
				+ " and bug.bug_id = stepexec.bug_id "
				+ " and  summary.bs_id in (SELECT max(bSumm.bs_id) FROM `%s`.xebt_bugsummary as bSumm where bSumm.bug_id = bug.bug_id)"
				+ " and testcase.testcase_id = stepexec.test_case_id  "
				+ " and build.build_id = stepexec.build_id  "
				+ " and rel.release_id = build.release_id  "
				+ " and stepexec.build_id "+ build +" order by bs_id desc) as bugTable "
				+ " where bugTable.btstatus "+ status +" and bugTable.btpriority "+ priority+" and bugTable.btseverity "+ severity +" "
				+ " GROUP BY bug_id;";
		
		/*this.bugFilteredData="select xebt_bugsummary.bug_status, xebt_bugsummary.bug_priority, xebt_bugsummary.bug_severity, xebt_bugsummary.bug_id, xebt_bug.bug_title, " 
				+"xebt_bug.bug_prefix, xebt_bugsummary.update_date, xebt_bug.create_date, xebt_status.bug_status, xebt_priority.bug_priority, xebt_severity.bug_severity, " 
				+"xe_user_details.first_name as assignFN, xe_user_details.last_name as assignLN, xe_user_details.first_name as reporterFN, xe_user_details.last_name as reporterLN, " 
				+"xe_release.release_name, xe_build.build_name, xetm_testcase.testcase_name, 'Manual' as buildExecutionType " 
				+"from `%s`.xebt_bugsummary join `%s`.xebt_bug join `%s`.xebt_status join `%s`.xebt_priority join `%s`.xebt_severity join `%s`.xe_user_details join `%s`.xe_release join `%s`.xe_build join `%s`.xetm_testcase join `%s`.xe_module "
				+"where xebt_bugsummary.bug_id = xebt_bug.bug_id "
				+"and xebt_bugsummary.bug_status = xebt_status.status_id "
				+"and xebt_bugsummary.bug_priority = xebt_priority.priority_id "
				+"and xebt_bugsummary.bug_severity = xebt_severity.severity_id "
				+"and xebt_bugsummary.assignee = xe_user_details.user_id "
				+"and xe_release.release_id = xe_build.release_id "
				+"and xe_module.module_id = xetm_testcase.module_id " 
				+"and xebt_bug.module= xe_module.module_id "
				+"and xe_build.build_id "+build+" "
				+"and xebt_status.status_id "+status+" " 
				+"and xebt_priority.priority_id "+priority+" " 
				+"and xebt_severity.severity_id "+severity+" "
				+"and xe_user_details.user_id = "+userID+" "
				+"GROUP BY bug_id;"; */
				
	}
	
	public void setBugFilteredData(int userID, Map<String, String> whereClauseFilters) {
			
			StringBuilder stringBuilder = new StringBuilder( "SELECT * FROM (SELECT summary.bug_status as btstatus,summary.bug_priority as btpriority,summary.bug_severity as btseverity,summary.bug_id,bug.bug_title,bug.bug_prefix,summary.update_date,bug.create_date,stat.bug_status as bugStatus, "
					+ " priority.bug_priority,severity.bug_severity,assignee.first_name as assigneeFN ,assignee.last_name as assigneeLN,reporter.first_name as reporterFN ,reporter.last_name as reporterLN , "
					+ " rel.release_name,build.build_name,testcase.testcase_name,'Manual' as buildExecutionType "
					+ " FROM `%s`.xe_step_execution_bug as stepexec , `%s`.xetm_testcase as testcase ,`%s`.xe_build as build , `%s`.xe_release as rel  ,`%s`.xebt_bugsummary summary, "
					+ " `%s`.xebt_bug bug ,`%s`.xe_module as xemodule,`%s`.xebt_status as stat,`%s`.xebt_priority as priority,`%s`.xebt_severity as severity,`%s`.xe_user_details as assignee , `%s`.xe_user_details as reporter "
					+ " where bug.project in (SELECT project_id FROM `%s`.xe_user_project where user = "+ userID +" ) and bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority and severity.severity_id= summary.bug_severity "
					+ " and assignee.user_id = summary.assignee and bug.created_by = reporter.user_id "
					+ " and bug.module = xemodule.module_id and xemodule.module_active = 1  "
					+ " and bug.module IN (select module_id from `%s`.xe_user_module where user_id= "+ userID +" ) "
					+ " and bug.bug_id = stepexec.bug_id "
					+ " and  summary.bs_id in (SELECT max(bSumm.bs_id) FROM `%s`.xebt_bugsummary as bSumm where bSumm.bug_id = bug.bug_id)"
					+ " and testcase.testcase_id = stepexec.test_case_id  "
					+ " and build.build_id = stepexec.build_id  "
					+ " and rel.release_id = build.release_id "
//					+ " and rel.release_active = 1 and build.build_status = 1 "
					+ ( whereClauseFilters.containsKey("release") ? " and rel.release_id "+ whereClauseFilters.get("release") : "" )
					+ ( whereClauseFilters.containsKey("build") ? " and stepexec.build_id "+ whereClauseFilters.get("build") : "" ) + " order by bs_id desc) as bugTable ");
//					+ " where bugTable.btstatus "+ status +" and bugTable.btpriority "+ priority+" and bugTable.btseverity "+ severity +" "
//					+ " GROUP BY bug_id;";
			whereClauseFilters.remove("build");
			whereClauseFilters.remove("release");
			int mapSize = whereClauseFilters.size();
			int filterCount = 1;
			if( mapSize > 0 )
				stringBuilder.append(" where ");
			for( Map.Entry<String, String> entry : whereClauseFilters.entrySet() ) {
				switch ( entry.getKey() ) {
				case "release":
					
					break;
				case "build":
					// added in on of the subquery where clause
					break;
				case "status":
					stringBuilder.append(" bugTable.btstatus "+ whereClauseFilters.get("status") );
					break;
				case "priority":
					stringBuilder.append(" bugTable.btpriority "+ whereClauseFilters.get("priority") );
					break;
				case "severity":
					stringBuilder.append(" bugTable.btseverity "+ whereClauseFilters.get("severity") );
					break;
/*				default:
					break;*/
				}
				if( filterCount < mapSize )
					stringBuilder.append(" and ");
				++filterCount;
			}
			stringBuilder.append(" GROUP BY bug_id;");
			this.bugFilteredData = stringBuilder.toString();
	}

	public String getAutomationBugFilteredData() {
		return automationBugFilteredData;
	}

	public void setAutomationBugFilteredData(int userID,String build,String status,String priority,String severity) {
		this.automationBugFilteredData = "SELECT * FROM (SELECT summary.bug_status as btstatus,summary.bug_priority as btpriority,summary.bug_severity as btseverity,summary.bug_id,bug.bug_title,bug.bug_prefix,summary.update_date,bug.create_date,stat.bug_status as bugStatus, "
				+ " priority.bug_priority,severity.bug_severity,assignee.first_name as assigneeFN ,assignee.last_name as assigneeLN,reporter.first_name as reporterFN ,reporter.last_name as reporterLN , "
				+ " rel.release_name,build.build_name,testcase.testcase_name,'Automation' as buildExecutionType "
				+ " FROM `%s`.xe_automation_step_execution_bug as stepexec , `%s`.xetm_testcase as testcase ,`%s`.xe_automation_build as build , `%s`.xe_release as rel  ,`%s`.xebt_bugsummary summary, "
				+ " `%s`.xebt_bug bug ,`%s`.xe_module as xemodule,`%s`.xebt_status as stat,`%s`.xebt_priority as priority,`%s`.xebt_severity as severity,`%s`.xe_user_details as assignee , `%s`.xe_user_details as reporter "
				+ " where bug.project in (SELECT project_id FROM `%s`.xe_user_project where user = "+ userID +" ) and bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority and severity.severity_id= summary.bug_severity "
				+ " and assignee.user_id = summary.assignee and bug.created_by = reporter.user_id "
				+ " and bug.module = xemodule.module_id and xemodule.module_active = 1  "
				+ " and bug.module IN (select module_id from `%s`.xe_user_module where user_id= "+ userID +" ) "
				+ " and bug.bug_id = stepexec.bug_id "
				+ " and  summary.bs_id in (SELECT max(bSumm.bs_id) FROM `%s`.xebt_bugsummary as bSumm where bSumm.bug_id = bug.bug_id)"
				+ " and testcase.testcase_id = stepexec.test_case_id  "
				+ " and build.build_id = stepexec.build_id  "
				+ " and rel.release_id = build.release_id  "
				+ " and stepexec.build_id "+ build +" order by bs_id desc) as bugTable "
				+ " where bugTable.btstatus "+ status +" and bugTable.btpriority "+ priority +" and bugTable.btseverity "+ severity +" "
				+ " GROUP BY bug_id ";
	}
	
	public void setAutomationBugFilteredData(int userID, Map<String, String> whereClauseFilters) {
		StringBuilder stringBuilder = new StringBuilder( "SELECT * FROM (SELECT summary.bug_status as btstatus,summary.bug_priority as btpriority,summary.bug_severity as btseverity,summary.bug_id,bug.bug_title,bug.bug_prefix,summary.update_date,bug.create_date,stat.bug_status as bugStatus, "
				+ " priority.bug_priority,severity.bug_severity,assignee.first_name as assigneeFN ,assignee.last_name as assigneeLN,reporter.first_name as reporterFN ,reporter.last_name as reporterLN , "
				+ " rel.release_name,build.build_name,testcase.testcase_name,'Automation' as buildExecutionType "
				+ " FROM `%s`.xe_automation_step_execution_bug as stepexec , `%s`.xetm_testcase as testcase ,`%s`.xe_automation_build as build , `%s`.xe_release as rel  ,`%s`.xebt_bugsummary summary, "
				+ " `%s`.xebt_bug bug ,`%s`.xe_module as xemodule,`%s`.xebt_status as stat,`%s`.xebt_priority as priority,`%s`.xebt_severity as severity,`%s`.xe_user_details as assignee , `%s`.xe_user_details as reporter "
				+ " where bug.project in (SELECT project_id FROM `%s`.xe_user_project where user = "+ userID +" ) and bug.bug_id = summary.bug_id and summary.bug_status = stat.status_id and priority.priority_id= summary.bug_priority and severity.severity_id= summary.bug_severity "
				+ " and assignee.user_id = summary.assignee and bug.created_by = reporter.user_id "
				+ " and bug.module = xemodule.module_id and xemodule.module_active = 1  "
				+ " and bug.module IN (select module_id from `%s`.xe_user_module where user_id= "+ userID +" ) "
				+ " and bug.bug_id = stepexec.bug_id "
				+ " and  summary.bs_id in (SELECT max(bSumm.bs_id) FROM `%s`.xebt_bugsummary as bSumm where bSumm.bug_id = bug.bug_id)"
				+ " and testcase.testcase_id = stepexec.test_case_id  "
				+ " and build.build_id = stepexec.build_id  "
				+ " and rel.release_id = build.release_id "
//				+ " and rel.release_active = 1 and build.build_status = 1 "
				+ ( whereClauseFilters.containsKey("release") ? " and rel.release_id "+ whereClauseFilters.get("release") : "" )
//				+ " and stepexec.build_id "+ build +" order by bs_id desc) as bugTable ");
				+ ( whereClauseFilters.containsKey("build") ? " and stepexec.build_id "+ whereClauseFilters.get("build") : "" ) + " order by bs_id desc) as bugTable ");
//				+ " where bugTable.btstatus "+ status +" and bugTable.btpriority "+ priority +" and bugTable.btseverity "+ severity +" "
//				+ " GROUP BY bug_id ";
		whereClauseFilters.remove("build");
		whereClauseFilters.remove("release");
		int mapSize = whereClauseFilters.size();
		int filterCount = 1;
		if( mapSize > 0 )
			stringBuilder.append(" where ");
		for( Map.Entry<String, String> entry : whereClauseFilters.entrySet() ) {
			System.out.println( entry.getKey()+" : "+entry.getValue());
			switch ( entry.getKey() ) {
			case "release":
				// added in on of the subquery where clause
				break;
			case "build":
				// added in on of the subquery where clause
				break;
			case "status":
				stringBuilder.append(" bugTable.btstatus "+ whereClauseFilters.get("status") );
				break;
			case "priority":
				stringBuilder.append(" bugTable.btpriority "+ whereClauseFilters.get("priority") );
				break;
			case "severity":
				stringBuilder.append(" bugTable.btseverity "+ whereClauseFilters.get("severity") );
				break;
/*				default:
				break;*/
			}
			if( filterCount < mapSize )
				stringBuilder.append(" and ");
			++filterCount;
		}
		stringBuilder.append(" GROUP BY bug_id;");
		this.automationBugFilteredData = stringBuilder.toString();
	}
	public String getAllbugs="select * from `%s`.xebt_bug";
	
	public String getDatasetData="SELECT * FROM `%s`.xetm_tc_dataset xtd,`%s`.xetm_exec_status xes WHERE xes.exst_id=xtd.status AND xtd.tc_id=?";
	
	public String getAllExtSysReleases = "select * from `%s`.xebm_ext_sys_release";
	public String getAllExtSysBuilds = "select * from `%s`.xebm_ext_sys_build";
	public String getAllExtSysTestcases = "select *,ES.description from `%s`.xebm_ext_sys_testcasesynch TS INNER JOIN `%s`.xetm_exec_status ES on TS.status=ES.exst_id";
	public String getAllExtSysTeststeps = "select *,ES.description from `%s`.xebm_ext_sys_teststepsynch TS INNER JOIN `%s`.xetm_exec_status ES on TS.status=ES.exst_id";
	
	public String getAllAutomationBuilds="select * from `%s`.xe_automation_build where build_status=1;";
    public String getAllManualBuilds="select * from `%s`.xe_build where build_status=1;";
	
	public String getExtSysTCCount = "SELECT COUNT(tc.test_case_id) AS tcCount, r.release_id FROM ((`%s`.xebm_ext_sys_testcasesynch tc INNER JOIN `%s`.xebm_ext_sys_build b ON b.build_id = tc.build_id) INNER JOIN `%s`.xebm_ext_sys_release r ON r.release_id = b.release_id) GROUP BY r.release_id ORDER BY r.release_id";
	 
	public String getReleaseTCCount() {
		return releaseTCCount;
	}
	public void setReleaseTCCount(String release_id){
		this.releaseTCCount = "SELECT COUNT(*) AS tcCount, ES.description as status FROM `%s`.xebm_ext_sys_testcasesynch TS INNER JOIN `%s`.xetm_exec_status ES on TS.status=ES.exst_id WHERE build_id IN (SELECT build_id FROM `%s`.xebm_ext_sys_build where release_id = " + release_id + " ) GROUP BY status ORDER BY status";
	}
	
	public String getBuildTCCount() {
		return buildTCCount;
	}
	public void setBuildTCCount(String build_id){
		this.buildTCCount = "SELECT COUNT(*) AS tcCount, ES.description as status FROM `%s`.xebm_ext_sys_testcasesynch TS INNER JOIN `%s`.xetm_exec_status ES on TS.status=ES.exst_id WHERE TS.build_id = " + build_id + " GROUP BY status ORDER BY TS.status";
	}
	
	public String QueryforBugTracker = "INSERT INTO `%s`.xe_bug_customize_query (user_id, query_text ,query_name) values (?,?,?)";
	
	public String QueryforTEMTracker = "INSERT INTO `%s`.xebm_customize_query (user_id, query_text ,query_name) values (?,?,?)";
	
	public String QueryforTEATracker = "INSERT INTO `%s`.xebm_customize_query_automation (user_id, query_text ,query_name) values (?,?,?)";
	
	public String getQueryforBugTracker ="select * from `%s`.xe_bug_customize_query;";
	
	public String getQueryforTEMTracker ="select * from `%s`.xebm_customize_query;";
	
	public String getQueryforTEATracker ="select * from `%s`.xebm_customize_query_automation;";
	
	/*public String ExecutionManualQueryFilter = "SELECT xtc.testcase_id, xtc.testcase_name, xtc.test_prefix, xp.project_id, xp.project_name, xr.release_id, xr.release_name, "
			+ "xb.build_id, xb.build_name, xm.module_id, xm.module_name, xs.scenario_id, xs.scenario_name, "
			+ "xud.user_id AS tester_id, CONCAT(xud.first_name, ' ', xud.last_name) AS tester_name, xes.exst_id, xes.description "
			+ "FROM `%s`.xe_testcase_execution_summary xtes "
			+ "JOIN `%s`.xetm_testcase xtc ON xtc.testcase_id = xtes.tc_id "
			+ "JOIN `%s`.xe_project xp ON xp.project_id = xtes.project_id "
			+ "JOIN `%s`.xe_build xb ON xb.build_id = xtes.build_id "
			+ "JOIN `%s`.xe_release xr ON xr.release_id = xb.release_id "
			+ "JOIN `%s`.xe_module xm ON xm.module_id = xtes.mudule_id "
			+ "JOIN `%s`.xetm_scenario xs ON xs.scenario_id = xtes.scenario_id "
			+ "JOIN `%s`.xe_user_details xud ON xud.user_id = xtes.tester_id "
			+ "JOIN `%s`.xetm_exec_status xes ON xes.exst_id = xtes.status_id where xp.project_id=?;";*/
	
	
	public String ExecutionAytomationQueryFilter = "SELECT xtc.testcase_id, xtc.testcase_name, xtc.test_prefix, xab.build_id, xab.build_name, xp.project_id, xp.project_name, "
			+ "xr.release_id, xr.release_name, "
			+ "xm.module_id, xm.module_name, xs.scenario_id, xs.scenario_name, xes.exst_id, xes.description "
			+ "FROM `%s`.xebm_automation_execution_summary xaes "
			+ "JOIN `%s`.xetm_testcase xtc ON xtc.testcase_id = xaes.testcase "
			+ "JOIN `%s`.xe_project xp ON xp.project_id = xaes.project "
			+ "JOIN `%s`.xe_automation_build xab ON xab.build_id = xaes.build "
			+ "JOIN `%s`.xe_release xr ON xr.release_id = xab.release_id "
			+ "JOIN `%s`.xe_module xm ON xm.module_id = xaes.module "
			+ "JOIN `%s`.xetm_scenario xs ON xs.scenario_id = xaes.scenario "
			+ "JOIN `%s`.xetm_exec_status xes ON xes.exst_id = xaes.testcase_status ";

}

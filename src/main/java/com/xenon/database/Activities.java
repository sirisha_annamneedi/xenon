package com.xenon.database;

public class Activities {
	private static Activities instance = null;

	protected Activities() {

	}

	public static Activities getInstance() {
		if (instance == null) {
			instance = new Activities();
		}
		return instance;
	}

	public String inserNewActivity="INSERT INTO `%s`.xe_recent_activities (activity, activity_desc,module,activity_date,user,activity_status) VALUES (?,?,?,NOW(),?,?);";
	
	public String getAllActivities = "SELECT a.activity_id,a.activity,a.activity_desc,a.module,DATE_FORMAT(a.activity_date , '%%d-%%m-%%Y %%h:%%i:%%s %%p') as activity_date,a.activity_date as actDate,TIMESTAMPDIFF(MINUTE,a.activity_date,CURRENT_TIME()) AS TimeDiff,u.first_name,u.last_name,a.user,u.user_photo,act.activity_desc as activityState,act.activity_format FROM `%s`.xe_recent_activities a INNER JOIN `%s`.xe_user_details u ON u.user_id=a.user  INNER JOIN %s.xe_activities act ON act.xetm_activity_id=a.activity and activity_status=1 ORDER BY a.activity_id DESC LIMIT 5";
} 

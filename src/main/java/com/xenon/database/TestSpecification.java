package com.xenon.database;

public class TestSpecification {

	private static TestSpecification instance = null;

	protected TestSpecification() {

	}

	public static TestSpecification getInstance() {
		if (instance == null) {
			instance = new TestSpecification();
		}
		return instance;
	}

	public String projectDetails = "SELECT * FROM `%s`.xe_project where project_id=?";
	public String moduleDetails = "SELECT * FROM `%s`.xe_module WHERE module_id IN (SELECT module_id FROM `%s`.xe_user_module where project_id=? and user_id = ?) and module_active = 1";
	public String scenarioDetails = "SELECT * FROM `%s`.xetm_scenario where module_id=? and status = 1";
	public String testCaseDetails = "SELECT distinct xt.testcase_id,xt.testcase_name,xt.summary,xt.precondition,xt.execution_type,xet.description,xt.execution_time,xts.tc_status_id,xts.status,xt.test_prefix,CONCAT(user.first_name,' ',user.last_name) authorFullName, user.about_me authorAboutMe, user.user_photo FROM `%s`.xetm_testcase xt,`%s`.xetm_execution_type xet,`%s`.xetm_testcase_status xts,`%s`.xe_user_details as user where xt.execution_type = xet.execution_type_id and xt.status = xts.tc_status_id and user.user_id = xt.author and xet.status = 1 and scenario_id=? and projectid=?";
	public String testStepDetails = "SELECT * FROM `%s`.xetm_testcase_steps where testcase_id=?";

	public String deleteExistingTestcaseDatasheet = "delete from `%s`.xetm_testcase_datasheet where testcase_id= ?";
	public String uploadTestcaseDatasheet = "insert into `%s`.xetm_testcase_datasheet(testcase_id,user_id,datasheet_file,datasheet_filename)"
			+ " values(?,?,?,?)";
	
	public String getDatasheetByTcId="SELECT * FROM `%s`.xetm_testcase_datasheet where testcase_id=?;";
	
	public String getDatasheetByBuildId="SELECT * FROM `%s`.xebm_build_datafile where build_id=?;";
	
	public String filterQuery = "INSERT INTO `%s`.xebt_customizesearch(queryname,query,user_id)"
			+ " values(?,?,?)";
}

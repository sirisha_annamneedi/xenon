package com.xenon.database;

public class Module {

	private static Module instance = null;

	protected Module() {

	}

	public static Module getInstance() {
		if (instance == null) {
			instance = new Module();
		}
		return instance;
	}

	public String getActiveProjects = "select * from `%s`.xe_project where project_active = 1 and project_id IN(SELECT project_id FROM `%s`.xe_user_project WHERE user = ?)";
	public String insertModule = "INSERT INTO `%s`.xe_module (module_name,project_id,module_description,module_active)"
			+ "VALUES(?,?,?,?)";
	public String getModuleDetails = "select * from `%s`.xe_module where module_id =?";
	public String getModuleId = "SELECT module_id from `%s`.xe_module where module_name =? and module_description=? and project_id=? and module_active =?";
	public String updateModule = "UPDATE `%s`.xe_module  SET `module_name`=?, `module_description`=?, `module_active`=? WHERE `module_id`=?";
	public String updateModuleStatus = "UPDATE `%s`.xe_module  SET `module_active`=? WHERE `module_id`=?";
	public String updateScenarioStatusByModuleId = "UPDATE `%s`.xetm_scenario  SET `status`=? WHERE `module_id`=?";

	public String getAllModulesById = "SELECT module_id,module_name,module.project_id,module_description,module_active,pro.project_name"
			+ " FROM `%s`.xe_module as module"
			+ " INNER JOIN `%s`.xe_project pro on pro.project_id = module.project_id"
			+ " WHERE module.project_id in(SELECT up.project_id FROM `%s`.xe_user_project as up where  user = ?)";
	
	public String getAllModules = "SELECT * from `%s`.xe_module";
	
	public String moduleAssignedUserDetails = "SELECT userPro.user as user_id,user.first_name,user.last_name,user.email_id,user.user_photo"
			+ " FROM `%s`.xe_user_project as userPro"
			+ " inner join `%s`.xe_user_details as user on user.user_id = userPro.user"
			+ " WHERE userPro.project_id = ?"
			+ " and  userPro.user in (SELECT module.user_id FROM `%s`.xe_user_module as module WHERE module_id = ?);";
	
	public String moduleUnAssignedUserDetails = "SELECT userPro.user as user_id,user.first_name,user.last_name,user.email_id,user.user_photo"
			+ " FROM `%s`.xe_user_project as userPro"
			+ " inner join `%s`.xe_user_details as user on user.user_id = userPro.user"
			+ " WHERE userPro.project_id = ?"
			+ " and  userPro.user not in (SELECT module.user_id FROM `%s`.xe_user_module as module WHERE module_id = ?);";
	
	public String deleteAssignedModuleRecords = "DELETE FROM `%s`.xe_user_module "
			+ " WHERE user_id=? and module_id = ?";
	
	public String insertAssignedModuleRecords ="INSERT INTO `%s`.xe_user_module(module_id,user_id,project_id) values(?,?,?)";
	
	public String createNewModuleStatus="{?= CALL %s.createNewModuleStatus(?)}";
	
	public String getModuleDetailsById = "select * from `%s`.xe_module where module_id =?";
	
	
	public String selectModuleDetailsByBuildId ="SELECT * FROM `%s`.xe_module where module_id IN (SELECT module_id FROM `%s`.xetm_scenario where scenario_id IN (SELECT scenario_id FROM `%s`.xetm_testcase where testcase_id IN (SELECT testcase_id FROM `%s`.xebm_buildtestcase where build_id=?))) and module_id IN "
			+ "(SELECT module_id FROM `%s`.xebm_assign_build where user_id =?);";
	
	public String createNewModule="{?= CALL %s.createNewModule(?,?,?,?)}";
	
	
	public String getAllModulesWithTcCounts = "SELECT module_id,module_name,module.project_id,module_description,module_active,pro.project_name"
			+ " FROM `%s`.xe_module as module"
			+ " INNER JOIN `%s`.xe_project pro on pro.project_id = module.project_id"
			+ " WHERE module.project_id in(SELECT up.project_id FROM `%s`.xe_user_project as up where  user = ?) and module.module_id IN (SELECT module_id FROM `%s`.xe_user_module where  user_id = ?) and module.module_active =1";
	
	
	public String removeUnassignedCurrentProjectModule= "DELETE FROM `%s`.xe_user_module WHERE project_id=? and user_id NOT IN ( SELECT user FROM `%s`.xe_user_project where project_id=?)";
	
	public String moduleUserDetails = "SELECT uMod.user_id ,CONCAT(user.first_name,' ',user.last_name) as userName"
			+ " FROM `%s`.xe_user_module as uMod "
			+ " LEFT JOIN `%s`.xe_user_details as user on user.user_id = uMod.user_id "
			+ " WHERE uMod.module_id = ? and user.user_status = 1 and user.bt_status=1";
	
	public String moduleBybugId="SELECT module FROM `%s`.xebt_bug WHERE bug_id=?";
	public String removeAlreadyAssignedModules= "DELETE FROM `%s`.xe_user_module WHERE project_id=? and user_id=?";
	
	public String insertAssignedModules ="INSERT INTO `%s`.xe_user_module(module_id,user_id,project_id) values(?,?,?)";
	
	public String getModulesByProjectAndUserId = "SELECT module_id,module_name,module.project_id,module_description,module_active,pro.project_name"
			+ " FROM `%s`.xe_module as module"
			+ " INNER JOIN `%s`.xe_project pro on pro.project_id = module.project_id"
			+ " WHERE module.project_id in(SELECT up.project_id FROM `%s`.xe_user_project as up where  user = ?) and module.project_id=?";
	
	
	public String getModuleDetailsByName = "SELECT * from `%s`.xe_module where module_name=?";
	
	public String getModuleDetailsByProject = "SELECT * from `%s`.xe_module where project_id=?";
	
	public String deleteModule="delete from `%s`.xe_module where module_id=?";
	
}
package com.xenon.database;

public class DocumentLibrary {
	private static DocumentLibrary instance = null;

	protected DocumentLibrary() {

	}

	public static DocumentLibrary getInstance() {
		if (instance == null) {
			instance = new DocumentLibrary();
		}
		return instance;
	}
	
	public String uploadDocument = "insert into `%s`.xe_doc_library (doc_title, doc_type, doc_data,upload_time,uploaded_by,project_id) values (?,?,?,NOW(),?,?)";
	public String getDocumentDetails="SELECT xd.xe_doc_library_id,xd.doc_title,xd.doc_type,DATE_FORMAT(xd.upload_time , '%%d-%%m-%%Y %%h:%%i %%p') as upload_time,CONCAT(xu.first_name, ' ',xu.last_name) uploaded_by,xu.user_photo ,xd.project_id FROM `%s`.xe_doc_library xd,`%s`.xe_user_details xu where xd.project_id=? and xd.uploaded_by = xu.user_id";
	public String getDocumentById = "SELECT * FROM `%s`.xe_doc_library where xe_doc_library_id = ?";
}

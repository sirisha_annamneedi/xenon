package com.xenon.database;

public class Mail {
	private static Mail instance = null;

	protected Mail() {

	}

	public static Mail getInstance() {
		if (instance == null) {
			instance = new Mail();
		}
		return instance;
	}
	
	public String insertSMTPDetails = "INSERT INTO `%s`.xe_email_details(smtp_host,smtp_port,smtp_user_id,smtp_password,is_enabled) "
			+ " values(?,?,?,?,?)"; 
	
	public String allSMTPDetails = "SELECT * FROM `%s`.xe_email_details";
	
	public String updateSmtp = "UPDATE `%s`.xe_email_details SET smtp_host = ? , "
			+ " smtp_port = ? , smtp_user_id= ? , smtp_password = ? , is_enabled=?"
			+ " wHERE id = ?";
	
	public String xenonSMTPDetails = "SELECT * FROM `xenon`.xe_email_details";
	
	public String getMailNotificationDetails = "SELECT * FROM `%s`.xe_mail_notifications";
	
	
	public String updateMailNotificationDetailsBm = "UPDATE `%s`.xe_mail_notifications SET create_user = ? , "
			+ " assign_project = ? , assign_module= ? , assign_build = ? , build_exec_complete=? "
			+ " WHERE mail_notification_id = ?";
	
	public String updateMailNotificationDetailsBt = "UPDATE `%s`.xe_mail_notifications SET create_bug = ? , "
			+ " update_bug = ?  "
			+ " WHERE mail_notification_id = ?";
	public String insertMailGroupRecords= "INSERT INTO  `%s`.xe_mail_group_details (user_id ,mail_group_id) "
			+ " values(?,?)";
	public String createNewMailGroup="{?= CALL %s.createNewMailGroup(?)}";
	
	public String updateMailGroups = "Delete FROM `%s`.xe_mail_group_details where mail_group_id = ?";
	
	public String updateMailGroup="{?= CALL %s.updateMailGroup(?)}";
	public String  updateMailGroupDetails  = "UPDATE `%s`.xe_mail_group SET mail_group_name = ? where mail_group_id = ?";
	
	public String getMailGroupUsers = "select email_id from `%s`.xe_user_details where user_id IN(select user_id from `%s`.xe_mail_group_details where mail_group_id = ?)";
	
	
	
}

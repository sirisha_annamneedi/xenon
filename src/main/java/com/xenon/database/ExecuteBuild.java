package com.xenon.database;

public class ExecuteBuild {
	private static ExecuteBuild instance = null;

	protected ExecuteBuild() {

	}

	public static ExecuteBuild getInstance() {
		if (instance == null) {
			instance = new ExecuteBuild();
		}
		return instance;
	}
	
	public String getExecuteBuildDetails = "SELECT exeBuild.exe_id,exeBuild.build,build.build_name,exeBuild.project,project.project_name,exeBuild.module,module.module_name,exeBuild.scenario,scenario.scenario_name,exeBuild.testcase,testcase.testcase_name,exeBuild.step_id,exeBuild.step_details,exeBuild.step_input_type,exeBuild.step_value,exeBuild.step_status,exeBuild.screenshot,exeBuild.screenshot_title "
			+ " FROM `%s`.xebm_trial_execution_summary as exeBuild"
			+ " INNER JOIN `%s`.xebm_trial_build as build ON build.build_id = exeBuild.build"
			+ " INNER JOIN `%s`.xe_project as project on project.project_id = exeBuild.project"
			+ " INNER JOIN `%s`.xe_module as module on module.module_id = exeBuild.module"
			+ " INNER JOIN `%s`.xetm_scenario as scenario on scenario.scenario_id = exeBuild.scenario"
			+ " INNER JOIN `%s`.xetm_testcase as testcase on testcase.testcase_id = exeBuild.testcase"
			+ " WHERE exeBuild.build= ?";
	public String screenshotById = "SELECT trial_step_id,screenshot FROM `%s`.xebm_automation_steps_execution_summary where trial_step_id = ?";
	
	public String autoTestCases="SELECT * FROM `%s`.xebm_autoexe";
	
}

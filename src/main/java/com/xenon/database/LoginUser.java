package com.xenon.database;

public class LoginUser {

	private static LoginUser instance = null;

	protected LoginUser() {

	}

	public static LoginUser getInstance() {
		if (instance == null) {
			instance = new LoginUser();
		}
		return instance;
	}
	public String validateUser = "SELECT * FROM `%s`.xe_user_details as ud ,`%s`.xe_role as ro where ud.user_password =SHA1(?) and ud.email_id=? and ro.role_id = ud.role_id and ud.user_status =1 ;";

	public String validateCustomerStatus = "SELECT issue_tracker,issue_tracker_type,issue_tracker_host,redwood_status,api_status,redwood_url,customer_schema,scriptless_status ,cust.on_premise,cust.ldap_status,cust.customer_name,ct.customer_type,cust.customer_id,cust.customer_logo,cust.automation_status as automation_status,"
			+ " ct.id as cust_type_id,cust.start_date,cust.end_date,cust.customer_id,licen.max_cloud_space , cust.jenkin_status, cust.git_status"
			+ " FROM `xenon-core`.xe_user as us , `xenon-core`.xe_customer_details as cust,`xenon-core`.xe_customer_type as ct,`xenon-core`.xe_licence_details as licen "
			+ " where us.customer_id = cust.customer_id and cust.customer_type =ct.id  and us.user_name= ? "
			+ " and licen.type_id = cust.customer_type and cust.customer_status=1";
	
	public String getCustomerUsedCloudSpace = " SELECT table_schema as cust_schema_name, "
			+ " round((sum(data_length + index_length) / 1024 / 1024), 2) 'Used_Space' "
			+ " FROM information_schema.TABLES WHERE table_schema=? GROUP BY table_schema ";
	
	
	public String getUserImageById="SELECT user_photo FROM `%s`.xe_user_details where user_id=?;";
}

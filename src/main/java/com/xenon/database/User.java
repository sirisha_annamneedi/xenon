package com.xenon.database;

public class User {

	@SuppressWarnings("unused")
	private static User instance = null;

	public String insertUser = "insert into `%s`.xe_user_details (user_password, email_id, first_name, last_name, role_id, tm_status, bt_status, user_status,user_photo) "
			+ "values (SHA1(?),?,?,?,?,?,?,?,?)";
	public String insertUserIntoCore = "insert into `xenon-core`.xe_user(user_name,user_email,customer_id)" + "values (?,?,?)";
	public String getUserRole = "select * from `%s`.xe_role where role_status = 1";
	public String getUserId = "select user_id from `%s`.xe_user_details where first_name=? and last_name=? and email_id=? and bt_status =? and tm_status=? and user_status =? and role_id =?";
	public String getUserDetails = "select * from `%s`.xe_user_details where user_id =?";
	public String checkUser = "select user_id from `xenon-core`.xe_user where user_name =?";
	public String updateUser = "UPDATE `%s`.xe_user_details  SET `first_name`=?, `last_name`=?,`role_id`=?, `tm_status`=?, `bt_status`=?, `user_status`=? WHERE `user_id`=?";
	public String updateProfile = "UPDATE `%s`.xe_user_details  SET `first_name`=?, `last_name`=? ,`location`=? , `about_me`= ? , `user_timezone`= ?  WHERE `user_id`=?";
	public String removeUser = "UPDATE `%s`.xe_user_details  SET `user_status`=3 WHERE `user_id`=?";
	public String getAllUserDetails = "SELECT user_id,first_name,last_name,email_id,role.role_type,userDetails.role_id as roleID ,user_name ,"
			+ " active.status userStatus,tmStatus.status tmStatus," + " btStatus.status btStatus,user_photo,about_me "
			+ " FROM `%s`.xe_user_details userDetails "
			+ " INNER JOIN `%s`.xe_role role ON role.role_id=userDetails.role_id"
			+ " INNER join `%s`.xe_active active ON active.active_id = userDetails.user_status"
			+ " INNER JOIN `%s`.xe_active tmStatus ON tmStatus.active_id=userDetails.tm_status"
			+ " INNER JOIN `%s`.xe_active btStatus ON btStatus.active_id=userDetails.bt_status Where userDetails.user_status<>3";

	public String updatePassword = "UPDATE `%s`.xe_user_details  SET `user_password`=SHA1(?) , pass_flag = 1 WHERE `user_id`=?";

	public String getUserRoleById = "select * from `%s`.xe_role where role_status = 1 and role_id=?";

	public String getAdminUserDetails = "select * from `%s`.xe_user_details where role_id =?";
	public String updateUserWithProfilePhoto = "UPDATE `%s`.xe_user_details  SET `first_name`=?, `last_name`=?, `email_id`=?,`role_id`=?, `tm_status`=?, `bt_status`=?, `user_status`=?,`user_photo`=LOAD_FILE('?') WHERE `user_id`=?";

	public String updateUserProfileWithProfilePhoto = "UPDATE `%s`.xe_user_details  SET `first_name`=?, `last_name`=?,`user_photo`=LOAD_FILE(?),`location`=? , `about_me`= ?  WHERE `user_id`=?";

	public String getUserDetailsByEmailId = "SELECT * FROM %s.xe_user_details where email_id=?";

	public String createNewUserStatus = "{?= CALL %s.createNewUserStatus(?)}";

	public String getUserDetailsById = "select * from `%s`.xe_user_details where user_id =?";

	public String getUpdatePasswordUserDetails = "select * from `%s`.xe_user_details where user_id =?";

	public String updateAllUsersStatus = " update `%s`.xe_user_details set tm_status = ?,  bt_status = ?"
			+ " where user_status in(1,2)";

	public String userAssignedProjectDetails = "SELECT uPro.project_id,pro.project_name,pro.project_active ,uMod.module_id,uMod.user_id ,module.module_name,module.module_active"
			+ " FROM `%s`.xe_user_project as uPro "
			+ " LEFT JOIN `%s`.xe_user_module as uMod ON uMod.project_id = uPro.project_id"
			+ " LEFT JOIN `%s`.xe_project as pro ON pro.project_id = uPro.project_id"
			+ " LEFT JOIN `%s`.xe_module as module ON module.module_id = uMod.module_id"
			+ " WHERE pro.project_id = uPro.project_id  and uPro.user = ?";
	
	public String getUsersDetailsByMailGroupId = "select * from `%s`.xe_user_details where user_id IN (SELECT user_id FROM `%s`.xe_mail_group_details where mail_group_id=?)";
	public String updateProfile1 = "UPDATE `%s`.xe_user_details  SET `first_name`=?, `last_name`=?, `update_date`=? WHERE `user_id`=?";
	
	public String updateIssueTracker = "UPDATE `%s`.xe_user_details  SET `issue_tracker`=?, `issue_tracker_type`=?, `issue_tracker_host`=? WHERE `user_id`=?";
	
	public String getUserEmailId = "select email_id from `%s`.xe_user_details where user_id =?";
	
	public String getUserDetailsByEmailIDs = "SELECT user_id,email_id FROM `%s`.xe_user_details ";
	
	public String getUserIDByEmailIDs = "SELECT user_id FROM `%s`.xe_user_details where email_id = ?";
	
	public String isEmailSettingEnabled = "SELECT is_enabled FROM `%s`.xe_email_details";
	
	public String updatePasswordByUserId = "UPDATE `%s`.xe_user_details  SET `user_password`=SHA1(?) WHERE `user_id`=?";
	
	public String checkUserName = "select user_id from `xenon-core`.xe_user where user_name =?";
	
	public String getAllLdapInactiveUserDetails = "SELECT user_id,first_name,last_name,email_id,role.role_type,userDetails.role_id as roleID ,user_name ,ldap_status,"
			+ " active.status userStatus,tmStatus.status tmStatus," + " btStatus.status btStatus,user_photo,about_me "
			+ " FROM `%s`.xe_user_details userDetails "
			+ " INNER JOIN `%s`.xe_role role ON role.role_id=userDetails.role_id"
			+ " INNER join `%s`.xe_active active ON active.active_id = userDetails.user_status"
			+ " INNER JOIN `%s`.xe_active tmStatus ON tmStatus.active_id=userDetails.tm_status"
			+ " INNER JOIN `%s`.xe_active btStatus ON btStatus.active_id=userDetails.bt_status Where userDetails.ldap_status=2 and userDetails.user_status<>3";
	
	public String getAllLdapActiveUserDetails = "SELECT user_id,first_name,last_name,email_id,role.role_type,userDetails.role_id as roleID ,user_name ,ldap_status,"
			+ " active.status userStatus,tmStatus.status tmStatus," + " btStatus.status btStatus,user_photo,about_me "
			+ " FROM `%s`.xe_user_details userDetails "
			+ " INNER JOIN `%s`.xe_role role ON role.role_id=userDetails.role_id"
			+ " INNER join `%s`.xe_active active ON active.active_id = userDetails.user_status"
			+ " INNER JOIN `%s`.xe_active tmStatus ON tmStatus.active_id=userDetails.tm_status"
			+ " INNER JOIN `%s`.xe_active btStatus ON btStatus.active_id=userDetails.bt_status Where userDetails.ldap_status=1 and userDetails.user_status<>3";

	
}
package com.xenon.database;

public class JiraDetails {

	public String insertInstance = "insert into `%s`.xead_jira_details (jira_integ_username, jira_integ_password) "

			+ "values (?,?)";
	public String getInstance="SELECT * from `%s`.xead_jira_details";
	
	public String getJiraRelease="SELECT * from `%s`.xebt_jira_release where jira_release_version_id = ?";
	
	public String updateJiraDetails="UPDATE `%s`.xead_jira_details set jira_integ_username=?,jira_integ_password=? where jira_id=?";
	
	
	public String insertJiraRelease = "insert into `%s`.xebt_jira_release (jira_release_version_name, jira_project_key,"
			+ "release_status,jira_release_start_date,jira_release_date,jira_release_description,jira_release_version_id) "
			+ "values (?,?,?,?,?,?,?)";
	public String updateJiraRelease = "UPDATE `%s`.`xebt_jira_release` SET `xebt_jira_release`.`jira_release_version_name` = ?,`xebt_jira_release`.`jira_project_key` = ?,`xebt_jira_release`.`release_status` = ?,`xebt_jira_release`.`jira_release_date`=?,`xebt_jira_release`.`jira_release_description`=? WHERE `xebt_jira_release`.`jira_release_version_id` = ?";
	
	
}

package com.xenon.database;

public class Utilization {
	private static Utilization instance = null;

	protected Utilization() {

	}

	public static Utilization getInstance() {
		if (instance == null) {
			instance = new Utilization();
		}
		return instance;
	}
	public String updateUsage="CALL `%s`.`insertUtilization`(?,?)";
} 

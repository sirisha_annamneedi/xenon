package com.xenon.database;

public class TestcaseComments {
	private static Testcase instance = null;

	protected TestcaseComments() {

	}

	public static Testcase getInstance() {
		if (instance == null) {
			instance = new Testcase();
		}
		return instance;
	}

	public String getAllTestcaseComments = "SELECT * FROM `%s`.xetm_testcase_comments";
	public String insertTestcaseComment="INSERT INTO `%s`.`xetm_testcase_comments` ( `comment_description`, `commnetor`,`comment_time`, `testcase_id`) VALUES (?,?,NOW(),?);";
	public String getTestcaseCommentByTcId = "SELECT tc.xetm_testcase_comments_id,tc.comment_description,tc.commnetor,DATE_FORMAT(tc.comment_time , '%%d-%%m-%%Y %%h:%%i %%p') as comment_time,tc.testcase_id, CONCAT(xud.first_name, ' ',xud.last_name) as user_name,xud.user_photo,TIMESTAMPDIFF(MINUTE,tc.comment_time,CURRENT_TIME()) AS TimeDiff FROM `%s`.xetm_testcase_comments as tc, `%s`.xe_user_details xud where tc.testcase_id=? and xud.user_id=tc.commnetor ORDER BY xetm_testcase_comments_id DESC";
	public String insertCommentLike="INSERT INTO `%s`.`xetm_comments_likes` ( `comment_id`, `liked_by_user`) VALUES ( ?,?);";
	public String removeCommentLike="DELETE FROM `%s`.xetm_comments_likes WHERE comment_id=? and liked_by_user=?";
	public String getCommentLikeByUserId="SELECT * FROM `%s`.xetm_comments_likes where liked_by_user=?";
	public String getCommentLikeCountByTcId="SELECT count(*) likeCount,comment_id FROM `%s`.xetm_comments_likes where comment_id IN (SELECT xetm_testcase_comments_id FROM `%s`.xetm_testcase_comments where testcase_id=?) GROUP BY comment_id";
	public String insertTestcaseSummary="INSERT INTO `%s`.`xetm_testcase_summary` ( `test_case_id`, `activity_desc`,`activity_date`, `user`) VALUES (?,?,NOW(),?);";

}

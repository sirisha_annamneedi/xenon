package com.xenon.database;


public class Scenario {

	private static Scenario instance = null;

	protected Scenario() {

	}

	public static Scenario getInstance() {
		if (instance == null) {
			instance = new Scenario();
		}
		return instance;
	}
	
	public  String getAllScenarios = "SELECT * FROM `%s`.xetm_scenario where status=1";
	public  String insertScenario = "INSERT INTO `%s`.xetm_scenario (scenario_name,scenario_description,module_id,status,jira_story_id,jira_story_name,fix_version)" + "VALUES(?,?,?,?,?,?,?)";
	public  String scenarioDetails = "select * from `%s`.xetm_scenario where scenario_id = ?";
	public  String updateScenario = "UPDATE `%s`.xetm_scenario  SET `scenario_name`=?, `scenario_description`=?, `jira_story_id`=?, `jira_story_name`=? WHERE `scenario_id`=?";
	public  String updateScenarioStatus = "UPDATE `%s`.xetm_scenario  SET `status`=? WHERE `scenario_id`=?";
	public String createNewScenarioStatus="{?= CALL %s.createNewScenarioStatus(?)}";
	
	public String selectScenarioDetailsByBuildId="SELECT * FROM `%s`.xetm_scenario where scenario_id IN (SELECT scenario_id FROM `%s`.xetm_testcase where testcase_id IN (SELECT testcase_id FROM `%s`.xebm_buildtestcase where build_id=?));";
	
	public String createNewScenario="{?= CALL `%s`.createNewScenario(?,?,?,?)}";
	
	public String getScenarioId = "select * from `%s`.xetm_scenario where scenario_name = ? and module_id =?";
	
	public String getComponentId = "select * from `%s`.xe_components where component_name = ?";
	
	public String getScenarioIds = "select * from `%s`.xetm_scenario where scenario_name = ? and created_date IN (select max(created_date) from `%s`.xetm_scenario where module_id =?);";
	
	public String deleteScenario="delete from `%s`.xetm_scenario where scenario_id=?";
	
	public  String insertComponent = "INSERT INTO `%s`.xe_components (component_name,component_description,component_active)" + "VALUES(?,?,?)";

	public  String insertScenarioComponent = "INSERT INTO `%s`.xe_scenario_components (component_id,scenario_id)" + "VALUES(?,?)";
	
	public  String getComponentName = "SELECT * FROM `%s`.xe_components where component_name=?";
}

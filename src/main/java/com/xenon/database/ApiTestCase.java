package com.xenon.database;

public class ApiTestCase {

	private static ApiTestCase instance = null;

	protected ApiTestCase() {

	}

	public static ApiTestCase getInstance() {
		if (instance == null) {
			instance = new ApiTestCase();
		}
		return instance;
	}
	
	public String insertApiTestCase="CALL `%s`.`insertApiTestCase`(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
}

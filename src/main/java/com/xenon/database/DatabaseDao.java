package com.xenon.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public interface DatabaseDao {
	
	int insert(String sql);

	int update(String sql);
	
	int update(String sql,Object[] obj,int[] data);

	void delete(String sql);

	void createTable(String sql);
	
	List<Map<String, Object>> select(String sql);
	List<Map<String, Object>> select(String sql, Object[] objects, int[] data);
	
	<T> List<T> select(String sql, Object[] args, Class<T> clazz) throws Exception;
	
	SqlRowSet selectRow(String sql, Object[] objects, int[] data);

	Connection getConnection() throws SQLException;
	
	int select(String sql, Object[] objects);
	
	String stringSelect(String sql, Object[] objects);
	
	int update(PreparedStatementCreator preparedStatementCreator,KeyHolder keyHolder);
}

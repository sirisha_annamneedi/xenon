package com.xenon.database;

public class Ldap {

	private static Ldap instance = null;

	protected Ldap() {

	}

	public static Ldap getInstance() {
		if (instance == null) {
			instance = new Ldap();
		}
		return instance;
	}
	
	public String insertLdapDetails = "CALL `xenon-core`.insertLdapDetails(?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public String updateCustomerLdapStatus = "Update `xenon-core`.xe_customer_details SET ldap_status=? WHERE customer_id=?";
	
	public String getCustomerLdapDetails = "SELECT * FROM `xenon-core`.xe_ldap_settings WHERE customer_id=?";
	
	public String updateAllUsersPassword="Update `%s`.xe_user_details SET user_password=sha1('admin@123'),ldap_status=2 , pass_flag=2;";
	
	public String deactivateUnmappedUser = "UPDATE `%s`.`xe_user_details` SET `user_status`='2' WHERE `user_name`=?;";

}

package com.xenon.database;

public class EnvVariableDetails {
public String insertVariables="insert into `%s`.xebm_env_variables(parameter_name,config_val,parameter_val,env_id) values(?,?,?,?)";

public String getEnvVariableData="select * from `%s`.xebm_env_variables where env_id=?";

public String deleteVariable="DELETE FROM `%s`.xebm_env_variables where variable_id=?";

public String updateVariable="UPDATE `%s`.xebm_env_variables SET parameter_name=?,config_val=?,parameter_val=? WHERE variable_id=?";
}

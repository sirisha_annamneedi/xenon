package com.xenon.database;

/**
 * 
 * @author prafulla.pol
 * This class is used to access the FAQ related queries
 *
 */
public class Faq {

	private static Faq instance = null;

	protected Faq() {

	}

	public static Faq getInstance() {
		if (instance == null) {
			instance = new Faq();
		}
		return instance;
	}
	
	public String insertFaq = "INSERT INTO xenonsupport.xes_faq (title,description,create_date,update_date,faq_creator)"
							+ "VALUES (?,?,now(),now(),?)";
	public String viewFaq = "SELECT faq_id,title,description,create_date,update_date,faq_creator"
						 + " From xenonsupport.xes_faq ORDER BY faq_id DESC" ;
	
	public String viewLastFiveFaq = "SELECT faq_id,title,description,create_date,update_date,faq_creator"
			 + " From xenonsupport.xes_faq ORDER BY faq_id DESC LIMIT 5" ;
	
	public String viewSingleFaq = "SELECT faq_id,title,description,create_date,update_date,faq_creator "
								 + "From xenonsupport.xes_faq "
								 + "WHERE faq_id=? ";

	public String getComponent="SELECT * FROM xenonsupport.xes_component";
	
	public String updateFaq = "UPDATE `%s`.`xes_faq` SET `description`= ?, `update_date`= now() WHERE `faq_id`= ?";

}

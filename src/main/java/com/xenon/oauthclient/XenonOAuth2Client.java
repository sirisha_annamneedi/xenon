package com.xenon.oauthclient;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * @author navnath.damale(navnath.damale@jadeglobal.com)
 * 
 */
public class XenonOAuth2Client {

	
	//final static Logger logger = Logger.getLogger(XenonOAuth2Client.class);
	final static Logger logger = LogManager.getLogger(XenonOAuth2Client.class);
	private static XenonOAuth2Client instance;
	private XenonOAuth2Client()
	{
	}
	
	public static XenonOAuth2Client getInstance()
	{
		if(instance==null)
		{
			return new XenonOAuth2Client();
		}
		return instance;
	}
	
	public String getAccessToken()
	{
		String accessToken=null;
		logger.info("Generate the OAuthDetails bean from the config properties file..." );
		OAuth2Details oauthDetails = OAuthUtils.createOAuthDetails();

		logger.info("Validating Input...");
		if(!OAuthUtils.isValidInput(oauthDetails)){
			//System.out.println("Please provide valid config properties to continue.");
			logger.error("Please provide valid config properties to continue.");
			System.exit(0);
		}

		logger.info("Determining operation...");
		if(oauthDetails.isAccessTokenRequest()){

			logger.info("Generate new Access token...");
			accessToken = OAuthUtils.getAccessToken(oauthDetails);
			if(OAuthUtils.isValid(accessToken)){
				////System.out.println("Successfully generated Access token for client_credentials grant_type: "+accessToken);
				logger.info("Successfully generated Access token for client_credentials grant_type: ");
			}
			else{
				logger.info("Could not generate Access token for client_credentials grant_type");
				//System.out.println("Could not generate Access token for client_credentials grant_type");
			}
		}
		return accessToken;
	}
	
}

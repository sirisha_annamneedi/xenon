package com.xenon.oauthclient;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * @author navnath.damale(navnath.damale@jadeglobal.com)
 * 
 */

public class OAuthUtils {
	
	//final static Logger logger = Logger.getLogger(OAuthUtils.class);
	final static Logger logger = LogManager.getLogger(OAuthUtils.class);
	/**
	 * 
	 * @param config
	 * @return
	 */
	public static OAuth2Details createOAuthDetails() {
		OAuth2Details oauthDetails = new OAuth2Details();
		oauthDetails.setAccessToken("");
		oauthDetails.setRefreshToken("refrash_token");
		oauthDetails.setGrantType("password");
		oauthDetails.setClientId("client");
		oauthDetails.setClientSecret("$2a$10$WAQZbr4C58raVdWTVWka7OR/0BnUKBRBfAAQ23fMgLAzOQxvjhYba");
		oauthDetails.setScope("");
		oauthDetails.setAuthenticationServerUrl(OAuthConstants.AUTHENTICATION_SERVER_URL);
		oauthDetails.setUsername("admin@jadeglobal.com");
		oauthDetails.setPassword("admin@123");
		oauthDetails.setResourceServerUrl("");
		oauthDetails.setAuthorization("authenticateUserRequests");

		if (!isValid(oauthDetails.getResourceServerUrl())) {
			logger.warn("Resource server url is null. Will assume request is for generating Access token");
			//System.out.println("Resource server url is null. Will assume request is for generating Access token");
			oauthDetails.setAccessTokenRequest(true);
		}

		return oauthDetails;
	}

	/**
	 * 
	 * @param path
	 * @return
	 */
	public static Properties getClientConfigProps(String path) {
		InputStream is = OAuthUtils.class.getClassLoader().getResourceAsStream(path);
		Properties config = new Properties();
		try {
			config.load(is);
		} catch (IOException e) {
			logger.error("Could not load properties from " + path);
			logger.error("Exception :" + e);
			//System.out.println("Could not load properties from " + path);
			e.printStackTrace();
			return null;
		}
		return config;
	}

	/**
	 * 
	 * @param oauthDetails
	 */
	public static void getProtectedResource(OAuth2Details oauthDetails) {
		String resourceURL = oauthDetails.getResourceServerUrl();

		HttpGet get = new HttpGet(resourceURL);
		get.addHeader(oauthDetails.getAuthorization(),
				getAuthorizationHeaderForAccessToken(oauthDetails.getAccessToken()));
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = null;
		int code = -1;
		try {
			response = client.execute(get);
			code = response.getStatusLine().getStatusCode();
			if (code == 401 || code == 403) {
				// Access token is invalid or expired.Regenerate the access
				// token
				//System.out.println("Access token is invalid or expired. Regenerating access token....");
				String accessToken = getAccessToken(oauthDetails);
				if (isValid(accessToken)) {
					// update the access token
					// //System.out.println("New access token: " + accessToken);
					oauthDetails.setAccessToken(accessToken);
					get.removeHeaders(OAuthConstants.AUTHORIZATION);
					get.addHeader(OAuthConstants.AUTHORIZATION,
							getAuthorizationHeaderForAccessToken(oauthDetails.getAccessToken()));
					get.releaseConnection();
					response = client.execute(get);
					code = response.getStatusLine().getStatusCode();
					if (code >= 400) {
						throw new RuntimeException(
								"Could not access protected resource. Server returned http code: " + code);

					}

				} else {
					throw new RuntimeException("Could not regenerate access token");
				}

			}

			handleResponse(response);

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			get.releaseConnection();
		}
	}

	/**
	 * 
	 * @param oauthDetails
	 * @return
	 */
	public static String getAccessToken(OAuth2Details oauthDetails) {
		HttpPost post = new HttpPost(oauthDetails.getAuthenticationServerUrl());
		String clientId = oauthDetails.getClientId();
		String clientSecret = oauthDetails.getClientSecret();
		String scope = oauthDetails.getScope();
		String username = oauthDetails.getUsername();
		String password = oauthDetails.getPassword();

		List<BasicNameValuePair> parametersBody = new ArrayList<BasicNameValuePair>();
		parametersBody.add(new BasicNameValuePair(OAuthConstants.GRANT_TYPE, oauthDetails.getGrantType()));
		parametersBody.add(new BasicNameValuePair(OAuthConstants.CLIENT_ID, clientId));
		parametersBody.add(new BasicNameValuePair(OAuthConstants.CLIENT_SECRET, clientSecret));
		parametersBody.add(new BasicNameValuePair(OAuthConstants.USERNAME, username));
		parametersBody.add(new BasicNameValuePair(OAuthConstants.PASSWORD, password));

		if (isValid(scope)) {
			parametersBody.add(new BasicNameValuePair(OAuthConstants.SCOPE, scope));
		}

		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = null;
		String accessToken = null;
		try {
			post.setEntity(new UrlEncodedFormEntity(parametersBody, "UTF-8"));

			response = client.execute(post);
			int code = response.getStatusLine().getStatusCode();
			if (code == OAuthConstants.HTTP_UNAUTHORIZED) {
				logger.info("Authorization server expects Basic authentication");
				//System.out.println("Authorization server expects Basic authentication");
				// Add Basic Authorization header
				post.addHeader(OAuthConstants.AUTHORIZATION,
						getBasicAuthorizationHeader(oauthDetails.getClientId(), oauthDetails.getClientSecret()));
				logger.warn("Retry with client credentials");
				//System.out.println("Retry with client credentials");
				post.releaseConnection();
				response = client.execute(post);
				code = response.getStatusLine().getStatusCode();
				if (code == 401 || code == 403) {
					logger.error("Could not authenticate using client credentials.");
					//System.out.println("Could not authenticate using client credentials.");
					logger.error("Could not retrieve access token for client: " + oauthDetails.getClientId());
					throw new RuntimeException(
							"Could not retrieve access token for client: " + oauthDetails.getClientId());
				}

			}
			Map<String, String> map = handleResponse(response);
			accessToken = map.get(OAuthConstants.ACCESS_TOKEN);
		} catch (ClientProtocolException e) {
			logger.error("Exception :" + e);
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("Exception :" + e);
			e.printStackTrace();
		}

		return accessToken;
	}

	/**
	 * 
	 * @param response
	 * @return
	 */
	public static Map<String, String> handleResponse(HttpResponse response) {
		String contentType = OAuthConstants.JSON_CONTENT;
		if (response.getEntity().getContentType() != null) {
			contentType = response.getEntity().getContentType().getValue();
		}
		if (contentType.contains(OAuthConstants.JSON_CONTENT)) {
			return handleJsonResponse(response);
		} else if (contentType.contains(OAuthConstants.URL_ENCODED_CONTENT)) {
			return handleURLEncodedResponse(response);
		} else if (contentType.contains(OAuthConstants.XML_CONTENT)) {
			return handleXMLResponse(response);
		} else {
			logger.error("Cannot handle " + contentType
					+ " content type. Supported content types include JSON, XML and URLEncoded");
			throw new RuntimeException("Cannot handle " + contentType
					+ " content type. Supported content types include JSON, XML and URLEncoded");
		}
	}

	@SuppressWarnings("unchecked")
	public static Map<String, String> handleJsonResponse(HttpResponse response) {
		Map<String, String> oauthLoginResponse = null;

		try {
			oauthLoginResponse = (Map<String, String>) new JSONParser()
					.parse(EntityUtils.toString(response.getEntity()));
		} catch (ParseException e) {
			logger.error("Exception :"+e);
			e.printStackTrace();
			throw new RuntimeException();
		} catch (org.json.simple.parser.ParseException e) {
			logger.error("Exception :"+e);
			e.printStackTrace();
			throw new RuntimeException();
		} catch (IOException e) {
			logger.error("Exception :"+e);
			e.printStackTrace();
			throw new RuntimeException();
		} catch (RuntimeException e) {
			logger.error("Could not parse JSON response");
			//System.out.println("Could not parse JSON response");
			throw e;
		}
		/*//System.out.println();
		//System.out.println("********** Response Received **********");
		for (Map.Entry<String, String> entry : oauthLoginResponse.entrySet()) {
			//System.out.println(String.format("  %s = %s", entry.getKey(), entry.getValue()));
		}*/
		return oauthLoginResponse;
	}

	
	/**
	 * 
	 * @param response
	 * @return
	 */
	@SuppressWarnings("unused")
	public static Map<String, String> handleURLEncodedResponse(HttpResponse response) {
		Map<String, Charset> map = Charset.availableCharsets();
		Map<String, String> oauthResponse = new HashMap<String, String>();
		Set<Map.Entry<String, Charset>> set = map.entrySet();
		HttpEntity entity = response.getEntity();

		/*//System.out.println();
		//System.out.println("********** Response Received **********");
*/
		/*for (Map.Entry<String, Charset> entry : set) {
			//System.out.println(String.format("  %s = %s", entry.getKey(), entry.getValue()));
			if (entry.getKey().equalsIgnoreCase("UTF-8")) {
			}
		}*/

		try {
			List<NameValuePair> list = URLEncodedUtils.parse(EntityUtils.toString(entity), Charset.forName("UTF-8"));
			for (NameValuePair pair : list) {
				//System.out.println(String.format("  %s = %s", pair.getName(), pair.getValue()));
				oauthResponse.put(pair.getName(), pair.getValue());
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("Could not parse URLEncoded Response");
		}

		return oauthResponse;
	}

	
	/**
	 * 
	 * @param response
	 * @return
	 */
	public static Map<String, String> handleXMLResponse(HttpResponse response) {
		Map<String, String> oauthResponse = new HashMap<String, String>();
		try {

			String xmlString = EntityUtils.toString(response.getEntity());
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = factory.newDocumentBuilder();
			InputSource inStream = new InputSource();
			inStream.setCharacterStream(new StringReader(xmlString));
			Document doc = db.parse(inStream);

			//System.out.println("********** Response Receieved **********");
			parseXMLDoc(null, doc, oauthResponse);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Exception occurred while parsing XML response");
		}
		return oauthResponse;
	}

	public static void parseXMLDoc(Element element, Document doc, Map<String, String> oauthResponse) {
		NodeList child = null;
		if (element == null) {
			child = doc.getChildNodes();

		} else {
			child = element.getChildNodes();
		}
		for (int j = 0; j < child.getLength(); j++) {
			if (child.item(j).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				org.w3c.dom.Element childElement = (org.w3c.dom.Element) child.item(j);
				if (childElement.hasChildNodes()) {
					//System.out.println(childElement.getTagName() + " : " + childElement.getTextContent());
					//oauthResponse.put(childElement.getTagName(), childElement.getTextContent());
					parseXMLDoc(childElement, null, oauthResponse);
				}

			}
		}
	}

	
	/**
	 * 
	 * @param accessToken
	 * @return
	 */
	public static String getAuthorizationHeaderForAccessToken(String accessToken) {
		return OAuthConstants.BEARER + " " + accessToken;
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public static String getBasicAuthorizationHeader(String username, String password) {
		return OAuthConstants.BASIC + " " + encodeCredentials(username, password);
	}

	
	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public static String encodeCredentials(String username, String password) {
		String cred = username + ":" + password;
		String encodedValue = null;
		byte[] encodedBytes = Base64.encodeBase64(cred.getBytes());
		encodedValue = new String(encodedBytes);
		//System.out.println("encodedBytes " + new String(encodedBytes));

		byte[] decodedBytes = Base64.decodeBase64(encodedBytes);
		//System.out.println("decodedBytes " + new String(decodedBytes));

		return encodedValue;

	}

	/**
	 * 
	 * @param input
	 * @return
	 */
	public static boolean isValidInput(OAuth2Details input) {

		if (input == null) {
			return false;
		}

		String grantType = input.getGrantType();

		if (!isValid(grantType)) {
			logger.warn("Please provide valid value for grant_type");
			//System.out.println("Please provide valid value for grant_type");
			return false;
		}

		if (!isValid(input.getAuthenticationServerUrl())) {
			logger.warn("Please provide valid value for authentication server url");
			//System.out.println("Please provide valid value for authentication server url");
			return false;
		}

		if (grantType.equals(OAuthConstants.GRANT_TYPE_PASSWORD)) {
			if (!isValid(input.getUsername()) || !isValid(input.getPassword())) {
				logger.warn("Please provide valid username and password for password grant_type");
				//System.out.println("Please provide valid username and password for password grant_type");
				return false;
			}
		}

		if (grantType.equals(OAuthConstants.GRANT_TYPE_CLIENT_CREDENTIALS)) {
			if (!isValid(input.getClientId()) || !isValid(input.getClientSecret())) {
				logger.warn("Please provide valid client_id and client_secret for client_credentials grant_type");
				System.out
						.println("Please provide valid client_id and client_secret for client_credentials grant_type");
				return false;
			}
		}
		logger.warn("Validated Input");
		//System.out.println("Validated Input");
		return true;
	}
	
	/**
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isValid(String str) {
		return (str != null && str.trim().length() > 0);
	}

}

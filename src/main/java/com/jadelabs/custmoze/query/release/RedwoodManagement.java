package com.jadelabs.custmoze.query.release;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;

public class RedwoodManagement {
	public  void createUser(String redwoodUrl,String firstName,String lastName, String emailId) {
		try {
			String url = redwoodUrl+"/saveUser";
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("username",emailId);
			jsonObject.put("name",firstName+" "+lastName);
			jsonObject.put("email",emailId);
			jsonObject.put("role", "Developer");
			jsonObject.put("password", "Welcome@123");
			//System.out.println(jsonObject);
			sendRequestToRedWood(url, jsonObject);
		}catch (Exception e) {
			e.printStackTrace();
		}
			
	}
	public  void createProject(String redwoodUrl,String name) {
		try {
			String url = redwoodUrl+"/saveProject";
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("name",name);
			jsonObject.put("language","Java/Groovy");
			jsonObject.put("template","Java Based Selenium");
			//System.out.println(jsonObject);
			sendRequestToRedWood(url, jsonObject);
		}catch (Exception e) {
			e.printStackTrace();
		}
			
	}
	public void sendRequestToRedWood(String url, JSONObject reqJson) {
		try {
			URL object = new URL(url);
			HttpURLConnection con = (HttpURLConnection) object.openConnection();
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestMethod("POST");
			OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
			wr.write(reqJson.toString());
			wr.flush();
			StringBuilder sb = new StringBuilder();
			int HttpResult = con.getResponseCode();
			if (HttpResult == HttpURLConnection.HTTP_OK) {
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
				String line = null;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();
//				 System.out.println("" + sb.toString());
			} else {
//				 System.out.println(con.getResponseMessage());
			}

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*public static void main(String[] args) {
		try {
			String url = "http://xenondev.jadeglobal.com:3000/saveProject";
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("name","XYX");
			jsonObject.put("language","Java/Groovy");
			jsonObject.put("template","Java Based Selenium");
			//System.out.println(jsonObject);
			new RedwoodManagement().sendRequestToRedWood(url, jsonObject);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	public void runExecutionByCli(String redwoodUrl, String projectName, String user, String testSetName,
            String browser, String executionName) {
		//if( )
		// user : admin : - role = 2
		// other emailId
        try {
            String url = redwoodUrl + "/executionengine/startexecutionCliXenon";
            JSONObject jsonOm = new JSONObject();
            jsonOm.put("project", projectName);
            jsonOm.put("user", user);
            jsonOm.put("testset", testSetName);
            jsonOm.put("browser", browser);
            jsonOm.put("execution_name", executionName);
            jsonOm.put("machine", "127.0.0.1:1");
            sendRequestToRedWood(url, jsonOm);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	/**
	 * shantaram.tupe 
	 * 05-Mar-2020 10:41:58 AM
	 * @param redwoodUrl
	 * @param projectName
	 * @param user
	 * @param testSetName
	 * @param browser
	 * @param executionName
	 * void
	 */
	public void runExecutionByCli2(String redwoodUrl, String projectName, String user, String testSetName,
            String browser, String executionName, String machine) {
		//if( )
		// user : admin : - role = 2
		// other emailId
		AsyncRestTemplate asyncRestTemplate = new AsyncRestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            String url = redwoodUrl + "/executionengine/startexecutionCliXenon";
            JSONObject jsonOm = new JSONObject();
            jsonOm.put("project", projectName);
            jsonOm.put("user", user);
            jsonOm.put("testset", testSetName);
            jsonOm.put("browser", browser);
            jsonOm.put("execution_name", executionName);
            jsonOm.put("machine", machine);
            ListenableFuture<ResponseEntity<String>> response = null;
            response = asyncRestTemplate.postForEntity(url, new HttpEntity<String>(jsonOm.toString(), headers) , String.class);
            response.addCallback( new ListenableFutureCallback<ResponseEntity<String>>() {
				@Override
				public void onSuccess(ResponseEntity<String> responseEntity) {
					// TODO Auto-generated method stub
				}
				@Override
				public void onFailure(Throwable throwable) {
					// TODO Auto-generated method stub
				}
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	/**
	 * shantaram.tupe 
	 * 05-Mar-2020 10:41:46 AM
	 * @param redWoodUrl
	 * @param testSetId
	 * @param testSetName
	 * @param projectName
	 * @param testcaseIds
	 * @return
	 * String
	 */
	public String saveOrUpdateTestSet( String redWoodUrl, String testSetId, String testSetName, String projectName, String[] testcaseIds ) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		JSONObject jsonObject = new JSONObject().put("name", testSetName).put("project", projectName);
		ResponseEntity<String> result = null;
		if( "".equals( testSetId ) ) {
			result = restTemplate.postForEntity(redWoodUrl+"/xenon/testsets", new HttpEntity<String>(jsonObject.toString(), headers), String.class);
			JSONObject jsonResponse = new JSONObject(result.getBody());
			if( /*"".equals( testSetId ) && */ jsonResponse.getBoolean("success") )
				return jsonResponse.getJSONArray("testsets").getJSONObject(0).getString("_id");
		} else {
			jsonObject.put("_id", testSetId);
			JSONArray jsonArray = new JSONArray();
			for( String testcaseId : testcaseIds )
				jsonArray.put( new JSONObject().put("_id", testcaseId) );
			jsonObject.put("testcases", jsonArray);
			restTemplate.exchange(redWoodUrl+"/xenon/testsets/{testSetId}", HttpMethod.PUT, new HttpEntity<String>(jsonObject.toString(), headers), String.class, testSetId);
		}
		return "";
	}
	public void updateProjectCookie( String projectName) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		JSONObject jsonObject = new JSONObject().put("project", projectName);
		ResponseEntity<String> result = restTemplate.postForEntity("http://localhost:3000/updateProjectCookie",new HttpEntity<String>(jsonObject.toString(), headers), String.class);
	}
}

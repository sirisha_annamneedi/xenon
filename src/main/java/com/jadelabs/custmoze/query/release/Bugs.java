package com.jadelabs.custmoze.query.release;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Bugs {
	private String query = "SELECT * FROM xenon.xebt_bugsummary as summary where summary.bs_id in ( select max(bs_id) from xebt_bugsummary as bySummary group by bug_id ORDER BY summary.update_date) and summary.bug_id  in (SELECT bug_id FROM xenon.xe_step_execution_bug as manualBug where manualBug.build_id in ( ";
	public String getBugQuery(String currentQuery) {
		if (currentQuery.isEmpty()) {
			query = "SELECT build.build_id FROM xenon.xe_build as build ";
		} else {
			query = query+currentQuery;
		}
		return query;
	}
	public static void main(String[] args) {
		
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		System.out.println(timeStamp);
	}
}

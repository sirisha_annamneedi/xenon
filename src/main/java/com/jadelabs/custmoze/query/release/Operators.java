package com.jadelabs.custmoze.query.release;

public class Operators {

	private enum operator {
		is, isnot
	};

	public String getOperator(String operatorVal) {
		String value = "" ;
		switch (operator.valueOf(operatorVal.toLowerCase())) {
		case is : 
			value = " in " ;
			break ;
		case isnot : 
			value = " not in " ;
			break;
		default:
			break;

		}
		
		return value;
	}
}

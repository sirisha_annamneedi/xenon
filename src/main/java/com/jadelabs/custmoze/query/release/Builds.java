package com.jadelabs.custmoze.query.release;


public class Builds {
	private String query = "";
	private Queryparser parscer = new Queryparser();

	public String getManualQuery(String operator, String data, boolean append, String currentQuery,
			String joinOperator) {
		if (!append) {
			if (data.isEmpty()) {
				query = "SELECT build.build_id FROM xenon.xe_build as build ";
			} else {
				query = "SELECT build.build_id FROM xenon.xe_build as build where build.build_id " + parscer.parseQuery(operator, data);
			}
		} else {
			query = currentQuery + joinOperator + " build.build_id " + parscer.parseQuery(operator, data);
		}
		return query;
	}
}

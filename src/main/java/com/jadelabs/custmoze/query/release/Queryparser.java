package com.jadelabs.custmoze.query.release;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

public class Queryparser {

	private Operators operator = new Operators();
	private String condition = "";

	public String parseQuery(String opt, String data) {
		opt = operator.getOperator(opt);
		condition = opt + "( " + data + ")";
		return condition;
	}

	public HashMap<String, List<JSONObject>> readQuery(JSONObject jsonObj,
			HashMap<String, List<JSONObject>> queryData) {
		String parameter = jsonObj.getString("parameter");
		List<JSONObject> tmp = new ArrayList<JSONObject>();

		tmp = new ArrayList<JSONObject>();

		if (parameter.equalsIgnoreCase("release")) {
			tmp = queryData.get("release");
			tmp.add(jsonObj);
		} else if (parameter.equalsIgnoreCase("build")) {
			tmp = new ArrayList<JSONObject>();
			tmp = queryData.get("build");
			tmp.add(jsonObj);
		} else if (parameter.equalsIgnoreCase("status")) {
			tmp = new ArrayList<JSONObject>();
			tmp = queryData.get("status");
			tmp.add(jsonObj);
		} else if (parameter.equalsIgnoreCase("priority")) {
			tmp = new ArrayList<JSONObject>();
			tmp = queryData.get("priority");
			tmp.add(jsonObj);
		} else if (parameter.equalsIgnoreCase("severity")) {
			tmp = new ArrayList<JSONObject>();
			tmp = queryData.get("serverity");
			tmp.add(jsonObj);
		} else if (parameter.equalsIgnoreCase("category")) {
			tmp = new ArrayList<JSONObject>();
			tmp = queryData.get("category");
			tmp.add(jsonObj);
		} else if (parameter.equalsIgnoreCase("project")) {
			tmp = new ArrayList<JSONObject>();
			tmp = queryData.get("project");
			tmp.add(jsonObj);
		} else if (parameter.equalsIgnoreCase("source")) {
			tmp = new ArrayList<JSONObject>();
			tmp = queryData.get("source");
			tmp.add(jsonObj);
		}

		return queryData;
	}
}

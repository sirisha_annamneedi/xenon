package com.jadelabs.custmoze.query.release;

public class Release {
	private String query = "";
	private Queryparser parscer = new Queryparser();

	public String getReleaseQuery(String operator, String data, String schema) {
		query = "";
		if (data.isEmpty() || operator.equalsIgnoreCase("any")) {
			query = "SELECT release_id FROM xe_release";
			query = String.format(query, schema);
		} else {
			query = "SELECT release_id FROM xe_release where release_id " + parscer.parseQuery(operator, data);
			query = String.format(query, schema);
		}
		return query;
	}

	public String getReleaseQuery(String operator, String data, String currentQuery, String joinOperator,
			boolean append, String schema) {
		query = "";
		if (!append) {
			if (data.isEmpty() || operator.equalsIgnoreCase("any")) {
				query = "SELECT release_id FROM %s.xe_release";
			} else {
				query = "SELECT release_id FROM %s.xe_release where release_id " + parscer.parseQuery(operator, data);
			}

		} else {
			query = currentQuery + " " + joinOperator + " release_id " + parscer.parseQuery(operator, data);
		}
		query = String.format(query, schema);
		return query;
	}

	public String getBuildQuery(String operator, String data, String currentQuery, String joinOperator, boolean append,
			String schema) {
		query = "";
		if (!append) {
			if (data.isEmpty() || operator.equalsIgnoreCase("any")) {
				query = "SELECT build.build_id FROM `%s`.xe_build as build ";
			} else {
				query = "SELECT build.build_id FROM `%s`.xe_build as build where build.build_id "
						+ parscer.parseQuery(operator, data);
			}
		} else {
			query = currentQuery + " " + joinOperator + " build.build_id " + parscer.parseQuery(operator, data);
		}
		query = String.format(query, schema);
		return query;
	}

	
	
	public String getAutoBuildQuery(String operator, String data, String currentQuery, String joinOperator, boolean append,
			String schema) {
		query = "";
		if (!append) {
			if (data.isEmpty() || operator.equalsIgnoreCase("any")) {
				query = "SELECT build.build_id FROM `%s`.xe_automation_build as build ";
			} else {
				query = "SELECT build.build_id FROM `%s`.xe_automation_build as build where build.build_id "
						+ parscer.parseQuery(operator, data);
			}
		} else {
			query = currentQuery + " " + joinOperator + " build.build_id " + parscer.parseQuery(operator, data);
		}
		query = String.format(query, schema);
		return query;
	}
	
	public String getStatusQuery(String operator, String data, String schema) {
		query = " ";
		if (operator.equalsIgnoreCase("any")) {
			query = " ";
		} else {
			query = " bug_status " + parscer.parseQuery(operator, data);
		}
		query = String.format(query, schema);
		return query;
	}

	public String getPriorityQuery(String operator, String data, String schema) {
		query = "";

		if (operator.equalsIgnoreCase("any")) {
			query = " ";
		} else {
			query = " bug_priority " + parscer.parseQuery(operator, data);
		}

		query = String.format(query, schema);
		return query;
	}

	public String getServerityQuery(String operator, String data, String schema) {
		query = "";

		if (operator.equalsIgnoreCase("any")) {
			query = " ";
		} else {
			query = " bug_severity " + parscer.parseQuery(operator, data);
		}

		query = String.format(query, schema);
		return query;
	}

	public String getCategoryQuery(String operator, String data, String schema) {
		query = "";

		if (operator.equalsIgnoreCase("any")) {
			query = " ";
		} else {
			query = " category " + parscer.parseQuery(operator, data);
		}
		query = String.format(query, schema);
		return query;

	}

	public String getProjectQuery(String operator, String data,String schema) {
		query = "";

		if (operator.equalsIgnoreCase("any")) {
			query = " ";
		} else {
			query = " bugdt.project " + parscer.parseQuery(operator, data);
		}
		query = String.format(query, schema);
		return query;
	}

	public String getSourceData(String operator, String data, String currentQuery, String joinOperator, boolean append,
			String schema) {
		query = "";

		if (data.equalsIgnoreCase("status")) {
			query = "stat.bug_status";
		} else if (data.equalsIgnoreCase("status")) {
			query = "stat.bug_status";
		} else if (data.equalsIgnoreCase("priority")) {
			query = "priority.bug_priority";
		} else if (data.equalsIgnoreCase("severity")) {
			query = "severity.bug_severity";
		} else if (data.equalsIgnoreCase("category")) {
			query = "category.category";
		}

		return query;
	}
}

package com.jadelabs.custmoze.query.release;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

public class CreateQuery {
	private Release rel = new Release();
	private String releaseQuery = "";
	private String buildQuery = "";
	private String autoBuildQuery = "";
	private String statusQuery = "";
	private String priorityQuery = "";
	private String serverityQuery = "";
	// private String categoryQuery = "";

	private String manualDefaultOperator = "";
	private String autoDefaultOperator = " ";
	private String orQueryValue = "";
	private String andQueryValue = "";
	private String projectQuery = "";
	private String aliasQuery = "select manualBuildBugs.bug_id,bug_prefix,bug_title,bugStatus,bugPriority,bugSeverity,category_name,update_date,assigneeName,UpdaterName from ( SELECT bugdt.bug_id,bugdt.bug_prefix, bugdt.bug_title,stat.bug_status bugStatus,priority.bug_priority bugPriority,severity.bug_severity as bugSeverity,category.category_name,summaryData.update_date,CONCAT(xud.first_name ,\" \",xud.last_name) as assigneeName,CONCAT(xu.first_name ,\" \",xu.last_name) as UpdaterName FROM (";
	private String mainbugsQuery = "SELECT bug_id,bug_status,bug_priority,bug_severity,assignee,category,updated_by,update_date FROM `%s`.xebt_bugsummary as summary where summary.bs_id in (select max(bs_id) from `%s`.xebt_bugsummary as bySummary group by bug_id ORDER BY summary.update_date) ";
	private String manualQuery = "summary.bug_id  in ( SELECT bug_id FROM `%s`.xe_step_execution_bug as manualBug where manualBug.build_id in (";
	private String automationQuery = "summary.bug_id  in ( SELECT bug_id FROM `%s`.xe_automation_step_execution_bug as autoBug where autoBug.build_id in (";

	private String aliasEndQuery = " ) as summaryData, `%s`.xebt_status as stat,`%s`.xebt_priority as priority , `%s`.xebt_severity as severity, `%s`.xebt_category as category ,`%s`.xe_user_details xud ,`%s`.xe_user_details xu ,`%s`.xebt_bug as bugdt where  summaryData.bug_status = stat.status_id and priority.priority_id = summaryData.bug_priority and summaryData.category = category.category_id and severity.severity_id = summaryData.bug_severity and xud.user_id = summaryData.assignee and xu.user_id = summaryData.updated_by and bugdt.bug_id = summaryData.bug_id )as manualBuildBugs inner Join ( ";
	private String aliasORQuery = "SELECT bug_id FROM (select * from (select * from (select xbs.bug_id, xb.project,@moduleId:=xb.module as module,xbs.bug_status,xbs.bug_priority,xbs.bug_severity, xbs.category,xum.user_id  from `%s`.xebt_bug xb,`%s`.xebt_bugsummary xbs,`%s`.xe_project xp,`%s`.xe_module xm ,`%s`.xe_user_module xum,`%s`.xe_user_details xu,`%s`.xe_user_details xud,`%s`.xebt_status as stat,`%s`.xebt_priority as priority , `%s`.xebt_severity as severity, `%s`.xebt_category as category where xb.module IN (SELECT module_id FROM `%s`.xe_user_module  xum WHERE xum.user_id=%s) AND xb.project IN (SELECT project_id FROM `%s`.xe_user_project xup WHERE xup.user=%s and xup.project_id=%s)  AND xb.bug_id=xbs.bug_id and xb.project=xp.project_id and xb.module = xm.module_id and xum.module_id = xm.module_id and  xu.user_id = xb.created_by and xud.user_id = xbs.assignee and xbs.bug_status = stat.status_id and  priority.priority_id = xbs.bug_priority and xbs.category = category.category_id and severity.severity_id = xbs.bug_severity and xp.project_active = 1 and xm.module_active = 1 order by bs_id desc ) as table1 group by bug_id)as table2 order by project) as allBugs";

	private String endORQuery = ")as table1 on table1.bug_id = manualBuildBugs.bug_id";
	private String aliasOrQueryBugs = "SELECT * FROM (select * from (select * from (select xbs.bug_id, xb.bug_prefix, xb.bug_title, xb.project, @moduleId:=xb.module AS module,xbs.bug_status,xbs.bug_priority , xbs.bug_severity,stat.bug_status AS bugStatus, priority.bug_priority AS bugPriority, severity.bug_severity AS bugSeverity, xbs.update_date, category.category_name, CONCAT(xud.first_name, ' ', xud.last_name) AS assigneeName, CONCAT(xu.first_name, ' ', xu.last_name) AS UpdaterName  from `%s`.xebt_bug xb,`%s`.xebt_bugsummary xbs,`%s`.xe_project xp,`%s`.xe_module xm ,`%s`.xe_user_module xum,`%s`.xe_user_details xu,`%s`.xe_user_details xud,`%s`.xebt_status as stat,`%s`.xebt_priority as priority , `%s`.xebt_severity as severity, `%s`.xebt_category as category where xb.module IN (SELECT module_id FROM `%s`.xe_user_module  xum WHERE xum.user_id=%s) AND xb.project IN (SELECT project_id FROM `%s`.xe_user_project xup WHERE xup.user=%s and xup.project_id=%s)  AND xb.bug_id=xbs.bug_id and xb.project=xp.project_id and xb.module = xm.module_id and xum.module_id = xm.module_id and  xu.user_id = xb.created_by and xud.user_id = xbs.assignee and xbs.bug_status = stat.status_id and  priority.priority_id = xbs.bug_priority and xbs.category = category.category_id and severity.severity_id = xbs.bug_severity and xp.project_active = 1 and xm.module_active = 1 order by bs_id desc ) as table1 group by bug_id)as table2 order by project) as allBugs where ";

	public Map<String, String> getNewQuery(HashMap<String, List<JSONObject>> queryData, String schema, int userId,
			int projectId) {

		List<JSONObject> release = new ArrayList<JSONObject>();
		List<JSONObject> buildTemp = new ArrayList<JSONObject>();
		List<JSONObject> statusTemp = new ArrayList<JSONObject>();
		List<JSONObject> priorityTemp = new ArrayList<JSONObject>();
		List<JSONObject> serverityTemp = new ArrayList<JSONObject>();
		List<JSONObject> categoryTemp = new ArrayList<JSONObject>();
		List<JSONObject> projectTemp = new ArrayList<JSONObject>();
		List<String> orquery = new ArrayList<String>();
		List<String> andquery = new ArrayList<String>();

		Map<String, String> queries = new HashMap<String, String>();

		release = queryData.get("release");
		buildTemp = queryData.get("build");
		statusTemp = queryData.get("status");
		priorityTemp = queryData.get("priority");
		serverityTemp = queryData.get("serverity");
		categoryTemp = queryData.get("category");
		projectTemp = queryData.get("project");

		String currentQuery = "";
		boolean append = false;
		for (JSONObject jsonObj : release) {
			System.out.println(jsonObj.getString("operator") + "\t" + jsonObj.getString("parameter") + "\t"
					+ jsonObj.getString("condition") + "\t" + jsonObj.getString("selectedData"));
			currentQuery = rel.getReleaseQuery(jsonObj.getString("condition"), jsonObj.getString("selectedData"),
					currentQuery, jsonObj.getString("operator"), append, schema);
			append = true;
		}
		if (currentQuery.isEmpty()) {
			releaseQuery = " build.release_id in (SELECT release_id FROM `%s`.xe_release)";
			releaseQuery = String.format(releaseQuery, schema);

		} else {
			releaseQuery = " build.release_id in (" + currentQuery + ")";
		}
		append = false;
		currentQuery = "";
		boolean manualBuildPresent = false;
		for (JSONObject jsonObj : buildTemp) {

			System.out.println(jsonObj.getString("operator") + "\t" + jsonObj.getString("parameter") + "\t"
					+ jsonObj.getString("condition") + "\t" + jsonObj.getString("selectedData") + "\t"
					+ jsonObj.getString("buildType"));

			if (jsonObj.getString("buildType").equalsIgnoreCase("1")) {
				if (jsonObj.getString("operator").isEmpty() && manualDefaultOperator.isEmpty()) {
					manualDefaultOperator = " and ";
				} else if (manualDefaultOperator.isEmpty()) {
					manualDefaultOperator = jsonObj.getString("operator") + " ";
				}
				currentQuery = rel.getBuildQuery(jsonObj.getString("condition"), jsonObj.getString("selectedData"),
						currentQuery, jsonObj.getString("operator"), append, schema);
				append = true;
				manualBuildPresent = true;
			}
		}
		if (currentQuery.isEmpty()) {
			buildQuery = "SELECT build.build_id FROM `%s`.xe_build as build where ";
			buildQuery = String.format(buildQuery, schema);
		} else {
			buildQuery = currentQuery + " and ";
		}

		append = false;
		currentQuery = "";
		boolean autoBuildPresent = false;
		autoDefaultOperator = " OR ";

		for (JSONObject jsonObj : buildTemp) {

			System.out.println(jsonObj.getString("operator") + "\t" + jsonObj.getString("parameter") + "\t"
					+ jsonObj.getString("condition") + "\t" + jsonObj.getString("selectedData") + "\t"
					+ jsonObj.getString("buildType"));

			if (jsonObj.getString("buildType").equalsIgnoreCase("2")) {
				autoBuildPresent = true;
				if (jsonObj.getString("operator").isEmpty() && autoDefaultOperator.isEmpty()) {
					autoDefaultOperator = " and ";
				} else {
					autoDefaultOperator = jsonObj.getString("operator") + " ";
				}
				currentQuery = rel.getAutoBuildQuery(jsonObj.getString("condition"), jsonObj.getString("selectedData"),
						currentQuery, jsonObj.getString("operator"), append, schema);
				append = true;
			}
		}
		if (currentQuery.isEmpty()) {
			autoBuildQuery = "SELECT build.build_id FROM `%s`.xe_automation_build as build where ";
			autoBuildQuery = String.format(autoBuildQuery, schema);
		} else {
			autoBuildQuery = currentQuery + " and ";
		}

		for (JSONObject jsonObj : statusTemp) {
			currentQuery = "";
			System.out.println(jsonObj.getString("operator") + "\t" + jsonObj.getString("parameter") + "\t"
					+ jsonObj.getString("condition") + "\t" + jsonObj.getString("selectedData") + "\t"
					+ jsonObj.getString("buildType"));
			currentQuery = rel.getStatusQuery(jsonObj.getString("condition"), jsonObj.getString("selectedData"),
					schema);
			statusQuery = currentQuery;

			if (jsonObj.getString("operator").equalsIgnoreCase("or")) {
				orquery.add(currentQuery);
			} else if (jsonObj.getString("operator").equalsIgnoreCase("and")
					|| jsonObj.getString("operator").isEmpty()) {
				andquery.add(currentQuery);
			}

		}

		for (JSONObject jsonObj : priorityTemp) {
			currentQuery = "";
			System.out.println(jsonObj.getString("operator") + "\t" + jsonObj.getString("parameter") + "\t"
					+ jsonObj.getString("condition") + "\t" + jsonObj.getString("selectedData") + "\t"
					+ jsonObj.getString("buildType"));
			currentQuery = rel.getPriorityQuery(jsonObj.getString("condition"), jsonObj.getString("selectedData"),
					schema);
			if (jsonObj.getString("operator").equalsIgnoreCase("or")) {
				orquery.add(currentQuery);
			} else if (jsonObj.getString("operator").equalsIgnoreCase("and")
					|| jsonObj.getString("operator").isEmpty()) {
				andquery.add(currentQuery);
			}
		}

		for (JSONObject jsonObj : serverityTemp) {
			currentQuery = "";
			System.out.println(jsonObj.getString("operator") + "\t" + jsonObj.getString("parameter") + "\t"
					+ jsonObj.getString("condition") + "\t" + jsonObj.getString("selectedData") + "\t"
					+ jsonObj.getString("buildType"));
			currentQuery = rel.getServerityQuery(jsonObj.getString("condition"), jsonObj.getString("selectedData"),
					schema);
			if (jsonObj.getString("operator").equalsIgnoreCase("or")) {
				orquery.add(currentQuery);
			} else if (jsonObj.getString("operator").equalsIgnoreCase("and")
					|| jsonObj.getString("operator").isEmpty()) {
				andquery.add(currentQuery);
			}
		}

		for (JSONObject jsonObj : categoryTemp) {
			currentQuery = "";
			System.out.println(jsonObj.getString("operator") + "\t" + jsonObj.getString("parameter") + "\t"
					+ jsonObj.getString("condition") + "\t" + jsonObj.getString("selectedData") + "\t"
					+ jsonObj.getString("buildType"));
			currentQuery = rel.getCategoryQuery(jsonObj.getString("condition"), jsonObj.getString("selectedData"),
					schema);
			if (jsonObj.getString("operator").equalsIgnoreCase("or")) {
				orquery.add(currentQuery);
			} else if (jsonObj.getString("operator").equalsIgnoreCase("and")
					|| jsonObj.getString("operator").isEmpty()) {
				andquery.add(currentQuery);
			}
		}

		for (JSONObject jsonObj : projectTemp) {
			currentQuery = "";
			System.out.println(jsonObj.getString("operator") + "\t" + jsonObj.getString("parameter") + "\t"
					+ jsonObj.getString("condition") + "\t" + jsonObj.getString("selectedData") + "\t"
					+ jsonObj.getString("buildType"));
			currentQuery = rel.getProjectQuery(jsonObj.getString("condition"), jsonObj.getString("selectedData"),
					schema);
			if (jsonObj.getString("operator").equalsIgnoreCase("or")) {
				orquery.add(currentQuery);
			} else if (jsonObj.getString("operator").equalsIgnoreCase("and")) {
				andquery.add(currentQuery);
			}
		}

		for (String query : orquery) {
			if (orQueryValue.isEmpty()) {
				orQueryValue = orQueryValue + " ( " + query;
			} else {
				orQueryValue = orQueryValue + " OR " + query;
			}
		}

		for (String query : andquery) {
			if (andQueryValue.isEmpty()) {
				andQueryValue = andQueryValue + "(" + query;
			} else {
				andQueryValue = andQueryValue + " AND " + query;
			}
		}
		if (!orQueryValue.isEmpty()) {
			if (!orQueryValue.isEmpty()) {
				orQueryValue = "  " + orQueryValue;
			}
			orQueryValue = orQueryValue + ")";
		}

		if (!andQueryValue.isEmpty()) {
			if (!orQueryValue.isEmpty()) {
				andQueryValue = " and " + andQueryValue;
			}
			andQueryValue = andQueryValue + ")";
		}

		if (!andQueryValue.isEmpty() || !orQueryValue.isEmpty()) {
			aliasORQuery = aliasORQuery + " where ";
		}

		String maque = "", autoque = "";
		/*
		 * if(release.isEmpty() && !manualBuildPresent){
		 * autoDefaultOperator=" and "; }
		 */
		maque = manualDefaultOperator + String.format(manualQuery, schema) + String.format(buildQuery, schema)
				+ releaseQuery + "))";
		// }
		if (!release.isEmpty() || autoBuildPresent) {
			autoque = autoDefaultOperator + String.format(automationQuery, schema)
					+ String.format(autoBuildQuery, schema) + releaseQuery + "))";
		} else {
			autoque = "";
		}

		if (!release.isEmpty() || manualBuildPresent) {
			maque = manualDefaultOperator + String.format(manualQuery, schema) + String.format(buildQuery, schema)
					+ releaseQuery + "))";
		} else {
			maque = "";
		}

		if (release.isEmpty() && statusQuery.isEmpty() && priorityQuery.isEmpty() && serverityQuery.isEmpty()
				&& !manualBuildPresent && autoBuildPresent) {
			autoque = "  and  " + String.format(automationQuery, schema) + String.format(autoBuildQuery, schema)
					+ releaseQuery + "))";
		}

		/*
		 * if (!manualBuildPresent) autoque = " and " + autoque; // }
		 */
		if (release.isEmpty() && buildTemp.isEmpty()) {
			currentQuery = String.format(aliasOrQueryBugs, schema, schema, schema, schema, schema, schema, schema,
					schema, schema, schema, schema, schema, userId, schema, userId, projectId) + orQueryValue
					+ andQueryValue;
		} else if (buildTemp.isEmpty() && statusQuery.isEmpty() && priorityQuery.isEmpty()
				&& serverityQuery.isEmpty()) {

			currentQuery = aliasQuery + String.format(mainbugsQuery, schema, schema) + " and " + maque + " OR "
					+ String.format(automationQuery, schema) + String.format(autoBuildQuery, schema) + releaseQuery
					+ "))" + String.format(aliasEndQuery, schema, schema, schema, schema, schema, schema, schema)
					+ String.format(aliasORQuery, schema, schema, schema, schema, schema, schema, schema, schema,
							schema, schema, schema, schema, userId, schema, userId, projectId)
					+ orQueryValue + andQueryValue + endORQuery;
		}

		else if (statusQuery.isEmpty() && priorityQuery.isEmpty() && serverityQuery.isEmpty()) {

			currentQuery = aliasQuery

					+ String.format(mainbugsQuery, schema, schema) + maque + autoque

					+ String.format(aliasEndQuery, schema, schema, schema, schema, schema, schema, schema)
					+ String.format(aliasORQuery, schema, schema, schema, schema, schema, schema, schema, schema,
							schema, schema, schema, schema, userId, schema, userId, projectId)
					+ endORQuery;
		} else {

			if (manualDefaultOperator.isEmpty()&& autoBuildPresent) {
				currentQuery = aliasQuery + String.format(mainbugsQuery, schema, schema) + maque + autoque
						+ String.format(aliasEndQuery, schema, schema, schema, schema, schema, schema, schema)
						+ String.format(aliasORQuery, schema, schema, schema, schema, schema, schema, schema, schema,
								schema, schema, schema, schema, userId, schema, userId, projectId)
						+ orQueryValue + andQueryValue + endORQuery;

			}else if (manualDefaultOperator.isEmpty()) {
				currentQuery = aliasQuery + String.format(mainbugsQuery, schema, schema) + " and " + maque + autoque
						+ String.format(aliasEndQuery, schema, schema, schema, schema, schema, schema, schema)
						+ String.format(aliasORQuery, schema, schema, schema, schema, schema, schema, schema, schema,
								schema, schema, schema, schema, userId, schema, userId, projectId)
						+ orQueryValue + andQueryValue + endORQuery;

			} 
			
			else {

				currentQuery = aliasQuery + String.format(mainbugsQuery, schema, schema) + maque + autoque
						+ String.format(aliasEndQuery, schema, schema, schema, schema, schema, schema, schema)
						+ String.format(aliasORQuery, schema, schema, schema, schema, schema, schema, schema, schema,
								schema, schema, schema, schema, userId, schema, userId, projectId)
						+ orQueryValue + andQueryValue + endORQuery;
			}

		}

		System.out.println("------------" + currentQuery);
		System.out.println(
				"\n===================================================================================================");

		System.out.println("Status Pie query = " + getStatusPieStartQuery(currentQuery));

		System.out.println(
				"\n===================================================================================================");
		System.out.println("Priority Pie query = " + getPriorityPieStartQuery(currentQuery));

		System.out.println(
				"\n===================================================================================================");
		System.out.println("Severity Pie query = " + getSeverityPieStartQuery(currentQuery));

		System.out.println(
				"\n===================================================================================================");
		queries.put("currentQuery", currentQuery);
		queries.put("severityQuery", getSeverityPieStartQuery(currentQuery));
		queries.put("priorityQuery", getPriorityPieStartQuery(currentQuery));
		queries.put("statusQuery", getStatusPieStartQuery(currentQuery));
		return queries;
	}

	public String getStatusPieStartQuery(String currentQuery) {
		String statusPieStart = "SELECT count(*) As Count,bugStatus FROM (";
		String statusPieEnd = ") as piechart group by bugStatus";

		return statusPieStart + currentQuery + statusPieEnd;
	}

	public String getPriorityPieStartQuery(String currentQuery) {
		String priorityPieStart = "SELECT count(*)  As Count,bugPriority FROM (";
		String priorityPieEnd = ") as piechart group by bugPriority";

		return priorityPieStart + currentQuery + priorityPieEnd;
	}

	public String getSeverityPieStartQuery(String currentQuery) {
		String severityPieStart = "SELECT count(*)  As Count,bugSeverity FROM (";
		String severityPieEnd = ") as piechart group by bugSeverity";

		return severityPieStart + currentQuery + severityPieEnd;
	}

}
